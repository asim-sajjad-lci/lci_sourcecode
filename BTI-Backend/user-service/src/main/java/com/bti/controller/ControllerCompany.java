/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.controller;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bti.authentication.SessionManager;
import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.Company;
import com.bti.model.UserSession;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoCompanyStats;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCompany;
import com.bti.service.ServiceCompany;
import com.bti.service.ServiceResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Description: ControllerCompany
 * Name of Project: BTI
 * Created on: May 17, 2017
 * Modified on: May 17, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/company")
public class ControllerCompany {

	@Autowired
	ServiceCompany serviceCompnay;

	@Autowired
	SessionManager sessionManager;

	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryCompany repositoryCompany;
	
	/**
	 * @author ALI
	 * @description create company with image
	 * @param request
	 * @param companyData
	 * @param image
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	
	@RequestMapping(value="/create", method= RequestMethod.POST)
	public ResponseMessage createCompany(HttpServletRequest request, @RequestParam(name="company") String companyData, @RequestParam(name="image", required=false) MultipartFile image) throws JsonParseException, JsonMappingException, IOException {
		ResponseMessage responseMessage = null;
		UserSession sesion = sessionManager.validateUserSessionId(request);
		if(sesion != null) {
			ObjectMapper mapping = new ObjectMapper();
			DtoCompany dtoCompany = mapping.readValue(companyData, DtoCompany.class);
			Company company = repositoryCompany.findTop1ByIsDeletedAndIsActiveAndName(false, true,dtoCompany.getName());
			if(company == null) {
				dtoCompany = serviceCompnay.saveOrUpdateCompany(dtoCompany, image);
				if(dtoCompany != null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED, 
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_CREATED, false),dtoCompany);
				}else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, 
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_NOT_CREATED, false));
				}
			}else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND, 
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
			}
		}else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED, 
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	

	
//	/**
//	 * @description : Create Company
//	 * @param request
//	 * @param dtoCompany
//	 * @return
//	 */
//	@RequestMapping(value = "/create", method = RequestMethod.POST)
//	public ResponseMessage createCompany(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
//		ResponseMessage responseMessage = null;
//		UserSession session = sessionManager.validateUserSessionId(request);
//		if (session != null) {
//			
//			Company company=repositoryCompany.findTop1ByIsDeletedAndIsActiveAndName(false,true,dtoCompany.getName());
//			if(company == null){
//			dtoCompany = serviceCompnay.saveOrUpdateCompany(dtoCompany);
//			if (dtoCompany != null) {
//				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
//						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_CREATED, false), dtoCompany);
//			} else {
//				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
//						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_NOT_CREATED, false), dtoCompany);
//			}
//		}
//		else
//		{
//			responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
//					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),
//					dtoCompany);
//		}
//			
//		} else {
//			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
//					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
//		}
//		return responseMessage;
//	}
	
	/**
	 * @description : Get all companies
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getAll", method = RequestMethod.PUT)
	public ResponseMessage getAllCompany(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch = serviceCompnay.getAllCompany(dtoCompany);
			if (dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_GET_ALL, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_LIST_NOT_GETTING, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Company List For Drop Down
	 * @param request
	 * @param dtoCompany
	 * @return
	 */
	@RequestMapping(value = "/getCompanyListForDropDown", method = RequestMethod.PUT)
	public ResponseMessage getCompanyListForDropDown(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoSearch dtoSearch = serviceCompnay.getAllCompanyListForDropDown(dtoCompany);
			if (dtoSearch.getRecords() != null) {
				@SuppressWarnings("unchecked")
				List<DtoCompany> list = (List<DtoCompany>) dtoSearch.getRecords();
				if (list!=null && !list.isEmpty()) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_GET_ALL, false), dtoSearch);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));

				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ERROR_OCCURED, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}

	/**
	 * @description : Update Company
	 * @param request
	 * @param dtoCompany
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseMessage updateCompany(HttpServletRequest request,@RequestParam(name="company") String companyData, @RequestParam(name="image", required=false) MultipartFile image) throws JsonParseException, JsonMappingException, IOException  {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			ObjectMapper mapping = new ObjectMapper();
			DtoCompany dtoCompany = mapping.readValue(companyData, DtoCompany.class);
			dtoCompany = serviceCompnay.saveOrUpdateCompany(dtoCompany, image);
			if (dtoCompany != null) {
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_UPDATE_SUCCESS, false), dtoCompany);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_LIST_NOT_GETTING, false), dtoCompany);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
//	/**
//	 * @description : Update Company
//	 * @param request
//	 * @param dtoCompany
//	 * @return
//	 */
//	@RequestMapping(value = "/update", method = RequestMethod.POST)
//	public ResponseMessage updateCompany(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
//		ResponseMessage responseMessage = null;
//		UserSession session = sessionManager.validateUserSessionId(request);
//		if (session != null) {
//			dtoCompany = serviceCompnay.saveOrUpdateCompany(dtoCompany);
//			if (dtoCompany != null) {
//				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
//						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_UPDATE_SUCCESS, false), dtoCompany);
//			} else {
//				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
//						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_LIST_NOT_GETTING, false), dtoCompany);
//			}
//		} else {
//			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
//					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
//		}
//		return responseMessage;
//	}
	
	/**
	 * @description : Delete company
	 * @param request
	 * @param dtoCompany
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.PUT)
	public ResponseMessage deleteCompany(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			if (dtoCompany.getIds() != null && !dtoCompany.getIds().isEmpty()) {
				DtoCompany dtoCompany2 = serviceCompnay.deleteCompany(dtoCompany.getIds());
				responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_DELETED, false), dtoCompany2);

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.LIST_IS_EMPTY, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Company By Id
	 * @param request
	 * @param dtoCompany
	 * @return
	 */
	@RequestMapping(value = "/getCompanyById", method = RequestMethod.POST)
	public ResponseMessage getCompanyById(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoCompany dtoCompanyObj = serviceCompnay.getCompanyByCompanyId(dtoCompany.getId());
			if (dtoCompanyObj != null) {
				if (dtoCompany.getMessageType() == null) {
					responseMessage = new ResponseMessage(HttpStatus.CREATED.value(), HttpStatus.CREATED,
							serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_GET_DETAIL, false), dtoCompanyObj);
				} else {
					responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
							serviceResponse.getMessageByShortAndIsDeleted(dtoCompany.getMessageType(), false),
							dtoCompanyObj);
				}

			} else {
				responseMessage = new ResponseMessage(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_NOT_GETTING, false), dtoCompanyObj);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}

	/**
	 * @description : Search Companies
	 * @param dtoSearch
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/searchCompanies", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage searchCompanies(@RequestBody DtoSearch dtoSearch, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoSearch = this.serviceCompnay.searchCompanies(dtoSearch);
			if (dtoSearch != null && dtoSearch.getRecords() != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_GET_ALL, false), dtoSearch);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_LIST_NOT_GETTING, false));
			}

		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}

		return responseMessage;
	}

	/**
	 * @description : Company List Count Of Users
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/companyListCountOfUsers", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getCompaniesListWithCountOfUsers(HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			List<DtoCompany> list = this.serviceCompnay.getTotalCompaniesWithTotalUser();
			if (list!=null && !list.isEmpty()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_GET_ALL, false), list);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}

	/**
	 * @description : Block Unblock Company
	 * @param dtoCompany
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/blockUnblockCompany", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage blockUnblockCompany(@RequestBody DtoCompany dtoCompany, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			dtoCompany = this.serviceCompnay.blockUnblockCompany(dtoCompany);
			if (dtoCompany != null && dtoCompany.getIsActive()) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_ACTIVATED_SUCCESS, false),
						dtoCompany);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_BLOCKED_SUCCESS, false),
						dtoCompany);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}

	/**
	 * @description : Get Company Stats
	 * @param dtoCompany
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getCompanyStats", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage getCompanyStats(@RequestBody DtoCompany dtoCompany, HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
			DtoCompanyStats dtoCompanyStats = this.serviceCompnay.getCompanyStats(dtoCompany.getId());
			if (dtoCompanyStats != null) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_GET_DETAIL, false),
						dtoCompanyStats);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_NOT_GETTING, false), null);
			}
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	/**
	 * @description : Get Company List By User Id
	 * @param request
	 * @param dtoUser
	 * @return
	 *//*
	@RequestMapping(value = "/companyListByUserId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=application/json")
	public ResponseMessage companyListByUserId(HttpServletRequest request,@RequestBody DtoUser dtoUser) 
	{
		ResponseMessage responseMessage = null;
		
			List<DtoCompany> list = this.serviceCompnay.getCompaniesListByUserId(dtoUser.getUserId());
			if (list.size() > 0) {
				responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
						this.serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.COMPANY_GET_ALL, false), list);
			} else {
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		
		return responseMessage;
	}
	*/
	
	@RequestMapping(value = "/checkCompanyName", method = RequestMethod.POST)
	public ResponseMessage checkCompanyName(HttpServletRequest request, @RequestBody DtoCompany dtoCompany) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		if (session != null) {
		    boolean response = serviceCompnay.getCompanyByCompanyName(dtoCompany.getName());
			if (response) 
			{
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false));
			}
			else 
			{
				responseMessage = new ResponseMessage(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
			
		} else {
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		return responseMessage;
	}
	
	@RequestMapping(value="/getCompanyByTenantId",method=RequestMethod.POST)
	public ResponseMessage getCompanyByTenantId(@RequestBody DtoCompany dtoCompany , HttpServletRequest request) {
		ResponseMessage responseMessage = null;
		UserSession session = sessionManager.validateUserSessionId(request);
		
		if(session != null) {
			dtoCompany = serviceCompnay.getCompanyByTenantId(dtoCompany);
			if(dtoCompany != null) {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_ALREADY_EXIST, false),dtoCompany);
			}else {
				responseMessage = new ResponseMessage(HttpStatus.FOUND.value(), HttpStatus.FOUND,
						serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.RECORD_NOT_FOUND, false));
			}
		}else{
			responseMessage = new ResponseMessage(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.FORBIDDEN, false));
		}
		
		return responseMessage;
	}


}
