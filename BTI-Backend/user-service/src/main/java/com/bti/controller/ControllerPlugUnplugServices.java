/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bti.config.ResponseMessage;
import com.bti.constant.MessageLabel;
import com.bti.model.dto.DtoBackendServices;
import com.bti.service.ServiceResponse;
import com.bti.service.ServiceServerProcessManagement;

/**
 * Description: Controller plug Unplug Services
 * Name of Project: BTI
 * Created on: July 21, 2017
 * Modified on: July 21, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */
@RestController
@RequestMapping("/plugUnplugServices")
public class ControllerPlugUnplugServices {

	@Autowired
	ServiceServerProcessManagement serviceServerProcessManagement;

	@Autowired
	ServiceResponse serviceResponse;

	/**
	 * @description : Get all backend services
	 * @return
	 */
	@RequestMapping(value = "/services", method = RequestMethod.GET)
	public ResponseMessage getAllServices() {
		ResponseMessage responseMessage = null;

		List<DtoBackendServices> dtoBackendServices = serviceServerProcessManagement.getAllBackendServices();
		if (dtoBackendServices!=null && !dtoBackendServices.isEmpty()) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.ALL_BACKEND_PROCESS_FETCHED, false),
					dtoBackendServices);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.NOT_ABLE_TO_GET_PROCESS, false));
		}

		return responseMessage;

	}

	/**
	 * @description : Add new backend service
	 * @param dtoBackendServices
	 * @return
	 */
	@RequestMapping(value = "/add/service", method = RequestMethod.POST)
	public ResponseMessage addService(@RequestBody DtoBackendServices dtoBackendServices) {
		ResponseMessage responseMessage = null;

		dtoBackendServices = serviceServerProcessManagement.addServerProcess(dtoBackendServices);
		if (dtoBackendServices.getId() > 0) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SERVICE_ADDED_SUCCESS, false), dtoBackendServices);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SERVICE_ADDED_FAIL, false));
		}

		return responseMessage;

	}

	/**
	 * @description : Remove existing backend service
	 * @return
	 */
	@RequestMapping(value = "/remove/service", method = RequestMethod.POST)
	public ResponseMessage removeService() {
		ResponseMessage responseMessage = null;

		return responseMessage;

	}

	/**
	 * @description : Plug Unplug service
	 * @param dtoBackendServices
	 * @return
	 */
	@RequestMapping(value = "/action/plugUnplug", method = RequestMethod.POST)
	public ResponseMessage actionService(@RequestBody DtoBackendServices dtoBackendServices) {
		ResponseMessage responseMessage = null;

		dtoBackendServices = serviceServerProcessManagement.plugUnplugBackenService(dtoBackendServices);
		if (dtoBackendServices.getStatus().equalsIgnoreCase("plugged")) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SERVICE_PLUG_SUCCESS, false), dtoBackendServices);
		} else if (dtoBackendServices.getStatus().equalsIgnoreCase("Unplugged")) {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SERVICE_UNPLUG_SUCCESS, false), dtoBackendServices);
		} else {
			responseMessage = new ResponseMessage(HttpStatus.OK.value(), HttpStatus.OK,
					serviceResponse.getMessageByShortAndIsDeleted(MessageLabel.SERVICE_PLUG_UNPLUG_FAIL, false));
		}

		return responseMessage;

	}
}
