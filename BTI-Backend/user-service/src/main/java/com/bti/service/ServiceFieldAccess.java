/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.bti.model.AccessRole;
import com.bti.model.AccessRoleScreenRelation;
import com.bti.model.Company;
import com.bti.model.Field;
import com.bti.model.FieldAccess;
import com.bti.model.Language;
import com.bti.model.Module;
import com.bti.model.Screen;
import com.bti.model.dto.DtoFieldAccess;
import com.bti.model.dto.DtoFieldAccessDeatils;
import com.bti.model.dto.DtoFieldDetail;
import com.bti.model.dto.DtoModule;
import com.bti.model.dto.DtoScreenDetail;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryFieldAccess;
import com.bti.repository.RepositoryFields;
import com.bti.repository.RepositoryLanguage;
import com.bti.repository.RepositoryModule;
import com.bti.repository.RepositoryScreen;

/**
 * Description: Service Field Access Name of Project: BTI
 * 
 */
@Service("serviceFieldAccess")
public class ServiceFieldAccess {

	/**
	 * @Description LOGGER use for put a logger in Department Service
	 */
	static Logger log = Logger.getLogger(ServiceFieldAccess.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Department service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Department service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryLanguage repositoryLanguage;
	/**
	 * @Description repositoryFieldAccess Autowired here using annotation of spring
	 *              for access of repositoryFieldAccess method in FieldAccess
	 *              service In short Access Department Query from Database using
	 *              repositoryFieldAccess.
	 */
	@Autowired
	RepositoryFieldAccess repositoryFieldAccess;

	@Autowired
	RepositoryCompany repositoryCompany;

	@Autowired
	RepositoryFields repositoryFields;

	@Autowired
	RepositoryModule repositoryModule;

	@Autowired
	RepositoryScreen repositoryScreen;

	public DtoFieldAccess saveOrUpdateFieldAccess(List<DtoFieldAccess> ids) throws ParseException {
		log.info("saveOrUpdateFieldAccess Method");
		DtoFieldAccess dtoFieldAccess = new DtoFieldAccess();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		List<DtoFieldAccess> fieldAccessList = new ArrayList<>();

		try {
			for (DtoFieldAccess dtoFieldAccessid : ids) {
				Field field = repositoryFields.findOne(dtoFieldAccessid.getFieldId());
				Company company = repositoryCompany.findOne(dtoFieldAccessid.getCompanyId());
				Module module = repositoryModule.findOne(dtoFieldAccessid.getModuleId());
				Screen screen = repositoryScreen.findOne(dtoFieldAccessid.getScreenId());
				Language language = repositoryLanguage.findOne(dtoFieldAccessid.getLanguageId());
				FieldAccess fieldAccess = new FieldAccess();
				fieldAccess.setField(field);
				fieldAccess.setMandatory(dtoFieldAccessid.getIsMandatory());
				fieldAccess.setLanguage(language);
				fieldAccess.setCompany(company);
				fieldAccess.setModule(module);
				fieldAccess.setScreen(screen);
				fieldAccess = repositoryFieldAccess.saveAndFlush(fieldAccess);
			}
			dtoFieldAccess.setAccessIds(ids);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Delete Department :" + dtoFieldAccess.getFieldAccessId());
		return dtoFieldAccess;
	}

	public DtoSearch getAllFieldsByCompanyId(DtoFieldAccess dtoFieldAccess) {
		log.info("getAllFields Method");
		List<DtoFieldAccess> listFieldAccesses = new ArrayList<>();
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoFieldAccess.getPageNumber());
		dtoSearch.setPageSize(dtoFieldAccess.getPageSize());
		List<FieldAccess> fieldAccesses = repositoryFieldAccess.findbyCompanyId(dtoFieldAccess.getCompanyId());
		List<FieldAccess> screenFieldList = repositoryFieldAccess.findScreenIdbyCompanyId(dtoFieldAccess.getScreenId(), dtoFieldAccess.getCompanyId());

		for (FieldAccess screenLists : screenFieldList) {
			DtoFieldAccess screenFieldAccess = new DtoFieldAccess();
			screenFieldAccess.setFieldAccessId(screenLists.getFieldAccessId());
			screenFieldAccess.setScreenId(screenLists.getScreen().getScreenId());
			screenFieldAccess.setScreenName(screenLists.getScreen().getScreenName());
			listFieldAccesses.add(screenFieldAccess);
		}

		for (FieldAccess fieldAccess : fieldAccesses) {
			DtoFieldAccess access = new DtoFieldAccess();
			access.setFieldAccessId(fieldAccess.getFieldAccessId());
			access.setFieldId(fieldAccess.getField().getFieldId());
			access.setFieldName(fieldAccess.getField().getFieldName());
			access.setCompanyId(fieldAccess.getCompany().getCompanyId());
			access.setIsMandatory(fieldAccess.getMandatory());
			access.setLanguageId(fieldAccess.getLanguage().getLanguageId());
			access.setModuleId(fieldAccess.getModule().getModuleId());
			access.setModuleName(fieldAccess.getModule().getName());
			access.setScreenId(fieldAccess.getScreen().getScreenId());
			access.setScreenName(fieldAccess.getScreen().getScreenName());
			listFieldAccesses.add(access);
		}

		Company company = repositoryCompany.findOne(dtoFieldAccess.getCompanyId());
		List<Integer> fieldIds = new ArrayList<>();

		for (FieldAccess fieldAccess : fieldAccesses) {
			if (fieldAccess.getField() != null) {
				fieldIds.add(fieldAccess.getField().getFieldId());
			}
		}

		List<Field> Listfield = repositoryFields.findByRemainFieldId(fieldIds);

		for (Field field : Listfield) {
			DtoFieldAccess access = new DtoFieldAccess();
			access.setFieldId(field.getFieldId());
			access.setFieldName(field.getFieldName());
			access.setCompanyId(company.getCompanyId());
			access.setIsMandatory(field.getIsMandatory());
			access.setLanguageId(field.getLanguage().getLanguageId());
			access.setModuleId(field.getScreen().getModule().getModuleId());
			access.setModuleName(field.getScreen().getModule().getName());
			access.setScreenId(field.getScreen().getScreenId());
			access.setScreenName(field.getScreen().getScreenName());
			listFieldAccesses.add(access);
		}
		dtoSearch.setRecords(listFieldAccesses);
		log.debug("All Field List Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public List<DtoModule> getAllScreenDetailList(DtoScreenDetail dtoScreenDetail) {
		String langId = httpServletRequest.getHeader("langid");
		List<DtoModule> dtoModulesList = new ArrayList<>();
		DtoFieldDetail dtoFieldDetail;
		DtoFieldAccess dtoFieldAccess = null;
		List<Module> moduleList = null;

		if (dtoScreenDetail.getPageNumber() != null && dtoScreenDetail.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoScreenDetail.getPageNumber(), dtoScreenDetail.getPageSize());
			moduleList = repositoryModule.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true, Integer.parseInt(langId));
		} else {
			moduleList = repositoryModule.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true, Integer.parseInt(langId));
		}

		if (moduleList != null && moduleList.size() > 0) {
			for (Module module : moduleList) {
				if (module.getIsActive()) {
					DtoModule dtoModule = new DtoModule(module);
					boolean moduleIsMandatory = false;

					List<DtoScreenDetail> dtoScreenDetailsList = new ArrayList<>();
					List<Screen> screenList = repositoryScreen.findByIsDeletedAndModuleModuleId(false,
							module.getModuleId());
					if (screenList != null && screenList.size() > 0) {
						for (Screen screen : screenList) {
							boolean screenIsMandatory = false;

							List<DtoFieldDetail> dtoFieldDetailsList = new ArrayList<>();

							dtoScreenDetail = new DtoScreenDetail(screen, langId);
							if (screen.getCompany() != null) {

								Company company = repositoryCompany.findOne(screen.getCompany().getCompanyId());
								System.out.println("Company: " + company);
								if (company != null) {
									List<Field> fieldList = repositoryFields.findByIsDeletedAndScreenScreenIdAndCompanyId(false, screen.getScreenId(), company.getCompanyId());

									if (fieldList != null && fieldList.size() > 0) {
										for (Field field : fieldList) {
											dtoFieldDetail = new DtoFieldDetail(field, langId);
											dtoFieldDetail.setIsMandatory(field.getIsMandatory());
											dtoFieldDetail.setLanguageId(field.getLanguage().getLanguageId());
											dtoFieldDetail.setCompanyId(company.getCompanyId());
											dtoFieldDetailsList.add(dtoFieldDetail);
										}
									}
								}
							}

							dtoScreenDetail.setIsMandatory(screenIsMandatory);
							dtoScreenDetail.setLanguageid(screen.getLanguage().getLanguageId());
							// dtoScreenDetail.setFieldAccessList(dtoFieldDetails);
							dtoScreenDetail.setFieldList(dtoFieldDetailsList);
							dtoScreenDetailsList.add(dtoScreenDetail);
						}
					}
					dtoModule.setIsMandatory(moduleIsMandatory);
					dtoModule.setScreensList(dtoScreenDetailsList);
					dtoModulesList.add(dtoModule);
				}
			}
		}
		return dtoModulesList;
	}

	public List<DtoModule> getAllScreenDetailListByComapnyId(FieldAccess fieldAccess, DtoScreenDetail dtoScreenDetail) {
		String langId = httpServletRequest.getHeader("langid");
		List<DtoModule> dtoModulesList = new ArrayList<>();
		DtoFieldAccess dtoFieldAccess = null;
		List<Module> moduleList = null;
		List<FieldAccess> fieldAccessList = null;
		if (dtoScreenDetail.getPageNumber() != null && dtoScreenDetail.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoScreenDetail.getPageNumber(), dtoScreenDetail.getPageSize());
			moduleList = repositoryModule.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true, Integer.parseInt(langId), pageable);
		} else {
			moduleList = repositoryModule.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true, Integer.parseInt(langId));
		}

		if (moduleList != null && moduleList.size() > 0) {
			for (Module module : moduleList) {
				if (module.getIsActive()) {
					DtoModule dtoModule = new DtoModule(module);
					boolean moduleFieldAccess = false;

					Company company = repositoryCompany.findByCompanyIdAndIsDeleted(dtoFieldAccess.getCompanyId(), false);
					fieldAccessList = repositoryFieldAccess.findFieldIdByCompanyId(company.getCompanyId());
					List<DtoScreenDetail> dtoScreenDetailsList = new ArrayList<>();
					List<Screen> screenList = repositoryScreen.findByIsDeletedAndModuleModuleId(false, module.getModuleId());
					if (screenList != null && screenList.size() > 0) {
						for (Screen screen : screenList) {
							boolean screenFieldAccess = false;
							Integer languageId = null;
							List<DtoFieldAccess> dtoFieldAccesses = new ArrayList<DtoFieldAccess>();
							dtoScreenDetail = new DtoScreenDetail(screen, langId);

							fieldAccessList = repositoryFieldAccess.findByScreenScreenIdAndScreenIsDeletedAndIsDeleted(screen.getScreenId(), false, false);
							if (fieldAccessList != null && fieldAccessList.size() > 0) {
								for (FieldAccess field : fieldAccessList) {
									dtoFieldAccess = new DtoFieldAccess(field, langId);

									FieldAccess fieldAccessscreenRelation = repositoryFieldAccess
											.findByModuleModuleIdAndScreenScreenIdAndCompanyCompanyIdAndFieldFieldIdAndLanguageLanguageIdAndIsDeleted(
													module.getModuleId(), screen.getScreenId(), company.getCompanyId(),
													fieldAccess.getField().getFieldId(), Integer.parseInt(langId),
													false);

									if (fieldAccessscreenRelation != null) {
										dtoFieldAccess.setIsMandatory(fieldAccessscreenRelation.getMandatory());
										dtoFieldAccess.setLanguageId(fieldAccessscreenRelation.getLanguage().getLanguageId());
										if (fieldAccessscreenRelation.getMandatory()) {
											screenFieldAccess = true;
											moduleFieldAccess = true;
										}
									} else {
										dtoFieldAccess.setIsMandatory(false);
										dtoFieldAccess.setLanguageId(languageId);
									}
									dtoFieldAccesses.add(dtoFieldAccess);
								}
							} else {
								Field field = repositoryFields.findByFieldIdAndIsDeleted(fieldAccess.getField().getFieldId(), false);
								if (field != null) {
									if (field.getIsMandatory()) {
										screenFieldAccess = true;
										moduleFieldAccess = true;
									}
								}
							}
							dtoScreenDetail.setIsMandatory(screenFieldAccess);
							dtoScreenDetail.setLanguageid(Integer.parseInt(langId));
							dtoScreenDetailsList.add(dtoScreenDetail);
						}
					} else {
						FieldAccess field = repositoryFieldAccess.findByModuleModuleIdAndCompanyCompanyIdAndFieldFieldIdAndLanguageLanguageIdAndIsDeleted(
										module.getModuleId(), company.getCompanyId(), fieldAccess.getField().getFieldId(), Integer.parseInt(langId), false);
						if (field != null) {
							if (field.getMandatory()) {

								moduleFieldAccess = true;
							}
						}
					}
					dtoScreenDetail.setIsMandatory(moduleFieldAccess);
					dtoScreenDetail.setLanguageid(Integer.parseInt(langId));
					dtoScreenDetailsList.add(dtoScreenDetail);
				}
			}
		}
		return dtoModulesList;
	}

}
