/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.constant.BTICodeType;
import com.bti.model.AccessRole;
import com.bti.model.RoleGroup;
import com.bti.model.RoleGroupAccessRole;
import com.bti.model.dto.DtoAccessRole;
import com.bti.model.dto.DtoRoleGroup;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryAccessRole;
import com.bti.repository.RepositoryRoleGroup;
import com.bti.repository.RepositoryRoleGroupAccessRole;
import com.bti.repository.RepositoryUserGroupRoleGroup;
import com.bti.util.CodeGenerator;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service Role Group
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceRoleGroup")
public class ServiceRoleGroup {

	private static final Logger LOGGER = Logger.getLogger(ServiceRoleGroup.class);

	@Autowired
	RepositoryRoleGroup repositoryRoleGroup;

	@Autowired
	RepositoryRoleGroupAccessRole repositoryRoleGroupAccessRole;

	@Autowired
	RepositoryAccessRole repositoryAccessRole;

	@Autowired
	CodeGenerator codeGenerator;


	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	RepositoryUserGroupRoleGroup repositoryUserGroupRoleGroup;
	
	private static final String USER_ID = "userid";

	/**
	 * Description: Save Role Group
	 * @param dtoRoleGroup
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public boolean saveRoleGroup(DtoRoleGroup dtoRoleGroup) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(USER_ID));
		boolean status = false;
		if (dtoRoleGroup != null) {
			RoleGroupAccessRole roleGroupApplicationRole = null;
			RoleGroup roleGroup = this.repositoryRoleGroup.findByRoleGroupIdAndIsDeleted(dtoRoleGroup.getId(), false);
			if (roleGroup != null) {
				// update existing role group
				roleGroup.setUpdatedBy(loggedInUserId);
				roleGroup.setRoleGroupName(dtoRoleGroup.getRoleGroupName());
				roleGroup.setRoleGroupDescription(dtoRoleGroup.getRoleGroupDescription());

				roleGroup = this.repositoryRoleGroup.saveAndFlush(roleGroup);
				if (roleGroup != null) {
					// save Or update roles for this role group
					if (dtoRoleGroup.getRoleIdList() != null && !dtoRoleGroup.getRoleIdList().isEmpty()) {
						// Firstly, will be deleting all the roles assigned to a role group 
						this.repositoryRoleGroupAccessRole.deleteAllRoleGroupAccessRoles(roleGroup.getRoleGroupId(),
								loggedInUserId);
						StringBuilder accessRoleIds = new StringBuilder("");
						for (Integer id : dtoRoleGroup.getRoleIdList()) {
							try {
								roleGroupApplicationRole = this.repositoryRoleGroupAccessRole
										.findByAccessRoleAccessRoleIdAndRoleGroupRoleGroupId(id,
												roleGroup.getRoleGroupId());
								if (roleGroupApplicationRole != null) {
									// if exists, then update the record
									roleGroupApplicationRole.setIsDeleted(false);  // set deleted false again
									roleGroupApplicationRole.setUpdatedBy(loggedInUserId);
									this.repositoryRoleGroupAccessRole.saveAndFlush(roleGroupApplicationRole);
								} else {
									// otherwise insert new record
									roleGroupApplicationRole = new RoleGroupAccessRole();
									roleGroupApplicationRole.setIsDeleted(false);
									roleGroupApplicationRole.setCreatedBy(loggedInUserId);
									roleGroupApplicationRole.setRoleGroup(roleGroup);
									roleGroupApplicationRole.setAccessRole(
											this.repositoryAccessRole.findByAccessRoleIdAndIsDeleted(id, false));
									this.repositoryRoleGroupAccessRole.saveAndFlush(roleGroupApplicationRole);
								}

								// need to update comma separated access role ids for solr search
								if (accessRoleIds.toString().isEmpty()) {
									accessRoleIds.append(String.valueOf(id));
								} else {
									accessRoleIds.append("," + id.toString());
								}
							} catch (NumberFormatException e) {
								LOGGER.info(Arrays.toString(e.getStackTrace()));
							}
						}
					}
					status = true;
				}
			} else {
				// Insert new role group entry
				roleGroup = new RoleGroup();

				roleGroup.setIsDeleted(false);
				roleGroup.setCreatedBy(loggedInUserId);
				roleGroup.setRoleGroupCode(this.codeGenerator.getGeneratedCode(BTICodeType.ROLEGROUP.name()));
				roleGroup.setRoleGroupName(dtoRoleGroup.getRoleGroupName());
				roleGroup.setRoleGroupDescription(dtoRoleGroup.getRoleGroupDescription());
				roleGroup = this.repositoryRoleGroup.saveAndFlush(roleGroup);

				if (roleGroup != null) {
					StringBuilder accessRoleIds = new StringBuilder("");
					// save roles for this role group
					if (dtoRoleGroup.getRoleIdList() != null && !dtoRoleGroup.getRoleIdList().isEmpty()) {
						for (Integer id : dtoRoleGroup.getRoleIdList()) {
							// need to update comma separated access role ids for solr search
							if (accessRoleIds.toString().isEmpty()) {
								accessRoleIds.append(String.valueOf(id));
							} else {
								accessRoleIds.append("," + id.toString());
							}
							roleGroupApplicationRole = new RoleGroupAccessRole();
							roleGroupApplicationRole.setIsDeleted(false);
							roleGroupApplicationRole.setCreatedBy(loggedInUserId);
							roleGroupApplicationRole.setRoleGroup(roleGroup);
							roleGroupApplicationRole
									.setAccessRole(this.repositoryAccessRole.findByAccessRoleIdAndIsDeleted(id, false));
							this.repositoryRoleGroupAccessRole.saveAndFlush(roleGroupApplicationRole);
						}
					}
						status = true;
				}
			}
		}
		return status;
	}

	/**
	 * Description: Get Role Group list
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getRoleGroupList(DtoSearch dtoSearch) {
		DtoSearch dtoSearch2 = new DtoSearch();
		dtoSearch2.setPageNumber(dtoSearch.getPageNumber());
		dtoSearch2.setPageSize(dtoSearch.getPageSize());
		dtoSearch2.setTotalCount(repositoryRoleGroup.getCountOfTotalRoleGroups());
		List<RoleGroup> roleGroupList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(), Direction.DESC, "createdDate");
			roleGroupList = this.repositoryRoleGroup.findByIsDeleted(false, pageable);
		} else {
			roleGroupList = this.repositoryRoleGroup.findByIsDeletedOrderByCreatedDateDesc(false);
		}

		List<DtoRoleGroup> dtoRoleGroupRoleList = new ArrayList<>();
		if (roleGroupList != null && !roleGroupList.isEmpty() ) {
			DtoRoleGroup dtoRoleGroup = null;

			DtoAccessRole dtoAccessRole = null;
			AccessRole accessRole = null;
			List<RoleGroupAccessRole> roleGroupAccessRoleList = null;
			for (RoleGroup roleGroup : roleGroupList) {
				List<Integer> roleIds = new ArrayList<>();
				List<String> roleNames = new ArrayList<>();
				dtoRoleGroup = new DtoRoleGroup(roleGroup);
				List<DtoAccessRole> roleGroupRoleList = new ArrayList<>();
				roleGroupAccessRoleList = this.repositoryRoleGroupAccessRole
						.findByRoleGroupRoleGroupIdAndIsDeleted(roleGroup.getRoleGroupId(), false);
				if (roleGroupAccessRoleList != null && !roleGroupAccessRoleList.isEmpty()) {
					for (RoleGroupAccessRole roleGroupApplicationRole : roleGroupAccessRoleList) {

						accessRole = roleGroupApplicationRole.getAccessRole();
						if (accessRole != null) {
							roleIds.add(accessRole.getAccessRoleId());
							roleNames.add(accessRole.getRoleName());
							dtoAccessRole = new DtoAccessRole(accessRole);
							roleGroupRoleList.add(dtoAccessRole);
						}

					}
				}
				dtoRoleGroup.setRoleGroupRoleList(roleGroupRoleList);
				dtoRoleGroup.setRoleIds(roleIds);
				dtoRoleGroup.setRoleNames(roleNames);
				dtoRoleGroupRoleList.add(dtoRoleGroup);

			}
		}
		dtoSearch2.setRecords(dtoRoleGroupRoleList);
		return dtoSearch2;
	}

	/**
	 * Description: Get Role Group list for drop down
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getRoleGroupListForDropDown(DtoSearch dtoSearch) {
		DtoSearch dtoSearch2 = new DtoSearch();
		dtoSearch2.setPageNumber(dtoSearch.getPageNumber());
		dtoSearch2.setPageSize(dtoSearch.getPageSize());
		dtoSearch2.setTotalCount(repositoryRoleGroup.getCountOfTotalRoleGroups());
		List<RoleGroup> roleGroupList = null;
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize());
			roleGroupList = this.repositoryRoleGroup.findByIsDeleted(false, pageable);
		} else {
			roleGroupList = this.repositoryRoleGroup.findByIsDeleted(false);
		}

		List<DtoRoleGroup> dtoRoleGroupRoleList = new ArrayList<>();
		if (roleGroupList != null && !roleGroupList.isEmpty()) {
			DtoRoleGroup dtoRoleGroup = null;
			for (RoleGroup roleGroup : roleGroupList) {
				dtoRoleGroup = new DtoRoleGroup();
				dtoRoleGroup.setId(roleGroup.getRoleGroupId());
				if (UtilRandomKey.isNotBlank(roleGroup.getRoleGroupName())) {
					dtoRoleGroup.setRoleGroupName(roleGroup.getRoleGroupName());
				} else {
					dtoRoleGroup.setRoleGroupName("N/A");
				}

				dtoRoleGroupRoleList.add(dtoRoleGroup);
			}
		}
		dtoSearch2.setRecords(dtoRoleGroupRoleList);
		return dtoSearch2;
	}

	/**
	 * Description: delete Role Group (one or more)
	 * @param idList
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public boolean deleteMultipleRoleGroups(List<Integer> idList) {
		boolean status = false;
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(USER_ID));
		try {
			this.repositoryRoleGroup.deleteMultipleRoleGroups(idList, true, loggedInUserId);
			// we also need to delete roles association with role group
			this.repositoryRoleGroupAccessRole.deleteAllRoleGroupAccessRolesForMultipleRoleGroups(idList,
					loggedInUserId);

			//Delete all user group mapping with role group
			this.repositoryUserGroupRoleGroup.deleteAllRoleGroupUserGroupForMultipleRoleGroups(idList, true,
					loggedInUserId);
			status = true;
		} catch (NumberFormatException e) {
			LOGGER.info(Arrays.toString(e.getStackTrace()));
		}
		return status;
	}
	
	/**
	 * Description: get Role Group detail by role group id
	 * @param id
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoRoleGroup getRoleGroupDetails(Integer id) {
		DtoRoleGroup dtoRoleGroup = null;
		RoleGroup roleGroup = this.repositoryRoleGroup.findByRoleGroupIdAndIsDeleted(id, false);
		if (roleGroup != null) {
			List<Integer> roleIds = new ArrayList<>();
			List<String> roleNames = new ArrayList<>();
			dtoRoleGroup = new DtoRoleGroup(roleGroup);
			List<DtoAccessRole> roleGroupRoleList = new ArrayList<>();
			DtoAccessRole dtoApplicationRole = null;
			AccessRole accessRole = null;
			List<RoleGroupAccessRole> roleGroupApplicationRoleList = this.repositoryRoleGroupAccessRole
					.findByRoleGroupRoleGroupIdAndIsDeleted(roleGroup.getRoleGroupId(), false);
			for (RoleGroupAccessRole roleGroupApplicationRole : roleGroupApplicationRoleList) {
				accessRole = roleGroupApplicationRole.getAccessRole();
				if (accessRole != null) {
					roleIds.add(accessRole.getAccessRoleId());
					roleNames.add(accessRole.getRoleName());
					dtoApplicationRole = new DtoAccessRole(accessRole);
					roleGroupRoleList.add(dtoApplicationRole);
				}
			}
			dtoRoleGroup.setRoleIds(roleIds);
			dtoRoleGroup.setRoleNames(roleNames);
			dtoRoleGroup.setRoleGroupRoleList(roleGroupRoleList);

		}
		return dtoRoleGroup;
	}

	/**
	 * Description: search role group by search keyword
	 * @param dtoSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch searchRoleGroup(DtoSearch dtoSearch) {
		if (dtoSearch != null) {
			dtoSearch.setTotalCount(
					this.repositoryRoleGroup.predictiveRoleGroupSearchTotalCount(dtoSearch.getSearchKeyword()));
			List<RoleGroup> roleGroupList = this.repositoryRoleGroup.predictiveRoleGroupSearchWithPagination(
					dtoSearch.getSearchKeyword(), new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize(),
							Direction.DESC, "createdDate"));
			if (roleGroupList != null && !roleGroupList.isEmpty()) {
				List<DtoRoleGroup> dtoRoleGroupList = new ArrayList<>();
				for (RoleGroup roleGroup : roleGroupList) {
					List<Integer> roleIds = new ArrayList<>();
					List<String> roleNames = new ArrayList<>();
					DtoRoleGroup dtoRoleGroup = new DtoRoleGroup(roleGroup);
					List<DtoAccessRole> roleGroupRoleList = new ArrayList<>();
					List<RoleGroupAccessRole> roleGroupAccessRoleList = this.repositoryRoleGroupAccessRole
							.findByRoleGroupRoleGroupIdAndIsDeleted(roleGroup.getRoleGroupId(), false);
					if (roleGroupAccessRoleList != null && !roleGroupAccessRoleList.isEmpty()) {
						for (RoleGroupAccessRole roleGroupApplicationRole : roleGroupAccessRoleList) {

							AccessRole accessRole = roleGroupApplicationRole.getAccessRole();
							if (accessRole != null) {
								roleIds.add(accessRole.getAccessRoleId());
								roleNames.add(accessRole.getRoleName());
								DtoAccessRole dtoAccessRole = new DtoAccessRole(accessRole);
								roleGroupRoleList.add(dtoAccessRole);
							}

						}
					}
					dtoRoleGroup.setRoleGroupRoleList(roleGroupRoleList);
					dtoRoleGroup.setRoleIds(roleIds);
					dtoRoleGroup.setRoleNames(roleNames);
					dtoRoleGroupList.add(dtoRoleGroup);
				}
				dtoSearch.setRecords(dtoRoleGroupList);
			}
		}
		return dtoSearch;
	}

}
