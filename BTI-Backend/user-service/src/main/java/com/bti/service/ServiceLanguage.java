package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.bti.model.Language;
import com.bti.model.dto.DtoLanguage;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryLanguage;

@Service("serviceLanguage")
public class ServiceLanguage {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ServiceLanguage.class);
	
	@Autowired
	RepositoryLanguage repositoryLanguage;

	
	public DtoSearch getAllLanguage() {
		DtoLanguage dtoLanguage;
		DtoSearch dtoSearch = new DtoSearch();
		List<Language> languageList = null;
		languageList = repositoryLanguage.findByIsDeletedAndIsActive(false, true);
		List<DtoLanguage> dtoLanguageList=new ArrayList<>();
		if(languageList!=null && languageList.size()>0)
		{
			for (Language language : languageList) 
			{
				dtoLanguage=new DtoLanguage(language);
				dtoLanguage.setLanguageId(language.getLanguageId());
				dtoLanguage.setLanguageName(language.getLanguageName());
				dtoLanguageList.add(dtoLanguage);
			}
			dtoSearch.setRecords(dtoLanguageList);
		}
		return dtoSearch;
	}

}
