package com.bti.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bti.constant.Constant;
import com.bti.constant.MessageLabel;
import com.bti.model.Company;
import com.bti.model.WorkflowUser;
import com.bti.model.dto.DtoRuleConnectors;
import com.bti.model.dto.DtoRuleDefinition;
import com.bti.model.dto.DtoRuleParams;
import com.bti.model.dto.DtoWorkflowUser;
import com.bti.model.dto.DtoWorkflowUserDetails;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryEntityManager;
import com.bti.repository.RepositoryWorkflowUsers;
import com.bti.util.CommonUtils;
import com.mysql.jdbc.Constants;

@Service
public class ServiceRuleEngine {
	
	private HashMap<String, List> rulesMap = new HashMap<String, List>();
	
	private enum OPERATORS {NONE, LESS_THAN, LESS_THAN_OR_EQUAL, EQUAL, GREATER_THAN_OR_EQUAL, GREATOR_THAN, NOT_EQUAL};
	private enum GATES {PLUS, MINUS, MULTIPLY, DIVIDE};
	
	private enum TYPES {INT, FLOAT, DOUBLE, STRING};
	
	public ServiceRuleEngine() {
		List loanRulesList = new ArrayList<String>();
		loanRulesList.add("LOAN_THRESHOLD");
//		loanRulesList.add("EMPL_CHK");
//		loanRulesList.add("AMT_CHK");
		
		rulesMap.put("LOAN_REQ", loanRulesList);
		
//		List vacRulesList = new ArrayList<String>();
//		vacRulesList.add("LEAVE_BALANCE");
//		vacRulesList.add("SERVICE_PERIOD");
//		
//		rulesMap.put("VAC_REQ", vacRulesList);
	}
	
	
	public List<String> getRules(String processCode) {
		return rulesMap.get(processCode);
	}

	public DtoRuleDefinition getRulesDefination(String processCode, String ruleCode) {
		
		switch (processCode) {
			case "LOAN_REQ":
				
				switch (ruleCode) {
					case "LOAN_THRESHOLD":
						return getLoanThresholdJson(processCode, ruleCode);
//						break;
		
					case "EMPL_CHK":
						
						break;
		
					case "AMT_CHK":
						
						break;

					default:
						break;
					}
					
				break;
			case "VAC_REQ":
				
				break;
				
	
			default:
				break;
		}
		return null;
	}
	 
	private DtoRuleDefinition getLoanThresholdJson(String processCode, String ruleCode) {
		DtoRuleDefinition dtoRuleDefinition = new DtoRuleDefinition();
		
		dtoRuleDefinition.setRuleId("1");
		dtoRuleDefinition.setRuleCode(ruleCode);
		dtoRuleDefinition.setProcessCode(processCode);
		
			List<DtoRuleParams> params = new ArrayList<DtoRuleParams>();
			List<DtoRuleConnectors> connectors = new ArrayList<DtoRuleConnectors>();
			
				DtoRuleParams param1 = new DtoRuleParams();
				param1.setId("PARAM_1");
				param1.setName("requestedAmount");
				param1.setOperator(String.valueOf(OPERATORS.NONE));
				param1.setValue("");
				param1.setValueType(String.valueOf(TYPES.FLOAT));
			params.add(param1);
			
				DtoRuleParams param2 = new DtoRuleParams();
				param2.setId("PARAM_2");
				param2.setName("remaining");
				param2.setOperator(String.valueOf(OPERATORS.NONE));
				param2.setValue("");
				param2.setValueType(String.valueOf(TYPES.FLOAT));
			params.add(param2);
			
				DtoRuleParams param3 = new DtoRuleParams();
				param3.setId("PARAM_3");
				param3.setName("loan_threshold");
				param3.setOperator(String.valueOf(OPERATORS.NONE));
				param3.setValue("");
				param3.setValueType(String.valueOf(TYPES.FLOAT));
			params.add(param3);

				DtoRuleConnectors connector1 = new DtoRuleConnectors();
				connector1.setId("CONN_1");
				connector1.setSrcId("PARAM_1");
				connector1.setTargetId("PARAM_2");
				connector1.setGate(String.valueOf(GATES.PLUS));
			
			connectors.add(connector1);
			
				DtoRuleConnectors connector2 = new DtoRuleConnectors();
				connector2.setId("CONN_2");
				connector2.setSrcId("CONN_1");
				connector2.setTargetId("PARAM_3");
				connector2.setGate(String.valueOf(GATES.PLUS));
			
			connectors.add(connector2);

			dtoRuleDefinition.setParams(params);
			dtoRuleDefinition.setConnectors(connectors);

		return dtoRuleDefinition;
	}
}