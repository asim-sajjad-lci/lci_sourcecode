/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.ServicesPlugUnplug;
import com.bti.model.dto.DtoBackendServices;
import com.bti.repository.RepositoryServicesPlugUnplug;

/**
 * Description: Service Server Process Management
 * Name of Project: BTI
 * Created on: July 21, 2017
 * Modified on: July 21, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service("serviceServerProcessManagement")
public class ServiceServerProcessManagement {
	private static final Logger LOGGER = Logger.getLogger(ServiceServerProcessManagement.class);
	@Autowired
	RepositoryServicesPlugUnplug repositoryServicesPlugUnplug;

	/**
	 * @param dtoBackendServices
	 * @return
	 */
	public DtoBackendServices addServerProcess(DtoBackendServices dtoBackendServices) {

		ServicesPlugUnplug plugUnplug = new ServicesPlugUnplug();
		plugUnplug.setServiceName(dtoBackendServices.getName());
		plugUnplug.setDescription(dtoBackendServices.getDescription());
		plugUnplug.setPort(dtoBackendServices.getPort());
		plugUnplug.setServerIP(dtoBackendServices.getServerIP());
		
		repositoryServicesPlugUnplug.saveAndFlush(plugUnplug);

		return dtoBackendServices;
	}

	/**
	 * @return
	 */
	public List<DtoBackendServices> getAllBackendServices() {

		List<DtoBackendServices> backendServices = new ArrayList<>();
		DtoBackendServices dtoBackendServices;

		List<ServicesPlugUnplug> plugUnplugs = repositoryServicesPlugUnplug.findByIsDeleted(false);
		for (ServicesPlugUnplug plugUnplug : plugUnplugs) {
			dtoBackendServices = new DtoBackendServices(plugUnplug);

			backendServices.add(dtoBackendServices);
		}

		return backendServices;
	}

	/**
	 * @param dtoBackendServices
	 * @return
	 */
	public DtoBackendServices plugUnplugBackenService(DtoBackendServices dtoBackendServices) {
		String s;
		Process p;

		ServicesPlugUnplug plugUnplug = repositoryServicesPlugUnplug.findByIdAndIsDeleted(dtoBackendServices.getId(),
				false);
		if (plugUnplug != null) {
			if (plugUnplug.getIsActive()) {
				try {
					p = Runtime.getRuntime().exec("kill " + plugUnplug.getProcessId());
					BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
					while ((s = br.readLine()) != null) {
						LOGGER.info(s);
					}
					p.waitFor();
					p.destroy();
					plugUnplug.setIsActive(false);
				} catch (Exception e) {
					LOGGER.info(Arrays.toString(e.getStackTrace()));
				}

			} else {

				try {
					String command = "bash -c (cd /home/staging/bti/21july; java -jar bti-user-services-0.1.0.jar &)";
					p = Runtime.getRuntime().exec(command);
					BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
					while ((s = br.readLine()) != null) {
						LOGGER.info(s);
					}
					p.waitFor();
					p.destroy();
					plugUnplug.setIsActive(true);
				} catch (Exception e) {
					LOGGER.info(Arrays.toString(e.getStackTrace()));
				}

			}
			repositoryServicesPlugUnplug.saveAndFlush(plugUnplug);
		}

		return dtoBackendServices;
	}
}
