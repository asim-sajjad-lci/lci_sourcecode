package com.bti.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bti.constant.Constant;
import com.bti.model.Company;
import com.bti.model.EjbiErpUserRelation;
import com.bti.model.Language;
import com.bti.model.Module;
import com.bti.model.ReportMaster;
import com.bti.model.ScreenCategory;
import com.bti.model.ScreenMenu;
import com.bti.model.User;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoEjbiErpUserRelation;
import com.bti.model.dto.DtoModule;
import com.bti.model.dto.DtoReportMaster;
import com.bti.model.dto.DtoReportSearch;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoUser;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryEjbiErpUserRelation;
import com.bti.repository.RepositoryLanguage;
import com.bti.repository.RepositoryModule;
import com.bti.repository.RepositoryReportMaster;
import com.bti.repository.RepositoryScreenCategory;
import com.bti.repository.RepositoryScreenMenu;
import com.bti.repository.RepositoryUser;

@Service("serviceReportMaster")
public class ServiceReportMaster {

	private static final Logger LOGGER = Logger.getLogger(ServiceReportMaster.class);

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryReportMaster repositoryReportMaster;

	@Autowired
	RepositoryCompany repositoryCompany;

	@Autowired
	RepositoryUser repositoryUser;
	
	@Autowired
	RepositoryModule repositoryModule;
	
	@Autowired
	RepositoryLanguage repositoryLanguage;
	
	@Autowired
	RepositoryScreenMenu repositoryScreenMenu;
	
	@Autowired
	RepositoryScreenCategory repositoryScreenCategory;

	@Autowired
	RepositoryEjbiErpUserRelation repositoryEjbiErpUserRelation;
	/**
	 * 
	 * @param dtoReportMaster
	 * @return
	 */
	public DtoReportMaster saveOrUpdateReportMaster(DtoReportMaster dtoReportMaster) {
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(Constant.USER_ID));
		ReportMaster reportMaster = null;
		Module module = repositoryModule.findOne(dtoReportMaster.getModuleId());
		
		try {
			if (dtoReportMaster.getId() != null && dtoReportMaster.getId() > 0) {
				reportMaster = repositoryReportMaster.findByIdAndIsDeleted(dtoReportMaster.getId(), false);
				reportMaster.setUpdatedDate(new Date());
				reportMaster.setUpdatedBy(loggedInUserId);
			} else {
				reportMaster = new ReportMaster();
				reportMaster.setCreatedDate(new Date());
				reportMaster.setCreatedBy(loggedInUserId);
			}
			reportMaster.setReportId(dtoReportMaster.getReportId());
			reportMaster.setReportName(dtoReportMaster.getReportName());
			reportMaster.setReportLink(dtoReportMaster.getReportLink());
			reportMaster.setReportDescription(dtoReportMaster.getReportDescription());
			reportMaster.setObjectId(dtoReportMaster.getObjectId());
			reportMaster.setContainerId(dtoReportMaster.getContainerId());
			reportMaster.setModule(module);
			repositoryReportMaster.saveAndFlush(reportMaster);
			
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return dtoReportMaster;
	}

	/**
	 * 
	 * @param ids
	 * @return
	 */
	public DtoReportMaster deleteReportMaster(List<Integer> ids) {
		DtoReportMaster dtoReportMaster = new DtoReportMaster();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(Constant.USER_ID));
		try {
			dtoReportMaster.setDeleteMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("REPORT_MASTER_DELETED", false));
			dtoReportMaster.setAssociateMessage(
					serviceResponse.getStringMessageByShortAndIsDeleted("REPORT_MASTER_ASSOCIATED", false));
			StringBuilder invlidDeleteMessage = new StringBuilder();
			invlidDeleteMessage.append(serviceResponse
					.getMessageByShortAndIsDeleted("REPORT_MASTER_NOT_DELETE_ID_MESSAGE", false).getMessage());
			List<DtoReportMaster> deleteReportMaster = new ArrayList<>();

			for (Integer id : ids) {
				ReportMaster reportMaster = repositoryReportMaster.findOne(id);

				DtoReportMaster dtoReportMaster2 = new DtoReportMaster();
				dtoReportMaster2.setId(id);
				dtoReportMaster2.setReportDescription(reportMaster.getReportDescription());
				repositoryReportMaster.deleteSingleReportMaster(true, loggedInUserId, id);
				deleteReportMaster.add(dtoReportMaster2);
			}

			dtoReportMaster.setDeleteReportMaster(deleteReportMaster);

			LOGGER.debug("Delete ReportMaster :" + dtoReportMaster.getReportId());
		} catch (Exception e) {
			LOGGER.error(e);

		}
		return dtoReportMaster;
	}

	/**
	 * 
	 * @param dtoReportSearch
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoReportSearch searchReportMaster(DtoReportSearch dtoReportSearch) {
		LOGGER.info("searchReportMaster Method");
		try {
			if (dtoReportSearch != null) {

				String searchWord = dtoReportSearch.getSearchKeyword();
				dtoReportSearch = searchByReportMaster(dtoReportSearch);
				dtoReportSearch.setTotalCount(
						this.repositoryReportMaster.predictiveReportMasterSearchTotalCount("%" + searchWord + "%"));

				List<ReportMaster> reportMasterList = searchByPageSizeAndNumber(dtoReportSearch);

				if (reportMasterList != null && !reportMasterList.isEmpty()) {
					List<DtoReportMaster> dtoReportMasterList = new ArrayList<>();
					DtoReportMaster dtoReportMaster = null;
					DtoModule dtoModule = null;
					for (ReportMaster reportMaster : reportMasterList) {
						dtoReportMaster = new DtoReportMaster(reportMaster);
						dtoReportMaster.setId(reportMaster.getId());
						dtoReportMaster.setReportId(reportMaster.getReportId());
						dtoReportMaster.setReportName(reportMaster.getReportName());
						dtoReportMaster.setReportDescription(reportMaster.getReportDescription());
						dtoReportMaster.setReportLink(reportMaster.getReportLink());
						dtoReportMaster.setObjectId(reportMaster.getObjectId());
						dtoReportMaster.setContainerId(reportMaster.getContainerId());
						if (reportMaster.getModule() != null) {
							Module module = repositoryModule.findOne(reportMaster.getModule().getModuleId());
							dtoModule = new DtoModule();
							dtoModule.setModuleId(module.getModuleId());
							dtoModule.setModuleName(module.getName());
							dtoReportMaster.setDtoModule(dtoModule);
						}
						dtoReportMasterList.add(dtoReportMaster);
					}
					dtoReportSearch.setRecords(dtoReportMasterList);
				}
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return dtoReportSearch;
	}

	/**
	 * 
	 * @param dtoReportSearch
	 * @return
	 */
	public DtoReportSearch searchByReportMaster(DtoReportSearch dtoReportSearch) {

		String condition = "";

		if (dtoReportSearch.getSortOn() != null || dtoReportSearch.getSortBy() != null) {
			switch (dtoReportSearch.getSortOn()) {
			case "reportLink":
				condition += dtoReportSearch.getSortOn();
				break;
			case "reportName":
				condition += dtoReportSearch.getSortOn();
				break;
			case "reportDescription":
				condition += dtoReportSearch.getSortOn();
				break;
			default:
				condition += "id";
			}
		} else {
			condition += "id";
			dtoReportSearch.setSortOn("");
			dtoReportSearch.setSortBy("");
		}
		dtoReportSearch.setCondition(condition);
		return dtoReportSearch;

	}

	/**
	 * 
	 * @param dtoReportSearch
	 * @return
	 */
	public List<ReportMaster> searchByPageSizeAndNumber(DtoReportSearch dtoReportSearch) {
		List<ReportMaster> reportMasterList = null;
		if (dtoReportSearch.getPageNumber() != null && dtoReportSearch.getPageSize() != null) {

			if (dtoReportSearch.getSortBy().equals("") && dtoReportSearch.getSortOn().equals("")) {
				reportMasterList = this.repositoryReportMaster.predictiveReportMasterSearchWithPagination(
						"%" + dtoReportSearch.getSearchKeyword() + "%", new PageRequest(dtoReportSearch.getPageNumber(),
								dtoReportSearch.getPageSize(), Sort.Direction.DESC, "id"));
			}
			if (dtoReportSearch.getSortBy().equals("ASC")) {
				reportMasterList = this.repositoryReportMaster.predictiveReportMasterSearchWithPagination(
						"%" + dtoReportSearch.getSearchKeyword() + "%", new PageRequest(dtoReportSearch.getPageNumber(),
								dtoReportSearch.getPageSize(), Sort.Direction.ASC, dtoReportSearch.getCondition()));
			} else if (dtoReportSearch.getSortBy().equals("DESC")) {
				reportMasterList = this.repositoryReportMaster.predictiveReportMasterSearchWithPagination(
						"%" + dtoReportSearch.getSearchKeyword() + "%", new PageRequest(dtoReportSearch.getPageNumber(),
								dtoReportSearch.getPageSize(), Sort.Direction.DESC, dtoReportSearch.getCondition()));
			}

		}

		return reportMasterList;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public DtoReportMaster getReportMasterById(int id) {
		LOGGER.info("getReportMasterById Method");
		DtoReportMaster dtoReportMaster = new DtoReportMaster();
		try {
			if (id > 0) {
				ReportMaster reportMaster = repositoryReportMaster.findByIdAndIsDeleted(id, false);
				if (reportMaster != null) {
					dtoReportMaster = new DtoReportMaster(reportMaster);
				} else {
					dtoReportMaster.setMessageType("REPORT_MASTER_NOT_GETTING");

				}
			} else {
				dtoReportMaster.setMessageType("INVALID_REPORT_MASTER_ID");

			}
			LOGGER.debug("ReportMaster By Id is:" + dtoReportMaster.getReportId());
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return dtoReportMaster;
	}

	/**
	 * 
	 * @return
	 */
	public List<DtoReportMaster> getAllReportMasterDropDownList() {
		LOGGER.info("getAllReportMasterDropDownList  Method");
		List<DtoReportMaster> dtoReportMasterList = new ArrayList<>();
		try {
			List<ReportMaster> list = repositoryReportMaster.findByIsDeleted(false);
			if (list != null && !list.isEmpty()) {
				for (ReportMaster reportMaster : list) {
					DtoReportMaster dtoReportMaster = new DtoReportMaster();
					dtoReportMaster.setId(reportMaster.getId());
					dtoReportMaster.setReportId(reportMaster.getReportId());
					dtoReportMaster.setReportName(reportMaster.getReportName());
					dtoReportMaster.setReportLink(reportMaster.getReportLink());
					dtoReportMaster.setReportDescription(reportMaster.getReportDescription());
					dtoReportMaster.setObjectId(reportMaster.getObjectId());
					dtoReportMaster.setContainerId(reportMaster.getContainerId());
					dtoReportMaster.setModuleId(reportMaster.getModule().getModuleId());
					dtoReportMaster.setModuleName(reportMaster.getModule().getName());
					dtoReportMasterList.add(dtoReportMaster);
				}
			}
			LOGGER.debug("Report is:" + dtoReportMasterList.size());
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return dtoReportMasterList;
	}
	
	/**
	 * @Description: check Repeat ReportId
	 * @param reportId
	 * @return
	 */

	public DtoReportMaster repeatByReportId(Integer reportId) {
		LOGGER.info("repeatByReportId Method");
		DtoReportMaster dtoReportMaster = new DtoReportMaster();
		try {
			List<ReportMaster> reportMasterList = repositoryReportMaster.findByReportId(reportId);
			if (reportMasterList != null && !reportMasterList.isEmpty()) {
				dtoReportMaster.setIsRepeat(true);
			} else {
				dtoReportMaster.setIsRepeat(false);
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
		return dtoReportMaster;
	}

	/**
	 * @param dtoCompany
	 * @return
	 */
	public DtoCompany getReportsByCompanyId(DtoCompany dtoCompany) {
		LOGGER.info("getReportsByCompanyId method called!!");
		List<DtoReportMaster> dtoReportMasterList = new ArrayList<>();
		Company company = repositoryCompany.findOne(dtoCompany.getCompanyId());
		dtoCompany = new DtoCompany();
		
		if (company != null) {
			List<ReportMaster> reportMasterList = company.getReportMaster();

			if (reportMasterList != null && !reportMasterList.isEmpty()) {
				for (ReportMaster reportMaster : reportMasterList) {
					DtoReportMaster dtoReportMaster = new DtoReportMaster();
					dtoReportMaster.setId(reportMaster.getId());
					dtoReportMaster.setReportId(reportMaster.getReportId());
					dtoReportMaster.setReportName(reportMaster.getReportName());
					dtoReportMaster.setReportLink(reportMaster.getReportLink());
					dtoReportMaster.setReportDescription(reportMaster.getReportDescription());
					dtoReportMaster.setObjectId(reportMaster.getObjectId());
					dtoReportMaster.setContainerId(reportMaster.getContainerId());
					dtoReportMasterList.add(dtoReportMaster);
				}
				dtoCompany.setCompanyId(company.getCompanyId());
				dtoCompany.setDtoReportMaster(dtoReportMasterList);
			}
		}
		return dtoCompany;
	}

	/**
	 * @return
	 */
	public DtoSearch getAllCompanyWiseReports() {
		LOGGER.info("getAllCompanyWiseReports method called!!");
		DtoSearch dtoSearch = new DtoSearch();
		if (httpServletRequest.getHeader(Constant.USER_ID) != null) {
			List<Company> companyList = repositoryReportMaster.findAllCompanyWiseReports();
			List<DtoReportMaster> dtoReportMasterList = new ArrayList<>();
			for (Company company : companyList) {
				DtoReportMaster dtoReportMaster = null;
				List<ReportMaster> reportMasterList = company.getReportMaster();
				if (reportMasterList != null && !reportMasterList.isEmpty()) {
					for (ReportMaster reportMaster : reportMasterList) {
						dtoReportMaster = new DtoReportMaster();
						dtoReportMaster.setCompanyId(company.getCompanyId());
						dtoReportMaster.setCompanyName(company.getName());
						dtoReportMaster.setId(reportMaster.getId());
						dtoReportMaster.setReportId(reportMaster.getReportId());
						dtoReportMaster.setReportName(reportMaster.getReportName());
						dtoReportMaster.setReportLink(reportMaster.getReportLink());
						dtoReportMaster.setReportDescription(reportMaster.getReportDescription());
						dtoReportMaster.setObjectId(reportMaster.getObjectId());
						dtoReportMaster.setContainerId(reportMaster.getContainerId());
						dtoReportMasterList.add(dtoReportMaster);
					}
				}
			}
			dtoSearch.setRecords(dtoReportMasterList);
		}
		return dtoSearch;
	}

	/**
	 * @param dtoUser
	 * @return
	 */
	public DtoUser getReportsByUserId(DtoUser dtoUser) {
		LOGGER.info("getReportsByCompanyId method called!!");
		List<DtoReportMaster> dtoReportMasterList = new ArrayList<>();
		User user = repositoryUser.findOne(dtoUser.getUserId());
		dtoUser = new DtoUser();
		
		if (user != null) {
			List<ReportMaster> reportMasterList = user.getReportMaster();

			if (reportMasterList != null && !reportMasterList.isEmpty()) {
				for (ReportMaster reportMaster : reportMasterList) {
					DtoReportMaster dtoReportMaster = new DtoReportMaster();
					dtoReportMaster.setId(reportMaster.getId());
					dtoReportMaster.setReportId(reportMaster.getReportId());
					dtoReportMaster.setReportName(reportMaster.getReportName());
					dtoReportMaster.setReportLink(reportMaster.getReportLink());
					dtoReportMaster.setReportDescription(reportMaster.getReportDescription());
					dtoReportMaster.setObjectId(reportMaster.getObjectId());
					dtoReportMaster.setContainerId(reportMaster.getContainerId());
					dtoReportMasterList.add(dtoReportMaster);
				}
				dtoUser.setUserId(user.getUserId());
				dtoUser.setUserName(user.getUsername());
				dtoUser.setDtoReportMaster(dtoReportMasterList);
			}
		}
		return dtoUser;
	}
	
	/**
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public DtoSearch getAllUserWiseReports() {
		LOGGER.info("getAllUserWiseReports method called!!");		
		DtoSearch dtoSearch = new DtoSearch();
		if (httpServletRequest.getHeader(Constant.USER_ID) != null) {
			List<User> userList = repositoryReportMaster.findAllUserWiseReports();
			List<DtoReportMaster> dtoReportMasterList = new ArrayList<>();
			for (User user : userList) {
				DtoReportMaster dtoReportMaster = null;
				List<ReportMaster> reportMasterList = user.getReportMaster();
				if (reportMasterList != null && !reportMasterList.isEmpty()) {
					for (ReportMaster reportMaster : reportMasterList) {
						dtoReportMaster = new DtoReportMaster();
						dtoReportMaster.setUserId(user.getUserId());
						dtoReportMaster.setUserName(user.getUsername());
						dtoReportMaster.setId(reportMaster.getId());
						dtoReportMaster.setReportId(reportMaster.getReportId());
						dtoReportMaster.setReportName(reportMaster.getReportName());
						dtoReportMaster.setReportLink(reportMaster.getReportLink());
						dtoReportMaster.setReportDescription(reportMaster.getReportDescription());
						dtoReportMaster.setObjectId(reportMaster.getObjectId());
						dtoReportMaster.setContainerId(reportMaster.getContainerId());
						dtoReportMasterList.add(dtoReportMaster);
					}
				}
			}	
			dtoSearch.setRecords(dtoReportMasterList);
		}		
		return dtoSearch;
	}
	
	/**
	 * 
	 * @param dtoSearch
	 * @return
	 */
	public DtoSearch searchByUserReport(DtoSearch dtoSearch) {
		
		
		String condition = "";

		if (dtoSearch.getSortOn() != null || dtoSearch.getSortBy() != null) {
			switch (dtoSearch.getSortOn()) {
			case "reportId":
				condition += dtoSearch.getSortOn();
				break;
			case "reportLink":
				condition += dtoSearch.getSortOn();
				break;
			case "reportName":
				condition += dtoSearch.getSortOn();
				break;
			case "reportDescription":
				condition += dtoSearch.getSortOn();
				break;
			default:
				condition += "id";
			}
		} else {
			condition += "id";
			dtoSearch.setSortOn("");
			dtoSearch.setSortBy("");
		}
		dtoSearch.setCondition(condition);
		return dtoSearch;
		
	}
	
	/**
	 * 
	 * @param dtoCompany
	 * @return
	 */
	public DtoUser assignReportToComapnyUser(DtoUser dtoUser) {
		int langId = Integer.parseInt(httpServletRequest.getHeader(Constant.LANG_ID));
		int loggedIInUser = Integer.parseInt(httpServletRequest.getHeader(Constant.USER_ID));
		Integer categoryId = null;
		Language language = repositoryLanguage.findByLanguageIdAndIsDeleted(langId, false);
		User user = repositoryUser.findByUserIdAndIsDeleted(dtoUser.getUserId(), false);
		Company company = repositoryCompany.findByCompanyIdAndIsDeleted(dtoUser.getCompanyId(), false);
		List<ReportMaster> reportMasterList = repositoryReportMaster.findBySelectedReportId(dtoUser.getReportIds());
		user.setReportMaster(reportMasterList);
		repositoryUser.saveAndFlush(user);
		company.setReportMaster(reportMasterList);
		repositoryCompany.saveAndFlush(company);
		
		for (ReportMaster reportMaster : reportMasterList) {
			ScreenMenu screenMenu = null;
			Module module = repositoryModule.findOne(reportMaster.getModule().getModuleId());
			List<ScreenCategory> screenCategories = repositoryScreenCategory.findByModuleModuleIdAndIsDeleted(module.getModuleId(), false);
			String sideBarName = reportMaster.getReportName();
			sideBarName = sideBarName.replaceAll("\\s", "");
			String sideBarUrl = Character.toLowerCase(sideBarName.charAt(0)) + (sideBarName.length() > 1 ? sideBarName.substring(1) : "");
			
			if (screenCategories != null) {
				for (ScreenCategory screenCategory : screenCategories) {
					if (screenCategory.getScreenCategoryCode().equals("SC-1004") && screenCategory.getLanguage().getLanguageId() == langId) {
						categoryId = screenCategory.getScreenCategoryId();
					}
				}
			}
			
			ScreenCategory screenCategory = repositoryScreenCategory.findOne(categoryId);
			if(module.getModuleId() == 2 || module.getModuleId() == 21) {
				sideBarUrl = "/hcm/"+sideBarUrl;
			} else if(module.getModuleId() == 99 || module.getModuleId() == 100) {
				sideBarUrl = "/ims/"+sideBarUrl;
			}
			
			screenMenu = repositoryScreenMenu.findScreenMenuByUserIdAndCompanyIdAndModuleIdAndLangId(module.getModuleId(), langId, dtoUser.getUserId(), dtoUser.getCompanyId(), sideBarUrl);
			if (screenMenu != null) {
				screenMenu = repositoryScreenMenu.findByScreenMenuIdAndIsDeleted(screenMenu.getScreenMenuId(), false);
				screenMenu.setUpdatedBy(loggedIInUser);
				screenMenu.setUpdatedDate(new Date());
			} else {
				screenMenu = new ScreenMenu();
				screenMenu.setCreatedBy(loggedIInUser);
				screenMenu.setCreatedDate(new Date());
			}
			screenMenu.setDescription(reportMaster.getReportDescription());
			screenMenu.setHelpMessage(reportMaster.getReportDescription());
			screenMenu.setScreenName(reportMaster.getReportName());
			screenMenu.setSideMenu(reportMaster.getReportName());
			screenMenu.setDescription(reportMaster.getReportDescription());
			screenMenu.setHelpMessage(reportMaster.getReportDescription());
			screenMenu.setScreenName(reportMaster.getReportName());
			screenMenu.setSideMenu(reportMaster.getReportName());
			screenMenu.setSideMenuURL(sideBarUrl);
			screenMenu.setCompany(company);
			screenMenu.setUser(user);
			screenMenu.setLanguage(language);
			screenMenu.setModule(module);
			screenMenu.setScreenCategory(screenCategory);
			repositoryScreenMenu.saveAndFlush(screenMenu);
		}
		return dtoUser;
	}
	
	public DtoEjbiErpUserRelation getEJBIUserCredentials(Integer reportId) {
		LOGGER.info("getEJBIUserCredentials Method");
		DtoEjbiErpUserRelation dtoEjbiErpUserRelation = new DtoEjbiErpUserRelation();
		int langId = Integer.parseInt(httpServletRequest.getHeader(Constant.LANG_ID));
		int loggedIInUser = Integer.parseInt(httpServletRequest.getHeader(Constant.USER_ID));
		String companyTenantId =httpServletRequest.getHeader(Constant.TENANT_ID);
		Language language = repositoryLanguage.findByLanguageIdAndIsDeleted(langId, false);
		User user = repositoryUser.findByUserIdAndIsDeleted(loggedIInUser, false);
		Company company= repositoryCompany.findByTenantIdAndIsDeleted(companyTenantId, false);
		ReportMaster reportMaster = repositoryReportMaster.findOne(reportId);
		if (user != null && company!= null && language != null && reportMaster != null) {
			EjbiErpUserRelation ejbiErpUserRelation = repositoryEjbiErpUserRelation.findByUserUserIdAndCompanyCompanyIdAndReportMasterIdAndLanguageLanguageIdAndIsDeleted(user.getUserId(), company.getCompanyId(), reportMaster.getId(), language.getLanguageId(), false);
		
			if (ejbiErpUserRelation != null) {
				dtoEjbiErpUserRelation.setUserId(user.getUserId());
				dtoEjbiErpUserRelation.setCompanyId(company.getCompanyId());
				dtoEjbiErpUserRelation.setReportId(reportMaster.getId());
				dtoEjbiErpUserRelation.setEjbiUserName(ejbiErpUserRelation.getEjbiUserName());
				dtoEjbiErpUserRelation.setEjbiUserPassword(ejbiErpUserRelation.getEjbiUserPassword());
				dtoEjbiErpUserRelation.setContainerId(ejbiErpUserRelation.getContainerId());
				dtoEjbiErpUserRelation.setObjectId(ejbiErpUserRelation.getObjectId());
				dtoEjbiErpUserRelation.setEjbiurl(ejbiErpUserRelation.getEjbiurl());
			}
		}
		return dtoEjbiErpUserRelation;
	}
}
