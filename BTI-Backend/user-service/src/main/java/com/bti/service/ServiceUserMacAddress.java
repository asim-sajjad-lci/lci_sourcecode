/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bti.model.User;
import com.bti.model.UserMacAddress;
import com.bti.model.WhitelistIp;
import com.bti.repository.RepositoryUser;
import com.bti.repository.RepositoryUserMacAddress;
import com.bti.repository.RepositoryWhiteListIp;
import com.bti.util.UtilFindIPAddress;


/**
 * Description: Service UserIp
 * Name of Project: BTI
 * Created on: May 16, 2017
 * Modified on: May 16, 2017 11:11:38 AM
 * @author seasia
 * Version: 
 */
@Service("serviceUserIp")
@Transactional
public class ServiceUserMacAddress {

	@Autowired
	RepositoryWhiteListIp repositoryWhiteListIp;

	@Autowired
	RepositoryUser repositoryUser;

	@Autowired
	RepositoryUserMacAddress repositoryUserMacAddress;

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	/**
	 * @param request
	 * @param userName
	 * @return
	 */
	public boolean checkAllowedUserIpRequest(HttpServletRequest request, String userName) 
	{
		String userIpAddress = UtilFindIPAddress.getUserIp(request);
		String macAddress = httpServletRequest.getHeader("Mac-Address");
		User user = repositoryUser.findByusernameAndIsDeleted(userName, false);
		List<WhitelistIp> whitelistIp;
		whitelistIp = repositoryWhiteListIp.findByIpAddressAndIsActiveAndIsDeleted(userIpAddress, true, false);
		
		if (whitelistIp != null && !whitelistIp.isEmpty()) 
		{
			return true;
		} 
		else 
		{
			if (user != null) {
				List<UserMacAddress> userIpList = repositoryUserMacAddress.findByMacAddressAndUserUserIdAndIsActiveAndIsDeleted(
						macAddress, user.getUserId(), true, false);
				if (userIpList != null && !userIpList.isEmpty()) {
					return true;
				}  
			} 
			return false;
		}
	}

	/**
	 * @param userIp
	 * @return
	 */
	public boolean allowedIpRequest(String userIp) 
	{
		String macAddress = httpServletRequest.getHeader("Mac-Address");
		List<WhitelistIp> whitelistIpList = repositoryWhiteListIp.findByIpAddressAndIsActiveAndIsDeleted(userIp, true,
				false);
		if (whitelistIpList != null && !whitelistIpList.isEmpty()) {
			return true;
		} else {
			List<UserMacAddress> userIpList = repositoryUserMacAddress.findByMacAddressAndIsActiveAndIsDeleted(macAddress, true, false);
			return userIpList != null && !userIpList.isEmpty();
		}
	}
	
}
