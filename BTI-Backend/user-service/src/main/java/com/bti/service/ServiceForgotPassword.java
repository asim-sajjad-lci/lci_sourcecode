/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bti.model.User;
import com.bti.repository.RepositoryUser;
import com.bti.util.UtilRandomKey;

/**
 * Description: Service Forgot Password
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 11:11:38 AM
 * @author seasia
 * Version: 
 */

@Service(value = "serviceForgotPassword")
public class ServiceForgotPassword {
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ServiceForgotPassword.class);

	@Autowired
	RepositoryUser repositoryUser;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	 /**
	 * Description: Reset password and send email for the same
	 * @param email
	 * @return
	 */
	public boolean resetPasswordAndSendEmail(User userExist) {
		if (userExist != null && userExist.getEmail() != null) {
			String uniqueRandomKey = getUniQueRandomKeyByCompareUserPasswords();
			userExist.setPassword(passwordEncoder.encode(uniqueRandomKey));
			repositoryUser.saveAndFlush(userExist);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getUniQueRandomKeyByCompareUserPasswords() {
		String randomKey = new UtilRandomKey().nextRandomKey();
		User user = repositoryUser.findByPassword(randomKey);
		if (user == null) {
			return randomKey;
		} else {
			getUniQueRandomKeyByCompareUserPasswords();
			return null;
		}
	}
}
