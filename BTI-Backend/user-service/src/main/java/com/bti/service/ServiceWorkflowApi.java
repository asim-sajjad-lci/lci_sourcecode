package com.bti.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bti.constant.Constant;
import com.bti.constant.MessageLabel;
import com.bti.model.Company;
import com.bti.model.WorkflowUser;
import com.bti.model.dto.DtoWorkflowUser;
import com.bti.model.dto.DtoWorkflowUserDetails;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryEntityManager;
import com.bti.repository.RepositoryWorkflowUsers;
import com.bti.util.CommonUtils;
import com.mysql.jdbc.Constants;

@Service
public class ServiceWorkflowApi {
	
	
	private static final Integer COMPANY_OFFSET = 1000000;

	@Autowired
	private RepositoryWorkflowUsers repositoryWorkflowUsers;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private RepositoryEntityManager repositoryEntityManager;

	@Autowired
	private RepositoryCompany repositoryCompany;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	public String validateAndFetchUserDetails(DtoWorkflowUserDetails dtoWorkflowUserDetails, String userName, String password) {
		boolean isFetchedSuccessfully = fetchUserDetailsByUserName(dtoWorkflowUserDetails, userName);
		
		if (!isFetchedSuccessfully) {
			return MessageLabel.WORKFLOW_USER_NOT_FOUND;

		} else if (!dtoWorkflowUserDetails.getEmployeeActive()) {
			return MessageLabel.WORKFLOW_USER_INACTIVE;
		
		} else if(passwordEncoder.matches(password, dtoWorkflowUserDetails.getPassword())) {
			return MessageLabel.WORKFLOW_USER_AUTHENTICATED;
		
		} else {
			return MessageLabel.WORKFLOW_INVALID_PASSWORD;
		
		}
		
	}
	
	public DtoWorkflowUserDetails getUserDetailsById(int id) {
		DtoWorkflowUser dtoWorkflowUser = new DtoWorkflowUser();
		DtoWorkflowUserDetails dtoWorkflowUserDetails = null;
		
		parseWorkflowUserId(dtoWorkflowUser, id);
		
		WorkflowUser workflowUser = repositoryWorkflowUsers.findByUserHcmId(dtoWorkflowUser.getUserHcmId());
		
		if (workflowUser != null) {
			dtoWorkflowUser = convertToDto(workflowUser);
			Object[] resultSet = repositoryEntityManager.getWorkflowUserDetailsByUserHcmId(dtoWorkflowUser.getUserTenantId(), dtoWorkflowUser.getUserHcmId());
			dtoWorkflowUserDetails = new DtoWorkflowUserDetails();
			populateDtoWorkflowUserDetails(dtoWorkflowUserDetails, resultSet, dtoWorkflowUser.getUserCompanyId(), dtoWorkflowUser.getUserTenantId());
		} 		
		return dtoWorkflowUserDetails;
	}
	
	public boolean fetchUserDetailsByUserName(DtoWorkflowUserDetails dtoWorkflowUserDetails, String userName) {
		boolean isFetchedSuccessfully = false;
		DtoWorkflowUser dtoWorkflowUser = new DtoWorkflowUser();
		Object[] resultSet = null;
		WorkflowUser workflowUser = repositoryWorkflowUsers.findByUserName(userName);
		
		if (workflowUser != null) {
		
			resultSet = repositoryEntityManager.getWorkflowUserDetailsByUserName(workflowUser.getUserTenantId(), workflowUser.getUserName());
			
			if (resultSet != null) {
				populateDtoWorkflowUserDetails(dtoWorkflowUserDetails, resultSet, workflowUser.getUserCompanyId(), workflowUser.getUserTenantId());
				isFetchedSuccessfully = true;
			}
		
		} else {
			
			List<Company> companyList = repositoryCompany.findByIsDeletedAndHasWorkflowUsers(false, true);
			
			for (Company company: companyList) {
				
				int companyId = company.getCompanyId();
				String tenantId = company.getTenantId();

				resultSet = repositoryEntityManager.getWorkflowUserDetailsByUserName(tenantId, userName);
				
				if (resultSet != null) {
					populateDtoWorkflowUserDetails(dtoWorkflowUserDetails, resultSet, companyId, tenantId);

					dtoWorkflowUser.setUserHcmId(CommonUtils.parseInteger(resultSet[0].toString()));
					dtoWorkflowUser.setUserName(userName);
					dtoWorkflowUser.setUserCompanyId(companyId);
					dtoWorkflowUser.setUserTenantId(tenantId);
					workflowUser = convertToModel(dtoWorkflowUser);
					repositoryWorkflowUsers.saveAndFlush(workflowUser);

					isFetchedSuccessfully = true;
					
					break;
				}
			}
		} 
		return isFetchedSuccessfully;
	}
	
	public DtoWorkflowUserDetails getUserDetailsByErpUserId(Integer erpUserId) {
		DtoWorkflowUserDetails dtoWorkflowUserDetails = null;
		DtoWorkflowUser dtoWorkflowUser = new DtoWorkflowUser();
		Object[] resultSet = null;
			
		List<Company> companyList = repositoryCompany.findByIsDeletedAndHasWorkflowUsers(false, true);
		
		for (Company company: companyList) {
			
			int companyId = company.getCompanyId();
			String tenantId = company.getTenantId();

			resultSet = repositoryEntityManager.getWorkflowUserDetailsByErpUserId(tenantId, erpUserId);
			
			if (resultSet != null) {
				dtoWorkflowUserDetails = new DtoWorkflowUserDetails();
				populateDtoWorkflowUserDetails(dtoWorkflowUserDetails, resultSet, companyId, tenantId);

				break;
			}
		}
		return dtoWorkflowUserDetails;
	}
	
	private void populateDtoWorkflowUserDetails(DtoWorkflowUserDetails dtoWorkflowUserDetails, Object[] resultSet, int userCompanyId, String tenantid) {
		dtoWorkflowUserDetails.setTenantid(tenantid);
		Integer userId = -1;
		
		if (resultSet[0] != null ) {
			userId = generateUserIdForWorkflow(CommonUtils.parseInteger(resultSet[0].toString()), userCompanyId);
		}
		
		dtoWorkflowUserDetails.setUserId(userId);
		if (resultSet[1] != null)  dtoWorkflowUserDetails.setUserName(resultSet[1].toString());
		
		if (resultSet[2] != null)  dtoWorkflowUserDetails.setEmployeeTitle(resultSet[2].toString());
		if (resultSet[3] != null)  dtoWorkflowUserDetails.setEmployeeFirstName(resultSet[3].toString());
		if (resultSet[4] != null)  dtoWorkflowUserDetails.setEmployeeLastName(resultSet[4].toString());

		if (resultSet[5] != null)  dtoWorkflowUserDetails.setEmployeeTitleArabic(resultSet[5].toString());
		if (resultSet[6] != null)  dtoWorkflowUserDetails.setEmployeeFirstNameArabic(resultSet[6].toString());
		if (resultSet[7] != null)  dtoWorkflowUserDetails.setEmployeeLastNameArabic(resultSet[7].toString());

		if (resultSet[8] != null)  dtoWorkflowUserDetails.setEmployeeJobTitle(resultSet[8].toString());
		if (resultSet[9] != null)  dtoWorkflowUserDetails.setEmployeeJobTitleArabic(resultSet[9].toString());

		if (resultSet[10] != null)  dtoWorkflowUserDetails.setManagerUserName(resultSet[10].toString());
		if (resultSet[11] != null)  dtoWorkflowUserDetails.setEmployeeActive(!Boolean.valueOf(resultSet[11].toString()));
		if (resultSet[12] != null)  dtoWorkflowUserDetails.setErpUserId(CommonUtils.parseInteger(resultSet[12].toString()));
//		if (resultSet[13] != null)  dtoWorkflowUserDetails.setIcon(resultSet[13].toString());
		if (resultSet[14] != null)  dtoWorkflowUserDetails.setPassword(resultSet[14].toString());
		
	}
	
	private DtoWorkflowUser convertToDto(WorkflowUser workflowUser) {
		DtoWorkflowUser dtoWorkflowUser = modelMapper.map(workflowUser, DtoWorkflowUser.class);
	    return dtoWorkflowUser;
	} 
	
	private WorkflowUser convertToModel(DtoWorkflowUser dtoWorkflowUser) throws ParseException {
		WorkflowUser workflowUser = modelMapper.map(dtoWorkflowUser, WorkflowUser.class);
	    return workflowUser;
	}
	
	
	/*
	 * Computed value
	 */
	private Integer generateUserIdForWorkflow(int userHcmId, int userCompanyId) {
		if (userHcmId < 1 || userCompanyId < 1) {
			return -1;
		} else {
			return (userCompanyId * COMPANY_OFFSET) + userHcmId;
		}
	}

	/*
	 * Reverse Computed value
	 */
	private void parseWorkflowUserId(DtoWorkflowUser dtoWorkflowUser, Integer userId) {
		if (dtoWorkflowUser != null) {
			if (userId == null || userId < 1) {
				dtoWorkflowUser.setUserHcmId (-1);
				dtoWorkflowUser.setUserCompanyId (-1);
			} else {
				dtoWorkflowUser.setUserHcmId (userId % COMPANY_OFFSET);
				dtoWorkflowUser.setUserCompanyId (userId / COMPANY_OFFSET);
			}
		}
	}

}
