/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.bti.repository.RepositoryUser;

/**
 * Description: Service Authentication
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Service
public class ServiceAuthentication implements UserDetailsService {

	static Logger log = Logger.getLogger(ServiceAuthentication.class.getName());

	@Autowired
	RepositoryUser repositoryUser;

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) {
		com.bti.model.User userTo = repositoryUser.findByusername(username);
		if (userTo == null)
			return null;

		GrantedAuthority authority = new SimpleGrantedAuthority("SUPERADMIN");
		 
		return new User(userTo.getUserId() + "", userTo.getPassword(),
				Arrays.asList(authority));
	}

}