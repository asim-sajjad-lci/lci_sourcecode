/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.BtiMessage;
import com.bti.model.dto.DtoBtiMessage;
import com.bti.repository.RepositoryException;

/**
 * Description: Service Response
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

@Service("ServiceResponse")
public class ServiceResponse {

	static Logger log = Logger.getLogger(ServiceResponse.class.getName());

	@Autowired
	RepositoryException repositoryException;

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @param message
	 * @param b
	 * @return
	 */
	public DtoBtiMessage getMessageByShortAndIsDeleted(String message, boolean b) {
		String langId = httpServletRequest.getHeader("langId");
		if (langId == null || "".equals(langId)) {
			langId = "1";
		}
		BtiMessage exceptionMessage = repositoryException.findByMessageShortAndIsDeletedAndLanguageLanguageId(message, false,Integer.parseInt(langId));
		return new DtoBtiMessage(exceptionMessage);
	}

	/**
	 * @param message
	 * @param b
	 * @return
	 */
	public String getStringMessageByShortAndIsDeleted(String message, boolean b) 
	{
		String responseMessage = "";
		String langId = httpServletRequest.getHeader("langId");
		BtiMessage exceptionMessage = repositoryException.findByMessageShortAndIsDeletedAndLanguageLanguageId(message, false,Integer.parseInt(langId));
		if(exceptionMessage!=null)
		{
			responseMessage = exceptionMessage.getMessage();
		}
		else
		{
			responseMessage="N/A";
		}
		return responseMessage;
	}

}
