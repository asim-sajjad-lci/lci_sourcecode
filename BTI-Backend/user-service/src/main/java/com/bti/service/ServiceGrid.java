package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.Grid;
import com.bti.model.GridData;
import com.bti.model.dto.DtoGrid;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryGrid;
import com.bti.repository.RepositoryGridData;

@Service("serviceGrid")
public class ServiceGrid {

	/**
	 * @Description LOGGER use for put a logger in Field Service
	 */
	static Logger logger = Logger.getLogger(ServiceGrid.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Field service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Field service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	RepositoryGrid repositoryGrid;

	@Autowired
	RepositoryGridData repositoryGridData;

	public DtoSearch getAllGrids(DtoGrid dtoGrid) {
		logger.info("getAllGrid method called");
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoGrid.getPageNumber());
		dtoSearch.setPageSize(dtoGrid.getPageSize());
		dtoSearch.setTotalCount(repositoryGrid.getCountOfTotalGrid());

		List<Grid> gridList = null;
		List<GridData> gridDataList = null;

		gridDataList = repositoryGridData.findByGridIdAndIsReset(dtoGrid.getGridId(), loggedInUserId);

		if (dtoGrid.getPageNumber() != null && dtoGrid.getPageSize() != null && gridDataList != null && gridDataList.size() > 0) {
			gridDataList = repositoryGridData.findByGridIdAndIsReset(dtoGrid.getGridId(), loggedInUserId);
		} else {
			gridList = repositoryGrid.findByGridIdAndIsDeleted(dtoGrid.getGridId());
		}

		List<DtoGrid> dtoGridList = new ArrayList<>();
		if (gridList != null && gridList.size() > 0) {
			for (Grid grid : gridList) {
				dtoGrid = new DtoGrid();
				dtoGrid.setGridId(grid.getGridId());
				dtoGridList.add(dtoGrid);
			}
		}
		return dtoSearch;
	}
}
