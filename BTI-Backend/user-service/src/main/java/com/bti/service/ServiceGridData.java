package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.Field;
import com.bti.model.Grid;
import com.bti.model.GridData;
import com.bti.model.Module;
import com.bti.model.Screen;
import com.bti.model.User;
import com.bti.model.dto.DtoGrid;
import com.bti.model.dto.DtoGridData;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryFields;
import com.bti.repository.RepositoryGrid;
import com.bti.repository.RepositoryGridData;
import com.bti.repository.RepositoryModule;
import com.bti.repository.RepositoryScreen;
import com.bti.repository.RepositoryUser;

@Service("serviceGridData")
public class ServiceGridData {

	/**
	 * @Description LOGGER use for put a logger in Field Service
	 */
	static Logger logger = Logger.getLogger(ServiceField.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for access of httpServletRequest method in Field service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for access of serviceResponse method in Field service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	@Autowired
	ServiceGrid serviceGrid;

	@Autowired
	RepositoryFields repositoryFields;
	@Autowired
	RepositoryGrid repositoryGrid;

	@Autowired
	RepositoryGridData repositoryGridData;

	@Autowired
	RepositoryModule repositoryModule;

	@Autowired
	RepositoryScreen repositoryScreen;

	@Autowired
	RepositoryUser repositoryUser;

	public DtoGridData changeVisible(DtoGridData dtoGridData) {
		logger.info("Change Visible Method");
		// DtoGridData dtoGridData = new DtoGridData();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		List<GridData> dtoGridDataList = new ArrayList<>();

		try {
			for (DtoGridData fieldId : dtoGridData.getColumnList()) {
				GridData gridData = repositoryGridData.findByFieldFieldIdAndIsDeleted(fieldId.getFieldId(), loggedInUserId);

				if (gridData != null) {
					if (fieldId.getIsVisible() == null) {
						repositoryGridData.changeStatus(true, loggedInUserId, fieldId.getFieldId(), fieldId.getColOrder(), loggedInUserId);
					} else {
						repositoryGridData.changeStatus(fieldId.getIsVisible(), loggedInUserId, fieldId.getFieldId(), fieldId.getColOrder(), loggedInUserId);
					}
					repositoryGridData.changeStatusReset(fieldId.getFieldId(), loggedInUserId);
					dtoGridDataList.add(gridData);
				} else {
					GridData gridDatas = new GridData();
					Field field = repositoryFields.findOne(fieldId.getFieldId());
					Screen screen = repositoryScreen.findOne(field.getScreen().getScreenId());
					Module module = repositoryModule.findOne(screen.getModule().getModuleId());
					Grid grid = repositoryGrid.findOne(field.getGrid().getGridId());
					gridDatas.setFieldId(field);
					gridDatas.setScreenId(screen);
					gridDatas.setModuleId(module);
					gridDatas.setGridId(grid);
					if (fieldId.getIsVisible() == null) {
						gridDatas.setIsVisible(true);
					} else {
						gridDatas.setIsVisible(fieldId.getIsVisible());
					}

					gridDatas.setIsReset(false);
					gridDatas.setColOrder(fieldId.getColOrder());
					gridDatas.setCreatedBy(loggedInUserId);
					gridDatas.setUpdatedBy(loggedInUserId);
					repositoryGridData.saveAndFlush(gridDatas);
				}
			}
		} catch (NumberFormatException ne) {
			ne.printStackTrace();
		}
		return dtoGridData;
	}

	public DtoGridData hideAllColumns(Integer gridId) {
		logger.info("Hide All Column Method called!!");
		DtoGridData dtoGridData = new DtoGridData();
		/* DtoGrid dtoGrid = new DtoGrid(); */
		DtoSearch dtoSearch = new DtoSearch();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());

		List<GridData> gridDataList = new ArrayList<>();
		List<DtoGridData> dtoGridDataList = new ArrayList<>();

		try {
			gridDataList = repositoryGridData.findByGridIdAndIsReset(gridId, loggedInUserId);
			if (gridDataList != null && gridDataList.size() > 0) {
				DtoGridData dtoGridData2 = new DtoGridData();
				repositoryGridData.hideAllColumns(loggedInUserId, gridId);
				dtoGridDataList.add(dtoGridData2);
				dtoGridData.setColumnList(dtoGridDataList);
			}
		} catch (NumberFormatException ne) {
			ne.printStackTrace();
		}
		logger.debug("Hide Columns :" + dtoGridData.getGridDataId());
		return dtoGridData;
	}

	public DtoGridData showAllColumns(Integer gridId) {
		logger.info("Show All Column Method called!!");
		DtoGridData dtoGridData = new DtoGridData();
		DtoSearch dtoSearch = new DtoSearch();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());

		List<GridData> gridDataList = new ArrayList<>();
		List<DtoGridData> dtoGridDataList = new ArrayList<>();

		try {
			gridDataList = repositoryGridData.findByGridIdAndIsReset(gridId, loggedInUserId);
			if (gridDataList != null && gridDataList.size() > 0) {
				DtoGridData dtoGridData2 = new DtoGridData();
				repositoryGridData.showAllColumns(loggedInUserId, gridId);
				dtoGridDataList.add(dtoGridData2);
				dtoGridData.setColumnList(dtoGridDataList);
			}
		} catch (NumberFormatException ne) {
			ne.printStackTrace();
		}
		logger.debug("Show Columns :" + dtoGridData.getGridDataId());
		return dtoGridData;
	}

	public DtoGridData resetGrid(Integer gridId) {
		logger.info("Reset All Column Method called!!");
		DtoGridData dtoGridData = new DtoGridData();
		/* DtoGrid dtoGrid = new DtoGrid(); */
		DtoSearch dtoSearch = new DtoSearch();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());

		List<GridData> gridDataList = new ArrayList<>();
		List<DtoGridData> dtoGridDataList = new ArrayList<>();

		try {
			gridDataList = repositoryGridData.findByGridIdAndIsReset(gridId, loggedInUserId);
			if (gridDataList != null && gridDataList.size() > 0) {
				// GridData GridData2 = new GridData();
				DtoGridData dtoGridData2 = new DtoGridData();
				repositoryGridData.resetGrid(loggedInUserId, gridId);
				dtoGridDataList.add(dtoGridData2);
				dtoGridData.setColumnList(dtoGridDataList);
			}
		} catch (NumberFormatException ne) {
			ne.printStackTrace();
		}
		logger.debug("Reset Columns :" + dtoGridData.getGridDataId());
		return dtoGridData;
	}

}
