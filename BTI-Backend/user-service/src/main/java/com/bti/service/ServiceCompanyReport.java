package com.bti.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bti.model.Company;
import com.bti.model.CompanyReport;
import com.bti.model.ReportMaster;
import com.bti.model.dto.DtoCompanyReport;
import com.bti.model.dto.DtoReportMaster;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryCompanyReport;
import com.bti.repository.RepositoryReportMaster;

@Service("serviceCompanyReport")
public class ServiceCompanyReport {

	
	
	private static final Logger LOGGER = Logger.getLogger(ServiceCompanyReport.class);
	
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;
	
	@Autowired(required = false)
	ServiceResponse serviceResponse;
	
	private static final String USER_ID ="userid";
	
	@Autowired
	RepositoryCompanyReport repositoryCompanyReport;
	
	
	@Autowired
	RepositoryCompany repositoryCompany;
	
	@Autowired
	RepositoryReportMaster repositoryReportMaster;


	/**
	 * 
	 * @param dtoCompanyReport
	 * @return
	 */
	public DtoCompanyReport saveOrUpdateCompanyReport(DtoCompanyReport dtoCompanyReport){
		CompanyReport companyReport = null;
		Company company = null;
		
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader(USER_ID));
		try {
			company= repositoryCompany.findByCompanyIdAndIsDeleted(dtoCompanyReport.getCompanyId(), false);
			List<ReportMaster> reportMasterList = new ArrayList<>();
			for (DtoReportMaster dtoReportMaster : dtoCompanyReport.getDtoReportMaster()) {
				ReportMaster reportMaster = repositoryReportMaster.findByIdAndIsDeleted(dtoReportMaster.getId(),false);
				reportMasterList.add(reportMaster);
			}
			if(company!=null && !reportMasterList.isEmpty() ) {
				if (dtoCompanyReport.getId() != null && dtoCompanyReport.getId() > 0) {

					companyReport = repositoryCompanyReport.findByIdAndIsDeleted(dtoCompanyReport.getId(), false);
					if(!companyReport.getReportMasterList().isEmpty()) {
						for (ReportMaster reportMaster : reportMasterList) {
							repositoryCompanyReport.deleteReportMasterList(reportMaster.getId());
						}
							
					}
					companyReport = new CompanyReport();
					companyReport.setCreatedDate(new Date());
					companyReport.setCreatedBy(loggedInUserId);
					companyReport.setUpdatedDate(new Date());
					companyReport.setUpdatedBy(loggedInUserId);
				} else {
					companyReport = new CompanyReport();
					companyReport.setCreatedDate(new Date());
					companyReport.setCreatedBy(loggedInUserId);
				}
				companyReport.setReportMasterList(reportMasterList);
				companyReport.setCompany(company);
				repositoryCompanyReport.saveAndFlush(companyReport);	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dtoCompanyReport;

	}


}
