/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bti.model.RequestResponseLog;
import com.bti.model.dto.DtoRequestResponseLog;
import com.bti.repository.RepositoryRequestResponseLog;


/**
 * Description: Service Role Group
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 4:19:38 PM
 * @author SNS
 * Version: 
 */
@Service("ServiceRequestResponseLog")
public class ServiceRequestResponseLog {

	private static final Logger LOGGER = Logger.getLogger(ServiceRequestResponseLog.class);


	@Autowired
	RepositoryRequestResponseLog repositoryRequestResponseLog;



	/**
	 * Description: search role group by search keyword
	 * @param dtoSearch
	 * @return
	 */
	@Transactional
	public void logRequest(DtoRequestResponseLog dtoRequestResponseLog) {
		
		RequestResponseLog requestResponseLog = new RequestResponseLog();
		
		requestResponseLog.setRequestId(dtoRequestResponseLog.getRequestId());
		requestResponseLog.setHost(dtoRequestResponseLog.getHost());
		requestResponseLog.setLangId(dtoRequestResponseLog.getLangId());
		requestResponseLog.setOrigin(dtoRequestResponseLog.getOrigin());
		requestResponseLog.setReferer(dtoRequestResponseLog.getReferer());
		requestResponseLog.setSession(dtoRequestResponseLog.getSession());
		requestResponseLog.setTenantId(dtoRequestResponseLog.getTenantId());
		requestResponseLog.setUserId(dtoRequestResponseLog.getUserId());
		requestResponseLog.setRequestJson(dtoRequestResponseLog.getRequestJson());
		requestResponseLog.setCreated(dtoRequestResponseLog.getCreated());
		
		repositoryRequestResponseLog.saveAndFlush(requestResponseLog);
	}
	
	@Transactional
	public void logResponse(DtoRequestResponseLog dtoRequestResponseLog) {
		
		boolean isSuccess = false;
		long rows = 0;
		try {
			rows = this.repositoryRequestResponseLog.logResponse(dtoRequestResponseLog.getResponseJson(), dtoRequestResponseLog.getUpdated(), dtoRequestResponseLog.getRequestId());
		} catch (Exception ex) {
			isSuccess = false;
		}
		
		if (rows <= 0 || !isSuccess) {
			
			RequestResponseLog requestResponseLog = new RequestResponseLog();
			
			requestResponseLog.setRequestId(dtoRequestResponseLog.getRequestId());
			requestResponseLog.setHost(dtoRequestResponseLog.getHost());
			requestResponseLog.setLangId(dtoRequestResponseLog.getLangId());
			requestResponseLog.setOrigin(dtoRequestResponseLog.getOrigin());
			requestResponseLog.setReferer(dtoRequestResponseLog.getReferer());
			requestResponseLog.setSession(dtoRequestResponseLog.getSession());
			requestResponseLog.setTenantId(dtoRequestResponseLog.getTenantId());
			requestResponseLog.setUserId(dtoRequestResponseLog.getUserId());
			requestResponseLog.setRequestJson(dtoRequestResponseLog.getRequestJson());
			requestResponseLog.setCreated(dtoRequestResponseLog.getCreated());

			requestResponseLog.setResponseJson(dtoRequestResponseLog.getResponseJson());
			requestResponseLog.setUpdated(dtoRequestResponseLog.getUpdated());
			
			this.repositoryRequestResponseLog.saveAndFlush(requestResponseLog);
			
		}
	}
	

}
