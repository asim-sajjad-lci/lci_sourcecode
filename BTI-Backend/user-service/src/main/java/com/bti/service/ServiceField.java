/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.bti.model.Company;
import com.bti.model.Field;
import com.bti.model.FieldAccess;
import com.bti.model.Language;
import com.bti.model.Module;
import com.bti.model.dto.DtoFieldAccess;
import com.bti.model.dto.DtoFieldAccessDeatils;
import com.bti.model.dto.DtoFieldDetail;
import com.bti.model.dto.DtoSearch;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryFieldAccess;
import com.bti.repository.RepositoryFields;
import com.bti.repository.RepositoryLanguage;
import com.bti.repository.RepositoryModule;

/**
 * Description: Service Field Project
 * Version: 0.0.1
 */
@Service("serviceField")
public class ServiceField {

	/**
	 * @Description LOGGER use for put a logger in Field Service
	 */
	static Logger log = Logger.getLogger(ServiceField.class.getName());

	/**
	 * @Description httpServletRequest Autowired here using annotation of spring for
	 *              access of httpServletRequest method in Field service
	 */
	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	/**
	 * @Description serviceResponse Autowired here using annotation of spring for
	 *              access of serviceResponse method in Field service
	 */
	@Autowired(required = false)
	ServiceResponse serviceResponse;

	/**
	 * @Description repositoryField Autowired here using annotation of spring for
	 *              access of repositoryField method in Field service In short
	 *              Access Field Query from Database using repositoryField.
	 */
	@Autowired
	RepositoryFields repositoryFields;

	@Autowired
	RepositoryLanguage repositoryLanguage;

	@Autowired
	RepositoryFieldAccess repositoryFieldAccess;

	@Autowired
	RepositoryCompany repositoryCompany;

	@Autowired
	RepositoryModule repositoryModule;

	public DtoSearch getAllFields(DtoFieldDetail dtoFieldDetail) {
		log.info("getAllFields Method");
		DtoSearch dtoSearch = new DtoSearch();
		dtoSearch.setPageNumber(dtoFieldDetail.getPageNumber());
		dtoSearch.setPageSize(dtoFieldDetail.getPageSize());
		dtoSearch.setTotalCount(repositoryFields.getCountOfTotalField());
		List<Field> fieldList = null;
		if (dtoFieldDetail.getPageNumber() != null && dtoFieldDetail.getPageSize() != null) {
			Pageable pageable = new PageRequest(dtoFieldDetail.getPageNumber(), dtoFieldDetail.getPageSize(),
					Direction.DESC, "createdDate");
			fieldList = repositoryFields.findByIsDeleted(false, pageable);
		} else {
			fieldList = repositoryFields.findByIsDeletedOrderByCreatedDateDesc(false);
		}

		List<DtoFieldDetail> dtoFieldDetailList = new ArrayList<>();
		if (fieldList != null && fieldList.size() > 0) {
			for (Field field : fieldList) {
				dtoFieldDetail = new DtoFieldDetail(field);
				dtoFieldDetail.setFieldName(field.getFieldName());
				dtoFieldDetail.setIsMandatory(field.getIsMandatory());
				dtoFieldDetail.setFieldId(field.getFieldId());
				dtoFieldDetailList.add(dtoFieldDetail);

			}
			dtoSearch.setRecords(dtoFieldDetailList);
		}
		log.debug("All Field List Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoSearch getAllFieldsByCompanyId(DtoFieldAccess dtoFieldAccess) {
		log.info("getAllFields Method");
		List<DtoFieldAccess> listFieldAccesses = new ArrayList<>();
		DtoSearch dtoSearch = new DtoSearch();
		List<FieldAccess> fieldAccesses = repositoryFieldAccess.findbyCompanyId(dtoFieldAccess.getCompanyId());

		for (FieldAccess fieldAccess : fieldAccesses) {
			DtoFieldAccess access = new DtoFieldAccess();
			access.setFieldAccessId(fieldAccess.getFieldAccessId());
			access.setFieldId(fieldAccess.getField().getFieldId());
			access.setFieldName(fieldAccess.getField().getFieldName());
			access.setCompanyId(fieldAccess.getCompany().getCompanyId());
			access.setIsMandatory(fieldAccess.getMandatory());
			access.setLanguageId(fieldAccess.getLanguage().getLanguageId());
			access.setModuleId(fieldAccess.getModule().getModuleId());
			// access.setScreenId(fieldAccess.getScreen().getScreenId());
			listFieldAccesses.add(access);
		}

		Company company = repositoryCompany.findOne(dtoFieldAccess.getCompanyId());
		// Module module = repositoryModule.findOne(dtoFieldAccess.getModuleId());
		List<Integer> fieldIds = new ArrayList<>();

		for (FieldAccess fieldAccess : fieldAccesses) {
			if (fieldAccess.getField() != null) {
				fieldIds.add(fieldAccess.getField().getFieldId());
			}
		}

		List<Field> Listfield = repositoryFields.findByRemainFieldId(fieldIds);

		for (Field field : Listfield) {
			DtoFieldAccess access = new DtoFieldAccess();
			// fieldAccess.setModule(field);
			access.setFieldId(field.getFieldId());
			access.setFieldName(field.getFieldName());
			access.setCompanyId(company.getCompanyId());
			access.setIsMandatory(field.getIsMandatory());
			access.setLanguageId(field.getLanguage().getLanguageId());
			// access.setModuleId(module.getModuleId());
			// access.setScreenId(field.getScreen().getScreenId());
			listFieldAccesses.add(access);
		}
		dtoSearch.setRecords(listFieldAccesses);
		log.debug("All Field List Size is:" + dtoSearch.getTotalCount());
		return dtoSearch;
	}

	public DtoFieldDetail changeStatus(List<Integer> ids) {
		log.info("deleteDepartment Method");
		DtoFieldDetail dtoDepartment = new DtoFieldDetail();
		List<Field> deleteDepartment = new ArrayList<>();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		try {
			for (Integer departmentId : ids) {
				Field department = repositoryFields.findOne(departmentId);

				if (department.getIsMandatory() != null) {
					Field dtoDepartment2 = new Field();
					repositoryFields.changeStatus(!department.getIsMandatory(), loggedInUserId, departmentId);
					deleteDepartment.add(dtoDepartment2);
				} else {
					Field dtoDepartment2 = new Field();
					repositoryFields.changeStatus(true, loggedInUserId, departmentId);
					deleteDepartment.add(dtoDepartment2);
				}
			}
			dtoDepartment.setStatus(deleteDepartment);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return dtoDepartment;
	}

	public DtoFieldDetail changeLanguage(List<DtoFieldAccessDeatils> languageIds) {
		log.info("change Language Id Method called");
		DtoFieldDetail dtoFieldDetail = new DtoFieldDetail();
		int loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid").toString());
		try {
			for (DtoFieldAccessDeatils dtoFieldAccessDeatils : languageIds) {
				Field field = repositoryFields.findOne(dtoFieldAccessDeatils.getFieldId());
				Language language = repositoryLanguage.findByLanguageIdAndIsDeleted(dtoFieldAccessDeatils.getLanguageId(), false);

				if (field.getLanguage().getLanguageId() != 0 && field.getLanguage().getLanguageId() > 0) {
					field.setLanguage(language);
					repositoryFields.saveAndFlush(field);
				}
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return dtoFieldDetail;
	}
}