/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bti.constant.BTIRoles;
import com.bti.constant.Constant;
import com.bti.constant.MessageLabel;
import com.bti.model.BtiMessage;
import com.bti.model.CityMaster;
import com.bti.model.CommonConstant;
import com.bti.model.Company;
import com.bti.model.CountryMaster;
import com.bti.model.Field;
import com.bti.model.FieldValidation;
import com.bti.model.Language;
import com.bti.model.Module;
import com.bti.model.Screen;
import com.bti.model.StateMaster;
import com.bti.model.User;
import com.bti.model.UserCompanyRelation;
import com.bti.model.UserDetail;
import com.bti.model.UserDraft;
import com.bti.model.UserSession;
import com.bti.model.ValidationMessage;
import com.bti.model.WeekDay;
import com.bti.model.dto.DtoCompany;
import com.bti.model.dto.DtoCountry;
import com.bti.model.dto.DtoLanguage;
import com.bti.model.dto.DtoModule;
import com.bti.model.dto.DtoScreenDetail;
import com.bti.model.dto.DtoSearch;
import com.bti.model.dto.DtoUser;
import com.bti.repository.RepositoryAuthorizationSetting;
import com.bti.repository.RepositoryBtiMessage;
import com.bti.repository.RepositoryCityMaster;
import com.bti.repository.RepositoryCommonConstant;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryCountryMaster;
import com.bti.repository.RepositoryFieldValidation;
import com.bti.repository.RepositoryFields;
import com.bti.repository.RepositoryLanguage;
import com.bti.repository.RepositoryLoginOtp;
import com.bti.repository.RepositoryModule;
import com.bti.repository.RepositoryRole;
import com.bti.repository.RepositoryRoleGroup;
import com.bti.repository.RepositoryScreen;
import com.bti.repository.RepositoryStateMaster;
import com.bti.repository.RepositoryUser;
import com.bti.repository.RepositoryUserCompanyRelation;
import com.bti.repository.RepositoryUserDetail;
import com.bti.repository.RepositoryUserDetailPagination;
import com.bti.repository.RepositoryUserDraft;
import com.bti.repository.RepositoryUserGroup;
import com.bti.repository.RepositoryUserSession;
import com.bti.repository.RepositoryValidationMessages;
import com.bti.repository.RepositoryWeekDay;
import com.bti.util.CodeGenerator;
import com.bti.util.UtilRandomKey;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import static java.util.concurrent.TimeUnit.*;


/**
 * Description: Service Home
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 11:11:38 AM
 * @author seasia
 * Version: 
 */
@Service("serviceHome")
@Transactional
@EnableScheduling
public class ServiceHome {

	private static final Logger LOGGER = Logger.getLogger(ServiceHome.class);

	@Autowired
	RepositoryUser repositoryUser;

	@Autowired
	RepositoryUserDetail repositoryUserDetail;

	@Autowired
	RepositoryRole repositoryRole;

	@Autowired
	RepositoryUserGroup repositoryUserGroup;

	@Autowired
	RepositoryUserDetailPagination repositoryUserDetailPagination;

	@Autowired
	RepositoryRoleGroup repositoryRoleGroup;

	@Autowired(required = false)
	HttpServletRequest httpServletRequest;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	RepositoryLoginOtp repositoryLoginOtp;

	@Autowired
	ServiceEmailHandler serviceEmailHandler;

	@Autowired
	RepositoryUserCompanyRelation repositoryUserCompanies;

	@Autowired
	RepositoryCompany repositoryCompany;

	@Autowired
	CodeGenerator codeGenerator;

	@Autowired
	RepositoryAuthorizationSetting repositoryAuthorizationSetting;

	@Autowired
	RepositoryCountryMaster repositoryCountryMaster;

	@Autowired
	RepositoryStateMaster repositoryStateMaster;

	@Autowired
	RepositoryCityMaster repositoryCityMaster;
	
	@Autowired
	RepositoryLanguage repositoryLanguage;	
	
	@Autowired
	RepositoryModule repositoryModule;
	
	@Autowired
	RepositoryScreen repositoryScreen;
	
	@Autowired
	RepositoryFields repositoryFields;
	
	@Autowired
	RepositoryValidationMessages repositoryValidationMessages;
	
	@Autowired
	RepositoryFieldValidation repositoryFieldValidation;
	
	@Autowired
	ServiceResponse serviceResponse;
	
	@Autowired
	RepositoryCommonConstant repositoryCommonConstant;
	
	@Autowired
	RepositoryUserCompanyRelation repositoryUserCompanyRelation;
	
	@Autowired
	RepositoryBtiMessage repositoryBtiMessage;
	
	@Autowired
	RepositoryWeekDay repositoryWeekDay;
	
	@Autowired
	RepositoryUserSession repositoryUserSession;
	
	@Autowired
	RepositoryUserDraft repositoryUserDraft;
	
	@Value("${script.accessfinancialpath}")
	private String accessfinancialpath;
	
	private final Integer ENG_LANG_ID=1;
	
	 
	/**
	 * Description: get country list for drop down
	 * @return
	 */
	public List<DtoCountry> getCountryList() {
		String langId = httpServletRequest.getHeader(Constant.LANG_ID);
		List<DtoCountry> countryList = new ArrayList<>();
		List<CountryMaster> list = repositoryCountryMaster.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true, Integer.parseInt(langId));
		
		if(list.isEmpty()){
			list = repositoryCountryMaster.findByIsDeletedAndIsActiveAndLanguageLanguageId(false, true, 1);
		}
		
		if (list != null && !list.isEmpty()) {
			for (CountryMaster countryMaster : list) {
				DtoCountry dtoCountry = new DtoCountry();
				dtoCountry.setCountryId(countryMaster.getCountryId());
				dtoCountry.setCountryName(countryMaster.getCountryName());
				dtoCountry.setShortName(countryMaster.getShortName());
				dtoCountry.setCountryCode(countryMaster.getCountryCode());
				countryList.add(dtoCountry);
			}
		}
		return countryList;
	}

	/**
	 * Description: get state list by country id for drop down
	 * @param countryId
	 * @return
	 */
	public List<DtoCountry> getStateListByCountryId(int countryId) {
		String langId = httpServletRequest.getHeader(Constant.LANG_ID);
		List<DtoCountry> stateList = new ArrayList<>();
		List<StateMaster> list = repositoryStateMaster.findByCountryMasterCountryIdAndIsDeletedAndLanguageLanguageId(countryId, false, Integer.parseInt(langId));
		if(list.isEmpty()){
			list=repositoryStateMaster.findByCountryMasterCountryIdAndIsDeletedAndLanguageLanguageId(countryId, false, 1);
		}
		if (list != null && !list.isEmpty()) {
			for (StateMaster stateMaster : list) {
				DtoCountry dtoCountry = new DtoCountry();
				dtoCountry.setStateId(stateMaster.getStateId());
				dtoCountry.setStateName(stateMaster.getStateName());
				stateList.add(dtoCountry);
			}
		}
		return stateList;
	}

	/**
	 * Description: get city list by state id for drop down
	 * @param stateId
	 * @return
	 */
	public List<DtoCountry> getCityListByStateId(int stateId) {
		String langId = httpServletRequest.getHeader(Constant.LANG_ID);
		List<DtoCountry> cityList = new ArrayList<>();
		List<CityMaster> list = repositoryCityMaster.findByStateMasterStateIdAndIsDeletedAndLanguageLanguageId(stateId, false,Integer.parseInt(langId));
		
		if(list.isEmpty()){
			list=repositoryCityMaster.findByStateMasterStateIdAndIsDeletedAndLanguageLanguageId(stateId, false,1);
		}
		
		if (list != null && !list.isEmpty()) {
			for (CityMaster cityMaster : list) {
				DtoCountry dtoCountry = new DtoCountry();
				dtoCountry.setCityId(cityMaster.getCityId());
				dtoCountry.setCityName(cityMaster.getCityName());
				cityList.add(dtoCountry);
			}
		}
		return cityList;
	}

	/**
	 * Description: get country code by country id
	 * @param countryId
	 * @return
	 */
	public DtoCountry getCountryCodeByCountryId(int countryId) {
		DtoCountry dtoCountry = new DtoCountry();
		CountryMaster countryMaster = repositoryCountryMaster.findByCountryIdAndIsDeletedAndIsActive(countryId, false,
				true);
		if (countryMaster != null) {

			dtoCountry.setCountryId(countryMaster.getCountryId());
			dtoCountry.setCountryCode("+" + countryMaster.getCountryCode());
		}
		return dtoCountry;
	}

	/**
	 * Description: reset password for user by super admin
	 * @param user
	 * @param newPassword
	 */
	public void resetPasswordOfUser(User user, String newPassword) {
		user.setPassword(passwordEncoder.encode(newPassword));
		user.setResetPassword(false);
		repositoryUser.saveAndFlush(user);

	}

	/**
	 * 
	 * @param user
	 * @param password
	 */
	public void changePasswordOfUser(User user, String password) {
		user.setPassword(passwordEncoder.encode(password));
		user.setResetPassword(true);
		repositoryUser.saveAndFlush(user);
	}

	/**
	 * @param dtoUser
	 * @return
	 */
	public boolean matchOldPassword(DtoUser dtoUser) {
		User user = repositoryUser.findByUserIdAndIsDeleted(dtoUser.getUserId(), false);
		return user != null && passwordEncoder.matches(dtoUser.getPassword(), user.getPassword());
	}
   
	/**
	 * @param user
	 * @return
	 */
	public Boolean sendForgotPasswordEmail(User user) 
	{
		Boolean status = false;
		try {
			if (user != null) {
				// send forgot password email
				String newPassword = new UtilRandomKey().nextRandomKey();
				user.setPassword(this.passwordEncoder.encode(newPassword));
				user.setResetPassword(true);
				user = this.repositoryUser.saveAndFlush(user);
				UserDetail userDetail = this.repositoryUserDetail.findByUserUserId(user.getUserId());
				String firstName = null;
				if (userDetail != null && userDetail.getFirstName() != null) {
					firstName = userDetail.getFirstName();
				} else {
					firstName = "";
				}
				status = serviceEmailHandler.sendForgotPasswordEmail(user.getEmail(), newPassword, firstName);
//				status = true;
			}
		} catch (Exception e) {
			LOGGER.info(Arrays.toString(e.getStackTrace()));
		}

		return status;
	}
	
	/**
	 * Description: get language list for drop down
	 * @return
	 */
	public DtoSearch getLanguageList(DtoSearch dtoSearch ) 
	{
		List<Language> list =null;
		dtoSearch.setTotalCount(repositoryLanguage.getTotalCount());
		List<DtoLanguage> languagesList = new ArrayList<>();
		
		if (dtoSearch.getPageNumber() != null && dtoSearch.getPageSize() != null) 
		{
			Pageable pageable = new PageRequest(dtoSearch.getPageNumber(), dtoSearch.getPageSize());
			list = repositoryLanguage.findByIsDeleted(false,pageable);
		}
		else
		{
			list= repositoryLanguage.findByIsDeleted(false);
		}
		
		if (list != null && !list.isEmpty()) 
		{
			for (Language language : list) 
			{
				DtoLanguage dtoLanguage = new DtoLanguage(language);
				
				if (language.getIsActive()) {
					dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted(Constant.ACTIVE, false).getMessage());
				} else {
					dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted(Constant.INACTIVE, false).getMessage());
				}
				
				languagesList.add(dtoLanguage);
			}
		}
		
		dtoSearch.setRecords(languagesList);
		return dtoSearch;
	}
	
	public DtoLanguage addNewLanguage(DtoLanguage dtoLanguage) throws IOException 
	{
		Language language= new Language();
		language.setLanguageName(dtoLanguage.getLanguageName());
		language.setLanguageOrientation(dtoLanguage.getLanguageOrientation());
		language.setIsActive(true);
		language=repositoryLanguage.save(language);	
		final ServletContext servletContext = httpServletRequest.getSession().getServletContext();      
	    final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
	    final String temperotyFilePath = tempDirectory.getAbsolutePath();
	    String filePathTemp = temperotyFilePath+"//"+"newLanguage.xlsx";
	    File csvFile =  new File(filePathTemp);
	    if(csvFile.exists()){
		   csvFile.deleteOnExit();
	    }
	    dtoLanguage.getFile().transferTo(new File(filePathTemp));
	    uploadNewLanguageData(language,filePathTemp);
		return new DtoLanguage(language);
	}
	
	public DtoLanguage updateLanguage(DtoLanguage dtoLanguage) 
	{
		Language language= repositoryLanguage.findOne(dtoLanguage.getLanguageId());
		language.setLanguageName(dtoLanguage.getLanguageName());
		language.setLanguageOrientation(dtoLanguage.getLanguageOrientation());
		language=repositoryLanguage.save(language);	
		return new DtoLanguage(language);
	}
	
	public DtoLanguage getLanguageByLangId(DtoLanguage dtoLanguage) 
	{
		Language language= repositoryLanguage.findByLanguageIdAndIsDeleted(dtoLanguage.getLanguageId(), false);
		dtoLanguage= new DtoLanguage(language);
		if (language.getIsActive()) {
			dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted("ACTIVE", false).getMessage());
		} else {
			dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted("INACTIVE", false).getMessage());
		}
		return dtoLanguage;
	}
	
	public DtoLanguage getLanguageByLanguageId(DtoLanguage dtoLanguage) 
	{
		Language language= repositoryLanguage.findOne(dtoLanguage.getLanguageId());
		dtoLanguage= new DtoLanguage(language);
		
		if (language.getIsActive()) {
			dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted("ACTIVE", false).getMessage());
		} else {
			dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted("INACTIVE", false).getMessage());
		}
		
		return dtoLanguage;
	}
	
	public void deleteLanguage(Language language) 
	{
		 repositoryLanguage.delete(language);
	}
	
	public void uploadNewLanguageData(Language language, String tempraryFilePath )
	{
		 try {
	            FileInputStream excelFile = new FileInputStream(new File(tempraryFilePath));
	            Workbook workbook = new XSSFWorkbook(excelFile);
	            Sheet moduleSheet = workbook.getSheetAt(0);
                
	            for (int i = 1; i <= moduleSheet.getLastRowNum(); i++) 
	            {
	            	Row nextRow = moduleSheet.getRow(i);
                    if(nextRow==null){
                        break;
                    }
                    
                    String moduleCode  =  nextRow.getCell(0).getStringCellValue();
                    String moduleDescription =  nextRow.getCell(1).getStringCellValue();
                    String modulname =  nextRow.getCell(2).getStringCellValue();
                    String helpMessage = nextRow.getCell(3).getStringCellValue();
                    
                    Module module = new Module();
                    module.setDescription(moduleDescription);
                    module.setHelpMessage(helpMessage);
                    module.setLanguage(language);
                    module.setModuleCode(moduleCode);
                    module.setName(modulname);
                    module.setCreatedBy(1);
                    repositoryModule.saveAndFlush(module);
	            }
	            
	            Sheet screenSheet = workbook.getSheetAt(1);
	            for (int i = 1; i <= screenSheet.getLastRowNum(); i++) 
	            {
	            	Row nextRow = screenSheet.getRow(i);
                    if(nextRow==null){
                        break;
                    }
                    
                    String screenCode  =  nextRow.getCell(0).getStringCellValue();
                    String moduleCode =  nextRow.getCell(1).getStringCellValue();
                    String descr =  nextRow.getCell(2).getStringCellValue();
                    String sideMenuUrl = nextRow.getCell(3).getStringCellValue();
                    String helpMessage =  nextRow.getCell(4).getStringCellValue();
                    String screenName =  nextRow.getCell(5).getStringCellValue();
                    String sideMenu = nextRow.getCell(6).getStringCellValue();
                    
                    Screen screen = new Screen();
                    screen.setScreenCode(screenCode);
                    screen.setModule(repositoryModule.findByModuleCodeAndIsDeletedAndLanguageLanguageId(moduleCode, false, language.getLanguageId()));
                    screen.setDescription(descr);
                    screen.setLanguage(language);
                    if(UtilRandomKey.isNotBlank(sideMenuUrl)){
                    	screen.setSideMenuURL(sideMenuUrl);
                    }
                    screen.setHelpMessage(helpMessage);
                    screen.setScreenName(screenName);
                    if(UtilRandomKey.isNotBlank(sideMenu)){
                    	   screen.setSideMenu(sideMenu);
                    }
                    repositoryScreen.save(screen);
	            }
	            
	            Sheet fieldSheet = workbook.getSheetAt(2);
	            for (int i = 1; i <= fieldSheet.getLastRowNum(); i++) 
	            {
	            	Row nextRow = fieldSheet.getRow(i);
                    if(nextRow==null){
                        break;
                    }
                    
                    Double fieldCode = nextRow.getCell(0).getNumericCellValue();
                    int fielCode = fieldCode.intValue();
                    String screenCode =  nextRow.getCell(1).getStringCellValue();
                    String fieldName =  nextRow.getCell(2).getStringCellValue();
                    String description = nextRow.getCell(3).getStringCellValue();
                    String fieldShort =  nextRow.getCell(4).getStringCellValue();
                    String helpMessage =  nextRow.getCell(5).getStringCellValue();
                    
                    Field field = new Field();
                    field.setFieldCode(String.valueOf(fielCode));
                    field.setFieldName(fieldName);
                    field.setDescription(description);
                    field.setScreen(repositoryScreen.findByScreenCodeAndIsDeletedAndLanguageLanguageId(screenCode, false, language.getLanguageId()));
                    field.setFieldShort(fieldShort);
                    field.setLanguage(language);
                    field.setHelpMessage(helpMessage);
                    repositoryFields.save(field);
	            }
	            
	            Sheet validationMessageSheet = workbook.getSheetAt(3);
	            for (int i = 1; i <= validationMessageSheet.getLastRowNum(); i++) 
	            {
	            	Row nextRow = validationMessageSheet.getRow(i);
	            	String messageShort =  nextRow.getCell(0).getStringCellValue();
                    String message =  nextRow.getCell(1).getStringCellValue();
	            	ValidationMessage validationMessage = new ValidationMessage();
	            	validationMessage.setMessage(message);
	            	validationMessage.setMessageShort(messageShort);
	            	validationMessage.setLanguage(language);
	            	repositoryValidationMessages.save(validationMessage);
	            }
	            
	            
	            Sheet fieldValidationSheet = workbook.getSheetAt(4);
	            for (int i = 1; i <= fieldValidationSheet.getLastRowNum(); i++) 
	            {
	            	Row nextRow = fieldValidationSheet.getRow(i);
                    if(nextRow==null){
                        break;
                    }
                    
                    Double fieldCode = nextRow.getCell(0).getNumericCellValue();
                    int fielCode = fieldCode.intValue();
                    
                    Double validationMessageId = nextRow.getCell(1).getNumericCellValue();
                    int valMessageId = validationMessageId.intValue();
                    
                    FieldValidation fieldValidation = new FieldValidation();
                    fieldValidation.setField(repositoryFields.findByFieldCodeAndLanguageLanguageId(String.valueOf(fielCode), language.getLanguageId()));
                    ValidationMessage validationMessage = repositoryValidationMessages.findOne(valMessageId);
                    if(validationMessage!=null)
                    {
                	   String messageShort = validationMessage.getMessageShort();
                	   fieldValidation.setValidationMessage(repositoryValidationMessages.findByMessageShortAndLanguageLanguageId(messageShort, language.getLanguageId()));
                	   repositoryFieldValidation.save(fieldValidation);
                    }
	            }
	        } 
		    catch (IOException e) {
		    	LOGGER.info(Arrays.toString(e.getStackTrace()));
	        } 
	}

	public DtoLanguage blockUnblockLanguage(DtoLanguage dtoLanguage) {
		Language language = this.repositoryLanguage.findByLanguageIdAndIsDeleted(dtoLanguage.getLanguageId(), false);
		if (language != null) {
			language.setIsActive(dtoLanguage.getIsActive());
			this.repositoryLanguage.saveAndFlush(language);
		}
		return dtoLanguage;
	}

	public List<DtoLanguage> getLanguageListForDropDown() {
		List<DtoLanguage> languagesList=new ArrayList<>();
		List<Language> list =null;
		list= repositoryLanguage.findByIsDeletedAndIsActive(false,true);
		if (list != null && !list.isEmpty()) 
		{
			for (Language language : list) 
			{
				DtoLanguage dtoLanguage = new DtoLanguage(language);
				
				if (language.getIsActive()) {
					dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted("ACTIVE", false).getMessage());
				} else {
					dtoLanguage.setLanguageStatus(serviceResponse.getMessageByShortAndIsDeleted("INACTIVE", false).getMessage());
				}
				
				languagesList.add(dtoLanguage);
			}
		}
		
		return languagesList;
	}
	
	/**
	 * Description: get company list by user Id
	 * @param userId
	 * @return
	 */
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<DtoCompany> getCompaniesListByUserId(int userId) {
		List<DtoCompany> dtoCompanyList = new ArrayList<>();
		Map<Integer,String> map = new HashMap<>();
		List<UserCompanyRelation> userCompanyRelations = this.repositoryUserCompanyRelation.findByUserUserIdAndIsDeleted(userId, false);
		if (userCompanyRelations != null && !userCompanyRelations.isEmpty()) 
		{
			for (UserCompanyRelation userCompanyRelation : userCompanyRelations) 
			{
				Company company = userCompanyRelation.getCompany();
				 
					if(company!=null && map.get(company.getCompanyId())==null)
					{
						map.put(company.getCompanyId(), company.getName());
						DtoCompany dtoCompany = new DtoCompany();
						dtoCompany.setCompanyId(company.getCompanyId());
						dtoCompany.setName(company.getName());
						dtoCompany.setCompanyCode(company.getCompanyCode());
						
						if (UtilRandomKey.isNotBlank(company.getTenantId())) {
							dtoCompany.setTenantId(company.getTenantId());
						} else {
							dtoCompany.setTenantId("");
						}
						
						dtoCompanyList.add(dtoCompany);
					}
				 
			}
		}
		return dtoCompanyList;
	}


	public Map<String, String> getCommonConstantList() 
	{
		
		Map<String, String> map=new HashMap<>();
		String langId = httpServletRequest.getHeader(Constant.LANG_ID);
		List<CommonConstant> list = repositoryCommonConstant.findByIsDeletedAndLanguageLanguageId(false,Integer.parseInt(langId));
		if (list != null && !list.isEmpty()) 
		{
			for (CommonConstant constant : list) 
			{
				map.put(constant.getConstantShort(),constant.getConstantValue());
			}
		}
		return map;
	}

	/**
	 * @Description: export master data in one excel file with multiple sheets as per number of tables.  
	 * @param request
	 * @param response
	 */
	public void exportMasterData(HttpServletRequest request, HttpServletResponse response) {

        final ServletContext servletContext = request.getSession().getServletContext();
        MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        String temperotyFilePath = tempDirectory.getAbsolutePath();
		XSSFWorkbook workbook = new XSSFWorkbook();
       
        Sheet countryMasterSheet = workbook.createSheet("CountryMaster");
        Sheet stateMasterSheet = workbook.createSheet("StateMaster");
        Sheet cityMasterSheet = workbook.createSheet("CityMaster");        
        Sheet btiMessageMasterSheet = workbook.createSheet("BtiMessageMaster");
        Sheet fieldMasterSheet = workbook.createSheet("FieldMaster");
        Sheet moduleMasterSheet = workbook.createSheet("ModuleMaster");        
        Sheet screensMasterSheet = workbook.createSheet("ScreensMaster");
        Sheet validationMessageMasterSheet = workbook.createSheet("ValidationMessageMaster");
        Sheet weekDaysMasterSheet = workbook.createSheet("WeekDaysMaster");          
        Sheet commonConstantSheet = workbook.createSheet("CommonConstant");          
        
        Map<Integer, Object[]> btiMessageData = new TreeMap<>();        
        Map<Integer, Object[]> countryData = new TreeMap<>();
        Map<Integer, Object[]> stateData = new TreeMap<>();
        Map<Integer, Object[]> cityData = new TreeMap<>();
        Map<Integer, Object[]> fieldData = new TreeMap<>();
        Map<Integer, Object[]> moduleData = new TreeMap<>();      
        Map<Integer, Object[]> screensData = new TreeMap<>();
        Map<Integer, Object[]> validationMessageData = new TreeMap<>();
        Map<Integer, Object[]> weekDaysData = new TreeMap<>();
        Map<Integer, Object[]> commonConstantData = new TreeMap<>();
        
		for (int i = 0; i < 10; i++) {
			
			countryMasterSheet.setColumnWidth(i, 10000);
			stateMasterSheet.setColumnWidth(i, 10000);
			cityMasterSheet.setColumnWidth(i, 10000);
			btiMessageMasterSheet.setColumnWidth(i, 10000);
			fieldMasterSheet.setColumnWidth(i, 10000);
			moduleMasterSheet.setColumnWidth(i, 10000);
			screensMasterSheet.setColumnWidth(i, 10000);
			validationMessageMasterSheet.setColumnWidth(i, 10000);
			weekDaysMasterSheet.setColumnWidth(i, 10000);
			commonConstantSheet.setColumnWidth(i, 10000); 
		}
		
			btiMessageData.put(1, new Object[] { "Message Short Name","Message","New Language Message"});
        	countryData.put(1, new Object[] { "Country Code","Short Name","Country Name","New Language Country Name"});
        	stateData.put(1, new Object[] { "Country Id","State Code","State Name","New Language State Name"});
        	cityData.put(1, new Object[] { "State Id","City Code","City Name","New Language City Name"});
        	weekDaysData.put(1, new Object[] { "Day Code","Day Name","New Language Day Name"});
        	validationMessageData.put(1, new Object[] {  "Message Short","Message","New Language Message"});
        	screensData.put(1, new Object[] { "Module Id","Screen Id","Side Menu Url","Help Message","New Language Help Message","Screen Name","New Language Screen Name","Side Menu","New Language Side Menu"});
        	moduleData.put(1, new Object[] { "Module Code","Description","New Language Description","Name","New Language Name","Help Message","New Language Help Message"});
        	fieldData.put(1, new Object[] { "Screen ID","Field Code","Field Short","Description","New Language Description","Field Name","New Language Field Name","Help Message","New Language Help Message"});
        	commonConstantData.put(1, new Object[] { "Constant Short","Constant Value","New Language Constant Value"});
         
            List<CountryMaster> countryList = repositoryCountryMaster.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<StateMaster> stateList = repositoryStateMaster.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<CityMaster> cityList = repositoryCityMaster.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<BtiMessage> btiMessageList = repositoryBtiMessage.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<Field> fieldList = repositoryFields.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<WeekDay> weekDayList = repositoryWeekDay.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<Module> moduleList = repositoryModule.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<Screen> screenList = repositoryScreen.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<ValidationMessage> validationMessageList = repositoryValidationMessages.findByIsDeletedAndLanguageLanguageId(false,1);
        	List<CommonConstant> commonConstantList = repositoryCommonConstant.findByIsDeletedAndLanguageLanguageId(false,1);
        	
        	if (commonConstantList!= null && !commonConstantList.isEmpty() ){
              	  int sno = 2;
                  for (CommonConstant commonConstant : commonConstantList) {
                  String constantShort = commonConstant.getConstantShort();
                  String value = commonConstant.getConstantValue();
                           sno++;
                           commonConstantData.put(sno, new Object[] { constantShort,value,""});
                  }
            }
              
    	  if (btiMessageList!= null && !btiMessageList.isEmpty() )
    	  {
         	 int sno = 2;
             for (BtiMessage btiMessage : btiMessageList) {
             String name = btiMessage.getMessageShort();
             String message = btiMessage.getMessage();
                      sno++;
                      btiMessageData.put(sno, new Object[] { name,message,""});
                }
          }
         
		if (countryList != null && !countryList.isEmpty()) {
			int sno = 2;
			for (CountryMaster country : countryList) {
				String countryName = country.getCountryName();
				String countryCode = country.getCountryCode();
				String shortName = country.getShortName();
				sno++;
				countryData.put(sno, new Object[] { countryCode, shortName, countryName, "" });
			}
		}
		
		if (stateList != null && !stateList.isEmpty()) {
			int sno = 2;
			for (StateMaster state : stateList) {
				String stateName = state.getStateName();
				String stateCode = state.getStateCode();
				int contryId = state.getCountryMaster().getCountryId();

				sno++;
				stateData.put(sno, new Object[] { contryId, stateCode, stateName, "" });
			}
		}
		
		if (cityList != null && !cityList.isEmpty()) {
			int sno = 2;
			for (CityMaster city : cityList) {
				String cityName = city.getCityName();
				String cityCode = city.getCityCode();
				int stateId = city.getStateMaster().getStateId();

				sno++;
				cityData.put(sno, new Object[] { stateId, cityCode, cityName, "" });
			}
		}
         
		if (fieldList != null && !fieldList.isEmpty()) {
			int sno = 2;
			for (Field field : fieldList) {
				int screenId = field.getScreen().getScreenId();
				String fieldCode = field.getFieldCode();
				String fieldShort = field.getFieldShort();
				String description = field.getDescription();
				String fieldName = field.getFieldName();
				String helpMessage = field.getHelpMessage();
				sno++;
				fieldData.put(sno, new Object[] { screenId, fieldCode,fieldShort, description,"", fieldName,"",helpMessage,""});
			}
		}
        	
		if (weekDayList != null && !weekDayList.isEmpty()) {
			int sno = 2;
			for (WeekDay weekDay : weekDayList) {
				 
				String dayCode = weekDay.getDayCode();
				String dayName = weekDay.getDayName();
				
				sno++;
				weekDaysData.put(sno, new Object[] { dayCode, dayName, "" });
			}
		}
        	 
		if (moduleList != null && !moduleList.isEmpty()) {
			int sno = 2;
			for (Module module : moduleList) {
				String moduleCode = module.getModuleCode();
				
				String description = module.getDescription();
				String name = module.getName();
				String helpMessage = module.getHelpMessage();
				sno++;
				moduleData.put(sno, new Object[] { moduleCode, description, "",name, "",helpMessage, "" });
			}
		}
        	 
		if (screenList != null && !screenList.isEmpty()) {
			int sno = 2;
			for (Screen screen : screenList) {
				int moduleId = screen.getModule().getModuleId();
				String screenCode = screen.getScreenCode();
				String sideMenuUrl = screen.getSideMenuURL();
				String helpMessage = screen.getHelpMessage();
				String screenName = screen.getScreenName();
				String sideMenu = screen.getSideMenu();
				sno++;
				screensData.put(sno, new Object[] { moduleId, screenCode,sideMenuUrl, helpMessage,"", screenName,"",sideMenu,""});
			}
		}
        	 
		if (validationMessageList != null && !validationMessageList.isEmpty()) {
			int sno = 2;
			for (ValidationMessage validationMessage : validationMessageList) {
				   String name = validationMessage.getMessageShort();
		             String message = validationMessage.getMessage();
		                      sno++;
		                      validationMessageData.put(sno, new Object[] { name,message,""});
			}
		}
        		 
		  
        	
            // Iterate over data and write to sheet
		Set<Integer> btiMessageKeyset = btiMessageData.keySet();
		Set<Integer> countryKeyset = countryData.keySet();
		Set<Integer> stateKeyset = stateData.keySet();
		Set<Integer> cityKeyset = cityData.keySet();
		Set<Integer> fieldKeyset = fieldData.keySet();
		Set<Integer> validationMessageKeyset = validationMessageData.keySet();
		Set<Integer> screensKeyset = screensData.keySet();
		Set<Integer> moduleKeyset = moduleData.keySet();
		Set<Integer> weekDaysKeyset = weekDaysData.keySet();
		Set<Integer> commonConstantKeyset = commonConstantData.keySet();
		 

         int rownum = 0;
         for (Integer key : commonConstantKeyset) {
                Row row = commonConstantSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = commonConstantData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
       	  rownum = 0;
		for (Integer key : fieldKeyset) {
			Row row = fieldMasterSheet.createRow(rownum++);
			CellStyle style;
			if (rownum <= 1) {
				style = workbook.createCellStyle();// Create style
				XSSFFont font = workbook.createFont();// Create font
				font.setFontName("sans-serif");// set font type
				font.setBold(true);
				style.setFont(font);// set it to bold
				style.setAlignment(CellStyle.ALIGN_CENTER);
			} else {
				style = workbook.createCellStyle();
				XSSFFont font = workbook.createFont();
				font.setFontName("sans-serif");
				style.setFont(font);
			}
			Object[] objArr = fieldData.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellStyle(style);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
       	rownum = 0;
        for (Integer key : validationMessageKeyset) {
               Row row = validationMessageMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = validationMessageData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	rownum = 0;
        for (Integer key : screensKeyset) {
               Row row = screensMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = screensData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
       	rownum = 0;
        for (Integer key : moduleKeyset) {
               Row row = moduleMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = moduleData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
     	rownum = 0;
        for (Integer key : weekDaysKeyset) {
               Row row = weekDaysMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = weekDaysData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	  
            rownum = 0;
            for (Integer key : btiMessageKeyset) {
                   Row row = btiMessageMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = btiMessageData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
             rownum = 0;
            for (Integer key : countryKeyset) {
                   Row row = countryMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = countryData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : stateKeyset) {
                   Row row = stateMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = stateData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : cityKeyset) {
                   Row row = cityMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = cityData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            try {
                   // Write the workbook in file system
                   String excelFileName = "MasterData.xlsx";
                   String fileName = URLEncoder.encode(excelFileName, "UTF-8");
                   fileName = URLDecoder.decode(fileName, "ISO8859_1");
                   response.setHeader("Content-disposition", "attachment; filename=" + fileName);
                   response.setContentType(mimetypesFileTypeMap.getContentType(excelFileName));

                   FileOutputStream out = new FileOutputStream(new File(temperotyFilePath + "\\" + excelFileName));
                   workbook.write(out);
                   out.close();

                   ByteArrayOutputStream baos = new ByteArrayOutputStream();
                   baos = convertexcelToByteArrayOutputStream(temperotyFilePath + "\\" + excelFileName);
                   OutputStream os = response.getOutputStream();
                   baos.writeTo(os);
                   os.flush();

            } catch (Exception e) {
            	LOGGER.info(Arrays.toString(e.getStackTrace()));
            }
	}
	
	/**
	 * @Description: convert file to output stream
	 * @param fileName
	 * @return
	 */
	private ByteArrayOutputStream convertexcelToByteArrayOutputStream(String fileName) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (InputStream inputStream = new FileInputStream(fileName)){
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();

			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}

		} catch (Exception e) {
			LOGGER.info(Arrays.toString(e.getStackTrace()));
		}   
		return baos;
	}
	
	 
	/**
	 * @Description: import master data from excel file.  
	 * @param languageName
	 * @param languageOrientation
	 * @param file
	 * @return
	 */
	public boolean importMasterData(String languageName, String languageOrientation, MultipartFile file) {

		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader("userid"))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		}

		Language language = repositoryLanguage.findByLanguageNameAndIsDeleted(languageName, false);
		if (language != null) {
			return false;
		}
		
		language = new Language();
		language.setIsActive(true);
		language.setIsDeleted(false);
		language.setLanguageName(languageName);
		language.setCreatedBy(loggedInUserId);
		language.setUpdatedBy(loggedInUserId);
		language.setLanguageOrientation(languageOrientation);
		language = repositoryLanguage.saveAndFlush(language);
		Workbook workbook;

		try {
			InputStream inputStream = file.getInputStream();
			workbook = new XSSFWorkbook(inputStream);

			  
				Sheet commonConstantSheet = workbook.getSheet("CommonConstant");
				Sheet btiMessageMasterSheet = workbook.getSheet("BtiMessageMaster");
				Sheet cityMasterSheet = workbook.getSheet("CityMaster");
				Sheet stateMasterSheet = workbook.getSheet("StateMaster");
				Sheet countryMasterSheet = workbook.getSheet("CountryMaster");
				Sheet fieldMaster = workbook.getSheet("FieldMaster");
				Sheet moduleMaster = workbook.getSheet("ModuleMaster");
				Sheet screensMaster = workbook.getSheet("ScreensMaster");
				Sheet validationMessageMaster = workbook.getSheet("ValidationMessageMaster");
				Sheet weekDaysMaster = workbook.getSheet("WeekDaysMaster");

				long commonConstantTotalRecords=commonConstantSheet.getLastRowNum()-1;
				long btiMessageTotal =btiMessageMasterSheet.getLastRowNum()-1;
				long cityMasterSheetTotal=cityMasterSheet.getLastRowNum()-1;
				long stateMasterSheetTotal=stateMasterSheet.getLastRowNum()-1;
				long countryMasterSheetTotal=countryMasterSheet.getLastRowNum()-1;
				long fieldMasterTotal=fieldMaster.getLastRowNum()-1;
				long moduleMasterTotal=moduleMaster.getLastRowNum()-1;
				long screensMasterTotal=screensMaster.getLastRowNum()-1;
				long validationMessageMasterTotal=validationMessageMaster.getLastRowNum()-1;
				long weekDaysMasterTotal=weekDaysMaster.getLastRowNum()-1;
				
				Long countCommonConstant= repositoryCommonConstant.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
				if(countCommonConstant>commonConstantTotalRecords){
					return false;
				}
				Long countBtiMessage= repositoryBtiMessage.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
				if(countBtiMessage>btiMessageTotal){
					return false;
				}
				Long countCity= repositoryCityMaster.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
			    if(countCity>cityMasterSheetTotal){
			    	return false;
			    }
			    Long countState= repositoryStateMaster.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
		        if(countState>stateMasterSheetTotal){
		        	return false;
		        }
		        Long countCountry= repositoryCountryMaster.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
	            if(countCountry>countryMasterSheetTotal){
	            	return false;
	            }
	            Long countField = repositoryFields.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
				if(countField>fieldMasterTotal){
					return false;
				}
				
				Long countModule = repositoryModule.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
				if(countModule>moduleMasterTotal){
					return false;
				}
				
				Long countScreen = repositoryScreen.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
				if(countScreen>screensMasterTotal){
					return false;
				}
				
				Long countValidation = repositoryScreen.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
				if(countValidation>validationMessageMasterTotal){
					return false;
				}
				
				Long countWeekDay = repositoryWeekDay.countByIsDeletedAndLanguageLanguageId(false, ENG_LANG_ID);
				if(countWeekDay>weekDaysMasterTotal){
					return false;
				}
				
				List<CommonConstant> commonConstantList = new ArrayList<>();
				for (int i = 1; i <= commonConstantSheet.getLastRowNum(); i++) {
					Row nextRow = commonConstantSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CommonConstant commonConstant = new CommonConstant();
					commonConstant.setConstantShort(nextRow.getCell(0).getStringCellValue());
					commonConstant.setConstantValue(nextRow.getCell(2).getStringCellValue());
					 

					commonConstant.setIsDeleted(false);
					commonConstant.setLanguage(language);
					commonConstant.setUpdatedBy(loggedInUserId);
					commonConstant.setCreatedBy(loggedInUserId);
					commonConstantList.add(commonConstant);
				}
				repositoryCommonConstant.save(commonConstantList);
				List<BtiMessage> btiMessageList = new ArrayList<>();
				for (int i = 1; i <= btiMessageMasterSheet.getLastRowNum(); i++) {
					Row nextRow = btiMessageMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					BtiMessage btiMessage = new BtiMessage();

					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						btiMessage.setMessageShort(nextRow.getCell(0).getStringCellValue());
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						btiMessage.setMessageShort(String.valueOf((int) nextRow.getCell(0).getNumericCellValue()));

					}
					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						btiMessage.setMessage(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						btiMessage.setMessage(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}

					btiMessage.setIsDeleted(false);
					btiMessage.setLanguage(language);
					btiMessage.setUpdatedBy(loggedInUserId);
					btiMessage.setCreatedBy(loggedInUserId);
					btiMessageList.add(btiMessage);
					
				}
				repositoryBtiMessage.save(btiMessageList);
				List<CityMaster> cityMasterList = new ArrayList<>();
				for (int i = 1; i <= cityMasterSheet.getLastRowNum(); i++) {
					Row nextRow = cityMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CityMaster cityMaster = new CityMaster();
					cityMaster.setStateMaster(repositoryStateMaster
							.findByStateIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false));

					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						cityMaster.setCityCode(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityMaster.setCityCode(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}
					if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_STRING) {
						cityMaster.setCityName(nextRow.getCell(3).getStringCellValue());
					} else if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityMaster.setCityName(String.valueOf((int) nextRow.getCell(3).getNumericCellValue()));
					}

					cityMaster.setIsDeleted(false);
					cityMaster.setLanguage(language);
					cityMaster.setUpdatedBy(loggedInUserId);
					cityMaster.setCreatedBy(loggedInUserId);
					cityMasterList.add(cityMaster);
				}
				repositoryCityMaster.save(cityMasterList);
				List<StateMaster> stateMasterList = new ArrayList<>();
				for (int i = 1; i <= stateMasterSheet.getLastRowNum(); i++) {
					Row nextRow = stateMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					StateMaster stateMaster = new StateMaster();
					CountryMaster countryMaster = repositoryCountryMaster
							.findByCountryIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false);

					stateMaster.setCountryMaster(countryMaster);

					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						stateMaster.setStateCode(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateMaster.setStateCode(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}
					if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_STRING) {
						stateMaster.setStateName(nextRow.getCell(3).getStringCellValue());
					} else if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateMaster.setStateName(String.valueOf((int) nextRow.getCell(3).getNumericCellValue()));
					}

					stateMaster.setIsDeleted(false);
					stateMaster.setLanguage(language);
					stateMaster.setUpdatedBy(loggedInUserId);
					stateMaster.setCreatedBy(loggedInUserId);
					stateMasterList.add(stateMaster);
				
				}
				repositoryStateMaster.save(stateMasterList);
				List<CountryMaster> countryMasterList = new ArrayList<>();
				
				for (int i = 1; i <= countryMasterSheet.getLastRowNum(); i++) {
					Row nextRow = countryMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					CountryMaster countryMaster = new CountryMaster();
					countryMaster.setActive(true);

					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryCode(nextRow.getCell(0).getStringCellValue());
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryCode(String.valueOf((int) nextRow.getCell(0).getNumericCellValue()));
					}

					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setShortName(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setShortName(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}
					if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryName(nextRow.getCell(3).getStringCellValue());
					} else if (nextRow.getCell(3).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryName(String.valueOf((int) nextRow.getCell(3).getNumericCellValue()));
					}

					countryMaster.setIsDeleted(false);
					countryMaster.setLanguage(language);
					countryMaster.setUpdatedBy(loggedInUserId);
					countryMaster.setCreatedBy(loggedInUserId);
					countryMasterList.add(countryMaster);
				}
				repositoryCountryMaster.save(countryMasterList);
				List<Field> fieldList = new ArrayList<>();

				for (int i = 1; i <= fieldMaster.getLastRowNum(); i++) {
					Row nextRow = fieldMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					Field field = new Field();
					field.setScreen(repositoryScreen.findByScreenIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(),false));
					field.setFieldCode(nextRow.getCell(1).getStringCellValue());
					field.setFieldShort(nextRow.getCell(2).getStringCellValue());
					field.setDescription(nextRow.getCell(4).getStringCellValue());
					field.setFieldName(nextRow.getCell(6).getStringCellValue());
					field.setHelpMessage(nextRow.getCell(8).getStringCellValue());
					field.setLanguage(language);
					fieldList.add(field);
				}
				
				repositoryFields.save(fieldList);
				List<Module> moduleList = new ArrayList<>();
				for (int i = 1; i <= moduleMaster.getLastRowNum(); i++) {
					Row nextRow = moduleMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					Module module = new Module();
					module.setModuleCode(nextRow.getCell(0).getStringCellValue());
					module.setDescription(nextRow.getCell(2).getStringCellValue());
					module.setName(nextRow.getCell(4).getStringCellValue());
					module.setHelpMessage(nextRow.getCell(6).getStringCellValue());
					module.setLanguage(language);
					moduleList.add(module);
				}
				
				repositoryModule.save(moduleList);
				List<Screen> screenList = new ArrayList<>();

				for (int i = 1; i <= screensMaster.getLastRowNum(); i++) {
					Row nextRow = screensMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					Screen screen = new Screen();
					screen.setModule(repositoryModule.findByModuleIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(),false));
					screen.setScreenCode(nextRow.getCell(1).getStringCellValue());
					screen.setSideMenuURL(nextRow.getCell(2).getStringCellValue());
					screen.setHelpMessage(nextRow.getCell(4).getStringCellValue());
					screen.setScreenName(nextRow.getCell(6).getStringCellValue());
					screen.setSideMenu(nextRow.getCell(8).getStringCellValue());
					screen.setLanguage(language);
					screenList.add(screen);
				}
				
				repositoryScreen.save(screenList);
				List<ValidationMessage> validationMessageList = new ArrayList<>();

				
				for (int i = 1; i <= validationMessageMaster.getLastRowNum(); i++) {
					Row nextRow = validationMessageMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					ValidationMessage validationMessage = new ValidationMessage();
					validationMessage.setMessageShort(nextRow.getCell(0).getStringCellValue());
					validationMessage.setMessage(nextRow.getCell(2).getStringCellValue());
					validationMessage.setLanguage(language);
					validationMessageList.add(validationMessage);
				}
			 
				repositoryValidationMessages.save(validationMessageList);
				List<WeekDay> weekDayList = new ArrayList<>();
				for (int i = 1; i <= weekDaysMaster.getLastRowNum(); i++) {
					Row nextRow = weekDaysMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					WeekDay weekDay = new WeekDay();
					weekDay.setDayCode(nextRow.getCell(0).getStringCellValue());
					weekDay.setDayName(nextRow.getCell(2).getStringCellValue());
					weekDay.setLanguage(language);
					weekDayList.add(weekDay);
				}
				repositoryWeekDay.save(weekDayList);
			    inputStream.close();
		} catch (Exception e) {
			LOGGER.info(e.getStackTrace());
			language.setIsDeleted(true);
			language.setUpdatedBy(loggedInUserId);
			repositoryLanguage.saveAndFlush(language);
			return false;
		}
		return true;
	}

	public String getCompanyTenant(Integer companyId) {
		Company company = repositoryCompany.findByCompanyIdAndIsDeleted(companyId, false);
		if(company!=null){
			return company.getTenantId();
		}
		return null;
	}
	
	public int getCompanyIdFromTenant(String companyTenant) {
		Company company = repositoryCompany.findByTenantIdAndIsDeleted(companyTenant, false);
		if(company!=null){
			return company.getCompanyId();
		}
		return 0;
	}
	
	public String getCompanyName(Integer companyId) {
		Company company = repositoryCompany.findByCompanyIdAndIsDeleted(companyId, false);
		if(company!=null){
			return company.getName();
		}
		return null;
	}

	public Map<String, String> importCompanyMasterData(String languageName, String languageOrientation, MultipartFile file,
			String[] dbNames) {
		Map<String, String> map = new HashMap<>();
		String langId = httpServletRequest.getHeader(Constant.LANG_ID);
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader("userid"))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		}
		String databaseNames="";
		for (String dbName : dbNames) {
			try {
				if(UtilRandomKey.isNotBlank(databaseNames)){
					databaseNames+=","+dbName;
				}
				else{
					databaseNames+=dbName;
				}
				Response response = RestAssured.given().contentType("multipart/form-data").header(Constant.LANG_ID, langId)
						.header(Constant.TENANT_ID, dbName.trim())
						.multiPart("file", file.getOriginalFilename(), file.getBytes(), file.getContentType())
						.formParam("languageName", languageName).formParam("languageOrientation", languageOrientation)
						.when().post(accessfinancialpath+"/importCompanyMasterData").andReturn();
				JsonPath jsonpath = response.getBody().jsonPath();
				String code = jsonpath.getString("code");
				if (code.equalsIgnoreCase("200")) {
					map.put(dbName, "Success");
				} else {
					map.put(dbName, "Failed");
				}
			} catch (Exception e) {
				map.put(dbName, "Failed");
			}
		}
		
		Language language=repositoryLanguage.findByLanguageNameAndIsDeleted(languageName, false);
		if(language!=null)
		{
			language.setIsActive(true);
			language.setIsDeleted(false);
			language.setLanguageName(languageName);
			language.setCreatedBy(loggedInUserId);
			language.setUpdatedBy(loggedInUserId);
			language.setLanguageOrientation(languageOrientation);
			language.setDbNames(databaseNames);
			repositoryLanguage.saveAndFlush(language);
		}
		
		return map;
	}
	
	public boolean updateActiveSession(DtoUser dtoUser){
		UserSession userSession = repositoryUserSession.findByUserUserIdAndSessionAndIsDeleted(dtoUser.getUserId(), dtoUser.getSession(), false);
		if(userSession!=null){
			userSession.setUpdatedDate(new Date());
			repositoryUserSession.saveAndFlush(userSession);
			return true;
		}
		return false;
	}
	
	@Scheduled(fixedDelay = 60000*3) // executed at every 5 minutes
	public void checkExpireTimingForUserSession()
	{
		List<UserSession> userSessionsList= repositoryUserSession.findAll();
		if(userSessionsList!=null && !userSessionsList.isEmpty())
		{
			for (UserSession userSession : userSessionsList) 
			{
				long maxDuration = MILLISECONDS.convert(3, MINUTES);
                 Date now = new Date();
				long duration = now.getTime() - userSession.getUpdatedDate().getTime();
				if (duration >= maxDuration) 
				{
					
					User user = userSession.getUser();
					if(user.getRole().getRoleName().equalsIgnoreCase(BTIRoles.SUPERADMIN.name()))
					{
						List<UserDraft> userDraftList= repositoryUserDraft.findByUserUserId(user.getUserId());
						if(userDraftList!=null && !userDraftList.isEmpty()){
							repositoryUserDraft.deleteInBatch(userDraftList);
						}
					}
					else if(user.getRole().getRoleName().equalsIgnoreCase(BTIRoles.USER.name()))
					{
						String tenantId = userSession.getCompnayTenantId();
						Company company=repositoryCompany.findByTenantIdAndIsDeleted(tenantId, false);
						if(company!=null){
							List<UserDraft> userDraftList= repositoryUserDraft.findByUserUserIdAndCompanyCompanyId(user.getUserId(),company.getCompanyId());
							if(userDraftList!=null && !userDraftList.isEmpty()){
								repositoryUserDraft.deleteInBatch(userDraftList);
							}
						}
					}
					
					repositoryUserSession.delete(userSession);
				}
			}
		}
	}
	
	public boolean updateUserLanguage(int languageId,MultipartFile file,
			String languageName,String languageOrientation){

		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader("userid"))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		}

		Language language = repositoryLanguage.findByLanguageIdAndIsDeleted(languageId, false);
		if (language == null) {
			return false;
		}
		language.setIsActive(true);
		language.setIsDeleted(false);
		language.setLanguageName(languageName);
		language.setCreatedBy(loggedInUserId);
		language.setUpdatedBy(loggedInUserId);
		language.setLanguageOrientation(languageOrientation);
		language=repositoryLanguage.saveAndFlush(language);
		try {
			if(file==null || file.getBytes().length<=0){
				return true;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Workbook workbook;
		try {
			    InputStream inputStream = file.getInputStream();
			    workbook = new XSSFWorkbook(inputStream);
			    
				Sheet commonConstantSheet = workbook.getSheet("CommonConstant");
				Sheet btiMessageMasterSheet = workbook.getSheet("BtiMessageMaster");
				Sheet cityMasterSheet = workbook.getSheet("CityMaster");
				Sheet stateMasterSheet = workbook.getSheet("StateMaster");
				Sheet countryMasterSheet = workbook.getSheet("CountryMaster");
				Sheet fieldMaster = workbook.getSheet("FieldMaster");
				Sheet moduleMaster = workbook.getSheet("ModuleMaster");
				Sheet screensMaster = workbook.getSheet("ScreensMaster");
				Sheet validationMessageMaster = workbook.getSheet("ValidationMessageMaster");
				Sheet weekDaysMaster = workbook.getSheet("WeekDaysMaster");
				
				List<CommonConstant> commonConstantList = new ArrayList<>();
				for (int i = 1; i <= commonConstantSheet.getLastRowNum(); i++) {
					Row nextRow = commonConstantSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					String constantShort=nextRow.getCell(0).getStringCellValue();
					CommonConstant commonConstant = repositoryCommonConstant.findByConstantShortAndIsDeletedAndLanguageLanguageId
							(constantShort,false,language.getLanguageId());
					if(commonConstant==null){
						commonConstant= new CommonConstant();
					}
					commonConstant.setConstantShort(constantShort);
					commonConstant.setConstantValue(nextRow.getCell(1).getStringCellValue());
					commonConstant.setIsDeleted(false);
					commonConstant.setLanguage(language);
					commonConstant.setUpdatedBy(loggedInUserId);
					commonConstant.setCreatedBy(loggedInUserId);
					commonConstantList.add(commonConstant);
				}
				repositoryCommonConstant.save(commonConstantList);
				List<BtiMessage> btiMessageList = new ArrayList<>();
				for (int i = 1; i <= btiMessageMasterSheet.getLastRowNum(); i++) {
					Row nextRow = btiMessageMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					String messageShort="";
					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						messageShort=nextRow.getCell(0).getStringCellValue();
						
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						messageShort=String.valueOf((int) nextRow.getCell(0).getNumericCellValue());
					}
					
					BtiMessage btiMessage =repositoryBtiMessage.
							findByMessageShortAndIsDeletedAndLanguageLanguageId
							(messageShort,false,language.getLanguageId());
					if(btiMessage==null){
						btiMessage= new BtiMessage();
					}
					
                    btiMessage.setMessageShort(messageShort);
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						btiMessage.setMessage(nextRow.getCell(1).getStringCellValue());
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						btiMessage.setMessage(String.valueOf((int) nextRow.getCell(1).getNumericCellValue()));
					}

					btiMessage.setIsDeleted(false);
					btiMessage.setLanguage(language);
					btiMessage.setUpdatedBy(loggedInUserId);
					btiMessage.setCreatedBy(loggedInUserId);
					btiMessageList.add(btiMessage);
				}
				repositoryBtiMessage.save(btiMessageList);
				
				List<CityMaster> cityMasterList = new ArrayList<>();
				for (int i = 1; i <= cityMasterSheet.getLastRowNum(); i++) {
					Row nextRow = cityMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					String cityCode="";
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						cityCode=nextRow.getCell(1).getStringCellValue();
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityCode=String.valueOf((int) nextRow.getCell(1).getNumericCellValue());
					}
					
					CityMaster cityMaster=repositoryCityMaster.findByCityCodeAndIsDeletedAndLanguageLanguageId(cityCode, false, language.getLanguageId());
					if(cityMaster==null){
						cityMaster= new CityMaster();
					}
					cityMaster.setStateMaster(repositoryStateMaster
							.findByStateIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false));
					cityMaster.setCityCode(cityCode);
					
					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						cityMaster.setCityName(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						cityMaster.setCityName(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}

					cityMaster.setIsDeleted(false);
					cityMaster.setLanguage(language);
					cityMaster.setUpdatedBy(loggedInUserId);
					cityMaster.setCreatedBy(loggedInUserId);
					cityMasterList.add(cityMaster);
				}
				repositoryCityMaster.save(cityMasterList);
				
				List<StateMaster> stateMasterList = new ArrayList<>();
				for (int i = 1; i <= stateMasterSheet.getLastRowNum(); i++) {
					Row nextRow = stateMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					String stateCode="";
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						stateCode=nextRow.getCell(1).getStringCellValue();
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateCode=String.valueOf((int) nextRow.getCell(1).getNumericCellValue());
					}
					
					StateMaster stateMaster=repositoryStateMaster.findByStateCodeAndIsDeletedAndLanguageLanguageId(stateCode, false,language.getLanguageId());
					if(stateMaster==null){
						stateMaster= new StateMaster();
					}
					
					CountryMaster countryMaster = repositoryCountryMaster
							.findByCountryIdAndIsDeleted((int) (nextRow.getCell(0).getNumericCellValue()), false);
					stateMaster.setCountryMaster(countryMaster);
					stateMaster.setStateCode(stateCode);
					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						stateMaster.setStateName(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						stateMaster.setStateName(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}

					stateMaster.setIsDeleted(false);
					stateMaster.setLanguage(language);
					stateMaster.setUpdatedBy(loggedInUserId);
					stateMaster.setCreatedBy(loggedInUserId);
					stateMasterList.add(stateMaster);
				
				}
				repositoryStateMaster.save(stateMasterList);
				List<CountryMaster> countryMasterList = new ArrayList<>();
				
				for (int i = 1; i <= countryMasterSheet.getLastRowNum(); i++) 
				{
					Row nextRow = countryMasterSheet.getRow(i);
					if (nextRow == null) {
						break;
					}
					
					String countryShortName="";
					if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_STRING) {
						countryShortName=nextRow.getCell(1).getStringCellValue();
					} else if (nextRow.getCell(1).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryShortName=String.valueOf((int) nextRow.getCell(1).getNumericCellValue());
					}
					
					CountryMaster countryMaster = repositoryCountryMaster.findByshortNameAndIsDeletedAndLanguageLanguageId(countryShortName,false,language.getLanguageId());
					if(countryMaster==null){
						countryMaster= new CountryMaster();
					}
					countryMaster.setActive(true);
					countryMaster.setShortName(countryShortName);
					
					if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryCode(nextRow.getCell(0).getStringCellValue());
					} else if (nextRow.getCell(0).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryCode(String.valueOf((int) nextRow.getCell(0).getNumericCellValue()));
					}

					if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_STRING) {
						countryMaster.setCountryName(nextRow.getCell(2).getStringCellValue());
					} else if (nextRow.getCell(2).getCellType() == Cell.CELL_TYPE_NUMERIC) {
						countryMaster.setCountryName(String.valueOf((int) nextRow.getCell(2).getNumericCellValue()));
					}

					countryMaster.setIsDeleted(false);
					countryMaster.setLanguage(language);
					countryMaster.setUpdatedBy(loggedInUserId);
					countryMaster.setCreatedBy(loggedInUserId);
					countryMasterList.add(countryMaster);
				}
				repositoryCountryMaster.save(countryMasterList);
				List<Field> fieldList = new ArrayList<>();

				for (int i = 1; i <= fieldMaster.getLastRowNum(); i++) {
					Row nextRow = fieldMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					String fieldCode=nextRow.getCell(1).getStringCellValue();
					Field field = repositoryFields.findByFieldCodeAndLanguageLanguageId(fieldCode, language.getLanguageId());
					if(field==null){
						field= new Field();
					}
					field.setScreen(repositoryScreen.findByScreenIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(),false));
					field.setFieldCode(fieldCode);
					field.setFieldShort(nextRow.getCell(2).getStringCellValue());
					field.setDescription(nextRow.getCell(3).getStringCellValue());
					field.setFieldName(nextRow.getCell(4).getStringCellValue());
					field.setHelpMessage(nextRow.getCell(5).getStringCellValue());
					field.setLanguage(language);
					fieldList.add(field);
				}
				
				repositoryFields.save(fieldList);
				List<Module> moduleList = new ArrayList<>();
				for (int i = 1; i <= moduleMaster.getLastRowNum(); i++) {
					Row nextRow = moduleMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					String moduleCode=nextRow.getCell(0).getStringCellValue();
					Module module = repositoryModule.findByModuleCodeAndIsDeletedAndLanguageLanguageId(moduleCode, false, language.getLanguageId());
					if(module==null){
						module= new Module();
					}
					module.setModuleCode(moduleCode);
					module.setDescription(nextRow.getCell(1).getStringCellValue());
					module.setName(nextRow.getCell(2).getStringCellValue());
					module.setHelpMessage(nextRow.getCell(3).getStringCellValue());
					module.setLanguage(language);
					moduleList.add(module);
				}
				
				repositoryModule.save(moduleList);
				List<Screen> screenList = new ArrayList<>();

				for (int i = 1; i <= screensMaster.getLastRowNum(); i++) {
					Row nextRow = screensMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					String screenCode=nextRow.getCell(1).getStringCellValue();
					Screen screen = repositoryScreen.findByScreenCodeAndIsDeletedAndLanguageLanguageId(screenCode, false, language.getLanguageId());
					if(screen==null){
						screen= new Screen();
					}
					screen.setModule(repositoryModule.findByModuleIdAndIsDeleted((int) nextRow.getCell(0).getNumericCellValue(),false));
					screen.setScreenCode(screenCode);
					screen.setSideMenuURL(nextRow.getCell(2).getStringCellValue());
					screen.setHelpMessage(nextRow.getCell(3).getStringCellValue());
					screen.setScreenName(nextRow.getCell(4).getStringCellValue());
					screen.setSideMenu(nextRow.getCell(5).getStringCellValue());
					screen.setLanguage(language);
					screenList.add(screen);
				}
				
				repositoryScreen.save(screenList);
				List<ValidationMessage> validationMessageList = new ArrayList<>();

				
				for (int i = 1; i <= validationMessageMaster.getLastRowNum(); i++) {
					Row nextRow = validationMessageMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					String validationMessageShort=nextRow.getCell(0).getStringCellValue();
					ValidationMessage validationMessage=repositoryValidationMessages.findByMessageShortAndLanguageLanguageId(validationMessageShort, language.getLanguageId());
					if(validationMessage==null){
						validationMessage= new ValidationMessage();
					}
					validationMessage.setMessageShort(validationMessageShort);
					validationMessage.setMessage(nextRow.getCell(1).getStringCellValue());
					validationMessage.setLanguage(language);
					validationMessageList.add(validationMessage);
				}
			 
				repositoryValidationMessages.save(validationMessageList);
				List<WeekDay> weekDayList = new ArrayList<>();
				for (int i = 1; i <= weekDaysMaster.getLastRowNum(); i++) {
					Row nextRow = weekDaysMaster.getRow(i);
					if (nextRow == null) {
						break;
					}
					String dayCode=nextRow.getCell(0).getStringCellValue();
					WeekDay weekDay = repositoryWeekDay.findByDayCodeAndIsDeletedAndLanguageLanguageId(dayCode,false,language.getLanguageId());
					if(weekDay==null){
						weekDay= new WeekDay();
					}
					weekDay.setDayCode(dayCode);
					weekDay.setDayName(nextRow.getCell(1).getStringCellValue());
					weekDay.setLanguage(language);
					weekDayList.add(weekDay);
				}
				repositoryWeekDay.save(weekDayList);
			    inputStream.close();
		} catch (Exception e) {
			LOGGER.info(e.getStackTrace());
			language.setIsDeleted(true);
			language.setUpdatedBy(loggedInUserId);
			repositoryLanguage.saveAndFlush(language);
			return false;
		}
		return true;
	
	}
	
	@Scheduled(fixedDelay = 60000*3) // executed at every 5 minutes
	public List<DtoModule> getAllModuleByLanguageAndIsActive() {
		String langId = httpServletRequest.getHeader("langid");
		List<DtoModule> dtoModuleList = new ArrayList<>();
		try {
			List<Module> moduleList = repositoryModule.findByIsDeletedAndIsActiveAndLanguageLanguageId(false,true,Integer.parseInt(langId));
			if (moduleList != null && moduleList.size() > 0) {
				for (Module module : moduleList) {
					if(module.getIsActive()) {
						DtoModule dtoModule = new DtoModule(module);
						dtoModule.setModuleId(module.getModuleId());
						dtoModule.setModuleCode(module.getModuleCode());
						dtoModule.setModuleName(module.getName());
						dtoModuleList.add(dtoModule);
					}
					
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return dtoModuleList;
	}
	
	public List<DtoModule> getAllModulesInDropDown() {
		String langId = httpServletRequest.getHeader(Constant.LANG_ID);
		List<DtoModule> dtoModuleList = new ArrayList<>();
		try {
			List<Module> moduleList = repositoryModule.findByIsDeletedAndLanguageLanguageId(false, Integer.parseInt(langId));
			if (moduleList != null && moduleList.size() > 0) {
				for (Module module : moduleList) {
					if(module.getIsActive()) {
						DtoModule dtoModule = new DtoModule(module);
						dtoModule.setModuleId(module.getModuleId());
						dtoModule.setModuleCode(module.getModuleCode());
						dtoModule.setModuleName(module.getName());
						dtoModuleList.add(dtoModule);
					}
					
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return dtoModuleList;
	}
	
	/**
	 * @Description: export master data in one excel file with multiple sheets as per number of tables.  
	 * @param request
	 * @param response
	 */
	public void exportMasterDataForUpdateLanguage(HttpServletRequest request, HttpServletResponse response,int languageId) {

        final ServletContext servletContext = request.getSession().getServletContext();
        MimetypesFileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        String temperotyFilePath = tempDirectory.getAbsolutePath();
		XSSFWorkbook workbook = new XSSFWorkbook();
       
        Sheet countryMasterSheet = workbook.createSheet("CountryMaster");
        Sheet stateMasterSheet = workbook.createSheet("StateMaster");
        Sheet cityMasterSheet = workbook.createSheet("CityMaster");        
        Sheet btiMessageMasterSheet = workbook.createSheet("BtiMessageMaster");
        Sheet fieldMasterSheet = workbook.createSheet("FieldMaster");
        Sheet moduleMasterSheet = workbook.createSheet("ModuleMaster");        
        Sheet screensMasterSheet = workbook.createSheet("ScreensMaster");
        Sheet validationMessageMasterSheet = workbook.createSheet("ValidationMessageMaster");
        Sheet weekDaysMasterSheet = workbook.createSheet("WeekDaysMaster");          
        Sheet commonConstantSheet = workbook.createSheet("CommonConstant");          
        
        Map<Integer, Object[]> btiMessageData = new TreeMap<>();        
        Map<Integer, Object[]> countryData = new TreeMap<>();
        Map<Integer, Object[]> stateData = new TreeMap<>();
        Map<Integer, Object[]> cityData = new TreeMap<>();
        Map<Integer, Object[]> fieldData = new TreeMap<>();
        Map<Integer, Object[]> moduleData = new TreeMap<>();      
        Map<Integer, Object[]> screensData = new TreeMap<>();
        Map<Integer, Object[]> validationMessageData = new TreeMap<>();
        Map<Integer, Object[]> weekDaysData = new TreeMap<>();
        Map<Integer, Object[]> commonConstantData = new TreeMap<>();
        
		for (int i = 0; i < 10; i++) {
			
			countryMasterSheet.setColumnWidth(i, 10000);
			stateMasterSheet.setColumnWidth(i, 10000);
			cityMasterSheet.setColumnWidth(i, 10000);
			btiMessageMasterSheet.setColumnWidth(i, 10000);
			fieldMasterSheet.setColumnWidth(i, 10000);
			moduleMasterSheet.setColumnWidth(i, 10000);
			screensMasterSheet.setColumnWidth(i, 10000);
			validationMessageMasterSheet.setColumnWidth(i, 10000);
			weekDaysMasterSheet.setColumnWidth(i, 10000);
			commonConstantSheet.setColumnWidth(i, 10000); 
			
		}
		
			btiMessageData.put(1, new Object[] { "Message Short Name","Message"});
        	countryData.put(1, new Object[] { "Country Code","Short Name","Country Name"});
        	stateData.put(1, new Object[] { "Country Id","State Code","State Name"});
        	cityData.put(1, new Object[] { "State Id","City Code","City Name"});
        	weekDaysData.put(1, new Object[] { "Day Code","Day Name"});
        	validationMessageData.put(1, new Object[] {  "Message Short","Message"});
        	screensData.put(1, new Object[] { "Module Id","Screen Id","Side Menu Url","Help Message","Screen Name","Side Menu"});
        	moduleData.put(1, new Object[] { "Module Code","Description","Name","Help Message"});
        	fieldData.put(1, new Object[] { "Screen ID","Field Code","Field Short","Description","Field Name","Help Message"});
        	commonConstantData.put(1, new Object[] { "Constant Short","Constant Value"});
         
            List<CountryMaster> countryList = repositoryCountryMaster.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<StateMaster> stateList = repositoryStateMaster.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<CityMaster> cityList = repositoryCityMaster.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<BtiMessage> btiMessageList = repositoryBtiMessage.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<Field> fieldList = repositoryFields.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<WeekDay> weekDayList = repositoryWeekDay.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<Module> moduleList = repositoryModule.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<Screen> screenList = repositoryScreen.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<ValidationMessage> validationMessageList = repositoryValidationMessages.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	List<CommonConstant> commonConstantList = repositoryCommonConstant.findByIsDeletedAndLanguageLanguageId(false,languageId);
        	
        	if (commonConstantList!= null && !commonConstantList.isEmpty() ){
              	  int sno = 2;
                  for (CommonConstant commonConstant : commonConstantList) {
                  String constantShort = commonConstant.getConstantShort();
                  String value = commonConstant.getConstantValue();
                  
                 /* commonConstant=repositoryCommonConstant.findByConstantShortAndIsDeletedAndLanguageLanguageId(constantShort, false, languageId);
                   String updateValue=commonConstant!=null?commonConstant.getConstantValue():"";*/
                           sno++;
                           commonConstantData.put(sno, new Object[] { constantShort,value});
                  }
            }
              
    	  if (btiMessageList!= null && !btiMessageList.isEmpty() )
    	  {
         	 int sno = 2;
             for (BtiMessage btiMessage : btiMessageList) {
                     String messageShort = btiMessage.getMessageShort();
                     String message = btiMessage.getMessage();
                    /* btiMessage=repositoryBtiMessage.findByMessageShortAndIsDeletedAndLanguageLanguageId(messageShort, false, languageId);
                     String updateLangMessage=btiMessage!=null?btiMessage.getMessage():"";*/
                     sno++;
                      btiMessageData.put(sno, new Object[] { messageShort,message});
                }
          }
         
		if (countryList != null && !countryList.isEmpty()) {
			int sno = 2;
			for (CountryMaster country : countryList) {
				String countryName = country.getCountryName();
				String countryCode = country.getCountryCode();
				String shortName = country.getShortName();
				/*country=repositoryCountryMaster.findByshortNameAndIsDeletedAndLanguageLanguageId(shortName, false, languageId);
				String updateCountryName=country!=null?country.getCountryName():"";*/
				sno++;
				countryData.put(sno, new Object[] { countryCode, shortName, countryName});
			}
		}
		
		if (stateList != null && !stateList.isEmpty()) {
			int sno = 2;
			for (StateMaster state : stateList) {
				String stateName = state.getStateName();
				String stateCode = state.getStateCode();
				int contryId = state.getCountryMaster().getCountryId();
				/*state=repositoryStateMaster.findByStateCodeAndIsDeletedAndLanguageLanguageId(stateCode, false, languageId);
				String updateStateName=state!=null?state.getStateName():"";*/
				sno++;
				stateData.put(sno, new Object[] { contryId, stateCode, stateName});
			}
		}
		
		if (cityList != null && !cityList.isEmpty()) {
			int sno = 2;
			for (CityMaster city : cityList) {
				String cityName = city.getCityName();
				String cityCode = city.getCityCode();
				int stateId = city.getStateMaster().getStateId();
				/*city=repositoryCityMaster.findByCityCodeAndIsDeletedAndLanguageLanguageId(cityCode, false, languageId);
				String updateCityName=city!=null?city.getCityName():"";*/
				sno++;
				cityData.put(sno, new Object[] { stateId, cityCode, cityName});
			}
		}
         
		if (fieldList != null && !fieldList.isEmpty()) {
			int sno = 2;
			for (Field field : fieldList) {
				int screenId = field.getScreen().getScreenId();
				String fieldCode = field.getFieldCode();
				String fieldShort = field.getFieldShort();
				String description = field.getDescription();
				String fieldName = field.getFieldName();
				String helpMessage = field.getHelpMessage();
				/*field=repositoryFields.findByFieldCodeAndLanguageLanguageId(fieldCode, languageId);
				String updateDescription="";
				String updateFieldName="";
				String updateHelpMessage="";
				if(field!=null){
					updateDescription=field.getDescription();
					updateFieldName=field.getFieldName();
					updateHelpMessage=field.getHelpMessage();
				}*/
				sno++;
				fieldData.put(sno, new Object[] {screenId,fieldCode,fieldShort, description,fieldName,helpMessage});
			}
		}
        	
		if (weekDayList != null && !weekDayList.isEmpty()) {
			int sno = 2;
			for (WeekDay weekDay : weekDayList) {
				 
				String dayCode = weekDay.getDayCode();
				String dayName = weekDay.getDayName();
				/*weekDay=repositoryWeekDay.findByDayCodeAndIsDeletedAndLanguageLanguageId(dayCode, false, languageId);
				String updateDayName=weekDay!=null?weekDay.getDayName():"";*/
				sno++;
				weekDaysData.put(sno, new Object[] { dayCode, dayName});
			}
		}
        	 
		if (moduleList != null && !moduleList.isEmpty()) {
			int sno = 2;
			for (Module module : moduleList) {
				String moduleCode = module.getModuleCode();
				String description = module.getDescription();
				String name = module.getName();
				String helpMessage = module.getHelpMessage();
				/*module=repositoryModule.findByModuleCodeAndIsDeletedAndLanguageLanguageId(moduleCode, false, languageId);
				String updateDescription="";
				String updateName="";
				String updateHelpMessage="";
				if(module!=null){
					updateDescription=module.getDescription();
					updateName=module.getName();
					updateHelpMessage=module.getHelpMessage();
				}*/
				
				sno++;
				moduleData.put(sno, new Object[] { moduleCode,description,name,helpMessage});
			}
		}
        	 
		if (screenList != null && !screenList.isEmpty()) {
			int sno = 2;
			for (Screen screen : screenList) {
				int moduleId = screen.getModule().getModuleId();
				String screenCode = screen.getScreenCode();
				String sideMenuUrl = screen.getSideMenuURL();
				String helpMessage = screen.getHelpMessage();
				String screenName = screen.getScreenName();
				String sideMenu = screen.getSideMenu();
				/*screen=repositoryScreen.findByScreenCodeAndIsDeletedAndLanguageLanguageId(screenCode, false, languageId);
				String updateHelpMessage="";
				String updateScreenName="";
				String updateSideMenu="";
				if(screen!=null){
					updateHelpMessage=screen.getHelpMessage();
					updateScreenName=screen.getScreenName();
					updateSideMenu=screen.getSideMenu();
				}*/
				sno++;
				screensData.put(sno, new Object[] {moduleId, screenCode,sideMenuUrl, helpMessage,screenName,sideMenu});
			}
		}
        	 
		if (validationMessageList != null && !validationMessageList.isEmpty()) 
		{
			int sno = 2;
			for (ValidationMessage validationMessage : validationMessageList) 
			{
				   String messageShort = validationMessage.getMessageShort();
		             String message = validationMessage.getMessage();
		             /*validationMessage= repositoryValidationMessages.findByMessageShortAndLanguageLanguageId(messageShort, languageId);
		             String updateMessage=validationMessage!=null?validationMessage.getMessage():"";*/
		             sno++;
		             validationMessageData.put(sno, new Object[] {messageShort,message});
			}
		}
            // Iterate over data and write to sheet
		Set<Integer> btiMessageKeyset = btiMessageData.keySet();
		Set<Integer> countryKeyset = countryData.keySet();
		Set<Integer> stateKeyset = stateData.keySet();
		Set<Integer> cityKeyset = cityData.keySet();
		Set<Integer> fieldKeyset = fieldData.keySet();
		Set<Integer> validationMessageKeyset = validationMessageData.keySet();
		Set<Integer> screensKeyset = screensData.keySet();
		Set<Integer> moduleKeyset = moduleData.keySet();
		Set<Integer> weekDaysKeyset = weekDaysData.keySet();
		Set<Integer> commonConstantKeyset = commonConstantData.keySet();
		 

         int rownum = 0;
         for (Integer key : commonConstantKeyset) {
                Row row = commonConstantSheet.createRow(rownum++);
                CellStyle style;
                if(rownum<=1){
                	style = workbook.createCellStyle();// Create style
         			XSSFFont font = workbook.createFont();// Create font
         			font.setFontName("sans-serif");// set font type
         			font.setBold(true);
         			style.setFont(font);// set it to bold
         			style.setAlignment(CellStyle.ALIGN_CENTER);
                } else{
                	style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontName("sans-serif");
                    style.setFont(font);
                }
                Object[] objArr = commonConstantData.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                      Cell cell = row.createCell(cellnum++);
                      cell.setCellStyle(style);
                      if (obj instanceof String)
                             cell.setCellValue((String) obj);
                      else if (obj instanceof Integer)
                             cell.setCellValue((Integer) obj);
                }
         }
       	  rownum = 0;
		for (Integer key : fieldKeyset) {
			Row row = fieldMasterSheet.createRow(rownum++);
			CellStyle style;
			if (rownum <= 1) {
				style = workbook.createCellStyle();// Create style
				XSSFFont font = workbook.createFont();// Create font
				font.setFontName("sans-serif");// set font type
				font.setBold(true);
				style.setFont(font);// set it to bold
				style.setAlignment(CellStyle.ALIGN_CENTER);
			} else {
				style = workbook.createCellStyle();
				XSSFFont font = workbook.createFont();
				font.setFontName("sans-serif");
				style.setFont(font);
			}
			Object[] objArr = fieldData.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				cell.setCellStyle(style);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
       	rownum = 0;
        for (Integer key : validationMessageKeyset) {
               Row row = validationMessageMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = validationMessageData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	rownum = 0;
        for (Integer key : screensKeyset) {
               Row row = screensMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = screensData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
       	rownum = 0;
        for (Integer key : moduleKeyset) {
               Row row = moduleMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = moduleData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
        
     	rownum = 0;
        for (Integer key : weekDaysKeyset) {
               Row row = weekDaysMasterSheet.createRow(rownum++);
               CellStyle style;
               if(rownum<=1){
               	style = workbook.createCellStyle();// Create style
        			XSSFFont font = workbook.createFont();// Create font
        			font.setFontName("sans-serif");// set font type
        			font.setBold(true);
        			style.setFont(font);// set it to bold
        			style.setAlignment(CellStyle.ALIGN_CENTER);
               } else{
               	style = workbook.createCellStyle();
                   XSSFFont font = workbook.createFont();
                   font.setFontName("sans-serif");
                   style.setFont(font);
               }
               Object[] objArr = weekDaysData.get(key);
               int cellnum = 0;
               for (Object obj : objArr) {
                     Cell cell = row.createCell(cellnum++);
                     cell.setCellStyle(style);
                     if (obj instanceof String)
                            cell.setCellValue((String) obj);
                     else if (obj instanceof Integer)
                            cell.setCellValue((Integer) obj);
               }
        }
       	  
            rownum = 0;
            for (Integer key : btiMessageKeyset) {
                   Row row = btiMessageMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = btiMessageData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
             rownum = 0;
            for (Integer key : countryKeyset) {
                   Row row = countryMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = countryData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : stateKeyset) {
                   Row row = stateMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = stateData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            rownum = 0;
            for (Integer key : cityKeyset) {
                   Row row = cityMasterSheet.createRow(rownum++);
                   CellStyle style;
                   if(rownum<=1){
                   	style = workbook.createCellStyle();// Create style
            			XSSFFont font = workbook.createFont();// Create font
            			font.setFontName("sans-serif");// set font type
            			font.setBold(true);
            			style.setFont(font);// set it to bold
            			style.setAlignment(CellStyle.ALIGN_CENTER);
                   } else{
                   	style = workbook.createCellStyle();
                       XSSFFont font = workbook.createFont();
                       font.setFontName("sans-serif");
                       style.setFont(font);
                   }
                   Object[] objArr = cityData.get(key);
                   int cellnum = 0;
                   for (Object obj : objArr) {
                         Cell cell = row.createCell(cellnum++);
                         cell.setCellStyle(style);
                         if (obj instanceof String)
                                cell.setCellValue((String) obj);
                         else if (obj instanceof Integer)
                                cell.setCellValue((Integer) obj);
                   }
            }
            
            try {
                   // Write the workbook in file system
                   String excelFileName = "MasterData.xlsx";
                   String fileName = URLEncoder.encode(excelFileName, "UTF-8");
                   fileName = URLDecoder.decode(fileName, "ISO8859_1");
                   response.setHeader("Content-disposition", "attachment; filename=" + fileName);
                   response.setContentType(mimetypesFileTypeMap.getContentType(excelFileName));

                   FileOutputStream out = new FileOutputStream(new File(temperotyFilePath + "\\" + excelFileName));
                   workbook.write(out);
                   out.close();

                   ByteArrayOutputStream baos = new ByteArrayOutputStream();
                   baos = convertexcelToByteArrayOutputStream(temperotyFilePath + "\\" + excelFileName);
                   OutputStream os = response.getOutputStream();
                   baos.writeTo(os);
                   os.flush();

            } catch (Exception e) {
            	LOGGER.info(Arrays.toString(e.getStackTrace()));
            }
	}

	public Map<String, String> importCompanyMasterDataForUpdateLanguage(String languageName, String languageOrientation,
			MultipartFile file, String[] dbNames, Integer languageId) {
		int loggedInUserId = 0;
		if (UtilRandomKey.isNotBlank(httpServletRequest.getHeader("userid"))) {
			loggedInUserId = Integer.parseInt(httpServletRequest.getHeader("userid"));
		}
		Map<String, String> map = new HashMap<>();
		String langId = httpServletRequest.getHeader(Constant.LANG_ID);
		String databaseNames="";
		for (String dbName : dbNames) {
			try {
				if(UtilRandomKey.isNotBlank(databaseNames)){
					databaseNames+=","+dbName;
				}
				else{
					databaseNames+=dbName;
				}
				
				Response response = RestAssured.given().contentType("multipart/form-data").header(Constant.LANG_ID, langId)
						.header(Constant.TENANT_ID, dbName.trim())
						/*.multiPart("file", file!=null && file.getBytes().length>0?file.getOriginalFilename():null,
								file!=null && file.getBytes().length>0?file.getBytes():null,
										file!=null && file.getBytes().length>0?file.getContentType():null)*/
						.multiPart("file",file.getOriginalFilename(),file.getBytes())
						.formParam("languageName", languageName).formParam("languageOrientation", languageOrientation)
						.formParam("languageId", languageId)
						.when().post(accessfinancialpath+"/importCompanyMasterDataForUpdateLanguage").andReturn();
				JsonPath jsonpath = response.getBody().jsonPath();
				String code = jsonpath.getString("code");
				if (code.equalsIgnoreCase("200")) {
					map.put(dbName, "Success");
				} else {
					map.put(dbName, "Failed");
				}
			} catch (Exception e) {
				map.put(dbName, "Failed");
			}
		}
		
		Language language=repositoryLanguage.findByLanguageIdAndIsDeleted(languageId, false);
		if(language!=null)
		{
			language.setIsActive(true);
			language.setIsDeleted(false);
			language.setLanguageName(languageName);
			language.setCreatedBy(loggedInUserId);
			language.setUpdatedBy(loggedInUserId);
			language.setLanguageOrientation(languageOrientation);
			language.setDbNames(databaseNames);
			repositoryLanguage.saveAndFlush(language);
		}
		return map;
	}
	
	public String saveUserDraftDetail(DtoScreenDetail dtoScreenDetail, int langId)
	{
		Company company=null;
		String tenantId = httpServletRequest.getHeader(Constant.TENANT_ID);
		User user = repositoryUser.findByUserIdAndIsDeleted(dtoScreenDetail.getUserId(), false);
		if(user.getRole().getRoleName().equalsIgnoreCase(BTIRoles.SUPERADMIN.name()))
		{
			UserDraft userDraft = repositoryUserDraft.checkScreenIsBlockByAnotherUserOrNot(dtoScreenDetail.getScreenCode(),dtoScreenDetail.getUserId());
			if(userDraft!=null){
				return MessageLabel.SCREEN_ALREADY_LOCKED;
			}
			
			List<UserDraft> userDraftList= repositoryUserDraft.findByUserUserId(dtoScreenDetail.getUserId());
			if(userDraftList!=null && !userDraftList.isEmpty()){
				repositoryUserDraft.deleteInBatch(userDraftList);
			}
		}
		else if(user.getRole().getRoleName().equalsIgnoreCase(BTIRoles.USER.name()))
		{
			company=repositoryCompany.findByTenantIdAndIsDeleted(tenantId, false);
			if(company!=null){
				
				UserDraft userDraft=repositoryUserDraft.checkScreenIsBlockByAnotherUserOrNotByCompany(dtoScreenDetail.getScreenCode(), dtoScreenDetail.getUserId(),company.getCompanyId());
				if(userDraft!=null){
					return MessageLabel.SCREEN_ALREADY_LOCKED;
				}
				List<UserDraft> userDraftList= repositoryUserDraft.findByUserUserIdAndCompanyCompanyId(dtoScreenDetail.getUserId(),company.getCompanyId());
				if(userDraftList!=null && !userDraftList.isEmpty()){
					repositoryUserDraft.deleteInBatch(userDraftList);
				}
			}
		}
		
		UserDraft userDraft = new UserDraft();
		userDraft.setUser(user);
		userDraft.setScreen(repositoryScreen.findByScreenCodeAndIsDeletedAndLanguageLanguageId(dtoScreenDetail.getScreenCode(), false,langId));
		userDraft.setCompany(company);
		repositoryUserDraft.saveAndFlush(userDraft);
		return MessageLabel.SCREEN_LOCKED;
	}
	
	/**
	 * Description: get company list by user Id
	 * @param userId
	 * @return
	 */
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public List<DtoUser> getUsersListByCompanyId(int companyId) {
		List<DtoUser> dtoUserList = new ArrayList<>();
		Map<Integer,String> map = new HashMap<>();
		List<UserCompanyRelation> userCompanyRelations = this.repositoryUserCompanyRelation.findByCompanyCompanyIdAndIsDeleted(companyId, false);
		if (userCompanyRelations != null && !userCompanyRelations.isEmpty()) {
			for (UserCompanyRelation userCompanyRelation : userCompanyRelations) {
				User user = userCompanyRelation.getUser();
				UserDetail userDetail = repositoryUserDetail.findByUserUserIdAndIsDeleted(user.getUserId(), false);
				if(map.get(user.getUserId()) == null && userDetail != null) {
					map.put(user.getUserId(), user.getUsername());
					DtoUser dtoUser = new DtoUser();
					dtoUser.setUserId(user.getUserId());
					dtoUser.setUserName(userDetail.getFirstName() + " " + userDetail.getLastName());
					dtoUser.setRoleId(user.getRole().getRoleId() + "");
					dtoUserList.add(dtoUser);
				}
			}
		}
		return dtoUserList;
	}
	
}
