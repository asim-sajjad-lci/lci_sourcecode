package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "grid")
@NamedQuery(name = "Grid.findAll", query = "SELECT g FROM Grid g")
public class Grid extends BaseEntity implements Serializable {

	/**
	 * default generated serial version UID
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "grid_id")
	private Integer gridId;

	@ManyToOne
	@JoinColumn(name = "screen_id")
	private Screen screenId;

	@ManyToOne
	@JoinColumn(name = "module_id")
	private Module moduleId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setGridId(Integer gridId) {
		this.gridId = gridId;
	}

	public int getGridId() {
		return gridId;
	}

	public Screen getScreenId() {
		return screenId;
	}

	public void setScreenId(Screen screenId) {
		this.screenId = screenId;
	}

	public Module getModuleId() {
		return moduleId;
	}

	public void setModuleId(Module moduleId) {
		this.moduleId = moduleId;
	}

}
