package com.bti.model.dto;

import java.util.List;

import com.bti.model.ReportMaster;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoReportMaster {

	private Integer id;
	private String reportId;
	private String reportLink;
	private String reportName;
	private String reportDescription;
	private String objectId;
	private String containerId;
	private Integer companyReportId;
	private Integer companyId;
	private String companyName;
	protected Integer pageNumber;
	protected Integer pageSize;
	protected List<Integer> ids;
	protected Boolean isActive;
	protected String messageType;
	protected String message;
	protected String deleteMessage;
	protected String associateMessage;
	protected Boolean isRepeat;
	private String sortOn;
	private String sortBy;
	private Integer totalCount;
	private Object records;
	private List<DtoReportMaster> deleteReportMaster;
	private Integer userId;
	private String userName;
	private Integer moduleId;
	private String moduleName;
	private DtoModule dtoModule;
	private DtoEjbiErpUserRelation dtoEjbiErpUserRelation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportLink() {
		return reportLink;
	}

	public void setReportLink(String reportLink) {
		this.reportLink = reportLink;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportDescription() {
		return reportDescription;
	}

	public void setReportDescription(String reportDescription) {
		this.reportDescription = reportDescription;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public Integer getCompanyReportId() {
		return companyReportId;
	}

	public void setCompanyReportId(Integer companyReportId) {
		this.companyReportId = companyReportId;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeleteMessage() {
		return deleteMessage;
	}

	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}

	public String getAssociateMessage() {
		return associateMessage;
	}

	public void setAssociateMessage(String associateMessage) {
		this.associateMessage = associateMessage;
	}

	public Boolean getIsRepeat() {
		return isRepeat;
	}

	public void setIsRepeat(Boolean isRepeat) {
		this.isRepeat = isRepeat;
	}

	public String getSortOn() {
		return sortOn;
	}

	public void setSortOn(String sortOn) {
		this.sortOn = sortOn;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Object getRecords() {
		return records;
	}

	public void setRecords(Object records) {
		this.records = records;
	}

	public List<DtoReportMaster> getDeleteReportMaster() {
		return deleteReportMaster;
	}

	public void setDeleteReportMaster(List<DtoReportMaster> deleteReportMaster) {
		this.deleteReportMaster = deleteReportMaster;
	}

	public DtoReportMaster() {

	}

	public DtoReportMaster(ReportMaster reportMaster) {
		this.reportName = reportMaster.getReportName();

	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public DtoModule getDtoModule() {
		return dtoModule;
	}

	public void setDtoModule(DtoModule dtoModule) {
		this.dtoModule = dtoModule;
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public DtoEjbiErpUserRelation getDtoEjbiErpUserRelation() {
		return dtoEjbiErpUserRelation;
	}

	public void setDtoEjbiErpUserRelation(DtoEjbiErpUserRelation dtoEjbiErpUserRelation) {
		this.dtoEjbiErpUserRelation = dtoEjbiErpUserRelation;
	}

}
