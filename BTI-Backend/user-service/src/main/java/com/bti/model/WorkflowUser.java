/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
* Description: The persistent class for the fields database table.
* Name of Project: BTI
* Created on: June 20, 2017
* Modified on: June 20, 2017 11:19:38 AM
* @author seasia
* Version: 
*/
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "workflow_users")
@NamedQuery(name = "WorkflowUser.findAll", query = "SELECT w FROM WorkflowUser w")
public class WorkflowUser extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "user_hcm_id")
	private Integer userHcmId;
	
	@Column(name = "user_name")
	private String userName;

	@Column(name = "user_company_id")
	private Integer userCompanyId;

	@Column(name = "user_tenant_id")
	private String userTenantId;
	
	public WorkflowUser() {
		
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public Integer getUserHcmId() {
		return userHcmId;
	}

	public void setUserHcmId(Integer userHcmId) {
		this.userHcmId = userHcmId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUserCompanyId() {
		return userCompanyId;
	}

	public void setUserCompanyId(Integer userCompanyId) {
		this.userCompanyId = userCompanyId;
	}

	public String getUserTenantId() {
		return userTenantId;
	}

	public void setUserTenantId(String userTenantId) {
		this.userTenantId = userTenantId;
	}

}