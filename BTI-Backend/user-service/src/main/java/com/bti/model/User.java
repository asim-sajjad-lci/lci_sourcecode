/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
* Description: The persistent class for the user database table.
* Name of Project: BTI
* Created on: June 20, 2017
 * Modified on: Sept 06, 2018 11:19:38 AM
 * 
* @author seasia
* Version: 
*/
@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int userId;

	private String email;

	@Column(name = "employee_code")
	private String employeeCode;

	@Column(name = "is_active", columnDefinition = "tinyint(0) default 0")
	private boolean isActive;

	@Column(name = "is_reset_password", columnDefinition = "tinyint(0) default 0")
	private boolean isResetPassword;
	
	@Column(name = "ip_checked", columnDefinition = "tinyint(0) default 1")
	protected Boolean ipChecked;

	private String password;

	private String username;

	// bi-directional many-to-one association to LoginOtp
	@OneToMany(mappedBy = "user")
	private List<LoginOtp> loginOtps;

	// bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;

	// bi-directional many-to-one association to UserCompanyRelation
	@OneToMany(mappedBy = "user")
	private List<UserCompanyRelation> userCompanyRelations;

	// bi-directional many-to-one association to UserDetail
	@OneToMany(mappedBy = "user")
	private List<UserDetail> userDetails;

	// bi-directional many-to-one association to UserSession
	@OneToMany(mappedBy = "user")
	private List<UserSession> userSessions;

	@ManyToMany(cascade = { CascadeType.ALL })
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ReportMaster> reportMaster;

	public User() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmployeeCode() {
		return this.employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<LoginOtp> getLoginOtps() {
		return this.loginOtps;
	}

	public void setLoginOtps(List<LoginOtp> loginOtps) {
		this.loginOtps = loginOtps;
	}

	public LoginOtp addLoginOtp(LoginOtp loginOtp) {
		getLoginOtps().add(loginOtp);
		loginOtp.setUser(this);

		return loginOtp;
	}

	public LoginOtp removeLoginOtp(LoginOtp loginOtp) {
		getLoginOtps().remove(loginOtp);
		loginOtp.setUser(null);

		return loginOtp;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<UserCompanyRelation> getUserCompanyRelations() {
		return this.userCompanyRelations;
	}

	public void setUserCompanyRelations(List<UserCompanyRelation> userCompanyRelations) {
		this.userCompanyRelations = userCompanyRelations;
	}

	public UserCompanyRelation addUserCompanyRelation(UserCompanyRelation userCompanyRelation) {
		getUserCompanyRelations().add(userCompanyRelation);
		userCompanyRelation.setUser(this);

		return userCompanyRelation;
	}

	public UserCompanyRelation removeUserCompanyRelation(UserCompanyRelation userCompanyRelation) {
		getUserCompanyRelations().remove(userCompanyRelation);
		userCompanyRelation.setUser(null);

		return userCompanyRelation;
	}

	public List<UserDetail> getUserDetails() {
		return this.userDetails;
	}

	public void setUserDetails(List<UserDetail> userDetails) {
		this.userDetails = userDetails;
	}

	public UserDetail addUserDetail(UserDetail userDetail) {
		getUserDetails().add(userDetail);
		userDetail.setUser(this);

		return userDetail;
	}

	public UserDetail removeUserDetail(UserDetail userDetail) {
		getUserDetails().remove(userDetail);
		userDetail.setUser(null);

		return userDetail;
	}

	public List<UserSession> getUserSessions() {
		return this.userSessions;
	}

	public void setUserSessions(List<UserSession> userSessions) {
		this.userSessions = userSessions;
	}

	public UserSession addUserSession(UserSession userSession) {
		getUserSessions().add(userSession);
		userSession.setUser(this);

		return userSession;
	}

	public UserSession removeUserSession(UserSession userSession) {
		getUserSessions().remove(userSession);
		userSession.setUser(null);

		return userSession;
	}

	public boolean isResetPassword() {
		return isResetPassword;
	}

	public void setResetPassword(boolean isResetPassword) {
		this.isResetPassword = isResetPassword;
	}

	public Boolean getIpChecked() {
		return ipChecked;
	}

	public void setIpChecked(Boolean ipChecked) {
		this.ipChecked = ipChecked;
	}
	
	public List<ReportMaster> getReportMaster() {
		return reportMaster;
	}

	public void setReportMaster(List<ReportMaster> reportMaster) {
		this.reportMaster = reportMaster;
	}
	
}