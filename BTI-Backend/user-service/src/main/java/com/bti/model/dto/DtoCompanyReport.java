package com.bti.model.dto;

import java.util.List;

import com.bti.model.CompanyReport;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoCompanyReport {
	
	private Integer id;
	private String description;
	private Integer companyId;
	private List<DtoReportMaster> dtoReportMaster;
	protected Integer pageNumber;
	protected Integer pageSize;
	protected List<Integer> ids;
	protected Boolean isActive;
	protected String messageType;
	protected String message;
	protected String deleteMessage;
	protected String associateMessage;
	protected Boolean isRepeat;
	private String sortOn;
	private String sortBy;
	private Integer totalCount;
	private Object records;
	private List<DtoCompanyReport> deleteCompanyReport;
	
	
	
public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public List<DtoReportMaster> getDtoReportMaster() {
		return dtoReportMaster;
	}
	public void setDtoReportMaster(List<DtoReportMaster> dtoReportMaster) {
		this.dtoReportMaster = dtoReportMaster;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public List<Integer> getIds() {
		return ids;
	}
	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDeleteMessage() {
		return deleteMessage;
	}
	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}
	public String getAssociateMessage() {
		return associateMessage;
	}
	public void setAssociateMessage(String associateMessage) {
		this.associateMessage = associateMessage;
	}
	public Boolean getIsRepeat() {
		return isRepeat;
	}
	public void setIsRepeat(Boolean isRepeat) {
		this.isRepeat = isRepeat;
	}
	public String getSortOn() {
		return sortOn;
	}
	public void setSortOn(String sortOn) {
		this.sortOn = sortOn;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Object getRecords() {
		return records;
	}
	public void setRecords(Object records) {
		this.records = records;
	}
	public List<DtoCompanyReport> getDeleteCompanyReport() {
		return deleteCompanyReport;
	}
	public void setDeleteCompanyReport(List<DtoCompanyReport> deleteCompanyReport) {
		this.deleteCompanyReport = deleteCompanyReport;
	}
	
	
public DtoCompanyReport() {
		
	}
	public DtoCompanyReport(CompanyReport companyReport) {
		this.companyId = companyReport.getCompany().getCompanyId();
	
	}

}
