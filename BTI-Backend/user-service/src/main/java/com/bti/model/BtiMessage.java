/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
* Description: The persistent class for the bti_message database table.
* Name of Project: BTI
* Created on: June 20, 2017
* Modified on: June 20, 2017 11:19:38 AM
* @author seasia
* Version: 
*/
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "bti_message")
@NamedQuery(name = "BtiMessage.findAll", query = "SELECT b FROM BtiMessage b")
public class BtiMessage extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bti_message_id")
	private int btiMessageId;

	@Column(name="message")
	private String message;

	@Column(name = "message_short")
	private String messageShort;

	@ManyToOne
	@JoinColumn(name="lang_id")
	private Language language;
 
	public int getBtiMessageId() {
		return this.btiMessageId;
	}

	public void setBtiMessageId(int btiMessageId) {
		this.btiMessageId = btiMessageId;
	}

	public String getMessageShort() {
		return this.messageShort;
	}

	public void setMessageShort(String messageShort) {
		this.messageShort = messageShort;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

}