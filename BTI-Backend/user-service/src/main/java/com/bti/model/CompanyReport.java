package com.bti.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "companyreport",indexes = {
        @Index(columnList = "COMPANYREPORTID")
})
public class CompanyReport extends BaseEntity implements Serializable{
	
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COMPANYREPORTID")
	private int id;


	@Column(name = "DESCRIPTION", columnDefinition = "char(150)")
	private String description;

	
	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	
	@ManyToMany(cascade = {CascadeType.ALL})
	@JoinTable(
	        name = "company_report", 
	        joinColumns = { @JoinColumn(name = "companyreport_id") }, 
	        inverseJoinColumns = { @JoinColumn(name = "report_master_id") }
	    )
	private List<ReportMaster> reportMasterList;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}


	public List<ReportMaster> getReportMasterList() {
		return reportMasterList;
	}


	public void setReportMasterList(List<ReportMaster> reportMasterList) {
		this.reportMasterList = reportMasterList;
	}

	
	
}
