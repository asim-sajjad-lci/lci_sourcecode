/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.Field;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
* Description: DTO Field Detail class having getter and setter for fields (POJO) Name
* Name of Project: BTI
* Created on: May 12, 2017
* Modified on: May 12, 2017 4:19:38 PM
* @author seasia
* Version: 
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoErpClient {


	private int id;
	private String clientId;
	private String clientDescription;
	private String clientDescriptionArabic;
	private String authToken;

	public DtoErpClient() {

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientDescription() {
		return clientDescription;
	}

	public void setClientDescription(String clientDescription) {
		this.clientDescription = clientDescription;
	}

	public String getClientDescriptionArabic() {
		return clientDescriptionArabic;
	}

	public void setClientDescriptionArabic(String clientDescriptionArabic) {
		this.clientDescriptionArabic = clientDescriptionArabic;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

}
