/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.Screen;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
* Description: DTO SideMenu class having getter and setter for fields (POJO) Name
* Name of Project: BTI
* Created on: May 12, 2017
* Modified on: May 12, 2017 4:19:38 PM
 * 
* @author seasia
* Version: 
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoSideMenu {
	
	private Integer screenId;
	private String sideMenuName;
	private String helpMessagesideMenu;
	private String sideMenuURL;
	private String screenCategoryCode;
	private String companyId;
	private String userId;

	public DtoSideMenu() {

	}

	public DtoSideMenu(Screen screen) {
		   this.screenId = screen.getScreenId();
			if (UtilRandomKey.isNotBlank(screen.getHelpMessage())) {
				this.helpMessagesideMenu = screen.getHelpMessage();
			} else {
				this.helpMessagesideMenu = "";
			}

			if (UtilRandomKey.isNotBlank(screen.getSideMenu())) {
				this.sideMenuName = screen.getSideMenu();
			} else {
				this.sideMenuName = "";
			}
		   this.sideMenuURL=screen.getSideMenuURL();
	}

	public Integer getScreenId() {
		return screenId;
	}

	public void setScreenId(Integer screenId) {
		this.screenId = screenId;
	}

	public String getSideMenuName() {
		return sideMenuName;
	}

	public void setSideMenuName(String sideMenuName) {
		this.sideMenuName = sideMenuName;
	}

	public String getHelpMessagesideMenu() {
		return helpMessagesideMenu;
	}

	public void setHelpMessagesideMenu(String helpMessagesideMenu) {
		this.helpMessagesideMenu = helpMessagesideMenu;
	}

	public String getSideMenuURL() {
		return sideMenuURL;
	}

	public void setSideMenuURL(String sideMenuURL) {
		this.sideMenuURL = sideMenuURL;
	}

	public String getScreenCategoryCode() {
		return screenCategoryCode;
	}
	
	public void setScreenCategoryCode(String screenCategoryCode) {
		this.screenCategoryCode = screenCategoryCode;
	}
	
	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
