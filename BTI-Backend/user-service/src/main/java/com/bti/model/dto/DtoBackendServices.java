/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.ServicesPlugUnplug;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoBackendServices {

	private int id;
	private String name;
	private String description;
	private int port;
	private String status;
	private int processId;
	private String serverIP;
	private String buildLocation;
	
	public DtoBackendServices(){
	
	}
	
	public DtoBackendServices(ServicesPlugUnplug servicesPlugUnplug){
		this.id = servicesPlugUnplug.getId();
		this.name = servicesPlugUnplug.getServiceName();
		this.description = servicesPlugUnplug.getDescription();
		this.port = servicesPlugUnplug.getPort();
		this.processId = servicesPlugUnplug.getProcessId();
		this.serverIP = servicesPlugUnplug.getServerIP();
		if(servicesPlugUnplug.getIsActive()!=null && servicesPlugUnplug.getIsActive()){
			this.status = "Plugged";
		}else{
			this.status = "Unplugged";
		}
		this.buildLocation = servicesPlugUnplug.getBuildLocation();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public int getProcessId() {
		return processId;
	}

	public void setProcessId(int processId) {
		this.processId = processId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getBuildLocation() {
		return buildLocation;
	}

	public void setBuildLocation(String buildLocation) {
		this.buildLocation = buildLocation;
	}
	
	
}
