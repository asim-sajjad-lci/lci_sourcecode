package com.bti.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoWorkflowUserDetails {


	private Date last_connection = null;
	private Integer created_by_user_id = 0;
	private Date creation_date = null;
	private String icon = "";
	private String password = "";
	private Date last_update_date = null;

	private String tenantid = "";
	
	private Integer userId = 0;
	private Integer erpUserId = 0;
	
	private String userName = "";
	private String employeeTitle = "";
	private String employeeTitleArabic = "";

	private String employeeFirstName = "";
	private String employeeLastName = "";

	private String employeeFirstNameArabic = "";
	private String employeeLastNameArabic = "";
	
	private String employeeJobTitle = "";
	private String employeeJobTitleArabic = "";
	
	private Integer employeeManagerId = 0;
	private String managerUserName = "";
	private Boolean employeeActive = false;
	

	public DtoWorkflowUserDetails() {

	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getErpUserId() {
		return erpUserId;
	}

	public void setErpUserId(Integer erpUserId) {
		this.erpUserId = erpUserId;
	}

	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getEmployeeTitle() {
		return employeeTitle;
	}


	public void setEmployeeTitle(String employeeTitle) {
		this.employeeTitle = employeeTitle;
	}


	public String getEmployeeTitleArabic() {
		return employeeTitleArabic;
	}


	public void setEmployeeTitleArabic(String employeeTitleArabic) {
		this.employeeTitleArabic = employeeTitleArabic;
	}


	public String getEmployeeFirstName() {
		return employeeFirstName;
	}


	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}


	public String getEmployeeLastName() {
		return employeeLastName;
	}


	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}


	public String getEmployeeFirstNameArabic() {
		return employeeFirstNameArabic;
	}


	public void setEmployeeFirstNameArabic(String employeeFirstNameArabic) {
		this.employeeFirstNameArabic = employeeFirstNameArabic;
	}


	public String getEmployeeLastNameArabic() {
		return employeeLastNameArabic;
	}


	public void setEmployeeLastNameArabic(String employeeLastNameArabic) {
		this.employeeLastNameArabic = employeeLastNameArabic;
	}


	public String getEmployeeJobTitle() {
		return employeeJobTitle;
	}


	public void setEmployeeJobTitle(String employeeJobTitle) {
		this.employeeJobTitle = employeeJobTitle;
	}


	public String getEmployeeJobTitleArabic() {
		return employeeJobTitleArabic;
	}


	public void setEmployeeJobTitleArabic(String employeeJobTitleArabic) {
		this.employeeJobTitleArabic = employeeJobTitleArabic;
	}


	public Integer getEmployeeManagerId() {
		return employeeManagerId;
	}


	public void setEmployeeManagerId(Integer employeeManagerId) {
		this.employeeManagerId = employeeManagerId;
	}

	public String getManagerUserName() {
		return managerUserName;
	}

	public void setManagerUserName(String managerUserName) {
		this.managerUserName = managerUserName;
	}

	public Boolean getEmployeeActive() {
		return employeeActive;
	}

	public void setEmployeeActive(Boolean employeeActive) {
		this.employeeActive = employeeActive;
	}
	
	public String getTenantid() {
		return tenantid;
	}

	public void setTenantid(String tenantid) {
		this.tenantid = tenantid;
	}

	public Date getLast_connection() {
		return last_connection;
	}


	public void setLast_connection(Date last_connection) {
		this.last_connection = last_connection;
	}


	public Integer getCreated_by_user_id() {
		return created_by_user_id;
	}


	public void setCreated_by_user_id(Integer created_by_user_id) {
		this.created_by_user_id = created_by_user_id;
	}


	public Date getCreation_date() {
		return creation_date;
	}


	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}


	public String getIcon() {
		return icon;
	}


	public void setIcon(String icon) {
		this.icon = icon;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Date getLast_update_date() {
		return last_update_date;
	}


	public void setLast_update_date(Date last_update_date) {
		this.last_update_date = last_update_date;
	}
	
	
}
