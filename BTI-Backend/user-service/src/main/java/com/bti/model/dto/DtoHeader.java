/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.List;

import com.bti.model.Module;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
* Description: DTO Module class having getter and setter for fields (POJO) Name
* Name of Project: BTI
* Created on: May 12, 2017
* Modified on: May 12, 2017 4:19:38 PM
* @author seasia
* Version: 
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoHeader {
	
	private Integer moduleId;
	private List<DtoSideMenu> sideMenuList;
	private String headerName;
	private String helpMessageHeader;
	private List<DtoScreenCategory> screenCategoryList;

	public DtoHeader() {

	}

	public DtoHeader(Module module) {
		this.moduleId = module.getModuleId();
			if (UtilRandomKey.isNotBlank(module.getHelpMessage())) {
				this.helpMessageHeader = module.getHelpMessage();
			} else {
				this.helpMessageHeader = "";
			}

			if (UtilRandomKey.isNotBlank(module.getName())) {
				this.headerName = module.getName();
			} else {
				this.headerName = "";
			}
	}

	public Integer getModuleId() {
		return moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public List<DtoSideMenu> getSideMenuList() {
		return sideMenuList;
	}

	public void setSideMenuList(List<DtoSideMenu> sideMenuList) {
		this.sideMenuList = sideMenuList;
	}

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	public String getHelpMessageHeader() {
		return helpMessageHeader;
	}

	public void setHelpMessageHeader(String helpMessageHeader) {
		this.helpMessageHeader = helpMessageHeader;
	}
	
	public List<DtoScreenCategory> getScreenCategoryList() {
		return screenCategoryList;
	}

	public void setScreenCategoryList(List<DtoScreenCategory> screenCategoryList) {
		this.screenCategoryList = screenCategoryList;
	}

}
