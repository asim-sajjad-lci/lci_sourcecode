/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */

package com.bti.model.dto;


import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoWorkflowUser {

	public DtoWorkflowUser() {
		
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getUserHcmId() {
		return userHcmId;
	}

	public void setUserHcmId(Integer userHcmId) {
		this.userHcmId = userHcmId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getUserCompanyId() {
		return userCompanyId;
	}

	public void setUserCompanyId(Integer userCompanyId) {
		this.userCompanyId = userCompanyId;
	}

	public String getUserTenantId() {
		return userTenantId;
	}

	public void setUserTenantId(String userTenantId) {
		this.userTenantId = userTenantId;
	}	

	private Integer id;
	private Integer userHcmId;
	private String userName;
	private Integer userCompanyId;
	private String userTenantId;
	private static final int COMPANY_OFFSET = 100000; 
	
}
