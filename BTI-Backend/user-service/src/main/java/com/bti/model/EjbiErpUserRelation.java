package com.bti.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "ejbi_erp_user_relation", indexes = { @Index(columnList = "user_rel_id") })
public class EjbiErpUserRelation extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_rel_id")
	private int id;

	@Column(name = "ejbi_user_id", columnDefinition = "char(50)")
	private String ejbiUserId;

	@Column(name = "ejbi_user_name", columnDefinition = "char(50)")
	private String ejbiUserName;

	@Column(name = "ejbi_user_password", columnDefinition = "char(50)")
	private String ejbiUserPassword;

	@Column(name = "container_id", columnDefinition = "char(50)")
	private String containerId;
	
	@Column(name = "ejbi_url", columnDefinition = "char(50)")
	private String ejbiurl;
	
	@Column(name = "object_id", columnDefinition = "char(50)")
	private String objectId;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id")
	private Company company;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "RPTINDX")
	private ReportMaster reportMaster;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "lang_id")
	private Language language;

	public EjbiErpUserRelation() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEjbiUserId() {
		return ejbiUserId;
	}

	public void setEjbiUserId(String ejbiUserId) {
		this.ejbiUserId = ejbiUserId;
	}

	public String getEjbiUserName() {
		return ejbiUserName;
	}

	public void setEjbiUserName(String ejbiUserName) {
		this.ejbiUserName = ejbiUserName;
	}

	public String getEjbiUserPassword() {
		return ejbiUserPassword;
	}

	public void setEjbiUserPassword(String ejbiUserPassword) {
		this.ejbiUserPassword = ejbiUserPassword;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getEjbiurl() {
		return ejbiurl;
	}

	public void setEjbiurl(String ejbiurl) {
		this.ejbiurl = ejbiurl;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ReportMaster getReportMaster() {
		return reportMaster;
	}

	public void setReportMaster(ReportMaster reportMaster) {
		this.reportMaster = reportMaster;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

}
