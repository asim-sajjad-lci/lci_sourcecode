package com.bti.model.dto;

import java.util.List;

public class DtoRuleDefinition {
	private String ruleId;
	private String ruleCode;
	private String processCode;
	private List<DtoRuleParams> params;
	private List<DtoRuleConnectors> connectors;
	
	public String getRuleId() {
		return ruleId;
	}
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}
	public String getRuleCode() {
		return ruleCode;
	}
	public void setRuleCode(String ruleCode) {
		this.ruleCode = ruleCode;
	}
	public String getProcessCode() {
		return processCode;
	}
	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}
	public List<DtoRuleParams> getParams() {
		return params;
	}
	public void setParams(List<DtoRuleParams> params) {
		this.params = params;
	}
	public List<DtoRuleConnectors> getConnectors() {
		return connectors;
	}
	public void setConnectors(List<DtoRuleConnectors> connectors) {
		this.connectors = connectors;
	}
	
}
