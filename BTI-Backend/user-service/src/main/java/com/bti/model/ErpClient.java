/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
* Description: The persistent class for the fields database table.
* Name of Project: BTI
* Created on: June 20, 2017
* Modified on: June 20, 2017 11:19:38 AM
* @author seasia
* Version: 
*/
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "erp_clients")
@NamedQuery(name = "ErpClient.findAll", query = "SELECT f FROM ErpClient f")
public class ErpClient extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;


	@Column(name = "client_id")
	private String clientId;

	@Column(name = "client_description")
	private String clientDescription;

	@Column(name = "client_description_ar")
	private String clientDescriptionArabic;

	@Column(name = "auth_token")
	private String authToken;
	

	public ErpClient() {
		
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientDescription() {
		return clientDescription;
	}

	public void setClientDescription(String clientDescription) {
		this.clientDescription = clientDescription;
	}

	public String getClientDescriptionArabic() {
		return clientDescriptionArabic;
	}

	public void setClientDescriptionArabic(String clientDescriptionArabic) {
		this.clientDescriptionArabic = clientDescriptionArabic;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

}