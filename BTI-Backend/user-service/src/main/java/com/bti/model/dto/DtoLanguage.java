/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.bti.model.Language;
import com.bti.util.UtilRandomKey;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Language class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: October 05, 2017
 * Modified on: October 05, 2017 2:19:38 PM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DtoLanguage {
	
	private int languageId;
	private String languageName;
	private String languageOrientation;
	private String languageStatus;
	private Boolean isActive;
	private MultipartFile file;
	List<String> dbNames;
	
	public DtoLanguage() {
		// TODO Auto-generated constructor stub
	}
	
	public DtoLanguage(Language language) {
		this.languageId=language.getLanguageId();
		this.languageName="";
		if(UtilRandomKey.isNotBlank(language.getLanguageName())){
			this.languageName=language.getLanguageName();
		}
		this.languageOrientation="";
		if(UtilRandomKey.isNotBlank(language.getLanguageOrientation())){
			this.languageOrientation=language.getLanguageOrientation();
		}
		
		this.isActive=language.getIsActive();
		dbNames= new ArrayList<>();
		if(UtilRandomKey.isNotBlank(language.getDbNames()))
		{
			String[] names= language.getDbNames().split(",");
			if(names.length>0){
				
				for (String name : names) {
					dbNames.add(name);
				}
			}
		}
	}
	
	public int getLanguageId() {
		return languageId;
	}
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getLanguageOrientation() {
		return languageOrientation;
	}
	public void setLanguageOrientation(String languageOrientation) {
		this.languageOrientation = languageOrientation;
	}

	public String getLanguageStatus() {
		return languageStatus;
	}

	public void setLanguageStatus(String languageStatus) {
		this.languageStatus = languageStatus;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
}
