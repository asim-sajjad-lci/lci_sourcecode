package com.bti.model.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoEjbiErpUserRelation {

	private Integer userRelationId;
	private String ejbiUserId;
	private String ejbiUserName;
	private String ejbiUserPassword;
	private String containerId;
	private String ejbiurl;
	private Integer userId;
	private String userName;
	private Integer companyId;
	private String companyName;
	private Integer reportId;
	private String reportLink;
	private String reportName;
	private String reportDescription;
	private String objectId;
	protected Integer pageNumber;
	protected Integer pageSize;
	protected List<Integer> ids;
	protected String messageType;
	protected String message;
	protected String deleteMessage;
	protected String associateMessage;

	public Integer getUserRelationId() {
		return userRelationId;
	}

	public void setUserRelationId(Integer userRelationId) {
		this.userRelationId = userRelationId;
	}

	public String getEjbiUserId() {
		return ejbiUserId;
	}

	public void setEjbiUserId(String ejbiUserId) {
		this.ejbiUserId = ejbiUserId;
	}

	public String getEjbiUserName() {
		return ejbiUserName;
	}

	public void setEjbiUserName(String ejbiUserName) {
		this.ejbiUserName = ejbiUserName;
	}

	public String getEjbiUserPassword() {
		return ejbiUserPassword;
	}

	public void setEjbiUserPassword(String ejbiUserPassword) {
		this.ejbiUserPassword = ejbiUserPassword;
	}

	public String getEjbiurl() {
		return ejbiurl;
	}

	public void setEjbiurl(String ejbiurl) {
		this.ejbiurl = ejbiurl;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public String getReportLink() {
		return reportLink;
	}

	public void setReportLink(String reportLink) {
		this.reportLink = reportLink;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getReportDescription() {
		return reportDescription;
	}

	public void setReportDescription(String reportDescription) {
		this.reportDescription = reportDescription;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public Integer getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDeleteMessage() {
		return deleteMessage;
	}

	public void setDeleteMessage(String deleteMessage) {
		this.deleteMessage = deleteMessage;
	}

	public String getAssociateMessage() {
		return associateMessage;
	}

	public void setAssociateMessage(String associateMessage) {
		this.associateMessage = associateMessage;
	}

}
