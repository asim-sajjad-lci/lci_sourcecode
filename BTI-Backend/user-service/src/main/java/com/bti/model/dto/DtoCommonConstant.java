/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.bti.model.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO Access Role class having getter and setter for fields (POJO) Name
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoCommonConstant {

	private Integer id;
	private String constantShort;
	private String constantValue;
	
	public DtoCommonConstant(CommonConstant commonConstant) {
		this.id=commonConstant.getId();
		this.constantShort=commonConstant.getConstantShort();
		this.constantValue=commonConstant.getConstantValue();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getConstantShort() {
		return constantShort;
	}
	public void setConstantShort(String constantShort) {
		this.constantShort = constantShort;
	}
	public String getConstantValue() {
		return constantValue;
	}
	public void setConstantValue(String constantValue) {
		this.constantValue = constantValue;
	}
}
