/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the access_role_report_relation database table.
 * Name of Project: BTI
 * Created on: June 20, 2017
 * Modified on: June 20, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "access_role_report_relation")
@NamedQuery(name = "AccessRoleReportRelation.findAll", query = "SELECT a FROM AccessRoleReportRelation a")
public class AccessRoleReportRelation extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "access_role_report_relation_id")
	private int accessRoleReportRelationId;

	// bi-directional many-to-one association to Task
	@ManyToOne
	@JoinColumn(name = "report_category_id")
	private ReportCategory reportCategory;

	@ManyToOne
	@JoinColumn(name = "report_id")
	private Reports reports;

	// bi-directional many-to-one association to Module
	@ManyToOne
	@JoinColumn(name = "access_role_module_relation_id")
	private AccessRoleModuleRelation accessRoleModuleRelation;

	@Column(name = "view_access")
	private Boolean viewAccess;

	@Column(name = "email_access")
	private Boolean emailAccess;

	@Column(name = "export_access")
	private Boolean exportAccess;

	public int getAccessRoleReportRelationId() {
		return accessRoleReportRelationId;
	}

	public void setAccessRoleReportRelationId(int accessRoleReportRelationId) {
		this.accessRoleReportRelationId = accessRoleReportRelationId;
	}

	public ReportCategory getReportCategory() {
		return reportCategory;
	}

	public void setReportCategory(ReportCategory reportCategory) {
		this.reportCategory = reportCategory;
	}

	public Reports getReports() {
		return reports;
	}

	public void setReports(Reports reports) {
		this.reports = reports;
	}

	public Boolean getViewAccess() {
		return viewAccess;
	}

	public void setViewAccess(Boolean viewAccess) {
		this.viewAccess = viewAccess;
	}

	public Boolean getEmailAccess() {
		return emailAccess;
	}

	public void setEmailAccess(Boolean emailAccess) {
		this.emailAccess = emailAccess;
	}

	public Boolean getExportAccess() {
		return exportAccess;
	}

	public void setExportAccess(Boolean exportAccess) {
		this.exportAccess = exportAccess;
	}

	public AccessRoleModuleRelation getAccessRoleModuleRelation() {
		return accessRoleModuleRelation;
	}

	public void setAccessRoleModuleRelation(AccessRoleModuleRelation accessRoleModuleRelation) {
		this.accessRoleModuleRelation = accessRoleModuleRelation;
	}

}