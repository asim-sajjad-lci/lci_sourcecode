/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Description: The persistent class for the access_role_task_relation database table.
 * Name of Project: BTI
 * Created on: June 20, 2017
 * Modified on: June 20, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */
@Entity @org.hibernate.annotations.Entity(dynamicInsert = true)
@Table(name = "access_role_transaction_relation")
@NamedQuery(name = "AccessRoleTransactionRelation.findAll", query = "SELECT a FROM AccessRoleTransactionRelation a")
public class AccessRoleTransactionRelation extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "access_role_transaction_relation_id")
	private int accessRoleTransactionRelationId;

	// bi-directional many-to-one association to Task
	@ManyToOne
	@JoinColumn(name = "transaction_type_id")
	private TransactionType transactionType;

	@ManyToOne
	@JoinColumn(name = "transactions_id")
	private Transactions transactions;

	@ManyToOne
	@JoinColumn(name = "access_role_module_relation_id")
	private AccessRoleModuleRelation accessRoleModuleRelation;

	@Column(name = "view_access")
	private Boolean viewAccess;

	@Column(name = "post_access")
	private Boolean postAccess;

	@Column(name = "delete_access")
	private Boolean deleteAccess;

	public int getAccessRoleTransactionRelationId() {
		return accessRoleTransactionRelationId;
	}

	public void setAccessRoleTransactionRelationId(int accessRoleTransactionRelationId) {
		this.accessRoleTransactionRelationId = accessRoleTransactionRelationId;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Transactions getTransactions() {
		return transactions;
	}

	public void setTransactions(Transactions transactions) {
		this.transactions = transactions;
	}

	public Boolean getViewAccess() {
		return viewAccess;
	}

	public void setViewAccess(Boolean viewAccess) {
		this.viewAccess = viewAccess;
	}

	public Boolean getPostAccess() {
		return postAccess;
	}

	public void setPostAccess(Boolean postAccess) {
		this.postAccess = postAccess;
	}

	public Boolean getDeleteAccess() {
		return deleteAccess;
	}

	public void setDeleteAccess(Boolean deleteAcces) {
		this.deleteAccess = deleteAcces;
	}

	public AccessRoleModuleRelation getAccessRoleModuleRelation() {
		return accessRoleModuleRelation;
	}

	public void setAccessRoleModuleRelation(AccessRoleModuleRelation accessRoleModuleRelation) {
		this.accessRoleModuleRelation = accessRoleModuleRelation;
	}

}