/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Description: DTO WeekDay class having getter and setter for fields (POJO) dayName
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoWeekDay {

	private Integer weekDayId;
	private String dayName;

	public DtoWeekDay() {
		super();
	}

	public DtoWeekDay(Integer weekDayId, String dayName) {
		super();
		this.weekDayId = weekDayId;
		this.dayName = dayName;
	}

	/**
	 * @return the weekDayId
	 */
	public Integer getWeekDayId() {
		return weekDayId;
	}
	/**
	 * @param weekDayId the weekDayId to set
	 */
	public void setWeekDayId(Integer weekDayId) {
		this.weekDayId = weekDayId;
	}
	/**
	 * @return the dayName
	 */
	public String getDayName() {
		return dayName;
	}
	/**
	 * @param dayName the dayName to set
	 */
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}

}
