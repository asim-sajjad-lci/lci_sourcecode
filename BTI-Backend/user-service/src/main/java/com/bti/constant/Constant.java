package com.bti.constant;

public class Constant {
	private Constant() {
	}
	public static final String USER_ID = "userid";
	public static final String ERP_USER_ID = "erpUserId";
	public static final String LANG_ID = "langId";
	public static final String SESSION = "session";
	public static final String TENANT_ID = "tenantid";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final Object APPLICATION_JSON = "application/json";
	public static final String ACTIVE = "ACTIVE";
	public static final String INACTIVE = "INACTIVE";
	public static final String SESSION_EXPIRED = "SESSION_EXPIRED";
	public static final String SUCCESS = "success";
	
	
	
	
	public static final String REM_EMPLOYEE_INDEX = "EMP_IDX";
	public static final String REM_EMPLOYEE_USER_ID = "EMP_ID";
	
	public static final String REM_EMPLOYEE_TITLE = "EMP_TITLE";
	public static final String REM_EMPLOYEE_FIRSTNAME = "EMP_FIRST_NAME";
	public static final String REM_EMPLOYEE_LASTNAME = "EMP_LAST_NAME";
	
	public static final String REM_EMPLOYEE_TITLE_AR = "EMP_TITLE_AR";
	public static final String REM_EMPLOYEE_FIRSTNAME_AR = "EMP_FIRST_NAME_AR";
	public static final String REM_EMPLOYEE_LASTNAME_AR = "EMP_LAST_NAME_AR";
	
	public static final String REM_EMPLOYEE_JOB_TITLE = "EMP_JOB_TITLE";
	public static final String REM_EMPLOYEE_JOB_TITLE_AR = "EMP_JOB_TITLE_AR";
	
	public static final String REM_EMPLOYEE_MANAGER_INDEX = "MANAGER_ID";
	
	public static final String REM_EMPLOYEE_DISALBED_FLAG = "DISABLED";
	public static final String REM_EMPLOYEE_ERP_USER_INDEX = "ERP_USER_ID";
	public static final String REM_EMPLOYEE_IMAGE = "EMP_IMAGE";
	public static final String REM_EMPLOYEE_PASSWORD = "WORKFLOW_PASSWORD";
	public static final String CLIENT_ID = "clientId";
	public static final String CLIENT_TOKEN = "token";
	public static final String PROCESS_CODE = "processCode";
	public static final String RULE_CODE = "ruleCode";
	
	
	
}
