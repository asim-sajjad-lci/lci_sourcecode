//package com.bti.config;
//
//import org.apache.solr.client.solrj.SolrClient;
//import org.apache.solr.client.solrj.SolrServer;
//import org.apache.solr.client.solrj.impl.HttpSolrClient;
//import org.apache.solr.client.solrj.impl.HttpSolrServer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.solr.core.SolrTemplate;
//import org.springframework.data.solr.repository.config.EnableSolrRepositories;
//
//import com.bti.QESXMLResponseParser;
//
//@Configuration
//@EnableSolrRepositories(
//  basePackages = {"com.bti.solrRepository"})
//@ComponentScan
//public class SolrConfig {
// 
//    @Bean
//    public SolrClient solrClient() {
//      HttpSolrClient httpSolrClient= new HttpSolrClient("http://localhost:8983/solr/#/Product");
//      httpSolrClient.setParser(new QESXMLResponseParser());
//      return httpSolrClient;
//    }
//    
////    @Bean
////    public SolrClient solrServer() {
////    	HttpSolrClient httpSolrClient= new HttpSolrClient("http://localhost:8983/solr/#/Product");
////        httpSolrClient.setParser(new QESXMLResponseParser());
////        return httpSolrClient;
////    }
//   
//    @Bean
//    public SolrTemplate solrTemplate(SolrServer server) throws Exception {
//      return new SolrTemplate(server);
//    }
//    
//}