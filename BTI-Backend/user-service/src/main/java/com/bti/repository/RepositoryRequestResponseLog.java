/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */package com.bti.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.RequestResponseLog;;


/**
 * Description: DAO Interface for RepositoryRoleGroupAccessRole 
 * Name of Project: BTI
 * Created on: May 15, 2017
 * Modified on: May 15, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryRequestResponseLog")
public interface RepositoryRequestResponseLog extends JpaRepository<RequestResponseLog, Integer> {

	/**
	 * @param responseJson
	 * @param updated
	 * @param requestId
	 */
	@Modifying(clearAutomatically = true)
	@Query("update RequestResponseLog rrl set rrl.responseJson = :responseJson, rrl.updated=:updated where rrl.requestId = :requestId")
	long logResponse(@Param("responseJson") String responseJson,
			@Param("updated") Date updated, @Param("requestId") String requestId);


}
