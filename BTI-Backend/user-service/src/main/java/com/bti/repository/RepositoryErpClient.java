/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.ErpClient;
import com.bti.model.UserSession;

/**
 * Description: Interface for RepositoryFields 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryErpClient")
public interface RepositoryErpClient extends JpaRepository<ErpClient, Integer> {


	public ErpClient findByIdAndIsDeleted(int id, boolean deleted);
	
	public ErpClient findByClientIdAndIsDeleted(String clientId, boolean deleted);

	public List<ErpClient> findByIsDeleted(Boolean deleted, Pageable pageable);

}
