package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.EjbiErpUserRelation;

@Repository("repositoryEjbiErpUserRelation")
public interface RepositoryEjbiErpUserRelation extends JpaRepository<EjbiErpUserRelation, Integer> {

	public EjbiErpUserRelation findByUserUserIdAndCompanyCompanyIdAndReportMasterIdAndLanguageLanguageIdAndIsDeleted(int userId, int companyId, int reportId, int langId, boolean isDeleted);
	
}
