package com.bti.repository;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bti.constant.Constant;
import com.bti.model.dto.DtoWorkflowUserDetails;
import com.bti.util.CommonUtils;

@Repository("repositoryEntityManager")
public class RepositoryEntityManager {

	@Autowired
	EntityManager entityManager;
	
	private String getEmployeeDetailSelectQuery(String tenantId) {
		String LINE_FEED = "\r\n";
		
		return "select emp.employindx as '" + Constant.REM_EMPLOYEE_INDEX + "'," +
				"  			emp.employid as '" + Constant.REM_EMPLOYEE_USER_ID + "', " + LINE_FEED +
				
				"			emp.emptitl as '" + Constant.REM_EMPLOYEE_TITLE + "', " + LINE_FEED + 
				"			emp.empfrtnm as '" + Constant.REM_EMPLOYEE_FIRSTNAME + "',  " + LINE_FEED +
				"			emp.emplstnm as '" + Constant.REM_EMPLOYEE_LASTNAME + "', " + LINE_FEED +
				
				"			emp.emptitla as '" + Constant.REM_EMPLOYEE_TITLE_AR + "',  " + LINE_FEED + 
				"			emp.empfrtnma as '" + Constant.REM_EMPLOYEE_FIRSTNAME_AR + "',  " + LINE_FEED +
				"			emp.emplstnma as '" + Constant.REM_EMPLOYEE_LASTNAME_AR + "',  " + LINE_FEED +
				
				"			pos.potdscr as '" + Constant.REM_EMPLOYEE_JOB_TITLE + "',  " + LINE_FEED +
				"			pos.potdscra as '" + Constant.REM_EMPLOYEE_JOB_TITLE_AR + "',  " + LINE_FEED + 
//				"			(select hr.employid from " + tenantId +".hr00101 hr where hr.employindx = emp.suprindx) as '" + Constant.REM_EMPLOYEE_MANAGER_INDEX + "', " + LINE_FEED + 
				"			(select employid from  " + tenantId + ".hr00101  where employindx = (select employee_id from " + tenantId + ".hr40105  where suprindx = (select suprindx from " + tenantId + ".hr00101  where employid = emp.employid))) as '" + Constant.REM_EMPLOYEE_MANAGER_INDEX + "', " + LINE_FEED + 
				"			emp.empactiv as '" + Constant.REM_EMPLOYEE_DISALBED_FLAG + "',  " + LINE_FEED + 
				"			emp.empuserid as '" + Constant.REM_EMPLOYEE_ERP_USER_INDEX + "',  " + LINE_FEED +
				"			emp.empimg as '" + Constant.REM_EMPLOYEE_IMAGE + "',  " + LINE_FEED +
				"			emp.wrkflwpass as '" + Constant.REM_EMPLOYEE_PASSWORD + "'  " + LINE_FEED +
				"from " + tenantId +".hr00101 emp \r\n" + 
				"left outer join " + tenantId +".hr40103 pos on emp.potindx = pos.potindx\r\n";
	}
 
	public Object[] getWorkflowUserDetailsByUserHcmId(String tenantId, Integer userHcmId) {
		Object[] result = null;

		String query = getEmployeeDetailSelectQuery(tenantId) + " where emp.employindx = ?"; 
		Query q = entityManager.createNativeQuery(query); 
		q.setParameter(1, userHcmId);

		List<Object[]> list =  q.getResultList();
		if (list != null && !list.isEmpty()) {
			result = list.get(0);
		}
		
		return result;
	}
	
	public Object[] getWorkflowUserDetailsByUserName(String tenantId, String userName) {
		Object[] result = null;

		String query = getEmployeeDetailSelectQuery(tenantId) + " where emp.employid = ?"; 
		Query q = entityManager.createNativeQuery(query); 
		q.setParameter(1, userName);

		List<Object[]> list =  q.getResultList();
		if (list != null && !list.isEmpty()) {
			result = list.get(0);
		}
		
		return result;
	}

	public Object[] getWorkflowUserDetailsByErpUserId(String tenantId, Integer erpUserId) {
		Object[] result = null;

		String query = getEmployeeDetailSelectQuery(tenantId) + " where emp.empuserid = ?"; 
		Query q = entityManager.createNativeQuery(query); 
		q.setParameter(1, erpUserId);

		List<Object[]> list =  q.getResultList();
		if (list != null && !list.isEmpty()) {
			result = list.get(0);
		}
		
		return result;
	}
}
