/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.UserGroupRoleGroup;

/**
 * Description: Interface for RepositoryUserGroupRoleGroup 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryUserGroupRoleGroup")
public interface RepositoryUserGroupRoleGroup extends JpaRepository<UserGroupRoleGroup, Integer> {

	/**
	 * @param userGroupId
	 * @param deleted
	 * @return
	 */
	public List<UserGroupRoleGroup> findByUserGroupUserGroupIdAndIsDeleted(int userGroupId, boolean deleted);

	/**
	 * @param id
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Query("update UserGroupRoleGroup ugrg set ugrg.isDeleted = true, ugrg.updatedBy=:updateById where ugrg.userGroup.userGroupId= :id")
	void deleteAllUserGroupRoleGroups(@Param("id") Integer id, @Param("updateById") Integer updateById);

	/**
	 * @param userGroupId
	 * @param roleGroupId
	 * @return
	 */
	public UserGroupRoleGroup findByUserGroupUserGroupIdAndRoleGroupRoleGroupId(int userGroupId, int roleGroupId);

	/**
	 * @param idList
	 * @param deleted
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Query("update UserGroupRoleGroup ugrg set ugrg.isDeleted =:deleted, ugrg.updatedBy=:updateById where ugrg.userGroup.userGroupId IN (:idList)")
	public void deleteAllRoleGroupUserGroupForMultipleUserGroups(@Param("idList") List<Integer> idList,
			@Param("deleted") boolean deleted, @Param("updateById") Integer updateById);

	/**
	 * @param idList
	 * @param deleted
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Query("update UserGroupRoleGroup ugrg set ugrg.isDeleted =:deleted, ugrg.updatedBy=:updateById where ugrg.roleGroup.roleGroupId IN (:idList)")
	public void deleteAllRoleGroupUserGroupForMultipleRoleGroups(@Param("idList") List<Integer> idList,
			@Param("deleted") boolean deleted, @Param("updateById") Integer updateById);

	/**
	 * @param idList
	 * @return
	 */
	@Query("select ugrl from UserGroupRoleGroup ugrl where ugrl.isDeleted=false and ugrl.roleGroup.isDeleted=false and ugrl.userGroup.userGroupId IN(:idList)")
	public List<UserGroupRoleGroup> getRoleGroupsForCompany(@Param("idList") List<Integer> idList);

	/**
	 * @param idList
	 * @return
	 */
	@Query("select count( DISTINCT ugrl.roleGroup.roleGroupId ) from UserGroupRoleGroup ugrl where ugrl.isDeleted=false and ugrl.roleGroup.isDeleted=false and ugrl.userGroup.userGroupId IN(:idList)")
	public Integer getRoleGroupCountForCompany(@Param("idList") List<Integer> idList);

}
