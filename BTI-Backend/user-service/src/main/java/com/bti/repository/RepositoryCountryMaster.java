/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.CountryMaster;

/**
 * Description: Interface for RepositoryCountryMaster 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCountryMaster")
public interface RepositoryCountryMaster extends JpaRepository<CountryMaster, Integer> {

	/**
	 * @param deleted
	 * @param isActive
	 * @return
	 */
	List<CountryMaster> findByIsDeletedAndIsActiveAndLanguageLanguageId(boolean deleted, boolean isActive, int langId);

	/**
	 * @param countryId
	 * @param deleted
	 * @param isActive
	 * @return
	 */
	CountryMaster findByCountryIdAndIsDeletedAndIsActive(int countryId, boolean deleted, boolean isActive);

	List<CountryMaster> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	CountryMaster findByCountryIdAndIsDeleted(int countryId, boolean b);
	
	CountryMaster findByshortNameAndIsDeletedAndLanguageLanguageId(String shortName,boolean b, int i);

	Long countByIsDeletedAndLanguageLanguageId(boolean b, int i);
}


