/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.jboss.logging.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.model.CommonConstant;

/**
 * Description: Interface for CommonConstant 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCommonConstant")
public interface RepositoryCommonConstant extends JpaRepository<CommonConstant, Integer> {

	/**
	 * @param deleted
	 * @return
	 */
	public List<CommonConstant> findByIsDeletedAndLanguageLanguageId(Boolean deleted,Integer langId);
	
	public CommonConstant findByConstantShortAndIsDeletedAndLanguageLanguageId(String constantShort,Boolean deleted,Integer langId);

	Long countByIsDeletedAndLanguageLanguageId(Boolean deleted,Integer langId);
}
