/**
	 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.EmailTemplate;


/**
 * Description: Interface for RepositoryEmailTemplates 
 * Name of Project: BTI
 * Created on: May 12, 2017
 * Modified on: May 12, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */

@Repository("repositoryEmailTemplates")
public interface RepositoryEmailTemplates extends JpaRepository<EmailTemplate, Integer> {

	/**
	 * @param templateName
	 * @return
	 */
	public EmailTemplate findByTemplateName(String templateName);

}