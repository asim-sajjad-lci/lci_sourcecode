/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.UserSession;


 /**
 * Description: Interface for RepositoryUserSession
 * Name of Project: BTI
 * Created on: May 12, 2017
 * Modified on: May 12, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryUserSession")
public interface RepositoryUserSession extends JpaRepository<UserSession, Integer> {

	/**
	 * @param userId
	 * @param deleted
	 * @return
	 */
	public UserSession findByUserUserIdAndIsDeleted(int userId, boolean deleted);

	/**
	 * @param userId
	 * @param session
	 * @param deleted
	 * @return
	 */
	public UserSession findByUserUserIdAndSessionAndIsDeleted(int userId, String session, boolean deleted);

	/**
	 * @param session
	 * @param deleted
	 * @return
	 */
	public UserSession findBySessionAndIsDeleted(String session, boolean deleted);
	
	List<UserSession> findByUserRoleRoleId(int roleId);
	
	List<UserSession> findByUserRoleRoleId(int roleId,Pageable pageable);
   
}
