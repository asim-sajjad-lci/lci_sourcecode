/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bti.model.Language;

/**
 * Description: Interface for RepositoryLanguage 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryLanguage")
public interface RepositoryLanguage extends JpaRepository<Language, Integer> {
	
	@Query("select count(*) from Language")
	int getTotalCount();
	
	public Language findByLanguageIdAndIsDeleted(int langId, boolean deleted);
	
	List<Language> findByIsDeletedAndIsActive(Boolean deleted,Boolean isActive,Pageable pageable);
	
	List<Language> findByIsDeletedAndIsActive(Boolean deleted,Boolean isActive);
	
	List<Language> findByIsDeleted(Boolean deleted);
	List<Language> findByIsDeleted(Boolean deleted,Pageable pageable);

	Language findByLanguageNameAndIsDeleted(String languageName, boolean b);
}