/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.AccessRoleTransactionRelation;

/**
 * Description: Interface for AccessRoleModuleRelation 
 * Name of Project: BTI
 * Created on: June 14, 2017
 * Modified on: June 14, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryAccessRoleTransactionRelation")
public interface RepositoryAccessRoleTransactionRelation extends JpaRepository<AccessRoleTransactionRelation, Integer> {
	/**
	 * @param accessRoleId
	 * @param moduleId
	 * @param transactionDetailId
	 * @param transactionTypeId
	 * @return
	 */
	AccessRoleTransactionRelation findByAccessRoleModuleRelationAccessRoleAccessRoleIdAndAccessRoleModuleRelationModuleModuleIdAndTransactionsTransactionIdAndTransactionTypeTransactionTypeId(
			int accessRoleId, int moduleId, int transactionDetailId, int transactionTypeId);

	/**
	 * @param transactionDetailId
	 * @param viewAccess
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<AccessRoleTransactionRelation> findByTransactionsTransactionIdAndViewAccessAndIsDeleted(
			int transactionDetailId, boolean viewAccess, boolean deleted, Pageable pageable);

	/**
	 * @param transactionDetailId
	 * @param postAccess
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<AccessRoleTransactionRelation> findByTransactionsTransactionIdAndPostAccessAndIsDeleted(
			int transactionDetailId, boolean postAccess, boolean deleted, Pageable pageable);

	/**
	 * @param transactionDetailId
	 * @param deleteAccess
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<AccessRoleTransactionRelation> findByTransactionsTransactionIdAndDeleteAccessAndIsDeleted(
			int transactionDetailId, boolean deleteAccess, boolean deleted, Pageable pageable);

	/**
	 * @param moduleId
	 * @param accessRoleId
	 * @param transactionId
	 * @param transactionTypeId
	 * @param deleted
	 * @return
	 */
	AccessRoleTransactionRelation findByAccessRoleModuleRelationModuleModuleIdAndAccessRoleModuleRelationAccessRoleAccessRoleIdAndTransactionsTransactionIdAndTransactionTypeTransactionTypeIdAndIsDeleted(
			int moduleId, int accessRoleId, int transactionId, int transactionTypeId, boolean deleted);
	
	AccessRoleTransactionRelation findByAccessRoleModuleRelationAccessRoleAccessRoleIdAndAccessRoleModuleRelationModuleModuleIdAndTransactionsTransactionIdIsNullAndTransactionTypeTransactionTypeIdIsNull(
			int accessRoleId, int moduleId);
	
	AccessRoleTransactionRelation findByAccessRoleModuleRelationAccessRoleAccessRoleIdAndAccessRoleModuleRelationModuleModuleIdAndTransactionTypeTransactionTypeIdAndTransactionsTransactionIdIsNull(
			int accessRoleId, int moduleId,int transactionTypeId);
	
}
