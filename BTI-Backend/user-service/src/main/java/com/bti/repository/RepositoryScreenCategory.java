package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.ScreenCategory;

/**
 * Description: Interface for RepositoryScreenCategory
 * Name of Project: BTI
 * Created on: July 23, 2018
 * Modified on: July 23, 2018 4:19:38 PM
 * @author Elegant
 * Version: 1.0
 */
@Repository("repositoryScreenCategory")
public interface RepositoryScreenCategory extends JpaRepository<ScreenCategory, Integer> {
	
	List<ScreenCategory> findByIsDeleted(boolean isDeleted);
	
	List<ScreenCategory> findByModuleModuleIdAndIsDeleted(Integer moduleId, boolean isDeleted);
}
