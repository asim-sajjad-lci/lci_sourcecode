package com.bti.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.CompanyReport;

@Repository("repositoryCompanyReport")
public interface RepositoryCompanyReport extends JpaRepository<CompanyReport, Integer>{
	
	public CompanyReport findByIdAndIsDeleted(int id, boolean deleted);

	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update CompanyReport d set d.isDeleted =:deleted ,d.updatedBy =:updateById where d.id =:id ")
	public void deleteSingleCompanyReport(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value="delete from company_report where companyreport_id:companyreport_id",nativeQuery=true)
	public void deleteReportMasterList(@Param("companyreport_id") Integer companyreport_id);
}
