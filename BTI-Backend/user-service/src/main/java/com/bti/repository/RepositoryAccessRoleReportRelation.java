/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.AccessRoleReportRelation;

/**
 * Description: Interface for AccessRoleReportRelation 
 * Name of Project: BTI
 * Created on: June 14, 2017
 * Modified on: June 14, 2017 11:19:38 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryAccessRoleReportRelation")
public interface RepositoryAccessRoleReportRelation extends JpaRepository<AccessRoleReportRelation, Integer> {
	/**
	 * @param moduleId
	 * @param accessRoleId
	 * @param reportDetailId
	 * @param reportCategoryId
	 * @return
	 */
	AccessRoleReportRelation findByAccessRoleModuleRelationModuleModuleIdAndAccessRoleModuleRelationAccessRoleAccessRoleIdAndReportsReportIdAndReportCategoryReportCategoryId(
			int moduleId, int accessRoleId, int reportDetailId, int reportCategoryId);

	/**
	 * @param reportDetailId
	 * @param viewAccess
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<AccessRoleReportRelation> findByReportsReportIdAndViewAccessAndIsDeleted(int reportDetailId,
			boolean viewAccess, boolean deleted, Pageable pageable);

	/**
	 * @param reportDetailId
	 * @param emailAccess
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<AccessRoleReportRelation> findByReportsReportIdAndEmailAccessAndIsDeleted(int reportDetailId,
			boolean emailAccess, boolean deleted, Pageable pageable);

	/**
	 * @param reportDetailId
	 * @param exportAccess
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<AccessRoleReportRelation> findByReportsReportIdAndExportAccessAndIsDeleted(int reportDetailId,
			boolean exportAccess, boolean deleted, Pageable pageable);

	/**
	 * @param moduleId
	 * @param reportDetailId
	 * @param reportCategoryId
	 * @param deleted
	 * @return
	 */
	AccessRoleReportRelation findByAccessRoleModuleRelationModuleModuleIdAndReportsReportIdAndReportCategoryReportCategoryIdAndIsDeleted(
			int moduleId, int reportDetailId, int reportCategoryId, boolean deleted);

	/**
	 * @param moduleId
	 * @param accessRoleId
	 * @param reportDetailId
	 * @param reportCategoryId
	 * @param deleted
	 * @return
	 */
	AccessRoleReportRelation findByAccessRoleModuleRelationModuleModuleIdAndAccessRoleModuleRelationAccessRoleAccessRoleIdAndReportsReportIdAndReportCategoryReportCategoryIdAndIsDeleted(
			int moduleId, int accessRoleId, int reportDetailId, int reportCategoryId, boolean deleted);
	
	
	AccessRoleReportRelation findByAccessRoleModuleRelationModuleModuleIdAndAccessRoleModuleRelationAccessRoleAccessRoleIdAndReportsReportIdIsNullAndReportCategoryReportCategoryIdIsNull(
			int moduleId, int accessRoleId);
	
	AccessRoleReportRelation findByAccessRoleModuleRelationModuleModuleIdAndAccessRoleModuleRelationAccessRoleAccessRoleIdAndReportCategoryReportCategoryIdAndReportsReportIdIsNull(
			int moduleId, int accessRoleId, int reportCategoryId);
	
}
