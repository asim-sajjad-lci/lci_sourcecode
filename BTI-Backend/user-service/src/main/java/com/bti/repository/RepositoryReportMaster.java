package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.Company;
import com.bti.model.ReportMaster;
import com.bti.model.User;

@Repository("repositoryReportMaster")
public interface RepositoryReportMaster extends JpaRepository<ReportMaster, Integer>{

	/**
	 * 
	 * @param id
	 * @param deleted
	 * @return
	 */
	public ReportMaster findByIdAndIsDeleted(int id, boolean deleted);
	
	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<ReportMaster> findByIsDeleted(Boolean deleted);
	
	/**
	 * 
	 * @return
	 */
	@Query("select count(*) from ReportMaster r where r.isDeleted=false")
	public Integer getCountOfTotalReportMaster();
	
	/**
	 * 
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	public List<ReportMaster> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	/**
	 * 
	 * @param deleted
	 * @param idList
	 * @param updateById
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ReportMaster r set r.isDeleted =:deleted, r.updatedBy=:updateById where r.id IN (:idList)")
	public void deleteReportMaster(@Param("deleted") Boolean deleted, @Param("idList") List<Integer> idList,
			@Param("updateById") Integer updateById);
	
	/**
	 * 
	 * @param deleted
	 * @param updateById
	 * @param id
	 */
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update ReportMaster r set r.isDeleted =:deleted ,r.updatedBy =:updateById where r.id =:id ")
	public void deleteSingleReportMaster(@Param("deleted") Boolean deleted, @Param("updateById") Integer updateById,
			@Param("id") Integer id);

	/**
	 * 
	 * @return
	 */
	public ReportMaster findTop1ByOrderByIdDesc();
	
	/**
	 * 
	 * @param searchKeyWord
	 * @param pageable
	 * @return
	 */
	@Query("select r from ReportMaster r where (r.reportLink like :searchKeyWord  or r.reportName like :searchKeyWord or r.reportDescription like :searchKeyWord) and r.isDeleted=false")
	public List<ReportMaster> predictiveReportMasterSearchWithPagination(@Param("searchKeyWord") String searchKeyWord,
			Pageable pageable);

	/**
	 * 
	 * @param deleted
	 * @return
	 */
	public List<ReportMaster> findByIsDeletedOrderByCreatedDateDesc(Boolean deleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from ReportMaster r where (r.reportLink like :searchKeyWord  or r.reportName like :searchKeyWord or r.reportDescription like :searchKeyWord) and r.isDeleted=false")
	public Integer predictiveReportMasterSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);

	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query("select count(*) from ReportMaster r where (r.reportLink like :searchKeyWord  or r.reportName like :searchKeyWord or r.reportDescription like :searchKeyWord) and r.isDeleted=false")
	public List<ReportMaster> predictiveReportMasterSearchWithPagination(@Param("searchKeyWord") String searchKeyWord);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@Query("select r from ReportMaster r where (r.id =:id) and r.isDeleted=false")
	public List<ReportMaster> findById(@Param("id")Integer id);
	
	@Query("select r from ReportMaster r where (r.id =:id) and r.isDeleted=false")
	public List<ReportMaster> findById(@Param("id")List<Integer> id);
	
	@Query("select r from ReportMaster r where r.id IN(:ids) and r.isDeleted=false")
	public List<ReportMaster> findBySelectedReportId(@Param("ids") List<Integer> ids);

	/**
	 * @return
	 */
	@Query("select c from Company c where c.reportMaster IS NOT EMPTY and c.isDeleted=false ")
	public List<Company> findAllCompanyWiseReports();

//	/**
//	 * @return
//	 */
//	@Query("select u from User u where u.reportMaster IS NOT EMPTY and u.isDeleted=false ")
//	public List<User> findAllUserWiseReports(Pageable pageable);

	/**
	 * @return
	 */
	@Query("select u from User u where u.reportMaster IS NOT EMPTY and u.isDeleted=false ")
	public List<User> findAllUserWiseReports();

	/**
	 * 
	 * @param departmentId
	 * @return
	 */
	@Query("select p from ReportMaster p where (p.id =:id) and p.isDeleted=false")
	public List<ReportMaster> findByReportId(@Param("id")Integer reportId);

	/**
	 * @param userId
	 * @param companyId
	 * @param isDeleted
	 * @return
	 */
	@Query(value = "select * from user_company_relation where user_company_relation.user_id =:userId and user_company_relation.company_id in"
			+ "(select * from company_report_master where company_report_master.company_id =:companyId) and is_deleted =:isDeleted", nativeQuery = true)
	List<ReportMaster> findReportsByUserIdAndCompanyIdAndIsDeleted(@Param("userId") int userId,
			@Param("companyId") int companyId, @Param("isDeleted") Boolean isDeleted);
	
	/**
	 * 
	 * @param searchKeyWord
	 * @return
	 */
	@Query(value = "select count(*) from user_company_relation uc where (uc.reportId like :searchKeyWord  or uc.reportLink like :searchKeyWord  or uc.reportName like :searchKeyWord or uc.reportDescription like :searchKeyWord) and uc.isDeleted=false", nativeQuery = true)
	public Integer predictiveUserReportSearchTotalCount(@Param("searchKeyWord") String searchKeyWord);
	
}
