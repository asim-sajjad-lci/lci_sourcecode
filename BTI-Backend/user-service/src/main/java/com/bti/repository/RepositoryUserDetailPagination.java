/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.UserDetail;


/**
 * Description: Interface for RepositoryUserDetailPagination 
 * Name of Project: BTI
 * Created on: May 12, 2017
 * Modified on: May 12, 2017 10:19:22 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryUserDetailPagination")
public interface RepositoryUserDetailPagination extends PagingAndSortingRepository<UserDetail, Long> {
	/**
	 * @param deleted
	 * @param pageRequest
	 * @return
	 */
	Page<UserDetail> findByIsDeleted(Boolean deleted, final Pageable pageRequest);
}