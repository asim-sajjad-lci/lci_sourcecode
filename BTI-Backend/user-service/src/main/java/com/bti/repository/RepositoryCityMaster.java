/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.CityMaster;

/**
 * Description: Interface for RepositoryCityMaster 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryCityMaster")
public interface RepositoryCityMaster extends JpaRepository<CityMaster, Integer> {

	/**
	 * @param stateId
	 * @param deleted
	 * @return
	 */
	List<CityMaster> findByStateMasterStateIdAndIsDeletedAndLanguageLanguageId(int stateId, boolean deleted, int langId);

	List<CityMaster> findByIsDeletedAndLanguageLanguageId(boolean b, int i);
	
	CityMaster findByCityCodeAndIsDeletedAndLanguageLanguageId(String cityCode,boolean b, int i);
	
	Long countByIsDeletedAndLanguageLanguageId(boolean b, int i);
}


