/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.Module;

/**
 * Description: Interface for RepositoryModule 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryModule")
public interface RepositoryModule extends JpaRepository<Module, Integer> {

	/**
	 * @param moduleCode
	 * @param deleted
	 * @return
	 */
	public Module findByModuleCodeAndIsDeleted(String moduleCode, Boolean deleted);

	/**
	 * @param deleted
	 * @return
	 */
	List<Module> findByIsDeleted(Boolean deleted);

	/**
	 * @param deleted
	 * @param pageable
	 * @return
	 */
	List<Module> findByIsDeleted(Boolean deleted, Pageable pageable);
	
	List<Module> findByIsDeletedAndIsActive(Boolean deleted,Boolean isActive);
	
	List<Module> findByIsDeletedAndIsActive(Boolean deleted,Boolean isActive,Pageable pageable);
	
	List<Module> findByIsDeletedAndIsActiveAndLanguageLanguageId(Boolean deleted,Boolean isActive, int langId, Pageable pageable);
	
	List<Module> findByIsDeletedAndIsActiveAndLanguageLanguageId(Boolean deleted,Boolean isActive,int languageId);
	
	public abstract Module findByModuleIdAndIsDeletedAndIsActive(Integer paramInteger, Boolean deleted, Boolean isActive);
	
	public Module findByModuleCodeAndIsDeletedAndLanguageLanguageId(String moduleCode, Boolean deleted,int langId);
	
	public Module findByModuleCodeAndIsDeletedAndIsActiveAndLanguageLanguageId(String moduleCode, Boolean deleted, Boolean isActive,int languageId);

	public List<Module> findByIsDeletedAndLanguageLanguageId(boolean b, int i);

	public Module findByModuleIdAndIsDeleted(int parseInt, boolean b);

	public Long countByIsDeletedAndLanguageLanguageId(boolean b, int i);

}
