/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.AppConfigSetting;

/**
 * Description: Interface for RepositoryAppConfigSetting 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryAppConfigSetting")
public interface RepositoryAppConfigSetting extends JpaRepository<AppConfigSetting, Integer> {

	/**
	 * @param appConfigName
	 * @param deleted
	 * @return
	 */
	AppConfigSetting findByConfigNameAndIsDeleted(String appConfigName, Boolean deleted);

}
