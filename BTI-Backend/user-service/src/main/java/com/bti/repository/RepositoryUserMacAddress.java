/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.UserMacAddress;

/**
 * Description: Interface for RepositoryUserIp 
 * Name of Project: BTI
 * Created on: May 30, 2017
 * Modified on: May 30, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryUserMacAddress")
public interface RepositoryUserMacAddress extends JpaRepository<UserMacAddress, Integer> {
	
	public List<UserMacAddress> findByIsDeleted(Boolean isDeleted);
	
	public List<UserMacAddress> findByMacAddressAndUserUserIdAndIsActiveAndIsDeleted(String macAddress, int userId,
			Boolean isActive, Boolean isDeleted);
	
	public List<UserMacAddress> findByMacAddressAndIsActiveAndIsDeleted(String macAddress, Boolean isActive, Boolean isDeleted);
	
	public UserMacAddress findByMacAddressAndUserUserId(String macAddress, int userId);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update UserMacAddress u set u.isDeleted =:deleted, u.updatedBy=:updateById where u.user.userId=:userId")
	public void deleteUserMacAddressByUserId(@Param("deleted") Boolean deleted, @Param("userId") Integer userId,
			@Param("updateById") Integer updateById);
	
}
