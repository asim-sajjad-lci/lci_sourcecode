/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.FieldValidation;

/**
 * Description: Interface for RepositoryFieldValidation 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryFieldValidation")
public interface RepositoryFieldValidation extends JpaRepository<FieldValidation, Integer> {

	/**
	 * @param fieldId
	 * @param deleted
	 * @return
	 */
	List<FieldValidation> findByFieldFieldIdAndIsDeleted(Integer fieldId, Boolean deleted);
	
	List<FieldValidation> findByFieldFieldCodeAndIsDeleted(String fieldCode, Boolean deleted);

}
