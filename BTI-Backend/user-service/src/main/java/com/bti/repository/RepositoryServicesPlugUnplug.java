/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.ServicesPlugUnplug;

/**
 * Description: Interface for Services Plug Unplug 
 * Name of Project: BTI
 * Created on: July 21, 2017
 * Modified on: July 21, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryServicesPlugUnplug")
public interface RepositoryServicesPlugUnplug extends JpaRepository<ServicesPlugUnplug, Integer> {
	
	/**
	 * @param deleted
	 * @return
	 */
	List<ServicesPlugUnplug> findByIsDeleted(Boolean deleted);
	/**
	 * @param id
	 * @param deleted
	 * @return
	 */
	ServicesPlugUnplug findByIdAndIsDeleted(int id, Boolean deleted);
}
