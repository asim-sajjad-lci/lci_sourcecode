/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.FieldCustomization;

@Repository("repositoryFieldCustomization")
public interface RepositoryFieldCustomization extends JpaRepository<FieldCustomization, Integer> {

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update FieldCustomization f set f.isDeleted =:deleted, f.updatedBy =:updatedById where f.id =:id ")
	public void deleteSingleFieldCustomization(@Param("deleted") Boolean deleted,
			@Param("updatedById") Integer updatedById, @Param("id") int id);

	/**
	 * @param name
	 * @param deleted
	 * @return
	 */
	@Query("select f from FieldCustomization f where f.code=:code and f.userId=:userId")
	public FieldCustomization findByCodeAndUser(@Param("code") String code, @Param("userId") int userId);

}
