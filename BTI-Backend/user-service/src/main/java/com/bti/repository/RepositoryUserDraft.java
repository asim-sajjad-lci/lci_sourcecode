/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bti.model.AccessRole;
import com.bti.model.UserDraft;

/**
 * Description: Interface for UserDraft 
 * Name of Project: BTI
 * Created on: April 16, 2018
 * Modified on: April 16, 2018 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryUserDraft")
public interface RepositoryUserDraft extends JpaRepository<UserDraft, Integer> 
{
	List<UserDraft> findByUserUserId(int userId);
	
	List<UserDraft> findByUserUserIdAndCompanyCompanyId(int userId,int companyId);
	
	@Query("select u from UserDraft u where u.user.userId !=:userId and u.screen.screenCode=:screenCode and u.company is null")
	UserDraft checkScreenIsBlockByAnotherUserOrNot(@Param("screenCode") String screenCode, 
			@Param("userId") int userId);
	
	@Query("select u from UserDraft u where u.user.userId !=:userId and u.screen.screenCode=:screenCode and u.company.companyId=:companyId")
	UserDraft checkScreenIsBlockByAnotherUserOrNotByCompany(@Param("screenCode") String screenCode, 
			@Param("userId") int userId,@Param("companyId") int companyId);
	
}
