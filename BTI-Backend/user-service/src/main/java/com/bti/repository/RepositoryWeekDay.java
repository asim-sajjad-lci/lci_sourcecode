/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.WeekDay;

/**
 * Description: Interface for RepositoryWeekDay 
 * Name of Project: BTI
 * Created on: May 09, 2017
 * Modified on: May 09, 2017 4:19:38 PM
 * @author seasia
 * Version: 
 */
@Repository("repositoryWeekDay")
public interface RepositoryWeekDay extends JpaRepository<WeekDay, Integer> {

	List<WeekDay> findByLanguageLanguageId(int langId);

	List<WeekDay> findByIsDeletedAndLanguageLanguageId(boolean b, int i);
	WeekDay findByDayCodeAndIsDeletedAndLanguageLanguageId(String dayCode,boolean b, int i);
	Long countByIsDeletedAndLanguageLanguageId(boolean b, int i);
}
