/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bti.model.LoginOtp;


/**
 * Description: Interface for RepositoryLoginOtp
 * Name of Project: BTI
 * Created on: May 12, 2017
 * Modified on: May 12, 2017 3:11:18 AM
 * @author seasia
 * Version: 
 */

@Repository("repositoryLoginOtp")
public interface RepositoryLoginOtp extends JpaRepository<LoginOtp,Integer> {
	
	/**
	 * @param code
	 * @param userId
	 * @param deleted
	 * @return
	 */
	public LoginOtp findByCodeAndUserUserIdAndIsDeleted(String code, int userId,boolean deleted);
	
	/**
	 * @param userId
	 * @param deleted
	 * @return
	 */
	public LoginOtp findByUserUserIdAndIsDeleted(int userId,boolean deleted);

	/**
	 * @param userId
	 * @return
	 */
	public LoginOtp findByUserUserId(int userId);

}