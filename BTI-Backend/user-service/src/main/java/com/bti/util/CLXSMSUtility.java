/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.util;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Description: Utility class sending out SMS  
 * Name of Project: BTI APP Created on: May 16,
 * 2017 Modified on: May 16, 2017 10:19:38 AM
 * 
 * @author seasia Version:
 */

@SuppressWarnings("deprecation")
@Component
public class CLXSMSUtility {

	private static final Logger LOGGER = Logger.getLogger(CLXSMSUtility.class);
	
	/**
	 * @param from
	 * @param messageBody
	 * @param numberList
	 */
	@Async
	public void sendSMS(String from, String messageBody, List<String> numberList){
		
		@SuppressWarnings("resource")
		HttpClient httpClient = new DefaultHttpClient();
        try {
            HttpPost request = new HttpPost("https://api.clxcommunications.com/xms/v1/btitd12/batches");
            JSONObject messageJSON = new JSONObject();
            messageJSON.put("from", from);	// from
            messageJSON.put("body", messageBody);	// message body
            messageJSON.put("to", new JSONArray(numberList));	//cell numbers 
            StringEntity requestEntity = new StringEntity(messageJSON.toString(), HTTP.UTF_8);
            request.addHeader("Content-Type", "application/json;charset=utf-8");
            request.addHeader("Authorization", "Bearer 3f14b1bb396746aab9ed9cf7fd58ef84");
            request.setEntity(requestEntity);
            HttpResponse response = httpClient.execute(request);
            String responseBody = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            LOGGER.info("Response Body --->>"+responseBody);
        }catch (Exception ex) {
        	LOGGER.info("Error occured due to ::"+ex.getMessage());
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
	}
	

}
