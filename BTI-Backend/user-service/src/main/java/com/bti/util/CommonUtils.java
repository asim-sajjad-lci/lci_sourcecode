/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import ch.qos.logback.classic.Logger;

/**
 * Description: Round Decimal Utility for BTI 
 * Name of Project: BTI
 * Created on: May 11, 2017
 * Modified on: May 12, 2017 5:39:32 PM
 * @author SNS
 * Version: 
 */
public class CommonUtils {

	public static double parseDouble(String value)
	{
		double d = 0.0;
		try {
			if(value!=null && !value.isEmpty()){
				d = Double.parseDouble(value);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			d = 0.0;
		}
		return d;
	}

	public static Integer parseInteger(String value)
	{
		Integer i = 0;
		try {
			if(value!=null && !value.isEmpty()){
				i = Integer.parseInt(value);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			i = 0;
		}
		return i;
	}
	
	public static String removeNull(String str) {
		if (str == null) {
			return "";
		}
		return str;
	}

	public static Object removeNullObject(Object obj) {
		if (obj == null) {
			return new Object();
		}
		return obj;
	}
	
	
}
