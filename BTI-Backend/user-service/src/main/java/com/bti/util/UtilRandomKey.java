/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.util;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * Description: RandomKey Utility for BTI 
 * Name of Project: BTI
 * Created on: May 11, 2017
 * Modified on: May 12, 2017 5:39:32 PM
 * @author seasia
 * Version: 
 */
public class UtilRandomKey {

    /**
     * it return the 6 digit random numeric number
     * @return
     */
	public static String getRandomOrderNumber() {
		char[] chars = "1234567890".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 6; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();
	}

	private SecureRandom random = new SecureRandom();

	/**
	 * @return
	 */
	public String nextRandomKey() {
		return new BigInteger(60, random).toString(32);
	}
	
	/**
	 * it generates encrypted session key
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	private static String nextRandomSessionKey() throws NoSuchAlgorithmException {
		KeyGenerator gen = KeyGenerator.getInstance("DES");
    	gen.init(56); /* 56-bit DES */
		SecretKey secret = gen.generateKey();
		byte[] binary = secret.getEncoded();
		String text = String.format("%08X", new BigInteger(+1, binary));
		return text;
	}
	
    /**
     * it return session key string for logged used
     * @return
     */
	public static String generateSessionKey() {
		String key = null;
		try {
			key = nextRandomSessionKey();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return key;
	}

	public static boolean isNotBlank(final String s) {
    	  // Null-safe, short-circuit evaluation.
		return s != null && !s.trim().isEmpty();
	}

	public static boolean isNotNull(final Integer i) {
  	  // Null-safe, short-circuit evaluation.
		return i != null && i != 0;
	}

}
