/**
 * BTI - BAAN for Technology And Trade IntL. 
 * Copyright @ 2017 BTI. 
 * 
 * All rights reserved.
 * 
 * THIS PRODUCT CONTAINS CONFIDENTIAL INFORMATION  OF BTI. 
 * USE, DISCLOSURE OR REPRODUCTION IS PROHIBITED WITHOUT THE 
 * PRIOR EXPRESS WRITTEN PERMISSION OF BTI.
 */
package com.bti.util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bti.constant.BTICodeType;
import com.bti.model.AccessRole;
import com.bti.model.Company;
import com.bti.model.RoleGroup;
import com.bti.model.User;
import com.bti.model.UserGroup;
import com.bti.repository.RepositoryAccessRole;
import com.bti.repository.RepositoryCompany;
import com.bti.repository.RepositoryRoleGroup;
import com.bti.repository.RepositoryUser;
import com.bti.repository.RepositoryUserGroup;

/**
 * Description: Utility class for code generation  
 * Name of Project: BTI APP Created on: May 16,
 * 2017 Modified on: May 16, 2017 10:19:38 AM
 * 
 * @author seasia Version:
 */

@Component
public class CodeGenerator {

	@Autowired
	RepositoryUserGroup repositoryUserGroup;

	@Autowired
	RepositoryAccessRole repositoryAccessRole;

	@Autowired
	RepositoryRoleGroup repositoryRoleGroup;

	@Autowired
	RepositoryUser repositoryUser;

	@Autowired
	RepositoryCompany repositoryCompany;

	/**
	 * @param codeType
	 * @return
	 */
	public String getGeneratedCode(String codeType) {
		String generatedCode = null;
		int codeId = 1000;
		//User Group Code
		if (BTICodeType.USERGROUP.name().equalsIgnoreCase(codeType)) {
			UserGroup userGroup = repositoryUserGroup.findTop1ByOrderByUserGroupIdDesc();
			if (userGroup != null) {
				String[] codeList = userGroup.getGroupCode().split("-");
				codeId = Integer.parseInt(codeList[1]) + 1;
				generatedCode = "UG-" + codeId;
			} else {
				generatedCode = "UG-" + codeId;
			}
		}
		//Role Code
		if (BTICodeType.ROLE.name().equalsIgnoreCase(codeType)) {
			AccessRole applicationRole = repositoryAccessRole.findTop1ByOrderByAccessRoleIdDesc();
			if (applicationRole != null) {
				if (applicationRole.getRoleCode() != null && applicationRole.getRoleCode().contains("-")) {
					String[] codeList = applicationRole.getRoleCode().split("-");
					codeId = Integer.parseInt(codeList[1]) + 1;
					generatedCode = "R-" + codeId;
				} else {
					generatedCode = "R-" + codeId;
				}
			} else {
				generatedCode = "R-" + codeId;
			}
		}
		//Role Group Code
		if (BTICodeType.ROLEGROUP.name().equalsIgnoreCase(codeType)) {
			RoleGroup roleGroup = repositoryRoleGroup.findTop1ByOrderByRoleGroupIdDesc();
			if (roleGroup != null) {
				String[] codeList = roleGroup.getRoleGroupCode().split("-");
				codeId = Integer.parseInt(codeList[1]) + 1;
				generatedCode = "RG-" + codeId;
			} else {
				generatedCode = "RG-" + codeId;
			}
		}
				
		//Employee Id Code
		if (BTICodeType.EMPLOYEECODE.name().equalsIgnoreCase(codeType)) {
			User user = repositoryUser.findTop1ByOrderByUserIdDesc();
			if (user != null) {
				if (user.getEmployeeCode() != null && user.getEmployeeCode().contains("-")) {
					String[] codeList = user.getEmployeeCode().split("-");
					codeId = Integer.parseInt(codeList[1]) + 1;
					generatedCode = "E-" + codeId;
				} else {
					generatedCode = "E-" + codeId;
				}
			} else {
				generatedCode = "E-" + codeId;
			}
		}
		//Company Code
		if (BTICodeType.COMPANYCODE.name().equalsIgnoreCase(codeType)) {
			Company company = repositoryCompany.findTop1ByOrderByCompanyIdDesc();
			if (company != null) {
				if (company.getCompanyCode() != null && company.getCompanyCode().contains("_")) {
					String[] codeList = company.getCompanyCode().split("_");
					codeId = Integer.parseInt(codeList[1]) + 1;
					generatedCode = "C_" + codeId;
				} else {
					generatedCode = "C_" + codeId;
				}
			} else {
				generatedCode = "C_" + codeId;
			}
		}
		 
		return generatedCode;
	}
}
