/*package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerCompanyTest { 
	*//**
	 * Create company
	 * @throws Exception
	 *//*
	@Test
	public void testCreate() throws Exception {
		JSONObject json = new JSONObject();
		json.put("name", "Microsoft6");
		json.put("description", "IT company");
		json.put("address", "phase-9, chd");
		json.put("isActive", true);
		json.put("fax", "123456");
		json.put("phone", "1245789");
		json.put("email", "test@test.com");
		json.put("zipCode", "123456");
		json.put("countryId", "1");
		json.put("stateId", "1");
		json.put("cityId", "1");
		json.put("countryCode", "+91");
		json.put("webAddress", "www.abc.com");
		json.put("latitude", "70.123456789");
		json.put("longitude", "31.1234567000");

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/company/create").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get all company 
	 * @throws Exception
	 *//*
	@Test
	public void testGetAllWithPagination() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/company/getAll").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get all company 
	 * @throws Exception
	 *//*
	@Test
	public void testGetAllWithOutPagination() throws Exception {
		JSONObject json = new JSONObject();
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/company/getAll").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Delete company 
	 * @throws Exception
	 *//*
	@Test
	public void testDelete() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("ids", ids);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/company/delete").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Create company
	 * @throws Exception
	 *//*
	@Test
	public void testUpdate() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);
		json.put("name", "Microsoft6");
		json.put("description", "IT company");
		json.put("address", "phase-9, chd");
		json.put("isActive", true);
		json.put("fax", "123456");
		json.put("phone", "1245789");
		json.put("email", "test@test.com");
		json.put("zipCode", "123456");
		json.put("countryId", "1");
		json.put("stateId", "1");
		json.put("cityId", "1");
		json.put("countryCode", "+91");
		json.put("webAddress", "www.abc.com");
		json.put("latitude", "70.123456789");
		json.put("longitude", "31.1234567000");

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/company/update").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get company detail
	 * @throws Exception
	 *//*
	@Test
	public void testGetCompanyById() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/company/getCompanyById").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Search company with pagination
	 * @throws Exception
	 *//*
	@Test
	public void testSearchCompanyWithPagination() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageSize", 10);
		json.put("pageNumber", 0);
		json.put("searchKeyword", "");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/company/searchCompanies").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Search company without pagination
	 * @throws Exception
	 *//*
	@Test
	public void testSearchCompanyWithoutPagination() throws Exception {
		JSONObject json = new JSONObject();
	 
		json.put("searchKeyword", "");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/company/searchCompanies").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Company List For DropDown
	 * @throws Exception
	 *//*
	@Test
	public void testGetCompanyListForDropDown() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageSize", 10);
		json.put("pageNumber", 0);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/company/getCompanyListForDropDown").andReturn();
		assert response.statusCode() == 200;
	}
	

	*//**
	 * Block/Unblock Company 
	 * @throws Exception
	 *//*
	@Test
	public void testBlockUnblockCompany() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 5);
		json.put("isActive", true);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/company/blockUnblockCompany").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Company Stats
	 * @throws Exception
	 *//*
	@Test
	public void testGetCompanyStats() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 5);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/company/getCompanyStats").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Company List Count Of Users
	 * @throws Exception
	 *//*
	@Test
	public void testCompanyListCountOfUsers() throws Exception {
	 
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.get("http://localhost:8888/company/companyListCountOfUsers").andReturn();
		assert response.statusCode() == 200;
	}
}
*/