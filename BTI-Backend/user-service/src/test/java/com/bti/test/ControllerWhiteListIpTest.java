/*package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerWhiteListIpTest {
	*//**
	 *  Add White List IP 
	 * @throws Exception
	 *//*
	@Test
	public void testAddWhiteListIP() throws Exception {
		JSONObject json = new JSONObject();
		json.put("ipAddress", "10.8.11.100");
		json.put("description", "description");
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/ip/addWhiteListIP").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 *  Search White List IP 
	 * @throws Exception
	 *//*
	@Test
	public void testSearchWhiteListIPs() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		json.put(  "searchKeyword" , "10.");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/ip/searchWhiteListIPs").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get White List IP 
	 * @throws Exception
	 *//*
	@Test
	public void testGetWhiteListIPById() throws Exception {
		JSONObject json = new JSONObject();
		json.put("whitelistIpId" , 7);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/ip/getWhiteListIPById").andReturn();
		assert response.statusCode() == 200;
	}
	*//**
	 * Update White List IP 
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateWhiteListIP() throws Exception {
		JSONObject json = new JSONObject();
		json.put("whitelistIpId" , 7);
		json.put("description", "description");
		json.put("ipAddress", "10.8.11.100");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/ip/updateWhiteListIP").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update White List IP 
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteWhiteListIP() throws Exception {
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		JSONObject json = new JSONObject();
		json.put("deleteIds" , ids);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/ip/deleteWhiteListIP").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Block Unblock White List IP 
	 * @throws Exception
	 *//*
	@Test
	public void testBlockUnblockWhiteListIP() throws Exception {
		JSONObject json = new JSONObject();
		json.put("whitelistIpId" , 1);
		json.put("isActive" , false);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/ip/blockUnblockWhiteListIP").andReturn();
		assert response.statusCode() == 200;
	}
}
*/