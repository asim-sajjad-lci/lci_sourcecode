/*package com.bti.test;


import static com.jayway.restassured.RestAssured.given;

import org.junit.Test;
import com.jayway.restassured.response.Response;
import net.minidev.json.JSONObject;

public class ControllerHomeTest {

	*//**
	 * Get labels related to screen and module with required parameters
	 * Required parameters are moduleCode and screenCode
	 * @throws Exception
	 *//*
	@Test
	public void testScreenDetail() throws Exception {
		JSONObject json = new JSONObject();
		json.put("moduleCode", "M-1000");
		json.put("screenCode", "S-1000");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/screenDetail").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get country list
	 * @throws Exception
	 *//*
	@Test
	public void testGetCountryList() throws Exception {
		 
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.get("http://localhost:8888/getCountryList").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get State List By CountryId
	 * @throws Exception
	 *//*
	@Test
	public void testGetStateListByCountryId() throws Exception {
		JSONObject json = new JSONObject();
		json.put("countryId", 1);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/getStateListByCountryId").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Get city list by State Id
	 * @throws Exception
	 *//*
	@Test
	public void testGetCityListByStateId() throws Exception {
		JSONObject json = new JSONObject();
		json.put("stateId", 1);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/getCityListByStateId").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get country code By CountryId
	 * @throws Exception
	 *//*
	@Test
	public void testGetCountryCodeByCountryId() throws Exception {
		JSONObject json = new JSONObject();
		json.put("countryId", 1);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/getCountryCodeByCountryId").andReturn();
		assert response.statusCode() == 200;
	}
	*//**
	 * Reset password
	 * @throws Exception
	 *//*
	@Test
	public void testResetPassword() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 1);
		json.put("password", "mind@123");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/resetPassword").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * return my IP
	 * @throws Exception
	 *//*
	@Test
	public void testMyIp() throws Exception {
		 
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.get("http://localhost:8888/myIP").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Match Old Password
	 * @throws Exception
	 *//*
	@Test
	public void testMatchOldPassword() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 1);
		json.put("password", "mind@123");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/matchOldPassword").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Forgot Password
	 * @throws Exception
	 *//*
	@Test
	public void testForgotPassword() throws Exception {
		JSONObject json = new JSONObject();
		json.put("email", "mind@gmail.com");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.body(jsonString).when()
				.post("http://localhost:8888/forgotPassword").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get List Of Screen Modules
	 * @throws Exception
	 *//*
	@Test
	public void testGetListOfScreenModules() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/listOfScreenModules").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get List Of Report Modules
	 * @throws Exception
	 *//*
	@Test
	public void testGetListOfReportModules() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/listOfReportModules").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get List Of Transaction Modules
	 * @throws Exception
	 *//*
	@Test
	public void testGetListOfTransactionModules() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/listOfTransactionModules").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * List Of Transaction Modules By Access Role
	 * @throws Exception
	 *//*
	@Test
	public void testListOfTransactionModulesByAccessRole() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		json.put("accessRoleId", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/listOfTransactionModulesByAccessRole").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * List Of Screen Modules By Access Role
	 * @throws Exception
	 *//*
	@Test
	public void testListOfScreenModulesByAccessRole() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		json.put("accessRoleId", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/listOfScreenModulesByAccessRole").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * List Of Report Modules By Access Role
	 * @throws Exception
	 *//*
	@Test
	public void testListOfReportModulesByAccessRole() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		json.put("accessRoleId", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/listOfReportModulesByAccessRole").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Screen Detail Of User
	 * @throws Exception
	 *//*
	@Test
	public void testGetScreenDetailOfUser() throws Exception {
		JSONObject json = new JSONObject();
		json.put("moduleCode", "M-1000");
		json.put("screenCode", "S-1001");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/getScreenDetailOfUser").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Delete Session by Id
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteSession() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 1);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/deleteSession").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Delete All Session
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteAllSession() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.get("http://localhost:8888/deleteAllSession").andReturn();
		assert response.statusCode() == 200;
	}
	
}
*/