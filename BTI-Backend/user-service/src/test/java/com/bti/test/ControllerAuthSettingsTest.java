/*package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerAuthSettingsTest { 
	*//**
	 * Get week days
	 * @throws Exception
	 *//*
	@Test
	public void testGetDays() throws Exception {
		 
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.get("http://localhost:8888/authSettings/getDays").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get User List By User GroupId
	 * @throws Exception
	 *//*
	@Test
	public void testGetUserListByUserGroupId() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/getUserListByUserGroupId").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get User Group List By CompanyId
	 * @throws Exception
	 *//*
	@Test
	public void testGetUserGroupListByCompanyId() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/getUserGroupListByCompanyId").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Save AuthSetting
	 * @throws Exception
	 *//*
	@Test
	public void testSaveAuthSetting() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> dayIds = new ArrayList<Integer>();
		dayIds.add(1);
		List<Integer> userIds = new ArrayList<Integer>();
		userIds.add(1);
		
		json.put("companyId", 1);
		json.put("authorizedUserGroupId", 1);
		json.put("dayIds", dayIds);
		json.put("userIds", userIds);
		json.put("startDate", "10/12/2018");
		json.put("endDate", "10/12/2018");
		json.put("startTime", "10:00 am");
		json.put("endTime", "10:00 am");
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/saveAuthSetting").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update AuthSetting
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateAuthSetting() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> dayIds = new ArrayList<Integer>();
		dayIds.add(1);
		List<Integer> userIds = new ArrayList<Integer>();
		userIds.add(1);
		json.put("id", 1);
		json.put("companyId", 1);
		json.put("authorizedUserGroupId", 1);
		json.put("dayIds", dayIds);
		json.put("userIds", userIds);
		json.put("startDate", "10/12/2018");
		json.put("endDate", "10/12/2018");
		json.put("startTime", "10:00 am");
		json.put("endTime", "10:00 am");
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/updateAuthSetting").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get User List By Company And User Group
	 * @throws Exception
	 *//*
	@Test
	public void testGetUserListByCompanyAndUserGroup() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);
		json.put("companyId", 1);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/getUserListByCompanyAndUserGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Delete Auth Setting
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteAuthSetting() throws Exception {
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(1);
		ids.add(2);
		JSONObject json = new JSONObject();
		json.put("ids", ids);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/deleteAuthSetting").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Get Auth Setting By Id
	 * @throws Exception
	 *//*
	@Test
	public void testGetAuthSettingById() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/getAuthSettingById").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get All Auth Settings
	 * @throws Exception
	 *//*
	@Test
	public void testGetAllAuthSettings() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageSize", 10);
		json.put("pageNumber", 0);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/authSettings/getAllAuthSettings").andReturn();
		assert response.statusCode() == 200;
	}
	
}
*/