/*package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.jayway.restassured.response.Response;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

public class ControllerGroupTest { 
	*//**
	 * save role
	 * @throws Exception
	 *//*
	@Test
	public void testSaveRole() throws Exception {
		JSONObject json = new JSONObject();
		json.put("roleName", "Account Manager");
		json.put("roleDescription", "Account Manager");

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/saveRole").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update Role
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateRole() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 14);
		json.put("roleName", "Account Manager");
		json.put("roleDescription", "Account Manager");

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/updateRole").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get role details by id
	 * @throws Exception
	 *//*
	@Test
	public void testGetRoleDetails() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 14);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/group/getRoleDetails").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Get access role list with pagination
	 * @throws Exception
	 *//*
	@Test
	public void testGetAccessRoleListWithPagination() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/getAccessRoleList").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get access role list without pagination
	 * @throws Exception
	 *//*
	@Test
	public void testGetAccessRoleListWithOutPagination() throws Exception {
		JSONObject json = new JSONObject();
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/getAccessRoleList").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Delete role
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteRole() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("ids", ids);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/deleteRoles").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Save role groups
	 * @throws Exception
	 *//*
	@Test
	public void testSaveRoleGroup() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("roleGroupName", "Accountant");
		json.put("roleGroupDescription", "Accountant");
		json.put("roleIdList", ids);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/saveRoleGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * update role groups
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateRoleGroup() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("id", 1);
		json.put("roleGroupName", "Accountant");
		json.put("roleGroupDescription", "Accountant");
		json.put("roleIdList", ids);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/group/updateRoleGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * get role group detail
	 * @throws Exception
	 *//*
	@Test
	public void testGetRoleGroupDetails() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/group/getRoleGroupDetails").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * delete role groups
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteRoleGroups() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("ids", ids);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/group/deleteRoleGroups").andReturn();
		assert response.statusCode() == 200;
	}
	

	*//**
	 * Get role group list with pagination
	 * @throws Exception
	 *//*
	@Test
	public void testGetRoleGroupListWithPagination() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/getRoleGroupList").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get role group list without pagination
	 * @throws Exception
	 *//*
	@Test
	public void testGetRoleGroupListWithoutPagination() throws Exception {
		JSONObject json = new JSONObject();

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/getRoleGroupList").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Save user group
	 * @throws Exception
	 *//*
	@Test
	public void testSaveUserGroup() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("groupName", "test");
		json.put("groupDesc", "desc");
		json.put("roleGroupList", ids);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/saveUserGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update user group
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateUserGroup() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("id", 1);
		json.put("groupName", "test");
		json.put("groupDesc", "desc");
		json.put("roleGroupList", ids);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/group/updateUserGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get user group
	 * @throws Exception
	 *//*
	@Test
	public void testGetUserGroup() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 1);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/group/getUserGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Delete user group
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteUserGroup() throws Exception {
		JSONObject json = new JSONObject();
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		json.put("deleteIds", ids);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/group/deleteUserGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get all user group
	 * @throws Exception
	 *//*
	@Test
	public void testGetAllUserGroup() throws Exception {
		JSONObject json = new JSONObject();
		 
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/group/getAllUserGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Access Role List For DropDown
	 * @throws Exception
	 *//*
	@Test
	public void testGetAccessRoleListForDropDown() throws Exception {
		JSONObject json = new JSONObject();
		 
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.put("http://localhost:8888/group/getAccessRoleListForDropDown").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Role Group List For DropDown
	 * @throws Exception
	 *//*
	@Test
	public void testGetRoleGroupListForDropDown() throws Exception {
		JSONObject json = new JSONObject();
		 
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.put("http://localhost:8888/group/getRoleGroupListForDropDown").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get All User Group List For DropDown
	 * @throws Exception
	 *//*
	@Test
	public void testGetAllUserGroupListForDropDown() throws Exception {
		JSONObject json = new JSONObject();
		 
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.put("http://localhost:8888/group/getAllUserGroupListForDropDown").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Search Access Roles
	 * @throws Exception
	 *//*
	@Test
	public void testSearchAccessRoles() throws Exception {
		JSONObject json = new JSONObject();
		json.put("searchKeyword", "");
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/group/searchAccessRoles").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Search Role Groups
	 * @throws Exception
	 *//*
	@Test
	public void testSearchRoleGroups() throws Exception {
		JSONObject json = new JSONObject();
		json.put("searchKeyword", "");
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/group/searchRoleGroups").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Search User Groups
	 * @throws Exception
	 *//*
	@Test
	public void testSearchUserGroups() throws Exception {
		JSONObject json = new JSONObject();
		json.put("searchKeyword", "");
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/group/searchUserGroups").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Save or Update Access Role Screen Access
	 * @throws Exception
	 *//*
	@Test
	public void testSaveorUpdateAccessRoleScreenAccess() throws Exception {
		
		JSONArray fieldList = new JSONArray();
		JSONObject field = new JSONObject();
		field.put("fieldId", 1);
		field.put("readAccess", true);
		field.put("writeAccess", true);
		field.put("deleteAccess", true);
		fieldList.add(field);
		
		JSONArray screensList = new JSONArray();
		JSONObject screens = new JSONObject();
		screens.put("screenId", 1);
		screens.put("fieldList", fieldList);
		screensList.add(screens);
		
		JSONArray modulesList = new JSONArray();
		JSONObject modules = new JSONObject();
		modules.put("moduleId", 1);
		modules.put("screensList", screensList);
		modulesList.add(modules);
		
		JSONObject json = new JSONObject();
		json.put("accessRoleId", 15);
		json.put("modulesList", modulesList);
		 
		String jsonString = json.toJSONString();
		
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.put("http://localhost:8888/group/saveorUpdateAccessRoleScreenAccess").andReturn();
		
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Save or Update Access Role Transaction Access
	 * @throws Exception
	 *//*
	@Test
	public void testSaveorUpdateAccessRoleTransactionAccess() throws Exception {
		
		JSONArray transactionsList = new JSONArray();
		JSONObject transaction = new JSONObject();
		transaction.put("transactionId", 1);
		transaction.put("viewAccess", true);
		transaction.put("postAccess", true);
		transaction.put("deleteAccess", true);
		transactionsList.add(transaction);
		
		JSONArray transactionTypeList = new JSONArray();
		JSONObject transactionType = new JSONObject();
		transactionType.put("transactionTypeId", 1);
		transactionType.put("transactionsList", transactionsList);
		transactionTypeList.add(transactionType);
		
		JSONArray modulesList = new JSONArray();
		JSONObject modules = new JSONObject();
		modules.put("moduleId", 1);
		modules.put("transactionTypeList", transactionTypeList);
		modulesList.add(modules);
		
		JSONObject json = new JSONObject();
		json.put("accessRoleId", 15);
		json.put("modulesList", modulesList);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.put("http://localhost:8888/group/saveorUpdateAccessRoleTransactionAccess").andReturn();
		
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Save or Update Access Role Report Access
	 * @throws Exception
	 *//*
	@Test
	public void testSaveorUpdateAccessRoleReportAccess() throws Exception {
		
		JSONArray reportList = new JSONArray();
		JSONObject report = new JSONObject();
		report.put("reportId", 1);
		report.put("viewAccess", true);
		report.put("emailAccess", true);
		report.put("exportAccess", true);
		reportList.add(report);
		
		JSONArray reportCategoryList = new JSONArray();
		JSONObject reportCategory = new JSONObject();
		reportCategory.put("reportCategoryId", 1);
		reportCategory.put("reportList", reportList);
		reportCategoryList.add(reportCategory);
		
		JSONArray modulesList = new JSONArray();
		JSONObject modules = new JSONObject();
		modules.put("moduleId", 1);
		modules.put("reportCategoryList", reportCategoryList);
		modulesList.add(modules);
		
		JSONObject json = new JSONObject();
		json.put("accessRoleId", 15);
		json.put("modulesList", modulesList);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.put("http://localhost:8888/group/saveorUpdateAccessRoleReportAccess").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Block/Unblock Role
	 * @throws Exception
	 *//*
	@Test
	public void testBlockUnblockRole() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 5);
		json.put("isActive", true);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/group/blockUnblockRoleGroup").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	
}
*/