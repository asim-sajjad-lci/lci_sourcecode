/*package com.bti.test;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.jayway.restassured.response.Response;

import net.minidev.json.JSONObject;

public class ControllerUserTest {

	*//**
	 * Create user
	 * @throws Exception
	 *//*
	@Test
	public void testCreateUser() throws Exception {
		JSONObject json = new JSONObject();
		 
		List<Integer> companyIds = new ArrayList<>();
		companyIds.add(1);
		
		List<String> macAddressList = new ArrayList<>();
		macAddressList.add("123");
		
		json.put("firstName", "Test");
		json.put("middleName", "");
		json.put("lastName", "singh");
		json.put("secondaryFirstName", "singh");
		json.put("secondaryLastName", "singh");
		json.put("phone", "9646252403");
		json.put("email","test124561222@yopmail.com");
		json.put("companyIds",companyIds);
		json.put("userGroupId","2");
		json.put("dob","30/12/2017");
		json.put("zipCode","143305");
		json.put("stateId",1);
		json.put("countryId",1);
		json.put("cityId",1);
		json.put("isActive",true);
		json.put("address","Mohali punjab");
		json.put("countryCode","+91");
		json.put("macAddressList",macAddressList);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/createUser").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update user
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateUser() throws Exception {
		JSONObject json = new JSONObject();
		 
		List<Integer> companyIds = new ArrayList<>();
		companyIds.add(1);
		
		List<String> macAddressList = new ArrayList<>();
		macAddressList.add("123");
		json.put("userId", 1);
		json.put("firstName", "Test");
		json.put("middleName", "");
		json.put("lastName", "singh");
		json.put("secondaryFirstName", "singh");
		json.put("secondaryLastName", "singh");
		json.put("phone", "9646252403");
		json.put("email","test124561222@yopmail.com");
		json.put("companyIds",companyIds);
		json.put("userGroupId","2");
		json.put("dob","30/12/2017");
		json.put("zipCode","143305");
		json.put("stateId",1);
		json.put("countryId",1);
		json.put("cityId",1);
		json.put("isActive",true);
		json.put("address","Mohali punjab");
		json.put("countryCode","+91");
		json.put("macAddressList",macAddressList);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/updateUser").andReturn();
		assert response.statusCode() == 200;
	}
	*//**
	 * delete multiple user
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteUser() throws Exception {
		JSONObject json = new JSONObject();
		 
		List<Integer> userIds = new ArrayList<>();
		userIds.add(1);
		json.put("userIds", userIds);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/deleteMutipleUsers").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Users list
	 * @throws Exception
	 *//*
	@Test
	public void testGetUsersList() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/user/getUsersList").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get User detail 
	 * @throws Exception
	 *//*
	@Test
	public void testGetUserDetailByUserId() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 1);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/getUserDetailByUserId").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Check User name 
	 * @throws Exception
	 *//*
	@Test
	public void testCheckUserName() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userName", "name");
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/checkUserName").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Check email
	 * @throws Exception
	 *//*
	@Test
	public void testCheckEmail() throws Exception {
		JSONObject json = new JSONObject();
		json.put("email", "test@test.com");
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/checkEmail").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Users List For DropDown
	 * @throws Exception
	 *//*
	@Test
	public void testGetUsersListForDropDown() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/user/getUsersListForDropDown").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * set User IP
	 * @throws Exception
	 *//*
	@Test
	public void testSetUserIP() throws Exception {
		JSONObject json = new JSONObject();
		json.put("ipAddress","10.8.11.192");
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/user/setUserIP").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update User IP
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateUserIP() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id",77);
		json.put("ipAddress","10.8.11.192");
		json.put("isActive",true);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.put("http://localhost:8888/user/setUserIP").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get Users List For DropDown
	 * @throws Exception
	 *//*
	@Test
	public void testSearchUsers() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageNumber", 0);
		json.put("pageSize", 10);
		json.put("searchKeyword", "");

		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/searchUsers").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Block/Unblock User 
	 * @throws Exception
	 *//*
	@Test
	public void testBlockUnblockUser() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 5);
		json.put("isActive", true);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/user/blockUnblockUser").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * reset password
	 * @throws Exception
	 *//*
	@Test
	public void testResetPassword() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 5);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/user/resetPassword").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Logout
	 * @throws Exception
	 *//*
	@Test
	public void testLogout() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 5);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/user/logout").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update admin Profile
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateAdminProfile() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 5);
		json.put("firstName", "first");
		json.put("middleName", "middle");
		json.put("lastName", "last");
		json.put("secondaryFirstName", "Afirst");
		json.put("secondaryMiddleName", "Amiddle");
		json.put("secondarylastName", "Alast");
		json.put("password", 5);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/user/updateAdminProfile").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get admin Profile
	 * @throws Exception
	 *//*
	@Test
	public void testGetAdminProfileDetail() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 5);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/user/getAdminProfileDetail").andReturn();
		assert response.statusCode() == 200;
	}
	
	
	*//**
	 * Save Mac Address
	 * @throws Exception
	 *//*
	@Test
	public void testSaveMacAddress() throws Exception {
		JSONObject json = new JSONObject();
		json.put("userId", 5);
		json.put("macAddress", "12345");
		json.put("deviceType", "deviceType");
		json.put("deviceDescription", "description");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/user/saveMacAddress").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Delete Mac Address
	 * @throws Exception
	 *//*
	@Test
	public void testDeleteMacAddress() throws Exception {
		JSONObject json = new JSONObject();
		json.put("id", 5);
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
	            .body(jsonString).when()
				.post("http://localhost:8888/user/deleteMacAddress").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Get All Users List
	 * @throws Exception
	 *//*
	@Test
	public void testGetAllUsersList() throws Exception {
		JSONObject json = new JSONObject();
		json.put("pageSize", 10);
		json.put("pageNumber", 0);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/getAllUsersList").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Update Ip Checked Status
	 * @throws Exception
	 *//*
	@Test
	public void testUpdateIpCheckedStatus() throws Exception {
		List<Integer> userIds = new ArrayList<>();
		userIds.add(1);
		userIds.add(2);
		JSONObject json = new JSONObject();
		json.put("userIds", userIds);
		json.put("ipChecked", false);
		
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/updateIpCheckedStatus").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Header Detail
	 * @throws Exception
	 *//*
	@Test
	public void testHeaderDetail() throws Exception {
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.get("http://localhost:8888/user/headerDetail").andReturn();
		assert response.statusCode() == 200;
	}
	
	*//**
	 * Side Menu By Header Id
	 * @throws Exception
	 *//*
	@Test
	public void testSideMenuByHeaderId() throws Exception {
		JSONObject json = new JSONObject();
		json.put("moduleId", "12");
		String jsonString = json.toJSONString();
		Response response = given().header("Content-Type", "application/json")
				.header("langid", "1")
	            .header("userid", "77")
	            .header("session", "12345678")
				.body(jsonString).when()
				.post("http://localhost:8888/user/sideMenuByHeaderId").andReturn();
		assert response.statusCode() == 200;
	}
}
*/