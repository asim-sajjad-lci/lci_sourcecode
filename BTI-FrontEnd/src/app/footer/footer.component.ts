import { Component, OnInit } from '@angular/core';
import {Constants} from '../_sharedresource/Constants';
import { GetScreenDetailService } from '../_sharedresource/_services/get-screen-detail.service';
import { CompanyService } from '../userModule/_services/companymanagement/company.service';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  providers: [GetScreenDetailService]
})
export class FooterComponent {
  footervalue=Constants.footervalue;
  moduleCode = Constants.selectedText;
  public comapnyName:string;
  constructor(private getScreenDetailService:GetScreenDetailService,private companyService:CompanyService) {


   }

  ngOnInit() {
    this.footervalue
    console.log(localStorage.getItem('tenantid'));
    if(localStorage.getItem('tenantid') != ''){
      this.companyService.getOneCompanyByTenatedId(localStorage.getItem('tenantid')).subscribe(data =>{
        
        this.companyService.getCurrentCompanyName().subscribe(companyName =>{
          this.comapnyName = companyName;
          console.log(this.comapnyName);
        });
      });
      
    }
    
    
  }
}
