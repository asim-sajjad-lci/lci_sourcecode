/**
 * A model for white list IP 
 */
export class WhiteListIP {
    whitelistIpId: number;
    ipAddress: string;
    description: string;
    isActive: boolean;

    //initializing white list IP parameters
    constructor(whitelistIpId: number, ipAddress: string,description: string,isActive: boolean) 
      {
            this.whitelistIpId = whitelistIpId;
            this.ipAddress = ipAddress;
            this.description = description;
            this.isActive = isActive;
      }
}