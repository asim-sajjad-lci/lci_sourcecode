/**
 * A model for department 
 */
export class CompanyReportManagementModel {
    id: number;
    company: string;
    companyId: number;
    companyName: string;
    description: string;
    arabicDescription: string;
    reportId: string;
    reportName: string;
    reportLink: string;
    reportIds: any;

    // initializing department parameters
    constructor(id: number, company: string, companyId: number, companyName: string, description: string,
        arabicDescription: string, reportId: string, reportName: string, reportLink: string, reportIds: any) {
        this.id = id;
        this.company = company;
        this.companyId = companyId;
        this.companyName = companyName;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.reportId = reportId;
        this.reportName = reportName;
        this.reportLink = reportLink;
        this.reportIds = reportIds;
    }
}
