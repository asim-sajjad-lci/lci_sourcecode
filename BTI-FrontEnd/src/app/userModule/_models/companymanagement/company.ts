//parameter for company modal
export class Company {
    id: number;
    name: string;
    nameArabic: string;
    address: string;
    isActive: string;
    state: string;
    stateId: number;
    city: string;
    cityId: number;
    country: string;
    countryId: number;
    fax: string;
    email: string;
    phone: string;
    zipCode: string;
    companyCode: string;
    countryCode: string;
    webAddress: string;
    latitude: string;
    longitude: string;


    //constructor calling company parameter
    constructor(id: number, name: string, description: string, address: string, isActive: string,
        state: string, stateId: number, city: string, cityId: number, country: string, countryId: number,
        fax: string, email: string, phone: string, zipCode: string, companyCode: string, countryCode: string,
        webAddress: string, latitude: string, longitude: string, nameArabic: string) 
    {
        this.id = id;
        this.name = name;
        this.address = address;
        this.isActive = isActive;
        this.state=state;
        this.stateId=stateId;
        this.city=city;
        this.cityId=cityId;
        this.country=country;
        this.countryId=countryId;
        this.fax=fax;
        this.email=email;
        this.phone=phone;
        this.zipCode=zipCode;
        this.companyCode=companyCode;
        this.countryCode=countryCode;
        this.webAddress=webAddress;
        this.latitude=latitude;
        this.longitude=longitude;
        this.nameArabic = nameArabic;
    }
}