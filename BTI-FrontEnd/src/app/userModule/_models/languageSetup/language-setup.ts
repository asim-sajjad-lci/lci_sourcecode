/**
 * A model for LanguageSetup
 */
export class LanguageSetup {
    languageId: number;
    languageName:string;
    languageOrientation:string;
    languageStatus:string;
    isActive:boolean
    
    //initializing LanguageSetup parameters
    constructor (languageId: number, languageName:string, languageOrientation:string,languageStatus:string,isActive:boolean)
    {
        this.languageId= languageId;
        this.languageName= languageName;
        this.languageOrientation= languageOrientation;
        this.languageStatus= languageStatus;
        this.isActive= isActive;
    }
}