/**
 * A model for an change password
 */
export class activityLog {
    id: number;
    userId: number;
    userName: string;
    services: string;
    method: string;
    activity: string;
    fieldOldValue: string;
    fieldNewValue: string;
    createdDate:Date;
    updatedDate:Date;
    updatedBy: number;
    screenName : string;
    companyName : string;
    moduleName : string;
    fromDate : Date;
    //initializing department parameters
    constructor(id: number,userId: number,userName: string,services: string,method: string,
        activity: string,fieldOldValue: string,fieldNewValue: string,createdDate:Date,
        updatedDate:Date,updatedBy: number,screenName : string,companyName : string,moduleName : string,fromDate : Date) {

       this.id = id;
       this.userId = userId;
       this.userName = userName;
       this.services = services;
       this.method = method;
       this.activity = activity
       this.fieldOldValue = fieldOldValue;
       this.fieldNewValue=fieldNewValue;
       this.createdDate = createdDate;
       this.updatedDate = updatedDate;
       this.updatedBy = updatedBy;
       this.screenName = screenName;
       this.companyName = companyName;
       this.moduleName = moduleName;
       this.fromDate = fromDate;

    }
}