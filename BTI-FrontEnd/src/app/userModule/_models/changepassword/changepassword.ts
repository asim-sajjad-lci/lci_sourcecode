/**
 * A model for an change password
 */
export class ChangePassword 
{
    password: string;

    //initializing authorization parameters
    constructor(password: string) 
      {
          this.password = password;
      }
}