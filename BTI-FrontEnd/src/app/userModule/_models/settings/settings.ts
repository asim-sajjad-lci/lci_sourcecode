/**
 * A model for settings
 */
export class Settings {
    userId:number;
    userDetailId:number;
    firstName:string;
    lastName:string;
    email:string;
    middleName:string;
    secondaryFirstName:string;
    secondaryMiddleName:string;
    secondaryLastName:string;
    secondaryFullName:string;
    primaryFullName:string;
    password:string;

    //initializing settings parameters
    constructor(userId:number,userDetailId:number,firstName:string,lastName:string,email:string,middleName:string,
                secondaryFirstName:string,secondaryMiddleName:string,secondaryLastName:string,secondaryFullName:string,
                primaryFullName:string,password:string) 
      {
            this.userId=userId;
            this.userDetailId=userDetailId;
            this.firstName=firstName;
            this.lastName=lastName;
            this.email=email;
            this.middleName=middleName;
            this.secondaryFirstName=secondaryFirstName;
            this.secondaryMiddleName=secondaryMiddleName;
            this.secondaryLastName=secondaryLastName;
            this.secondaryFullName=secondaryFullName;
            this.primaryFullName=primaryFullName;
            this.password=password;
      }
}