/**
 * A model for department 
 */
export class UserReportManagementModel {
    id: number;
    user: string;
    userId: number;
    userName: string;
    reportId: string;
    reportName: string;
    reportLink: string;
    reportIds: any;

    // initializing department parameters
    constructor(id: number, user: string, userId: number, userName: string, reportId: string, reportName: string,
                reportLink: string, reportIds: any) {
        this.id = id;
        this.user = user;
        this.userId = userId;
        this.userName = userName;
        this.reportId = reportId;
        this.reportName = reportName;
        this.reportLink = reportLink;
        this.reportIds = reportIds;
    }
}
