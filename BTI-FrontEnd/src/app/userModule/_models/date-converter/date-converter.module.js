"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A model for department
 */
var DateConverterModule = (function () {
    //initializing department parameters
    function DateConverterModule(id, departmentId, departmentDescription, arabicDepartmentDescription, gregorianDate, convertedHijriDate, hijriDate, convertedGregorianDate, hijriDay, hijriMonth, hijriYear, d, m, y, hm, hi) {
        this.id = id;
        this.departmentId = departmentId;
        this.arabicDepartmentDescription = arabicDepartmentDescription;
        this.departmentDescription = departmentDescription;
        this.gregorianDate = gregorianDate;
        this.convertedHijriDate = convertedHijriDate;
        this.hijriDate = hijriDate;
        this.convertedGregorianDate = convertedGregorianDate;
        this.hijriDay = hijriDay;
        this.hijriMonth = hijriMonth;
        this.hijriYear = hijriYear;
        this.d = d;
        this.m = m;
        this.y = y;
        this.hm = hm;
        this.hi = hi;
    }
    return DateConverterModule;
}());
exports.DateConverterModule = DateConverterModule;
//# sourceMappingURL=date-converter.module.js.map