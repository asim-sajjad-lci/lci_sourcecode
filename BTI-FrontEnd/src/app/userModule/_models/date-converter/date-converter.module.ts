/**
 * A model for department
 */
export class DateConverterModule {
    id: number;
    departmentId: string;
    departmentDescription: string;
    arabicDepartmentDescription: string;
    gregorianDate: Date;
    convertedHijriDate: string;
    hijriDate: string;
    convertedGregorianDate: string;
    hijriDay: number;
    hijriMonth: string;
    hijriYear: number;
    d: string;
    m: string;
    y: string;


    // initializing department parameters
    constructor(id: number, departmentId: string, departmentDescription: string,
                arabicDepartmentDescription: string, gregorianDate: Date, convertedHijriDate: string, hijriDate: string,
                convertedGregorianDate: string, hijriDay: number, hijriMonth: string, hijriYear: number, d: string,
                m: string, y: string) {
        this.id = id;
        this.departmentId = departmentId;
        this.arabicDepartmentDescription = arabicDepartmentDescription;
        this.departmentDescription = departmentDescription;
        this.gregorianDate = gregorianDate;
        this.convertedHijriDate = convertedHijriDate;
        this.hijriDate = hijriDate;
        this.convertedGregorianDate = convertedGregorianDate;
        this.hijriDay = hijriDay;
        this.hijriMonth = hijriMonth;
        this.hijriYear = hijriYear;
        this.d = d;
        this.m = m;
        this.y = y;

    }
}
