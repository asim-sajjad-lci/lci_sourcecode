/**
 * A model for user group
 */
export class UserGroup{
    id:number;
    roleGroupid:string;
    groupName : string;
    groupDesc : string;
    groupCode:string;

    //initializing user group parameters
    constructor(id:number,groupCode:string,roleGroupid:string,groupName:string,groupDesc:string)
      {
            this.id=id;
            this.groupCode=groupCode;
            this.roleGroupid=roleGroupid;
            this.groupName=groupName;
            this.groupDesc=groupDesc;
      }
}