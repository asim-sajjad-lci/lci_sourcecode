/**
 * A model for role group
 */
export class RoleGroup{
    id:number;
    roleIdList:string;
    roleGroupName : string;
    roleGroupDescription : string;
    roleGroupCode:string;

    //initializing role group parameters
    constructor(id:number,roleGroupCode:string,roleIdList:string,roleGroupName:string,roleGroupDescription:string)
      {
            this.id=id;
            this.roleGroupCode=roleGroupCode;
            this.roleIdList=roleIdList;
            this.roleGroupName=roleGroupName;
            this.roleGroupDescription=roleGroupDescription;
      }
}