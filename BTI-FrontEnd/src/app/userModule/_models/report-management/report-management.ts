/**
 * A model for report master
 */
export class ReportManagementModel {
    id: number;
    reportId: string;
    reportName: string;
    reportLink: string;
    reportDescription: string;
    objectId: string;
    containerId: string;
    moduleId: number;

    // initializing report master parameters
    constructor(id: number, reportId: string, reportName: string, reportLink: string, reportDescription: string, objectId: string,
                containerId: string, moduleId: number) {
        this.id = id;
        this.reportId = reportId;
        this.reportName = reportName;
        this.reportLink = reportLink;
        this.reportDescription = reportDescription;
        this.objectId = objectId;
        this.containerId = containerId;
        this.moduleId = moduleId;
    }
}
