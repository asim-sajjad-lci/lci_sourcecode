/**
 * A model for fields
 */
export class Fields {
    fieldAccessId: number;
    fieldId: number;
    companyId: string;
    fieldName: string;
    description: string;
    tenantId: string;
    isMandatory: boolean;
    modulesList = [];
    keyboardInput: string;
    languageId: number;
    languageName: string;
    screenId: string;
    moduleId: string;
    fieldLists: Array<any> = [];

    // initializing role parameters
    constructor(fieldAccessId: number, fieldId: number, companyId: string, fieldName: string, description: string, tenantId: string,
                isMandatory: boolean, modulesList: any[], keyboardInput: string, languageId: number,
                languageName: string, screenId: string, moduleId: string, fieldLists: Array<any>) {
        this.fieldAccessId = fieldAccessId;
        this.fieldId = fieldId;
        this.companyId = companyId;
        this.fieldName = fieldName;
        this.description = description;
        this.tenantId = tenantId;
        this.isMandatory = isMandatory;
        this.modulesList = modulesList;
        this.keyboardInput = keyboardInput;
        this.languageId = languageId;
        this.languageName = languageName;
        this.screenId = screenId;
        this.moduleId = moduleId;
        this.fieldLists = fieldLists;
    }
}
