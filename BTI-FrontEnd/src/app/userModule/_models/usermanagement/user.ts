/**
 * A model for user 
 */
export class User {
    userId: number;
    userDetailId: number;
    firstName: string;
    middleName: string;
    lastName: string;
    secondaryFirstName: string;
    secondaryMiddleName: string;
    secondaryLastName: string;
    phone: number;
    email: string;
    password: string;
    listOfCompanies: string;
    userGroup: number;
    dob: string;
    zipCode: number;
    stateId: number;
    countryId: number;
    cityId: number;
    isActive: string;
    address: string;
    countryCode: string;
    userGroupName: string;
    companyIds: string;
    companyNames: string;
    secondaryFullName: string;

    //initializing user parameters
    constructor(userId: number,userDetailId: number,firstName: string,middleName: string,lastName: string,secondaryFirstName: string,
                secondaryMiddleName: string,secondaryLastName: string,phone: number,email: string,password: string,
                listOfCompanies: string,userGroup: number,dob: string,zipCode: number,stateId: number,countryId: number,
                cityId: number,isActive: string,address: string,countryCode: string,userGroupName: string,
                companyIds: string,companyNames: string,secondaryFullName: string)
            {
                    this.userId = userId;
                    this.userDetailId = userDetailId;
                    this.firstName = firstName;
                    this.middleName = middleName;
                    this.lastName=lastName;
                    this.secondaryFirstName = secondaryFirstName;
                    this.secondaryMiddleName = secondaryMiddleName;
                    this.secondaryLastName=secondaryLastName;
                    this.phone=phone;
                    this.email=email;
                    this.password=password;
                    this.listOfCompanies=listOfCompanies;
                    this.userGroup=userGroup;
                    this.dob=dob;
                    this.zipCode=zipCode;
                    this.stateId=stateId;
                    this.countryId=countryId;
                    this.cityId=cityId;
                    this.isActive=isActive;
                    this.address=address;
                    this.countryCode=countryCode;
                    this.userGroupName=userGroupName;
                    this.companyIds=companyIds;
                    this.companyNames=companyNames;
                    this.secondaryFullName=secondaryFullName;
            }
}