import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../_sharedresource/page';
import { UserGroup } from '../../_models/usergroup/usergroup';
import { UserGroupService } from '../../_services/usergroupmanagement/usergroup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';


@Component({
    selector: '<usergroup></usergroup>',
    templateUrl: './user-group-management.component.html',
    styles: ["company.component.css"],
    providers: [UserGroupService,GetScreenDetailService]
})

// export to make it available for other classes
export class UserGroupManageComponent {
    page = new Page();
    rows = new Array<UserGroup>();
    temp = new Array<UserGroup>();
    selected = [];
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1014";
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
    isScreenLock;
    userGroupId = {};
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    isConfirmationModalOpen:boolean=false;
    currentSelectedRow: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction : boolean = false;
    EmptyMessage = Constants.EmptyMessage;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private UserGroupService: UserGroupService,
        private getScreenDetailService: GetScreenDetailService,
    ) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        
        //default form values adding new user group
        this.defaultFormValues = [
            { 'fieldName': 'ADD_NEW_USER_GROUP', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUP_SEARCH', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPNAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_ROLEGROUP', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPDESCRIPTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPMEMBERS', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPVIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPEDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPDELETE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPTABLEVIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_GROUPACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
    }
    
    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0 });
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                this.moduleName = data.result.moduleName;
                this.screenName = data.result.dtoScreenDetail.screenName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            });
        }
        
        // //setting pagination 
        // setPage(pageInfo) {
        //     this.selected = [];
        //     this.page.pageNumber = pageInfo.offset;
        //     this.UserGroupService.getAllGroup(this.page, this.searchKeyword).subscribe(pagedData => {
        //         this.page = pagedData.page;
        //         this.rows = pagedData.data;
        //     });
        // }
        
        //default list on page
        onSelect({ selected }) {
            this.selected.splice(0, this.selected.length);
            this.selected.push(...selected);
        }
        
        //edit user group by row id
        edit(row: any) {
            this.userGroupId = row.id;
            this.router.navigate(['usergroup/createusergroup', this.userGroupId]);
        }
        
        varifyDelete()
        {
            this.isConfirmationModalOpen = true;
        }
        
        //delete one or multiple user groups
        delete() {
            
            var selectedRoles = [];
            for (var i = 0; i < this.selected.length; i++) {
                selectedRoles.push(this.selected[i].id);
            }
            this.UserGroupService.deleteGroup(selectedRoles).then(data => {
                window.scrollTo(0, 0);
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0 });
                    this.closeModal();
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    
                    this.hasMsg = true;
                    
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        // //setting pagination 
        setPage(pageInfo) {
            this.selected = [];
            this.page.pageNumber = pageInfo.offset;
            this.UserGroupService.searchGroup(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        
        //search user group by user name
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.UserGroupService.searchGroup(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            }, error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        // page size change event
        changePageSize(event) {
            this.page.size = event.target.value;
            this.setPage({ offset: 0 });
        }
        closeModal()
        {
            this.isConfirmationModalOpen = false;
        }
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
        
    }