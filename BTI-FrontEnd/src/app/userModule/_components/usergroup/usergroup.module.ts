
import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UserGroupRoutingModule } from './usergroup-routing.module';
import { UserGroupManageComponent } from './user-group-management.component';
import { CreateUserGroupComponent } from './create-user-group-management.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@NgModule({
  imports: [
    CommonModule,
    UserGroupRoutingModule,
    FormsModule,
    NgxDatatableModule,
    MultiselectDropdownModule
  ],
  declarations: [UserGroupManageComponent, CreateUserGroupComponent,AutoFocusDirective]
})
export class UserGroupModule { }
