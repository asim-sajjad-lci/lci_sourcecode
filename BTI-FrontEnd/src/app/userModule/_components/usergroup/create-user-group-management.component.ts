import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { NgForm } from '@angular/forms';
import { UserGroupService } from '../../_services/usergroupmanagement/usergroup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';

@Component({
    selector: 'create-user-group',
    templateUrl: './create-user-group-management.component.html',
    providers: [UserGroupService,GetScreenDetailService]
})
export class CreateUserGroupComponent {
    moduleName;
    screenName;
    moduleCode = Constants.userModuleCode;
    screenCode;
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    defaultFormValues: [object];
    availableFormValues: [object];
    model: any = {};
    userGroupId: string;
    lblRole = "Create Group";
    ddOptions: IMultiSelectOption[];
    LstCompany: IMultiSelectOption[];
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selectUserGroup= Constants.selectUserGroup;
    btndisabled:boolean=false;
    isScreenLock;
    
    // Settings configuration
    ddSettings: IMultiSelectSettings = {
        buttonClasses: 'btn dropdown-toggle formm-dd',
        enableSearch: true,
        showCheckAll: true,
        showUncheckAll: true
    };
    
    // Text configuration
    ddTexts: IMultiSelectTexts = {
        defaultTitle: this.selectUserGroup,
        checkAll: this.selectAll,
        uncheckAll: this.unselectAll,
    };
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userGroupService: UserGroupService,
        
        private getScreenDetailService: GetScreenDetailService,
    ) {
        
    }
    
    // Screen initialization
    ngOnInit() {
        this.userGroupService.getRoleGroupList().subscribe(data => {
            this.ddOptions = data;
        });
        this.route.params.subscribe((params: Params) => {
            this.userGroupId = params['userGroupId'];
            if (this.userGroupId != '' && this.userGroupId != undefined) {
                this.screenCode = "S-1016";
                
                //default form values for editting user group
                this.defaultFormValues = [
                    { 'fieldName': 'EDIT_USER_GROUP_GROUPNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'EDIT_USER_GROUP_ROLEGROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'EDIT_USER_GROUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'EDIT_USER_GROUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'EDIT_USER_GROUP_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                ];
                
                this.lblRole = "Edit Group";
                this.userGroupService.getGroup(this.userGroupId).then(data => {
                    this.model = data.result; 
                    window.setTimeout(() => {
                        this.model.roleGroupList = this.model.roleGroupIds;
                    }, 100);
                });
            }
            else {
                this.model.groupName = "";
                this.model.groupDesc = "";
                this.model.id = "";
                this.screenCode = "S-1015";
                
                //default form values adding new user group
                this.defaultFormValues = [
                    { 'fieldName': 'ADD_NEW_USER_GROUP_GROUPNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
                    { 'fieldName': 'ADD_NEW_USER_GROUP_ROLEGROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'ADD_NEW_USER_GROUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'ADD_NEW_USER_GROUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'ADD_NEW_USER_GROUP_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                ];
            }
        });
        
        //getting screen for user group
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                this.moduleName = data.result.moduleName;
                this.screenName = data.result.dtoScreenDetail.screenName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            });
        }
        
        // function call for creating new group
        CreateGroup(f: NgForm) {
            this.btndisabled=true;
            if (this.model.roleGroupList.length > 0) {
                if (this.userGroupId != '' && this.userGroupId != undefined) {
                    this.userGroupService.updateGroup(this.model).then(data => {
                        var datacode = data.code;
                        this.btndisabled=false;
                        if (datacode == 200) {
                            window.setTimeout(() => {
                                this.hasMsg = true;
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.messageText = data.btiMessage.message;;
                            }, 100);
                            window.setTimeout(() => {
                                this.router.navigate(['usergroup']);
                            }, 2000);
                        }else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.hasMsg = false;  
                            }, 4000);
                        }
                    });
                }
                else {
                    this.userGroupService.createGroup(this.model).then(data => {
                        window.scrollTo(0, 0);
                        var datacode = data.code;
                        this.btndisabled=false;
                        if (datacode == 201) {
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.messageText = data.btiMessage.message;;
                            }, 100);
                            window.setTimeout(() => {
                                this.router.navigate(['usergroup']);
                            }, 2000);
                            this.hasMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            f.resetForm();
                        } else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.hasMsg = false;  
                            }, 4000);
                            
                        }
                    });
                }
            }
            else {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Please select User Group'!";
                }, 100);
            }
        }
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
    }