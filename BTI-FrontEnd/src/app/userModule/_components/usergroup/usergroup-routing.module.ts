import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserGroupManageComponent } from './user-group-management.component';
import { CreateUserGroupComponent } from './create-user-group-management.component';

const routes: Routes = [
  { path: '', component: UserGroupManageComponent },
  { path: 'usergroup', component: UserGroupManageComponent },
  { path: 'createusergroup', component: CreateUserGroupComponent },
  { path: 'createusergroup/:userGroupId', component: CreateUserGroupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserGroupRoutingModule { }
