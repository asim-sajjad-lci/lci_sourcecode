import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyManagementComponent } from './company-management.component';
import { CreateCompanyComponent } from './create-company.component';


const routes: Routes = [
  { path: '', component: CompanyManagementComponent },
  { path: 'company', component: CompanyManagementComponent },
  { path: 'createcompany', component: CreateCompanyComponent },
  { path: 'createcompany/:companyId', component: CreateCompanyComponent },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
