import { NgModule,Directive,ElementRef } from '@angular/core';
// import { AutoFocusDirective } from '../../../_sharedresource/myFocus.directive';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { CompanyRoutingModule } from './company-routing.module';
import { CompanyManagementComponent } from './company-management.component';
import { CreateCompanyComponent } from './create-company.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { AgmCoreModule } from '@agm/core';
// import { OnlyNumber } from '../../../_sharedresource/onlynumber.directive';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    CompanyRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    MultiselectDropdownModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBFqCv1IAzVgnWA7D4uO6B_GDG-KedssKw'}),
  ],
  declarations: [CompanyManagementComponent,AutoFocusDirective,
    CreateCompanyComponent,
  //  OnlyNumber,
  ]
})
export class CompanyModule { }
