import { Component, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DOCUMENT } from '@angular/platform-browser';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Company } from '../../_models/companymanagement/company';
import { CompanyService } from '../../_services/companymanagement/company.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';
@Component({
    selector: '<company></company>',
    templateUrl: './company-management.component.html',
    styles: ["company.component.css"],
    providers: [CompanyService,GetScreenDetailService]
})

// export to make it available for other classes
export class CompanyManagementComponent {
    page = new Page();
    rows = new Array<Company>();
    temp = new Array<Company>();
    selected = [];
    companyId = {};
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1005";
    moduleName;
    defaultFormValues: any = [];
    availableFormValues: any = [];
    searchKeyword = '';
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    ddPageSize=5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    isConfirmationModalOpen:boolean=false;
    currentSelectedRow: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction : boolean = false;
    EmptyMessage = Constants.EmptyMessage;
    isScreenLock;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private companyService: CompanyService,
        private getScreenDetailService: GetScreenDetailService,
        
        @Inject(DOCUMENT) private document: any
    ) 
    {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.defaultFormValues = [
            { 'fieldName': 'ADD_COMPANY', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'COMPANY_ID', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'COMPANY_NAME', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'COUNTRY', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'FAX', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'EMAIL_ID', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADDRESS', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'STATE', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'CITY', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'COMPANY_BLOCK', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'COMPANY_UNBLOCK', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'CONFIRM_COMPANY_DELETE_OK', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'WEB_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'COMPANY_LATITUDE', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'COMPANY_LONGITUDE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'COMPANY_LONGITUDE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'COMPANY_EDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' }
            
            
            
        ];
    }
    
    // Screen initialization 
    ngOnInit() {
        
        this.setPage({ offset: 0 });
        // getting screen for company
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
        {
            this.isScreenLock = res;
        });
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.moduleName = data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
    }
    
    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.companyService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
    
    //default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }
    
    // get company detail by row id
    edit(row: any) {
        this.companyId = row.id;
        this.router.navigate(['company/createcompany', this.companyId]);  
    }
    
    varifyDelete()
    {
        this.isDeleteAction=true;
        this.confirmationModalBody = this.deleteConfirmationText;
        this.isConfirmationModalOpen = true;
    }
    
    // delete one or multiple companies
    delete() {
        
        
        var selectedRoles = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedRoles.push(this.selected[i].id);
        }
        this.companyService.deleteCompany(selectedRoles).then(data => {
            window.scrollTo(0, 0);
            var datacode = data.code;
            if (datacode == 201) {
                this.setPage({ offset: 0 });
                this.hasMsg = true;
                this.closeModal();
                if(data.result.associateCompanies.length > 0 && data.result.deleteCompanies.length > 0  )
                {
                    var associateCompanyName='';
                    for(var i=0;i<data.result.associateCompanies.length;i++)
                    {
                        associateCompanyName += data.result.associateCompanies[i].name + ','
                    }
                    associateCompanyName =  associateCompanyName.replace(/,\s*$/, "");
                    
                    var DeletedCompanyName='';
                    for(var i=0;i<data.result.deleteCompanies.length;i++)
                    {
                        DeletedCompanyName += data.result.deleteCompanies[i].name + ','
                    }
                    DeletedCompanyName =  DeletedCompanyName.replace(/,\s*$/, "");
                    
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    var MessageToShow='';
                    if(DeletedCompanyName.length > 0)
                    {
                        MessageToShow = DeletedCompanyName + ' Deleted Successfully.'
                    }
                    if(associateCompanyName.length > 0)
                    {
                        MessageToShow += Constants.butText + ' ' + associateCompanyName + ' ' + Constants.deleteCompanyAssociatedMessage +' ' + associateCompanyName+'.';
                    }
                    this.messageText = MessageToShow ;
                    this.showMsg = true;
                }
                else if(data.result.associateCompanies.length > 0 && data.result.deleteCompanies.length == 0 )
                {
                    var MessageToShow='';
                    var associateCompanyName='';
                    for(var i=0;i<data.result.associateCompanies.length;i++)
                    {
                        associateCompanyName += data.result.associateCompanies[i].name + ','
                    }
                    associateCompanyName =  associateCompanyName.replace(/,\s*$/, "");
                    
                    if(associateCompanyName.length > 0)
                    {
                        MessageToShow +=  associateCompanyName + ' ' + Constants.deleteCompanyAssociatedMessage +' '+associateCompanyName+' .';
                    }
                    this.messageText = MessageToShow ;
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                }
                else if(data.result.associateCompanies.length == 0 && data.result.deleteCompanies.length > 0 )
                {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.messageText = data.result.deleteMessage ;
                    this.showMsg = true;
                }
                
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 7000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText =  Constants.serverErrorText;
            }, 100)
        });
        
    }
    // search company details by company name,company code,etc 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.companyService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.hasMsg = false;
            this.showMsg = false;
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
        }, error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText =  Constants.serverErrorText;
            }, 100)
        });
    }
    
    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
    
    varifyOperation(row: any)
    {
        this.isDeleteAction=false;
        this.confirmationModalBody = Constants.confirmationModalBody;
        this.isConfirmationModalOpen = true;
        this.currentSelectedRow=row;
    }
    updateStatus() {
        this.companyService.blockUnblockCompany(this.currentSelectedRow.id,this.currentSelectedRow.isActive).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0 });
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                }, 100);
                this.hasMsg = true;
                this.closeModal();
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText =  Constants.serverErrorText;
            }, 100)
        });
    }
    
    closeModal()
    {
        this.isDeleteAction=false;
        this.isConfirmationModalOpen = false;
    }

     /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
}
