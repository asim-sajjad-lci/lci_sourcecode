import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Role } from '../../_models/rolemanagement/role';
import { CompanyService } from '../../_services/companymanagement/company.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../_sharedresource/Autofocus';
import {Constants} from '../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';


@Component({
    selector: 'create-company',
    styles: [`
    agm-map {
        height: 300px;
    }
    `],
    templateUrl: './create-company.component.html',
    providers: [CompanyService,GetScreenDetailService]
})
export class CreateCompanyComponent {
    companyId: string;
    moduleCode = Constants.userModuleCode;
    screenCode;
    moduleName;
    screenName;
    isScreenLock;
    disabledButton;
    defaultFormValues: any =[];
    availableFormValues: any =[];
    messageText;
    hasMsg = false;
    showloader = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    countryOptions;
    stateOptions;
    cityOptions;
    isActive= true;
    select=Constants.select;
    lat: number;
    lng: number;
    ShowInvalidWebAddress:boolean=false;
    msgCompanyExist:string;
    isModify:boolean=false;
    image;
    uploadAttachfile;
    // hideElement: boolean;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private companyService: CompanyService,
        private getScreenDetailService: GetScreenDetailService){
        }
        
        // Screen initialization 
        ngOnInit() {
            // this.hideElement = true;
            //this.showLoader();
            this.disabledButton=false;
            this.model.isActive = "Y";
            this.companyService.getCountryList().then(data => {
                this.countryOptions = data.result;
                this.countryOptions.splice(0, 0, { "countryId": "", "countryName": this.select });
                
            });
            
            this.route.params.subscribe((params: Params) => {
                this.companyId = params['companyId'];
                
                if (this.companyId != '' && this.companyId != undefined) {
                    //defaul form parameter for edit screen
                    this.isModify = true;
                    this.defaultFormValues = [
                        
                        { 'fieldName': 'EDIT_COMPANY_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_POSTAL_ZIPCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_BACK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_COMPANY_WEB_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_COMPANY_LATITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                        { 'fieldName': 'EDIT_COMPANY_LONGITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                    ];
                    
                    this.screenCode = "S-1020";
                    this.companyService.getCompany(this.companyId).then(data => {
                        console.log(data.result);
                        this.model = data.result;
                        this.uploadAttachfile = data.result.logo;
                        this.lat=this.model.latitude;
                        this.lng=this.model.longitude;
                        this.image = "data:image/png;base64," + data.result.logo;
                        this.companyService.getStateList(this.model.countryId).then(data => {
                            this.stateOptions = data.result;
                            this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
                        });
                        this.companyService.getCityList(this.model.stateId).then(data => {
                            this.cityOptions = data.result;
                            this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
                        });
                    });
                }
                else {
                    this.isModify = false;
                    this.model.countryId = "";
                    this.model.stateId = "";
                    this.model.cityId = "";
                    this.screenCode = "S-1006";
                    
                    // default form parameter for adding company
                    this.defaultFormValues = [
                        { 'fieldName': 'COMPANY_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                        { 'fieldName': 'COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                        { 'fieldName': 'POSTAL_ZIPCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                        { 'fieldName': 'COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                        { 'fieldName': 'PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                        { 'fieldName': 'FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                        { 'fieldName': 'EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                        { 'fieldName': 'STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                        { 'fieldName': 'CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                        { 'fieldName': 'ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                        { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                        { 'fieldName': 'BACK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                        { 'fieldName': 'ADD_NEW_COMPANY_WEB_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
                        { 'fieldName': 'ADD_NEW_COMPANY_LATITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
                        { 'fieldName': 'ADD_NEW_COMPANY_LONGITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                    ];
                }
                //getting screen for adding new company
                this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                    {
                        this.isScreenLock = res;
                    });
                    this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                        this.screenName=data.result.dtoScreenDetail.screenName;
                        this.moduleName=data.result.moduleName;
                        this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        }
                    });
                });
            }
            
            // event returning country data by country id
            getCountryData(event) {
                this.model.stateId = "";
                this.model.cityId = "";
                var countryId = event.target.value ? event.target.value : -1;
                this.companyService.getCountryCode(countryId).then(data => {
                    this.model.countryCode = data.result.countryCode;
                });
                this.companyService.getStateList(countryId).then(data => {
                    this.stateOptions = data.result;
                    this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
                });
            }
            
            
            
            //event returning state data by state id
            getStateData(event) {
                this.model.cityId = "";
                var stateId = event.target.value ? event.target.value : -1;
                this.companyService.getCityList(stateId).then(data => {
                    this.cityOptions = data.result;
                    this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
                });
            }
            
            getLatLong(){
                if(this.model.webAddress)
                {
                    if(this.isUrlValid(this.model.webAddress))
                    {
                        this.companyService.getlatlng(this.model.webAddress).then(data => {	
                            
                            this.lat=data.results[0].geometry.location.lat;
                            this.lng=data.results[0].geometry.location.lng;
                            this.model.latitude = data.results[0].geometry.location.lat;		
                            this.model.longitude = data.results[0].geometry.location.lng;	
                            // this.hideElement = false;	
                        });	      
                    }
                    else
                    {
                        this.model.latitude='';
                        this.model.longitude='';
                    }
                }	
                else
                {
                    this.model.latitude='';
                    this.model.longitude='';
                }
                
                
            }
            
            isUrlValid(userInput) {
                var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
                var arr = userInput.split(".");
                if(!userInput.match('www'))
                {
                    this.ShowInvalidWebAddress=true;
                    return false;
                }
                else if(arr.length < 3)
                {
                    this.ShowInvalidWebAddress=true;
                    return false;
                }
                
                if(res == null)
                {
                    this.ShowInvalidWebAddress=true;
                    return false;
                }        
                else
                {
                    this.ShowInvalidWebAddress=false;
                    return true;
                }
                
            }
            ValidURL(userInput) {
                
                var regexQuery = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";
                var url = new RegExp(regexQuery,"i");
                if (url.test(userInput)) {
                    // alert('valid url: ' + userInput);
                    this.ShowInvalidWebAddress=false;
                    return true;
                }
                
                //  alert('invalid url: ' + userInput);
                this.ShowInvalidWebAddress=true;
                return false;
            }
            
            ValidateCompany()
            {
                
                if(this.model.name && !this.isModify)
                {
                    this.companyService.checkCompanyName(this.model.name).then(data => {
                        
                        if(data.btiMessage.messageShort =='RECORD_ALREADY_EXIST')
                        {
                            this.model.name = '';
                            this.msgCompanyExist = data.btiMessage.message;
                        }
                        else{
                            this.msgCompanyExist='';
                        }
                    });
                }
            }
            // function call for adding new company
            CreateCompany(f: NgForm) {
                window.scrollTo(0, 0);
                this.showloader=true;
                this.disabledButton=true;
                if(this.model.webAddress && this.ShowInvalidWebAddress ==  true)
                {
                    return false;
                }
                if (this.companyId != '' && this.companyId != undefined) {
                    //    this.hideElement = false;
                    this.companyService.updateCompanyWithLogo(this.model, this.uploadAttachfile).then(data => {
                        var datacode = data.code;
                        this.showloader=false;
                        this.disabledButton=false;
                        if (datacode == 201) {
                            // this.hideElement = true;
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.messageText = data.btiMessage.message;
                            }, 100);
                            window.setTimeout(() => {
                                this.router.navigate(['company']);
                            }, 2000);
                            this.hasMsg = true;
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        this.showloader=false;
                        this.disabledButton=false;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        },100)
                        
                    });
                }
                else {
                    
                    this.model.isActive=true;
                    // this.hideElement = true;
                    this.companyService.createCompanyWithLogo(this.model, this.uploadAttachfile).then(data => {
                        
                        this.model.isActive=true;
                        var datacode = data.code;
                        this.showloader=false;
                        this.disabledButton=false;
                        if (datacode == 201) {
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.messageText = data.btiMessage.message;;
                            }, 100);
                            window.setTimeout(() => {
                                this.router.navigate(['company']);
                            }, 2000);
                            this.hasMsg = true;
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        this.showloader=false;
                        this.disabledButton=false;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
            }
            
            onlyDecimalNumberKey(event) {
                return this.getScreenDetailService.onlyDecimalNumberKey(event);
            }
            
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.disabledButton)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }

            uploadImage(event) {
                console.log(event);
                if (event.target.files && event.target.files[0]) {
                    let fileName = event.target.files[0].name;
                    let fileSelect = event.target.files[0];
                    var reader = new FileReader();
        
                    if (
                        (fileSelect.type == "image/png" ||
                            fileSelect.type == "image/jpg" ||
                            fileSelect.type == "image/jpeg")
                    ) {
                        let isSelected = false;
                    } else {
                        let isSelected = false;
                        let errorUpload = { isError: true, errorMessage: 'Invalid File.' };
                        return false;
                    }
                    console.log(event.target.files[0].size / 1048576 > 2);
                    if (event.target.files[0].size / 1048576 > 3) {
                        let errorUpload = { isError: true, errorMessage: 'File size exceeds 2 MB.' };
                        return false;
                    }
        
                    this.uploadAttachfile = event.target.files[0];
                    reader.readAsDataURL(event.target.files[0]); // read file as data url
                    reader.onload = (event: any) => { // called once readAsDataURL is completed 
                        this.image = event.target.result;
                        //console.log(this.url);
                    }
                }
            }
        }