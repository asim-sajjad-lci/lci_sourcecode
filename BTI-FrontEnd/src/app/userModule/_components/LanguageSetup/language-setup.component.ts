import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LanguageSetup } from '../../_models/languageSetup/language-setup';
import { LanguageSetupService } from '../../_services/languageSetup/language-setup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import {Constants} from '../../../_sharedresource/Constants';
import { Page } from '../../../_sharedresource/page';
// import * as $ from 'jquery';

@Component({
    selector: '<languge-setup></language-setup>',
    templateUrl: './language-setup.component.html',
    styles: ["language-setup.component.css"],
    providers:[LanguageSetupService,GetScreenDetailService,CommonService]
})

//export to make it available for other classes
export class LanguageSetupComponent {
    page = new Page();
    rows = new Array<LanguageSetup>();
    ddl_MultiSelectCompany = [];
    ddlCompanySetting = {};
    selectCompany = Constants.selectCompany;
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selected = [];
    SelectedCompany = [];
    moduleCode = Constants.userModuleCode;
    btnCancelText=Constants.btnCancelText;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    showBtns:boolean=false;
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    ArrShippmentStatus =[];
    arrlanguageOrientation=[];
    add;
    myHtml;
    disableId;
    arrLanguage=[];
    bodyText;
    isConfirmationModalOpen:boolean=false;
    currentSelectedRow: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    file:any;
    FinancialFile:any;
    multipartItem:any;
    isUploadAllowed:boolean=true;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    showloader:boolean=false;
    btndisabled:boolean=false;
    isScreenLock;
    constructor(
        private router: Router,
        private LanguageSetupService: LanguageSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService: CommonService,
        ){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.add=true;
        this.disableId = false;
    }

    ngOnInit() {
            this.getAddScreenDetail();
            this.getViewScreenDetail();
            this.getLanguageList({ offset: 0 });
            this.getlanguageOrientation();

            this.ddlCompanySetting = { 
                singleSelection: false, 
                text:this.selectCompany,
                selectAllText: this.selectAll,
                unSelectAllText: this.unselectAll,
                enableSearchFilter: true,
                classes:"myclass custom-class",
            };  
            this.getCompanyList();
    }

    getCompanyList()
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        if(userData.role == 'SUPERADMIN')
        {
            this.LanguageSetupService.getCompanyList().then(data => {
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    for(var i=0;i<data.result.records.length;i++)	
                    {
                        if(data.result.records[i].isActive)
                        {
                            this.ddl_MultiSelectCompany.push({ "id": data.result.records[i].tenantId, "itemName": data.result.records[i].name })
                        }
                    }
                }
            });
        }
        else{
            this.LanguageSetupService.getUserCompanyList(userData.userId).then(data => {
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    for(var i=0;i<data.result.length;i++)	
                    {
                        this.ddl_MultiSelectCompany.push({ "id": data.result[i].tenantId, "itemName": data.result[i].name })
                    }
                }
            });
        }
       
    }

    getAddScreenDetail()
    {
        this.screenCode = "U-1220";
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_LANGUAGE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_LANGUAGE_ORIENTATION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_LANGUAGE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_LANGUAGE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_LANGUAGE_EXCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_LANGUAGE_SELECT_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_LANGUAGE_DOWNLOAD_USER_MGT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_LANGUAGE_DOWNLOAD_FINANCIAL_MGT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_LANGUAGE_UPLOAD_USER_MGT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_LANGUAGE_UPLOAD_FINANCIAL_MGT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            
            ];
        
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "U-1221";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_LANGUAGE_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_LANGUAGE_ORIENTATION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_LANGUAGE_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_LANGUAGE_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_LANGUAGE_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_LANGUAGE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_LANGUAGE_COMPANY_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
     
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

   //setting pagination
    getLanguageList(pageInfo) {
      
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
      
         this.LanguageSetupService.getLanguageList(this.page).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    selectFile($event): void {
		var inputValue = $event.target;
		this.file = inputValue.files[0];
    }
    
    selectFinancialFile($event): void {
        var inputValue = $event.target;
       
		this.FinancialFile = inputValue.files[0];
	}

    getLanguageById(row: any)
    {
         this.isUploadAllowed=false;
         this.isModify=true;
         this.disableId = true;
         this.myHtml = "border-vanish";
         this.showBtns=true;
         this.LanguageSetupService.getLanguageById(row.languageId).then(data => {
            
             this.model = data.result; 
             
         });
    }

    getlanguageOrientation(){
        this.arrlanguageOrientation=[{'id':'LTR','name':'LTR'},{'id':'RTL','name':'RTL'}]
    }
    saveLanguageSetup(f: NgForm)
    {
    
       
        if(this.isModify)
        {
            let formData:FormData = new FormData(); 
            formData.append("languageId",  this.model.languageId);
            formData.append("languageName",  this.model.languageName);
            formData.append("languageOrientation",  this.model.languageOrientation);
            formData.append("file",  this.file);
             this.LanguageSetupService.ImportUserMasterDataForUpdateLanguage(formData).then(data => {
                window.scrollTo(0,0);
                this.showloader=true;
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 200) {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.showloader=true;
                    this.showBtns=false;
                    
                    if(this.SelectedCompany.length > 0 && this.FinancialFile)
                    {
                        var SelectedCompany = [];
                        for (var i = 0; i < this.SelectedCompany.length; i++) {
                            var MyCompany = this.SelectedCompany;
                            SelectedCompany.push(MyCompany[i].id);
                        }
                        var companys = SelectedCompany.join();
                        let FmformData:FormData = new FormData(); 
                        formData.append("languageId",  this.model.languageId);
                        FmformData.append("languageName",  this.model.languageName);
                        FmformData.append("languageOrientation",  this.model.languageOrientation);                    
                        FmformData.append("file",  this.FinancialFile);
                        formData.append("dbNames",  companys);
                        this.LanguageSetupService.ImportCompanyMasterDataForUpdateLanguage(FmformData).then(data => {
                            this.showloader=false;
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                           this.getLanguageList({ offset: 0 });
                           this.file=[];
                           this.isUploadAllowed=true;
                           var  UpdImport = <HTMLInputElement>document.getElementById("UpdImport")
                           UpdImport.value = "";
    
                           var  UpdFMImport = <HTMLInputElement>document.getElementById("UpdFMImport")
                           UpdFMImport.value = "";
    
                           this.FinancialFile=[];
                           this.isModify=false;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        });
                       
                    }
                    else{
                        this.showloader=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                       this.getLanguageList({ offset: 0 });
                       this.file=[];
                       this.isUploadAllowed=true;
                       var  UpdImport = <HTMLInputElement>document.getElementById("UpdImport")
                       UpdImport.value = "";
    
                       var  UpdFMImport = <HTMLInputElement>document.getElementById("UpdFMImport")
                       UpdFMImport.value = "";
    
                       this.FinancialFile=[];
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    }
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                var selectedValue = <HTMLInputElement>document.getElementById("txtshipmentMethodId")
                selectedValue=this.model.shipmentMethodType;
             }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else{
            
            if(f.valid && this.SelectedCompany.length > 0 && this.file && this.FinancialFile)
            {
                var SelectedCompany = [];
                for (var i = 0; i < this.SelectedCompany.length; i++) {
                    var MyCompany = this.SelectedCompany;
                    SelectedCompany.push(MyCompany[i].id);
                }
                var companys = SelectedCompany.join();
                let formData:FormData = new FormData(); 
                formData.append("languageName",  this.model.languageName);
                formData.append("languageOrientation",  this.model.languageOrientation);
                formData.append("file",  this.file);
                 this.LanguageSetupService.ImportUserMasterData(formData).then(data => {
                    
                    window.scrollTo(0,0);
                    this.showloader=true;
                    var datacode = data.code;
                    this.btndisabled=false;
                      if (datacode == 200) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showloader=true;
                        this.showBtns=false;
                        if(this.SelectedCompany.length > 0)
                        {
                            let FmformData:FormData = new FormData(); 
                            FmformData.append("languageName",  this.model.languageName);
                            FmformData.append("languageOrientation",  this.model.languageOrientation);                    
                            FmformData.append("file",  this.FinancialFile);
                            formData.append("dbNames",  companys);
                            this.LanguageSetupService.ImportFinancialMasterData(FmformData).then(data => {
                                
                                this.showloader=false;
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                               this.getLanguageList({ offset: 0 });
                               this.file=[];
                               this.isUploadAllowed=true;
                               var  UpdImport = <HTMLInputElement>document.getElementById("UpdImport")
                               UpdImport.value = "";
        
                               var  UpdFMImport = <HTMLInputElement>document.getElementById("UpdFMImport")
                               UpdFMImport.value = "";
        
                               this.FinancialFile=[];
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            });
                        }
                        else{
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                           this.getLanguageList({ offset: 0 });
                           this.file=[];
                           this.isUploadAllowed=true;
                           var  UpdImport = <HTMLInputElement>document.getElementById("UpdImport")
                           UpdImport.value = "";
        
                           var  UpdFMImport = <HTMLInputElement>document.getElementById("UpdFMImport")
                           UpdFMImport.value = "";
        
                           this.FinancialFile=[];
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        }
                        
                           
                    }
                    else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                    var selectedValue = <HTMLInputElement>document.getElementById("txtshipmentMethodId")
                    selectedValue=this.model.shipmentMethodType;
                 }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                 });
            }
          
        }
    }
    
    saveLanguageSetup2(f: NgForm)
    {
        if(this.isModify)
        {
            this.disableId = false;
             this.LanguageSetupService.updateLanguageSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode =  data.btiMessage.messageShort;
                  if (datacode == 'LANGUAGE_UPDATED') {
                        this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                       this.getLanguageList({ offset: 0 });
                        f.resetForm();
                        this.isUploadAllowed=true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
            let formData:FormData = new FormData(); 
		    formData.append("languageName",  this.model.languageName);
			formData.append("languageOrientation",  this.model.languageOrientation);
            formData.append("file",  this.file);
            
             this.LanguageSetupService.ImportUserMasterData(formData).then(data => {
                
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                    if(this.SelectedCompany.length > 0)
                    {
                        let FmformData:FormData = new FormData(); 
                        FmformData.append("languageName",  this.model.languageName);
                        FmformData.append("languageOrientation",  this.model.languageOrientation);                    
                        FmformData.append("file",  this.FinancialFile);
                        this.LanguageSetupService.ImportFinancialMasterData(FmformData).then(data => {
                            
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                           this.getLanguageList({ offset: 0 });
                           this.file=[];
                           this.isUploadAllowed=true;
                           var  UpdImport = <HTMLInputElement>document.getElementById("UpdImport")
                           UpdImport.value = "";
    
                           var  UpdFMImport = <HTMLInputElement>document.getElementById("UpdFMImport")
                           UpdFMImport.value = "";
    
                           this.FinancialFile=[];
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        });
                    }
                    else{
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                       this.getLanguageList({ offset: 0 });
                       this.file=[];
                       this.isUploadAllowed=true;
                       var  UpdImport = <HTMLInputElement>document.getElementById("UpdImport")
                       UpdImport.value = "";

                       var  UpdFMImport = <HTMLInputElement>document.getElementById("UpdFMImport")
                       UpdFMImport.value = "";

                       this.FinancialFile=[];
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    }
                    
                       
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                var selectedValue = <HTMLInputElement>document.getElementById("txtshipmentMethodId")
                selectedValue=this.model.shipmentMethodType;
             }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }
    
    changePageSize(event) {
            this.page.size = event.target.value;
            this.getLanguageList({ offset: 0 });
    }


    clearForm()
    {
        this.showMsg = false;
        this.hasMsg = false;
        this.messageText ='';
        this.model.languageName='';
        this.model.languageOrientation='';
        this.isUploadAllowed=true;
        this.file=[];
        this.FinancialFile=[];
        this.SelectedCompany = [];
        
        var  UpdImport = <HTMLInputElement>document.getElementById("UpdImport")
        UpdImport.value = "";

        var  UpdFMImport = <HTMLInputElement>document.getElementById("UpdFMImport")
        UpdFMImport.value = "";

    }
    
    Cancel(f){
        f.resetForm();
        this.clearForm();
        this.isModify=false;
    }
    varifyOperation(row: any)
    {
         this.isConfirmationModalOpen = true;
         this.currentSelectedRow=row;
    }
    updateStatus() {
             this.LanguageSetupService.blockUnblock(this.currentSelectedRow.languageId,this.currentSelectedRow.isActive).then(data => {
                  var datacode = data.code;
                    if (datacode == 200) {
                        this.getLanguageList({ offset: 0 });
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.messageText = data.btiMessage.message;
                        }, 100);
                            this.hasMsg = true;
                            this.isConfirmationModalOpen = false;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                        this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText =  Constants.serverErrorText;
                    }, 100)
                });
        }
    
    ExportUserMasterData()
    {
       
        this.showloader=true;
        var ExportFileApiName = ""
        if(this.isModify)
        {
            ExportFileApiName = "exportUserMasterDataForUpdateLanguage" 
            this.commonService.ExportDataForUpdate(Constants.userModuleApiBaseUrl+ExportFileApiName,this.model.languageId)
            .subscribe(blob => {
                this.showloader=false;
                this.downloadFile(blob,'Edit User Master')
             },
            )
        }
        else{
            ExportFileApiName = "exportMasterData"
            this.commonService.ExportData(Constants.userModuleApiBaseUrl+ExportFileApiName)
            .subscribe(blob => {
                this.showloader=false;
                this.downloadFile(blob,'User Master')
             },
            )
        }

       
    };

    ExportFinancialMasterData()
    {
       
        this.showloader=true;
        if(this.isModify)
        {
            this.commonService.ExportDataForUpdate(Constants.financialModuleApiBaseUrl+'exportCompanyMasterDataForUpdateLanguage',this.model.languageId)
            .subscribe(blob => {
                this.showloader=false;
                this.downloadFile(blob,'Edit Financial Master')
             },
            )
        }
        else{
            this.commonService.ExportData(Constants.financialModuleApiBaseUrl+'exportCompanyMasterData')
            .subscribe(blob => {
                this.showloader=false;
                this.downloadFile(blob,'Financial Master')
             },
            )
        }

       
    };
    

    downloadFile(data: any,FileName:string){
        
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(data);
        link.download=FileName +'_'+ mm + '-' + dd + '-' + yyyy + ".xlsx";
        link.click();
      }
    

    closeModal()
    {
         this.isConfirmationModalOpen = false;
    }

    onItemSelect(item:any){
        //this.SelectedCompany.push(item.id);
       }
       OnItemDeSelect(item:any){
      //  this.SelectedCompany=[];
       }
       onSelectAll(items: any){
       }
       onDeSelectAll(items: any){
           
       }

/** If Screen is Lock then prevent user to perform any action.
*  This function also cover Role Management Write acceess functionality */
LockScreen(writeAccess)
{
    if(!writeAccess)
    {
        return true
    }
    else if(this.btndisabled)
    {
        return true
    }
    else if(this.isScreenLock)
    {
        return true;
    }
    else{
        return false;
    }
}
}