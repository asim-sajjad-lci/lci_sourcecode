import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LanguageRoutingModule } from './language-routing.module';
import { LanguageSetupComponent } from './language-setup.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    LanguageRoutingModule,
    AngularMultiSelectModule
  ],
  declarations: [LanguageSetupComponent,AutoFocusDirective]
})
export class LanguageSetupModule { }
