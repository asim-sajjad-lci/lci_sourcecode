import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthSettingComponent } from './auth-setting.component';
import { CreateAuthSettingComponent } from './create-auth-setting.component';

const routes: Routes = [
  { path: '', component: AuthSettingComponent },
  { path: 'authSetting', component: AuthSettingComponent },
  { path: 'createauthSetting', component: CreateAuthSettingComponent },
  { path: 'createauthSetting/:authId', component: CreateAuthSettingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthSettingRoutingModule { }
