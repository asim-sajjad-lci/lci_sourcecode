
import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AuthSettingRoutingModule } from './auth-setting-routing.module';
import { AuthSettingComponent } from './auth-setting.component';
import { CreateAuthSettingComponent } from './create-auth-setting.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
// import { DialogModule } from "../../../generic-component/ngx-dialog/dialog.module";
@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    AuthSettingRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule,
    MultiselectDropdownModule,
   // DialogModule
  ],
  declarations: [AuthSettingComponent, CreateAuthSettingComponent,AutoFocusDirective]
})
export class AuthSettingModule { }
