import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import {NgForm} from '@angular/forms';
import { AuthSetting } from '../../_models/authsetting/authsetting';
import { AuthSettingService } from '../../_services/authsetting/authsetting.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import * as moment from 'moment';
import {Constants} from '../../../_sharedresource/Constants';


@Component({
    selector: 'app-create-auth-setting',
    templateUrl: './create-auth-setting.component.html',
    providers: [AuthSettingService,GetScreenDetailService,CommonService]
})

// export to make it available for other classes
export class CreateAuthSettingComponent {
    moduleName;
    screenName;
    moduleCode = Constants.userModuleCode;
    screenCode;
    defaultFormValues:any = [];
    availableFormValues:any = [];
    SelectedUser: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    roleGroupId: string;
    lblRole = "Create Group";
    companyId=0;
    AuthorizedUserGroupId=0;
    authId: string;
    LstCompany;
    LstUserGroup;
    LstGroup;
    LstUserList;
    ddOptions: IMultiSelectOption[];
    ddUsersOption: IMultiSelectOption[];
    LstDays:IMultiSelectOption[];
    LstUsers:IMultiSelectOption[];
    alertmessage = Constants.alertmessage;
    validationMessage = Constants.validationMessage;
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selectUser =Constants.selectUser;
    selectDays= Constants.selectDays;
    
    btndisabled:boolean=false;
    isScreenLock;
    // selectCompany = Constants.selectCompany;
    select=Constants.select; 
    ddl_MultiSelectUser = [];
    SelectedUsers = [];
    ddlCompanySetting = {};
    // Settings configuration
    ddSettings: IMultiSelectSettings = {
        buttonClasses: 'btn dropdown-toggle formm-dd',
        enableSearch: true,
        showCheckAll: true,
        showUncheckAll: true
    };
    // Text configuration
    ddTexts: IMultiSelectTexts = {
        defaultTitle: this.selectUser,
        checkAll: this.selectAll,
        uncheckAll: this.unselectAll,
    };
    
    // DaysText configuration 
    ddDaysTexts: IMultiSelectTexts = {
        defaultTitle: this.selectDays,
        checkAll: this.selectAll,
        uncheckAll: this.unselectAll,
    };
    
    
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
    
    myOptions2: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year:0, month: 0, day: 0 }
    };
    
    // Initialized to specific date (09.10.2018)
    private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
    private momentVariable = null;
    
    // optional date changed callback
    onDateChanged(event: IMyDateModel): void {
        // date selected
        this.model.startTime='';
        this.model.endTime='';
        this.myOptions2.disableUntil = {year: event.date.year, month: event.date.month, day: event.date.day - 1 }
    }
    
    onDateChanged2(event: IMyDateModel): void {
        this.model.startTime='';
        this.model.endTime='';
        // date selected
    }
    clearStartDate()
    {
        this.model.startDate='';
        this.model.startTime='';
        this.model.endTime='';
    }
    clearEndDate()
    {
        this.model.endDate='';
        this.model.startTime='';
        this.model.endTime='';
    }
    
    // Returns copy of myOptions
    getCopyOfOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.myOptions2));
    }
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authSettingService:AuthSettingService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService,
    ){
    }
    
    // Screen initialization 
    ngOnInit() {
        
        this.getAllCompany();
        this.getDays();
        
        this.SelectedUser = [{'userId':'0','firstName':''}];
        this.route.params.subscribe((params: Params) => {
            this.authId = params['authId'];
            
            if (this.authId != '' && this.authId != undefined) 
            {
                //defaul form parameter for edit screen
                
                this.defaultFormValues = [
                    
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_AUTHORIZEDTO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_USER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_DATES_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_DATES_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_TIME_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_TIME_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_EDIT_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                ];
                
                this.screenCode = "S-1030";
                
                
                
                this.authSettingService.getAuthSetting(this.authId).then(data => {
                    this.model = data.result;
                    this.model.companyId = this.model.companyId;
                    this.changeCompany();
                    this.model.authorizedUserGroupId=this.model.authorizedUserGroupId;
                    this.changeUserGroup();
                    var allUserLst=data.result.usersList;
                    for(var i=0;i<allUserLst.length;i++)
                    {
                        this.SelectedUsers.push({"id":allUserLst[i].userId,"itemName":allUserLst[i].firstName +' ' + allUserLst[i].lastName });
                    }
                    
                    if(data.result.startTime) {
                        this.model.startTime=data.result.startTime;
                    }
                    if(data.result.endTime) {
                        this.model.endTime =data.result.endTime;
                    }
                    var startdate = this.model.startDate;
                    var startdateData = startdate.split('/');
                    this.model.startDate = {"date":{"year":parseInt(startdateData[2]),"month":parseInt(startdateData[1]),"day":parseInt(startdateData[0])}};
                    
                    var enddate = this.model.endDate;
                    var enddateData = enddate.split('/');
                    this.model.endDate = {"date":{"year":parseInt(enddateData[2]),"month":parseInt(enddateData[1]),"day":parseInt(enddateData[0])}};
                });
            }
            else {
                this.screenCode = "S-1024";
                
                
                // default form parameter for adding company
                this.defaultFormValues = [
                    { 'fieldName': 'AUTHORIZED_SETTING_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_AUTHORIZEDTO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_USER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_SELECT_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_SELECT_DATES_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_SELECT_DATES_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_SELECT_TIME_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_SELECT_TIME_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'AUTHORIZED_SETTING_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
                ];
            }
            
               this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
                //getting screen for adding new company
                this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                    this.screenName=data.result.dtoScreenDetail.screenName;
                    this.moduleName=data.result.moduleName;
                    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                });
            });
            /*this.ddlCompanySetting = { 
                singleSelection: false, 
                text:this.selectUser,
                disabled : true,
                selectAllText: this.selectAll,
                unSelectAllText: this.unselectAll,
                enableSearchFilter: true,
                classes:"myclass custom-class"
            };  */
        }
        
        onItemSelect(item:any){
            //   this.commonService.closeMultiselect()
        }
        OnItemDeSelect(item:any){
            //  this.commonService.closeMultiselect()
        }
        onSelectAll(items: any){
            // this.commonService.closeMultiselect()
        }
        onDeSelectAll(items: any){
            //  this.commonService.closeMultiselect() 
        }
        
        // get all company
        getAllCompany()
        {
            this.authSettingService.getCompanyList().then(data => {
                
                if(data.btiMessage.messageShort == 'SESSION_EXPIRED')
                {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.router.navigate(['login']);
                    }, 100);
                }
                this.LstCompany = data.result.records;
                this.LstCompany.splice(0, 0, { "id": "", "name": this.select });
                
            });
            this.model.startTime = '';
            this.model.endTime = '';
            
        }
        
        // select checkbox for user
        SelectUser(option, event)
        {
            if(event.target.checked)
            {
                this.SelectedUser.push(option); 
                
            }
        }
        
        // Get all user group by company 
        changeCompany()
        {
            this.SelectedUsers=[];
            this.ddl_MultiSelectUser=[];
            
            this.companyId=this.model.companyId;
            this.authSettingService.getUserGroupListByCompany(this.model.companyId).then(data => {
                this.LstGroup = data.result;
            });
        }
        
        changeAuthsetting()
        {
            this.AuthorizedUserGroupId=this.model.authorizedUserGroupId;
            this.authSettingService.getUserGroupListByAuth(this.model.authorizedUserGroupId).then(data => {
                this.LstGroup = data.result;
            });
        }
        // Get all users group by user group 
        changeUserGroup()
        {
            this.SelectedUsers=[];
            this.ddl_MultiSelectUser=[];
            this.authSettingService.getUserListByUserGroup(this.model.authorizedUserGroupId,this.companyId).then(data => {
                this.ddUsersOption=this.bindUsers(data.result);
                var allUserLst=data.result;
                for(var i=0;i<allUserLst.length;i++)
                {
                    this.ddl_MultiSelectUser.push({"id":allUserLst[i].userId,"itemName":allUserLst[i].firstName +' ' + allUserLst[i].lastName });
                }
            });
        }
        
        // Bind user data
        private bindUsers(data: any): IMultiSelectOption[] {
            let roleDataObj: IMultiSelectOption;
            let roleData = new Array<IMultiSelectOption>();
            for (let i = 0; i < data.length; i++) {
                roleDataObj = { 'id': data[i].userId, 'name': data[i].firstName };
                roleData.push(roleDataObj);
            }
            return roleData;
        }
        
        // Get all days
        getDays()
        {
            this.authSettingService.getDays().subscribe(data => {
                this.ddOptions = data;
            });
        }
        
        formatstartTime(selectedTime) { 
            
            this.model.startTime = moment(selectedTime).format('HH:mm');
            if(this.model.startDate == undefined || this.model.endDate == undefined || this.model.startDate == '' || this.model.endDate == '')
            {
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.validationMessage;
                this.model.startTime = '';
                window.scrollTo(0,0);
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                return false;
            }
            this.route.params.subscribe((params: Params) => {
                this.authId = params['authId'];
                
                if (this.authId != '' && this.authId != undefined) {
                    if(this.model.startDate.formatted == undefined)
                    {
                        var startdate = this.model.startDate;
                        if(startdate.date != undefined)
                        {
                            this.model.startDate = startdate.date.day +'/'+ startdate.date.month +'/'+ startdate.date.year;
                        }
                    }
                    else
                    {
                        this.model.startDate = this.model.startDate.formatted;
                    }
                    
                    if(this.model.endDate.formatted == undefined)
                    {
                        var enddate = this.model.endDate;
                        if(enddate.date != undefined)
                        {
                            this.model.endDate = enddate.date.day +'/'+ enddate.date.month +'/'+ enddate.date.year;
                        }
                    }
                    else
                    {
                        this.model.endDate = this.model.endDate.formatted;
                    }
                    if(this.model.startDate == this.model.endDate)
                    {
                        
                        if(this.model.startTime.length > 0 && this.model.endTime.length > 0  && this.model.startTime >=  this.model.endTime )
                        {
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = this.alertmessage;
                            this.model.startTime = '';
                            window.scrollTo(0,0);
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else
                        {
                            this.model.startTime = moment(selectedTime).format('HH:mm');
                        }
                    }
                }
                else
                {
                    if(this.model.startDate.formatted == this.model.endDate.formatted)
                    {
                        
                        
                        if(this.model.startTime.length > 0 && this.model.endTime.length > 0  && this.model.startTime >=  this.model.endTime )
                        {
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = this.alertmessage;
                            this.model.startTime = '';
                            window.scrollTo(0,0);
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else
                        {
                            this.model.startTime = moment(selectedTime).format('HH:mm');
                        }
                    }
                    
                }
            });
            //{year: event.date.year, month: event.date.month, day: event.date.day - 1 }
            
            
        }
        
        formatendTime(selectedTime) { 
            
            this.model.endTime = moment(selectedTime).format('HH:mm');
            if(this.model.startDate == undefined || this.model.endDate == undefined || this.model.startDate == '' || this.model.endDate == '')
            {
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.validationMessage;
                this.model.endTime = '';
                window.scrollTo(0,0);
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                return false;
            }
            this.route.params.subscribe((params: Params) => {
                this.authId = params['authId'];
                
                if (this.authId != '' && this.authId != undefined) {
                    if(this.model.startDate.formatted == undefined)
                    {
                        var startdate = this.model.startDate;
                        if(startdate.date != undefined)
                        {
                            this.model.startDate = startdate.date.day +'/'+ startdate.date.month +'/'+ startdate.date.year;
                        }
                    }
                    else
                    {
                        this.model.startDate = this.model.startDate.formatted;
                    }
                    
                    if(this.model.endDate.formatted == undefined)
                    {
                        var enddate = this.model.endDate;
                        if(enddate.date != undefined)
                        {
                            this.model.endDate = enddate.date.day +'/'+ enddate.date.month +'/'+ enddate.date.year;
                        }
                        
                    }
                    else
                    {
                        this.model.endDate = this.model.endDate.formatted;
                    }
                    if(this.model.startDate == this.model.endDate)
                    {
                        
                        if(this.model.startTime.length > 0 && this.model.endTime.length > 0  && this.model.startTime >=  this.model.endTime )
                        {
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = this.alertmessage;
                            this.model.endTime = '';
                            window.scrollTo(0,0);
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else
                        {
                            this.model.endTime = moment(selectedTime).format('HH:mm');
                        }
                    }
                }
                else
                {
                    if(this.model.startDate.formatted == this.model.endDate.formatted)
                    {
                        if(this.model.startTime.length > 0 && this.model.startTime >=  this.model.endTime)
                        {
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = this.alertmessage;
                            this.model.endTime = '';
                            window.scrollTo(0,0);
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else
                        {
                            this.model.endTime = moment(selectedTime).format('HH:mm');
                        }
                    }
                }});
            }
            // function call for adding new company
            CreateAuthSetting(f: NgForm) {
                this.btndisabled=true;
                if(this.SelectedUsers.length == 0)
                {
                    return false;
                }
                var selectedUserIds = [];
                for (var i = 0; i < this.SelectedUsers.length; i++) {
                    var MyCompany = this.SelectedUsers;
                    
                    selectedUserIds.push(MyCompany[i].id);
                }
                // var ArrSelectedUsers = SelectedUsersIds.join();
                this.model.userIds=selectedUserIds;
                if (this.authId != '' && this.authId != undefined) {
                    if(this.model.startDate.formatted == undefined)
                    {
                        var startdate = this.model.startDate;
                        if(startdate.date != undefined)
                        {
                            this.model.startDate = startdate.date.day +'/'+ startdate.date.month +'/'+ startdate.date.year;
                        }
                        
                    }
                    else
                    {
                        this.model.startDate = this.model.startDate.formatted;
                    }
                    
                    if(this.model.endDate.formatted == undefined)
                    {
                        var enddate = this.model.endDate;
                        if(enddate.date != undefined)
                        {
                            this.model.endDate = enddate.date.day +'/'+ enddate.date.month +'/'+ enddate.date.year;
                        }
                    }
                    else
                    {
                        this.model.endDate = this.model.endDate.formatted;
                    }
                    
                    
                    this.model.id = this.model.authSettingId;
                    this.authSettingService.UpdateAuthSetting(this.model).then(data => {
                        this.btndisabled=false;
                        var datacode = data.code;
                        
                        if (datacode == 200) {
                            
                            window.scrollTo(0,0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                // this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                            }, 100);
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            window.setTimeout(() => {
                                this.router.navigate(['authSetting']);
                            }, 2000);
                            this.hasMsg = true;
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        },100)
                        
                    });
                }
                else {
                    
                    this.model.startDate = this.model.startDate.formatted;
                    this.model.endDate = this.model.endDate.formatted;
                    this.authSettingService.saveAuthSetting(this.model).then(data => {
                        this.btndisabled=false;
                        window.scrollTo(0,0);
                        var datacode = data.code;
                        if (datacode == 200) {
                            
                            // window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            // }, 1000);
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                                this.router.navigate(['authSetting']);
                            }, 4000);
                            // f.resetForm();
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
            }
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
        }
        