import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { activityLog } from '../../_models/activityLog/activityLog';
import { activityLogService } from '../../_services/activityLog/activityLog.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
@Component({
    selector: 'activityLog',
    templateUrl: './activityLog.component.html',
    providers: [activityLogService]
})

// export Department component to make it available for other classes
export class ActivityLogComponent {
    page = new Page();
    rows = new Array<activityLog>();
    temp = new Array<activityLog>();
    selected = [];
    moduleCode = "M-UMG";
    screenCode = "S-1428";
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    departmentId = {};
    searchKeyword = '';
	getDepartment:any[]=[];
    ddPageSize: number = 5;
    model: activityLog;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    departmentIdvalue : string;
	departmentIdList: Observable<any>;
	typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(
        private router: Router,
        private activityLogService: activityLogService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for department  screen
        // this.defaultFormValues = [
        //     { 'fieldName': 'DEPARTMENT_SEARCH', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_ACTION', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'DEPARTMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
        //     { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        // ];
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
	    this.page.searchKeyword = '';
        this.activityLogService.getlist(this.page,this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

   

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }

}