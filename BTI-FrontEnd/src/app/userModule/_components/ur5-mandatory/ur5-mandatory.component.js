"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var page_1 = require("../../../_sharedresource/page");
var department_service_1 = require("../../_services/department/department.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var Constants_1 = require("../../../_sharedresource/Constants");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var UR5MandatoryComponent = (function () {
    function UR5MandatoryComponent(router, departmentService, getScreenDetailService, alertService) {
        this.router = router;
        this.departmentService = departmentService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = "M-1011";
        this.screenCode = "S-1220";
        this.message = { 'type': '', 'text': '' };
        this.departmentId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'DEPARTMENT_SEARCH', 'fieldValue': '', 'helpMessage': '', 'isMandatory': '' },
            { 'fieldName': 'DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '', 'isMandatory': '' },
            { 'fieldName': 'DEPARTMENT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    // Screen initialization
    UR5MandatoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                _this.mandatory = _this.availableFormValues[j]['isMandatory'];
                console.log('mandatory' + _this.mandatory);
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                // var objMandatoryAvailable = this.availableFormValues.find(x => x['isMandatory'] === this.mandatory);
                // var objMandatoryDefault = this.defaultFormValues.find(x => x['isMandatory'] === this.mandatory);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['isMandatory'] = objAvailable['isMandatory'];
                if (objAvailable['listDtoFieldValidationMessage'] && objAvailable['isMandatory']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    // setting pagination
    UR5MandatoryComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        }
        else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        }
        else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.departmentService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create department
    UR5MandatoryComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            arabicDepartmentDescription: '',
            departmentDescription: '',
            departmentId: ''
        };
    };
    // Clear form to reset to default blank
    UR5MandatoryComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    // function call for creating new department
    UR5MandatoryComponent.prototype.CreateDepartment = function (f, event) {
        var _this = this;
        event.preventDefault();
        var deptIdx = this.model.departmentId;
        // Check if the id is available in the model.
        // If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            // Check for duplicate Division Id according to it create new division
            this.departmentService.checkDuplicateDeptId(deptIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = "success";
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    // Call service api for Creating new department
                    _this.departmentService.createDepartment(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                _this.hasMessage = false;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new department
                            _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = "Server error. Please contact admin.";
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = "Server error. Please contact admin.";
                }, 100);
            });
        }
    };
    // edit department by row
    UR5MandatoryComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.departmentId = row.departmentId;
        this.departmentIdvalue = this.model.departmentId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    UR5MandatoryComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        // Call service api for updating selected department
        this.model.departmentId = this.departmentIdvalue;
        this.departmentService.updateDepartment(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                // Refresh the Grid data after editing department
                _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
                // Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMessage = false;
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = "Server error. Please contact admin.";
            }, 100);
        });
    };
    UR5MandatoryComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    // delete department by passing whole object of perticular Department
    UR5MandatoryComponent.prototype.delete = function () {
        var _this = this;
        var selectedDepartments = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDepartments.push(this.selected[i].id);
        }
        this.departmentService.deleteDepartment(selectedDepartments).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
            }
            _this.hasMessage = true;
            _this.message.type = "success";
            // this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            // Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = "error";
            var errorCode = error.status;
            _this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    };
    // default list on page
    UR5MandatoryComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    UR5MandatoryComponent.prototype.updateFilter = function (event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    };
    // Set default page size
    UR5MandatoryComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    };
    UR5MandatoryComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    };
    UR5MandatoryComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    UR5MandatoryComponent.prototype.sortColumn = function (val) {
        if (this.page.sortOn === val) {
            if (this.page.sortBy === 'DESC') {
                this.page.sortBy = 'ASC';
            }
            else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    };
    return UR5MandatoryComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], UR5MandatoryComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], UR5MandatoryComponent.prototype, "myScrollContainer", void 0);
UR5MandatoryComponent = __decorate([
    core_1.Component({
        selector: 'ur5',
        templateUrl: './ur5-mandatory.component.html',
        providers: [department_service_1.DepartmentService]
    })
    // export Department component to make it available for other classes
    ,
    __metadata("design:paramtypes", [router_1.Router,
        department_service_1.DepartmentService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], UR5MandatoryComponent);
exports.UR5MandatoryComponent = UR5MandatoryComponent;
//# sourceMappingURL=ur5-mandatory.component.js.map