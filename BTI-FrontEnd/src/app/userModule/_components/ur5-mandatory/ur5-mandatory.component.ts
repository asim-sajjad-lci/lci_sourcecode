import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Department } from '../../../hcm/_models/department/department';
import { DepartmentService } from '../../../hcm/_services/department/department.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';

@Component({
    selector: 'ur5',
    templateUrl: './ur5-mandatory.component.html',
    providers: [DepartmentService]
})

// export Department component to make it available for other classes
export class UR5MandatoryComponent {
    page = new Page();
    rows = new Array<Department>();
    temp = new Array<Department>();
    selected = [];
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1266";
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    departmentId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: Department;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    departmentIdvalue : string;
    mandatory: boolean;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(
        private router: Router,
        private departmentService: DepartmentService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'DEPARTMENT_SEARCH', 'fieldValue': '', 'helpMessage': '' , 'isMandatory': ''},
            { 'fieldName': 'DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' , 'isMandatory': ''},
            { 'fieldName': 'DEPARTMENT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                this.mandatory = this.availableFormValues[j]['isMandatory'];
                console.log('mandatory' + this.mandatory);

                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);

                // var objMandatoryAvailable = this.availableFormValues.find(x => x['isMandatory'] === this.mandatory);
                // var objMandatoryDefault = this.defaultFormValues.find(x => x['isMandatory'] === this.mandatory);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['isMandatory'] = objAvailable['isMandatory'];
                if (objAvailable['listDtoFieldValidationMessage'] && objAvailable['isMandatory']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if ( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.departmentService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            arabicDepartmentDescription: '',
            departmentDescription: '',
            departmentId: ''
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }

    // function call for creating new department
    CreateDepartment(f: NgForm, event: Event) {
        event.preventDefault();
        var deptIdx = this.model.departmentId;

        // Check if the id is available in the model.
        // If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            // Check for duplicate Division Id according to it create new division
            this.departmentService.checkDuplicateDeptId(deptIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    // Call service api for Creating new department
                    this.departmentService.createDepartment(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new department
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }

    // edit department by row
    edit(row: Department) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.isUnderUpdate = true;
        this.departmentId = row.departmentId;
        this.departmentIdvalue = this.model.departmentId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        // Call service api for updating selected department
        this.model.departmentId = this.departmentIdvalue;
        this.departmentService.updateDepartment(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                // Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    // delete department by passing whole object of perticular Department
    delete() {
        var selectedDepartments = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDepartments.push(this.selected[i].id);
        }
        this.departmentService.deleteDepartment(selectedDepartments).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = "success";
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn === val ) {
            if (this.page.sortBy === 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}