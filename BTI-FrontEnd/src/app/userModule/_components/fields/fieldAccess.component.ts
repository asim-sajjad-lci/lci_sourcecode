import {Component} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { NgForm } from '@angular/forms';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';
import {CompanyService} from '../../_services/companymanagement/company.service';
import {FieldAccessService} from '../../_services/fieldAccess/field-access.service';
import {Fields} from '../../_models/fields/fields.module';
import {Page} from '../../../_sharedresource/page';

@Component({
    selector: 'fieldAccess',
    styles: [`
        agm-map {
            height: 300px;
        }
    `],
    templateUrl: './fieldAccess.component.html',
    providers: [CompanyService, FieldAccessService],
})

export class FieldAccessComponent {
    fieldAccessId;
    page = new Page();
    rows = new Array<Fields>();
    temp = new Array<Fields>();
    selected = [];
    fieldId: string;
    moduleCode = Constants.userModuleCode;
    companyId: string;
    screenCode = 'S-1405';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    fieldAccess;
    PageNumber: 0;
    PageSize = 1;
    moduleId;
    hasMessage;
    message = { 'type': '', 'text': '' };
    screenId = [];
    currentLanguage: any;
    LstScreenModule: any = [];
    showloader: boolean= false;
    arrLanguage: [object];
    fieldAccesses: any = [];
    isDeleteAction: boolean = false;
    isConfirmationModalOpen: boolean = false;
    isMandatory: boolean = false;
    rowIndex: number;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private companyService: CompanyService,
        private getScreenDetailService: GetScreenDetailService,
        private fieldAccessService: FieldAccessService
    ) {
        //  var linkId =  <HTMLAnchorElement>document.getElementById("role");
        // linkId.className="active";
    }

    // Screen Initialization
    ngOnInit() {
        this.getScreenDetailService.getLanguageListForDropDown().subscribe(currentdata => {
            this.arrLanguage = currentdata;
        });

        this.currentLanguage = localStorage.getItem('currentLanguage');

        this.route.params.subscribe((params: Params) => {
            this.companyId = params['companyId'];

        });

        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.moduleName = data.result.moduleName;
            this.moduleId = data.result.moduleId;
            this.model.screenName = data.result.dtoScreenDetail.screenName;
            this.model.screenId = data.result.dtoScreenDetail.screenId;
            console.log('ModuleId: ' + this.moduleId, 'ScreenId: ' + this.model.screenId)
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
            }
        });

        // Load all fields based on selected company
        this.GetFieldsByCompanyScreenModule();
    }

    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
    }

    GetFieldsByCompanyScreenModule() {
        this.fieldAccessService.getListOfFieldsByCompanyScreenModule(0, 1).subscribe(data => {
            // this.model.moduleId = data.result.moduleId;
            /* this.screenId = data.result.screensList;
             console.log('screen ids');
             console.log(this.screenId);*/
            if (data.btiMessage.messageShort === 'SESSION_EXPIRED') {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.router.navigate(['login']);
                }, 100);
            }
            this.LstScreenModule = data.result;

        });
    }

    changeLanguages() {
        this.showloader = true;
        console.log('selected: ' + this.selected);
        this.model.modulesList = this.LstScreenModule;
        var selectedLanguages = [];
        var languageIds = {};

        for (var i = 0; i < this.selected.length; i++) {
            if (this.selected[i] !== undefined && this.selected[i] !== null /*&& this.selected[i].fieldId !== undefined && this.selected[i].fieldId !== null && this.selected[i].fieldId > 0*/) {
                languageIds = {
                    fieldId: this.selected[i].fieldId,
                    languageId: this.selected[i].languageId,
                }

                selectedLanguages.push(languageIds);
            }
        }

        console.log('---selectedLanguages---');
        console.log(selectedLanguages);
        this.fieldAccessService.changeLanguageIds(selectedLanguages).then(data => {
            // this.fieldAccess = data.result;
            // console.log(selectedLanguages);
            var datacode = data.code;
            if (datacode === 201) {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                this.showloader = false;
                this.messageText = data.btiMessage.message;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.showloader = false;
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100);

        });
    }

    saveFieldAccess() {
        this.showloader = true;
        console.log('selected: ' + this.selected);
        this.model.modulesList = this.LstScreenModule;
        var selectedFields = [];
        var field = {};

        for (var i = 0; i < this.selected.length; i++) {
            console.log('selected i');
            console.log(this.selected[i]);
            console.log('moduleId: ' + this.moduleId)
            if (this.selected[i] !== undefined && this.selected[i] !== null /*&& this.selected[i].fieldId !== undefined && this.selected[i].fieldId !== null && this.selected[i].fieldId > 0*/) {
                field = {
                    fieldId: this.selected[i].fieldId,
                    languageId: this.selected[i].languageId,
                    isMandatory: this.selected[i].isMandatory,
                    companyId: this.selected[i].companyId,
                    moduleId: this.moduleId,
                    screenId: this.model.screenId
                }

                selectedFields.push(field);
            }
        }

        // console.log(this.companyId, this.moduleId, this.screenId);
        console.log('--------------');
        console.log(selectedFields);
        this.fieldAccessService.saveFieldAccessTest(selectedFields).then(data => {
            this.fieldAccess = data.result;
            console.log(selectedFields);
            var datacode = data.code;
            if (datacode === 201) {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                this.showloader = false;
                this.messageText = data.btiMessage.message;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.showloader = false;
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100);

        });
    }

    onSelect(event, field, rowIndex) {
        console.log('row: ' + field.fieldId);
        console.log('rowIndex: ' + rowIndex);
        // this.model.isMandatory =  event.target.checked;
        if (this.selected[rowIndex] === undefined || this.selected[rowIndex] === null) {
            this.selected[rowIndex] = field;
        }
        this.selected[rowIndex].isMandatory =  event.target.checked;
        var arr = [];

        if (this.selected.filter(x => x.fieldId === field.fieldId)[0]) {
            this.selected.filter(x => x.fieldId === field.fieldId)[0] = field;
        } else {
            this.selected.push(arr);
        }
        console.log('mandatory: ' + this.selected);
        console.log('moduleId: ' + this.moduleId);
    }

    onLanguageSelect(event, field, rowIndex) {
        console.log('row: ' + field.fieldId);
        console.log('rowIndex: ' + rowIndex);

        if (this.selected[rowIndex] === undefined || this.selected[rowIndex] === null) {
            this.selected[rowIndex] = field;
        }
        this.selected[rowIndex].languageId = Number( event.target.value);
        var arr = [];

        if (this.selected.filter(x => x.fieldId === field.fieldId)[0]) {
            this.selected.filter(x => x.fieldId === field.fieldId)[0] = field;
        } else {
            this.selected.push(arr);
        }
        console.log('mandatory: ' + this.selected);
        console.log('screenId: ' + this.model.screenId);
    }

    SelectModule(moduleId, actionType, event) {
        for ( var i = 0; i < this.LstScreenModule.length; i++) {
            if (actionType === 'IsMandatory') {
                if (event.target.checked) {
                    if (moduleId === this.LstScreenModule[i].moduleId) {
                        this.LstScreenModule[i].isMandatory = true;
                        for (var j = 0; j < this.LstScreenModule[i].screensList.length; j++) {
                            this.LstScreenModule[i].screensList[j].isMandatory = true;
                            for (var k = 0; k < this.LstScreenModule[i].screensList[j].fieldList.length; k++) {
                                this.LstScreenModule[i].screensList[j].fieldList[k].isMandatory = true;
                            }
                        }
                    }
                } else  {
                    if (moduleId === this.LstScreenModule[i].moduleId) {
                        this.LstScreenModule[i].isMandatory = false;
                        for ( var j = 0; j < this.LstScreenModule[i].screensList.length; j++) {
                            for ( var k = 0; k < this.LstScreenModule[i].screensList[j].fieldList.length; k++) {
                                this.LstScreenModule[i].screensList[j].fieldList[k].isMandatory = false;
                            }
                        }
                    }
                }
            }

            if (actionType === 'Keyboard') {
                if (event.target.value !== undefined || event.target.value != null) {
                    if (moduleId === this.LstScreenModule[i].moduleId) {
                        this.LstScreenModule[i].languageId = event.target.value;
                        for (var j = 0; j < this.LstScreenModule[i].screensList.length; j++) {
                            this.LstScreenModule[i].languageId = event.target.value;
                            for (var k = 0; k < this.LstScreenModule[i].screensList[j].fieldList.length; k++) {
                                this.LstScreenModule[i].screensList[j].fieldList[k].languageId = event.target.value;
                            }
                        }
                    }
                }
            }
        }
    }

    SelectScreen(screenId, actionType, event) {
        for ( var i = 0; i < this.LstScreenModule.length; i++) {
            if (actionType === 'IsMandatory') {
                if (event.target.checked) {
                    for (var j = 0; j < this.LstScreenModule[i].screensList.length; j++) {
                        if (screenId === this.LstScreenModule[i].screensList[j].screenId) {
                            this.LstScreenModule[i].screensList[j].isMandatory = true;
                            for (var k = 0; k < this.LstScreenModule[i].screensList[j].fieldList.length; k++) {
                                this.LstScreenModule[i].screensList[j].fieldList[k].isMandatory = true;
                            }
                        }
                    }
                } else {
                    let selected = this.LstScreenModule[i].screensList.filter((x) => x.isMandatory)
                    for (var j = 0; j < this.LstScreenModule[i].screensList.length; j++) {
                        if (screenId === this.LstScreenModule[i].screensList[j].screenId) {
                            this.LstScreenModule[i].screensList[j].isMandatory = false;
                            for (var k = 0; k < this.LstScreenModule[i].screensList[j].fieldList.length; k++) {
                                this.LstScreenModule[i].screensList[j].fieldList[k].isMandatory = false;
                            }
                        }
                    }
                }
            }

            if (actionType === 'Keyboard') {
                if (event.target.value !== undefined || event.target.value != null) {
                    for (var j = 0; j < this.LstScreenModule[i].screensList.length; j++) {
                        if (screenId === this.LstScreenModule[i].screensList[j].screenId) {
                            this.LstScreenModule[i].screensList[j].languageId = event.target.value;
                            for (var k = 0; k < this.LstScreenModule[i].screensList[j].fieldList.length; k++) {
                                this.LstScreenModule[i].screensList[j].fieldList[k].languageId = event.target.value;
                            }
                        }
                    }
                }
            }
        }
    }

    SelectField(screenId, actionType, event) {
        for ( var i = 0; i < this.LstScreenModule.length; i++) {
            if (event.target.checked) {
                for ( var j = 0; j < this.LstScreenModule[i].screensList.length; j++) {
                    if (screenId === this.LstScreenModule[i].screensList[j].screenId) {
                        this.LstScreenModule[i].screensList[j].readAccess = true;
                        for ( var k = 0; k < this.LstScreenModule[i].screensList[j].fieldList.length; k++) {
                            this.LstScreenModule[i].screensList[j].fieldList[k].readAccess = true;
                        }
                    }
                }
            }
        }
    }

    showHideGroup(divId: any, level: string, action: string) {
        if (level === 'Level1') {
            var level1 = <HTMLUListElement>document.getElementById('ul_' + divId);
            if (level1.className === 'visible') {
                level1.className = 'hide';
            } else {
                level1.className = 'visible';
            }
        } else {
            var level2 = <HTMLUListElement>document.getElementById('ulsubgroup_' + divId);
            if (level2.className === 'visible') {
                level2.className = 'hide';
            } else {
                level2.className = 'visible';
            }
        }
    }

    SaveOrUpdateFieldAccess() {
        this.showloader = true;
        this.model.modulesList = this.LstScreenModule;

        var selectedFields = [];
        var field = {};

        for (var i = 0; i < this.selected.length; i++) {
            if (this.selected[i] !== undefined && this.selected[i] !== null && this.selected[i].fieldId !== undefined && this.selected[i].fieldId !== null && this.selected[i].fieldId > 0) {
                field = {
                    fieldId: this.selected[i].fieldId,
                    languageId: this.selected[i].languageId,
                    isMandatory: this.selected[i].isMandatory
                };
                selectedFields.push(field);
            }
        }

        this.fieldAccessService.saveOrUpdateFieldAccess(this.model).then(data => {
            var datacode = data.code;
            if (datacode === 201) {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                this.showloader = false;
                this.messageText = data.btiMessage.message;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.showloader = false;
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100);

        });
    }

    // delete department by passing whole object of perticular Department
    changeStatus() {
        var selectedDepartments = [];
        debugger;
        for (var i = 0; i < this.selected.length; i++) {
            selectedDepartments.push(this.selected[i].fieldId);
        }
        console.log('selected departmensr');
        console.log(selectedDepartments);
        this.fieldAccessService.changeStatusIsMandatory(selectedDepartments).then(data => {
            var datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0 });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            // this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message + ' !';
            }, 100);

            // Refresh the Grid data after deletion of department
            this.setPage({ offset: 0 });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

}