import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { ReportManagementModel } from '../../_models/report-management/report-management';
import { ReportManagementService } from '../../_services/report-management/report-management.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'report-management',
    templateUrl: './report-management.component.html',
    providers: [ReportManagementService, CommonService]
})

// export Report component to make it available for other classes
export class ReportManagementComponent {
    page = new Page();
    rows = new Array<ReportManagementModel>();
    temp = new Array<ReportManagementModel>();
    selected = [];
    moduleCode = Constants.userModuleCode;
    screenCode = 'S-1269';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    reportId = {};
    searchKeyword = '';
    getReportMasterList: any[]= [];
    getModuleList: any[]= [];
    ddPageSize: number = 5;
    model: ReportManagementModel;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    reportIdvalue: string;
    reportIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    moduleId: number;

    // Create the label properties to use in the frontend

    REPORT_MANAGEMENT_REPORT_SEARCH: any;
    REPORT_MANAGEMENT_REPORT_ID: any;
    REPORT_MANAGEMENT_REPORT_DESCRIPTION: any;
    REPORT_MANAGEMENT_REPORT_NAME: any;
    REPORT_MANAGEMENT_REPORT_LINK: any;
    REPORT_MANAGEMENT_ACTION: any;
    REPORT_MANAGEMENT_CREATE_BUTTON_LABEL: any;
    REPORT_MANAGEMENT_SAVE_BUTTON_LABEL: any;
    REPORT_MANAGEMENT_CLEAR_BUTTON_LABEL: any;
    REPORT_MANAGEMENT_CANCEL_BUTTON_LABEL: any;
    REPORT_MANAGEMENT_UPDATE_BUTTON_LABEL: any;
    REPORT_MANAGEMENT_DELETE_BUTTON_LABEL: any;
    REPORT_MANAGEMENT_CREATE_FORM_LABEL: any;
    REPORT_MANAGEMENT_UPDATE_FORM_LABEL: any;
    REPORT_MANAGEMENT_MODULE_LABEL: any;
    REPORT_MANAGEMENT_REPORT_OBJECT_ID: any;
    REPORT_MANAGEMENT_REPORT_CONTAINER_ID: any;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(
        private router: Router,
        private reportManagementService: ReportManagementService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        // default form parameter for report master  screen
        this.defaultFormValues = [
            { 'fieldName': 'REPORT_MANAGEMENT_REPORT_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': '' },
            { 'fieldName': 'REPORT_MANAGEMENT_REPORT_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': '' },
            { 'fieldName': 'REPORT_MANAGEMENT_REPORT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': '' },
            { 'fieldName': 'REPORT_MANAGEMENT_REPORT_NAME', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_REPORT_LINK', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_ACTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_CREATE_BUTTON_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_SAVE_BUTTON_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_CLEAR_BUTTON_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_CANCEL_BUTTON_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_UPDATE_BUTTON_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_DELETE_BUTTON_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_MODULE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_REPORT_OBJECT_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
            { 'fieldName': 'REPORT_MANAGEMENT_REPORT_CONTAINER_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' , 'isMandatory': ''},
        ];
        this.REPORT_MANAGEMENT_REPORT_SEARCH = this.defaultFormValues[0];
        this.REPORT_MANAGEMENT_REPORT_ID = this.defaultFormValues[1];
        this.REPORT_MANAGEMENT_REPORT_DESCRIPTION = this.defaultFormValues[2];
        this.REPORT_MANAGEMENT_REPORT_NAME = this.defaultFormValues[3];
        this.REPORT_MANAGEMENT_REPORT_LINK = this.defaultFormValues[4];
        this.REPORT_MANAGEMENT_ACTION = this.defaultFormValues[5];
        this.REPORT_MANAGEMENT_CREATE_BUTTON_LABEL = this.defaultFormValues[6];
        this.REPORT_MANAGEMENT_SAVE_BUTTON_LABEL = this.defaultFormValues[7];
        this.REPORT_MANAGEMENT_CLEAR_BUTTON_LABEL = this.defaultFormValues[8];
        this.REPORT_MANAGEMENT_CANCEL_BUTTON_LABEL = this.defaultFormValues[9];
        this.REPORT_MANAGEMENT_UPDATE_BUTTON_LABEL = this.defaultFormValues[10];
        this.REPORT_MANAGEMENT_DELETE_BUTTON_LABEL = this.defaultFormValues[11];
        this.REPORT_MANAGEMENT_CREATE_FORM_LABEL = this.defaultFormValues[12];
        this.REPORT_MANAGEMENT_UPDATE_FORM_LABEL = this.defaultFormValues[13];
        this.REPORT_MANAGEMENT_MODULE_LABEL = this.defaultFormValues[14];
        this.REPORT_MANAGEMENT_REPORT_OBJECT_ID = this.defaultFormValues[15];
        this.REPORT_MANAGEMENT_REPORT_CONTAINER_ID = this.defaultFormValues[16]

        this.reportIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.reportId);
        }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        this.reportManagementService.getAllReportsList().then(data => {
            this.getReportMasterList = data.result;
        });

        this.reportManagementService.getAllModuleList().then(data => {
            this.getModuleList = data.result;
        });
    }

    getRowValue(row, gridFieldName) {
        if (gridFieldName == 'moduleName') {
            return row['dtoModule'] && row['dtoModule']['moduleName'] ? row['dtoModule']['moduleName'] : '';
        } else {
            return row[gridFieldName];
        }
    }

    getReportPositionsAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getReportMasterList.filter((reportId: any) => {
                return query.test(reportId.reportId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        // console.log('Selected value: ', e.value);
    }
    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if ( pageInfo.sortOn === undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if ( pageInfo.sortBy === undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.reportManagementService.getReportMasterList(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create report
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            reportId: '',
            reportName: '',
            reportLink: '',
            reportDescription: '',
            objectId: '',
            containerId: '',
            moduleId: this.moduleId
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }

    onChange(moduleId) {
        this.moduleId = moduleId;
    }
    // function call for creating new report
    CreateReportMaster(f: NgForm, event: Event) {
        event.preventDefault();
        let reportIdx = this.model.reportId;

        // Check if the id is available in the model.
        // If id available then update the report, else Add new report.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Check for duplicate Report Id according to it create new report
            this.reportManagementService.checkDuplicateReportId(reportIdx).then(response => {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    // Call service api for Creating new report
                    this.reportManagementService.createReportMaster(this.model).then(data => {
                        let datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new report
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    }

    // edit report by row
    edit(row: ReportManagementModel) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.reportId = row.reportId;
        this.reportIdvalue = this.model.reportId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        // Call service api for updating selected report
        this.model.reportId = this.reportIdvalue;
        this.reportManagementService.updateReportMaster(this.model).then(data => {
            let datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing report
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing report
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    // delete reports by passing whole object of particular Report
    delete() {
        let selectedReportss = [];
        for (let i = 0; i < this.selected.length; i++) {
            selectedReportss.push(this.selected[i].id);
        }
        this.reportManagementService.deleteReportMaster(selectedReportss).then(data => {
            let datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of report
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            let errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search report by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if ( this.page.sortOn === val ) {
            if ( this.page.sortBy === 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}
