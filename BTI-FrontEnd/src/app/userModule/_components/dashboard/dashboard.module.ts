
import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxChartsModule,
    DashboardRoutingModule,
  ],
  declarations: [DashboardComponent,AutoFocusDirective]
})
export class DashboardModule { }
