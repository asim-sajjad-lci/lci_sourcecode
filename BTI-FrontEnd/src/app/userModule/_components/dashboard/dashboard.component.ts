import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import {Constants} from '../../../_sharedresource/Constants';
import { DashboardService } from '../../_services/dashboardmanagement/dashboard.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [DashboardService,GetScreenDetailService]
})

export class DashboardComponent implements OnInit {
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1025"; 
  defaultFormValues: any = [];
  availableFormValues: [object];
  moduleName;
  screenName;
  getFirstTime = 1;
  companyOptions;
  activeUserCount;
  inActiveUserCount;
  userGroupCount;
  roleGroupCount;
  roleCount;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  messageText;
  select=Constants.select;
  activeUsers=Constants.activeUsers;
  inactiveUsers=Constants.inactiveUsers;
  userGroups=Constants.userGroups;
  roleGroups=Constants.roleGroups;
  roles=Constants.roles;
  //pie chart for company
  single: any[] =[
      {
          "name": "",
          "value": 0
      }
  ];
  multi: any[];
  view: any[] = [500, 500];
  // pie chart options
  showLegend = false;
  colorScheme = {
      domain: ['#5AA454', '#A10A28', '#C7B42C', '#A23AA1', '#c66300', '#776644', '#668833', '#03485e', '#f69321']
  };
  showLabels = true;
  explodeSlices = false;
  doughnut = false;
  message = { 'type': '', 'text': '' };

  constructor(
      private router: Router,
      private dashboardService: DashboardService,
      private getScreenDetailService: GetScreenDetailService
  ) {
      //default form parameter for dashboard screen
      this.defaultFormValues = [
          { 'fieldName': 'DASHBOARD_TOTAL_USERS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DASHBOARD_SELECT_COMPANY', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DASHBOARD_ACTIVE_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
           { 'fieldName': 'DASHBOARD_INACTIVE_USERS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DASHBOARD_ACTIVE_USERS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DASHBOARD_ROLE_GROUPS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DASHBOARD_USER_GROUPS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DASHBOARD_ROLES', 'fieldValue': '', 'helpMessage': '' },
      ];
      
        if(!localStorage.getItem('currentUser'))
        {
            this.router.navigate(['login']);
        }
  }
  // Screen initialization 
  ngOnInit() {
          //getting screen
        this.getScreenDetailService.updateScreenCode(this.screenCode);
          this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
          this.moduleName = data.result.moduleName
          this.screenName = data.result.dtoScreenDetail.screenName
          this.availableFormValues = data.result.dtoScreenDetail.fieldList;
          for (var j = 0; j < this.availableFormValues.length; j++) {
              var fieldKey = this.availableFormValues[j]['fieldName'];
              var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
              var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
              objDefault['fieldValue'] = objAvailable['fieldValue'];
              objDefault['helpMessage'] = objAvailable['helpMessage'];
          }
      });
      this.dashboardService.getAllCompanyList().then(data => {
          var tt = data.result;
          var newData = [];
          if(data.btiMessage.messageShort == 'COMPANY_GET_ALL')
          {
            for(var i=0; i < data.result.length; i++){
                var TotalUser = data.result[i].value ;
                if(TotalUser == 0)
                {
                   TotalUser = 1
                }
                var ll = { "name": data.result[i].name, "value":TotalUser }
                newData.push(ll)
            }
            this.single = newData;
            this.companyOptions = data.result;
            this.companyOptions.splice(0, 0, { "id": "", "name": this.select });
          }
      });
  }
  //event for company Stats details
  getCompanyData(event){
     var companyId = event.target.value;
      if(companyId != '' && companyId != undefined ){
          this.dashboardService.getAllCompanyStats(companyId).then(data => {
              var datacode = data.code;
              if (datacode == 200) {
                  this.activeUserCount = data.result.activeUserCount;
                  this.inActiveUserCount = data.result.inActiveUserCount;
                  this.userGroupCount = data.result.userGroupCount;
                  this.roleGroupCount = data.result.roleGroupCount;
                  this.roleCount = data.result.roleCount;
                  this.getFirstTime = 2;
              }
              else{
                  this.hasMsg = true;
                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      this.messageText = data.btiMessage.message;;
                  }, 100)
                  this.hasMsg = true;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                  }, 3000);
              }
          });
      }else {
          this.hasMsg = true;
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = "Please select a company!";
          }, 100)
          this.hasMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
          }, 3000);
      }
  }

  //default list on page
  onSelect(event) {
  }
}
