"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var page_1 = require("../../../_sharedresource/page");
var date_converter_service_1 = require("../../_services/date-converter/date-converter.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var Constants_1 = require("../../../_sharedresource/Constants");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var browser_1 = require("ngx-bootstrap/utils/facade/browser");
var DateConverterComponent = (function () {
    // public convertedDateModel: any = {date: {day: this.model.d, month: this.mName, year: this.model.y}}
    function DateConverterComponent(router, dateConveterService, getScreenDetailService, alertService) {
        this.router = router;
        this.dateConveterService = dateConveterService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = "M-1011";
        this.screenCode = "S-1220";
        this.message = { 'type': '', 'text': '' };
        this.departmentId = {};
        this.convertedHijriDate = '';
        this.convertedGregorianDate = '';
        this.hijriDay = '';
        this.hijriMonth = '';
        this.hijriYear = '';
        this.days = [];
        this.months = [];
        this.years = [];
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.mName = '';
        this.hMonthName = '';
        this.hMonthId = '';
        this.myOptions = {
            // other options...
            dateFormat: 'dd-mm-yyyy',
        };
        // Initialized to specific date (09.10.2018).
        this.startDateModel = { date: { day: 12, month: 10, year: 2018 } };
        this.page.pageNumber = 0;
        this.page.size = 5;
        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'DEPARTMENT_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_GREGORIAN_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_HIJRI_START_DATE', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    // Screen initialization
    DateConverterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    //setting pagination
    DateConverterComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.dateConveterService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.dateConveterService.getAllHijriDays().then(function (data) {
                _this.days = data.result.records;
            });
            _this.dateConveterService.getAllHijriMonth().then(function (data) {
                _this.months = data.result.records;
            });
            _this.dateConveterService.getAllHijriYear().then(function (data) {
                _this.years = data.result.records;
            });
        });
    };
    // Open form for create department
    DateConverterComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            arabicDepartmentDescription: '',
            departmentDescription: '',
            departmentId: '',
            gregorianDate: this.startDateModel,
            convertedHijriDate: '',
            hijriDate: '',
            convertedGregorianDate: '',
            hijriDay: 0,
            hijriMonth: '',
            hijriYear: 0,
            d: '',
            m: '',
            y: '',
            hm: '',
            hi: ''
        };
    };
    // Clear form to reset to default blank
    DateConverterComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new department
    DateConverterComponent.prototype.CreateDepartment = function (f, event) {
        var _this = this;
        event.preventDefault();
        var deptIdx = this.model.departmentId;
        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
            this.hijriDate = this.model.convertedHijriDate;
            this.gregorianDate = this.model.convertedGregorianDate;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.dateConveterService.checkDuplicateDeptId(deptIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = "success";
                    window.setTimeout(function () {
                        _this.isSuccessMsg = true;
                        _this.isfailureMsg = false;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    //Call service api for Creating new department
                    _this.dateConveterService.createDepartment(_this.model).then(function (data) {
                        console.log('convertToHijri', data.result.convertToHijri);
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new department
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = "Server error. Please contact admin.";
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = "Server error. Please contact admin.";
                }, 100);
            });
        }
    };
    // optional date changed callback
    DateConverterComponent.prototype.onStartDateChanged = function (event) {
        console.log(event.formatted);
        this.gregorianDate = event.formatted;
        this.dateConverter();
    };
    DateConverterComponent.prototype.dateConverter = function () {
        var _this = this;
        this.dateConveterService.convertGregorianDate(this.gregorianDate).then(function (data) {
            console.log('convertToHijri', data.result.convertToHijri);
            _this.model.convertedHijriDate = data.result.convertToHijri;
            console.log('Converted Hijir Date: ', _this.model.convertedHijriDate);
            _this.model.y = _this.model.convertedHijriDate.substring(0, 4);
            _this.mName = _this.model.convertedHijriDate.substring(5, 7);
            if (_this.model.convertedHijriDate.substring(8, 10).charAt(0) === '0') {
                _this.model.d = _this.model.convertedHijriDate.substring(9, 10);
                console.log('ifD', _this.model.d);
            }
            else {
                _this.model.d = _this.model.convertedHijriDate.substring(8, 10);
                console.log('elseD', _this.model.d);
            }
            _this.hijriMonthById();
            console.log('d: ', _this.model.d);
            console.log('m:', _this.mName);
            console.log('y: ', _this.model.y);
            // this.departmentService.convertHijriDate(this.years, this.months, this.days).then(data => {
            //     this.model.convertedGregorianDate = data.result.convertedGregorianDate;
            // });
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Data conversion Failed.';
            }, 100);
        });
    };
    DateConverterComponent.prototype.hijriDateConverter = function (event) {
        var _this = this;
        var day = browser_1.document.getElementById('selectday').value;
        var month = browser_1.document.getElementById('selectmonth').value;
        var year = browser_1.document.getElementById('selectyear').value;
        if (day != null || month != null || year != null)
            this.dateConveterService.convertHijriDate(year, month, day).then(function (data) {
                _this.model.convertedGregorianDate = data.result.convertToGregorian;
                console.log('Converted Gregorian Date: ', _this.model.convertedGregorianDate);
                _this.startDateModel = _this.model.convertedGregorianDate;
                // console.log('STrte Date model: ', this.startDateModel);
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Data conversion Failed.';
                }, 100);
            });
    };
    DateConverterComponent.prototype.hijriMonthById = function () {
        var _this = this;
        this.dateConveterService.getMonthNameById(this.mName).then(function (data) {
            // console.log('Month Name: ', data.result.monthName);
            _this.hMonthName = data.result.monthName;
            _this.hMonthId = data.result.id;
            _this.model.hi = _this.hMonthId;
            console.log('Model Month M: ', _this.hMonthName);
            console.log('Model Month Id: ', _this.hMonthId);
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Month Name not get.';
            }, 100);
        });
    };
    //edit department by row
    DateConverterComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.departmentId = row.departmentId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    DateConverterComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        //Call service api for updating selected department
        this.dateConveterService.updateDepartment(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = "Server error. Please contact admin.";
            }, 100);
        });
    };
    DateConverterComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    DateConverterComponent.prototype.delete = function () {
        var _this = this;
        var selectedDepartments = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDepartments.push(this.selected[i].id);
        }
        this.dateConveterService.deleteDepartment(selectedDepartments).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = "success";
            //this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = "error";
            var errorCode = error.status;
            _this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    };
    // default list on page
    DateConverterComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    DateConverterComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.dateConveterService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    DateConverterComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    DateConverterComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    };
    DateConverterComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    return DateConverterComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], DateConverterComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], DateConverterComponent.prototype, "myScrollContainer", void 0);
DateConverterComponent = __decorate([
    core_1.Component({
        selector: 'dateConverter',
        templateUrl: './date-converter.component.html',
        providers: [date_converter_service_1.DateConverterService]
    })
    // export Department component to make it available for other classes
    ,
    __metadata("design:paramtypes", [router_1.Router,
        date_converter_service_1.DateConverterService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], DateConverterComponent);
exports.DateConverterComponent = DateConverterComponent;
//# sourceMappingURL=date-converter.component.js.map