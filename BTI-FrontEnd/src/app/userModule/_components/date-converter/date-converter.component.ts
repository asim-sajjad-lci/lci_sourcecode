import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { DateConverterModule } from '../../_models/date-converter/date-converter.module';
import { DateConverterService } from '../../_services/date-converter/date-converter.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import {error} from 'selenium-webdriver';
import {IMyDateModel, INgxMyDpOptions} from 'ngx-mydatepicker';
import {document} from 'ngx-bootstrap/utils/facade/browser';
import {by, element} from 'protractor';

@Component({
    selector: 'dateConverter',
    templateUrl: './date-converter.component.html',
    providers: [DateConverterService]
})

// export Department component to make it available for other classes
export class DateConverterComponent {
    page = new Page();
    rows = new Array<DateConverterModule>();
    temp = new Array<DateConverterModule>();
    selected = [];
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1264";
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    departmentId = {};
    gregorianDate;
    convertedHijriDate = '';
    convertedGregorianDate = '';
    hijriDate;
    hijriDay= '';
    hijriMonth= '';
    hijriYear= '';
    years = [];
    searchKeyword = '';
    ddPageSize: number = 5;
    model: DateConverterModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    mName= '';
    hMonthName= '';
    hMonthId= '';


    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    // Initialized to specific date (09.10.2018).
    public startDateModel: any = { date: { day: 12, month: 10, year: 2018 } };
    // public convertedDateModel: any = {date: {day: this.model.d, month: this.mName, year: this.model.y}}
    private selectedLink: string = 'g';

    constructor(
        private router: Router,
        private dateConveterService: DateConverterService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'DEPARTMENT_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'GREGORIAN_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HIJRI_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_DATE_TYPE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_CONVERSION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    // setting pagination
    setPage(pageInfo) {
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.dateConveterService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
        this.dateConveterService.getAllHijriYear().then(data => {
            this.years = data.result.records;
        });
    }

    // Open form for create department
    Create() {
        this.showCreateForm = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            arabicDepartmentDescription: '',
            departmentDescription: '',
            departmentId: '',
            gregorianDate: this.startDateModel,
            convertedHijriDate: '',
            hijriDate: '',
            convertedGregorianDate: '',
            hijriDay: 0,
            hijriMonth: '',
            hijriYear: 0,
            d: '',
            m: '',
            y: '',
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }

    // function call for creating new department
    CreateDepartment(f: NgForm, event: Event) {
        event.preventDefault();
        var deptIdx = this.model.departmentId;

        // Check if the id is available in the model.
        // If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
            this.hijriDate = this.model.convertedHijriDate;
            this.gregorianDate = this.model.convertedGregorianDate;
        } else {
            // Check for duplicate Division Id according to it create new division
            this.dateConveterService.checkDuplicateDeptId(deptIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message + " !";
                    }, 100);
                } else {
                    // Call service api for Creating new department
                    this.dateConveterService.createDepartment(this.model).then(data => {

                        console.log('convertToHijri', data.result.convertToHijri);
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new department
                            this.setPage({ offset: 0 });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin !";
                        }, 100);
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin !";
                }, 100);
            });

        }
    }

    isSelected(name: string): boolean {
        if (!this.selectedLink) {
            return false;
        }
        return (this.selectedLink === name);
    }

    showdiv(g: string): void {
        this.selectedLink = g;
    }

    // optional date changed callback
    onStartDateChanged(event: IMyDateModel): void {
        // console.log(event.formatted)
        this.gregorianDate = event.formatted;
        this.dateConverter();

    }
    dateConverter() {
        this.dateConveterService.convertGregorianDate(this.gregorianDate).then(data => {
            console.log('convertToHijri', data.result.convertToHijri);
            this.model.convertedHijriDate = data.result.convertToHijri;
            console.log('Converted Hijir Date: ', this.model.convertedHijriDate);
            this.model.y = this.model.convertedHijriDate.substring(0, 4);
            this.mName = this.model.convertedHijriDate.substring(5, 7);

            if (this.model.convertedHijriDate.substring(8, 10).charAt(0) === '0') {
                this.model.d = this.model.convertedHijriDate.substring(9, 10);
                /*console.log('ifD', this.model.d);*/
            } else {
                this.model.d = this.model.convertedHijriDate.substring(8, 10);
                /*console.log('elseD', this.model.d);*/
            }

            this.hijriMonthById();

            /*console.log('d: ', this.model.d);
            console.log('m:', this.mName);
            console.log('y: ', this.model.y);*/
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Data conversion Failed !';
            }, 100);
        });
    }

    hijriDateConverter(event) {
        var day = document.getElementById('selectday').value;
        var month = document.getElementById('selectmonth').value;
        var year = document.getElementById('selectyear').value;
        if (day != null || month != null || year != null)
            this.dateConveterService.convertHijriDate(year, month, day).then(data => {
                this.model.convertedGregorianDate = data.result.convertToGregorian;
                console.log('Converted Gregorian Date: ', this.model.convertedGregorianDate);
                this.startDateModel = this.model.convertedGregorianDate;
                // console.log('STrte Date model: ', this.startDateModel);
            });

    }

    hijriMonthById() {
        this.dateConveterService.getMonthNameById(this.mName).then(data => {
            // console.log('Month Name: ', data.result.monthName);
            this.hMonthName = data.result.monthName;
            this.hMonthId = data.result.id;

            this.model.m = this.hMonthId;

            /*console.log('Model Month M: ', this.hMonthName);
            console.log('Model Month Id: ', this.hMonthId);*/
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Month Name not get !';
            }, 100);
        });
    }

    // edit department by row
    edit(row: DateConverterModule) {
        this.showCreateForm = true;
        this.model = row;
        this.departmentId = row.departmentId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        // Call service api for updating selected department
        this.dateConveterService.updateDepartment(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                // Refresh the Grid data after editing department
                this.setPage({ offset: 0 });

                // Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin !";
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
    }


    // delete department by passing whole object of perticular Department
    delete() {
        var selectedDepartments = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDepartments.push(this.selected[i].id);
        }
        this.dateConveterService.deleteDepartment(selectedDepartments).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0 });
            }
            this.hasMessage = true;
            this.message.type = "success";
            // this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message + " !";
            }, 100);

            // Refresh the Grid data after deletion of department
            this.setPage({ offset: 0 });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin !";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.dateConveterService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
        });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
}
