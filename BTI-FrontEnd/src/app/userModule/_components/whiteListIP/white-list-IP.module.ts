import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WhiteListIPRoutingModule } from './white-list-IP-routing.module';
import { WhiteListIPComponent } from './white-list-IP.component';
import { CreateWhiteListIPComponent } from './create-whiteListIP.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
//import { TrimWhitespace } from '../../../_sharedresource/TrimWhitespace.directive';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    WhiteListIPRoutingModule,
    FormsModule,
    NgxDatatableModule,
    MultiselectDropdownModule
  ],
  declarations: [WhiteListIPComponent, CreateWhiteListIPComponent,AutoFocusDirective]
})
export class WhiteListIPModule { }
