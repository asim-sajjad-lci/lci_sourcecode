import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { WhiteListIP } from '../../_models/whiteListIP/whiteListIP';
import { WhiteListIPService } from '../../_services/whiteListIP/whiteListIP.service';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';

@Component({
    selector: '<whiteListIP></whiteListIP>',
    templateUrl: './white-list-IP.component.html',
    styles: ["company.component.css"],
    providers: [WhiteListIPService,GetScreenDetailService]
})

// export to make it available for other classes
export class WhiteListIPComponent {
    page = new Page();
    rows = new Array<WhiteListIP>();
    temp = new Array<WhiteListIP>();
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1022";
    moduleName;
    isScreenLock;
    defaultFormValues: [object];
    availableFormValues: [object];
    messageText;
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    message = { 'type': '', 'text': '' };
    searchKeyword = '';
    selected = [];
    WhiteListIP = {};
    toggoleShowHide:string ="visible";
    ddPageSize=5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    isConfirmationModalOpen:boolean=false;
    currentSelectedRow: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction : boolean = false;
    EmptyMessage = Constants.EmptyMessage;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private whiteListIPService: WhiteListIPService,
        private getScreenDetailService: GetScreenDetailService,
    ) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        //default form values for whiteList IP
        this.defaultFormValues = [
            { 'fieldName': 'WHITELIST_BUTTON_ADD_NEW_IP', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_SEARCH', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_ADDRESS_TABLE_HEADER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_DESCRIPTION_TABLE_HEADER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_STATUS_TABLE_HEADER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_ACTION_TABLE_HEADER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_VIEW_TABLE_HEADER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_BLOCK_TABLE_HEADER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_ALLOW_TABLE_HEADER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_DELETE_BUTTON', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
            { 'fieldName': 'WHITELIST_IP_BUTTON_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':''  },
        ];
    }
    
    //Screen Initialization 
    ngOnInit() {
        this.setPage({ offset: 0 });
        // getting screen for whiteList IP
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                this.moduleName = data.result.moduleName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            });
        }
        
        //setting pagination
        setPage(pageInfo) {
            this.selected = [];
            this.page.pageNumber = pageInfo.offset;
            this.whiteListIPService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        
        //default list on page
        onSelect({ selected }) {
            this.selected.splice(0, this.selected.length);
            this.selected.push(...selected);
        }
        
        //edit whiteList IP by row id
        edit(row: any) {
            this.WhiteListIP = row.whitelistIpId;
            this.router.navigate(['whiteListIP/addNewIP', this.WhiteListIP]);
        }
        
        varifyDelete()
        {
            this.isDeleteAction=true;
            this.confirmationModalBody = this.deleteConfirmationText;
            this.isConfirmationModalOpen = true;
        }
        //delete one or multiple IP
        delete() {
            
            var selectedRoles = [];
            for (var i = 0; i < this.selected.length; i++) {
                selectedRoles.push(this.selected[i].whitelistIpId);
            }
            this.whiteListIPService.deleteWhiteListIP(selectedRoles).then(data => {
                window.scrollTo(0, 0);
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0 });
                    this.closeModal();
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
            
        }
        
        //block  or unblock whiteList IP by row id
        
        //block or unblock company by row id
        varifyOperation(row: any)
        {
            this.isDeleteAction=false;
            this.confirmationModalBody = Constants.confirmationModalBody;
            this.isConfirmationModalOpen = true;
            this.currentSelectedRow=row;
        }
        updateStatus() {
            this.whiteListIPService.blockWhiteListIP(this.currentSelectedRow.whitelistIpId,this.currentSelectedRow.isActive).then(data => {
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0 });
                    this.closeModal();
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        //search WhiteList details by IP Address
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.whiteListIPService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
                this.hasMsg = false;
                this.showMsg = false;
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            }, 
            // error => {
            //     this.hasMsg = true;
            //         window.setTimeout(() => {
            //             this.isSuccessMsg = false;
            //             this.isfailureMsg = true;
            //             this.showMsg = true;
            //             this.messageText = data.btiMessage.message;
            //         }, 100)
            //     }
        );
    }
    
    // page size change event    
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
    closeModal()
    {
        this.isDeleteAction=false;
        this.isConfirmationModalOpen = false;
    }
    /** If Screen is Lock then prevent user to perform any action.
    *  This function also cover Role Management Write acceess functionality */
    LockScreen(writeAccess)
    {
        if(!writeAccess)
        {
            return true
        }
        else if(this.isScreenLock)
        {
            return true;
        }
        else{
            return false;
        }
    }
}