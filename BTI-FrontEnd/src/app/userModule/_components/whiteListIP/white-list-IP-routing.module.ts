import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WhiteListIPComponent } from './white-list-IP.component';
import { CreateWhiteListIPComponent } from './create-whiteListIP.component';

const routes: Routes = [
  { path: '', component: WhiteListIPComponent },
  { path: 'whiteListIP', component: WhiteListIPComponent },
  { path: 'addNewIP', component: CreateWhiteListIPComponent },
  { path: 'addNewIP/:WhiteListIP', component: CreateWhiteListIPComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WhiteListIPRoutingModule { }
