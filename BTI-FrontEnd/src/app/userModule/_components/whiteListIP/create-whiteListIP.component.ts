import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Role } from '../../_models/rolemanagement/role';
import { WhiteListIPService } from '../../_services/whiteListIP/whiteListIP.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../_sharedresource/Autofocus';
import {Constants} from '../../../_sharedresource/Constants';

@Component({
    selector: 'create-whiteListIP',
    templateUrl: './create-whiteListIP.component.html',
    providers: [WhiteListIPService,GetScreenDetailService]
})
// export to make it available for other classes
export class CreateWhiteListIPComponent {
    moduleCode = Constants.userModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultFormValues: [object];
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    WhiteListIP: string;
    lblRole = "Create IP";
    btndisabled:boolean=false;
    isScreenLock
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private whiteListIPService: WhiteListIPService,
        
        private getScreenDetailService: GetScreenDetailService,
    ){
    }
    
    // Screen initialization
    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.WhiteListIP = params['WhiteListIP'];
            console.log("this.WhiteListIP"+this.WhiteListIP)
            if (this.WhiteListIP != '' && this.WhiteListIP != undefined) {
                this.screenCode = "S-1023";
                //default form values for editting whiteList IP
                this.defaultFormValues = [
                    { 'fieldName': 'WHITELIST_IP_EDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'WHITELIST_IP_DESCRIPTION_EDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'WHITELIST_IP_UPDATE_BUTTON', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'WHITELIST_IP_CANCEL_BUTTON', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }, 
                ];
                this.whiteListIPService.getWhiteListIP(this.WhiteListIP).then(data => {
                    this.model = data.result;
                });
            }
            else {
                this.screenCode = "S-1021";
                //default form values for creating new whiteList IP
                this.defaultFormValues = [
                    { 'fieldName': 'WHITELIST_IP_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'WHITELIST_IP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'WHITELIST_IP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'WHITELIST_IP_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                ];
            }
            //getting screen for whiteList IP
            
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
                this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                    this.screenName=data.result.dtoScreenDetail.screenName;
                    this.moduleName=data.result.moduleName;
                    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                });
            });
        }
        // function call for adding whiteList IP
        CreateWhiteListIP(f: NgForm) {
            this.btndisabled=true;
            if (this.WhiteListIP != '' && this.WhiteListIP != undefined) {
                this.whiteListIPService.updateWhiteListIP(this.model).then(data => {
                    var datacode = data.code;
                    this.btndisabled=false;
                    if (datacode == 200) {
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.messageText = data.btiMessage.message;;
                        }, 100);
                        window.setTimeout(() => {
                            this.router.navigate(['whiteListIP']);
                        }, 2000);
                        this.hasMsg = true;
                    }
                    else{
                        
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            else {
                this.whiteListIPService.addNewIP(this.model).then(data => {
                    window.scrollTo(0, 0);
                    var datacode = data.code;
                    this.btndisabled=false;
                    if (datacode == 201) {
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.messageText = data.btiMessage.message;;
                        }, 100);
                        window.setTimeout(() => {
                            this.router.navigate(['whiteListIP']);
                        }, 2000);
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        f.resetForm();
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
        }
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
        
    }
    
    
    