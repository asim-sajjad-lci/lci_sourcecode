import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { UserRoutingModule } from './user-routing.module';
import { UserManagementComponent } from './user-management.component';
import { CreateUserComponent } from './create-user.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
//import { TrimWhitespace } from '../../../_sharedresource/TrimWhitespace.directive';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    UserRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule,
  ],
  declarations: [UserManagementComponent, CreateUserComponent,AutoFocusDirective]
})
export class UserModule { }
