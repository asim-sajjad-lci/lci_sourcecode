import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { User } from '../../_models/usermanagement/user';
import { UserService } from '../../_services/usermanagement/user.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';

@Component({
    selector: '<user></user>',
    templateUrl: './user-management.component.html',
    styles: ["user.component.css"],
    providers: [UserService,GetScreenDetailService]
})

// export to make it available for other classes
export class UserManagementComponent {
    page = new Page();
    rows = new Array<User>();
    temp = new Array<User>();
    selected = [];
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1017"; 
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
    messageText;
    message = { 'type': '', 'text': '' };
    userId = {};
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    isConfirmationModalOpen:boolean=false;
    currentSelectedRow: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction : boolean = false;
    isScreenLock;
    EmptyMessage = Constants.EmptyMessage;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private userService: UserService,
        private getScreenDetailService: GetScreenDetailService,
    ) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        //default form parameter for user management screen
        this.defaultFormValues = [
            { 'fieldName': 'MANAGE_USER_SEARCH', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_UID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_COMPANY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_FIRST_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_LAST_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_EMAIL_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_USER_GROUP', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_LINK_BLOCK', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_LINK_UNBLOCK', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_DELETE_CONFIRM_OK', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'MANAGE_USER_VIEW_TABLE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':'' },
            
        ];
    }
    
    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0 });
        
        //getting screen
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                
                this.moduleName = data.result.moduleName
                this.screenName = data.result.dtoScreenDetail.screenName
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            });
            
        }
        
        // //setting pagination
        // setPage(pageInfo) {
        
        //     this.selected = []; // remove any selected checkbox on paging
        //     this.page.pageNumber = pageInfo.offset;
        //     this.userService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //         this.page = pagedData.page;
        //         this.rows = pagedData.data;
        //     });
        // }
        
        //default list on page
        onSelect({ selected }) {
            this.selected.splice(0, this.selected.length);
            this.selected.push(...selected);
        }
        
        //edit user by row id
        edit(row: any) {
            this.userId = row.userId;
            this.router.navigate(['user/createuser', this.userId]); 
        }
        
        varifyDelete()
        {
            this.isDeleteAction=true;
            this.confirmationModalBody = this.deleteConfirmationText;
            this.isConfirmationModalOpen = true;
        }
        
        //delete one or multiple users
        delete() {
            var selectedRoles = [];
            for (var i = 0; i < this.selected.length; i++) {
                selectedRoles.push(this.selected[i].userId);
            }
            this.userService.deleteUser(selectedRoles).then(data => {
                window.scrollTo(0, 0);
                
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0 });
                    this.closeModal();
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    
                    this.hasMsg = true;
                    
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        //block or unblock company by row id
        varifyOperation(row: any)
        {
            this.isDeleteAction=false;
            this.confirmationModalBody = Constants.confirmationModalBody;
            this.isConfirmationModalOpen = true;
            this.currentSelectedRow=row;
        }
        updateStatus() {
            this.userService.blockUnblockUser(this.currentSelectedRow.userId,this.currentSelectedRow.isActive).then(data => {
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0 });
                    this.closeModal();
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        
        //setting pagination
        setPage(pageInfo) {
            
            this.selected = []; // remove any selected checkbox on paging
            this.page.pageNumber = pageInfo.offset;
            this.userService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        
        // search rolegroup details by group name 
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.userService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            });
        }
        
        // Set default page size
        changePageSize(event) {
            this.page.size = event.target.value;
            this.setPage({ offset: 0 });
        }
        
        closeModal()
        {
            this.isDeleteAction=false;
            this.isConfirmationModalOpen = false;
        }
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
    }