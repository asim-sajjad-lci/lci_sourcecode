import { Component,OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { NgForm } from '@angular/forms';
import { UserService } from '../../_services/usermanagement/user.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {Constants} from '../../../_sharedresource/Constants';


@Component({
    selector: 'create-user',
    templateUrl: './create-user.component.html',
    providers: [UserService,GetScreenDetailService]
})
// export to make it available for other classes
export class CreateUserComponent {
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1019";
    moduleName;
    screenName;
    isScreenLock
    defaultFormValues: [object];
    availableFormValues: [object]; 
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    userId: string;
    lblRole = "Create User";
    countryOptions;
    stateOptions;
    cityOptions;
    userGroupOptions;
    ddOptions: IMultiSelectOption[];
    LstCompany:IMultiSelectOption[];
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selectCompany = Constants.selectCompany;
    select=Constants.select;
     ddl_MultiSelectCompany = [];
    SelectedCompany = [];
    ddlCompanySetting = {};
    iSEmailReadonly:boolean=false;
    isDisabled:boolean= false;
    private myOptions: INgxMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
    disableSince: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() +1 }
};

 // Settings configuration
    ddSettings: IMultiSelectSettings = {
        buttonClasses: 'btn dropdown-toggle formm-dd',
        enableSearch: true,
        showCheckAll: true,
        showUncheckAll: true
    };
     // Text configuration
    ddTexts: IMultiSelectTexts = {
         defaultTitle: this.selectCompany,
        checkAll: this.selectAll,
        uncheckAll: this.unselectAll,
    };
    // Initialized to specific date (09.10.2018)
    private  Object = { date: { year: 2018, month: 10, day: 9 } };

    // optional date changed callback
    onDateChanged(event: IMyDateModel): void {
       
    }


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
       
        private getScreenDetailService: GetScreenDetailService
    ) {
        //default form parameters for adding user
        this.defaultFormValues = [
            { 'fieldName': 'ADD_NEW_USER_LABEL_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NEW_USER_LABEL_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ARABIC_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ARABIC_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ARABIC_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_USER_GROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_TEXT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_TEXT_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_POSTALCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_DOB', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},

        ];
    }
    onChange() {
   
    }
  
   // Screen initialization
    ngOnInit() {
        this.ddlCompanySetting = { 
           singleSelection: false, 
           text:this.selectCompany,
           selectAllText: this.selectAll,
           unSelectAllText: this.unselectAll,
           enableSearchFilter: true,
           classes:"myclass custom-class"
         };            
    
    
        // Multiselect Dropdown End Here
    this.route.params.subscribe((params: Params) => {
            this.userId = params['userId'];
            if (this.userId != '' && this.userId != undefined) {
                this.iSEmailReadonly=true;
                this.lblRole = "Edit User";
                this.screenCode = "S-1018";
                // default form parameters for edit user detail
                this.defaultFormValues = [
                    { 'fieldName': 'EDIT_USER_LABEL_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ARABIC_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ARABIC_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ARABIC_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_USER_GROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_TEXT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_TEXT_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_POSTALCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_DOB', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_USER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_TEXT_RESET_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_OLD_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_NEW_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_CONFIRM_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_POSTALCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_DOB', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},

                ];
        this.userService.getUser(this.userId).then(data => {
                    this.model = data.result; // filling model
                    var allCompanyLst=this.model.listOfCompanies;
                    for(var i=0;i<allCompanyLst.length;i++)
                    {
                        this.SelectedCompany.push({"id":allCompanyLst[i].id,"itemName":allCompanyLst[i].name});
                    }
                    this.userService.getStateList(this.model.countryId).then(data => {
                        this.stateOptions = data.result;
                        this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
                    });
                    this.userService.getCityList(this.model.stateId).then(data => {
                        this.cityOptions = data.result;
                        this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
                    });
                    this.model.companyIds = this.model.companyIds;
                    this.model.isActive = this.model.isActive;
                    this.model.userGroupId = this.model.listOfCompanies[0].userGroup.id;
                    var today = this.model.dob;
                    var dateData = today.split('/');
                    this.model.dob = {"date":{"year":parseInt(dateData[2]),"month":parseInt(dateData[1]),"day":parseInt(dateData[0])}};
              });
            }
            else {
                this.iSEmailReadonly=false;
                this.model.countryId = "";
                this.model.stateId = "";
                this.model.cityId = "";
            }
        });

        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
        //getting screen for user details
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        this.model.isActive = true;

        // Country List Step 1
        this.userService.getCountryList().then(data => {
        this.countryOptions = data.result;
        this.countryOptions.splice(0, 0, { "countryId": "", "countryName": this.select });
        });

        this.userService.getCompanyList().then(data => {
            
            for(var i=0;i<data.result.records.length;i++)
            {
              
                this.ddl_MultiSelectCompany.push({ "id": data.result.records[i].id, "itemName": data.result.records[i].name })
            }
        });

        this.userService.getUserGroupList().then(data => {
            this.userGroupOptions = data.result.records;
            this.userGroupOptions.splice(0, 0, { "userGroupId": "", "groupName": this.select });
        });
    }
    onItemSelect(item:any){
    
    }
    OnItemDeSelect(item:any){
     
    }
    onSelectAll(items: any){
    }
    onDeSelectAll(items: any){
        
    }
    // event returning country data by country id
    getCountryData(event) {
        this.model.stateId = "";
        this.model.cityId = "";
        var countryId = event.target.value ? event.target.value : -1;
        this.userService.getCountryCode(countryId).then(data => {
            this.model.countryCode = data.result.countryCode;
        });
        // Country List Step 2
        this.userService.getStateList(countryId).then(data => {
            this.stateOptions = data.result;
            this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
        });
    }

    // event fired for reset password by userId
    resetUserPassword(event){
        event.preventDefault();
        if (this.userId != '' && this.userId != undefined) {
            this.userService.resetUserPassword(this.model.userId).then(data => {
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message+' !!';
                    }, 100);
            this.hasMsg = true;
                   window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
            }
            }).catch(error => {
            this.hasMsg = true;
                   window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)

            });
        }else{
            this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                         this.messageText = "Server error. Please contact admin !";
            }, 100)

        }

    }

    //event returning state data by state id
     getStateData(event) {
        this.model.cityId = "";
        // City List Step 3
        var stateId = event.target.value ? event.target.value : -1;
        this.userService.getCityList(stateId).then(data => {
            this.cityOptions = data.result;
            this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
        });
    }

    	      validateEmail(){		
           if (this.userId == '' || this.userId == undefined) {		
                this.userService.validateEmail(this.model.email).then(data => {		
                var datacode = data.code;		
                if (datacode == 302) {		
                    window.scrollTo(0, 0);		
                        window.setTimeout(() => {		
                            this.isSuccessMsg = false;		
                            this.isfailureMsg = true;		
                            this.showMsg = true;		
                            this.messageText = data.btiMessage.message;		
                        }, 100);		
                        this.hasMsg = true;		
                        // window.setTimeout(() => {		
                        //     this.showMsg = false;		
                        //     this.hasMsg = false;		
                        // }, 4000);		
                    }		
                else if(datacode == 200){		
                       window.setTimeout(() => {		
                            this.showMsg = false;		
                            this.hasMsg = false;		
                        }, 100);		
            }    		
         });		
        }		
       		
    }

    //function call for creating new user
    CreateUser(f: NgForm) {
        
        this.isDisabled=true;
        if(this.hasMsg || this.SelectedCompany.length == 0)
        {
            return false;
        }
        var selectedCompanyIds = [];
        for (var i = 0; i < this.SelectedCompany.length; i++) {
            var MyCompany = this.SelectedCompany;

            selectedCompanyIds.push(MyCompany[i].id);
        }
       // var ArrSelectedCompany = selectedCompanyIds.join();
        this.model.companyIds=selectedCompanyIds;
        if (this.userId != '' && this.userId != undefined) {
           // this.model.dob = this.model.dob.formatted;
            if(this.model.dob.formatted == undefined)
            {
               var dob = this.model.dob;
               this.model.dob = dob.date.day +'/'+ dob.date.month +'/'+ dob.date.year;
            }
            else
            {
                this.model.dob = this.model.dob.formatted;
            }
            
            this.userService.updateUser(this.model).then(data => {
                this.isDisabled=false;
                var datacode = data.code;
                if (datacode == 200) {
                window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                        this.messageText = data.btiMessage.message;;
                    }, 100);
                        window.setTimeout(() => {
                        this.router.navigate(['user']);
                        }, 2000);

                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                // else if (datacode == 302) {
                // window.scrollTo(0, 0);
                //     window.setTimeout(() => {
                //         this.isSuccessMsg = false;
                //         this.isfailureMsg = true;
                //         this.showMsg = true;
                //     window.setTimeout(() => {
                //         this.showMsg = false;
                //         this.hasMsg = false;
                //     }, 4000);
                //         this.messageText = data.btiMessage.message;
                //     }, 100);
                //         // window.setTimeout(() => {
                //         // this.router.navigate(['createuser/:userId']);
                //         // }, 2000);
                //     this.hasMsg = true;
                //     window.setTimeout(() => {
                //         this.showMsg = false;
                //         this.hasMsg = false;
                //     }, 4000);
                //     }
            }).catch(error => {
                this.isDisabled=false;
                this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
                });
        }
        else {
            this.model.dob = this.model.dob.formatted;
            this.userService.createUser(this.model).then(data => {
                this.isDisabled=false;
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                        this.messageText = data.btiMessage.message;;
                    }, 100);
                        window.setTimeout(() => {
                        this.router.navigate(['user']);
                        }, 2000);

                    this.hasMsg = true;

                    f.resetForm();

                }
                else if (datacode == 302) {
                window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isDisabled=false;
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                    // window.setTimeout(() => {
                    //     this.showMsg = false;
                    //     this.hasMsg = false;
                    // }, 4000);
                        this.messageText = data.btiMessage.message;
                    }, 100);
                        // window.setTimeout(() => {
                        // this.router.navigate(['createuser/:userId']);
                        // }, 2000);
                    this.hasMsg = true;
                    // window.setTimeout(() => {
                    //     this.showMsg = false;
                    //     this.hasMsg = false;
                    // }, 4000);
                    }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
    }
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }


    onlyAlphabets(event)
    {
       
        var charCode = event.keyCode;
        if(charCode == 17 || charCode == 16)
        {
            return false;
        }
        if (charCode == 9 || charCode == 8 || charCode == 46 ||(charCode > 64 && charCode < 91) || (charCode > 111 && charCode < 123))
            return true;
        else
        {
            return false;
        }
    }
    onlyAlphabets2(event)
    {
        
        var charCode = event.keyCode;
        if(charCode == 8 || charCode == 46)
        {
            return true;
        }
        if (charCode > 64 && charCode < 91)
        {
            return true;
        }
        else if(charCode > 105 && charCode < 123)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

     /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.isDisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
}