import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserManagementComponent } from './user-management.component';
import { CreateUserComponent } from './create-user.component';

const routes: Routes = [
  { path: '', component: UserManagementComponent },
  { path: 'user', component: UserManagementComponent },
  { path: 'createuser', component: CreateUserComponent },
  { path: 'createuser/:userId', component: CreateUserComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule { }
