
import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RoleRoutingModule } from './role-routing.module';
import { RoleManagementComponent } from './role-management.component';
import { CreateRoleComponent } from './create-role.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@NgModule({
  imports: [
    CommonModule,
    RoleRoutingModule,
    FormsModule,
    NgxDatatableModule,
    MultiselectDropdownModule
  ],
  declarations: [CreateRoleComponent, RoleManagementComponent,AutoFocusDirective]
})
export class RoleModule { }
