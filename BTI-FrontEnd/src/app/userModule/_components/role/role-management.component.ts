import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Role } from '../../_models/rolemanagement/role';
import { RoleService } from '../../_services/rolemanagement/role.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';

@Component({
    selector: 'manage-role',
    templateUrl: './role-management.component.html',
    providers: [RoleService,GetScreenDetailService]
})
// export to make it available for other classes
export class RoleManagementComponent {
    page = new Page();
    rows = new Array<Role>();
    selected = [];
    temp = new Array<Role>();
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1008";
    moduleName;
    screenName;
    isScreenLock;
    defaultFormValues: [object];
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    fullPath = "../src/app/companymanagement/edit2.jpg";
    roleId = {};
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    isConfirmationModalOpen:boolean=false;
    currentSelectedRow: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction : boolean = false;
    EmptyMessage = Constants.EmptyMessage;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private roleService: RoleService,
        private route: ActivatedRoute,
        private router: Router,
        private getScreenDetailService: GetScreenDetailService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        
        //defaultFormValues for role management screen
        this.defaultFormValues = [
            { 'fieldName': 'ADD_ROLE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'SEARCH_ROLE', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_NAME', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_VIEW_ICON', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_EDIT_EDIT', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_EDIT_DELETE', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_EDIT_ICON_VIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
        ];
    }
    // Screen initialization
    ngOnInit() {
        //getting role management screen
        
        
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.moduleName = data.result.moduleName;
            this.screenName = data.result.dtoScreenDetail.screenName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                if (objDefault) {
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
        this.setPage({ offset: 0 });
        this.ValidateScreen();
    }
    
    //edit role by row id
    edit(row: any) {
        
        this.roleId = row.id;
        this.router.navigate(['/role/createrole', this.roleId]);
        // window.location.href='createrole/'+this.roleId;
    }
    
    //default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }
    
    varifyDelete()
    {
        this.isConfirmationModalOpen = true;
    }
    
    //delete one or multiple role
    delete() {
        var selectedRoles = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedRoles.push(this.selected[i].id);
        }
        this.roleService.deleteRoles(selectedRoles).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0 });
                this.closeModal();
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                }, 100);
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });
    }
    
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.roleService.getAllRoles(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
    
    // search role details by role name 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.roleService.getAllRoles(this.page, this.searchKeyword).subscribe(pagedData => {
            this.hasMsg = false;
            this.showMsg = false;
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
        }, error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
    
    closeModal()
    {
        this.isConfirmationModalOpen = false;
    }
    
    ValidateScreen()
    {
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
    }

    /** If Screen is Lock then prevent user to perform any action.
     *  This function also cover Role Management Write acceess functionality */
    LockScreen(writeAccess)
    {
        if(!writeAccess)
        {
          return true
        }
        else if(this.isScreenLock)
        {
            return true;
        }
        else{
            return false;
        }
    }
} 
    