import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleManagementComponent } from './role-management.component';
import { CreateRoleComponent } from './create-role.component';

const routes: Routes = [
  { path: '', component: RoleManagementComponent },
  { path: 'role', component: RoleManagementComponent },
  { path: 'createrole', component: CreateRoleComponent },
  { path: 'createrole/:roleId', component: CreateRoleComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleRoutingModule { }
