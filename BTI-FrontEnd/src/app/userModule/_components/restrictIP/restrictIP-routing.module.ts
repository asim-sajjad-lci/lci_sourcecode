import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestrictIpComponent } from './restrictIP.component';

const routes: Routes = [
  { path: '', component: RestrictIpComponent },
  { path: 'restrictIp', component: RestrictIpComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RestrictIPRoutingModule { }
