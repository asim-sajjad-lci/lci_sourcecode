import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RestrictIPRoutingModule } from './restrictIP-routing.module';
import { RestrictIpComponent } from './restrictIP.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@NgModule({
  imports: [
    CommonModule,
    RestrictIPRoutingModule,
    FormsModule,
    AngularMultiSelectModule
  ],
  declarations: [RestrictIpComponent,AutoFocusDirective]
})
export class RestrictIPModule { }
