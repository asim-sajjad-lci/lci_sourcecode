import { NgModule, Directive, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { CompanyReportManagementRoutingModule } from './company-report-management-routing.module';
import { CompanyReportManagementComponent } from './company-report-management.component';
import { AssignReportToCompanyComponent } from './assign-report-to-company.component';
import { CompanyWiseReportComponent } from './company-wise-report.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { AgmCoreModule } from '@agm/core';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    CompanyReportManagementRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    MultiselectDropdownModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBFqCv1IAzVgnWA7D4uO6B_GDG-KedssKw'}),
  ],
  declarations: [CompanyReportManagementComponent, AutoFocusDirective,
    AssignReportToCompanyComponent, CompanyWiseReportComponent,
  ]
})
export class CompanyReportManagementModule { }
