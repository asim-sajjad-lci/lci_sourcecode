import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CompanyReportManagementModel } from '../../_models/company-report-management/company-report-management'
import { CompanyReportManagementService } from '../../_services/company-report-management/company-report-management.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Company } from '../../_models/companymanagement/company';

@Component({
    selector: 'company-report-management',
    templateUrl: './company-report-management.component.html',
    providers: [CompanyReportManagementService],
})
export class CompanyReportManagementComponent {
    page = new Page();
    rows = new Array<Company>();
    temp = new Array<CompanyReportManagementModel>();
    selected = [];
    moduleCode = 'M-1000';
    screenCode = 'S-1267';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    ddPageSize: number = 5;
    message = { 'type': '', 'text': '' };
    siteID: string;
    searchKeyword = '';
    model: any = {};
    showForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    error: any = { isError: false, errorMessage: '' };
    newAttribute: any = {
        id: 0,
        company: null,
        description: '',
        arabicDescription: ''
    };
    mandatory: boolean;
    CompanyList;
    companyId= 0;
    select = Constants.select;
    reportList: any = [];
    
    COMPANY_REPORT_MANAGEMENT_COMPANY_NAME: any;
    COMPANY_REPORT_MANAGEMENT_REPORT_ID: any;
    COMPANY_REPORT_MANAGEMENT_REPORT_NAME: any;
    COMPANY_REPORT_MANAGEMENT_REPORT_LINK: any;
    COMPANY_REPORT_MANAGEMENT_REPORT_DESCRIPTION: any;
    ASSIGN_REPORT_TO_COMPANY_FORM_LABEL: any;
    COMPANY_WISE_REPORTS_SHOW_LIST_LABEL: any;
    COMPANY_WISE_REPORT_LIST_LABEL: any;
    SUBMIT_BUTTON: any;
    CANCEL_BUTTON: any;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private router: Router,
        private reportManagementService: CompanyReportManagementService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'COMPANY_REPORT_MANAGEMENT_COMPANY_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
            { 'fieldName': 'COMPANY_REPORT_MANAGEMENT_REPORT_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
            { 'fieldName': 'COMPANY_REPORT_MANAGEMENT_REPORT_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
            { 'fieldName': 'COMPANY_REPORT_MANAGEMENT_REPORT_LINK', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'COMPANY_REPORT_MANAGEMENT_REPORT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'ASSIGN_REPORT_TO_COMPANY_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'COMPANY_WISE_REPORTS_SHOW_LIST_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'COMPANY_WISE_REPORT_LIST_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'SUBMIT_BUTTON', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'CANCEL_BUTTON', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
        ];
        this.COMPANY_REPORT_MANAGEMENT_COMPANY_NAME = this.defaultFormValues[0];
        this.COMPANY_REPORT_MANAGEMENT_REPORT_ID = this.defaultFormValues[1];
        this.COMPANY_REPORT_MANAGEMENT_REPORT_NAME = this.defaultFormValues[2];
        this.COMPANY_REPORT_MANAGEMENT_REPORT_LINK = this.defaultFormValues[3];
        this.COMPANY_REPORT_MANAGEMENT_REPORT_DESCRIPTION = this.defaultFormValues[4];
        this.ASSIGN_REPORT_TO_COMPANY_FORM_LABEL = this.defaultFormValues[5];
        this.COMPANY_WISE_REPORTS_SHOW_LIST_LABEL = this.defaultFormValues[6];
        this.COMPANY_WISE_REPORT_LIST_LABEL = this.defaultFormValues[7];
        this.SUBMIT_BUTTON = this.defaultFormValues[8];
        this.CANCEL_BUTTON = this.defaultFormValues[9];
    }

    ngOnInit() {

        this.getAllCompany();
        console.log('screen code');
        console.log(this.screenCode);
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.showForm = true;
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.moduleName = data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                this.mandatory = this.availableFormValues[j]['isMandatory'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                objDefault['isMandatory'] = objAvailable['isMandatory'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                if (objAvailable['listDtoFieldValidationMessage'] && objAvailable['isMandatory']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }

    // get all company
  getAllCompany() {
      this.reportManagementService.getAllCompanyList().then(data => {
          if (data.btiMessage.messageShort === 'SESSION_EXPIRED') {
               this.isSuccessMsg = true;
               this.isfailureMsg = false;
               this.showMsg = true;
               this.messageText = data.btiMessage.message;
                  window.setTimeout(() => {
                      this.router.navigate(['login']);
                  }, 100);
          }
              this.CompanyList = data.result.records;
              console.log(this.CompanyList);
              this.CompanyList.splice(0, 0, { 'id': '', 'name': this.select });
      });
      this.model.startTime = '';
      this.model.endTime = '';
  }

  onChange(companyId) {
    this.newAttribute = {
        id: 0,
        company: null,
        companyId: 0,
        companyName: '',
        description: '',
        arabicDescription: '',
        reportId: 0,
        reportName: '',
        reportLink: ''
    };
    
    this.reportManagementService.getReportsByCompanyId(this.page, companyId).then(response => {
        this.reportList = response.result.dtoReportMaster;
        let datacode = response.code;
        if (datacode === 404) {
            this.hasMessage = true;
            this.message.type = 'success';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = response.btiMessage.message + ' !';
            }, 100);
        } else {
        }
    }).catch(error => {
        this.hasMessage = true;
        this.message.type = 'error';
        var errorCode = error.status;
        this.message.text = 'Server issue. Please contact admin !';
    });
  }

}
