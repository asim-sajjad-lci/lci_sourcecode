import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyReportManagementComponent } from './company-report-management.component';
import { AssignReportToCompanyComponent } from './assign-report-to-company.component';
import {CompanyWiseReportComponent } from './company-wise-report.component';

const routes: Routes = [
  { path: '', component: CompanyReportManagementComponent },
  { path: 'companyReportManagement', component: CompanyReportManagementComponent },
   { path: 'companyWiseReports', component: CompanyWiseReportComponent },
  { path: 'assignReportToCompany', component: AssignReportToCompanyComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyReportManagementRoutingModule { }
