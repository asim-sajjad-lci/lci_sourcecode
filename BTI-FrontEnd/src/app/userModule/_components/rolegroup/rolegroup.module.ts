import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RoleGroupRoutingModule } from './rolegroup-routing.module';
import { RoleGroupManageComponent } from './role-group-management.component';
import { CreateRoleGroupComponent } from './create-role-group-management.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@NgModule({
  imports: [
    CommonModule,
    RoleGroupRoutingModule,
    FormsModule,
    NgxDatatableModule,
    MultiselectDropdownModule
  ],
  declarations: [CreateRoleGroupComponent, RoleGroupManageComponent,AutoFocusDirective]
})
export class RoleGroupModule { }
