import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGroupManageComponent } from './role-group-management.component';
import { CreateRoleGroupComponent } from './create-role-group-management.component';

const routes: Routes = [
  { path: '', component: RoleGroupManageComponent },
  { path: 'rolegroup', component: RoleGroupManageComponent },
  { path: 'createrolegroup', component: CreateRoleGroupComponent },
  { path: 'createrolegroup/:roleGroupId', component: CreateRoleGroupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleGroupRoutingModule { }
