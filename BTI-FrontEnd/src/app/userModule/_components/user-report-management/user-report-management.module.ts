import { NgModule, Directive, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { UserReportManagementRoutingModule } from './user-report-management-routing.module';
import { UserReportManagementComponent } from './user-report-management.component';
import { AssignReportToUserComponent } from './assign-report-to-user.component';
import { UserWiseReportComponent } from './user-wise-report.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { AgmCoreModule } from '@agm/core';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    UserReportManagementRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    MultiselectDropdownModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBFqCv1IAzVgnWA7D4uO6B_GDG-KedssKw'}),
  ],
  declarations: [UserReportManagementComponent, AutoFocusDirective,
    AssignReportToUserComponent, UserWiseReportComponent,
  ]
})
export class UserReportManagementModule { }
