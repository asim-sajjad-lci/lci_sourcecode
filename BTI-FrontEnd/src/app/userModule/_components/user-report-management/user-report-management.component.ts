import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { UserReportManagementModel } from '../../_models/user-report-management/user-report-management'
import { UserReportManagementService } from '../../_services/user-report-management/user-report-management.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { User } from '../../_models/usermanagement/user';

@Component({
    selector: 'user-report-management',
    templateUrl: './user-report-management.component.html',
    providers: [UserReportManagementService],
})
export class UserReportManagementComponent {
    page = new Page();
    rows = new Array<User>();
    temp = new Array<UserReportManagementModel>();
    selected = [];
    moduleCode = 'M-1000';
    screenCode = 'S-1268';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    ddPageSize: number = 5;
    message = { 'type': '', 'text': '' };
    searchKeyword = '';
    model: any = {};
    showForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    error: any = { isError: false, errorMessage: '' };
    newAttribute: any = {
        id: 0,
        user: null,
        description: '',
        arabicDescription: ''
    };
    mandatory: boolean;
    UserList;
    userId= 0;
    select = Constants.select;
    reportList: any = [];
    pageNumber= 0;
    pageSize= 50;

    USER_REPORT_MANAGEMENT_USER_NAME: any;
    USER_REPORT_MANAGEMENT_REPORT_ID: any;
    USER_REPORT_MANAGEMENT_REPORT_NAME: any;
    USER_REPORT_MANAGEMENT_REPORT_LINK: any;
    USER_REPORT_MANAGEMENT_REPORT_DESCRIPTION: any;
    ASSIGN_REPORT_TO_USER_FORM_LABEL: any;
    USER_WISE_REPORTS_SHOW_LIST_LABEL: any;
    USER_WISE_REPORT_LIST_LABEL: any;
    USER_REPORT_SUBMIT_BUTTON: any;
    USER_REPORT_CANCEL_BUTTON: any;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(private router: Router,
        private reportManagementService: UserReportManagementService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'USER_REPORT_MANAGEMENT_USER_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
            { 'fieldName': 'USER_REPORT_MANAGEMENT_REPORT_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
            { 'fieldName': 'USER_REPORT_MANAGEMENT_REPORT_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
            { 'fieldName': 'USER_REPORT_MANAGEMENT_REPORT_LINK', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'USER_REPORT_MANAGEMENT_REPORT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'ASSIGN_REPORT_TO_USER_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'USER_WISE_REPORTS_SHOW_LIST_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'USER_WISE_REPORT_LIST_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'USER_REPORT_SUBMIT_BUTTON', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
            { 'fieldName': 'USER_REPORT_CANCEL_BUTTON', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess':'' , 'isMandatory':''},
        ];
        this.USER_REPORT_MANAGEMENT_USER_NAME = this.defaultFormValues[0];
        this.USER_REPORT_MANAGEMENT_REPORT_ID = this.defaultFormValues[1];
        this.USER_REPORT_MANAGEMENT_REPORT_NAME = this.defaultFormValues[2];
        this.USER_REPORT_MANAGEMENT_REPORT_LINK = this.defaultFormValues[3];
        this.USER_REPORT_MANAGEMENT_REPORT_DESCRIPTION = this.defaultFormValues[4];
        this.ASSIGN_REPORT_TO_USER_FORM_LABEL = this.defaultFormValues[5];
        this.USER_WISE_REPORTS_SHOW_LIST_LABEL = this.defaultFormValues[6];
        this.USER_WISE_REPORT_LIST_LABEL = this.defaultFormValues[7];
        this.USER_REPORT_SUBMIT_BUTTON = this.defaultFormValues[8];
        this.USER_REPORT_CANCEL_BUTTON = this.defaultFormValues[9];
    }

    ngOnInit() {

        this.getAllUser();
        console.log('screen code');
        console.log(this.screenCode);
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.showForm = true;
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.moduleName = data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                this.mandatory = this.availableFormValues[j]['isMandatory'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                objDefault['isMandatory'] = objAvailable['isMandatory'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                if (objAvailable['listDtoFieldValidationMessage'] && objAvailable['isMandatory']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }

    // get all users
  getAllUser() {
      this.reportManagementService.getAllUserList(this.pageNumber, this.pageSize).then(data => {
          if (data.btiMessage.messageShort === 'SESSION_EXPIRED') {
               this.isSuccessMsg = true;
               this.isfailureMsg = false;
               this.showMsg = true;
               this.messageText = data.btiMessage.message;
                  window.setTimeout(() => {
                      this.router.navigate(['login']);
                  }, 100);
          }
              this.UserList = data.result.records;
              console.log(this.UserList);
              this.UserList.splice(0, 0, { 'id': '', 'userName': this.select });
      });
  }

  onChange(userId) {
    this.newAttribute = {
        id: 0,
        user: null,
        userId: 0,
        userName: '',
        reportId: 0,
        reportName: '',
        reportLink: ''
    };
    
    this.reportManagementService.getReportsByUserId(this.page, userId).then(response => {
        this.reportList = response.result.dtoReportMaster;
        let datacode = response.code;
        if (datacode === 404) {
            this.hasMessage = true;
            this.message.type = 'success';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = response.btiMessage.message + ' !';
            }, 100);
        } else {
        }
    }).catch(error => {
        this.hasMessage = true;
        this.message.type = 'error';
        var errorCode = error.status;
        this.message.text = 'Server issue. Please contact admin !';
    });
  }

}
