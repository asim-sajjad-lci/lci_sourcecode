import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserReportManagementComponent } from './user-report-management.component';
import { AssignReportToUserComponent } from './assign-report-to-user.component';
import { UserWiseReportComponent } from './user-wise-report.component';

const routes: Routes = [
  { path: '', component: UserReportManagementComponent },
  { path: 'userReportManagement', component: UserReportManagementComponent },
   { path: 'userWiseReports', component: UserWiseReportComponent },
  { path: 'assignReportToUser', component: AssignReportToUserComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserReportManagementRoutingModule { }
