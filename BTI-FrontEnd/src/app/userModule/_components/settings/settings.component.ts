import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Role } from '../../_models/rolemanagement/role';
import { SettingsService } from '../../_services/settings/settings.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';
import {TooltipModule} from "ngx-tooltip";

@Component({
    selector: 'settings',
    templateUrl: './settings.component.html',
    providers: [SettingsService]
})
// export to make it available for other classes
export class SettingsComponent {
    moduleCode = Constants.userModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultFormValues: [object];
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    isVisible = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    Settings: string;
    userId:number;
    ConfirmPasswordText = Constants.ConfirmPasswordText;
     TooltipModule;
    passwordPolicy=Constants.passwordPolicy;
    invalidPassword=Constants.invalidPassword;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = '';
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isConfirmationModalOpen:boolean=false;
 constructor(
        private router: Router,
        private route: ActivatedRoute,
        private settingsService: SettingsService,
       
        private getScreenDetailService: GetScreenDetailService) {
     }

    // Screen initialization
    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.Settings = params['Settings'];
            this.screenCode = "S-1026";
    //default form parameter for setting screen
            this.defaultFormValues = [
            { 'fieldName': 'ADMIN_PROFILE_PRIMARY_FNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_PRIMARY_MNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_PRIMARY_LNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_SECONDARY_FNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_SECONDARY_MNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_SECONDARY_LNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_EMAIL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_CHANGE_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_OLD_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_NEW_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_CONFIRM_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADMIN_PROFILE_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'' , 'writeAccess':''},
            ];

            this.settingsService.getProfile(this.Settings).then(data => {
                 if (data.status != 'BAD_REQUEST')
                 {
                    this.model = data.result; 
                 }
                 else{
                  this.confirmationModalBody=data.btiMessage.message;
                  this.isConfirmationModalOpen=true;
                  return false; 
                 }
            });
            
            this.getScreenDetailService.ValidateScreen(this.screenCode);
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
            }
        });
        });
    }
     
     onBlurMethod(event)
       {
        this.settingsService.getoldPassword(this.model.userId,this.model.oldpassword).then(data => {
            var datacode = data.code;
            
                if (datacode == 406) {
                window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                        this.messageText = data.btiMessage.message;;
                    }, 100);
                    this.hasMsg = true;
                     this.model.oldpassword='';

                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
          
        
 
     }

    //function call for changing password
    changePassword(event){
        event.preventDefault();
        this.isVisible=true;
     } 

     //function call for updating user screen
    CreateSettings(f: NgForm) {
          if(this.model.password != this.model.confirmpassword)
      {
         window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = this.ConfirmPasswordText;
          }, 100);
                this.hasMsg = true;
          window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
          }, 4000);
    return false;
      }
            this.settingsService.updateProfile(this.model).then(data => {
                var datacode = data.code;
                
                if (datacode == 201) {
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
            this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
            }
            }).catch(error => {
            this.hasMsg = true;
                   window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                 }, 100)
               });
        
    }

    closeModal()
    {
         this.isConfirmationModalOpen = false;
           this.router.navigate(['dashboard']);
    }
}