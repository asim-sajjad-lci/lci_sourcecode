import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UserService } from '../../_services/usermanagement/user.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
    templateUrl: './userSession.component.html',
    providers:[UserService]
})

//export to make it available for other classes
export class UserSessionComponent {
    screenCode;
    screenName;
    moduleName;
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    arrUserList:any;
    @ViewChild(DatatableComponent) table: DatatableComponent;
       constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
        )
        {
           
        }

        ngOnInit() 
        {
            this.getActiveUsersList();
        }
        
        getActiveUsersList()
        {
            this.userService.getActiveUsersList().then(data => {
                this.arrUserList = data.result.records;
            });
        }
        killSession(userArr:any)
        {
            this.userService.logOut(userArr).then(data => {
                var datacode = data.response.code;
                this.hasMsg = true;
                if (datacode == 200) {
                    this.getActiveUsersList();
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.response.btiMessage.message+' !!';
                    }, 100);
                        this.hasMsg = true;
                   window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
            }
            }).catch(error => {
            this.hasMsg = true;
                   window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = false;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)

            });
        }
}