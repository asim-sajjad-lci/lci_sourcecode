/**
 * A service class for authsetting
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { activityLog } from "../../_models/activityLog/activityLog";
import { Constants } from '../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()

export class activityLogService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllUrl = Constants.hcmModuleApiBaseUrl + 'activityLog/getAll';
   
    //initializing parameter for constructor  
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
   
  //get list
  getlist(page: Page, searchKeyword): Observable<PagedData<activityLog>> {
    return this.http.post(this.getAllUrl, {
        'searchKeyword': searchKeyword,
        'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
}
               //bind json data for view
    private getPagedData(page: Page, data: any): PagedData<activityLog> {
        let pagedData = new PagedData<activityLog>();
        if (data) {
           var gridRecords= data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let employee = new activityLog(
                    jsonObj.id,
                    jsonObj.userId,
                    jsonObj.userName,
                    jsonObj.services,
                    jsonObj.method,
                    jsonObj.activity,
                    jsonObj.fieldOldValue,
                    jsonObj.fieldNewValue,
                    jsonObj.createdDate,
                    jsonObj.updatedDate,
                    jsonObj.updatedBy,
                    jsonObj.screenName,
                    jsonObj.companyName,
                    jsonObj.moduleName,
                    jsonObj.fromDate
                    
                );
                pagedData.data.push(employee);
            }
                pagedData.page = page;
            }
        return pagedData;
            }
 
    
}