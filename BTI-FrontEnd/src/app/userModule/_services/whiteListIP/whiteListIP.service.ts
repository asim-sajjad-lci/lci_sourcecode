/**
 * A service class for usergroup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { WhiteListIP } from "../../_models/whiteListIP/whiteListIP";
import { Constants } from '../../../_sharedresource/Constants'


@Injectable()
export class WhiteListIPService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getIPUrl = Constants.userModuleApiBaseUrl + 'ip/getWhiteListIPById';
    private getList  = Constants.userModuleApiBaseUrl + 'ip/searchWhiteListIPs';
    private deleteWhiteListIPUrl = Constants.userModuleApiBaseUrl + 'ip/deleteWhiteListIP';
    private updateWhiteListIPUrl = Constants.userModuleApiBaseUrl + 'ip/updateWhiteListIP';
    private blockUnblockWhiteListIP = Constants.userModuleApiBaseUrl + 'ip/blockUnblockWhiteListIP';
    private addNewIPUrl = Constants.userModuleApiBaseUrl + 'ip/addWhiteListIP';

    //initializing parameter for constructor 
    constructor(private http: Http){
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //delete whitelist
    deleteWhiteListIP(ids: any) {
        return this.http.post(this.deleteWhiteListIPUrl, { 'deleteIds': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
 
    }

    //getting detail of whitelistIp
    getWhiteListIP(id: string) {
        return this.http.post(this.getIPUrl, { 'whitelistIpId': id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   //update whiteListIp for edit
    updateWhiteListIP(whiteListIP: WhiteListIP) {
        return this.http.post(this.updateWhiteListIPUrl, JSON.stringify(whiteListIP), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //add new whiteLitsip
    addNewIP(whiteListIP: WhiteListIP) {
        return this.http.post(this.addNewIPUrl,JSON.stringify(whiteListIP), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getlist for whiteListIp
    getlist(page: Page,searchKeyword): Observable<PagedData<WhiteListIP>> {
        return this.http
            .post(this.getList, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<WhiteListIP> {
        let pagedData = new PagedData<WhiteListIP>();
        if(data)
        {
            var gridRecords = data.records;
             page.totalElements = data.totalCount;
             page.totalPages = page.totalElements / page.size;
             let start = page.pageNumber * page.size;
             let end = Math.min((start + page.size), page.totalElements);
             for (let i = 0; i < gridRecords.length; i++) {
                 let jsonObj = gridRecords[i];
                 let employee = new WhiteListIP(jsonObj.whitelistIpId,jsonObj.ipAddress, jsonObj.description, jsonObj.isActive);
                 pagedData.data.push(employee);
             }
             pagedData.page = page;     
        }
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
     //block unblock user
  blockWhiteListIP(ids: any, IsActive) {
    return this.http.post(this.blockUnblockWhiteListIP, { 'whitelistIpId': ids,'isActive':!IsActive }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
 
    }
}