/**
 * A service class for restrictIp
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { AuthSetting } from "../../_models/authsetting/authsetting";
import { Constants } from '../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()

export class restrictIPService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getUserListUrl = Constants.userModuleApiBaseUrl + 'user/getAllUsersList';
    private updateIpURL = Constants.userModuleApiBaseUrl + 'user/updateIpCheckedStatus';
   
    //initializing parameter for constructor  
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //update IP Setting
    updateIPSetting(role: AuthSetting) {
        
        return this.http.post(this.updateIpURL, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //getting list of User
    getUserList() {
        return this.http.post(this.getUserListUrl, { },{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}