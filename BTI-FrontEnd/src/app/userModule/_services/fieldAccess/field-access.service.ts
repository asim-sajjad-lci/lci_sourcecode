import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Page} from '../../../_sharedresource/page';
import {Fields} from '../../_models/fields/fields.module';
import {Constants} from '../../../_sharedresource/Constants';
import {IMultiSelectOption} from 'angular-2-dropdown-multiselect';
import {Company} from '../../_models/companymanagement/company';
import {FieldAccessComponent} from '../../_components/fields/fieldAccess.component';

@Injectable()
export class FieldAccessService {

    private headers = new Headers({'content-type': 'application/json'});
    private getAllUrl = Constants.userModuleApiBaseUrl + 'company/searchCompanies';
    private saveOrUpdateFieldAccessUrl = Constants.userModuleApiBaseUrl + 'fields/saveFieldsAccess';
    private saveFieldAccessTestUrl = Constants.userModuleApiBaseUrl + 'fields/saveFieldsAccess';
    private getListOfFieldsByCompanyScreenModuleUrl = Constants.userModuleApiBaseUrl + 'fields/listOfFieldsByCompanyScreenModules';
    private saveOrUpdateFieldForMandatoryUrl = Constants.userModuleApiBaseUrl + 'fields/saveOrUpdateFieldForMandatory';
    private saveOrUpdateFieldLanguageUrl = Constants.userModuleApiBaseUrl + 'fields/saveOrUpdateFieldLanguage';

    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // search company
    getlist(page: Page, searchKeyword): Observable<PagedData<Company>> {
        return this.http.post(this.getAllUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, {headers: this.headers})
            .map(data => this.getPagedData(page, data.json().result))
            .catch(this.handleError);
    }

    saveOrUpdateFieldAccessData(companyId: string, moduleId: string, screenId: string, fields: any[]) {
        return this.http.post(this.saveOrUpdateFieldAccessUrl, {'companyId': companyId, 'moduleId': moduleId, 'ids': fields}, {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    saveOrUpdateFieldAccess(fields: Fields) {
        var RequestData = {
            fieldAccessId : fields.fieldAccessId,
            modulesList : fields.modulesList
        }
        return this.http.post(this.saveOrUpdateFieldAccessUrl, JSON.stringify(fields), {headers: this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    saveFieldAccessTest(accessIds: any[]) {
        return this.http.post(this.saveFieldAccessTestUrl, {'accessIds': accessIds}, {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // change status of Fields is mandatory true or false
    changeStatusIsMandatory(ids: any) {
        return this.http.put(this.saveOrUpdateFieldForMandatoryUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // chnage Language Ids for field id
    changeLanguageIds(languageIds: any) {
        return this.http.put(this.saveOrUpdateFieldLanguageUrl, {'languageIds': languageIds}, {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getListOfFieldsByCompanyScreenModule(PageNumber, PageSize) {
        return this.http
            .post(this.getListOfFieldsByCompanyScreenModuleUrl, { 'pageNumber': PageNumber, 'pageSize': PageSize }, { headers: this.headers })
            .map(data => data.json());
    }

    // bind json data for view
    private getPagedData(page: Page, data: any): PagedData<Company> {
        let pagedData = new PagedData<Company>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let employee = new Company(jsonObj.id, jsonObj.name, jsonObj.description, jsonObj.address,
                    jsonObj.isActive, jsonObj.state, jsonObj.stateId, jsonObj.city, jsonObj.cityId, jsonObj.country,
                    jsonObj.countryId, jsonObj.fax, jsonObj.email, jsonObj.phone, jsonObj.zipCode, jsonObj.companyCode,
                    jsonObj.countryCode, jsonObj.webAddress, jsonObj.lat, jsonObj.longitude, jsonObj.nameArabic);
                pagedData.data.push(employee);
            }
            pagedData.page = page;
        }
        return pagedData;
    }

    private getFieldPagedData(page: Page, data: any): PagedData<Fields> {
        let fieldPagedData = new PagedData<Fields>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let fields = new Fields(
                    jsonObj.fieldAccessId,
                    jsonObj.fieldId,
                    jsonObj.companyId,
                    jsonObj.fieldName,
                    jsonObj.description,
                    jsonObj.tenantId,
                    jsonObj.isMandatory,
                    jsonObj.modulesList,
                    jsonObj.keyboardInput,
                    jsonObj.languageId,
                    jsonObj.languageName,
                    jsonObj.screenId,
                    jsonObj.moduleId,
                    jsonObj.fieldLists
                );
                console.log('Service const' + jsonObj.fieldLists);
                fieldPagedData.data.push(fields);
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        fieldPagedData.page = page;
        return fieldPagedData;
    }

    // transforming data into IMultiSelectOption format
    private getFieldAccessData(data: any): IMultiSelectOption[] {
        let fieldDataObj: IMultiSelectOption;
        let fieldData = new Array<IMultiSelectOption>();
        for (let i = 0; i < data.length; i++) {
            fieldDataObj = {'id': data[i].id, 'name': data[i].fieldName};
            fieldData.push(fieldDataObj);
        }
        return fieldData;
    }

    // error handler
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
