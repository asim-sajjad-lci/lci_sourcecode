/**
 * A service class for shipping method set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions,ResponseOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { LanguageSetup } from '../../_models/languageSetup/language-setup';
import { ResponseContentType } from '@angular/http/src/enums';
declare var $: any;

@Injectable()
export class LanguageSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
     private headers2 = new Headers();
    private SaveLanguageUrl1 = Constants.userModuleApiBaseUrl + 'language/add';
    private ImportUserMasterDataUrl = Constants.userModuleApiBaseUrl + 'importMasterData';
    private ImportFinancialMasterDataUrl = Constants.userModuleApiBaseUrl + 'importCompanyMasterData';
    private importUserMasterDataForUpdateLanguageUrl = Constants.userModuleApiBaseUrl + 'importUserMasterDataForUpdateLanguage';
    private importCompanyMasterDataForUpdateLanguageUrl = Constants.userModuleApiBaseUrl + 'importCompanyMasterDataForUpdateLanguage';
    private exportUserMasterDataUrl = Constants.userModuleApiBaseUrl + 'exportMasterData';
    private exportFinancialMasterDataUrl = Constants.userModuleApiBaseUrl + 'exportCompanyMasterData';
    private exportUserMasterDataForUpdateLanguageUrl = Constants.userModuleApiBaseUrl + 'exportUserMasterDataForUpdateLanguageUrl';
    private exportCompanyMasterDataForUpdateLanguageUrl = Constants.userModuleApiBaseUrl + 'exportCompanyMasterDataForUpdateLanguage';
    private getLanguageListURL =  Constants.userModuleApiBaseUrl +'getLanguageList';
    private getLanguageListForDropDownURL =  Constants.userModuleApiBaseUrl +'getLanguageListForDropDown';
    private getUserCompanyListUrl = Constants.userModuleApiBaseUrl + 'companyListByUserId';
    private getCompanyListUrl = Constants.userModuleApiBaseUrl + 'company/searchCompanies';
    private updateLanguageUrl = Constants.userModuleApiBaseUrl + 'language/update';
    private getLanguageByIdUrl = Constants.userModuleApiBaseUrl + 'language/getById';
    private deleteLanguageUrl = Constants.userModuleApiBaseUrl + 'language/delete';
    private blockUnblockUrl = Constants.userModuleApiBaseUrl + 'language/activeInactive';
    private SaveLanguageUrl = Constants.userModuleApiBaseUrl + 'importMasterData';
    private exportCompanyTestDataUrl = Constants.financialModuleApiBaseUrl + 'exportCompanyTestData';
   
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        if(localStorage.getItem('currentUser'))
        {
            this.headers.append('session', userData.session);
            this.headers.append('userid', userData.userId);
            this.currentLanguage=localStorage.getItem('currentLanguage')?
                                localStorage.getItem('currentLanguage'):"1";
            this.headers.append("langid", this.currentLanguage);
            this.headers.append("tenantid", localStorage.getItem('tenantid'));
        }
    }

 
    // getLanguageList1(page,) {
    //     
    //          return this.http
    //         .get(this.getLanguageListURL, { headers: this.headers})
    //     .map(
    //        res => res.json()
    //     );
    // }
    getLanguageList(page: Page): Observable<PagedData<LanguageSetup>> {
        return this.http
            .post(this.getLanguageListURL, {'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result))
            .catch(this.handleError);
    }
    
     //getting details of  shipping method detail By LanguageId
    getLanguageById(languageId: string) {
        return this.http.post(this.getLanguageByIdUrl, { languageId: languageId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //getting All company list
    getCompanyList() {
        return this.http.post(this.getCompanyListUrl,{ 'searchKeyword': ''}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    // get User Company List
    getUserCompanyList(userId:string) {
        return this.http.post(this.getUserCompanyListUrl, {'userId':userId},{ headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

       //addede new deatils of LanguageSetup
    createLanguageSetup1(LanguageSetup:LanguageSetup)
    {
      return this.http.post(this.SaveLanguageUrl,JSON.stringify(LanguageSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
       //addede new deatils of LanguageSetup
    createLanguageSetup(formData:FormData)
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers2.append('session', userData.session);
        this.headers2.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers2.append("langid", this.currentLanguage);
 
      return this.http.post(this.SaveLanguageUrl1,formData,{headers:this.headers2})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    ImportUserMasterData(formData:FormData)
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers2.append('session', userData.session);
        this.headers2.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers2.append("langid", this.currentLanguage);
 
      return this.http.post(this.ImportUserMasterDataUrl,formData,{headers:this.headers2})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    ImportFinancialMasterData(formData:FormData)
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers2.append('session', userData.session);
        this.headers2.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers2.append("langid", this.currentLanguage);
 
      return this.http.post(this.ImportFinancialMasterDataUrl,formData,{headers:this.headers2})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    ImportUserMasterDataForUpdateLanguage(formData:FormData)
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        
        this.headers2.append('session', userData.session);
        this.headers2.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers2.append("langid", this.currentLanguage);
 
      return this.http.post(this.importUserMasterDataForUpdateLanguageUrl,formData,{headers:this.headers2})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    ImportCompanyMasterDataForUpdateLanguage(formData:FormData)
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers2.append('session', userData.session);
        this.headers2.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers2.append("langid", this.currentLanguage);
 
      return this.http.post(this.importCompanyMasterDataForUpdateLanguageUrl,formData,{headers:this.headers2})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    //update LanguageSetup for edit 
    updateLanguageSetup(LanguageSetup:LanguageSetup){
        return this.http.post(this.updateLanguageUrl,JSON.stringify(LanguageSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

      //Delete Language By LanguageId
    deleteLanguage(languageId: string) {
        return this.http.post(this.deleteLanguageUrl, { languageId: languageId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //blockUnblock Language
    blockUnblock(languageId: String, IsActive) {
       return this.http.post(this.blockUnblockUrl, { 'languageId': languageId,'isActive':!IsActive }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
 
    }

    // Add New Language With Import User and Financial Master Data
    SaveLanguageSetup(formData:FormData)
    {
      return this.http.post(this.SaveLanguageUrl,formData,{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
   
    downloadFile(data: Response){
        
        var blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        var url= window.URL.createObjectURL(blob);
        window.open(url);
      }
     //Export User Master Data
    exportUserMasterData() {
       
        this.headers.append('Accept','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        const options = {headers: this.headers, params: {}};
        return this.http.get(this.exportUserMasterDataUrl,options)
        .toPromise()
        .then(res =>{
            this.SaveFile('User Master Data','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', res)
        } )
        .catch(this.handleError);
    }

    SaveFile(name, type, data) {
        
        if (data != null && navigator.msSaveBlob)
            return navigator.msSaveBlob(new Blob([data], { type: type }), name);

        var a = $("<a style='display: none;'/>");
        var url = window.URL.createObjectURL(new Blob([data], { type: type }));
        a.attr("href", url);
        a.attr("download", name);
        $("body").append(a);
        a[0].click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }

    
    
    //Export User Master Data
    exportFinancialMasterData() {
        return this.http.get(this.exportFinancialMasterDataUrl, { headers: this.headers })
        .toPromise()
        .then(res => res)
        .catch(this.handleError);
    }

   
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<LanguageSetup> {
        let pagedData = new PagedData<LanguageSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new LanguageSetup(jsonObj.languageId,jsonObj.languageName,jsonObj.languageOrientation,jsonObj.languageStatus,jsonObj.isActive);
                pagedData.data.push(group);
            }

            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}