import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import {UserReportManagementModule} from '../../_components/user-report-management/user-report-management.module';
import {UserReportManagementModel} from '../../_models/user-report-management/user-report-management';

@Injectable()
export class UserReportManagementService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllReportsByUserId = Constants.userModuleApiBaseUrl + 'reportMaster/getReportsByUserId';
    private getAllUserListUrl = Constants.userModuleApiBaseUrl + 'user/getAllUsersList';
    private getAllReportMasterListUrl = Constants.userModuleApiBaseUrl + 'reportMaster/getAllReportMasterInDropdown';
    private assignReportToUserUrl = Constants.userModuleApiBaseUrl + 'reportMaster/assignReportToUser';
    private getAllUserWiseReportUrl = Constants.userModuleApiBaseUrl + 'reportMaster/getAllUserWiseReports';

    // initializing parameter for constructor
    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // getting list of company
    getAllUserList(pageNumber, pageSize) {
        return this.http.post(this.getAllUserListUrl, { 'pageNumber': pageNumber, 'pageSize': pageSize },{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of reports by companyID
    getReportsByUserId(page: Page, userId) {
        return this.http
            .post(this.getAllReportsByUserId, { 'userId': userId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of report masters
    getAllReportsList() {
        return this.http.get(this.getAllReportMasterListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Assign report to company
    assignReportToUser(userId: number, reportIds: any) {
        return this.http
            .post(this.assignReportToUserUrl, { 'userId': userId, 'reportIds': reportIds }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of all company wise reports
    getAllUserWiseReportsList() {
        return this.http.get(this.getAllUserWiseReportUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    /*//get list
    getlist(page: Page, searchKeyword): Observable<PagedData<Department>> {
        return this.http.post(this.getAllDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }*/

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
