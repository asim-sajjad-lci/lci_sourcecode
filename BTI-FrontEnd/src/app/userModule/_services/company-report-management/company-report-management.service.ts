import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';

@Injectable()
export class CompanyReportManagementService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllReportsByCompanyId = Constants.userModuleApiBaseUrl + 'reportMaster/getReportsByCompanyId';
    private getAllCompanyListUrl = Constants.userModuleApiBaseUrl + 'company/getCompanyListForDropDown';
    private getAllReportMasterListUrl = Constants.userModuleApiBaseUrl + 'reportMaster/getAllReportMasterInDropdown';
    private assignReportToCompanyUrl = Constants.userModuleApiBaseUrl + 'reportMaster/assignReportToCompany';
    private getAllCompanyWiseReportUrl = Constants.userModuleApiBaseUrl + 'reportMaster/getAllCompanyWiseReports';

    // initializing parameter for constructor
    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // getting list of company
    getAllCompanyList() {
        return this.http.put(this.getAllCompanyListUrl, { },{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of reports by companyID
    getReportsByCompanyId(page: Page, companyId) {
        return this.http
            .post(this.getAllReportsByCompanyId, { 'companyId': companyId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of report masters
    getAllReportsList() {
        return this.http.get(this.getAllReportMasterListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Assign report to company
    assignReportToCompany(companyId: number, reportIds: any) {
        return this.http
            .post(this.assignReportToCompanyUrl, { 'companyId': companyId, 'reportIds': reportIds }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of all company wise reports
    getAllCompanyWiseReportsList() {
        return this.http.get(this.getAllCompanyWiseReportUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
