/**
  * A service class for rolegroup
  */
  import { Injectable } from '@angular/core'; 
  import { Headers, Http, RequestOptions } from '@angular/http'; 
  import { Observable } from "rxjs"; import 'rxjs/add/operator/toPromise'; 
  import 'rxjs/Rx';
  import { PagedData } from '../../../_sharedresource/paged-data'; 
  import { Page } from '../../../_sharedresource/page'; 
  import { RoleGroup } from "../../_models/rolegroup/rolegroup";
  import { IMultiSelectOption } from 'angular-2-dropdown-multiselect'; 
  import { Constants } from '../../../_sharedresource/Constants';
  

@Injectable()

export class RoleGroupService
{
    private headers = new Headers({ 'content-type': 'application/json' });
    private roleList = Constants.userModuleApiBaseUrl +'group/getAccessRoleList';
    private getAllRoleGroup =  Constants.userModuleApiBaseUrl + 'group/getRoleGroupList';
    private getRoleGroup =  Constants.userModuleApiBaseUrl + 'group/getRoleGroupDetails';
    private createRoleGroup = Constants.userModuleApiBaseUrl + 'group/saveRoleGroup';
    private updateRoleGroup = Constants.userModuleApiBaseUrl +'group/updateRoleGroup';
    private deleteRoleGroup = Constants.userModuleApiBaseUrl +'group/deleteRoleGroups';
    private searchRoleGroups = Constants.userModuleApiBaseUrl +'group/searchRoleGroups';

    //initializing parameter for constructor 
    constructor(private http: Http) {
      var userData = JSON.parse(localStorage.getItem('currentUser'));
      this.headers.append('session', userData.session);
      this.headers.append('userid', userData.userId);
      var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //getting rolelist
    getRoleList(){
         return this.http.put(this.roleList,{"pageNumber":0,"pageSize":500},{headers:this.headers})
            .map(data => this.getRoleGroupData(data.json().result));
    }

    //transforming data into IMultiSelectOption format  
    private getRoleGroupData(data: any): IMultiSelectOption[] {
        let roleDataObj: IMultiSelectOption;
        let roleData = new Array<IMultiSelectOption>();
        for (let i = 0; i < data.records.length; i++) {
            roleDataObj = { 'id': data.records[i].id, 'name': data.records[i].roleName };
            roleData.push(roleDataObj);
        }
        return roleData;
    }
    
    //get all userGroup
    getAllGroup(page:Page, searchKeyword):Observable<PagedData<RoleGroup>>{
        return this.http.put(this.getAllRoleGroup,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

    //get userGroup by Id for Edit User Group 
    getGroup(id:string){
        return this.http.post(this.getRoleGroup,{id:id},{headers:this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //create rolegroup 
    createGroup(roleGroup:RoleGroup)
    {
        return this.http.put(this.createRoleGroup,JSON.stringify(roleGroup),{headers:this.headers})
            .toPromise().then(data => data.json())
            .catch(this.handleError)
    }
    
  //update rolegroup for edit 
    updateGroup(roleGroup:RoleGroup){
        return this.http.post(this.updateRoleGroup,JSON.stringify(roleGroup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //delete rolegroup 
    deleteGroup(ids:any){
        return this.http.post(this.deleteRoleGroup,{'ids':ids},{headers:this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }   

  //search userGroup
    searchGroup(page: Page,searchKeyword): Observable<PagedData<RoleGroup>> {
        return this.http
            .post(this.searchRoleGroups, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }

    //pagination of data
    private getPagedData(page: Page, data: any): PagedData<RoleGroup> {
        let pagedData = new PagedData<RoleGroup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new RoleGroup(jsonObj.id,jsonObj.roleGroupCode,jsonObj.roleNames, jsonObj.roleGroupName, jsonObj.roleGroupDescription);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }

    
    //error handler
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}