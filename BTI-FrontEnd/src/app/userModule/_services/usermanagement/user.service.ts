/**
 * A service class for usergroup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { User } from "../../_models/usermanagement/user";
import { Constants } from '../../../_sharedresource/Constants'


@Injectable()
export class UserService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllUserUrl = Constants.userModuleApiBaseUrl + 'user/searchUsers';
    private createUserUrl = Constants.userModuleApiBaseUrl + 'user/createUser';
    private getUserByUserIdUrl = Constants.userModuleApiBaseUrl + 'user/getUserDetailByUserId';
    private updateUserUrl = Constants.userModuleApiBaseUrl + 'user/updateUser';
    private deleteUserUrl = Constants.userModuleApiBaseUrl + 'user/deleteMutipleUsers';
    private getActiveUsersListUrl = Constants.userModuleApiBaseUrl + 'user/getActiveUsersList';
    private getCompanyListUrl = Constants.userModuleApiBaseUrl + 'company/getCompanyListForDropDown';
    private getUserGroupListUrl = Constants.userModuleApiBaseUrl + 'group/getAllUserGroupListForDropDown';
    private getCountryListUrl = Constants.userModuleApiBaseUrl + 'getCountryList';
    private getStateListUrl = Constants.userModuleApiBaseUrl + 'getStateListByCountryId';
    private getCityListUrl = Constants.userModuleApiBaseUrl + 'getCityListByStateId';
    private getCountryCodeUrl = Constants.userModuleApiBaseUrl + 'getCountryCodeByCountryId';
    private resetUserPasswordUrl = Constants.userModuleApiBaseUrl + 'user/resetPassword';
    private blockUnblockUserUrl = Constants.userModuleApiBaseUrl + 'user/blockUnblockUser';
    private validateEmailUrl = Constants.userModuleApiBaseUrl + '/user/checkEmail';
    private logOutUrl = Constants.userModuleApiBaseUrl + 'user/logout';
    private IsActive;

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //getting country code list
    getCountryCode(countryId: any) {
        return this.http.post(this.getCountryCodeUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting country list
    getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getting state list
    getStateList(countryId: any) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //getting city list
    getCityList(stateId: any) {
        return this.http.post(this.getCityListUrl, { 'stateId': stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //getting company list
    getCompanyList() {
        return this.http.put(this.getCompanyListUrl, { },{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   
    getActiveUsersList()
    {
        return this.http.put(this.getActiveUsersListUrl, { },{ headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
    //getting usergroup list
    getUserGroupList() {
        return this.http.put(this.getUserGroupListUrl, { 'pageNumber': '0', 'pageSize': 1000},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //Log Out User
     logOut(userArr:any) {
        var headers = new Headers({ 'content-type': 'application/json' });
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        headers.append('session', userArr.session);
        headers.append('userid', userArr.userId);
        headers.append("langid", '1');
        headers.append("tenantid", localStorage.getItem('tenantid'));
        return this.http.post(this.logOutUrl, {'userId':userArr.userId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    validateEmail(email: string){
        return this.http.post(this.validateEmailUrl, { 'email': email }, { headers: this.headers })
                    .toPromise()
                    .then(res => res.json())
                    .catch(this.handleError);
    }

    //add new user
    createUser(user: User) {
        return this.http.post(this.createUserUrl, JSON.stringify(user), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit user
    updateUser(user: User) {
        return this.http.post(this.updateUserUrl, JSON.stringify(user), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete user
    deleteUser(ids: any) {
        return this.http.post(this.deleteUserUrl, { 'userIds': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get user detail by Id
    getUser(userId: string) {
        return this.http.post(this.getUserByUserIdUrl, { userId: userId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }  
    
    //reset password for user
    resetUserPassword(userId: string) {
        return this.http.post(this.resetUserPasswordUrl, { userId: userId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
 
    //get list
    getlist(page: Page,searchKeyword): Observable<PagedData<User>> {
     return this.http
            .post(this.getAllUserUrl, {
                'searchKeyword':searchKeyword,
                'pageNumber': page.pageNumber,
                'pageSize': page.size
            },{ headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<User> {
        let pagedData = new PagedData<User>();
        if(data)
        {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let user = new User(jsonObj.userId,
                    jsonObj.userDetailId,
                    jsonObj.firstName,
                    jsonObj.middleName,
                    jsonObj.lastName,
                    jsonObj.secondaryFirstName,
                    jsonObj.secondaryMiddleName,
                    jsonObj.secondaryLastName,
                    jsonObj.phone,
                    jsonObj.email,
                    jsonObj.password,
                    jsonObj.listOfCompanies,
                    jsonObj.userGroup,
                    jsonObj.dob,
                    jsonObj.zipCode,
                    jsonObj.stateId,
                    jsonObj.countryId,
                    jsonObj.cityId,
                    jsonObj.isActive,
                    jsonObj.address,
                    jsonObj.countryCode,
                    jsonObj.userGroupName,
                    jsonObj.companyIds,
                    jsonObj.companyNames,
                    jsonObj.secondaryFullName
                );
                pagedData.data.push(user);
            }
            pagedData.page = page;
        }
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
      //block unblock user
  blockUnblockUser(ids: any, IsActive) {
      
        return this.http.post(this.blockUnblockUserUrl, { 'userId': ids,'isActive':!IsActive }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
 
    }
}