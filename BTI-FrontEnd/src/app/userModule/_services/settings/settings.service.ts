/**
 * A service class for settings
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Settings } from "../../_models/settings/settings";
import { Constants } from '../../../_sharedresource/Constants'


@Injectable()
export class SettingsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getProfileUrl = Constants.userModuleApiBaseUrl + 'user/getAdminProfileDetail';
    private updateProfileUrl = Constants.userModuleApiBaseUrl + '/user/updateAdminProfile';
    private matchOldPasswordUrl = Constants.userModuleApiBaseUrl + 'matchOldPassword';
    private logOutUrl = Constants.userModuleApiBaseUrl + 'user/logout';
    private logOutBeforeCompanySelectionUrl = Constants.userModuleApiBaseUrl + 'user/logoutBeforeCompanySelection';

   UserId;


 

    //initializing parameter for constructor 
    constructor(
        private http: Http,private router: Router,
    ) {
        
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        if(localStorage.getItem('currentUser'))
        {
            this.UserId = userData.userId;
            
                  this.headers.append('session', userData.session);
                  this.headers.append('userid', userData.userId);
                  var currentLanguage = localStorage.getItem('currentLanguage') ?
                      localStorage.getItem('currentLanguage') : "1";
                  this.headers.append("langid", currentLanguage);
                  this.headers.append("tenantid", localStorage.getItem('tenantid'));
        }
        else{
            this.router.navigate(['login']);
          }
    }

    //get user profile details
    getProfile(userId: string) {
        return this.http.post(this.getProfileUrl, {'userId':this.UserId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get user profile details
    getoldPassword(userId: string,password: string) {
        
        return this.http.post(this.matchOldPasswordUrl, {'userId':this.UserId,'password':password}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for user profile edit
    updateProfile(settings: Settings) {
        return this.http.post(this.updateProfileUrl, JSON.stringify(settings), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    
    }

    //pagination of data
    private getPagedData(page: Page, data: any): PagedData<Settings> {
        let pagedData = new PagedData<Settings>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let employee = new Settings(jsonObj.userId, jsonObj.userDetailId, jsonObj.firstName, jsonObj.lastName,
                    jsonObj.email, jsonObj.middleName, jsonObj.secondaryFirstName, jsonObj.secondaryMiddleName, jsonObj.secondaryLastName, 
                    jsonObj.secondaryFullName,jsonObj.primaryFullName,jsonObj.password);
                pagedData.data.push(employee);
            }
            pagedData.page = page;
        }
        return pagedData;
    }

     //Log Out User
    logOut(userId: string) {
        
        return this.http.post(this.logOutUrl, {'userId':this.UserId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    logOutBeforeCompanySelection(userId: string) {
        
        return this.http.post(this.logOutBeforeCompanySelectionUrl, {'userId':this.UserId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}