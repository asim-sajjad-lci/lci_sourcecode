/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { User } from "../../_models/usermanagement/user";
import { Constants } from '../../../_sharedresource/Constants'

@Injectable()
export class DashboardService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllCompanyListUrl = Constants.userModuleApiBaseUrl + 'company/companyListCountOfUsers';
    private getAllCompanyStatsUrl = Constants.userModuleApiBaseUrl + 'company/getCompanyStats';

    //initializing parameter for constructor
    constructor(private http: Http) {
        if(localStorage.getItem('currentUser') != '')
        {
            var userData = JSON.parse(localStorage.getItem('currentUser'));
            if(userData != null){
                 this.headers.append('session', userData.session);
                 this.headers.append('userid', userData.userId);
                 var currentLanguage=localStorage.getItem('currentLanguage')?
                                     localStorage.getItem('currentLanguage'):"1";
                 this.headers.append("langid", currentLanguage);
                 this.headers.append("tenantid", localStorage.getItem('tenantid'));
            }
        }
       
    }

    //getting list of company
    getAllCompanyList() {
        return this.http.get(this.getAllCompanyListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting list of companystats
    getAllCompanyStats(companyId: string) {
        return this.http.post(this.getAllCompanyStatsUrl, { 'id': companyId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}