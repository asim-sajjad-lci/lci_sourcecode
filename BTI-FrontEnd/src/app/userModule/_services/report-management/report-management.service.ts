/**
 * A service class for report management
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { ReportManagementModel } from '../../_models/report-management/report-management';
import { Constants } from '../../../_sharedresource/Constants';

@Injectable()
export class ReportManagementService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllReportMasterUrl = Constants.userModuleApiBaseUrl + 'reportMaster/getAll';
    private createReportMasterUrl = Constants.userModuleApiBaseUrl + 'reportMaster/create';
    private updateReportMasterUrl = Constants.userModuleApiBaseUrl + 'reportMaster/update';
    private deleteReportMasterUrl = Constants.userModuleApiBaseUrl + 'reportMaster/delete';
    private checkReportIdx = Constants.userModuleApiBaseUrl + 'reportMaster/reportIdCheck';
    private getAllReportMasterListUrl = Constants.userModuleApiBaseUrl + 'reportMaster/getAllReportMasterInDropdown';
    private getAllModulesInDropDownUrl = Constants.userModuleApiBaseUrl + 'getAllModulesInDropDown';
    private getEJBIUserCredentialsUrl = Constants.userModuleApiBaseUrl + 'reportMaster/getEJBIUserCredentials';

    // initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // add new report
    createReportMaster(reportMaster: ReportManagementModel) {
        return this.http.post(this.createReportMasterUrl, JSON.stringify(reportMaster), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit department
    updateReportMaster(reportMaster: ReportManagementModel) {
        return this.http.post(this.updateReportMasterUrl, JSON.stringify(reportMaster), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete department
    deleteReportMaster(ids: any) {
        return this.http.put(this.deleteReportMasterUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of report masters
    getAllReportsList() {
        return this.http.get(this.getAllReportMasterListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of report masters
    getAllModuleList() {
        return this.http.get(this.getAllModulesInDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // check for duplicate ID department
    checkDuplicateReportId(reportId: any) {
        return this.http.post(this.checkReportIdx, { 'reportId': reportId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting list of report masters
    getReportMasterList(page: Page, searchKeyword): Observable<PagedData<ReportManagementModel>> {
        return this.http.post(this.getAllReportMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<ReportManagementModel> {
        let pagedData = new PagedData<ReportManagementModel>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let reportMaster = new ReportManagementModel(
                        jsonObj.id,
                        jsonObj.reportId,
                        jsonObj.reportName,
                        jsonObj.reportLink,
                        jsonObj.reportDescription,
                        jsonObj.objectId,
                        jsonObj.containerId,
                        jsonObj.moduleId
                    );
                    pagedData.data.push(reportMaster);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // pagination for data
    private getPagedDataa(data: any): PagedData<ReportManagementModel> {
        let pagedData = new PagedData<ReportManagementModel>();
        if (data) {
            let gridRecords = data.records;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let reportMaster = new ReportManagementModel(
                        jsonObj.id,
                        jsonObj.reportId,
                        jsonObj.reportName,
                        jsonObj.reportLink,
                        jsonObj.reportDescription,
                        jsonObj.objectId,
                        jsonObj.containerId,
                        jsonObj.moduleId
                    );
                    pagedData.data.push(reportMaster);
                }
            }
        }
        return pagedData;
    }

    // check for duplicate ID department
    getEJBIUserCredentials(reportId: any) {
        return this.http.post(this.getEJBIUserCredentialsUrl, { 'reportId': reportId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
