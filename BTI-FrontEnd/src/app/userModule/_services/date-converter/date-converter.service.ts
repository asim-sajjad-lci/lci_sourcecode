/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Constants } from '../../../_sharedresource/Constants';
import {DateConverterModule} from '../../_models/date-converter/date-converter.module';
import {getResponseURL} from '@angular/http/src/http_utils';
import {concat} from 'rxjs/operators';
import {concatAll} from 'rxjs/operator/concatAll';


@Injectable()
export class DateConverterService {
    private variable = '';
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/getAll';
    private searchDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/searchDepartment';
    private createDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/create';
    private getDepartmentByDepartmentIdUrl = Constants.hcmModuleApiBaseUrl + 'department/getDepartmentDetailByDepartmentId';
    private updateDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/update';
    private deleteDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/delete';
    private checkDepartmentIdx = Constants.hcmModuleApiBaseUrl + 'department/departmentIdcheck';
    private convertDateUrl = Constants.hcmModuleApiBaseUrl + 'common/convertDate';
    private getAllHijriYearUrl = Constants.hcmModuleApiBaseUrl + 'hijriDate/getAllYear';
    private getMonthNameByIdUrl = Constants.hcmModuleApiBaseUrl + 'hijriDate/getMonthById';

    // initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }

    // convert date
    convertGregorianDate(gregorianDate: any) {
        console.log('string date:', JSON.stringify(gregorianDate));
        console.log('ConvertGregorianDate service is called');
        return this.http.post(this.convertDateUrl, {'gregorianDate': gregorianDate}, {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    convertHijriDate (years: any, months: any, days: any) {
        if (days != null || months != null || years != null) {
            let hijriDate = this.variable.concat(years.toString() + '-' + months.toString() + '-' + days.toString()).toString();
            console.log('Hijri Date: ', hijriDate);
            return this.http.post(this.convertDateUrl, {'hijriDate': hijriDate}, {headers: this.headers})
                .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
        } else {
            alert('Please select valid data!!');
        }
    }

    // add new department
    createDepartment(department: DateConverterModule) {
        return this.http.post(this.createDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit department
    updateDepartment(department: DateConverterModule) {
        return this.http.post(this.updateDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete department
    deleteDepartment(ids: any) {
        return this.http.put(this.deleteDepartmentUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // check for duplicate ID department
    checkDuplicateDeptId(departmentId: any) {
        return this.http.post(this.checkDepartmentIdx, { 'departmentId': departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get department detail by Id
    getDepartment(departmentId: string) {
        return this.http.post(this.getDepartmentByDepartmentIdUrl, { departmentId: departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    // get list
    getlist(page: Page, searchKeyword): Observable<PagedData<DateConverterModule>> {
        return this.http.put(this.getAllDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get all hijri year for drop down
    getAllHijriYear() {
        return this.http.get(this.getAllHijriYearUrl, {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get Hijri Month name by ID
    getMonthNameById(monthId: any) {
        return this.http.post(this.getMonthNameByIdUrl, {'id' : monthId}, {headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    searchDepartmentlist(page: Page, searchKeyword): Observable<PagedData<DateConverterModule>> {
        return this.http.post(this.searchDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<DateConverterModule> {
        let pagedData = new PagedData<DateConverterModule>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let department = new DateConverterModule(
                        jsonObj.id,
                        jsonObj.departmentId,
                        jsonObj.departmentDescription,
                        jsonObj.arabicDepartmentDescription,
                        jsonObj.gregorianDate,
                        jsonObj.convertedHijriDate,
                        jsonObj.hijriDate,
                        jsonObj.convertedGregorianDate,
                        jsonObj.hijriDay,
                        jsonObj.hijriMonth,
                        jsonObj.hijriYear,
                        jsonObj.d,
                        jsonObj.m,
                        jsonObj.y
                    );
                    pagedData.data.push(department);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}

