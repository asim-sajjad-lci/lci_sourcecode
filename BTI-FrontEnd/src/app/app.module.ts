import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { Component,Directive,ElementRef,Renderer,OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule }     from './app-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TooltipModule} from "ngx-tooltip";
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { SelectModule } from 'ng2-select';
import { TextMaskModule } from 'angular2-text-mask';
import * as moment from 'moment';
// import { ModalModule } from 'angular2-modal';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { BootstrapModalModule } from '../../node_modules/angular2-modal/plugins/bootstrap';
import {ModalModule} from "ng2-modal";
import { LocationStrategy, PathLocationStrategy, Location } from '@angular/common';

import { AlertService } from './_sharedresource/_services/alert.service';
import {AuthGuardService} from './_sharedresource/_services/auth-guard.service';
import {GetScreenDetailService} from './_sharedresource/_services/get-screen-detail.service';
import { OnlyNumber } from './_sharedresource/onlynumber.directive';
import { OnlyNumber1 } from './_sharedresource/onlynumber.directive.1';
import { PhonenumberFaxDirective } from './_sharedresource/phonenumber.fax.directive';
import { onlyAlphabet } from './_sharedresource/onlyAlphabet.directive';
//import { OnlyAlphaNumeric } from './_sharedresource/OnlyAlphanumeric.directive';
import { TrimValueAccessor } from './_sharedresource/TrimValueAccessor';
import {ContenteditableModelDirective} from './_sharedresource/contenteditableModel.directive'
import {NoCommaPipe} from './_sharedresource/no-comma.pipe';
import {SecurePipe} from './_sharedresource/secure.pipe';
import {CustomMaxDirective} from './_sharedresource/custom-max.directive';
import { HcmModule } from "./hcm/_components/hcm.module";
// import { ImsModule } from "./ims/_components/ims.module";
//#region 1
//#end Region
import { AppComponent } from './app.component';
import { LoginHeaderComponent } from './_sharedcomponent/login-header.component';
import { LoginSidebarComponent } from './_sharedcomponent/login-sidebar.component';
import { MainHeaderComponent } from './_sharedcomponent/main-header.component';
import { MainSidebarComponent } from './_sharedcomponent/main-sidebar.component';

import { LoginComponent } from './loginModule/_components/login.component';
import { VerifyOtpComponent } from './loginModule/_components/verify-otp.component';
import { AlertComponent } from './_sharedresource/_components/alert.component';
import { ForgotPasswordComponent } from './loginModule/_components/forgot-password.component';
import { ResetPasswordComponent } from './loginModule/_components/reset-password.component';
import { SelectCompanyComponent } from './loginModule/_components/selectCompany.component';
import { UserSessionComponent } from './userModule/_components/userSession/userSession.component';
import { FooterComponent } from './footer/footer.component';
import { SettingsComponent } from './userModule/_components/settings/settings.component';
import { CookieService } from 'ngx-cookie-service';
import { AngularSessionComponent } from './angularSession/_components/angularSession.component';
import { DialogModule } from "./generic-component/ngx-dialog/dialog.module";

import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { UploadFileService } from './_sharedresource/_services/upload-file.service';

import { FieldByCompanyManagementComponent } from './userModule/_components/fields/field-by-company-management.component';
import { FieldAccessComponent } from './userModule/_components/fields/fieldAccess.component';

// import { BillMaterialsSetupComponent } from './ims/_components/bill-materials-setup/bill-materials-setup.component';
// import { BillMaterialsSetupService } from './ims/_services/bill-materials-setup/bill-materials-setup.service';

import { ActivityLogComponent } from './userModule/_components/activityLog/activityLog.component';
// import { PriceLevelSetupComponent } from './ims/_components/price-level-setup/price-level-setup.component';
// import { PriceLevelSetupService } from './ims/_services/price-level-setup/price-level-setup.service';
// import { PricegroupSetupComponent } from './ims/_components/price-group-setup/price-group-setup.component';
// import { PriceGroupSetupService } from './ims/_services/price-group-setup/price-group-setup.service';

// import { ItemLotCategorySetupComponent } from './ims/_components/item-lot-category-setup/item-lot-category-setup.component';
// import { SiteSetupComponent } from './ims/_components/site-setup/site-setup.component';
// import { SiteBinsSetupComponent } from './ims/_components/bin-setup/bin-setup.component';
// import { ItemCategoryComponent } from './ims/_components/item-category/item-category.component';
// import { SiteSetupService } from './ims/_services/site-setup/site-setup.service';
// import { ItemCategoryService } from './ims/_services/item-category/item-category-service';
// import { SiteBinsSetupService } from './ims/_services/bin-setup/bin-setup.service';
// import { StockCalendarSetupComponent } from './ims/_components/stock-calendar-setup/stock-calendar-setup.component';
// import { InventoryControlSetupComponent } from './ims/_components/inventory-control-setup/inventory-control-setup.component';
// import { ItemClassSetupComponent } from './ims/_components/item-class-setup/item-class-setup.component';
// import { ClassCurrencySetupService } from './ims/_services/class-currency-setup/classCurrencySetup.service'
// import { UnitOfMeasureService } from './ims/_services/unit-of-measure/unit-of-measure.service';
// import { UnitOfMeasureComponent } from './ims/_components/unit-of-measure/unit-of-measure.component';
// import { SalesDocumentsSetupComponent } from './ims/_components/sales-documents-setup/sales-documents.component';
// import { SalesDocumentsSetupService } from './ims/_services/sales-documents-setup/salse-documents-setup.service';
// import { SalesInventorySetupService } from './ims/_services/sales-inventory-setup/sales-inventory-setup.service';
// import { SalesInventorySetupComponent } from './ims/_components/sales-inventory-setup/sales-inventory-setup.component';
// import { ItemMaintenanceComponent } from './ims/_components/item-maintenance/item-maintenance.component';
// import { ItemMaintenanceOptionComponent } from './ims/_components/item-maintenance-option/item-maintenance-option.component';
// import { ItemSerialNumberDefinationComponent } from './ims/_components/Item-serial-number-defination/Item-serial-number-defination.component';
// import { itemClassAccuntSetupComponent } from './ims/_components/item-class-account-setup/item-class-account.component';
// import { ItemQuantityMasterService } from './ims/_services/item-quantity-master/item-quantity-master.service';
// import { ItemKistMaintenanceService } from './ims/_services/item-kist-maintenance-master/item-quantity-master.service';
// import { ItemQuantityMasterComponent } from './ims/_components/item-quantity-master/item-quantitiy-master.component';
// import { ItemKistMaintenanceComponent } from './ims/_components/item-kist-maintenance/item-kist-maintenance.component';

import { InventoryControlUserCategoryService } from './_sharedresource/_services/inventory-control-user-category.service';

// import { HCMSidebarComponent } from "./hcm/_components/hcm-sidebar.component";


// HCM Starts

import { DepartmentComponent } from './hcm/_components/department/department.component';
import { DivisionComponent } from './hcm/_components/division/division.component';
import { PositionClassComponent } from './hcm/_components/position-class/position-class.component';
import { LocationComponent } from './hcm/_components/location/location.component';
import { SupervisorComponent } from './hcm/_components/supervisor/supervisor.component';
import { PositionSetupComponent } from './hcm/_components/position-setup/position-setup.component';
import { PositionPlanSetupComponent } from './hcm/_components/position-plan-setup/position-plan-setup.component';
import { PositionTrainingComponent } from './hcm/_components/position-training/position-training.component';
import { PositionAttachmentComponent } from './hcm/_components/position-attachment/position-attachment.component';
import { PositionPayCodeComponent } from './hcm/_components/position-pay-code/position-pay-code.component';
import { PositionBudgetComponent } from './hcm/_components/position-budget/position-budget.component';
// import { DateConverterComponent } from './hcm/_components/date-converter/date-converter.component';
import { AttendanceSetupComponent } from './hcm/_components/attendance-setup/attendance-setup.component' ;
import { AttendanceSetupOptionsComponent } from './hcm/_components/attendance-setup-options/attendance-setup-options.component' ;
import { DeductionCodeSetupComponent } from './hcm/_components/deduction-code-setup/deduction-code-setup.component';
import { AccrualSetupComponent } from './hcm/_components/accrual-setup/accrual-setup.component';
import { HealthCoverageTypeSetupComponent } from './hcm/_components/health-coverage-type-setup/health-coverage-type-setup.component';
import { SkillsSetupComponent } from './hcm/_components/skills-setup/skills-setup.component';
import { BenefitCodeSetupComponent } from './hcm/_components/benefit-code-setup/benefit-code-setup.component';
import { HealthInsuranceSetupComponent } from './hcm/_components/health-insurance-setup/health-insurance-setup.component';
import { AccrualMainCodeSetupComponent } from './hcm/_components/accrual-setup-main/accrual-setup-main.component';
import { RetirementPlanSetupComponent } from './hcm/_components/retirement-plan-setup/retirement-plan-setup.component';
import { TimeCodeComponent } from './hcm/_components/time-code/time-code.component';
import { AccrualScheduleSetupComponent } from './hcm/_components/accrual-schedule-setup/accrual-schedule-setup.component';
import { SalaryMatrixSetupComponent } from './hcm/_components/salary-matrix-setup/salary-matrix-setup.component';
import { ShiftCodeSetupComponent } from './hcm/_components/shift-code-setup/shift-code-setup.component';
import { SkillSetSetupComponent } from './hcm/_components/skill-set-setup/skill-set-setup.component';
import { MiscellaneousBenefitsComponent } from './hcm/_components/miscellaneous-benefits/miscellaneous-benefits.component';
import { TerminationSetupComponent } from './hcm/_components/termination-setup/termination-setup.component';
import { LifeInsuranceSetupComponent } from './hcm/_components/life-insurance-setup/life-insurance-setup.component';
import { HealthInsSetupComponent } from './hcm/_components/health-ins-setup/health-ins-setup.component';
// import { UR5MandatoryComponent } from './hcm/_components/ur5-mandatory/ur5-mandatory.component';
import { PredefineChecklistComponent } from './hcm/_components/predefine-checklist/predefine-checklist.component';
import { PaycodeSetupComponent } from './hcm/_components/paycode-setup/paycode-setup.component';
import { ExitInterviewComponent } from './hcm/_components/exit-interview/exit-interview.component';
import { RequisitionsSetupComponent } from './hcm/_components/requisitions-setup/requisitions-setup.component';
import { BenefitPreferencesComponent } from './hcm/_components/benefit-preferences/benefit-preferences.component';
import { TrainingCourseComponent } from './hcm/_components/training-course/training-course.component';
import { TrainingBatchSignupComponent } from './hcm/_components/training-batch-signup/training-batch-signup.component';
import { OrientationSetupComponent } from './hcm/_components/orientation-setup/orientation-setup.component';
import { InterviewTypeSetupComponent } from './hcm/_components/interview-type-setup/interview-type-setup.component';
import { PayScheduleSetupComponent } from './hcm/_components/pay-schedule-setup/pay-schedule-setup.component';
import { ClassSkillComponent } from './hcm/_components/class-skill/class-skill.component';
import { ClassEnrollmentComponent } from './hcm/_components/class-enrollment/class-enrollment.component';
// import { AccountStatementReportComponent } from './hcm/_components/account-statement-report/account-statement-report.component';
// import { AccountReportComponent } from './hcm/_components/account-report/account-report.component';


// import { HcmRoutingModule } from "./hcm-routing.module";
// import {CustomMaxDirective} from './_sharedresource/custom-max.directive';
// import {SecurePipe} from './_sharedresource/secure.pipe';
// import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SharedModule } from "./shared/shared.module";

import { ReportManagementComponent } from './userModule/_components/report-management/report-management.component';
// import { FieldAccessComponent } from "./userModule/_components/fields/fieldAccess.component";
// import { FieldByCompanyManagementComponent } from './userModule/_components/fields/field-by-company-management.component';
// import { MainHeaderComponent } from './_sharedcomponent/main-header.component';
import { EmployeeMasterComponent } from './hcm/_components/employee-master/employee-master.component';
// import { UploadFileService } from './_sharedresource/_services/upload-file.service';
import { BuildCheckStorageService } from './_sharedresource/_services/build-check-storage.service';
import { EmployeeDeductionMaintenanceComponent } from './hcm/_components/employee-deduction-maintenance/employee-deduction-maintenance.component';
import { EmployeeBenefitMaintenanceComponent } from './hcm/_components/employee-benefit-maintenance/employee-benefit-maintenance.component';
import { HealthInsuranceEnrollmentComponent } from './hcm/_components/health-insurance-enrollment/health-insurance-enrollment.component';
import { EmployeePayCodeMaintenanceComponent } from './hcm/_components/employee-pay-code-maintenance/employee-pay-code-maintenance.component';
import { MiscellaneousBenefitsEnrollmentComponent } from './hcm/_components/miscellaneous-benefits-enrollment/miscellaneous-benefits-enrollment.component';
import { EmployeePostDatedPayrateComponent } from './hcm/_components/employee-post-dated-payrate/employee-post-dated-payrate.component';
import { EmployeeQuickAssignmentComponent } from './hcm/_components/employee-quick-assignment/employee-quick-assignment.component';
import { EmployeeNationalityComponent } from './hcm/_components/employee-nationality/employee-nationality.component';
import { EmployeeEducationComponent } from './hcm/_components/employee-education/employee-education.component';
import { EmployeeDependentsComponent } from './hcm/_components/employee-dependents/employee-dependents.component';
import { EmployeeSkillsComponent } from './hcm/_components/employee-skills/employee-skills.component';
import { EmployeeDirectDepositComponent } from './hcm/_components/employee-direct-deposit/employee-direct-deposit.component';
import { ReportComponent } from './hcm/_components/reports/reports.component';
// import { AccountStatementReportComponent } from './hcm/_components/account-statement-report/account-statement-report.component';
// import { AccountReportComponent } from './hcm/_components/account-report/account-report.component';
import { EmployeeContactsComponent } from './hcm/_components/employee-contacts/employee-contacts.component';
import { EmployeeAddressMasterComponent } from './hcm/_components/employee-address-master/employee-address-master.component';
import { EmployeePositionHistoryComponent } from './hcm/_components/employee-position-history/employee-position-history.component';
import { BatchesComponent } from './hcm/_components/batches/batches.component';
import { TransactionEntryComponent } from './hcm/_components/transaction-entry/transaction-entry.component';
import { BuildChecksComponent } from './hcm/_components/build-checks/build-checks.component';
import { BuildPayrollCheckComponentPayCode } from './hcm/_components/build-payroll-check-paycode/build-payroll-check-paycode.component';
import { BuildPayrollCheckDeductionsComponent } from './hcm/_components/build-payroll-check-deductions/build-payroll-check-deductions.component';
import { BuildPayrollCheckBenefitsComponent } from './hcm/_components/build-payroll-check-benefits/build-payroll-check-benefits.component';
import { BuildPayrollCheckBatchesComponent } from './hcm/_components/build-payroll-check-batches/build-payroll-check-batches.component';
import{ActiveEmployeePostDatedPayrateComponent} from './hcm/_components/active-employee-post-dated-payrate/active-employee-post-dated-payrate.component';
import{CalculateChecksComponent} from './hcm/_components/calculate-checks/calculate-checks.component';
import{BuildCheckReportComponent} from './hcm/_components/buildcheck-report/buildcheck-report.component';
import{BuildPayrollCheckDefaultComponent} from './hcm/_components/build-payroll-check-default-setup/build-payroll-check-default-setup.component';
import { HCMSidebarComponent } from "./hcm/_components/hcm-sidebar.component";
// import { ClassCurrencySetupComponent } from "./ims/_components/currency-setup/currency-setup.component";
// import { ItemSiteAssignmentsComponent } from "./ims/_components/item-site-assignments/item-site-assignments.component";
// import { ItemVendorMasterComponent } from "./ims/_components/item-vendor-master/item-vendor-master.component";
import { BuildCheckCommonService } from './_sharedresource/_services/build-check-common.service';
import { CompanyService } from './userModule/_services/companymanagement/company.service';
import { ProfitAndLossService } from './financialModule/_services/reports/profitAndLoss.service';
import { BalanceSheetReportComponent } from './financialModule/_components/reports/balanceSheetReport/balance-sheet-report.component';
import { BalanceSheetService } from './financialModule/_services/reports/BalanceSheet.service';


// HCM Ends

@Directive({
  selector: '[myAutofocus]'
})
export class SelectFirstInputDirective {
  constructor(private elementRef: ElementRef) { };

  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
 
}
@NgModule({
    declarations: [
        AppComponent,
        LoginHeaderComponent,
        LoginSidebarComponent,
        MainHeaderComponent,
        MainSidebarComponent,
        LoginComponent,
        VerifyOtpComponent,
        AlertComponent,
        OnlyNumber,
		OnlyNumber1,
        PhonenumberFaxDirective,
        ContenteditableModelDirective,
        onlyAlphabet, 
        TrimValueAccessor,
        //OnlyAlphaNumeric,
        ForgotPasswordComponent,
        ResetPasswordComponent,
		SelectFirstInputDirective,
        SelectCompanyComponent,
        UserSessionComponent,
        FooterComponent,
        SettingsComponent,
        AngularSessionComponent,
        FieldAccessComponent,
        FieldByCompanyManagementComponent,
        ReportManagementComponent,
        
        // CustomMaxDirective,
        // MainHeaderComponent,
        /*
        BillMaterialsSetupComponent,
        IMSSidebarComponent,
        ActivityLogComponent,
        PriceLevelSetupComponent,
        PricegroupSetupComponent,
        ItemLotCategorySetupComponent,
        SiteSetupComponent,
        SiteBinsSetupComponent,
        ItemCategoryComponent,
        StockCalendarSetupComponent,
        InventoryControlSetupComponent,
        ItemClassSetupComponent,
        UnitOfMeasureComponent,
        SalesDocumentsSetupComponent,
        SalesInventorySetupComponent,
        ItemMaintenanceComponent,
        ItemMaintenanceOptionComponent,
        ItemSerialNumberDefinationComponent,
        PhonenumberFaxDirective,
        itemClassAccuntSetupComponent,
        ClassCurrencySetupComponent,
        ItemSiteAssignmentsComponent,
        ItemVendorMasterComponent,
        ItemQuantityMasterComponent,
        ItemKistMaintenanceComponent, 
*/

        // NoCommaPipe,

        // HCM Starts
 /*        
       DepartmentComponent,
        DivisionComponent,
        LocationComponent,
        PositionClassComponent,
        SupervisorComponent,
        PositionSetupComponent,
        PositionPlanSetupComponent,
        PositionTrainingComponent,
        PositionAttachmentComponent,
        PositionPayCodeComponent,
        PositionBudgetComponent,
        DateConverterComponent,
        AttendanceSetupComponent,
        AttendanceSetupOptionsComponent,
        DeductionCodeSetupComponent,
        AccrualSetupComponent,
        AccrualScheduleSetupComponent,
        HealthCoverageTypeSetupComponent,
        SkillsSetupComponent,
		    SkillSetSetupComponent,
        BenefitCodeSetupComponent,
        HealthInsuranceSetupComponent,
        TimeCodeComponent,
        SalaryMatrixSetupComponent,
        HealthInsuranceSetupComponent,
        TimeCodeComponent,
        LifeInsuranceSetupComponent,
        AccrualMainCodeSetupComponent,
        RetirementPlanSetupComponent,
        TimeCodeComponent,
        MiscellaneousBenefitsComponent,
        TerminationSetupComponent,
        HealthInsSetupComponent,
        UR5MandatoryComponent,
        ShiftCodeSetupComponent,
        PredefineChecklistComponent,
        PaycodeSetupComponent,
        ExitInterviewComponent,
        RequisitionsSetupComponent,
        BenefitPreferencesComponent,
        TrainingCourseComponent,
        TrainingBatchSignupComponent,
        OrientationSetupComponent,
        InterviewTypeSetupComponent,
        PayScheduleSetupComponent,
        OrientationSetupComponent,
        ClassSkillComponent,
        ClassEnrollmentComponent,
        CustomMaxDirective,
        // AutoFocusDirective,
        EmployeeMasterComponent,
        EmployeeDeductionMaintenanceComponent,
        EmployeeBenefitMaintenanceComponent,
        HealthInsuranceEnrollmentComponent,
        EmployeePayCodeMaintenanceComponent,
        MiscellaneousBenefitsEnrollmentComponent,
        EmployeePostDatedPayrateComponent,
        EmployeeQuickAssignmentComponent,
        EmployeeNationalityComponent,
        EmployeeEducationComponent,
		EmployeeDependentsComponent,
        EmployeeSkillsComponent,
        EmployeeDirectDepositComponent,
        ReportComponent,
        EmployeeContactsComponent,
BatchesComponent,
		// AccountReportComponent,
		ReportComponent,
		EmployeeAddressMasterComponent,
		EmployeePositionHistoryComponent,
		// AccountStatementReportComponent,
        TransactionEntryComponent,
        BuildChecksComponent,
        BuildPayrollCheckComponentPayCode,
        BuildPayrollCheckDeductionsComponent,
        BuildPayrollCheckBenefitsComponent,
        BuildPayrollCheckBatchesComponent,
        ActiveEmployeePostDatedPayrateComponent,
        CalculateChecksComponent,
        BuildCheckReportComponent,
        BuildPayrollCheckDefaultComponent,
        SecurePipe,
        HCMSidebarComponent,
 */        
        // HCM Ends

    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
		//HcmModule,
		// ImsModule,
        AppRoutingModule,
        NgxDatatableModule,
        ReactiveFormsModule,
        MultiselectDropdownModule,
        NgxChartsModule,
        BrowserAnimationsModule,
        NgxMyDatePickerModule,
        DateTimePickerModule,
        AngularMultiSelectModule,
        
		TextMaskModule,
        NgbModule.forRoot(),
        TypeaheadModule.forRoot(),
        AgmCoreModule.forRoot({
        apiKey: 'AIzaSyBFqCv1IAzVgnWA7D4uO6B_GDG-KedssKw'}),
        TooltipModule,
        ModalModule,
        ToastModule.forRoot(),
        // ModalModule.forRoot(),
        BootstrapModalModule,
        DialogModule,
        SelectModule,
        SharedModule,
		
    ],
    providers: [
        AlertService,
        AuthGuardService,
        CookieService,
        UploadFileService,
        GetScreenDetailService,
        BuildCheckCommonService,
        // BillMaterialsSetupService,
        // PriceLevelSetupService,
        // PriceGroupSetupService,
        // SiteSetupService,
        // SiteBinsSetupService,
        // ItemCategoryService,
        // ClassCurrencySetupService,
        // UnitOfMeasureService,
        CompanyService,
        // SalesDocumentsSetupService,
        // SalesInventorySetupService,
        InventoryControlUserCategoryService,
        ProfitAndLossService,
        BalanceSheetService,
        { provide: LocationStrategy, useClass: PathLocationStrategy }
    ],
    bootstrap: [AppComponent],
    exports:[SharedModule]
})
export class AppModule { } 
