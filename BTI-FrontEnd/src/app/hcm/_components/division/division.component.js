"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var page_1 = require("../../../_sharedresource/page");
var division_service_1 = require("../../_services/division/division.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var Constants_1 = require("../../../_sharedresource/Constants");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var DivisionComponent = (function () {
    function DivisionComponent(router, divisionService, getScreenDetailService, alertService) {
        this.router = router;
        this.divisionService = divisionService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = "M-1011";
        this.screenCode = "S-1221";
        this.message = { 'type': '', 'text': '' };
        this.divisionId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.cityOptions = [];
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
        this.isModalPopUpActive = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
        //default form parameter for division  screen
        this.defaultFormValues = [
            { 'fieldName': 'DIVISION_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_ADDRESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_FAX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_EMAIL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' }
        ];
    }
    // Screen initialization
    DivisionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.setPage({ offset: 0 });
        //getting screen
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            //debugger;
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    //setting pagination
    DivisionComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.divisionService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.divisionService.getCityList().then(function (data) {
                _this.cityOptions = data.result;
            });
        });
    };
    DivisionComponent.prototype.create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 600);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            arabicDivisionDescription: '',
            city: '',
            divisionAddress: '',
            divisionDescription: '',
            divisionId: '',
            email: '',
            fax: null,
            phoneNumber: null
        };
    };
    // Clear form to reset to default blank
    DivisionComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new division
    DivisionComponent.prototype.CreateDivision = function (f, event) {
        var _this = this;
        event.preventDefault();
        var divIdx = this.model.divisionId;
        //Check if the id is available in the model.
        //If id avalable then update the division, else Add new division.
        if (this.model.id > 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.divisionService.checkDivisionId(divIdx).then(function (res) {
                if (res && res.result && res.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = "success";
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = res.btiMessage.message;
                    }, 100);
                }
                else {
                    //Call service api for Creating new division
                    _this.divisionService.createDivision(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new division
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = "Server error. Please contact admin.";
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = "Server error. Please contact admin.";
                }, 100);
            });
        }
    };
    //edit division by row id
    DivisionComponent.prototype.edit = function (row) {
        console.log('row ', row);
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.divisionId = row.divisionId;
        this.divisionIdvalue = this.model.divisionId;
        setTimeout(function () {
            window.scrollTo(0, 500);
        }, 10);
    };
    DivisionComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        this.model.divisionId = this.divisionIdvalue;
        //Call service api for updating selected division
        this.divisionService.updateDivision(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = "Server error. Please contact admin.";
            }, 100);
        });
    };
    DivisionComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete one or multiple divisions
    DivisionComponent.prototype.delete = function () {
        var _this = this;
        var selectedDivisions = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDivisions.push(this.selected[i].id);
        }
        this.divisionService.deleteDivision(selectedDivisions).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = "success";
            //this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of division
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = "error";
            var errorCode = error.status;
            _this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    };
    // default list on page
    DivisionComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search rolegroup details by group name 
    DivisionComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.divisionService.searchDivisionlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    DivisionComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    DivisionComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        this.delete();
    };
    DivisionComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    DivisionComponent.prototype.keyPress = function (event) {
        var pattern = /^[0-9\-()]+$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    return DivisionComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], DivisionComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], DivisionComponent.prototype, "myScrollContainer", void 0);
DivisionComponent = __decorate([
    core_1.Component({
        selector: 'division',
        templateUrl: './division.component.html',
        providers: [division_service_1.DivisionService]
    })
    // export to make it available for other classes
    ,
    __metadata("design:paramtypes", [router_1.Router,
        division_service_1.DivisionService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], DivisionComponent);
exports.DivisionComponent = DivisionComponent;
//# sourceMappingURL=division.component.js.map