import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { OnlyDecimalDirective } from '../../../_sharedresource/onlyDecimal.directive';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { RetirementPlanSetup } from '../../_models/retirement-plan-setup/retirement-plan-setup.module';
import { RetirementPlanSetupService } from '../../_services/retirement-plan-setup/retirement-plan-setup.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
    selector: 'retirement',
    templateUrl: './retirement-plan-setup.component.html',
    providers: [RetirementPlanSetupService],
})
export class RetirementPlanSetupComponent {
    page = new Page();
    rows = new Array<RetirementPlanSetup>();
    temp = new Array<RetirementPlanSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1422';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
	retirementPlanId:string;
    diductionId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: RetirementPlanSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
	fmamount:boolean=false;
	fpercent:boolean=false;
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
	start_date;
	end_date;
	islifeTimeValid: boolean = true;
	islifeTimeValidperyear: boolean = true;
	islifeTimeValidperperiod: boolean = true;
	islifeTimeValidamount: boolean = true;
	islifeTimeValidEmployerpay:boolean=true;
	islifeTimepercent:boolean=true;
	islifeTimeMaxpercent:boolean=true;
	decimalpattern='/^(((0|[1-9]\d{0,2})(\.\d{2})?)|())$/';
	tempp:string[]=[];
	companyinsuranceoption:any[]=[];
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    diductionIdvalue: string;
    fundsArray:any[]=[];
    newAttribute:any = {};
    fundsItemArray:any[] = [];
    fundsRowId:any = {};
    entranceDate: string;
    subItems = [];
    subItemsObj= {};
    fundID: string;
    fundName: string;
    active: boolean;
    error:any={isError:false,errorMessage:''};
    methodArray = ["Fixed Amount","Amount Per Unit","Percent of Gross Wages","Percent of Net Wages"];
    frequencyArray = ["Weekly","Biweekly","Semimonthly","Monthly","Quarterly","Semianually","Anually"];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('dp2') input:ElementRef; 
	

    constructor(private router: Router,
        private deductionCodeSetupService: RetirementPlanSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_ARBIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_ACOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_MATCH_PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_MAX_PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_MAX_ACCOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_LOANS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_BONUS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_RETIREMENT_AGE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_RETIREMENT_CONTRIBUTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_WAITING_METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_PLAN_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_PLAN_PERCENT', 'fieldValue': '', 'helpMessage': '' },
			{ 'fieldName': 'RETIREMENT_PLAN_SETUP_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_PLAN_FREQUENCY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_SETUP_HELTH_INSURANCE', 'fieldValue': '', 'helpMessage': '' },
			{ 'fieldName': 'RETIREMENT_PLAN_SETUP_PERCENT', 'fieldValue': '', 'helpMessage': '' },
			{ 'fieldName': 'RETIREMENT_PLAN_SETUP_MAX_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
			{ 'fieldName': 'RETIREMENT_PLAN_SETUP_WAITING_PERIODS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_ENTRANCE_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_ACC_NUM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_COMP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_TABLE_DESC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RETIREMENT_PLAN_TABLE_RETIRE_ID', 'fieldValue': '', 'helpMessage': '' },

        ];

    }

    ngOnInit() {
			this.deductionCodeSetupService.getcompInsurances().then(data=>{
			this.companyinsuranceoption=data.result.records;
				console.log(this.companyinsuranceoption);
			}
			
			);
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.deductionCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
    compareTwoDates(f){
        if(new Date(f.controls['frmEndDate'].value)<new Date(f.controls['frmStartDate'].value)){
           this.error={isError:true,errorMessage:'End Date cannot before start date'};
        }
     }
    // Open form for create Deduction Code
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.fundsArray = [];
        this.fundsItemArray = [];
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
         this.model = {
             id: 0,
             retirementPlanId:'',
             retirementPlanDescription: '',
             retirementPlanArbicDescription: '',
             retirementPlanAccountNumber: null,
             retirementPlanMatchPercent: null,
             retirementPlanMaxPercent:null,
			 retirementPlanMaxAmount:null,
             bonus: false,
             loans: false,
			 watingMethod:1,
			 
             watingPeriods: null,
			 retirementAge:null,
			 retirementContribution:null,
			 retirementPlanAmount:null,
			 retirementPlanPercent:null,
			 retirementFrequency:0,
			 helthInsuranceId:0,
             retirementEntranceDate:'',
             subItems:[],
             insCompanyId:''
            
        };
		
		
        
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({retirementFrequency: 0,helthInsuranceId:0});
		this.islifeTimeValid=true;
		this.islifeTimeValidperyear=true;
		this.islifeTimepercent=true;
		this.islifeTimeMaxpercent=true;
		this.islifeTimeValidEmployerpay=true;
    }


    //function call for creating new Deduction Code
    CreateDeductionCode(f: NgForm, event: Event) {
        this.model.retirementEntranceDate = this.entranceDate;
        event.preventDefault();
        var locIdx = this.model.retirementPlanId;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.deductionCodeSetupService.checkDuplicateDeductionCodeId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code
                    this.deductionCodeSetupService.createDeductionCode(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        }
    }

    //edit department by row
    edit(row: RetirementPlanSetup) {
		this.error={isError:false,errorMessage:''};
		this.islifeTimeValid= true;
		this.islifeTimeValidperyear= true;
		this.islifeTimeValidperperiod=true;
		this.islifeTimeValidamount= true;
        this.showCreateForm = true;
        this.model = Object.assign({},row);
		if(this.model.watingMethod==1)
		{
       
			this.fmamount=true;
		}
		else if(this.model.watingMethod==2)
		{
			this.fpercent=true;
		}
       else
	   {
		this.fmamount=false;
		this.fpercent=false;
	   }
        this.retirementPlanId = row.retirementPlanId;
        this.isUnderUpdate = true;
        this.diductionIdvalue = this.model.retirementPlanId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        this.model.retirementPlanId = this.retirementPlanId;
       
        //Call service api for Creating new Deduction Code
       this.deductionCodeSetupService.updateDeductionCode(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });

    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedDeductionCodes = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDeductionCodes.push(this.selected[i].id);
        }
        this.deductionCodeSetupService.deleteDeductionCode(selectedDeductionCodes).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.deductionCodeSetupService.searchDeductionCodelist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed.';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'mm/yyyy',
    };

    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        console.log('this.frmStartDate', event);
		 if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
       console.log('this.frmEndDate', this.frmEndDate, event);

        if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.retirementPlanId = '';
            return false;
        }
    }


    keyPress(event: any) {
        const pattern = /^[0-9\-.()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
	
	validateDecimal(event){
		console.log(event);
	}
	
	checkdecimal(digit)
	{
		console.log(digit);
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValid = false;
			console.log(this.islifeTimeValid);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValid = false;
			console.log(this.islifeTimeValid);
		}
		
		else{
			this.islifeTimeValid = true;
			console.log(this.islifeTimeValid);
		}
	
	}
	checkdecimalperyear(digit)
	{
		console.log(digit);
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidperyear = false;
			console.log(this.islifeTimeValidperyear);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidperyear = false;
			console.log(this.islifeTimeValidperyear);
		}
		else{
			this.islifeTimeValidperyear = true;
			console.log(this.islifeTimeValidperyear);
		}
	
	}
	
	checkdecimalperperiod(digit)
	{
		console.log(digit);
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidperperiod = false;
			console.log(this.islifeTimeValidperperiod);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidperperiod = false;
			console.log(this.islifeTimeValidperperiod);
		}
		else{
			this.islifeTimeValidperperiod = true;
			console.log(this.islifeTimeValidperperiod);
		}
	
	}
	checkdecimalamount(digit)
	{
		console.log(digit);
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidamount = false;
			console.log(this.islifeTimeValidamount);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidamount = false;
			console.log(this.islifeTimeValidamount);
		}
		
		else{
			this.islifeTimeValidamount = true;
			console.log(this.islifeTimeValidamount);
		}
	
	}

	checkdecimalemployeepay(digit)
    {
        console.log(digit);
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidEmployerpay = false;
            console.log(this.islifeTimeValidEmployerpay);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidEmployerpay = false;
            console.log(this.islifeTimeValidEmployerpay);
        }

        else{
            this.islifeTimeValidEmployerpay = true;
            console.log(this.islifeTimeValidEmployerpay);
        }

    }
	checkdecimalpercent(digit)
    {
        console.log(digit);
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 5) || (this.tempp[1] != null && this.tempp[1].length > 5))){
            console.log("in the condition");
            this.islifeTimepercent = false;
            console.log(this.islifeTimepercent);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))){
            console.log("in the condition");
            this.islifeTimepercent = false;
            console.log(this.islifeTimepercent);
        }

        else{
            this.islifeTimepercent = true;
            console.log(this.islifeTimepercent);
        }

    }
	checkdecimalmaximumpercent(digit)
    {
        console.log(digit);
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 5) || (this.tempp[1] != null && this.tempp[1].length > 5))){
            console.log("in the condition");
            this.islifeTimeMaxpercent = false;
            console.log(this.islifeTimeMaxpercent);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))){
            console.log("in the condition");
            this.islifeTimeMaxpercent = false;
            console.log(this.islifeTimeMaxpercent);
        }

        else{
            this.islifeTimeMaxpercent = true;
            console.log(this.islifeTimeMaxpercent);
        }

    }


    addFundsRow($event){
        this.fundsArray.push(this.newAttribute);
        this.newAttribute = {};
        //console.log('funds arr '+JSON.stringify(this.fundsArray)+'/n');
        this.subItemsObj ={
          "fundID" : '',
          "fundName" : '',
          "active" : false
        };
        this.model.subItems.push(this.subItemsObj);
        //console.log(JSON.stringify(this.model.subItems));
    }

    checkUncheck(event,index){
        if(event.target.checked){
            this.fundsRowId = {id : index} ;
            this.fundsItemArray.push(this.fundsRowId);
           /* console.log(JSON.stringify(this.fundsItemArray));*/
        } else{
            for(var i=0;i<this.fundsItemArray.length;i++){
                if(this.fundsItemArray[i].id==index){
                  /*  console.log('indexxx '+ i);*/
                    this.fundsItemArray.splice(i,1);
                    /*console.log(JSON.stringify(this.fundsItemArray));*/
                }
            }
        }
    }

    removeFundsRow() {
                if (this.fundsItemArray.length === this.fundsArray.length) {
                    this.fundsItemArray = [];
                    this.fundsArray = [];
                    this.model.subItems=[];
                }

                this.fundsItemArray = this.sortArray(this.fundsItemArray,'desc','id');

                for (var j = 0; j < this.fundsItemArray.length; j++) {
                    this.fundsArray.splice(this.fundsItemArray[j].id, 1);
                    this.model.subItems.splice(this.fundsItemArray[j].id, 1);
                }
                console.log(JSON.stringify(this.model.subItems));
                this.fundsItemArray = [];

     }

sortArray(arrayItem:any, direction:string, field:string) {
       if(direction==='desc') {
           arrayItem.sort(function (name1, name2) {
               if (name1[field] > name2[field]) {
                   return -1;
               } else if (name1[field] < name2[field]) {
                   return 1;
               } else {
                   return 0;
               }
           });
       }else{
           arrayItem.sort(function (name1, name2) {
               if (name1[field] < name2[field]) {
                   return -1;
               } else if (name1[field] > name2[field]) {
                   return 1;
               } else {
                   return 0;
               }
           });
       }
       return arrayItem;
    }

    onEndDateChangedEntrance(event: IMyDateModel): void {
        this.entranceDate = event.jsdate.toString();
    }



}
