import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { MiscellaneousBenefits } from '../../_models/miscellaneous-benefits/miscellaneous-benefits.module';
import { MiscellaneousBenefitsService } from '../../_services/miscellaneous-benefits/miscellaneous-benefits.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
@Component({
    selector: 'miscellaneous-benefits',
    templateUrl: './miscellaneous-benefits.component.html',
    providers: [MiscellaneousBenefitsService,CommonService]
})
export class MiscellaneousBenefitsComponent {
    page = new Page();
    rows = new Array<MiscellaneousBenefits>();
    temp = new Array<MiscellaneousBenefits>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1420';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    BenefitsId = {};
    positionClassOptions: any;
    benefitsId;
    searchKeyword = '';
    ddPageSize: number = 5;
    model: MiscellaneousBenefits;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    tempp: string[] = [];
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
    error: any = { isError: false, errorMessage: '' };
    islifeTimeValid: boolean = true;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    islifeTimeValidamount: boolean = true;
    dudctionAmountPattern: boolean = true;
    monthlyAmountPattern: boolean = true;
    yearlyAmountPattern: boolean = true;
    lifetimeAmountPattern: boolean = true;
    benefitAmountPattern: boolean = true;
    employermonthlyAmountPattern: boolean = true;
    employeryearlyAmountPattern: boolean = true;
    employerlifetimeAmountPattern: boolean = true;
    varToBeFalse: any;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    isdedYearly: boolean = true;
    isbenYearly: boolean = true;
    isdedLifetime: boolean = true;
    isbenLifetime: boolean = true;
    isdedTotal: boolean = true;
    isbenTotal: boolean = true;
    BenefitsIdvalue: number;
    isEmployeeFormdisabled: boolean = false;
    isEmployerFormdisabled: boolean = false;
    MISCELLANEOUS_BENEFITS_ID:any;
    MISCELLANEOUS_BENEFITS_DESCRIPTION:any;
    MISCELLANEOUS_BENEFITS_ARABIC_DESCRIPTION:any;
    MISCELLANEOUS_BENEFITS_FREQUENCY:any;
    MISCELLANEOUS_BENEFITS_START_DATE:any;
    MISCELLANEOUS_BENEFITS_END_DATE:any;
    MISCELLANEOUS_BENEFITS_INACTIVE:any;
    MISCELLANEOUS_BENEFITS_METHOD:any;
    MISCELLANEOUS_BENEFITS_DEDUCTION_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_DEDUCTION_PERCENT:any;
    MISCELLANEOUS_BENEFITS_MONTHLY_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_YEARLY_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_LIFETIME_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_INACTIVE:any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_METHOD:any;
    MISCELLANEOUS_BENEFITS_BENEFIT_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_BENEFIT_PERCENT:any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_MONTHLY_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_YEARLY_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_LIFETIME_AMOUNT:any;
    MISCELLANEOUS_BENEFITS_ACTION_LABEL:any;
    MISCELLANEOUS_BENEFITS_CREATE_LABEL:any;
    MISCELLANEOUS_BENEFITS_SAVE_LABEL:any;
    MISCELLANEOUS_BENEFITS_CLEAR_LABEL:any;
    MISCELLANEOUS_BENEFITS_CANCEL_LABEL:any;
    MISCELLANEOUS_BENEFITS_UPDATE_LABEL:any;
    MISCELLANEOUS_BENEFITS_DELETE_LABEL:any;
    MISCELLANEOUS_BENEFITS_CREATE_FORM_LABEL:any;
    MISCELLANEOUS_BENEFITS_UPDATE_FORM_LABEL:any;
    MISCELLANEOUS_BENEFITS_SEARCH:any;
    MISCELLANEOUS_BENEFITS_BENEFIT_START_DATE:any;
    MISCELLANEOUS_BENEFITS_BENEFIT_END_DATE:any;
    MISCELLANEOUS_BENEFITS_TABLEVIEW:any;
    MISCELLANEOUS_BENEFITS_EMPLOYEE:any;
    MISCELLANEOUS_BENEFITS_EMPLOYER:any;
    MISCELLANEOUS_MAXBENEFITS:any;
    MISCELLANEOUS_MAXDUDECTN:any;
    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    frequencyArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semiannually", "Annually"];
    employeeMethodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
    employerMethodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    dataSource: Observable<any>;
    asyncSelected: string;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    dudctionAmountPercentPattern: boolean = true;
    benefitAmountPercentPattern: boolean = true;

    constructor(private router: Router,
        private miscellaneousBenefitsService: MiscellaneousBenefitsService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'BenefitsId';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_FREQUENCY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_START_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_END_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_INACTIVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_METHOD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DEDUCTION_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DEDUCTION_PERCENT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_MONTHLY_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_YEARLY_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_LIFETIME_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_INACTIVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_METHOD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_PERCENT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_MONTHLY_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_YEARLY_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_LIFETIME_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ACTION_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_START_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_END_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYEE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_MAXBENEFITS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'MISCELLANEOUS_MAXDUDECTN', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];
        this.MISCELLANEOUS_BENEFITS_ID = this.defaultFormValues[0];
        this.MISCELLANEOUS_BENEFITS_DESCRIPTION = this.defaultFormValues[1];
        this.MISCELLANEOUS_BENEFITS_ARABIC_DESCRIPTION = this.defaultFormValues[2];
        this.MISCELLANEOUS_BENEFITS_FREQUENCY = this.defaultFormValues[3];
        this.MISCELLANEOUS_BENEFITS_START_DATE = this.defaultFormValues[4];
        this.MISCELLANEOUS_BENEFITS_END_DATE = this.defaultFormValues[5];
        this.MISCELLANEOUS_BENEFITS_INACTIVE = this.defaultFormValues[6];
        this.MISCELLANEOUS_BENEFITS_METHOD = this.defaultFormValues[7];
        this.MISCELLANEOUS_BENEFITS_DEDUCTION_AMOUNT = this.defaultFormValues[8];
        this.MISCELLANEOUS_BENEFITS_DEDUCTION_PERCENT = this.defaultFormValues[9];
        this.MISCELLANEOUS_BENEFITS_MONTHLY_AMOUNT = this.defaultFormValues[10];
        this.MISCELLANEOUS_BENEFITS_YEARLY_AMOUNT = this.defaultFormValues[11];
        this.MISCELLANEOUS_BENEFITS_LIFETIME_AMOUNT = this.defaultFormValues[12];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_INACTIVE = this.defaultFormValues[13];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_METHOD = this.defaultFormValues[14];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_AMOUNT = this.defaultFormValues[15];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_PERCENT = this.defaultFormValues[16];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_MONTHLY_AMOUNT = this.defaultFormValues[17];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_YEARLY_AMOUNT = this.defaultFormValues[18];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_LIFETIME_AMOUNT = this.defaultFormValues[19];
        this.MISCELLANEOUS_BENEFITS_ACTION_LABEL = this.defaultFormValues[20];
        this.MISCELLANEOUS_BENEFITS_CREATE_LABEL = this.defaultFormValues[21];
        this.MISCELLANEOUS_BENEFITS_SAVE_LABEL = this.defaultFormValues[22];
        this.MISCELLANEOUS_BENEFITS_CLEAR_LABEL = this.defaultFormValues[23];
        this.MISCELLANEOUS_BENEFITS_CANCEL_LABEL = this.defaultFormValues[24];
        this.MISCELLANEOUS_BENEFITS_UPDATE_LABEL = this.defaultFormValues[25];
        this.MISCELLANEOUS_BENEFITS_DELETE_LABEL = this.defaultFormValues[26];
        this.MISCELLANEOUS_BENEFITS_CREATE_FORM_LABEL = this.defaultFormValues[27];
        this.MISCELLANEOUS_BENEFITS_UPDATE_FORM_LABEL = this.defaultFormValues[28];
        this.MISCELLANEOUS_BENEFITS_SEARCH = this.defaultFormValues[29];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_START_DATE = this.defaultFormValues[30];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_END_DATE = this.defaultFormValues[31];
        this.MISCELLANEOUS_BENEFITS_TABLEVIEW = this.defaultFormValues[32];
        this.MISCELLANEOUS_BENEFITS_EMPLOYEE = this.defaultFormValues[33];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER = this.defaultFormValues[34];
        this.MISCELLANEOUS_MAXBENEFITS = this.defaultFormValues[35];
        this.MISCELLANEOUS_MAXDUDECTN = this.defaultFormValues[36];
        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.benefitsId);
        }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.miscellaneousBenefitsService.getPositionClassList().then(data => {
            this.positionClassOptions = data.result.records;
        });

    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'frequency') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                        row[gridFieldName] === 7 ? 'Annually' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

    getReportPositionsAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.positionClassOptions.filter((id: any) => {
                return query.test(id.benefitsId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        console.log('E' ,e);
        this.miscellaneousBenefitsService.getMiscellaneousBenefits(e.item.id).then(pagedData => {
            console.log('PageData', pagedData);
            this.model = pagedData.result;
            this.model.id = 0;
            this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
            this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);

        });
    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.miscellaneousBenefitsService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;

        });
    }

    // Open form for create location
    Create() {
        this.modelStartDate = null;
        this.modelEndDate = null;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.islifeTimeValidamount = true;
        this.isEmployerFormdisabled = false;
        this.isEmployeeFormdisabled = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 800);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            benefitsId: null,
            desc: '',
            arbicDesc: '',
            frequency: 0,
            startDate: null,
            endDate: null,
            inactive: false,
            method: 0,
            dudctionAmount: null,
            dudctionPercent: null,
            monthlyAmount: null,
            yearlyAmount: null,
            lifetimeAmount: null,
            empluyeeerinactive: false,
            empluyeeermethod: 0,
            benefitAmount: null,
            benefitPercent: null,
            employermonthlyAmount: null,
            employeryearlyAmount: null,
            employerlifetimeAmount: null
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ frequency: 0, method: 0, empluyeeermethod: 0 });
        this.isEmployerFormdisabled = false;
        this.isEmployeeFormdisabled = false;
    }


    //function call for creating new location
    CreateMiscellaneousBenefits(f: NgForm, event: Event) {
        this.model.startDate = this.frmStartDate;
        this.model.endDate = this.frmEndDate;
        event.preventDefault();
        var locIdx = this.model.benefitsId;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.miscellaneousBenefitsService.checkDuplicateMiscellaneousBenefitsId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code
                    this.miscellaneousBenefitsService.createMiscellaneousBenefits(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: MiscellaneousBenefits) {
        this.error = { isError: false, errorMessage: '' };
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.BenefitsId = row.benefitsId;
        this.isUnderUpdate = true;
        this.islifeTimeValidamount = true;

        this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
        this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
        
        let strDate = new Date(this.model.startDate);
        this.startDate = (strDate.getTime())/1000;
        let enddate = new Date(this.model.endDate);
        this.endDate = (enddate.getTime())/1000;
        this.BenefitsIdvalue = this.model.benefitsId;
        this.benefitsId = this.model.benefitsId;
        if (this.model.inactive == true) {
            this.isEmployeeFormdisabled = true;
        } else {
            this.isEmployeeFormdisabled = false;
        }

        if (this.model.empluyeeerinactive == true) {
            this.isEmployerFormdisabled = true;
        } else {
            this.isEmployerFormdisabled = false;
        }

        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        this.model.benefitsId = this.BenefitsIdvalue;
        //Call service api for updating selected department
        this.miscellaneousBenefitsService.updateMiscellaneousBenefits(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedMiscellaneousBenefitss = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedMiscellaneousBenefitss.push(this.selected[i].id);
        }
        this.miscellaneousBenefitsService.deleteMiscellaneousBenefits(selectedMiscellaneousBenefitss).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.benefitsId = 0;
            return false;
        }
    }
    checkFrequency(event) {
        if (event.target.value == 0) {
            return false;
        }
    }
    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };
    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;

        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }
    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkdecimal(digit) {
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValid = false;
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValid = false;
        }
        else {
            this.islifeTimeValid = true;
        }

    }
    checkdecimalamount(digit, varToBeFalse) {
        var temp = true;
        if (digit == '' || digit == null) {
            if (varToBeFalse == 'dudctionAmountPattern') {
                this.dudctionAmountPattern = true;
            }
            if (varToBeFalse == 'monthlyAmountPattern') {
                this.monthlyAmountPattern = true;
            }
            if (varToBeFalse == 'yearlyAmountPattern') {
                this.yearlyAmountPattern = true;
            }
            if (varToBeFalse == 'lifetimeAmountPattern') {
                this.lifetimeAmountPattern = true;
            }
            if (varToBeFalse == 'benefitAmountPattern') {
                this.benefitAmountPattern = true;
            }
            if (varToBeFalse == 'employermonthlyAmountPattern') {
                this.employermonthlyAmountPattern = true;
            }
            if (varToBeFalse == 'employeryearlyAmountPattern') {
                this.employeryearlyAmountPattern = true;
            }
            if (varToBeFalse == 'employerlifetimeAmountPattern') {
                this.employerlifetimeAmountPattern = true;
            }
            return;
        }

        if (digit != null) {
            this.tempp = digit.toString().split(".");
            if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            } else {
                temp = true;
            }
        }

        if (varToBeFalse == 'dudctionAmountPattern') {
            this.dudctionAmountPattern = temp;
        }
        if (varToBeFalse == 'monthlyAmountPattern') {
            this.monthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'yearlyAmountPattern') {
            this.yearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'lifetimeAmountPattern') {
            this.lifetimeAmountPattern = temp;
        }
        if (varToBeFalse == 'benefitAmountPattern') {
            this.benefitAmountPattern = temp;
        }
        if (varToBeFalse == 'employermonthlyAmountPattern') {
            this.employermonthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employeryearlyAmountPattern') {
            this.employeryearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employerlifetimeAmountPattern') {
            this.employerlifetimeAmountPattern = temp;
        }
    }

    checkdecimalamountSophasticated(digit, varToBeFalse) {
        var temp = true;
        if (digit == '' || digit == null) {
            if (varToBeFalse == 'dudctionAmountPattern') {
                this.dudctionAmountPattern = true;
            }
            if (varToBeFalse == 'monthlyAmountPattern') {
                this.monthlyAmountPattern = true;
            }
            if (varToBeFalse == 'yearlyAmountPattern') {
                this.yearlyAmountPattern = true;
            }
            if (varToBeFalse == 'lifetimeAmountPattern') {
                this.lifetimeAmountPattern = true;
            }
            if (varToBeFalse == 'benefitAmountPattern') {
                this.benefitAmountPattern = true;
            }
            if (varToBeFalse == 'employermonthlyAmountPattern') {
                this.employermonthlyAmountPattern = true;
            }
            if (varToBeFalse == 'employeryearlyAmountPattern') {
                this.employeryearlyAmountPattern = true;
            }
            if (varToBeFalse == 'employerlifetimeAmountPattern') {
                this.employerlifetimeAmountPattern = true;
            }
            if (varToBeFalse == 'dudctionAmountPercentPattern') {
                this.dudctionAmountPercentPattern = true;
            }
            if (varToBeFalse == 'benefitAmountPercentPattern') {
                this.benefitAmountPercentPattern = true;
            }
            return;
        }

        if (digit != null) {
            this.tempp = digit.toString().split(".");
            if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
                temp = false;
            } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
                temp = false;
            } else {
                temp = true;
            }
        }

        if (varToBeFalse == 'dudctionAmountPattern') {
            this.dudctionAmountPattern = temp;
        }
        if (varToBeFalse == 'monthlyAmountPattern') {
            this.monthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'yearlyAmountPattern') {
            this.yearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'lifetimeAmountPattern') {
            this.lifetimeAmountPattern = temp;
        }
        if (varToBeFalse == 'benefitAmountPattern') {
            this.benefitAmountPattern = temp;
        }
        if (varToBeFalse == 'employermonthlyAmountPattern') {
            this.employermonthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employeryearlyAmountPattern') {
            this.employeryearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employerlifetimeAmountPattern') {
            this.employerlifetimeAmountPattern = temp;
        }
        if (varToBeFalse == 'dudctionAmountPercentPattern') {
            this.dudctionAmountPercentPattern = temp;
        }
        if (varToBeFalse == 'benefitAmountPercentPattern') {
            this.benefitAmountPercentPattern = temp;
        }
    }

    checkEmployeeForm(event) {
        if (event.target.checked == true) {
            this.isEmployeeFormdisabled = true;
        } else {
            this.isEmployeeFormdisabled = false;
        }
    }

    checkEmployerForm(event) {
        if (event.target.checked == true) {
            this.isEmployerFormdisabled = true;
        } else {
            this.isEmployerFormdisabled = false;
        }
    }
    checkdedmonthlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.monthlyAmount != null && this.model.yearlyAmount != null && this.yearlyAmountPattern && this.monthlyAmountPattern) {
            if (temp >= this.model.yearlyAmount && varrr == 'dedmonthly') {
                this.isdedYearly = false;
            }
            else if (temp <= this.model.monthlyAmount && varrr == 'dedyearly') {
                this.isdedYearly = false;
            }
            else if (temp < this.model.yearlyAmount && varrr == 'dedmonthly') {
                this.isdedYearly = true;
            }
            else if (temp > this.model.monthlyAmount && varrr == 'dedyearly') {
                this.isdedYearly = true;
            }
        }
        else {
            this.isdedYearly = true;
        }

    }
    checkbenmonthlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.employermonthlyAmount != null && this.model.employeryearlyAmount != null && this.employermonthlyAmountPattern && this.employeryearlyAmountPattern) {
            if (temp >= this.model.employeryearlyAmount && varrr == 'benmonthly') {
                this.isbenYearly = false;
            }
            else if (temp <= this.model.employermonthlyAmount && varrr == 'benyearly') {
                this.isbenYearly = false;
            }
            else if (temp < this.model.employeryearlyAmount && varrr == 'benmonthly') {
                this.isbenYearly = true;
            }
            else if (temp > this.model.employermonthlyAmount && varrr == 'benyearly') {
                this.isbenYearly = true;
            }
        }
        else {
            this.isbenYearly = true;
        }

    }
    checkdedyearlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.lifetimeAmount != null && this.model.yearlyAmount != null && this.lifetimeAmountPattern && this.yearlyAmountPattern) {
            if (temp > this.model.yearlyAmount && varrr == 'dedlifetime') {
                this.isdedLifetime = true;
            }
            else if (temp < this.model.lifetimeAmount && varrr == 'dedyearly') {
                this.isdedLifetime = true;
            }
            else if (temp <= this.model.yearlyAmount && varrr == 'dedlifetime') {
                this.isdedLifetime = false;
            }
            else if (temp >= this.model.lifetimeAmount && varrr == 'dedyearly') {
                this.isdedLifetime = false;
            }
        }
        else {
            this.isdedLifetime = true;
        }

    }
    checkbenyearlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.employerlifetimeAmount != null && this.model.employeryearlyAmount != null && this.employeryearlyAmountPattern && this.employerlifetimeAmountPattern) {
            if (temp > this.model.employeryearlyAmount && varrr == 'benlifetime') {
                this.isbenLifetime = true;
            }
            else if (temp < this.model.employerlifetimeAmount && varrr == 'benyearly') {
                this.isbenLifetime = true;
            }
            else if (temp <= this.model.employeryearlyAmount && varrr == 'benlifetime') {
                this.isbenLifetime = false;
            }
            else if (temp >= this.model.employerlifetimeAmount && varrr == 'benyearly') {
                this.isbenLifetime = false;
            }
        }
        else {
            this.isbenLifetime = true;
        }

    }
    checkdedlifetimeamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.lifetimeAmount != null && this.model.dudctionAmount != null && this.dudctionAmountPattern && this.lifetimeAmountPattern) {
            if (temp > this.model.dudctionAmount && varrr == 'dedlifetime') {
                this.isdedTotal = true;
            }
            else if (temp < this.model.lifetimeAmount && varrr == 'dedTotal') {
                this.isdedTotal = true;
            }
            else if (temp <= this.model.dudctionAmount && varrr == 'dedlifetime') {
                this.isdedTotal = false;
            }
            else if (temp >= this.model.lifetimeAmount && varrr == 'dedTotal') {
                this.isdedTotal = false;
            }
        }
        else {
            this.isdedTotal = true;
        }

    }
    checkbenlifetimeamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.employerlifetimeAmount != null && this.model.benefitAmount != null && this.employerlifetimeAmountPattern && this.benefitAmountPattern) {
            if (temp > this.model.benefitAmount && varrr == 'benlifetime') {
                this.isbenTotal = true;
            }
            else if (temp < this.model.employerlifetimeAmount && varrr == 'benTotal') {
                this.isbenTotal = true;
            }
            else if (temp <= this.model.benefitAmount && varrr == 'benlifetime') {
                this.isbenTotal = false;
            }
            else if (temp >= this.model.employerlifetimeAmount && varrr == 'benTotal') {
                this.isbenTotal = false;
            }
        }
        else {
            this.isbenTotal = true;
        }

    }
}


