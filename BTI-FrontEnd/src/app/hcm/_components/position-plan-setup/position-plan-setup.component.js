"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var page_1 = require("../../../_sharedresource/page");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var router_1 = require("@angular/router");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var position_plan_setup_service_1 = require("../../_services/position-plan-setup/position-plan-setup.service");
var PositionPlanSetupComponent = (function () {
    function PositionPlanSetupComponent(router, positionPlanSetupService, getScreenDetailService, alertService, route) {
        this.router = router;
        this.positionPlanSetupService = positionPlanSetupService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.route = route;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1314';
        this.message = { 'type': '', 'text': '' };
        this.positionPlanId = {};
        this.positionId = '';
        this.currentPositionId = '';
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.pattern = /^[\d]*$/;
        this.myOptions = {
            // other options...
            dateFormat: 'dd.mm.yyyy',
        };
        // Initialized to specific date (09.10.2018).
        this.startDateModel = { date: { year: 2018, month: 10, day: 9 } };
        // Initialized to specific date (09.10.2018).
        this.endDateModel1 = { date: { year: 2018, month: 10, day: 9 } };
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for position  screen
        this.defaultFormValues = [
            { 'fieldName': 'POSITION_PLAN_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' }
        ];
    }
    PositionPlanSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.positionId = params['positionId']; // (+) converts string 'id' to a number
            _this.currentPositionId = params['id'];
            _this.setPage({ offset: 0 });
            _this.currentLanguage = localStorage.getItem('currentLanguage');
            // getting screen labels, help messages and validation messages
            _this.getScreenDetailService.getScreenDetail(_this.moduleCode, _this.screenCode).then(function (data) {
                _this.moduleName = data.result.moduleName;
                _this.screenName = data.result.dtoScreenDetail.screenName;
                _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                var _loop_1 = function (j) {
                    var fieldKey = _this.availableFormValues[j]['fieldName'];
                    var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                    var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    if (objAvailable['listDtoFieldValidationMessage']) {
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    }
                };
                for (var j = 0; j < _this.availableFormValues.length; j++) {
                    _loop_1(j);
                }
            });
            // In a real app: dispatch action to load the details here.
        });
    };
    // setting pagination
    PositionPlanSetupComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        console.log(this.currentPositionId);
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.positionPlanSetupService.getlist(this.page, this.searchKeyword, this.currentPositionId).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.currentPositionId = _this.currentPositionId;
        });
    };
    // optional date changed callback
    PositionPlanSetupComponent.prototype.onStartDateChanged = function (event) {
        this.positionPlanStart = event.jsdate;
    };
    // optional date changed callback
    PositionPlanSetupComponent.prototype.onEndDateChanged = function (event) {
        this.positionPlanEnd = event.jsdate;
    };
    // Open form for create position class
    PositionPlanSetupComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionId: '',
            positionPlanId: '',
            positionPlanDesc: '',
            positionPlanArbicDesc: '',
            positionPlanStart: this.startDateModel,
            positionPlanEnd: this.startDateModel,
            inactive: true,
        };
    };
    // Clear form to reset to default blank
    PositionPlanSetupComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    // function call for creating new positionClass
    PositionPlanSetupComponent.prototype.CreatePositionPlanSetup = function (f, event) {
        var _this = this;
        event.preventDefault();
        var posIdx = this.model.positionPlanId;
        this.model.positionId = this.currentPositionId;
        this.model.positionPlanStart = this.positionPlanStart;
        this.model.positionPlanEnd = this.positionPlanEnd;
        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            // Check for duplicate positionClass Id according to it create new position class
            this.positionPlanSetupService.checkDuplicatePositionPlanSetupId(posIdx).then(function (response) {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message + ' !';
                    }, 100);
                }
                else {
                    // Call service api for Creating new position class
                    _this.positionPlanSetupService.createPositionPlanSetup(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new position class
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin !';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin !';
                }, 100);
            });
        }
    };
    // edit position class by row
    PositionPlanSetupComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.positionPlanId = row.positionPlanId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    PositionPlanSetupComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        // Call service api for updating selected position class
        this.positionPlanSetupService.updatePositionClass(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                _this.setPage({ offset: 0 });
                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin !';
            }, 100);
        });
    };
    PositionPlanSetupComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    // delete position class by passing whole object of perticular position class
    PositionPlanSetupComponent.prototype.delete = function () {
        var _this = this;
        var selectedPositionClass = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedPositionClass.push(this.selected[i].id);
        }
        this.positionPlanSetupService.deletePositionClass(selectedPositionClass).then(function (data) {
            var datacode = data.code;
            if (datacode === 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            // this.message.text = data.btiMessage.message + " !";
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.result.messageType + ' !';
            }, 100);
            // Refresh the Grid data after deletion of position class
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    };
    // default list on page
    PositionPlanSetupComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search position class by keyword
    PositionPlanSetupComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.positionPlanSetupService.searchPositionClasslist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    PositionPlanSetupComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    PositionPlanSetupComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    };
    PositionPlanSetupComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    return PositionPlanSetupComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], PositionPlanSetupComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], PositionPlanSetupComponent.prototype, "myScrollContainer", void 0);
PositionPlanSetupComponent = __decorate([
    core_1.Component({
        selector: 'positionPlanSetup',
        templateUrl: './position-plan-setup.component.html',
        providers: [position_plan_setup_service_1.PositionPlanSetupService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        position_plan_setup_service_1.PositionPlanSetupService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService,
        router_1.ActivatedRoute])
], PositionPlanSetupComponent);
exports.PositionPlanSetupComponent = PositionPlanSetupComponent;
//# sourceMappingURL=position-plan-setup.component.js.map