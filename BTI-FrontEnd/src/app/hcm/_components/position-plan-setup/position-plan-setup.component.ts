import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Page} from '../../../_sharedresource/page';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {PositionPlanSetupService} from '../../_services/position-plan-setup/position-plan-setup.service';
import {PositionPlanSetupModule} from '../../_models/position-plan-setup/position-plan-setup.module';
import {NgForm} from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';

@Component({
  selector: 'positionPlanSetup',
  templateUrl: './position-plan-setup.component.html',
  providers: [PositionPlanSetupService]
})
export class PositionPlanSetupComponent implements OnInit {


    page = new Page();
    rows = new Array<PositionPlanSetupModule>();
    temp = new Array<PositionPlanSetupModule>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1314';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    positionPlanId = {};
    positionId = '';
    currentPositionId = '';
    positionPlanStart;
    positionPlanEnd;
    startDate;
    endDate;
	tempp:string[]=[];
	error:any={isError:false,errorMessage:''};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: PositionPlanSetupModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
	modelStartDate;
    modelEndDate;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    private sub: any;
    pattern = /^[\d]*$/ ;
    isPositionPlanFormdisabled: boolean = false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };
    // Initialized to specific date (09.10.2018).
    public startDateModel: any = { date: { year: 2018, month: 10, day: 9 } };

    // Initialized to specific date (09.10.2018).
    public endDateModel1: any = { date: { year: 2018, month: 10, day: 9 } };

    constructor( private router: Router,
                 private positionPlanSetupService: PositionPlanSetupService,
                 private getScreenDetailService: GetScreenDetailService,
                 private alertService: AlertService,
                 private route: ActivatedRoute) {

        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        // default form parameter for position  screen
        this.defaultFormValues = [
            {'fieldName': 'POSITION_PLAN_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_START_DATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_END_DATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_INACTIVE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_ACTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': ''}
        ];
    }

  ngOnInit() {

      this.sub = this.route.params.subscribe(params => {

          this.positionId =  params['positionId']; // (+) converts string 'id' to a number

          this.currentPositionId = params['id'];

          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });


          this.currentLanguage = localStorage.getItem('currentLanguage');


          // getting screen labels, help messages and validation messages
          this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

              this.moduleName = data.result.moduleName;
              this.screenName = data.result.dtoScreenDetail.screenName;
              this.availableFormValues = data.result.dtoScreenDetail.fieldList;
              for (let j = 0; j < this.availableFormValues.length; j++) {
                  let fieldKey = this.availableFormValues[j]['fieldName'];
                  let objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                  let objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                  objDefault['fieldValue'] = objAvailable['fieldValue'];
                  objDefault['helpMessage'] = objAvailable['helpMessage'];
                  if (objAvailable['listDtoFieldValidationMessage']) {
                      objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                  }
              }
          });

          // In a real app: dispatch action to load the details here.
      });


  }

    // setting pagination
    setPage(pageInfo) {

        console.log(this.currentPositionId)

        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.positionPlanSetupService.getlist(this.page, this.searchKeyword, this.currentPositionId).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.currentPositionId = this.currentPositionId;
        });
    }

    // optional date changed callback
    onStartDateChanged(event: IMyDateModel): void {
        this.positionPlanStart = event.jsdate;
        this.startDate = event.epoc;
        
		 if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.positionPlanEnd = event.jsdate;
        this.endDate = event.epoc;
       

        if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    // Open form for create position class
    Create() {
		this.modelStartDate=null;
		this.modelEndDate=null;
        this.showCreateForm = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionId: '',
            positionPlanId: '',
            positionPlanDesc: '',
            positionPlanArbicDesc: '',
            positionPlanStart: this.startDateModel,
            positionPlanEnd: this.startDateModel,
            inactive: false,
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
		var temp = this.model.positionPlanId;
		this.isPositionPlanFormdisabled = false;
        f.resetForm({inactive:false,positionPlanId:temp});
        
    }

    // function call for creating new positionClass
    CreatePositionPlanSetup(f: NgForm, event: Event) {
		
        event.preventDefault();
        let posIdx = this.model.positionPlanId;
        this.model.positionId = this.currentPositionId;
        this.model.positionPlanStart = this.positionPlanStart;
        this.model.positionPlanEnd = this.positionPlanEnd;

        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Check for duplicate positionClass Id according to it create new position class
            this.positionPlanSetupService.checkDuplicatePositionPlanSetupId(posIdx).then(response => {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message + ' !';
                    }, 100);
                } else {
                    // Call service api for Creating new position class
                    this.positionPlanSetupService.createPositionPlanSetup(this.model).then(data => {
                        let datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new position class
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin !';
                        }, 100);
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin !';
                }, 100);
            });

        }
    }

    // edit position class by row
    edit(row: PositionPlanSetupModule) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.positionPlanId = row.positionPlanId;
		this.isPositionPlanFormdisabled = true;
		console.log('model == ',this.model);
		
		this.modelStartDate = this.formatDateFordatePicker(this.model.positionPlanStart);
		this.modelEndDate = this.formatDateFordatePicker(this.model.positionPlanEnd);
		
		this.positionPlanStart = this.model.positionPlanStart;
		this.positionPlanEnd = this.model.positionPlanEnd;
		
		this.model.positionPlanStart = this.positionPlanStart;
		this.model.positionPlanEnd = this.positionPlanEnd;
		
        /*if(this.model.inactive==true){
            this.isPositionPlanFormdisabled = true;
        } else{
            this.isPositionPlanFormdisabled = false;
        }*/
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        // Call service api for updating selected position class
        this.positionPlanSetupService.updatePositionClass(this.model).then(data => {
            let datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;

                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin !';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }
    // delete position class by passing whole object of perticular position class
    delete() {
        let selectedPositionClass = [];
        for (let i = 0; i < this.selected.length; i++) {
            selectedPositionClass.push(this.selected[i].id);
        }
        this.positionPlanSetupService.deletePositionClass(selectedPositionClass).then(data => {
            let datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            // this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of position class
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            let errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search position class by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.positionPlanSetupService.searchPositionClasslist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkpositionplanform(event){
        if(event.target.checked==true){
            this.isPositionPlanFormdisabled = false;
        } else{
            this.isPositionPlanFormdisabled = false;
        }

    }
	formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }

}
