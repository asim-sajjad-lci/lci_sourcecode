import { Component, ElementRef, OnInit, ViewChild, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { OnlyDecimalDirective } from '../../../_sharedresource/onlyDecimal.directive';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { EmployeeDeductionMaintnce } from '../../_models/employee-deduction-maintenance/employee-deduction-maintenance.module';
import { EmployeeDeductionMaintenanceComponentService } from '../../_services/employee-deduction-maintenance/employee-deduction-maintenance-component.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-employee-deduction-maintenance',
  templateUrl: './employee-deduction-maintenance.component.html',
  styleUrls: ['./employee-deduction-maintenance.component.css'],
  providers: [EmployeeDeductionMaintenanceComponentService, CommonService, DatePipe]
})
export class EmployeeDeductionMaintenanceComponent implements OnInit {
  selectedIdRows: any;
  errorMaxLt: string;
  errorMaxPy: string;
  errorMaxPp: string;
  errorDeductn: string;
  isCompMaxTrxnLt: boolean = true;
  isCompMaxTrxnPp: boolean = true;
  isCompMaxTrxnPy: boolean = true;
  isLifeTime: boolean = true;
  isperYear: boolean = true;
  isPerPeriod: boolean = true;
  page = new Page();
  rows = new Array<EmployeeDeductionMaintnce>();
  temp = new Array<EmployeeDeductionMaintnce>();
  selected = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1452';
  moduleName;
  screenName;
  defaultFormValues: Array<any> = [];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  searchKeyword = '';
  ddPageSize: number = 5;
  model: EmployeeDeductionMaintnce;
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  startDateModel;
  endDateModel;
  frmstartDate;
  frmendDate;
  startDate;
  endDate;
  start_date;
  end_date;
  islifeTimeValid: boolean = true;
  islifeTimeValidperyear: boolean = true;
  islifeTimeValidperperiod: boolean = true;
  islifeTimeValidamount: boolean = true;
  islifeTimeValidamountPercent: boolean = true;
  decimalpattern = '/^(((0|[1-9]\d{0,2})(\.\d{2})?)|())$/';
  tempp: string[] = [];
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  empDiductionIdvalue: {};
  error: any = { isError: false, errorMessage: '' };
  isDeductionCodeFormdisabled: boolean = false;
  methodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
  frequencyArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semiannually", "Annually", "Daily/Miscellaneous"];
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;
  @ViewChild('dp2') input: ElementRef;
  deductioncodeIdList: Observable<any>;
  employeeIDList: Observable<any>;
  getDeductionCode: any = [];
  getEmployeeCodeID: any = [];
  typeaheadLoading: boolean;
  employeeFirstName: any;
  deductionDesc: any;
  deductionArabicDesc: any;
  modeldeductionCode: any;
  modelemployeeId: any;
  employeeIdValue: any;
  deductionCodeValue: any;
  empPrimaryIdValue: any;
  deductionPrimaryValue: any;
  tableView = 'TABLE VIEW'
  employeeJoiningDate: any = null;
  isCustom: any;

  payCodeSettings = {
    singleSelection: false,
    enableCheckAll: true,
    text: "Select Paycode",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: "myclass custom-class",
    searchPlaceholderText: "Search Pay Code",
    badgeShowLimit: 2,
    disabled: true
  }
  showSelectedPaycode: any[] = [];
  AllBasedOnPayCodeList: any[] = [];

  EMPDEDUCTION_CODE_ID: any;
  DEDUCTION_CODE_ID: any;
  DEDUCTION_CODE_DESCRIPTION: any;
  DEDUCTION_CODE_ARABIC_DESCRIPTION: any;
  DEDUCTION_CODE_STARTDATE: any;
  DEDUCTION_CODE_ENDDATE: any;
  DEDUCTION_CODE_FREQUENCY: any;
  DEDUCTION_CODE_METHOD: any;
  DEDUCTION_CODE_ACTION: any;
  DEDUCTION_CODE_CREATE_LABEL: any;
  DEDUCTION_CODE_SAVE_LABEL: any;
  DEDUCTION_CODE_CLEAR_LABEL: any;
  DEDUCTION_CODE_CANCEL_LABEL: any;
  DEDUCTION_CODE_UPDATE_LABEL: any;
  DEDUCTION_CODE_DELETE_LABEL: any;
  DEDUCTION_CODE_CREATE_FORM_LABEL: any;
  DEDUCTION_CODE_UPDATE_FORM_LABEL: any;
  DEDUCTION_CODE_AMOUNT: any;
  DEDUCTION_CODE_PER_PERIOD: any;
  DEDUCTION_CODE_PER_YEAR: any;
  DEDUCTION_CODE_LIFETIME: any
  DEDUCTION_CODE_INACTIVE: any;
  DEDUCTION_CODE_NAME: any;
  DEDUCTION_CODE_TRANSREQ: any;
  DEDUCTION_CODE_PERCENT: any;
  DEDUCTION_CODE_MAXTRANS: any;
  DEDUCTION_CODE_SEARCH: any
  DEDUCTION_CODE_EDIT: any;
  BENEFIT_CODE: any;

  tableViewList = {
    first: '5 at a time',
    second: '15 at a time',
    thied: '50 at a time',
    forth: '100 at a time ',
  }
  isCompAmount: boolean = true;
  isCompMaxTrxn: boolean = true;
  errorGrterthn: string = '';
  deductionCodeDuplicate: boolean = false;

  myOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: 'dd-mm-yyyy',
  };
  checkPayCodeValidId: boolean = true;
  checkEmpValidId: boolean = true;
  tempTotal: number;
  daySet: boolean = true;
  dateSet: boolean = false;
  dateSectionDateDisplay: boolean = true;
  dateSectionDayDisplay: boolean = true;

  typeId = 2;
  typeData= [];
  isDeductionAmount = true;

  constructor(private router: Router,
    private employeeDeductionMaintenanceComponentService: EmployeeDeductionMaintenanceComponentService,
    private getScreenDetailService: GetScreenDetailService,
    private alertService: AlertService,
    private commonService: CommonService,
    private datePipe: DatePipe) {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'id';
    this.page.sortBy = 'DESC';

    
    // default form parameter for department  screen
    this.defaultFormValues = [
      { 'fieldName': 'EMPDEDUCTION_CODE_ID', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_ID', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_STARTDATE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_ENDDATE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_FREQUENCY', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_METHOD', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_ACTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_PER_PERIOD', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_PER_YEAR', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_LIFETIME', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_NAME', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_TRANSREQ', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_PERCENT', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_MAXTRANS', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'DEDUCTION_CODE_EDIT', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BENEFIT_CODE', 'fieldValue': '', 'helpMessage': '' },
    ];

    this.EMPDEDUCTION_CODE_ID = this.defaultFormValues[0];
    this.DEDUCTION_CODE_ID = this.defaultFormValues[1];
    this.DEDUCTION_CODE_DESCRIPTION = this.defaultFormValues[2];
    this.DEDUCTION_CODE_ARABIC_DESCRIPTION = this.defaultFormValues[3];
    this.DEDUCTION_CODE_STARTDATE = this.defaultFormValues[4];
    this.DEDUCTION_CODE_ENDDATE = this.defaultFormValues[5];
    this.DEDUCTION_CODE_FREQUENCY = this.defaultFormValues[6];
    this.DEDUCTION_CODE_METHOD = this.defaultFormValues[7];
    this.DEDUCTION_CODE_ACTION = this.defaultFormValues[8];
    this.DEDUCTION_CODE_CREATE_LABEL = this.defaultFormValues[9];
    this.DEDUCTION_CODE_SAVE_LABEL = this.defaultFormValues[10];
    this.DEDUCTION_CODE_CLEAR_LABEL = this.defaultFormValues[11];
    this.DEDUCTION_CODE_CANCEL_LABEL = this.defaultFormValues[12];
    this.DEDUCTION_CODE_UPDATE_LABEL = this.defaultFormValues[13];
    this.DEDUCTION_CODE_DELETE_LABEL = this.defaultFormValues[14];
    this.DEDUCTION_CODE_CREATE_FORM_LABEL = this.defaultFormValues[15];
    this.DEDUCTION_CODE_UPDATE_FORM_LABEL = this.defaultFormValues[16];
    this.DEDUCTION_CODE_AMOUNT = this.defaultFormValues[17];
    this.DEDUCTION_CODE_PER_PERIOD = this.defaultFormValues[18];
    this.DEDUCTION_CODE_PER_YEAR = this.defaultFormValues[19];
    this.DEDUCTION_CODE_LIFETIME = this.defaultFormValues[20];
    this.DEDUCTION_CODE_INACTIVE = this.defaultFormValues[21];
    this.DEDUCTION_CODE_NAME = this.defaultFormValues[22];
    this.DEDUCTION_CODE_TRANSREQ = this.defaultFormValues[23];
    this.DEDUCTION_CODE_PERCENT = this.defaultFormValues[24];
    this.DEDUCTION_CODE_MAXTRANS = this.defaultFormValues[25];
    this.DEDUCTION_CODE_SEARCH = this.defaultFormValues[26];
    this.DEDUCTION_CODE_EDIT = this.defaultFormValues[27];
    this.BENEFIT_CODE = this.defaultFormValues[28];

    // Deduction Code List
    this.deductioncodeIdList = Observable.create((observer: any) => {
      observer.next(this.modeldeductionCode);
    }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

    // Employee ID List
    this.employeeIDList = Observable.create((observer: any) => {
      observer.next(this.modelemployeeId);
    }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));
  }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    this.getTypeData();

    // getting screen labels, help messages and validation messages
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

    //Following shifted from SetPage for better performance
    // Get Deduction ID
    this.employeeDeductionMaintenanceComponentService.getDeductioncodeId().then(data => {
      debugger;
      this.getDeductionCode = data.result;
    });

    // Get Employee Code ID
    this.employeeDeductionMaintenanceComponentService.getEmployeeCodeId().then(data => {
      this.getEmployeeCodeID = data.result;
    });
  }

  // For Employee ID
  getEmployeeIdAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getEmployeeCodeID.filter((id: any) => {
        return query.test(id.employeeId);
      })
    );
  }
  // For Deduction Code ID
  getSuperviserIdAsObservable(token: string): Observable<any> {

    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getDeductionCode.filter((id: any) => {
        return query.test(id.diductionId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  getTypeData(){
    this.employeeDeductionMaintenanceComponentService.getTypeFieldDetails(this.typeId).then(data => {
     if(data.code == 201)
     {
         let val = data.result.records;
         for(let i=0;i<val.length;i++)
         {
             this.typeData.push({id:val[i].id,value:val[i].desc})
         }
     }
     else{
        this.typeData = [];
     }
    });
}

  typeaheadOnSelect(e: TypeaheadMatch, typeofSource: any): void {
    debugger;
    let tmpArr = [];
    this.isDeductionAmount = true;

    if (typeofSource == "employeeId") {
      console.log("E", e);
      this.checkEmpValidId = false;
      this.employeeFirstName = e.item.employeeFirstName;
      this.employeeJoiningDate = e.item.employeeHireDate;
      this.model.employeeMaster["employeeIndexId"] = e.item.employeeIndexId;

      console.log('this.selectedIdRows', this.selectedIdRows)
      if (this.selectedIdRows) {
        tmpArr = this.selectedIdRows.filter(item => item.employeeMaster.employeeIndexId == this.model.employeeMaster["employeeIndexId"] && item.deductionCode.deductionId == this.model.deductionCode["diductionId"]);
        if (tmpArr.length > 0)
          this.deductionCodeDuplicate = true;
        else
          this.deductionCodeDuplicate = false;
      }
    } else if (typeofSource == "deductionId") {
      console.log('E', e)
      this.model.noOfDays = e.item.noOfDays;
      this.model.endDateDays = e.item.endDateDays;
      this.model.startDate = e.item.startDate;
      this.model.endDate = e.item.endDate;
      this.employeeDeductionMaintenanceComponentService.getlistByEmployeeID(this.model.employeeMaster["employeeIndexId"]).then(data => {
        console.log('DATA', data)
      })
      if (e.item.customDate) {
        this.dateSectionDateDisplay = true;
        this.dateSectionDayDisplay = false;
      } else {
        this.dateSectionDateDisplay = false;
        this.dateSectionDayDisplay = true;
      }
      this.model.deductionCode["id"] = e.item.id;
      if (this.model.employeeMaster["employeeIndexId"] && this.model.deductionCode["id"]) {
        let payload = {
          employeeId: this.model.employeeMaster["employeeIndexId"],
          deductionId: this.model.deductionCode["id"]
        }
        this.AllBasedOnPayCodeList = [];
        this.employeeDeductionMaintenanceComponentService.getAlldeductionCodefindCodeByEmployeeId(payload).then(data => {
          console.log('Data', data.result.records);
          if (data.result.records != null) {
            if (data.result.records.length) {
              for (let i = 0; i < data.result.records.length; i++) {
                let empdpt = {
                  id: data.result.records[i].payCode.id,
                  itemName: data.result.records[i].payCode.payCodeId,
                  payRate: data.result.records[i].payRate
                }
                this.AllBasedOnPayCodeList.push(empdpt);
              }
              this.showSelectedPaycode = this.AllBasedOnPayCodeList;
              setTimeout(() => {
                this.amountCal()
                setTimeout(() => {
                  this.totalAmount(e.item.method,e.item.amount);
                }, 500)
              }, 200)
            }
          } else {
            this.model.deductionAmount = e.item.amount;
          }

          // if(Number(this.model.deductionAmount) == 0 && this.model.benefitMethod == 1 && Number(this.model.deductionAmount) != null)
          // {
          //   this.isDeductionAmount = false;
          // }
        });
      }
      this.employeeDeductionMaintenanceComponentService.getlistByEmployeeID(this.model.employeeMaster["employeeIndexId"]).then(data => {
        this.selectedIdRows = data.result.records;
        if (this.selectedIdRows) {
          tmpArr = this.selectedIdRows.filter(item => item.employeeMaster.employeeIndexId == this.model.employeeMaster["employeeIndexId"] && item.deductionCode.deductionId == this.model.deductionCode["diductionId"]);
          if (tmpArr.length > 0)
            this.deductionCodeDuplicate = true;
          else
            this.deductionCodeDuplicate = false;
        }
      });
      this.deductionDesc = e.item.discription;
      this.deductionArabicDesc = e.item.arbicDiscription;

      this.startDateModel = this.formatDateFordatePicker(e.item.startDate);
      this.endDateModel = this.formatDateFordatePicker(e.item.endDate);
      this.model.frequency = e.item.frequency;
      this.model.benefitMethod = e.item.method;
      this.model.deductionPercent = e.item.percent;
      // this.model.deductionAmount = e.item.amount;
      this.model.payFactor = e.item.payFactor;
      this.model.transactionRequired = e.item.transction;
      this.model.inactive = e.item.inActive;
      this.model.perPeriord = e.item.perPeriod;
      this.model.perYear = e.item.perYear;
      this.model.lifeTime = e.item.lifeTime;
      this.checkPayCodeValidId = e.item.false;
      this.model.deductionTypeId = e.item.deductionTypeId;
      this.model.roundOf = e.item.roundOf;

      setTimeout(() => {
        if(Number(this.model.deductionAmount) == 0 && this.model.benefitMethod == 1 && Number(this.model.deductionAmount) != null)
        {
          this.isDeductionAmount = false;
        }
      },1000);
     

    }
  }

  // Set Page
  setPage(pageInfo) {
    this.selected = [];
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
    } else {
      this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
    } else {
      this.page.sortBy = pageInfo.sortBy;
    }

    // Get All
    this.page.searchKeyword = '';
    this.employeeDeductionMaintenanceComponentService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;

      console.log("akjdflkjff")
      console.log(this.rows)
    });
  }

  // Open form for create Deduction Code
  Create() {
    this.dateSectionDateDisplay = true;
    this.dateSectionDayDisplay = true;
    this.employeeJoiningDate = null;
    this.isPerPeriod = false;
    this.isperYear = false;
    this.isLifeTime = false;
    this.islifeTimeValid = true;
    this.islifeTimeValidperyear = true;
    this.islifeTimeValidperperiod = true;
    this.islifeTimeValidamount = true;
    this.islifeTimeValidamountPercent = true;
    this.startDateModel = null;
    this.endDateModel = null;
    this.showCreateForm = false;
    this.isUnderUpdate = false;
    this.isDeductionCodeFormdisabled = false;
    this.isCompMaxTrxn = false;
    this.isCompMaxTrxnLt = false;
    this.isCompMaxTrxnPp = false;
    this.isCompMaxTrxnPy = false;
    this.isCompAmount = false;
    this.errorGrterthn = '';
    this.deductionCodeDuplicate = false;
    setTimeout(() => {
      this.showCreateForm = true;
      this.employeeFirstName = null;
      this.deductionDesc = null;
      this.deductionArabicDesc = null;
      this.checkPayCodeValidId = false;
      this.checkEmpValidId = false;
      setTimeout(() => {
        window.scrollTo(0, 500);
      }, 10);
    }, 10);
    this.model = {
      id: 0,
      employeeMaster: { "employeeIndexId": null },
      deductionCode: { "id": null },
      startDate: this.startDateModel,
      endDate: this.endDateModel,
      frequency: 0,
      benefitMethod: 0,
      deductionAmount: null,
      deductionPercent: null,
      transactionRequired: false,
      inactive: false,
      perPeriord: null,
      perYear: null,
      lifeTime: null,
      dtoPayCode: [],
      payFactor: null,
      noOfDays: 0,
      endDateDays: 0,
      deductionTypeId:0,
      roundOf: 0
    };
  }

  // Cancel function
  Cancel() {
    this.islifeTimeValid = true;
    this.islifeTimeValidperyear = true;
    this.islifeTimeValidperperiod = true;
    this.islifeTimeValidamount = true;
    this.islifeTimeValidamountPercent = true;
    this.showCreateForm = false;
    this.isPerPeriod = false;
    this.isperYear = false;
    this.isLifeTime = false;
    this.isCompAmount = false;
    this.isCompMaxTrxn = false;
    this.isCompMaxTrxn = false;
    this.isCompMaxTrxnLt = false;
    this.isCompMaxTrxnPp = false;
    this.isCompMaxTrxnPy = false;
    this.errorGrterthn = '';
    this.checkPayCodeValidId = false;
    this.checkEmpValidId = false;
  }
  // Clear form to reset to default blank
  Clear(f: NgForm) {
    f.resetForm({ benefitMethod: '0', frequency: '0', deductionAmount: null, deductionPercent: null, perPeriord: null, perYear: null, lifeTime: null });
    this.isDeductionCodeFormdisabled = false;
    this.islifeTimeValid = true;
    this.islifeTimeValidperyear = true;
    this.islifeTimeValidperperiod = true;
    this.islifeTimeValidamount = true;
    this.islifeTimeValidamountPercent = true;
    this.isPerPeriod = false;
    this.isperYear = false;
    this.isLifeTime = false;
    this.isCompAmount = false;
    this.isCompMaxTrxn = false;
    this.isCompMaxTrxnLt = false;
    this.isCompMaxTrxnPp = false;
    this.isCompMaxTrxnPy = false;
    this.errorGrterthn = '';
    if (!this.isUnderUpdate) {
      this.employeeFirstName = '';
    }

  }

  // function call for creating new Emp Deduction Code
  CreateDeductionCode(f: NgForm, event: Event) {
    event.preventDefault();
    this.checkPayCodeValidId = false;
    this.checkEmpValidId = false;
    // if (this.deductionCodeDuplicate) {
    //   this.duplicateWarning = true;
    //   this.message.type = 'success';

    //   window.setTimeout(() => {
    //     this.isSuccessMsg = false;
    //     this.isfailureMsg = true;
    //     this.showMsg = true;
    //     window.setTimeout(() => {
    //       this.showMsg = false;
    //       this.duplicateWarning = false;
    //     }, 4000);
    //     this.message.text = "Employee Deduction for this employee already exists.";
    //   }, 100);
    // }
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
      this.isConfirmationModalOpen = true;
      this.isDeleteAction = false;
    } else {
      //Check for duplicate Benefit Code Id according to it create new Benefit Code
      this.employeeDeductionMaintenanceComponentService.checkDuplicateBenefitId(this.model).then(response => {
        console.log('response', response);
        if (response && response.code == 201 && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = 'success';
          window.scrollTo(0, 800);
          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;
          }, 100);
        } else {
          // Call service api for Creating new Deduction Code
          this.model.dtoPayCode = this.showSelectedPaycode
          this.employeeDeductionMaintenanceComponentService.createEmpDeductionCode(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
                }, 4000);
                this.showCreateForm = false;
                this.messageText = data.btiMessage.message;
                ;
              }, 100);

              this.hasMsg = true;
              f.resetForm();
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = 'Server error. Please contact admin.';
            }, 100)
          });
        }
      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = 'Server error. Please contact admin.';
        }, 100)
      });
    }
  }

  //edit department by row
  edit(row: EmployeeDeductionMaintnce) {
    debugger;
    this.isDeductionAmount = true;
    this.showSelectedPaycode = [];
    console.log('EDIT', row)
    this.model = Object.assign({}, row);
    this.model.deductionTypeId = this.model.deductionCode["deductionTypeId"];
    this.model.roundOf = this.model.deductionCode["roundOf"];
    if (row.deductionCode['customDate']) {
      this.dateSectionDateDisplay = true;
      this.dateSectionDayDisplay = false;
    } else {
      this.dateSectionDateDisplay = false;
      this.dateSectionDayDisplay = true;
    }
    this.employeeDeductionMaintenanceComponentService.getlistByEmployeeID(row.id).then(data => {
      console.log('DATA', data)
      console.log('(data.result.dtoPayCode', data.result.dtoPayCode)
      // this.selectedMethod(data.result.method);
      /* if (data.result.dtoPayCode != null) {
         if (data.result.dtoPayCode.length) {
           for (let i = 0; i < data.result.dtoPayCode.length; i++) {
             let empdpt = {
               id: data.result.dtoPayCode[i].id,
               itemName: data.result.dtoPayCode[i].payCodeId,
               payRate: data.result.dtoPayCode[i].payRate
             }
             this.showSelectedPaycode.push(empdpt);
           }
           setTimeout(() => {
             this.amountCal();
             setTimeout(() => {
               this.totalAmount();
             }, 1000);
           }, 500);
         }
       } else {
         this.model.deductionAmount = null;
       }*/


      this.model.payFactor = data.result.payFactor;
      this.model.noOfDays = data.result.noOfDays;
      this.model.endDateDays = data.result.endDateDays;

    })
    if (this.model.employeeMaster["employeeIndexId"] && this.model.deductionCode["id"]) {
      let payload = {
        employeeId: this.model.employeeMaster["employeeIndexId"],
        deductionId: this.model.deductionCode["id"]
      }
      this.AllBasedOnPayCodeList = [];
      this.employeeDeductionMaintenanceComponentService.getAlldeductionCodefindCodeByEmployeeId(payload).then(data => {
        console.log('Data', data.result.records);
        if (data.result.records != null) {
          if (data.result.records.length) {
            for (let i = 0; i < data.result.records.length; i++) {
              let empdpt = {
                id: data.result.records[i].payCode.id,
                itemName: data.result.records[i].payCode.payCodeId,
                payRate: data.result.records[i].payRate
              }
              this.AllBasedOnPayCodeList.push(empdpt);
            }
            this.showSelectedPaycode = this.AllBasedOnPayCodeList;
            setTimeout(() => {
              this.amountCal();
              setTimeout(() => {
                this.totalAmount(this.model.benefitMethod,this.model.deductionAmount);
              }, 500)
            }, 200)
          }
        }
      });
    }
    this.error = { isError: false, errorMessage: '' };
    this.isPerPeriod = false;
    this.isperYear = false;
    this.isLifeTime = false;
    this.islifeTimeValid = true;
    this.islifeTimeValidperyear = true;
    this.islifeTimeValidperperiod = true;
    this.islifeTimeValidamount = true;
    this.islifeTimeValidamountPercent = true;
    this.showCreateForm = true;
    this.isCompMaxTrxn = false;
    this.isCompAmount = false;
    this.errorGrterthn = '';
    this.startDateModel = this.formatDateFordatePicker(this.model.startDate);
    this.endDateModel = this.formatDateFordatePicker(this.model.endDate);
    let strDate = new Date(this.model.startDate)
    this.frmstartDate = (strDate.getTime()) / 1000;
    let endDate = new Date(this.model.endDate);
    this.frmendDate = (endDate.getTime()) / 1000;
    this.startDate = this.model.startDate;
    this.endDate = this.model.endDate;
    this.isUnderUpdate = true;
    this.deductionCodeDuplicate = false;
    this.checkPayCodeValidId = false;
    this.checkEmpValidId = false;
    this.isCompMaxTrxnPp = false;
    this.isCompMaxTrxnLt = false;
    this.isCompMaxTrxnPy = false;
    this.empPrimaryIdValue = this.model.employeeMaster['employeeIndexId'];
    this.employeeIdValue = this.model.employeeMaster['employeeId'];
    this.deductionCodeValue = this.model.deductionCode['diductionId'];
    this.employeeFirstName = this.model.employeeMaster['employeeFirstName'];
    this.employeeJoiningDate = this.model.employeeMaster['employeeHireDate'];
    this.deductionPrimaryValue = this.model.deductionCode['id'];
    this.isCustom = this.model.deductionCode['customDate'];
    this.deductionDesc = this.model.deductionCode['discription'];
    this.deductionArabicDesc = this.model.deductionCode['arbicDiscription'];

    if (this.model.inactive == true) {
      this.isDeductionCodeFormdisabled = true;
    } else {
      this.isDeductionCodeFormdisabled = false;
    }


    setTimeout(() => {
      window.scrollTo(0, 2000);
      if(Number(this.model.deductionAmount) == 0 && this.model.benefitMethod == 1 && Number(this.model.deductionAmount) != null)
      {
        this.isDeductionAmount = false;
      }
    }, 10);
  }

  // Update Case
  updateStatus() {
    this.checkPayCodeValidId = false;
    this.checkEmpValidId = false;
    this.model.employeeMaster["employeeIndexId"] = this.empPrimaryIdValue;
    this.model.deductionCode["id"] = this.deductionPrimaryValue;
    //Call service api for Update Emp Deduction Code
    this.model.dtoPayCode = this.showSelectedPaycode;
    this.employeeDeductionMaintenanceComponentService.updateEmpDeductionCode(this.model).then(data => {
      var datacode = data.code;
      this.closeModal();
      if (datacode == 201) {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        window.scrollTo(0, 0);
        window.setTimeout(() => {
          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          this.messageText = data.btiMessage.message;
          ;
          this.showCreateForm = false;
        }, 100);
        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = 'Server error. Please contact admin.';
      }, 100)
    });
  }

  // verify Delete
  varifyDelete() {
    if (this.selected.length > 0) {
      this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
    }
    else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;
      this.message.text = 'Please select at least one record to delete.';
      window.scrollTo(0, 0);
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.hasMessage = false;

      }, 4000);
    }
  }

  // Delete Case
  delete() {
    debugger;
    var selectedDeductionCodes = [];
    for (var i = 0; i < this.selected.length; i++) {
      selectedDeductionCodes.push(this.selected[i].id);
    }
    this.employeeDeductionMaintenanceComponentService.deleteEmpDeductionCode(selectedDeductionCodes).then(data => {
      var datacode = data.code;
      console.log("data code")
      console.log(datacode)
      if (datacode == 200) {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if(datacode == "302"){
          this.message.type = "error";
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
      } else {
          this.isSuccessMsg = true;
          this.message.type = "success";
          this.isfailureMsg = false;
      }
      window.scrollTo(0, 0);
      window.setTimeout(() => {
        this.showMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMessage = false;
        }, 4000);
        this.message.text = data.btiMessage.message;
      }, 100);
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }).catch(error => {
      this.hasMessage = true;
      this.message.type = 'error';
      var errorCode = error.status;
      this.message.text = 'Server issue. Please contact admin.';
    });
    this.closeModal();
  }

  // default list on page
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  // search department by keyword
  searchFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
  }

  // Set default page size
  changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  // Delete Conformation
  confirm(): void {
    this.messageText = 'Confirmed.';
    this.delete();
  }

  formatDateFordatePicker(strDate: Date): any {
    if (strDate != null) {
      var setDate = new Date(strDate);
      return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
    } else {
      return null;
    }
  }

  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }

  onStartDateChanged(event: IMyDateModel): void {
    this.startDate = event.jsdate;
    this.frmstartDate = event.epoc;
    if ((this.frmstartDate > this.frmendDate) && this.frmendDate != undefined) {
      this.error = { isError: true, errorMessage: 'Invalid End Date.' };
    }
    if (this.frmstartDate <= this.frmendDate) {
      this.error = { isError: false, errorMessage: '' };
    }
    if (this.frmendDate == undefined && this.frmstartDate == undefined) {
      this.error = { isError: false, errorMessage: '' };
    }
  }


  onEndDateChanged(event: IMyDateModel): void {
    this.endDate = event.jsdate;
    this.frmendDate = event.epoc;

    if ((this.frmstartDate > this.frmendDate) && this.frmendDate != undefined) {
      this.error = { isError: true, errorMessage: 'Invalid End Date.' };
    }
    if (this.frmstartDate <= this.frmendDate) {
      this.error = { isError: false, errorMessage: '' };
    }
    if (this.frmendDate == undefined && this.frmstartDate == undefined) {
      this.error = { isError: false, errorMessage: '' };
    }

  }

  CheckNumber(event) {
    if (isNaN(event.target.value) == true) {
      this.model.employeeMaster = {};
      return false;
    }
  }

  keyPress(event: any) {
    const pattern = /^[0-9\-.()]+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  sortColumn(val) {
    if (this.page.sortOn == val) {
      if (this.page.sortBy == 'DESC') {
        this.page.sortBy = 'ASC';
      } else {
        this.page.sortBy = 'DESC';
      }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  validateDecimal(event) {
    console.log(event);
  }

  checkTranRequed(event) {
    if (event.target.checked == true) {
      this.isPerPeriod = true;
      this.isperYear = true;
      this.isLifeTime = true;
      if (event.target.checked && (this.model.perPeriord != null && this.model.perYear != null && this.model.lifeTime != null)) {
        this.isPerPeriod = false;
        this.isperYear = false;
        this.isLifeTime = false;
      }
    }
    else {
      this.isPerPeriod = false;
      this.isperYear = false;
      this.isLifeTime = false;
    }
  }

  checkdecimal(digit) {
    if (digit == '' || digit == null) {
      this.islifeTimeValid = true;
      return;
    }
    this.tempp = digit.toString().split(".");
    if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValid = false;
    }
    else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValid = false;
    }
    else {
      this.islifeTimeValid = true;
    }
  }

  checkdecimalperyear(digit) {
    if (digit == '' || digit == null) {
      this.islifeTimeValidperyear = true;
      return;
    }
    this.tempp = digit.toString().split(".");
    if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValidperyear = false;
    }
    else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValidperyear = false;
    }
    else {
      this.islifeTimeValidperyear = true;
    }
  }

  checkdecimalperperiod(digit) {
    if (digit == '' || digit == null) {
      this.islifeTimeValidperperiod = true;
      return;
    }
    this.tempp = digit.toString().split(".");
    if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValidperperiod = false;
    }
    else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValidperperiod = false;
    }
    else {
      this.islifeTimeValidperperiod = true;
    }
  }

  checkdecimalamount(digit) {
    if (digit == '' || digit == null) {
      this.islifeTimeValidamount = true;
      return;
    }
    this.tempp = digit.toString().split(".");
    if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValidamount = false;
    }
    else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
      this.islifeTimeValidamount = false;
    }
    else {
      this.islifeTimeValidamount = true;
    }
  }
  checkdecimalamountPercent(digit) {
    if (digit == '' || digit == null) {
      this.islifeTimeValidamountPercent = true;
      return;
    }
    this.tempp = digit.toString().split(".");
    if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 5) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
      this.islifeTimeValidamountPercent = false;
    }
    else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
      this.islifeTimeValidamountPercent = false;
    }
    else {
      this.islifeTimeValidamountPercent = true;
    }
  }

  checkDeductionCodeForm(event) {
    if (event.target.checked == true) {
      this.isDeductionCodeFormdisabled = true;
    } else {
      this.isDeductionCodeFormdisabled = false;
    }
  }

  // Amount Compair with deduction amount
  amountComp(e) {
    let deduAmount = parseFloat(e);
    let pPAmount = this.model.perPeriord;
    let pYAmoount = this.model.perYear;
    let lAmount = this.model.lifeTime;
    if (deduAmount != 0 && ((pPAmount != null || pYAmoount != null || lAmount != null))) {
      if (deduAmount < pPAmount) {
        this.isCompAmount = true;
        this.errorDeductn = 'This can not be less than Per Period';
      }
      else if (deduAmount < pYAmoount) {
        this.isCompAmount = true;
        this.errorDeductn = 'This can not be less than Per Year';
      }
      else if (deduAmount < lAmount) {
        this.isCompAmount = true;
        this.errorDeductn = 'This can not be less than Life Time';
      }
      else {
        this.isCompAmount = false;
      }
    }
    else {
      this.isCompAmount = false;
    }
  }

  // compair amount with trans Deduction

  amountTrnsDeductnPp(e) {
    let deduAmount = this.model.deductionAmount;
    let pPAmount = parseFloat(e);
    if (deduAmount != 0 && pPAmount != 0) {
      if (pPAmount > deduAmount) {
        this.isCompAmount = true;
        this.errorDeductn = 'This can not be less than Per Period';
      }
      else {
        this.isCompAmount = false;
      }
    }
    else {
      this.isCompAmount = false;
    }

  }

  // compair amount with trans Deduction
  amountTrnsDeductnPy(e) {
    let deduAmount = this.model.deductionAmount;
    let pYAmount = parseFloat(e);
    if (deduAmount != null && pYAmount != null) {
      if (pYAmount > deduAmount) {
        this.isCompAmount = true;
        this.errorDeductn = 'This can not be less than Per Year';
      }
      else {
        this.isCompAmount = false;
      }
    }
    else {
      this.isCompAmount = false;
    }

  }

  // compair amount with trans Deduction
  amountTrnsDeductnLt(e) {
    let deduAmount = this.model.deductionAmount;
    if (e == "") {
      this.isCompAmount = true;
      this.errorDeductn = 'This must be greater than Maximum Deduction';
      return;
    }
    let lTAmount = parseFloat(e);
    // if (deduAmount != 0 && lTAmount != 0) {
    if (lTAmount > deduAmount) {
      this.isCompAmount = true;
      this.errorDeductn = 'This can not be less than Life Time';
    }
    else {
      this.isCompAmount = false;
    }
    // }
    // else {
    //   this.isCompAmount = false;
    // }

  }

  // Maximun Amount Compaire with Per Period
  maxAmountCompPp(e) {
    let pYAmoount = this.model.perYear;
    let deduAmount = this.model.deductionAmount;
    if (e == "") {

      this.isCompMaxTrxnLt = false;
      this.isCompMaxTrxnPp = true;
      this.isCompMaxTrxnPy = false;
      this.errorMaxPp = 'This can not be null';
    }
    let pPAmount = parseFloat(e);

    if (pPAmount > deduAmount) {
      this.errorMaxPp = 'This can not be Per Period';
    }
    if (pYAmoount != null && pPAmount > pYAmoount) {
      this.isCompMaxTrxnLt = false;
      this.isCompMaxTrxnPp = true;
      this.isCompMaxTrxnPy = false;
      this.errorMaxPp = 'This must be less than Per Year Amount';
    } else {
      this.isCompMaxTrxnPp = false;
    }
    // else {
    //   this.isCompMaxTrxnPp = false;
    // }
  }

  // // Maximun Amount Compaire with Per year
  maxAmountCompPy(e) {
    let pPAmount = this.model.perPeriord;
    let lTAmount = this.model.lifeTime;
    if (e == "") {
      this.isCompMaxTrxnLt = false;
      this.isCompMaxTrxnPy = true;
      this.errorMaxPy = 'This must be greater than Per Period';
      return;
    }
    let pYAmoount = parseFloat(e);
    // if (pPAmount != 0 && pYAmoount != 0) {
    if (pYAmoount < pPAmount) {
      this.isCompMaxTrxnLt = false;
      this.isCompMaxTrxnPy = true;
      this.errorMaxPy = 'This must be greater than Per Period';
    }
    else {
      this.isCompMaxTrxnPy = false;
    }
    // }
    // else {
    //   this.isCompMaxTrxnPp = false;
    // }
    // if (pYAmoount != 0 && lTAmount != 0) {
    if (lTAmount != null) {
      if (pYAmoount > lTAmount) {
        this.isCompMaxTrxnLt = true;
        this.errorMaxLt = 'This must be less than LifeTime';
      } else {
        this.isCompMaxTrxnLt = false;
      }
    }
    // }
  }

  // Maximun Amount Compaire with Life Time
  maxAmountCompLt(e) {
    if (e == "") {
      this.isCompMaxTrxnLt = true;
      this.errorMaxLt = 'This must be greater than Per Year';
      return;
    }
    let lAmount = parseFloat(e);

    let pYAmoount = this.model.perYear;
    // if (lAmount != 0 && pYAmoount != null) {
    if (lAmount < pYAmoount) {
      this.isCompMaxTrxnLt = true;
      this.errorMaxLt = 'This must be greater than Per Year';
    }
    else {
      this.isCompMaxTrxnLt = false;
    }
    // }
    // else {
    //   this.isCompMaxTrxnLt = false;
    // }
  }

  RequiredValidation(eve: any) {
    if (this.model.perPeriord) {
      this.isPerPeriod = true;
    } else if (this.model.perYear) {
      this.isperYear = true;
    } else if (this.model.lifeTime) {
      this.isLifeTime = true;
    } else {
      this.isPerPeriod = false;
      this.isperYear = false;
      this.isLifeTime = false;
    }
  }

  CheckValidID(eve: any, empId: any) {
    // console.log("Event", eve);
    // switch(empId){
    // case 'employeeId':
    if (eve.target.value == "") {
      this.checkEmpValidId = false;
      this.startDateModel = null;
      this.endDateModel = null;
    } else {
      for (let i = 0; i < this.getEmployeeCodeID.length; i++) {
        if (this.getEmployeeCodeID[i].employeeId.includes(eve.target.value)) {
          this.checkEmpValidId = false;
          break;
        } else {
          this.checkEmpValidId = true;
        }
      }
    }
    // break;
    // case 'deductionId':
    // if(eve.target.value == ""){
    //   this.checkPayCodeValidId = false;
    // }else{
    //   for(let i = 0; i < this.getDeductionCode.length; i++){
    //     if(this.getDeductionCode[i].diductionId.includes(eve.target.value)){
    //       this.checkPayCodeValidId = false;
    //       break;
    //     }else{
    //       this.checkPayCodeValidId = true;
    //     }
    //   }
    // }
    // break;
    // }
  }

  CheckValidID2(eve: any, empId: any, deductionCode: any) {
    // console.log("deductionCode", deductionCode)
    if (eve.target.value == "") {
      this.checkPayCodeValidId = false;
    } else {
      for (let i = 0; i < this.getDeductionCode.length; i++) {
        if (this.getDeductionCode[i].diductionId.includes(eve.target.value)) {
          this.checkPayCodeValidId = false;
          break;
        } else {
          this.checkPayCodeValidId = true;
        }
      }
    }
  }

  /** Method Change Function */
  selectedMethod(indx: any) {
    if (indx == 5) {
      let payload = {
        "id": indx,
        "sortOn": "id",
        "sortBy": "DESC",
        "searchKeyword": "",
        "pageNumber": 0,
        "pageSize": 5
      }
      this.AllBasedOnPayCodeList = []
      this.employeeDeductionMaintenanceComponentService.getAllBasedOnPayCodeDropDown(payload).then(data => {
        if (data.result.records.length) {
          for (let i = 0; i < data.result.records.length; i++) {
            let empdpt = {
              id: data.result.records[i].id,
              itemName: data.result.records[i].payCodeId,
              payRate: data.result.records[i].payRate
            }
            this.AllBasedOnPayCodeList.push(empdpt);
          }
        }

      });
    }

  }

  /** Function for Multipal Paycode Select */


  onItemSelect(item: any) {
    console.log('item', item);

  }
  OnItemDeSelect(item: any) {
    console.log('item', item);

  }
  onSelectAll(items: any) {
    console.log('items', items);

  }
  onDeSelectAll(items: any) {
    console.log('items', items);

  }

  /* Total amount Calculation */
  amountCal() {
    this.tempTotal = 0
    this.showSelectedPaycode.forEach(ele => {
      this.tempTotal = Number(this.tempTotal) + Number(ele.payRate);
    })
  }

  /* Total amount Bind to Model */
  totalAmount(method:any,amount:number) {
    console.log("TOTAL amount",amount);
    console.log("SELECTED method",method);
    if (this.tempTotal && method == 5) {
      this.model.deductionAmount = Number(this.tempTotal) * Number(this.model.payFactor)
    } else {
      this.model.deductionAmount = amount;
    }
  }
}
