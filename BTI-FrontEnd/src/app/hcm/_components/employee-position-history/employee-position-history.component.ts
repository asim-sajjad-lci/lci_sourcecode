import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
//import { employeePositionHistory } from '../../_models/employee-position-history/employeePositionHistory';
import { employeePosition } from '../../_models/employee-position-history/employeePosition';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { EmployeePositionHistoryService } from '../../_services/employee-position-history/employeepositionhistory.service';
import { Constants } from '../../../_sharedresource/Constants';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-employee-position-history',
  templateUrl: './employee-position-history.component.html',
  providers: [EmployeePositionHistoryService, CommonService, DatePipe]
})
export class EmployeePositionHistoryComponent implements OnInit {
    idvalue: any;
    divisionDescription: any;
    employeeFirstName: any;
    departmentDescription:any;
    positionDescription1:any;
    supervisorDescription:any;
    locationDescription:any;
    positionReason:any;

  selectedChildDelete: any;
  isDeleteAction1: boolean;
  isConfirmationModalOpen1: boolean;
  selectedInterviewTypeSetups1 = [];
  duplicateWarning: boolean;
  hasMsg: boolean;
  showMsg: boolean;
  ddPageSize: number = 5;
  hasMessage: boolean;
  isConfirmationModalOpen: boolean;
  isDeleteAction: boolean;
  page = new Page();
  rows = new Array<employeePosition>();
  temp = new Array<employeePosition>();
  //model: employeePositionHistory;
  model1:employeePosition;
  moduleCode = "M-1011";
  screenCode = "S-1456";
  defaultFormValues = [];
  availableFormValues = [];
  searchKeyword = '';
  message = { 'type': '', 'text': '' };
  selected = [];
  currentLanguage: any;
  moduleName:any;
  screenName:any;
  interviewTypeId;
  EmployeePositionid : number;
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  messageText: string;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  rowsToDelete=[];
  employeeMasterIdOption=[];
  divisionIdOption=[];
  departmentIdoption=[];
  positionIdOption=[];
  superVisorIdOption=[];
  locationIdOption=[];
  employeePositionReasonIdOption=[];
  positionEffectiveDate;
  typeaheadLoading: boolean;
  employeeId;
  divisionId;
  typeaheadNoResults = [];
  typeaheadNoResults1 = [];
  typeaheadNoResults2 = [];
  typeaheadNoResults3 = [];
  typeaheadNoResults4 = [];
  typeaheadNoResults5 = [];
  clearvalue : number;
  DepartmentId;
  PositionId;
  supervisiorId;
  employeeIdList: Observable<any>;
  divisionIdList: Observable<any>;
  departmentIdList: Observable<any>;
  positionIdList: Observable<any>;
  superviserIdList: Observable<any>;
  locationIdList: Observable<any>;
  getEmployee: any[] = [];
  getDivision:any[]=[];
  getDepartment:any[]=[];
  getPosition=[];
  getLocation=[];
  LocationId;
  getSuperviser=[];
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;

  EMPLOYEE_POSITION_HISTORY_SEARCH: any;
  EMPLOYEE_POSITION_HISTORY_ACTION: any;
  EMPLOYEE_POSITION_HISTORY_CREATE_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_SAVE_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_CLEAR_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_CANCEL_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_UPDATE_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_DELETE_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_CREATE_FORM_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_UPDATE_FORM_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_POSITION_ID_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_POSITION_DESCRIPTION_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_DATE_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_DIVISION_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_DEPARTMENT_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_LOCATION_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_SUPERVISOR_LABEL: any;
  EMPLOYEE_POSITION_HISTORY_EMPLOYEE_ID: any;
  EMPLOYEE_POSITION_HISTORY_EFFECTIVE_DATE: any;
  EMPLOYEE_POSITION_HISTORY_DIVISION_ID: any;
  EMPLOYEE_POSITION_HISTORY_DEPARTMENT_ID: any;
  EMPLOYEE_POSITION_HISTORY_POSITION_ID: any;
  EMPLOYEE_POSITION_HISTORY_SUPERVISIOR_ID: any;
  EMPLOYEE_POSITION_HISTORY_LOCATION_ID: any;
  EMPLOYEE_POSITION_HISTORY_POSITION_CHANGE_REASON: any;
  EMPLOYEE_POSITION_HISTORY_EMPLOYEEMENT_TYPE: any;

  constructor( 
    private employeePositionHistoryService: EmployeePositionHistoryService,
    private getScreenDetailService: GetScreenDetailService,
    private commonService: CommonService,
    private datePipe: DatePipe) 
    {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_POSITION_ID_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_POSITION_DESCRIPTION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_DATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_DIVISION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_DEPARTMENT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_LOCATION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_SUPERVISOR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_EFFECTIVE_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_DIVISION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_SUPERVISIOR_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_POSITION_CHANGE_REASON', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_POSITION_HISTORY_EMPLOYEEMENT_TYPE', 'fieldValue': '', 'helpMessage': '' }
        ];
        this.EMPLOYEE_POSITION_HISTORY_SEARCH = this.defaultFormValues[0];
        this.EMPLOYEE_POSITION_HISTORY_ACTION = this.defaultFormValues[1];
        this.EMPLOYEE_POSITION_HISTORY_CREATE_LABEL = this.defaultFormValues[2];
        this.EMPLOYEE_POSITION_HISTORY_SAVE_LABEL = this.defaultFormValues[3];
        this.EMPLOYEE_POSITION_HISTORY_CLEAR_LABEL = this.defaultFormValues[4];
        this.EMPLOYEE_POSITION_HISTORY_CANCEL_LABEL = this.defaultFormValues[5];
        this.EMPLOYEE_POSITION_HISTORY_UPDATE_LABEL = this.defaultFormValues[6];
        this.EMPLOYEE_POSITION_HISTORY_DELETE_LABEL = this.defaultFormValues[7];
        this.EMPLOYEE_POSITION_HISTORY_CREATE_FORM_LABEL = this.defaultFormValues[8];
        this.EMPLOYEE_POSITION_HISTORY_UPDATE_FORM_LABEL = this.defaultFormValues[9];
        this.EMPLOYEE_POSITION_HISTORY_POSITION_ID_LABEL = this.defaultFormValues[10];
        this.EMPLOYEE_POSITION_HISTORY_POSITION_DESCRIPTION_LABEL = this.defaultFormValues[11];
        this.EMPLOYEE_POSITION_HISTORY_DATE_LABEL = this.defaultFormValues[12];
        this.EMPLOYEE_POSITION_HISTORY_DIVISION_LABEL = this.defaultFormValues[13];
        this.EMPLOYEE_POSITION_HISTORY_DEPARTMENT_LABEL = this.defaultFormValues[14];
        this.EMPLOYEE_POSITION_HISTORY_LOCATION_LABEL = this.defaultFormValues[15];
        this.EMPLOYEE_POSITION_HISTORY_SUPERVISOR_LABEL = this.defaultFormValues[16];
        this.EMPLOYEE_POSITION_HISTORY_EMPLOYEE_ID = this.defaultFormValues[17];
        this.EMPLOYEE_POSITION_HISTORY_EFFECTIVE_DATE = this.defaultFormValues[18];
        this.EMPLOYEE_POSITION_HISTORY_DIVISION_ID = this.defaultFormValues[19];
        this.EMPLOYEE_POSITION_HISTORY_DEPARTMENT_ID = this.defaultFormValues[20];
        this.EMPLOYEE_POSITION_HISTORY_POSITION_ID = this.defaultFormValues[21];
        this.EMPLOYEE_POSITION_HISTORY_SUPERVISIOR_ID = this.defaultFormValues[22];
        this.EMPLOYEE_POSITION_HISTORY_LOCATION_ID = this.defaultFormValues[23];
        this.EMPLOYEE_POSITION_HISTORY_POSITION_CHANGE_REASON = this.defaultFormValues[24];
        this.EMPLOYEE_POSITION_HISTORY_EMPLOYEEMENT_TYPE = this.defaultFormValues[25];

        this.employeeIdList = Observable.create((observer: any) => {
            observer.next(this.employeeId);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));
        this.divisionIdList = Observable.create((observer: any) => {
            observer.next(this.divisionId);
        }).mergeMap((token: string) => this.getDivisionAsObservable(token));

        this.departmentIdList = Observable.create((observer: any) => {
            observer.next(this.DepartmentId);
        }).mergeMap((token: string) => this.getDepartmentIdAsObservable(token));

        this.positionIdList = Observable.create((observer: any) => {
            observer.next(this.PositionId);
        }).mergeMap((token: string) => this.getPositionAsObservable(token));

        this.superviserIdList = Observable.create((observer: any) => {
            observer.next(this.supervisiorId);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

        this.locationIdList = Observable.create((observer: any) => {
            observer.next(this.LocationId);
        }).mergeMap((token: string) => this.getLocationAsObservable(token));

      }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

    this.employeePositionHistoryService.getEmployee().then(data => {
        this.getEmployee = data.result;

    });

    this.employeePositionHistoryService.getEmployeeList().then(data => {
        this.employeeMasterIdOption = data.result;
      console.log('test',this.employeeMasterIdOption );
  });
  this.employeePositionHistoryService.getDivisionList().then(data => {
      this.getDivision = data.result;
      console.log('test',this.divisionIdOption );
  });
  this.employeePositionHistoryService.getDepartmentList().then(data => {
      this.getDepartment = data.result;
      console.log('test',this.departmentIdoption );
  });
  this.employeePositionHistoryService.getPositionList().then(data => {
      this.getPosition = data.result;
      console.log('test',this.positionIdOption );
  });
  this.employeePositionHistoryService.getSupervisorList().then(data => {
      this.getSuperviser = data.result;
      console.log('test',this.superVisorIdOption );
  });
  this.employeePositionHistoryService.getLocationList().then(data => {
      this.getLocation = data.result;
      console.log('test',this.locationIdOption );
  });
  this.employeePositionHistoryService.getEmployeePositionReasonList().then(data => {
      this.employeePositionReasonIdOption = data.result.records;
      console.log('test',this.employeePositionReasonIdOption );
  });
  }

  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
  }

  changePageSize(event) {
      this.page.size = event.target.value;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  delete() {
    var selectedEmployeePositionHistory = [];
    for (var i = 0; i < this.selected.length; i++) {
      selectedEmployeePositionHistory.push(this.selected[i].id);
    }
    this.employeePositionHistoryService.deleteEmployeePositionHistory(selectedEmployeePositionHistory).then(data => {
        var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
       
    }).catch(error => {
        this.hasMessage = true;
        this.message.type = "error";
        var errorCode = error.status;
        this.message.text = "Server issue. Please contact admin.";
    });
    this.closeModal();
}


getRowValue(row, gridFieldName) {
    if (gridFieldName === 'employeeId') {
        return row['employeeMaster'] && row['employeeMaster']['employeeId'] ? row['employeeMaster']['employeeId'] : '';
    } else if (gridFieldName == 'positionId') {
        return row['position'] && row['position']['positionId'] ? row['position']['positionId'] : '';
    } else if (gridFieldName == 'description') {
        return row['position'] && row['position']['description'] ? row['position']['description'] : '';
    } else if (gridFieldName === 'divisionId') {
        return row['division'] && row['division']['divisionId'] ? row['division']['divisionId'] : '';
    } else if (gridFieldName === 'departmentId') {
        return row['department'] && row['department']['departmentId'] ? row['department']['departmentId'] : '';
    } else if (gridFieldName === 'locationId') {
        return row['location'] && row['location']['locationId'] ? row['location']['locationId'] : '';
    } else if (gridFieldName === 'superVisionCode') {
        return row['supervisor'] && row['supervisor']['superVisionCode'] ? row['supervisor']['superVisionCode'] : '';
    } else if (gridFieldName === 'positionEffectiveDate') {
        return row[gridFieldName]?this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy'):'';
    } else {
        return row[gridFieldName];
    }
}

closeModal() {
  this.isDeleteAction1 = false;
  this.isDeleteAction = false;
  this.isConfirmationModalOpen = false;
  this.isConfirmationModalOpen1 = false;
}

getEmployeeIdAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
    return Observable.of(
        this.getEmployee.filter((id: any) => {
            return query.test(id.employeeId);
        })
    );
}
getDivisionAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'ig');
    console.log(query);
    return Observable.of(
        this.getDivision.filter((id: any) => {
            return query.test(id.divisionId);
        })
    );
}
getDepartmentIdAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'ig');
    console.log(query);
    return Observable.of(
        this.getDepartment.filter((id: any) => {
            return query.test(id.departmentId);
        })
    );
}

getPositionAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'ig');
    console.log(query);
    return Observable.of(
        this.getPosition.filter((id: any) => {
            return query.test(id.positionId);
        })
    );
}

getSuperviserIdAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'ig');
    
    return Observable.of(
        this.getSuperviser.filter((id: any) => {
            return query.test(id.superVisionCode);
        })
    );
}
getLocationAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'ig');
    console.log(query);
    return Observable.of(
        this.getLocation.filter((id: any) => {
            return query.test(id.locationId);
        })
    );
}
changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
    //this.model.employeeFirstName = '';
    console.log(e);
}

typeaheadOnSelect(e: TypeaheadMatch): void {
    console.log('Selected value: ', e.item);
    if (typeof (e.item.employeeFirstName) != 'undefined')
        this.employeeId = e.item.employeeId;
        this.model1.employeeMaster.employeeIndexId=e.item.employeeIndexId;
        this.employeeFirstName=e.item.employeeFirstName+' '+e.item.employeeMiddleName+' '+e.item.employeeLastName;
}
typeaheadOnSelectDivisionId(e: TypeaheadMatch): void {
    console.log('typeaheadOnSelectDivisionId: ', e.item);
    if (typeof (e.item.id) != 'undefined')
        this.divisionId = e.item.divisionId;
        this.model1.division.id=e.item.id;
        this.divisionDescription=e.item.divisionDescription;
}
typeaheadOnSelectDepartmentId(e: TypeaheadMatch): void {
    console.log('typeaheadOnSelectDepartmentId: ', e.item);
    if (typeof (e.item.id) != 'undefined')
        this.DepartmentId = e.item.departmentId;
        this.model1.department.id=e.item.id;
        this.departmentDescription=e.item.departmentDescription;
}

typeaheadOnSelectPositionId(e: TypeaheadMatch): void {
    console.log('typeaheadOnSelectPositionId: ', e.item);
    if (typeof (e.item.id) != 'undefined')
        this.PositionId = e.item.positionId;
        this.model1.position.id=e.item.id;
        this.positionDescription1=e.item.description;
}
typeaheadOnSelectSuperVisionCode(e: TypeaheadMatch): void {
    console.log('typeaheadOnSelectSuperVisionCode: ', e.item);
    if (typeof (e.item.id) != 'undefined')
//this.supervisiorId = e.item.SuperVisionCode;
        this.model1.supervisor.id=e.item.id;
        this.supervisorDescription=e.item.description;
}
typeaheadOnSelectLocationId(e: TypeaheadMatch): void {
    console.log('typeaheadOnSelectLocationId: ', e.item);
    if (typeof (e.item.id) != 'undefined')
       // this.LocationId = e.item.locationId;
        this.model1.location.id = e.item.id;
        this.locationDescription = e.item.description;
}

myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
};

updateStatus() {
  this.closeModal();
  //Call service api for updating selected department
  this.model1.id = this.EmployeePositionid;
  
  //var date = new Date(this.positionEffectiveDate.date.year+"-"+this.positionEffectiveDate.date.month+"-"+this.positionEffectiveDate.date.day);
  //this.model1.positionEffectiveDate=date.getTime();

  this.employeePositionHistoryService.updateEmployeePositionHistory(this.model1).then(data => {
      var datacode = data.code;
      if (datacode == 201) {
          //Refresh the Grid data after editing department
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

          //Scroll to top after editing department
          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.isSuccessMsg = true;
              this.isfailureMsg = false;
              this.showMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
              this.messageText = data.btiMessage.message;
              ;
              this.showCreateForm = false;
          }, 100);

          this.hasMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
          }, 4000);
      }
  }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = 'Server error. Please contact admin !';
      }, 100)
  });
}
Create() {
  this.showCreateForm = false;
  this.isUnderUpdate = false;

  this.divisionDescription= '';
  this.employeeFirstName= '';
  this.departmentDescription='';
  this.positionDescription1='';
  this.supervisorDescription='';
  this.locationDescription='';
  this.positionReason='';
  this.divisionId='';
  this.DepartmentId='';
  this.PositionId='';
  this.supervisiorId='';
  this.LocationId='';
  this.employeeId='';

  setTimeout(() => {
      this.showCreateForm = true;
      setTimeout(() => {
          window.scrollTo(0, 2000);
      }, 10);
  }, 10);
  this.model1 = {
    id:0,
    positionEffectiveDate: null,
    employeeType: 0,
    employeeMaster:{
        employeeIndexId:"0"
    },
    division:{
        id:"0"
    },
    department:{ id:"0"},
    position:{ id:"0"},
    location:{ id:"0"},
    supervisor:{ id:"0"},
    dtoEmployeePositionReason:{id:"0"}
  };
}

varifyDelete() {
  if (this.selected.length > 0) {
      this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
  } 
  else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;

      window.setTimeout(() => {
        this.showMsg = false;
        this.hasMessage = false;
    }, 4000);
      this.message.text = 'Please select at least one record to delete.';
      window.scrollTo(0, 0);
  }
}
  setPage(pageInfo) {
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if( pageInfo.sortOn == undefined ) {
        this.page.sortOn = this.page.sortOn;
    } else {
        this.page.sortOn = pageInfo.sortOn;
    }
    if( pageInfo.sortBy == undefined ) {
        this.page.sortBy = this.page.sortBy;
    } else {
        this.page.sortBy = pageInfo.sortBy;   
    }
    this.page.searchKeyword = '';
    this.employeePositionHistoryService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    // this.employeePositionHistoryService.getEmployeeList().then(data => {
    //       this.employeeMasterIdOption = data.result;
    //     console.log('test',this.employeeMasterIdOption );
    // });
    // this.employeePositionHistoryService.getDivisionList().then(data => {
    //     this.divisionIdOption = data.result;
    //     console.log('test',this.divisionIdOption );
    // });
    // this.employeePositionHistoryService.getDepartmentList().then(data => {
    //     this.departmentIdoption = data.result;
    //     console.log('test',this.departmentIdoption );
    // });
    // this.employeePositionHistoryService.getPositionList().then(data => {
    //     this.positionIdOption = data.result;
    //     console.log('test',this.positionIdOption );
    // });
    // this.employeePositionHistoryService.getSupervisorList().then(data => {
    //     this.superVisorIdOption = data.result;
    //     console.log('test',this.superVisorIdOption );
    // });
    // this.employeePositionHistoryService.getLocationList().then(data => {
    //     this.locationIdOption = data.result;
    //     console.log('test',this.locationIdOption );
    // });
    // this.employeePositionHistoryService.getEmployeePositionReasonList().then(data => {
    //     this.employeePositionReasonIdOption = data.result.records;
    //     console.log('test',this.employeePositionReasonIdOption );
    // });
      console.log(pagedData);
    });
   
}
sortColumn(val){
  if( this.page.sortOn == val ) {
      if( this.page.sortBy == 'DESC' ) {
          this.page.sortBy = 'ASC';
      } else {
          this.page.sortBy = 'DESC';
      }
  }
  this.page.sortOn = val;
  this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}
clearval()
{
    this.clearvalue = 1;
}

Clear(f: NgForm) {
  //f.resetForm();
 debugger;
 this.clearvalue = 0;
    this.divisionDescription = '';
    //this.employeeFirstName= '';
    this.departmentDescription = '';
    this.positionDescription1 = '';
    this.supervisorDescription = '';
    this.locationDescription = '';
    this.positionReason = '';
    this.model1.positionEffectiveDate = null;
    this.model1.employeeType = '0';
    this.model1.division.id = 0;
    this.model1.location.id = 0;
    this.model1.supervisor.id = '0';
    this.model1.dtoEmployeePositionReason.id = '0';
    this.model1.department.id = 0;
    this.model1.position.id = 0;
    this.divisionId = '';
    this.DepartmentId = '';
    this.PositionId = '';
    this.supervisiorId = '';
    this.LocationId = '';

}


 //edit department by row
  edit(row: employeePosition) {
  window.scrollTo(0, 0);
  this.divisionDescription= '';
  this.employeeFirstName= '';
  this.departmentDescription='';
  this.positionDescription1='';
  this.supervisorDescription='';
  this.locationDescription='';
  this.positionReason='';
  this.showCreateForm = true;
  this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  this.model1 = Object.assign({},row);
  this.model1.id=row.id;
   console.log(this.model1);
   var setDate = new Date(this.model1.positionEffectiveDate);
   this.positionEffectiveDate=this.model1.positionEffectiveDate;
   this.model1.positionEffectiveDate = this.formatDateFordatePicker(this.model1.positionEffectiveDate);
   this.EmployeePositionid = row.id;
   this.isUnderUpdate=true;
   this.idvalue=this.model1.id;
   this.employeeId=this.model1.employeeMaster.employeeId;
   this.employeeFirstName=this.model1.employeeMaster.employeeFirstName+' '+this.model1.employeeMaster.employeeMiddleName+' '+this.model1.employeeMaster.employeeLastName;
   this.divisionId = this.model1.division.divisionId;
   this.divisionDescription=this.model1.division.divisionDescription;
   this.DepartmentId = this.model1.department.departmentId;
   this.departmentDescription=this.model1.department.departmentDescription;
   this.PositionId=this.model1.position.positionId;
   this.positionDescription1=this.model1.position.description;
   this.supervisiorId=this.model1.supervisor.superVisionCode;
   this.supervisorDescription=this.model1.supervisor.description;
   this.LocationId=this.model1.location.locationId;
   this.locationDescription=this.model1.location.description;
   this.getEmployeeName(this.model1.employeeMaster.employeeIndexId);
   this.getDivisionDesc(this.model1.division.id);
   this.getDepartmentDesc(this.model1.department.id);
   this.getPositionDesc(this.model1.position.id);
   this.getSupervisorDesc(this.model1.supervisor.id);
   this.getLocationDesc(this.model1.location.id);
   this.getPositionReason(this.model1.position.id);
  
   setTimeout(() => {
       window.scrollTo(0, 500);
   }, 10);
   
 }

 //function call for creating new location
 CreateEmployeePositionHistory(f: NgForm, event: Event) {
    
    this.model1.positionEffectiveDate = this.positionEffectiveDate;
    event.preventDefault();
    var locIdx = this.model1.id;

    //Check if the id is available in the model.
    //If id avalable then update the Deduction Code, else Add new Deduction Code.
    if (this.model1.id > 0 && this.model1.id != 0 && this.model1.id != undefined) {
        this.isConfirmationModalOpen = true;
        this.isDeleteAction = false;
    }
    else {
                   
                //Call service api for Creating new Deduction Code
                this.employeePositionHistoryService.createEmployeePositionHistory(this.model1).then(data => {
                    var datacode = data.code;
                    if (datacode == 201) {
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.showCreateForm = false;
                            this.messageText = data.btiMessage.message;
                            ;
                        }, 100);

                        this.hasMsg = true;
                        f.resetForm();
                        //Refresh the Grid data after adding new Deduction Code
                        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = 'Server error. Please contact admin.';
                    }, 100)
                });
            
    }
}

getEmployeeName(id) {
    
        this.employeeMasterIdOption.forEach(element => {
            if(element.employeeIndexId==id){
                this.employeeFirstName = element.employeeFirstName+' '+element.employeeMiddleName+' '+element.employeeLastName;
            }
        });
}

getDivisionDesc(divisionId) {
         this.divisionIdOption.forEach(element => {
             if(element.id==divisionId){
                 this.divisionDescription = element.divisionDescription;
             }
         });
 }

 getDepartmentDesc(departmentId) {
        this.departmentIdoption.forEach(element => {
             if(element.id==departmentId){
                 this.departmentDescription = element.departmentDescription;
             }
         });
 }

 getPositionDesc(positionId) {
        this.positionIdOption.forEach(element => {
             if(element.id==positionId){
                 this.positionDescription1 = element.description;
             }
         });
 }

 
 changeTypeaheadNoResults(e: boolean, ErrorType): void {
    // debugger;
    if (ErrorType == 'EmployeeID') {
        const index: number = this.typeaheadNoResults.indexOf(ErrorType);
        if (index == -1 && e) {

            this.typeaheadNoResults.push(ErrorType);
        }
        else if (index !== -1 && !e) {

            this.typeaheadNoResults.splice(index, 1);
        }
    }
    else if (ErrorType == 'DivisionID') {
        const index: number = this.typeaheadNoResults1.indexOf(ErrorType);
        if (index == -1 && e) {

            this.typeaheadNoResults1.push(ErrorType);
        }
        else if (index !== -1 && !e) {

            this.typeaheadNoResults1.splice(index, 1);
        }
    }
    else if (ErrorType == 'DepartmentID') {
        const index: number = this.typeaheadNoResults2.indexOf(ErrorType);
        if (index == -1 && e) {

            this.typeaheadNoResults2.push(ErrorType);
        }
        else if (index !== -1 && !e) {

            this.typeaheadNoResults2.splice(index, 1);
        }
    }
    else if (ErrorType == 'Posision') {
        const index: number = this.typeaheadNoResults3.indexOf(ErrorType);
        if (index == -1 && e) {

            this.typeaheadNoResults3.push(ErrorType);
        }
        else if (index !== -1 && !e) {

            this.typeaheadNoResults3.splice(index, 1);
        }
    }
    else if (ErrorType == 'Supervisor') {
        const index: number = this.typeaheadNoResults4.indexOf(ErrorType);
        if (index == -1 && e) {

            this.typeaheadNoResults4.push(ErrorType);
        }
        else if (index !== -1 && !e) {

            this.typeaheadNoResults4.splice(index, 1);
        }
    }
    else if (ErrorType == 'Location') {
        const index: number = this.typeaheadNoResults5.indexOf(ErrorType);
        if (index == -1 && e) {

            this.typeaheadNoResults5.push(ErrorType);
        }
        else if (index !== -1 && !e) {

            this.typeaheadNoResults5.splice(index, 1);
        }
    }

    

}


 getSupervisorDesc(supervisorId) {
        this.superVisorIdOption.forEach(element => {
             if(element.id==supervisorId){
                 this.supervisorDescription = element.description;
             }
         });
 }

 getLocationDesc(locationId) {
        this.locationIdOption.forEach(element => {
             if(element.id==locationId){
                 this.locationDescription = element.description;
             }
         });
 }

 getPositionReason(positionreasonId) {
        this.employeePositionReasonIdOption.forEach(element => {
             if(element.id==positionreasonId){
                 this.positionReason = element.positionReason;
             }
         });
 }

 onEffectiveDateDateChanged(event: IMyDateModel): void {
     this.positionEffectiveDate = event.jsdate;
}

getEffectiveDate(date){
    let d = new Date(date);
    return d;
}

formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
}



}
