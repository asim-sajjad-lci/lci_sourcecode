import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { EmployeeDependents } from '../../_models/employee-dependents/employee-dependents.module';
import { EmployeeDependentsService } from '../../_services/employee-dependents/employee-dependents.service';
import { LocationService } from '../../_services/location/location.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'employee-dependents',
    templateUrl: './employee-dependents.component.html',
    providers: [EmployeeDependentsService,LocationService, CommonService, DatePipe]
})
export class EmployeeDependentsComponent {
    page = new Page();
    rows = new Array<EmployeeDependents>();
    temp = new Array<EmployeeDependents>();
    selected = [];
    selectedEmp = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1444';
    moduleName;
    screenName;
    skillsetupindexdesc;
    defaultFormValues:Object [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    skillId = {};
    skillSetIndexId: any = [];
    skillSetId = {};
    employeeIdOptions = [];
    searchKeyword = '';
    ddPageSize: number = 5;
    model: EmployeeDependents;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    tempp: string[] = [];
    islifeTimeValid: boolean = true;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    skillIdvalue: string;
    getSetId: any[] = [];
    setIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    skillSetDescId: any;
    skillsetsetupdesc: any[] = [];
    skillsetdesparr: any[] = [];
    skillSetsetId: any = [];
    employeeIndexId: any;
    empLastName: any;
    empFirstName: any;
    empMidleName: any;
    employeeDependentsId: any;
    address: any;
    phoneNumber: any;
    workNumber: any;
    city: any;
    comments: any;
    gender: any;
    dateOfBirth: any;
    dateofBirth: any;
    age: number;
    emcmname:any;
    employeeId:any;
    submitted:boolean=false;
    empDependent: any[] = [];
    phonePattern = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    employyeIdValid:boolean=false;
    frequencyArray = ["Hourly", "Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually"];
    skillrequiredpara = { "true": "Yes", "false": "No" };
    genderArry = ['', 'Male', 'Female'];
    relationship=[];
    countries = [];
    states = [];
    cities = [];
    countryId:number;
    stateId:number;
    cityId:number;
    cityName;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder:boolean = false;
    agevalid:boolean=true;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    EMPLOYEE_DEPENDENTS_SEARCH: any;
    EMPLOYEE_DEPENDENTS_EMPLOYEE_ID: any;
    EMPLOYEE_DEPENDENTS_LAST_NAME: any;
    EMPLOYEE_DEPENDENTS_FIRST_NAME: any;
    EMPLOYEE_DEPENDENTS_MIDDLE_NAME: any;
    EMPLOYEE_DEPENDENTS_RELATIONSHIP: any;
    EMPLOYEE_DEPENDENTS_ADDRESS: any;
    EMPLOYEE_DEPENDENTS_PHONE_NUMBER: any;
    EMPLOYEE_DEPENDENTS_WORK_NUMBER: any;
    EMPLOYEE_DEPENDENTS_CITY: any;
    EMPLOYEE_DEPENDENTS_COMMENT: any;
    EMPLOYEE_DEPENDENTS_GENDER: any;
    EMPLOYEE_DEPENDENTS_DATE_OG_BIRTH: any;
    EMPLOYEE_DEPENDENTS_EDIT: any;
    CREATE: any;
    UPDATE: any;
    EMPLOYEE_DEPENDENTS_CLEAR_LABEL: any;
    EMPLOYEE_DEPENDENTS_CANCEL_LABEL: any;
    EMPLOYEE_DEPENDENTS_SAVE: any;
    EMPLOYEE_DEPENDENTS_CREATE_FORM_LEBEL: any;
    EMPLOYEE_DEPENDENTS_UPDATE_FORM_LEBEL: any;
    EMPLOYEE_DEPENDENTS_DELETE: any;
    EMPLOYEE_DEPENDENTS_ACTION: any;
    SITE_SETUP_ACTION: any;

    constructor(private router: Router,
        private EmployeeDependentsService: EmployeeDependentsService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private locationService: LocationService,
        private commonService: CommonService,
        private datePipe: DatePipe) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        this.model = { listEmployeeDependent: [] };
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_LAST_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_FIRST_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_RELATIONSHIP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_ADDRESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_WORK_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_COMMENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_GENDER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_DATE_OG_BIRTH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_EDIT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_CREATE_FORM_LEBEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_UPDATE_FORM_LEBEL', 'fieldValue': '', 'helpMessage': '' },

            { 'fieldName': 'EMPLOYEE_DEPENDENTS_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SITE_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' }
        ];
        this.EMPLOYEE_DEPENDENTS_SEARCH = this.defaultFormValues[0];
        this.EMPLOYEE_DEPENDENTS_EMPLOYEE_ID = this.defaultFormValues[1];
        this.EMPLOYEE_DEPENDENTS_LAST_NAME = this.defaultFormValues[2];
        this.EMPLOYEE_DEPENDENTS_FIRST_NAME = this.defaultFormValues[3];
        this.EMPLOYEE_DEPENDENTS_MIDDLE_NAME = this.defaultFormValues[4];
        this.EMPLOYEE_DEPENDENTS_RELATIONSHIP = this.defaultFormValues[5];
        this.EMPLOYEE_DEPENDENTS_ADDRESS = this.defaultFormValues[6];
        this.EMPLOYEE_DEPENDENTS_PHONE_NUMBER = this.defaultFormValues[7];
        this.EMPLOYEE_DEPENDENTS_WORK_NUMBER = this.defaultFormValues[8];
        this.EMPLOYEE_DEPENDENTS_CITY = this.defaultFormValues[9];
        this.EMPLOYEE_DEPENDENTS_COMMENT = this.defaultFormValues[10];
        this.EMPLOYEE_DEPENDENTS_GENDER = this.defaultFormValues[11];
        this.EMPLOYEE_DEPENDENTS_DATE_OG_BIRTH = this.defaultFormValues[12];
        this.EMPLOYEE_DEPENDENTS_EDIT = this.defaultFormValues[13];
        this.CREATE = this.defaultFormValues[14];
        this.UPDATE = this.defaultFormValues[15];
        this.EMPLOYEE_DEPENDENTS_CLEAR_LABEL = this.defaultFormValues[16];
        this.EMPLOYEE_DEPENDENTS_CANCEL_LABEL = this.defaultFormValues[17];
        this.EMPLOYEE_DEPENDENTS_SAVE = this.defaultFormValues[18];
        this.EMPLOYEE_DEPENDENTS_CREATE_FORM_LEBEL = this.defaultFormValues[19];
        this.EMPLOYEE_DEPENDENTS_UPDATE_FORM_LEBEL = this.defaultFormValues[20];
        this.EMPLOYEE_DEPENDENTS_DELETE = this.defaultFormValues[21];
        this.EMPLOYEE_DEPENDENTS_ACTION = this.defaultFormValues[22];
        this.SITE_SETUP_ACTION = this.defaultFormValues[23];
        this.setIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.employeeId);
          }).mergeMap((token: string) => this.getSetIdAsObservable(token));
    }

    ngOnInit() {
        // this.skillSetsetId.forEach(element => {
        //     this.model.listSkillSteup.push({id:element});
        // });

        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.locationService.getCountry().then(data => {
            this.countries = data.result;
           //console.log(data.result);
        });
        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


        //Following shifted from SetPage for better performance
        this.EmployeeDependentsService.getEmployeeIdList().then(data => {
            this.employeeIdOptions = data.result;
            //console.log('test', this.employeeIdOptions);
        });
        this.EmployeeDependentsService.getRelationDropdown().then(data => {
            this.relationship = data.result;
            //console.log("Relation:", this.relationship);
        });
    }

    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'employeeId') {
            return row['employeeMaster'] && row['employeeMaster']['employeeId'] ? row['employeeMaster']['employeeId'] : '';
        } else if (gridFieldName === 'employeeFirstName') {
            return row['employeeMaster'] && row['employeeMaster']['employeeFirstName'] ? row['employeeMaster']['employeeFirstName'] : '';
        } else if (gridFieldName === 'employeeLastName') {
            return row['employeeMaster'] && row['employeeMaster']['employeeLastName'] ? row['employeeMaster']['employeeLastName'] : '';
        } else if (gridFieldName === 'employeeMiddleName') {
            return row['employeeMaster'] && row['employeeMaster']['employeeMiddleName'] ? row['employeeMaster']['employeeMiddleName'] : '';
        } else if (gridFieldName === 'empRelationshipId') {
            return row['employeeDependents'] && row['employeeDependents']['empRelationshipId'] ? row['employeeDependents']['empRelationshipId'] : '';
        } else if (gridFieldName === 'dateOfBirth') {
            return row[gridFieldName] ? this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy'): '';
        } else if (gridFieldName === 'gender') {
            return row[gridFieldName] === true ? 'Male' : 'Female';
        } else {
            return row[gridFieldName];
        }
    }

    getSetIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.employeeIdOptions.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
       console.log('Selected value: ', e);
        this.employeeIndexId=e.item.employeeIndexId;
        this.employeeId=e.item.employeeId;
        console.log('employeeindexid',this.employeeIndexId);
        if(this.employeeId=!undefined &&  this.employeeId==e.item.employeeId){
            this.employyeIdValid=true;
            this.employeeId=e.item.employeeId;
           //console.log('valid',this.employyeIdValid)
            this.EmployeeDependentsService.getEmployeeDependent(this.page,this.employeeIndexId).then(data=>{
                this.model.listEmployeeDependent=[];
                this.empDependent=[];
                if(data.status!="NOT_FOUND"){
                
                this.model.listEmployeeDependent=data.result.records;
               //console.log('listcontact',this.model.listEmployeeDependent);
                this.empDependent = Object.assign([], this.model.listEmployeeDependent);
                }
            })
        }
        else{
            this.employyeIdValid=false;
           //console.log('valid',this.employyeIdValid)
        }
        this.emcmname=e.item.employeeFirstName+' '+e.item.employeeMiddleName+' '+e.item.employeeLastName;
    }
    checkValid1(event){
        if(event.target.value == ''){
            this.employyeIdValid = true;
            
        } else{
            for(var i=0;i<this.employeeIdOptions.length;i++){
                if(this.employeeIdOptions[i].employeeId.includes(event.target.value)){
                    this.employyeIdValid = true;
                   
                    break;
                } else{
                    
                    this.employyeIdValid = false;
                }
            }


        }
}

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.EmployeeDependentsService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
           
           //console.log('all data',this.rows);
        });
    }

    // Print Document
    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        //printContents = document.getElementById('grid-print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto,scale=50');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
              <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
              <style>
              @media print{
                .doNotPrint{display:none;!important}
              }
              @page { size: landscape; 
                 margin-top: 25px;
                margin-bottom: 250px;
                margin-right: 0px;
                margin-left: 0px;
                -webkit-transform: scale(0.5);  /* Saf3.1+, Chrome */
                -moz-transform: scale(0.5);  /* FF3.5+ */
                -ms-transform: scale(0.5);  /* IE9 */
                -o-transform: scale(0.5);  /* Opera 10.5+ */
                transform: scale(0.5);  }
            
              
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }

    printEmployeeDetails(){
        console.log('test');  
        this.doNotPrintHearder = true;
        this.noPrintTable = true;
        this.printDetails = false;

        //setTimeout(()=> { window.print(); }, 100);
        setTimeout(()=> {this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint(){
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }

    // Open form for create location
    Create() {
        this.agevalid=true;
        this.employeeId='';
        this.submitted = false;
        this.empFirstName = '';
        this.empMidleName = '';
        this.empLastName = '';
        this.employeeDependentsId = '';
        this.phoneNumber = '';
        this.cityId = null;
        this.countryId = null;
        this.stateId = null;
        this.workNumber = '';
        this.address = '';
        this.city = '';
        this.dateOfBirth = '';
        this.gender = 0;
        this.employeeIndexId = '';
        this.comments = '';
        this.dateofBirth = '';
        this.age = 0;
        this.employeeDependentsId=0;
        this.emcmname='';
        this.model.listEmployeeDependent=[];
        this.empDependent=[];
        this.skillSetsetId = [];
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.employyeIdValid=false;
        setTimeout(() => {
            this.showCreateForm = true;
            this.doNotPrintHearder = false;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
       console.log('upper', this.model.listEmployeeDependent);
        this.model.listEmployeeDependent = [];
       //console.log('niche', this.model.listEmployeeDependent);

       this.locationService.getCountry().then(data => {
        this.countries = data.result;
        console.log(data.result);
    });
    this.countries.forEach(ele => {
        if(ele.defaultCountry==1){
            this.countryId = ele.countryId;
        }
    })
    console.log('counytyid',this.countryId);  
    this.locationService.getStatesByCountryId(this.countryId).then(data => {
        this.states = data.result;
        this.stateId=this.states[0].stateId;

    this.locationService.getCitiesByStateId(this.stateId).then(data => {
        this.cities = data.result;
        this.cityId = this.cities[0].cityId;
        this.cityName = this.cities[0].cityName;
    });
    });
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        this.agevalid=true;
      // f.resetForm({ gender:0,employeeDependentsId:0});
        this.empFirstName = '';
        this.empMidleName = '';
        this.empLastName = '';
        this.employeeDependentsId = '';
        this.phoneNumber = '';
        this.workNumber = '';
        this.address = '';
        this.city = '';
        this.dateOfBirth = '';
        this.gender = 0;
        this.employeeIndexId = '';
        this.comments = '';
        this.dateofBirth = '';
        this.age = 0;
        this.employeeDependentsId=0;
        this.cityId = 0;
        this.countryId = 0;
        this.stateId = 0;
        //this.model.listEmployeeDependent=[];
        this.empDependent=[];
       this.submitted=false;
       this.doNotPrintHearder = false;
    }


    //function call for creating new location
    CreateSkillsSetup(f: NgForm, event: Event) {
        
        event.preventDefault();
        console.log('this.model', this.model);
        if(this.model.listEmployeeDependent.length == 0){
            window.scroll(0,0)
            this.hasMsg = true;
            window.setTimeout(() => {
                console.log('Not Exe')
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Please insert atleast one record in second grid from given arrow key.';
                window.setTimeout(() => {
                    console.log('HI Exe')
                    this.showMsg = false;
                    this.hasMsg = false
                }, 4000);
            }, 100);
            return;
        }
        var locIdx = this.model.listEmployeeDependent[0].id;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.listEmployeeDependent[0].id > 0 && this.model.listEmployeeDependent[0].id != 0 && this.model.listEmployeeDependent[0].id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            // this.skillSetsetId.forEach(element => {
            //     this.model.listSkillSteup.push({id:element});
            // });
            this.EmployeeDependentsService.checkDuplicateSkillsSetupId(locIdx).then(response => {


                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code


                    this.EmployeeDependentsService.createEmployeeDependent(this.model).then(data => {

                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            this.skillsetupindexdesc = '';
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }
    moveToAllEmployee() {
        
        this.model.listEmployeeDependent.push({
            employeeMaster: { employeeIndexId: this.employeeIndexId },
            empFistName: this.empFirstName,
            empMidleName: this.empMidleName,
            empLastName: this.empLastName,
            employeeDependents: { id: this.employeeDependentsId,desc:this.relationship[this.employeeDependentsId-1]['desc'] },
            phoneNumber: this.phoneNumber,
            countryId: this.countryId,
            stateId: this.stateId,
            cityId: this.cityId,
            workNumber: this.workNumber,
            address: this.address,
            city: this.city,
            cityName:this.cityName,
            dateOfBirth: this.dateOfBirth,
            completeName: this.empFirstName + ' ' + this.empMidleName + ' ' + this.empLastName,
            gender: this.gender,
            comments: this.comments,
            age: this.age,
            relation:this.relationship[this.employeeDependentsId-1]['desc']

        });
        this.empDependent = [];
        
        this.empDependent = Object.assign([], this.model.listEmployeeDependent);
       //console.log("Inserted Value:", this.empDependent['employeeDependentsId']);
       //console.log('listobj', this.model.listEmployeeDependent);
        this.submitted = false;
        this.empFirstName = '';
        this.empMidleName = '';
        this.empLastName = '';
        this.phoneNumber = '';
        this.cityId = 0;
        this.countryId = 0;
        this.stateId = 0;
        this.workNumber = '';
        this.address = '';
        this.city = '';
        this.dateOfBirth = '';
        this.gender = 0;
        this.comments = '';
        this.dateofBirth = '';
        this.age = 0;
        this.employeeDependentsId=0;

    }
    moveToSelected() {

       //console.log("selected", this.selected);
        for (var i = 0; i < this.model.listEmployeeDependent.length; i++) {
            for(var j=0; j<this.selected.length;j++){
            if (this.model.listEmployeeDependent[i].employeeDependents.id == this.selected[j].employeeDependents.id && this.model.listEmployeeDependent[i].completename == this.selected[j].completename
                && this.model.listEmployeeDependent[i].age == this.selected[j].age && this.model.listEmployeeDependent[i].gender == this.selected[j].gender &&
                this.model.listEmployeeDependent[i].city == this.selected[j].city && this.model.listEmployeeDependent[i].phoneNumber == this.selected[j].phoneNumber
                ) {
                this.model.listEmployeeDependent.splice(i,1);
            }
        }
        }
        this.empDependent=[];
        this.empDependent = Object.assign([], this.model.listEmployeeDependent);
        this.selected=[];
    }

    //edit department by row
    // editcheckbox(row: EmployeeDependents,event) {        
    //     this.model = Object.assign({},row);
    // 	this.model.skillRequired =  event.target.checked;
    // 	this.skillSetId = row.skillSetId;
    //     this.skillSetDescId = this.model.skillSetDescId;
    // 	this.skillIdvalue = this.model.skillSetId;
    // 	this.isConfirmationModalOpen = true;
    //     this.isDeleteAction = false;
    // 	//console.log('model == ',this.model);
    // }
    //edit department by row
    edit(row: any) {
        this.employyeIdValid=true;
        this.submitted = true;
        this.agevalid=true;
       //console.log(row);
        this.showCreateForm = true;
        this.doNotPrintHearder = false;
        this.model.listEmployeeDependent[0] = Object.assign({}, row);
        this.model.listEmployeeDependent[0].id=row.id;
         this.employeeIndexId = row.employeeMaster.employeeIndexId;
         this.employeeId=row.employeeMaster.employeeId;
        //console.log('this.employeeIndexId',this.employeeIndexId);
        this.isUnderUpdate = true;
        this.skillIdvalue = this.model.listEmployeeDependent[0].id;
        this.empLastName = this.model.listEmployeeDependent[0].empLastName;
        this.empFirstName = this.model.listEmployeeDependent[0].empFistName;
        this.empMidleName = this.model.listEmployeeDependent[0].empMidleName;
        this.employeeDependentsId = this.model.listEmployeeDependent[0].employeeDependents.id;
        this.address = this.model.listEmployeeDependent[0].address;
        this.phoneNumber = this.model.listEmployeeDependent[0].phoneNumber;
        this.countryId = this.model.listEmployeeDependent[0].countryId;
        this.stateId = this.model.listEmployeeDependent[0].stateId;
        this.cityId = this.model.listEmployeeDependent[0].cityId;
        this.cityName = this.model.listEmployeeDependent[0].cityName;
        this.workNumber = this.model.listEmployeeDependent[0].workNumber;
        this.city = this.model.listEmployeeDependent[0].city;
        this.comments = this.model.listEmployeeDependent[0].comments;
        this.gender = this.model.listEmployeeDependent[0].gender;
        this.dateofBirth = this.formatDateFordatePicker(this.model.listEmployeeDependent[0].dateOfBirth);
        this.emcmname = this.model.listEmployeeDependent[0].employeeMaster.employeeFirstName+' '+this.model.listEmployeeDependent[0].employeeMaster.employeeMiddleName+' '+this.model.listEmployeeDependent[0].employeeMaster.employeeLastName;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
        this.locationService.getStatesByCountryId(row.countryId).then(data => {
            this.states = data.result;
        });
        this.locationService.getCitiesByStateId(row.stateId).then(data => {
            this.cities = data.result;
        });
    }

    updateStatus() {
        this.submitted=false;
        this.closeModal();
        this.doNotPrintHearder = false;
        this.model.listEmployeeDependent[0].id = this.skillIdvalue;
        this.model.listEmployeeDependent[0].employeeMaster.employeeIndexId = this.employeeIndexId;
        this.model.listEmployeeDependent[0].employeeMaster.employeeId = this.employeeId;
        this.model.listEmployeeDependent[0].empLastName =  this.empLastName;
        this.model.listEmployeeDependent[0].empFistName = this.empFirstName;
        this.model.listEmployeeDependent[0].empMidleName = this.empMidleName
        this.model.listEmployeeDependent[0].employeeDependents.id = this.employeeDependentsId;
        this.model.listEmployeeDependent[0].address = this.address;
        this.model.listEmployeeDependent[0].phoneNumber =  this.phoneNumber;
        this.model.listEmployeeDependent[0].countryId =  this.countryId;
        this.model.listEmployeeDependent[0].stateId =  this.stateId;
        this.model.listEmployeeDependent[0].cityId =  this.cityId;
        this.model.listEmployeeDependent[0].cityName =  this.cityName;
        this.model.listEmployeeDependent[0].workNumber = this.workNumber;
        this.model.listEmployeeDependent[0].city = this.city;
        this.model.listEmployeeDependent[0].comments = this.comments;
        this.model.listEmployeeDependent[0].gender = this.gender;
        this.model.listEmployeeDependent[0].dateOfBirth =this.dateOfBirth;
        this.model.listEmployeeDependent[0].age =this.age;
        

        //Call service api for updating selected department
        this.EmployeeDependentsService.updateEmployeeDependent(this.model).then(data => {

            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.skillsetupindexdesc = '';
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000)
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedSkillsSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSkillsSetups.push(this.selected[i].id);
        }
        this.EmployeeDependentsService.deleteEmployeeDependent(selectedSkillsSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }
    onSelectEmpDep({ selectedEmp }) {
        this.selectedEmp.splice(0, this.selectedEmp.length);
        this.selectedEmp.push(...selectedEmp);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.SkillsSetupService.searchSkillsSetuplist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    // CheckNumber(event) {
    //     if (isNaN(event.target.value) == true) {
    //         //this.model.skillId = 0;
    //         this.model.compensation = 1;
    //         return false;
    //     }
    // }
    // CheckNumberNonZero(event) {
    // 	if (event.target.value <= 0) {
    //         this.model.compensation = 1;
    //         return false;
    //     }
    // }
    // checkFrequency(event) {
    //     this.skillSetsetId.forEach(element => {
    //         this.skillsetdesparr = [];
    //         console.log('id', element);
    //         this.EmployeeDependentsService.getSkillSetupById(element).then(pagedData => {
    //             //this.skillsetupindexdesc = pagedData.result.skillDesc;
    //             this.skillsetdesparr.push(pagedData.result.skillDesc);
    //             this.skillsetupindexdesc = this.skillsetdesparr.join();
    //         });
    //         if (event.target.value == 0) {
    //             return false;
    //         }
    //     });
    // }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkdecimal(digit) {
       //console.log(digit);
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValid = false;
           //console.log(this.islifeTimeValid);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValid = false;
           //console.log(this.islifeTimeValid);
        }

        else {
            this.islifeTimeValid = true;
           //console.log(this.islifeTimeValid);
        }

    }
    updateSkillCheckbox(val1, val2) {
        console.log('val2 == ', val2);
        console.log('val2 == ', val2);
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onStartDateChanged(event: IMyDateModel): void {
        var datebirth = event.jsdate;
        this.dateOfBirth = event.jsdate;
        var todayd =  new Date(this.dateOfBirth);
        var dd = todayd.getDay();

        var mm = todayd.getMonth()+1; 
        var yyyy = todayd.getFullYear();
        if(dd<10) 
        {
        var  ddd='0'+dd;
        } 

        if(mm<10) 
        {
        var mmm='0'+mm;
        } 
        var todaydd = yyyy+'-'+mmm+'-'+ddd;
        
       
        var today = new Date();
        var birthDate = new Date(datebirth);
       //console.log('birthyear', birthDate.getFullYear());
        var curage = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        // if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        //     curage--;
        // }
        this.age = curage;
        if(this.age<15){
            this.agevalid=false;
        }
        else{
            this.agevalid=true;
        }

       //console.log('this.frmStartDate', this.dateOfBirth);
        
    }

    onCountrySelect(event) {
        this.stateId = 0;
        this.cityId = 0;
        this.locationService.getStatesByCountryId(event.target.value).then(data => {
            this.states = data.result;
        });
        this.cities = [];
    }

    onStateSelect(event) {
        this.cityId = 0;
        this.locationService.getCitiesByStateId(event.target.value).then(data => {
            this.cities = data.result;
        });
    }

}
