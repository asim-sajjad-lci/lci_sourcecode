import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { Page } from '../../../_sharedresource/page';
import { ProjectSetup } from '../../_models/project-setup/project-setup';
import { ProjectSetupService } from '../../_services/project-setup/project-setup.service';
import { NgForm } from '@angular/forms';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { IMyDateModel, INgxMyDpOptions } from 'ngx-mydatepicker';

@Component({
  selector: 'app-project-setup',
  templateUrl: './project-setup.component.html',
  styleUrls: ['./project-setup.component.css'],
  providers: [CommonService, ProjectSetupService]
})
export class ProjectSetupComponent implements OnInit {

  moduleCode = "M-1011";
  screenCode = "S-1787";
  defaultFormValues: object[];
  isUnderUpdate: boolean;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  selected = [];
  searchKeyword = '';

  PROJECT_ID: any;
  PROJECT_SEARCH: any;
  PROJECT_DESCRIPTION: any;
  PROJECT_ARABIC_DESCRIPTION: any;
  PROJECT_ACTION: any;
  PROJECT_CREATE_LABEL: any;
  PROJECT_SAVE_LABEL: any;
  PROJECT_CLEAR_LABEL: any;
  PROJECT_CANCEL_LABEL: any;
  PROJECT_UPDATE_LABEL: any;
  PROJECT_DELETE_LABEL: any;
  PROJECT_CREATE_FORM_LABEL: any;
  PROJECT_UPDATE_FORM_LABEL: any;
  PROJECT_GREGORIAN_START_DATE: any;
  PROJECT_HIJRI_START_DATE: any;
  PROJECT_NAME: any;
  PROJECT_ARABIC_NAME: any;
  PROJECT_START_DATE: any;
  PROJECT_END_DATE: any;

  rows = new Array<ProjectSetup>();
  showCreateForm: boolean = false;
  model: ProjectSetup;
  projectIdValue;
  projectId = {};
  projId;
  page = new Page();

  modelStartDate;
  modelEndDate;
  startDate;
  endDate;
  ddPageSize: number = 5;
  error: any = { isError: false, errorMessage: '' };
  getProject: any[] = [];
  typeaheadLoading: boolean;
  projectIdList: Observable<any>;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private commonService: CommonService,
    private service: ProjectSetupService
  ) {
    this.defaultFormValues = [
      { 'fieldName': 'PROJECT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_ACTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_GREGORIAN_START_DATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_HIJRI_START_DATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_NAME', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_START_DATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'PROJECT_END_DATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
      { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
    ];

    this.PROJECT_DESCRIPTION = this.defaultFormValues[0];
    this.PROJECT_ID = this.defaultFormValues[1];
    this.PROJECT_SEARCH = this.defaultFormValues[2];
    this.PROJECT_ARABIC_DESCRIPTION = this.defaultFormValues[3];
    this.PROJECT_ACTION = this.defaultFormValues[4];
    this.PROJECT_CREATE_LABEL = this.defaultFormValues[5];
    this.PROJECT_SAVE_LABEL = this.defaultFormValues[6];
    this.PROJECT_CLEAR_LABEL = this.defaultFormValues[7];
    this.PROJECT_CANCEL_LABEL = this.defaultFormValues[8];
    this.PROJECT_UPDATE_LABEL = this.defaultFormValues[9];
    this.PROJECT_DELETE_LABEL = this.defaultFormValues[10];
    this.PROJECT_CREATE_FORM_LABEL = this.defaultFormValues[11];
    this.PROJECT_UPDATE_FORM_LABEL = this.defaultFormValues[12];
    this.PROJECT_GREGORIAN_START_DATE = this.defaultFormValues[13];
    this.PROJECT_HIJRI_START_DATE = this.defaultFormValues[14];
    this.PROJECT_NAME = this.defaultFormValues[15];
    this.PROJECT_ARABIC_NAME = this.defaultFormValues[16];
    this.PROJECT_START_DATE = this.defaultFormValues[17];
    this.PROJECT_END_DATE = this.defaultFormValues[18];


    this.projectIdList = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.model.projectId);
  }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));

  }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    console.log("default", this.defaultFormValues)

    //Following shifted from SetPage for better performance
          this.service.getAllProject().then(data => {
            debugger
         this.getProject = data.result;
         console.log("getProject",this.getProject)

     });
  }

    getReportPositionsAsObservable(token: string): Observable<any> {
      debugger
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
        this.getProject.filter((id: any) => {
            return query.test(id.projectId);
        })
    );
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    debugger;
    // console.log('Selected value: ', e);
    this.service.getProject(e.item.id).then(allData => {
        // console.log('allData', allData);
        debugger;
        this.model = allData.result;
        this.model.id = 0;
    });

  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  Create() {
    this.showCreateForm = false;
    this.isUnderUpdate = false;
    setTimeout(() => {
      this.showCreateForm = true;
      setTimeout(() => {
        window.scrollTo(0, 2000);
      }, 10);
    }, 10);


    this.model = {
      id: 0,
      projectId: '',
      projectName: '',
      projectArabicName: '',
      projectDescription: '',
      projectArabicDescription: '',
      startDate: '',
      endDate: ''
    };

    this.modelStartDate = "";
    this.modelEndDate = "";
    this.projectIdValue = "";
  }


  CreateProject(f: NgForm, event: Event) {
    debugger;
    event.preventDefault();
    var projectIdx = this.model.projectId;

    this.model.endDate = this.modelEndDate.date.year + "-" + this.modelEndDate.date.month + "-" + this.modelEndDate.date.day;
    this.model.startDate = this.modelStartDate.date.year + "-" + this.modelStartDate.date.month + "-" + this.modelStartDate.date.day;

    //Check if the id is available in the model.
    //If id avalable then update the Project, else Add new Project.
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
      this.isConfirmationModalOpen = true;
      this.isDeleteAction = false;
    }
    else {
      debugger
      //Check for duplicate Project Id according to it create new Project
      this.service.checkDuplicateProjectId(projectIdx).then(response => {
        debugger
        if (response && response.code == 302 && response.result && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = "success";

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;
          }, 100);
        } else {
          //Call service api for Creating new Project
          this.service.createProject(this.model).then(data => {
            debugger;
            var datacode = data.code;
            if (datacode == 201) {
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                this.hasMessage = false;
                window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
                }, 4000);
                // this. isValueDisable = false;
                // this.MiscId = data.result.id;
                this.showCreateForm = false;
                this.messageText = data.btiMessage.message;
              }, 100);

              this.hasMsg = true;
                f.resetForm();
              //Refresh the Grid data after adding new Project
                 this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = "Server error. Please contact admin.";
            }, 100)
          });
        }

      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = "Server error. Please contact admin.";
        }, 100)
      });

    }
  }

  setPage(pageInfo) {
debugger;
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
    } else {
      this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
    } else {
      this.page.sortBy = pageInfo.sortBy;
    }

    this.page.searchKeyword = '';
    this.service.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      debugger;
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

  // search department by keyword 
  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;

  }

  updateStatus() {
    debugger
    this.closeModal();
    //Call service api for updating selected Project
    this.model.projectId = this.projectIdValue;
    this.service.updateProject(this.model).then(data => {
      debugger
      var datacode = data.code;
      if (datacode == 201) {
        //Refresh the Grid data after editing Project
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        //Scroll to top after editing Project
        window.scrollTo(0, 0);
        window.setTimeout(() => {
          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          this.messageText = data.btiMessage.message;
          this.showCreateForm = false;
          this.isUnderUpdate = false;
          this.modelStartDate = "";
          this.modelEndDate = "";
        }, 100);
        this.hasMessage = false;
        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = "Server error. Please contact admin.";
      }, 100)
    });
  }


  edit(row: ProjectSetup) {
    debugger;
    this.showCreateForm = true;
    //this.isValueDisable = false;
   // this.projId = row.id;
    this.model = Object.assign({}, row);
    this.isUnderUpdate = true;
    this.projectId = row.projectId;
    this.projectIdValue = this.model.projectId;
    this.modelStartDate = this.formatDateFordatePicker(this.model.startDate) ;
    this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
    setTimeout(() => {
      window.scrollTo(0, 2000);
    }, 10);
  }

  formatDateFordatePicker(strDate: string): any {
    debugger
    if (strDate != null) {
        var setDate = new Date(strDate);
        return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
    } else {
        return null;
    }
}

  Clear(f: NgForm) {
    f.resetForm();
  }

  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }

  // Set default page size
  changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  sortColumn(val) {
    if (this.page.sortOn == val) {
      if (this.page.sortBy == 'DESC') {
        this.page.sortBy = 'ASC';
      } else {
        this.page.sortBy = 'DESC';
      }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  myOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: 'dd-mm-yyyy',
};

onStartDateChanged(event: IMyDateModel): void {
  debugger;
    this.modelStartDate = event.jsdate;
    this.startDate = event.epoc;
   
    //console.log('this.frmStartDate', event);
    if ((this.startDate > this.endDate) && this.endDate != undefined) {
        this.error = { isError: true, errorMessage: 'Invalid End Date.' };
    }
    if (this.startDate <= this.endDate) {
        this.error = { isError: false, errorMessage: '' };
    }
    if (this.endDate == undefined && this.startDate == undefined) {
        this.error = { isError: false, errorMessage: '' };
    }
}

onEndDateChanged(event: IMyDateModel): void {
  debugger;
    this.modelEndDate = event.jsdate;
    this.endDate = event.epoc;
  
    if ((this.startDate > this.endDate) && this.endDate != undefined) {
        this.error = { isError: true, errorMessage: ' Invalid End Date' };
    }
    if (this.startDate <= this.endDate) {
        this.error = { isError: false, errorMessage: '' };
    }
    if (this.endDate == undefined && this.startDate == undefined) {
        this.error = { isError: false, errorMessage: '' };
    }

}

varifyDelete() {
  if (this.selected.length > 0) {
      this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
  } else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;
      this.message.text = 'Please select at least one record to delete.';
      window.scrollTo(0, 0);
  }
}

//delete Project by passing whole object of perticular Project
delete() {
    debugger;
  var selectedprojects = [];
  for (var i = 0; i < this.selected.length; i++) {
    selectedprojects.push(this.selected[i].id);
  }
  this.service.deleteProject(selectedprojects).then(data => {
    debugger;
      var datacode = data.code;
      if (datacode == 200) {
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if(datacode == "302"){
          this.message.type = "error";
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
      } else {
          this.isSuccessMsg = true;
          this.message.type = "success";
          this.isfailureMsg = false;
      }
      //this.message.text = data.btiMessage.message;

      window.scrollTo(0, 0);
      window.setTimeout(() => {
          this.showMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMessage = false;
          }, 4000);
          this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of Project
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

  }).catch(error => {
      this.hasMessage = true;
      this.message.type = "error";
      var errorCode = error.status;
      this.message.text = "Server issue. Please contact admin.";
  });
  this.closeModal();
}

}
