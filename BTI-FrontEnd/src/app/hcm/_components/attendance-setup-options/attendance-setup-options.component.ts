import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AttendanceSetupOptions } from '../../_models/attendance-setup-options/attendance-setup-options.module';
import { AttendanceSetupOptionsService } from '../../_services/attendance-setup-options/attendance-setup-options.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';


@Component({
    selector: 'attendanceSetupOptions',
    templateUrl: './attendance-setup-options.component.html',
    providers: [AttendanceSetupOptionsService],
})
export class AttendanceSetupOptionsComponent {

    page = new Page();
    pageType = new Page();
    rows = new Array<AttendanceSetupOptions>();
    dataGridType = new Array<AttendanceSetupOptions>();
    temp = new Array<AttendanceSetupOptions>();
    selectedOptn = [];
    selectedType = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1407';
    moduleName;
    screenName;
    defaultFormValues: Array<any> = [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    locationId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: AttendanceSetupOptions;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    seqn: number;
    modelReasonDesc;
    reasonDescVal;
    reasonIndx: number;
    reasonDesc: string;
    atteandanceTypeIndx: number;
    atteandanceTypeSeqn: number;
    atteandanceTypeDesc: string;
    isActive: boolean = false;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    locationIdvalue: number;
    showDiv: boolean;

    isType: boolean = false;

    pattern = /*/^\d*\.?\d{0,3}$/*/ /^[\d]*$/;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;



    constructor(private router: Router,
        private attendanceSetupOptionsService: AttendanceSetupOptionsService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.pageType.pageNumber = 0;
        this.page.size = 5;
        this.pageType.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'LOCATION_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_REASON_INDEX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_REASON_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_TYPE_INDEX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_TYPE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_OPTION_INSERT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_OPTION_REMOVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
        ];

    }

    public startDateModel: any = { date: { year: 2018, month: 10, day: 9 } };

    ngOnInit() {
        this.setPage({ offset: 0 });
        this.setPageType({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.model = {
            id: 0,
            seqn: 0,
            reasonIndx: 0,
            reasonDesc: '',
            atteandanceTypeIndx: 0,
            atteandanceTypeSeqn: 0,
            atteandanceTypeDesc: '',
            isActive: true
        };
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    Clear(f: NgForm) {
        f.resetForm();
    }

    //setting pagination
    setPage(pageInfo) {
        //debugger;
        this.selectedOptn = [];
        // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.attendanceSetupOptionsService.getlist(this.page).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            //console.log("ROWS:", this.rows);
        });
    }

    //setting pagination for type
    setPageType(pageInfo) {
        //debugger;
        this.selectedType = []
        this.pageType.pageNumber = pageInfo.offset;
        this.pageType.searchKeyword = '';
        this.attendanceSetupOptionsService.getlistType(this.pageType, this.searchKeyword).subscribe(pagedData => {
            this.pageType = pagedData.page;
            this.dataGridType = pagedData.data;
            //console.log("DATAGRIDTYPE:", this.dataGridType);
        });
    }



    CreateAttendanceOptionsSetup(f: NgForm, event: Event) {

        this.attendanceSetupOptionsService.createAttendanceSetupOption(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.showCreateForm = false;
                    this.messageText = data.btiMessage.message;
                }, 100);

                this.hasMsg = true;
                f.resetForm();
                //Refresh the Grid data after adding new department
                this.setPage({ offset: 0 });
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });


    }

    createAttendanceSetupOptionType(tf: NgForm, event: Event) {
        this.attendanceSetupOptionsService.createAttendanceSetupOptionType(this.model).then(data => {
            var datacode = data.code;
            // this.dataGrid2 = data.result;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.showCreateForm = false;
                    this.messageText = data.btiMessage.message;
                }, 100);
                this.hasMsg = true;
                tf.resetForm();
                this.setPageType({ offset: 0 });

            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";

            }, 100)
        })
    }

    /*onEndDateChanged(event: IMyDateModel): void {
        this.lastaccruedDate = event.jsdate;
    }*/
    // default list on page
    onSelect({ selected }) {
        this.selectedOptn.splice(0, this.selectedOptn.length);
        this.selectedOptn.push(...selected);
    }

    onSelectType({ selected }) {
        //console.log(selected) ;             
        this.selectedType.splice(0, this.selectedType.length);
        this.selectedType.push(...selected);

    }

    verifyDelete() {
        if (this.selectedOptn.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
            this.isType = false;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    // Delete Type

    verifyDeleteType() {
        if (this.selectedType.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
            this.isType = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    //delete department by passing whole object of perticular Department
    delete() {
        if (this.selectedType.length != 0 && this.isType === true) {
            this.deleteType();
        } else {
            var selectedAttendanceOptions = [];
            for (var i = 0; i < this.selectedOptn.length; i++) {
                selectedAttendanceOptions.push(this.selectedOptn[i].id);
            }
            this.attendanceSetupOptionsService.deleteAttendanceOption(selectedAttendanceOptions).then(data => {
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0 });

                }
                this.hasMessage = true;
                this.message.type = "success";
                //this.message.text = data.btiMessage.message + " !";

                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message + " !";
                }, 100);

                //Refresh the Grid data after deletion of department
                this.setPage({ offset: 0 });


            }).catch(error => {
                this.hasMessage = true;
                this.message.type = "error";
                var errorCode = error.status;
                this.message.text = "Server issue. Please contact admin.";
            });
            this.closeModal();
        }

    }
    // delete For Option Type

    deleteType() {
        var selectedAttendanceOptions = [];
        for (var i = 0; i < this.selectedType.length; i++) {
            selectedAttendanceOptions.push(this.selectedType[i].id);
        }

        this.attendanceSetupOptionsService.deleteAttendanceOptionType(selectedAttendanceOptions).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0 });
                this.setPageType({ offset: 0 });
            }
            this.hasMessage = true;
            this.message.type = "success";


            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department

            this.setPageType({ offset: 0 });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.isType = false;
        this.closeModal();
    }

    confirm(): void {
        this.messageText = 'Confirmed!';

        this.delete();

    }
    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
    hideShowDiv(event) {
        if (event.target.value == 1) {
            this.showDiv = true;
        } else {
            this.showDiv = false;

        }

    }
}
