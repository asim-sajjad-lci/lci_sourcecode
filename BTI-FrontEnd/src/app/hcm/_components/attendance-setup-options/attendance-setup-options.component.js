"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var attendance_setup_options_service_1 = require("../../_services/attendance-setup-options/attendance-setup-options.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var AttendanceSetupOptionsComponent = (function () {
    function AttendanceSetupOptionsComponent(router, attendanceSetupOptionsService, getScreenDetailService, alertService) {
        this.router = router;
        this.attendanceSetupOptionsService = attendanceSetupOptionsService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1407';
        this.message = { 'type': '', 'text': '' };
        this.locationId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isActive = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.pattern = /^[\d]*$/;
        this.startDateModel = { date: { year: 2018, month: 10, day: 9 } };
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'LOCATION_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_REASON_INDEX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_REASON_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_TYPE_INDEX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_TYPE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_INSERT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_REMOVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_FAX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    AttendanceSetupOptionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.model = {
            id: null,
            seqn: 0,
            reasonIndx: 0,
            reasonDesc: '',
            atteandanceTypeIndx: 0,
            atteandanceTypeSeqn: 0,
            atteandanceTypeDesc: '',
            ids: '',
            isActive: false
        };
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            console.log(data);
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    AttendanceSetupOptionsComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //setting pagination
    AttendanceSetupOptionsComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.attendanceSetupOptionsService.getlist(this.page).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    AttendanceSetupOptionsComponent.prototype.verifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = false;
            this.hasMsg = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    AttendanceSetupOptionsComponent.prototype.CreateAttendanceOptionsSetup = function (f, event) {
        var _this = this;
        this.model.ids = null;
        this.attendanceSetupOptionsService.createAttendanceSetupOption(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.showCreateForm = false;
                    _this.messageText = data.btiMessage.message;
                }, 100);
                _this.hasMsg = true;
                f.resetForm();
                //Refresh the Grid data after adding new department
                _this.setPage({ offset: 0 });
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = "Server error. Please contact admin.";
            }, 100);
        });
    };
    /*onEndDateChanged(event: IMyDateModel): void {
        this.lastaccruedDate = event.jsdate;
    }*/
    // default list on page
    AttendanceSetupOptionsComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    //delete department by passing whole object of perticular Department
    AttendanceSetupOptionsComponent.prototype.delete = function () {
        var _this = this;
        var selectedAttendanceOptions = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedAttendanceOptions.push(this.selected[i].id);
        }
        this.attendanceSetupOptionsService.deleteAttendanceOption(selectedAttendanceOptions).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = "success";
            //this.message.text = data.btiMessage.message + " !";
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message + " !";
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = "error";
            var errorCode = error.status;
            _this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    };
    AttendanceSetupOptionsComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    };
    AttendanceSetupOptionsComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    AttendanceSetupOptionsComponent.prototype.hideShowDiv = function (event) {
        if (event.target.value == 1) {
            this.showDiv = true;
        }
        else {
            this.showDiv = false;
        }
    };
    return AttendanceSetupOptionsComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], AttendanceSetupOptionsComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], AttendanceSetupOptionsComponent.prototype, "myScrollContainer", void 0);
AttendanceSetupOptionsComponent = __decorate([
    core_1.Component({
        selector: 'attendanceSetupOptions',
        templateUrl: './attendance-setup-options.component.html',
        providers: [attendance_setup_options_service_1.AttendanceSetupOptionsService],
    }),
    __metadata("design:paramtypes", [router_1.Router,
        attendance_setup_options_service_1.AttendanceSetupOptionsService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], AttendanceSetupOptionsComponent);
exports.AttendanceSetupOptionsComponent = AttendanceSetupOptionsComponent;
//# sourceMappingURL=attendance-setup-options.component.js.map