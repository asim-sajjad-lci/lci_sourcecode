import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountStatementReportComponent } from './account-statement-report.component';

describe('AccountStatementReportComponent', () => {
  let component: AccountStatementReportComponent;
  let fixture: ComponentFixture<AccountStatementReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountStatementReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountStatementReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
