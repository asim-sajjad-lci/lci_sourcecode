import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { EmployeeQuickAssignmentService } from '../../_services/employee-quick-assignment/employee-quick-assignment.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';
import { Observable } from 'rxjs/Observable';
import { EmployeeMaster } from '../../_models/employee-master/employee-master.module';
import { EmployeeMasterService } from '../../_services/employee-master/employee-master.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { EmployeeQuickAssignmentModule } from '../../_models/employee-quick-assignment/employee-quick-assignment.module';
import { FormControl } from 'ng2-datepicker-bootstrap/node_modules/@angular/forms/src/model';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
  selector: 'accountStatementReport',
  templateUrl: './account-statement-report.component.html',
  styleUrls: ['./account-statement-report.component.css'],
  providers:  [EmployeeMasterService, EmployeeQuickAssignmentService]
})
export class AccountStatementReportComponent implements OnInit {
    page = new Page();
    selected = [];
    model: Object;
    moduleCode = 'M-1011';
    screenCode = 'S-1460';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    Warning;
    message = { 'type': '', 'text': '' };
    searchKeyword = '';
    ddPageSize: number = 5;
    showTable: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    printConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isPrintAction: boolean = false;
    accountNumvalue: string;
    selectedRowId: number = 0;
    startDate: any;
    endDate: any;
    fromStDate: Date;
    toEndDate: Date;
    endDateInvalid: boolean = false;
    infoLoaded: boolean = false;
    getEmployee: any[] = [];
    accountNumList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private employeeMasterService: EmployeeMasterService,
        private getScreenDetailService: GetScreenDetailService,
        private employeeQuickAssignmentService: EmployeeQuickAssignmentService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'ACCOUNT_STMT_REPORT_FROM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_TO', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_GET_DATA', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_ACC_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_DESC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_ACC_CATEGORY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_JOURNAL_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_JOURNAL_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_DISTR_REF', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_DEBIT_AMT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_CREDIT_AMT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_BALANCE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_BEGIN_BAL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_TOTAL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_STMT_REPORT_PRINT', 'fieldValue': '', 'helpMessage': '' }
        ];

        this.model = {
            employeeMaster: new EmployeeMaster(0, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null, null, null,null,null,null,null,null,null,null,null, null),
            codeType: "",
            mainCodeType: "",
            allCompanyCode: true,
            rowsFromDB: Array<EmployeeQuickAssignmentModule>()
        };
        this.model["codeType"] = "";
        this.accountNumList = Observable.create((observer: any) => {
            observer.next(this.model["employeeMaster"].accountNum);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

    }

    getEmployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployee.filter((id: any) => {
                return query.test(id.accountNum);
            })
        );
    }

    ngOnInit() {
        this.showTable = false;

        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        this.employeeMasterService.getEmployee().then(data => {
            this.getEmployee = data.result;
        });
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        this.model["employeeMaster"].employeeFirstName = null;
        this.model["employeeMaster"].employeeMiddleName = null;
        this.model["employeeMaster"].employeeLastName = null;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        //console.log('Selected value: ', e.item);
        this.model["employeeMaster"] = cloneDeep(e.item);
        if (this.model["codeType"] != "")
            this.setPage({ offset: 0 });
    }

    checkEmployeeSelect(allCompanyCode: FormControl) {
        if (this.model["employeeMaster"].employeeFirstName == null) {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.model["allCompanyCode"] = true;
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = "Please select an employee";
            }, 100);
        }
        else if (this.model["codeType"] == "") {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.model["allCompanyCode"] = true;
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = "Please select a code type";
            }, 100);
        }
        else
            this.setPage({ offset: 0 });
    }

    getData() {
      this.infoLoaded=true;
    }

    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        //this.showTable = false;

        this.infoLoaded = true;
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        /* this.employeeQuickAssignmentService.getlist(this.page, null, null).subscribe(pagedData2 => {
          this.page = cloneDeep(pagedData.page);
            this.selected = cloneDeep(pagedData2.data);
            //console.log('SELECTED Data Retrieved ', this.selected);
            this.showTable = true;
            this.infoLoaded = false;
        }); */

    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onStartDateChanged(event: IMyDateModel, Indx: number): void {
        this.startDate = event.jsdate;
        this.fromStDate = new Date(0);
        this.fromStDate.setUTCSeconds(event.epoc);

        if ((new Date(this.toEndDate)).valueOf() < event.jsdate.valueOf())
            this.endDateInvalid = true;
        else
            this.endDateInvalid = false;
    }

    onEndDateChanged(event: IMyDateModel, Indx: number): void {
        this.endDate = event.jsdate;
        this.toEndDate = new Date(0);
        this.toEndDate.setUTCSeconds(event.epoc);
        if ((new Date(this.startDate)).valueOf() > event.jsdate.valueOf())
            this.endDateInvalid = true;
        else
            this.endDateInvalid = false;
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({
            employeeMaster: new EmployeeMaster(0, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null, null, null, null, null,null,null,null,null,null,null, null),
            codeType: "",
            mainCodeType: "",
            allCompanyCode: true,
            rowsFromDB: Array<EmployeeQuickAssignmentModule>()
        });
        //this.rowsFromDB = [];
        this.showTable = false;
        this.infoLoaded = false;
    }

    //edit department by row
    edit(row) {
        //WHOLE CHANGES
        this.selectedRowId = row.id;
        this.model = cloneDeep(row);
        this.isUnderUpdate = true;
        this.accountNumvalue = this.model["employeeMaster"].accountNum;
        this.setPage({ offset: 0 });
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    Print(f: NgForm) {
        this.closeModal();
        //this.model["employeeMaster"].accountNum = this.accountNumvalue;

        this.infoLoaded = true;
        this.selected.forEach(item => {
            item.masterId = null;
            item.accountNum = null;
        });
        //Call service api for Creating new Accrual
        this.employeeQuickAssignmentService.createQuickAssignment(this.model["employeeMaster"].employeeIndexId, parseInt(this.model["codeType"]), !this.model["allCompanyCode"], this.selected).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                //this.setPage();
                this.Clear(f);

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    // search department by keyword
    /* updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage();
        this.table.offset = 0;
        // this.accrualSetupService.searchAccruallist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    } */

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    closeModal() {
        this.isPrintAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model["employeeMaster"].employeeIndexId = 0;
            return false;
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0 });
    }

    _keyPress(event: any) {
        const pattern = /[0-9]/;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }

    private summaryForDistRef(cells: string[]) {    
        return this.defaultFormValues[13]['fieldValue'];
      }

      private summaryForDebit(cells: number[]) :number {
        const filteredCells = cells.filter(cell => !!cell);
        return filteredCells.reduce((sum, cell) => sum += cell, 0);
      }

      private summaryForCredit(cells: number[]) : number {
        const filteredCells = cells.filter(cell => !!cell);
        return filteredCells.reduce((sum, cell) => sum += cell, 0);
      }

      private summaryForBalance(cells: number[]) : number{
        const filteredCells = cells.filter(cell => !!cell);
        return filteredCells.reduce((sum, cell) => sum += cell, 0);
      }

}

