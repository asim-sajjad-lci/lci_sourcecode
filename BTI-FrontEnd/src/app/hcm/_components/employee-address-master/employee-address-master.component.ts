import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Constants } from '../../../_sharedresource/Constants';
import { NgForm } from '@angular/forms';
import { EmployeeAddressMasterService } from '../../_services/employee-address-master/employeeaddressmaster.service';
import { LocationService } from '../../_services/location/location.service';
import { EmployeeAddressMaster } from '../../_models/employee-address-master/employeeAddressMaster';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Observable } from 'rxjs/Observable';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
@Component({
    selector: 'app-employee-address-master',
    templateUrl: './employee-address-master.component.html',
    providers: [EmployeeAddressMasterService, LocationService, CommonService]
})
export class EmployeeAddressMasterComponent implements OnInit {

    eValue: string;
    duplicateWarning: boolean;
    hasMsg: boolean;
    showMsg: boolean;
    ddPageSize: number = 5;
    hasMessage: boolean;
    isConfirmationModalOpen: boolean;
    EmployeeAddressIDdisplay: any;
    isDeleteAction: boolean;
    addressIdList: Observable<any>;
    checkclear: number;
    typeaheadLoading: boolean;
    typeaheadNoResults = [];
    page = new Page();
    rows = new Array<EmployeeAddressMaster>();
    temp = new Array<EmployeeAddressMaster>();
    model: EmployeeAddressMaster;
    moduleCode = "M-1011";
    screenCode = "S-1457";
    defaultFormValues = [];
    availableFormValues = [];
    getEmployeeAddressId: any[] = [];
    AddressIdOption = [];
    searchKeyword = '';
    message = { 'type': '', 'text': '' };
    selected = [];
    currentLanguage: any;
    moduleName: any;
    screenName: any;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    messageText: string;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    employeeAddressIndexId = {};
    employeeAddressIndexIdvalue: number;
    addressId: string;
    countries = [];
    states = [];
    cities = [];
    phonePattern = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    // Create the label properties to use in the frontend

    SEARCH_EMPLOYEE_ADDRESS_MASTER : any;
    CITY : any;
    CITY_ABABIC : any;
    COUNTRY : any;
    COUNTRY_ARABIC : any;
    EXT : any;
    ADDRESS_ID : any;
    ADDRESS1 : any;
    ADDRESS2 : any;
    PERSONAL_EMAIL : any;
    BUSINESS_EMAIL : any;
    ADDRESS_ARABIC1 : any;
    ADDRESS_ARABIC2 : any;
    PO_BOX : any;
    POST_CODE : any;
    PERSONAL_PHONE : any;
    BUSINESS_PHONE : any;
    OTHER_PHONE : any;
    LOCATION_LINK_GOOGLE_MAP : any;
    EMPLOYEE_ADDRESS_MASTER_CREATE_LABEL : any;
    EMPLOYEE_ADDRESS_MASTER_SAVE_LABEL : any;
    EMPLOYEE_ADDRESS_MASTER_CLEAR_LABEL : any;
    EMPLOYEE_ADDRESS_MASTER_UPDATE_LABEL : any;
    EMPLOYEE_ADDRESS_MASTER_DELETE_LABEL : any;
    EMPLOYEE_ADDRESS_MASTER_CREATE_FORM_LABEL : any;
    EMPLOYEE_ADDRESS_MASTER_UPDATE_FORM_LABEL : any;
    EMPLOYEE_ADDRESS_MASTER_ACTION : any;
    EMPLOYEE_ADDRESS_MASTER_STATE : any;
    EMPLOYEE_ADDRESS_MASTER_CANCEL_LABEL : any;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private employeeAddressMasterService: EmployeeAddressMasterService,
        private getScreenDetailService: GetScreenDetailService,
        private locationService: LocationService,
        private commonService: CommonService) {

        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        this.defaultFormValues = [
            { 'fieldName': 'SEARCH_EMPLOYEE_ADDRESS_MASTER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CITY_ABABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COUNTRY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COUNTRY_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EXT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS1', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS2', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PERSONAL_EMAIL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUSINESS_EMAIL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS_ARABIC1', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS_ARABIC2', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PO_BOX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POST_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PERSONAL_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUSINESS_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'OTHER_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_LINK_GOOGLE_MAP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ADDRESS_MASTER_STATE', 'fieldValue': '', 'helpMessage': '' },
        ];

        this.SEARCH_EMPLOYEE_ADDRESS_MASTER = this.defaultFormValues[0];
        this.CITY = this.defaultFormValues[1];
        this.CITY_ABABIC = this.defaultFormValues[2];
        this.COUNTRY = this.defaultFormValues[3];
        this.COUNTRY_ARABIC = this.defaultFormValues[4];
        this.EXT = this.defaultFormValues[5];
        this.ADDRESS_ID = this.defaultFormValues[6];
        this.ADDRESS1 = this.defaultFormValues[7];
        this.ADDRESS2 = this.defaultFormValues[8];
        this.PERSONAL_EMAIL = this.defaultFormValues[9];
        this.BUSINESS_EMAIL = this.defaultFormValues[10];
        this.ADDRESS_ARABIC1 = this.defaultFormValues[11];
        this.ADDRESS_ARABIC2 = this.defaultFormValues[12];
        this.PO_BOX = this.defaultFormValues[13];
        this.POST_CODE = this.defaultFormValues[14];
        this.PERSONAL_PHONE = this.defaultFormValues[15];
        this.BUSINESS_PHONE = this.defaultFormValues[16];
        this.OTHER_PHONE = this.defaultFormValues[17];
        this.LOCATION_LINK_GOOGLE_MAP = this.defaultFormValues[18];
        this.EMPLOYEE_ADDRESS_MASTER_CREATE_LABEL = this.defaultFormValues[19];
        this.EMPLOYEE_ADDRESS_MASTER_SAVE_LABEL = this.defaultFormValues[20];
        this.EMPLOYEE_ADDRESS_MASTER_CLEAR_LABEL = this.defaultFormValues[21];
        this.EMPLOYEE_ADDRESS_MASTER_CANCEL_LABEL = this.defaultFormValues[22];
        this.EMPLOYEE_ADDRESS_MASTER_UPDATE_LABEL = this.defaultFormValues[23];
        this.EMPLOYEE_ADDRESS_MASTER_DELETE_LABEL = this.defaultFormValues[24];
        this.EMPLOYEE_ADDRESS_MASTER_CREATE_FORM_LABEL = this.defaultFormValues[25];
        this.EMPLOYEE_ADDRESS_MASTER_UPDATE_FORM_LABEL = this.defaultFormValues[26];
        this.EMPLOYEE_ADDRESS_MASTER_ACTION = this.defaultFormValues[27];
        this.EMPLOYEE_ADDRESS_MASTER_STATE = this.defaultFormValues[28];

        this.addressIdList = Observable.create((observer: any) => {
            observer.next(this.model.addressId);
        }).mergeMap((token: string) => this.getEmployeeAddressIdAsObservable(token));

    }
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.locationService.getCountry().then(data => {
            this.countries = data.result;
            this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
        });
        // getting screen labels, help messages and validation messages
        
        this.employeeAddressMasterService.getAddressList().then(data => {
            this.getEmployeeAddressId = data.result;
            console.log('test', this.AddressIdOption);
        });

    }

    getEmployeeAddressIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployeeAddressId.filter((id: any) => {
                return query.test(id.addressId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        // this.model.employeeFirstName = '';
    }

    typeaheadOnSelect(e: any, input: any): void {
        console.log('Selected value: ', e);
        if (input == 'input') {
            this.eValue = e.target.value;
            // this.CheckDuplicateAddressId();
        } else {
            this.eValue = e.value;
        }

    }

    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.employeeAddressMasterService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for Employee Address Master
    Create() {
        this.EmployeeAddressIDdisplay = "";
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            employeeAddressIndexId: 0,
            addressId: '',
            address1: '',
            address2: '',
            personalEmail: '',
            businessEmail: '',
            addressArabic1: '',
            addressArabic2: '',
            pOBox: '',
            postCode: '',
            personalPhone: '',
            businessPhone: '',
            otherPhone: '',
            locationLinkGoogleMap: '',
            city: '',
            cityArabic: '',
            country: '',
            countryArabic: '',
            ext: '',
            countryId: null,
            stateId: null,
            cityId: null,
            cityName: '',
            countryName: '',
            stateName: ''
        };
        this.locationService.getCountry().then(data => {
            this.countries = data.result;
        });
        this.countries.forEach(ele => {
            if (ele.defaultCountry === 1) {
                this.model.countryId = ele.countryId;
            }
        })
        this.locationService.getStatesByCountryId(this.model.countryId).then(data => {
            this.states = data.result;
            this.model.stateId = this.states[0].stateId;

            this.locationService.getCitiesByStateId(this.model.stateId).then(data => {
                this.cities = data.result;
                this.model.cityId = this.cities[0].cityId;
            });
        });
    }

    ClearValue() {
        this.checkclear = 1;
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        this.checkclear = 0;
        f.resetForm({ country: 2, state: 3, city: 6 });
    }

    // function call for creating new employee address
    CreateEmployeeAddressMaster(f: NgForm, event: Event) {

        event.preventDefault();
        for (let i = 0; i < this.getEmployeeAddressId.length; i++) {
            if (this.getEmployeeAddressId[i].addressId === this.eValue) {
                this.hasMsg = true;
                // window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = 'Address Id already exist.';
                return;
            } else {
                this.messageText = '';
            }
        }
        var employeeAddressIndexId = this.model.employeeAddressIndexId;

        // Check if the id is available in the model.
        // If id avalable then update the employee address, else Add new employee address.
        if (this.model.employeeAddressIndexId > 0 && this.model.employeeAddressIndexId != 0 && this.model.employeeAddressIndexId != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Call service api for Creating new employee address
            this.employeeAddressMasterService.createEmploeeAddressMaster(this.model).then(data => {
                var datacode = data.code;
                if (datacode === 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.showCreateForm = false;
                        this.messageText = data.btiMessage.message;
                        ;
                    }, 100);

                    this.hasMsg = true;
                    f.resetForm();
                    this.EmployeeAddressIDdisplay = '';
                    // Refresh the Grid data after adding new employee master
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    }

    // edit department by row
    edit(row: EmployeeAddressMaster) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.employeeAddressIndexId = row.employeeAddressIndexId;
        this.isUnderUpdate = true;
        this.employeeAddressIndexIdvalue = this.model.employeeAddressIndexId;
        this.addressId = this.model.addressId;

        this.locationService.getStatesByCountryId(row.countryId).then(data => {
            this.states = data.result;
        });
        this.locationService.getCitiesByStateId(row.stateId).then(data => {
            this.cities = data.result;
        });
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    changeTypeaheadNoResults(e: boolean, ErrorType): void {
        if (ErrorType === 'addressId') {
            const index: number = this.typeaheadNoResults.indexOf(ErrorType);
            if (index === -1 && e) {
                this.typeaheadNoResults.push(ErrorType);
            } else if (index !== -1 && !e) {
                this.typeaheadNoResults.splice(index, 1);
            }
        }
    }

    updateStatus() {
        this.closeModal();
        this.model.employeeAddressIndexId = this.employeeAddressIndexIdvalue;

        // Call service api for Creating new Accrual
        this.employeeAddressMasterService.updateEmploeeAddressMaster(this.model).then(data => {
            var datacode = data.code;
            if (datacode === 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;

            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
                this.isSuccessMsg = false;
                this.isfailureMsg = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    // delete department by passing whole object of perticular Department
    delete() {
        var selectedEmployeeAddress = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedEmployeeAddress.push(this.selected[i].employeeAddressIndexId);
        }
        this.employeeAddressMasterService.deleteEmploeeAddressMaster(selectedEmployeeAddress).then(data => {
            var datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                    this.isSuccessMsg = false;
                    this.isfailureMsg = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 4000);

            // Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search employee address by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn === val) {
            if (this.page.sortBy === 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    onCountrySelect(event) {
        this.model.stateId = 0;
        this.model.cityId = 0;
        this.locationService.getStatesByCountryId(event.target.value).then(data => {
            this.states = data.result;
        });
        this.cities = [];
    }

    onStateSelect(event) {
        this.model.cityId = 0;
        this.locationService.getCitiesByStateId(event.target.value).then(data => {
            this.cities = data.result;
        });
    }

}
