import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanESBSetupComponent } from './loan-esbsetup.component';

describe('LoanESBSetupComponent', () => {
  let component: LoanESBSetupComponent;
  let fixture: ComponentFixture<LoanESBSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanESBSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanESBSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
