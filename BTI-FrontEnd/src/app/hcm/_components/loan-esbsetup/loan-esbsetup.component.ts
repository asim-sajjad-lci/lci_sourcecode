import { Component, OnInit } from '@angular/core';
import { LoanESBSetupService } from '../../_services/loan-esbsetup/loan-esbsetup.service';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { LoanEsbSetupModel } from '../../_models/loan-esbsetup/loan-esbsetup';

@Component({
  selector: 'app-loan-esbsetup',
  templateUrl: './loan-esbsetup.component.html',
  styleUrls: ['./loan-esbsetup.component.css'],
  providers: [LoanESBSetupService, CommonService]
})
export class LoanESBSetupComponent implements OnInit {

  typeSettings = {
    singleSelection: false,
    enableCheckAll: true,
    text: "Select Value",
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    // classes: "myclass custom-class",
    searchPlaceholderText: "Search Pay Code",
    badgeShowLimit: 3,
    // disabled: true,
    showCheckbox: true,
    isLazyLoad: true,
    maxHeight: '1000px',
    stopScrollPropagation: true,
  }
  typedata=[];
  type;
  typeId = 3;
  model: LoanEsbSetupModel;


  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  hasMessage;

  constructor(
    private Service: LoanESBSetupService,
  ) { }

  ngOnInit() {
    this.getTypeData();
    this.getData();

    this.model ={
      typeId: this.typeId,
      dtoMultiSelectList: []
    }
  }

  getTypeData(){
    this.Service.getTypeFieldDetails(this.typeId).then(data => {
     if(data.code == 201)
     {
         let val = data.result.records;
         for(let i=0;i<val.length;i++)
         {
             this.typedata.push({id:val[i].id,itemName:val[i].desc})
         }
     }
     else{
        this.typedata = [];
     }
    });
}

getData(){
  this.Service.getTypeData(this.typeId).then(data => {
    if(data.code == 201) 
    {
      for(let i=0;i<data.result.records.dtoMultiSelectList.length;i++)
      {
       this.model.dtoMultiSelectList.push({"id":data.result.records.dtoMultiSelectList[i].id,"itemName":data.result.records.dtoMultiSelectList[i].itemName})
      }
     // this.model.dtoMultiSelectList = data.result.dtoMultiSelectList.id
    }
    else
    {
     this.model.dtoMultiSelectList = [];
    }
  });
}

Save() {
  debugger;
  console.log(this.model)
  this.Service.saveLoanPackageSetup(this.model).then(data => {
    var datacode = data.code;
    if (datacode == 201) {
      window.scrollTo(0, 0);
      window.setTimeout(() => {
        this.isSuccessMsg = true;
        this.isfailureMsg = false;
        this.showMsg = true;
        this.hasMessage = false;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
        this.messageText = "Record Save Successfully";
      }, 100);

      this.hasMsg = true;
    }
  }).catch(error => {
    this.hasMsg = true;
    window.setTimeout(() => {
      this.isSuccessMsg = false;
      this.isfailureMsg = true;
      this.showMsg = true;
      this.messageText = "Server error. Please contact admin.";
    }, 100)
  });

}

}
