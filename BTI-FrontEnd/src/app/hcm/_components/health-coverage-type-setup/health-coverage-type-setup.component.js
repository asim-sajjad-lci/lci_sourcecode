"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var router_1 = require("@angular/router");
var health_coverage_type_setup_service_1 = require("../../_services/health-coverage-type-setup/health-coverage-type-setup.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var HealthCoverageTypeSetupComponent = (function () {
    function HealthCoverageTypeSetupComponent(router, HealthCoverageTypeSetupService, getScreenDetailService, alertService) {
        this.router = router;
        this.HealthCoverageTypeSetupService = HealthCoverageTypeSetupService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1409';
        this.message = { 'type': '', 'text': '' };
        this.helthCoverageId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CONTACT_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ADDRESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_COUNTRY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_FAX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_HEALTH_COVERAGE_ID', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    HealthCoverageTypeSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    HealthCoverageTypeSetupComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.HealthCoverageTypeSetupService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create location
    HealthCoverageTypeSetupComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            helthCoverageId: 0,
            desc: '',
            arbicDesc: ''
        };
    };
    // Clear form to reset to default blank
    HealthCoverageTypeSetupComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new location
    HealthCoverageTypeSetupComponent.prototype.CreateHealthCoverageTypeSetup = function (f, event) {
        var _this = this;
        event.preventDefault();
        var locIdx = this.model.helthCoverageId;
        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.HealthCoverageTypeSetupService.checkDuplicateHealthCoverageTypeSetupId(locIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message + ' !';
                    }, 100);
                }
                else {
                    //Call service api for Creating new location
                    _this.HealthCoverageTypeSetupService.createHealthCoverageTypeSetup(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new HealthCoverageTypeSetup
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    //edit department by row
    HealthCoverageTypeSetupComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.helthCoverageId = row.helthCoverageId;
        this.isUnderUpdate = true;
        this.helthCoverageIdvalue = this.model.helthCoverageId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    HealthCoverageTypeSetupComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        this.model.helthCoverageId = this.helthCoverageIdvalue;
        //Call service api for updating selected department
        /*var locIdx = this.model.helthCoverageId;
        this.locationService.checkDuplicatehelthCoverageId(locIdx).then(response => {
            if (response && response.code == 302 && response.result && response.result.isRepeat) {
                this.duplicateWarning = true;
                this.message.type = 'success';

                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = response.btiMessage.message;
                }, 100);
            } else {*/
        //Call service api for Creating new location
        this.HealthCoverageTypeSetupService.updateHealthCoverageTypeSetup(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
        /*}*/
        /*}).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });*/
    };
    HealthCoverageTypeSetupComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    HealthCoverageTypeSetupComponent.prototype.delete = function () {
        var _this = this;
        var selectedHealthCoverageTypeSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            console.log(this.selected[i]);
            selectedHealthCoverageTypeSetups.push(this.selected[i].id);
        }
        this.HealthCoverageTypeSetupService.deleteHealthCoverageTypeSetup(selectedHealthCoverageTypeSetups).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    HealthCoverageTypeSetupComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    HealthCoverageTypeSetupComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.HealthCoverageTypeSetupService.searchlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    HealthCoverageTypeSetupComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    HealthCoverageTypeSetupComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    };
    HealthCoverageTypeSetupComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    HealthCoverageTypeSetupComponent.prototype.CheckNumber = function (event) {
        if (isNaN(event.target.value) == true) {
            this.model.helthCoverageId = 0;
            return false;
        }
    };
    return HealthCoverageTypeSetupComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], HealthCoverageTypeSetupComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], HealthCoverageTypeSetupComponent.prototype, "myScrollContainer", void 0);
HealthCoverageTypeSetupComponent = __decorate([
    core_1.Component({
        selector: 'health-coverage-type-setup',
        templateUrl: './health-coverage-type-setup.component.html',
        providers: [health_coverage_type_setup_service_1.HealthCoverageTypeSetupService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        health_coverage_type_setup_service_1.HealthCoverageTypeSetupService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], HealthCoverageTypeSetupComponent);
exports.HealthCoverageTypeSetupComponent = HealthCoverageTypeSetupComponent;
//# sourceMappingURL=health-coverage-type-setup.component.js.map