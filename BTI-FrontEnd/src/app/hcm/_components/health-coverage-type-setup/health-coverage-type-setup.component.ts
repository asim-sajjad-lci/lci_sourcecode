import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { HealthCoverageTypeSetup } from '../../_models/health-coverage-type-setup/health-coverage-type-setup.module';
import { HealthCoverageTypeSetupService } from '../../_services/health-coverage-type-setup/health-coverage-type-setup.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'health-coverage-type-setup',
    templateUrl: './health-coverage-type-setup.component.html',
    providers: [HealthCoverageTypeSetupService,CommonService]
})
export class HealthCoverageTypeSetupComponent {
    page = new Page();
    rows = new Array<HealthCoverageTypeSetup>();
    temp = new Array<HealthCoverageTypeSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1409';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    helthCoverageId: string;
    searchKeyword = '';
    ddPageSize: number = 5;
    model: HealthCoverageTypeSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    helthCoverageIdvalue: string;
    selected1: string;
    positionClassOptions = [];
    healthcoverageIdList: Observable<any>;
    getHealthCoverageId: any[] = [];
    typeaheadLoading: boolean;
    // LABEL VERIABLE
    HEALTH_COVERAGE_TYPE_SETUP_SEARCH: any
    HEALTH_COVERAGE_TYPE_SETUP_ID: any
    HEALTH_COVERAGE_TYPE_SETUP_DESCRIPTION: any
    HEALTH_COVERAGE_TYPE_SETUP_ARABIC_DESCRIPTION: any
    HEALTH_COVERAGE_TYPE_SETUP_HEALTH_COVERAGE_TYPE_ID: any
    HEALTH_COVERAGE_TYPE_SETUP_ACTION: any
    HEALTH_COVERAGE_TYPE_SETUP_CREATE_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_SAVE_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_CLEAR_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_CANCEL_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_UPDATE_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_DELETE_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_CREATE_FORM_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_UPDATE_FORM_LABEL: any
    HEALTH_COVERAGE_TYPE_SETUP_HEALTH_COVERAGE_ID: any
    HEALTH_COVERAGE_TYPE_SETUP_TYPEID: any
    HEALTH_COVERAGE_TYPE_SETUP_TABLEVIEW: any
    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private HealthCoverageTypeSetupService: HealthCoverageTypeSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_HEALTH_COVERAGE_TYPE_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_HEALTH_COVERAGE_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_TYPEID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTH_COVERAGE_TYPE_SETUP_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];
        this.HEALTH_COVERAGE_TYPE_SETUP_SEARCH = this.defaultFormValues[0];
        this.HEALTH_COVERAGE_TYPE_SETUP_ID = this.defaultFormValues[1];
        this.HEALTH_COVERAGE_TYPE_SETUP_DESCRIPTION = this.defaultFormValues[2];
        this.HEALTH_COVERAGE_TYPE_SETUP_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.HEALTH_COVERAGE_TYPE_SETUP_HEALTH_COVERAGE_TYPE_ID = this.defaultFormValues[4];
        this.HEALTH_COVERAGE_TYPE_SETUP_ACTION = this.defaultFormValues[5];
        this.HEALTH_COVERAGE_TYPE_SETUP_CREATE_LABEL = this.defaultFormValues[6];
        this.HEALTH_COVERAGE_TYPE_SETUP_SAVE_LABEL = this.defaultFormValues[7];
        this.HEALTH_COVERAGE_TYPE_SETUP_CLEAR_LABEL = this.defaultFormValues[8];
        this.HEALTH_COVERAGE_TYPE_SETUP_CANCEL_LABEL = this.defaultFormValues[9];
        this.HEALTH_COVERAGE_TYPE_SETUP_UPDATE_LABEL = this.defaultFormValues[10];
        this.HEALTH_COVERAGE_TYPE_SETUP_DELETE_LABEL = this.defaultFormValues[11];
        this.HEALTH_COVERAGE_TYPE_SETUP_CREATE_FORM_LABEL = this.defaultFormValues[12];
        this.HEALTH_COVERAGE_TYPE_SETUP_UPDATE_FORM_LABEL = this.defaultFormValues[13];
        this.HEALTH_COVERAGE_TYPE_SETUP_HEALTH_COVERAGE_ID = this.defaultFormValues[14];
        this.HEALTH_COVERAGE_TYPE_SETUP_TYPEID = this.defaultFormValues[15];
        this.HEALTH_COVERAGE_TYPE_SETUP_TABLEVIEW = this.defaultFormValues[16];
        this.healthcoverageIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.helthCoverageId);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        /// autocomplete
        this.HealthCoverageTypeSetupService.getPositionClassList().then(data => {
            console.log(data.result);
            this.positionClassOptions = data.result;
        });
       
    }

    getSuperviserIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getHealthCoverageId.filter((id: any) => {
                return query.test(id.helthCoverageId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        // console.log('Selected value: ', e);
        this.HealthCoverageTypeSetupService.getHealthCoverageTypeSetup(e.item.id).then(pagedData => {
        //    console.log('PageData', pagedData)
           this.model = pagedData.result;
           this.model.id = 0;
        });

    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.HealthCoverageTypeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
        this.HealthCoverageTypeSetupService.getHealthId().then(data => {
            this.getHealthCoverageId = data.result.records;
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            helthCoverageId: '',
            desc: '',
            arbicDesc: ''
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }


    //function call for creating new location
    CreateHealthCoverageTypeSetup(f: NgForm, event: Event) {
        event.preventDefault();
        var locIdx = this.model.helthCoverageId;

        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.HealthCoverageTypeSetupService.checkDuplicateHealthCoverageTypeSetupId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new location
                    this.HealthCoverageTypeSetupService.createHealthCoverageTypeSetup(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new HealthCoverageTypeSetup
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: HealthCoverageTypeSetup) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.helthCoverageId = row.helthCoverageId;
        this.isUnderUpdate = true;
        this.helthCoverageIdvalue = this.model.helthCoverageId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        this.model.helthCoverageId = this.helthCoverageIdvalue;

        //Call service api for Creating new location
        this.HealthCoverageTypeSetupService.updateHealthCoverageTypeSetup(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
        /*}*/

        /*}).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });*/

    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedHealthCoverageTypeSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            console.log(this.selected[i]);
            selectedHealthCoverageTypeSetups.push(this.selected[i].id);
        }
        this.HealthCoverageTypeSetupService.deleteHealthCoverageTypeSetup(selectedHealthCoverageTypeSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.HealthCoverageTypeSetupService.searchlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.helthCoverageId = '';
            return false;
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}
