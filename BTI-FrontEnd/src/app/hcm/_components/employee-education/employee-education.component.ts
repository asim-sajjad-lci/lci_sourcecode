import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { EmployeeEducationModule } from '../../_models/employee-education/employee-education.module';
import { EmployeeEducationService } from '../../_services/employee-education/employee-education.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import * as cloneDeep from 'lodash/cloneDeep';
import { Observable } from 'rxjs/Observable';
import { EmployeeMaster } from '../../_models/employee-master/employee-master.module';
import { EmployeeMasterService } from '../../_services/employee-master/employee-master.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'employeeEducation',
    templateUrl: './employee-education.component.html',
    providers: [EmployeeEducationService, EmployeeMasterService, CommonService]
})
export class EmployeeEducationComponent {
    page = new Page();
    rows = new Array<EmployeeEducationModule>();
    selectedFirst = [];
    model1: EmployeeEducationModule;
    rows2 = new Array<EmployeeEducationModule>();
    selectedSecond = [];
    ids = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1449';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    Warning;
    message = { 'type': '', 'text': '' };
    searchKeyword = '';
    ddPageSize: number = 5;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    isUnderCreate: boolean = false;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    employeeIdvalue: string;
    selectedRowId: number = 0;
    addSubmitted: boolean = false;
    addSubmittedOnce: boolean = false;
    minYear = 1930;
    maxYear = (new Date()).getFullYear() + 10;
    years: Array<number>;
    gpaValid: boolean = true;
    getEmployee: any[] = [];
    employeeIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder: boolean = false;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    EMPLOYEE_EDUCATION_EMPLOYEE_ID: any;
    EMPLOYEE_EDUCATION_SCHOOL_NAME: any;
    EMPLOYEE_EDUCATION_MAJOR: any;
    EMPLOYEE_EDUCATION_START_YEAR: any;
    EMPLOYEE_EDUCATION_END_YEAR: any;
    EMPLOYEE_EDUCATION_DEGREE: any;
    EMPLOYEE_EDUCATION_GPA: any;
    EMPLOYEE_EDUCATION_COMMENTS: any;
    EMPLOYEE_EDUCATION_DELETE_LABEL: any;
    EMPLOYEE_EDUCATION_SAVE_LABEL: any;
    EMPLOYEE_EDUCATION_CLEAR_LABEL: any;
    EMPLOYEE_EDUCATION_CREATE_LABEL: any;
    EMPLOYEE_EDUCATION_UPDATE_LABEL: any;
    EMPLOYEE_EDUCATION_ACTION: any;
    EMPLOYEE_EDUCATION_CANCEL_LABEL: any;
    EMPLOYEE_EDUCATION_SEARCH: any;
    EMPLOYEE_EDUCATION_CREATE_FORM_LABEL: any;
    EMPLOYEE_EDUCATION_UPDATE_FORM_LABEL: any;
    EMPLOYEE_EDUCATION_PRINT: any;

    constructor(private router: Router,
        private employeeEducationService: EmployeeEducationService,
        private employeeMasterService: EmployeeMasterService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'scheduleId';
        this.page.sortBy = 'DESC';

        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_EDUCATION_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_SCHOOL_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_MAJOR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_START_YEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_END_YEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_DEGREE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_GPA', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_COMMENTS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_EDUCATION_PRINT', 'fieldValue': '', 'helpMessage': '' }
        ];

        this.EMPLOYEE_EDUCATION_EMPLOYEE_ID = this.defaultFormValues[0];
        this.EMPLOYEE_EDUCATION_SCHOOL_NAME = this.defaultFormValues[1];
        this.EMPLOYEE_EDUCATION_MAJOR = this.defaultFormValues[2];
        this.EMPLOYEE_EDUCATION_START_YEAR = this.defaultFormValues[3];
        this.EMPLOYEE_EDUCATION_END_YEAR = this.defaultFormValues[4];
        this.EMPLOYEE_EDUCATION_DEGREE = this.defaultFormValues[5];
        this.EMPLOYEE_EDUCATION_GPA = this.defaultFormValues[6];
        this.EMPLOYEE_EDUCATION_COMMENTS = this.defaultFormValues[7];
        this.EMPLOYEE_EDUCATION_DELETE_LABEL = this.defaultFormValues[8];
        this.EMPLOYEE_EDUCATION_SAVE_LABEL = this.defaultFormValues[9];
        this.EMPLOYEE_EDUCATION_CLEAR_LABEL = this.defaultFormValues[10];
        this.EMPLOYEE_EDUCATION_CREATE_LABEL = this.defaultFormValues[11];
        this.EMPLOYEE_EDUCATION_UPDATE_LABEL = this.defaultFormValues[12];
        this.EMPLOYEE_EDUCATION_ACTION = this.defaultFormValues[13];
        this.EMPLOYEE_EDUCATION_CANCEL_LABEL = this.defaultFormValues[14];
        this.EMPLOYEE_EDUCATION_SEARCH = this.defaultFormValues[15];
        this.EMPLOYEE_EDUCATION_CREATE_FORM_LABEL = this.defaultFormValues[16];
        this.EMPLOYEE_EDUCATION_UPDATE_FORM_LABEL = this.defaultFormValues[17];
        this.EMPLOYEE_EDUCATION_PRINT = this.defaultFormValues[18];
        this.years = new Array<number>();
        for (let i = this.minYear; i <= this.maxYear; i++)
            this.years.push(i);

        this.employeeIdList = Observable.create((observer: any) => {
            observer.next(this.model1.employeeMaster.employeeId);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

    }

    getEmployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployee.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        this.currentLanguage = localStorage.getItem('currentLanguage');

        // getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        this.employeeMasterService.getEmployee().then(data => {
            this.getEmployee = data.result;
        });
    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'employeeId') {
            return row['employeeMaster'] && row['employeeMaster']['employeeId'] ? row['employeeMaster']['employeeId'] : '';
        } else {
            return row[gridFieldName];
        }
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
         this.model1.employeeMaster.employeeFirstName = '';
         this.model1.employeeMaster.employeeMiddleName = '';
         this.model1.employeeMaster.employeeLastName = '';
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        this.model1.employeeMaster = cloneDeep( e.item);
        this.setPage2();
    }

    // Open form for create Accrual
    Create() {
        this.ids = [];
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.isUnderCreate = true;
        setTimeout(() => {
            this.showCreateForm = true;
            this.doNotPrintHearder = false;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model1 = {
            id: 0,
            employeeMaster: new EmployeeMaster(0, null, null, null, null, null, null, null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null, null, null, null, null,null,null,null, null, null, null, null),
            schoolName: '',
            major: '',
            endYear: '',
            startYear: '',
            degree: '',
            gpa: null,
            comments: '',
            listEmployeeEducation: Array<EmployeeEducationModule>()
        };
        this.rows2 = new Array<EmployeeEducationModule>();
        this.addSubmitted = false;
        this.addSubmittedOnce = false;
    }

    setPage(pageInfo) {
        this.selectedFirst = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn === undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy === undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.employeeEducationService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    setPage2() {
        this.ids = [];
        this.selectedSecond = []; // remove any selected checkbox on paging
        this.isUnderCreate = false;
        this.employeeEducationService.getlistByEmployeeID(this.model1.employeeMaster.employeeIndexId).then(data => {
            this.rows2 = data.result.records;
            this.isUnderCreate = true;
        });

    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({endYear: '', startYear: ''});
        this.addSubmitted = false;
        if (!this.addSubmittedOnce) {
            this.model1.employeeMaster = new EmployeeMaster(0, '', null, '', '', '', null, null, null,null, null, null, null, null, null, null, null, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null, null, null, null, null,null,null,null, null, null, null, null);
        }
    }

    formIsClear() {
        if ((this.model1.schoolName != null && this.model1.schoolName !== '') || (this.model1.major != null && this.model1.major !== '') ||
            (this.model1.startYear != null && this.model1.startYear !== '') || (this.model1.comments != null && this.model1.comments !== '') ||
            (this.model1.endYear != null && this.model1.endYear !== '') || this.model1.gpa != null || (this.model1.degree != null && this.model1.degree !== '')) {
            return false;

        } else {
            return true;
        }
    }

    // function call for creating new Accrual
    CreateEmployeeEducation(f: NgForm, event: Event) {
        event.preventDefault();
        var employeeIdx = this.model1.employeeMaster.employeeId;

        // Check if the id is available in the model.
        // If id avalable then update the Accrual, else Add new Accrual.
        if (this.model1.id > 0 && this.model1.id !== 0 && this.model1.id !== undefined) {
            if (f.valid) {
                this.isConfirmationModalOpen = true;
                this.isDeleteAction = false;
            }
        } else if (this.model1.employeeMaster.employeeFirstName === '') {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = 'No employee selected.';
            }, 500);
        } else if (!this.rows2.length) {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = 'Nothing to save.';
            }, 500);
        } else if (this.formIsClear() && this.ids.length !== 0) {

            this.employeeEducationService.deleteEmployeeEducation(this.ids).then(data => {
                var datacode = data.code;
                if (datacode === 200 || datacode === 201) {
                    // Call service api for Creating new Accrual
                    this.employeeEducationService.createEmployeeEducation(this.rows2).then(data => {
                        var datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            this.Clear(f);
                            // Refresh the Grid data after adding new Accrual
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                    this.isUnderCreate = false;
                }

            }).catch(error => {
                this.hasMessage = true;
                this.message.type = 'error';
                var errorCode = error.status;
                this.message.text = 'Server issue. Please contact admin.';
            });
        } else if (this.formIsClear()) {
            // Call service api for Creating new Accrual
            this.employeeEducationService.createEmployeeEducation(this.rows2).then(data => {
                var datacode = data.code;
                if (datacode === 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.showCreateForm = false;
                        this.messageText = data.btiMessage.message;
                        ;
                    }, 100);

                    this.hasMsg = true;
                    this.Clear(f);
                    // Refresh the Grid data after adding new Accrual
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
            this.isUnderCreate = false;
        } else {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = 'Push or clear the current details before submitting.';
            }, 500);
        }

    }

    // edit department by row
    edit(row: EmployeeEducationModule) {

        this.selectedRowId = row.id;
        this.showCreateForm = true;
        this.model1 = cloneDeep(row);
        this.rows2 = [];
        // this.rows2 = row.listEmployeeEducation;
        this.isUnderUpdate = true;
        this.isUnderCreate = false;
        this.doNotPrintHearder = false;
        this.employeeIdvalue = this.model1.employeeMaster.employeeId;
        this.setPage2();
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    addToList(f: NgForm) {
        let temp: EmployeeEducationModule = cloneDeep(this.model1);
        if (this.model1.employeeMaster.employeeFirstName === '') {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = 'Employee does not exist.';
            }, 100);
        } else if (this.rows2.length > 0 && this.rows2 != null) {
            let tmpArr = [];
            tmpArr = this.rows2.filter(item => item.employeeMaster.employeeId === temp.employeeMaster.employeeId && item.schoolName === temp.schoolName && item.endYear === temp.endYear && item.degree === temp.degree);

            if (tmpArr.length > 0) {
                this.Warning = true;
                this.message.type = 'success';

                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.Warning = false;
                    }, 4000);
                    this.message.text = 'Employee Education already exists.';
                }, 100);
            } else {
                let tmpArr1 = [];
                tmpArr1 = this.rows2.filter(item => item.employeeMaster.employeeId === temp.employeeMaster.employeeId && item.degree === temp.degree);

                if (tmpArr1.length > 0) {
                    this.Warning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.Warning = false;
                        }, 4000);
                        this.message.text = 'Degree already exists for current employee.';
                    }, 100);
                } else {
                    this.rows2.push(temp);
                    this.addSubmitted = false;
                    this.addSubmittedOnce = true;
                    this.Clear(f);
                    this.model1.employeeMaster = temp.employeeMaster;
                }
            }
        } else {
            this.rows2 = new Array<EmployeeEducationModule>();
            this.rows2.push(temp);
            this.addSubmitted = false;
            this.addSubmittedOnce = true;
            this.Clear(f);
            this.model1.employeeMaster = temp.employeeMaster;
        }
    }

    removeFromList() {
        let tmpArr = cloneDeep(this.rows2);
        for (let j = this.selectedSecond.length - 1; j > -1; j--) {
            this.ids.push(this.selectedSecond[j].id);
            tmpArr = tmpArr.filter(item => item.employeeMaster.employeeId !== this.selectedSecond[j].employeeMaster.employeeId || item.schoolName !== this.selectedSecond[j].schoolName || item.endYear !== this.selectedSecond[j].endYear || item.degree !== this.selectedSecond[j].degree);
        }
        if (this.selectedSecond.length) {
            this.addSubmitted = false;
            this.addSubmittedOnce = true;
        }
        this.selectedSecond = [];
        this.rows2 = new Array<EmployeeEducationModule>();
        this.rows2 = tmpArr;
    }

    updateStatus() {
        this.closeModal();
        this.doNotPrintHearder = false;
        this.model1.employeeMaster.employeeId = this.employeeIdvalue;
        let temp: EmployeeEducationModule = cloneDeep(this.model1);
        let tmpArr = [];
        if (this.rows2 == null)
            this.rows2 = new Array<EmployeeEducationModule>();
        tmpArr = this.rows2.filter(item => item.id !== temp.id && item.employeeMaster.employeeId === temp.employeeMaster.employeeId && item.schoolName === temp.schoolName && item.endYear === temp.endYear && item.degree === temp.degree);

        if (tmpArr.length > 0) {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = 'Employee Education already exists.';
            }, 100);
        } else {
            let tmpArr1 = [];
            tmpArr1 = this.rows2.filter(item => item.id !== temp.id && item.employeeMaster.employeeId === temp.employeeMaster.employeeId && item.degree === temp.degree);

            if (tmpArr1.length > 0) {
                this.Warning = true;
                this.message.type = 'success';

                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.Warning = false;
                    }, 4000);
                    this.message.text = 'Degree already exists for current employee.';
                }, 100);
            } else {

                this.rows2.push(temp);
                // Call service api for Creating new Accrual
                this.employeeEducationService.updateEmployeeEducation(this.rows2).then(data => {
                    var datacode = data.code;
                    if (datacode === 201) {
                        // Refresh the Grid data after editing department
                        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                        // Scroll to top after editing department
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.messageText = data.btiMessage.message;
                            ;
                            this.showCreateForm = false;
                            this.addSubmitted = false;
                        }, 100);

                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = 'Server error. Please contact admin.';
                    }, 100);
                });
            }
        }
    }

    varifyDelete() {
        if (this.selectedFirst.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            window.scrollTo(0, 0);
        }
    }

    // delete department by passing whole object of perticular Department
    delete() {
        var selectedAccruals = [];
        for (var i = 0; i < this.selectedFirst.length; i++) {
            selectedAccruals.push(this.selectedFirst[i].id);
        }
        this.employeeEducationService.deleteEmployeeEducation(selectedAccruals).then(data => {
            var datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selectedFirst.splice(0, this.selectedFirst.length);
        this.selectedFirst.push(...selected);
    }

    onSelect2({ selected }) {
        this.selectedSecond.splice(0, this.selectedSecond.length);
        this.selectedSecond.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.setPage2();
        this.table.offset = 0;
        // this.accrualSetupService.searchAccruallist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model1.id = 0;
            return false;
        }
    }

    sortColumn(val) {
        if (this.page.sortOn === val) {
            if (this.page.sortBy === 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    sortColumn2(val) {
        this.setPage2();
    }

    _keyPress(event: any) {
        const pattern = /[0-9]/;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }

    checkdecimalgpa(digit) {
        let tempp = digit.split('.');
        if (tempp != null && tempp[1] != null && ((tempp[0].length > 5) || (tempp[1] != null && tempp[1].length > 3))) {
            this.gpaValid = false;
        } else if (tempp != null && ((tempp[1] == null && tempp[0].length > 5) || (tempp[1] != null && tempp[1].length > 3))) {
            this.gpaValid = false;
        } else {
            this.gpaValid = true;
        }

    }

    // Print Screen
    print() {

        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        printContents = printContents.replace(/\r?\n|\r/g, "");
        printContents = printContents.replace(" ", "");
        printContents = printContents.replace(/<!--.*?-->/g, "");
    
        // var postedOnes = this.getElementsByIdStartsWith("print-section", "canvas", "canvasdiv-", printContents);
        // var postedOnes1 = this.getElementsByIdStartsWith("print-section", "canvas", "categorychart-", postedOnes);
        // this.getElementsByIdStartsWith("print-section", "canvas", "innerchart-", postedOnes1);
    
        printContents = document.getElementById('print-section').innerHTML;
    
        popupWin = window.open('', '_blank', 'top=0,left=0,height=400px,width=800px');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
        <link rel="stylesheet" type="text/css" media="screen,print" href="assets/css/bootstrap.min.css">
          <title>Employee Education</title>
          <style>
            .print-tabel{disaplay:block}
          </style>
        </head>
    <body onload="">${printContents}</body>
    <script type="text/javascript">
    window.print();
    if(navigator.userAgent.match(/iPad/i)){
      window.onfocus=function(){window.close(); }
    }else{        
      window.close();
    }
    </script>
      </html>`
        );
        popupWin.document.close();
    }
    printEmployeeDetails() {
        this.doNotPrintHearder = true;
        this.noPrintTable = true;
        this.printDetails = false;
        
        setTimeout(() => { this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint() {
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }

}
