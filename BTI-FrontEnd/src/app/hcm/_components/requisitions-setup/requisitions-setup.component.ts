import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../_sharedresource/page';
import {Constants} from '../../../_sharedresource/Constants';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Router} from '@angular/router';
import {RequisitionsSetup} from '../../_models/requisitions-setup/requisitions-setup.module';
import {RequisitionsSetupService} from '../../_services/requisitions-setup/requisitions-setup.service';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {NgForm} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';   
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
    selector: 'requisitionsSetup',
    templateUrl: './requisitions-setup.component.html',
    providers : [RequisitionsSetupService]
})
export class RequisitionsSetupComponent {
    page = new Page();
    rows = new Array<RequisitionsSetup>();
    temp = new Array<RequisitionsSetup>();
    selected = [];
    countries = [];
    states = [];
    cities = [];
	accrualScheduleList = [];
    linkedPayCodeList = [];
    companyIdsList = [];
    departmentIdsList = [];
    divisionIdsList = [];
    positionIdsList = [];
    supervisorIdsList = [];
    modelState = '';
    modelCompanyId : any;
    modelDepartmentId : any;
    modelDivisionId : any;
    modelPositionId : any;
    modelSupervisorId : any;
	
	departmentDescription : any;
	divisionDescription : any;
	supervisorDescription : any;
	positionDescription : any;
	
	internalPostDate: any;
	internalCloseDate: any;
	openingDate: any;
	modelInternalPostDate: any;
	modelInternalCloseDate: any;
	modelOpeningDate: any;
	startDate: any;
	endDate: any;
	error:any={isError:false,errorMessage:''};
    moduleCode = 'M-1011';
    screenCode = 'S-1429';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    requisitionsSetupId;
    searchKeyword = '';
    ddPageSize: number = 5;
    model: RequisitionsSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    requisitionsSetupIdvalue : string;
	scheduleId : number;
	scheduleIdSelected : number;
    schedulePrimaryId : number;
    payPrimaryId: number;
	frequencyArray = ["Weekly","Biweekly","Semimonthly","Monthly","Quarterly","Semianually","Anually"];
	timeTypeArray = ["Hourly","Salary","Overtime","Benefit","Absent","Other"];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    dataSource: Observable<any>;
    departmentSource: Observable<any>;
    divisionSource: Observable<any>;
    positionSource: Observable<any>;
    supervisorSource: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    modelDivisionDescription: any;

    constructor(private router: Router,
                private requisitionsSetupService: RequisitionsSetupService,
                private getScreenDetailService: GetScreenDetailService,
                private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            {'fieldName': 'REQUISITIONS_SETUP__SEARCH_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP__ACTION_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP__CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP__UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP__CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP_NO', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP_STATUS', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP_INTERNAL_POST_DATE', 'fieldValue': '', 'helpMessage': ''},
			{'fieldName': 'REQUISITIONS_SETUP_INTERNAL_CLOSE_DATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'REQUISITIONS_SETUP_OPENING_DATE', 'fieldValue': '', 'helpMessage': ''},
			{'fieldName': 'REQUISITIONS_SETUP_RECRUITER_NAME', 'fieldValue': '', 'helpMessage': ''},            
			{'fieldName': 'REQUISITIONS_SETUP_JOB_POSTING', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_INTERNAL', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_EXTERNAL', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_ADVERTISING', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_COMPANY_ID', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_DIVISION_ID', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_POSITION_ID', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_SUPERVISIOR_ID', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_MANAGER_NAME', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_POSITIONS_AVAILABLE', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_POSITIONS_FIELD', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_APPLICANTS_APPLIED', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_APPLICANTS_INTERVIEWED', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_COST_ADVERTISING', 'fieldValue': '', 'helpMessage': ''}, 
			{'fieldName': 'REQUISITIONS_SETUP_COST_RECRUITER', 'fieldValue': '', 'helpMessage': ''},  
			{'fieldName': 'REQUISITIONS_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
			{'fieldName': 'REQUISITIONS_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},

        ];
        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.modelCompanyId);
        }).mergeMap((token: string) => this.getCompanyAsObservable(token));
        this.departmentSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.modelDepartmentId);
        }).mergeMap((token: string) => this.getDepartmentAsObservable(token));
        this.divisionSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.modelDivisionId);
        }).mergeMap((token: string) => this.getDivisionAsObservable(token));
		this.positionSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.modelPositionId);
        }).mergeMap((token: string) => this.getPositionAsObservable(token));
		this.supervisorSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.modelSupervisorId);
        }).mergeMap((token: string) => this.getSupervisorAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.requisitionsSetupService.getCountry().then(data => {
            this.countries = data.result;    
           //console.log(data.result);       
        });
		
		this.requisitionsSetupService.getAccrualSchedule().then(data => {
            this.accrualScheduleList = data.result.records;   
        });
		
		this.requisitionsSetupService.getlinkedPayCode().then(data => {
            this.linkedPayCodeList = data.result.records;
        });
		
		
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        //Following shifted from SetPage for better performance
        this.requisitionsSetupService.getAllDepartmentDropDown().then(data => {
            this.departmentIdsList = data.result;                        
        });
        this.requisitionsSetupService.getAllDivisionDropDown().then(data => {
            this.divisionIdsList = data.result;                            
        });
        this.requisitionsSetupService.getAllPositionDropDown().then(data => {
            this.positionIdsList = data.result;
        });
        this.requisitionsSetupService.getAllSupervisorDropDown().then(data => {
			this.supervisorIdsList = data.result;
		});

    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.requisitionsSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;            
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
			requisitionNo:null,
            stats: null,
            internalPostDate: '',
            internalCloseDate: '',
            openingDate: '',
            recruiterName: '',
            companyId: null,
            managerName: '',
			jobPosting: 0,
            advertisingField1 :'',
            advertisingField2 :'',
            advertisingField3 :'',
            advertisingField4 :'',
            positionsAvailable :null,
            positionsFilled :null,
            applicantsApplied :null,
            applicantsInterviewed :null,
            costadvirs :null,
            costrecri :null,
            departmentId :null,
            divisionId :null,
            supervisorId :null,
            positionId: null
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({timeType: '',accrualPeriod: '',payCodeId: 0,scheduleId: 0});
    }


    //function call for creating new location
    CreateRequisitionsSetup(f: NgForm, event: Event) {
        event.preventDefault();
        var locIdx = this.model.requisitionNo;

        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.requisitionsSetupService.checkDuplicateRequisitionsSetupId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new location
                    this.requisitionsSetupService.createRequisitionsSetup(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.hasMessage = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new location
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: RequisitionsSetup) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        /*this.id = row.id;
        this.isUnderUpdate = true;
        this.requisitionsSetupIdvalue = this.model.id;
		this.schedulePrimaryId = this.model.schedulePrimaryId;
		this.model.scheduleId = this.model.schedulePrimaryId;
        
        this.payPrimaryId = this.model.payPrimaryId;
        this.model.payCodeId = this.model.payPrimaryId;

		this.scheduleIdSelected = this.model.scheduleId;
		//this.scheduleId = this.model.scheduleId;
        this.requisitionsSetupService.getStatesByCountryId(1).then(data => {
            this.states = data.result;    
        });
        this.requisitionsSetupService.getCitiesByStateId(1).then(data => {
            this.cities = data.result;    
        });*/
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    updateStatus() {
        this.closeModal();
        //this.model.requisitionsSetupId = this.requisitionsSetupId;
        //Call service api for updating selected department
        
                //Call service api for Creating new location
                this.requisitionsSetupService.updateRequisitionsSetup(this.model).then(data => {
                    var datacode = data.code;
                    if (datacode == 201) {
                        //Refresh the Grid data after editing department
                        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                        //Scroll to top after editing department
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.messageText = data.btiMessage.message;
                            ;
                            this.showCreateForm = false;
                        }, 100);
                        this.hasMessage = false;
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = 'Server error. Please contact admin.';
                    }, 100)
                });
            /*}*/

    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedLocations = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedLocations.push(this.selected[i].id);
        }
        this.requisitionsSetupService.deleteRequisitionsSetup(selectedLocations).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.    ';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    onCountrySelect(event) {
        this.requisitionsSetupService.getStatesByCountryId(event.target.value).then(data => {
            this.states = data.result;    
        });
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.locationService.searchLocationlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    onStateSelect(event) {  
        this.requisitionsSetupService.getCitiesByStateId(event.target.value).then(data => {
            this.cities = data.result;    
        });
    }

	onStartDateChanged(event: IMyDateModel): void {
        this.internalPostDate = event.jsdate;
        this.internalPostDate = event.epoc;
       //console.log('this.internalPostDate', event);
		 if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.internalCloseDate = event.jsdate;
        this.endDate = event.epoc;
      //console.log('this.internalCloseDate', this.internalCloseDate, event);

        if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }
	
    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event){
        if(isNaN(event.target.value) == true ) {
            //this.model.requisitionsSetupId='';
            return false;
        }
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
    getCompanyAsObservable(token: string): Observable<any> {
          token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
          this.companyIdsList.filter((id: any) => {
            return query.test(id.companyId);
          })
        );
      }
      getDepartmentAsObservable(token: string): Observable<any> {
          token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
          this.departmentIdsList.filter((id: any) => {
            return query.test(id.departmentId);
          })
        );
      }
      getDivisionAsObservable(token: string): Observable<any> {
          token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
          this.divisionIdsList.filter((id: any) => {
            return query.test(id.divisionId);
          })
        );
      }	  
	  getPositionAsObservable(token: string): Observable<any> {
          token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
          this.positionIdsList.filter((id: any) => {
            return query.test(id.positionId);
          })
        );
      }
	  getSupervisorAsObservable(token: string): Observable<any> {
          token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
		return Observable.of(
		  this.supervisorIdsList.filter((id: any) => {
			return query.test(id.superVisionCode);
		  })
		);
	  }
    changeTypeaheadLoading(e: boolean): void {
		this.typeaheadLoading = e;
    }
     
    typeaheadOnSelect(e: TypeaheadMatch,typeofSource:any): void {        		
		if(typeofSource == "department") {				
			this.departmentDescription = e.item.departmentDescription;
			this.model.departmentId = e.item.id;
		} else if(typeofSource == "division") {					
			this.divisionDescription = e.item.divisionDescription;
            this.model.divisionId = e.item.id;
		} else if(typeofSource == "supervisor") {
			this.supervisorDescription = e.item.description;
            this.model.supervisorId = e.item.id;
		} else if(typeofSource == "position") {			
			this.positionDescription = e.item.description;
			this.model.positionId = e.item.id;
		}
    }

}
