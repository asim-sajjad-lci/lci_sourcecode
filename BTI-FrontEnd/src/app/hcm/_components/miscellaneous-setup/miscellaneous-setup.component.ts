import { Component, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { MiscellaneousSetup } from '../../_models/miscellaneous-setup/miscellaneous-setup';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Constants } from '../../../_sharedresource/Constants';
import { MiscellaneousSetupService } from '../../_services/miscellaneous-setup/miscellaneous-setup.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

@Component({
  selector: 'app-miscellaneous-setup',
  templateUrl: './miscellaneous-setup.component.html',
  styleUrls: ['./miscellaneous-setup.component.css'],
  providers: [CommonService,MiscellaneousSetupService]
})
export class MiscellaneousSetupComponent implements OnInit {
    moduleCode = "M-1011";
    screenCode = "S-1786";
    defaultFormValues:object[];
  page = new Page();
  rows = new Array<MiscellaneousSetup>();
  showCreateForm: boolean = false;
  model: MiscellaneousSetup;
  codeIdvalue: string;
  codeId = {};
  isUnderUpdate: boolean;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  selected = [];
  searchKeyword = '';

  MISCELLANEOUS_ID: any;
  MISCELLANEOUS_SEARCH: any;
  MISCELLANEOUS_DESCRIPTION: any;
  MISCELLANEOUS_ARABIC_DESCRIPTION: any;
  MISCELLANEOUS_ACTION: any;
  MISCELLANEOUS_CREATE_LABEL: any;
  MISCELLANEOUS_SAVE_LABEL: any;
  MISCELLANEOUS_CLEAR_LABEL: any;
  MISCELLANEOUS_CANCEL_LABEL: any;
  MISCELLANEOUS_UPDATE_LABEL: any;
  MISCELLANEOUS_DELETE_LABEL: any;
  MISCELLANEOUS_CREATE_FORM_LABEL: any;
  MISCELLANEOUS_UPDATE_FORM_LABEL: any;
  MISCELLANEOUS_GREGORIAN_START_DATE: any;
  MISCELLANEOUS_HIJRI_START_DATE: any;
  MISCELLANEOUS_VALUES_LABEL: any;
  MISCELLANEOUS_DIMENSIONS_LABEL: any;

  miscellaneousIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    getMiscellaneous: any[] = [];
    ddPageSize: number = 5;
    isValueDisable = true;
    MiscId;


  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private commonService: CommonService,
    private router: Router,
    private service:MiscellaneousSetupService
    
  ) { 
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'id';
    this.page.sortBy = 'DESC';

    this.defaultFormValues = [
        { 'fieldName': 'MISCELLANEOUS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_ACTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_GREGORIAN_START_DATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_HIJRI_START_DATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_VALUES_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'MISCELLANEOUS_DIMENSIONS_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
        { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
    ];

    this.MISCELLANEOUS_DESCRIPTION = this.defaultFormValues[0];
    this.MISCELLANEOUS_ID = this.defaultFormValues[1];
    this.MISCELLANEOUS_SEARCH = this.defaultFormValues[2];
    this.MISCELLANEOUS_ARABIC_DESCRIPTION = this.defaultFormValues[3];
    this.MISCELLANEOUS_ACTION = this.defaultFormValues[4];
    this.MISCELLANEOUS_CREATE_LABEL = this.defaultFormValues[5];
    this.MISCELLANEOUS_SAVE_LABEL = this.defaultFormValues[6];
    this.MISCELLANEOUS_CLEAR_LABEL = this.defaultFormValues[7];
    this.MISCELLANEOUS_CANCEL_LABEL = this.defaultFormValues[8];
    this.MISCELLANEOUS_UPDATE_LABEL = this.defaultFormValues[9];
    this.MISCELLANEOUS_DELETE_LABEL = this.defaultFormValues[10];
    this.MISCELLANEOUS_CREATE_FORM_LABEL = this.defaultFormValues[11];
    this.MISCELLANEOUS_UPDATE_FORM_LABEL = this.defaultFormValues[12];
    this.MISCELLANEOUS_GREGORIAN_START_DATE = this.defaultFormValues[13];
    this.MISCELLANEOUS_HIJRI_START_DATE = this.defaultFormValues[14];
    this.MISCELLANEOUS_VALUES_LABEL = this.defaultFormValues[15];
    this.MISCELLANEOUS_DIMENSIONS_LABEL = this.defaultFormValues[16];


    this.miscellaneousIdList = Observable.create((observer: any) => {
        // Runs on every search
        observer.next(this.model.codeId);
    }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));
  }

  ngOnInit() {

    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    console.log("default",this.defaultFormValues)
       //Following shifted from SetPage for better performance
       this.service.getAllMiscellaneous().then(data => {
           debugger
        this.getMiscellaneous = data.result;
        console.log("getMiscellaneous",this.getMiscellaneous)
       
    });
   
  }


  getReportPositionsAsObservable(token: string): Observable<any> {
      debugger
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
        this.getMiscellaneous.filter((id: any) => {
            return query.test(id.codeId);
        })
    );
}

typeaheadOnSelect(e: TypeaheadMatch): void {
    // console.log('Selected value: ', e);
    this.service.getMiscellaneous(e.item.id).then(allData => {
        // console.log('allData', allData);
        debugger;
        this.model = allData.result;
        this.model.id = 0;
    });

}

changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
}

      // Open form for create Miscellneous
      Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            codeArabicName: '',
            codeName: '',
            codeId: '',
            dimension:false
        };
    }


    goToValuesList(){
     // var myurl = `${'AccountDetails'}/${row.id}`;
     var myurl = `${'hcm/miscellaneoussetupvalues'}/${this.MiscId}`;
      this.router.navigateByUrl(myurl);
    }

    CreateMiscellaneous(f: NgForm, event: Event) {
      debugger;
      event.preventDefault();
      var miscIdx = this.model.codeId;

      //Check if the id is available in the model.
      //If id avalable then update the Miscellneous, else Add new Miscellneous.
      if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
          this.isConfirmationModalOpen = true;
          this.isDeleteAction = false;
      }
      else {
          debugger
          //Check for duplicate Miscellneous Id according to it create new division
          this.service.checkDuplicateMiscId(miscIdx).then(response => {
            debugger
              if (response && response.code == 302 && response.result && response.result.isRepeat) {
                  this.duplicateWarning = true;
                  this.message.type = "success";

                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      window.setTimeout(() => {
                          this.showMsg = false;
                          this.duplicateWarning = false;
                      }, 4000);
                      this.message.text = response.btiMessage.message;
                  }, 100);
              } else {
                  //Call service api for Creating new miscellaneous
                  this.service.createMiscellaneous(this.model).then(data => {
                    debugger;
                      var datacode = data.code;
                      if (datacode == 201) {
                          window.scrollTo(0, 0);
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              this.hasMessage = false;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this. isValueDisable = false;
                              this.MiscId = data.result.id;
                              this.messageText = data.btiMessage.message;
                          }, 100);

                          this.hasMsg = true;
                        //  f.resetForm();
                          //Refresh the Grid data after adding new Miscellneous
                           this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                      }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = "Server error. Please contact admin.";
                      }, 100)
                  });
             }

          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = "Server error. Please contact admin.";
              }, 100)
          });

      }
  }

  updateStatus() {
      debugger
    this.closeModal();
    //Call service api for updating selected Miscellneous
    this.model.codeId = this.codeIdvalue;
    this.service.updateMiscellaneous(this.model).then(data => {
        debugger
        var datacode = data.code;
        if (datacode == 201) {
            //Refresh the Grid data after editing Miscellneous
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

            //Scroll to top after editing Miscellneous
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = data.btiMessage.message;
                this.showCreateForm = false;
            }, 100);
            this.hasMessage = false;
            this.hasMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
        }
    }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = "Server error. Please contact admin.";
        }, 100)
    });
}

setPage(pageInfo) {

  this.selected = []; // remove any selected checkbox on paging
  this.page.pageNumber = pageInfo.offset;
  if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
  } else {
      this.page.sortOn = pageInfo.sortOn;
  }
  if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
  } else {
      this.page.sortBy = pageInfo.sortBy;
  }

  this.page.searchKeyword = '';
  this.service.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
  debugger;
      this.page = pagedData.page;
      this.rows = pagedData.data;
  });
}


  // search department by keyword 
  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
    // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
    //     this.page = pagedData.page;
    //     this.rows = pagedData.data;
    //     this.table.offset = 0;
    // });
}

varifyDelete() {
    if (this.selected.length > 0) {
        this.showCreateForm = false;
        this.isDeleteAction = true;
        this.isConfirmationModalOpen = true;
    } else {
        this.isSuccessMsg = false;
        this.hasMessage = true;
        this.message.type = 'error';
        this.isfailureMsg = true;
        this.showMsg = true;
        this.message.text = 'Please select at least one record to delete.';
        window.scrollTo(0, 0);
    }
}

  //delete department by passing whole object of perticular Department
  delete() {
      debugger;
    var selectedMiscellaneous = [];
    for (var i = 0; i < this.selected.length; i++) {
        selectedMiscellaneous.push(this.selected[i].id);
    }
    this.service.deleteMiscellaneous(selectedMiscellaneous).then(data => {
        var datacode = data.code;
        if (datacode == 200) {
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        }
        this.hasMessage = true;
        if(datacode == "302"){
            this.message.type = "error";
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
        } else {
            this.isSuccessMsg = true;
            this.message.type = "success";
            this.isfailureMsg = false;
        }
        //this.message.text = data.btiMessage.message;

        window.scrollTo(0, 0);
        window.setTimeout(() => {
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = data.btiMessage.message;
        }, 100);

        //Refresh the Grid data after deletion of department
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    }).catch(error => {
        this.hasMessage = true;
        this.message.type = "error";
        var errorCode = error.status;
        this.message.text = "Server issue. Please contact admin.";
    });
    this.closeModal();
}

closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
}

   // Set default page size
   changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

sortColumn(val) {
    if (this.page.sortOn == val) {
        if (this.page.sortBy == 'DESC') {
            this.page.sortBy = 'ASC';
        } else {
            this.page.sortBy = 'DESC';
        }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

   //edit Miscellaneous by row
   edit(row: MiscellaneousSetup) {
       debugger;
    this.showCreateForm = true;
    this.isValueDisable = false;
    this.MiscId = row.id;
    this.model = Object.assign({}, row);
    this.isUnderUpdate = true;
    this.codeId = row.codeId;
    this.codeIdvalue = this.model.codeId;
    setTimeout(() => {
        window.scrollTo(0, 2000);
    }, 10);
}

 // Clear form to reset to default blank
 Clear(f: NgForm) {
    f.resetForm();
}

}
