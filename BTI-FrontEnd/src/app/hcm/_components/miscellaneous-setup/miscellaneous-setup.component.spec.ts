import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscellaneousSetupComponent } from './miscellaneous-setup.component';

describe('MiscellaneousSetupComponent', () => {
  let component: MiscellaneousSetupComponent;
  let fixture: ComponentFixture<MiscellaneousSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscellaneousSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscellaneousSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
