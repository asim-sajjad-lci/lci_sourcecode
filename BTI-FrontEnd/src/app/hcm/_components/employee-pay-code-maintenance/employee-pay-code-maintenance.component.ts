import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { OnlyDecimalDirective } from '../../../_sharedresource/onlyDecimal.directive';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { EmployeePayCodeMaintenanceService } from '../../_services/employee-pay-code-maintenance/employee-pay-code-maintenance.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { EmployeePayCodeMaintenanceModule } from '../../_models/employee-pay-code-maintenance/employee-pay-code-maintenance.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
  selector: 'app-employee-pay-code-maintenance',
  templateUrl: './employee-pay-code-maintenance.component.html',
  styleUrls: ['./employee-pay-code-maintenance.component.css'],
  providers: [EmployeePayCodeMaintenanceService, CommonService]
})
export class EmployeePayCodeMaintenanceComponent implements OnInit {
  checkEmpValidId: boolean = true;
  checkPayCodeValidId: boolean = true;
  payRateValue: any;
  payFactorValue: any;
  tempf: any;
  decimalValue: boolean = true;
  page = new Page();
  rows = new Array<EmployeePayCodeMaintenanceModule>();
  temp = new Array<EmployeePayCodeMaintenanceModule>();
  selected = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1480';
  defaultFormValues: Array<any> = [];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  searchKeyword = '';
  ddPageSize: number = 5;
  model: EmployeePayCodeMaintenanceModule;
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  decimalpattern = '/^(((0|[1-9]\d{0,2})(\.\d{2})?)|())$/';
  pf: string[] = [];
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  empDiductionIdvalue: {};
  error: any = { isError: false, errorMessage: '' };
  methodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
  frequencyArray = ["Hourly", "Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually", "Daily/Miscellaneous"];
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;
  @ViewChild('dp2') input: ElementRef;
  payCodeIdList: Observable<any>;
  employeeIDList: Observable<any>;
  payCodeTypeIdList: Observable<any>;
  basedOnPayCodeIdList: Observable<any>;
  getPayCodeId: any = [];
  getEmployeeCodeID: any = [];
  getPayCodeTypeId: any = [];
  getBasedOnPayCodeId: any = [];
  getBasedOnPayCodeRecords: any = [];
  modelPayCodeId: any;
  modelemployeeId: any;
  modelPayCodeTypeId: any;
  modelBasedOnPayCodeId: any;
  typeaheadLoading: boolean;
  employeeFirstName: any;
  payCodeDesc: any;
  payCodeArabicDesc: any;
  employeeIdValue: any;
  payCodeValue: any;
  setBasePayCode: boolean = false;
  isPaycodeFormdisabled: boolean = false;
  islifeTimeValidEmployerpay: boolean = false;
  isPayRateReadOnly: boolean = false;

  PAY_CODE_CREATE_LABEL: any;
  PAY_CODE_SEARCH_LABEL: any;
  PAY_CODE_EMPLOYEE_ID: any;
  PAY_CODE_INACTIVE: any;
  PAY_CODE_EMPLOYEE_NAME: any;
  PAY_CODE_ID: any;
  PAY_CODE_DESCRIPTION: any;
  PAY_CODE_ARABIC_DESCRIPTION: any;
  PAY_CODE_PAY_TYPE: any;
  PAY_CODE_BASE_ON_PAY_CODE: any;
  PAY_CODE_AMOUNT: any;
  PAY_CODE_PAY_FACTOR: any;
  PAY_CODE_PAY_RATE: any;
  PAY_CODE_UNIT_OF_PAY: any;
  PAY_CODE_PAY_PERIOD: any;
  PAY_CODE_UPDATE_LABEL: any;
  PAY_CODE_SAVE_LABEL: any;
  PAY_CODE_CLEAR_LABEL: any;
  PAY_CODE_CANCEL_LABEL: any;
  PAY_CODE_CREATE_FORM_LABEL: any;
  PAY_CODE_UPDATE_FORM_LABEL: any;
  PAY_CODE_DELETE_LABEL: any;
  PAY_CODE_ACTION: any;

  typeId = 1;
  typeData= [];

  constructor(private router: Router,
    private employeePayCodeMaintenanceService: EmployeePayCodeMaintenanceService,
    private getScreenDetailService: GetScreenDetailService,
    private alertService: AlertService, private commonService: CommonService) {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'id';
    this.page.sortBy = 'DESC';

    // default form parameter for department  screen
    this.defaultFormValues = [
      { 'fieldName': 'PAY_CODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_SEARCH_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_EMPLOYEE_NAME', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_ID', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_PAY_TYPE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_BASE_ON_PAY_CODE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_PAY_FACTOR', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_PAY_RATE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_UNIT_OF_PAY', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_PAY_PERIOD', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'PAY_CODE_ACTION', 'fieldValue': '', 'helpMessage': '' },
    ];

    this.PAY_CODE_CREATE_LABEL = this.defaultFormValues[0];
    this.PAY_CODE_SEARCH_LABEL = this.defaultFormValues[1];
    this.PAY_CODE_EMPLOYEE_ID = this.defaultFormValues[2];
    this.PAY_CODE_INACTIVE = this.defaultFormValues[3];
    this.PAY_CODE_EMPLOYEE_NAME = this.defaultFormValues[4];
    this.PAY_CODE_ID = this.defaultFormValues[5];
    this.PAY_CODE_DESCRIPTION = this.defaultFormValues[6];
    this.PAY_CODE_ARABIC_DESCRIPTION = this.defaultFormValues[7];
    this.PAY_CODE_PAY_TYPE = this.defaultFormValues[8];
    this.PAY_CODE_BASE_ON_PAY_CODE = this.defaultFormValues[9];
    this.PAY_CODE_AMOUNT = this.defaultFormValues[10];
    this.PAY_CODE_PAY_FACTOR = this.defaultFormValues[11];
    this.PAY_CODE_PAY_RATE = this.defaultFormValues[12];
    this.PAY_CODE_UNIT_OF_PAY = this.defaultFormValues[13];
    this.PAY_CODE_PAY_PERIOD = this.defaultFormValues[14];
    this.PAY_CODE_UPDATE_LABEL = this.defaultFormValues[15];
    this.PAY_CODE_SAVE_LABEL = this.defaultFormValues[16];
    this.PAY_CODE_CLEAR_LABEL = this.defaultFormValues[17];
    this.PAY_CODE_CANCEL_LABEL = this.defaultFormValues[18];
    this.PAY_CODE_CREATE_FORM_LABEL = this.defaultFormValues[19];
    this.PAY_CODE_UPDATE_FORM_LABEL = this.defaultFormValues[20];
    this.PAY_CODE_DELETE_LABEL = this.defaultFormValues[21];
    this.PAY_CODE_ACTION = this.defaultFormValues[22];

    // Pay Code List
    this.payCodeIdList = Observable.create((observer: any) => {
      observer.next(this.modelPayCodeId);
    }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

    // Employee ID List
    this.employeeIDList = Observable.create((observer: any) => {
      observer.next(this.modelemployeeId);
    }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

    // Pay Code Type ID List
    this.payCodeTypeIdList = Observable.create((observer: any) => {
      observer.next(this.modelPayCodeTypeId);
    }).mergeMap((token: string) => this.getPayCodeTypeIdAsObservable(token));

    // Based On Pay Code List
    this.basedOnPayCodeIdList = Observable.create((observer: any) => {
      observer.next(this.model.baseOnPayCode);
    }).mergeMap((token: string) => this.getBasedOnPayCodeIdAsObservable(token));
  }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    this.getTypeData();


    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    //Following shifted from SetPage for better performance
    // Get Pay Code ID
    // this.employeePayCodeMaintenanceService.getPaycodeId().then(data => {
    //   this.getPayCodeId = data.result;
    //   //console.log('this.getPayCodeId:', this.getPayCodeId);
    // });

    // // Get Employee Code ID
    // this.employeePayCodeMaintenanceService.getEmployeeCodeId().then(data => {
    //   this.getEmployeeCodeID = data.result;
    //   //console.log('GetEmployeeCodeID :', this.getEmployeeCodeID);      
    // });

    // // Get Pay Code Type ID
    // this.employeePayCodeMaintenanceService.getPaycodeTypeId().then(data => {
    //   this.getPayCodeTypeId = data.result;
    //   //console.log('this.getPayCodeTypeId :',  this.getPayCodeTypeId);      
    // });

    // // Get Based On Pay Code ID


    //   this.employeePayCodeMaintenanceService.getBasedOnPayCodeRecords().then(data => {
    //       this.getBasedOnPayCodeRecords = data.result;
    //       //console.log('this.getBasedOnPayCode :',  this.getBasedOnPayCodeId);
    //   });
  }

  // For Employee ID
  getEmployeeIdAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getEmployeeCodeID.filter((id: any) => {
        return query.test(id.employeeId);
      })
    );
  }

  // For Pay Code ID
  getSuperviserIdAsObservable(token: string): Observable<any> {
    // console.log("token :: ", token);
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getPayCodeId.filter((id: any) => {
        return query.test(id.payCodeId);
      })
    );
  }

  // For Pay Code Type ID
  getPayCodeTypeIdAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getPayCodeTypeId.filter((id: any) => {
        return query.test(id.desc);
      })
    );
  }

  // For Based on Pay Code ID
  getBasedOnPayCodeIdAsObservable(token: string): Observable<any> {
    this.model.amount = null
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getBasedOnPayCodeRecords.length ? this.getBasedOnPayCodeRecords.filter((data: any) => {
        // console.log("Data", data);
        if(data.dtoPayCode != undefined) {
          return query.test(data.dtoPayCode.payCodeId)
        }
      }) : null,
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
    if (!e) {
      this.setBasePayCode = false;
    }
  }

  getTypeData(){
    this.employeePayCodeMaintenanceService.getTypeFieldDetails(this.typeId).then(data => {
     if(data.code == 201)
     {
         let val = data.result.records;
         for(let i=0;i<val.length;i++)
         {
             this.typeData.push({id:val[i].id,value:val[i].desc})
         }
     }
     else{
        this.typeData = [];
     }
    });
}

  typeaheadOnSelect(e: TypeaheadMatch, typeofSource: any): void {
    debugger;
    if (typeofSource == "employeeId") {
      //console.log('eee:', e)			      			
      this.employeeFirstName = e.item.employeeFirstName;
      this.model.employeeMaster["employeeIndexId"] = e.item.employeeIndexId;
      let payload1 = {
        employeeId: this.model.employeeMaster["employeeIndexId"]
      }
      this.employeePayCodeMaintenanceService.getEmpPayCodeRefreshed(payload1).then(data => {
        // console.log("data for new Updated PayCode", data);
        data.result.records != null ? this.getBasedOnPayCodeRecords = data.result.records : this.getBasedOnPayCodeRecords = []
        console.log("this.getBasedOnPayCodeRecords", this.getBasedOnPayCodeRecords);
      })
    } else if (typeofSource == "payCodeId") {
      console.log('payCodeId:', e)
      this.payCodeDesc = e.item.description;
      this.payCodeArabicDesc = e.item.arbicDescription;
      this.model.payCode["id"] = e.item.id;
      this.model.unitOfPay = e.item.unitofPay;
      this.model.payPeriod = e.item.payperiod;
      this.model.payCodeType['id'] = e.item.payCodeTypeId;
      this.modelPayCodeTypeId = e.item.payCodeTypeDesc;
      if (this.getBasedOnPayCodeRecords.length > 0 && e.item.baseOnPayCode != '') {
        this.isPayRateReadOnly = true;
        this.getBasedOnPayCodeRecords.forEach(ele => {
          if (ele.dtoPayCode.payCodeId.toString().trim() == e.item.baseOnPayCode.toString().trim()) {
            this.model.baseOnPayCode = e.item.baseOnPayCode;
            this.model.amount = ele.payRate;
            this.model.payFactory = e.item.payFactor;
            this.model.payRate = parseFloat(this.model.amount * this.model.payFactory + '').toFixed(3);
          }
        })
      } else {
        this.model.baseOnPayCode = '';
        this.model.amount = null;
        this.model.payFactory = null;
        this.model.payRate = e.item.payRate;
        this.isPayRateReadOnly = false;
      }

      this.model.payTypeId = e.item.payTypeId;
      this.model.roundOf = e.item.roundOf;
      // this.model.payFactory = '';
      // if (e.item.baseOnPayCodeAmount != null) {
      //   this.model.payRate = '';
      // } else {
      //   this.model.payRate = e.item.payRate;
      // }
    } else if (typeofSource == "payCodeType") {
      // console.log('payCodeType:', e)
      this.model.payCodeType["id"] = e.item.id;
    } else if (typeofSource == "basedOnPayCode") {
      console.log('basedOnPayCode:', e)
      if (this.model.amount == null || this.isUnderUpdate) {
        this.model.baseOnPayCode = e.value;
        this.model.amount = e.item.payRate;
        this.model.baseOnPayCodeId = e.item.id;
        this.model.payRate = parseFloat(this.model.amount * this.model.payFactory + '').toFixed(3);
      }
      this.setBasePayCode = true;
      if (e.value == '') {
        this.setBasePayCode = false;
      }
    }
  }
  // Set Page
  setPage(pageInfo) {
    debugger;
    this.selected = [];
    this.getBasedOnPayCodeRecords = [];
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
    } else {
      this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
    } else {
      this.page.sortBy = pageInfo.sortBy;
    }

    // Get All
    this.page.searchKeyword = '';
    this.employeePayCodeMaintenanceService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      //console.log("aaaaaaaa", pagedData);
      debugger;
      this.page = pagedData.page;
      this.rows = pagedData.data;
      //console.log("PAGE:", this.page);
      //console.log("ROWS:", this.rows);
      this.employeePayCodeMaintenanceService.getBasedOnPayCodeId().then(data => {
        this.getBasedOnPayCodeId = data.result;
        //console.log('this.getBasedOnPayCode :',  this.getBasedOnPayCodeId);      
      });

      this.employeePayCodeMaintenanceService.getPaycodeId().then(data => {
        debugger;
        this.getPayCodeId = data.result;
        //console.log('this.getPayCodeId:', this.getPayCodeId);
      });

      // Get Employee Code ID
      this.employeePayCodeMaintenanceService.getEmployeeCodeId().then(data => {
        this.getEmployeeCodeID = data.result;
        //console.log('GetEmployeeCodeID :', this.getEmployeeCodeID);      
      });

      this.employeePayCodeMaintenanceService.getPaycodeTypeId().then(data => {
        this.getPayCodeTypeId = data.result;
        //console.log('this.getPayCodeTypeId :',  this.getPayCodeTypeId);      
      });

      // Get Based On Pay Code ID
      // this.employeePayCodeMaintenanceService.getBasedOnPayCodeRecords().then(data => {
      //   this.getBasedOnPayCodeRecords = data.result;
      //   //console.log('this.getBasedOnPayCode :',  this.getBasedOnPayCodeId);
      // });
    });

  }

  // Open form for create Deduction Code
  Create() {
    this.showCreateForm = false;
    this.isUnderUpdate = false;
    this.isPaycodeFormdisabled = false;
    this.employeeFirstName = '';
    this.payCodeDesc = '';
    this.payCodeArabicDesc = '';
    this.modelPayCodeTypeId = null;
    this.modelemployeeId = null;
    this.modelPayCodeId = null;
    this.islifeTimeValidEmployerpay = false;
    this.isPayRateReadOnly = false;
    setTimeout(() => {
      this.showCreateForm = true;
      this.checkPayCodeValidId = false;
      this.checkEmpValidId = false;
      setTimeout(() => {
        window.scrollTo(0, 2000);
      }, 10);
    }, 10);
    this.model = {
      id: 0,
      employeeMaster: { 'employeeIndexId': '' },
      payCode: { 'id': '' },
      payCodeType: { 'id': '' },
      baseOnPayCode: '',
      amount: null,
      payFactory: '',
      inactive: false,
      payPeriod: '0',
      unitOfPay: '0',
      payRate: '',
      baseOnPayCodeId: null,
      payTypeId:0,
      roundOf:0

    };

    // Get Pay Code Type ID
    this.employeePayCodeMaintenanceService.getPaycodeTypeId().then(data => {
      this.getPayCodeTypeId = data.result;
      //console.log('this.getPayCodeTypeId :',  this.getPayCodeTypeId);      
    });

    // Get Based On Pay Code ID


    this.employeePayCodeMaintenanceService.getBasedOnPayCodeRecords().then(data => {
      this.getBasedOnPayCodeRecords = data.result;
      //console.log('this.getBasedOnPayCode :',  this.getBasedOnPayCodeId);
    });
  }

  // function call for creating new Emp Deduction Code
  CreateDeductionCode(f: NgForm, event: Event) {
    event.preventDefault();

    if (!this.decimalValue) {
      return;
    }

    this.checkPayCodeValidId = false;
    this.checkEmpValidId = false;
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
      this.isConfirmationModalOpen = true;
      this.isDeleteAction = false;
    }
    else {
      // Call service api for Creating new Deduction Code
      let locIdx = {
        payCodeId: this.model.payCode['id'],
        employeeId: this.model.employeeMaster['employeeIndexId']
      }
      this.employeePayCodeMaintenanceService.checkDuplicateMiscellaneousBenefitsId(locIdx).then(response => {
        // console.log('response', response);
        if (response && response.code == 302 && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = 'success';

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.scrollTo(0, 400);
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;
          }, 100);
        } else {
          //Call service api for Creating new Deduction Code
          this.employeePayCodeMaintenanceService.createEmpPayCode(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
                }, 4000);
                this.showCreateForm = false;
                this.messageText = data.btiMessage.message;
                ;
              }, 100);

              this.hasMsg = true;
              f.resetForm();
              //Refresh the Grid data after adding new Deduction Code
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = 'Server error. Please contact admin.';
            }, 100)
          });
        }

      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = 'Server error. Please contact admin.';
        }, 100)
      });

    }
  }

  // Amount Calculation
  calculatePayRate(event) {
    this.model.payRate = parseFloat(this.model.amount * this.model.payFactory + "").toFixed(3);
    //console.log("aa:", this.model.payRate);
  }

  //edit Employee Pay COde Maintenance by row
  edit(row: EmployeePayCodeMaintenanceModule) {
    debugger;
    console.log("Edit Row", row)
    this.showCreateForm = true;
    this.checkPayCodeValidId = false;
    this.checkEmpValidId = false;
    this.islifeTimeValidEmployerpay = false;
    this.isPayRateReadOnly = false;
    this.model = Object.assign({}, row);
    this.model.payTypeId = this.model.payCode["payTypeId"];
    this.model.roundOf = this.model.payCode["roundOf"];
    // console.log("this.model", this.model)

    this.isUnderUpdate = true;
    if (this.model.inactive == true) {
      this.isPaycodeFormdisabled = true;
    } else {
      this.isPaycodeFormdisabled = false;
    }
    this.employeePayCodeMaintenanceService.getEmpPayCodeById(row.id).then(data => {
      this.getBasedOnPayCodeId = data.result;
      console.log("getEmpPayCodeById in Edit Row", this.getBasedOnPayCodeId);
      this.employeeIdValue = this.getBasedOnPayCodeId.employeeMaster['employeeId'];
      this.employeeFirstName = this.getBasedOnPayCodeId.employeeMaster['employeeFirstName'];
      this.payCodeValue = this.getBasedOnPayCodeId.payCode['payCodeId'];
      this.payCodeDesc = this.getBasedOnPayCodeId.payCode['description'];
      this.payCodeArabicDesc = this.getBasedOnPayCodeId.payCode['arbicDescription'];
      this.modelPayCodeTypeId = this.getBasedOnPayCodeId.payCodeType['desc'];
      this.model.payPeriod = this.getBasedOnPayCodeId.payPeriod;
      console.log('this.model', this.model);
      if (this.model.baseOnPayCode != '') {
        let payload1 = {
          employeeId: row.employeeMaster['employeeIndexId']
        }
        this.employeePayCodeMaintenanceService.getEmpPayCodeRefreshed(payload1).then(data => {
          console.log("data for new Amount", data);
          this.getBasedOnPayCodeRecords = data.result.records;
          if (data.result.records.length) {
            data.result.records.forEach((element, i) => {
              console.log("ELEMENT", element);
              console.log("Index", i);
              if (element.id == this.getBasedOnPayCodeRecords[i].id && (element.dtoPayCode.payCodeId).toString().trim() == this.getBasedOnPayCodeId.baseOnPayCode.toString().trim()) {
                this.model.amount = element.payRate;
                this.isPayRateReadOnly = true;
                this.model.payRate = parseFloat(this.model.amount * this.model.payFactory + '').toFixed(3);
              }
            })
          }
        })
      } else {
        this.model.amount = null;
      }
    });

    // Get Pay Code Type ID
    this.employeePayCodeMaintenanceService.getPaycodeTypeId().then(data => {
      this.getPayCodeTypeId = data.result;
      //console.log('this.getPayCodeTypeId :',  this.getPayCodeTypeId);      
    });
    setTimeout(() => {
      window.scrollTo(0, 2000);
    }, 10);
  }

  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }

  // Update Pay Code Maintenance
  updateStatus() {
    this.closeModal();
    // this.model.payCodeId = this.payCodeIdvalue;
    this.employeePayCodeMaintenanceService.updateEmpPaycodeMntnce(this.model).then(data => {
      var datacode = data.code;
      if (datacode == 201) {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        window.scrollTo(0, 0);
        window.setTimeout(() => {
          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          this.messageText = data.btiMessage.message;
          ;
          this.showCreateForm = false;
        }, 100);

        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = 'Server error. Please contact admin !';
      }, 100)
    });
  }

  varifyDelete() {
    if (this.selected.length > 0) {
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
    }
    else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;
      this.message.text = 'Please select at least one record to delete.';
      window.setTimeout(() => {
        this.showMsg = false;
        this.hasMessage = false;
      }, 4000);
      window.scrollTo(0, 0);
    }
  }

  //delete DEmployee Pay Maintenance by passing whole object of perticular Department
  delete() {
    var selectedSupervisors = [];
    for (var i = 0; i < this.selected.length; i++) {
      selectedSupervisors.push(this.selected[i].id);
    }
    this.employeePayCodeMaintenanceService.deleteEmpPaycode(selectedSupervisors).then(data => {
      var datacode = data.code;
      if (datacode == 200) {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if(datacode == "302"){
        this.message.type = "error";
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
      } else {
          this.isSuccessMsg = true;
          this.message.type = "success";
          this.isfailureMsg = false;
      }
      window.scrollTo(0, 0);
      window.setTimeout(() => {
        this.showMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMessage = false;
        }, 4000);
        this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of department
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    }).catch(error => {
      this.hasMessage = true;
      this.message.type = 'error';
      var errorCode = error.status;
      this.message.text = 'Server issue. Please contact admin !';
    });
    this.closeModal();
  }

  // default list on page
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  searchFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
  }

  // Clear form to reset to default blank
  Clear(f: NgForm) {
    f.resetForm(
      {
        amount: '',
        payFactory: '',
        payPeriod: '0',
        unitOfPay: '0'
      }
    );
    if (!this.isUnderUpdate) {
      this.employeeFirstName = '';
      this.payCodeDesc = '';
      this.payCodeArabicDesc = '';
    }
    this.isPaycodeFormdisabled = false;
    this.islifeTimeValidEmployerpay = true;

  }

  // check Form Active
  checkDeductionCodeForm(event) {
    if (event.target.checked == true) {
      this.isPaycodeFormdisabled = true;
    }
    else {
      this.isPaycodeFormdisabled = false;
    }
  }
  sortColumn(val) {
    if (this.page.sortOn == val) {
      if (this.page.sortBy == 'DESC') {
        this.page.sortBy = 'ASC';
      } else {
        this.page.sortBy = 'DESC';
      }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  // CheckValid ID

  CheckValidID(eve: any, empId: any) {
    // console.log("eve :: ", eve);
    // console.log("empId :: ", empId);
    if (this.getEmployeeCodeID.length != 0) {
      // console.log("test1");
      if (empId == 'employeeId') {
        for (let i = 0; i < this.getEmployeeCodeID.length; i++) {
          if (this.getEmployeeCodeID[i].employeeId == eve.value) {
            this.checkEmpValidId = false;
            return;
          } else {
            this.checkEmpValidId = true;
          }
        }
      } else {
        for (let i = 0; i < this.getPayCodeId.length; i++) {
          if (this.getPayCodeId[i].payCodeId == eve.value) {
            this.checkPayCodeValidId = false;
            return;
          } else {
            this.checkPayCodeValidId = true;
          }
        }
      }

    } else {
      // console.log("test2");
      this.checkPayCodeValidId = false;
      this.checkEmpValidId = false;
    }
  }

  // Set default page size
  changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }
  clearPrefixedData() {
    if (this.model.baseOnPayCode == '') {
      this.model.payRate = '';
      this.model.amount = '';
      this.model.payFactory = '';
    }
  }

  checkdecimalpayRate(digit) {

    digit != null ? digit = digit.toString() : digit = '';
    let isFirstDecimal = digit.indexOf('.') > -1 ? digit.split('.')[0].length == 0 : false

    if (digit == null || digit == '' || isFirstDecimal) {
      this.decimalValue = true;
      return false;
    } else {
      let count = (digit.indexOf('.') > -1) ? 11 : 10;
      this.tempf = digit.split(".");
      if (count > 10) {
        if (this.tempf[0].length > 7) {
          this.decimalValue = false;
          return true;
        } else if (this.tempf[1].length > 3) {
          this.decimalValue = false;
          return true;
        } else {
          this.decimalValue = true;
          return false;
        }

      } else {
        if (this.tempf[0].length > 10) {
          this.decimalValue = false;
          return true;
        } else {
          this.decimalValue = true;
          return false;
        }
      }
    }
  }

}
