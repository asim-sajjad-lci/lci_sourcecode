import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildPayrollCheckComponent } from './build-payroll-check-paycode.component';

describe('BuildPayrollCheckComponent', () => {
  let component: BuildPayrollCheckComponent;
  let fixture: ComponentFixture<BuildPayrollCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildPayrollCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildPayrollCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
