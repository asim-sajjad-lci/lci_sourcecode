import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { PayScheduleSetup } from '../../_models/pay-schedule-setup/payScheduleSetup';
import { PayScheduleSetupPeriod } from '../../_models/pay-schedule-setup/payScheduleSetupPeriod';
import { PayScheduleSetupPosition } from '../../_models/pay-schedule-setup/payScheduleSetupPosition';
import { PayScheduleSetupLocation } from '../../_models/pay-schedule-setup/payScheduleSetupLocation';
import { PayScheduleSetupEmployee } from '../../_models/pay-schedule-setup/payScheduleSetupEmployee';
import { PayScheduleSetupDepartment } from '../../_models/pay-schedule-setup/payScheduleSetupDepartment';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { PayScheduleSetupService } from '../../_services/Pay-schedule-setup/payschedulesetup.service';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PayScheduleSetupPeriodService } from '../../_services/Pay-schedule-setup/payScheduleSetupPeriod.Service';
import { PayScheduleSetupPositionService } from '../../_services/Pay-schedule-setup/payScheduleSetupPosition.Service';
import { PayScheduleSetupLocationService } from '../../_services/Pay-schedule-setup/payScheduleSetupLocation.Service';
import { PayScheduleSetupEmployeeService } from '../../_services/Pay-schedule-setup/payScheduleSetupEmployee.Service';
import { payScheduleSetupDepartmentService } from '../../_services/Pay-schedule-setup/payScheduleSetupDepartment.Service';
import { NgForm } from '@angular/forms';
import { IMyDateModel, INgxMyDpOptions } from 'ngx-mydatepicker';
import { PaycodeSetupComponent } from '../paycode-setup/paycode-setup.component';
import { PaycodeSetupModule } from '../../_models/paycode-setup/paycode-setup.module';
import {Observable} from 'rxjs/Observable';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-pay-schedule-setup',
  templateUrl: './pay-schedule-setup.component.html',
  providers:[PayScheduleSetupService,PayScheduleSetupPeriodService,PayScheduleSetupPositionService,
    payScheduleSetupDepartmentService,PayScheduleSetupLocationService,PayScheduleSetupEmployeeService,CommonService, DatePipe],
  })
export class PayScheduleSetupComponent implements OnInit {
    rowsEmployeeTemp: PayScheduleSetupEmployee[];
    rowsPositionTemp: PayScheduleSetupPosition[];
    rowsDepartmentTemp: PayScheduleSetupDepartment[];
    rowsLocationTemp: PayScheduleSetupLocation[];
    endDate: any;
    startDate: any;
    frmStartDate: Date;
    hasMessage: boolean;
    duplicateWarning: boolean;
    isDeleteAction: boolean;
    hasMsg: boolean;
    showMsg: boolean;
    ddPageSize: number = 5;
    isConfirmationModalOpen: boolean;
  model: PayScheduleSetup;
  screenName: any;
  moduleName: any;
 
  selected: any[];
  selectedPeriod:any[];
  selectedDepartment: any[];
  selectedPosition: any[];
  selectedEmployee: any[];
  selectedLocation: any[];

  currentLanguage: string;
  page = new Page();
  pagePeriod=new Page();
  pagePosition=new Page();
  pageDepartment=new Page();
  pageEmployee=new Page();
  pageLocation=new Page();
  defaultFormValues = [];
  availableFormValues = [];
  moduleCode = "M-1011";
  screenCode = "S-1438";
  searchKeyword = '';
  message = { 'type': '', 'text': '' };
  payScheduleId;
  payScheduleIdValue : string;
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  messageText: string;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  rows = new Array<PayScheduleSetup>();
  rowsPeriod=new Array<PayScheduleSetupPeriod>();
  rowsDepartment = new Array<PayScheduleSetupDepartment>();
  rowsPosition = new Array<PayScheduleSetupPosition>();
  rowsLocation = new Array<PayScheduleSetupLocation>();
  rowsEmployee = new Array<PayScheduleSetupEmployee>();
  temp = new Array<PayScheduleSetup>();
  tempDepartment = new Array<PayScheduleSetupDepartment>();
  paySchedule = [];
  isEndDateValid:boolean = true;
  payscheduleIdList: Observable<any>;
  getPaySchedule:any[]=[];
  typeaheadLoading: boolean;
           
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;

            PAY_SCHEDULE_SETUP_SEARCH:any;
            PAY_SCHEDULE_SETUP_ACTION:any;
            PAY_SCHEDULE_SETUP_CREATE_LABEL:any;
            PAY_SCHEDULE_SETUP_SAVE_LABEL:any;
            PAY_SCHEDULE_SETUP_CLEAR_LABEL:any;
            PAY_SCHEDULE_SETUP_CANCEL_LABEL:any;
            PAY_SCHEDULE_SETUP_UPDATE_LABEL:any;
            PAY_SCHEDULE_SETUP_DELETE_LABEL:any;
            PAY_SCHEDULE_SETUP_CREATE_FORM_LABEL:any;
            PAY_SCHEDULE_SETUP_UPDATE_FORM_LABEL:any;
            PAY_SCHEDULE_SETUP_SCHEDULE_ID:any;
            PAY_SCHEDULE_SETUP_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_ARABIC_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_PAY_PERIOD:any;
            PAY_SCHEDULE_SETUP_BEGIN_DATE:any;
            PAY_SCHEDULE_SETUP_YEAR:any;
            PAY_SCHEDULE_SETUP_SCHEDULE:any;
            PAY_SCHEDULE_SETUP_LOCATION:any;
            PAY_SCHEDULE_SETUP_POSITION:any;
            PAY_SCHEDULE_SETUP_DEPARTMENT:any;
            PAY_SCHEDULE_SETUP_EMPLOYEE:any;
            PAY_SCHEDULE_SETUP_SCHEDULE_TAB_YEAR:any;
            PAY_SCHEDULE_SETUP_SCHEDULE_TAB_PERIOD:any;
            PAY_SCHEDULE_SETUP_SCHEDULE_TAB_PERIOD_NAME:any;
            PAY_SCHEDULE_SETUP_SCHEDULE_TAB_START_DATE:any;
            PAY_SCHEDULE_SETUP_SCHEDULE_TAB_END_DATE:any;
            PAY_SCHEDULE_SETUP_LOCATION_TAB_LOCATION_ID:any;
            PAY_SCHEDULE_SETUP_LOCATION_TAB_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_LOCATION_TAB_ARABIC_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_POSITION_TAB_POSITION_ID:any;
            PAY_SCHEDULE_SETUP_POSITION_TAB_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_POSITION_TAB_ARABIC_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_DEPARTMENT_ID:any;
            PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_ARABIC_DESCRIPTION:any;
            PAY_SCHEDULE_SETUP_EMPLOYEE_TAB_EMPLOYEE_ID:any;
            PAY_SCHEDULE_SETUP_EMPLOYEE_TAB_EMPLOYEE_NAME:any;

  constructor(
    private payScheduleSetupService: PayScheduleSetupService,
    private payScheduleSetupPeriodService:PayScheduleSetupPeriodService,
    private payScheduleSetupPositionService:PayScheduleSetupPositionService,
    private payScheduleSetupLocationService:PayScheduleSetupLocationService,
    private payScheduleSetupEmployeeService:PayScheduleSetupEmployeeService,
    private payScheduleSetupDepartmentService:payScheduleSetupDepartmentService,
    private getScreenDetailService: GetScreenDetailService,
    private commonService:CommonService,
    private datePipe: DatePipe) {
    this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        this.pagePeriod.pageNumber=0;
        this.pagePeriod.size=5;
        this.pagePeriod.sortOn="id";
        this.pagePeriod.sortBy = 'DESC';

        this.paySchedule = [
            {id:1,name:"Weekly", value:"Paid once a week.", days:52, period:7},
            {id:2,name:"Biweekly", value:"Paid every two weeks.", days:26, period:14},
            {id:3,name:"Semimonthly", value:"Paid twice a month.", days:24, period:15},
            {id:4,name:"Monthly", value:"Paid once a month.", days:12, period:30},
            {id:5,name:"Quarterly", value:"Paid four times a year.", days:4, period:91},
            {id:6,name:"Semiannually", value:"Paid twice a year.", days:2, period:182},
            {id:7,name:"Annually", value:"Paid once a year.", days:1, period:364}]

        this.defaultFormValues = [
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SCHEDULE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_PAY_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_BEGIN_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_YEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SCHEDULE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_LOCATION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_POSITION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_DEPARTMENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_EMPLOYEE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SCHEDULE_TAB_YEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SCHEDULE_TAB_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SCHEDULE_TAB_PERIOD_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SCHEDULE_TAB_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_SCHEDULE_TAB_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_LOCATION_TAB_LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_LOCATION_TAB_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_LOCATION_TAB_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_POSITION_TAB_POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_POSITION_TAB_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_POSITION_TAB_ARABIC-DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_EMPLOYEE_TAB_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_SCHEDULE_SETUP_EMPLOYEE_TAB_EMPLOYEE_NAME', 'fieldValue': '', 'helpMessage': '' }
        ];

            this.PAY_SCHEDULE_SETUP_SEARCH=this.defaultFormValues[0];
            this.PAY_SCHEDULE_SETUP_ACTION=this.defaultFormValues[1];
            this.PAY_SCHEDULE_SETUP_CREATE_LABEL=this.defaultFormValues[2];
            this.PAY_SCHEDULE_SETUP_SAVE_LABEL=this.defaultFormValues[3];
            this.PAY_SCHEDULE_SETUP_CLEAR_LABEL=this.defaultFormValues[4];
            this.PAY_SCHEDULE_SETUP_CANCEL_LABEL=this.defaultFormValues[5];
            this.PAY_SCHEDULE_SETUP_UPDATE_LABEL=this.defaultFormValues[6];
            this.PAY_SCHEDULE_SETUP_DELETE_LABEL=this.defaultFormValues[7];
            this.PAY_SCHEDULE_SETUP_CREATE_FORM_LABEL=this.defaultFormValues[8];
            this.PAY_SCHEDULE_SETUP_UPDATE_FORM_LABEL=this.defaultFormValues[9];
            this.PAY_SCHEDULE_SETUP_SCHEDULE_ID=this.defaultFormValues[10];
            this.PAY_SCHEDULE_SETUP_DESCRIPTION=this.defaultFormValues[11];
            this.PAY_SCHEDULE_SETUP_ARABIC_DESCRIPTION=this.defaultFormValues[12];
            this.PAY_SCHEDULE_SETUP_PAY_PERIOD=this.defaultFormValues[13];
            this.PAY_SCHEDULE_SETUP_BEGIN_DATE=this.defaultFormValues[14];
            this.PAY_SCHEDULE_SETUP_YEAR=this.defaultFormValues[15];
            this.PAY_SCHEDULE_SETUP_SCHEDULE=this.defaultFormValues[16];
            this.PAY_SCHEDULE_SETUP_LOCATION=this.defaultFormValues[17];
            this.PAY_SCHEDULE_SETUP_POSITION=this.defaultFormValues[18];
            this.PAY_SCHEDULE_SETUP_DEPARTMENT=this.defaultFormValues[19];
            this.PAY_SCHEDULE_SETUP_EMPLOYEE=this.defaultFormValues[20];
            this.PAY_SCHEDULE_SETUP_SCHEDULE_TAB_YEAR=this.defaultFormValues[21];
            this.PAY_SCHEDULE_SETUP_SCHEDULE_TAB_PERIOD=this.defaultFormValues[22];
            this.PAY_SCHEDULE_SETUP_SCHEDULE_TAB_PERIOD_NAME=this.defaultFormValues[23];
            this.PAY_SCHEDULE_SETUP_SCHEDULE_TAB_START_DATE=this.defaultFormValues[24];
            this.PAY_SCHEDULE_SETUP_SCHEDULE_TAB_END_DATE=this.defaultFormValues[25];
            this.PAY_SCHEDULE_SETUP_LOCATION_TAB_LOCATION_ID=this.defaultFormValues[26];
            this.PAY_SCHEDULE_SETUP_LOCATION_TAB_DESCRIPTION=this.defaultFormValues[27];
            this.PAY_SCHEDULE_SETUP_LOCATION_TAB_ARABIC_DESCRIPTION=this.defaultFormValues[28];
            this.PAY_SCHEDULE_SETUP_POSITION_TAB_POSITION_ID=this.defaultFormValues[29];
            this.PAY_SCHEDULE_SETUP_POSITION_TAB_DESCRIPTION=this.defaultFormValues[30];
            this.PAY_SCHEDULE_SETUP_POSITION_TAB_ARABIC_DESCRIPTION=this.defaultFormValues[31];
            this.PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_DEPARTMENT_ID=this.defaultFormValues[32];
            this.PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_DESCRIPTION=this.defaultFormValues[33];
            this.PAY_SCHEDULE_SETUP_DEPARTMENT_TAB_ARABIC_DESCRIPTION=this.defaultFormValues[34];
            this.PAY_SCHEDULE_SETUP_EMPLOYEE_TAB_EMPLOYEE_ID=this.defaultFormValues[35];
            this.PAY_SCHEDULE_SETUP_EMPLOYEE_TAB_EMPLOYEE_NAME=this.defaultFormValues[36];

      this.payscheduleIdList = Observable.create((observer: any) => {
          // Runs on every search
          observer.next(this.model.payScheduleId);
      }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

   }

   
   changePageSize(event) {
      
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

   ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
   // this.setPagePeriod({ offset: 0, sortOn: this.pagePeriod.sortOn, sortBy: this.pagePeriod.sortBy });
    this.setPageLocation({ offset: 0, sortOn: this.pageLocation.sortOn, sortBy: this.pageLocation.sortBy });
    this.setPageDepartment({ offset: 0, sortOn: this.pageDepartment.sortOn, sortBy: this.pageDepartment.sortBy });
    this.setPageEmployee({ offset: 0, sortOn: this.pageEmployee.sortOn, sortBy: this.pageEmployee.sortBy });
    this.setPagePosition({ offset: 0, sortOn: this.pagePosition.sortOn, sortBy: this.pagePosition.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    // getting screen labels, help messages and validation messages
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

    //Following shifted from SetPage for better performance
    this.payScheduleSetupService.getPayScheduleId().then(data => {
        this.getPaySchedule = data.result.records;
        console.log("Data class options : "+this.getPaySchedule);
    });
  }

    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'payScheduleBeggingDate') {
            return this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy');
        } else if (gridFieldName === 'paySchedulePayPeriod') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                        row[gridFieldName] === 7 ? 'Annually' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

  CreatePayScheduleSetup(f: NgForm, event: Event) {
    event.preventDefault();
    var payScheduleId = this.model.payScheduleId;
    
    if (this.model.payScheduleIndexId > 0 && this.model.payScheduleIndexId != 0 && this.model.payScheduleIndexId != undefined) {
        this.isConfirmationModalOpen = true;
        this.isDeleteAction = false;
    }
    else {
        this.payScheduleSetupService.checkDuplicatePayScheduleSetupId(payScheduleId).then(response => {
            if (response && response.code == 302 && response.result && response.result.isRepeat) {
                this.duplicateWarning = true;
                this.message.type = "success";

                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = response.btiMessage.message;
                }, 100);
                window.scrollTo(700, 700);

            } 
            else {
                console.log(this.model);
                this.model.payScheduleBeggingDate = this.model.payScheduleBeggingDate * 1000;
                    this.model.subItems.forEach(element => {
                        if(element.periodStartDate.date != undefined){
                            var startDate = new Date(element.periodStartDate.date.year+"-"+element.periodStartDate.date.month+"-"+element.periodStartDate.date.day);
                            element.periodStartDate=startDate.getTime();
                        }
                        if(element.periodEndDate.date != undefined){
                            var endDate = new Date(element.periodEndDate.date.year+"-"+element.periodEndDate.date.month+"-"+element.periodEndDate.date.day);
                            element.periodEndDate=endDate.getTime();
                        }
                    });
                    if(this.model.subItems.length > 0){
                        this.payScheduleSetupService.createPayScheduleSetup(this.model).then(data => {
                            var datacode = data.code;
                            if (datacode == 201) {
                                window.scrollTo(0, 0);
                                this.clearData();
                                window.setTimeout(() => {
                                    this.isSuccessMsg = true;
                                    this.isfailureMsg = false;
                                    this.showMsg = true;
                                    this.hasMessage = false;
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.hasMsg = false;
                                    }, 4000);
                                    this.showCreateForm = false;
                                    this.messageText = data.btiMessage.message;
                                }, 100);
        
                                this.hasMsg = true;
                                f.resetForm();
                                //Refresh the Grid data after adding new department
                                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                            }
                        }).catch(error => {
                            this.hasMsg = true;
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.messageText = "Server error. Please contact admin.";
                                window.scrollTo(0, 0);
                            }, 100)
                        });
                    }else{
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Please enter atleast one Pay Schedule Setup Detail record.";
                            window.scrollTo(0, 0);
                        }, 100)
                    }
                
                
            }

        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
                window.scrollTo(0, 0);
            }, 100)
        });

    }
}
payScheduleBeggingDate:any;
myOptions: INgxMyDpOptions = {
    dateFormat: 'dd-mm-yyyy',
};
selectedPeriodDate:any;
selectedPeriodStartDate:any;
selectedPeriodEndDate:any;
onStartDateChanged(event: IMyDateModel): void {
    let tdate = new Date(event.jsdate);
    console.log(tdate);
    this.selectedPeriodDate = new Date(event.jsdate);
    this.selectedPeriodStartDate = new Date(event.jsdate);
    this.selectedPeriodEndDate = new Date(event.jsdate);
    this.model.payScheduleBeggingDate = event.epoc;
    this.model.payScheduleYear = tdate.getFullYear();
    this.selectedPeriodStartDate = new Date(this.selectedPeriodDate.getTime());
    this.selectedPeriodEndDate = new Date(this.selectedPeriodDate.getTime());
    this.selectedPeriodEndDate.setMonth(11);
    this.selectedPeriodEndDate.setDate(31);
    this.model.subItems =[];
    console.log(this.selectedPeriodDate);
    console.log(this.selectedPeriodEndDate);
    let datelist = [];
    let days = this.daysInMonth(new Date().getFullYear());
    
    while(this.selectedPeriodStartDate.getTime() <= this.selectedPeriodEndDate.getTime()){
        datelist.push(new Date(this.selectedPeriodStartDate.getTime()));
        
        if(this.selectedPayPeriod.name=='Monthly'){
            if(this.selectedPeriodStartDate.getDate() != 1){
                this.selectedPeriodStartDate.setDate(1);
            }
            this.selectedPeriodStartDate.setDate(this.selectedPeriodStartDate.getDate() + days[this.selectedPeriodStartDate.getMonth()]);
        }else if(this.selectedPayPeriod.name=='Quarterly'){
            if(this.selectedPeriodStartDate.getDate() != 1){
                this.selectedPeriodStartDate.setDate(1);
            }
            this.selectedPeriodStartDate.setMonth(this.selectedPeriodStartDate.getMonth() + 3);
        }else if(this.selectedPayPeriod.name=='Semiannually'){
            if(this.selectedPeriodStartDate.getDate() != 1){
                this.selectedPeriodStartDate.setDate(1);
            }
            this.selectedPeriodStartDate.setMonth(this.selectedPeriodStartDate.getMonth() + 6);
        }else if(this.selectedPayPeriod.name=='Weekly'){
            this.selectedPeriodStartDate.setDate(this.selectedPeriodStartDate.getDate() + 7);
            console.log(this.selectedPeriodStartDate);
        }else if(this.selectedPayPeriod.name=='Biweekly'){
            this.selectedPeriodStartDate.setDate(this.selectedPeriodStartDate.getDate() + 14);
            console.log(this.selectedPeriodStartDate);
        }else if(this.selectedPayPeriod.name=='Semimonthly'){
            this.selectedPeriodStartDate.setDate(this.selectedPeriodStartDate.getDate() + 15);
            console.log(this.selectedPeriodStartDate);
        }else{
                this.selectedPeriodStartDate.setDate(this.selectedPeriodStartDate.getDate() + Number(this.selectedPayPeriod.period));
 
        }
    }
    let month = ['Jan', 'Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec'];
    console.log(datelist.length)
    datelist.forEach((element,index) => {
        let d = new Date(element.getTime());
        if(index != datelist.length - 1){
            if(this.selectedPayPeriod.name=='Monthly'){
                d.setDate(days[d.getMonth()]);
            }else if(this.selectedPayPeriod.name=='Quarterly'){
                d.setDate(1);
                d.setMonth(d.getMonth()+3);
                d.setDate(d.getDate()-1);
            }else if(this.selectedPayPeriod.name=='Semiannually'){
                d.setDate(1);
                d.setMonth(d.getMonth()+6);
                d.setDate(d.getDate()-1);
            }else if(this.selectedPayPeriod.name=='Weekly'){
                d.setDate(d.getDate()+6);
            }else if(this.selectedPayPeriod.name=='Biweekly'){
                d.setDate(d.getDate()+13);
            }else if(this.selectedPayPeriod.name=='Semimonthly'){
                d.setDate(d.getDate()+14);
            }else{
                d.setDate(d.getDate()+Number(this.selectedPayPeriod.period - 1));
            }
        }else{
            d = new Date(this.selectedPeriodEndDate.getTime());
                d.setDate(days[d.getMonth()]);
        }
        
        let start = "";
        if(month[element.getMonth()] != month[d.getMonth()]){
         start = month[element.getMonth()]+" - "+month[d.getMonth()];
        }else{
         start = month[element.getMonth()];
        }
 
        let subItemsObj = {
            year:element.getFullYear(),
            periodId:this.selectedPayPeriod.id,
            periodNameId:this.selectedPayPeriod.name,
            periodName:start,
            periodStartDate:this.formatDateFordatePicker(element.getTime()),
            periodEndDate:this.formatDateFordatePicker(d.getTime()),
            invalid:false
        };
        this.model.subItems.push(Object.assign({}, subItemsObj));
    });
}

// onperiodStartDate(event: IMyDateModel,num): void {
   
//     if(event.jsdate==null){
//         this.model.subItems[num]="";
//     }else{
//         let tdate = new Date(event.jsdate);
//         console.log(tdate);
        
//         if(this.model.subItems[num].periodId==1){
//             tdate.setDate(tdate.getDate()+7);
//         }else if(this.model.subItems[num].periodId==2){
//             tdate.setDate(tdate.getDate()+14);
//         }
//         else if(this.model.subItems[num].periodId==3){
//             tdate.setDate(tdate.getDate()+15);
//         }else if(this.model.subItems[num].periodId==4){
//             tdate.setDate(tdate.getDate()+30);
//         }else if(this.model.subItems[num].periodId==5){
//             tdate.setDate(tdate.getDate()+90);
//         }else if(this.model.subItems[num].periodId==6){
//             tdate.setDate(tdate.getDate()+180);
//         }else if(this.model.subItems[num].periodId==7){
//             tdate.setDate(tdate.getDate()+360);
//         }
    
//         this.model.subItems[num].periodEndDate = this.formatDateFordatePicker(tdate)
//     }
   
   
// }



onperiodStartDate(event: IMyDateModel,num): void {
        console.log(this.model.subItems[num].periodStartDate)
        this.model.subItems[num].periodStartDate = event;
        if(this.model.subItems[num].periodStartDate.date.year != undefined &&
            this.model.subItems[num].periodEndDate.date.year != undefined){
            let endDate = new Date(this.model.subItems[num].periodEndDate.date.year+"-"+this.model.subItems[num].periodEndDate.date.month+"-"+this.model.subItems[num].periodEndDate.date.day);
            let newstartdate = new Date(this.model.subItems[num].periodStartDate.date.year+"-"+this.model.subItems[num].periodStartDate.date.month+"-"+this.model.subItems[num].periodStartDate.date.day);

            let startDate = new Date(event.date.year+"-"+event.date.month+"-"+event.date.day);

            if (endDate.getTime() < startDate.getTime()) {
                this.model.subItems[num].invalid = true;
                this.isEndDateValid = false;
            } else{
                this.model.subItems[num].invalid = false;
                this.isEndDateValid = true;
            }
        }
        console.log(this.model.subItems[num].periodEndDate)

}

onperiodEndDate(event: IMyDateModel,num): void {
    console.log(this.model.subItems[num].periodStartDate)
    this.model.subItems[num].periodEndDate = event;
    if(this.model.subItems[num].periodStartDate.date.year != undefined && 
        this.model.subItems[num].periodEndDate.date.year != undefined){
        let startDate = new Date(this.model.subItems[num].periodStartDate.date.year+"-"+this.model.subItems[num].periodStartDate.date.month+"-"+this.model.subItems[num].periodStartDate.date.day);
        let newstartdate = new Date(this.model.subItems[num].periodStartDate.date.year+"-"+this.model.subItems[num].periodStartDate.date.month+"-"+this.model.subItems[num].periodStartDate.date.day);
        
        let endDate = new Date(event.date.year+"-"+event.date.month+"-"+event.date.day);

        if (endDate.getTime() < startDate.getTime()) {
            this.model.subItems[num].invalid = true;
            this.isEndDateValid = false;
        } else{
            this.model.subItems[num].invalid = false;
            this.isEndDateValid = true;
        }
    }
    console.log(this.model.subItems[num].periodEndDate)
       
}
formatDateFordatePicker(strDate): any {
    if (strDate != null) {
        var setDate = new Date(strDate);
        return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
    } else {
        return null;
    }
}
updateStatus() {
    var date = new Date(this.payScheduleBeggingDate.date.year+"-"+this.payScheduleBeggingDate.date.month+"-"+this.payScheduleBeggingDate.date.day);
    this.model.payScheduleBeggingDate=date.getTime();
    
    this.closeModal();
    
    this.model.subItems.forEach(element => {
        if(element.periodStartDate.date != undefined){
            var startDate = new Date(element.periodStartDate.date.year+"-"+element.periodStartDate.date.month+"-"+element.periodStartDate.date.day);
            element.periodStartDate=startDate.getTime();
        }
        if(element.periodEndDate.date != undefined){
            var endDate = new Date(element.periodEndDate.date.year+"-"+element.periodEndDate.date.month+"-"+element.periodEndDate.date.day);
            element.periodEndDate=endDate.getTime();
        }
        
       
        
    });
    //Call service api for updating selected department
    this.model.payScheduleId = this.payScheduleId;
    if(this.model.subItems.length > 0){
    this.payScheduleSetupService.updatePayScheduleSetup(this.model).then(data => {
        var datacode = data.code;

        if (datacode == 201) {
            //Refresh the Grid data after editing department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.clearData();
            //Scroll to top after editing department
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = data.btiMessage.message;
                this.showCreateForm = false;
            }, 100);
            this.hasMessage = false;
            this.hasMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
        }
        this.clearData();
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
                window.scrollTo(0, 0);
            }, 100)
        });
        }else{
            this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = "Please enter atleast one Pay Schedule Setup Detail record.";
                        window.scrollTo(0, 0);
                    }, 100)
                }
        }

closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
}

  Create() {
    this.clearData();
    this.showCreateForm = false;
    this.isUnderUpdate = false;
    this.isEndDateValid = true;
    setTimeout(() => {
        this.showCreateForm = true;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }, 10);
    this.model = {
        id:0,
        payScheduleIndexId : 0,
        payScheduleId :'',
        payScheduleDescription:'',
        payScheduleDescriptionArabic:'',
        paySchedulePayPeriod:'0', 
        payScheduleBeggingDate:'', 
        payScheduleYear:'',
        subItems:[],
        payScheduleLocation:[],
        payScheduleSetupDepartments:[],
        payScheduleSetupEmployees:[],
        payScheduleSetupPositions:[]
    };
    let subItemsObj = {
        periodId:"0",
        year:"",
        periodName:"",
        periodStartDate:"",
        periodEndDate:"",
        invalid:false
    };
    this.model.subItems.push(Object.assign({}, subItemsObj));
    
}

  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
  }

  sortColumn(val){
    if( this.page.sortOn == val ) {
        if( this.page.sortBy == 'DESC' ) {
            this.page.sortBy = 'ASC';
        } else {
            this.page.sortBy = 'DESC';
        }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
 }
 sortColumnPeriod(val){
    if( this.pagePeriod.sortOn == val ) {
        if( this.pagePeriod.sortBy == 'DESC' ) {
            this.pagePeriod.sortBy = 'ASC';
        } else {
            this.pagePeriod.sortBy = 'DESC';
        }
    }
    this.pagePeriod.sortOn = val;
    this.setPagePeriod({ offset: 0, sortOn: this.pagePeriod.sortOn, sortBy: this.pagePeriod.sortBy });
 }

 sortColumnPosition(val){
    if( this.pagePosition.sortOn == val ) {
        if( this.pagePosition.sortBy == 'DESC' ) {
            this.pagePosition.sortBy = 'ASC';
        } else {
            this.pagePosition.sortBy = 'DESC';
        }
    }
    this.pagePosition.sortOn = val;
    this.setPagePosition({ offset: 0, sortOn: this.pagePosition.sortOn, sortBy: this.pagePosition.sortBy });
 }

 sortColumnLocation(val){
    if( this.pageLocation.sortOn == val ) {
        if( this.pageLocation.sortBy == 'DESC' ) {
            this.pageLocation.sortBy = 'ASC';
        } else {
            this.pageLocation.sortBy = 'DESC';
        }
    }
    this.pageLocation.sortOn = val;
    this.setPageLocation({ offset: 0, sortOn: this.pageLocation.sortOn, sortBy: this.pageLocation.sortBy });
 }

 sortColumnEmployee(val){
    if( this.pageEmployee.sortOn == val ) {
        if( this.pageEmployee.sortBy == 'DESC' ) {
            this.pageEmployee.sortBy = 'ASC';
        } else {
            this.pageEmployee.sortBy = 'DESC';
        }
    }
    this.pageEmployee.sortOn = val;
    this.setPageEmployee({ offset: 0, sortOn: this.pageEmployee.sortOn, sortBy: this.pageEmployee.sortBy });
 }

 sortColumnDepartment(val){
    if( this.pageDepartment.sortOn == val ) {
        if( this.pageDepartment.sortBy == 'DESC' ) {
            this.pageDepartment.sortBy = 'ASC';
        } else {
            this.pageDepartment.sortBy = 'DESC';
        }
    }
    this.pageDepartment.sortOn = val;
    this.setPageDepartment({ offset: 0, sortOn: this.pageDepartment.sortOn, sortBy: this.pageDepartment.sortBy });
 }

 onSelect({ selected }) {
  this.selected.splice(0, this.selected.length);
  this.selected.push(...selected);
 }
 onSelectPeriod({ selectedPeriod }) {
    this.selectedPeriod.splice(0, this.selectedPeriod.length);
    this.selectedPeriod.push(...selectedPeriod);
  }
  onSelectDepartment(selectedDepartment) {
    this.model.payScheduleSetupDepartments = [];
    console.log(selectedDepartment.selected);
    selectedDepartment.selected.forEach(element => {
        this.model.payScheduleSetupDepartments.push({
            "departmentId":element.id,
            "departmentDescription": element.departmentDescription,
            "arabicDepartmentDescription" : element.arabicDepartmentDescription,
            "payScheduleStatus" : element.payScheduleStatus
          })
    });
   // this.selectedDepartment.splice(0, this.selectedDepartment.length);
   // this.selectedDepartment.push(...selectedDepartment);
  }
  onSelectPosition( selectedPosition ) {
    this.model.payScheduleSetupPositions = [];
    selectedPosition.selected.forEach(element => {
        this.model.payScheduleSetupPositions.push({
            "positionDescription": element.positionDescription,
            "arabicDescription": element.positionDescriptionArabic,
            "payScheduleStatus": "false",
            "positionId": element.id
          })
    });

    //this.selectedPosition.splice(0, this.selectedPosition.length);
    //this.selectedPosition.push(...selectedPosition);
  }
  onSelectLocation( selectedLocation ) {
    this.model.payScheduleLocation = [];
    this.model.payScheduleLocation.splice(0, this.model.payScheduleLocation.length);
    selectedLocation.selected.forEach(element => {
        this.model.payScheduleLocation.push({
            "locationId": element.id,
            "locationName": element.description,
            "payScheduleStatus":"false"
          })
    });
  }
  onSelectEmployee( selectedEmployee ) {
    this.model.payScheduleSetupEmployees = [];
    console.log(this.selectedEmployee);
    selectedEmployee.selected.forEach(element => {
        console.log(element)
        this.model.payScheduleSetupEmployees.push({
            "employeeIndexId":element.employeeId,
            "paySchedulEmployeeIndexId":element.paySchedulEmployeeIndexId,
            "payScheduleStatus":"true"
          })
          console.log(this.model.payScheduleSetupEmployees);
    });
    console.log(this.model);
  }
  cancelModel(){
      console.log("cancel00")
    this.model = {
        id:0,
        payScheduleIndexId : 0,
        payScheduleId :'',
        payScheduleDescription:'',
        payScheduleDescriptionArabic:'',
        paySchedulePayPeriod:'0', 
        payScheduleBeggingDate:'', 
        payScheduleYear:'',
        subItems:[],
        payScheduleLocation:[],
        payScheduleSetupDepartments:[],
        payScheduleSetupEmployees:[],
        payScheduleSetupPositions:[]
    };
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }
  date:Date;
    edit(row: PayScheduleSetup) {
        this.clearData();
        this.showCreateForm = true;
        this.isEndDateValid = true;
        this.model = {
            id:0,
            payScheduleIndexId : 0,
            payScheduleId :'',
            payScheduleDescription:'',
            payScheduleDescriptionArabic:'',
            paySchedulePayPeriod:'0', 
            payScheduleBeggingDate:'', 
            payScheduleYear:'',
            subItems:[],
            payScheduleLocation:[],
            payScheduleSetupDepartments:[],
            payScheduleSetupEmployees:[],
            payScheduleSetupPositions:[]
        };
        this.isUnderUpdate = true;
        this.model =  JSON.parse(JSON.stringify(row));
        this.selectedPayPeriod = this.paySchedule[Number (this.model.paySchedulePayPeriod) - 1];
        console.log(this.model);
        var setDate = new Date(this.model.payScheduleBeggingDate);
        let d = { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        this.payScheduleBeggingDate = d;
        console.log(this.model.payScheduleBeggingDate);

        this.model.subItems.forEach(element => {
            element.periodStartDate = this.formatDateFordatePicker(element.periodStartDate);
            element.periodEndDate = this.formatDateFordatePicker(element.periodEndDate);
        });

        this.payScheduleId = true;
        this.payScheduleId = row.payScheduleId;
        this.payScheduleIdValue = this.model.payScheduleId;
        console.log(this.model.payScheduleLocation);
        this.model.payScheduleLocation.forEach(element => {
            this.rowsLocation.forEach(jelement => {
                if (jelement.id == element.locationId) {
                    this.selectedLocation.push(jelement);
                }
            });
        });
        //
        this.model.payScheduleSetupDepartments.forEach(element => {
            this.rowsDepartment.forEach(jelement => {
                if (jelement.id == element.departmentId) {
                    this.selectedDepartment.push(jelement);
                }
            });
        });
        this.model.payScheduleSetupEmployees.forEach(element => {
            console.log('roesEmployee',this.rowsEmployee)
            console.log('element',element)
            this.rowsEmployee.forEach(jelement => {
                if (jelement.employeeId == element.employeeId) {
                    this.selectedEmployee.push(jelement);
                }
            });
        });
        this.model.payScheduleSetupPositions.forEach(element => {
            console.log('Positions',this.rowsPosition)
            console.log('element',element)
            this.rowsPosition.forEach(jelement => {
                if (jelement.id == element.positionId) {
                    this.selectedPosition.push(jelement);
                }
            });
        });
        
    this.allPosition = "0";
    this.allDepartment = "0";
    this.allEmployee = "0";
    this.allLocation = "0";
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }
date1:Date;

    getSuperviserIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getPaySchedule.filter((id: any) => {
                return query.test(id.payScheduleId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        console.log('Selected value: ', e);
        let payload = {
            payScheduleIndexId: e.item.payScheduleIndexId
        }
        this.payScheduleSetupService.getPayScheduleSetupById(payload).then(pagedData => {
            console.log('pagedData', pagedData);
            this.model = pagedData.result;
            this.model.payScheduleIndexId = 0;
            this.payScheduleBeggingDate = '';
            let setDate = new Date(this.model.payScheduleBeggingDate);
            let d = { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
            this.payScheduleBeggingDate = d;
            // console.log(this.model.payScheduleBeggingDate);
        });
    }

setPage(pageInfo) {
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if( pageInfo.sortOn == undefined ) {
        this.page.sortOn = this.page.sortOn;
    } else {
        this.page.sortOn = pageInfo.sortOn;
    }
    if( pageInfo.sortBy == undefined ) {
        this.page.sortBy = this.page.sortBy;
    } else {
        this.page.sortBy = pageInfo.sortBy;   
    }
    this.page.searchKeyword = '';
    this.payScheduleSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
      
    });
}

getUserDate(date){
    let d = new Date(date);
    return d;
}
deleteRow(num){
    this.model.subItems.splice(num, 1);
}

setPagePeriod(pageInfo) {
    this.selectedPeriod = []; // remove any selectedPeriod checkbox on paging
    this.pagePeriod.pageNumber = pageInfo.offset;
    if( pageInfo.sortOn == undefined ) {
        this.pagePeriod.sortOn = this.pagePeriod.sortOn;
    } else {
        this.pagePeriod.sortOn = pageInfo.sortOn;
    }
    if( pageInfo.sortBy == undefined ) {
        this.pagePeriod.sortBy = this.pagePeriod.sortBy;
    } else {
        this.pagePeriod.sortBy = pageInfo.sortBy;   
    }
    this.pagePeriod.searchKeyword = '';
    this.payScheduleSetupPeriodService.getlist(this.pagePeriod, this.searchKeyword).subscribe(pagedData => {
      this.pagePeriod = pagedData.page;
      this.rowsPeriod = pagedData.data;
      console.log(pagedData);
    });
}

setPagePosition(pageInfo) {
    this.selectedPosition = []; // remove any selectedPosition checkbox on paging
    this.pagePosition.pageNumber = pageInfo.offset;
    if( pageInfo.sortOn == undefined ) {
        this.pagePosition.sortOn = this.pagePosition.sortOn;
    } else {
        this.pagePosition.sortOn = pageInfo.sortOn;
    }
    if( pageInfo.sortBy == undefined ) {
        this.pagePosition.sortBy = this.pagePosition.sortBy;
    } else {
        this.pagePosition.sortBy = pageInfo.sortBy;   
    }
    this.pagePosition.searchKeyword = '';
    this.payScheduleSetupPositionService.getlist(this.pagePosition, this.searchKeyword).subscribe(pagedData => {
      this.pagePosition = pagedData.page;
      this.rowsPosition = pagedData.data;
      this.rowsPositionTemp = pagedData.data;
      console.log(pagedData);
    });
}

setPageLocation(pageInfo) {
    this.selectedLocation = []; // remove any selectedLocation checkbox on paging
    this.pageLocation.pageNumber = pageInfo.offset;
    if( pageInfo.sortOn == undefined ) {
        this.pageLocation.sortOn = this.pageLocation.sortOn;
    } else {
        this.pageLocation.sortOn = pageInfo.sortOn;
    }
    if( pageInfo.sortBy == undefined ) {
        this.pageLocation.sortBy = this.pageLocation.sortBy;
    } else {
        this.pageLocation.sortBy = pageInfo.sortBy;   
    }
    this.pageLocation.searchKeyword = '';
    this.payScheduleSetupLocationService.getlist(this.pageLocation, this.searchKeyword).subscribe(pagedData => {
    this.pageLocation = pagedData.page;
    this.rowsLocation = pagedData.data;
    this.rowsLocationTemp = pagedData.data;
    console.log(pagedData);
    });
}

setPageDepartment(pageInfo) {
    this.selectedDepartment = []; // remove any selectedDepartment checkbox on paging
    this.pageDepartment.pageNumber = pageInfo.offset;
    if( pageInfo.sortOn == undefined ) {
        this.pageDepartment.sortOn = this.pageDepartment.sortOn;
    } else {
        this.pageDepartment.sortOn = pageInfo.sortOn;
    }
    if( pageInfo.sortBy == undefined ) {
        this.pageDepartment.sortBy = this.pageDepartment.sortBy;
    } else {
        this.pageDepartment.sortBy = pageInfo.sortBy;   
    }
    this.pageDepartment.searchKeyword = '';
    this.payScheduleSetupDepartmentService.getlist(this.pageDepartment, this.searchKeyword).subscribe(pagedData => {
    this.pageDepartment = pagedData.page;
    this.rowsDepartment = pagedData.data;
    this.rowsDepartmentTemp = pagedData.data;
    console.log(pagedData);
    });
}

setPageEmployee(pageInfo) {
    this.selectedEmployee = []; // remove any selectedDepartment checkbox on paging
    this.pageEmployee.pageNumber = pageInfo.offset;
    if( pageInfo.sortOn == undefined ) {
        this.pageEmployee.sortOn = this.pageEmployee.sortOn;
    } else {
        this.pageEmployee.sortOn = pageInfo.sortOn;
    }
    if( pageInfo.sortBy == undefined ) {
        this.pageEmployee.sortBy = this.pageEmployee.sortBy;
    } else {
        this.pageEmployee.sortBy = pageInfo.sortBy;   
    }
    this.pageEmployee.searchKeyword = '';
    this.payScheduleSetupEmployeeService.getlist(this.pageEmployee, this.searchKeyword).subscribe(pagedData => {
    this.pageEmployee = pagedData.page;
    this.rowsEmployee = pagedData.data;
    this.rowsEmployeeTemp = pagedData.data;
    console.log(pagedData);
    });
}

Clear(f: NgForm) {
    f.resetForm({paySchedulePayPeriod:'0' });
    this.model.subItems=[];
    let subItemsObj = {
        periodId:"0",
        year:"",
        periodName:"",
        periodStartDate:"",
        periodEndDate:"",
        invalid:false
    };
    
    this.model.subItems.push(Object.assign({}, subItemsObj));
    this.selectedDepartment=[];
    this.selectedEmployee=[];
    this.selectedLocation=[];
    this.selectedPosition=[];
  
}

clearData(){
    this.selectedEmployee = [];
    this.selectedDepartment = [];
    this.selectedLocation = [];
    this.selectedPeriod = [];
    this.selectedPosition = [];
    this.allPosition = "0";
    this.allDepartment = "0";
    this.allEmployee = "0";
    this.allLocation = "0";
    this.startDate="";
    this.payScheduleBeggingDate="";
    this.endDate="";
}

varifyDelete() {
    if (this.selected.length > 0) {
        this.showCreateForm = false;
        this.isDeleteAction = true;
        this.isConfirmationModalOpen = true;
    }
    else {
        this.isSuccessMsg = false;
        this.hasMessage = true;
        this.message.type = 'error';
        this.isfailureMsg = true;
        this.showMsg = true;
        this.message.text = 'Please select at least one record to delete.';
        window.scrollTo(0, 0);
    }
}

delete() {
   var selectedInterviewTypeSetups = [];
    for (var i = 0; i < this.selected.length; i++) {
        selectedInterviewTypeSetups.push(this.selected[i].payScheduleIndexId);
    }
    this.payScheduleSetupService.deletePayScheduleSetup(selectedInterviewTypeSetups).then(data => {
        var datacode = data.code;
        if (datacode == 200) {
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        }
        this.hasMessage = true;
        this.message.type = "success";
        window.scrollTo(0, 0);
        window.setTimeout(() => {
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = data.btiMessage.message;
        }, 100);
        this.clearData();
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }).catch(error => {
        this.hasMessage = true;
        this.message.type = "error";
        var errorCode = error.status;
        this.message.text = "Server issue. Please contact admin.";
    });
    this.closeModal();
}

addSubItem(subItemData: NgForm, subitem){
    let subItemsObj = {
        periodId:null,
        year:"",
        periodName:"",
        periodStartDate:"",
        periodEndDate:"",
    };
    this.model.subItems.push(Object.assign({}, subItemsObj));
 }
 selectedPayPeriod = {
    id:0,name:"", value:"", days:0, period:0
 };
 setPayPeriod(event){
     this.selectedPayPeriod = this.paySchedule[Number (event-1)];

    this.onStartDateChanged(this.payScheduleBeggingDate);

   console.log(this.selectedPayPeriod);
   
}
daysInMonth(year) {
   let days = [];
   for(var  i = 1; i < 13; i++) {
       days.push( new Date(year, i, 0).getDate())
   }
   return days;
}
 
    getPayloadValue(id) {
        let selectedNode = { id: 0, name: "", value: "", days: 0 };
        this.paySchedule.forEach(element => {
            if (element.id == id)
                selectedNode = element;
        });
        return selectedNode.name;
    }
    setPeriodName(event, num) {
        this.model.subItems[num].periodName = this.paySchedule[Number(event) - 1].value;
    }
    clearDate() {

    }

    allLocation:string = "0";
    allPosition:string = "0";
    allDepartment:string = "0";
    allEmployee:string = "0";
    removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }
    @ViewChild('locationGrid') locationGrid;
    getSelectedLocation(event){
        this.rowsLocation = [];
        if(parseInt(this.allLocation) == 0){
            this.rowsLocation = this.rowsLocationTemp;
        }else if(parseInt(this.allLocation) == 1){
            this.rowsLocationTemp.length?
                this.rowsLocationTemp.forEach(element => {
                    let status = 0;
                    this.model.payScheduleLocation != undefined && this.model.payScheduleLocation.length > 0?
                        this.model.payScheduleLocation.forEach(temp => {
                            if(temp.locationId == element.id){
                                status = 1;
                            }
                        }):null
                    if(status == 1){
                        this.rowsLocation.push(element);
                    }
                }): null
        }else if(parseInt(this.allLocation) == 2){
            this.rowsLocationTemp.length?
                this.rowsLocationTemp.forEach(element => {
                    let status = 0;
                    this.model.payScheduleLocation != undefined && this.model.payScheduleLocation.length > 0?
                        this.model.payScheduleLocation.forEach(temp => {
                            if(temp.locationId == element.id){
                                status = 1;
                            }
                        }): null
                    if(status == 0){
                        this.rowsLocation.push(element);
                    }
                }): null
        }
       // this.rowsLocation = this.removeDuplicates(this.rowsLocation, 'id');
       //this.rowsLocation = rowsLocation1;
     //   this.pageLocation.totalElements = this.rowsLocation.length;
        this.locationGrid.offset = 0;
        console.log(this.rowsLocation);
       }


       @ViewChild('positionGrid') positionGrid;
       getSelectedPosition(event){
           this.rowsPosition = [];
           if (parseInt(this.allPosition) == 0) {
               this.rowsPosition = this.rowsPositionTemp;
           } else if (parseInt(this.allPosition) == 1) {
               this.rowsPositionTemp.forEach(element => {
                   let status = 0;
                   this.model.payScheduleSetupPositions.forEach(temp => {
                       if (temp.positionId == element.id) {
                           status = 1;
                       }
                   })
                   if (status == 1) {
                       this.rowsPosition.push(element);
                   }
               })
           } else if (parseInt(this.allPosition) == 2) {
               this.rowsPositionTemp.forEach(element => {
                   let status = 0;
                   this.model.payScheduleSetupPositions.forEach(temp => {
                       if (temp.positionId == element.id) {
                           status = 1;
                       }
                   })
                   if (status == 0) {
                       this.rowsPosition.push(element);
                   }
               })
           }
           // this.rowsPosition = this.removeDuplicates(this.rowsPosition, 'id');
           //this.pagePosition.totalElements = this.rowsPosition.length;
           this.positionGrid.offset = 0;
           console.log(this.rowsPosition);
       }

       @ViewChild('departmentGrid') departmentGrid;
       getSelectedDepartment(event){
           this.rowsDepartment = [];
           if (parseInt(this.allDepartment) == 0) {
               this.rowsDepartment = this.rowsDepartmentTemp;
           } else if (parseInt(this.allDepartment) == 1) {
               this.rowsDepartmentTemp.forEach(element => {
                   let status = 0;
                   this.model.payScheduleSetupDepartments.forEach(temp => {
                       if (temp.departmentId == element.id) {
                           status = 1;
                       }
                   })
                   if (status == 1) {
                       this.rowsDepartment.push(element);
                   }
               })
           } else if (parseInt(this.allDepartment) == 2) {
               this.rowsDepartmentTemp.forEach(element => {
                   let status = 0;
                   this.model.payScheduleSetupDepartments.forEach(temp => {
                       if (temp.departmentId == element.id) {
                           status = 1;
                       }
                   })
                   if (status == 0) {
                       this.rowsDepartment.push(element);
                   }
               })
           }
           // this.rowsDepartment = this.removeDuplicates(this.rowsDepartment, 'id');
          // this.pageDepartment.totalElements = this.rowsDepartment.length;
           this.departmentGrid.offset = 0;
           console.log(this.rowsDepartment);
       }

       @ViewChild('employeeGrid') employeeGrid;
       getSelectedEmployee(event){
        this.rowsEmployee = [];
           if (parseInt(this.allEmployee) == 0) {
            this.rowsEmployee = this.rowsEmployeeTemp;
           } else if (parseInt(this.allEmployee) == 1) {
               this.rowsEmployeeTemp.forEach(element => {
                   let status = 0;
                   this.model.payScheduleSetupEmployees.forEach(temp => {
                       console.log(temp)
                       console.log(element)
                       if (temp.employeeIndexId == element.employeeId) {
                           status = 1;
                       }
                   })
                   if (status == 1) {
                    this.rowsEmployee.push(element);
                   }
               })
           } else if (parseInt(this.allEmployee) == 2) {
               this.rowsEmployeeTemp.forEach(element => {
                   let status = 0;
                   this.model.payScheduleSetupEmployees.forEach(temp => {
                       if (temp.employeeIndexId == element.employeeId) {
                           status = 1;
                       }
                   })
                   if (status == 0) {
                    this.rowsEmployee.push(element);
                   }
               })
           }
           // this.rowsEmployee = this.removeDuplicates(this.rowsEmployee, 'id');
           //this.rowsEmployee = rowsEmployee1;           
           //  this.pageEmployee.totalElements = this.rowsEmployee.length;
           this.employeeGrid.offset = 0;
           console.log(this.rowsEmployee);
       }

       makeValid(num){
           this.model.subItems[num].invalid = false;
       }
} 
 
