import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Page } from '../../../_sharedresource/page';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { PositionAttchmentService } from '../../_services/position-attachment/position-attchment.service';
import { PositionAttachmentModule } from '../../_models/position-attachment/position-attachment.module';
import 'rxjs/Rx';

import { UploadFileService } from '../../../_sharedresource/_services/upload-file.service';
import { DomSanitizer } from '@angular/platform-browser';
import { isType } from '@angular/core/src/type';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
    selector: 'positionAttachment',
    templateUrl: './position-attachment.component.html',
    styleUrls: ['./position-attachment.component.css'],
    providers: [PositionAttchmentService]
})
export class PositionAttachmentComponent implements OnInit {
    @ViewChild('file') file;

    page = new Page();
    // pageAtt = {
    //     "pageNumber": 0,
    //     "pageSize": "5",
    //     "totalCount": "0",
    // }
    rows: any = [];
    temp = new Array<PositionAttachmentModule>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1319';
    moduleName;
    screenName;
    // filemessage = '';
    defaultFormValues: Array<any> = [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    positionPlanId = {};
    positionIds = '';
    positionDesc = '';
    documentAttachmentName = '';
    startDate;
    endDate;
    searchKeyword = '';
    ddPageSize: number = 5;
    model: PositionAttachmentModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    private sub: any;
    positionPid: number;
    positionData: any;
    isAttachedFile: boolean;
    isattachFiles: boolean = false;
    url = '';
    fileName: any;
    isSelected: boolean = false;
    fileSelect: File;
    FileNotFetched: boolean = false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private positionAttchmentService: PositionAttchmentService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private route: ActivatedRoute,
        private uploadFileService: UploadFileService,
        private _DomSanitizationService: DomSanitizer) {

        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        // default form parameter for position  screen
        this.defaultFormValues = [
            { 'fieldName': 'POSITION_ATTACHMENT_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_TIME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_USER_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_PREVIEW_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_ATTACH_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' }
        ];
    }

    ngOnInit() {
        //console.log("PD:", this.uploadFileService);
        this.positionData = this.uploadFileService.positionModelData;
        if (!this.positionData) {
            this.router.navigate(['hcm/positionSetup']);
            return;
        }
        //console.log("PositionData:", this.positionData);
        if (this.positionData.id === 0) {
            this.Create();
        }

        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.positionPid = this.positionData.id;
        this.positionIds = this.positionData.positionId; // (+) converts string 'id' to a number
        this.positionDesc = this.positionData.description;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });



        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName;
            this.screenName = data.result.dtoScreenDetail.screenName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (let j = 0; j < this.availableFormValues.length; j++) {
                let fieldKey = this.availableFormValues[j]['fieldName'];
                let objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                let objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }

    // setting pagination
    setPage(pageInfo) {
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        // this.pageAtt.pageNumber = pageInfo.offset;
        //console.log("Page details : ",this.page);
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        //console.log("SearchWord:", this.searchKeyword);
        this.page.searchKeyword = this.searchKeyword;
        //console.log("Position page size",this.page.pageNumber);
        if (this.positionPid !== 0) {
            this.positionAttchmentService.getPositionClass(this.page, this.positionPid, this.page.searchKeyword).then(pagedData => {
                //console.log("Pagedatasssssssss:", pagedData);
                if (pagedData.code !== 404) {
                    // this.pageAtt = pagedData.result;                
                    this.page = pagedData.result;
                    this.page.size = pagedData.result.pageSize;
                    this.page.totalElements = pagedData.result.totalCount;
                    this.rows = pagedData.result.records;
                } else {
                    this.rows = [];
                    // this.pageAtt = this.pageAtt;
                    this.page = this.page;

                }

            });
        }

    }

    // Open form for create position class
    Create() {
        this.showCreateForm = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionIds: '',
            documentAttachmenDesc: '',
            attachmentType: null
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
        this.fileName = null;
        this.fileSelect = undefined;
        this.isSelected = false;
        this.url = "";
    }

    Cancel(f: NgForm) {
        this.fileName = null;
        this.fileSelect = undefined;
        this.isSelected = false;
        this.url = "";
        this.showCreateForm = false;

    }

    // function call for creating new positionClass
    CreatePositionAttachment(f: NgForm, event: Event) {
        event.preventDefault();

        //console.log("PositionAttachData:", this.uploadFileService);
        // this.model.positionIds = this.positionIds;

        //console.log('file:', this.file.nativeElement.files[0]);

        if (this.model.attachmentType !== null && this.model.documentAttachmenDesc !== "" && this.file.nativeElement.files[0] !== undefined) {
            this.isAttachedFile = true;
            this.uploadFileService.positionAttachData(this.model, this.file.nativeElement.files[0], this.isAttachedFile);
            //console.log("Attached Data:",this.uploadFileService);
            //console.log('file:', this.file.nativeElement.files[0]);
            //console.log("Model:",this.model);
            this.fileName = this.file.nativeElement.files[0].name;

            this.isattachFiles = false;
            this.router.navigate(['hcm/positionSetup']);
        } else {
            this.isattachFiles = true;
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please upload the file.';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 5000);
        }
    }

    download(row: PositionAttachmentModule) {
        this.positionAttchmentService.download(row.id, row["documentAttachmentName"]);
        /* this.positionAttchmentService.getAttachmentById(row.id).subscribe(data => { 
            window.open(data, '_blank');
        }); */
    }

    // edit position class by row
    edit(row: any) {
        //console.log("Row:", row);
        this.fileName = row.documentAttachmentName;
        this.FileNotFetched=true;
        this.showCreateForm = true;
        this.isSelected = false;        
        this.model = row;
        this.positionPlanId = row.positionIds;
        this.positionAttchmentService.getAttachmentById(row.id).subscribe(data => {
            this.url = data;
        });
        var x = this.positionAttchmentService.getFile(row.id,row.documentAttachmentName.split('.')[1]);
        x.subscribe((blob) => {
            this.fileSelect = new File([blob], row.documentAttachmentName, {type:blob.type});
            console.log("FILE FETCHED: ", this.fileSelect);
            if (this.fileSelect) {
                this.fileName = this.fileSelect.name;
                var reader = new FileReader();
                if (this.model.attachmentType != null) {
                    if (Number(this.model.attachmentType) == 1 &&
                        (this.fileSelect.type == "application/pdf" ||
                            this.fileSelect.type == "application/doc" ||
                            this.fileSelect.type == "text/plain" ||
                            this.fileSelect.type == "" ||
                            this.fileSelect.type == "application/msword" ||
                            this.fileSelect.type == "application/xlsx" ||
                            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.template" ||
                            this.fileSelect.type == "application/vnd.ms-word.document.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-word.template.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-excel" ||
                            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.template" ||
                            this.fileSelect.type == "application//vnd.ms-excel.sheet.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-excel.template.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-excel.addin.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-excel.sheet.binary.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-powerpoint" ||
                            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
                            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.template" ||
                            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.slideshow" ||
                            this.fileSelect.type == "application/vnd.ms-powerpoint.addin.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-powerpoint.presentation.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-powerpoint.template.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-powerpoint.slideshow.macroEnabled.12" ||
                            this.fileSelect.type == "application/vnd.ms-access")
                    )

                        this.isSelected = false;

                    else if (Number(this.model.attachmentType) == 2 &&
                        (this.fileSelect.type == "image/png" ||
                            this.fileSelect.type == "image/jpg" ||
                            this.fileSelect.type == "image/jpeg")
                    )
                        this.isSelected = false;
                    else
                        this.isSelected = true;
                }
                this.FileNotFetched=false;
                //reader.readAsDataURL(this.fileSelect);
            }
        });
        //this.file.nativeElement.files[0]=row.attachment;
        //console.log("URL: ", this.file.nativeElement.url);
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    onSelectFile(event) {
        //console.log("aaaa:", event.target.files[0]);
        this.fileName = null;

        if (event.target.files && event.target.files[0]) {
            this.fileName = event.target.files[0].name;
            this.fileSelect = event.target.files[0];
            var reader = new FileReader();
            if (this.model.attachmentType != null) {
                if (this.model.attachmentType == 1 &&
                    (this.fileSelect.type == "application/pdf" ||
                        this.fileSelect.type == "application/doc" ||
                        this.fileSelect.type == "text/plain" ||
                        this.fileSelect.type == "" ||
                        this.fileSelect.type == "application/msword" ||
                        this.fileSelect.type == "application/xlsx" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.template" ||
                        this.fileSelect.type == "application/vnd.ms-word.document.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-word.template.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.template" ||
                        this.fileSelect.type == "application//vnd.ms-excel.sheet.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel.template.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel.addin.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel.sheet.binary.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.template" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.slideshow" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.addin.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.presentation.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.template.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.slideshow.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-access")
                )

                    this.isSelected = false;

                else if (this.model.attachmentType == 2 &&
                    (this.fileSelect.type == "image/png" ||
                        this.fileSelect.type == "image/jpg" ||
                        this.fileSelect.type == "image/jpeg")
                )
                    this.isSelected = false;
                else
                    this.isSelected = true;
            }
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = (event: any) => { // called once readAsDataURL is completed 
                this.url = event.target.result;
            }
        }
    }

    checkAttachedFile(v) {
        if (this.fileSelect === undefined) {
            this.isSelected = false;
            return;
        }
        if (v == 1 &&
            (this.fileSelect.type == "application/pdf" ||
                this.fileSelect.type == "application/doc" ||
                this.fileSelect.type == "text/plain" ||
                this.fileSelect.type == "" ||
                this.fileSelect.type == "application/msword" ||
                this.fileSelect.type == "application/xlsx" ||
                this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.template" ||
                this.fileSelect.type == "application/vnd.ms-word.document.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-word.template.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-excel" ||
                this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.template" ||
                this.fileSelect.type == "application//vnd.ms-excel.sheet.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-excel.template.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-excel.addin.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-excel.sheet.binary.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-powerpoint" ||
                this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
                this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.template" ||
                this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.slideshow" ||
                this.fileSelect.type == "application/vnd.ms-powerpoint.addin.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-powerpoint.presentation.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-powerpoint.template.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-powerpoint.slideshow.macroEnabled.12" ||
                this.fileSelect.type == "application/vnd.ms-access")
        )

            this.isSelected = false;

        else if (v == 2 &&
            (this.fileSelect.type == "image/png" ||
                this.fileSelect.type == "image/jpg" ||
                this.fileSelect.type == "image/jpeg")
        )

            this.isSelected = false;
        else
            this.isSelected = true;
    }

    // else {
    //     this.isSelected = true; 
    //     if(this.model.attachmentType)
    //         this.filemessage = "Allowed files : png,jpg and jpeg";
    //     else
    //         this.filemessage  = "";
    // }


    navigateToPositionSetup() {
        //console.log("Go to Position setup!!");
        this.isAttachedFile = false;
        this.uploadFileService.positionAttchModelData = this.model;
        /* this.router.events.forEach((event) => {
           //console.log(event);
        }); */
        this.router.navigate(['hcm/positionSetup']);
    }

    updateStatus(f: NgForm) {
        if (this.isSelected || f.form.invalid) {
            return;
        }
        this.closeModal();
        this.uploadFileService.positionAttchModelData = this.model;
        //console.log("Attached File:", this.uploadFileService.positionAttchModelData);
        // Call service api for updating selected position class
        this.positionAttchmentService.updatePositionAttachment(this.model, this.fileSelect).then(data => {
            let datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;

                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 5000);
        }
    }
    // delete position class by passing whole object of perticular position class
    delete() {
        let selectedPositionAttachment = [];
        for (let i = 0; i < this.selected.length; i++) {
            selectedPositionAttachment.push(this.selected[i].id);
        }
        this.positionAttchmentService.deletePositionAttachment(selectedPositionAttachment).then(data => {
            let datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of position class
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            let errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search position class by keyword
    updateFilter(event) {

        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        //console.log("aaaaa:Search", this.searchKeyword);
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }


    // Set default page size
    changePageSize(event) {
        //console.log('CHHHHHHHHH : ',event);
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    confirm(): void {
        this.messageText = 'Confirmed';
        // this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}
