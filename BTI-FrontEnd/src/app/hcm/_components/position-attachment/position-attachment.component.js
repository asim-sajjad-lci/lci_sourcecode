"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var page_1 = require("../../../_sharedresource/page");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var router_1 = require("@angular/router");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var position_attchment_service_1 = require("../../_services/position-attachment/position-attchment.service");
require("rxjs/Rx");
var PositionAttachmentComponent = (function () {
    function PositionAttachmentComponent(router, positionAttchmentService, getScreenDetailService, alertService, route) {
        this.router = router;
        this.positionAttchmentService = positionAttchmentService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.route = route;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1319';
        this.message = { 'type': '', 'text': '' };
        this.positionPlanId = {};
        this.positionIds = '';
        this.positionDesc = '';
        this.selectedFile = '';
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for position  screen
        this.defaultFormValues = [
            { 'fieldName': 'POSITION_ATTACHMENT_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_TIME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_USER_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_PREVIEW_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_ATTACH_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' }
        ];
    }
    PositionAttachmentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.sub = this.route.params.subscribe(function (params) {
            _this.positionIds = params['positionId']; // (+) converts string 'id' to a number
            _this.positionDesc = params['positionDescription'];
            _this.setPage({ offset: 0 });
        });
        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            var _loop_1 = function (j) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            };
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                _loop_1(j);
            }
        });
    };
    // setting pagination
    PositionAttachmentComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.positionAttchmentService.getPositionClass(this.page, this.positionIds).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create position class
    PositionAttachmentComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionIds: '',
            documentAttachmenDesc: '',
            attachmentType: 0,
            attachmentDate: new Date,
            attachmentTime: new Date,
            userId: 0,
            documentAttachmentName: ''
        };
    };
    // Clear form to reset to default blank
    PositionAttachmentComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    // function call for creating new positionClass
    PositionAttachmentComponent.prototype.CreatePositionAttachment = function (f, event) {
        var _this = this;
        event.preventDefault();
        this.model.positionIds = this.positionIds;
        //console.log('file:', this.file.nativeElement.files[0]);
        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            this.positionAttchmentService.createPositionAttachment(this.model, this.file.nativeElement.files[0]).then(function (data) {
                var datacode = data.code;
                if (datacode === 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(function () {
                        _this.isSuccessMsg = true;
                        _this.isfailureMsg = false;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.hasMsg = false;
                        }, 4000);
                        _this.showCreateForm = false;
                        _this.messageText = data.btiMessage.message;
                    }, 100);
                    _this.hasMsg = true;
                    f.resetForm();
                    // Refresh the Grid data after adding new position class
                    _this.setPage({ offset: 0 });
                    //this.router.navigate(['hcm/positionSetup']);
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    PositionAttachmentComponent.prototype.download = function (row) {
        this.positionAttchmentService.getAttachmentById(row.id).subscribe(function (data) {
            window.open(data, '_blank');
        });
    };
    // edit position class by row
    PositionAttachmentComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = row;
        this.positionPlanId = row.positionIds;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    PositionAttachmentComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        // Call service api for updating selected position class
        this.positionAttchmentService.updatePositionAttachment(this.model, this.file.nativeElement.files[0]).then(function (data) {
            var datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                _this.setPage({ offset: 0 });
                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    };
    PositionAttachmentComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    // delete position class by passing whole object of perticular position class
    PositionAttachmentComponent.prototype.delete = function () {
        var _this = this;
        var selectedPositionAttachment = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedPositionAttachment.push(this.selected[i].id);
        }
        this.positionAttchmentService.deletePositionAttachment(selectedPositionAttachment).then(function (data) {
            var datacode = data.code;
            if (datacode === 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            // this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            // Refresh the Grid data after deletion of position class
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    PositionAttachmentComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search position class by keyword
    PositionAttachmentComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.positionAttchmentService.searchPositionAttachmentlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    PositionAttachmentComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    PositionAttachmentComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed';
        // this.modalRef.hide();
        this.delete();
    };
    PositionAttachmentComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    return PositionAttachmentComponent;
}());
__decorate([
    core_1.ViewChild('file'),
    __metadata("design:type", Object)
], PositionAttachmentComponent.prototype, "file", void 0);
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], PositionAttachmentComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], PositionAttachmentComponent.prototype, "myScrollContainer", void 0);
PositionAttachmentComponent = __decorate([
    core_1.Component({
        selector: 'positionAttachment',
        templateUrl: './position-attachment.component.html',
        styleUrls: ['./position-attachment.component.css'],
        providers: [position_attchment_service_1.PositionAttchmentService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        position_attchment_service_1.PositionAttchmentService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService,
        router_1.ActivatedRoute])
], PositionAttachmentComponent);
exports.PositionAttachmentComponent = PositionAttachmentComponent;
//# sourceMappingURL=position-attachment.component.js.map