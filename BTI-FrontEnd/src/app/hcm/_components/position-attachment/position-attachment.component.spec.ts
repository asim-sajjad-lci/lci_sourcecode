import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionAttachmentComponent } from './position-attachment.component';

describe('PositionAttachmentComponent', () => {
  let component: PositionAttachmentComponent;
  let fixture: ComponentFixture<PositionAttachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionAttachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
