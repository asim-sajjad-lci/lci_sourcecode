import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildPayrollCheckDeductionsComponent } from './build-payroll-check-deductions.component';

describe('BuildPayrollCheckDeductionsComponent', () => {
  let component: BuildPayrollCheckDeductionsComponent;
  let fixture: ComponentFixture<BuildPayrollCheckDeductionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildPayrollCheckDeductionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildPayrollCheckDeductionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
