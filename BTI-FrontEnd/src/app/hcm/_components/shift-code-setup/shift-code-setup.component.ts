import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../_sharedresource/page';
import {Constants} from '../../../_sharedresource/Constants';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Router} from '@angular/router';
import {ShiftCodeSetupModule} from '../../_models/shift-code-setup/shift-code-setup.module';
import {ShiftCodeSetupService} from '../../_services/shift-code-setup/shift-code-setup.service';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {NgForm} from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

@Component({
    selector: 'shiftCodeSetup',
    templateUrl: './shift-code-setup.html',
    providers : [ShiftCodeSetupService]
})
export class ShiftCodeSetupComponent {
    page = new Page();
    rows = new Array<ShiftCodeSetupModule>();
    temp = new Array<ShiftCodeSetupModule>();
    selected = [];
    countries = [];
    states = [];
    cities = [];
    modelState = '';
    moduleCode = 'M-1011';
    screenCode = 'S-1426';
    moduleName;
    screenName;
    defaultFormValues:Array<any> =  [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    shiftCodeId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: ShiftCodeSetupModule;
    showCreateForm: boolean = false;
	islifeTimeValidEmployerpay:boolean=true;
	islifeTimepercent:boolean=true;
	tempp:string[]=[];
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    shiftCodeIdvalue : string;
    isShiftcodeFormdisabled: boolean = false;

    dataSource: Observable<any>;
    shiftCodeIdSearch = [];
    typeaheadLoading: boolean;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
                private shiftCodeSetupService: ShiftCodeSetupService,
                private getScreenDetailService: GetScreenDetailService,
                private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            {'fieldName': 'SHIFT_CODE_SEARCH_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_CODE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_DECRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_INACTIVE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_SHIFT_PREMIUM', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_AMOUNT', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_PERCENT', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_ACTION_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'SHIFT_CODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''}
        ];
        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.shiftCodeId);
          }).mergeMap((token: string) => this.getReportShiftCodeAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
        this.shiftCodeSetupService.getShiftCodeIdList().then(data => {
            this.shiftCodeIdSearch = data.result.records;                
            console.log("Data class options : ", this.shiftCodeIdSearch);
        });

    }
    // Exiting Id Search on every click
   getReportShiftCodeAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'ig');
    return Observable.of(
      this.shiftCodeIdSearch.filter((id: any) => {
        return query.test(id.shiftCodeId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
 
  typeaheadOnSelect(e: TypeaheadMatch): void {
    console.log('Selected value: ', e.value);
  }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.shiftCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            
        });
        

        
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.isShiftcodeFormdisabled = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            shiftCodeId: '',
            desc: '',
            arabicDesc: '',
            inActive: false,
            shitPremium: 1,
            amount: null,
            percent: null
        
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({id: 0,
            shiftCodeId: '',
            desc: '',
            arabicDesc: '',
            inActive: false,
            shitPremium: 1,
            amount: null,
            percent: null});
            this.isShiftcodeFormdisabled = false;
    }


    //function call for creating new location
    createShiftCodeSetup(f: NgForm, event: Event) {
        event.preventDefault();
        var locIdx = this.model.shiftCodeId;

        console.log("In createShiftCodeSetup");
        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else{
            //Check for duplicate ShiftCodeSetup Id according to it create new location
            this.shiftCodeSetupService.checkDuplicateShiftCodeSetupId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new location
                    this.shiftCodeSetupService.createShiftCodeSetup(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.hasMessage = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new location
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: ShiftCodeSetupModule) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.shiftCodeId = row.shiftCodeId;
        this.isUnderUpdate = true;
        this.shiftCodeIdvalue = this.model.shiftCodeId;
        if(this.model.inActive==true){
            this.isShiftcodeFormdisabled = true;
        } else{
            this.isShiftcodeFormdisabled = false;
        }
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    updateStatus() {
        this.closeModal();
        this.model.shiftCodeId = this.shiftCodeIdvalue;
        //Call service api for updating selected department
        
                //Call service api for Creating new location
                this.shiftCodeSetupService.updateShiftCodeSetup(this.model).then(data => {
                    var datacode = data.code;
                    if (datacode == 201) {
                        //Refresh the Grid data after editing department
                        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                        //Scroll to top after editing department
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.messageText = data.btiMessage.message;
                            ;
                            this.showCreateForm = false;
                        }, 100);
                        this.hasMessage = false;
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = 'Server error. Please contact admin.';
                    }, 100)
                });
            /*}*/

    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedShiftCodeSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedShiftCodeSetups.push(this.selected[i].id);
        }
        this.shiftCodeSetupService.deleteShiftCodeSetup(selectedShiftCodeSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.    ';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.shiftCodeSetupService.searchShiftCodeSetuplist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event){
        if(isNaN(event.target.value) == true ) {
            this.model.id=0;
            return false;
        }
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkShiftcodeForm(event){
        if(event.target.checked==true){
            this.isShiftcodeFormdisabled = true;
        } else{
            this.isShiftcodeFormdisabled = false;
        }

    }
	
	checkdecimalemployeepay(digit)
    {
        //console.log(digit);
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            //console.log("in the condition");
            this.islifeTimeValidEmployerpay = false;
            //console.log(this.islifeTimeValidEmployerpay);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            //console.log("in the condition");
            this.islifeTimeValidEmployerpay = false;
            //console.log(this.islifeTimeValidEmployerpay);
        }
        else{
            this.islifeTimeValidEmployerpay = true;
            //console.log(this.islifeTimeValidEmployerpay);
        }

    }
	checkdecimalpercent(digit)
    {
        //console.log(digit);
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 5) || (this.tempp[1] != null && this.tempp[1].length > 5))){
            //console.log("in the condition");
            this.islifeTimepercent = false;
            //console.log(this.islifeTimepercent);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))){
            //console.log("in the condition");
            this.islifeTimepercent = false;
            //console.log(this.islifeTimepercent);
        }

        else{
            this.islifeTimepercent = true;
            //console.log(this.islifeTimepercent);
        }

    }

}
