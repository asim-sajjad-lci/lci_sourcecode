import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Page} from '../../../_sharedresource/page';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {PositionBudgetService} from '../../_services/position-budget/position-budget.service';
import {PositionBudgetModule} from '../../_models/position-budget/position-budget.module';
import {NgForm} from '@angular/forms';
import {INgxMyDpOptions, IMyDateModel} from 'ngx-mydatepicker';

@Component({
  selector: 'positionBudget',
  templateUrl: './position-budget.component.html',
  providers: [PositionBudgetService]
})
export class PositionBudgetComponent implements OnInit {

    page = new Page();
    rows = new Array<PositionBudgetModule>();
    temp = new Array<PositionBudgetModule>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1401';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    positionPlanId = 0;
    positionPlanOptions = [];
    budgetAmount = 0;
    budgetDesc = '';
    budgetScheduleStart;
    budgetScheduleEnd;
    startDate;
    endDate;
	islifeTimeValidamount: boolean = true;
	tempp:string[]=[];
	error:any={isError:false,errorMessage:''};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: PositionBudgetModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    pattern = /*/^\d*\.?\d{0,3}$/*/ /^[\d]*$/;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    
    // Initialized to specific date (09.10.2018).
    public startDateModel: any = { date: { year: 2018, month: 10, day: 9 } };

    // Initialized to specific date (09.10.2018).
    public endDateModel1: any = { date: { year: 2018, month: 10, day: 9 } };


    constructor( private router: Router,
                 private positionBudgetService: PositionBudgetService,
                 private getScreenDetailService: GetScreenDetailService,
                 private alertService: AlertService,
                 private route: ActivatedRoute) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';


        // default form parameter for position  screen
        this.defaultFormValues = [
            {'fieldName': 'POSITION_BUDGET_SEARCH', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'BUDGET_SCHEDULE_START_DATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'BUDGET_SCHEDULE_END_DATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'BUDGET_AMOUNT', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'BUDGET_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_ARBIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_ACTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_BUDGET_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
        ];

    }

  ngOnInit() {

      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      //this.getPositionPlan();
      this.getsearchPositionPlanId();
      this.currentLanguage = localStorage.getItem('currentLanguage');

      // getting screen labels, help messages and validation messages
      this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

          this.moduleName = data.result.moduleName;
          this.screenName = data.result.dtoScreenDetail.screenName;
          this.availableFormValues = data.result.dtoScreenDetail.fieldList;
          for (let j = 0; j < this.availableFormValues.length; j++) {
              let fieldKey = this.availableFormValues[j]['fieldName'];
              let objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
              let objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
              objDefault['fieldValue'] = objAvailable['fieldValue'];
              objDefault['helpMessage'] = objAvailable['helpMessage'];
              if (objAvailable['listDtoFieldValidationMessage']) {
                  objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
              }
          }
      });
  }

    // setting pagination
    setPage(pageInfo) {
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.positionBudgetService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });

    }
	
    getPositionPlan() {
        this.positionBudgetService.getPositionPlanList().then(response => {
            this.positionPlanOptions = response.result.records;
        });
    }
	
    getsearchPositionPlanId() {
        this.positionBudgetService.getsearchPositionPlanIdList().then(response => {
            this.positionPlanOptions = response.result.records;
        });
    }

    changePosPlan(event) {       
		let value = event.target.value;       
		if(value == 0){
			return false;
		}
        for ( let i = 0; i < this.positionPlanOptions.length ; i++) {
            if (parseInt(value) === this.positionPlanOptions[i].id) {
                this.model.description = this.positionPlanOptions[i].positionPlanDesc;
                this.model.budgetArbicDesc = this.positionPlanOptions[i].positionPlanArbicDesc;
                break;
            }
        }
    }

    // optional date changed callback
    onStartDateChanged(event: IMyDateModel): void {
        this.budgetScheduleStart = event.jsdate;
        this.startDate = event.epoc;
		console.log(this.startDate);
		 if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    // optional date changed callback
    onEndDateChanged(event: IMyDateModel): void {
        this.budgetScheduleEnd = event.jsdate;
		this.endDate = event.epoc;
		console.log(this.endDate);
        if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }
	

    // Open form for create position class
    Create() {
		this.startDateModel=null;
		this.endDateModel1=null;
        this.showCreateForm = false;
		this.islifeTimeValidamount= true;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionPlanId: '',
            description: '',
            budgetScheduleStart: this.startDateModel,
            budgetScheduleEnd: this.endDateModel1,
            budgetAmount: null,
            budgetDesc: '',
            budgetArbicDesc: '',
            positionPlanPrimaryId: '',
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({positionPlanId:''});
    }

    // function call for creating new positionClass
    CreatePositionBudget(f: NgForm, event: Event) {

        event.preventDefault();        
        this.model.budgetScheduleStart = this.budgetScheduleStart;
        this.model.budgetScheduleEnd = this.budgetScheduleEnd;

        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Call service api for Creating new position class
            this.positionBudgetService.createPositionBudget(this.model).then(data => {
                let datacode = data.code;
                if (datacode === 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMessage = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.showCreateForm = false;
                        this.messageText = data.btiMessage.message;
                    }, 100);

                    this.hasMsg = true;
                    f.resetForm();
                    // Refresh the Grid data after adding new position class
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    }

    // edit position class by row
    edit(row: PositionBudgetModule) {
		this.error={isError:false,errorMessage:''};
		this.islifeTimeValidamount= true;
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.model.positionPlanId = row.positionPlanPrimaryId;
		this.startDateModel = this.formatDateFordatePicker(this.model.budgetScheduleStart);
		this.endDateModel1 = this.formatDateFordatePicker(this.model.budgetScheduleEnd);
		
		this.budgetScheduleStart = this.model.budgetScheduleStart;
		this.budgetScheduleEnd = this.model.budgetScheduleEnd;
		
		this.model.budgetScheduleStart = this.budgetScheduleStart;
		this.model.budgetScheduleEnd = this.budgetScheduleEnd;
		
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        // Call service api for updating selected position class
        this.positionBudgetService.updatePositionBudget(this.model).then(data => {
            let datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;

                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }
    // delete position class by passing whole object of perticular position class
    delete() {
        let selectedPositionClass = [];
        for (let i = 0; i < this.selected.length; i++) {
            selectedPositionClass.push(this.selected[i].id);
        }
        this.positionBudgetService.deletePositionBudget(selectedPositionClass).then(data => {
            let datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of position class
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            let errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search position class by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.positionBudgetService.searchPositionBudgetlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
	myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };
    confirm(): void {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
	formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
	checkdecimalamount(digit)
	{
		console.log(digit);
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidamount = false;
			console.log(this.islifeTimeValidamount);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValidamount = false;
			console.log(this.islifeTimeValidamount);
		}
		
		else{
			this.islifeTimeValidamount = true;
			console.log(this.islifeTimeValidamount);
		}
	
	}
}
