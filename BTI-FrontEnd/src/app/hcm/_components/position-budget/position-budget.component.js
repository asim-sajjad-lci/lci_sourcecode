"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var page_1 = require("../../../_sharedresource/page");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var router_1 = require("@angular/router");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var position_budget_service_1 = require("../../_services/position-budget/position-budget.service");
var PositionBudgetComponent = (function () {
    function PositionBudgetComponent(router, positionBudgetService, getScreenDetailService, alertService, route) {
        this.router = router;
        this.positionBudgetService = positionBudgetService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.route = route;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1401';
        this.message = { 'type': '', 'text': '' };
        this.positionPlanId = 0;
        this.positionPlanOptions = [];
        this.budgetAmount = 0;
        this.budgetDesc = '';
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.pattern = /^[\d]*$/;
        this.myOptions = {
            // other options...
            dateFormat: 'dd.mm.yyyy',
        };
        // Initialized to specific date (09.10.2018).
        this.startDateModel = { date: { year: 2018, month: 10, day: 9 } };
        // Initialized to specific date (09.10.2018).
        this.endDateModel1 = { date: { year: 2018, month: 10, day: 9 } };
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for position  screen
        this.defaultFormValues = [
            { 'fieldName': 'POSITION_BUDGET_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUDGET_SCHEDULE_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUDGET_SCHEDULE_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUDGET_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUDGET_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ARBIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_BUDGET_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    PositionBudgetComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.getPositionPlan();
        this.currentLanguage = localStorage.getItem('currentLanguage');
        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            var _loop_1 = function (j) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            };
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                _loop_1(j);
            }
        });
    };
    // setting pagination
    PositionBudgetComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.positionBudgetService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    PositionBudgetComponent.prototype.getPositionPlan = function () {
        var _this = this;
        this.positionBudgetService.getPositionPlanList().then(function (response) {
            _this.positionPlanOptions = response.result.records;
        });
    };
    PositionBudgetComponent.prototype.changePosPlan = function (event) {
        var value = event.target.value;
        for (var i = 0; i < this.positionPlanOptions.length; i++) {
            if (parseInt(value) === this.positionPlanOptions[i].id) {
                this.model.description = this.positionPlanOptions[i].positionPlanDesc;
                this.model.budgetArbicDesc = this.positionPlanOptions[i].positionPlanArbicDesc;
                break;
            }
        }
    };
    // optional date changed callback
    PositionBudgetComponent.prototype.onStartDateChanged = function (event) {
        this.budgetScheduleStart = event.jsdate;
    };
    // optional date changed callback
    PositionBudgetComponent.prototype.onEndDateChanged = function (event) {
        this.budgetScheduleEnd = event.jsdate;
    };
    // Open form for create position class
    PositionBudgetComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionPlanId: 0,
            description: '',
            budgetScheduleStart: this.startDateModel,
            budgetScheduleEnd: this.endDateModel1,
            budgetAmount: 0,
            budgetDesc: '',
            budgetArbicDesc: ''
        };
    };
    // Clear form to reset to default blank
    PositionBudgetComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    // function call for creating new positionClass
    PositionBudgetComponent.prototype.CreatePositionBudget = function (f, event) {
        var _this = this;
        event.preventDefault();
        // this.model.positionPlanId = this.positionPlanId;
        this.model.budgetScheduleStart = this.budgetScheduleStart;
        this.model.budgetScheduleEnd = this.budgetScheduleEnd;
        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            // Call service api for Creating new position class
            this.positionBudgetService.createPositionBudget(this.model).then(function (data) {
                var datacode = data.code;
                if (datacode === 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(function () {
                        _this.isSuccessMsg = true;
                        _this.isfailureMsg = false;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.hasMsg = false;
                        }, 4000);
                        _this.showCreateForm = false;
                        _this.messageText = data.btiMessage.message;
                    }, 100);
                    _this.hasMsg = true;
                    f.resetForm();
                    // Refresh the Grid data after adding new position class
                    _this.setPage({ offset: 0 });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    // edit position class by row
    PositionBudgetComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.positionPlanId = row.positionPlanId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    PositionBudgetComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        // Call service api for updating selected position class
        this.positionBudgetService.updatePositionBudget(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                _this.setPage({ offset: 0 });
                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    };
    PositionBudgetComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    // delete position class by passing whole object of perticular position class
    PositionBudgetComponent.prototype.delete = function () {
        var _this = this;
        var selectedPositionClass = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedPositionClass.push(this.selected[i].id);
        }
        this.positionBudgetService.deletePositionBudget(selectedPositionClass).then(function (data) {
            var datacode = data.code;
            if (datacode === 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            // this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            // Refresh the Grid data after deletion of position class
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    PositionBudgetComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search position class by keyword
    PositionBudgetComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.positionBudgetService.searchPositionBudgetlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    PositionBudgetComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    PositionBudgetComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    };
    PositionBudgetComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    return PositionBudgetComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], PositionBudgetComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], PositionBudgetComponent.prototype, "myScrollContainer", void 0);
PositionBudgetComponent = __decorate([
    core_1.Component({
        selector: 'positionBudget',
        templateUrl: './position-budget.component.html',
        providers: [position_budget_service_1.PositionBudgetService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        position_budget_service_1.PositionBudgetService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService,
        router_1.ActivatedRoute])
], PositionBudgetComponent);
exports.PositionBudgetComponent = PositionBudgetComponent;
//# sourceMappingURL=position-budget.component.js.map