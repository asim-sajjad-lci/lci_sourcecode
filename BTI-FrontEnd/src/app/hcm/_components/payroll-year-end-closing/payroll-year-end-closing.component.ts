import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { PaycodeSetupModule } from '../../_models/paycode-setup/paycode-setup.module';
import { PaycodeSetupService } from '../../_services/paycode-setup/paycode-setup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { PayrollCodeModifierModule } from '../../_models/payroll-code-modifier/payroll-code-modifier.module';
import { RoutineUtilitiesService } from '../../_services/routines-utilities/routine-utilities.service';
import { DeductionCodeSetup } from '../../_models/deduction-code-setup/deduction-code-setup.module';
import { BenefitCodeSetup } from '../../_models/benefit-code-setup/benefit-code-setup.module';

@Component({
    selector: 'payroll-year-end-closing',
    templateUrl: './payroll-year-end-closing.component.html',
    providers: [CommonService, RoutineUtilitiesService]
})

export class PayrollYearEndClosingComponent {

    moduleCode = 'M-1011';
    screenCode = 'S-1474';
    defaultFormValues: object[];
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    PAYROLL_YEAR_END_CLOSING_WAGE_YEAR: any;
    PAYROLL_YEAR_END_CLOSING_PROCESS: any;
    PAYROLL_YEAR_END_CLOSING_CLEAR: any;
    messageText: string;
    year:any;

    years: DropDownModule[] = [
        { id: 1, name: '2018' }
    ];


    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('fieldName1')

    typeaheadLoading: boolean;

    loaderImg: boolean = true;

    constructor(public commonService: CommonService, private routineService: RoutineUtilitiesService) {
        // this.model = new PayrollCodeModifierModule();

        this.defaultFormValues = [
            { 'fieldName': 'PAYROLL_YEAR_END_CLOSING_WAGE_YEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYROLL_YEAR_END_CLOSING_PROCESS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYROLL_YEAR_END_CLOSING_CLEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' }
        ];

        this.PAYROLL_YEAR_END_CLOSING_WAGE_YEAR = this.defaultFormValues[0];
        this.PAYROLL_YEAR_END_CLOSING_PROCESS = this.defaultFormValues[1];
        this.PAYROLL_YEAR_END_CLOSING_CLEAR = this.defaultFormValues[2];
    }


    ngOnInit() {

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    }

    process(f: NgForm, event: Event){
        console.log(this.year);
        this.routineService.processClosing(this.year).then(() => {
            f.resetForm({
                codeType: '',
            });
        });
    }

    Clear(f: NgForm) {
        f.resetForm({
            year: ''
        });
    }

}

