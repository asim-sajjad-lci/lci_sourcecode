import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { PaycodeSetupModule } from '../../_models/paycode-setup/paycode-setup.module';
import { PaycodeSetupService } from '../../_services/paycode-setup/paycode-setup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { PayrollCodeModifierModule } from '../../_models/payroll-code-modifier/payroll-code-modifier.module';
import { RoutineUtilitiesService } from '../../_services/routines-utilities/routine-utilities.service';
import { DeductionCodeSetup } from '../../_models/deduction-code-setup/deduction-code-setup.module';
import { BenefitCodeSetup } from '../../_models/benefit-code-setup/benefit-code-setup.module';

@Component({
    selector: 'employee-mass-update',
    templateUrl: './employee-mass-update.component.html',
    providers: [CommonService, RoutineUtilitiesService]
})

export class EmployeeMassUpdateComponent {

    moduleCode = 'M-1011';
    screenCode = 'S-1476';
    defaultFormValues: object[];
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    EMPLOYEE_MASS_UPDATE_UPDATE: any;
    EMPLOYEE_MASS_UPDATE_CODE: any;
    EMPLOYEE_MASS_UPDATE_RANGE: any;
    EMPLOYEE_MASS_UPDATE_ID: any;
    EMPLOYEE_MASS_UPDATE_DEPARTMENT: any;
    EMPLOYEE_MASS_UPDATE_POSITION: any;
    EMPLOYEE_MASS_UPDATE_START_DATE: any;
    EMPLOYEE_MASS_UPDATE_ALL: any;
    EMPLOYEE_MASS_UPDATE_FROM: any;
    EMPLOYEE_MASS_UPDATE_TO: any;
    EMPLOYEE_MASS_UPDATE_METHOD: any;
    EMPLOYEE_MASS_UPDATE_AMOUNT: any;
    EMPLOYEE_MASS_UPDATE_INCREASE: any;
    EMPLOYEE_MASS_UPDATE_DECREASE: any;
    EMPLOYEE_MASS_UPDATE_EDIT_RECORDS: any;
    EMPLOYEE_MASS_UPDATE_NEAREST_AMOUNT: any;
    EMPLOYEE_MASS_UPDATE_CLEAR: any;

    messageText: string;


    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('fieldName1')

    typeaheadLoading: boolean;

    loaderImg: boolean = true;

    constructor(public commonService: CommonService, private routineService: RoutineUtilitiesService) {
        // this.model = new PayrollCodeModifierModule();

        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_UPDATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_CODE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_RANGE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_DEPARTMENT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_POSITION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_START_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_ALL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_FROM', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_TO', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_METHOD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_INCREASE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_DECREASE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_EDIT_RECORDS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_NEAREST_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'EMPLOYEE_MASS_UPDATE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' }
        ];

        this.EMPLOYEE_MASS_UPDATE_UPDATE = this.defaultFormValues[0];
        this.EMPLOYEE_MASS_UPDATE_CODE = this.defaultFormValues[1];
        this.EMPLOYEE_MASS_UPDATE_RANGE = this.defaultFormValues[2];
        this.EMPLOYEE_MASS_UPDATE_ID = this.defaultFormValues[3];
        this.EMPLOYEE_MASS_UPDATE_DEPARTMENT = this.defaultFormValues[4];
        this.EMPLOYEE_MASS_UPDATE_POSITION = this.defaultFormValues[5];
        this.EMPLOYEE_MASS_UPDATE_START_DATE = this.defaultFormValues[6];
        this.EMPLOYEE_MASS_UPDATE_ALL = this.defaultFormValues[7];
        this.EMPLOYEE_MASS_UPDATE_FROM = this.defaultFormValues[8];
        this.EMPLOYEE_MASS_UPDATE_TO = this.defaultFormValues[9];
        this.EMPLOYEE_MASS_UPDATE_METHOD = this.defaultFormValues[10];
        this.EMPLOYEE_MASS_UPDATE_AMOUNT = this.defaultFormValues[11];
        this.EMPLOYEE_MASS_UPDATE_INCREASE = this.defaultFormValues[12];
        this.EMPLOYEE_MASS_UPDATE_DECREASE = this.defaultFormValues[13];
        this.EMPLOYEE_MASS_UPDATE_EDIT_RECORDS = this.defaultFormValues[14];
        this.EMPLOYEE_MASS_UPDATE_NEAREST_AMOUNT = this.defaultFormValues[15];
        this.EMPLOYEE_MASS_UPDATE_CLEAR = this.defaultFormValues[16];
    }


    ngOnInit() {

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    }

    Clear(f: NgForm) {
        f.resetForm({
            year: ''
        });
    }

}

