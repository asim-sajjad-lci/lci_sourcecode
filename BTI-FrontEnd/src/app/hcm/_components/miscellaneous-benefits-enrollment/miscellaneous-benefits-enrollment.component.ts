import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { MiscellaneousBenefitsEnrollmentModule } from '../../_models/miscellaneous-benefits-enrollment/miscellaneous-benefits-enrollment.module';
import { MiscellaneousBenefitsService } from '../../_services/miscellaneous-benefits-enrollment/miscellaneous-benefits-enrollment.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { DatePipe } from '@angular/common';
@Component({
    selector: 'app-miscellaneous-benefits-enrollment',
    templateUrl: './miscellaneous-benefits-enrollment.component.html',
    providers: [MiscellaneousBenefitsService, CommonService, DatePipe]
})
export class MiscellaneousBenefitsEnrollmentComponent {
    page = new Page();
    rows = new Array<MiscellaneousBenefitsEnrollmentModule>();
    temp = new Array<MiscellaneousBenefitsEnrollmentModule>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1447';
    moduleName;
    screenName;
    defaultFormValues:object [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    BenefitsId = {};
    positionClassOptions: any;
    benefitsId;
    searchKeyword = '';
    ddPageSize: number = 5;
    model: MiscellaneousBenefitsEnrollmentModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    tempp: string[] = [];
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder: boolean = false;
    error: any = { isError: false, errorMessage: '' };
    islifeTimeValid: boolean = true;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    islifeTimeValidamount: boolean = true;
    dudctionAmountPercentPattern: boolean = true;
    dudctionAmountPattern: boolean = true;
    monthlyAmountPattern: boolean = true;
    yearlyAmountPattern: boolean = true;
    lifetimeAmountPattern: boolean = true;
    benefitAmountPattern: boolean = true;
    benefitAmountPercentPattern: boolean = true;
    employermonthlyAmountPattern: boolean = true;
    employeryearlyAmountPattern: boolean = true;
    employerlifetimeAmountPattern: boolean = true;
    varToBeFalse: any;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    isdedYearly: boolean = true;
    isbenYearly: boolean = true;
    isdedLifetime: boolean = true;
    isbenLifetime: boolean = true;
    isdedTotal: boolean = true;
    isbenTotal: boolean = true;
    BenefitsIdvalue: number;
    isEmployeeFormdisabled: boolean = false;
    isEmployerFormdisabled: boolean = false;
    frequencyArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semiannually", "Annually"];
    employeeMethodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
    employerMethodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
    statusArray = ["Active", "Inactive", "Waived", "Ineligible", "Terminated", "Family Leave", "Pending"]
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    dataSource: Observable<any>;
    employeeIdList: Observable<any>;
    getMasterEmployee: any[] = [];
    asyncSelected: string;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    employeeDesc: string;
    isEmployeeIdExist: boolean = true;
    isBenefitIdExist: boolean = true;
    BenefitDesc: string;
    employeeMaster: object;
    benefitsMaster: object;
    employeeIdValue;
    tempBenefitsId: string;
    tempId: number;
    tempemployeeIndexId: number;
    tempemployeeId: string;
    MISCELLANEOUS_BENEFITS_SEARCH: any;
    MISCELLANEOUS_BENEFITS_ID: any;
    MISCELLANEOUS_BENEFITS_DESCRIPTION: any;
    MISCELLANEOUS_BENEFITS_ARABIC_DESCRIPTION: any;
    MISCELLANEOUS_BENEFITS_START_DATE: any;
    MISCELLANEOUS_BENEFITS_FREQUENCY: any;
    MISCELLANEOUS_BENEFITS_END_DATE: any;
    MISCELLANEOUS_BENEFITS_ACTION_LABEL: any;
    MISCELLANEOUS_BENEFITS_CREATE_LABEL: any;
    MISCELLANEOUS_BENEFITS_SAVE_LABEL: any;
    MISCELLANEOUS_BENEFITS_CLEAR_LABEL: any;
    MISCELLANEOUS_BENEFITS_CANCEL_LABEL: any;
    MISCELLANEOUS_BENEFITS_UPDATE_LABEL: any;
    MISCELLANEOUS_BENEFITS_DELETE_LABEL: any;
    MISCELLANEOUS_BENEFITS_CREATE_FORM_LABEL: any;
    MISCELLANEOUS_BENEFITS_UPDATE_FORM_LABEL: any;
    MANAGE_USER_TABLE_VIEW: any;
    MANAGE_USER_TABLE_ACCESS: any;
    MANAGE_USER_TABLE_DELETE: any;
    MANAGE_USER_TEXT_ADD_NEW_USER: any;
    MANAGE_USER_TABLE_STATE: any;
    MANAGE_USER_TABLE_CITY: any;
    MANAGE_USER_TABLE_POSTALCODE: any;
    MANAGE_USER_TABLE_DOB: any;
    MISCELLANEOUS_BENEFITS_INACTIVE: any;
    MISCELLANEOUS_BENEFITS_METHOD: any;
    MISCELLANEOUS_BENEFITS_DEDUCTION_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_DEDUCTION_PERCENT: any;
    MISCELLANEOUS_BENEFITS_MONTHLY_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_YEARLY_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_LIFETIME_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_INACTIVE: any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_METHOD: any;
    MISCELLANEOUS_BENEFITS_BENEFIT_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_BENEFIT_PERCENT: any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_MONTHLY_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_YEARLY_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_EMPLOYER_LIFETIME_AMOUNT: any;
    MISCELLANEOUS_BENEFITS_BENEFIT_START_DATE: any;
    MISCELLANEOUS_BENEFITS_BENEFIT_END_DATE: any;
    MISCELLANEOUS_BENEFITS_NAME: any;
    MISCELLANEOUS_BENEFITS_CODE: any;
    MISCELLANEOUS_BENEFITS_STATUS: any;

    constructor(private router: Router,
        private miscellaneousBenefitsService: MiscellaneousBenefitsService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService,
        private datePipe: DatePipe) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'BenefitsId';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_FREQUENCY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ACTION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DEDUCTION_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DEDUCTION_PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_MONTHLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_YEARLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_LIFETIME_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_MONTHLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_YEARLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_LIFETIME_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_STATUS', 'fieldValue': '', 'helpMessage': '' }
        ];

        this.MISCELLANEOUS_BENEFITS_SEARCH = this.defaultFormValues[0];
        this.MISCELLANEOUS_BENEFITS_ID = this.defaultFormValues[1];
        this.MISCELLANEOUS_BENEFITS_DESCRIPTION = this.defaultFormValues[2];
        this.MISCELLANEOUS_BENEFITS_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.MISCELLANEOUS_BENEFITS_START_DATE = this.defaultFormValues[4];
        this.MISCELLANEOUS_BENEFITS_FREQUENCY = this.defaultFormValues[5];
        this.MISCELLANEOUS_BENEFITS_END_DATE = this.defaultFormValues[6];
        this.MISCELLANEOUS_BENEFITS_ACTION_LABEL = this.defaultFormValues[7];
        this.MISCELLANEOUS_BENEFITS_CREATE_LABEL = this.defaultFormValues[8];
        this.MISCELLANEOUS_BENEFITS_SAVE_LABEL = this.defaultFormValues[9];
        this.MISCELLANEOUS_BENEFITS_CLEAR_LABEL = this.defaultFormValues[10];
        this.MISCELLANEOUS_BENEFITS_CANCEL_LABEL = this.defaultFormValues[11];
        this.MISCELLANEOUS_BENEFITS_UPDATE_LABEL = this.defaultFormValues[12];
        this.MISCELLANEOUS_BENEFITS_DELETE_LABEL = this.defaultFormValues[13];
        this.MISCELLANEOUS_BENEFITS_CREATE_FORM_LABEL = this.defaultFormValues[14];
        this.MISCELLANEOUS_BENEFITS_UPDATE_FORM_LABEL = this.defaultFormValues[15];
        this.MANAGE_USER_TABLE_VIEW = this.defaultFormValues[16];
        this.MANAGE_USER_TABLE_ACCESS = this.defaultFormValues[17];
        this.MANAGE_USER_TABLE_DELETE = this.defaultFormValues[18];
        this.MANAGE_USER_TEXT_ADD_NEW_USER = this.defaultFormValues[19];
        this.MANAGE_USER_TABLE_STATE = this.defaultFormValues[20];
        this.MANAGE_USER_TABLE_CITY = this.defaultFormValues[21];
        this.MANAGE_USER_TABLE_POSTALCODE = this.defaultFormValues[22];
        this.MANAGE_USER_TABLE_DOB = this.defaultFormValues[23];
        this.MISCELLANEOUS_BENEFITS_INACTIVE = this.defaultFormValues[24];
        this.MISCELLANEOUS_BENEFITS_METHOD = this.defaultFormValues[25];
        this.MISCELLANEOUS_BENEFITS_DEDUCTION_AMOUNT = this.defaultFormValues[26];
        this.MISCELLANEOUS_BENEFITS_DEDUCTION_PERCENT = this.defaultFormValues[27];
        this.MISCELLANEOUS_BENEFITS_MONTHLY_AMOUNT = this.defaultFormValues[28];
        this.MISCELLANEOUS_BENEFITS_YEARLY_AMOUNT = this.defaultFormValues[29];
        this.MISCELLANEOUS_BENEFITS_LIFETIME_AMOUNT = this.defaultFormValues[30];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_INACTIVE = this.defaultFormValues[31];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_METHOD = this.defaultFormValues[32];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_AMOUNT = this.defaultFormValues[33];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_PERCENT = this.defaultFormValues[34];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_MONTHLY_AMOUNT = this.defaultFormValues[35];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_YEARLY_AMOUNT = this.defaultFormValues[36];
        this.MISCELLANEOUS_BENEFITS_EMPLOYER_LIFETIME_AMOUNT = this.defaultFormValues[37];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_START_DATE = this.defaultFormValues[38];
        this.MISCELLANEOUS_BENEFITS_BENEFIT_END_DATE = this.defaultFormValues[39];
        this.MISCELLANEOUS_BENEFITS_NAME = this.defaultFormValues[40];
        this.MISCELLANEOUS_BENEFITS_CODE = this.defaultFormValues[41];
        this.MISCELLANEOUS_BENEFITS_STATUS = this.defaultFormValues[42];

        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.benefitsMaster['benefitsId']);
        }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));
        this.employeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.employeeMaster['employeeId']);
        }).mergeMap((token: string) => this.getEmployeeMasterAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.miscellaneousBenefitsService.getPositionClassList().then(data => {
            this.positionClassOptions = data.result.records;
        });
        this.miscellaneousBenefitsService.getAllEmployeeMaster().then(data => {
            this.getMasterEmployee = data.result;
        });
    }
    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'employeeId') {
            return row['employeeMaster'] && row['employeeMaster']['employeeId'] ? row['employeeMaster']['employeeId'] : '';
        } else if (gridFieldName == 'employeeFirstName') {
            return row['employeeMaster'] && row['employeeMaster']['employeeFirstName'] ? row['employeeMaster']['employeeFirstName'] : '';
        } else if (gridFieldName == 'benefitsId') {
            return row['dtoMiscellaneousBenefits'] && row['dtoMiscellaneousBenefits']['benefitsId'] ? row['dtoMiscellaneousBenefits']['benefitsId'] : '';
        } else if (gridFieldName === 'desc') {
            return row['dtoMiscellaneousBenefits'] && row['dtoMiscellaneousBenefits']['desc'] ? row['dtoMiscellaneousBenefits']['desc'] : '';
        } else if (gridFieldName === 'frequency') {
            return row[gridFieldName]?this.frequencyArray[row[gridFieldName]-1]:'';
        } else if (gridFieldName === 'status') {
            return row[gridFieldName]?this.statusArray[row[gridFieldName]-1]:'';
        } else if (gridFieldName === 'startDate') {
            return row[gridFieldName]?this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy'):'';
        } else if (gridFieldName === 'endDate') {
            return row[gridFieldName]?this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy'):'';
        } else {
            return row[gridFieldName];
        }
    }
    getReportPositionsAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.positionClassOptions.filter((id: any) => {
                return query.test(id.benefitsId);
            })
        );
    }

    getEmployeeMasterAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getMasterEmployee.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect1(e: TypeaheadMatch): void {
        this.employeeMaster['employeeId'] = e.value;
        this.isEmployeeIdExist = true;
        for (var i = 0; i < this.getMasterEmployee.length; i++) {
            if (this.getMasterEmployee[i].employeeId == e.value) {
                this.employeeDesc = this.getMasterEmployee[i].employeeFirstName;
                this.employeeMaster['employeeIndexId'] = this.getMasterEmployee[i].employeeIndexId;
                break;
            } else {
                this.employeeDesc = '';
            }
        }
        //this.model.benefitsId = e.value;
    }

    typeaheadOnSelect2(e: TypeaheadMatch): void {
        this.benefitsMaster['benefitsId'] = e.value;
        this.isBenefitIdExist = true;
        for (var i = 0; i < this.positionClassOptions.length; i++) {
            if (this.positionClassOptions[i].benefitsId == e.value) {
                this.BenefitDesc = this.positionClassOptions[i].desc;
                this.benefitsMaster['id'] = this.positionClassOptions[i].id;
                // if (!this.isUnderUpdate) {
                    this.model.inActive = this.positionClassOptions[i].inActive;
                    this.model.empluyeeerinactive = this.positionClassOptions[i].empluyeeerinactive;
                    this.model.method = this.positionClassOptions[i].method;
                    this.model.empluyeeermethod = this.positionClassOptions[i].empluyeeermethod;
                    this.model.monthlyAmount = this.positionClassOptions[i].monthlyAmount;
                    this.model.yearlyAmount = this.positionClassOptions[i].yearlyAmount;
                    this.model.lifetimeAmount = this.positionClassOptions[i].lifetimeAmount;
                    this.model.employermonthlyAmount = this.positionClassOptions[i].employermonthlyAmount;
                    this.model.employeryearlyAmount = this.positionClassOptions[i].employeryearlyAmount;
                    this.model.employerlifetimeAmount = this.positionClassOptions[i].employerlifetimeAmount;
                    this.model.dudctionAmount = this.positionClassOptions[i].dudctionAmount;
                    this.model.dudctionPercent = this.positionClassOptions[i].dudctionPercent;
                    this.model.benefitAmount = this.positionClassOptions[i].benefitAmount;
                    this.model.benefitPercent = this.positionClassOptions[i].benefitPercent;
                }
                // break;
            // } 
            // else {
            //     this.BenefitDesc = '';
            //     // if (!this.isUnderUpdate) {
            //         this.model.inActive = false;
            //         this.model.empluyeeerinactive = false;
            //         this.model.method = 0;
            //         this.model.empluyeeermethod = 0;
            //         this.model.monthlyAmount = null;
            //         this.model.yearlyAmount = null;
            //         this.model.lifetimeAmount = null;
            //         this.model.employermonthlyAmount = null;
            //         this.model.employeryearlyAmount = null;
            //         this.model.employerlifetimeAmount = null;
            //         this.model.dudctionAmount = null;
            //         this.model.dudctionPercent = null;
            //         this.model.benefitAmount = null;
            //         this.model.benefitPercent = null;
            //     // }
            // }
        }
        //this.model.benefitsId = e.value;
    }

    checkValid1(event) {
        if (event.target.value == '') {
            this.isEmployeeIdExist = true;
            this.employeeDesc = '';
        } else {
            for (var i = 0; i < this.getMasterEmployee.length; i++) {
                if (this.getMasterEmployee[i].employeeId.includes(event.target.value)) {
                    this.isEmployeeIdExist = true;
                    this.employeeDesc = this.getMasterEmployee[i].employeeFirstName;
                    this.employeeMaster['employeeIndexId'] = this.getMasterEmployee[i].employeeIndexId;
                    break;
                } else {
                    this.employeeDesc = '';
                    this.isEmployeeIdExist = false;
                }
            }
        }
    }

    checkValid2(event) {
        if (event.target.value == '') {
            this.isBenefitIdExist = true;
            this.BenefitDesc = '';
            if (!this.isUnderUpdate) {
                this.model.inActive = false;
                this.model.empluyeeerinactive = false;
                this.model.method = 0;
                this.model.empluyeeermethod = 0;
                this.model.monthlyAmount = null;
                this.model.yearlyAmount = null;
                this.model.lifetimeAmount = null;
                this.model.employermonthlyAmount = null;
                this.model.employeryearlyAmount = null;
                this.model.employerlifetimeAmount = null;
                this.model.dudctionAmount = null;
                this.model.dudctionPercent = null;
                this.model.benefitAmount = null;
                this.model.benefitPercent = null;
            }
        } else {
            for (var i = 0; i < this.positionClassOptions.length; i++) {
                if (this.positionClassOptions[i].benefitsId.includes(event.target.value)) {
                    this.isBenefitIdExist = true;
                    this.BenefitDesc = this.positionClassOptions[i].desc;
                    this.benefitsMaster['id'] = this.positionClassOptions[i].id;
                    if (!this.isUnderUpdate) {
                        this.model.inActive = this.positionClassOptions[i].inActive;
                        this.model.empluyeeerinactive = this.positionClassOptions[i].empluyeeerinactive;
                        this.model.method = this.positionClassOptions[i].method;
                        this.model.empluyeeermethod = this.positionClassOptions[i].empluyeeermethod;
                        this.model.monthlyAmount = this.positionClassOptions[i].monthlyAmount;
                        this.model.yearlyAmount = this.positionClassOptions[i].yearlyAmount;
                        this.model.lifetimeAmount = this.positionClassOptions[i].lifetimeAmount;
                        this.model.employermonthlyAmount = this.positionClassOptions[i].employermonthlyAmount;
                        this.model.employeryearlyAmount = this.positionClassOptions[i].employeryearlyAmount;
                        this.model.employerlifetimeAmount = this.positionClassOptions[i].employerlifetimeAmount;
                        this.model.dudctionAmount = this.positionClassOptions[i].dudctionAmount;
                        this.model.dudctionPercent = this.positionClassOptions[i].dudctionPercent;
                        this.model.benefitAmount = this.positionClassOptions[i].benefitAmount;
                        this.model.benefitPercent = this.positionClassOptions[i].benefitPercent;
                    }
                    break;
                } else {
                    this.BenefitDesc = '';
                    this.isBenefitIdExist = false;
                    if (!this.isUnderUpdate) {
                        this.model.inActive = false;
                        this.model.empluyeeerinactive = false;
                        this.model.method = 0;
                        this.model.empluyeeermethod = 0;
                        this.model.monthlyAmount = null;
                        this.model.yearlyAmount = null;
                        this.model.lifetimeAmount = null;
                        this.model.employermonthlyAmount = null;
                        this.model.employeryearlyAmount = null;
                        this.model.employerlifetimeAmount = null;
                        this.model.dudctionAmount = null;
                        this.model.dudctionPercent = null;
                        this.model.benefitAmount = null;
                        this.model.benefitPercent = null;
                    }
                }
            }
        }
    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.miscellaneousBenefitsService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create location
    Create() {
        this.modelStartDate = null;
        this.modelEndDate = null;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.islifeTimeValidamount = true;
        this.isEmployerFormdisabled = false;
        this.isEmployeeFormdisabled = false;
        this.isEmployeeIdExist = true;
        this.isBenefitIdExist = true;
        this.employeeDesc = '';
        this.BenefitDesc = '';
        this.employeeMaster = {
            employeeIndexId: 0,
            employeeId: ''
        };
        this.benefitsMaster = {
            id: 0,
            benefitsId: ''
        }
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 800);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            dtoMiscellaneousBenefits: {
                id: 0,
                benefitsId: ''
            },
            employeeMaster: {
                employeeIndexId: 0,
                employeeId: ''
            },
            status: 0,
            frequency: 0,
            startDate: null,
            endDate: null,
            inActive: false,
            method: 0,
            dudctionAmount: null,
            dudctionPercent: null,
            monthlyAmount: null,
            yearlyAmount: null,
            lifetimeAmount: null,
            empluyeeerinactive: false,
            empluyeeermethod: 0,
            benefitAmount: null,
            benefitPercent: null,
            employermonthlyAmount: null,
            employeryearlyAmount: null,
            employerlifetimeAmount: null
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ frequency: 0, method: 0, empluyeeermethod: 0, status: 0 });
        /*this.model.employeeMaster['employeeIndexId'] = this.tempemployeeIndexId;
        this.model.employeeMaster['employeeId'] = this.tempemployeeId;
        this.model.dtoMiscellaneousBenefits['id'] = this.tempId;
        this.model.dtoMiscellaneousBenefits['benefitsId'] = this.tempBenefitsId;*/
        this.employeeMaster = {
            employeeIndexId: 0,
            employeeId: ''
        };
        this.benefitsMaster = {
            id: 0,
            benefitsId: ''
        }
        this.isEmployerFormdisabled = false;
        this.isEmployeeFormdisabled = false;
        this.isBenefitIdExist = true;
        this.isEmployeeIdExist = true;
        this.model.dudctionAmount = null;
        this.model.dudctionPercent = null;
        this.model.benefitAmount = null;
        this.model.benefitPercent = null;
        this.BenefitDesc = '';
        if (!this.isUnderUpdate) {
            this.employeeDesc = '';
        }
    }

    //function call for creating new location
    CreateMiscellaneousBenefits(f: NgForm, event: Event) {
        this.model.startDate = this.frmStartDate;
        this.model.endDate = this.frmEndDate;
        this.model.employeeMaster = this.employeeMaster;
        this.model.dtoMiscellaneousBenefits = this.benefitsMaster;
        event.preventDefault();
        var locIdx = this.model.employeeMaster;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.miscellaneousBenefitsService.checkDuplicateMiscellaneousBenefitsId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.scrollTo(0, 400);
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code
                    this.miscellaneousBenefitsService.createMiscellaneousBenefits(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        }
    }

    //edit department by row
    edit(row: MiscellaneousBenefitsEnrollmentModule) {
        this.error = { isError: false, errorMessage: '' };
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        //  this.BenefitsId = row.benefitsId;
        this.isUnderUpdate = true;
        this.islifeTimeValidamount = true;
        this.isEmployeeIdExist = true;
        this.isBenefitIdExist = true;
        this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
        this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
        this.frmStartDate = this.model.startDate;
        this.frmEndDate = this.model.endDate;
        let strDate = new Date(this.model.startDate);
        this.startDate = (strDate.getTime())/1000;
        let enddate = new Date(this.model.endDate);
        this.endDate = (enddate.getTime())/1000;
        this.model.startDate = this.modelStartDate;
        this.model.endDate = this.modelEndDate;
        this.employeeDesc = row.employeeMaster['employeeFirstName'];
        this.BenefitDesc = row.dtoMiscellaneousBenefits['desc'];
        this.employeeMaster = Object.assign({}, this.model.employeeMaster);
        this.benefitsMaster = Object.assign({}, this.model.dtoMiscellaneousBenefits);
        this.tempemployeeId = this.model.employeeMaster['employeeId'];
        this.tempemployeeIndexId = this.model.employeeMaster['employeeIndexId'];
        this.tempId = this.model.dtoMiscellaneousBenefits['id'];
        this.tempBenefitsId = this.model.dtoMiscellaneousBenefits['benefitsId'];
        if (this.model.inActive == true) {
            this.isEmployeeFormdisabled = true;
        } else {
            this.isEmployeeFormdisabled = false;
        }

        if (this.model.empluyeeerinactive == true) {
            this.isEmployerFormdisabled = true;
        } else {
            this.isEmployerFormdisabled = false;
        }

        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        //this.model.benefitsId = this.BenefitsIdvalue;
        //Call service api for updating selected department
        this.miscellaneousBenefitsService.updateMiscellaneousBenefits(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            window.scrollTo(0, 0);
        }
    }

    //delete department by passing whole object of perticular Department
    delete() {
        var selectedMiscellaneousBenefitss = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedMiscellaneousBenefitss.push(this.selected[i].id);
        }
        this.miscellaneousBenefitsService.deleteMiscellaneousBenefits(selectedMiscellaneousBenefitss).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    /*CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.benefitsId = 0;
            return false;
        }
    }*/
    checkFrequency(event) {
        if (event.target.value == 0) {
            return false;
        }
    }
    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };
    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;

        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }
    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkdecimal(digit) {
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValid = false;
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValid = false;
        }

        else {
            this.islifeTimeValid = true;
        }

    }
    checkdecimalamount(digit, varToBeFalse) {
        var temp = true;
        if (digit == '' || digit == null) {
            if (varToBeFalse == 'dudctionAmountPattern') {
                this.dudctionAmountPattern = true;
            }
            if (varToBeFalse == 'monthlyAmountPattern') {
                this.monthlyAmountPattern = true;
            }
            if (varToBeFalse == 'yearlyAmountPattern') {
                this.yearlyAmountPattern = true;
            }
            if (varToBeFalse == 'lifetimeAmountPattern') {
                this.lifetimeAmountPattern = true;
            }
            if (varToBeFalse == 'benefitAmountPattern') {
                this.benefitAmountPattern = true;
            }
            if (varToBeFalse == 'employermonthlyAmountPattern') {
                this.employermonthlyAmountPattern = true;
            }
            if (varToBeFalse == 'employeryearlyAmountPattern') {
                this.employeryearlyAmountPattern = true;
            }
            if (varToBeFalse == 'employerlifetimeAmountPattern') {
                this.employerlifetimeAmountPattern = true;
            }
            return;
        }

        if (digit != null) {
            this.tempp = digit.toString().split(".");
            if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            } else {
                temp = true;
            }
        }

        if (varToBeFalse == 'dudctionAmountPattern') {
            this.dudctionAmountPattern = temp;
        }
        if (varToBeFalse == 'monthlyAmountPattern') {
            this.monthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'yearlyAmountPattern') {
            this.yearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'lifetimeAmountPattern') {
            this.lifetimeAmountPattern = temp;
        }
        if (varToBeFalse == 'benefitAmountPattern') {
            this.benefitAmountPattern = temp;
        }
        if (varToBeFalse == 'employermonthlyAmountPattern') {
            this.employermonthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employeryearlyAmountPattern') {
            this.employeryearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employerlifetimeAmountPattern') {
            this.employerlifetimeAmountPattern = temp;
        }
    }

    checkdecimalamountforSophasticated(digit, varToBeFalse) {
        var temp = true;
        if (digit == '' || digit == null) {
            if (varToBeFalse == 'dudctionAmountPattern') {
                this.dudctionAmountPattern = true;
            }
            if (varToBeFalse == 'monthlyAmountPattern') {
                this.monthlyAmountPattern = true;
            }
            if (varToBeFalse == 'yearlyAmountPattern') {
                this.yearlyAmountPattern = true;
            }
            if (varToBeFalse == 'lifetimeAmountPattern') {
                this.lifetimeAmountPattern = true;
            }
            if (varToBeFalse == 'benefitAmountPattern') {
                this.benefitAmountPattern = true;
            }
            if (varToBeFalse == 'employermonthlyAmountPattern') {
                this.employermonthlyAmountPattern = true;
            }
            if (varToBeFalse == 'employeryearlyAmountPattern') {
                this.employeryearlyAmountPattern = true;
            }
            if (varToBeFalse == 'employerlifetimeAmountPattern') {
                this.employerlifetimeAmountPattern = true;
            }
            if (varToBeFalse == 'dudctionAmountPercentPattern') {
                this.dudctionAmountPercentPattern = true;
            }
            if (varToBeFalse == 'benefitAmountPercentPattern') {
                this.benefitAmountPercentPattern = true;
            }
            return;
        }

        if (digit != null) {
            this.tempp = digit.toString().split(".");
            if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
                temp = false;
            } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
                temp = false;
            } else {
                temp = true;
            }
        }

        if (varToBeFalse == 'dudctionAmountPattern') {
            this.dudctionAmountPattern = temp;
        }
        if (varToBeFalse == 'monthlyAmountPattern') {
            this.monthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'yearlyAmountPattern') {
            this.yearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'lifetimeAmountPattern') {
            this.lifetimeAmountPattern = temp;
        }
        if (varToBeFalse == 'benefitAmountPattern') {
            this.benefitAmountPattern = temp;
        }
        if (varToBeFalse == 'employermonthlyAmountPattern') {
            this.employermonthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employeryearlyAmountPattern') {
            this.employeryearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employerlifetimeAmountPattern') {
            this.employerlifetimeAmountPattern = temp;
        }
        if (varToBeFalse == 'dudctionAmountPercentPattern') {
            this.dudctionAmountPercentPattern = temp;
        }
        if (varToBeFalse == 'benefitAmountPercentPattern') {
            this.benefitAmountPercentPattern = temp;
        }
    }

    checkEmployeeForm(event) {
        if (event.target.checked == true) {
            this.isEmployeeFormdisabled = true;
        } else {
            this.isEmployeeFormdisabled = false;
        }
    }

    checkEmployerForm(event) {
        if (event.target.checked == true) {
            this.isEmployerFormdisabled = true;
        } else {
            this.isEmployerFormdisabled = false;
        }

    }
    checkdedmonthlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.monthlyAmount != null && this.model.yearlyAmount != null && this.yearlyAmountPattern && this.monthlyAmountPattern) {
            if (temp >= this.model.yearlyAmount && varrr == 'dedmonthly') {
                this.isdedYearly = false;
            }
            else if (temp <= this.model.monthlyAmount && varrr == 'dedyearly') {
                this.isdedYearly = false;
            }
            else if (temp < this.model.yearlyAmount && varrr == 'dedmonthly') {
                this.isdedYearly = true;
            }
            else if (temp > this.model.monthlyAmount && varrr == 'dedyearly') {
                this.isdedYearly = true;
            }
        }
        else {
            this.isdedYearly = true;
        }

    }
    checkbenmonthlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.employermonthlyAmount != null && this.model.employeryearlyAmount != null && this.employermonthlyAmountPattern && this.employeryearlyAmountPattern) {
            if (temp >= this.model.employeryearlyAmount && varrr == 'benmonthly') {
                this.isbenYearly = false;
            }
            else if (temp <= this.model.employermonthlyAmount && varrr == 'benyearly') {
                this.isbenYearly = false;
            }
            else if (temp < this.model.employeryearlyAmount && varrr == 'benmonthly') {
                this.isbenYearly = true;
            }
            else if (temp > this.model.employermonthlyAmount && varrr == 'benyearly') {
                this.isbenYearly = true;
            }
        }
        else {
            this.isbenYearly = true;
        }

    }
    checkdedyearlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.lifetimeAmount != null && this.model.yearlyAmount != null && this.lifetimeAmountPattern && this.yearlyAmountPattern) {
            if (temp > this.model.yearlyAmount && varrr == 'dedlifetime') {
                this.isdedLifetime = true;
            }
            else if (temp < this.model.lifetimeAmount && varrr == 'dedyearly') {
                this.isdedLifetime = true;
            }
            else if (temp <= this.model.yearlyAmount && varrr == 'dedlifetime') {
                this.isdedLifetime = false;
            }
            else if (temp >= this.model.lifetimeAmount && varrr == 'dedyearly') {
                this.isdedLifetime = false;
            }
        }
        else {
            this.isdedLifetime = true;
        }

    }
    checkbenyearlyamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.employerlifetimeAmount != null && this.model.employeryearlyAmount != null && this.employeryearlyAmountPattern && this.employerlifetimeAmountPattern) {
            if (temp > this.model.employeryearlyAmount && varrr == 'benlifetime') {
                this.isbenLifetime = true;
            }
            else if (temp < this.model.employerlifetimeAmount && varrr == 'benyearly') {
                this.isbenLifetime = true;
            }
            else if (temp <= this.model.employeryearlyAmount && varrr == 'benlifetime') {
                this.isbenLifetime = false;
            }
            else if (temp >= this.model.employerlifetimeAmount && varrr == 'benyearly') {
                this.isbenLifetime = false;
            }
        }
        else {
            this.isbenLifetime = true;
        }
    }

    checkdedlifetimeamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.lifetimeAmount != null && this.model.dudctionAmount != null && this.dudctionAmountPattern && this.lifetimeAmountPattern) {
            if (temp > this.model.dudctionAmount && varrr == 'dedlifetime') {
                this.isdedTotal = true;
            }
            else if (temp < this.model.lifetimeAmount && varrr == 'dedTotal') {
                this.isdedTotal = true;
            }
            else if (temp <= this.model.dudctionAmount && varrr == 'dedlifetime') {
                this.isdedTotal = false;
            }
            else if (temp >= this.model.lifetimeAmount && varrr == 'dedTotal') {
                this.isdedTotal = false;
            }
        }
        else {
            this.isdedTotal = true;
        }
    }

     // Print Screen
     print() {

        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        printContents = printContents.replace(/\r?\n|\r/g, "");
        printContents = printContents.replace(" ", "");
        printContents = printContents.replace(/<!--.*?-->/g, "");
    
        // var postedOnes = this.getElementsByIdStartsWith("print-section", "canvas", "canvasdiv-", printContents);
        // var postedOnes1 = this.getElementsByIdStartsWith("print-section", "canvas", "categorychart-", postedOnes);
        // this.getElementsByIdStartsWith("print-section", "canvas", "innerchart-", postedOnes1);
    
        printContents = document.getElementById('print-section').innerHTML;
    
        popupWin = window.open('', '_blank', 'top=0,left=0,height=400px,width=800px');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
        <link rel="stylesheet" type="text/css" media="screen,print" href="assets/css/bootstrap.min.css">
          <title>MISCELLANEOUS BENEFITS ENROLLMENT</title>
          <style>
            .print-tabel{disaplay:block}
          </style>
        </head>
    <body onload="">${printContents}</body>
    <script type="text/javascript">
    window.print();
    if(navigator.userAgent.match(/iPad/i)){
      window.onfocus=function(){window.close(); }
    }else{        
      window.close();
    }
    </script>
      </html>`
        );
        popupWin.document.close();
    }
    printEmployeeDetails() {
        this.doNotPrintHearder = true;
        this.noPrintTable = true;
        this.printDetails = false;

        setTimeout(() => { this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint() {
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }


    checkbenlifetimeamount(eve, varrr: string) {
        var temp = parseFloat(eve);
        if (this.model.employerlifetimeAmount != null && this.model.benefitAmount != null && this.employerlifetimeAmountPattern && this.benefitAmountPattern) {
            if (temp > this.model.benefitAmount && varrr == 'benlifetime') {
                this.isbenTotal = true;
            }
            else if (temp < this.model.employerlifetimeAmount && varrr == 'benTotal') {
                this.isbenTotal = true;
            }
            else if (temp <= this.model.benefitAmount && varrr == 'benlifetime') {
                this.isbenTotal = false;
            }
            else if (temp >= this.model.employerlifetimeAmount && varrr == 'benTotal') {
                this.isbenTotal = false;
            }
        }
        else {
            this.isbenTotal = true;
        }
    }
}

