"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var router_1 = require("@angular/router");
var miscellaneous_benefits_enrollment_service_1 = require("../../_services/miscellaneous-benefits-enrollment/miscellaneous-benefits-enrollment.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var Observable_1 = require("rxjs/Observable");
var MiscellaneousBenefitsEnrollmentComponent = (function () {
    function MiscellaneousBenefitsEnrollmentComponent(router, miscellaneousBenefitsService, getScreenDetailService, alertService) {
        var _this = this;
        this.router = router;
        this.miscellaneousBenefitsService = miscellaneousBenefitsService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1447';
        this.message = { 'type': '', 'text': '' };
        this.BenefitsId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.tempp = [];
        this.error = { isError: false, errorMessage: '' };
        this.islifeTimeValid = true;
        this.isConfirmationModalOpen = false;
        this.islifeTimeValidamount = true;
        this.dudctionAmountPattern = true;
        this.monthlyAmountPattern = true;
        this.yearlyAmountPattern = true;
        this.lifetimeAmountPattern = true;
        this.benefitAmountPattern = true;
        this.employermonthlyAmountPattern = true;
        this.employeryearlyAmountPattern = true;
        this.employerlifetimeAmountPattern = true;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.isdedYearly = true;
        this.isbenYearly = true;
        this.isdedLifetime = true;
        this.isbenLifetime = true;
        this.isdedTotal = true;
        this.isbenTotal = true;
        this.isEmployeeFormdisabled = false;
        this.isEmployerFormdisabled = false;
        this.frequencyArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semiannually", "Annually"];
        this.employeeMethodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
        this.employerMethodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
        this.getMasterEmployee = [];
        this.myOptions = {
            // other options...
            dateFormat: 'dd-mm-yyyy',
        };
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'BenefitsId';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_FREQUENCY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_ACTION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DEDUCTION_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_DEDUCTION_PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_MONTHLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_YEARLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_LIFETIME_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_MONTHLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_YEARLY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_EMPLOYER_LIFETIME_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_BENEFIT_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MISCELLANEOUS_BENEFITS_STATUS', 'fieldValue': '', 'helpMessage': '' }
        ];
        this.dataSource = Observable_1.Observable.create(function (observer) {
            // Runs on every search
            observer.next(_this.model.desc);
        }).mergeMap(function (token) { return _this.getReportPositionsAsObservable(token); });
        this.employeeIdList = Observable_1.Observable.create(function (observer) {
            // Runs on every search
            observer.next(_this.model.benefitsId);
        }).mergeMap(function (token) { return _this.getEmployeeMasterAsObservable(token); });
    }
    MiscellaneousBenefitsEnrollmentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.getReportPositionsAsObservable = function (token) {
        var query = new RegExp(token, 'i');
        return Observable_1.Observable.of(this.positionClassOptions.filter(function (id) {
            return query.test(id.benefitsId);
        }));
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.getEmployeeMasterAsObservable = function (token) {
        var query = new RegExp(token, 'i');
        return Observable_1.Observable.of(this.getMasterEmployee.filter(function (id) {
            return query.test(id.employeeId);
        }));
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.typeaheadOnSelect = function (e) {
        for (var i = 0; i < this.getMasterEmployee.length; i++) {
            if (this.getMasterEmployee[i].employeeIndexId == e.value) {
                this.employeeDesc = this.getMasterEmployee[i].employeeFirstName;
                break;
            }
            else {
                this.employeeDesc = '';
            }
        }
        console.log('Selected value: ', e.value);
        //this.model.benefitsId = e.value;
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        }
        else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        }
        else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.miscellaneousBenefitsService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.miscellaneousBenefitsService.getPositionClassList().then(function (data) {
                _this.positionClassOptions = data.result.records;
                console.log("Data class options : " + data.result.records);
            });
        });
        this.miscellaneousBenefitsService.getAllEmployeeMaster().then(function (data) {
            _this.getMasterEmployee = data.result;
        });
    };
    // Open form for create location
    MiscellaneousBenefitsEnrollmentComponent.prototype.Create = function () {
        var _this = this;
        this.modelStartDate = null;
        this.modelEndDate = null;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.islifeTimeValidamount = true;
        this.isEmployerFormdisabled = false;
        this.isEmployeeFormdisabled = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 800);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            benefitsId: null,
            desc: '',
            arbicDesc: '',
            frequency: 0,
            startDate: null,
            endDate: null,
            inactive: false,
            method: 0,
            dudctionAmount: null,
            dudctionPercent: null,
            monthlyAmount: null,
            yearlyAmount: null,
            lifetimeAmount: null,
            empluyeeerinactive: false,
            empluyeeermethod: 0,
            benefitAmount: null,
            benefitPercent: null,
            employermonthlyAmount: null,
            employeryearlyAmount: null,
            employerlifetimeAmount: null
        };
    };
    // Clear form to reset to default blank
    MiscellaneousBenefitsEnrollmentComponent.prototype.Clear = function (f) {
        f.resetForm({ frequency: 0, method: 0, empluyeeermethod: 0 });
        this.isEmployerFormdisabled = false;
        this.isEmployeeFormdisabled = false;
    };
    //function call for creating new location
    MiscellaneousBenefitsEnrollmentComponent.prototype.CreateMiscellaneousBenefits = function (f, event) {
        var _this = this;
        this.model.startDate = this.frmStartDate;
        this.model.endDate = this.frmEndDate;
        event.preventDefault();
        var locIdx = this.model.benefitsId;
        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.miscellaneousBenefitsService.checkDuplicateMiscellaneousBenefitsId(locIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    //Call service api for Creating new Deduction Code
                    _this.miscellaneousBenefitsService.createMiscellaneousBenefits(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Deduction Code
                            _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    //edit department by row
    MiscellaneousBenefitsEnrollmentComponent.prototype.edit = function (row) {
        this.error = { isError: false, errorMessage: '' };
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.BenefitsId = row.benefitsId;
        this.isUnderUpdate = true;
        this.islifeTimeValidamount = true;
        this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
        this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
        this.model.startDate = this.frmStartDate;
        this.model.endDate = this.frmEndDate;
        this.BenefitsIdvalue = this.model.benefitsId;
        this.benefitsId = this.model.benefitsId;
        if (this.model.inactive == true) {
            this.isEmployeeFormdisabled = true;
        }
        else {
            this.isEmployeeFormdisabled = false;
        }
        if (this.model.empluyeeerinactive == true) {
            this.isEmployerFormdisabled = true;
        }
        else {
            this.isEmployerFormdisabled = false;
        }
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        this.model.benefitsId = this.BenefitsIdvalue;
        //Call service api for updating selected department
        this.miscellaneousBenefitsService.updateMiscellaneousBenefits(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    MiscellaneousBenefitsEnrollmentComponent.prototype.delete = function () {
        var _this = this;
        var selectedMiscellaneousBenefitss = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedMiscellaneousBenefitss.push(this.selected[i].id);
        }
        this.miscellaneousBenefitsService.deleteMiscellaneousBenefits(selectedMiscellaneousBenefitss).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message + " !";
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    MiscellaneousBenefitsEnrollmentComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    MiscellaneousBenefitsEnrollmentComponent.prototype.updateFilter = function (event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    };
    // Set default page size
    MiscellaneousBenefitsEnrollmentComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        this.delete();
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.CheckNumber = function (event) {
        if (isNaN(event.target.value) == true) {
            this.model.benefitsId = 0;
            return false;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkFrequency = function (event) {
        if (event.target.value == 0) {
            return false;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.formatDateFordatePicker = function (strDate) {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        }
        else {
            return null;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.onStartDateChanged = function (event) {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        console.log('this.frmStartDate', event);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.onEndDateChanged = function (event) {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
        console.log('this.frmEndDate', this.frmEndDate, event);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.sortColumn = function (val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            }
            else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkdecimal = function (digit) {
        console.log(digit);
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValid = false;
            console.log(this.islifeTimeValid);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValid = false;
            console.log(this.islifeTimeValid);
        }
        else {
            this.islifeTimeValid = true;
            console.log(this.islifeTimeValid);
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkdecimalamount = function (digit, varToBeFalse) {
        var temp = true;
        console.log('digit', digit);
        if (digit == '' || digit == null) {
            if (varToBeFalse == 'dudctionAmountPattern') {
                this.dudctionAmountPattern = true;
            }
            if (varToBeFalse == 'monthlyAmountPattern') {
                this.monthlyAmountPattern = true;
            }
            if (varToBeFalse == 'yearlyAmountPattern') {
                this.yearlyAmountPattern = true;
            }
            if (varToBeFalse == 'lifetimeAmountPattern') {
                this.lifetimeAmountPattern = true;
            }
            if (varToBeFalse == 'benefitAmountPattern') {
                this.benefitAmountPattern = true;
            }
            if (varToBeFalse == 'employermonthlyAmountPattern') {
                this.employermonthlyAmountPattern = true;
            }
            if (varToBeFalse == 'employeryearlyAmountPattern') {
                this.employeryearlyAmountPattern = true;
            }
            if (varToBeFalse == 'employerlifetimeAmountPattern') {
                this.employerlifetimeAmountPattern = true;
            }
            return;
        }
        if (digit != null) {
            this.tempp = digit.toString().split(".");
            if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            }
            else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            }
            else {
                temp = true;
            }
        }
        if (varToBeFalse == 'dudctionAmountPattern') {
            this.dudctionAmountPattern = temp;
        }
        if (varToBeFalse == 'monthlyAmountPattern') {
            this.monthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'yearlyAmountPattern') {
            this.yearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'lifetimeAmountPattern') {
            this.lifetimeAmountPattern = temp;
        }
        if (varToBeFalse == 'benefitAmountPattern') {
            this.benefitAmountPattern = temp;
        }
        if (varToBeFalse == 'employermonthlyAmountPattern') {
            this.employermonthlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employeryearlyAmountPattern') {
            this.employeryearlyAmountPattern = temp;
        }
        if (varToBeFalse == 'employerlifetimeAmountPattern') {
            this.employerlifetimeAmountPattern = temp;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkEmployeeForm = function (event) {
        if (event.target.checked == true) {
            this.isEmployeeFormdisabled = true;
        }
        else {
            this.isEmployeeFormdisabled = false;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkEmployerForm = function (event) {
        if (event.target.checked == true) {
            this.isEmployerFormdisabled = true;
        }
        else {
            this.isEmployerFormdisabled = false;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkdedmonthlyamount = function (eve, varrr) {
        var temp = parseFloat(eve);
        if (this.model.monthlyAmount != null && this.model.yearlyAmount != null && this.yearlyAmountPattern && this.monthlyAmountPattern) {
            if (temp >= this.model.yearlyAmount && varrr == 'dedmonthly') {
                this.isdedYearly = false;
                console.log(this.isdedYearly);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp <= this.model.monthlyAmount && varrr == 'dedyearly') {
                this.isdedYearly = false;
                console.log(this.isdedYearly);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp < this.model.yearlyAmount && varrr == 'dedmonthly') {
                this.isdedYearly = true;
                console.log(this.isdedYearly);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp > this.model.monthlyAmount && varrr == 'dedyearly') {
                this.isdedYearly = true;
                console.log(this.isdedYearly);
                console.log(varrr);
                console.log(temp);
            }
        }
        else {
            this.isdedYearly = true;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkbenmonthlyamount = function (eve, varrr) {
        var temp = parseFloat(eve);
        if (this.model.employermonthlyAmount != null && this.model.employeryearlyAmount != null && this.employermonthlyAmountPattern && this.employeryearlyAmountPattern) {
            if (temp >= this.model.employeryearlyAmount && varrr == 'benmonthly') {
                this.isbenYearly = false;
                console.log(this.isbenYearly);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp <= this.model.employermonthlyAmount && varrr == 'benyearly') {
                this.isbenYearly = false;
                console.log(this.isbenYearly);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp < this.model.employeryearlyAmount && varrr == 'benmonthly') {
                this.isbenYearly = true;
                console.log(this.isbenYearly);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp > this.model.employermonthlyAmount && varrr == 'benyearly') {
                this.isbenYearly = true;
                console.log(this.isbenYearly);
                console.log(varrr);
                console.log(temp);
            }
        }
        else {
            this.isbenYearly = true;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkdedyearlyamount = function (eve, varrr) {
        var temp = parseFloat(eve);
        if (this.model.lifetimeAmount != null && this.model.yearlyAmount != null && this.lifetimeAmountPattern && this.yearlyAmountPattern) {
            if (temp > this.model.yearlyAmount && varrr == 'dedlifetime') {
                this.isdedLifetime = true;
                console.log(this.isdedLifetime);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp < this.model.lifetimeAmount && varrr == 'dedyearly') {
                this.isdedLifetime = true;
                console.log(this.isdedLifetime);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp <= this.model.yearlyAmount && varrr == 'dedlifetime') {
                this.isdedLifetime = false;
                console.log(this.isdedLifetime);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp >= this.model.lifetimeAmount && varrr == 'dedyearly') {
                this.isdedLifetime = false;
                console.log(this.isdedLifetime);
                console.log(varrr);
                console.log(temp);
            }
        }
        else {
            this.isdedLifetime = true;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkbenyearlyamount = function (eve, varrr) {
        var temp = parseFloat(eve);
        if (this.model.employerlifetimeAmount != null && this.model.employeryearlyAmount != null && this.employeryearlyAmountPattern && this.employerlifetimeAmountPattern) {
            if (temp > this.model.employeryearlyAmount && varrr == 'benlifetime') {
                this.isbenLifetime = true;
                console.log(this.isbenLifetime);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp < this.model.employerlifetimeAmount && varrr == 'benyearly') {
                this.isbenLifetime = true;
                console.log(this.isbenLifetime);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp <= this.model.employeryearlyAmount && varrr == 'benlifetime') {
                this.isbenLifetime = false;
                console.log(this.isbenLifetime);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp >= this.model.employerlifetimeAmount && varrr == 'benyearly') {
                this.isbenLifetime = false;
                console.log(this.isbenLifetime);
                console.log(varrr);
                console.log(temp);
            }
        }
        else {
            this.isbenLifetime = true;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkdedlifetimeamount = function (eve, varrr) {
        var temp = parseFloat(eve);
        if (this.model.lifetimeAmount != null && this.model.dudctionAmount != null && this.dudctionAmountPattern && this.lifetimeAmountPattern) {
            if (temp > this.model.dudctionAmount && varrr == 'dedlifetime') {
                this.isdedTotal = true;
                console.log(this.isdedTotal);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp < this.model.lifetimeAmount && varrr == 'dedTotal') {
                this.isdedTotal = true;
                console.log(this.isdedTotal);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp <= this.model.dudctionAmount && varrr == 'dedlifetime') {
                this.isdedTotal = false;
                console.log(this.isdedTotal);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp >= this.model.lifetimeAmount && varrr == 'dedTotal') {
                this.isdedTotal = false;
                console.log(this.isdedTotal);
                console.log(varrr);
                console.log(temp);
            }
        }
        else {
            this.isdedTotal = true;
        }
    };
    MiscellaneousBenefitsEnrollmentComponent.prototype.checkbenlifetimeamount = function (eve, varrr) {
        var temp = parseFloat(eve);
        if (this.model.employerlifetimeAmount != null && this.model.benefitAmount != null && this.employerlifetimeAmountPattern && this.benefitAmountPattern) {
            if (temp > this.model.benefitAmount && varrr == 'benlifetime') {
                this.isbenTotal = true;
                console.log(this.isbenTotal);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp < this.model.employerlifetimeAmount && varrr == 'benTotal') {
                this.isbenTotal = true;
                console.log(this.isbenTotal);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp <= this.model.benefitAmount && varrr == 'benlifetime') {
                this.isbenTotal = false;
                console.log(this.isbenTotal);
                console.log(varrr);
                console.log(temp);
            }
            else if (temp >= this.model.employerlifetimeAmount && varrr == 'benTotal') {
                this.isbenTotal = false;
                console.log(this.isbenTotal);
                console.log(varrr);
                console.log(temp);
            }
        }
        else {
            this.isbenTotal = true;
        }
    };
    return MiscellaneousBenefitsEnrollmentComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], MiscellaneousBenefitsEnrollmentComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], MiscellaneousBenefitsEnrollmentComponent.prototype, "myScrollContainer", void 0);
MiscellaneousBenefitsEnrollmentComponent = __decorate([
    core_1.Component({
        selector: 'app-miscellaneous-benefits-enrollment',
        templateUrl: './miscellaneous-benefits-enrollment.component.html',
        providers: [miscellaneous_benefits_enrollment_service_1.MiscellaneousBenefitsService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        miscellaneous_benefits_enrollment_service_1.MiscellaneousBenefitsService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], MiscellaneousBenefitsEnrollmentComponent);
exports.MiscellaneousBenefitsEnrollmentComponent = MiscellaneousBenefitsEnrollmentComponent;
//# sourceMappingURL=miscellaneous-benefits-enrollment.component.js.map