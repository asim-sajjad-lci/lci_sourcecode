import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { CalculateChecksModule } from '../../_models/calculate-checks/calculate-checks';
import { CalculateChecksService } from '../../_services/calculate-checks/calculate-checks.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { BuildCheckCommonService } from '../../../_sharedresource/_services/build-check-common.service';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { setRootDomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';
@Component({
    selector: 'calculate-checks',
    templateUrl: './calculate-checks.component.html',
    providers: [CalculateChecksService, CommonService]
})

// export Department component to make it available for other classes
export class CalculateChecksComponent {

    showloader: boolean =false;

    pageCalc = new Page();
    pgeOne = new Page();
    pgeTwo = new Page();
    rows = new Array<CalculateChecksModule>();
    temp = new Array<CalculateChecksModule>();
    selected = [];
    moduleCode = "M-1011";
    screenCode = "S-1470";
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    departmentId = {};
    searchKeyword = '';
    getDepartment: any[] = [];
    ddPageSize: number = 5;
    ddPageSize1: number = 5;
    model: CalculateChecksModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    departmentIdvalue: number;
    batchIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    modelStartDate: any = null;
    postingDateValidation: boolean = false;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
    emdescription;
    CodeId;
    codeDesc;
    batchDropdownList = [];
    defaultId;
    description;
    createdby;
    payrollDate;
    buildtime;
    bId;
    transDataList: Observable<any>;
    transDeductionDataList: Observable<any>;
    transBenefitDataList: Observable<any>;
    employeeIdList: Observable<any>;
    defaultIdList: Observable<any>;
    defaultIdDropDownList = [];
    transTypeIdList = [];
    employeeIdOption = [];
    isDeleteBatch: boolean = false;
    batchId;
    empId = [];
    defaultID;
    all: boolean = true;
    codeType = ["", "Pay Code", "Deduction", "Benefit"]
    error: any = { isError: false, errorMessage: '' };
    count: any;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    processedRows: any = [];
    Gridrows: any = [];
    GridAllrows: any = [];
    batchIdvalue: any;
    minDate: Date = new Date();
    processedChecked: boolean = false;
    sortColumnNameArray = ["", "employee", "department", "codeId", "projectId"]
    sortColumnName = 'employee';
    idForPagination: any;
    loaderImg: boolean = false;

    CALCULATE_CHECKS_SEARCH: any;
    CALCULATE_CHECKS_DEFAULT_ID: any;
    CALCULATE_CHECKS_CREATED_BY: any;
    CALCULATE_CHECKS_DESCRIPTION: any;
    CALCULATE_CHECKS_PAYROLL_DATE: any;
    CALCULATE_CHECKS_BUILD_TIME: any;
    CALCULATE_CHECKS_POSTED_DATE: any;
    CALCULATE_CHECKS_VIEW_BY: any;
    CALCULATE_CHECKS_RADIO_ALL: any;
    CALCULATE_CHECKS_RADIO_FROM: any;
    CALCULATE_CHECKS_TO: any;
    CALCULATE_CHECKS_EMPLOYEE_ID: any;
    CALCULATE_CHECKS_NO: any;
    CALCULATE_CHECKS_DEPARTMENT_ID: any;
    CALCULATE_CHECKS_TRX_TYPE: any;
    CALCULATE_CHECKS_CODE_ID: any;
    CALCULATE_CHECKS_AMOUNT: any;
    CALCULATE_CHECKS_STATUS: any;
    CALCULATE_CHECKS_PROCESS_LABEL: any;
    CALCULATE_CHECKS_DISTRIBUTION_LABEL: any;
    CALCULATE_CHECKS_PRINT_LABEL: any;
    CALCULATE_CHECKS_CLEAR_LABEL: any;
    CALCULATE_CHECKS_CREATE_FORM_LABEL: any;
    CALCULATE_CHECKS_UPDATE_FORM_LABEL: any;
    CALCULATE_CHECKS_UPDATE_LABEL: any;
    CALCULATE_CHECKS_CREATE_LABEL: any;
    GENERATE_CSV_LABEL: any;
    GENERATE_PALSLIP_LABEL: any;
    serialNoDisplay: any[] = [];

    totalEmp = 0;
    totalPayCode = 0;
    totalDeduction = 0;
    totalBenefit = 0;
    totalNet = 0;

    constructor(
        private router: Router,
        private calculateChecksService: CalculateChecksService,
        private getScreenDetailService: GetScreenDetailService,
        private buildCheckCommonService: BuildCheckCommonService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.pageCalc.pageNumber = 0;
        this.pageCalc.size = 5;
        this.pageCalc.sortOn = 'id';
        this.pageCalc.sortBy = 'DESC';
        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'CALCULATE_CHECKS_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DEFAULT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_CREATED_BY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_PAYROLL_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_BUILD_TIME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_POSTED_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_VIEW_BY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_RADIO_ALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_RADIO_FROM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_TO', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_NO', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_TRX_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_CODE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_STATUS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_PROCESS_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DISTRIBUTION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_PRINT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'GENERATE_CSV_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'GENERATE_PALSLIP_LABEL', 'fieldValue': '', 'helpMessage': '' },
            
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_UPDATE', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_EDIT', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_ACTIONL', 'fieldValue': '', 'helpMessage': '' },
        ];
        this.CALCULATE_CHECKS_SEARCH = this.defaultFormValues[0];
        this.CALCULATE_CHECKS_DEFAULT_ID = this.defaultFormValues[1];
        this.CALCULATE_CHECKS_CREATED_BY = this.defaultFormValues[2];
        this.CALCULATE_CHECKS_DESCRIPTION = this.defaultFormValues[3];
        this.CALCULATE_CHECKS_PAYROLL_DATE = this.defaultFormValues[4];
        this.CALCULATE_CHECKS_BUILD_TIME = this.defaultFormValues[5];
        this.CALCULATE_CHECKS_POSTED_DATE = this.defaultFormValues[6];
        this.CALCULATE_CHECKS_VIEW_BY = this.defaultFormValues[7];
        this.CALCULATE_CHECKS_RADIO_ALL = this.defaultFormValues[8];
        this.CALCULATE_CHECKS_RADIO_FROM = this.defaultFormValues[9];
        this.CALCULATE_CHECKS_TO = this.defaultFormValues[10];
        this.CALCULATE_CHECKS_EMPLOYEE_ID = this.defaultFormValues[11];
        this.CALCULATE_CHECKS_NO = this.defaultFormValues[12];
        this.CALCULATE_CHECKS_DEPARTMENT_ID = this.defaultFormValues[13];
        this.CALCULATE_CHECKS_TRX_TYPE = this.defaultFormValues[14];
        this.CALCULATE_CHECKS_CODE_ID = this.defaultFormValues[15];
        this.CALCULATE_CHECKS_AMOUNT = this.defaultFormValues[16];
        this.CALCULATE_CHECKS_STATUS = this.defaultFormValues[17];
        this.CALCULATE_CHECKS_PROCESS_LABEL = this.defaultFormValues[18];
        this.CALCULATE_CHECKS_DISTRIBUTION_LABEL = this.defaultFormValues[19];
        this.CALCULATE_CHECKS_PRINT_LABEL = this.defaultFormValues[20];
        this.CALCULATE_CHECKS_CLEAR_LABEL = this.defaultFormValues[21];
        this.CALCULATE_CHECKS_CREATE_FORM_LABEL = this.defaultFormValues[22];
        this.CALCULATE_CHECKS_UPDATE_FORM_LABEL = this.defaultFormValues[23];
        this.CALCULATE_CHECKS_UPDATE_LABEL = this.defaultFormValues[24];
        this.CALCULATE_CHECKS_CREATE_LABEL = this.defaultFormValues[25];
        this.GENERATE_CSV_LABEL = this.defaultFormValues[27];
        this.GENERATE_PALSLIP_LABEL = this.defaultFormValues[28];
        this.defaultIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.defaultId);
        }).mergeMap((token: string) => this.getDefaultIdAsObservable(token));

        this.transDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getpayCodeIdAsObservable(token));


        this.transDeductionDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getdeductionCodeIdAsObservable(token));

        this.transBenefitDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getbenefitCodeIdAsObservable(token));

        //   this.employeeIdList = Observable.create((observer: any) => {
        //     // Runs on every search
        //     observer.next(this.employeeId);
        //   }).mergeMap((token: string) => this.getemployeeIdAsObservable(token));
    }

    // Screen initialization
    ngOnInit() {
        this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        console.log('this.buildCheckCommonService.defaultIdDetailsForDistribution', this.buildCheckCommonService.defaultIdDetailsForDistribution)
        // if (this.buildCheckCommonService.defaultIdDetailsForDistribution['id'] != undefined) {
        //     this.Create();
        //     this.calculateChecksService.getDefaultID(this.buildCheckCommonService.defaultIdDetailsForDistribution['id'], true).then(data => {
        //         this.Gridrows = data.result.records;
        //         this.description = this.buildCheckCommonService.defaultIdDetailsForDistribution['description'];
        //         this.payrollDate = this.buildCheckCommonService.defaultIdDetailsForDistribution['payrollDate'];
        //         this.buildtime = this.buildCheckCommonService.defaultIdDetailsForDistribution['buildtime'];
        //         this.defaultId = this.buildCheckCommonService.defaultIdDetailsForDistribution['defaultId'];
        //         this.defaultID = this.buildCheckCommonService.defaultIdDetailsForDistribution['id'];
        //         this.createdby = this.buildCheckCommonService.defaultIdDetailsForDistribution['createdby'];
        //     })

        // } else {
        //     this.model = {
        //         list: [{
        //             auditTransNum: null,
        //             employeeMaster: {
        //                 employeeIndexId: null
        //             },
        //             dimensions: {
        //                 id: null
        //             },
        //             totalAmount: null,
        //             recordStatus: null,
        //             codeType: null

        //         }]
        //     };
        // }
        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


        //Following shifted from SetPage for better performance
        this.calculateChecksService.getDepartments().then(data => {
            this.getDepartment = data.result;
            //console.log("Data class options : "+this.getDepartment);
        });
        // this.Create();
    }

    getDefaultIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.defaultIdDropDownList.filter((id: any) => {
                return query.test(id.defaultID);
            })
        );
    }
    getpayCodeIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.payCodeId);
            })
        );
    }

    getdeductionCodeIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.diductionId);
            })
        );
    }

    getbenefitCodeIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.benefitId);
            })
        );
    }

    //   getemployeeIdAsObservable(token: string): Observable<any> {
    //     let query = new RegExp(token, 'i');
    //     return Observable.of(
    //       this.employeeIdOption.filter((id: any) => {
    //         return query.test(id.employeeId);
    //       })
    //     );
    //   }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        this.buildCheckCommonService.batchesForProcess = [];
        this.pgeTwo = this.pageCalc;
        this.idForPagination = e.item.id;
        this.loaderImg = true;
        this.calculateChecksService.getDefaultID(e.item.id, true, this.pgeTwo).then(data => {
            console.log("Data on Typehead", data);
            this.loaderImg = false;
            this.Gridrows = data.result.records;
            this.pgeTwo.size = data.result.pageSize;
            this.pgeTwo.totalElements = data.result.totalCount;
            this.pgeTwo.pageNumber = data.result.pageNumber;
            if (this.Gridrows.length > 0) {
                this.processedChecked = false;
                for (let k = 0; k < this.Gridrows.length; k++) {
                    // this.count = k + 1;
                    if (this.Gridrows[k].status == 'fail') {
                        this.processedChecked = true;
                    }
                }
                this.serialNoDisplayFunction(this.pgeTwo);
            }

            if (this.Gridrows.length > 0) {
                this.empId = [];
                for (let i = 0; i < this.Gridrows.length; i++) {
                    this.empId.push(this.Gridrows[i].employeePrimaryId)
                }
                this.buildCheckCommonService.batchesForProcess = this.empId;
                this.calculateChecksService.getUserDetailsByUserId(data.result.records["0"].userId).then(data => {
                    if (data.result != undefined) {
                        this.createdby = data.result.primaryFullName;
                        this.buildCheckCommonService.defaultIdDetailsForDistribution = {
                            id: this.defaultID,
                            payrollDate: this.payrollDate,
                            buildtime: this.buildtime,
                            defaultId: this.defaultId,
                            createdby: this.createdby,
                            description: this.description,
                            postDate: this.modelStartDate,
                            empId: []
                        }
                    }
                    this.loaderImg = false;
                })
            }
this.setData(e.item.id);

        }).catch(error => {
            this.hasMsg = true;
            this.loaderImg = false;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
        this.description = e.item.desc;
        let paycodedate = new Date(e.item.buildDate)
        this.payrollDate = paycodedate;
        this.buildtime = e.item.buildtime;
        this.defaultId = e.item.defaultID;
        this.defaultID = e.item.id;
    }

    typeaheadPayCodeOnSelect(e: TypeaheadMatch): void {
        this.codeDesc = e.item.description;

    }

    typeaheadDeductionCodeOnSelect(e: TypeaheadMatch): void {
        this.codeDesc = e.item.discription;

    }


    typeaheadBenefitCodeOnSelect(e: TypeaheadMatch): void {
        this.codeDesc = e.item.desc;

    }

    typeaheadEmployeeOnSelect(e: TypeaheadMatch): void {

        this.emdescription = e.item.employeeFirstName;

    }
    //setting pagination
    setPageCalChecks(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.pgeOne.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.pgeOne.sortOn = this.pgeOne.sortOn;
        } else {
            this.pgeOne.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.pgeOne.sortBy = this.pgeOne.sortBy;
        } else {
            this.pgeOne.sortBy = pageInfo.sortBy;
        }

        //this.pageCalc.searchKeyword = '';
        // this.transactionEntryService.getlist(this.pageCalc, this.searchKeyword).subscribe(pagedData => {
        //     this.pageCalc = pagedData.page;
        //     this.rows = pagedData.data;
        // });

        /* Commented the code due to paging error
        this.calculateChecksService.getlist(this.pageCalc, this.pageCalc.searchKeyword).then(pagedata => {
            this.rows = pagedata.result.records;
            this.pageCalc.size = pagedata.result.pageSize;
            this.pageCalc.totalElements = pagedata.result.totalCount;
            this.pageCalc.pageNumber = pagedata.result.pageNumber;
        })*/ 
        this.calculateChecksService.getAllBatch(this.pageCalc.searchKeyword).then(data => {
            this.batchDropdownList = data.result;
        });

        // this.calculateChecksService.getEmployeeId(this.pageCalc).then(data => 
        //     {
        //         this.employeeIdOption = data.result.records;
        //     }
        // );
        this.calculateChecksService.getAllUnprocessedData(this.pageCalc).then(data => {
            this.defaultIdDropDownList = data.result.records;
        })
        this.calculateChecksService.getAllDistributions(this.pgeOne, this.pageCalc.searchKeyword).then(data => {
            this.processedRows = data.result.records;
            this.pgeOne.size = data.result.pageSize;
            this.pgeOne.totalElements = data.result.totalCount;
            this.pgeOne.pageNumber = data.result.pageNumber;
        })
    }
    setPageTransList(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.pgeTwo.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.pgeTwo.sortOn = this.pgeTwo.sortOn;
        } else {
            this.pgeTwo.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.pgeTwo.sortBy = this.pgeTwo.sortBy;
        } else {
            this.pgeTwo.sortBy = pageInfo.sortBy;
        }

        // this.pgeTwo.searchKeyword = '';
        this.calculateChecksService.getDefaultID(this.idForPagination, true, this.pgeTwo).then(data => {
            console.log("Data on page selection", data)
            this.Gridrows = data.result.records;
            this.pgeTwo.size = data.result.pageSize;
            this.pgeTwo.totalElements = data.result.totalCount;
            this.pgeTwo.pageNumber = data.result.pageNumber;
            this.serialNoDisplayFunction(this.pgeTwo);
        });
        // this.transactionEntryService.getlist(this.pageCalc, this.searchKeyword).subscribe(pagedData => {
        //     this.pageCalc = pagedData.page;
        //     this.rows = pagedData.data;
        // });

        // this.calculateChecksService.getlist(this.pageCalc, this.searchKeyword).then(pagedata => {
        //     this.rows = pagedata.result.records;

        // })
        // this.calculateChecksService.getAllBatch(this.searchKeyword).then(data => {
        //     this.batchDropdownList = data.result;
        // });

        // this.calculateChecksService.getEmployeeId(this.pageCalc).then(data => 
        //     {
        //         this.employeeIdOption = data.result.records;
        //     }
        // );
        // this.calculateChecksService.getAllUnprocessedData(this.pageCalc).then(data => {
        //     this.defaultIdDropDownList = data.result.records;
        // })
        // this.calculateChecksService.getAllDistributions(this.pageCalc).then(data => {
        //     this.processedRows = data.result.records;
        //     this.pageCalc.size = data.result.pageSize;
        //     this.pageCalc.totalElements = data.result.totalCount;
        //     this.pageCalc.pageNumber = data.result.pageNumber;
        // })
    }

    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.defaultId = '';
        this.description = '';
        this.payrollDate = '';
        this.buildtime = '';
        this.createdby = '';
        this.Gridrows = [];
        this.selected = [];
        this.processedChecked = true;
        this.modelStartDate = '';

        this.totalEmp = 0;
        this.totalPayCode = 0;
        this.totalDeduction = 0;
        this.totalBenefit = 0;
        this.totalNet = 0;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            list: [{
                auditTransNum: null,
                employeeMaster: {
                    employeeIndexId: null
                },
                dimensions: {
                    id: null
                },
                totalAmount: null,
                recordStatus: null,
                codeType: null,
                postingDate:null
            }]
        };
        this.batchId = 0;
        var temp = {
            auditTransNum: null,
            employeeMaster: {
                employeeIndexId: null
            },
            dimensions: {
                id: null
            },
            totalAmount: null,
            recordStatus: null,
            codeType: null

        }
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ batchId: 0 });
        this.processedChecked = true;
        this.defaultId = '';
        this.description = '';
        this.payrollDate = '';
        this.buildtime = '';
        this.createdby = '';
        this.Gridrows = [];
        this.totalEmp = 0;
        this.totalPayCode = 0;
        this.totalDeduction = 0;
        this.totalBenefit = 0;
        this.totalNet = 0;
        this.postingDateValidation = false;
    }

    //function call for creating new department
    createCalculateChecks(f: NgForm, event: Event) {
        event.preventDefault();
        // this.model.transactionEntryDetail['fromDate']=this.frmStartDate;
        // this.model.transactionEntryDetail['toDate'] =this.frmEndDate;
        // var deptIdx = this.model.id;

        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        // if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
        //     this.isConfirmationModalOpen = true;
        //     this.isDeleteAction = false;
        // }
        // else {
        //     //Check for duplicate Division Id according to it create new division
        //     this.calculateChecksService.checkDuplicateDeptId(deptIdx).then(response => {
        //         if (response && response.code == 302 && response.result && response.result.isRepeat) {
        //             this.duplicateWarning = true;
        //             this.message.type = "success";

        //             window.setTimeout(() => {
        //                 this.isSuccessMsg = false;
        //                 this.isfailureMsg = true;
        //                 this.showMsg = true;
        //                 window.setTimeout(() => {
        //                     this.showMsg = false;
        //                     this.duplicateWarning = false;
        //                 }, 4000);
        //                 this.message.text = response.btiMessage.message;
        //             }, 100);
        //         } else {
        //Call service api for Creating new department

        var temp = {
            auditTransNum: null,
            employeeMaster: {
                employeeIndexId: null
            },
            dimensions: {
                id: null
            },
            totalAmount: null,
            recordStatus: null,
            codeType: null

        }

        for (var i = 0; i < this.selected.length - 1; i++) {
            this.model.list.push(temp);
        }

        for (var i = 0; i < this.selected.length; i++) {
            this.model.list[i].auditTransNum = this.selected[i].transcationNumber;
            this.model.list[i].employeeMaster.employeeIndexId = this.selected[i].employeePrimaryId;
            this.model.list[i].dimensions.id = this.selected[i].projectPrimaryId;
            this.model.list[i].totalAmount = this.selected[i].amount;
            this.model.list[i].codeType = this.selected[i].trxType;
        }
        console.log('this.selected', this.selected)
        console.log('this.model', this.model)
        this.loaderImg = true;
        this.calculateChecksService.createCalculateCheck(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMessage = false;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);

                    this.messageText = data.btiMessage.message;
                }, 100);

                this.hasMsg = true;
                this.loaderImg = false;
                f.resetForm();
                //Refresh the Grid data after adding new department
                this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });
            }
        }).catch(error => {
            this.hasMsg = true;
            this.loaderImg = false;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    // }).catch(error => {
    //     this.hasMsg = true;
    //     window.setTimeout(() => {
    //         this.isSuccessMsg = false;
    //         this.isfailureMsg = true;
    //         this.showMsg = true;
    //         this.messageText = "Server error. Please contact admin.";
    //     }, 100)
    // });

    //     }
    // }

    //edit department by row
    // getallCalculateChecks() {

    //     this.calculateChecksService.getDefaultID(this.defaultID, true).then(data => {
    //         this.rows = data.result.records;
    //         for (let k = 0; k < this.rows.length; k++) {
    //             this.count = k + 1;
    //             console.log('>>>>>', this.count);
    //         }
    //         this.calculateChecksService.getUserDetailsByUserId(data.result.records["0"].userId).then(data => {
    //             console.log("User Data", data)
    //             if (data.result != undefined) {
    //                 this.createdby = data.result.primaryFullName;
    //             }
    //         })

    //     })

    // }
    edit(row: CalculateChecksModule) {
        setTimeout(() => {
            window.scrollTo(0, 650);
        }, 10);
        console.log('Row', row)
        this.showCreateForm = true;
        this.postingDateValidation = false;
        this.model = Object.assign({}, row);
        this.idForPagination = row['id'];
        this.isUnderUpdate = true;
        this.batchId = 0;
        this.defaultId = row['defaultID'];
        this.description = row['desc'];
        let paycodedate = new Date(row['buildDate'])
        this.payrollDate = paycodedate;
        this.buildtime = row['buildtime'];
        this.defaultID = row['id'];
        this.modelStartDate = this.formatDateFordatePicker((row['postingDate'])),
        this.calculateChecksService.getUserDetailsByUserId(row['userId']).then(data => {
            if (data.result != undefined) {
                this.createdby = data.result.primaryFullName;
            }
            this.calculateChecksService.getAllProcessedDefaultID(row['id'], true, this.pgeTwo).then(data => {
                // console.log('Data', data);
                this.Gridrows = data.result.records;
                this.pgeTwo.size = data.result.pageSize;
                this.pgeTwo.totalElements = data.result.totalCount;
                this.pgeTwo.pageNumber = data.result.pageNumber;
                this.serialNoDisplayFunction(this.pgeTwo);
                this.setData(row['id']);
            })
        })

    }

    setData(id) {
        this.totalEmp = 0;
        this.totalPayCode = 0;
        this.totalDeduction = 0;
        this.totalBenefit = 0;
        this.totalNet = 0;
        debugger;
        this.calculateChecksService.getDefaultIDwithoutPaging(id).then(data => {
            this.GridAllrows = data.result.records;

            const groupBy = key => array =>
            array.reduce((objectsByKeyValue, obj) => {
                const value = obj[key];
                objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
                return objectsByKeyValue;
            }, {});

        const groupByEmp = groupBy('employeePrimaryId');
        const groupByCodeType = groupBy('codeType');

       // const groupByColor = groupBy('color');

        let empDetailsList = []
        let empDetails = groupByEmp(this.GridAllrows)
        for (let i in empDetails)
        empDetailsList.push([empDetails[i]]);
         console.log(empDetailsList)
        // console.log(empDetailsList.length)
        this.totalEmp = empDetailsList.length;

        let CodeTypeDetailsList = []
        let CodeTypeDetails = groupByCodeType(this.GridAllrows)
        for (let i in CodeTypeDetails)
        CodeTypeDetailsList.push([CodeTypeDetails[i]]);
         console.log(CodeTypeDetailsList)
        // console.log(CodeTypeDetailsList.length)
        for(let i=0;i<CodeTypeDetailsList.length;i++)
        {
            for(let j=0;j<CodeTypeDetailsList[i][0].length;j++)
            {
               if (CodeTypeDetailsList[i][0][j].codeType == 1)
               {
                   this.totalPayCode += CodeTypeDetailsList[i][0][j].totalAmount
               }
               if (CodeTypeDetailsList[i][0][j].codeType == 2)
               {
                   this.totalDeduction += CodeTypeDetailsList[i][0][j].totalAmount
               }
               if (CodeTypeDetailsList[i][0][j].codeType == 3)
               {
                   this.totalBenefit += CodeTypeDetailsList[i][0][j].totalAmount
               }
            }
            //console.log("iiii",CodeTypeDetailsList[i][0][j].codeType)
        }

        this.totalNet =(this.totalPayCode + this.totalBenefit) - this.totalDeduction


        })
        // console.log(
        //     JSON.stringify({
        //         Employee: groupByBrand(this.Gridrows),
        //         carsByColor: groupByColor(cars)
        //     }, null, 2)
        // );
    }

    Cancel() {
        this.defaultId = '';
        this.description = '';
        this.payrollDate = '';
        this.buildtime = '';
        this.createdby = '';
        this.Gridrows = [];
        this.postingDateValidation = false;
        this.isUnderUpdate = false;
        this.showCreateForm = false;
    }
    transType(event) {
        if (event == 1) {
            this.calculateChecksService.getPayCodeId(this.pageCalc).then(data => {
                this.CodeId = '';
                this.codeDesc = '';
                // this.model.transactionEntryDetail['amount']=null;
                // this.model.transactionEntryDetail['payRate']=null;
                this.transTypeIdList = [];
                this.transTypeIdList = data.result.records;
                console.log('transtpe', this.model);
            }

            )


        }

        if (event == 2) {
            this.calculateChecksService.getDeductionCodeId(this.pageCalc).then(data => {
                // this.model.transactionEntryDetail['amount']=null;
                // this.model.transactionEntryDetail['payRate']=null;
                this.CodeId = '';
                this.codeDesc = '';
                this.transTypeIdList = [];
                this.transTypeIdList = data.result.records;
            }

            )


        }

        if (event == 3) {
            this.calculateChecksService.getBenefitCodeId(this.pageCalc).then(data => {
                // this.model.transactionEntryDetail['amount']=null;
                // this.model.transactionEntryDetail['payRate']=null;
                this.CodeId = '';
                this.codeDesc = '';
                this.transTypeIdList = [];
                this.transTypeIdList = data.result.records;
            }

            )


        }

    }

    // updateStatus() {
    //     this.closeModal();
    //     //Call service api for updating selected department
    //     this.model.id = this.departmentIdvalue;
    //     this.calculateChecksService.updateDepartment(this.model).then(data => {
    //         var datacode = data.code;
    //         if (datacode == 201) {
    //             //Refresh the Grid data after editing department
    //             this.setPage({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });

    //             //Scroll to top after editing department
    //             window.scrollTo(0, 0);
    //             window.setTimeout(() => {
    //                 this.isSuccessMsg = true;
    //                 this.isfailureMsg = false;
    //                 this.showMsg = true;
    //                 window.setTimeout(() => {
    //                     this.showMsg = false;
    //                     this.hasMsg = false;
    //                 }, 4000);
    //                 this.messageText = data.btiMessage.message;
    //                 this.showCreateForm = false;
    //             }, 100);
    //             this.hasMessage = false;
    //             this.hasMsg = true;
    //             window.setTimeout(() => {
    //                 this.showMsg = false;
    //                 this.hasMsg = false;
    //             }, 4000);
    //         }
    //     }).catch(error => {
    //         this.hasMsg = true;
    //         window.setTimeout(() => {
    //             this.isSuccessMsg = false;
    //             this.isfailureMsg = true;
    //             this.showMsg = true;
    //             this.messageText = "Server error. Please contact admin.";
    //         }, 100)
    //     });
    // }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = true;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    getDistribution() {
        if (this.selected.length > 0) {
            // this.empId = [];

            this.showCreateForm = true;

            // this.empId = this.buildCheckCommonService.defaultIdDetailsForDistribution['empId']
            console.log('this.empId', this.empId);
            console.log('this.defaultID', this.defaultID);
            if (this.selected.length == 1) {
                let result = [];
                result.push(this.selected["0"].employeePrimaryId);
                this.calculateChecksService.distribution(result, this.defaultID).then(data => {
                    console.log('Data', data)
                    if (data.result.records != []) {
                        result = data.result.records;
                        this.buildCheckCommonService.buildReport = result;
                        localStorage.setItem('reports', JSON.stringify(result));
                        this.router.navigate(['hcm/buildcheckReport']);
                    }
                })
            }
            if (this.selected.length > 1) {
                this.isSuccessMsg = false;
                this.hasMessage = true;
                this.message.type = 'error';
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = 'Please select only one record.';
                window.scrollTo(0, 0);
            }

        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record.';
            window.scrollTo(0, 0);
        }
    }

    // /* Arrange Array */

    // myFunction() {
    //     this.buildCheckCommonService.batchesForProcess = this.buildCheckCommonService.batchesForProcess.sort(function (a, b) { return a - b });
    // }

    process() {
        if (this.modelStartDate == null || this.modelStartDate == '') {
            this.postingDateValidation = true;
            return
        }
        // else {
        //     this.myFunction();
        // }
        console.log('This EmpId', this.buildCheckCommonService.batchesForProcess)
        console.log('This Post Date', this.model)
        console.log('This Post Date', this.modelStartDate)
        let postedDate = this.modelStartDate.epoc*1000;
        // let batchesForProcess: any[] = []
        // this.buildCheckCommonService.batchesForProcess.filter((ele) => {
        //     if (batchesForProcess.length !== 0) {
        //         if (ele != batchesForProcess[batchesForProcess.length - 1]) {
        //             batchesForProcess.push(ele);
        //         }
        //     } else {
        //         batchesForProcess.push(ele);
        //     }
        // })
        // console.log('This batchesForProcess', batchesForProcess)
        this.loaderImg = true;
        this.calculateChecksService.process(this.buildCheckCommonService.batchesForProcess, postedDate, this.defaultID, 1).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMessage = false;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);

                    this.messageText = data.btiMessage.message;
                }, 100);

                this.hasMsg = true;

                this.defaultId = '';
                this.description = '';
                this.payrollDate = '';
                this.buildtime = '';
                this.createdby = '';
                this.Gridrows = [];
                this.isUnderUpdate = false;
                this.showCreateForm = false;
                this.loaderImg = false;
                //Refresh the Grid data after adding new department
                this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.loaderImg = false;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });

    }

    varifyDeleteBatch() {

        this.showCreateForm = true;
        this.isDeleteBatch = true;
        this.isConfirmationModalOpen = true;
        window.scrollTo(0, 0);
    }

    deleteBatch() {

        this.calculateChecksService.deleteTransactionEntryBatch(this.bId).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });

            }
            this.hasMessage = true;
            this.message.type = "success";
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    //delete department by passing whole object of perticular Department
    delete() {
        var selectedTransaction = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedTransaction.push(this.selected[i].id);
        }
        this.calculateChecksService.deleteTransactionEntryByrow(selectedTransaction).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });

            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        console.log("Selected", selected);
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        this.selected.forEach(ele => {
            this.empId.push(ele.employeePrimaryId);
            // this.buildCheckCommonService.defaultIdDetailsForDistribution['empId'] = this.empId;
        })
    }

    // search department by keyword 
    updateFilter(event) {
        this.pageCalc.searchKeyword = event.target.value.toLowerCase();
        this.setPageCalChecks({ offset: 0, sortOn: this.pageCalc.sortOn, sortBy: this.pageCalc.sortBy });
        this.table.offset = 0;

        /* this.calculateChecksService.getlist(this.pageCalc, this.searchKeyword).then(pagedata => {
             this.rows = pagedata.result.records;
             this.pageCalc = pagedata.page;    
         })*/
        // this.departmentService.searchDepartmentlist(this.pageCalc, this.searchKeyword).subscribe(pagedData => {
        //     this.pageCalc = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.pgeOne.size = event.target.value;
        this.setPageCalChecks({ offset: 0, sortOn: this.pgeOne.sortOn, sortBy: this.pgeOne.sortBy });
    }

    changePageSize1(event) {
        this.pgeTwo.size = event.target.value;
        this.setPageTransList({ offset: 0, sortOn: this.pgeTwo.sortOn, sortBy: this.pgeTwo.sortBy });
    }
    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.pgeOne.sortOn == val) {
            if (this.pgeOne.sortBy == 'DESC') {
                this.pgeOne.sortBy = 'ASC';
            } else {
                this.pgeOne.sortBy = 'DESC';
            }
        }
        this.pgeOne.sortOn = val;
        this.setPageCalChecks({ offset: 0, sortOn: this.pgeOne.sortOn, sortBy: this.pgeOne.sortBy });
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
        disableUntil: { day: this.minDate.getDate() - 1, month: this.minDate.getMonth() + 1, year: this.minDate.getFullYear() }
    };

    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        //console.log('this.frmStartDate', event);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
        //console.log('this.frmEndDate', this.frmEndDate, event);
        //console.log(this.endDate);
        //console.log(this.frmStartDate);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: ' Invalid End Date' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
        //this.frmEndDate = event.jsdate;
        //this.frmEndDate = event.jsdate;
        //console.log('>>>>>>>>>>', f.controls.['frmEndDate']);
        //console.log('<<<<<<<<<<<<', f.controls['frmStartDate'].value);
        //if(f.controls['frmEndDate'].value.formatted < f.controls['frmStartDate'].value.formatted){
        //this.error={isError:true,errorMessage:' Invalid End Date'};
        // }
    }

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onColumnChange(sortOn) {
        console.log("Column ID", sortOn);
        this.pgeTwo.sortOn = this.sortColumnNameArray[sortOn];
        this.buildCheckCommonService.batchesForProcess = [];
        this.calculateChecksService.getDefaultID(this.defaultID, true, this.pgeTwo).then(data => {
            this.Gridrows = data.result.records;
            this.pgeTwo.size = data.result.pageSize;
            this.pgeTwo.totalElements = data.result.totalCount;
            this.pgeTwo.pageNumber = data.result.pageNumber;
            if (this.Gridrows.length > 0) {
                this.processedChecked = false;
                for (let k = 0; k < this.Gridrows.length; k++) {
                    // this.count = k + 1;
                    if (this.Gridrows[k].status == 'fail') {
                        this.processedChecked = true;
                    }
                }
            }

            if (this.Gridrows.length > 0) {
                this.empId = [];
                for (let i = 0; i < this.Gridrows.length; i++) {
                    this.empId.push(this.Gridrows[i].employeePrimaryId)
                }
                this.buildCheckCommonService.batchesForProcess = this.empId;
                this.calculateChecksService.getUserDetailsByUserId(data.result.records["0"].userId).then(data => {
                    if (data.result != undefined) {
                        this.createdby = data.result.primaryFullName;
                        this.buildCheckCommonService.defaultIdDetailsForDistribution = {
                            id: this.defaultID,
                            payrollDate: this.payrollDate,
                            buildtime: this.buildtime,
                            defaultId: this.defaultId,
                            createdby: this.createdby,
                            description: this.description,
                            postDate: this.modelStartDate,
                            empId: []
                        }
                    }
                })
            }


        })
    }

    // Serial Number DisplayFunction
    serialNoDisplayFunction(pgeTwo) {
        let tempLen: number = 0;
        if (pgeTwo.totalElements > pgeTwo.size) {
            tempLen = pgeTwo.size;
        } else {
            tempLen = pgeTwo.totalElements;
        }
        let serialNo: number = 0;
        this.serialNoDisplay = [];
        if (pgeTwo.pageNumber === 0) {
            while (tempLen > 0) {
                serialNo += 1; 
                this.serialNoDisplay.push(serialNo);
                tempLen--;
            }
        } else {
            serialNo = 1 + pgeTwo.pageNumber*pgeTwo.size;
            tempLen = pgeTwo.totalElements - pgeTwo.pageNumber*pgeTwo.size
            while (tempLen > 0) {
                this.serialNoDisplay.push(serialNo);
                serialNo += 1;
                tempLen--;
            }
        } 
        return this.serialNoDisplay

    }

    generateCSVFile(){
        this.calculateChecksService.generateCSVFile(this.defaultID, this.modelStartDate);
    }

    generateAndEmailPayslip(){

        // alert('this.defaultID: ' + this.defaultID);
        // alert('this.defaultId: ' + this.defaultId);
        this.showloader = true;
        window.scrollTo(0,0);
        this.calculateChecksService.generateAndEmailPayslip(this.defaultID).then(data => {
            if (data.code == 201) {
                this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.showloader = false;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
            }else {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.showloader = false;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });
    }

    generateExcelReportForPayroll(){
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.calculateChecksService.generateCSVFileAli(this.defaultID,currentLanguage).subscribe(data => {
            this.downloadFile(data,'Payroll Report')
        });
    }

    downloadFile(data: any,FileName:string){
        
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        
        var link=document.createElement('a');
        link.href=window.URL.createObjectURL(data);
        link.download=FileName +'_'+ mm + '-' + dd + '-' + yyyy + ".xlsx";
        link.click();
      }

}