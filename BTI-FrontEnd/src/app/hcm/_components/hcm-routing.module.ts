import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HCMSidebarComponent } from "./hcm-sidebar.component";
import { DepartmentComponent } from './department/department.component';
import { DivisionComponent } from './division/division.component';
import { PositionClassComponent } from './position-class/position-class.component';
import { PositionSetupComponent } from './position-setup/position-setup.component';
import { PositionPlanSetupComponent } from './position-plan-setup/position-plan-setup.component';

import { LocationComponent } from './location/location.component';
import { SupervisorComponent } from './supervisor/supervisor.component';
import { PositionTrainingComponent } from './position-training/position-training.component';
import { PositionAttachmentComponent } from './position-attachment/position-attachment.component';
import { PositionPayCodeComponent } from './position-pay-code/position-pay-code.component';
import { PositionBudgetComponent } from './position-budget/position-budget.component';
import { AttendanceSetupComponent } from './attendance-setup/attendance-setup.component';
import { AttendanceSetupOptionsComponent } from './attendance-setup-options/attendance-setup-options.component';
import { DeductionCodeSetupComponent } from './deduction-code-setup/deduction-code-setup.component';
import { AccrualSetupComponent } from './accrual-setup/accrual-setup.component';
import { HealthCoverageTypeSetupComponent } from './health-coverage-type-setup/health-coverage-type-setup.component';
import { SkillsSetupComponent } from './skills-setup/skills-setup.component';
import { BenefitCodeSetupComponent } from './benefit-code-setup/benefit-code-setup.component';
import { HealthInsuranceSetupComponent } from './health-insurance-setup/health-insurance-setup.component';
import { AccrualMainCodeSetupComponent } from './accrual-setup-main/accrual-setup-main.component';
import { RetirementPlanSetupComponent } from './retirement-plan-setup/retirement-plan-setup.component';
import { TimeCodeComponent } from './time-code/time-code.component';
import { ExitInterviewComponent } from './exit-interview/exit-interview.component';
import { RequisitionsSetupComponent } from './requisitions-setup/requisitions-setup.component';

import { SkillSetSetupComponent } from './skill-set-setup/skill-set-setup.component';
import { AccrualScheduleSetupComponent } from './accrual-schedule-setup/accrual-schedule-setup.component';
import { SalaryMatrixSetupComponent } from './salary-matrix-setup/salary-matrix-setup.component';
import { MiscellaneousBenefitsComponent } from './miscellaneous-benefits/miscellaneous-benefits.component';
import { TerminationSetupComponent } from './termination-setup/termination-setup.component';
import { LifeInsuranceSetupComponent } from './life-insurance-setup/life-insurance-setup.component';
import { HealthInsSetupComponent } from './health-ins-setup/health-ins-setup.component';
import { ShiftCodeSetupComponent } from './shift-code-setup/shift-code-setup.component';
import { PredefineChecklistComponent } from './predefine-checklist/predefine-checklist.component';
import { PaycodeSetupComponent } from './paycode-setup/paycode-setup.component';
import { BenefitPreferencesComponent } from './benefit-preferences/benefit-preferences.component';
import { TrainingCourseComponent } from './training-course/training-course.component';
import { TrainingBatchSignupComponent } from './training-batch-signup/training-batch-signup.component';
import { OrientationSetupComponent } from './orientation-setup/orientation-setup.component';
import { InterviewTypeSetupComponent } from './interview-type-setup/interview-type-setup.component';
import { PayScheduleSetupComponent } from './pay-schedule-setup/pay-schedule-setup.component';
import { ClassSkillComponent } from './class-skill/class-skill.component';
import { ClassEnrollmentComponent } from './class-enrollment/class-enrollment.component';
import { MainHeaderComponent } from '../../_sharedcomponent/main-header.component';

import { EmployeeMasterComponent } from './employee-master/employee-master.component';
import { EmployeeDeductionMaintenanceComponent } from './employee-deduction-maintenance/employee-deduction-maintenance.component';
import { EmployeeBenefitMaintenanceComponent } from './employee-benefit-maintenance/employee-benefit-maintenance.component';
import { HealthInsuranceEnrollmentComponent } from './health-insurance-enrollment/health-insurance-enrollment.component';
import { MiscellaneousBenefitsEnrollmentComponent } from './miscellaneous-benefits-enrollment/miscellaneous-benefits-enrollment.component';
import { EmployeePostDatedPayrateComponent } from './employee-post-dated-payrate/employee-post-dated-payrate.component';
import { EmployeeQuickAssignmentComponent } from './employee-quick-assignment/employee-quick-assignment.component';
import { EmployeeNationalityComponent } from './employee-nationality/employee-nationality.component';
import { EmployeeEducationComponent } from './employee-education/employee-education.component';
import { EmployeeSkillsComponent } from './employee-skills/employee-skills.component';
import { EmployeePayCodeMaintenanceComponent } from './employee-pay-code-maintenance/employee-pay-code-maintenance.component';
import { EmployeeDependentsComponent } from './employee-dependents/employee-dependents.component';
import { EmployeeDirectDepositComponent } from './employee-direct-deposit/employee-direct-deposit.component';
import { ReportComponent } from './reports/reports.component';
import { EmployeeContactsComponent } from './employee-contacts/employee-contacts.component';
import { EmployeeAddressMasterComponent } from './employee-address-master/employee-address-master.component';
import { EmployeePositionHistoryComponent } from './employee-position-history/employee-position-history.component';
import { BatchesComponent } from './batches/batches.component';
import { TransactionEntryComponent } from './transaction-entry/transaction-entry.component';
import { BuildChecksComponent } from './build-checks/build-checks.component';
import { BuildPayrollCheckComponentPayCode } from './build-payroll-check-paycode/build-payroll-check-paycode.component';
import { BuildPayrollCheckDeductionsComponent } from './build-payroll-check-deductions/build-payroll-check-deductions.component';
import { BuildPayrollCheckBenefitsComponent } from './build-payroll-check-benefits/build-payroll-check-benefits.component';
import { BuildPayrollCheckBatchesComponent } from './build-payroll-check-batches/build-payroll-check-batches.component';
import { ActiveEmployeePostDatedPayrateComponent } from './active-employee-post-dated-payrate/active-employee-post-dated-payrate.component';
import { CalculateChecksComponent } from './calculate-checks/calculate-checks.component';
import { BuildCheckReportComponent } from './buildcheck-report/buildcheck-report.component';
import { BuildPayrollCheckDefaultComponent } from './build-payroll-check-default-setup/build-payroll-check-default-setup.component';
import { BankStatementReportComponent } from './bank-statement-report/bank-statement-report.component';
import { PayrollStatementReportComponent } from './payroll-statement-report/payroll-statement-report.component';
import { PayslipReportComponent } from './payslip-report/payslip-report.component';
import { PayrollCodeModifierComponent } from './payroll-code-modifier/payroll-code-modifier.component';
import { PayrollYearEndClosingComponent } from './payroll-year-end-closing/payroll-year-end-closing.component';
import { ReconcileEmployeeInfoComponent } from './reconcile-employee-info/reconcile-employee-info.component';
import { EmployeeMassUpdateComponent } from './employee-mass-update/employee-mass-update.component';
import { MiscellaneousSetupComponent } from './miscellaneous-setup/miscellaneous-setup.component';
import { MiscellaneousSetupValuesComponent } from './miscellaneous-setup-values/miscellaneous-setup-values.component';
import { ProjectSetupComponent } from './project-setup/project-setup.component';
// import { AccountStructureComponent } from './account-structure/account-structure.component';
// import { AccountStructureSegmentComponent } from './account-structure-segment/account-structure-segment.component';
import { LoanAndAdvanceComponent } from './loan-and-advance/loan-and-advance.component';
import { LoanPackageSetupComponent } from './loan-package-setup/loan-package-setup.component';
import { LoanESBSetupComponent } from './loan-esbsetup/loan-esbsetup.component';

const hcmRoutes: Routes = [
    {
        path: '',
        component: HCMSidebarComponent,
        children: [
            { path: '', redirectTo: 'division', pathMatch: 'full' },
            { path: 'department', component: DepartmentComponent },
            { path: 'division', component: DivisionComponent },
            { path: 'positionClass', component: PositionClassComponent },
            { path: 'positionSetup', component: PositionSetupComponent },
            { path: 'positionPlanSetup/:positionId/:id', component: PositionPlanSetupComponent },
            { path: 'positionTraining/:positionId/:positionDescription/:positionArbicDescription/:id', component: PositionTrainingComponent },
            { path: 'positionAttachment', component: PositionAttachmentComponent },
            { path: 'positionBudget', component: PositionBudgetComponent },
            { path: 'location', component: LocationComponent },
            { path: 'supervisor', component: SupervisorComponent },
            { path: 'positionPlanPayCode', component: PositionPayCodeComponent },
            { path: 'attendanceSetup', component: AttendanceSetupComponent },
            { path: 'attendanceSetupOptions', component: AttendanceSetupOptionsComponent },
            { path: 'deductionCodeSetup', component: DeductionCodeSetupComponent },
            { path: 'accrualSetupMain', component: AccrualMainCodeSetupComponent },
            { path: 'accrualSetupList', component: AccrualSetupComponent },
            { path: 'accrualScheduleSetup', component: AccrualScheduleSetupComponent },
            { path: 'healthCoverageTypeSetup', component: HealthCoverageTypeSetupComponent },
            { path: 'skillsSetup', component: SkillsSetupComponent },
            { path: 'skillSetSetup', component: SkillSetSetupComponent },
            { path: 'benefitCodeSetup', component: BenefitCodeSetupComponent },
            { path: 'healthInsuranceSetup', component: HealthInsuranceSetupComponent },
            { path: 'salaryMatrixSetup', component: SalaryMatrixSetupComponent },
            { path: 'healthInsuranceSetup', component: HealthInsuranceSetupComponent },
            { path: 'timeCode', component: TimeCodeComponent },
            { path: 'miscellaneousBenefits', component: MiscellaneousBenefitsComponent },
            { path: 'terminationSetup', component: TerminationSetupComponent },
            { path: 'lifeInsuranceSetup', component: LifeInsuranceSetupComponent },
            { path: 'retirementPlanSetup', component: RetirementPlanSetupComponent },
            { path: 'healthInsSetup', component: HealthInsSetupComponent },
            { path: 'shiftCodeSetup', component: ShiftCodeSetupComponent },
            { path: 'predefinechecklist', component: PredefineChecklistComponent },
            { path: 'payCodeSetup', component: PaycodeSetupComponent },
            { path: 'exitInterview', component: ExitInterviewComponent },
            { path: 'requisitionsSetup', component: RequisitionsSetupComponent },
            { path: 'benefitPreferences', component: BenefitPreferencesComponent },
            { path: 'trainingCourse', component: TrainingCourseComponent },
            { path: 'trainingBatchSignup', component: TrainingBatchSignupComponent },
            { path: 'orientationSetup', component: OrientationSetupComponent },
            { path: 'interviewTypeSetup', component: InterviewTypeSetupComponent },
            { path: 'payScheduleSetup', component: PayScheduleSetupComponent },
            { path: 'class-skill', component: ClassSkillComponent },
            { path: 'clsenrollment', component: ClassEnrollmentComponent },
            { path: 'employeeMaster', component: EmployeeMasterComponent },
            { path: 'employeeDeductnMaster', component: EmployeeDeductionMaintenanceComponent },
            { path: 'employeeBenefitMaster', component: EmployeeBenefitMaintenanceComponent },
            { path: 'employeePayCodeMaster', component: EmployeePayCodeMaintenanceComponent },
            { path: 'healthInsuranceEnrollment', component: HealthInsuranceEnrollmentComponent },
            { path: 'miscellaneousBenefitEnroll', component: MiscellaneousBenefitsEnrollmentComponent },
            { path: 'employeePostDatedPayRate', component: EmployeePostDatedPayrateComponent },
            { path: 'employeeQuickAssignment', component: EmployeeQuickAssignmentComponent },
            { path: 'employeeNationality', component: EmployeeNationalityComponent },
            { path: 'employeeEducation', component: EmployeeEducationComponent },
            { path: 'employeeDependents', component: EmployeeDependentsComponent },
            { path: 'employeeEducation', component: EmployeeEducationComponent },
            { path: 'employeeSkills', component: EmployeeSkillsComponent },
            { path: 'employeeDirectDeposit', component: EmployeeDirectDepositComponent },
            { path: 'employeeContacts', component: EmployeeContactsComponent },
            { path: 'report', component: ReportComponent },
            { path: 'employeeAddressMaster', component: EmployeeAddressMasterComponent },
            { path: 'employeePositionHistory', component: EmployeePositionHistoryComponent },
            { path: 'batches', component: BatchesComponent },
            // { path: 'accountStatementReport', component: AccountStatementReportComponent },
            { path: 'transactionEntry', component: TransactionEntryComponent },
            { path: 'buildChecks', component: BuildChecksComponent },
            { path: 'buildPayrollCheckPayCodes', component: BuildPayrollCheckComponentPayCode },
            { path: 'buildPayrollCheckDeductions', component: BuildPayrollCheckDeductionsComponent },
            { path: 'buildPayrollCheckBenefits', component: BuildPayrollCheckBenefitsComponent },
            { path: 'buildPayrollCheckBatches', component: BuildPayrollCheckBatchesComponent },
            // { path: 'accountReport', component: AccountReportComponent },
            // { path: 'accountReport', component: AccountReportComponent },
            { path: 'activateEmployeePostDatePayRate', component: ActiveEmployeePostDatedPayrateComponent },
            { path: 'calculateChecks', component: CalculateChecksComponent },
            { path: 'buildcheckReport', component: BuildCheckReportComponent },
            { path: 'buildPayrollCheckDefault', component: BuildPayrollCheckDefaultComponent },
            { path: 'bankStatementReport', component: BankStatementReportComponent },
            { path: 'payrollStatementReport', component: PayrollStatementReportComponent },
            { path: 'payslipReport', component: PayslipReportComponent },
            { path: 'payrollCodeModifier', component: PayrollCodeModifierComponent },
            { path: 'payrollYearEndClosing', component: PayrollYearEndClosingComponent },
            { path: 'reconcileEmployeeInfo', component: ReconcileEmployeeInfoComponent },
            { path: 'employeeMassUpdate', component: EmployeeMassUpdateComponent },
            { path: 'miscellaneoussetup', component: MiscellaneousSetupComponent },
            { path: 'miscellaneoussetupvalues/:id', component: MiscellaneousSetupValuesComponent },
            { path: 'projectsetup', component: ProjectSetupComponent },
            // { path: 'accountstructure', component: AccountStructureComponent },
            // { path: 'accountstructuresegment/:id', component: AccountStructureSegmentComponent },
            { path: 'loanandadvance', component: LoanAndAdvanceComponent },
            { path: 'loanpackagesetup', component: LoanPackageSetupComponent },
            { path: 'loanesbsetup', component: LoanESBSetupComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(hcmRoutes)],
    exports: [RouterModule],
})
export class HcmRoutingModule {

}
