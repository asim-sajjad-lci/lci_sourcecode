import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { ClassSkill } from '../../_models/class-skill/class-skill';
import { ClassSkillService } from '../../_services/class-skill/class-skill.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { TrainingBatchSignupService } from '../../_services/training-batch-signup/training-batch-signup.service';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'class-skill',
    templateUrl: './class-skill.component.html',
    styleUrls: ['./class-skill.component.css'],
    providers: [ClassSkillService, TrainingBatchSignupService, CommonService]
})

// export to make it available for other classes
export class ClassSkillComponent {
    page = new Page();
    rows = new Array<ClassSkill>();
    temp = new Array<ClassSkill>();
    selected = [];
    selectedAllSkillSet = [];
    selSkillSets = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1441';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    trainingBatchSignupId = {};
    searchKeyword = '';
    ddPageSize = 5;
    model: ClassSkill;
    cityOptions = [];
    showCreateForm: boolean = false;
    isSuccessMsg;
    isfailureMsg;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText;
    emailPattern = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}';
    // emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
    currentLanguage: any;
    isConfirmationModalOpen: boolean = false;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    AllTraningCourseId = [];
    AllTrainingClass = [];
    AllSkillSetSetup = [];
    AllSkillSet = [];
    dataSource: Observable<any>;
    trainingBatchSignupIdSearch = [];
    typeaheadLoading: boolean;
    SelectedSkillSets = [];

    CLASS_DESCRIPTION: any;
    CLASS_ID: any;
    TRAINING_ID: any;
    TRAINING_DESCRIPTION: any;
    SKILL_DESCRIPTION: any;
    SKILL_SET_ID: any;
    SKILLS_IN_CLASS: any;
    DELETE: any;
    SAVE: any;
    CLEAR: any;
    ACTION: any;
    CREATE: any;
    SEARCH: any;
    SELECT: any;
    UPDATE: any;
    CANCEL: any;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(
        private router: Router,
        private classSkillService: ClassSkillService,
        private trainingBatchSignupService: TrainingBatchSignupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for trainingBatchSignup  screen
        this.defaultFormValues = [
            { 'fieldName': 'CLASS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRAINING_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRAINING_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_SET_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILLS_IN_CLASS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SELECT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '' }
        ];

        this.CLASS_DESCRIPTION = this.defaultFormValues[0];
        this.CLASS_ID = this.defaultFormValues[1];
        this.TRAINING_ID = this.defaultFormValues[2];
        this.TRAINING_DESCRIPTION = this.defaultFormValues[3];
        this.SKILL_DESCRIPTION = this.defaultFormValues[4];
        this.SKILL_SET_ID = this.defaultFormValues[5];
        this.SKILLS_IN_CLASS = this.defaultFormValues[6];
        this.DELETE = this.defaultFormValues[7];
        this.SAVE = this.defaultFormValues[8];
        this.CLEAR = this.defaultFormValues[9];
        this.ACTION = this.defaultFormValues[10];
        this.CREATE = this.defaultFormValues[11];
        this.SEARCH = this.defaultFormValues[12];
        this.SELECT = this.defaultFormValues[13];
        this.UPDATE = this.defaultFormValues[14];
        this.CANCEL = this.defaultFormValues[15];
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.setPage({ offset: 0 });
        this.classSkillService.getAllTraningCourse(this.page, this.searchKeyword).then(data => {
            this.AllTraningCourseId = data.result.records;
        });
        this.classSkillService.getTrainingCourse(this.page, this.searchKeyword).then(data => {
            this.AllTrainingClass = data.result.records;
        });
        
        this.classSkillService.getAllSkillSetSetup(this.searchKeyword).then(data => {

            this.AllSkillSetSetup = data.result.records;
            // this.AllSkillSet = data.result.records;

        });
        // getting screen
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    }
    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'classId') {
            return row['trainingClass'] && row['trainingClass']['classId'] ? row['trainingClass']['classId'] : '';
        } else if (gridFieldName === 'className') {
            return row['trainingClass'] && row['trainingClass']['className'] ? row['trainingClass']['className'] : '';
        } else if (gridFieldName === 'traningId') {
            return row['trainingCourse'] && row['trainingCourse']['traningId'] ? row['trainingCourse']['traningId'] : '';
        } else if (gridFieldName === 'desc') {
            return row['trainingCourse'] && row['trainingCourse']['desc'] ? row['trainingCourse']['desc'] : '';
        } else if (gridFieldName === 'skillSetId') {
            return row['skillSetSetup'] && row['skillSetSetup']['skillSetId'] ? row['skillSetSetup']['skillSetId'] : '';
        } else if (gridFieldName === 'skillSetDiscription') {
            return row['skillSetSetup'] && row['skillSetSetup']['skillSetDiscription'] ? row['skillSetSetup']['skillSetDiscription'] : '';
        } else {
            return row[gridFieldName];
        }
    }
    // Exiting Id Search on every click
    getReportClassSkillAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.trainingBatchSignupIdSearch.filter((id: any) => {
                return query.test(id.trainingBatchSignupId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
    }

    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn === undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy === undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.classSkillService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 600);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            skillDesc: '',
            skillArabicDesc: '',
            skillSequnce: 0,
            trainingCourse: {
                id: 0
            },
            trainingClass: {
                id: 0
            },
            skillSetSetup: {
                id: 0
            },
            skillsSetup: [{
                id: 0
            }],
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        
        this.model = {
            id: 0,
            skillDesc: '',
            skillArabicDesc: '',
            skillSequnce: 0,
            trainingCourse: {
                id: 0
            },
            trainingClass: {
                id: 0
            },
            skillSetSetup: {
                id: 0
            },
            skillsSetup: [{
                id: 0
            }],
        };
        
        this.SelectedSkillSets = [];
    }

    // function call for creating new trainingBatchSignup
    CreateClassSkill(f: NgForm, event: Event) {
        event.preventDefault();
        var divIdx = this.model.id;

        this.model.skillsSetup = [];
        this.SelectedSkillSets.forEach(element => {
            this.model.skillsSetup.push({ id: element.id });
        });

        if (this.model.id > 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Call service api for Creating new trainingBatchSignup
            this.classSkillService.createClassSkill(this.model).then(data => {
                var datacode = data.code;
                if (datacode === 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.showCreateForm = false;
                        this.SelectedSkillSets = [];
                        // this.AllSkillSet = this.AllSkillSetSetup;
                        this.messageText = data.btiMessage.message;;
                    }, 100);

                    this.hasMsg = true;

                    f.resetForm();

                    // Refresh the Grid data after adding new trainingBatchSignup
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    }

    // edit trainingBatchSignup by row id
    edit(row: ClassSkill) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        // this.trainingBatchSignupId = row.trainingBatchSignupId;
        // this.trainingBatchSignupIdvalue = this.model.trainingBatchSignupId;

        this.SelectedSkillSets = Object.assign([], row.skillsSetup);

        setTimeout(() => {
            window.scrollTo(0, 500);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        // this.model.trainingBatchSignupId = this.trainingBatchSignupIdvalue;
        // Call service api for updating selected trainingBatchSignup
        this.classSkillService.updateClassSkill(this.model).then(data => {
            var datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    // delete one or multiple trainingBatchSignups
    delete() {
        var selectedClassSkills = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedClassSkills.push(this.selected[i].id);
        }
        this.classSkillService.deleteClassSkill(selectedClassSkills).then(data => {
            var datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of trainingBatchSignup
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search rolegroup details by group name 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        /* this.classSkillService.searchClassSkilllist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
        }); */
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn === val) {
            if (this.page.sortBy === 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    moveToSelected() {
        for (var j = 0; j < this.selectedAllSkillSet.length; j++) {
            const index = this.AllSkillSet.indexOf(this.selectedAllSkillSet[j]);
            if (index !== -1) {
                this.AllSkillSet.splice(index, 1);
                this.SelectedSkillSets.push(this.selectedAllSkillSet[j])
            }
        }
        this.selectedAllSkillSet = [];
    }

    moveToAllEmployee() {
        for (var j = 0; j < this.selSkillSets.length; j++) {
            const index = this.SelectedSkillSets.indexOf(this.selSkillSets[j]);

            if (index !== -1) {
                this.SelectedSkillSets.splice(index, 1);
                this.AllSkillSet.push(this.selSkillSets[j]);
            }
        }
        this.selSkillSets = [];
    }
    // default list on page
    onSelectAllSkillSet({ selected }) {
        this.selectedAllSkillSet.splice(0, this.selectedAllSkillSet.length);
        this.selectedAllSkillSet.push(...selected);
    }
    // default list on page
    onSelectSkillSets({ selected }) {
        this.selSkillSets.splice(0, this.selSkillSets.length);
        this.selSkillSets.push(...selected);

    }
    changeTrainingCourses(event) {
        if (this.model.trainingCourse['id']) {
            this.trainingBatchSignupService.getTrainingCourseDetailTraningId(this.model.trainingCourse['id']).subscribe(data => {
                this.AllTrainingClass = data.records;
            });
        }
    }

    changeSkillSetup(event) {
        this.AllSkillSet = [];
        // let value = '';
        // for (let i = 0; i < this.AllSkillSetSetup.length; i++) {
        //     const element = this.AllSkillSetSetup[i];
        //     if (element.id === event.target.value) {
        //         value = element.id;
        //     }
        // }

        this.classSkillService.getSkillSetSteupgetById(this.page, event.target.value).subscribe(pagedData => {
            this.AllSkillSet = pagedData.records['skillIds'];
            // this.page = pagedData.page;
            // this.rows = pagedData.data;
        });
    }
}
