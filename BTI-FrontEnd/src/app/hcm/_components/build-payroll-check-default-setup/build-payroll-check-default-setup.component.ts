import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BuildPayrollCheckDefault } from '../../_models/build-payroll-chek-default-setup/build-payroll-chek-default-setup';
import { BuildPayRollService } from '../../_services/build-payroll-check-default-setup/build-payroll-check-default-setup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { BuildCheckCommonService } from '../../../_sharedresource/_services/build-check-common.service';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
@Component({
    selector: 'build-payroll-check-default',
    templateUrl: './build-payroll-check-default-setup.component.html',
    providers: [BuildPayRollService,CommonService]
})

// export Department component to make it available for other classes
export class BuildPayrollCheckDefaultComponent {
    page = new Page();
    rows = new Array<BuildPayrollCheckDefault>();
    temp = new Array<BuildPayrollCheckDefault>();
    selected = [];
    moduleCode = "M-1011";
    screenCode = "S-1472";
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    defaultId = {};
    searchKeyword = '';
    getDefault: any[] = [];
    ddPageSize: number = 5;
    model: BuildPayrollCheckDefault;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    defaultIdvalue: string;
    defaultIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_SEARCH_LABEL: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DEFAULT_ID: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DESCRIPTION: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_ARABIC_DESCRIPTION: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_ACTION: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DELETE: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_SAVE: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CLEAR: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CREATE_LABEL: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_UPDATE: any;
    BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CANCEL: any;

    constructor(
        private router: Router,
        private buildPayRollService: BuildPayRollService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private buildCheckCommonService: BuildCheckCommonService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_SEARCH_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DEFAULT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CANCEL', 'fieldValue': '', 'helpMessage': '' }

        ];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_SEARCH_LABEL = this.defaultFormValues[0];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DEFAULT_ID = this.defaultFormValues[1];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DESCRIPTION = this.defaultFormValues[2];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_ACTION = this.defaultFormValues[4];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_DELETE = this.defaultFormValues[5];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_SAVE = this.defaultFormValues[6];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CLEAR = this.defaultFormValues[7];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CREATE_LABEL = this.defaultFormValues[8];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_UPDATE = this.defaultFormValues[9];
        this.BUILD_PAYROLL_CHECK_DEFAULT_SETUP_CANCEL = this.defaultFormValues[10];
        this.defaultIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.defaultID);
        }).mergeMap((token: string) => this.getDefaultIdAsObservable(token));
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


        //Following shifted from SetPage for better performance
        this.buildPayRollService.getDefaultIdList().then(data => {
            this.getDefault = data.result;
            //console.log("Data class options : "+this.getDepartment);
        });
    }
    getDefaultIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getDefault.filter((id: any) => {
                return query.test(id.defaultID);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        //console.log('Selected value: ', e.value);
    }
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.buildPayRollService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            defaultID: '',
            desc: '',
            arabicDesc: ''

        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }

    //function call for creating new department
    CreateDefault(f: NgForm, event: Event) {
        event.preventDefault();
        var defaultIdx = this.model.defaultID;

        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.buildPayRollService.checkDuplicateDefaultId(defaultIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new department
                    this.buildPayRollService.createDefaultId(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new department
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: BuildPayrollCheckDefault) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.defaultId = row.defaultID;
        this.defaultIdvalue = this.model.defaultID;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
        this.model.defaultID = this.defaultIdvalue;
        this.buildPayRollService.updateDefault(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedDefault = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDefault.push(this.selected[i].id);
        }
        this.buildPayRollService.deleteDefault(selectedDefault).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                this.hasMessage = true;
                if(datacode == "302"){
                    this.message.type = "error";
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                } else {
                    this.isSuccessMsg = true;
                    this.message.type = "success";
                    this.isfailureMsg = false;
                }
                //this.message.text = data.btiMessage.message;

                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);
            }
            if (datacode == 202) {
                this.hasMessage = true;
                this.message.type = "success";
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);
            }

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}