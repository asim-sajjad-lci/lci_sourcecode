import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { EmployeeQuickAssignmentService } from '../../_services/employee-quick-assignment/employee-quick-assignment.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';

import { forEach } from '@angular/router/src/utils/collection';
import * as cloneDeep from 'lodash/cloneDeep';
import { NgFor } from '@angular/common/src/directives/ng_for_of';
import { Observable } from 'rxjs/Observable';
import { TEMPLATE_DRIVEN_DIRECTIVES } from 'ng2-datepicker-bootstrap/node_modules/@angular/forms/src/directives';
import { EmployeeMaster } from '../../_models/employee-master/employee-master.module';
import { EmployeeMasterService } from '../../_services/employee-master/employee-master.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { EmployeeQuickAssignmentModule } from '../../_models/employee-quick-assignment/employee-quick-assignment.module';
import { NgControl } from 'ng2-datepicker-bootstrap/node_modules/@angular/forms/src/directives/ng_control';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { FormControl } from 'ng2-datepicker-bootstrap/node_modules/@angular/forms/src/model';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatePickerModule } from 'ng2-datepicker-bootstrap';

@Component({
    selector: 'employeeQuickAssignment',
    templateUrl: './employee-quick-assignment.component.html',
    styleUrls: ['./employee-quick-assignment.component.css'],
    providers: [EmployeeMasterService, EmployeeQuickAssignmentService]
})
export class EmployeeQuickAssignmentComponent implements OnInit {
    page = new Page();
    include = [];
    selected = [];
    model: Object;


    moduleCode = 'M-1011';
    screenCode = 'S-1451';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    Warning;
    message = { 'type': '', 'text': '' };
    searchKeyword = '';
    ddPageSize: number = 5;
    showTable: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;

    isEmpValid:boolean=false;

    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    selectedChange: boolean = false;
    employeeIdvalue: string;
    selectedRowId: number = 0;

    employeeStartDate: any[] = [];
    employeeEndDate: any[] = [];
    prevPage: number;
    pageChangeAttempt: boolean = false;
    loadingInfo: boolean = false;
    changedIndices: any[] = [];

    activeTabid;
    codeTypeId;
    isErrorModalOpen: boolean = false;

    //rowsFromDB: Array<EmployeeQuickAssignmentModule>;

    getEmployee: any[] = [];
    employeeIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder: boolean = false;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    codeSelect = [{ code: 'Employee Pay Code', value: "2" },
    { code: 'Employee Benefit', value: "3" },
    { code: 'Employee Deduction', value: "1" }
    ];

    constructor(private router: Router,
        private employeeMasterService: EmployeeMasterService,
        private getScreenDetailService: GetScreenDetailService,
        private employeeQuickAssignmentService: EmployeeQuickAssignmentService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        this.prevPage = 0;

        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_CODE_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_ALL_COMPANY_CODES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_ALL_EMPLOYEE_CODES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_INCLUDE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_CODE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_MARK_ALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_UNMARK_ALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_OK', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_QUICK_ASSIGNMENT_PRINT_LABEL', 'fieldValue': '', 'helpMessage': '' }
        ];

        this.model = {
            employeeMaster: new EmployeeMaster(0, null, null, null, null,null , null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null, null, null, null, null,null,null, null, null, null, null),
            codeType: "",
            mainCodeType: "",
            allCompanyCode: true,
            rowsFromDB: Array<EmployeeQuickAssignmentModule>()
        };
        this.model["codeType"] = "";
        this.employeeIdList = Observable.create((observer: any) => {
            observer.next(this.model["employeeMaster"].employeeId);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

    }

    getEmployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployee.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    ngOnInit() {
        this.showTable = false;

       

        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        this.employeeMasterService.getEmployee().then(data => {
            this.getEmployee = data.result;
        });

        //Get paycodes on pageload
        
        this.model['codeType']=2;
        this.activeTabid=2;
        this.codeTypeId=2;
        this.setPage({offset:0});
       
       
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        this.model["employeeMaster"].employeeFirstName = null;
        this.model["employeeMaster"].employeeMiddleName = null;
        this.model["employeeMaster"].employeeLastName = null;

      
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        //console.log('Selected value: ', e.item);
        this.model["employeeMaster"] = cloneDeep(e.item);
       
        if (this.model["codeType"] != ""){
            this.setPage({ offset: 0 }); 
          
        }
           
        if (this.model["employeeMaster"].employeeIndexId!=0 || this.model["employeeMaster"].employeeIndexId!=null)
         this.isEmpValid=true;

            
    }

    checkEmployeeSelect(allCompanyCode: FormControl) {
        if (this.model["employeeMaster"].employeeFirstName == null) {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.model["allCompanyCode"] = true;
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = "Please select an employee";
            }, 100);
        }
        else if (this.model["codeType"] == "") {
            this.Warning = true;
            this.message.type = 'success';

            window.setTimeout(() => {
                this.model["allCompanyCode"] = true;
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.Warning = false;
                }, 4000);
                this.message.text = "Please select a code type";
            }, 100);
        }
        else
            this.setPage({ offset: 0 });
    }

    loadPage() {
        if (this.prevPage != this.page.pageNumber) {
            this.prevPage = this.page.pageNumber;
            this.setPage({ offset: this.page.pageNumber });
        }
    }

    changePage(pageInfo) {
        this.pageChangeAttempt = true;
        if (!this.formValidity())
            return;
        else {
            this.employeeStartDate = [];
            this.employeeEndDate = [];
            //this.showTable = false;
            this.model['rowsFromDB'].forEach(row => {
                let temp = this.selected.filter(item => item["codeId"] == row["codeId"]);
                if (temp.length > 0) {
                    this.selected = this.selected.filter(item => item["codeId"] != row["codeId"]);
                }

                if (row.include) {
                    this.selected.push(row);
                }
            });

            if (this.model["codeType"] == "" || this.model["codeType"] == null) {
                this.showTable = false;
                return;
            }
            this.loadingInfo = true;
            this.page.pageNumber = pageInfo.offset;
            if (pageInfo.sortOn == undefined) {
                this.page.sortOn = this.page.sortOn;
            } else {
                this.page.sortOn = pageInfo.sortOn;
            }
            if (pageInfo.sortBy == undefined) {
                this.page.sortBy = this.page.sortBy;
            } else {
                this.page.sortBy = pageInfo.sortBy;
            }

            let param = {
                "employeeId": this.model["employeeMaster"].employeeIndexId,
                "codeType": this.model["codeType"],
                "allCompanyCode": this.model["allCompanyCode"]
            };

            this.employeeQuickAssignmentService.getlist(this.page, this.page.pageNumber * this.page.size, (this.page.pageNumber + 1) * this.page.size, param).subscribe(pagedData => {
                this.page = pagedData.page;
                this.model["rowsFromDB"] = cloneDeep(pagedData.data);



                for (var i = 0; i < this.model["rowsFromDB"].length; i++) {

                    console.log("aldkfjaldfjl;ajfdalfjd")
                    console.log(this.model["rowsFromDB"][i])

                    if (this.changedIndices.filter(indx => indx == this.model["rowsFromDB"][i].index).length) {
                        let temp = this.selected.filter(row => row.codeId == this.model["rowsFromDB"][i].codeId);
                        if (temp.length > 0) {

                            this.model["rowsFromDB"][i].include = temp[0].include;
                            this.model["rowsFromDB"][i].method = temp[0].method;
                            this.model["rowsFromDB"][i].percent = temp[0].percent;
                            this.model["rowsFromDB"][i].amount = temp[0].amount;
                            this.model["rowsFromDB"][i].payRate = temp[0].payRate;
                            // this.model["rowsFromDB"][i].payFactory = temp[0].payFactory;
                            // this.model["rowsFromDB"][i].baseOnPayCode = temp[0].baseOnPayCode;
                            this.model["rowsFromDB"][i].startDate = temp[0].startDate;
                            this.model["rowsFromDB"][i].endDate = temp[0].endDate;
                            this.model["rowsFromDB"][i].inActive = temp[0].inActive;
                            this.model["rowsFromDB"][i].isAmtType = temp[0].isAmtType;
                            this.model["rowsFromDB"][i].amtInvalid = temp[0].amtInvalid;
                            this.model["rowsFromDB"][i].endDateInvalid = temp[0].endDateInvalid;
                            this.model["rowsFromDB"][i].startDateInvalid = temp[0].startDateInvalid;
                        }
                        else {
                            this.model["rowsFromDB"][i].include = false;
                        }
                    }


                    if (this.model["rowsFromDB"][i].startDate == null)
                        this.employeeStartDate.push("");
                    else
                        this.employeeStartDate.push(this.formatDateFordatePicker(new Date(this.model["rowsFromDB"][i].startDate)));

                    if (this.model["rowsFromDB"][i].endDate == null)
                        this.employeeEndDate.push("");
                    else
                        this.employeeEndDate.push(this.formatDateFordatePicker(new Date(this.model["rowsFromDB"][i].endDate)));
                }
                //console.log('Data Retrieved ' + this.model["rowsFromDB"]);
                this.showTable = true;
                this.pageChangeAttempt = false;
                this.loadingInfo = false;
            });

        }

    }

    setPage(pageInfo) {
        debugger;
     // alert(this.model["codeType"])

        this.employeeStartDate = [];
        this.employeeEndDate = [];
        this.selected = []; // remove any selected checkbox on paging
        //this.showTable = false;

     

        if (this.model["codeType"] == "" || this.model["codeType"] == null) {
            this.showTable = false;
            return;
        }

        this.loadingInfo = true;
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        let param = {
            "employeeId": this.model["employeeMaster"].employeeIndexId,
            "codeType": this.model["codeType"],
            "allCompanyCode": this.model["allCompanyCode"]
        };

        let param0 = {
            "employeeId": this.model["employeeMaster"].employeeIndexId,
            "codeType": this.model["codeType"],
            "allCompanyCode": false
        };

        this.employeeQuickAssignmentService.getlist(this.page, null, null, param0).subscribe(pagedData2 => {
            this.selected = cloneDeep(pagedData2.data);

            console.log("selected data");
            console.log(pagedData2.data);
            //console.log('SELECTED Data Retrieved ', this.selected);
        });

        this.employeeQuickAssignmentService.getlist(this.page, this.page.pageNumber * this.page.size, (this.page.pageNumber + 1) * this.page.size, param).subscribe(pagedData => {
            this.page = cloneDeep(pagedData.page);
            this.model["rowsFromDB"] = cloneDeep(pagedData.data);

            console.log("all data")
            console.log(this.model["rowsFromDB"]);

            for (var i = 0; i < this.model["rowsFromDB"].length; i++) {
                this.employeeStartDate.push(this.formatDateFordatePicker(new Date(this.model["rowsFromDB"][i].startDate)));
                this.employeeEndDate.push(this.formatDateFordatePicker(new Date(this.model["rowsFromDB"][i].endDate)));
            }
            //console.log('Data Retrieved ' + this.model["rowsFromDB"]);
            this.showTable = true;
            this.loadingInfo = false;
        });

    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onEmployeeStartDateChanged(event: IMyDateModel, Indx: number): void {

        this.employeeStartDate[Indx] = event.jsdate;
        this.model["rowsFromDB"][Indx].startDate = new Date(0);
        this.model["rowsFromDB"][Indx].startDate.setUTCSeconds(event.epoc);
        if (event.jsdate == null)
            this.model["rowsFromDB"][Indx].startDateInvalid = true;
        else
            this.model["rowsFromDB"][Indx].startDateInvalid = false;

        if ((new Date(this.model["rowsFromDB"][Indx].endDate)).valueOf() < event.jsdate.valueOf())
            this.model["rowsFromDB"][Indx].endDateInvalid = true;
        else
            this.model["rowsFromDB"][Indx].endDateInvalid = false;
        this.selectedChange = true;
    }

    onEmployeeEndDateChanged(event: IMyDateModel, Indx: number): void {
        this.employeeEndDate[Indx] = event.jsdate;
        this.model["rowsFromDB"][Indx].endDate = new Date(0);
        this.model["rowsFromDB"][Indx].endDate.setUTCSeconds(event.epoc);
        if (event.jsdate == null)
            this.model["rowsFromDB"][Indx].endDateInvalid = true;
        else if ((new Date(this.model["rowsFromDB"][Indx].startDate)).valueOf() > event.jsdate.valueOf())
            this.model["rowsFromDB"][Indx].endDateInvalid = true;
        else
            this.model["rowsFromDB"][Indx].endDateInvalid = false;
        this.selectedChange = true;
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({
            employeeMaster: new EmployeeMaster(0, null, null, null, null, null,null ,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null, null,null,null, null, null, null, null),
            codeType: "",
            mainCodeType: "",
            allCompanyCode: true,
            rowsFromDB: Array<EmployeeQuickAssignmentModule>()
        });
        //this.rowsFromDB = [];
        this.model["employeeMaster"].employeeFirstName = null;
        this.model["employeeMaster"].employeeMiddleName = null;
        this.model["employeeMaster"].employeeLastName = null;
        this.model['codeType'] = "";
        this.selectedChange = false;
        this.pageChangeAttempt = false;
        this.showTable = false;
        this.loadingInfo = false;
        this.doNotPrintHearder = false;
        this.changedIndices = [];
    }

    //function call for creating new Accrual
    CreateEmployeeQuickAss(event: Event) {
        debugger;
        event.preventDefault();
        this.doNotPrintHearder = false;
        this.employeeIdvalue = this.model["employeeMaster"].employeeId;

        if (this.formValidity()) {

            Observable.forkJoin([this.replaceVals()]).subscribe(t => {
                this.isConfirmationModalOpen = true;
                this.isDeleteAction = false;
            });

        }

    }

    replaceVals() {
        this.model['rowsFromDB'].forEach(row => {
            let temp = this.selected.filter(item => item["codeId"] == row["codeId"]);
            if (temp.length > 0) {
                this.selected = this.selected.filter(item => item["codeId"] != row["codeId"]);
            }

            if (row.include) {
                //console.log("IN INCC");
                this.selected.push(row);
            }
        });
        return Observable.of(true);
    }

    formValidity() {
        let temp = this.model['rowsFromDB'].filter(item => (item.amtInvalid || item.endDateInvalid || item.startDateInvalid) && item.include);

        
        if (temp.length > 0)
            return false;
        else
            return true;
    }

    //edit department by row
    edit(row) {
        //WHOLE CHANGES
        this.selectedRowId = row.id;
        this.model = cloneDeep(row);
        this.isUnderUpdate = true;
        this.doNotPrintHearder = false;
        this.employeeIdvalue = this.model["employeeMaster"].employeeId;
        this.setPage({ offset: 0 });
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    // changeSelection(row: Object, Indx: number) {
    //     this.model['rowsFromDB'][Indx].include = !this.model['rowsFromDB'][Indx].include;
    //     this.selectedChange = true;
    //     this.changedIndices.push(this.model['rowsFromDB'][Indx].index);
    //     if (this.model['rowsFromDB'][Indx].include) {
    //         debugger;
    //         console.log("selected row")
    //         console.log(row["empolyeeMaintananceId"]);
    //         // row["empolyeeMaintananceId"]=18;
    //         this.selected.push(row);
          
    //     }
    //     else {
    //         this.selected  = this.selected.filter(item => item["index"] != row["index"]);
    //         this.selected.splice(this.selected.length, 1);
    //     }
    //     //console.log(this.selected);
    // }

    changeSelection(row: Object, Indx: number, e) {
        debugger;
        let canDeleted;

        if (row["parentPaycode"] == "" || row["parentPaycode"] == null) {
            this.model['rowsFromDB'][Indx].include = !this.model['rowsFromDB'][Indx].include;
           // this.selectedChange = true;
            this.changedIndices.push(this.model['rowsFromDB'][Indx].index);
            if (this.model['rowsFromDB'][Indx].include) {
                debugger;
                console.log("selected row")
                console.log(row["empolyeeMaintananceId"]);
                // row["empolyeeMaintananceId"]=18;
                this.selected.push(row);

            }
            else {
                debugger
                let isCodeUsed;
                console.log(row);
                if(row["masterId"] != null)
                {
                    this.employeeQuickAssignmentService.checkCodeUsage(row["employeeId"],row["codeId"],this.codeTypeId).then(data => {
                        debugger;
                       canDeleted=data.result;
                       if(canDeleted)
                       {
                      //       this.selected = this.selected.filter(item => item["index"] != row["index"]);
                      // this.selected.splice(this.selected.length, 1);
                      for(let i=0 ;i<this.selected.length;i++)
                      {
                          if(this.selected[i].masterId == row["masterId"])
                          {
                              this.selected[i]["isDeleted"] = true;
                          }
                      }
                       }else{
                         
                          // e.preventDefault();
                          this.model['rowsFromDB'][Indx].include = !this.model['rowsFromDB'][Indx].include;
                          this.hasMsg = true;
                   //Scroll to top after editing department
                   window.scrollTo(0, 0);
                   window.setTimeout(() => {
                       this.isSuccessMsg = false;
                       this.isfailureMsg = true;
                       this.showMsg = true;
                       window.setTimeout(() => {
                           this.showMsg = false;
                           this.hasMsg = false;
                           this.setPage({ offset: 0 });
                       }, 4000);
                       this.messageText = "Code is in use";
                       
                   }, 100);
      
                  //  this.hasMsg = true;
                  //  window.setTimeout(() => {
                  //      this.showMsg = false;
                  //      this.hasMsg = false;
                  //      this.setPage({ offset: 0 });
                  //  }, 4000);
                       }
                      
                      });
                }
                else
                {
                           this.selected = this.selected.filter(item => item["masterId"] != row["masterId"]);
                       this.selected.splice(this.selected.length, 1);
                }
              
               
              
                   
                // this.selected = this.selected.filter(item => item["index"] != row["index"]);
                // this.selected.splice(this.selected.length, 1);
            }
            console.log("Selected Item",this.selected);
        }
        else {
          let val = row["parentPaycode"];
          let isValid = false;
          for(let i=0;i< this.selected.length;i++)
          {
              if(this.selected[i].code == val)
              {
               isValid = true;
              }
          }

          if(!isValid)
          {
            e.preventDefault();
            this.isErrorModalOpen = true;
            setTimeout(() => {
              this.isErrorModalOpen = false;
          }, 2000);
          }
          else
          {
            this.model['rowsFromDB'][Indx].include = !this.model['rowsFromDB'][Indx].include;
         //   this.selectedChange = true;
            this.changedIndices.push(this.model['rowsFromDB'][Indx].index);
            if (this.model['rowsFromDB'][Indx].include) {
                debugger;
                console.log("selected row")
                console.log(row["empolyeeMaintananceId"]);
                // row["empolyeeMaintananceId"]=18;
                this.selected.push(row);

            }
            else {
                this.selected = this.selected.filter(item => item["index"] != row["index"]);
                this.selected.splice(this.selected.length, 1);
            }
          }
        }
       

    }
    changeInactive(Indx: number) {
        this.model['rowsFromDB'][Indx].inActive = !this.model['rowsFromDB'][Indx].inActive;
        this.selectedChange = true;
    }

    changeAmt(event, Indx: number) {
        event.preventDefault();
        this.model['rowsFromDB'][Indx].payRate = event.target.valueAsNumber;
        //console.log("Amt: ", this.model['rowsFromDB'][Indx].amount);
        this.selectedChange = true;
    }

    changePercent(event, Indx: number) {
        event.preventDefault();
        this.model['rowsFromDB'][Indx].percent = event.target.valueAsNumber;
        //console.log(this.model['rowsFromDB'][Indx].percent);
        this.selectedChange = true;
    }

    markAll() {
        this.loadingInfo = true;
        Observable.forkJoin([Observable.of(this.getAll())]).subscribe(t => {
            this.changePage({ offset: this.page.pageNumber });
        });

        /* 
        this.selected = this.model["rowsFromDB"]; */
        /* this.model["rowsFromDB"].forEach(row => {
            this.selected=this.selected.filter(item => item != row);
            this.changedIndices.push(row.index);
            row.include = true;
            this.selected.push(row);
        }); */
    }

    getAll() {
        this.selected = [];
        let param0 = {
            "employeeId": this.model["employeeMaster"].employeeIndexId,
            "codeType": this.model["codeType"],
            "allCompanyCode": true
        };
        this.employeeQuickAssignmentService.getlist(this.page, null, null, param0).subscribe(pagedData2 => {
            this.selected = cloneDeep(pagedData2.data);
            this.selected.forEach(row => {
                row.include = true;
                this.changedIndices.push(row.index);
            });

            this.model["rowsFromDB"].forEach(row => {
                row.include = true;
            });
            //console.log('SELECTED Data Retrieved ', this.selected);
        });
        return true;
    }

    unmarkAll() {
        //this.selected = [];

        this.loadingInfo = true;
        Observable.forkJoin([Observable.of(this.getNone())]).subscribe(t => {
            this.changePage({ offset: this.page.pageNumber });
        });
    }

    getNone() {
        this.model["rowsFromDB"].forEach(row => {
            row.include = false;
        });
        this.selected.forEach(row => {
            this.changedIndices.push(row.index);
            row.include = false;
        });
        this.selected = [];
        return true;
    }

    updateStatus(f: NgForm) {

        debugger;
        // this.model["rowsFromDB"].payFactory = 0;
        // this.model["rowsFromDB"].baseOnPayCode = '';
        this.closeModal();
        this.doNotPrintHearder = false;
        //this.model["employeeMaster"].employeeId = this.employeeIdvalue;

        this.loadingInfo = true;
        this.selected.forEach(item => {
            item.masterId = null;
            item.employeeId = null;
            //alert(item.empolyeeMaintananceId)
        });
        
        // this.selected.forEach(item=>{
        //     item.payFactory = 0;
        //     item.baseOnPayCode = '';
        // })
        //Call service api for Creating new Accrual
console.log("selected",this.selected)

        this.employeeQuickAssignmentService.createQuickAssignment(this.model["employeeMaster"].employeeIndexId, parseInt(this.model["codeType"]), !this.model["allCompanyCode"], this.selected).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                //this.setPage();
               // this.Clear(f);

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                    this.setPage({ offset: 0 });
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            window.scrollTo(0, 0);
        }
    }

    //delete department by passing whole object of perticular Department
    delete() {
        /* var selectedAccruals = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedAccruals.push(this.selected[i].id);
        } */

        // SWITCH HERE
    }

    // default list on page
    onSelect({ selected }) {
        //this.selected.splice(0, this.selected.length);
        //this.selected.push(...selected);
        //console.log(selected);
    }

    onSelect2({ selected }) {
        //console.log("SEL2 pre:",this.selected);
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        //console.log("seleected: ",selected);
        //console.log("SEL2:",this.selected);
    }

    // search department by keyword
    /* updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage();
        this.table.offset = 0;
        // this.accrualSetupService.searchAccruallist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    } */

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.selectedChange = false;
        this.pageChangeAttempt = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model["employeeMaster"].employeeIndexId = 0;
            return false;
        }
    }

    checkdecimalcell(input, idx) {
        if (input.value == null || input.value.toString() == "") {
            this.model['rowsFromDB'][idx].amtInvalid = true;
            return;
        }
        let tempd = input.value.toString().split(".");
        if (tempd != null && tempd[1] != null && ((tempd[0].length > 7) || (tempd[1] != null && tempd[1].length > 3))) {
            this.model['rowsFromDB'][idx].amtInvalid = true;
        }
        else if (tempd != null && ((tempd[1] == null && tempd[0].length > 7) || (tempd[1] != null && tempd[1].length > 3))) {
            this.model['rowsFromDB'][idx].amtInvalid = true;
        }
        else {
            this.model['rowsFromDB'][idx].amtInvalid = false;
        }
    }

    checkpercentcell(input, idx) {
        if (input.value == null || input.value.toString() == "") {
            this.model['rowsFromDB'][idx].amtInvalid = true;
            return;
        }
        let tempd = input.value.toString().split(".");
        if (tempd != null && tempd[1] != null && ((tempd[0].length > 5) || (tempd[1] != null && tempd[1].length > 5))) {
            this.model['rowsFromDB'][idx].amtInvalid = true;
        }
        else if (tempd != null && ((tempd[1] == null && tempd[0].length > 5) || (tempd[1] != null && tempd[1].length > 5))) {
            this.model['rowsFromDB'][idx].amtInvalid = true;
        }
        else {
            this.model['rowsFromDB'][idx].amtInvalid = false;
        }

    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        //this.setPage({ offset: 0 });
    }

    _keyPress(event: any) {
        const pattern = /[0-9]/;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }



    // PrintScreen
    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto,scale=50');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
              <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
              <style>
              @media print{
                .doNotPrint{display:none;!important}
              }
              @page { size: landscape; 
                 margin-top: 25px;
                margin-bottom: 250px;
                margin-right: 0px;
                margin-left: 0px;
                -webkit-transform: scale(0.5);  /* Saf3.1+, Chrome */
                -moz-transform: scale(0.5);  /* FF3.5+ */
                -ms-transform: scale(0.5);  /* IE9 */
                -o-transform: scale(0.5);  /* Opera 10.5+ */
                transform: scale(0.5);  }
            
              
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }
    printEmployeeDetails() {
        this.noPrintTable = true;
        this.printDetails = false;
        this.doNotPrintHearder = true;

        setTimeout(() => { window.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint() {
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }

}
