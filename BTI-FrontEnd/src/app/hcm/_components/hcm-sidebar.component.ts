import { Component } from '@angular/core';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Constants } from "../../_sharedresource/Constants";

@Component({
  selector: 'hcm-sidebar',
  templateUrl: './hcm-sidebar.component.html'
})
export class HCMSidebarComponent {
  isActive: number;
  moduleCode = Constants.hcmModuleCode;
  screenCode = 'S-1222';
  screenName;
  moduleId: string;
  defaultFormValues: [object];
  availableFormValues: [object];
  location: Location;
  sidebarValues: Array<Object>;
  oldLinkId: string= '';
  currentUrl: string;
  screenCategoryList: [object];
  resultList: [object];

  constructor(private getScreenDetailService: GetScreenDetailService, location: Location,
    private router: Router) {
    this.location = location;
    this.getScreenDetailService
          .getCurrentModule()
          .subscribe(sidebarValues => {
            if (typeof sidebarValues === 'string') {
              this.sidebarValues = [];
            } else {
              this.sidebarValues = sidebarValues;
            }
    });

    this.defaultFormValues = [
      { 'fieldName': 'MANAGE_DEPARTMENT', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_DIVISION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_POSITION', 'fildValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_POSITION_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_LOCATION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_SUPERVISOR', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_POSITION_PAY_CODE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_POSITION_BUDGET', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_DATE_CONVERTER', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_ATTENDANCE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_DEDUCTION_CODE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_ACCURAL_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_HEALTH_COVERAGE_TYPE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_SKILLS_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_BENEFIT_CODE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_HEALTH_INSURANCE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_SALARY_MATRIX_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_LIFE_INSURANCE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_HEALTH_INS_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_UR5_MANDATORY', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_PREDEFINED_CHECKLIST_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_ACCRUAL_SCHEDULE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_SHIFT_CODE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_PAYCODE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_REQUISITIONS_SETUPP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_RETIREMENT_PLAN_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_BENEFIT_PREFERENCE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_TRAINING_COURSE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_INTERVIEW_TYPE_SETUP', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'MANAGE_PAY_SCHEDULE_SETUP', 'fieldValue': '', 'helpMessage': '' },
	  { 'fieldName': 'MANAGE_TRAINING_BATCH_SIGNUP', 'fieldValue': '', 'helpMessage': '' },
	  { 'fieldName': 'MANAGE_TRAINING_CLASS_ENROLLMENT', 'fieldValue': '', 'helpMessage': '' },
	  { 'fieldName': 'MANAGE_TRAINING_CLASS_SKILL', 'fieldValue': '', 'helpMessage': '' }
    ];
  }

  ngOnInit() {

    this.currentUrl = this.router.url.replace('/', '');
    // getting screen
    this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
      this.availableFormValues = data.result.dtoScreenDetail.fieldList;
      this.moduleId = data.result.moduleId;
      for (var j = 0; j < this.availableFormValues.length; j++) {
        var fieldKey = this.availableFormValues[j]['fieldName'];
        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
        var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
        objDefault['fieldValue'] = objAvailable['fieldValue'];
        objDefault['helpMessage'] = objAvailable['helpMessage'];
      }
    });

    // getting initial sidebar menu based on first header
    this.getScreenDetailService.getHeaderDetail().subscribe(data => {
        this.resultList = data.result;
        for (let i = 0; i < this.resultList.length; i++) {
          let mid = this.resultList[i]['moduleId'];
          if (this.moduleId === mid) {
              this.screenCategoryList = data.result[i].screenCategoryList;
              this.getScreenDetailService.setCurrentModule(this.moduleId);
          }
        }
    });
  
    // setting active language
    var currentLanguage = localStorage.getItem('currentLanguage');
    if (currentLanguage === '1') {
      this.isActive = 1;
    } else if (currentLanguage === '2') {
      this.isActive = 2;
    } else {
      this.isActive = 1;
    }
  }

  redirectTo(RefUrl) {
    var oldlinkId =  <HTMLAnchorElement>document.getElementById(this.oldLinkId);
    var currentUrl =  <HTMLAnchorElement>document.getElementById(this.currentUrl);

   if (oldlinkId) {
     oldlinkId.removeAttribute('class');
   }
   if (currentUrl) {
     currentUrl.removeAttribute('class');
   }
   this.oldLinkId = RefUrl;
   this.router.navigate([RefUrl]);
 }

  getClass(path) {
    if (this.location.path().substr(0, path.length) === path) {
      return 'active';
    } else {
      return '';
    }
  }

}
