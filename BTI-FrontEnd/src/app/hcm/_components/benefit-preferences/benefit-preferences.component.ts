import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BenefitPreferences } from '../../_models/benefit-preferences/benefit-preferences';
import { BenefitPreferencesService } from '../../_services/benefit-preferences/benefit-preferences';
import { SettingFamilyLeave } from '../../_models/benefit-preferences/setting-family-leave';
import { SettingFamilyLeaveService } from '../../_services/benefit-preferences/setting-family-leave';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import {IMyDateModel, INgxMyDpOptions} from 'ngx-mydatepicker';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-benefit-preferences',
    templateUrl: './benefit-preferences.component.html',
    providers: [BenefitPreferencesService,SettingFamilyLeaveService,DatePipe]
})

// export to make it available for other classes
export class BenefitPreferencesComponent {
    page = new Page();
    rows = new Array<BenefitPreferences>();
    temp = new Array<BenefitPreferences>();
    selected = [];
    moduleCode = "M-1011";
    screenCode = "S-1431";
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    benefitPreferencesId = {};
    searchKeyword = '';
    ddPageSize = 5;
    model: BenefitPreferences;
    modelSetFamily: SettingFamilyLeave;
    cityOptions = [];
  
    isSuccessMsg;
    isfailureMsg;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText;
    emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}";
    //emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
    isModalPopUpActive = false;
    currentLanguage: any;
    isConfirmationModalOpen: boolean = false;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    benefitPreferencesIdvalue : string;
    isFamilyLeaveCalender:boolean = false;
    periodStart;
    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    frmStartDate;
    startDate;
    startDateformatted;
    

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    // Initialized to specific date (09.10.2018).
    public startDateModel: any = { date: { day: 12, month: 4, year: 2018 } };

    constructor(
        private router: Router,
        private benefitPreferencesService: BenefitPreferencesService,
        private settingFamilyLeaveService: SettingFamilyLeaveService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private datePipe: DatePipe) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        //default form parameter for division  screen
        this.defaultFormValues = [
            { 'fieldName': 'BENEFIT_PREFERENCE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POST_ESTIMATED_RETURN_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POST_PAYMENT_DATES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NUMBER_OF_WORKING_DAYS_IN_A_WEEK', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NUMBER_OF_HOURS_IN_A_WORK_DAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POST_ELIGIBLITY_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POST_PAPERWORK_DEADLINES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SET_FAMILY_LEAVE_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'OK', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LEAVE_PREFERENCES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MEDICAL_PREFERENCES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SELECT_THE_METHOD_YOUR_COMPANY_WILL_USE_TO_CALCULATE_ITS_FAMILY_LEAVES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'IS_THIS_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'WHEN_DOES_THIS_PERIOD_START', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SETTING_FAMILY_LEAVE_CALENDER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLOSE', 'fieldValue': '', 'helpMessage': '' },

        ];
        
    }

    // Screen initialization
    ngOnInit() {
       
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.isUnderUpdate = false;
      
        this.model = {
            id: 0,
            postEstimatedDate: 0,
            postPaymentDates: 0,
            workingdaysWeek: 0,
            workingHoursDay: 0,
            eligibilityDate: 0,
            paperworkDeadlines: 0
            
        };

        this.modelSetFamily = {
            id: 0,
            method: 0,
            isThisPeriod: 0,
            periodStart: this.startDateModel            
        };

        this.benefitPreferencesService.getAllBenefitPreferences({"sortOn" : "","sortBy" : "","searchKeyword" : "","pageNumber":"0","pageSize":"10"}).then(data => {
           if(data && data.result && data.result.records && data.result.records.length){
            this.model = data.result.records[0];
           }
        });
        
        this.settingFamilyLeaveService.getAllSettingFamilyLeave({"sortOn" : "","sortBy" : "","searchKeyword" : "","pageNumber":"0","pageSize":"10"}).then(data => {
            if(data && data.result && data.result.records && data.result.records.length){
             this.modelSetFamily = data.result.records[0];
             this.frmStartDate =this.formatDateFordatePicker(new Date(this.modelSetFamily.periodStart));
           
            }
         });
         
        //getting screen
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
            //debugger;
            console.log(data);
            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
            console.log(this.defaultFormValues);
        });
    }

    

   
    // Clear form to reset to default blank
    Clear(f: NgForm) {
        
        this.isFamilyLeaveCalender = false;
    }

    //function call for SetFamilyLeavePeriod
    SetFamilyLeavePeriod() {
        event.preventDefault();
        this.isFamilyLeaveCalender = true;
        
    }

    //function call for save new BenefitPreferences
    SaveBenefitPreferences(f: NgForm, event: Event) {
        event.preventDefault();
        console.log(this.model);
        var Idx = this.model.id;
        //Check if the id is available in the model.
        //If id avalable then update the BenefitPreferences, else Add new BenefitPreferences.
        if (this.model.id > 0 && this.model.id != undefined) {
           //Call service api for Update benefitPreferences
           this.benefitPreferencesService.updateBenefitPreferences(this.model).then(data => {

                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        
                        this.messageText = data.btiMessage.message;;
                        this.model = data.result;
                    }, 100);

                    this.hasMsg = true;

                  
                    
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });
        } else {
            console.log('create')
            //Call service api for Creating new benefitPreferences
            this.benefitPreferencesService.createBenefitPreferences(this.model).then(data => {

                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        
                        this.messageText = data.btiMessage.message;;
                    }, 100);

                    this.hasMsg = true;

                   
                    
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });
               
        }
    }
    

    //function call for save new SettingFamilyLeave
    SaveSettingFamilyLeave(f: NgForm, event: Event) {
        event.preventDefault();
        console.log(this.frmStartDate);
        this.modelSetFamily.periodStart = this.frmStartDate.date.year+'-'+this.frmStartDate.date.day+'-'+this.frmStartDate.date.month;
      
        
        var Idx = this.modelSetFamily.id;
        //Check if the id is available in the modelSetFamily.
        //If id avalable then update the BenefitPreferences, else Add new BenefitPreferences.
        if (this.modelSetFamily.id > 0 && this.modelSetFamily.id != undefined) {
           //Call service api for Update SettingFamilyLeave
           this.settingFamilyLeaveService.updateSettingFamilyLeave(this.modelSetFamily).then(data => {

                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        
                        this.messageText = data.btiMessage.message;;
                        this.modelSetFamily = data.result;
                        
                    }, 100);

                    this.hasMsg = true;

                  
                    
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });
        } else {
            console.log('create')
            //Call service api for Creating new modelSetFamily
            this.settingFamilyLeaveService.createSettingFamilyLeave(this.modelSetFamily).then(data => {

                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        
                        this.messageText = data.btiMessage.message;;
                    }, 100);

                    this.hasMsg = true;

                   
                    
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });
               
        }
    }
 
    

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() }, jsdate: setDate};
        } else {
            return null;
        }
    }
	onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        this.startDateformatted = event.formatted;
    }

    _keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
    
        if (!pattern.test(inputChar)) {
          // invalid character, prevent input
          event.preventDefault();
        }
    }
    maxLengthAllow(event: any,max) {
        if (event.target.value.length >= max) {
          // invalid character, prevent input
          event.preventDefault();
        }
    }

    

  

   

}