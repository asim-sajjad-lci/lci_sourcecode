import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { TrainingBatchSignup } from '../../_models/training-batch-signup/training-batch-signup';
import { TrainingBatchSignupService } from '../../_services/training-batch-signup/training-batch-signup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'training-batch-signup',
    templateUrl: './training-batch-signup.component.html',
    styleUrls: ['./training-batch-signup.component.css'],
    providers: [TrainingBatchSignupService,CommonService]
})

// export to make it available for other classes
export class TrainingBatchSignupComponent {
    page = new Page();
    rows = new Array<TrainingBatchSignup>();
    temp = new Array<TrainingBatchSignup>();
    selected = [];
    selectedSelectedEmployees = [];
    selectedAllEmployees = [];
    moduleCode = "M-1011";
    screenCode = "S-1434";
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    trainingBatchSignupId = {};
    searchKeyword = '';
    ddPageSize = 5;
    model;
    cityOptions = [];
    showCreateForm: boolean = false;
    isSuccessMsg;
    isfailureMsg;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText;
    emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}";
    //emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
    isModalPopUpActive = false;
    currentLanguage: any;
    isConfirmationModalOpen: boolean = false;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    trainingBatchSignupIdvalue: string;
    AllTraningCourseId = [];
    AllTrainingClass = [];
    AllEmployeeMaster = [];
    AllEmployees = [];
    dataSource: Observable<any>;
    trainingBatchSignupIdSearch = [];
    typeaheadLoading: boolean;
    SelectedEmployees = [];
    TraningCourseIdDesc;
    TrainingClassDesc;
    TrainingClassmaximum;
    itemsList: Observable<any>;
    getItems:any[]=[];
    searchBy: string;
    fieldval: string;


    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

            TRAINING_ID:any;
            CLASS_ID:any;
            EMPLOYEES:any;
            PARAMERER:any;
            SELECT:any;
            ALL_EMPLOYEES:any;
            SELECTED_EMPLOYEES:any;
            DELETE:any;
            SAVE:any;
            CLEAR:any;
            ACTION:any;
            TRAINING_DESCRIPTION:any;
            EMPLOYEE_NAME:any;
            CREATE:any;
            SEARCH:any;
            UPDATE:any;
    constructor(
        private router: Router,
        private trainingBatchSignupService: TrainingBatchSignupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,private commonService:CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for trainingBatchSignup  screen
        this.defaultFormValues = [
            { 'fieldName': 'TRAINING_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PARAMERER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SELECT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ALL_EMPLOYEES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SELECTED_EMPLOYEES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRAINING_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' }
        ];

            this.TRAINING_ID=this.defaultFormValues[0];
            this.CLASS_ID=this.defaultFormValues[1];
            this.EMPLOYEES=this.defaultFormValues[2];
            this.PARAMERER=this.defaultFormValues[3];
            this.SELECT=this.defaultFormValues[4];
            this.ALL_EMPLOYEES=this.defaultFormValues[5];
            this.SELECTED_EMPLOYEES=this.defaultFormValues[6];
            this.DELETE=this.defaultFormValues[7];
            this.SAVE=this.defaultFormValues[8];
            this.CLEAR=this.defaultFormValues[9];
            this.ACTION=this.defaultFormValues[10];
            this.TRAINING_DESCRIPTION=this.defaultFormValues[11];
            this.EMPLOYEE_NAME=this.defaultFormValues[12];
            this.CREATE=this.defaultFormValues[13];
            this.SEARCH=this.defaultFormValues[14];
            this.UPDATE=this.defaultFormValues[15];

        this.itemsList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.courseComments);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

   }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.setPage({ offset: 0 });
        this.GetTraningCourseID();
        this.GetTraningCourseClass();
        // this.trainingBatchSignupService.getAllTraningCourse(this.page, this.searchKeyword).then(data => {
        //     this.AllTraningCourseId = data.result.records;
        //     console.log('AllTraningCourseId');
        //     console.log(this.AllTraningCourseId);
        // });
        // this.trainingBatchSignupService.getTrainingCourse(this.page, this.searchKeyword).then(data => {
        //     this.AllTrainingClass = data.result.records;
        //     console.log('this.AllTrainingClass');
        //     console.log(this.AllTrainingClass);
        // });
        


        //getting screen
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.trainingBatchSignupService.getAllEmployeeMaster().then(data => {

            this.AllEmployeeMaster = data.result;

            /*for(var i=0; i<this.AllEmployeeMaster.length;i++){
                console.log('arrrayy dept_id '+ this.AllEmployeeMaster[i].department_Id);
                var index = this.getItems.findIndex(x => x.department_Id == this.AllEmployeeMaster[i].department_Id)
                if(index == -1){
                    this.getItems.push(this.AllEmployeeMaster[i]);
                }
            }*/
            this.AllEmployees = this.AllEmployeeMaster;
           //console.log('this.AllEmployeeMaster');
           //console.log(this.AllEmployeeMaster);

        });
    }
    // Exiting Id Search on every click
    getReportTrainingBatchSignupAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.trainingBatchSignupIdSearch.filter((id: any) => {
                return query.test(id.trainingBatchSignupId);
            })
        );
    }

    getRowValue(row, gridFieldName) {
        if (gridFieldName == 'traningId') {
            return row['traningCourse'] && row['traningCourse']['traningId'] ? row['traningCourse']['traningId'] : '';
        }  else if (gridFieldName == 'desc') {
            return row['traningCourse'] && row['traningCourse']['desc'] ? row['traningCourse']['desc'] : '';
        }  else if (gridFieldName == 'classId') {
            let cid = '';
            for (let i = 0; i< row['trainingClassList'].length; i++) {
                cid = cid + row['trainingClassList'][i]['classId'] + ',';
            }
            return cid;
        } else if (gridFieldName == 'employeeFirstName') {
            let eid = '';
            for (let i = 0; i< row['employeeMaster'].length; i++) {
                eid = eid + row['employeeMaster'][i]['employeeFirstName'] + ',';
            }
            return eid;
        } else {
            return row[gridFieldName];
        }
    }

    changeTypeaheadLoading(e: boolean, courseComments): void {
        this.typeaheadLoading = e;
        if((courseComments == '' || courseComments == null) && this.AllEmployees.length==0){
            this.AllEmployees = this.AllEmployeeMaster;
        }
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
       //console.log('Selected value: ', e.value);
    }

    //setting pagination
    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.trainingBatchSignupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
           //console.log('this.rows');
           //console.log(this.rows);

        });
        
    }
    
    GetTraningCourseID(){
        this.trainingBatchSignupService.getAllTraningCourse(this.page, this.searchKeyword).then(data => {
            this.AllTraningCourseId = data.result.records;
           //console.log('AllTraningCourseId');
           //console.log(this.AllTraningCourseId);
        });
    }

    GetTraningCourseClass(){
        this.trainingBatchSignupService.getTrainingCourse(this.page, this.searchKeyword).then(data => {
            this.AllTrainingClass = data.result.records;
           //console.log('this.AllTrainingClass');
           //console.log(this.AllTrainingClass);
        });
    }

    Cancel(){
        this.GetTraningCourseID();
       //console.log("aa:", this.AllTraningCourseId);
        this.GetTraningCourseID();
       //console.log("aaa:", this.AllTrainingClass);
        this.showCreateForm = false;
       //console.log("After Cancel:",this.model);
    }

    create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            this.TrainingClassDesc = '';
            this.TraningCourseIdDesc = '';
            this.AllEmployees = this.AllEmployeeMaster;
            setTimeout(() => {
                window.scrollTo(0, 600);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            selectedEmployes: '',
            completeDate: '',
            courseComments: '',
            trainingClassStatus: '0',
            employeeMaster: [{
                employeeIndexId: 0
            }],
            trainingClassList:[ {
                id: 0
            }],
            traningCourse: {
                id: 0
            }
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        if(!this.model.id){
            // this.model = {
            //     id: 0,
            //     selectedEmployes: '',
            //     completeDate: '',
            //     courseComments: '',
            //     trainingClassStatus: '0',
            //     employeeMaster: [{
            //         employeeIndexId: 0
            //     }],
            //     trainingClassList:[ {
            //         id: 0
            //     }],
            //     traningCourse: {
            //         id: 0
            //     }
            // };
           
            f.resetForm();
            this.TrainingClassDesc = '';
            this.TraningCourseIdDesc = '';
            this.SelectedEmployees = [];
            this.AllTrainingClass = [];
        }else{
            // this.model['selectedEmployes'] = '';
            // this.model['completeDate'] = '212121212';
            // this.model['courseComments'] = '';
            // this.model['trainingClassStatus'] = 1212,
            // this.model['employeeMaster']['employeeIndexId'] = 0;
            // this.model['trainingClass']['id'] = 0;
            // this.model['traningCourse']['id'] = 0;            
           
            f.resetForm();
            this.TrainingClassDesc = '';
            this.TraningCourseIdDesc = '';
            this.SelectedEmployees = [];
            this.AllTrainingClass = [];
        }
        
    }

    //function call for creating new trainingBatchSignup
    CreateTrainingBatchSignup(f: NgForm, event: Event) {
        event.preventDefault();
        var divIdx = this.model.id;
       
        this.model.employeeMaster = [];
        this.SelectedEmployees.forEach(element => {
            this.model.employeeMaster.push({employeeIndexId: element.employeeIndexId});
        });

        if(!this.model || !this.model.trainingClass){
            this.model.trainingClass = {
                id : 0
            }; 
        }
        if(!this.model || !this.model.employeeMaster || this.model.employeeMaster.length == 0){
            this.model.employeeMaster = [{
                employeeIndexId : 0
            }]; 
        }
        if(!this.model || !this.model.dtoTrainingCourseDetail){this.model.dtoTrainingCourseDetail = [{
                id : this.model.trainingClassList[0].id
            }]; 
                       
        }

        //Check if the id is available in the model.
        //If id avalable then update the trainingBatchSignup, else Add new trainingBatchSignup.
        if (this.model.id > 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {

            //Call service api for Creating new trainingBatchSignup
            this.trainingBatchSignupService.createTrainingBatchSignup(this.model).then(data => {

                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.showCreateForm = false;
                        this.SelectedEmployees = [];
                        this.selectedAllEmployees = []
                        this.selectedSelectedEmployees = [];
                       // this.AllEmployees = this.AllEmployeeMaster;
                        this.messageText = data.btiMessage.message;;
                    }, 100);

                    this.hasMsg = true;

                    f.resetForm();

                    //Refresh the Grid data after adding new trainingBatchSignup
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                    this.AllEmployees = this.AllEmployeeMaster;
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });



        }
    }

    //edit trainingBatchSignup by row id
    edit(row) {

       //console.log('row ')
       //console.log( row)
        this.showCreateForm = true;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.GetTraningCourseID();
        this.GetTraningCourseClass();
       //console.log("aaaaaaaaaaID:", this.AllTraningCourseId);
       //console.log("aaaaaaaaaaClass:", this.AllTrainingClass);
       //console.log('PreviousModel',this.model);
        this.model = Object.assign({}, row);
       //console.log('AfterModel',this.model);
        this.isUnderUpdate = true;
        this.TraningCourseIdDesc = row.traningCourse['desc'];
       // this.TrainingClassmaximum = row.trainingClassList[0]['maximum'];
        this.trainingBatchSignupService.getTrainingCourseDetailTraningId(row.traningCourse.id).subscribe(data => {
             this.AllTrainingClass =data.records;
             this.TrainingClassDesc = row.trainingClassList[0].className ;
        });

        this.trainingBatchSignupService.getAllEmployeeMaster().then(data => {
            this.AllEmployeeMaster = data.result;         
            this.AllEmployees = this.AllEmployeeMaster;
            this.SelectedEmployees = Object.assign([], row.employeeMaster);
           //console.log('selectedemployee',this.SelectedEmployees)
           //console.log('allemployees',this.AllEmployees);
            for (var j = 0; j < this.AllEmployees.length; j++) {
              //  const index = this.AllEmployees.indexOf(this.SelectedEmployees[j]);
               // alert(index);
               for(var i=0; i<this.SelectedEmployees.length;i++){
                   if(this.AllEmployees[j].employeeIndexId == this.SelectedEmployees[i].employeeIndexId){
                    this.AllEmployees.splice(j, 1);
                   }
               }
                
                // if (index !== -1) {
                //     this.AllEmployees.splice(index, 1);
                // }
            } 
        });    
        

        
        setTimeout(() => {
            window.scrollTo(0, 500);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        //this.model.trainingBatchSignupId = this.trainingBatchSignupIdvalue;
       //console.log(this.model);
        //Call service api for updating selected trainingBatchSignup
        this.trainingBatchSignupService.updateTrainingBatchSignup(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    //delete one or multiple trainingBatchSignups
    delete() {
        var selectedTrainingBatchSignups = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedTrainingBatchSignups.push(this.selected[i].id);
        }
        this.trainingBatchSignupService.deleteTrainingBatchSignup(selectedTrainingBatchSignups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of trainingBatchSignup
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }


    // search rolegroup details by group name 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        /* this.trainingBatchSignupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
        }); */
       
    }
    

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    moveToSelected() {

        if(this.TrainingClassmaximum && this.TrainingClassmaximum < this.selectedSelectedEmployees.length){
            this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = " Can’t select more than "+this.TrainingClassmaximum+" employees";
                }, 100);
                return false;
        }       

        for (var j = 0; j < this.selectedAllEmployees.length; j++) {
            const index = this.AllEmployees.indexOf(this.selectedAllEmployees[j]);
            if (index !== -1) {
                this.AllEmployees.splice(index, 1);
                this.SelectedEmployees.push(this.selectedAllEmployees[j])
            }
        }   
        this.trainingBatchSignupService.setEnrolledEmployees(this.SelectedEmployees.length);
        this.selectedAllEmployees = [];    
    }

    moveToAllEmployee() {
        for (var j = 0; j < this.selectedSelectedEmployees.length; j++) {
            const index = this.SelectedEmployees.indexOf(this.selectedSelectedEmployees[j]);

            if (index !== -1) {
                this.SelectedEmployees.splice(index, 1);
                this.AllEmployees.push(this.selectedSelectedEmployees[j])
            }
        }
        this.selectedSelectedEmployees = [];
    }
    // default list on page
    onSelectAllEmployees({ selected }) {
        this.selectedAllEmployees.splice(0, this.selectedAllEmployees.length);
        this.selectedAllEmployees.push(...selected);
       //console.log(this.selectedAllEmployees);

    }
    
    // default list on page
    onSelectSelectedEmployees({ selected }) {
        this.selectedSelectedEmployees.splice(0, this.selectedSelectedEmployees.length);
        this.selectedSelectedEmployees.push(...selected);
       //console.log(this.selectedSelectedEmployees);

    }

    changeTraningCourseId(event){
        for (let index = 0; index < this.AllTraningCourseId.length; index++) {
            const element = this.AllTraningCourseId[index];
            if(element.id == event.target.value){
                this.TraningCourseIdDesc = element.desc;
                this.TrainingClassDesc = '';
               //console.log(element);
               //console.log(this.AllTrainingClass);
                //this.AllTrainingClass

                this.trainingBatchSignupService.getTrainingCourseDetailTraningId(element.id).subscribe(data => {
                   //console.log(data.records);
                    this.AllTrainingClass =data.records;
                    
                });

            }
            
        }
    }
    changeTrainingClass(event){
        for (let index = 0; index < this.AllTrainingClass.length; index++) {
            const element = this.AllTrainingClass[index];
            if(element.id == event.target.value){
                this.TrainingClassDesc = element.className ;
                this.TrainingClassmaximum = element.maximum;
               //console.log(element);
            }
            
        }
    }    

    changeEmployee(event){
        this.getItems = [];
        if(event.target.value=="Department ID"){
            this.searchBy = "Department ID" ;
            this.fieldval = "department_Id";
            for(var i=0; i<this.AllEmployeeMaster.length;i++){
                var index = this.getItems.findIndex(x => x.department_Id == this.AllEmployeeMaster[i].department_Id)
                if(index == -1){
                    this.getItems.push(this.AllEmployeeMaster[i]);
                }
            }
        } else if(event.target.value=="Division ID"){
            this.searchBy = "Division ID" ;
            this.fieldval = "division_Id";
            for(var i=0; i<this.AllEmployeeMaster.length;i++){
                var index = this.getItems.findIndex(x => x.division_Id == this.AllEmployeeMaster[i].division_Id)
                if(index == -1){
                    this.getItems.push(this.AllEmployeeMaster[i]);
                }
            }
        } else{
            this.searchBy = "Position ID" ;
            this.fieldval = "position_Id";
            for(var i=0; i<this.AllEmployeeMaster.length;i++){
                var index = this.getItems.findIndex(x => x.position_Id == this.AllEmployeeMaster[i].position_Id)
                if(index == -1){
                    this.getItems.push(this.AllEmployeeMaster[i]);
                }
            }
        }
        this.AllEmployees = this.AllEmployeeMaster;
    }
    // search rolegroup details by group name 
    /* updateFilterEmployee(event) {
        this.searchKeyword = event.target.value.toLowerCase();        
        
    } */

    getSuperviserIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        this.AllEmployees = this.AllEmployeeMaster;
       //console.log('query '+ query);
        return Observable.of(
            this.getItems.filter((id: any) => {
                if(this.searchBy=="Department ID"){
                    // this.AllEmployees = this.AllEmployees.filter((id: any) => {
                    //     return query.test(id.department_Id);
                    // });
                    return query.test(id.department_Id);
                } else if(this.searchBy=="Division ID"){
                    // this.AllEmployees = this.AllEmployees.filter((id: any) => {
                    //     return query.test(id.division_Id);
                    // });
                    return query.test(id.division_Id);
                } else{
                    // this.AllEmployees = this.AllEmployees.filter((id: any) => {
                    //     return query.test(id.position_Id);
                    // });
                    return query.test(id.position_Id);
                }

            })
        );
    }

}