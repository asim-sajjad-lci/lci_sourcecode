import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { ActiveEmployeePostDatedPayRateModule, ActiveEmployeePostDatedPayRateModulepayload } from '../../_models/active-employee-post-dated-payrate/active-employee-post-dated-payrate';
import { ActiveEmployeePostDatedPayRateService } from '../../_services/active-employee-post-dated-payrate/active-employee-post-dated-payrate.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
@Component({
    selector: 'active-employee',
    templateUrl: './active-employee-post-dated-payrate.component.html',
    providers: [ActiveEmployeePostDatedPayRateService]
})

// export Department component to make it available for other classes
export class ActiveEmployeePostDatedPayrateComponent {
    page = new Page();
    rows = new Array<ActiveEmployeePostDatedPayRateModule>();
    temp = new Array<ActiveEmployeePostDatedPayRateModule>();
    selected = [];
    moduleCode = "M-1011";
    screenCode = "S-1469";
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    departmentId = {};
    searchKeyword = '';
    getDepartment: any[] = [];
    ddPageSize: number = 5;
    model: ActiveEmployeePostDatedPayRateModule;
    model1: ActiveEmployeePostDatedPayRateModulepayload;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    departmentIdvalue: number;
    batchIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
    employeeId;
    emdescription;
    CodeId;
    codeDesc;
    batchDropdownList = [];
    batchId;
    bId;
    all;
    from;
    fromemployeeId;
    toemployeeId;
    frompaycodeId;
    topaycodeId;
    fromreason;
    toreason;
    transDataList: Observable<any>;
    transDeductionDataList: Observable<any>;
    transBenefitDataList: Observable<any>;
    employeeIdList: Observable<any>;
    toemployeeIdList: Observable<any>;
    formpaycodeIdList: Observable<any>;
    topaycodeIdList: Observable<any>;
    fromreasonIdList: Observable<any>;
    toreasonIdList: Observable<any>;
    reasonIdDropdown = [];
    transTypeIdList = [];
    employeeIdOption = [];
    isDeleteBatch: boolean = false;
    paycodeIdDropdownList = [];
    error: any = { isError: false, errorMessage: '' };
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(
        private router: Router,
        private activeEmployeePostDatedPayRateService: ActiveEmployeePostDatedPayRateService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_EDIT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_REDISPLAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_ALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_RANGES', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_FROM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_TO', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_ALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_SORTBY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_REDISPLAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_MARKALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_UNMARKALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_SELECT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_EFFECTIVE_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_PAY_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_NEW_RATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_CURRENT_RATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_PROCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_PRINT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_ACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_ACTION', 'fieldValue': '', 'helpMessage': '' },

            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_UPDATE', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_EDIT', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_ACTIONL', 'fieldValue': '', 'helpMessage': '' },
        ];
        this.batchIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.batchId);
        }).mergeMap((token: string) => this.getBatchIdAsObservable(token));

        this.transDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getpayCodeIdAsObservable(token));


        this.transDeductionDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getdeductionCodeIdAsObservable(token));

        this.transBenefitDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getbenefitCodeIdAsObservable(token));

        this.employeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.fromemployeeId);
        }).mergeMap((token: string) => this.getfromemployeeIdAsObservable(token));

        this.toemployeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.toemployeeId);
        }).mergeMap((token: string) => this.gettoemployeeIdAsObservable(token));

        this.formpaycodeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.frompaycodeId);
        }).mergeMap((token: string) => this.getpayCodeIdAsObservable(token));

        this.topaycodeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.topaycodeId);
        }).mergeMap((token: string) => this.gettopayCodeIdAsObservable(token));


        this.fromreasonIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.fromreason);
        }).mergeMap((token: string) => this.getfromreasonIdAsObservable(token));

        this.toreasonIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.toreason);
        }).mergeMap((token: string) => this.gettoreasonIdAsObservable(token));
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }

        });

        this.activeEmployeePostDatedPayRateService.getAllActiveEmployee(this.page).then(pageData => {

            this.rows = pageData.result.records;
            this.rows.forEach(row=>{row.active=false;});
            console.log('row', this.rows);
        });
        //Following shifted from SetPage for better performance
        this.activeEmployeePostDatedPayRateService.getDepartments().then(data => {
            this.getDepartment = data.result;
            //console.log("Data class options : "+this.getDepartment);
        });
        this.Create();
    }
    getBatchIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.batchDropdownList.filter((id: any) => {
                return query.test(id.batchId);
            })
        );
    }
    getpayCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.paycodeIdDropdownList.filter((id: any) => {
                return query.test(id.payCodeId);
            })
        );
    }

    gettopayCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.paycodeIdDropdownList.filter((id: any) => {
                return query.test(id.payCodeId);
            })
        );
    }

    getdeductionCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.diductionId);
            })
        );
    }

    getbenefitCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.benefitId);
            })
        );
    }

    getfromemployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.employeeIdOption.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    gettoemployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.employeeIdOption.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    getfromreasonIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.reasonIdDropdown.filter((id: any) => {
                return query.test(id.reasonforChange);
            })
        );
    }

    gettoreasonIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.reasonIdDropdown.filter((id: any) => {
                return query.test(id.reasonforChange);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        //console.log('Selected value: ', e.value);
        //    this.model.batches={id:e.item.id}
        //    this.bId=e.item.id;
        //    console.log('batches',JSON.stringify(this.model));
        //    this.model.description=e.item.description;
        //    this.model.arabicDescription=e.item.arabicDescription;
        //    this.transactionEntryService.getTransctionEntryByBatch(this.page,e.item.id).then(data => 
        //         {
        //             this.rows=data.result.records;  
        //         }
        //     )
    }

    typeaheadPayCodeOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail['codeId']=e.item.id;
        // this.model.transactionEntryDetail['payRate']=e.item.payRate;
        // this.model.transactionEntryDetail['amount']= e.item.baseOnPayCodeAmount;
        // this.codeDesc = e.item.description;
        // console.log('paycode',this.codeDesc);
        this.model.fromPayCodeId = e.item.id;
    }

    typeaheadtoPayCodeOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail['codeId']=e.item.id;
        // this.model.transactionEntryDetail['payRate']=e.item.payRate;
        // this.model.transactionEntryDetail['amount']= e.item.baseOnPayCodeAmount;
        // this.codeDesc = e.item.description;
        // console.log('paycode',this.codeDesc);
        this.model.toPayCodeId = e.item.id;
    }

    typeaheadDeductionCodeOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail['codeId']=e.item.id;
        // this.model.transactionEntryDetail['amount']=e.item.amount;
        // this.codeDesc = e.item.discription;
        // console.log('deduction',this.codeDesc);
    }


    typeaheadBenefitCodeOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail['codeId']=e.item.id;
        // this.model.transactionEntryDetail['amount']=e.item.amount;
        // this.codeDesc = e.item.desc;
        // console.log('benefit',this.codeDesc);
    }

    typeaheadEmployeeOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail["employeeMaster"].employeeIndexId=e.item.employeeIndexId;
        // this.emdescription = e.item.employeeFirstName;
        // console.log('employee',this.model);
        this.model.fromemployeeIndexId = e.item.employeeIndexId;
        console.log('fromemployee', this.model.fromemployeeIndexId);
    }

    typeaheadtoEmployeeOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail["employeeMaster"].employeeIndexId=e.item.employeeIndexId;
        // this.emdescription = e.item.employeeFirstName;
        // console.log('employee',this.model);
        this.model.toemployeeIndexId = e.item.employeeIndexId;
    }

    typeaheadFromreasonOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail["employeeMaster"].employeeIndexId=e.item.employeeIndexId;
        // this.emdescription = e.item.employeeFirstName;
        // console.log('employee',this.model);
        this.model.fromReasion = e.item.id;
        console.log('fromemployee', this.model.fromReasion);
    }

    typeaheadtoreasonOnSelect(e: TypeaheadMatch): void {

        // this.model.transactionEntryDetail["employeeMaster"].employeeIndexId=e.item.employeeIndexId;
        // this.emdescription = e.item.employeeFirstName;
        // console.log('employee',this.model);
        this.model.toReasion = e.item.id;
    }
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        // this.transactionEntryService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        // });
        this.activeEmployeePostDatedPayRateService.getAllBatch(this.searchKeyword).then(data => {
            this.batchDropdownList = data.result;
        });

        this.activeEmployeePostDatedPayRateService.getEmployeeId().then(data => {
            this.employeeIdOption = data.result;
        }
        );
        this.activeEmployeePostDatedPayRateService.getPayCodeId().then(data => {
            this.paycodeIdDropdownList = data.result;
        })

        this.activeEmployeePostDatedPayRateService.getReasonId().then(data => {
            this.reasonIdDropdown = data.result;
        })
    }

    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            ranges: '0',
            fromDate: null,
            toDate: null,
            select: false,
            fromemployeeIndexId: null,
            toemployeeIndexId: null,
            fromPayCodeId: null,
            toPayCodeId: null,
            fromReasion: null,
            toReasion: null,
            active: false

        };
        this.all = true;
        this.model1 = {
            listActivateEmployeePostDatePayRate: [{
                "employeeMaster": {
                    "employeeIndexId": 0
                },
                "payCode": {
                    "id": 0
                },
                "effectiveDate": "",
                "newRate": 0,
                "currentRate": 0,
                "active": false

            }]
        };
    }
    display() {
        this.model.fromDate = this.frmStartDate;
        this.model.toDate = this.frmEndDate;
        if (this.all == true) {
            this.activeEmployeePostDatedPayRateService.getAllActiveEmployee(this.page).then(pageData => {

                this.rows = pageData.result.records;
                console.log('row', this.rows);
            });
        } 
        else {
            this.activeEmployeePostDatedPayRateService.getActiveEmployee(this.page, this.model).then(data => {
                this.rows = data.result.records;
            });
        }

        this.rows.forEach(row=>{row.active=false;});
    }
    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ ranges: '0' });
        this.all = true;
    }

    //function call for creating new department
    CreateActiveEmployeePostdatedPayrate(f: NgForm, event: Event) {
        var temp = {
            "employeeMaster": {
                "employeeIndexId": 0
            },
            "payCode": {
                "id": 0
            },
            "effectiveDate": "",
            "newRate": 0,
            "currentRate": 0,
            "active": false
        };
        for (var i = 0; i < this.selected.length - 1; i++) {
            this.model1.listActivateEmployeePostDatePayRate.push(temp);
        }
        for (var i = 0; i < this.selected.length; i++) {
            this.model1.listActivateEmployeePostDatePayRate[i]["employeeMaster"]["employeeIndexId"] = this.selected[i].employeeMasterHcm.employeeIndexId;
            this.model1.listActivateEmployeePostDatePayRate[i]["payCode"]["id"] = this.selected[i].payCode.id;
            this.model1.listActivateEmployeePostDatePayRate[i]["effectiveDate"] = this.selected[i].effectiveDate;
            this.model1.listActivateEmployeePostDatePayRate[i]["newRate"] = this.selected[i].newPayRate;
            this.model1.listActivateEmployeePostDatePayRate[i]["currentRate"] = this.selected[i].currentPayRate;
            this.model1.listActivateEmployeePostDatePayRate[i]["active"] = this.selected[i].active;
        }

        event.preventDefault();
        this.model.fromDate = this.frmStartDate;
        this.model.toDate = this.frmEndDate;
        var deptIdx = this.model.id;

        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.activeEmployeePostDatedPayRateService.checkDuplicateDeptId(deptIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new department
                    this.activeEmployeePostDatedPayRateService.createTransaction(this.model1).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);

                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            this.activeEmployeePostDatedPayRateService.getTransctionEntryByBatch(this.page, this.bId).then(data => {
                                this.rows = data.result.records;
                            }
                            )
                            f.resetForm({ ranges: '0' });
                            this.all=true;
                            //Refresh the Grid data after adding new department
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: ActiveEmployeePostDatedPayRateModule) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.departmentId = row.id;
        this.departmentIdvalue = this.model.id;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
        this.model.id = this.departmentIdvalue;
        this.activeEmployeePostDatedPayRateService.updateDepartment(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = true;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    varifyDeleteBatch() {

        this.showCreateForm = true;
        this.isDeleteBatch = true;
        this.isConfirmationModalOpen = true;
        window.scrollTo(0, 0);
    }

    deleteBatch() {

        this.activeEmployeePostDatedPayRateService.deleteTransactionEntryBatch(this.bId).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.activeEmployeePostDatedPayRateService.getTransctionEntryByBatch(this.page, this.bId).then(data => {
                    this.rows = data.result.records;
                }
                )
            }
            this.hasMessage = true;
            this.message.type = "success";
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.activeEmployeePostDatedPayRateService.getTransctionEntryByBatch(this.page, this.bId).then(data => {
                this.rows = data.result.records;
            }
            )
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    //delete department by passing whole object of perticular Department
    delete() {
        var selectedTransaction = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedTransaction.push(this.selected[i].id);
        }
        this.activeEmployeePostDatedPayRateService.deleteTransactionEntryByrow(selectedTransaction).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.activeEmployeePostDatedPayRateService.getTransctionEntryByBatch(this.page, this.bId).then(data => {
                    this.rows = data.result.records;
                }
                )
            }
            this.hasMessage = true;
            this.message.type = "success";
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.activeEmployeePostDatedPayRateService.getTransctionEntryByBatch(this.page, this.bId).then(data => {
                this.rows = data.result.records;
            }
            )
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        //console.log('this.frmStartDate', event);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
        //console.log('this.frmEndDate', this.frmEndDate, event);
        //console.log(this.endDate);
        //console.log(this.frmStartDate);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: ' Invalid End Date' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
        //this.frmEndDate = event.jsdate;
        //this.frmEndDate = event.jsdate;
        //console.log('>>>>>>>>>>', f.controls.['frmEndDate']);
        //console.log('<<<<<<<<<<<<', f.controls['frmStartDate'].value);
        //if(f.controls['frmEndDate'].value.formatted < f.controls['frmStartDate'].value.formatted){
        //this.error={isError:true,errorMessage:' Invalid End Date'};
        // }
    }

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    markAll() {
        this.selected = [];
        this.selected = this.rows;
        this.rows.forEach(row => {
            //this.selected = true;
            row.select = true;
        });
    }

    unmarkAll() {
        this.selected = [];
        this.rows.forEach(row => {
            // row.include = false;
            row.select = false;
        });
    }

    changeSelection(row:ActiveEmployeePostDatedPayRateModule, Indx: number){
        this.rows[Indx].active = !this.rows[Indx].active;
    }

}