import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { PredefineChecklist } from '../../_models/predefine-checklist/predefine-checklist.module';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { PredefineChecklistService } from '../../_services/predefine-checklist/predefine-checklist.service';
import { NgForm } from '@angular/forms';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';

@Component({
  selector: 'app-predefine-checklist',
  templateUrl: './predefine-checklist.component.html',
  styleUrls: ['./predefine-checklist.component.css'],
  providers: [PredefineChecklistService],
  
})
export class PredefineChecklistComponent implements OnInit {

    
  page = new Page();
  rows = new Array<PredefineChecklist>();
  temp = new Array<PredefineChecklist>();
  selected = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1427';
  moduleName;
  screenName;

  defaultFormValues: Array<any> = [];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  preDefItemDesc = {};
  searchKeyword = '';
  ddPageSize: number = 5;
  model: PredefineChecklist;
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  error: any = {isError:false,errorMessage:''};
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  isSubItemDeleteAction: boolean = false;
  typeValue: number;
  preDefCkLstItemDescVal:string;
  subItems = [];
  
   subItemsObj = {
        // "isDelete": false,
       "itemDesc": '',
       "itemDescArabic": ''
     }
  
  isSubItem = true;
  editRowId: number;
  isEdit: boolean = false;
  idx: number = 0;
  isDelete:boolean = false;
  isSubitemSubmited:boolean = true;



    // Initializing Checklist Type

    checklistTypes: DropDownModule[] = [
        {id: 1, name: 'Orientation'},
        {id: 2, name: 'Termination'}
        
    ];
  
  
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;

  
  constructor(private router: Router,
      private PredefineChecklistService: PredefineChecklistService,
      private getScreenDetailService: GetScreenDetailService,
      private alertService: AlertService) {
      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortOn = 'id';
      this.page.sortBy = 'DESC';

      // default form parameter for department  screen
      this.defaultFormValues = [

          { 'fieldName': 'PREDEFINEDCHECKLIST_SEARCH', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' }, 
          { 'fieldName': 'PREDEFINEDCHECKLIST_CHECKLIST_TYPE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_ITEM_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_ITEM_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_ACTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_DELETE_ROW_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PREDEFINEDCHECKLIST_CHECKBOX_LABEL', 'fieldValue': '', 'helpMessage': '' },
        
        ];

    }

  ngOnInit() {
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.currentLanguage = localStorage.getItem('currentLanguage'); 

     
      //getting screen labels, help messages and validation messages
      this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

          this.moduleName = data.result.moduleName
          this.screenName = data.result.dtoScreenDetail.screenName
          this.availableFormValues = data.result.dtoScreenDetail.fieldList;
          this.availableFormValues = data.result.dtoScreenDetail.fieldList;
          for (var j = 0; j < this.availableFormValues.length; j++) {
              var fieldKey = this.availableFormValues[j]['fieldName'];
              var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
              var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
              objDefault['fieldValue'] = objAvailable['fieldValue'];
              objDefault['helpMessage'] = objAvailable['helpMessage'];
              if (objAvailable['listDtoFieldValidationMessage']) {
                  objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
              }
            }
        });

    }
  //setting pagination
  setPage(pageInfo) {
    this.selected = []; // remove any selected checkbox on paging
       this.page.pageNumber = pageInfo.offset;
      if( pageInfo.sortOn == undefined ) {
          this.page.sortOn = this.page.sortOn;
      } else {
          this.page.sortOn = pageInfo.sortOn;
      }
      if( pageInfo.sortBy == undefined ) {
          this.page.sortBy = this.page.sortBy;
        } else {
          this.page.sortBy = pageInfo.sortBy;   
        }
      this.page.searchKeyword = '';
      this.PredefineChecklistService.getlist(this.page,this.searchKeyword).subscribe(pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
         
        console.log("Page Data", pagedData.data);
         
      });
    }

  // Open form for create location
  Create() {
      this.showCreateForm = false;
      this.isUnderUpdate = false;
      setTimeout(() => {
          this.showCreateForm = true;
          setTimeout(() => {
              window.scrollTo(0, 2000);
            }, 10);
        }, 10);
      this.model = {
          id: 0,
          preDefineCheckListType: null,
          preDefineCheckListItemDescription: '',
          preDefineCheckListItemDescriptionArabic: '',
          subItems:[]
            
        };
    }


  // Clear form to reset to default blank
  Clear(f: NgForm) {
      f.resetForm({preDefineCheckListType: null});
      
    }
  
  
  //function call for creating new Predefined Checklist
  CreatePredefinedCkLst(f: NgForm, event: Event) {
    this.isSubitemSubmited = true;
    if (f.invalid) {
          return;
      }
      event.preventDefault();
    //   var locIdx = this.model.preDefineCheckListItemDescription;

    console.log("aa:", this.model.subItems.itemDesc);
      
      

      //Check if the id is available in the model.
      //If id avalable then update the Predefined Checklist, else Add new Predefined Checklist.
      if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
          this.isConfirmationModalOpen = true;
          this.isDeleteAction = false;
        }
      
      else {
            //Call service api for Creating new Predefined Checklist
            this.PredefineChecklistService.createPredefinedCkLst(this.model).then(data => {
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    this.showCreateForm = false;
                    this.messageText = data.btiMessage.message;
                    
                    }, 100);

                this.hasMsg = true;
                f.resetForm();
                //Refresh the Grid data after adding new Predefined Checklist
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        } 
  
    }
      
  
  //edit department by row
  edit(row: PredefineChecklist) {
      this.showCreateForm = true;
      this.model = Object.assign({},row);
      this.isUnderUpdate = true;
      this.isDelete = true;
      this.preDefItemDesc = row.preDefineCheckListItemDescription;
      this.preDefCkLstItemDescVal = this.model.preDefineCheckListItemDescription;
      setTimeout(() => {
          window.scrollTo(0, 2000);
        }, 10);
      
    }

  updateStatus() {
      this.closeModal();
      console.log("aa:", this.model.subItems.itemDesc);
       //Call service api for updating  setup
      this.model.preDefineCheckListItemDescription = this.preDefCkLstItemDescVal;
      this.PredefineChecklistService.updatePredefinedCkLst(this.model).then(data => {
          var datacode = data.code;
          
          if (datacode == 201) {
              //Refresh the Grid data after editing department
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

              //Scroll to top after editing department
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                    }, 4000);
                  this.messageText = data.btiMessage.message;
                  this.showCreateForm = false;
                }, 100);

              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
                
        });
       
    }

  varifyDelete() {
      if (this.selected.length > 0) {
          this.isDeleteAction = true;
          this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


  //delete department by passing whole object of perticular Department
  delete() {
      if(this.idx > 0){

        this.deleteSubItem();

      }else{
        var predefinechecklists = [];
        for (var i = 0; i < this.selected.length; i++) {
            predefinechecklists.push(this.selected[i].id);
          }
        this.PredefineChecklistService.deletePredefinedCkLst(predefinechecklists).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                  }, 4000);
                this.message.text = data.btiMessage.message;
               }, 100);
  
            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  
          }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
          });
          this.closeModal();
        }
      
    }

    

  // default list on page
  onSelect({selected}) {
      
        if(selected.length !==0){
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        console.log(selected);

        }
    

    }  

    
  
  // search department by keyword
  updateFilter(event) {
      this.searchKeyword = event.target.value.toLowerCase();
      this.page.pageNumber = 0;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.table.offset = 0;
      
    }


  // Set default page size
  changePageSize(event) {
      this.page.size = event.target.value;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

  confirm(): void {
      this.messageText = 'Confirmed!';
      this.delete();
    }

  closeModal() {
      this.isDeleteAction = false;
      this.isConfirmationModalOpen = false;
    }

  
  CheckNumber(event) {
      if (isNaN(event.target.value) == true) {
          this.model.preDefineCheckListItemDescription = '';
          return false;
        }
    }


  sortColumn(val){
      if( this.page.sortOn == val ) {
          if( this.page.sortBy == 'DESC' ) {
              this.page.sortBy = 'ASC';
            } else {
              this.page.sortBy = 'DESC';
            }
        }
      this.page.sortOn = val;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
   
   
   
    addSubItem(subItemData: NgForm, subitem){
        this.isSubitemSubmited = false;
        
        console.log(subItemData);            
        this.subItemsObj.itemDesc = subitem.itemDesc;
        this.subItemsObj.itemDescArabic = subitem.itemDescArabic;
    
    
        this.model.subItems.push(Object.assign({}, this.subItemsObj));
   
    }

   


    varifySubDelete(obj,num) {
        this.idx = obj.id;
        let index = num;
        if(this.idx === undefined){
            this.model.subItems.splice(index, 1);
        }
        else if (this.idx > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
          } else {
              this.isSuccessMsg = false;
              this.hasMessage = true;
              this.message.type = 'error';
              this.isfailureMsg = true;
              this.showMsg = true;
              this.message.text = 'Please select at least one record to delete.';
              window.scrollTo(0, 0);
          }
    }

    deleteSubItem() {
        // this.varifySubDelete(id);
            let indx= this.idx;
            var predefinechecklists = [];
          if (indx==null) {
            this.model.subItems.pop();
            } else{
                predefinechecklists.push(indx);
            }
        this.PredefineChecklistService.deleteSubitemPredefinedCkLst(predefinechecklists).then(data => {
            this.model.subItems = data.result.deleteDtoOrientationCheckListSetup["0"].list;
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            
            this.hasMessage = true;
            this.message.type = 'success';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                  }, 4000);
                this.message.text = data.btiMessage.message;
               }, 100);
  
            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            // this.model.subItems = this.rows.

  
          }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
          });
          this.closeModal();

      }

    // deleteSubRow(id){
    //     this.varifySubDelete(id);
    //     let indx= id;
    //     var predefinechecklists = [];
    //   if (indx==null) {
    //     this.model.subItems.pop();
    //     } else{
    //         predefinechecklists.push(indx);
    //     }
    //   this.PredefineChecklistService.deleteSubitemPredefinedCkLst(predefinechecklists).then(data => {
    //       var datacode = data.code;
    //       console.log("AA:", datacode);
    //       if (datacode == 200) {
    //         this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    //     }
    //   });

    // }

    // private fieldArray: Array<any> = [];
    // private newAttribute: any = {};

    // addFieldValue() {
    //     this.fieldArray.push(this.newAttribute)
    //     this.newAttribute = {};
    // }

    // deleteFieldValue(index) {
    //     this.fieldArray.splice(index, 1);
    // }
}
