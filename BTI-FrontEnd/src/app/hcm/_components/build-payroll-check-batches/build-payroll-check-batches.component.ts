import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BuildPayrollCheckBatch } from '../../_models/build-payroll-check-batches/build-payroll-check-batches';
import { BuildPayrollCheckBatchService } from '../../_services/build-payroll-check-batches/build-payroll-check-batches.service';
import { BuildCheckStorageService } from '../../../_sharedresource/_services/build-check-storage.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { arrayUnion } from 'angular2-modal';
import { BuildCheckCommonService } from '../../../_sharedresource/_services/build-check-common.service';

@Component({
    selector: 'buildPayrollCheckBatches',
    templateUrl: './build-payroll-check-batches.component.html',
    providers: [BuildPayrollCheckBatchService, BuildCheckStorageService]
})

// export Batches component to make it available for other classes
export class BuildPayrollCheckBatchesComponent {
    page = new Page();
    rows = new Array<BuildPayrollCheckBatch>();
    temp = new Array<BuildPayrollCheckBatch>();
    selected = new Array<BuildPayrollCheckBatch>();
    moduleCode = "M-1011";
    screenCode = "S-1464";
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    batchId = {};
    searchKeyword = '';
    getBatch: any[] = [];
    ddPageSize: number = 5;
    model: BuildPayrollCheckBatch;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    statusArr = new Array('New', 'Busy', 'Clear', 'Posting');
    batchRow = [];
    userName = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    matchUserId: any[] = [];
    selectedBatches: any[] = [];
    checkedDisabled: any[] = [];
    markAndUnmarkedArray: any[] = [];
    disabled: boolean = false;
    loaderImg: boolean = false;

    constructor(
        private router: Router,
        private buildPayrollCheckBatchService: BuildPayrollCheckBatchService,
        private buildCheckStorageService: BuildCheckStorageService,
        private getScreenDetailService: GetScreenDetailService,
        private buildCheckCommonService: BuildCheckCommonService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_MARK_ALL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_UNMARK_ALL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_BATCH_ID_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_BATCH_DESCRIPTION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_BATCH_STATUS_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_USER_ID_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_OK_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_SEARCH_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_PAYROLL_CHECK_BATCHES_SELECT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_POST_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_CREATE_TITLE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_UPDATE_BATCH_LABEL', 'fieldValue': '', 'helpMessage': '' },
        ];
        if (JSON.parse(localStorage.getItem('model')) == null)
            this.router.navigate(['hcm/buildChecks']);
    }

    // Screen initialization
    ngOnInit() {
        this.buildCheckCommonService.pathFromBCSBs = true;
        if (this.buildCheckCommonService.modelData == null) {
            window.location.replace('hcm/buildChecks');
        }
        let model1 = JSON.parse(localStorage.getItem('model'));
        this.model = {
            id: this.buildCheckCommonService.modelData['id'],
            batchId: null,
            description: '',
            userId: '',
            status: null,
            include: false
        };

        this.initTables({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }
    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }

    //function call for creating new department
    CreateBuildPayrollCheckBatches(f: NgForm, event: Event) {
        event.preventDefault();
        if (this.selected.length == 0) {
            window.scrollTo(0, 0);
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = "Please select at least one record.";
            }, 100);
            return;
        }
        //Check if the id is available in the model.
        //If id avalable then update the batches
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            //Call service api for Creating new batch
            this.selectedBatches = [];
            // console.log('this.selected', this.selected)
            for (var i = 0; i < this.selected.length; i++) {
                let createPayload = {
                    id: this.selected[i]["batchPrimaryId"]
                }
                this.selectedBatches.push(createPayload);
            }
            // console.log('this.selectedBatches', this.selectedBatches)
            let batchId: any = [];
            this.selectedBatches.filter(row => {
                console.log(row);
                batchId.push(row.id)
            })
            console.log('batchId', batchId)
            console.log('this.selectedBatches', this.selectedBatches);
            this.loaderImg = true;
            this.buildPayrollCheckBatchService.createPayrollCheckBatches(this.model.id, this.selectedBatches).then(data => {
                //this.buildPayrollCheckBatchService.batchMarkUnmark(batchId).then(data => {
                    // console.log('DATA', data)
                //})
                // this.buildPayrollCheckBatchService.batchNewApi(batchId).then(data => {
                    // console.log('DATA', data)
                // })
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMessage = false;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                            this.router.navigate(['hcm/buildChecks']);
                        }, 1500);
                        this.showCreateForm = false;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    this.loaderImg = false;
                    this.hasMsg = true;
                    f.resetForm();
                    //Refresh the Grid data after adding new department
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }
            }).catch(error => {
                this.hasMsg = true;
                this.loaderImg = false;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedBatches = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedBatches.push(this.selected[i].id);
        }
        this.buildPayrollCheckBatchService.deleteBatch(selectedBatches).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = "success";
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);

    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.initTables({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy, size: this.page.size });
        // this.table.offset = 0;
        // this.buildPayrollCheckBatchService.searchBatches(this.buildCheckCommonService.modelData['id'],this.page, this.searchKeyword).then(pagedData => {
        //     this.rows = [];           
        //     this.page.pageNumber = pagedData.result.pageNumber;
        //     this.page.sortBy = pagedData.result.sortBy;
        //     this.page.sortOn = pagedData.result.sortOn;
        //     this.page.totalElements = pagedData.result.totalCount;
        //     this.rows = pagedData.result.records;            
        //     if (this.rows.length) {
        //         this.selected = [];
        //         for (let i = 0; i < this.rows.length; i++) {
        //             if (this.rows[i]['buildId'] != null) {
        //                 this.selected.push(this.rows[i]);
        //             }
        //         }
        //     }
        // });
    }

    changePageSize(event) {
        this.page.size = Number(event.target.value);
        // this.setPage({ offset: 0, ddPageSize: Number(event.target.value) });
        this.initTables({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    setPage(pageInfo) {
        this.page.pageNumber = pageInfo.offset;
        this.page.size = pageInfo.ddPageSize;
    }
    Cancel() {
        this.router.navigate(['hcm/buildChecks']);
    }

    initTables(pageInfo) {
        // this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        let getUserId = [];
        console.log('this.buildCheckCommonService.modelData[id]', this.buildCheckCommonService.modelData)
        this.disabled = this.buildCheckCommonService.modelData.disable;
        this.buildPayrollCheckBatchService.getAll(this.buildCheckCommonService.modelData['id'], this.page, this.searchKeyword).then(res => {
            this.rows = [];
            this.page.pageNumber = res.result.pageNumber;
            this.page.sortBy = res.result.sortBy;
            this.page.sortOn = res.result.sortOn;
            this.page.totalElements = res.result.totalCount;
            this.rows = res.result.records;
            console.log('this.rows', this.rows)
            if (this.rows.length) {
                this.checkedDisabled = [];
                for (let i = 0; i < this.rows.length; i++) {
                    if (this.rows[i]['status'] == 2 || this.rows[i]['status'] == 1) {
                        this.selected.push(this.rows[i]);
                    }
                    if (this.rows[i]['status'] == 2) {
                        this.checkedDisabled.push(true);
                    } else {
                        this.checkedDisabled.push(false);
                    }
                }

                this.markAndUnmarkedArray = this.selected;
            }

            this.buildPayrollCheckBatchService.getUser().then(data => {
                let user = data.result;
                console.log('user', user);
                this.matchUserId = [];
                for (let i = 0; i < this.rows.length; i++) {
                    for (let j = 0; j < user.length; j++) {
                        if (user[j]['udId'] != undefined) {
                            if (this.rows[i]['userID'] == user[j]['udId']) {
                                this.matchUserId[this.rows[i]['userID']] = (user[j]['firstName']);
                            }
                        }
                    }
                }
                console.log('this.matchUserId', this.matchUserId);
            })
        })
    }

    //edit department by row
    edit(row: BuildPayrollCheckBatch) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.batchId = row.batchId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    markAll() {
        if (this.rows.length) {
            this.selected = [];
            this.selected = this.rows;
            // for(let i = 0; i < this.checkedDisabled.length; i++){
            //     if(this.checkedDisabled[i] == false){
            //         this.selected.push(this.rows[i]);
            //     }
            // }
        }
        // this.selected = [];
        // this.selected = this.rows;
        // this.selected.forEach(row => {
        //     row.include = true;
        // });
        window.scrollTo(0, 0);
        this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
            this.messageText = "Build Payroll Check-Batched marked successfully.";
        }, 100);
    }

    unmarkAll() {
        if (this.rows.length) {
            this.selected = [];
            for (let i = 0; i < this.rows.length; i++) {
                if (this.checkedDisabled[i] == true) {
                    this.selected.push(this.rows[i]);
                }
            }
        }
        window.scrollTo(0, 0);
        this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
            this.messageText = "Build Payroll Check-Batched unmarked successfully.";
        }, 100);
    }

    displayCheck() {
        console.log("displayHHHHHHHHH");
        return false;
        // rows.forEach((r) => {
        //     if(r.status === 1){
        //         console.log('11111', r);
        //         this.checkboxIsChecked = true;
        //     } else {
        //         this.checkboxIsChecked = false;
        //     }
        // });
    }
}