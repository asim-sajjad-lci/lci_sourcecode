import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { BuildCheckPayrollCodes } from '../../_models/build-check-payroll-codes/build-check-payroll-codes.module';
import { BuildCheckStorageService } from '../../../_sharedresource/_services/build-check-storage.service';
import { BuildPayrollCheckBenefitsService } from '../../_services/build-payroll-check-benefits/build-payroll-check-benefits.service';
import * as cloneDeep from 'lodash/cloneDeep';
import { BuildCheckCommonService } from '../../../_sharedresource/_services/build-check-common.service';

@Component({
  selector: 'buildPayrollCheckBenefits',
  templateUrl: './build-payroll-check-benefits.component.html',
  styleUrls: ['./build-payroll-check-benefits.component.css'],
  providers: [BuildPayrollCheckBenefitsService, BuildCheckStorageService]
})
export class BuildPayrollCheckBenefitsComponent implements OnInit {
  page = new Page();
  pageType = new Page();
  applicableRows = new Array<BuildCheckPayrollCodes>();
  includedRows = new Array<BuildCheckPayrollCodes>();
  temp = new Array<BuildCheckPayrollCodes>();
  selectedCode = [];
  selectedMaster = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1467';
  moduleName;
  screenName;
  defaultFormValues: Array<any> = [];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  locationId = {};
  searchKeyword = '';
  ddPageSize: number = 5;
  model: Object;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  seqn: number;
  modelReasonDesc;
  reasonDescVal;
  reasonIndx: number;
  reasonDesc: string;
  atteandanceTypeIndx: number;
  atteandanceTypeSeqn: number;
  atteandanceTypeDesc: string;
  isActive: boolean = false;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  locationIdvalue: number;
  disabled: boolean = false;
  isType: boolean = false;
  deductionId: any[] = [];

  pattern = /*/^\d*\.?\d{0,3}$/*/ /^[\d]*$/;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;
  selectedRadiobutton: boolean;

  constructor(private router: Router,
    private buildPayrollCheckBenefitsService: BuildPayrollCheckBenefitsService,
    private buildCheckStorageService: BuildCheckStorageService,
    private getScreenDetailService: GetScreenDetailService,
    private buildCheckCommonService: BuildCheckCommonService,
    private alertService: AlertService) {

    if (JSON.parse(localStorage.getItem('model')) == null)
      window.location.replace('hcm/buildChecks');
    this.page.pageNumber = 0;
    this.page.size = 10;
    // default form parameter for department  screen
    this.defaultFormValues = [
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_INCLUDE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_NONE_RADIO_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_ALL_RADIO_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_SELECTED_RADIO_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_AVAILABLE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_SELECT_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_BENEFIT_CODE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_INSERT_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_REMOVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'BUILD_PAYROLL_CHECK_BENEFITS_OK_LABEL', 'fieldValue': '', 'helpMessage': '' },
    ];
  }

  ngOnInit() {
    this.currentLanguage = localStorage.getItem('currentLanguage')
    if (this.buildCheckCommonService.modelData == null) {
      window.location.replace('hcm/buildChecks');
    };
    let model1 = JSON.parse(localStorage.getItem('model'));
    this.buildCheckCommonService.pathFromBCIB = true;
    this.model = {
      id: this.buildCheckCommonService.modelData['id'],
      include: 0
    };
    this.disabled = this.buildCheckCommonService.modelData.disable;
    //getting screen labels, help messages and validation messages
    this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

      this.moduleName = data.result.moduleName
      this.screenName = data.result.dtoScreenDetail.screenName
      this.availableFormValues = data.result.dtoScreenDetail.fieldList;
      for (var j = 0; j < this.availableFormValues.length; j++) {
        var fieldKey = this.availableFormValues[j]['fieldName'];
        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
        var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
        objDefault['fieldValue'] = objAvailable['fieldValue'];
        objDefault['helpMessage'] = objAvailable['helpMessage'];
        if (objAvailable['listDtoFieldValidationMessage']) {
          objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
        }
      }
    });
    this.initTables({ offset: 0 });
  }

  Cancel() {
    this.router.navigate(['hcm/buildChecks']);
  }

  initTables(pageInfo) {
    //debugger;
    this.selectedCode = [];
    this.selectedMaster = [];
    this.includedRows = [];
    this.applicableRows = [];
    this.deductionId = [];

    // remove any selected checkbox on paging
    let startDate: any = this.buildCheckCommonService.modelData['dateFrom']
    let endDate: any = this.buildCheckCommonService.modelData['dateTo']
    this.page.pageNumber = pageInfo.offset;
    this.buildPayrollCheckBenefitsService.getlist(this.model["id"], startDate, endDate).subscribe(data => {
      data.forEach(row => {
        if (row.buildChecksId == this.model["id"])
          this.includedRows.push(row);
        else
          this.applicableRows.push(row);
      });
      this.temp = cloneDeep(this.includedRows);
      this.setPage({ offset: 0 });
    });
  }

  /* changePageSize(event) {
      this.page.size = event.target.value;
      this.page.pageNumber=0;
  } */

  setPage(pageInfo) {
    this.applicableRows = this.applicableRows.sort((a, b) => this.sortComparator(a.codeId, b.codeId));
    this.page.pageNumber = pageInfo.offset;

  }

  onOptionChange() {
    if (this.model["include"] == 1) {
      this.includedRows = this.temp.slice(0, this.temp.length);
      //this.includedRows.push(...this.temp);
      this.none();
    }
    else if (this.model["include"] == 2) {
      this.includedRows = this.temp.slice(0, this.temp.length);
      //this.includedRows.push(...this.temp);
      this.all();
    }
    /* else if (this.model["include"] == 3) {
      this.includedRows = this.includedRows.filter(row => row.include);
    } */
  }

  changeSelection(row: Object, Indx: number) {
    this.applicableRows[Indx].include = !this.applicableRows[Indx].include;
    if (this.applicableRows[Indx].include) {
      this.selectedCode.push(row);
    }
    else {
      let idx = this.selectedCode.indexOf(row);
      this.selectedCode.splice(idx, 1);
    }
    if (this.model['include'] != 3) {
      //this.includedRows = this.includedRows.filter(row => row.include);
      this.model["include"] = 0;
    }
  }

  changeSelection2(row: Object, Indx: number) {
    this.selectedRadiobutton = true;
    this.selectedCode = [];
    this.includedRows[Indx].include = !this.includedRows[Indx].include;
    if (this.includedRows[Indx].include) {
      this.selectedMaster.push(row);
    }
    else {
      let idx = this.selectedMaster.indexOf(row);
      this.selectedMaster.splice(idx, 1);
    }
    this.model["include"] = 3;
    // if (this.model['include'] != 3) {
    //   //this.includedRows = this.includedRows.filter(row => row.include);
    //   this.model["include"] = 0;
    // }
  }

  selectAllInclude() {
    this.selectedRadiobutton = true;
    this.selectedCode = [];
    if (this.includedRows.length == this.selectedMaster.length) {
      this.includedRows.forEach(row => {
        row.include = false;
      });
      this.selectedMaster = [];
    }
    else {
      this.selectedMaster = [];
      this.includedRows.forEach(row => {
        this.selectedMaster.push(row);
        row.include = true;
      });

    }
    this.model["include"] = 3;
  }

  selectAllApp() {
    if (this.applicableRows.length == this.selectedCode.length) {
      this.applicableRows.forEach(row => {
        row.include = false;
      });
      this.selectedCode = [];
    }
    else {
      this.selectedCode = [];
      this.applicableRows.forEach(row => {
        this.selectedCode.push(row);
        row.include = true;
      });

    }
  }

  all() {
    //this.selectedMaster = [];
    this.selectedCode = [];
    /* this.includedRows.forEach(row => {
      this.selectedMaster.push(row);
      row.include = true;
    }); */
    this.applicableRows.forEach(row => {
      this.selectedCode.push(row);
      row.include = true;
    });
  }

  none() {
    this.selectedMaster = [];
    this.selectedCode = [];
    this.includedRows.forEach(row => {
      row.include = false;
    });
    this.applicableRows.forEach(row => {
      row.include = false;
    });
  }

  insert() {
    this.setPage({ offset: 0 });
    window.setTimeout(() => {
      this.selectedCode.forEach(row => {
        this.applicableRows = this.applicableRows.filter(item => item["codeId"] != row["codeId"]);
        row.include = false;
      });
      this.applicableRows.slice(0, this.applicableRows.length);
      this.temp.push(...this.selectedCode);
      this.includedRows = this.temp.slice(0, this.temp.length);
      this.selectedCode = [];

    }, 100);

  }

  remove() {
    console.log('this.selectedMaster', this.selectedMaster);
    this.selectedMaster.forEach(row => {
      row.buildChecksId ? this.deductionId.push(row.id) : null;
    });
    this.selectedMaster.forEach(row => {
      this.temp = this.temp.filter(item => item["codeId"] != row["codeId"]);
      row.include = false;
    });
    this.includedRows = this.temp.slice(0, this.temp.length);
    this.applicableRows.push(...this.selectedMaster);
    this.selectedMaster = [];
    this.setPage({ offset: 0 });
  }


  CreateBenefitCodesSetup(f: NgForm, event: Event) {
    let toSend = [];
    switch (this.model['include']) {
      case 1:
        toSend = [];
        this.deductionId = [];
        break;

      case 2:
        toSend.push(...this.includedRows);
        toSend.push(...this.applicableRows);
        this.deductionId.push(...this.includedRows);
        this.deductionId.push(...this.applicableRows);
        break;

      case 3:
        console.log('this.includedRows', this.includedRows);
        this.includedRows.filter(row => {
          if (row.include) {
            toSend.push(row);
            this.deductionId.push(row.id);
          }
          //  else if (row.buildChecksId) {
          //     this.deductionId.push(row.id);
          // }
        });
        console.log('this.deductionId', this.deductionId)
        // toSend = this.includedRows.filter(row => row.include);
        if (toSend.length == 0) {
          window.scrollTo(0, 0);
          this.hasMsg = true;
          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
            }, 4000);
            this.messageText = "Please select at least one Deduction Code.";
          }, 100);
          return;
        }
        break;

      default:
        toSend = this.includedRows;
        break;
    }
    if (this.model['include'] == 0) {
      window.scrollTo(0, 0);
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
        this.messageText = "Please select at least one Include.";
      }, 100);
      return;
    }
    console.log('this.model', this.model);
    let payload = {
      defaultId: this.model["id"],
      deductionId: this.deductionId
    }
    console.log('this.deductionId', this.deductionId);
    this.buildPayrollCheckBenefitsService.createBenefitCode(this.model["id"], toSend).then(data => {
      var datacode = data.code;
      this.buildPayrollCheckBenefitsService.deleteTransBenefit(payload).subscribe(data => {
        console.log('DATA', data);
      });
      if (datacode == 201) {
        window.scrollTo(0, 0);
        window.setTimeout(() => {
          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
            this.router.navigate(['hcm/buildChecks']);
          }, 1500);
          this.messageText = data.btiMessage.message;
        }, 100);

        this.hasMsg = true;

        //Refresh the Grid data after adding new department
        this.initTables({ offset: 0 });
      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = "Server error. Please contact admin.";
      }, 100)
    });


  }

  /*onEndDateChanged(event: IMyDateModel): void {
      this.lastaccruedDate = event.jsdate;
  }*/
  // default list on page
  onSelectInsert({ selected }) {
    this.selectedCode.splice(0, this.selectedCode.length);
    this.selectedCode.push(...selected);
  }

  onSelectRemove({ selected }) {
    //console.log(selected) ;             
    this.selectedMaster.splice(0, this.selectedMaster.length);
    this.selectedMaster.push(...selected);
  }

  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }

  sortComparator(a: string, b: string): number {
    if (a > b) {
      return 1;
    } else if (a < b) {
      return -1;
    }
    else
      return 0;
  }
}
