import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildPayrollCheckBenefitsComponent } from './build-payroll-check-benefits.component';

describe('BuildPayrollCheckBenefitsComponent', () => {
  let component: BuildPayrollCheckBenefitsComponent;
  let fixture: ComponentFixture<BuildPayrollCheckBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildPayrollCheckBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildPayrollCheckBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
