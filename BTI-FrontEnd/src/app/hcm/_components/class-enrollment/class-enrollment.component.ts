import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
//import { ClassSkill } from '../../_models/class-skill/class-skill';
import { ClassSkillService } from '../../_services/class-skill/class-skill.service';
import { TrainingBatchSignupService } from '../../_services/training-batch-signup/training-batch-signup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'class-enrollment',
    templateUrl: './class-enrollment.component.html',
    styleUrls: ['./class-enrollment.component.css'],
    providers: [ClassSkillService,TrainingBatchSignupService,CommonService]
})

// export to make it available for other classes
export class ClassEnrollmentComponent {
    page = new Page();
    rows = [];
    rowsEmployeeTraining = [];
    temp = [];
    selected = [];
    selectedEmployeeTraining = [];
    moduleCode = "M-1011";
    screenCode = "S-1440";
    moduleName;
    screenName;
    defaultFormValues = [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    trainingBatchSignupId = {};
    searchKeyword = '';
    ddPageSize = 5;
    model;
    modelEmployee: any;
    cityOptions = [];
    showCreateForm: boolean = false;
    isSuccessMsg;
    isfailureMsg;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText;
    emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}";
    //emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
    isModalPopUpActive = false;
    currentLanguage: any;
    isConfirmationModalOpen: boolean = false;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    trainingBatchSignupIdvalue: string;
    AllTraningCourseId = [];
    AllTrainingClass = [];
    AllSkillSetSetup = [];
    AllEmployeeMaster = [];
    AllSkillSet = [];
    dataSource: Observable<any>;
    trainingBatchSignupIdSearch = [];
    typeaheadLoading: boolean;
    EmployeeTrainingWindow: boolean = false;
    SelectedSkillSets = [];
    modelCompleteDate;
    frmCompleteDate;
    startCompleteDate;
    getClass:any=[];
    modelcompleteDate;
    updatecompleteDate;
    obj :any = {};
    zoomid:any;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

            OK:any;
            CURRENTLY_ENROLLED:any;
            CURRENTLY_COMPLETED:any;
            ALL:any;
            REDISPLAY:any;
            ZOOM:any;
            DESCRIPTION:any;
            EMPLOYEE_NAME:any;
            EMPLOYEE_ID:any;
            TRAINING_ID:any;
            CLASS_DESCRIPTION:any;
            CLASS_ID:any;
            ACTION:any;
            SELECT:any;
            CREATE:any;
            TRAINING_COURSE_AND_CLASS_DEFINCATION_SETUP_EMPLOYEE_TRAINING:any;
            TRAINING_DESCRIPTION:any;
            COMPLETE:any;
            DATE_COMPLETED:any;
            COMMENTS:any;
            COMPLETED:any;
            DELETE_ROW:any;
            DELETE:any;
            SAVE:any;
            CLEAR:any;
            UPDATE:any;

    constructor(
        private router: Router,
        private classSkillService: ClassSkillService,
        private trainingBatchSignupService: TrainingBatchSignupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,private commonService:CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for trainingBatchSignup  screen
        this.defaultFormValues = [
            { 'fieldName': 'OK', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CURRENTLY_ENROLLED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CURRENTLY_COMPLETED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ALL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'REDISPLAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ZOOM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRAINING_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SELECT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },

            { 'fieldName': 'TRAINING_COURSE_AND_CLASS_DEFINCATION_SETUP_EMPLOYEE_TRAINING', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRAINING_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COMPLETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_COMPLETED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COMMENTS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COMPLETED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE_ROW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
        ];

        this.OK=this.defaultFormValues[0];
        this.CURRENTLY_ENROLLED=this.defaultFormValues[1];
        this.CURRENTLY_COMPLETED=this.defaultFormValues[2];
        this.ALL=this.defaultFormValues[3];
        this.REDISPLAY=this.defaultFormValues[4];
        this.ZOOM=this.defaultFormValues[5];
        this.DESCRIPTION=this.defaultFormValues[6];
        this.EMPLOYEE_NAME=this.defaultFormValues[7];
        this.EMPLOYEE_ID=this.defaultFormValues[8];
        this.TRAINING_ID=this.defaultFormValues[9];
        this.CLASS_DESCRIPTION=this.defaultFormValues[10];
        this.CLASS_ID=this.defaultFormValues[11];
        this.ACTION=this.defaultFormValues[12];
        this.SELECT=this.defaultFormValues[13];
        this.CREATE=this.defaultFormValues[14];
        this.TRAINING_COURSE_AND_CLASS_DEFINCATION_SETUP_EMPLOYEE_TRAINING=this.defaultFormValues[15];
        this.TRAINING_DESCRIPTION=this.defaultFormValues[16];
        this.COMPLETE=this.defaultFormValues[17];
        this.DATE_COMPLETED=this.defaultFormValues[18];
        this.COMMENTS=this.defaultFormValues[19];
        this.COMPLETED=this.defaultFormValues[20];
        this.DELETE_ROW=this.defaultFormValues[21];
        this.DELETE=this.defaultFormValues[22];
        this.SAVE=this.defaultFormValues[23];
        this.CLEAR=this.defaultFormValues[24];
        this.UPDATE=this.defaultFormValues[25];

    }

    // Screen initialization
    ngOnInit() {    
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');


        //getting screen
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        this.trainingBatchSignupService.getAllTraningCourse(this.page, this.searchKeyword).then(data => {
            this.AllTraningCourseId = data.result.records;
           //console.log(data.result);
        });
        this.trainingBatchSignupService.getTrainingCourse(this.page, this.searchKeyword).then(data => {
            this.AllTrainingClass = data.result.records;
           //console.log(this.AllTrainingClass);
        });
        this.trainingBatchSignupService.getAllEmployeeMaster().then(data => {

            this.AllEmployeeMaster = data.result;
            
           //console.log(this.AllEmployeeMaster);

        });
    }
    // Exiting Id Search on every click
    getReportClassEnrollmentAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.trainingBatchSignupIdSearch.filter((id: any) => {
                return query.test(id.trainingBatchSignupId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
       //console.log('Selected value: ', e.value);
    }

    //setting pagination
    setPage(pageInfo) {

        this.model = {
            trainingCourse:{
                id:0
            },
            desc:'',
            filter:'all',
        }
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.trainingBatchSignupService.getBatchSighnUpFromTrainingId(this.page, this.model.trainingCourse.id).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        
           //console.log(this.page);
           //console.log( pagedData);
           //console.log(this.rows);

        });

        // this.trainingBatchSignupService.getClassIdByTrainingId(this.modelEmployee.trainingClass.id).then(data=>{
        //     this.getClass = data.result.records.subItems;
        //     console.log('subitems',this.getClass);
        // })
    }
    //setting pagination
    setPageEmployeeTraining(pageInfo) {
        //debugger;        
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
       //console.log(this.selected);
       
        this.page = {
            size:  5,                
            totalElements:  0,                
            totalPages:  0,                
            pageNumber:  0,
            sortOn:  '',
            sortBy:  'DESC',
            searchKeyword:  ''
        };
        this.rowsEmployeeTraining = [];
        this.trainingBatchSignupService.getTraningBatchSighUpByClassId(this.page, pageInfo.id).subscribe(pagedData => {
           
            
                this.page = {
                    size:  5,                
                    totalElements:  1,                
                    totalPages:  1,                
                    pageNumber:  1,
                    sortOn:  '',
                    sortBy:  'DESC',
                    searchKeyword:  ''
                };
                this.rowsEmployeeTraining = pagedData;
            
           //console.log(this.rowsEmployeeTraining);
        });

        
    }

    

   
    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
       
    }
    // default list on page
    onSelectEmployeeTraining({ selected }) {
        this.selectedEmployeeTraining.splice(0, this.selectedEmployeeTraining.length);
        this.selectedEmployeeTraining.push(...selected);
       
    }



    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

   

   

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    moveToSelected() {

        this.SelectedSkillSets = [];
        this.AllSkillSet.forEach(element => {
            if (element['isSelected']) {
                this.AllSkillSet.pop();
                element['isSelected'] = false;
                this.SelectedSkillSets.push(element)
            }
        });
    }

    moveToAllEmployee() {
        this.SelectedSkillSets.forEach(element => {
            if (element['isSelected']) {
                this.SelectedSkillSets.pop();
                element['isSelected'] = false;
                this.AllSkillSet.push(element)
            }
        });

    }

    onChangeTrainingCourse(event){
        for (let index = 0; index < this.AllTraningCourseId.length; index++) {
            const element = this.AllTraningCourseId[index];
            if(event.target.value == element.id){
                this.model.desc = element.desc;
                
            }
            
        }
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });


    }
    changeTraningCourseId(event){
        for (let index = 0; index < this.AllTraningCourseId.length; index++) {
            const element = this.AllTraningCourseId[index];
            if(element.id == event.target.value){
             
               //console.log(element);
               //console.log(this.getClass);
                //this.AllTrainingClass

                this.trainingBatchSignupService.getTrainingCourseDetailTraningId(element.id).subscribe(data => {
                   //console.log(data.records);
                    this.getClass =data.records;
                    
                });

            }
            
        }
    }
    onFilterChange(){
       //console.log(this.model)
        let self = this;
        let status = 0;
        if(this.model.filter == "currentcompleted"){
            status = 2;
        }else if(this.model.filter == "currentenrolled"){
            status = 1;
        }
       
        this.trainingBatchSignupService.getTraningIdByStatus(this.page,status,this.model.trainingCourse.id).subscribe(pagedData => {           
            this.page = pagedData.page;
            this.rows = pagedData.data; 
           //console.log(this.rows);          

        });
    }
    zoom(){
        if( this.selected.length === 1){
            this.EmployeeTrainingWindow = true;
           //console.log('selected',this.selected);
            this.zoomid = this.selected[0].id;
            //Refresh the Grid data after deletion of traningCourse
            this.setPageEmployeeTraining({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy ,id:this.selected[0].id});
            this.trainingBatchSignupService.getAllEmployeeMaster().then(data => {
                this.AllEmployeeMaster = data.result;                
               //console.log(this.AllEmployeeMaster);
    
            });
        }else{            
            this.isSuccessMsg = false;
            this.hasMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = "Only one record can be select for zoom feature.";
        }
        

       
       
    }
    refreshClassEnrolment(){
        this.model = {
            trainingCourse:{
                id:0
            },
            desc:'',
            filter:'all',
        }
        this.rows = [];
        //this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
       
    }
    clear(f: NgForm){
        //this.EmployeeTrainingWindow = false;
        //this.showCreateForm = false;
        f.resetForm({
           
            trainingClassStatus:1,
            

        });
    }
    deleteEmployeeTraining(){
        console.log(' deleteEmployeeTraining')
    }
    createEmployeeTraining() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 600);
            }, 10);
        }, 10);
        this.modelEmployee = {
            selectedEmployes:'',
            completeDate:'',
            courseComments:'',
            trainingClassStatus:1,
            employeeMaster : [{
                employeeId : 0
            }],            
            trainingClass : {
                id : 0
            },
            traningCourse : [{
                id : 0
            }]
        }
    }
     //function call for creating new EmployeeTraining
     CreateEmployeeTraining(f: NgForm, event: Event) {
        event.preventDefault();
        var divIdx = this.modelEmployee.id;
       // this.modelEmployee.id=this.zoomid;
       //console.log('fdf',this.modelEmployee.id);
       //console.log('selected',this.selected);
      
        this.modelEmployee.completeDate = this.frmCompleteDate ? this.startCompleteDate : this.updatecompleteDate;
      //console.log(this.frmCompleteDate);
       ;
      //console.log(this.startCompleteDate);
       
       
        //Check if the id is available in the modelEmployee.
        //If id avalable then update the EmployeeTraining, else Add new EmployeeTraining.
        if (this.modelEmployee.id > 0 && this.modelEmployee.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {

            //Call service api for Creating new EmployeeTraining
            this.trainingBatchSignupService.createTrainingBatchSignup(this.modelEmployee).then(data => {

                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.showCreateForm = false;
                        this.SelectedSkillSets = [];
                        this.AllSkillSet = this.AllSkillSetSetup;
                        this.messageText = data.btiMessage.message;;
                    }, 100);

                    this.hasMsg = true;

                    f.resetForm();

                    //Refresh the Grid data after adding new trainingBatchSignup
                    this.setPageEmployeeTraining({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });



       }
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
    };
    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
	onStartDateChanged(event: IMyDateModel): void {
        this.frmCompleteDate = event.jsdate;
        
        this.startCompleteDate = event.formatted;
        
        
    }

    updateTrainingClassStatus(checked){
       //console.log(checked);
        if(checked){
            this.modelEmployee.trainingClassStatus = 2;
        }else{
            this.modelEmployee.trainingClassStatus = 1;
        }
       //console.log(this.modelEmployee);
    }
     //edit traningCourse by row
     edit(row) {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.setPageEmployeeTraining({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
       //console.log(row);
        this.showCreateForm = true;
        this.obj = this.rows[0];
       //console.log('obj',this.obj);
        this.updatecompleteDate = row.completeDate;
        this.modelEmployee = Object.assign({},row);
        this.modelCompleteDate =new Date(this.modelEmployee.completeDate);
        if(this.modelEmployee.completeDate!=null){
        this.modelEmployee.completeDate =this.formatDateFordatePicker(new Date(this.modelEmployee.completeDate));
        }
       
       //console.log(row);
       //console.log('modelemployee',this.modelEmployee)
        if(!row || !row.trainingClass){
            this.modelEmployee.trainingClass = {
                id : 0
            }; 
        }
        if(!row || !row.employeeMaster || row.employeeMaster.length == 0){
            this.modelEmployee.employeeMaster = [{
                employeeId : 0
            }]; 
        }
        if(!row || !row.traningCourse){
            this.modelEmployee.traningCourse = [{
                id : this.modelEmployee &&  this.modelEmployee.traningCourse.id ? this.modelEmployee.traningCourse.id : 0,

              
            }];            
        }
      //  console.log(this.modelEmployee);
        
            
        this.isUnderUpdate = true;
        if(this.modelEmployee.traningCourse.id){
            this.trainingBatchSignupService.getClassIdByTrainingId(this.modelEmployee.traningCourse.id).then(data=>{
                this.getClass = data.result.records;    
               //console.log('subitems',this.getClass);
            });
        }
       
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    markAsComplete(data,checked){
        
      //console.log(data.completed);
      //console.log(checked);
       
       if(checked){
        data.trainingClassStatus = 2;
       }else{
        data.trainingClassStatus = 1;
       }
       if(data.employeeMaster.length == 0){
            data.employeeMaster = [{
                employeeId : 0
            }];          
        
        }
        if(!data.dtoTrainingCourseDetail){
            data.dtoTrainingCourseDetail = [{
                id : data.trainingCoursedetail.id ? data.trainingCoursedetail.id : 0
            }];            
        }
       //console.log(data);
        let PostData = {
            "id":data.id,
            "selectedEmployes":data.selectedEmployes,
            "completeDate":data.completeDate,
            "courseComments":data.courseComments,
            "trainingClassStatus":data.trainingClassStatus,
            "employeeMaster" :data.employeeMaster,            
            "trainingClass" : data.trainingClass,
            "dtoTrainingCourseDetail" : data.dtoTrainingCourseDetail,
            "trainingCoursedetail" : data.trainingCoursedetail
        }

       //console.log(PostData);
         //Call service api for updating selected traningCourse      
         this.trainingBatchSignupService.updateTrainingBatchSignup(PostData).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                              
            }
        }).catch(error => {
           
        });
    }
    updateStatus() {
        this.closeModal();
       //console.log(this.modelEmployee);
       
        // if(this.modelEmployee.trainingClassStatus){
        //     this.modelEmployee.trainingClassStatus = 2;
        // }else{
        //     this.modelEmployee.trainingClassStatus = 1;
        // }
        //Call service api for updating selected traningCourse      
        this.trainingBatchSignupService.updateTrainingBatchSignup(this.modelEmployee).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing traningCourse
                //this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.setPageEmployeeTraining({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                //Scroll to top after editing traningCourse
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selectedEmployeeTraining.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }
    


    //delete traningCourse by passing whole object of perticular TrainingCourse
    delete() {
        var selectedTrainingCourses = [];
        for (var i = 0; i < this.selectedEmployeeTraining.length; i++) {
            selectedTrainingCourses.push(this.selectedEmployeeTraining[i].id);
        }
        this.trainingBatchSignupService.deleteTrainingBatchSignup(selectedTrainingCourses).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPageEmployeeTraining({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of traningCourse
            this.setPageEmployeeTraining({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    

}