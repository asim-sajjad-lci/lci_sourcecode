"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var router_1 = require("@angular/router");
var employee_master_service_1 = require("../../_services/employee-master/employee-master.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var Observable_1 = require("rxjs/Observable");
var supervisor_service_1 = require("../../_services/supervisor/supervisor.service");
var division_service_1 = require("../../_services/division/division.service");
var department_service_1 = require("../../_services/department/department.service");
var position_setup_service_1 = require("../../_services/position-setup/position-setup.service");
var location_service_1 = require("../../_services/location/location.service");
var upload_file_service_1 = require("../../../_sharedresource/_services/upload-file.service");
var EmployeeMasterComponent = (function () {
    function EmployeeMasterComponent(router, employeeMasterService, getScreenDetailService, alertService, supervisorService, divisionService, departmentService, positionSetupService, locationService, uploadFileService) {
        var _this = this;
        this.router = router;
        this.employeeMasterService = employeeMasterService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.supervisorService = supervisorService;
        this.divisionService = divisionService;
        this.departmentService = departmentService;
        this.positionSetupService = positionSetupService;
        this.locationService = locationService;
        this.uploadFileService = uploadFileService;
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1445';
        this.message = { 'type': '', 'text': '' };
        this.supervisorId = {};
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.getDivision = [];
        this.getDepartment = [];
        this.getPosition = [];
        this.getLocation = [];
        this.getEmployeeUserIdInSystem = [];
        this.getEmployeeNationalities = [];
        this.getEmployeeAddressMaster = [];
        this.getSuperviser = [];
        this.getEmployee = [];
        this.url = '';
        this.isSelected = false;
        this.myOptions = {
            // other options...
            dateFormat: 'dd-mm-yyyy',
        };
        // default form parameter for department  screen
        this.defaultFormValues = [
            //0
            { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TITLE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TITLE_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LAST_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'FIRST_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LAST_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'FIRST_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MIDDLE_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HIRE_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADJUSTED_HIRE_DATE', 'fieldValue': '', 'helpMessage': '' },
            //11
            { 'fieldName': 'LAST_DAY_WORKED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'REASON', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS1', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS2', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PERSONAL_EMAIL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUSINESS_EMAIL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COUNTRY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CITY', 'fieldValue': '', 'helpMessage': '' },
            //21
            { 'fieldName': 'PERSONAL_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUSINESS_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EXT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYMENT_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'GENDER', 'fieldValue': '', 'helpMessage': '' },
            //31
            { 'fieldName': 'MARITAL_STATUS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BIRTH_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SYSTEM_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'WORK_HOURS_YEARLY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CITIZEN', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SMOKER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_IMMIGRATION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'IMAGE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NATIONALITY', 'fieldValue': '', 'helpMessage': '' },
            //41
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PRINT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPLOAD', 'fieldValue': '', 'helpMessage': '' },
        ];
        this.employeeIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.employeeId);
        }).mergeMap(function (token) { return _this.getEmployeeIdAsObservable(token); });
        this.superviserIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.supervisorId);
        }).mergeMap(function (token) { return _this.getSuperviserIdAsObservable(token); });
        this.divisionIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.divisionId);
        }).mergeMap(function (token) { return _this.getDivisionAsObservable(token); });
        this.departmentIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.departmentId);
        }).mergeMap(function (token) { return _this.getDepartmentIdAsObservable(token); });
        this.positionIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.positionId);
        }).mergeMap(function (token) { return _this.getPositionAsObservable(token); });
        this.locationIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.locationId);
        }).mergeMap(function (token) { return _this.getLocationAsObservable(token); });
        this.employeeUserIdInSystemList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.employeeUserIdInSystem);
        }).mergeMap(function (token) { return _this.getEmployeeUserIdInSystemAsObservable(token); });
        this.employeeNationalitiesIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.employeeNationalitiesId);
        }).mergeMap(function (token) { return _this.getEmployeeNationalitiesAsObservable(token); });
        this.employeeAddressMasterIdList = Observable_1.Observable.create(function (observer) {
            observer.next(_this.model.employeeAddressMasterId);
        }).mergeMap(function (token) { return _this.getEmployeeAddressMasterAsObservable(token); });
    }
    EmployeeMasterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
        this.AddressDetails = {
            "employeeAddress1": "",
            "employeeAddress2": "",
            "employeePersonalEmail": "",
            "employeeBusinessEmail": "",
            "employeeCountry": "",
            "employeeCity": "",
            "employeePersonalPhone": "",
            "employeeBusinessPhone": "",
            "employeeExt": "",
        };
        this.model = {
            "id": 0,
            "employeeId": "",
            "employeeTitle": "",
            "employeeLastName": "",
            "employeeFirstName": "",
            "employeeMiddleName": "",
            "employeeLastNameArabic": "",
            "employeeFirstNameArabic": "",
            "employeeMiddleNameArabic": "",
            "employeeHireDate": null,
            "employeeAdjustHireDate": null,
            "employeeLastWorkDate": null,
            "employeeInactiveDate": null,
            "employeeInactiveReason": "",
            "employeeType": 0,
            "employeeGender": 0,
            "employeeMaritalStatus": 0,
            "employeeBirthDate": null,
            "employeeUserIdInSystem": null,
            "employeeWorkHourYearly": null,
            "employeeCitizen": null,
            "employeeInactive": null,
            "employeeImmigration": null,
            "divisionId": null,
            "departmentId": null,
            "locationId": null,
            "supervisorId": null,
            "positionId": null,
            "employeeNationalitiesId": null,
            "employeeAddressMasterId": null,
            "employeeTitleArabic": "",
            "employeeSmoker": null
        };
        this.divisionService.getDivisionClassList().then(function (data) {
            _this.getDivision = data.result;
            console.log('this.getDivision');
            console.log(_this.getDivision);
        });
        this.departmentService.getDepartments().then(function (data) {
            _this.getDepartment = data.result;
            console.log('this.getDepartment');
            console.log(_this.getDepartment);
        });
        this.positionSetupService.getPositionIdList().then(function (data) {
            _this.getPosition = data.result.records;
            console.log('this.getPosition');
            console.log(_this.getPosition);
        });
        this.locationService.getAllLocationDropDownId().then(function (data) {
            _this.getLocation = data.result;
            console.log('this.getLocation');
            console.log(_this.getLocation);
        });
        this.supervisorService.getSuperviserId().then(function (data) {
            _this.getSuperviser = data.result;
        });
        this.supervisorService.getEmployee().then(function (data) {
            _this.getEmployee = data.result;
            console.log('this.getEmployee');
            console.log(_this.getEmployee);
        });
        this.employeeMasterService.getAllEmployeeAddressMasterDropDown().then(function (data) {
            _this.getEmployeeAddressMaster = data.result;
            console.log('this.getEmployeeAddressMaster');
            console.log(_this.getEmployee);
        });
        this.employeeMasterService.getAllEmployeeNationalitiesDropDown().then(function (data) {
            _this.getEmployeeNationalities = data.result;
            console.log('this.getEmployeeNationalities');
            console.log(_this.getEmployeeNationalities);
        });
        this.employeeMasterService.getAllUserDetailDropDown().then(function (data) {
            _this.getEmployeeUserIdInSystem = data.result;
            console.log('this.getEmployeeUserIdInSystem');
            console.log(_this.getEmployeeUserIdInSystem);
        });
    };
    EmployeeMasterComponent.prototype.getEmployeeIdAsObservable = function (token) {
        var query = new RegExp(token, 'i');
        return Observable_1.Observable.of(this.getEmployee.filter(function (id) {
            return query.test(id.employeeId);
        }));
    };
    EmployeeMasterComponent.prototype.getSuperviserIdAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(Observable_1.Observable.of(this.getSuperviser.filter(function (id) {
            return query.test(id.superVisionCode);
        })));
        return Observable_1.Observable.of(this.getSuperviser.filter(function (id) {
            return query.test(id.superVisionCode);
        }));
    };
    EmployeeMasterComponent.prototype.getDivisionAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(query);
        return Observable_1.Observable.of(this.getDivision.filter(function (id) {
            return query.test(id.divisionId);
        }));
    };
    EmployeeMasterComponent.prototype.getDepartmentIdAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(query);
        return Observable_1.Observable.of(this.getDepartment.filter(function (id) {
            return query.test(id.departmentId);
        }));
    };
    EmployeeMasterComponent.prototype.getPositionAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(query);
        return Observable_1.Observable.of(this.getPosition.filter(function (id) {
            return query.test(id.positionId);
        }));
    };
    EmployeeMasterComponent.prototype.getLocationAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(query);
        return Observable_1.Observable.of(this.getLocation.filter(function (id) {
            return query.test(id.locationId);
        }));
    };
    EmployeeMasterComponent.prototype.getEmployeeUserIdInSystemAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(query);
        return Observable_1.Observable.of(this.getEmployeeUserIdInSystem.filter(function (id) {
            return query.test(id.udId);
        }));
    };
    EmployeeMasterComponent.prototype.getEmployeeNationalitiesAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(query);
        return Observable_1.Observable.of(this.getEmployeeNationalities.filter(function (id) {
            return query.test(id.employeeNationalityId);
        }));
    };
    EmployeeMasterComponent.prototype.getEmployeeAddressMasterAsObservable = function (token) {
        var query = new RegExp(token, 'ig');
        console.log(this.getEmployeeAddressMaster);
        console.log(Observable_1.Observable.of(this.getEmployeeAddressMaster.filter(function (id) {
            return query.test(id.addressId);
        })));
        return Observable_1.Observable.of(this.getEmployeeAddressMaster.filter(function (id) {
            return query.test(id.addressId);
        }));
    };
    EmployeeMasterComponent.prototype.changeTypeaheadLoading = function (e) {
        this.typeaheadLoading = e;
        this.model.employeeFirstName = '';
        console.log(e);
    };
    EmployeeMasterComponent.prototype.typeaheadOnSelect = function (e) {
        console.log('Selected value: ', e.item);
        if (typeof (e.item.employeeFirstName) != 'undefined')
            this.employeeId = e.item.id;
    };
    EmployeeMasterComponent.prototype.typeaheadOnSelectDivisionId = function (e) {
        console.log('typeaheadOnSelectDivisionId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.DivisionId = e.item.id;
    };
    EmployeeMasterComponent.prototype.typeaheadOnSelectDepartmentId = function (e) {
        console.log('typeaheadOnSelectDepartmentId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.DepartmentId = e.item.id;
    };
    EmployeeMasterComponent.prototype.typeaheadOnSelectLocationId = function (e) {
        console.log('typeaheadOnSelectLocationId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.LocationId = e.item.id;
    };
    EmployeeMasterComponent.prototype.typeaheadOnSelectSuperVisionCode = function (e) {
        console.log('typeaheadOnSelectSuperVisionCode: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.SuperVisionCode = e.item.id;
    };
    EmployeeMasterComponent.prototype.typeaheadOnSelectEmployeeNationalityId = function (e) {
        console.log('typeaheadOnSelectEmployeeNationalityId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.EmployeeNationalityId = e.item.id;
    };
    EmployeeMasterComponent.prototype.typeaheadOnSelectEmployeeaddressId = function (e) {
        console.log('typeaheadOnSelectEmployeeaddressId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.addressId = e.item.id;
    };
    // Clear form to reset to default blank
    EmployeeMasterComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new location
    EmployeeMasterComponent.prototype.CreateEmployeeMaster = function (f, event) {
        var _this = this;
        console.log(this.model);
        this.model.employeeInactive = this.model.employeeInactive ? 1 : 0;
        this.model.employeeCitizen = this.model.employeeCitizen ? 1 : 0;
        this.model.employeeSmoker = this.model.employeeSmoker ? 1 : 0;
        this.model.employeeImmigration = this.model.employeeImmigration ? 1 : 0;
        this.model.divisionId = this.DivisionId ? this.DivisionId : null;
        this.model.departmentId = this.DepartmentId ? this.DepartmentId : null;
        this.model.positionId = this.PositionId ? this.PositionId : null;
        this.model.locationId = this.LocationId ? this.LocationId : null;
        this.model.supervisorId = this.SuperVisionCode ? this.SuperVisionCode : null;
        this.model.employeeNationalitiesId = this.EmployeeNationalityId ? this.EmployeeNationalityId : null;
        this.model.employeeAddressMasterId = this.addressId ? this.addressId : null;
        this.model.employeeId = this.employeeId ? this.employeeId : this.model.employeeId;
        event.preventDefault();
        var supIdx = this.model.employeeId;
        if (this.model.employeeId == '') {
            this.duplicateWarning = true;
            this.message.type = 'success';
            console.log("function is called");
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.duplicateWarning = false;
                }, 4000);
                _this.message.text = "Employee ID not found";
            }, 100);
        }
        else if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            //Check if the id is available in the model.
            //If id avalable then update the location, else Add new location.
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            // this.employeeMasterService.checkDuplicateEmployeeCode(supIdx).then(response => {
            //     if (response && response.code == 302 && response.result && response.result.isRepeat) {
            //         this.duplicateWarning = true;
            //         this.message.type = 'success';
            //         window.setTimeout(() => {
            //             this.isSuccessMsg = false;
            //             this.isfailureMsg = true;
            //             this.showMsg = true;
            //             window.setTimeout(() => {
            //                 this.showMsg = false;
            //                 this.duplicateWarning = false;
            //             }, 4000);
            //             this.message.text = response.btiMessage.message;
            //         }, 100);
            //     } else {
            //Call service api for Creating new location
            this.employeeMasterService.createEmployeeMasterAttachment(this.model, this.uploadAttachfile).then(function (data) {
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(function () {
                        _this.isSuccessMsg = true;
                        _this.isfailureMsg = false;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.hasMsg = false;
                        }, 4000);
                        _this.messageText = data.btiMessage.message;
                        ;
                    }, 100);
                    _this.hasMsg = true;
                    f.resetForm();
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin !';
                }, 100);
            });
            //    }
            // }).catch(error => {
            //     this.hasMsg = true;
            //     window.setTimeout(() => {
            //         this.isSuccessMsg = false;
            //         this.isfailureMsg = true;
            //         this.showMsg = true;
            //         this.messageText = 'Server error. Please contact admin !';
            //     }, 100)
            // });
        }
    };
    EmployeeMasterComponent.prototype.getEmployeeDetail = function (event) {
        var _this = this;
        if (event.target.value != "") {
            if (isNaN(event.target.value) == true) {
                this.model.employeeId = '';
                return false;
            }
            else {
                this.employeeMasterService.getEmployeeDetail(event.target.value).then(function (response) {
                    var datacode = response.code;
                    if (datacode == 404) {
                        _this.hasMessage = true;
                        _this.message.type = 'success';
                        window.scrollTo(0, 0);
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            window.setTimeout(function () {
                                _this.showMsg = false;
                                _this.hasMessage = false;
                            }, 4000);
                            _this.message.text = response.btiMessage.message + ' !';
                        }, 100);
                        _this.model.employeeFirstName = '';
                    }
                    else {
                        _this.model.employeeFirstName = response.result.records.firstName + " " + response.result.records.lastName;
                    }
                }).catch(function (error) {
                    _this.hasMessage = true;
                    _this.message.type = 'error';
                    var errorCode = error.status;
                    _this.message.text = 'Server issue. Please contact admin !';
                });
            }
        }
        else {
            this.model.employeeFirstName = '';
        }
    };
    EmployeeMasterComponent.prototype.formatDateFordatePicker = function (strDate) {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        }
        else {
            return null;
        }
    };
    EmployeeMasterComponent.prototype.onEmployeeHireDateChanged = function (event) {
        this.frmEmployeeHireDate = event.jsdate;
        this.model.employeeHireDate = event.epoc;
    };
    EmployeeMasterComponent.prototype.onEmployeeAdjustHireDateChanged = function (event) {
        this.frmEmployeeAdjustHireDate = event.jsdate;
        this.model.employeeAdjustHireDate = event.epoc;
    };
    EmployeeMasterComponent.prototype.onEmployeeLastWorkDateChanged = function (event) {
        this.frmEmployeeLastWorkDate = event.jsdate;
        this.model.employeeLastWorkDate = event.epoc;
    };
    EmployeeMasterComponent.prototype.onEmployeeInactiveDateChangedd = function (event) {
        this.frmEmployeeInactiveDate = event.jsdate;
        this.model.employeeInactiveDate = event.epoc;
    };
    EmployeeMasterComponent.prototype.onEmployeeBirthDateChangedd = function (event) {
        this.frmEmployeeBirthDate = event.jsdate;
        this.model.employeeBirthDate = event.epoc;
    };
    EmployeeMasterComponent.prototype.print = function () {
        var printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto,scale=50');
        popupWin.document.open();
        popupWin.document.write("\n          <html>\n            <head>\n              <title>Print tab</title>\n              <link rel=\"stylesheet\" href=\"/assets/css/AdminLTE.min.css\">\n              <link rel=\"stylesheet\" href=\"/assets/css/bootstrap.min.css\">\n              <style>\n              @media print{\n                .doNotPrint{display:none;!important}\n              }\n              @page { size: landscape; \n                 margin-top: 25px;\n                margin-bottom: 250px;\n                margin-right: 0px;\n                margin-left: 0px;\n                -webkit-transform: scale(0.5);  /* Saf3.1+, Chrome */\n                -moz-transform: scale(0.5);  /* FF3.5+ */\n                -ms-transform: scale(0.5);  /* IE9 */\n                -o-transform: scale(0.5);  /* Opera 10.5+ */\n                transform: scale(0.5);  }\n            \n              \n              </style>\n            </head>\n        <body onload=\"window.print();window.close()\">" + printContents + "</body>\n          </html>");
        popupWin.document.close();
    };
    EmployeeMasterComponent.prototype.onUploadBtn = function (event) {
        var _this = this;
        console.log("aaaa:", event.target.files[0]);
        this.fileName = "";
        if (event.target.files && event.target.files[0]) {
            this.fileName = event.target.files[0].name;
            this.fileSelect = event.target.files[0];
            var reader = new FileReader();
            if ((this.fileSelect.type == "image/png" ||
                this.fileSelect.type == "image/jpg" ||
                this.fileSelect.type == "image/jpeg")) {
                this.isSelected = false;
            }
            else {
                this.isSelected = false;
            }
            this.uploadAttachfile = event.target.files[0];
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = function (event) {
                _this.url = event.target.result;
                console.log(_this.url);
            };
        }
    };
    EmployeeMasterComponent.prototype.varifyDelete = function () {
        this.isDeleteAction = true;
        this.isConfirmationModalOpen = true;
    };
    //delete traningCourse by passing whole object of perticular TrainingCourse
    EmployeeMasterComponent.prototype.delete = function () {
        var _this = this;
        var selected = [];
        selected.push(this.model.id);
        this.employeeMasterService.deleteEmployeeMaster(selected).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
            }
            _this.hasMessage = true;
            _this.message.type = "success";
            //this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = "error";
            var errorCode = error.status;
            _this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    };
    EmployeeMasterComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    };
    EmployeeMasterComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    EmployeeMasterComponent.prototype._keyPress = function (event) {
        var pattern = /[0-9\+\-\ ]/;
        var inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    };
    return EmployeeMasterComponent;
}());
EmployeeMasterComponent = __decorate([
    core_1.Component({
        selector: 'employee-master',
        templateUrl: './employee-master.component.html',
        providers: [
            employee_master_service_1.EmployeeMasterService,
            supervisor_service_1.SupervisorService,
            division_service_1.DivisionService,
            department_service_1.DepartmentService,
            position_setup_service_1.PositionSetupService,
            location_service_1.LocationService
        ]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        employee_master_service_1.EmployeeMasterService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService,
        supervisor_service_1.SupervisorService,
        division_service_1.DivisionService,
        department_service_1.DepartmentService,
        position_setup_service_1.PositionSetupService,
        location_service_1.LocationService,
        upload_file_service_1.UploadFileService])
], EmployeeMasterComponent);
exports.EmployeeMasterComponent = EmployeeMasterComponent;
//# sourceMappingURL=employee-master.component.js.map