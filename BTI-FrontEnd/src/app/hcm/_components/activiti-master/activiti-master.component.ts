import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Router } from '@angular/router';
import { ActivitiMaster } from '../../_models/activiti-master/activiti-master.module';
import { ActivitiMasterService } from '../../_services/activiti-master/activiti-master.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatePickerModule } from 'ng2-datepicker-bootstrap';
import { SupervisorService } from '../../_services/supervisor/supervisor.service';
import { DivisionService } from '../../_services/division/division.service';
import { DepartmentService } from '../../_services/department/department.service';
import { PositionSetupService } from '../../_services/position-setup/position-setup.service';
import { LocationService } from '../../_services/location/location.service';
import { UploadFileService } from '../../../_sharedresource/_services/upload-file.service';
import { Page } from '../../../_sharedresource/page';

@Component({
    selector: 'activiti-master',
    templateUrl: './activiti-master.component.html',
    providers: [
        ActivitiMasterService,
        SupervisorService,
        DivisionService,
        DepartmentService,
        PositionSetupService,
        LocationService
    ]
})
export class ActivitiMasterComponent {

    page = new Page();
    rows = new Array<ActivitiMaster>();
    temp = new Array<ActivitiMaster>();


    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1445';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    supervisorId = {};
    model: ActivitiMaster;
    searchKeyword = '';

    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;


    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    supervisorIdvalue: string;
    getDivision: any[] = [];
    getDepartment: any[] = [];
    getPosition: any[] = [];
    getLocation: any[] = [];
    getActivitiUserIdInSystem: any[] = [];
    getActivitiNationalities: any[] = [];
    getActivitiAddressMaster: any[] = [];

    divisionIdList: Observable<any>;
    departmentIdList: Observable<any>;
    positionIdList: Observable<any>;
    locationIdList: Observable<any>;
    activitiUserIdInSystemList: Observable<any>;
    activitiNationalitiesIdList: Observable<any>;
    activitiAddressIndexIdList: Observable<any>;
    superviserIdList: Observable<any>;
    activitiIdList: Observable<any>;
    getSuperviser: any[] = [];
    getActiviti: any[] = [];
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;

    //Date
    modelActivitiHireDate;
    frmActivitiHireDate;
    modelActivitiAdjustHireDate;
    frmActivitiAdjustHireDate;
    modelActivitiLastWorkDate;
    frmActivitiLastWorkDate;
    modelActivitiInactiveDate;
    frmActivitiInactiveDate;
    modelActivitiBirthDate;
    frmActivitiBirthDate;
    url = '';
    fileName: string;
    isSelected: boolean = false;
    fileSelect: any;
    uploadAttachfile;
    AddressDetails;
    ddPageSize = 5;

    DivisionId = null;
    DepartmentId = null;
    PositionId = null;
    LocationId = null;
    SuperVisionCode = null;
    SystemUserId = null;
    ActivitiNationalityId = null;
    addressId = null;
    activitiId = null;
    activitiIndexId = null;
    activitiIdValue = null;
    activitiIndexIdValue = null;

    gridLists;
    getScreenDetailArr;
    isConfigure = false;
    isShowHideGrid = false;
    erroractivitiHireDate: any = { isError: false, errorMessage: '' };
    erroractivitiAdjustHireDate: any = { isError: false, errorMessage: '' };
    erroractivitiLastWorkDate: any = { isError: false, errorMessage: '' };
    erroractivitiInactiveDate: any = { isError: false, errorMessage: '' };
    erroractivitiMaritalStatus: any = { isError: false, errorMessage: '' };
    errorUpload: any = { isError: false, errorMessage: '' };

    NationalityDescription;
    activitiHireDate;
    activitiAdjustHireDate;
    activitiLastDayDate;
    constAddressDetails = {
        "address1": "",
        "address2": "",
        "addressArabic1": "",
        "addressArabic2": "",
        "addressId": "",
        "associateMessage": "",
        "businessEmail": "",
        "businessPhone": "",
        "activitiAddressIndexId": "",
        "locationLinkGoogleMap": "",
        "message": "",
        "messageType": "",
        "otherPhone": "",
        "pOBox": "",
        "pageNumber": "",
        "pageSize": "",
        "personalEmail": "",
        "personalPhone": "",
        "postCode": "",
        "city": '',
        "country": ''
    };

    constructor(private router: Router,
        private activitiMasterService: ActivitiMasterService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private supervisorService: SupervisorService,
        private divisionService: DivisionService,
        private departmentService: DepartmentService,
        private positionSetupService: PositionSetupService,
        private locationService: LocationService,
        private uploadFileService: UploadFileService, ) {

        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            //0
            { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TITLE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TITLE_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LAST_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'FIRST_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LAST_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'FIRST_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MIDDLE_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HIRE_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADJUSTED_HIRE_DATE', 'fieldValue': '', 'helpMessage': '' },
            //11
            { 'fieldName': 'LAST_DAY_WORKED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DATE_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'REASON', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS1', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS2', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PERSONAL_EMAIL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUSINESS_EMAIL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COUNTRY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CITY', 'fieldValue': '', 'helpMessage': '' },
            //21
            { 'fieldName': 'PERSONAL_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUSINESS_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EXT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DIVISION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYMENT_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'GENDER', 'fieldValue': '', 'helpMessage': '' },
            //31
            { 'fieldName': 'MARITAL_STATUS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BIRTH_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SYSTEM_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'WORK_HOURS_YEARLY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CITIZEN', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SMOKER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_IMMIGRATION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'IMAGE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NATIONALITY', 'fieldValue': '', 'helpMessage': '' },
            //41
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PRINT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPLOAD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'STATUSEMP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '' },

        ];
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.getScreenDetails();

        this.AddressDetails = {
            "address1": "",
            "address2": "",
            "addressArabic1": "",
            "addressArabic2": "",
            "addressId": "",
            "associateMessage": "",
            "businessEmail": "",
            "businessPhone": "",
            "activitiAddressIndexId": "",
            "locationLinkGoogleMap": "",
            "message": "",
            "messageType": "",
            "otherPhone": "",
            "pOBox": "",
            "pageNumber": "",
            "pageSize": "",
            "personalEmail": "",
            "personalPhone": "",
            "postCode": "",
            "city": '',
            "country": ''
        }

    }
    // Open form for create department
    Create() {
        window.scrollTo(0, 0);
        this.modelActivitiBirthDate = null;
        this.modelActivitiHireDate = null;
        this.modelActivitiAdjustHireDate = null;
        this.modelActivitiLastWorkDate = null;
        this.modelActivitiInactiveDate = null;
        this.url = null;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
        this.erroractivitiHireDate = { isError: false, errorMessage: '' };
        this.activitiHireDate = undefined;
        this.activitiAdjustHireDate = undefined;
        this.activitiLastDayDate = undefined;
        this.AddressDetails = this.constAddressDetails;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 500);
            }, 10);
        }, 10);
        this.model = {
            "activitiIndexId": 0,
            "activitiId": "",
            "activitiTitle": "",
            "activitiLastName": "",
            "activitiFirstName": "",
            "activitiMiddleName": "",
            "activitiLastNameArabic": "",
            "activitiFirstNameArabic": "",
            "activitiMiddleNameArabic": "",
            "activitiHireDate": null,
            "activitiAdjustHireDate": null,
            "activitiLastWorkDate": null,
            "activitiInactiveDate": null,
            "activitiInactiveReason": "",
            "activitiType": 0,
            "activitiGender": '',
            "activitiMaritalStatus": '',
            "activitiBirthDate": null,
            "activitiUserIdInSystem": null,
            "activitiWorkHourYearly": null,
            "activitiCitizen": null,
            "activitiInactive": null,
            "activitiImmigration": null,
            "divisionId": null,
            "departmentId": null,
            "locationId": null,
            "supervisorId": null,
            "positionId": null,
            "activitiNationalitiesId": null,
            "activitiAddressIndexId": null,
            "activitiTitleArabic": "",
            "activitiSmoker": null

        };

        this.DivisionId = null;
        this.DepartmentId = null;
        this.PositionId = null;
        this.LocationId = null;
        this.SuperVisionCode = null;
        this.ActivitiNationalityId = null;
        this.addressId = null;
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    changeAactivitiMaritalStatus(e) {
        //console.log(e.target.value);
        if (e.target.value == 4) {
            this.erroractivitiMaritalStatus = { isError: true, errorMessage: 'Invalid Marital Status.' };
        } else {
            this.erroractivitiMaritalStatus = { isError: false, errorMessage: '' };
        }

    }

    getScreenDetails() {
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.screenGridDetail(this.moduleCode, this.screenCode).then(data => {
            this.gridLists = data.result.dtoScreenDetail.girdList;
            this.getScreenDetailArr = data.result;

            for (let i = 0; i < this.gridLists.length; i++) {
                this.gridLists[i]['fieldDetailList'].sort(function (a, b) {
                    var keyA = parseInt(a.colOrder),
                        keyB = parseInt(b.colOrder);
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });

            }


            //console.log(this.gridLists);
            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        this.model.activitiFirstName = '';
        //console.log(e);
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        //console.log('Selected value: ', e.item);
        if (typeof (e.item.activitiFirstName) != 'undefined')
            this.activitiId = e.item.id;
    }

    typeaheadOnSelectDivisionId(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectDivisionId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.DivisionId = e.item.id;
    }
    typeaheadOnSelectDepartmentId(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectDepartmentId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.DepartmentId = e.item.id;
    }
    typeaheadOnSelectPositionId(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectPositionId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.PositionId = e.item.id;
    }
    typeaheadOnSelectLocationId(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectLocationId: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.LocationId = e.item.id;
    }
    typeaheadOnSelectSuperVisionCode(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectSuperVisionCode: ', e.item);
        if (typeof (e.item.id) != 'undefined')
            this.SuperVisionCode = e.item.id;
        //console.log('supervisor', this.SuperVisionCode);
    }
    typeaheadOnSelectSystemUserId(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectSystemUserId: ', e.item);
        if (typeof (e.item.firstName) != 'undefined')
            this.SystemUserId = e.item.udId;
    }
    typeaheadOnSelectActivitiNationalityId(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectActivitiNationalityId: ', e.item);
        if (typeof (e.item.activitiNationalityIndexId) != 'undefined')
            this.ActivitiNationalityId = e.item.activitiNationalityIndexId;
        this.NationalityDescription = e.item.activitiNationalityDescription;
    }
    typeaheadOnSelectActivitiaddressId(e: TypeaheadMatch): void {
        //console.log('typeaheadOnSelectActivitiaddressId: ', e.item);
        if (typeof (e.item.activitiAddressIndexId) != 'undefined') {
            this.addressId = e.item.activitiAddressIndexId;
            this.AddressDetails = e.item;
            //console.log(this.addressId);
        }

    }



    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ 'activitiGender': '', 'activitiType': 0, 'activitiMaritalStatus': '' });
        this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
        this.erroractivitiHireDate = { isError: false, errorMessage: '' };
        this.activitiHireDate = undefined;
        this.activitiAdjustHireDate = undefined;
        this.activitiLastDayDate = undefined;
        this.AddressDetails = this.constAddressDetails;
    }


    //function call for creating new location
    CreateActivitiMaster(f: NgForm, event: Event) {
        
        if (!this.DivisionId ||
            !this.DepartmentId ||
            !this.PositionId ||
            !this.LocationId ||
            !this.SuperVisionCode ||
            !this.ActivitiNationalityId ||
            !this.addressId
        ) {
            //console.log('INvalide selection')
            return false;
        }
        var supIdx = this.model.activitiId;
        this.model.activitiInactive = this.model.activitiInactive ? 1 : 0;
        this.model.activitiCitizen = this.model.activitiCitizen ? 1 : 0;
        this.model.activitiSmoker = this.model.activitiSmoker ? 1 : 0;
        this.model.activitiImmigration = this.model.activitiImmigration ? 1 : 0;

        this.model.divisionId = this.DivisionId ? this.DivisionId : null;
        this.model.departmentId = this.DepartmentId ? this.DepartmentId : null;
        this.model.positionId = this.PositionId ? this.PositionId : null;
        this.model.locationId = this.LocationId ? this.LocationId : null;
        this.model.supervisorId = this.SuperVisionCode ? this.SuperVisionCode : null;
        this.model.activitiUserIdInSystem = this.SystemUserId ? this.SystemUserId : null;
        this.model.activitiNationalitiesId = this.ActivitiNationalityId ? this.ActivitiNationalityId : null;
        this.model.activitiAddressIndexId = this.addressId ? this.addressId : null;
        this.model.activitiId = this.activitiId ? this.activitiId : this.model.activitiId;
        this.model.activitiHireDate = this.frmActivitiHireDate;
        this.model.activitiAdjustHireDate = this.frmActivitiAdjustHireDate;
        this.model.activitiLastWorkDate = this.frmActivitiLastWorkDate;
        this.model.activitiBirthDate = this.frmActivitiBirthDate;
        this.model.activitiInactiveDate = this.frmActivitiInactiveDate;
        event.preventDefault();
        
        if (this.model.activitiIndexId > 0 && this.model.activitiIndexId != 0 && this.model.activitiIndexId != undefined) {
            //Check if the id is available in the model.
            //If id avalable then update the location, else Add new location.

            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.activitiMasterService.checkDuplicateActivitiCode(supIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new location


                    this.activitiMasterService.createActivitiMasterAttachment(this.model, this.uploadAttachfile).then(data => {
                        var datacode = data.code;


                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);


                            //Refresh the Grid data after deletion of division
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                            this.hasMsg = true;
                            f.resetForm();

                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin !';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin !';
                }, 100)
            });

        }
    }

    getActivitiDetail(event) {
        if (event.target.value != "") {
            if (isNaN(event.target.value) == true) {
                this.model.activitiId = '';
                return false;
            } else {

                this.activitiMasterService.getActivitiDetail(event.target.value).then(response => {
                    var datacode = response.code;
                    if (datacode == 404) {
                        this.hasMessage = true;
                        this.message.type = 'success';

                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMessage = false;
                            }, 4000);
                            this.message.text = response.btiMessage.message + ' !';
                        }, 100);
                        this.model.activitiFirstName = '';
                    } else {
                        this.model.activitiFirstName = response.result.records.firstName + " " + response.result.records.lastName;
                    }
                }).catch(error => {
                    this.hasMessage = true;
                    this.message.type = 'error';
                    var errorCode = error.status;
                    this.message.text = 'Server issue. Please contact admin !';
                });

            }
        } else {
            this.model.activitiFirstName = '';
        }
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onActivitiHireDateChanged(event: IMyDateModel): void {
        this.frmActivitiHireDate = event.jsdate;

        this.activitiHireDate = event.epoc;

        /*if((this.model.activitiAdjustHireDate < this.model.activitiHireDate) && this.model.activitiAdjustHireDate!=undefined){
            this.erroractivitiHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
        }else{
            this.erroractivitiHireDate={isError:false,errorMessage:''};
        }
        if((this.model.activitiLastWorkDate < this.model.activitiHireDate) && this.model.activitiLastWorkDate!=undefined){
            this.erroractivitiHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
        }else{
            this.erroractivitiHireDate={isError:false,errorMessage:''};
        }*/

        if ((this.activitiHireDate > this.activitiLastDayDate) && this.activitiLastDayDate != undefined) {
            this.erroractivitiHireDate = { isError: true, errorMessage: 'Invalid Hire Date.' };
        }
        if (this.activitiHireDate <= this.activitiLastDayDate) {
            this.erroractivitiHireDate = { isError: false, errorMessage: '' };
        }
        if (this.activitiLastDayDate == undefined && this.activitiHireDate == undefined) {
            this.erroractivitiHireDate = { isError: false, errorMessage: '' };
        }
    }

    onActivitiAdjustHireDateChanged(event: IMyDateModel): void {
        this.frmActivitiAdjustHireDate = event.jsdate;
        this.activitiAdjustHireDate = event.epoc;

        /*if((this.model.activitiHireDate > this.model.activitiAdjustHireDate) && this.model.activitiAdjustHireDate!=undefined){
            this.erroractivitiAdjustHireDate={isError:true,errorMessage:'Invalid Adjust Hire Date.'};
        }else if(this.model.activitiHireDate <= this.model.activitiAdjustHireDate){
            this.erroractivitiAdjustHireDate={isError:false,errorMessage:''};
        }else if(this.model.activitiAdjustHireDate==undefined && this.model.activitiHireDate==undefined){
			 this.erroractivitiAdjustHireDate={isError:false,errorMessage:''};
        }
        if((this.model.activitiAdjustHireDate < this.model.activitiLastWorkDate) && this.model.activitiLastWorkDate!=undefined){
            this.erroractivitiAdjustHireDate={isError:true,errorMessage:'Invalid Adjust Hire Date.'};
        }else{
            this.erroractivitiAdjustHireDate={isError:false,errorMessage:''};
        }
        if((this.model.activitiAdjustHireDate < this.model.activitiHireDate) && this.model.activitiAdjustHireDate!=undefined){
            this.erroractivitiHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
        }else{
            this.erroractivitiHireDate={isError:false,errorMessage:''};
        }
        if((this.model.activitiLastWorkDate < this.model.activitiHireDate) && this.model.activitiLastWorkDate!=undefined){
            this.erroractivitiHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
        }else{
            this.erroractivitiHireDate={isError:false,errorMessage:''};
        }*/
        if ((this.activitiAdjustHireDate > this.activitiLastDayDate) && this.activitiLastDayDate != undefined) {
            this.erroractivitiAdjustHireDate = { isError: true, errorMessage: 'Invalid Adjust Hire Date.' };
        }
        if (this.activitiAdjustHireDate <= this.activitiLastDayDate) {
            this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
        }
        if (this.activitiAdjustHireDate == undefined && this.activitiLastDayDate == undefined) {
            this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
        }

    }
    onActivitiLastWorkDateChanged(event: IMyDateModel): void {
        this.frmActivitiLastWorkDate = event.jsdate;
        this.activitiLastDayDate = event.epoc;
        /*if((this.model.activitiAdjustHireDate > this.model.activitiLastWorkDate) && this.model.activitiLastWorkDate!=undefined){
            this.erroractivitiLastWorkDate={isError:true,errorMessage:'Invalid Last Work Date.'};
        }else if((this.model.activitiHireDate > this.model.activitiLastWorkDate) && this.model.activitiLastWorkDate!=undefined){
            this.erroractivitiLastWorkDate={isError:true,errorMessage:'Invalid Last Work Date.'};
        }else if(this.model.activitiAdjustHireDate <= this.model.activitiLastWorkDate){
            this.erroractivitiLastWorkDate={isError:false,errorMessage:''};
        }else if(this.model.activitiLastWorkDate==undefined && this.model.activitiAdjustHireDate==undefined){
			 this.erroractivitiLastWorkDate={isError:false,errorMessage:''};
        }

        if((this.model.activitiAdjustHireDate < this.model.activitiHireDate) && this.model.activitiAdjustHireDate!=undefined){
            this.erroractivitiHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
        }else{
            this.erroractivitiHireDate={isError:false,errorMessage:''};
        }
        if((this.model.activitiLastWorkDate < this.model.activitiHireDate) && this.model.activitiLastWorkDate!=undefined){
            this.erroractivitiHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
        }else{
            this.erroractivitiHireDate={isError:false,errorMessage:''};
        }*/
        if (this.activitiLastDayDate == 0) {
            this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
            this.erroractivitiHireDate = { isError: false, errorMessage: '' };
            this.activitiLastDayDate = undefined;
            return;
        }

        if ((this.activitiAdjustHireDate > this.activitiLastDayDate) && this.activitiAdjustHireDate != undefined) {
            this.erroractivitiAdjustHireDate = { isError: true, errorMessage: 'Invalid Adjust Hire Date.' };
        }
        if (this.activitiAdjustHireDate <= this.activitiLastDayDate) {
            this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
        }
        if (this.activitiAdjustHireDate == undefined && this.activitiLastDayDate == undefined) {
            this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
        }

        if ((this.activitiHireDate > this.activitiLastDayDate) && this.activitiHireDate != undefined) {
            this.erroractivitiHireDate = { isError: true, errorMessage: 'Invalid Hire Date.' };
        }
        if (this.activitiHireDate <= this.activitiLastDayDate) {
            this.erroractivitiHireDate = { isError: false, errorMessage: '' };
        }
        if (this.activitiLastDayDate == undefined && this.activitiHireDate == undefined) {
            this.erroractivitiHireDate = { isError: false, errorMessage: '' };
        }
    }
    onActivitiInactiveDateChangedd(event: IMyDateModel): void {
        this.frmActivitiInactiveDate = event.jsdate;
        this.model.activitiInactiveDate = event.epoc;

        if ((this.modelActivitiLastWorkDate.epoc > this.model.activitiInactiveDate) && this.model.activitiInactiveDate != undefined) {
            this.erroractivitiInactiveDate = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.modelActivitiLastWorkDate.epoc <= this.model.activitiInactiveDate) {
            this.erroractivitiInactiveDate = { isError: false, errorMessage: '' };
        }
        if (this.model.activitiInactiveDate == undefined && this.modelActivitiLastWorkDate.epoc == undefined) {
            this.erroractivitiInactiveDate = { isError: false, errorMessage: '' };
        }
    }

    onActivitiBirthDateChangedd(event: IMyDateModel): void {
        this.frmActivitiBirthDate = event.jsdate;
        this.model.activitiBirthDate = event.epoc;
    }

    //edit department by row
    edit(row: ActivitiMaster) {
        console.log(row);
        /*for(var i=0;i<this.getActivitiAddressMaster.length;i++){
            if(this.getActivitiAddressMaster[i].)
        }*/

        

        this.erroractivitiAdjustHireDate={isError:false,errorMessage:''};
        this.erroractivitiHireDate={isError:false,errorMessage:''};
        this.DivisionId = row.divisionId;
        this.DepartmentId = row.departmentId;
        this.PositionId = row.positionId;
        this.LocationId = row.locationId;
        this.SuperVisionCode = row.supervisorId;
        this.SystemUserId = row.activitiUserIdInSystem;
        this.ActivitiNationalityId = row.activitiNationalitiesId;
        this.addressId = row['dtoActivitiAddressMaster'].activitiAddressIndexId;
        this.activitiId = row.activitiId;
        this.AddressDetails = row['dtoActivitiAddressMaster'];
        this.modelActivitiBirthDate = this.formatDateFordatePicker(new Date(row.activitiBirthDate));
        this.modelActivitiHireDate = this.formatDateFordatePicker(new Date(row.activitiHireDate));
        this.modelActivitiAdjustHireDate = this.formatDateFordatePicker(new Date(row.activitiAdjustHireDate));
        this.modelActivitiLastWorkDate = this.formatDateFordatePicker(new Date(row.activitiLastWorkDate));
        this.frmActivitiHireDate = row.activitiHireDate;
        this.frmActivitiAdjustHireDate = row.activitiAdjustHireDate;
        this.frmActivitiLastWorkDate = row.activitiLastWorkDate;
        this.frmActivitiBirthDate = row.activitiBirthDate;
        this.NationalityDescription = row['dtoActivitiNationalities'].activitiNationalityDescription;
        if (row['activitiImage'] != null) {
            this.url = "data:image/png;base64," + row['activitiImage'];
        }
        else {
            this.url = '/assets/img/no-profile-pic-tiny.png';
        }
        this.showCreateForm = true;
        this.model = Object.assign({}, row);

        this.model.divisionId = row['division_Id'];
        this.model.departmentId = row['department_Id'];
        this.model.positionId = row['position_Id'];
        this.model.locationId = row['dtoLocation']['locationId'];
        this.model.supervisorId = row['dtoSupervisor']['superVisionCode'];
      
        this.model.activitiNationalitiesId = row['dtoActivitiNationalities']['activitiNationalityId'];
        this.model.activitiAddressIndexId = row['dtoActivitiAddressMaster']['addressId'];
        this.model.activitiGender = row['activitiGender'].toString();

        if (row && !row['activitiInactive']) {
            this.modelActivitiInactiveDate = null;
            this.model.activitiInactiveReason = '';
        } else {
            this.modelActivitiInactiveDate = this.formatDateFordatePicker(new Date(row['activitiInactiveDate']));
            this.model.activitiInactiveReason = row['activitiInactiveReason'];
            this.frmActivitiInactiveDate = row['activitiInactiveDate'];
        }
        this.supervisorService.getUserDetailByUserId(row['activitiUserIdInSystem']).then(data => {
            //console.log(data);
            this.model.activitiUserIdInSystem = data.result.firstName;
        });
        this.isUnderUpdate = true;
        this.activitiIndexId = row.activitiIndexId;
        this.activitiIndexIdValue = this.model.activitiIndexId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    dataURLtoFile(dataurl, filename) {
        console.log(dataurl);
        if(dataurl != "/assets/img/no-profile-pic-tiny.png"){
            var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }
            return new File([u8arr], filename, { type: mime });
        }
        return new File([], filename, { type: mime });
        
    }


    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
        this.model.activitiIndexId = this.activitiIndexId;
        let UpdatePostData = {
            "activitiIndexId": this.model.activitiIndexId,
            "activitiId": this.model.activitiId,
            "activitiTitle": this.model.activitiTitle,
            "activitiTitleArabic": this.model.activitiTitleArabic,
            "activitiLastName": this.model.activitiLastName,
            "activitiFirstName": this.model.activitiFirstName,
            "activitiMiddleName": this.model.activitiMiddleName,
            "activitiLastNameArabic": this.model.activitiLastNameArabic,
            "activitiFirstNameArabic": this.model.activitiFirstNameArabic,
            "activitiMiddleNameArabic": this.model.activitiMiddleNameArabic,
            "activitiHireDate": this.model.activitiHireDate,
            "activitiAdjustHireDate": this.model.activitiAdjustHireDate,
            "activitiLastWorkDate": this.model.activitiLastWorkDate,
            "activitiInactiveDate": this.model.activitiInactiveDate,
            "activitiInactiveReason": this.model.activitiInactiveReason,
            "activitiType": this.model.activitiType,
            "activitiGender": this.model.activitiGender,
            "activitiMaritalStatus": this.model.activitiMaritalStatus,
            "activitiBirthDate": this.model.activitiBirthDate,
            "activitiUserIdInSystem": this.model.activitiUserIdInSystem,
            "activitiWorkHourYearly": this.model.activitiWorkHourYearly,
            "activitiCitizen": this.model.activitiCitizen,
            "activitiInactive": this.model.activitiInactive,
            "activitiImmigration": this.model.activitiImmigration,
            "divisionId": this.model.divisionId,
            "departmentId": this.model.departmentId,
            "locationId": this.model.locationId,
            "supervisorId": this.model.supervisorId,
            "positionId": this.model.positionId,
            "activitiNationalitiesId": this.model.activitiNationalitiesId,
            "activitiAddressIndexId": this.model.activitiAddressIndexId,
            "activitiSmoker": this.model.activitiSmoker
        }
        
        if((!this.uploadAttachfile || !this.uploadAttachfile.name ) && this.url ){
            this.uploadAttachfile = this.dataURLtoFile(this.url, 'a.png');
        }
        
        // console.log(this.uploadAttachfile);


        this.activitiMasterService.updateActivitiMaster(UpdatePostData, this.uploadAttachfile).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                    this.modelActivitiBirthDate = null;
                    this.modelActivitiHireDate = null;
                    this.modelActivitiAdjustHireDate = null;
                    this.modelActivitiLastWorkDate = null;
                    this.modelActivitiInactiveDate = null;
                    this.url = null;
                    
                    this.isUnderUpdate = false;
                    this.erroractivitiAdjustHireDate = { isError: false, errorMessage: '' };
                    this.erroractivitiHireDate = { isError: false, errorMessage: '' };
                    this.activitiHireDate = undefined;
                    this.activitiAdjustHireDate = undefined;
                    this.activitiLastDayDate = undefined;
                    this.AddressDetails = this.constAddressDetails;
                   
                    this.model = {
                        "activitiIndexId": 0,
                        "activitiId": "",
                        "activitiTitle": "",
                        "activitiLastName": "",
                        "activitiFirstName": "",
                        "activitiMiddleName": "",
                        "activitiLastNameArabic": "",
                        "activitiFirstNameArabic": "",
                        "activitiMiddleNameArabic": "",
                        "activitiHireDate": null,
                        "activitiAdjustHireDate": null,
                        "activitiLastWorkDate": null,
                        "activitiInactiveDate": null,
                        "activitiInactiveReason": "",
                        "activitiType": 0,
                        "activitiGender": '',
                        "activitiMaritalStatus": '',
                        "activitiBirthDate": null,
                        "activitiUserIdInSystem": null,
                        "activitiWorkHourYearly": null,
                        "activitiCitizen": null,
                        "activitiInactive": null,
                        "activitiImmigration": null,
                        "divisionId": null,
                        "departmentId": null,
                        "locationId": null,
                        "supervisorId": null,
                        "positionId": null,
                        "activitiNationalitiesId": null,
                        "activitiAddressIndexId": null,
                        "activitiTitleArabic": "",
                        "activitiSmoker": null
            
                    };
            
                    this.DivisionId = null;
                    this.DepartmentId = null;
                    this.PositionId = null;
                    this.LocationId = null;
                    this.SuperVisionCode = null;
                    this.ActivitiNationalityId = null;
                    this.addressId = null;
                    this.url = null;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto,scale=50');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
              <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
              <style>
              @media print{
                .doNotPrint{display:none;!important}
              }
              @page { size: landscape; 
                 margin-top: 25px;
                margin-bottom: 250px;
                margin-right: 0px;
                margin-left: 0px;
                -webkit-transform: scale(0.5);  /* Saf3.1+, Chrome */
                -moz-transform: scale(0.5);  /* FF3.5+ */
                -ms-transform: scale(0.5);  /* IE9 */
                -o-transform: scale(0.5);  /* Opera 10.5+ */
                transform: scale(0.5);  }
            
              
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }

    DeleteImage() {
        this.uploadAttachfile = '';
        this.url = '';
    }

    CheckIsInactive(){
        if(!this.model.activitiInactive){
            this.modelActivitiInactiveDate =null;
            this.model.activitiInactiveReason ='';
        }
    }

    onUploadBtn(event) {
        //console.log("aaaa:", event.target.files[0]);
        this.fileName = ""

        if (event.target.files && event.target.files[0]) {
            this.fileName = event.target.files[0].name;
            this.fileSelect = event.target.files[0];
            var reader = new FileReader();

            if (
                (this.fileSelect.type == "image/png" ||
                    this.fileSelect.type == "image/jpg" ||
                    this.fileSelect.type == "image/jpeg")
            ) {
                this.isSelected = false;
            } else {
                this.isSelected = false;
                this.errorUpload = { isError: true, errorMessage: 'Invalid File.' };
                return false;
            }

            if (event.target.files[0].size / 1048576 > 2) {
                this.errorUpload = { isError: true, errorMessage: 'File size exceeds 2 MB.' };
                return false;
            }

            this.uploadAttachfile = event.target.files[0];
            reader.readAsDataURL(event.target.files[0]); // read file as data url
            reader.onload = (event: any) => { // called once readAsDataURL is completed 
                this.url = event.target.result;
                //console.log(this.url);
            }
        }
    }



    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    getRowValue(row, gridFieldName) {

        if (gridFieldName == 'dtoActivitiNationalities') {
            return row[gridFieldName] && row[gridFieldName]['activitiNationalityId'] ? row[gridFieldName]['activitiNationalityId'] : '';
        } else if (gridFieldName == 'dtoLocation') {
            return row[gridFieldName] && row[gridFieldName]['locationId'] ? row[gridFieldName]['locationId'] : '';
        } else if (gridFieldName == 'dtoSupervisor') {
            return row[gridFieldName] && row[gridFieldName]['superVisionCode'] ? row[gridFieldName]['superVisionCode'] : '';
        } else if (gridFieldName == 'activitiInactive') {
            return row[gridFieldName] == true ? "Inactive" : "Active";
        } else if (gridFieldName == 'activitiImmigration') {
            return row[gridFieldName] == true ? "Yes" : "No";
        }else if (gridFieldName == 'activitiSmoker') {
            return row[gridFieldName] == true ? "Yes" : "No";
        }else if (gridFieldName == 'activitiCitizen') {
            return row[gridFieldName] == true ? "Yes" : "No";
        }
        else {
            return row[gridFieldName];
        }

    }



    //delete traningCourse by passing whole object of perticular TrainingCourse
    delete() {
        var selectedEmp = [];
        //console.log(this.selected);
        for (var i = 0; i < this.selected.length; i++) {
            selectedEmp.push(this.selected[i].activitiIndexId);
        }
        //console.log(selectedEmp);

        this.activitiMasterService.deleteActivitiMaster(selectedEmp).then(data => {
            var datacode = data.code;
            if (datacode == 200) {

            }
            //Refresh the Grid data after deletion of division
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.hasMessage = true;
            this.message.type = "success";
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);



        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }
    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    _keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }



}
