"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var router_1 = require("@angular/router");
var paycode_setup_service_1 = require("../../_services/paycode-setup/paycode-setup.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var PaycodeSetupComponent = (function () {
    function PaycodeSetupComponent(router, paycodeSetupService, getScreenDetailService, alertService) {
        this.router = router;
        this.paycodeSetupService = paycodeSetupService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1414';
        this.message = { 'type': '', 'text': '' };
        this.payCodeId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.unitofPayArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually", "Daily/Miscellaneous"];
        this.pattern = /^\d*\.?\d{0,3}$/;
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'PAYCODE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAY_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_PAY_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_PAY_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_UNIT_OF_PAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_BASE_ON_PAYCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_PAY_FACTOR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_PAY_RATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PAYCODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    PaycodeSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    PaycodeSetupComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        }
        else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        }
        else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.paycodeSetupService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create location
    PaycodeSetupComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            payCodeId: '',
            description: '',
            arbicDescription: '',
            payType: '',
            baseOnPayCode: '',
            baseOnPayCodeAmount: '',
            payFactor: '',
            payRate: '',
            unitofPay: '',
            payperiod: '',
            inActive: false,
        };
    };
    // Clear form to reset to default blank
    PaycodeSetupComponent.prototype.Clear = function (f) {
        f.resetForm({ payType: '',
            baseOnPayCode: '',
            unitofPay: '',
            payperiod: '' });
    };
    //function call for creating new location
    PaycodeSetupComponent.prototype.CreatePayCode = function (f, event) {
        var _this = this;
        event.preventDefault();
        var supIdx = this.model.payCodeId;
        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.paycodeSetupService.checkDuplicatePayCode(supIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message + ' !';
                    }, 100);
                }
                else {
                    //Call service api for Creating new location
                    _this.paycodeSetupService.createPaycode(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new location
                            _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin !';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin !';
                }, 100);
            });
        }
    };
    //edit department by row
    PaycodeSetupComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.payCodeId = row.payCodeId;
        this.isUnderUpdate = true;
        this.payCodeIdvalue = this.model.payCodeId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    PaycodeSetupComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        //Call service api for updating selected department
        this.model.payCodeId = this.payCodeIdvalue;
        this.paycodeSetupService.updatePaycode(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin !';
            }, 100);
        });
    };
    PaycodeSetupComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    PaycodeSetupComponent.prototype.delete = function () {
        var _this = this;
        var selectedSupervisors = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSupervisors.push(this.selected[i].id);
        }
        this.paycodeSetupService.deletePaycode(selectedSupervisors).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message + " !";
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message + ' !';
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0, sortOn: _this.page.sortOn, sortBy: _this.page.sortBy });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    };
    // default list on page
    PaycodeSetupComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    PaycodeSetupComponent.prototype.updateFilter = function (event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.supervisorService.searchSupervisorlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    };
    // Set default page size
    PaycodeSetupComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    };
    /*confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }*/
    PaycodeSetupComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    PaycodeSetupComponent.prototype.sortColumn = function (val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            }
            else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    };
    return PaycodeSetupComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], PaycodeSetupComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], PaycodeSetupComponent.prototype, "myScrollContainer", void 0);
PaycodeSetupComponent = __decorate([
    core_1.Component({
        selector: 'paycode-setup',
        templateUrl: './paycode-setup.component.html',
        providers: [paycode_setup_service_1.PaycodeSetupService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        paycode_setup_service_1.PaycodeSetupService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], PaycodeSetupComponent);
exports.PaycodeSetupComponent = PaycodeSetupComponent;
//# sourceMappingURL=paycode-setup.component.js.map