import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { EmployeeContacts } from '../../_models/employee-contacts/employee-contacts.module';
import { EmployeeContactsService } from '../../_services/employee-contacts/employee-contacts.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
@Component({
    selector: 'employee-contacts',
    templateUrl: './employee-contacts.component.html',
    providers: [EmployeeContactsService,CommonService]
})
export class EmployeeContactsComponent {
    page = new Page();
    rows = new Array<EmployeeContacts>();
    temp = new Array<EmployeeContacts>();
    selected = [];
    selectedEmp = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1458';
    moduleName;
    screenName;
    skillsetupindexdesc;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    skillId = {};
    skillSetIndexId: any = [];
    skillSetId = {};
    employeeIdOptions = [];
    searchKeyword = '';
    ddPageSize: number = 5;
    model: any;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean= false;
    hasMsg = false;
    showMsg = false;
    tempp: string[] = [];
    islifeTimeValid: boolean = true;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    skillIdvalue: string;
    getSetId: any[] = [];
    setIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    skillSetDescId: any;
    skillsetsetupdesc: any[] = [];
    skillsetdesparr: any[] = [];
    skillSetsetId: any = [];
    employeeIndexId: any;
    empLastName: any;
    empFirstName: any;
    empMidleName: any;
    employeeDependentsId: any;
    contactName;
    contactNameArbic;
    relation;
    homePhone;
    mobileNo;
    workNo;
    extention;
    address: any;
    phoneNumber: any;
    workNumber: any;
    city: any;
    comments: any;
    gender: any;
    dateOfBirth: any;
    dateofBirth: any;
    agevalid:boolean=true;
    age: number;
    emcmname:any;
    employeeId:any;
    submitted:boolean=false;
    readFlage:boolean=false;
    empDependent: any[] = [];
    phonePattern = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    employyeIdValid:boolean=false;
    noPrintTable:boolean=true;
    frequencyArray = ["Hourly", "Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually"];
    skillrequiredpara = { "true": "Yes", "false": "No" };
    genderArry = ['', 'Male', 'Female'];
    relationship=[];
    doNotPrintHearder:boolean = false;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    SEARCH:any;
    EMPLOYEE_ID:any;
    EMPLOYEE_CONTACT_NAME:any;
    EMPLOYEE_CONTACT_NAME_ARABIC:any;
    RELATIONSHIP:any;
    HOME_PHONE:any;
    MOBILE_PHONE:any;
    WORK_PHONE:any;
    ADDRESS:any;
    EXT:any;
    EMPLOYEE_DEPENDENTS_COMMENT:any;
    EMPLOYEE_DEPENDENTS_GENDER:any;
    EMPLOYEE_DEPENDENTS_DATE_OG_BIRTH:any;
    EMPLOYEE_DEPENDENTS_EDIT:any;
    EMPLOYEE_CONTACTS_CREATE:any;
    EMPLOYEE_CONTACTS_UPDATE:any;
    EMPLOYEE_CONTACTS_CLEAR:any;
    EMPLOYEE_CONTACTS_CANCLE:any;
    EMPLOYEE_CONTACTS_SAVE:any;
    EMPLOYEE_CONTACTS_CREATE_FORM:any;
    EMPLOYEE_CONTACTS_UPDATE_FORM:any;
    EMPLOYEE_CONTACTS_DELETE:any;
    ACTION:any;
    SITE_SETUP_ACTION:any;

    constructor(private router: Router,
        private EmployeeDependentsService: EmployeeContactsService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,private commonService:CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        this.model = { listContact: [] };
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACT_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACT_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'RELATIONSHIP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HOME_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MOBILE_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'WORK_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDRESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EXT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_COMMENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_GENDER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_DATE_OG_BIRTH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_EDIT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACTS_CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACTS_UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACTS_CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACTS_CANCLE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACTS_SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACTS_CREATE_FORM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_CONTACTS_UPDATE_FORM', 'fieldValue': '', 'helpMessage': '' },

            { 'fieldName': 'EMPLOYEE_CONTACTS_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SITE_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
        ];

            this.SEARCH=this.defaultFormValues[0];
            this.EMPLOYEE_ID=this.defaultFormValues[1];
            this.EMPLOYEE_CONTACT_NAME=this.defaultFormValues[2];
            this.EMPLOYEE_CONTACT_NAME_ARABIC=this.defaultFormValues[3];
            this.RELATIONSHIP=this.defaultFormValues[4];
            this.HOME_PHONE=this.defaultFormValues[5];
            this.MOBILE_PHONE=this.defaultFormValues[6];
            this.WORK_PHONE=this.defaultFormValues[7];
            this.ADDRESS=this.defaultFormValues[8];
            this.EXT=this.defaultFormValues[9];
            this.EMPLOYEE_DEPENDENTS_COMMENT=this.defaultFormValues[10];
            this.EMPLOYEE_DEPENDENTS_GENDER=this.defaultFormValues[11];
            this.EMPLOYEE_DEPENDENTS_DATE_OG_BIRTH=this.defaultFormValues[12];
            this.EMPLOYEE_DEPENDENTS_EDIT=this.defaultFormValues[13];
            this.EMPLOYEE_CONTACTS_CREATE=this.defaultFormValues[14];
            this.EMPLOYEE_CONTACTS_UPDATE=this.defaultFormValues[15];
            this.EMPLOYEE_CONTACTS_CLEAR=this.defaultFormValues[16];
            this.EMPLOYEE_CONTACTS_CANCLE=this.defaultFormValues[17];
            this.EMPLOYEE_CONTACTS_SAVE=this.defaultFormValues[18];
            this.EMPLOYEE_CONTACTS_CREATE_FORM=this.defaultFormValues[19];
            this.EMPLOYEE_CONTACTS_UPDATE_FORM=this.defaultFormValues[20];
            this.EMPLOYEE_CONTACTS_DELETE=this.defaultFormValues[21];
            this.ACTION=this.defaultFormValues[22];
            this.SITE_SETUP_ACTION=this.defaultFormValues[23];

        this.setIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.employeeId);
          }).mergeMap((token: string) => this.getSetIdAsObservable(token));
    }

    ngOnInit() {
        // this.skillSetsetId.forEach(element => {
        //     this.model.listSkillSteup.push({id:element});
        // });

        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
        
        //Following shifted from SetPage for better performance   
        this.EmployeeDependentsService.getRelationDropdown().then(data => {
            this.relationship = data.result;
            //console.log("Relation:", this.relationship);
        });

        this.EmployeeDependentsService.getEmployeeIdList().then(data => {
            this.employeeIdOptions = data.result;
            //console.log('test', this.employeeIdOptions);
        });
    }
    getSetIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.employeeIdOptions.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
       //console.log('Selected value: ', e);
        this.employeeIndexId=e.item.employeeIndexId;
        this.employeeId=e.item.employeeId;
        if(this.employeeId=!undefined &&  this.employeeId==e.item.employeeId){
            this.employyeIdValid=true;
            this.employeeId=e.item.employeeId;
           //console.log('valid',this.employyeIdValid);
            this.EmployeeDependentsService.getEmployeeContact(this.page,this.employeeIndexId).then(data=>{
                this.model.listContact=[];
                this.empDependent=[];
                if(data.status!="NOT_FOUND"){
                
                this.model.listContact=data.result.records;
               //console.log('listcontact',this.model.listContact);
                this.empDependent = Object.assign([], this.model.listContact);
                }
            })
        }
        else{
            this.employyeIdValid=false;
           //console.log('valid',this.employyeIdValid)
        }
        this.emcmname=e.item.employeeFirstName+' '+e.item.employeeMiddleName+' '+e.item.employeeLastName;
    }
    checkValid1(event){
        if(event.target.value == ''){
            this.employyeIdValid = true;
            
        } else{
            for(var i=0;i<this.employeeIdOptions.length;i++){
                if(this.employeeIdOptions[i].employeeId.includes(event.target.value)){
                    this.employyeIdValid = true;
                   
                    break;
                } else{
                    
                    this.employyeIdValid = false;
                }
            }


        }
}

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.EmployeeDependentsService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
           
           //console.log('all data',this.rows);
        });
    }

    // Open form for create location
    Create() {
        this.employeeId='';
        this.submitted = false;
        this.contactName='';
        this.contactNameArbic='';
        this.relation='';
        this.homePhone='';
        this.mobileNo='';
        this.workNo='';
        this.extention='';
        this.address='';
        this.emcmname='';
        this.model.listContact=[];
        this.empDependent=[];
        this.skillSetsetId = [];
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.employyeIdValid=false;
        this.readFlage=false;
        setTimeout(() => {
            this.showCreateForm = true;
            this.doNotPrintHearder = false;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        // console.log('upper', this.model.listContact);
        // this.model.listContact = [];
        // console.log('niche', this.model.listContact);
        
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
      // f.resetForm({ gender:0,employeeDependentsId:0});
        this.contactName='';
        this.contactNameArbic='';
        this.relation='';
        this.homePhone='';
        this.mobileNo='';
        this.workNo='';
        this.extention='';
        this.address='';
        //this.model.listEmployeeDependent=[];
        this.empDependent=[];
        this.submitted=false;
        this.readFlage=false;
        this.doNotPrintHearder = false;
    }


    //function call for creating new location
    CreateSkillsSetup(f: NgForm, event: Event) {
        
        event.preventDefault();
        this.doNotPrintHearder = false;
        var locIdx = this.model.listContact[0].id;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.listContact[0].id > 0 && this.model.listContact[0].id != 0 && this.model.listContact[0].id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            // this.skillSetsetId.forEach(element => {
            //     this.model.listSkillSteup.push({id:element});
            // });
            this.EmployeeDependentsService.checkDuplicateSkillsSetupId(locIdx).then(response => {


                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code


                    this.EmployeeDependentsService.createEmployeeDependent(this.model).then(data => {

                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            this.skillsetupindexdesc = '';
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }
    moveToAllEmployee() {
        
        this.model.listContact.push({
            employeeMaster: { employeeIndexId: this.employeeIndexId },
            employeeContactName: this.contactName,
            employeeContactNameArabic: this.contactNameArbic,
            employeeContactNameRelationship: this.relation,
            employeeContactHomePhone: this.homePhone,
            employeeContactMobilePhone: this.mobileNo,
            employeeContactWorkPhone:this.workNo,
            employeeContactWorkPhoneExt:this.extention,
            employeeContactAddress: this.address
            
        });
        this.empDependent = [];
        
        this.empDependent = Object.assign([], this.model.listContact);
       //console.log("Inserted Value:", this.empDependent['employeeDependentsId']);
       //console.log('listobj', this.model.listContact);
        this.submitted = false;
        this.contactName='';
        this.contactNameArbic='';
        this.relation='';
        this.homePhone='';
        this.mobileNo='';
        this.workNo='';
        this.extention='';
        this.address='';
        this.readFlage=true;

    }
    moveToSelected() {

       //console.log("selected", this.selected);
        for (var i = 0; i < this.model.listContact.length; i++) {

            if (this.model.listContact[i].contactName == this.selected[0].contactName && this.model.listContact[i].relation == this.selected[0].relation
                && this.model.listContact[i].homePhone == this.selected[0].homePhone && this.model.listContact[i].mobileNo == this.selected[0].mobileNo) 
                {
                    this.model.listContact.splice(i,1);
                }
        }
        this.empDependent=[];
        this.empDependent = Object.assign([], this.model.listContact);
        this.selected=[];
    }

    //edit department by row
    // editcheckbox(row: EmployeeDependents,event) {        
    //     this.model = Object.assign({},row);
    // 	this.model.skillRequired =  event.target.checked;
    // 	this.skillSetId = row.skillSetId;
    //     this.skillSetDescId = this.model.skillSetDescId;
    // 	this.skillIdvalue = this.model.skillSetId;
    // 	this.isConfirmationModalOpen = true;
    //     this.isDeleteAction = false;
    // 	//console.log('model == ',this.model);
    // }
    //edit department by row
    edit(row: any) {
        this.model.listContact=[];
        this.employyeIdValid=true;
        this.submitted = true;
       //console.log(row);
        this.showCreateForm = true;
        this.doNotPrintHearder = false;
        this.model.listContact[0] = Object.assign({}, row);
        this.model.listContact[0].id=row.id;
         this.employeeIndexId = row.employeeMaster.employeeIndexId;
         this.employeeId=row.employeeMaster.employeeId;
        //console.log('this.employeeIndexId',this.employeeIndexId);
        this.isUnderUpdate = true;
        this.skillIdvalue = this.model.listContact[0].id;
        this.contactName = this.model.listContact[0].employeeContactName;
        this.contactNameArbic = this.model.listContact[0].employeeContactNameArabic;
        this.relation = this.model.listContact[0].employeeContactNameRelationship;
        this.homePhone = this.model.listContact[0].employeeContactHomePhone;
        this.mobileNo = this.model.listContact[0].employeeContactMobilePhone;
        this.workNo = this.model.listContact[0].employeeContactWorkPhone;
        this.extention = this.model.listContact[0].employeeContactWorkPhoneExt;
        this.address = this.model.listContact[0].employeeContactAddress;
        
        this.emcmname = this.model.listContact[0].employeeMaster.employeeFirstName+' '+this.model.listContact[0].employeeMaster.employeeMiddleName+' '+this.model.listContact[0].employeeMaster.employeeLastName;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);

    }

    updateStatus() {
        this.submitted=false;
        this.doNotPrintHearder = false;
        this.closeModal();
        this.model.listContact[0].id = this.skillIdvalue;
        this.model.listContact[0].employeeMaster.employeeIndexId = this.employeeIndexId;
        this.model.listContact[0].employeeMaster.employeeId = this.employeeId;
        this.model.listContact[0].employeeContactName =  this.contactName;
        this.model.listContact[0].employeeContactNameArabic = this.contactNameArbic;
        this.model.listContact[0].employeeContactNameRelationship = this.relation;
        this.model.listContact[0].employeeContactHomePhone = this.homePhone;
        this.model.listContact[0].employeeContactMobilePhone = this.mobileNo;
        this.model.listContact[0].employeeContactWorkPhone =  this.workNo;
        this.model.listContact[0].employeeContactWorkPhoneExt = this.extention;
        this.model.listContact[0].employeeContactAddress = this.address;
       
        

        //Call service api for updating selected department
        this.EmployeeDependentsService.updateEmployeeDependent(this.model).then(data => {

            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.skillsetupindexdesc = '';
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000)
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedSkillsSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSkillsSetups.push(this.selected[i].id);
        }
        this.EmployeeDependentsService.deleteEmployeeDependent(selectedSkillsSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }
    onSelectEmpDep({ selectedEmp }) {
        this.selectedEmp.splice(0, this.selectedEmp.length);
        this.selectedEmp.push(...selectedEmp);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.SkillsSetupService.searchSkillsSetuplist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    // CheckNumber(event) {
    //     if (isNaN(event.target.value) == true) {
    //         //this.model.skillId = 0;
    //         this.model.compensation = 1;
    //         return false;
    //     }
    // }
    // CheckNumberNonZero(event) {
    // 	if (event.target.value <= 0) {
    //         this.model.compensation = 1;
    //         return false;
    //     }
    // }
    // checkFrequency(event) {
    //     this.skillSetsetId.forEach(element => {
    //         this.skillsetdesparr = [];
    //         console.log('id', element);
    //         this.EmployeeDependentsService.getSkillSetupById(element).then(pagedData => {
    //             //this.skillsetupindexdesc = pagedData.result.skillDesc;
    //             this.skillsetdesparr.push(pagedData.result.skillDesc);
    //             this.skillsetupindexdesc = this.skillsetdesparr.join();
    //         });
    //         if (event.target.value == 0) {
    //             return false;
    //         }
    //     });
    // }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkdecimal(digit) {
       //console.log(digit);
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValid = false;
           //console.log(this.islifeTimeValid);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValid = false;
           //console.log(this.islifeTimeValid);
        }

        else {
            this.islifeTimeValid = true;
           //console.log(this.islifeTimeValid);
        }

    }
    updateSkillCheckbox(val1, val2) {
        console.log('val2 == ', val2);
        console.log('val2 == ', val2);
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onStartDateChanged(event: IMyDateModel): void {
        var datebirth = event.jsdate;
        this.dateOfBirth = event.jsdate;
        var todayd =  new Date(this.dateOfBirth);
        var dd = todayd.getDay();

        var mm = todayd.getMonth()+1; 
        var yyyy = todayd.getFullYear();
        if(dd<10) 
        {
        var  ddd='0'+dd;
        } 

        if(mm<10) 
        {
        var mmm='0'+mm;
        } 
        var todaydd = yyyy+'-'+mmm+'-'+ddd;
        
       
        var today = new Date();
        var birthDate = new Date(datebirth);
       //console.log('birthyear', birthDate.getFullYear());
        var curage = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        // if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        //     curage--;
        // }
        this.age = curage;
        if(this.age<=0){
            this.agevalid=false;
        }
        else{
            this.agevalid=true;
        }

       //console.log('this.frmStartDate', this.dateOfBirth);
        
    }
    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=100%,scale=100');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
              <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
              <style>
              @media print{
                .doNotPrint{display:none;!important}
              }
              @page { size: landscape; 
                 margin-top: 25px;
                margin-bottom: 250px;
                margin-right: 0px;
                margin-left: 0px;
                -webkit-transform: scale(0.5);  /* Saf3.1+, Chrome */
                -moz-transform: scale(0.5);  /* FF3.5+ */
                -ms-transform: scale(0.5);  /* IE9 */
                -o-transform: scale(0.5);  /* Opera 10.5+ */
                transform: scale(0.5);  }
            
              
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }

    printEmployeeContactTable(){
        this.noPrintTable = false;
        this.doNotPrintHearder = true;
        
        setTimeout(()=> { this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint(){
        this.noPrintTable = true;
        this.doNotPrintHearder = false;
    }

}
