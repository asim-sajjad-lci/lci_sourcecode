import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { Router } from '@angular/router';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { Observable } from 'rxjs';
import { LoanAndAdvancePayments } from '../../_models/loan-and-advance/loan-and-advance';
import { LoanAndAdvanceService } from '../../_services/loan-and-advance/loan-and-advance.service';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { Page } from '../../../_sharedresource/page';
import { LoanAndAdvancePaymentsDetailsGrid } from '../../_models/loan-and-advance/loan-and-advance-details-grid';
import { DatePipe } from '@angular/common';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-loan-and-advance',
  templateUrl: './loan-and-advance.component.html',
  styleUrls: ['./loan-and-advance.component.css'],
  providers: [CommonService, LoanAndAdvanceService]
})
export class LoanAndAdvanceComponent implements OnInit {

  moduleCode = "M-1011";
  screenCode = "S-1789";
  defaultFormValues: object[];

  page = new Page();
  page1 = new Page();
  page2 = new Page();
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  rows;
  detailsRows;

  selected;
  isDeductionAmtError = false;

  isGridError = false;

  currentLanguage: any;
  hasMessage;
  isDetailModalOpen: boolean = false;

  LOANANDADVANCE_REFRENCENUMBER: any;
  LOANANDADVANCE_TRANSACTIONDATE: any;
  LOANANDADVANCE_EMPLOYEEID: any;
  LOANANDADVANCE_AMOUNTREQUESTED: any;
  LOANANDADVANCE_DEDUCTIONPERMONTH: any;
  LOANANDADVANCE_TOTALNUMBEROFMONTHS: any;
  LOANANDADVANCE_STARTINGFROM: any;
  LOANANDADVANCE_EMPLOYEENAME: any;
  LOANANDADVANCE_HIRINGDATE: any;
  LOANANDADVANCE_POSITION: any;
  LOANANDADVANCE_SITE: any;
  LOANANDADVANCE_TOTALPACKAGE: any;
  LOANANDADVANCE_PREVIOUSLOAN: any;
  LOANANDADVANCE_TOTALDEDUCTION: any;
  LOANANDADVANCE_TOTALREMAINING: any;
  LOANANDADVANCE_CURRENTESB: any;
  LOANANDADVANCE_SAVE: any;
  LOANANDADVANCE_ACTIVATE: any;
  LOANANDADVANCE_PRINT: any;
  LOANANDADVANCE_CANCEL: any;

  myOptions: INgxMyDpOptions = {
    // other options...
    dateFormat: 'dd-mm-yyyy',
    // monthSelector:false,
  };

  employeeIdList: Observable<any>;
  getEmployee: any[] = [];

  model: LoanAndAdvancePayments;
  typeaheadLoading: boolean;
  employeeId;
  startingMonth = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  startingYear = [2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030];

  transsactionDate;
  searchKeyword = "";

  loaderImg: boolean = false;
  ddPageSize: number = 5;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  serialNoDisplayArr: any[] = [];

  constructor(
    private commonService: CommonService,
    private router: Router,
    private Service: LoanAndAdvanceService,
  ) {
    this.defaultFormValues = [
      { 'fieldName': 'LOANANDADVANCE_REFRENCENUMBER', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_TRANSACTIONDATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_EMPLOYEEID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_AMOUNTREQUESTED', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_DEDUCTIONPERMONTH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_TOTALNUMBEROFMONTHS', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_STARTINGFROM', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_EMPLOYEENAME', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_HIRINGDATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_POSITION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_SITE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_TOTALPACKAGE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_PREVIOUSLOAN', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_TOTALDEDUCTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_TOTALREMAINING', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_CURRENTESB', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_SAVE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_ACTIVATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_PRINT', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'LOANANDADVANCE_CANCEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    ];

    this.LOANANDADVANCE_REFRENCENUMBER = this.defaultFormValues[0];
    this.LOANANDADVANCE_TRANSACTIONDATE = this.defaultFormValues[1];
    this.LOANANDADVANCE_EMPLOYEEID = this.defaultFormValues[2];
    this.LOANANDADVANCE_AMOUNTREQUESTED = this.defaultFormValues[3];
    this.LOANANDADVANCE_DEDUCTIONPERMONTH = this.defaultFormValues[4];
    this.LOANANDADVANCE_TOTALNUMBEROFMONTHS = this.defaultFormValues[5];
    this.LOANANDADVANCE_STARTINGFROM = this.defaultFormValues[6];
    this.LOANANDADVANCE_EMPLOYEENAME = this.defaultFormValues[7];
    this.LOANANDADVANCE_HIRINGDATE = this.defaultFormValues[8];
    this.LOANANDADVANCE_POSITION = this.defaultFormValues[9];
    this.LOANANDADVANCE_SITE = this.defaultFormValues[10];
    this.LOANANDADVANCE_TOTALPACKAGE = this.defaultFormValues[11];
    this.LOANANDADVANCE_PREVIOUSLOAN = this.defaultFormValues[12];
    this.LOANANDADVANCE_TOTALDEDUCTION = this.defaultFormValues[13];
    this.LOANANDADVANCE_TOTALREMAINING = this.defaultFormValues[14];
    this.LOANANDADVANCE_CURRENTESB = this.defaultFormValues[15];
    this.LOANANDADVANCE_SAVE = this.defaultFormValues[16];
    this.LOANANDADVANCE_ACTIVATE = this.defaultFormValues[17];
    this.LOANANDADVANCE_PRINT = this.defaultFormValues[18];
    this.LOANANDADVANCE_CANCEL = this.defaultFormValues[19];


    this.employeeIdList = Observable.create((observer: any) => {

      observer.next(this.model.employeeId);
    }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

    this.model = {
      id: 0,
      refId: "",
      transDate: "",
      employeeId: "",
      loanAmt: null,
      deductionPerMonthDefault: null,
      numOfMonths: null,
      startingFromMonth: "",
      startingFromYear: "",
      employeeName: "",
      hiringDate: "",
      position: "",
      site: "",
      totPackage: "",
      prevLoan: "",
      totDeduction: "",
      totRemaining: "",
      currentESB: "",
      transactionDate: "",
      startDate: "",
      endDate: "",
      employeeIdx: 0,
      loanDetailsList: []
    }


    this.rows = [];
    this.detailsRows = [];
    this.selected = [];
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'id';
    this.page.sortBy = 'DESC';

    this.page1.pageNumber = 0;
    this.page1.size = 5;
    this.page1.sortOn = 'id';
    this.page1.sortBy = 'DESC';

    this.page2.pageNumber = 0;
    this.page2.size = 5;
    this.page2.sortOn = 'id';
    this.page2.sortBy = 'DESC';
  }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    console.log("default", this.defaultFormValues);

    this.Service.getEmployee().then(data => {

      this.getEmployee = data.result;

    });
  }

  Create() {
    this.showCreateForm = !this.showCreateForm;
    this.transsactionDate = "";
    this.model = {
      id: 0,
      refId: "",
      transDate: "",
      employeeId: "",
      loanAmt: null,
      deductionPerMonthDefault: null,
      numOfMonths: null,
      startingFromMonth: "",
      startingFromYear: "",
      employeeName: "",
      hiringDate: "",
      position: "",
      site: "",
      totPackage: "",
      prevLoan: "",
      totDeduction: "",
      totRemaining: "",
      currentESB: "",
      transactionDate: "",
      startDate: "",
      endDate: "",
      employeeIdx: 0,
      loanDetailsList: []
    }

    this.page2.pageNumber = 0;
    this.page2.size = 5;
    this.page2.sortOn = 'id';
    this.page2.sortBy = 'DESC';
    this.page2.totalElements = 0;

    this.detailsRows = [];

    this.Service.getEmployee().then(data => {

      this.getEmployee = data.result;

    });
  }

  getEmployeeIdAsObservable(token: string): Observable<any> {

    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getEmployee.filter((id: any) => {

        return query.test(id.employeeId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
    this.employeeId = 0;

  }

  typeaheadOnSelect(e: TypeaheadMatch): void {

    console.log('Selected value: ', e.item);

    this.Service.getEmployeeDetails(e.item.employeeIndexId).then(data => {

      this.model.employeeIdx = data.result.employeeIndexId;
      this.model.employeeName = data.result.empFullName;
      let hireDt = new DatePipe('en-US').transform(data.result.employeeHireDate, 'yyyy-MM-dd')

      this.model.hiringDate = hireDt;
      this.model.position = data.result.positionName;
      this.model.site = data.result.locationName;
      this.model.totPackage = data.result.totalPackage;
      this.model.prevLoan = data.result.previousLoan;
      this.model.totDeduction = data.result.totalDeduction;
      this.model.totRemaining = data.result.totalRemaining;
      this.model.currentESB = data.result.currentESB;
    });
  }
  onAmountChange() {

    this.isDeductionAmtError = false;
    if (this.model.deductionPerMonthDefault > this.model.loanAmt) {
      this.isDeductionAmtError = true;
    }
    else {
      this.model.loanDetailsList = [];
      this.model.numOfMonths = Number(this.model.loanAmt / this.model.deductionPerMonthDefault)

      
    this.model.loanDetailsList = [];
    let val = this.checkMonths();

    if (val != 0) {
      if (this.model.startingFromMonth != "" && this.model.startingFromYear != "") {
     //   if (this.model.startingFromYear != "") {
          let numOfMonths = Math.floor(this.model.numOfMonths)
          let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
          let newDate = new Date(dateString);
          let totAmt = 0;
          for (let i = 0; i < numOfMonths; i++) {
            totAmt += this.model.deductionPerMonthDefault;
            if (i == 0) {
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }
            else {
              let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }

          }

          for (let j = 0; j < 1; j++) {
            let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
            let amt = this.model.loanAmt - totAmt;
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), amt.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }

     //   }
      }
    }
    else {


      if (this.model.startingFromMonth != "" && this.model.startingFromYear != "") {
        let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
        let newDate = new Date(dateString);
        for (let i = 0; i < this.model.numOfMonths; i++) {

          if (i == 0) {
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }
          else {
            let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }

        }
      }
    }
    }
  }

  // onAmountChange() {

  //   this.isDeductionAmtError = false;
  //   if (this.model.deductionPerMonthDefault > this.model.loanAmt) {
  //     this.isDeductionAmtError = true;
  //   }
  //   else {
  //     this.model.loanDetailsList = [];
  //     this.model.numOfMonths = Number(this.model.loanAmt / this.model.deductionPerMonthDefault)

  //     if (this.model.startingFromMonth != "" && this.model.startingFromYear != "") {
  //       let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
  //       let newDate = new Date(dateString);
  //       for (let i = 0; i < this.model.numOfMonths; i++) {

  //         if (i == 0) {
  //           let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
  //           this.model.loanDetailsList.push(rec)
  //         }
  //         else {
  //           let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
  //           let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
  //           this.model.loanDetailsList.push(rec)
  //         }

  //       }
  //     }
  //   }
  // }

  onMonthChange() {

    this.model.deductionPerMonthDefault = Number(this.model.loanAmt / this.model.numOfMonths)

    this.model.loanDetailsList = [];
    let val = this.checkMonths();

    if (val != 0) {
      if (this.model.startingFromMonth != "" && this.model.startingFromYear != "") {
     //   if (this.model.startingFromYear != "") {
          let numOfMonths = Math.floor(this.model.numOfMonths)
          let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
          let newDate = new Date(dateString);
          let totAmt = 0;
          for (let i = 0; i < numOfMonths; i++) {
            totAmt += this.model.deductionPerMonthDefault;
            if (i == 0) {
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }
            else {
              let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }

          }

          for (let j = 0; j < 1; j++) {
            let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
            let amt = this.model.loanAmt - totAmt;
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), amt.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }

     //   }
      }
    }
    else {

    if (this.model.startingFromMonth != "" && this.model.startingFromYear != "") {
      let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
      let newDate = new Date(dateString);
      for (let i = 0; i < this.model.numOfMonths; i++) {

        if (i == 0) {
          let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
          this.model.loanDetailsList.push(rec)
        }
        else {
          let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
          let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
          this.model.loanDetailsList.push(rec)
        }

      }
    }
    }
  }
  // onMonthChange() {

  //   this.model.loanDetailsList = [];
  //   this.model.deductionPerMonthDefault = Number(this.model.loanAmt / this.model.numOfMonths)

  //   if (this.model.startingFromMonth != "" && this.model.startingFromYear != "") {
  //     let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
  //     let newDate = new Date(dateString);
  //     for (let i = 0; i < this.model.numOfMonths; i++) {

  //       if (i == 0) {
  //         let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
  //         this.model.loanDetailsList.push(rec)
  //       }
  //       else {
  //         let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
  //         let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
  //         this.model.loanDetailsList.push(rec)
  //       }

  //     }
  //   }
  // }

  Save(form, event) {

    this.loaderImg = true;
    this.model.transactionDate = new DatePipe('en-US').transform(this.transsactionDate.jsdate, 'yyyy-MM-dd')
    let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear;
    const startdateSendingToServer = new DatePipe('en-US').transform(dateString, 'yyyy-MM-dd')
    this.model.startDate = startdateSendingToServer;
    let enddateString = this.model.loanDetailsList[this.model.loanDetailsList.length - 1].monthYear;
    const enddateSendingToServer = new DatePipe('en-US').transform(enddateString, 'yyyy-MM-dd')
    this.model.endDate = enddateSendingToServer;
    console.log("Save", this.model);

    this.Service.saveLoanAndPayment(this.model).then(data => {

      this.loaderImg = false;
      var datacode = data.code;
      if (datacode == 201) {
        window.scrollTo(0, 0);
        window.setTimeout(() => {
          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          this.hasMessage = false;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          this.messageText = "Record Save Successfully";
        }, 100);

        this.hasMsg = true;
        this.Create();
        //  f.resetForm();
        //Refresh the Grid data after adding new LoanAndPayment
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = "Server error. Please contact admin.";
      }, 100)
    });

  }

  onStartMonthChange() {

    this.model.loanDetailsList = [];
    let val = this.checkMonths();

    if (val != 0) {
      if (this.model.numOfMonths > 0 && this.model.numOfMonths != null) {
        if (this.model.startingFromYear != "") {
          let numOfMonths = Math.floor(this.model.numOfMonths)
          let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
          let newDate = new Date(dateString);
          let totAmt = 0;
          for (let i = 0; i < numOfMonths; i++) {
            totAmt += this.model.deductionPerMonthDefault;
            if (i == 0) {
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }
            else {
              let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }

          }

          for (let j = 0; j < 1; j++) {
            let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
            let amt = this.model.loanAmt - totAmt;
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), amt.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }

        }
      }
    }
    else {
    if (this.model.numOfMonths > 0 && this.model.numOfMonths != null) {
      if (this.model.startingFromYear != "") {
        let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
        let newDate = new Date(dateString);
        for (let i = 0; i < this.model.numOfMonths; i++) {

          if (i == 0) {
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }
          else {
            let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }

        }
      }
    }
    }
  }

  checkMonths() {
    if (Math.floor(this.model.numOfMonths) === this.model.numOfMonths) return 0;
    return this.model.numOfMonths.toString().split(".")[1].length || 0;
  }

  onStartYearChange() {

    this.model.loanDetailsList = [];
    //let val = this.model.numOfMonths.toString().split(".")[1].length || 0;
    let val = this.checkMonths();
    // alert(val);
    if (val != 0) {
      if (this.model.numOfMonths > 0 && this.model.numOfMonths != null) {
        if (this.model.startingFromMonth != "") {
          let numOfMonths = Math.floor(this.model.numOfMonths)
          let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
          let newDate = new Date(dateString);
          let totAmt = 0;
          for (let i = 0; i < numOfMonths; i++) {
            totAmt += this.model.deductionPerMonthDefault;
            if (i == 0) {
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }
            else {
              let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }

          }

          for (let j = 0; j < 1; j++) {
            let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
            let amt = this.model.loanAmt - totAmt;
            let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), amt.toString(), false, 2, false);
            this.model.loanDetailsList.push(rec)
          }

        }
      }
    }
    else {
      if (this.model.numOfMonths > 0 && this.model.numOfMonths != null) {
        if (this.model.startingFromMonth != "") {
          let dateString = this.model.startingFromMonth + "-" + this.model.startingFromYear
          let newDate = new Date(dateString);
          for (let i = 0; i < this.model.numOfMonths; i++) {

            if (i == 0) {
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }
            else {
              let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
              let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.deductionPerMonthDefault.toString(), false, 2, false);
              this.model.loanDetailsList.push(rec)
            }

          }
        }
      }
    }

  }

  checkValue(rowNum, e) {
    if (e.target.checked == true) {
      let len = this.model.loanDetailsList.length - 1;
      let newDate = new Date(this.model.loanDetailsList[len].monthYear);
      let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
      let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), this.model.loanDetailsList[rowNum].deductionAmount.toString(), false, 2, false);
      this.model.loanDetailsList.push(rec)
      this.model.loanDetailsList[rowNum].status = 3;
      this.model.loanDetailsList[rowNum].deductionAmount = "0";
    }

  }

  calculateGridDeductionAmt(event, rowNum) {
    debugger;
    if (event.target.value != this.model.loanDetailsList[rowNum].deductionAmount) {
    //  if (event.target.value > this.model.deductionPerMonthDefault || event.target.value == 0) {
      if (Number(event.target.value) > Number(this.model.loanDetailsList[rowNum].deductionAmount) || Number(event.target.value) == 0) {
        this.isGridError = true;
        this.isfailureMsg = true;
        this.isSuccessMsg = false;
        this.showMsg = true
        window.setTimeout(() => {
          this.isGridError = false;

          this.isfailureMsg = false;
          this.isSuccessMsg = false;
          this.showMsg = false
          const element = document.getElementsByClassName('customAmt-' + rowNum);
          for (let j = 0; j < element.length; j++) {

            (<HTMLInputElement>element[j]).value = this.model.loanDetailsList[rowNum].deductionAmount.toString();;
          }
        }, 3000);
      }
      else {
        let len = this.model.loanDetailsList.length - 1;
        let newDate = new Date(this.model.loanDetailsList[len].monthYear);
        let newDate1 = new Date(newDate.setMonth(newDate.getMonth() + 1));
        let val = Number(this.model.loanDetailsList[rowNum].deductionAmount) - Number(event.target.value)
        let rec = new LoanAndAdvancePaymentsDetailsGrid(0, new DatePipe('en-US').transform(newDate1, 'yyyy-MM-dd'), val.toString(), false, 2, false);
        this.model.loanDetailsList.push(rec);
        this.model.loanDetailsList[rowNum].deductionAmount = event.target.value;
      }
    }

  }

  setPage(pageInfo) {
debugger;
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;

    if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
    } else {
      this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
    } else {
      this.page.sortBy = pageInfo.sortBy;
    }

    this.page.searchKeyword = '';
   // this.searchKeyword = '';
    this.Service.getlist(this.page, this.searchKeyword).subscribe(pagedData => {

      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

  setPage2(pageInfo) {

    this.selected = []; // remove any selected checkbox on paging
    this.page2.pageNumber = pageInfo.offset;
    this.page2.totalElements = 0;
    if (pageInfo.sortOn == undefined) {
      this.page2.sortOn = this.page2.sortOn;
    } else {
      this.page2.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page2.sortBy = this.page2.sortBy;
    } else {
      this.page2.sortBy = pageInfo.sortBy;
    }

    this.page2.searchKeyword = '';
    this.Service.getRemaininglist(this.model.employeeIdx, this.page2).subscribe(pagedData => {

      this.page2 = pagedData.page;
      this.detailsRows = pagedData.data;

      this.serialNoDisplayCalc(this.page2);
    });


  }


  edit(row) {
    console.log("row", row)

    this.Service.getLoanDetails(row.id).then(data => {

      this.model = data.result;

      this.model.startingFromMonth = new DatePipe('en-US').transform(this.model.startDate, 'MMM');
      this.model.startingFromYear = new DatePipe('en-US').transform(this.model.startDate, 'yyyy');
      this.transsactionDate = this.formatDateFordatePicker(this.model.transactionDate != null ? new Date(this.model.transactionDate) : null);

      for (let i = 0; i < this.model.loanDetailsList.length; i++) {
        if (this.model.loanDetailsList[i].isPostpone == false && this.model.loanDetailsList[i].isPaid == true) {
          this.model.loanDetailsList[i].status = 1;
        }
        else if (this.model.loanDetailsList[i].isPostpone == true && this.model.loanDetailsList[i].isPaid == false) {
          this.model.loanDetailsList[i].status = 3;
        }
        else {
          this.model.loanDetailsList[i].status = 2;
        }

      }

      this.Service.getEmployeeDetails(this.model.employeeIdx).then(data => {

        this.model.employeeId = data.result.employeeId;
        this.model.employeeIdx = data.result.employeeIndexId;
        this.model.employeeName = data.result.empFullName;
        let hireDt = new DatePipe('en-US').transform(data.result.employeeHireDate, 'yyyy-MM-dd')

        this.model.hiringDate = hireDt;
        this.model.position = data.result.positionName;
        this.model.site = data.result.locationName;
        this.model.totPackage = data.result.totalPackage;
        this.model.prevLoan = data.result.previousLoan;
        this.model.totDeduction = data.result.totalDeduction;
        this.model.totRemaining = data.result.totalRemaining;
        this.model.currentESB = data.result.currentESB;
      });
      this.showCreateForm = true;
      console.log("Model", this.model)
    });
  }

  formatDateFordatePicker(strDate: any): any {
    if (strDate != null) {
      var setDate = new Date(strDate);
      return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
    } else {
      return null;
    }
  }


  getRemaining() {
    this.isDetailModalOpen = true;
    this.setPage2({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  closeModal() {
    this.isDetailModalOpen = false;
  }

  changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
  }

  // Serial Number DisplayFunction
  serialNoDisplayCalc(page2) {
    // console.log('page2', page2)
    let tempLen: number = 0;
    if (page2.totalElements > page2.size) {
      tempLen = page2.size;
    } else {
      tempLen = page2.totalElements;
    }
    let serialNo: number = 0;
    this.serialNoDisplayArr = [];
    if (page2.pageNumber === 0) {
      while (tempLen > 0) {
        serialNo += 1;
        this.serialNoDisplayArr.push(serialNo);
        tempLen--;
      }
    } else {
      serialNo = 1 + page2.pageNumber * page2.size;
      tempLen = page2.totalElements - page2.pageNumber * page2.size
      while (tempLen > 0) {
        this.serialNoDisplayArr.push(serialNo);
        serialNo += 1;
        tempLen--;
      }
    }
    console.log("Serial", this.serialNoDisplayArr);
    return this.serialNoDisplayArr;

  }

}
