import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { NgForm } from '@angular/forms';
import { LifeInsuranceSetup } from '../../_models/life-insurance-setup/life-insurance-setup.module';
import { LifeInsuranceSetupService } from '../../_services/life-insurance-setup/life-insurance-setup.service';
import { PagedData } from '../../../_sharedresource/paged-data';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { TextMaskModule } from 'angular2-text-mask';


@Component({
  selector: 'app-life-insurance-setup',
  templateUrl: './life-insurance-setup.component.html',
  providers: [LifeInsuranceSetupService],
  
})
// export LifeInsurance component to make it available for other classes
export class LifeInsuranceSetupComponent implements OnInit {

    page = new Page();
    rows = new Array<LifeInsuranceSetup>();
    temp = new Array<LifeInsuranceSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1413';
    moduleName;
    screenName;
    defaultFormValues: Array<any> = [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    lifeInsuranceId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: LifeInsuranceSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
	amountPattern:boolean=true;
	spouseAmountAmountPattern:boolean=true;
	childAmountAmountPattern:boolean=true;
	coverageTotalAmountPattern:boolean=true;
	employeePayAmountPattern:boolean=true;
	startDateModel;
	endDateModel;
	frmstartDate;
	frmendDate;
    hasMsg = false;
    showMsg = false;
    startDate;
    endDate;
	error:any={isError:false,errorMessage:''};
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    lifeInsuranceIdvalue: string;
    tempp:string[]=[];
    
    compInsurances = [];
    
    islifeTimeValidamount: boolean = true;

    isAmount: boolean= true;
    isCovAmount: boolean= true;
    isEmperMTP: boolean=true;
    isempMTP: boolean= true;
    empamountCov: number;
    empeeamount: number;
    empamountMTP: number;

    // Initializing frequency

    lifeInsuranceFrequencys: DropDownModule[] = [
        
        {id: 1, name: 'Weekly'},
        {id: 2, name: 'Biweekly'},
        {id: 3, name: 'Semimonthly'},
        {id: 4, name: 'Monthly'},
        {id: 5, name: 'Quarterly'},
        {id: 6, name: 'Semiannually'},
        {id: 7, name: 'Annually'}

    ];  

    // Initializing Coverage Type

    insuranceTypes: DropDownModule[] = [
        {id: 1, name: 'Full Amount'},
        {id: 2, name: 'Employer Pays Nothing'},
        {id: 3, name: 'Increment Whole Family'},
        {id: 4, name: 'Increment Employee'}
    ];
    
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };
    
    // Initialized to specific date (14.03.2018).
    //public startDateModel: any = { date: { day: 14, month: 3, year: 2018 } };
    // Initialized to specific date (14.03.2018).
   // public endDateModel: any = { date: { day: 14, month: 3, year: 2018 } };
	


    constructor(private router: Router,
        private lifeInsuranceSetupService: LifeInsuranceSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        
        // default form parameter for department  screen
        this.defaultFormValues = [

            { 'fieldName': 'LIFEINSURANCESETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_FREQUENCY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_COMPANY_INSURANCE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_GROUP_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_PLAN_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_PLAN_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_EMPLOYEE_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_EMPLOYEE_SPOUSE_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_EMPLOYEE_CHILDREN_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_LIFE_INSURANCE_COVERAGE_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_COVERAGE_MAXIMUM_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_EMPLOYER_MUST_TO_PAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_FIELD_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_EMPLOYER_AMT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LIFEINSURANCESETUP_COVERAGE_TYPE', 'fieldValue': '', 'helpMessage': '' },
        ];

    } 

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage'); 
       
        this.lifeInsuranceSetupService.getcompInsurances().then(data =>{
            console.log(data.records);
            this.compInsurances= data.result.records;
           console.log(this.compInsurances);
        });
    
        
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }
    //setting pagination
    setPage(pageInfo) {
    
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.lifeInsuranceSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create Life Insurance
    Create() {
		this.startDateModel=null;
		this.endDateModel=null;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id:0,
            lifeInsuraceId: '',
            lifeInsuraceDescription: '',
            lifeInsuraceDescriptionArabic: '',
            healthInsuranceGroupNumber: '',
            lifeInsuranceFrequency: 0,
            amount: null,
            spouseAmount: 0,
            childAmount: 0,
            coverageTotalAmount: null,
            employeePay: null,
            startDate: this.startDateModel,
            endDate: this.endDateModel,
            insuranceType: 0,
            healthInsuranceId: 0,
            
        };
        console.log(this.model);
		
    }


    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({lifeInsuranceFrequency:0,healthInsuranceId:0,insuranceType: 0});
        this.spouseAmountAmountPattern=true;
        this.childAmountAmountPattern=true;
        this.coverageTotalAmountPattern=true;
        this.employeePayAmountPattern=true;
        this.isAmount= true;
        this.isCovAmount= true;
        this.isEmperMTP=true;
        this.isempMTP= true;
        this.islifeTimeValidamount = true;
    }
    
    
    //function call for creating new Life Insurance
    CreateLifeInsurance(f: NgForm, event: Event) {
        this.model.startDate = this.startDate;
        this.model.endDate = this.endDate;
        event.preventDefault();
        var locIdx = this.model.lifeInsuraceId;
        
        
        

        //Check if the id is available in the model.
        //If id avalable then update the Life Insurance, else Add new Life Insurance.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            //Check for duplicate Life Insurance Id according to it create new Life Insurance
            this.lifeInsuranceSetupService.checkDuplicateLifeInsId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                }
        
        else {
                //Call service api for Creating new Life Insurance
                this.lifeInsuranceSetupService.createLifeInsurance(this.model).then(data => {
                    var datacode = data.code;
                    if (datacode == 201) {
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        this.showCreateForm = false;
                        this.messageText = data.btiMessage.message;
                        
                    }, 100);

                    this.hasMsg = true;
                    f.resetForm();
                    //Refresh the Grid data after adding new Life Insurance
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });

    }
}
        
    
    //edit department by row
    edit(row: LifeInsuranceSetup) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
         this.startDateModel = this.formatDateFordatePicker(this.model.startDate);
        this.endDateModel = this.formatDateFordatePicker(this.model.endDate);
        this.lifeInsuranceId = row.lifeInsuraceId;
        this.isUnderUpdate = true;
        this.lifeInsuranceIdvalue = this.model.lifeInsuraceId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
        // console.log(this.lifeInsuranceIdvalue);
    }

    updateStatus() {
        this.closeModal();
         //Call service api for Creating new department
        this.model.lifeInsuraceId = this.lifeInsuranceIdvalue;
        this.lifeInsuranceSetupService.updateLifeInsurance(this.model).then(data => {
            var datacode = data.code;
            console.log(this.lifeInsuranceIdvalue);
            if (datacode == 201) {
                //Refresh the Grid data after editing department
            this.setPage({ offset: 0 /*, sortOn: this.page.sortOn, sortBy: this.page.sortBy */});

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedInsurances = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedInsurances.push(this.selected[i].id);
        }
        this.lifeInsuranceSetupService.deleteLifeInsurance(selectedInsurances).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }
    
    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    onStartDateChanged(event: IMyDateModel): void {
        this.startDate = event.jsdate;
		this.frmstartDate = event.epoc;
        console.log('this.frmStartDate', event);
		 if((this.frmstartDate > this.frmendDate) && this.frmendDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.frmstartDate <= this.frmendDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.frmendDate==undefined && this.frmstartDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
        console.log('start Date: ' + this.startDate);
        
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.endDate = event.jsdate;
       this.frmendDate = event.epoc;
       console.log('this.frmEndDate', this.frmendDate, event);

        if((this.frmstartDate > this.frmendDate) && this.frmendDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.frmstartDate <= this.frmendDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.frmendDate==undefined && this.frmstartDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}

    }

    // Comp. Employee amount with Coverage Amount
    onChangeAmount(eve){
        console.log("Event:", event);
        this.empamountCov = this.model.coverageTotalAmount; 
        this.empamountMTP = this.model.employeePay;
        this.empeeamount =  parseFloat(eve);
        if(this.empeeamount === 0 || this.empamountCov == 0 || this.empamountMTP == 0 ){
            this.isAmount = true;
            this.isEmperMTP = true;
        } else{
            if(this.islifeTimeValidamount === true && this.coverageTotalAmountPattern === true && this.employeePayAmountPattern === true ){
                if(this.empamountCov !== null && this.empeeamount !== null){
                    if( this.empeeamount >= this.empamountCov){
                        this.isAmount = false
            
                    }else{
                        this.isAmount = true;
                    }
                }else{
                    this.isAmount = true;
                }
                if((this.empeeamount !== null && this.empamountMTP !== null)){
                    if(this.empeeamount <= this.empamountMTP){
                        this.isEmperMTP = false;
        
                    }else{
                        this.isEmperMTP = true;
                    }
        
                }else{
                    this.isEmperMTP = true;
                }
            }else{
                this.isEmperMTP = true;
                this.isAmount = true;
            }
            
        }
        
    }
    // Comp. Coverage Amount with Employee Amount
    onChangeAmountCov(eve){
        console.log("Event:", event);
        this.empeeamount = this.model.amount;
        this.empamountCov = parseFloat(eve);
        if(this.empamountCov === 0 || this.empeeamount == 0){
            this.isAmount = true;
        } else {
            if(this.islifeTimeValidamount === true && this.coverageTotalAmountPattern === true && this.employeePayAmountPattern === true ){
                if(this.empeeamount !== null && this.empamountCov !== null){
                    if(this.empamountCov <= this.empeeamount){
                        this.isAmount = false
            
                    }else{
                        this.isAmount = true;
                    }
                }else{
                    this.isAmount = true;
                }
            }else{
                this.isAmount = true;
            }
            
            
        }
        
    }
    
        // comp. Employer Must To Pay with Employeey Amount and Coverage Max Amount
    onChangeEmperMTPAmount(eve){
        console.log("Event:", event);
        this.empeeamount = this.model.amount;
        this.empamountMTP =  parseFloat(eve);
        if(this.empamountMTP === 0 || this.empeeamount == 0){
            this.isEmperMTP = true;
        } else{
            if(this.islifeTimeValidamount === true && this.coverageTotalAmountPattern === true && this.employeePayAmountPattern === true ){
                if(this.empamountMTP !== null && this.empeeamount !== null){
                    if( this.empamountMTP >= this.empeeamount){
                        this.isEmperMTP = false
            
                    }else{
                        this.isEmperMTP = true;
                    }
                }else{
                    this.isEmperMTP = true;
                }
            }else{
                this.isEmperMTP = true;
            }
            
        }
        
        
    }
    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.lifeInsuraceId = '';
            return false;
        }
    }


    onCompInsSelect(event) {
        this.lifeInsuranceSetupService.getcompInsurances().then(data => {
            this.compInsurances = data.result.records;    
            console.log(data.result);       
        });
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-.()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

   
    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
	formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    // check Desimal Amount
    checkdecimalamount(digit)
	{
		console.log(digit);
        if(digit==null || digit==''){
            this.islifeTimeValidamount = true;
            return;
        }
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			this.islifeTimeValidamount = false;
			console.log(this.islifeTimeValidamount);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			this.islifeTimeValidamount = false;
			console.log(this.islifeTimeValidamount);
			return false;
		}
		
		else{
			this.islifeTimeValidamount = true;
			console.log(this.islifeTimeValidamount);
			return true;
		}
	
    }
    
    checkdecimalspouseAmount(digit)
	{
		console.log(digit);
        if(digit==null || digit==''){
            this.amountPattern = true;
           
            return;
        }
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			this.amountPattern = false;
			console.log(this.amountPattern);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			this.amountPattern = false;
			console.log(this.amountPattern);
		}
		
		else{
			this.amountPattern = true;
			console.log(this.amountPattern);
		}
	
    }
    
    checkdecimalchildAmount(digit)
	{
		console.log(digit);
        if(digit==null || digit==''){
            this.childAmountAmountPattern = true;
           
            return;
        }
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			this.childAmountAmountPattern = false;
			console.log(this.childAmountAmountPattern);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			this.childAmountAmountPattern = false;
			console.log(this.childAmountAmountPattern);
		}
		
		else{
			this.childAmountAmountPattern = true;
			console.log(this.childAmountAmountPattern);
		}
	
    }
    
    checkdecimalcoverageTotalAmount(digit)
	{
		console.log(digit);
        if(digit==null || digit==''){
            this.coverageTotalAmountPattern = true;
            return;
        }
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            this.coverageTotalAmountPattern = false;
            this.isAmount = true;
			console.log(this.coverageTotalAmountPattern);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            this.coverageTotalAmountPattern = false;
            this.isAmount = true;
			console.log(this.coverageTotalAmountPattern);
		}
		
		else{
			this.coverageTotalAmountPattern = true;
			console.log(this.coverageTotalAmountPattern);
		}
	
    }
    
    checkdecimalemployeePay(digit)
	{
		console.log(digit);
        if(digit==null || digit==''){
            this.employeePayAmountPattern = true;
            return;
        }
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            this.employeePayAmountPattern = false;
            this.isEmperMTP = true;
			console.log(this.employeePayAmountPattern);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            this.employeePayAmountPattern = false;
            this.isEmperMTP = true;
			console.log(this.employeePayAmountPattern);
		}
		
		else{
			this.employeePayAmountPattern = true;
			console.log(this.employeePayAmountPattern);
		}
	
	}


 
   
    
    

}