import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Batch, getAllBatch } from '../../_models/batches/batches';
import { BatchService } from '../../_services/batches/batches.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import {DatePipe} from '@angular/common';

@Component({
    selector: 'batch',
    templateUrl: './batches.component.html',
    providers: [BatchService, CommonService, DatePipe]
})

// export Batches component to make it available for other classes
export class BatchesComponent {
    page = new Page();
    rows = new Array<Batch>();
    temp = new Array<Batch>();
    selected = [];
    moduleCode = "M-1011";
    screenCode = "S-1461";
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    batchId = {};
    searchKeyword = '';
    getBatch: any[] = [];
    ddPageSize: number = 5;
    model: Batch;
    getAllmodel: getAllBatch;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    startDate;
    frmStartDate;
    batchID;
    username;
    isbatchValid;
    disableApprove: boolean;
    minDate: Date = new Date();
    modelStartDate;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    approvedDate;
    postingDisplayDate: any = null;
    Status = ['New', 'Busy', 'Clear', 'Posting']

            BATCHES_CREATE_LABEL:any;
            BATCHES_BATCH_ID:any;
            BATCHES_TRANSACTION_TYPE_LABEL:any;
            BATCHES_DESCRIPTION_LABEL:any;
            BATCHES_POSTING_DATE_LABEL:any;
            BATCHES_TOTAL_TRANSACTIONS_LABEL:any;
            BATCHES_QUANTITY_TOTAL_LABEL:any;
            BATCHES_APPROVED_LABEL:any;
            BATCHES_USER_ID_LABEL:any;
            BATCHES_SAVE_LABEL:any;
            BATCHES_DELETE_LABEL:any;
            BATCHES_POST_LABEL:any;
            BATCHES_CLEAR_LABEL:any;
            BATCHES_UPDATE_LABEL:any;
            BATCHES_SEARCH:any;
            BATCHES_ACTION:any;
            BATCHES_CREATE_TITLE:any;
            BATCHES_CANCEL_LABEL:any;
            BATCHES_UPDATE_BATCH_LABEL:any;
            BATCHES_APPROVED_DATE:any;
            BATCHES_POST_TO_GENERAL_AUTO:any;

    constructor(
        private router: Router,
        private batchService: BatchService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,private commonService:CommonService,
        private datePipe: DatePipe) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'BATCHES_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_BATCH_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_TRANSACTION_TYPE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_DESCRIPTION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_POSTING_DATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_TOTAL_TRANSACTIONS_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_QUANTITY_TOTAL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_APPROVED_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_USER_ID_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_POST_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_CREATE_TITLE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_UPDATE_BATCH_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_APPROVED_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_POST_TO_GENERAL_AUTO', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BATCHES_STATUS', 'fieldValue': '', 'helpMessage': '' },
        ];

        this.BATCHES_CREATE_LABEL=this.defaultFormValues[0];
        this.BATCHES_BATCH_ID=this.defaultFormValues[1];
        this.BATCHES_TRANSACTION_TYPE_LABEL=this.defaultFormValues[2];
        this.BATCHES_DESCRIPTION_LABEL=this.defaultFormValues[3];
        this.BATCHES_POSTING_DATE_LABEL=this.defaultFormValues[4];
        this.BATCHES_TOTAL_TRANSACTIONS_LABEL=this.defaultFormValues[5];
        this.BATCHES_QUANTITY_TOTAL_LABEL=this.defaultFormValues[6];
        this.BATCHES_APPROVED_LABEL=this.defaultFormValues[7];
        this.BATCHES_USER_ID_LABEL=this.defaultFormValues[8];
        this.BATCHES_SAVE_LABEL=this.defaultFormValues[9];
        this.BATCHES_DELETE_LABEL=this.defaultFormValues[10];
        this.BATCHES_POST_LABEL=this.defaultFormValues[11];
        this.BATCHES_CLEAR_LABEL=this.defaultFormValues[12];
        this.BATCHES_UPDATE_LABEL=this.defaultFormValues[13];
        this.BATCHES_SEARCH=this.defaultFormValues[14];
        this.BATCHES_ACTION=this.defaultFormValues[15];
        this.BATCHES_CREATE_TITLE=this.defaultFormValues[16];
        this.BATCHES_CANCEL_LABEL=this.defaultFormValues[17];
        this.BATCHES_UPDATE_BATCH_LABEL=this.defaultFormValues[18];
        this.BATCHES_APPROVED_DATE=this.defaultFormValues[19];
        this.BATCHES_POST_TO_GENERAL_AUTO=this.defaultFormValues[20];
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
        this.disableApprove = false;
        this.getAllmodel = {
            id: 0,
            batchId: '',
            description: '',
            arabicDescription: '',
            transactionType: '',
            postingDate: null,
            totalTransactions: 0,
            quantityTotal: 0,
            approved: false,
            userID: '',
            approvedDate: null,
            status: null
        };
    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'status') {
            return (
                row[gridFieldName] === 1 ? 'Busy' :
                    row[gridFieldName] === 2 ? 'Clear' :
                        row[gridFieldName] === 3 ? 'Posting' : ''
            );
        } else if (gridFieldName === 'postingDate') {
            return row[gridFieldName]?this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy'):'';
        } else {
            return row[gridFieldName];
        }
    }

    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.disableApprove = false;
        this.modelStartDate = null;
        this.approvedDate = null;
        this.username = '';
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            batchId: '',
            description: '',
            arabicDescription: '',
            transactionType: '',
            postingDate: null,
            totalTransactions: 0,
            quantityTotal: 0,
            approved: false,
            userID: '',
            approvedDate: null
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        if (this.isUnderUpdate) {
            f.resetForm({
                'batchId': this.model.batchId,
                'totalTransactions': this.model.totalTransactions,
                'quantityTotal': this.model.quantityTotal,
                'approvedDate': this.model.approvedDate,
                'userId': this.username,
                'transactionType': '',
                'approved': this.model.approved,
            });
            window.setTimeout(() => {
                if (this.postingDisplayDate != null) {
                    this.modelStartDate = Object.assign({}, this.postingDisplayDate);
                } else {
                    this.modelStartDate = null;
                }
            }, 10);
        } else {
            f.resetForm({
                'transactionType': '',
                'totalTransactions': this.model.totalTransactions,
                'quantityTotal': this.model.quantityTotal
            });
            this.approvedDate = null;
            this.modelStartDate = null;
        }
    }

    //function call for creating new department
    CreateBatch(f: NgForm, event: Event) {
        event.preventDefault();
        var batchIdx = this.model.batchId;

        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.batchService.checkDuplicateBatchId(batchIdx).then(response => {
                if (response && response.code == 201 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new department
                    this.batchService.createBatch(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new department
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }
    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
        this.model.id = this.batchID;
        console.log('This model', this.model)
        this.batchService.updateBatch(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedBatches = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedBatches.push(this.selected[i].id);
        }
        this.batchService.deleteBatch(selectedBatches).then(data => {
            var datacode = data.code;
           
            if(datacode == "302"){
                this.hasMessage = true;
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                //console.log("DATA CODE",datacode);
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);

                //Refresh the Grid data after deletion of department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            if (datacode == "201") {
                this.hasMessage = true;
                this.message.type = "success";
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);

                //Refresh the Grid data after deletion of department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }

            if (datacode == "202") {
                this.hasMessage = true;
                this.message.type = "success";
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);

                //Refresh the Grid data after deletion of department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.deductionCodeSetupService.searchDeductionCodelist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.batchService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
        disableUntil: { day: this.minDate.getDate() - 1, month: this.minDate.getMonth() + 1, year: this.minDate.getFullYear() }
    };
    formatDateFordatePicker(strDate: any): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            // return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
            let day = setDate.getDate();
            let month = setDate.getMonth() + 1;
            let year = setDate.getFullYear();
            if (month < 10) {
                this.model.approvedDate = day + '-' + '0' + month + '-' + year;
            }
            else {
                this.model.approvedDate = day + '-' + month + '-' + year;
            }

        } else {
            return null;
        }
    }

    formatDateFordatecommonPicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        this.model.postingDate = (event.epoc) * 1000;
        //console.log('this.frmStartDate', event);

    }
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
    //edit department by row
    edit(row: any) {
        console.log("Rows", row);
        this.isUnderUpdate = true;
        this.modelStartDate = null;
        this.approvedDate = null;
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.batchID = this.model.id;
        if (this.model.userID != null) {
            this.batchService.getUser(this.model.userID).then(data => {
                let user = data.result;
                this.username = user.primaryFullName;
                console.log('username', this.username);
            })
        }
        this.modelStartDate = this.formatDateFordatecommonPicker(this.model.postingDate);
        this.postingDisplayDate = this.modelStartDate;
        console.log('posting', this.modelStartDate);
        // if(this.model.approved){
        //     this.disableApprove = true;
        // }
        // else{
        //     this.disableApprove = false;
        // }
        this.model.approvedDate = row.approvedDate;
        //  this.approvedDate = this.formatDateFordatecommonPicker(row.approvedDate); 
        this.approvedDate = row.approvedDate;
        console.log('this.approvedDate', this.approvedDate);

        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }
    // CheckValidID(eve: any, Case: any) {
    //     switch (Case) {






    //         case 'batchId':
    //             if (eve.target.value == "") {
    //                 this.isbatchValid = true;
    //             } else {
    //                 for (let i = 0; i < this.getUserCategoryName6.length; i++) {
    //                     //if (this.getItemList[i].itemNumber.includes(eve.target.value)) {
    //                     if (this.getUserCategoryName6[i].valueCategory.toLowerCase().match(eve.target.value.toLowerCase()))
    //                     {
    //                         this.isbatchValid = true;
    //                         break;
    //                     } else {
    //                         this.isbatchValid = false;
    //                     }
    //                 }
    //             }
    //         break;

    //     }
    // }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    Cancel(f: NgForm) {
        f.resetForm();
        this.showCreateForm = false;
        this.modelStartDate = null;
        this.approvedDate = null;

    }
}