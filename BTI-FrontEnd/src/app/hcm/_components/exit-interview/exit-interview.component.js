"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var router_1 = require("@angular/router");
var location_service_1 = require("../../_services/location/location.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var LocationComponent = (function () {
    function LocationComponent(router, locationService, getScreenDetailService, alertService) {
        this.router = router;
        this.locationService = locationService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.countries = [];
        this.states = [];
        this.cities = [];
        this.modelState = '';
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1312';
        this.message = { 'type': '', 'text': '' };
        this.locationId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'LOCATION_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CONTACT_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ADDRESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_COUNTRY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_PHONE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_FAX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    LocationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.locationService.getCountry().then(function (data) {
            _this.countries = data.result;
            console.log(data.result);
        });
        console.log(this.countries);
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    LocationComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.locationService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create location
    LocationComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            locationId: null,
            description: '',
            arabicDescription: '',
            contactName: '',
            locationAddress: '',
            cityName: '',
            countryName: '',
            stateName: '',
            cityId: 0,
            countryId: 0,
            stateId: 0,
            phone: '',
            fax: ''
        };
    };
    // Clear form to reset to default blank
    LocationComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new location
    LocationComponent.prototype.CreateLocation = function (f, event) {
        var _this = this;
        event.preventDefault();
        var locIdx = this.model.locationId;
        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.locationService.checkDuplicateLocationId(locIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    //Call service api for Creating new location
                    _this.locationService.createLocation(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new location
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    //edit department by row
    LocationComponent.prototype.edit = function (row) {
        var _this = this;
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.locationId = row.locationId;
        this.isUnderUpdate = true;
        this.locationIdvalue = this.model.locationId;
        this.locationService.getStatesByCountryId(1).then(function (data) {
            _this.states = data.result;
            console.log(data.result);
        });
        this.locationService.getCitiesByStateId(1).then(function (data) {
            _this.cities = data.result;
            console.log(data.result);
        });
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    LocationComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        this.model.locationId = this.locationIdvalue;
        //Call service api for updating selected department
        /*var locIdx = this.model.locationId;
        this.locationService.checkDuplicateLocationId(locIdx).then(response => {
            if (response && response.code == 302 && response.result && response.result.isRepeat) {
                this.duplicateWarning = true;
                this.message.type = 'success';

                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = response.btiMessage.message;
                }, 100);
            } else {*/
        //Call service api for Creating new location
        this.locationService.updateLocation(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
        /*}*/
        /*}).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });*/
    };
    LocationComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    LocationComponent.prototype.delete = function () {
        var _this = this;
        var selectedLocations = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedLocations.push(this.selected[i].id);
        }
        this.locationService.deleteLocation(selectedLocations).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.    ';
        });
        this.closeModal();
    };
    // default list on page
    LocationComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    LocationComponent.prototype.onCountrySelect = function (event) {
        var _this = this;
        this.locationService.getStatesByCountryId(event.target.value).then(function (data) {
            _this.states = data.result;
            console.log(data.result);
        });
        /* this.states = this._dataService.getStates()
                      .filter((item)=> item.countryid == countryid);*/
    };
    // search department by keyword
    LocationComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.locationService.searchLocationlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    LocationComponent.prototype.onStateSelect = function (event) {
        var _this = this;
        this.locationService.getCitiesByStateId(event.target.value).then(function (data) {
            _this.cities = data.result;
            console.log(data.result);
        });
        /* this.states = this._dataService.getStates()
                      .filter((item)=> item.countryid == countryid);*/
    };
    // Set default page size
    LocationComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    LocationComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    };
    LocationComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    LocationComponent.prototype.CheckNumber = function (event) {
        if (isNaN(event.target.value) == true) {
            this.model.locationId = 0;
            return false;
        }
    };
    LocationComponent.prototype.keyPress = function (event) {
        var pattern = /^[0-9\-()]+$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    return LocationComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], LocationComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], LocationComponent.prototype, "myScrollContainer", void 0);
LocationComponent = __decorate([
    core_1.Component({
        selector: 'location',
        templateUrl: './location.component.html',
        providers: [location_service_1.LocationService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        location_service_1.LocationService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], LocationComponent);
exports.LocationComponent = LocationComponent;
//# sourceMappingURL=location.component.js.map