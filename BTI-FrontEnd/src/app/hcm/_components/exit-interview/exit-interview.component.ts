import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../_sharedresource/page';
import {Constants} from '../../../_sharedresource/Constants';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Router} from '@angular/router';
import {ExitInterview} from '../../_models/exit-interview/exit-interview.module';
import {ExitInterviewService} from '../../_services/exit-interview/exit-interview.service';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'exitInterview',
    templateUrl: './exit-interview.component.html',
    providers : [ExitInterviewService]
})
export class ExitInterviewComponent {
    page = new Page();
    rows = new Array<ExitInterview>();
    temp = new Array<ExitInterview>();
    selected = [];
    countries = [];
    states = [];
    cities = [];
	accrualScheduleList = [];
	linkedPayCodeList = [];
    modelState = '';
    buttontype = '';
    moduleCode = 'M-1011';
    screenCode = 'S-1425';
    moduleName;
    screenName;
    defaultFormValues: Array<any>= [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    exitInterviewId;
    searchKeyword = '';
    ddPageSize: number = 5;
    model: ExitInterview;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    exitInterviewIdvalue : any;
	scheduleId : number;
	scheduleIdSelected : number;
    schedulePrimaryId : number;
    payPrimaryId: number;
    isInterviewFormdisabled: boolean = false;

    isAdd:boolean = true;
    dataResult:any;

	frequencyArray = ["Weekly","Biweekly","Semimonthly","Monthly","Quarterly","Semianually","Anually"];
    timeTypeArray = ["Hourly","Salary","Overtime","Benefit","Absent","Other"];
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
                private exitInterviewService: ExitInterviewService,
                private getScreenDetailService: GetScreenDetailService,
                private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            {'fieldName': 'EXIT_INTERVIEW_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_ID_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_INACTIVE_LABLE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_DESCRIPTION_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_ARABIC_DESCRIPTION_LABLE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_ADD_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_ACTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
			{'fieldName': 'EXIT_INTERVIEW_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EXIT_INTERVIEW_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
			{'fieldName': 'EXIT_INTERVIEW_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
        ];

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.exitInterviewService.getCountry().then(data => {
            this.countries = data.result;    
            console.log(data.result);       
        });
		
		this.exitInterviewService.getAccrualSchedule().then(data => {
            this.accrualScheduleList = data.result.records;   
        });
		
		this.exitInterviewService.getlinkedPayCode().then(data => {
            this.linkedPayCodeList = data.result.records;
        });
		
		
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
			this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.exitInterviewService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.isInterviewFormdisabled = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            exitInterviewId:'',
            inActive: false,
            desc: '',
            arbicDesc: ''
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({timeType: '',accrualPeriod: '',payCodeId: 0,scheduleId: 0});
        this.isInterviewFormdisabled = false;
    }


    //function call for creating new location
    CreateExitInterview(f: NgForm, event: Event) {
 
        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }   
        else{
            //Call service api for Creating new location
            this.exitInterviewService.createExitInterview(this.model).then(data => {
                var datacode = data.code;
                this.dataResult = data.result;
                
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMessage = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        if(this.buttontype == 'add') {
                            this.showCreateForm = true;
                            this.buttontype = '';    
                        } else {
                            this.showCreateForm = false;
                        }
                            
                        
                        
                        this.messageText = data.btiMessage.message;
                    }, 100);

                    this.hasMsg = true;
                    
                    f.resetForm({exitInterviewId:this.dataResult.exitInterviewId});
                   
                    //Refresh the Grid data after adding new location
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        }

            

        
    }

    // Add Sequence Item
    showHideForm() {
       // this.showCreateForm = true;    
    }
    add(f:NgForm, event:Event,buttontype){
        this.buttontype = buttontype;
        console.log("Show create val : "+this.showCreateForm);
        
    }

    //edit department by row
    edit(row: ExitInterview) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.exitInterviewId = row.exitInterviewId;        
        this.isUnderUpdate = true;
        this.exitInterviewIdvalue = this.model.exitInterviewId;
        if(this.model.inActive==true){
            this.isInterviewFormdisabled = true;
        } else{
            this.isInterviewFormdisabled = false;
        }
		
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    updateStatus() {
        this.closeModal();
        this.model.exitInterviewId = this.exitInterviewId;
        //Call service api for updating selected department
        
                //Call service api for Creating new location
                this.exitInterviewService.updateExitInterview(this.model).then(data => {
                    var datacode = data.code;
                    if (datacode == 201) {
                        //Refresh the Grid data after editing department
                        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                        //Scroll to top after editing department
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.messageText = data.btiMessage.message;
                            ;
                            this.showCreateForm = false;
                        }, 100);
                        this.hasMessage = false;
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = 'Server error. Please contact admin.';
                    }, 100)
                });
            /*}*/

    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedLocations = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedLocations.push(this.selected[i].id);
        }
        this.exitInterviewService.deleteExitInterview(selectedLocations).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.    ';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    onCountrySelect(event) {
        this.exitInterviewService.getStatesByCountryId(event.target.value).then(data => {
            this.states = data.result;    
        });
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
       
    }

    onStateSelect(event) {  
        this.exitInterviewService.getCitiesByStateId(event.target.value).then(data => {
            this.cities = data.result;    
        });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkExitInterviewForm(event){
        if(event.target.checked==true){
            this.isInterviewFormdisabled = true;
        } else{
            this.isInterviewFormdisabled = false;
        }

    }

}
