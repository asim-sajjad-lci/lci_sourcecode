import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { SalaryMatrixSetupService } from '../../_services/salary-matrix-setup/salary-matrix-setup.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { ContenteditableModelDirective } from '../../../_sharedresource/contenteditableModel.directive'
import { SalaryMatrixSetupModule } from '../../_models/salary-matrix-setup/salary-matrix-setup.module';

import { SalaryMatrixRowModule } from '../../_models/salary-matrix-setup/salary-matrix-row.module';
import { SalaryMatrixColumnModule } from '../../_models/salary-matrix-setup/salary-matrix-column.module';
import { SalaryMatrixRowValueModule } from '../../_models/salary-matrix-setup/salary-matrix-row-value.module';

import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms/src/model';
import * as cloneDeep from 'lodash/cloneDeep';
import { concat } from 'rxjs/operators/concat';
import 'rxjs/add/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';


@Component({
  selector: 'app-salary-matrix-setup',
  templateUrl: './salary-matrix-setup.component.html',
  styleUrls: ['./salary-matrix-setup.component.css'],
  providers: [SalaryMatrixSetupService,CommonService]
})
export class SalaryMatrixSetupComponent implements OnInit {
  page = new Page();
  page2: number = 0;
  currentLanguage: any;
  selected = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1419';
  moduleName;
  screenName;
  defaultFormValues:any = [];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  searchKeyword = '';
  ddPageSize: number = 5;
  model: SalaryMatrixSetupModule;
  matrixId = {};
  showCreateForm: boolean = false;
  generateMatrix: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  isUnderMatrixUpdate: boolean = false;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  isConfirmationModalOpen2: boolean = false;
  submitted: boolean = false;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  isNotGeneratedAlert: boolean = true;
  isNotGeneratedText = "Please generate the Salary Matrix first.";
  salaryMatrixSetupIdvalue: string;
  rowValCell = [];
  rowColCell = [];
  tempd;
  prevTotCol: number;
  prevTotRow: number;
  prevTotRowVal: number;


  first: boolean = false;
  last: boolean = false;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;
  @ViewChild('scrollable', { read: ElementRef }) public scrollable: ElementRef;

  selectedRow: number;
  rows = new Array<SalaryMatrixSetupModule>();
  temp: Array<SalaryMatrixRowValueModule>;
  matrixIdList: Observable<any>;
  getMatrixId: any[] = [];
  typeaheadLoading: boolean;

  PayUnitValues = [
    { period: "Weekly", value: "1" },
    { period: "Biweekly", value: "2" },
    { period: "Semimonthly", value: "3" },
    { period: "Monthly", value: "4" },
    { period: "Quarterly", value: "5" },
    { period: "Semiannually", value: "6" },
    { period: "Annually", value: "7" }];

      SALARY_MATRIX_SEARCH:any;
      SALARY_MATRIX_ID:any;
      SALARY_MATRIX_DESCRIPTION:any;
      SALARY_MATRIX_ARABIC_DESCRIPTION:any;
      SALARY_MATRIX_ARBIC_PAY_UNIT:any;
      SALARY_MATRIX_TOTAL_ROW:any;
      SALARY_MATRIX_TOTAL_COLUMNS:any;
      SALARY_MATRIX_TOTAL_ROW_VALUES:any;
      SALARY_MATRIX_ACTION:any;
      SALARY_MATRIX_CREATE_LABEL:any;
      SALARY_MATRIX_SAVE_LABEL:any;
      SALARY_MATRIX_CLEAR_LABEL:any;
      SALARY_MATRIX_CANCEL_LABEL:any;
      SALARY_MATRIX_UPDATE_LABEL:any;
      SALARY_MATRIX_DELETE_LABEL:any;
      SALARY_MATRIX_CREATE_FORM_LABEL:any;
      SALARY_MATRIX_UPDATE_FORM_LABEL:any;
      SALARY_MATRIX_GENERATE_TABLE:any;
      SALARY_MATRIX_MATRIX_ID:any;
      SALARY_MATRIX_ROWS:any;
      SALARY_MATRIX_COLUMNS:any;


  public salaryMatrix: FormGroup;

  constructor(private router: Router,
    private salaryMatrixSetupService: SalaryMatrixSetupService,
    private getScreenDetailService: GetScreenDetailService,
    private alertService: AlertService,private commonService: CommonService, private _fb: FormBuilder) {
    this.page.pageNumber = 0;
    this.page.size = 5;

    this.defaultFormValues = [
      { 'fieldName': 'SALARY_MATRIX_SEARCH', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_ID', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_ARBIC_PAY_UNIT', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_TOTAL_ROW', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_TOTAL_COLUMNS', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_TOTAL_ROW_VALUES', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_ACTION', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_GENERATE_TABLE', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_MATRIX_ID', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_ROWS', 'fieldValue': '', 'helpMessage': '' },
      { 'fieldName': 'SALARY_MATRIX_COLUMNS', 'fieldValue': '', 'helpMessage': '' },
    ];

   this.SALARY_MATRIX_SEARCH=this.defaultFormValues[0];
   this. SALARY_MATRIX_ID=this.defaultFormValues[1];
   this. SALARY_MATRIX_DESCRIPTION=this.defaultFormValues[2];
   this.SALARY_MATRIX_ARABIC_DESCRIPTION=this.defaultFormValues[3];
   this.SALARY_MATRIX_ARBIC_PAY_UNIT=this.defaultFormValues[4];
   this.SALARY_MATRIX_TOTAL_ROW=this.defaultFormValues[5];
   this.SALARY_MATRIX_TOTAL_COLUMNS=this.defaultFormValues[6];
   this. SALARY_MATRIX_TOTAL_ROW_VALUES=this.defaultFormValues[7];
   this. SALARY_MATRIX_ACTION=this.defaultFormValues[8];
   this.SALARY_MATRIX_CREATE_LABEL=this.defaultFormValues[9];
   this.SALARY_MATRIX_SAVE_LABEL=this.defaultFormValues[10];
   this.SALARY_MATRIX_CLEAR_LABEL=this.defaultFormValues[11];
   this.SALARY_MATRIX_CANCEL_LABEL=this.defaultFormValues[12];
   this. SALARY_MATRIX_UPDATE_LABEL=this.defaultFormValues[13];
   this.SALARY_MATRIX_DELETE_LABEL=this.defaultFormValues[14];
   this. SALARY_MATRIX_CREATE_FORM_LABEL=this.defaultFormValues[15];
   this. SALARY_MATRIX_UPDATE_FORM_LABEL=this.defaultFormValues[16];
   this. SALARY_MATRIX_GENERATE_TABLE=this.defaultFormValues[17];
   this.SALARY_MATRIX_MATRIX_ID=this.defaultFormValues[18];
   this. SALARY_MATRIX_ROWS=this.defaultFormValues[19];
   this. SALARY_MATRIX_COLUMNS=this.defaultFormValues[20];

    this.matrixIdList = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.model.salaryMatrixId);
    }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));
  }

  
  ngOnInit() {
    this.setPage({ offset: 0 });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    //getting screen labels, help messages and validation messages
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
  }

    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'payUnit') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                      row[gridFieldName] === 7 ? 'Annually' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }
  getSuperviserIdAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
    return Observable.of(
      this.getMatrixId.filter((id: any) => {
        return query.test(id.salaryMatrixId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    // console.log('Selected value: ', e);
    this.salaryMatrixSetupService.getSalaryMatrixSetupById(e.item.id).then(data => {
      // console.log("Data class options : ", data);
      this.model = data.result;
      this.model.id = 0;
      this.model.listSalaryMatrixRow = [];
    });
  }

  setPage(pageInfo) {
    //debugger;
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    this.page.searchKeyword = '';
    if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
    } else {
      this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
    } else {
      this.page.sortBy = pageInfo.sortBy;
    }


    this.salaryMatrixSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });

    this.salaryMatrixSetupService.getMatrixId().then(data => {
      this.getMatrixId = data.result.records;
      //console.log("Data class options : " + this.getMatrixId);
    });
  }

  //scroll to next/previous columns
  public onFirstSearchPosition(): void {
    this.scrollable.nativeElement.scrollLeft = 0;
    this.checkScrollable();
  }

  public onPreviousSearchPosition(): void {
    this.scrollable.nativeElement.scrollLeft = (Math.ceil(this.scrollable.nativeElement.scrollLeft / (3 * 261)) - 1) * 3 * 261;
    this.checkScrollable();
  }

  public onNextSearchPosition(): void {
    this.scrollable.nativeElement.scrollLeft = (Math.floor(this.scrollable.nativeElement.scrollLeft / (3 * 261)) + 1) * 3 * 261;
    console.log(this.scrollable.nativeElement.scrollLeft);
    console.log("native", this.scrollable.nativeElement.scrollWidth);
    this.checkScrollable();
  }

  public onLastSearchPosition(): void {
    this.scrollable.nativeElement.scrollLeft = this.scrollable.nativeElement.scrollWidth;
    this.checkScrollable();
  }

  checkScrollable() {
    if (Number(this.scrollable.nativeElement.scrollLeft) <= 0)
      this.first = true;
    else
      this.first = false;

    if (Number(this.scrollable.nativeElement.scrollLeft) >= (Number(this.scrollable.nativeElement.scrollWidth) - 986))
      this.last = true;
    else
      this.last = false;

  }

  // Open form for create salary matrix
  Create() {
    this.generateMatrix = true;
    this.isUnderUpdate = false;
    this.showCreateForm = false;
    this.isUnderMatrixUpdate = true;
    this.isNotGeneratedAlert = true;
    this.submitted = false;         
    this.prevTotCol = 0;
    this.prevTotRow = 0;
    this.prevTotRowVal = 0;
    setTimeout(() => {
      this.showCreateForm = true;
      setTimeout(() => {
        window.scrollTo(0, 2000);
      }, 10);
    }, 10);
    this.model = {
      id: 0,
      salaryMatrixId: '',
      description: '',
      arabicSalaryMatrixDescription: '',
      payUnit: "",
      totalRow: null,
      totalRowValues: null,
      totalColumns: null,
      listSalaryMatrixRow: Array<SalaryMatrixRowModule>(),
      listSalaryMatrixColSetup: Array<SalaryMatrixColumnModule>()
    };
  }

  // Clear form to reset to default blank
  Clear(f: NgForm, t: NgForm) {
    this.model.listSalaryMatrixRow = new Array<SalaryMatrixRowModule>();
    this.model.listSalaryMatrixColSetup = new Array<SalaryMatrixColumnModule>();
    t.resetForm({
      id: 0,
      salaryMatrixId: '',
      description: '',
      arabicSalaryMatrixDescription: '',
      payUnit: "",
      totalRow: null,
      totalRowValues: null,
      totalColumns: null
    });
    //console.log(this.salaryMatrixSetupIdvalue);
    this.isNotGeneratedAlert = true;
    this.generateMatrix = true;
    this.submitted = false;
  }

  //function call for creating new salary matrix
  CreateSalaryMatrix(f: NgForm, event: Event) {
    event.preventDefault();
    var salIdx = this.model.salaryMatrixId;
    //Check if the id is available in the model.
    //If id avalable then update the salary matrix, else Add new salary matrix
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
      if (this.isNotGeneratedAlert) {
        this.isConfirmationModalOpen2 = true;
        this.message.type = 'success';

        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          window.setTimeout(() => {
            this.showMsg = false;
            this.isConfirmationModalOpen2 = false;
          }, 4000);
          this.message.text = this.isNotGeneratedText;
        }, 100);
      }
      else {
        this.isConfirmationModalOpen = true;
        this.isConfirmationModalOpen2 = false;
        this.isDeleteAction = false;
      }
    }
    else {
      //Check for duplicate Salary Matrix Id
      this.salaryMatrixSetupService.checkDuplicateSalaryMatrixSetupId(salIdx).then(response => {
        if (response && response.code == 302 && response.result && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = 'success';

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;
          }, 100);
        } else {

          if (this.isNotGeneratedAlert) {
            this.isConfirmationModalOpen2 = true;
            this.message.type = 'success';

            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              window.setTimeout(() => {
                this.showMsg = false;
                this.isConfirmationModalOpen2 = false;
              }, 4000);
              this.message.text = this.isNotGeneratedText;
            }, 100);
          }
          else {
            //Call service api for Creating new Salary Matrix
            this.salaryMatrixSetupService.createSalaryMatrixSetup(this.model).then(data => {
              var datacode = data.code;
              if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                  }, 4000);
                  this.showCreateForm = false;
                  this.prevTotCol = 0;
                  this.prevTotRow = 0;
                  this.prevTotRowVal = 0;
                  this.messageText = data.btiMessage.message;
                  ;
                }, 100);

                this.hasMsg = true;
                f.resetForm();
                //Refresh the Grid data after adding new location
                this.setPage({ offset: 0 });
              }
            }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin !';
              }, 100)
            });
          }
        }

      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = 'Server error. Please contact admin !';
        }, 100)
      });
    }
  }

  //edit matrix by row
  edit(row: SalaryMatrixSetupModule) {
    this.submitted = false;
    this.generateMatrix = true;
    this.showCreateForm = true;
    this.isUnderUpdate = true;
    this.model = cloneDeep(row);
    this.prevTotCol = row.totalColumns;
    this.prevTotRow = row.totalRow;
    this.prevTotRowVal = row.totalRowValues;
    this.salaryMatrixSetupIdvalue = this.model.salaryMatrixId;
    this.generateSalaryMatrix();
    setTimeout(() => {
      window.scrollTo(0, 2000);
    }, 1000);

  }

  onRVCChange() {
    if (Number(this.model.totalRow) != Number(this.prevTotRow))
      this.isNotGeneratedAlert = true;
    else if (Number(this.model.totalRowValues) != Number(this.prevTotRowVal))
      this.isNotGeneratedAlert = true;
    else if (Number(this.model.totalColumns) != Number(this.prevTotCol))
      this.isNotGeneratedAlert = true;
    else
      this.isNotGeneratedAlert = false;
  }

  updateStatus() {
    this.closeModal();
    this.model.salaryMatrixId = this.salaryMatrixSetupIdvalue;
    //Call service api for Creating new location
    //console.log(this.model);
    this.salaryMatrixSetupService.updateSalaryMatrixSetup(this.model).then(data => {
      var datacode = data.code;
      if (datacode == 201) {
        //Refresh the Grid data after editing matrix
        this.setPage({ offset: 0 });

        //Scroll to top after editing Salary Matrix
        window.scrollTo(0, 0);
        window.setTimeout(() => {
          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          this.messageText = data.btiMessage.message;          
          this.prevTotCol = 0;
          this.prevTotRow = 0;
          this.prevTotRowVal = 0;
          this.showCreateForm = false;
        }, 100);

        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = 'Server error. Please contact admin !';
      }, 100)
    });

  }

  varifyDelete() {
    if (this.selected.length > 0) {
      this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
    }
  }


  //delete matrix by passing whole object of perticular matrix
  delete() {
    var selectedMatrices = [];
    for (var i = 0; i < this.selected.length; i++) {
      selectedMatrices.push(this.selected[i].id);
    }
    this.salaryMatrixSetupService.deleteSalaryMatrixSetup(selectedMatrices).then(data => {
      var datacode = data.code;
      if (datacode == 200) {
        this.setPage({ offset: 0 });
      }
      this.hasMessage = true;
      if(datacode == "302"){
        this.message.type = "error";
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
      } else {
          this.isSuccessMsg = true;
          this.message.type = "success";
          this.isfailureMsg = false;
      }
      //this.message.text = data.btiMessage.message;

      window.scrollTo(0, 0);
      window.setTimeout(() => {
        this.showMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMessage = false;
        }, 4000);
        this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of matrix
      this.setPage({ offset: 0 });

    }).catch(error => {
      this.hasMessage = true;
      this.message.type = 'error';
      var errorCode = error.status;
      this.message.text = 'Server issue. Please contact admin !';
    });
    this.closeModal();
  }

  // default list on page
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  // search matrix by keyword
  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    //this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.salaryMatrixSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;
      //console.log(pagedData.data);
      this.table.offset = 0;
    });
  }

  generateSalaryMatrix() {
    window.setTimeout(() => {
      this.isUnderMatrixUpdate = true;
    }, 200);

    this.isNotGeneratedAlert = true;
    if (this.model.listSalaryMatrixRow.length == 0)
      Observable.forkJoin([this.initSalaryMatrix(), this.matrixRowValsAndCols()]).subscribe(t => {
        this.isNotGeneratedAlert = false;
        this.prevTotCol = this.model.totalColumns;
        this.prevTotRow = this.model.totalRow;
        this.prevTotRowVal = this.model.totalRowValues;
        this.isUnderMatrixUpdate = false;
        this.submitted = false;
        this.first = true;
        if (Number(this.model.totalColumns) <= 3)
          this.last = true;
        else
          this.last = false;
      });
    else
      Observable.forkJoin([this.updateSalaryMatrix(), this.matrixRowValsAndCols()]).subscribe(t => {
        this.isNotGeneratedAlert = false;
        this.prevTotCol = this.model.totalColumns;
        this.prevTotRow = this.model.totalRow;
        this.prevTotRowVal = this.model.totalRowValues;
        this.isUnderMatrixUpdate = false;
        this.submitted = false;
        this.first = true;
        if (Number(this.model.totalColumns) <= 3)
          this.last = true;
        else
          this.last = false;
      });


  }

  matrixRowValsAndCols() {
    this.rowValCell = [];
    this.rowColCell = [];
    for (let i = 0; i < this.model.totalRowValues; i++) {
      this.rowValCell.push(i);
    }
    for (let i = 0; i < this.model.totalColumns; i++) {
      this.rowColCell.push(i);
    }
    return Observable.of(true);
  }

  initSalaryMatrix() {
    this.temp = new Array<SalaryMatrixRowValueModule>();
    for (let i = 0; i < this.model.totalRowValues * this.model.totalColumns; i++) {
      this.temp.push(new SalaryMatrixRowValueModule(i + 1, 0.00, ''));
    }
    for (let i = 0; i < this.model.totalRow; i++) {
      this.model.listSalaryMatrixRow.push(new SalaryMatrixRowModule('', '', cloneDeep(this.temp)));
    }
    for (let i = 0; i < this.model.totalColumns; i++) {
      this.model.listSalaryMatrixColSetup.push(new SalaryMatrixColumnModule('', ''));
    }
    return Observable.of(true);
  }

  updateSalaryMatrix() {

    this.temp = new Array<SalaryMatrixRowValueModule>();

    //row update
    if (this.model.listSalaryMatrixRow.length < Number(this.model.totalRow)) {
      for (let i = 0; i < Number(this.model.totalRowValues) * Number(this.model.totalColumns); i++) {
        this.temp.push(new SalaryMatrixRowValueModule(i + 1, 0.00, ''));
      }
      for (let i = this.model.listSalaryMatrixRow.length; i < Number(this.model.totalRow); i++) {
        this.model.listSalaryMatrixRow.push(new SalaryMatrixRowModule('', '', cloneDeep(this.temp)));
      }
    }
    else if (this.model.listSalaryMatrixRow.length > Number(this.model.totalRow)) {
      this.model.listSalaryMatrixRow.splice(Number(this.model.totalRow), this.model.listSalaryMatrixRow.length - Number(this.model.totalRow));
    }

    // column update
    if (this.model.listSalaryMatrixColSetup.length < Number(this.model.totalColumns)) {
      for (let i = this.model.listSalaryMatrixColSetup.length; i < Number(this.model.totalColumns); i++) {
        this.model.listSalaryMatrixColSetup.push(new SalaryMatrixColumnModule('', ''));
      }
    }
    else if (this.model.listSalaryMatrixColSetup.length > Number(this.model.totalColumns)) {
      this.model.listSalaryMatrixColSetup.splice(Number(this.model.totalColumns), this.model.listSalaryMatrixColSetup.length - Number(this.model.totalColumns));
    }

    //row value update
    if (Number(this.prevTotRowVal) < Number(this.model.totalRowValues)) {
      let numRow;
      if (Number(this.prevTotRow) < Number(this.model.totalRow))
        numRow = Number(this.prevTotRow);
      else
        numRow = Number(this.model.totalRow);
      for (let j = 0; j < numRow; j++) {
        let seq = Number(this.prevTotCol) * Number(this.prevTotRowVal) - 1;

        this.temp = new Array<SalaryMatrixRowValueModule>();
        this.temp = this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue;

        for (let i = seq + 1; i < Number(this.model.totalRowValues) * Number(this.model.totalColumns); i++) {
          this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue.push(new SalaryMatrixRowValueModule(i + 1, 0.00, ''));
        }
        //console.log("UPDATED: ", this.temp, "New: ", this.model.listSalaryMatrixRow[j]);

        if (Number(this.prevTotCol) < Number(this.model.totalColumns)) {
          for (let k = Number(this.prevTotCol) - 1; k > -1; k--) {
            for (let i = Number(this.prevTotRowVal) - 1; i > -1; i--) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].amount = this.temp[seq].amount;
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].desc = this.temp[seq--].desc;
            }
            for (let i = Number(this.model.totalRowValues) - 1; i > Number(this.prevTotRowVal) - 1; i--) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].amount = 0;
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].desc = '';
            }
          }
          for (let k = Number(this.model.totalColumns) - 1; k > Number(this.prevTotCol) - 1; k--) {
            for (let i = Number(this.model.totalRowValues) - 1; i > - 1; i--) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].amount = 0;
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].desc = '';
            }
          }
        }
        else {
          for (let k = Number(this.model.totalColumns) - 1; k > -1; k--) {
            for (let i = Number(this.prevTotRowVal) - 1; i > -1; i--) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].amount = this.temp[seq].amount;
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].desc = this.temp[seq--].desc;
            }
            for (let i = Number(this.model.totalRowValues) - 1; i > Number(this.prevTotRowVal) - 1; i--) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].amount = 0;
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.model.totalRowValues) + i].desc = '';
            }
          }
        }
      }
    }
    else if (Number(this.prevTotRowVal) > Number(this.model.totalRowValues)) {
      let numRow;
      if (Number(this.prevTotRow) < Number(this.model.totalRow))
        numRow = Number(this.prevTotRow);
      else
        numRow = Number(this.model.totalRow);

      for (let j = 0; j < numRow; j++) {
        let seq = Number(this.prevTotCol) * Number(this.prevTotRowVal) - 1;

        if (Number(this.prevTotCol) <= Number(this.model.totalColumns)) {
          for (let k = Number(this.prevTotCol) - 1; k > -1; k--) {
            for (let i = 0; i < Number(this.model.totalRowValues); i++) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.prevTotRowVal) + i].sequence = k * Number(this.model.totalRowValues) + i + 1;
            }
            this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue.splice(k * Number(this.prevTotRowVal) + Number(this.model.totalRowValues), Number(this.prevTotRowVal) - Number(this.model.totalRowValues));
          }
          for (let k = Number(this.model.totalColumns) - 1; k > Number(this.prevTotCol) - 1; k--) {
            for (let i = 0; i < Number(this.model.totalRowValues); i++) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue.push(new SalaryMatrixRowValueModule(k * Number(this.model.totalRowValues) + i + 1, 0.00, ''));
            }
          }
        }
        else {
          this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue.splice(Number(this.model.totalColumns) * Number(this.prevTotRowVal), Number(this.prevTotRowVal) * Number(this.prevTotCol) - Number(this.model.totalColumns) * Number(this.prevTotRowVal));
          for (let k = Number(this.model.totalColumns) - 1; k > -1; k--) {
            for (let i = 0; i < Number(this.model.totalRowValues); i++) {
              this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue[k * Number(this.prevTotRowVal) + i].sequence = k * Number(this.model.totalRowValues) + i + 1;
            }
            this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue.splice(k * Number(this.prevTotRowVal) + Number(this.model.totalRowValues), Number(this.prevTotRowVal) - Number(this.model.totalRowValues));
          }
        }
      }
    }
    else {
      let numRow;
      if (Number(this.prevTotRow) < Number(this.model.totalRow))
        numRow = Number(this.prevTotRow);
      else
        numRow = Number(this.model.totalRow);

      for (let j = 0; j < numRow; j++) {
        let seq = Number(this.prevTotCol) * Number(this.prevTotRowVal) - 1;
        for (let i = seq + 1; i < Number(this.model.totalRowValues) * Number(this.model.totalColumns); i++) {
          this.model.listSalaryMatrixRow[j].listSalaryMatrixRowValue.push(new SalaryMatrixRowValueModule(i + 1, 0.00, ''));
        }
      }
    }
    return Observable.of(true);
  }

  // Set default page size
  changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0 });
  }

  confirm(): void {
    this.messageText = 'Confirmed!';
    this.delete();
  }

  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }

  closeAlert() {
    this.isConfirmationModalOpen2 = false;
  }

  CheckNumber(event) {
    if (isNaN(event.target.value) == true) {
      this.model.id = 0;
      return false;
    }
  }

  checkdecimalcell(input) {
    //console.log(digit);
    this.tempd = input.value.split(".");
    //console.log(this.tempd);
    if (this.tempd != null && this.tempd[1] != null && ((this.tempd[0].length > 7) || (this.tempd[1] != null && this.tempd[1].length > 3))) {
      //console.log("in the condition");
      input.setErrors({ 'incorrect': true });
      //console.log(this.islifeTimeValidEmployerpay);
    }
    else if (this.tempd != null && ((this.tempd[1] == null && this.tempd[0].length > 7) || (this.tempd[1] != null && this.tempd[1].length > 3))) {
      //console.log("in the condition");
      input.setErrors({ 'incorrect': true });
      //console.log(this.islifeTimeValidEmployerpay);
    }
    else {
      input.setErrors({ 'incorrect': false });
      //console.log(this.islifeTimeValidEmployerpay);
    }

  }


}

