import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Router } from '@angular/router';
import { HealthInsuranceEnrollment } from '../../_models/health-insurance-enrollment/health-insurance-enrollment.module';
import { HealthInsuranceEnrollmentService } from '../../_services/health-insurance-enrollment/health-insurance-enrollment.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { SupervisorService } from '../../_services/supervisor/supervisor.service';
import { DivisionService } from '../../_services/division/division.service';
import { DepartmentService } from '../../_services/department/department.service';
import { PositionSetupService } from '../../_services/position-setup/position-setup.service';
import { LocationService } from '../../_services/location/location.service';
import { UploadFileService } from '../../../_sharedresource/_services/upload-file.service';
import { Page } from '../../../_sharedresource/page';
import { DatePipe } from '@angular/common';
import {CommonService} from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'health-insurance-enrollment',
    templateUrl: './health-insurance-enrollment.component.html',
    providers: [
        HealthInsuranceEnrollmentService,
        SupervisorService,
        DivisionService,
        DepartmentService,
        PositionSetupService,
        LocationService,
        DatePipe,
        CommonService
    ]
})
export class HealthInsuranceEnrollmentComponent {
    page = new Page();
    rows = new Array<HealthInsuranceEnrollment>();
    temp = new Array<HealthInsuranceEnrollment>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1446';
    moduleName;
    screenName;
    defaultFormValues:object [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    healthInsuranceId: number;
    model: HealthInsuranceEnrollment;
    searchKeyword = '';
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    ddPageSize: number = 5;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    getDepartment: any[] = [];
    getLocation: any[] = [];
    tempp: string[] = [];
    departmentIdList: Observable<any>;
    healthInsuranceIdList: Observable<any>;
    employeeIdList: Observable<any>;
    getHealthInsurance: any[] = [];
    getEmployee: any[] = [];
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    employeeIndexId = null;
    modelEligibilityDate;
    frmEligibilityDate;
    modelBenefitStartDate;
    frmBenefitStartDate;
    modelBenefitEndDate;
    frmBenefitEndDate;
    Description;
    EmployeeName;
    employeeId;
    groupNumber;
    gridLists;
    getScreenDetailArr;
    isConfigure = false;
    isShowHideGrid = false;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder: boolean = false;
    HelthInsuranceId;
    helthInsuranceIdValue;
    typeOfCoverage: any;
    InsAmount: number;
    CostToEmp: number;
    CostToEmployer: number;
    AddCost: number;
    errorbenefitEndDate: any = { isError: false, errorMessage: '' };
    errorbenefitStartDate: any = { isError: false, errorMessage: '' };
    errorbenefitEligibilityDate: any = { isError: false, errorMessage: '' };
    insuredAmountPattern:boolean = true;
    costToEmployeeAmountPattern: boolean = true;
    costToEmployerAmountPattern: boolean = true;
    additionalCostAmountPattern: boolean = true;
    selectedHelthInsDetails;

    EMPLOYEE_ID: any;
    HEALTH_INSURANCE_ID: any;
    STATUS: any;
    ELIGIBILITY_DATE: any;
    BENEFIR_START_DATE: any;
    BENEFIT_END_DATE: any;
    OVERRIDE_COST: any;
    INSURED_AMOUNT: any;
    COST_TO_EMPLOYEE: any;
    COST_TO_EMPLOYER: any;
    ADDITIONAL_COST: any;
    PLOICY_NUMBER: any;
    DELETE: any;
    SAVE: any;
    PRINT: any;
    CLEAR: any;
    NAME: any;
    DESCRIPTION: any;
    TYPEOF_COVERAGE: any;
    GROUP_NUMBER: any;
    CREATE: any;
    UPDATE: any;
    ACTION: any;
    CANCEL: any;
    SEARCH: any;

    constructor(private router: Router,
        private healthInsuranceEnrollmentService: HealthInsuranceEnrollmentService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private supervisorService: SupervisorService,
        private divisionService: DivisionService,
        private departmentService: DepartmentService,
        private positionSetupService: PositionSetupService,
        private locationService: LocationService,
        private datePipe: DatePipe,
        private uploadFileService: UploadFileService,
        private commonService: CommonService) {

        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'HEALTH_INSURANCE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'STATUS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ELIGIBILITY_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIR_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'OVERRIDE_COST', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INSURED_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COST_TO_EMPLOYEE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COST_TO_EMPLOYER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDITIONAL_COST', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PLOICY_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PRINT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TYPEOF_COVERAGE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'GROUP_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
        ];

        this.EMPLOYEE_ID = this.defaultFormValues[0];
        this.HEALTH_INSURANCE_ID = this.defaultFormValues[1];
        this.STATUS = this.defaultFormValues[2];
        this.ELIGIBILITY_DATE = this.defaultFormValues[3];
        this.BENEFIR_START_DATE = this.defaultFormValues[4];
        this.BENEFIT_END_DATE = this.defaultFormValues[5];
        this.OVERRIDE_COST = this.defaultFormValues[6];
        this.INSURED_AMOUNT = this.defaultFormValues[7];
        this.COST_TO_EMPLOYEE = this.defaultFormValues[8];
        this.COST_TO_EMPLOYER = this.defaultFormValues[9];
        this.ADDITIONAL_COST = this.defaultFormValues[10];
        this.PLOICY_NUMBER = this.defaultFormValues[11];
        this.DELETE = this.defaultFormValues[12];
        this.SAVE = this.defaultFormValues[13];
        this.PRINT = this.defaultFormValues[14];
        this.CLEAR = this.defaultFormValues[15];
        this.NAME = this.defaultFormValues[16];
        this.DESCRIPTION = this.defaultFormValues[17];
        this.TYPEOF_COVERAGE = this.defaultFormValues[18];
        this.GROUP_NUMBER = this.defaultFormValues[19];
        this.CREATE = this.defaultFormValues[20];
        this.UPDATE = this.defaultFormValues[21];
        this.ACTION = this.defaultFormValues[22];
        this.CANCEL = this.defaultFormValues[23];
        this.SEARCH = this.defaultFormValues[24];

        this.employeeIdList = Observable.create((observer: any) => {
            observer.next(this.model.employeeMaster.employeeId);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

        this.healthInsuranceIdList = Observable.create((observer: any) => {
            observer.next(this.helthInsuranceIdValue);
        }).mergeMap((token: string) => this.gethealthInsuranceIdAsObservable(token));
    }

    ngOnInit() {

        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        this.healthInsuranceEnrollmentService.gethealthInsuranceId().then(data => {
            this.getHealthInsurance = data.result;
        });
        this.supervisorService.getEmployee().then(data => {
            this.getEmployee = data.result;
        });

        // Following shifted from SetPage for better performance
        this.departmentService.getDepartments().then(data => {
            this.getDepartment = data.result;
        });
    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'employeeId') {
            return row['employeeMaster'] && row['employeeMaster']['employeeId'] ? row['employeeMaster']['employeeId'] : '';
        } else if (gridFieldName === 'employeeTitle') {
            return row['employeeMaster'] && row['employeeMaster']['employeeTitle'] ? row['employeeMaster']['employeeTitle'] : '';
        } else if (gridFieldName === 'employeeFirstName') {
            return row['employeeMaster'] && row['employeeMaster']['employeeFirstName'] ? row['employeeMaster']['employeeFirstName'] : '';
        } else if (gridFieldName === 'helthInsuranceId') {
            return row['helthInsurance'] && row['helthInsurance']['helthInsuranceId'] ? row['helthInsurance']['helthInsuranceId'] : '';
        } else if (gridFieldName === 'helthCoverageTypeId') {
            return row['helthInsurance'] && row['helthInsurance']['helthCoverageTypeId'] ? row['helthInsurance']['helthCoverageTypeId'] : '';
        } else if (gridFieldName === 'groupNumber') {
            return row['helthInsurance'] && row['helthInsurance']['groupNumber'] ? row['helthInsurance']['groupNumber'] : '';
        } else if (gridFieldName === 'eligibilityDate') {
            return row[gridFieldName] ? this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy') : '';
        } else if (gridFieldName === 'benefitStartDate') {
            return row[gridFieldName] ? this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy') : '';
        } else if (gridFieldName === 'benefitEndDate') {
            return row[gridFieldName] ? this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy') : '';
        } else if (gridFieldName === 'status') {
            return (
                row[gridFieldName] === 1 ? 'Active' :
                    row[gridFieldName] === 2 ? 'Inactive' :
                        row[gridFieldName] === 3 ? 'Waived' :
                            row[gridFieldName] === 4 ? 'Ineligible' :
                                row[gridFieldName] === 5 ? 'Terminated' :
                                    row[gridFieldName] === 6 ? 'Family Leave' :
                                        row[gridFieldName] === 7 ? 'Pending' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

    // Open form for create department
    Create() {
        this.groupNumber = null;
        this.typeOfCoverage = null;
        this.helthInsuranceIdValue = null;
        this.EmployeeName = null;
        this.employeeId = null;
        this.Description = null;
        this.modelEligibilityDate = null;
        this.modelBenefitStartDate = null;
        this.modelBenefitEndDate = null;
        this.InsAmount = null;
        this.CostToEmp = null;
        this.CostToEmployer = null;
        this.AddCost = null;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.errorbenefitEndDate = {isError: false, errorMessage: ''};
        this.insuredAmountPattern = true;
        this.costToEmployeeAmountPattern = true;
        this.costToEmployerAmountPattern = true;
        this.additionalCostAmountPattern = true;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            status: null,
            eligibilityDate: null,
            benefitStartDate: null,
            benefitEndDate: null,
            overrideCost: false,
            insuredAmount: null,
            costToEmployee: null,
            costToEmployeer: null,
            additionalCost: null,
            policyNumber: null,
            employeeMaster: {
                employeeIndexId: null,
                employeeId:null
            },
            helthInsurance: {
                id: null
            }
        };
        this.employeeIndexId = null;
        this.HelthInsuranceId = null;
    }

    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn === undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy === undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.healthInsuranceEnrollmentService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    getEmployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployee.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }
    gethealthInsuranceIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        console.log(Observable.of(
            this.getHealthInsurance.filter((id: any) => {
                return query.test(id.helthInsuranceId);
            })
        ));
        return Observable.of(
            this.getHealthInsurance.filter((id: any) => {
                return query.test(id.helthInsuranceId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        console.log('E', e)
        this.healthInsuranceEnrollmentService.getEmpHealthInsEnrolmntById(e.item.employeeIndexId).then(pagedData => {
            console.log('pageData', pagedData)
            this.model = pagedData.result;
            this.model.id = 0;
            this.model.employeeMaster = this.model['dtoEmployeeMaster']
            this.model.helthInsurance = this.model['dtoHelthInsurance']
            this.groupNumber = this.model.helthInsurance['groupNumber'];
            this.typeOfCoverage = this.model.helthInsurance['helthCoverageDesc'];
            this.helthInsuranceIdValue = this.model.helthInsurance['helthInsuranceId'];
            this.InsAmount = this.model.insuredAmount;
            this.CostToEmp = this.model.costToEmployee;
            this.CostToEmployer = this.model.costToEmployeer;
            this.AddCost = this.model.additionalCost;
            this.Description = this.model.helthInsurance['desc'];
            this.HelthInsuranceId = this.model.helthInsurance['id'];
            console.log('this.model', this.model)
            this.modelBenefitStartDate = this.formatDateFordatePicker(new Date(this.model.benefitStartDate));
            this.modelBenefitEndDate = this.formatDateFordatePicker(new Date(this.model.benefitEndDate));
            this.modelEligibilityDate = this.formatDateFordatePicker(new Date(this.model.eligibilityDate));
        });
        if (typeof (e.item.employeeFirstName) !== 'undefined')
            this.employeeIndexId = e.item.employeeIndexId;
            this.employeeId = e.item.employeeId;
            this.EmployeeName = e.item.employeeFirstName + ' ' + e.item.employeeMiddleName + ' ' + e.item.employeeLastName;
    }
    typeaheadOnSelectHelthInsurance(e: TypeaheadMatch): void {
        if (typeof (e.item.id) !== 'undefined')
            this.HelthInsuranceId = e.item.id;
        this.Description = e.item.desc;
        this.typeOfCoverage = e.item.helthCoverageDesc;
        this.groupNumber = e.item.groupNumber;
        this.selectedHelthInsDetails = e.item;
        // if(this.model.overrideCost){
            this.model.costToEmployee = e.item.employeePay;
            this.model.costToEmployeer = e.item.employerPay;
       // }
    }

    // edit department by row
    edit(row: any) {
        this.healthInsuranceId = row.helthInsurance.id;
        this.HelthInsuranceId = row.helthInsurance.id;
        this.modelBenefitStartDate = this.formatDateFordatePicker(new Date(row.benefitStartDate));
        this.modelBenefitEndDate = this.formatDateFordatePicker(new Date(row.benefitEndDate));
        this.modelEligibilityDate = this.formatDateFordatePicker(new Date(row.eligibilityDate));
        this.frmEligibilityDate = row.eligibilityDate;
        this.frmBenefitStartDate = row.benefitStartDate;
        this.frmBenefitEndDate = row.benefitEndDate;
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        console.log('this.model', this.model);
        this.groupNumber = row.helthInsurance.groupNumber;
        this.typeOfCoverage = row.helthInsurance.helthCoverageDesc;
        this.helthInsuranceIdValue = row.helthInsurance.helthInsuranceId;
        this.EmployeeName = row.employeeMaster.employeeFirstName + ' ' + row.employeeMaster.employeeMiddleName + ' ' + row.employeeMaster.employeeLastName;
        this.employeeId = row.employeeMaster.employeeId;
        this.Description = row.helthInsurance.desc;
        this.isUnderUpdate = true;
        this.employeeIndexId = row.employeeMaster.employeeIndexId;
        this.InsAmount = this.model.insuredAmount;
        this.CostToEmp = this.model.costToEmployee;
        this.CostToEmployer = this.model.costToEmployeer;
        this.AddCost = this.model.additionalCost;
        this.insuredAmountPattern = true;
        this.costToEmployeeAmountPattern = true;
        this.costToEmployerAmountPattern = true;
        this.additionalCostAmountPattern = true;
        if (this.model.overrideCost) {
            this.model.overrideCost = false;
        }
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        if (this.isUnderUpdate) {
            this.model.status = null;
            this.model.policyNumber = null;
        } else {
            f.resetForm({'overrideCost' : false});
        this.InsAmount = null;
        this.CostToEmp = null;
        this.CostToEmployer = null;
        this.AddCost = null;
        this.errorbenefitEndDate = {isError: false, errorMessage: ''};
        this.insuredAmountPattern = true;
        this.costToEmployeeAmountPattern = true;
        this.costToEmployerAmountPattern = true;
        this.additionalCostAmountPattern = true;
        }
    }

    // function call for creating new location
    CreateHealthInsuranceEnrollment(f: NgForm, event: Event) {
        event.preventDefault();
        this.model.eligibilityDate = this.frmEligibilityDate;
        this.model.benefitStartDate = this.frmBenefitStartDate;
        this.model.benefitEndDate = this.frmBenefitEndDate;
        var supIdx = this.HelthInsuranceId;
        this.model.helthInsurance.id = this.HelthInsuranceId;
        this.model.employeeMaster.employeeIndexId = this.employeeIndexId;
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            // Check if the id is available in the model.
            // If id avalable then update the location, else Add new location.

            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            let payload = {
                employeeId:this.model.employeeMaster.employeeIndexId,
                helthInsuranceId:this.model.helthInsurance.id
            }
            // Check for duplicate employeeIndexId according to it create new employeeIndexId
            this.healthInsuranceEnrollmentService.employeeIdcheck(payload).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.scrollTo(0, 400);
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                        this.showCreateForm = true;
                    }, 100);
                } else {
                    // Call service api for Creating new location
                    this.healthInsuranceEnrollmentService.createHealthInsuranceEnrollment(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);
                            this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after deletion of division
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                       }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin !';
                        }, 100);
                    });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin !';
                }, 100);
            });
        }
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    onEligibilityDateChanged(event: IMyDateModel): void {
        this.frmEligibilityDate = event.jsdate;
        this.model.eligibilityDate = event.epoc;

        /*if ((this.model.benefitStartDate < this.model.eligibilityDate) && this.model.benefitStartDate != undefined) {
            this.errorbenefitEligibilityDate = { isError: true, errorMessage: 'Invalid Eligibility Date.' };
        } else {
            this.errorbenefitEligibilityDate = { isError: false, errorMessage: '' };
        }
        if ((this.model.benefitEndDate < this.model.eligibilityDate) && this.model.benefitEndDate != undefined) {
            this.errorbenefitEligibilityDate = { isError: true, errorMessage: 'Invalid Eligibility Date.' };
        } else {
            this.errorbenefitEligibilityDate = { isError: false, errorMessage: '' };
        }*/

    }

    onBenefitStartDateChanged(event: IMyDateModel): void {
        this.frmBenefitStartDate = event.jsdate;
        this.model.benefitStartDate = event.epoc;

        /*if ((this.model.eligibilityDate > this.model.benefitStartDate) && this.model.benefitStartDate != undefined) {
            this.errorbenefitStartDate = { isError: true, errorMessage: 'Invalid Start Date.' };
        }
        if (this.model.eligibilityDate <= this.model.benefitStartDate) {
            this.errorbenefitStartDate = { isError: false, errorMessage: '' };
        }
        if (this.model.benefitStartDate == undefined && this.model.eligibilityDate == undefined) {
            this.errorbenefitStartDate = { isError: false, errorMessage: '' };
        }

        if ((this.model.benefitStartDate < this.model.benefitEndDate) && this.model.benefitEndDate != undefined) {
            this.errorbenefitStartDate = { isError: true, errorMessage: 'Invalid Start Date.' };
        } else {
            this.errorbenefitStartDate = { isError: false, errorMessage: '' };
        }

        if ((this.model.benefitStartDate < this.model.eligibilityDate) && this.model.benefitStartDate != undefined) {
            this.errorbenefitEligibilityDate = { isError: true, errorMessage: 'Invalid Eligibility Date.' };
        } else {
            this.errorbenefitEligibilityDate = { isError: false, errorMessage: '' };
        }
        if ((this.model.benefitEndDate < this.model.eligibilityDate) && this.model.benefitEndDate != undefined) {
            this.errorbenefitEligibilityDate = { isError: true, errorMessage: 'Invalid Eligibility Date.' };
        } else {
            this.errorbenefitEligibilityDate = { isError: false, errorMessage: '' };
        }*/

        if ((this.model.benefitStartDate > this.model.benefitEndDate) && this.model.benefitEndDate !== undefined) {
            this.errorbenefitEndDate = {isError: true, errorMessage: 'Invalid End Date.'};
        }
        if (this.model.benefitStartDate <= this.model.benefitEndDate) {
            this.errorbenefitEndDate = {isError: false, errorMessage: ''};
        }
        if (this.model.benefitEndDate === undefined && this.model.benefitStartDate === undefined) {
            this.errorbenefitEndDate = {isError: false, errorMessage: ''};
        }
    }
    onBenefitEndDateChanged(event: IMyDateModel): void {
        this.frmBenefitEndDate = event.jsdate;
        this.model.benefitEndDate = event.epoc;

        if (this.model.benefitEndDate === 0) {
            /*this.erroremployeeAdjustHireDate={isError:false,errorMessage:''};
            this.erroremployeeHireDate={isError:false,errorMessage:''};
            this.employeeLastDayDate= undefined;*/
            this.errorbenefitEndDate = {isError: false, errorMessage: ''};
            this.model.benefitEndDate = undefined;
            return;
        }
        /*if ((this.model.benefitStartDate > this.model.benefitEndDate) && this.model.benefitEndDate != undefined) {
            this.errorbenefitEndDate = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.model.benefitStartDate <= this.model.benefitEndDate) {
            this.errorbenefitEndDate = { isError: false, errorMessage: '' };
        }
        if (this.model.benefitEndDate == undefined && this.model.benefitStartDate == undefined) {
            this.errorbenefitEndDate = { isError: false, errorMessage: '' };
        }

        if ((this.model.benefitStartDate < this.model.eligibilityDate) && this.model.benefitStartDate != undefined) {
            this.errorbenefitEligibilityDate = { isError: true, errorMessage: 'Invalid Eligibility Date.' };
        } else {
            this.errorbenefitEligibilityDate = { isError: false, errorMessage: '' };
        }
        if ((this.model.benefitEndDate < this.model.eligibilityDate) && this.model.benefitEndDate != undefined) {
            this.errorbenefitEligibilityDate = { isError: true, errorMessage: 'Invalid Eligibility Date.' };
        } else {
            this.errorbenefitEligibilityDate = { isError: false, errorMessage: '' };
        }*/

        let startDate = this.model.benefitStartDate.toString().slice(0, 10);
        console.log(parseInt(startDate) + '>' + this.model.benefitEndDate)
        if ((parseInt(startDate) > this.model.benefitEndDate) && this.model.benefitEndDate !== undefined) {
            this.errorbenefitEndDate = {isError: true, errorMessage: 'Invalid End Date.'};
        }
        if (parseInt(startDate) <= this.model.benefitEndDate) {
            this.errorbenefitEndDate = {isError: false, errorMessage: ''};
        }
        if (this.model.benefitEndDate === undefined && this.model.benefitStartDate === undefined) {
            this.errorbenefitEndDate = {isError: false, errorMessage: ''};
        }
    }

    updateStatus() {
        this.closeModal();
        // Call service api for updating selected department
        this.model.helthInsurance.id = this.healthInsuranceId;
        this.healthInsuranceEnrollmentService.updateHealthInsuranceEnrollment(this.model).then(data => {
            var datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.showCreateForm = false;
                this.groupNumber = null;
                this.typeOfCoverage = null;
                this.helthInsuranceIdValue = null;
                this.EmployeeName = null;
                this.employeeId = null;
                this.Description = null;
                this.modelEligibilityDate = null;
                this.modelBenefitStartDate = null;
                this.modelBenefitEndDate = null;
                this.InsAmount = null;
                this.CostToEmp = null;
                this.CostToEmployer = null;
                this.AddCost = null;
                this.showCreateForm = false;
                this.isUnderUpdate = false;
                this.errorbenefitEndDate = {isError: false, errorMessage: ''};
                this.insuredAmountPattern = true;
                this.costToEmployeeAmountPattern = true;
                this.costToEmployerAmountPattern = true;
                this.additionalCostAmountPattern = true;
                this.model = {
                    id: 0,
                    status: null,
                    eligibilityDate: null,
                    benefitStartDate: null,
                    benefitEndDate: null,
                    overrideCost: false,
                    insuredAmount: null,
                    costToEmployee: null,
                    costToEmployeer: null,
                    additionalCost: null,
                    policyNumber: null,
                    employeeMaster: {
                        employeeIndexId: null,
                        employeeId:null
                    },
                    helthInsurance: {
                        id: null
                    }
                };
                this.employeeIndexId = null;
                this.HelthInsuranceId = null;

                // Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    print() {

        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        printContents = printContents.replace(/\r?\n|\r/g, "");
        printContents = printContents.replace(" ", "");
        printContents = printContents.replace(/<!--.*?-->/g, "");
    
        // var postedOnes = this.getElementsByIdStartsWith("print-section", "canvas", "canvasdiv-", printContents);
        // var postedOnes1 = this.getElementsByIdStartsWith("print-section", "canvas", "categorychart-", postedOnes);
        // this.getElementsByIdStartsWith("print-section", "canvas", "innerchart-", postedOnes1);
    
        printContents = document.getElementById('print-section').innerHTML;
    
        popupWin = window.open('', '_blank', 'top=0,left=0,height=400px,width=800px');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
        <link rel="stylesheet" type="text/css" media="screen,print" href="assets/css/bootstrap.min.css">
          <title>EMPLOYEE HEALTH INSURANCE ENROLLMENT</title>
          <style>
            .print-tabel{disaplay:block}
          </style>
        </head>
    <body onload="">${printContents}</body>
    <script type="text/javascript">
    window.print();
    if(navigator.userAgent.match(/iPad/i)){
      window.onfocus=function(){window.close(); }
    }else{        
      window.close();
    }
    </script>
      </html>`
        );
        popupWin.document.close();
    }

    printEmployeeDetails() {
        this.doNotPrintHearder = true;
        this.noPrintTable = true;
        this.printDetails = false;

        setTimeout(() => { this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint() {
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }


    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
        }
    }

    // delete traningCourse by passing whole object of perticular TrainingCourse
    delete() {
        var selectedEmp = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedEmp.push(this.selected[i].id);
        }

        this.healthInsuranceEnrollmentService.deleteHealthInsuranceEnrollment(selectedEmp).then(data => {
            var datacode = data.code;
            if (datacode === 200) {

            }
            // Refresh the Grid data after deletion of division
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }
    confirm(): void {
        this.messageText = 'Confirmed!';
        // this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn === val) {
            if (this.page.sortBy === 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    AmountPersistance(event) {
        console.log(event);
        if (!event && this.isUnderUpdate) {
            this.model.insuredAmount = this.InsAmount;
            this.model.costToEmployee = this.CostToEmp;
            this.model.costToEmployeer = this.CostToEmployer;
            this.model.additionalCost = this.AddCost;
        } else {
            this.model.insuredAmount = null;
            this.model.costToEmployee = event ? this.selectedHelthInsDetails.employeePay : null;
            this.model.costToEmployeer = event ? this.selectedHelthInsDetails.employerPay : null;
            this.model.additionalCost = null;
        }

        this.insuredAmountPattern = true;
        this.costToEmployeeAmountPattern = true;
        this.costToEmployerAmountPattern = true;
        this.additionalCostAmountPattern = true;
    }

    checkdecimalamount(digit, varToBeFalse) {
        var temp = true;
        if (digit === '' || digit === null) {
            if (varToBeFalse === 'insuredAmountPattern') {
                this.insuredAmountPattern = true;
            }
            if (varToBeFalse === 'costToEmployeeAmountPattern') {
                this.costToEmployeeAmountPattern = true;
            }
            if (varToBeFalse === 'costToEmployerAmountPattern') {
                this.costToEmployerAmountPattern = true;
            }
            if (varToBeFalse === 'additionalCostAmountPattern') {
                this.additionalCostAmountPattern = true;
            }
            return;
        }

        if (digit != null) {
            this.tempp = digit.toString().split('.');
            if (this.tempp != null && this.tempp[1] !== null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            } else if (this.tempp != null &&  ((this.tempp[1] === null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
                temp = false;
            } else {
                temp = true;
            }
        }

        if (varToBeFalse === 'insuredAmountPattern') {
            this.insuredAmountPattern = temp;
        }
        if (varToBeFalse === 'costToEmployeeAmountPattern') {
            this.costToEmployeeAmountPattern = temp;
        }
        if (varToBeFalse === 'costToEmployerAmountPattern') {
            this.costToEmployerAmountPattern = temp;
        }
        if (varToBeFalse === 'additionalCostAmountPattern') {
            this.additionalCostAmountPattern = temp;
        }
    }

}
