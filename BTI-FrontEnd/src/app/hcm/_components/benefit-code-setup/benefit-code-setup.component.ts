import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BenefitCodeSetup } from '../../_models/benefit-code-setup/benefit-code-setup.module';
import { BenefitCodeSetupService } from '../../_services/benefit-code-setup/benefit-code-setup.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'benefitCodeSetup',
    templateUrl: './benefit-code-setup.component.html',
    providers: [BenefitCodeSetupService, CommonService],
})
export class BenefitCodeSetupComponent {
    page = new Page();
    rows = new Array<BenefitCodeSetup>();
    temp = new Array<BenefitCodeSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1412';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    benefitId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: BenefitCodeSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
    start_date;
    end_date;
    islifeTimeValid: boolean = true;
    islifeTimeValidperyear: boolean = true;
    islifeTimeValidperperiod: boolean = true;
    islifeTimeValidamount: boolean = true;
    islifeTimeValidamountPercent: boolean = true;
    decimalpattern = '/^(((0|[1-9]\d{0,2})(\.\d{2})?)|())$/';
    tempp: string[] = [];
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    benefitIdvalue: number;
    error: any = { isError: false, errorMessage: '' };
    isBenefitCodeFormdisabled: boolean = false;
    isRadioSelected: boolean;
    isJoiningDateRadioSelected: boolean;

    BENEFIT_CODE: any;
    BENEFIT_DESCRIPTION: any;
    BENEFIT_ARABIC_DESCRIPTION: any;
    BENEFIT_START_DATE: any;
    BENEFIT_END_DATE: any;
    BENEFIT_FREQUENCY: any;
    BENEFIT_METHOD: any;
    BENEFIT_AMOUNT: any;
    BENEFIT_INACTIVE: any;
    BENEFIT_TRANSACTION_REQUIRED: any;
    BENEFIT_PER_PERIOD: any;
    BENEFIT_PER_YEAR: any;
    BENEFIT_LIFETIME: any;
    BENEFIT_CODE_DELETE_LABEL: any;
    BENEFIT_CODE_ACTION: any;
    BENEFIT_CODE_CREATE_LABEL: any;
    BENEFIT_CODE_SAVE_LABEL: any;
    BENEFIT_CODE_CLEAR_LABEL: any;
    BENEFIT_CODE_CANCEL_LABEL: any;
    BENEFIT_CODE_UPDATE_LABEL: any;
    BENEFIT_CODE_CREATE_FORM_LABEL: any;
    BENEFIT_CODE_UPDATE_FORM_LABEL: any;
    BENEFIT_CODE_SHORT: any;
    BENEFIT_AMOUNT_PERCENT: any;
    BENEFIT_CODE_TABLEVIEW: any;
    BENEFIT_CODE_MAXBENEFIT: any;
    BENEFIT_CODE_SEARCH: any;
    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    methods: DropDownModule[] = [
        { id: 1, name: 'Fixed Amount' },
        { id: 2, name: 'Amount Per Unit' },
        { id: 3, name: 'Percent Of Gross Wages' },
        { id: 4, name: 'Percent Of Net Wages' },
        { id: 5, name: 'Base on pay code' },
    ];
    frequencies: DropDownModule[] = [
        { id: 1, name: 'Weekly' },
        { id: 2, name: 'Biweekly' },
        { id: 3, name: 'Semimonthly' },
        { id: 4, name: 'Monthly' },
        { id: 5, name: 'Quarterly' },
        { id: 6, name: 'Semiannually' },
        { id: 7, name: 'Annually' },
        { id: 8, name: 'Daily/Miscellaneous' },
    ];
    methodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
    frequencyArray = ["Hourly", "Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually", "Daily/Miscellaneous"];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('dp2') input: ElementRef;
    benefitcodeIdList: Observable<any>;
    getbenefitCode: any[] = [];
    typeaheadLoading: boolean;
    isPerPeriod: boolean = false;
    isperYear: boolean = false;
    isLifeTime: boolean = false;
    isCompMaxTrxn: boolean = false;
    isCompAmount: boolean = false;
    checkEmpValidId: boolean = false;
    isCompMaxTrxnPp: boolean = false;
    isCompMaxTrxnLt: boolean = false;
    isCompMaxTrxnPy: boolean = false;
    errorMaxLt: string;
    errorMaxPy: string;
    errorMaxPp: string;
    errorDeductn: string;
    employeeJoiningDate: any = null;
    dateSection: boolean = true
    decimalValue: boolean = true;
    numOfDays: boolean;
    numDays: boolean;
    dateSectionDateDisplay: boolean;
    dateSectionDayDisplay: boolean;

    payCodeSettings = {
        singleSelection: false,
        enableCheckAll: true,
        text: "Select Paycode",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        searchPlaceholderText: "Search Pay Code",
        badgeShowLimit: 2,
        disabled: false
    }
    showSelectedPaycode: any[] = [];
    AllBasedOnPayCodeList: any[] = [];
    tempTotal: number;
    tempf: any;

    typeId = 3;
    typeData= [];

    constructor(private router: Router,
        private benefitCodeSetupService: BenefitCodeSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'BENEFIT_CODE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_START_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_END_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_FREQUENCY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_METHOD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_INACTIVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_TRANSACTION_REQUIRED', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_PER_PERIOD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_PER_YEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_LIFETIME', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_SHORT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_AMOUNT_PERCENT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_MAXBENEFIT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BENEFIT_CODE_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },

        ];
        this.BENEFIT_CODE = this.defaultFormValues[0];
        this.BENEFIT_DESCRIPTION = this.defaultFormValues[1];
        this.BENEFIT_ARABIC_DESCRIPTION = this.defaultFormValues[2];
        this.BENEFIT_START_DATE = this.defaultFormValues[3];
        this.BENEFIT_END_DATE = this.defaultFormValues[4];
        this.BENEFIT_FREQUENCY = this.defaultFormValues[5];
        this.BENEFIT_METHOD = this.defaultFormValues[6];
        this.BENEFIT_AMOUNT = this.defaultFormValues[7];
        this.BENEFIT_INACTIVE = this.defaultFormValues[8];
        this.BENEFIT_TRANSACTION_REQUIRED = this.defaultFormValues[9];
        this.BENEFIT_PER_PERIOD = this.defaultFormValues[10];
        this.BENEFIT_PER_YEAR = this.defaultFormValues[11];
        this.BENEFIT_LIFETIME = this.defaultFormValues[12];
        this.BENEFIT_CODE_DELETE_LABEL = this.defaultFormValues[13];
        this.BENEFIT_CODE_ACTION = this.defaultFormValues[14];
        this.BENEFIT_CODE_CREATE_LABEL = this.defaultFormValues[15];
        this.BENEFIT_CODE_SAVE_LABEL = this.defaultFormValues[16];
        this.BENEFIT_CODE_CLEAR_LABEL = this.defaultFormValues[17];
        this.BENEFIT_CODE_CANCEL_LABEL = this.defaultFormValues[18];
        this.BENEFIT_CODE_UPDATE_LABEL = this.defaultFormValues[19];
        this.BENEFIT_CODE_SHORT = this.defaultFormValues[20];
        this.BENEFIT_AMOUNT_PERCENT = this.defaultFormValues[21];
        this.BENEFIT_CODE_MAXBENEFIT = this.defaultFormValues[22];
        this.BENEFIT_CODE_SEARCH = this.defaultFormValues[23];
        this.BENEFIT_CODE_CREATE_FORM_LABEL = this.defaultFormValues[24];
        this.BENEFIT_CODE_UPDATE_FORM_LABEL = this.defaultFormValues[25];
        this.BENEFIT_CODE_TABLEVIEW = this.defaultFormValues[26];
        this.benefitcodeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.benefitId);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.getTypeData();
        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
        // Following shifted from SetPage for better performance
        this.benefitCodeSetupService.getBenefitcodeId().then(data => {
            this.getbenefitCode = data.result.records;
        });

        // this.dateSectionDateDisplay = true;
        // this.dateSectionDayDisplay = false;
    }

    getTypeData(){
        this.benefitCodeSetupService.getTypeFieldDetails(this.typeId).then(data => {
         if(data.code == 201)
         {
             let val = data.result.records;
             for(let i=0;i<val.length;i++)
             {
                 this.typeData.push({id:val[i].id,value:val[i].desc})
             }
         }
         else{
            this.typeData = [];
         }
        });
    }

    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'frequency') {
            return (
                row[gridFieldName] === 1 ? 'Hourly' :
                    row[gridFieldName] === 2 ? 'Weekly' :
                        row[gridFieldName] === 3 ? 'Biweekly' :
                            row[gridFieldName] === 4 ? 'Semimonthly' :
                                row[gridFieldName] === 5 ? 'Monthly' :
                                    row[gridFieldName] === 6 ? 'Quarterly' :
                                        row[gridFieldName] === 7 ? 'Semiannually' :
                                            row[gridFieldName] === 8 ? 'Annually' :
                                                row[gridFieldName] === 9 ? 'Daily/Miscellaneous' : ''
            );
        } else if (gridFieldName === 'method') {
            return (
                row[gridFieldName] === 1 ? 'Fixed Amount' :
                    row[gridFieldName] === 2 ? 'Amount Per Unit' :
                        row[gridFieldName] === 3 ? 'Percent Of Gross Wages' :
                            row[gridFieldName] === 4 ? 'Percent Of Net Wages' :
                                row[gridFieldName] === 5 ? 'Base on pay code' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

    getSuperviserIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getbenefitCode.filter((id: any) => {
                return query.test(id.benefitId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        console.log('E', e);
        this.benefitCodeSetupService.getBenefitCode(e.item.id).then(data => {
            console.log('DATA', data)
            this.model = data.result;
            this.model.id = 0;
            if(this.model.startDate && this.model.endDate){
                this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
                this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
            }
            this.selectedMethod(this.model.method);
        });
    }

    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn === undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy === undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.benefitCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
    compareTwoDates(f) {
        if (new Date(f.controls['frmEndDate'].value) < new Date(f.controls['frmStartDate'].value)) {
            this.error = { isError: true, errorMessage: 'End Date cannot before start date' };
        }
    }
    // Open form for create Benefit Code
    Create() {
        // this.islifeTimeValid = true;
        // this.islifeTimeValidperyear = true;
        this.islifeTimeValidperperiod = true;
        this.islifeTimeValidamount = true;
        this.islifeTimeValidamountPercent = true;
        this.dateSectionDateDisplay = false;
        this.dateSectionDayDisplay = false;
        this.numOfDays = true;
        this.numDays = true;
        this.modelStartDate = null;
        this.modelEndDate = null;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.isBenefitCodeFormdisabled = false;
        this.isRadioSelected = false;
        this.isJoiningDateRadioSelected = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            benefitId: null,
            desc: '',
            arbicDesc: '',
            startDate: this.modelStartDate,
            endDate: this.modelEndDate,
            frequency: 0,
            method: 0,
            amount: null,
            percent: null,
            transction: true,
            inActive: false,
            perPeriod: null,
            perYear: null,
            lifeTime: null,
            dtoPayCode: [],
            payFactor: null,
            customDate: null,
            noOfDays: 0,
            endDateDays: 0,
            benefitTypeId:0,
            roundOf:0
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ method: '0', frequency: '0' });
        this.isBenefitCodeFormdisabled = false;
    }


    //function call for creating new Benefit Code
    CreateBenefitCode(f: NgForm, event: Event) {
        console.log('this.model', this.model)
        if(this.model.customDate){
            this.model.startDate = this.startDate;
            this.model.endDate = this.endDate;
        } else{
            this.model.startDate = null;
            this.model.endDate = null;
        }
        event.preventDefault();
        var locIdx = this.model.benefitId;
        console.log('this.model', this.model)
        //Check if the id is available in the model.
        //If id avalable then update the Benefit Code, else Add new Benefit Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Benefit Code Id according to it create new Benefit Code
            this.benefitCodeSetupService.checkDuplicateBenefitId(locIdx).then(response => {
                if (response && response.code == 201 && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';
                    window.scrollTo(0, 800);
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Benefit Code
                    this.model.dtoPayCode = this.showSelectedPaycode
                    this.benefitCodeSetupService.createBenefitCodeSetup(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Benefit Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });

        }
    }

    //edit department by row
    edit(row: BenefitCodeSetup) {
        this.showSelectedPaycode = [];
        console.log('EDIT', row)
        this.model = Object.assign({}, row);
        console.log('this.model', this.model)
        this.benefitCodeSetupService.getBenefitCode(row.id).then(data => {
            console.log('DATA', data)
            this.model.noOfDays = data.result.noOfDays;
            this.model.endDateDays = data.result.endDateDays;
            this.selectedMethod(data.result.method);
            console.log("CUSTOM", data.result.customDate);
            if (data.result.customDate) {
                this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
                this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
                this.dateSectionDateDisplay = true;
                this.dateSectionDayDisplay = false;
                this.numOfDays = true;
                this.numDays = true;
                this.model.noOfDays = null;
                this.model.endDateDays = null;
            } else {
                this.dateSectionDateDisplay = false;
                this.dateSectionDayDisplay = true;
                this.numOfDays = false;
                this.numDays = false;
                this.modelEndDate = null;
                this.modelStartDate = null;
                this.model.startDate = null
                this.model.endDate = null
            }
            if (data.result.dtoPayCode != null && data.result.method == 5) {
                if (data.result.dtoPayCode.length) {
                    for (let i = 0; i < data.result.dtoPayCode.length; i++) {
                        let empdpt = {
                            id: data.result.dtoPayCode[i].id,
                            itemName: data.result.dtoPayCode[i].payCodeId,
                            payRate: data.result.dtoPayCode[i].payRate
                        }
                        this.showSelectedPaycode.push(empdpt);
                    }
                }
                setTimeout(() => {
                    this.amountCal();
                    setTimeout(() => {
                        this.totalAmount();
                    }, 1000);
                }, 500);
            }

        })
        // this.islifeTimeValid = true;
        // this.islifeTimeValidperyear = true;
        this.error = { isError: false, errorMessage: '' };
        this.islifeTimeValidperperiod = true;
        this.islifeTimeValidamount = true;
        this.islifeTimeValidamountPercent = true;
        this.showCreateForm = true;
        this.startDate = this.model.startDate;
        let strDate = new Date(this.model.startDate);
        this.frmStartDate = (strDate.getTime()) / 1000;
        let enddate = new Date(this.model.endDate);
        this.frmEndDate = (enddate.getTime()) / 1000;
        this.endDate = this.model.endDate;

        // this.model.startDate = this.modelStartDate
        // this.model.endDate 	= this.modelEndDate	
        this.benefitId = row.benefitId;
        this.isUnderUpdate = true;
        this.benefitIdvalue = this.model.benefitId;
        //this.model.id = this.model.benefitId;
        if (this.model.inActive == true) {
            this.isBenefitCodeFormdisabled = true;
        } else {
            this.isBenefitCodeFormdisabled = false;
        }

        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }



    updateStatus() {
        this.closeModal();
        this.model.benefitId = this.benefitIdvalue;
        if (this.model.startDate === undefined) {
            return;
        }
        //Call service api for Creating new Benefit Code
        console.log('PAYCODE', this.showSelectedPaycode)
        console.log('model', this.model)
        this.model.dtoPayCode = this.showSelectedPaycode;
        this.benefitCodeSetupService.updateBenefitCode(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedBenefitCodes = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedBenefitCodes.push(this.selected[i].id);
        }
        //console.log(this.selected);
        this.benefitCodeSetupService.deleteBenefitCode(selectedBenefitCodes).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.benefitCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    onStartDateChanged(event: IMyDateModel): void {
        this.isJoiningDateRadioSelected = false;
        this.startDate = event.jsdate;
        this.frmStartDate = event.epoc;
        if ((this.frmStartDate > this.frmEndDate) && this.frmEndDate !== undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.frmStartDate <= this.frmEndDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.frmEndDate === undefined && this.frmStartDate === undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.isJoiningDateRadioSelected = false;
        this.endDate = event.jsdate;
        this.frmEndDate = event.epoc;

        if ((this.frmStartDate > this.frmEndDate) && this.frmEndDate !== undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.frmStartDate <= this.frmEndDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.frmEndDate === undefined && this.frmStartDate === undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.benefitId = 0;
            return false;
        }
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-.()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn === val) {
            if (this.page.sortBy === 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    validateDecimal(event) {
    }
    checkdecimal(digit) {
        // if (digit === '' || digit == null) {
        //     this.islifeTimeValid = true;
        //     return;
        // }
        // this.tempp = digit.toString().split('.');
        // if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
        //     this.islifeTimeValid = false;
        // } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
        //     this.islifeTimeValid = false;
        // } else {
        //     this.islifeTimeValid = true;
        // }

    }
    checkdecimalperyear(digit) {
        if (digit === '' || digit == null) {
            this.islifeTimeValidperyear = true;
            return;
        }
        this.tempp = digit.toString().split('.');
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidperyear = false;
        } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidperyear = false;
        } else {
            this.islifeTimeValidperyear = true;
        }

    }

    checkdecimalperperiod(digit) {
        if (digit === '' || digit == null) {
            this.islifeTimeValidperperiod = true;
            return;
        }
        this.tempp = digit.toString().split('.');
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidperperiod = false;
        } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidperperiod = false;
        } else {
            this.islifeTimeValidperperiod = true;
        }

    }
    checkdecimalamount(digit) {
        this.isJoiningDateRadioSelected = false;
        if (digit === '' || digit == null) {
            this.islifeTimeValidamount = true;
            return;
        }
        this.tempp = digit.toString().split('.');
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidamount = false;
        } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidamount = false;
        } else {
            this.islifeTimeValidamount = true;
        }
    }
    // check percentage amount
    checkdecimalamountPercent(digit) {
        if (digit === '' || digit == null) {
            this.islifeTimeValidamountPercent = true;
            return;
        }
        this.tempp = digit.toString().split('.');
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
            this.islifeTimeValidamountPercent = false;
        } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
            this.islifeTimeValidamountPercent = false;
        } else {
            this.islifeTimeValidamountPercent = true;
        }
    }

    checkBenefitCodeForm(event) {
        if (event.target.checked == true) {
            this.isBenefitCodeFormdisabled = true;
        } else {
            this.isBenefitCodeFormdisabled = false;
        }

    }

    // Amount Compair with deduction amount
    amountComp(e) {
        let deduAmount = parseFloat(e);
        let pPAmount = Number(this.model.perPeriod);
        let pYAmoount = Number(this.model.perYear);
        let lAmount = Number(this.model.lifeTime);
        if (deduAmount != 0 && ((pPAmount != null || pYAmoount != null || lAmount != null))) {
            if (deduAmount < pPAmount) {
                this.isCompAmount = true;
                this.errorDeductn = 'This must be greater than Maximum Deduction';
            }
            else if (deduAmount < pYAmoount) {
                this.isCompAmount = true;
                this.errorDeductn = 'This must be greater than Maximum Deduction';
            }
            else if (deduAmount < lAmount) {
                this.isCompAmount = true;
                this.errorDeductn = 'This must be greater than Maximum Deduction';
            }
            else {
                this.isCompAmount = false;
            }
        }
        else {
            this.isCompAmount = false;
        }
    }

    // compair amount with trans Deduction

    amountTrnsDeductnPp(e) {
        let deduAmount = Number(this.model.amount);
        let pPAmount = parseFloat(e);
        if (deduAmount != 0 && pPAmount != 0) {
            if (pPAmount > deduAmount) {
                this.isCompAmount = true;
                this.errorDeductn = 'This must be greater than Maximum Deduction';
            }
            else {
                this.isCompAmount = false;
            }
        }
        else {
            this.isCompAmount = false;
        }

    }

    // compair amount with trans Deduction
    amountTrnsDeductnPy(e) {
        let deduAmount = Number(this.model.amount);
        let pYAmount = parseFloat(e);
        if (deduAmount != null && pYAmount != null) {
            if (pYAmount > deduAmount) {
                this.isCompAmount = true;
                this.errorDeductn = 'This must be greater than Maximum Deduction';
            }
            else {
                this.isCompAmount = false;
            }
        }
        else {
            this.isCompAmount = false;
        }

    }

    // compair amount with trans Deduction
    amountTrnsDeductnLt(e) {
        let deduAmount = Number(this.model.amount);
        if (e == "") {
            this.isCompAmount = true;
            this.errorDeductn = 'This must be greater than Maximum Deduction';
            return;
        }
        let lTAmount = parseFloat(e);
        // if (deduAmount != 0 && lTAmount != 0) {
        if (lTAmount > deduAmount) {
            this.isCompAmount = true;
            this.errorDeductn = 'This must be greater than Maximum Deduction';
        }
        else {
            this.isCompAmount = false;
        }
        // }
        // else {
        //   this.isCompAmount = false;
        // }

    }

    // Maximun Amount Compaire with Per Period
    maxAmountCompPp(e) {
        let pYAmoount = Number(this.model.perYear);
        let deduAmount = Number(this.model.amount);
        if (e == "") {

            this.isCompMaxTrxnLt = false;
            this.isCompMaxTrxnPp = true;
            this.isCompMaxTrxnPy = false;
            this.errorMaxPp = 'This can not be null';
        }
        let pPAmount = parseFloat(e);

        if (pPAmount > deduAmount) {
            this.errorMaxPp = 'This must be less than Deduction Amount';
        }
        // if (pYAmoount != null && pPAmount > pYAmoount) {
        //     this.isCompMaxTrxnLt = false;
        //     this.isCompMaxTrxnPp = true;
        //     this.isCompMaxTrxnPy = false;
        //     this.errorMaxPp = 'This must be less than Per Year Amount';
        // } else {
        //     this.isCompMaxTrxnPp = false;
        // }
        // else {
        //   this.isCompMaxTrxnPp = false;
        // }
    }

    // // Maximun Amount Compaire with Per year
    maxAmountCompPy(e) {
        let pPAmount = Number(this.model.perPeriod);
        let lTAmount = Number(this.model.lifeTime);
        // if (e == "") {
        //     this.isCompMaxTrxnLt = false;
        //     this.isCompMaxTrxnPy = true;
        //     this.errorMaxPy = 'This must be greater than Per Period';
        //     return;
        // }
        // let pYAmoount = parseFloat(e);
        // if (pPAmount != 0 && pYAmoount != 0) {
        // if (pYAmoount < pPAmount) {
        //     this.isCompMaxTrxnLt = false;
        //     this.isCompMaxTrxnPy = true;
        //     this.errorMaxPy = 'This must be greater than Per Period';
        // }
        // else {
        //     this.isCompMaxTrxnPy = false;
        // }
        // }
        // else {
        //   this.isCompMaxTrxnPp = false;
        // }
        // if (pYAmoount != 0 && lTAmount != 0) {
        // if (lTAmount != null) {
        //     if (pYAmoount > lTAmount) {
        //         this.isCompMaxTrxnLt = true;
        //         this.errorMaxLt = 'This must be less than LifeTime';
        //     } else {
        //         this.isCompMaxTrxnLt = false;
        //     }
        // }
        // }
    }

    // Maximun Amount Compaire with Life Time
    maxAmountCompLt(e) {
        // if (e == "") {
        //     this.isCompMaxTrxnLt = true;
        //     this.errorMaxLt = 'This must be greater than Per Year';
        //     return;
        // }
        // let lAmount = parseFloat(e);

        // let pYAmoount = Number(this.model.perYear);
        // if (lAmount != 0 && pYAmoount != null) {
        // if (lAmount < pYAmoount) {
        //     this.isCompMaxTrxnLt = true;
        //     this.errorMaxLt = 'This must be greater than Per Year';
        // }
        // else {
        //     this.isCompMaxTrxnLt = false;
        // }
        // }
        // else {
        //   this.isCompMaxTrxnLt = false;
        // }
    }

    RequiredValidation(eve: any) {
        // if (Number(this.model.perPeriod)) {
        //     this.isPerPeriod = true;
        // } else if (Number(this.model.perYear)) {
        //     this.isperYear = true;
        // } else if (Number(this.model.lifeTime)) {
        //     this.isLifeTime = true;
        // } else {
        //     this.isPerPeriod = false;
        //     this.isperYear = false;
        //     this.isLifeTime = false;
        // }
    }


    /** Method Change Function */
    selectedMethod(indx: any) {
        if (indx == 5) {
            let payload = {
                "id": indx,
                "sortOn": "id",
                "sortBy": "DESC",
                "searchKeyword": "",
                "pageNumber": 0,
                "pageSize": 5
            }
            this.AllBasedOnPayCodeList = []
            this.benefitCodeSetupService.getAllBasedOnPayCodeDropDown(payload).then(data => {
                if (data.result.records.length) {
                    for (let i = 0; i < data.result.records.length; i++) {
                        let empdpt = {
                            id: data.result.records[i].id,
                            itemName: data.result.records[i].payCodeId,
                            payRate: data.result.records[i].payRate
                        }
                        this.AllBasedOnPayCodeList.push(empdpt);
                    }
                }

            });
        }

    }

    /** Function for Multipal Paycode Select */


    onItemSelect(item: any) {
        console.log('item', item);
        this.model.amount = 0;
        let tempamount = 0;
        if (this.showSelectedPaycode.length) {
            this.showSelectedPaycode.forEach(ele => {
                tempamount = tempamount + ele.payRate
            })
        }
        this.model.amount = tempamount * Number(this.model.payFactor)

    }
    OnItemDeSelect(item: any) {
        console.log('item', item);
        this.model.amount = 0;
        let tempamount = 0;
        if (this.showSelectedPaycode.length) {
            this.showSelectedPaycode.forEach(ele => {
                tempamount = tempamount + ele.payRate
            })
        }
        this.model.amount = tempamount * Number(this.model.payFactor)

    }
    onSelectAll(items: any) {
        console.log('item', items);
        this.model.amount = 0;
        let tempamount = 0;
        if (this.showSelectedPaycode.length) {
            this.showSelectedPaycode.forEach(ele => {
                tempamount = tempamount + ele.payRate
            })
        }
        this.model.amount = tempamount * Number(this.model.payFactor)

    }
    onDeSelectAll(items: any) {
        console.log('item', items);
        this.model.amount = 0;
        let tempamount = 0;
        if (this.showSelectedPaycode.length) {
            this.showSelectedPaycode.forEach(ele => {
                tempamount = tempamount + ele.payRate
            })
        }
        this.model.amount = tempamount * Number(this.model.payFactor)

    }
    /** Day Selection */
    daySelection(event: any) {
        this.model.customDate = false;
        this.dateSectionDateDisplay = false;
        this.dateSectionDayDisplay = true;
        this.numOfDays = false;
        this.numDays = false;
        this.model.noOfDays = null;
        this.model.endDateDays = null;
        this.modelStartDate = null;
        this.modelEndDate = null;
        this.isJoiningDateRadioSelected = true;
        this.isRadioSelected = false;
        console.log('Day Selection', event)
        console.log('this.model.customDate', this.model.customDate)
    }
    dateSelection(event: any) {
        this.model.customDate = true;
        this.dateSectionDateDisplay = true;
        this.dateSectionDayDisplay = false;
        this.numOfDays = true;
        this.numDays = true;
        this.model.noOfDays = null;
        this.model.endDateDays = null;
        this.isRadioSelected = true;
        this.isJoiningDateRadioSelected = false;
        console.log('Date Selection', event)
        console.log('this.model.customDate', this.model.customDate)
    }
    /* Total amount Calculation */
    amountCal() {
        this.tempTotal = 0
        this.showSelectedPaycode.forEach(ele => {
            this.tempTotal = this.tempTotal + ele.payRate;
        })
    }

    /* Total amount Bind to Model */
    totalAmount() {
        this.tempTotal ? this.model.amount = parseFloat(this.tempTotal * Number(this.model.payFactor) + '').toFixed(3) : null
    }

    checkdecimalpayRate(digit) {

        digit != null ? digit = digit.toString() : digit = '';
        let isFirstDecimal = digit.indexOf('.') > -1 ? digit.split('.')[0].length == 0 : false

        if (digit == null || digit == '' || isFirstDecimal) {
            this.decimalValue = true;
            return false;
        } else {
            let count = (digit.indexOf('.') > -1) ? 11 : 10;
            this.tempf = digit.split(".");
            if (count > 10) {
                if (this.tempf[0].length > 7) {
                    this.decimalValue = false;
                    return true;
                } else if (this.tempf[1].length > 3) {
                    this.decimalValue = false;
                    return true;
                } else {
                    this.decimalValue = true;
                    return false;
                }

            } else {
                if (this.tempf[0].length > 10) {
                    this.decimalValue = false;
                    return true;
                } else {
                    this.decimalValue = true;
                    return false;
                }
            }
        }
    }
}

