"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var benefit_code_setup_service_1 = require("../../_services/benefit-code-setup/benefit-code-setup.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var BenefitCodeSetupComponent = (function () {
    function BenefitCodeSetupComponent(router, benefitCodeSetupService, getScreenDetailService, alertService) {
        this.router = router;
        this.benefitCodeSetupService = benefitCodeSetupService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1412';
        this.message = { 'type': '', 'text': '' };
        this.benefitId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.methodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
        this.frequencyArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually", "Daily/Miscellaneous"];
        this.myOptions = {
            // other options...
            dateFormat: 'dd-mm-yyyy',
        };
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'BENEFIT_CODE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_FREQUENCY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BENEFIT_CODE_SHORT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    BenefitCodeSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    BenefitCodeSetupComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.benefitCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create Benefit Code
    BenefitCodeSetupComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            benefitId: null,
            desc: '',
            arbicDesc: '',
            startDate: null,
            endDate: null,
            frequency: 1,
            method: 1,
            amount: '',
            transction: true,
            inActive: true,
            perPeriod: '',
            perYear: '',
            lifeTime: ''
        };
    };
    // Clear form to reset to default blank
    BenefitCodeSetupComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new Benefit Code
    BenefitCodeSetupComponent.prototype.CreateBenefitCode = function (f, event) {
        var _this = this;
        this.model.startDate = this.frmStartDate;
        this.model.endDate = this.frmEndDate;
        event.preventDefault();
        var locIdx = this.model.benefitId;
        //Check if the id is available in the model.
        //If id avalable then update the Benefit Code, else Add new Benefit Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Benefit Code Id according to it create new Benefit Code
            this.benefitCodeSetupService.checkDuplicateBenefitId(locIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    //Call service api for Creating new Benefit Code
                    _this.benefitCodeSetupService.createBenefitCodeSetup(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Benefit Code
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    //edit department by row
    BenefitCodeSetupComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
        this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
        this.frmEndDate = this.model.endDate;
        this.frmStartDate = this.model.startDate;
        this.benefitId = row.benefitId;
        this.isUnderUpdate = true;
        this.benefitIdvalue = this.model.benefitId;
        //this.model.id = this.model.benefitId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    BenefitCodeSetupComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        this.model.benefitId = this.benefitIdvalue;
        //Call service api for updating selected department
        /*var locIdx = this.model.benefitId;
        this.benefitCodeService.checkDuplicateBenefitCodeId(locIdx).then(response => {
            if (response && response.code == 302 && response.result && response.result.isRepeat) {
                this.duplicateWarning = true;
                this.message.type = 'success';
    
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = response.btiMessage.message;
                }, 100);
            } else {*/
        //Call service api for Creating new Benefit Code
        this.benefitCodeSetupService.updateBenefitCode(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin';
            }, 100);
        });
        /*}*/
        /*}).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin';
            }, 100)
        });*/
    };
    BenefitCodeSetupComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    BenefitCodeSetupComponent.prototype.delete = function () {
        var _this = this;
        var selectedBenefitCodes = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedBenefitCodes.push(this.selected[i].id);
        }
        console.log(this.selected);
        this.benefitCodeSetupService.deleteBenefitCode(selectedBenefitCodes).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    BenefitCodeSetupComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    BenefitCodeSetupComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.benefitCodeSetupService.searchBenefitCodelist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    BenefitCodeSetupComponent.prototype.formatDateFordatePicker = function (strDate) {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        }
        else {
            return null;
        }
    };
    // Set default page size
    BenefitCodeSetupComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    BenefitCodeSetupComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    };
    BenefitCodeSetupComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    BenefitCodeSetupComponent.prototype.onStartDateChanged = function (event) {
        this.frmStartDate = event.jsdate;
    };
    BenefitCodeSetupComponent.prototype.onEndDateChanged = function (event) {
        this.frmEndDate = event.jsdate;
    };
    BenefitCodeSetupComponent.prototype.CheckNumber = function (event) {
        if (isNaN(event.target.value) == true) {
            this.model.benefitId = 0;
            return false;
        }
    };
    BenefitCodeSetupComponent.prototype.keyPress = function (event) {
        var pattern = /^[0-9\-.()]+$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    return BenefitCodeSetupComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], BenefitCodeSetupComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], BenefitCodeSetupComponent.prototype, "myScrollContainer", void 0);
BenefitCodeSetupComponent = __decorate([
    core_1.Component({
        selector: 'benefitCodeSetup',
        templateUrl: './benefit-code-setup.component.html',
        providers: [benefit_code_setup_service_1.BenefitCodeSetupService],
    }),
    __metadata("design:paramtypes", [router_1.Router,
        benefit_code_setup_service_1.BenefitCodeSetupService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], BenefitCodeSetupComponent);
exports.BenefitCodeSetupComponent = BenefitCodeSetupComponent;
//# sourceMappingURL=benefit-code-setup.component.js.map