import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { OnlyDecimalDirective } from '../../../_sharedresource/onlyDecimal.directive';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AccrualSetupMain } from '../../_models/accrual-setup-main/accrual-setup-main.module';
import { AccrualCodeSetupMainService } from '../../_services/accrual-setup-main/accrual-setup-main.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

@Component({
    selector: 'accrualMain',
    templateUrl: './accrual-setup-main.component.html',
    providers: [AccrualCodeSetupMainService],
})
export class AccrualMainCodeSetupComponent {
    page = new Page();
    rows = new Array<AccrualSetupMain>();
    temp = new Array<AccrualSetupMain>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1415';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    accuralId: string;
    diductionId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: AccrualSetupMain;
    showCreateForm: boolean = false;
    isHourToAccrual: boolean = true;
    isMaxAccrualHour: boolean = true;
    isDaysPerInterval: boolean = true;
    isWorkHour: boolean = true;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    typereasonOptions: any[];
    accrualbyOptions: any[];
    hasMsg = false;
    showMsg = false;
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
    start_date;
    end_date;
    islifeTimeValid: boolean = true;
    islifeTimeValidperyear: boolean = true;
    islifeTimeValidperperiod: boolean = true;
    islifeTimeValidamount: boolean = true;
    decimalpattern = '/^(((0|[1-9]\d{0,2})(\.\d{2})?)|())$/';
    tempp: string[] = [];
    messageText: string;
    numHourDisabled: boolean = false;
    workHourDisabled: boolean = false;
    accrualPeriodDisabled: boolean = false;
    numDayDisabled: boolean = false;
    maxAccrualHourDisabled: boolean = false;
    maxHourDisabled: boolean = false;
    workHourcheck: boolean = false;
    maxHourcheck: boolean = false;
    maxAccrualHourcheck: boolean = false;
    numHourcheck: boolean = false;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    diductionIdvalue: string;
    showValid1: boolean = false;
    showValid2: boolean = false;
    showValid3: boolean = false;
    error: any = { isError: false, errorMessage: '' };
    methodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
    frequencyArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually"];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('dp2') input: ElementRef;
    dataSource: Observable<any>;
    asyncSelected: string;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    positionClassOptions: any;


    constructor(private router: Router,
        private deductionCodeSetupService: AccrualCodeSetupMainService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'ACCRUAL_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SETUP_NUMHOUR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_NUMDAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_MAX_ACCRUAL_HOUR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_MAX_HOUR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_WORK_HOUR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_ACCURAL_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_ID_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_REASIONS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SETUP_TYPES', 'fieldValue': '', 'helpMessage': '' },

        ];

        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.accuralId);
        }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));

    }

    ngOnInit() {

        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        //Following shifted from SetPage for better performance
        this.deductionCodeSetupService.typereasoncodemethod().then(data => {
            this.typereasonOptions = data.result.records;
            //console.log(this.typereasonOptions);
        });
        this.deductionCodeSetupService.getaccrualbymethod().then(data => {
            this.accrualbyOptions = data.result.records;
            //console.log(this.accrualbyOptions);
        });

        this.deductionCodeSetupService.getPositionClassList().then(data => {
            this.positionClassOptions = data.result.records;
            //console.log("Data class options : " + data.result.records);
        });

    }
    getReportPositionsAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.positionClassOptions.filter((id: any) => {
                return query.test(id.accuralId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        //console.log('Selected value: ', e.value);
        //this.model.benefitsId = e.value;
    }
    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.deductionCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
    compareTwoDates(f) {
        if (new Date(f.controls['frmEndDate'].value) < new Date(f.controls['frmStartDate'].value)) {
            this.error = { isError: true, errorMessage: 'End Date cannot before start date' };
        }
    }
    // Open form for create Deduction Code
    Create() {
        this.workHourcheck = false;
        this.maxHourcheck = false;
        this.numHourcheck = false;
        this.maxAccrualHourcheck = false;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.numDayDisabled = false;
        this.workHourDisabled = false;
        this.maxAccrualHourDisabled = false;
        this.numHourDisabled = false;
        this.maxHourDisabled = false;
        this.accrualPeriodDisabled = false;
        this.showValid1 = false;
        this.showValid2 = false;
        this.showValid3 = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            accuralId: '',
            desc: '',
            arbic: '',
            numHour: null,
            maxAccrualHour: null,
            maxHour: null,
            accrualTyepId: 0,
            workHour: null,
            numDay: null,
            period: 0,
            reasons: 0,
            type: 0,


        };



    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ period: 0, accrualTyepId: '0', reasons: 0, type: 0 });
        this.numDayDisabled = false;
        this.workHourDisabled = false;
        this.maxAccrualHourDisabled = false;
        this.numHourDisabled = false;
        this.maxHourDisabled = false;
        this.accrualPeriodDisabled = false;
        this.workHourcheck = false;
        this.maxHourcheck = false;
        this.numHourcheck = false;
        this.maxAccrualHourcheck = false;
        this.showValid1 = false;
        this.showValid2 = false;
        this.showValid3 = false;
    }


    //function call for creating new Deduction Code
    CreateDeductionCode(f: NgForm, event: Event) {

        event.preventDefault();
        var locIdx = this.model.accuralId;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.deductionCodeSetupService.checkDuplicateDeductionCodeId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code
                    this.deductionCodeSetupService.createDeductionCode(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        }
    }

    //edit department by row
    edit(row: AccrualSetupMain) {
        this.workHourcheck = false;
        this.maxHourcheck = false;
        this.numHourcheck = false;
        this.maxAccrualHourcheck = false;
        this.error = { isError: false, errorMessage: '' };
        this.islifeTimeValid = true;
        this.islifeTimeValidperyear = true;
        this.islifeTimeValidperperiod = true;
        this.islifeTimeValidamount = true;
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        var id = this.model.accrualTyepId;
        var acc = this.accrualbyOptions.filter(x => x.id === id)[0];
        var accdescription = acc.desc;
        var splitted = accdescription.split("-");
        var accrualtypeVal = splitted[0].trim();
        //console.log('acctype', accrualtypeVal);
        accrualtypeVal = accrualtypeVal.trim();
        if (accrualtypeVal === "One Time") {
            this.numDayDisabled = true;
            this.workHourDisabled = true;
            this.maxAccrualHourDisabled = false;
            this.numHourDisabled = false;
            this.maxHourDisabled = false;
            this.accrualPeriodDisabled = true;
            //console.log(accrualtypeVal);
        }
        else if (accrualtypeVal === "Interval") {
            this.numDayDisabled = false;
            this.workHourDisabled = false;
            this.accrualPeriodDisabled = true;
            this.maxAccrualHourDisabled = false;
            this.maxHourDisabled = false;
            this.numHourDisabled = false;

            //console.log(accrualtypeVal);

        }
        else if (accrualtypeVal === "Period") {
            this.numDayDisabled = true;
            this.workHourDisabled = true;
            this.accrualPeriodDisabled = false;
            this.maxHourDisabled = false;
            this.numHourDisabled = false;

            //console.log(accrualtypeVal);
        }
        else if (accrualtypeVal === "Hourly") {
            this.numDayDisabled = true;
            this.workHourDisabled = false;
            this.accrualPeriodDisabled = true;
            this.maxHourDisabled = false;
            this.numHourDisabled = true;
            //console.log(accrualtypeVal);

        }
        else {
            this.numHourDisabled = false;
            this.numDayDisabled = false;
            this.workHourDisabled = false;
            this.maxAccrualHourDisabled = false;
            this.accrualPeriodDisabled = false;
            this.maxHourDisabled = false;
        }
        this.accuralId = row.accuralId;
        this.isUnderUpdate = true;
        this.diductionIdvalue = this.model.accuralId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }
    accrualbyChanged(event) {
        var id = Number(event.target.value);
        //console.log('id', id);
        if (id == 0) {
            this.numHourDisabled = false;
            this.numDayDisabled = false;
            this.workHourDisabled = false;
            this.maxAccrualHourDisabled = false;
            this.accrualPeriodDisabled = false;
            this.maxHourDisabled = false;


        }

        var acc = this.accrualbyOptions.filter(x => x.id === id)[0];
        var accdescription = acc.desc;
        var splitted = accdescription.split("-");
        var accrualtypeVal = splitted[0].trim();
        //console.log('acctype', accrualtypeVal);
        accrualtypeVal = accrualtypeVal.trim();

        if (accrualtypeVal === "One Time") {
            this.numDayDisabled = true;
            this.workHourDisabled = true;
            this.maxAccrualHourDisabled = false;
            this.numHourDisabled = false;
            this.maxHourDisabled = false;
            this.accrualPeriodDisabled = true;
            this.model.numDay = null;
            this.model.workHour = null;
            this.model.maxAccrualHour = null;
            this.model.numHour = null;
            this.model.maxHour = null;
            this.model.period = 0;
            this.workHourcheck = false;
            this.maxHourcheck = false;
            this.numHourcheck = false;
            this.maxAccrualHourcheck = false;
            //console.log(accrualtypeVal);
        } else if (accrualtypeVal === "Interval") {
            this.numDayDisabled = false;
            this.workHourDisabled = false;
            this.accrualPeriodDisabled = true;
            this.maxAccrualHourDisabled = false;
            this.maxHourDisabled = false;
            this.numHourDisabled = false;
            this.model.numDay = null;
            this.model.workHour = null;
            this.model.maxAccrualHour = null;
            this.model.numHour = null;
            this.model.maxHour = null;
            this.model.period = 0;
            this.workHourcheck = false;
            this.maxHourcheck = false;
            this.numHourcheck = false;
            this.maxAccrualHourcheck = false;
            //console.log(accrualtypeVal);

        } else if (accrualtypeVal === "Period") {
            this.numDayDisabled = true;
            this.workHourDisabled = true;
            this.accrualPeriodDisabled = false;
            this.maxHourDisabled = false;
            this.numHourDisabled = false;
            this.model.numDay = null;
            this.model.workHour = null;
            this.model.maxAccrualHour = null;
            this.model.numHour = null;
            this.model.maxHour = null;
            this.model.period = 0;
            this.workHourcheck = false;
            this.maxHourcheck = false;
            this.numHourcheck = false;
            this.maxAccrualHourcheck = false;
            //console.log(accrualtypeVal);
        }
        else if (accrualtypeVal === "Hourly") {
            this.numDayDisabled = true;
            this.workHourDisabled = false;
            this.accrualPeriodDisabled = true;
            this.maxHourDisabled = false;
            this.numHourDisabled = true;
            this.maxAccrualHourDisabled = false;
            //console.log(accrualtypeVal);
            this.model.numDay = null;
            this.model.workHour = null;
            this.model.maxAccrualHour = null;
            this.model.maxHour = null;
            this.model.numHour = null;
            this.workHourcheck = false;
            this.maxHourcheck = false;
            this.numHourcheck = false;
            this.maxAccrualHourcheck = false;
            this.model.period = 0;
        }

        else {
            this.numHourDisabled = false;
            this.numDayDisabled = false;
            this.workHourDisabled = false;
            this.maxAccrualHourDisabled = false;
            this.accrualPeriodDisabled = false;
            this.maxHourDisabled = false;
            this.workHourcheck = false;
            this.maxHourcheck = false;
            this.numHourcheck = false;
            this.maxAccrualHourcheck = false;
        }
        this.isHourToAccrual = true;
        this.isMaxAccrualHour = true;
        this.isWorkHour = true;
        /*this.deductionCodeSetupService.getDeductionCode(this.model.id).then(data => {
            var datacode = data.code;
            if(datacode == 201 && data.result.accrualTyepId == this.model.accrualTyepId) {
                this.model.numDay=data.result.numDay;
                this.model.workHour=data.result.workHour;
                this.model.maxAccrualHour=data.result.maxAccrualHour;
                this.model.numHour = data.result.numHour;
                this.model.maxHour = data.result.maxHour;
                this.model.period = data.result.period;
            }               
            /!*if (datacode == 201) {
            }*!/
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin';
            }, 100)
        });*/

    }
    updateStatus() {
        this.closeModal();
        this.model.accuralId = this.accuralId;

        //Call service api for Creating new Deduction Code
        this.deductionCodeSetupService.updateDeductionCode(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });

    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedDeductionCodes = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDeductionCodes.push(this.selected[i].id);
        }
        this.deductionCodeSetupService.deleteDeductionCode(selectedDeductionCodes).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.deductionCodeSetupService.searchDeductionCodelist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed.';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        //console.log('this.frmStartDate', event);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
        //console.log('this.frmEndDate', this.frmEndDate, event);

        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.accuralId = '';
            return false;
        }
    }


    keyPress(event: any) {
        const pattern = /^[0-9\-.()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    validateDecimal(event) {
        //console.log(event);
    }

    checkdecimal(digit) {
        //console.log(digit);
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValid = false;
            //console.log(this.islifeTimeValid);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValid = false;
            //console.log(this.islifeTimeValid);
        }

        else {
            this.islifeTimeValid = true;
            //console.log(this.islifeTimeValid);
        }

    }
    checkdecimalperyear(digit) {
        //console.log(digit);
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValidperyear = false;
            //console.log(this.islifeTimeValidperyear);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValidperyear = false;
            //console.log(this.islifeTimeValidperyear);
        }
        else {
            this.islifeTimeValidperyear = true;
            //console.log(this.islifeTimeValidperyear);
        }

    }

    checkdecimalperperiod(digit) {
        //console.log(digit);
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValidperperiod = false;
            //console.log(this.islifeTimeValidperperiod);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {

            this.islifeTimeValidperperiod = false;
            //console.log(this.islifeTimeValidperperiod);
        }
        else {
            this.islifeTimeValidperperiod = true;
            //console.log(this.islifeTimeValidperperiod);
        }

    }
    checkdecimalamount(digit) {
        //console.log(digit);
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            //console.log("in the condition");
            this.islifeTimeValidamount = false;
            //console.log(this.islifeTimeValidamount);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            //console.log("in the condition");
            this.islifeTimeValidamount = false;
            //console.log(this.islifeTimeValidamount);
        }

        else {
            this.islifeTimeValidamount = true;
            //console.log(this.islifeTimeValidamount);
        }

    }
    maxlengthcheck(tempvar, fieldcheck) {
        if (tempvar > 9999 && fieldcheck == 'workHour') {
            this.workHourcheck = true;
            //console.log("workhour", this.workHourcheck);
        }
        else if (tempvar > 9999 && fieldcheck == 'maxHour') {
            this.maxHourcheck = true;

        }
        else if (tempvar > 9999 && fieldcheck == 'maxAccrualHour') {
            this.maxAccrualHourcheck = true;
            //console.log("maxaccrual", this.maxAccrualHourcheck);
        }
        else if (tempvar > 9999 && fieldcheck == 'numHour') {
            this.numHourcheck = true;
        }
        else if (tempvar <= 9999 && fieldcheck == 'workHour') {
            this.workHourcheck = false;

        }
        else if (tempvar <= 9999 && fieldcheck == 'maxHour') {
            this.maxHourcheck = false;
        }
        else if (tempvar <= 9999 && fieldcheck == 'maxAccrualHour') {
            this.maxAccrualHourcheck = false;
        }
        else if (tempvar < 9999 && fieldcheck == 'numHour') {
            this.numHourcheck = false;
        }

    }

    Check1(event) {
        var hour = parseFloat(event);
        if (hour > this.model.numDay) {
            this.showValid1 = true;
        } else {
            this.showValid1 = false;
        }

    }

    Check2(event) {
        var hour = parseFloat(event);
        if (hour > this.model.maxAccrualHour) {
            this.showValid2 = true;
        } else {
            this.showValid2 = false;
        }
    }

    Check3(event) {
        var hour = parseFloat(event);
        if (hour > this.model.workHour) {
            this.showValid3 = true;
        } else {
            this.showValid3 = false;
        }
    }

    hoursToAccrualCheck(eve, str) {

        var temp = parseFloat(eve);

        if (this.model.numDay != null && this.model.numHour != null) {

            //console.log(this.model.numDay);
            //console.log(this.model.numHour);
            if (temp <= this.model.numHour && str == 'numDay') {
                this.isHourToAccrual = false;
                //console.log("isHourToAccrual", this.isHourToAccrual);
            }
            else if (temp > this.model.numHour && str == 'numDay') {
                this.isHourToAccrual = true;
                //console.log("isHourToAccrual", this.isHourToAccrual);
            }
            else if (temp >= this.model.numDay && str == 'numHour') {
                this.isHourToAccrual = false;
                //console.log("isHourToAccrual", this.isHourToAccrual);
            }
            else if (temp < this.model.numDay && str == 'numHour') {
                this.isHourToAccrual = true;
                //console.log("isHourToAccrual", this.isHourToAccrual);
            }
        }
        else {
            this.isHourToAccrual = true;
        }

    }
    maxAcrruaHourCheck(eve, str) {
        var temp = parseFloat(eve);
        if (this.model.numDay != null && this.model.maxAccrualHour != null) {

            //console.log(this.model.numDay);
            if (temp >= this.model.maxAccrualHour && str == 'numDay') {
                this.isMaxAccrualHour = false;
                //console.log("isMaxAccrualHour", this.isMaxAccrualHour);
            }
            else if (temp < this.model.maxAccrualHour && str == 'numDay') {
                this.isMaxAccrualHour = true;
                //console.log("isMaxAccrualHour", this.isMaxAccrualHour);
            }
            else if (temp <= this.model.numDay && str == 'maxAccrualHour') {
                this.isMaxAccrualHour = false;
                //console.log("isMaxAccrualHour", this.isMaxAccrualHour);
            }
            else if (temp > this.model.numDay && str == 'maxAccrualHour') {
                this.isMaxAccrualHour = true;
                //console.log("isMaxAccrualHour", this.isMaxAccrualHour);
            }
        }
        else {
            this.isMaxAccrualHour = true;
        }

    }

    workHourCheck(eve, str) {
        if (this.model.numDay != null && this.model.workHour != null) {
            var temp = parseFloat(eve);
            //console.log(this.model.numDay);
            //console.log(this.model.workHour);
            if (temp >= this.model.workHour && str == 'numDay') {
                this.isWorkHour = false;
                //console.log("isWorkHour", this.isWorkHour);
            }
            else if (temp < this.model.workHour && str == 'numDay') {
                this.isWorkHour = true;
                //console.log("isWorkHour", this.isWorkHour);
            }
            if (temp <= this.model.numDay && str == 'workHour') {
                this.isWorkHour = false;
                //console.log("isWorkHour", this.isWorkHour);
            }
            else if (temp > this.model.numDay && str == 'workHour') {
                this.isWorkHour = true;
                //console.log("isWorkHour", this.isWorkHour);
            }
        }
        else {
            this.isWorkHour = true;
        }

    }

}
