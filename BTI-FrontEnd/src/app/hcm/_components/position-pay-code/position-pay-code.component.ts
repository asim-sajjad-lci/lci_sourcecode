import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../_sharedresource/page';
import {Constants} from '../../../_sharedresource/Constants';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Router} from '@angular/router';
import {PositionPayCode} from '../../_models/position-pay-code/position-pay-code.module';
import {PositionPayCodeService} from '../../_services/position-pay-code/position-pay-code.service';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {NgForm} from '@angular/forms';
import {getResponseURL} from '@angular/http/src/http_utils';
import {document} from 'ngx-bootstrap/utils/facade/browser';

@Component({
    selector: 'positionPlanPayCode',
    templateUrl: './position-pay-code.component.html',
    providers : [PositionPayCodeService]
})
export class PositionPayCodeComponent {
    page = new Page();
    rows = new Array<PositionPayCode>();
    temp = new Array<PositionPayCode>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1317';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    positionPlanIds = [] ;
    positionPlanDesc;
    positionId;
    positionDescription;
    message = {'type': '', 'text': ''};
    superVisionCode = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: PositionPayCode;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    pattern = /^\d*\.?\d{0,2}$/;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
                private positionPayCodeService: PositionPayCodeService,
                private getScreenDetailService: GetScreenDetailService,
                private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            {'fieldName': 'POSITION_PAYCODE_SEARCH', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_TYPE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_PAYRATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_ACTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PLAN_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_TYPE1', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_UNIT_CODE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_CREATE_FORM_LABEL1', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'POSITION_PAYCODE_UPDATE_FORM_LABEL1', 'fieldValue': '', 'helpMessage': ''}

        ];

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.getPositionPlan();
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.positionPayCodeService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create location
    Create() {

        this.showCreateForm = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionPlanId: '',
            codeType: '',
            payCodeId: '',
            payRate: '',
            positionPlanDesc: '',
            positiondescription: '',
            positionId: '',
            positionPlan: ''
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({positionPlanId : '',
        codeType : '',
        payCodeId : ''});
        //this.Create();
    }


    //function call for creating new location
    CreatePositionPayCode(f: NgForm, event: Event) {
        event.preventDefault();
        //Call service api for Creating new location

        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {

            this.positionPayCodeService.createPositionPayCode(this.model).then(data => {
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMessage = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.showCreateForm = false;
                        this.messageText = data.btiMessage.message;
                        ;
                    }, 100);

                    this.hasMsg = true;
                    f.resetForm();
                    //Refresh the Grid data after adding new location
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        }
                }



    //edit department by row
    edit(row: PositionPayCode) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        //this.superVisionCode = row.superVisionCode;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
        this.positionPayCodeService.updatePositionPayCode(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedPayCodes = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedPayCodes.push(this.selected[i].id);
        }
        this.positionPayCodeService.deletePositionPayCode(selectedPayCodes).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.positionPayCodeService.searchPositionPayCodelist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    getPositionPlan() {
        this.positionPayCodeService.getPositionPlan().then(response => {
           this.positionPlanIds = response.result.records;

      });
    }

    changePosPlan(event){
        this.positionPayCodeService.getPositionDetail(event.target.value).then(response => {
            this.model.positiondescription = response.result.records[0].positionDescription;
            this.model.positionId = response.result.records[0].positionId;
        });
        let value = event.target.value;
        for(var i=0; i < this.positionPlanIds.length ; i++){
            if(value == this.positionPlanIds[i].id){
                this.model.positionPlanDesc = this.positionPlanIds[i].positionPlanDesc
                //this.positionPlanDesc = this.positionPlanIds[i].positionPlanDesc;
                break;
            }
        }
    }

    checkNumber(event){
        var length = this.model.payRate.length;
        var exp = new RegExp(/^\d*\.?\d{0,2}$/);
        if (exp.test(event.target.value) == true) {
            console.log('true');
        } else {
            this.model.payRate = '';
            document.getElementById("payRateId").value = '';
        }

    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}
