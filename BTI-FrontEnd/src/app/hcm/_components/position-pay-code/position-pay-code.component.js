"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var router_1 = require("@angular/router");
var position_pay_code_service_1 = require("../../_services/position-pay-code/position-pay-code.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var browser_1 = require("ngx-bootstrap/utils/facade/browser");
var PositionPayCodeComponent = (function () {
    function PositionPayCodeComponent(router, positionPayCodeService, getScreenDetailService, alertService) {
        this.router = router;
        this.positionPayCodeService = positionPayCodeService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1317';
        this.positionPlanIds = [];
        this.message = { 'type': '', 'text': '' };
        this.superVisionCode = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.pattern = /^\d*\.?\d{0,2}$/;
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'POSITION_PAYCODE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_PAYRATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PLAN_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_TYPE1', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_UNIT_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_CREATE_FORM_LABEL1', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_PAYCODE_UPDATE_FORM_LABEL1', 'fieldValue': '', 'helpMessage': '' }
        ];
    }
    PositionPayCodeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.getPositionPlan();
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    PositionPayCodeComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.positionPayCodeService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create location
    PositionPayCodeComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionPlanId: '',
            codeType: '',
            payCodeId: '',
            payRate: '',
            positionPlanDesc: '',
            positiondescription: '',
            positionId: '',
            positionPlan: ''
        };
    };
    // Clear form to reset to default blank
    PositionPayCodeComponent.prototype.Clear = function (f) {
        f.resetForm({ positionPlanId: '',
            codeType: '',
            payCodeId: '' });
        //this.Create();
    };
    //function call for creating new location
    PositionPayCodeComponent.prototype.CreatePositionPayCode = function (f, event) {
        var _this = this;
        event.preventDefault();
        //Call service api for Creating new location
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            this.positionPayCodeService.createPositionPayCode(this.model).then(function (data) {
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(function () {
                        _this.isSuccessMsg = true;
                        _this.isfailureMsg = false;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.hasMsg = false;
                        }, 4000);
                        _this.showCreateForm = false;
                        _this.messageText = data.btiMessage.message;
                        ;
                    }, 100);
                    _this.hasMsg = true;
                    f.resetForm();
                    //Refresh the Grid data after adding new location
                    _this.setPage({ offset: 0 });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    //edit department by row
    PositionPayCodeComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        //this.superVisionCode = row.superVisionCode;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    PositionPayCodeComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        //Call service api for updating selected department
        this.positionPayCodeService.updatePositionPayCode(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    };
    PositionPayCodeComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    PositionPayCodeComponent.prototype.delete = function () {
        var _this = this;
        var selectedPayCodes = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedPayCodes.push(this.selected[i].id);
        }
        this.positionPayCodeService.deletePositionPayCode(selectedPayCodes).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    PositionPayCodeComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    PositionPayCodeComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.positionPayCodeService.searchPositionPayCodelist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    PositionPayCodeComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    /*confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }*/
    PositionPayCodeComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    /*getEmployeeDetail(event) {
        if (event.target.value != "") {
            this.supervisorService.getEmployeeDetail(event.target.value).then(response => {
                var datacode = response.code;
                if (datacode == 404) {
                    this.hasMessage = true;
                    this.message.type = 'success';

                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMessage = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                    this.model.employeeName = '';
                } else {
                    this.model.employeeName = response.result.records.firstName + " " + response.result.records.lastName;
                }
            }).catch(error => {
                this.hasMessage = true;
                this.message.type = 'error';
                var errorCode = error.status;
                this.message.text = 'Server issue. Please contact admin.';
            });

        } else {
            this.model.employeeName = '';
        }
    }*/
    PositionPayCodeComponent.prototype.getPositionPlan = function () {
        var _this = this;
        this.positionPayCodeService.getPositionPlan().then(function (response) {
            _this.positionPlanIds = response.result.records;
        });
    };
    PositionPayCodeComponent.prototype.changePosPlan = function (event) {
        var _this = this;
        this.positionPayCodeService.getPositionDetail(event.target.value).then(function (response) {
            _this.model.positiondescription = response.result.records[0].positionDescription;
            _this.model.positionId = response.result.records[0].positionId;
        });
        var value = event.target.value;
        for (var i = 0; i < this.positionPlanIds.length; i++) {
            if (value == this.positionPlanIds[i].id) {
                this.model.positionPlanDesc = this.positionPlanIds[i].positionPlanDesc;
                //this.positionPlanDesc = this.positionPlanIds[i].positionPlanDesc;
                break;
            }
        }
    };
    PositionPayCodeComponent.prototype.checkNumber = function (event) {
        var length = this.model.payRate.length;
        var exp = new RegExp(/^\d*\.?\d{0,2}$/);
        if (exp.test(event.target.value) == true) {
            console.log('true');
        }
        else {
            this.model.payRate = '';
            browser_1.document.getElementById("payRateId").value = '';
        }
    };
    return PositionPayCodeComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], PositionPayCodeComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], PositionPayCodeComponent.prototype, "myScrollContainer", void 0);
PositionPayCodeComponent = __decorate([
    core_1.Component({
        selector: 'positionPlanPayCode',
        templateUrl: './position-pay-code.component.html',
        providers: [position_pay_code_service_1.PositionPayCodeService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        position_pay_code_service_1.PositionPayCodeService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], PositionPayCodeComponent);
exports.PositionPayCodeComponent = PositionPayCodeComponent;
//# sourceMappingURL=position-pay-code.component.js.map