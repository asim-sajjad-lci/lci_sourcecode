import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { PaycodeSetupModule } from '../../_models/paycode-setup/paycode-setup.module';
import { PaycodeSetupService } from '../../_services/paycode-setup/paycode-setup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { PayrollCodeModifierModule } from '../../_models/payroll-code-modifier/payroll-code-modifier.module';
import { RoutineUtilitiesService } from '../../_services/routines-utilities/routine-utilities.service';
import { DeductionCodeSetup } from '../../_models/deduction-code-setup/deduction-code-setup.module';
import { BenefitCodeSetup } from '../../_models/benefit-code-setup/benefit-code-setup.module';

@Component({
    selector: 'payroll-code-modifier',
    templateUrl: './payroll-code-modifier.component.html',
    providers: [CommonService, RoutineUtilitiesService]
})

export class PayrollCodeModifierComponent {

    moduleCode = 'M-1011';
    screenCode = 'S-1473';
    defaultFormValues: object[];
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    PAYROLL_CODE_MODIFIER_CODE_TYPE: any;
    PAYROLL_CODE_MODIFIER_OLD_CODE: any;
    PAYROLL_CODE_MODIFIER_NEW_CODE: any;
    PAYROLL_CODE_MODIFIER_UPDATE: any;
    PAYROLL_CODE_MODIFIER_CLEAR: any;
    model: PayrollCodeModifierModule = {
        id: null,
        newCode: null
    };
    ids: number;
    payCodes: PaycodeSetupModule[];
    deductionCodes: DeductionCodeSetup[];
    benefitCodes: BenefitCodeSetup[];
    messageText: string;

    codeTypes: DropDownModule[] = [
        { id: 1, name: 'Pay code' },
        { id: 2, name: 'Beneficial' },
        { id: 3, name: 'Deduction' }
    ];
    codeType: any = null;
    arrays:any[] = [];

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('fieldName1')

    typeaheadLoading: boolean;

    loaderImg: boolean = true;

    constructor(public commonService: CommonService, private routineService: RoutineUtilitiesService) {
        // this.model = new PayrollCodeModifierModule();

        this.defaultFormValues = [
            { 'fieldName': 'PAYROLL_CODE_MODIFIER_CODE_TYPE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYROLL_CODE_MODIFIER_OLD_CODE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYROLL_CODE_MODIFIER_NEW_CODE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYROLL_CODE_MODIFIER_UPDATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYROLL_CODE_MODIFIER_CLEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' }
        ];

        this.PAYROLL_CODE_MODIFIER_CODE_TYPE = this.defaultFormValues[0];
        this.PAYROLL_CODE_MODIFIER_OLD_CODE = this.defaultFormValues[1];
        this.PAYROLL_CODE_MODIFIER_NEW_CODE = this.defaultFormValues[2];
        this.PAYROLL_CODE_MODIFIER_UPDATE = this.defaultFormValues[3];
        this.PAYROLL_CODE_MODIFIER_CLEAR = this.defaultFormValues[4];
    }


    ngOnInit() {

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    }

    getCode(id: number): void {
        this.arrays = []
        if (id == 1) {
            this.routineService.getPayCodes().then(data => {
                this.arrays = data.result;
                // console.log('Pay',this.arrays)
            });
        }
        if (id == 2) {
            this.routineService.getBenefitCodes().then(data => {
                this.arrays = data.result;
                // console.log('Bene',this.arrays)
            });
        }
        if (id == 3) {
            this.routineService.getDeductionCodes().then(data => {
                this.arrays = data.result;
                // console.log('Dedu',this.arrays)
            });
        }
    }


    updateCode(f: NgForm, event: Event) {
        event.preventDefault();
        if (this.ids == 1) {
            this.routineService.getByPayCodeId(this.model).then(payCode => {
                payCode.result.payCodeId = this.model.newCode;
                this.routineService.updatePayCode(payCode.result).then(data => {
                    this.codeType = '';
                    this.model.id = '';
                    this.model.newCode = '';
                    f.resetForm({
                        codeType: 0,
                        newCode: '',
                        ids: 0
                    });
                });
            });
        }

        if (this.ids == 2) {
            this.routineService.getBenefitCodesById(this.model).then(benefitCode => {
                benefitCode.result.benefitId = this.model.newCode;
                this.routineService.updateBeneCode(benefitCode.result).then(data => {
                    this.codeType = '';
                    this.model.id = '';
                    this.model.newCode = '';
                    f.resetForm({
                        codeType: 0,
                        newCode: '',
                        ids: 0
                    });
                });
            });
        }

        if (this.ids == 3) {
            this.routineService.getDeductionCodeById(this.model).then(deductionCode => {
                deductionCode.result.diductionId = this.model.newCode;
                this.routineService.updateDeduCode(deductionCode.result).then(data => {
                    this.codeType = '';
                    this.model.id = '';
                    this.model.newCode = '';
                    f.resetForm({
                        codeType: 0,
                        newCode: '',
                        ids: 0
                    });
                });
            });
        }

    }

    Clear(f: NgForm) {
        f.resetForm({
            codeType: 0,
            newCode: '',
            ids: 0
        });
    }
}

