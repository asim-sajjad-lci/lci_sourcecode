import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { OrientationSetup } from '../../_models/orientation-setup/orientation-setup.module';
import { OrientationSetupService } from '../../_services/orientation-setup/orientation-setup.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'orientation-setup',
    templateUrl: './orientation-setup.component.html',
    providers: [OrientationSetupService]
})
export class OrientationSetupComponent {
    page = new Page();
    rows = new Array<OrientationSetup>();
    temp = new Array<OrientationSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1439';
    moduleName;
    screenName;
	skillsetupindexdesc;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    skillId={};
	skillSetIndexId : any;
    skillSetId = {};
	orientationId = {};
    skillSetIdOptions=[];
    searchKeyword = '';
    ddPageSize: number = 5;
    model: OrientationSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
	idx;
    showMsg = false;
	tempp:string[]=[];
	predfinedOption:any[]=[];
	sublistitem:any[]=[];
	subItemsObj= {};
	islifeTimeValid: boolean = true;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    skillIdvalue: string;
    skillSetDescId: any;
    frequencyArray = ["Hourly","Weekly","Biweekly","Semimonthly","Monthly","Quarterly","Semianually","Anually"];
	skillrequiredpara = {"true" : "Yes","false" : "No"};
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private OrientationSetupService: OrientationSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'SEARCH_ORIENTATION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ORIENTATION_SETUP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ORIENTATION_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ORIENTATION_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SKILL_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SKILL_REQUIRED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_SET_SETUP_COMMENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SEQUENCE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE_ORIENTATION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE_ORIENTATION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR_ORIENTATION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE_ORIENTATION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE_ORIENTATION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION_ORIENTATION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ORIENTATION_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SKILL_SET_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
			{ 'fieldName': 'ORIENTATION_SETUP_PREDEFINE_CHECKLIST_ITEM', 'fieldValue': '', 'helpMessage': '' },
        ];

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
			
        });
		
        //Following shifted from SetPage for better performance
		this.OrientationSetupService.getPredefinedChilist().then(data => {
			this.predfinedOption=data.result.records;
			console.log(this.predfinedOption);
		});
    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.OrientationSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
           
			
        });
       
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            orientationId:'',
            orientationDesc:'',
            orientationArbicDesc: '',
			orientationPredefinedCheckListItemId:null,
            subItems:[]
        };
		this.skillsetupindexdesc='';
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({frequency:0,skillId:'',skillSetIndexId:''});
		this.skillsetupindexdesc='';
    }


    //function call for creating new location
    CreateOrientationSetup(f: NgForm, event: Event) {
        event.preventDefault();
        var locIdx = this.model.orientationId;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.OrientationSetupService.checkDuplicateOrientationSetupId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code
                    this.OrientationSetupService.createOrientationSetup(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
							this.skillsetupindexdesc='';
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: OrientationSetup) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.orientationId = row.orientationId;
        this.isUnderUpdate = true;
        this.skillIdvalue = this.model.orientationId;
		
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
		/*this.SkillSetSetupService.getSkillSetupById(row.skillSetIndexId).then(pagedData => {
            this.skillsetupindexdesc = pagedData.result.skillDesc;
        });*/
    }
	//edit department by row
    

    updateStatus() {
        this.closeModal();
        this.model.orientationId = this.skillIdvalue;
       
        //Call service api for updating selected department
        this.OrientationSetupService.updateOrientationSetup(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
				this.skillsetupindexdesc='';
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete(){
		if(this.idx > 0){

        this.deleteSubItem();

      }else {
        var selectedOrientationSetup = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedOrientationSetup.push(this.selected[i].id);
        }
        this.OrientationSetupService.deleteOrientationSetup(selectedOrientationSetup).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }
}
    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.SkillsSetupService.searchSkillsSetuplist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    // CheckNumber(event) {
    //     if (isNaN(event.target.value) == true) {
    //         //this.model.skillId = 0;
    //         this.model.compensation = 1;
    //         return false;
    //     }
    // }
    // CheckNumberNonZero(event) {
	// 	if (event.target.value <= 0) {
    //         this.model.compensation = 1;
    //         return false;
    //     }
    // }


    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
	
	checkdecimal(digit)
	{
		console.log(digit);
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValid = false;
			console.log(this.islifeTimeValid);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValid = false;
			console.log(this.islifeTimeValid);
		}
		
		else{
			this.islifeTimeValid = true;
			console.log(this.islifeTimeValid);
		}
	
	}
	updateSkillCheckbox(val1,val2){
		console.log('val2 == ',val2);
		console.log('val2 == ',val2);
	}
	checklistbyChanged(id){
		 var id = id;
		 this.model.subItems=[];
		 this.OrientationSetupService.getTerminationsubitem(id).then(data=>{
			this.sublistitem = data.result;
			this.sublistitem.forEach(item =>{
			
			this.subItemsObj = {
					  "checklistitem": item.itemDesc,
					  "sequence": null,
					  "personResponsible":null,
					  "startdate":null,
					  "starttime":null,
					  "enddate":null,
					  "endtime":null,
				}
				console.log('obj',this.subItemsObj);
				 this.model.subItems.push(Object.assign({}, this.subItemsObj));
				//console.log('item',item);
			});
			console.log('subitem',this.sublistitem);
		 });
		 
		/* var acc = this.positionClassOptions.filter(x => x.id === id)[0];        
		 var accdescription = acc.checklistType;
		  this.subItemsObj = {
          "checklistitem": accdescription,
          "sequence": null,
		  "personResponsible":null,
		  "completeDate":null
        }
        this.model.subItems.push(Object.assign({}, this.subItemsObj));
		 console.log(accdescription);*/
		 
	}
	varifySubDelete(obj) {
        this.idx = obj.id;
        if(this.idx === undefined){
            this.model.subItems.pop();
        }
        else if (this.idx > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
          } else {
              this.isSuccessMsg = false;
              this.hasMessage = true;
              this.message.type = 'error';
              this.isfailureMsg = true;
              this.showMsg = true;
              this.message.text = 'Please select at least one record to delete.';
              window.scrollTo(0, 0);
          }
    }
	deleteSubItem() {
        // this.varifySubDelete(id);
            let indx= this.idx;
            var predefinechecklists = [];
          if (indx==null) {
            this.model.subItems.pop();
            } else{
                predefinechecklists.push(indx);
            }
        this.OrientationSetupService.deleteOrientationSetupsubitem(predefinechecklists).then(data => {
            this.model.subItems = data.result.delete[0].list;
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            
            this.hasMessage = true;
            this.message.type = 'success';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                  }, 4000);
                this.message.text = data.btiMessage.message;
               }, 100);
  
            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            // this.model.subItems = this.rows.

  
          }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
          });
          this.closeModal();

      }
}
