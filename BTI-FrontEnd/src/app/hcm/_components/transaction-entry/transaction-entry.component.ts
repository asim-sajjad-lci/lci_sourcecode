import { Component, TemplateRef, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { TransactionEntryModule, SaveDataEntryModule } from '../../_models/transaction-entry/transaction-entry';
import { TransactionEntryService } from '../../_services/transaction-entry/transaction-entry.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
@Component({
    selector: 'transaction',
    templateUrl: './transaction-entry.component.html',
    providers: [TransactionEntryService, CommonService]
})

// export Department component to make it available for other classes
export class TransactionEntryComponent {
    page = new Page();
    page1 = new Page();
    rows = new Array<TransactionEntryModule>();
    temp = new Array<TransactionEntryModule>();
    selected = [];
    selectedBatches = [];
    moduleCode = "M-1011";
    screenCode = "S-1463";
    moduleName;
    saving:boolean = false;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    departmentId = {};
    searchKeyword = '';
    getDepartment: any[] = [];
    ddPageSize: number = 5;
    ddPageSize1: number = 5;
    model: TransactionEntryModule = {
        id: 0,
        batches: { id: 0 },
        dimensions: { id: 0 },
        entryNumber: '',
        entryDate: '',
        description: '',
        arabicDescription: '',


        transactionEntryDetail: {

            employeeMaster: {
                employeeIndexId: 0
            },
            transactionType: 0,
            codeId: null,
            amount: null,
            payRate: null
        },
        fromDate: '',
        toDate: ''

    };

    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder: boolean = false;
    departmentIdvalue: number;
    batchIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    modelStartDate;
    modelEndDate;
    startDate;
    endDate;
    employeeId;
    emdescription;
    CodeId;
    codeDesc;
    batchDropdownList = [];
    projectId;
    projectIdList: Observable<any>;
    projectIdDropdownList = [];
    batchId;
    bId;
    ViewBy;
    transDataList: Observable<any>;
    transDeductionDataList: Observable<any>;
    transBenefitDataList: Observable<any>;
    employeeIdList: Observable<any>;
    transTypeIdList = [];
    employeeIdOption = [];
    isDeleteBatch: boolean = false;
    transactionType = ["", "Pay Code", "Deduction", "Benefit"];
    error: any = { isError: false, errorMessage: '' };
    transdetails: any[];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    transDetails: any = [];
    defaultIdvalue: any;
    rowDataId: any = '';
    tempp;
    arbicDescription;
    AmountPattern;
    PayratePattern;
    islifeTimeValidamount: boolean = true;
    islifeTimeValidpayRate: boolean = true;
    sortColumnNameArray = ["", "employee", "department", "codeId", "projectId"]
    sortColumnName = 'employee';

    TRANSACTION_ENTRY_SEARCH: any;
    TRANSACTION_ENTRY_BATCH_ID: any;
    TRANSACTION_ENTRY_DESCRIPTION: any;
    TRANSACTION_ENTRY_ARABIC_DESCRIPTION: any;
    TRANSACTION_ENTRY_PAY_PERIOD_FROM: any;
    TRANSACTION_ENTRY_PAY_PERIOD_TO: any;
    TRANSACTION_ENTRY_EMPLOYEE_ID: any;
    TRANSACTION_ENTRY_TRANSACTION_TYPE: any;
    TRANSACTION_ENTRY_CODE_ID: any;
    TRANSACTION_ENTRY_AMOUNT: any;
    TRANSACTION_ENTRY_PAY_RATE: any;
    TRANSACTION_ENTRY_VIEW_BY: any;
    DEPARTMENT_UPDATE_FORM_LABEL: any;
    TRANSACTION_ENTRY_DELETE_ROW_LABEL: any;
    TRANSACTION_ENTRY_DELETE_LABEL: any;
    TRANSACTION_ENTRY_SAVE_LABEL: any;
    TRANSACTION_ENTRY_CREATE_LABEL: any;
    TRANSACTION_ENTRY_PRINT_LABEL: any;
    TRANSACTION_ENTRY_CLEAR_LABEL: any;
    TRANSACTION_ENTRY_UPDATE_LABEL: any;
    TRANSACTION_ENTRY_CREATE_FORM_LABEL: any;
    TRANSACTION_ENTRY_UPDATE_FORM_LABEL: any;
    TRANSACTION_ENTRY_TOTAL_TRANSACTION_GRID: any;
    TRANSACTION_ENTRY_PROJECT_ID: any;
    modelPayRateofCodeId: any;
    serialNoDisplayArr: any[] = [];


    columns = [

        { prop: 'No.#' },
        { prop: 'EmployeeId', name: 'EMPLOYEE ID' },
        { prop: 'EmployeeName', name: 'EMPLOYEE NAME', sortable: false },
        { prop: 'ProjectId', name: 'PROJECT ID', sortable: false },
        { prop: 'CodeId', name: 'CODE ID' },
        { prop: 'CodeFactor', name: 'CODE FACTOR' },
        { prop: 'PayFactor', name: 'PAY FACTOR' },
        { prop: 'PayRate', name: 'PAY RATE' },

    ];
    page2 = new Page();

    data = new Array<SaveDataEntryModule>();

    payCodeId;

    delId;
    delIdFromDatabase;
    isDelFromDatabase;
    batchIdForPagination;
    TransactionTypeValid = false;
    CodeIdValid = false;
    BatchTypeValid = false;



    constructor(
        private router: Router,
        private transactionEntryService: TransactionEntryService,
        private getScreenDetailService: GetScreenDetailService,
        private changeDetectorRef: ChangeDetectorRef,
        private alertService: AlertService, private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        this.page2.pageNumber = 0;
        this.page2.size = 5;
        this.page2.sortOn = 'transcationPrimaryId';
        this.page2.sortBy = 'ASC';
        this.page2.totalElements = 1;


        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'TRANSACTION_ENTRY_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_BATCH_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_PAY_PERIOD_FROM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_PAY_PERIOD_TO', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_TRANSACTION_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_CODE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_PAY_RATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_VIEW_BY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEPARTMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_DELETE_ROW_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_PRINT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_TOTAL_TRANSACTION_GRID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANSACTION_ENTRY_PROJECT_ID', 'fieldValue': '', 'helpMessage': '' },
        ];

        this.TRANSACTION_ENTRY_SEARCH = this.defaultFormValues[0];
        this.TRANSACTION_ENTRY_BATCH_ID = this.defaultFormValues[1];
        this.TRANSACTION_ENTRY_DESCRIPTION = this.defaultFormValues[2];
        this.TRANSACTION_ENTRY_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.TRANSACTION_ENTRY_PAY_PERIOD_FROM = this.defaultFormValues[4];
        this.TRANSACTION_ENTRY_PAY_PERIOD_TO = this.defaultFormValues[5];
        this.TRANSACTION_ENTRY_EMPLOYEE_ID = this.defaultFormValues[6];
        this.TRANSACTION_ENTRY_TRANSACTION_TYPE = this.defaultFormValues[7];
        this.TRANSACTION_ENTRY_CODE_ID = this.defaultFormValues[8];
        this.TRANSACTION_ENTRY_AMOUNT = this.defaultFormValues[9];
        this.TRANSACTION_ENTRY_PAY_RATE = this.defaultFormValues[10];
        this.TRANSACTION_ENTRY_VIEW_BY = this.defaultFormValues[11];
        this.DEPARTMENT_UPDATE_FORM_LABEL = this.defaultFormValues[12];
        this.TRANSACTION_ENTRY_DELETE_ROW_LABEL = this.defaultFormValues[13];
        this.TRANSACTION_ENTRY_DELETE_LABEL = this.defaultFormValues[14];
        this.TRANSACTION_ENTRY_SAVE_LABEL = this.defaultFormValues[15];
        this.TRANSACTION_ENTRY_CREATE_LABEL = this.defaultFormValues[16];
        this.TRANSACTION_ENTRY_PRINT_LABEL = this.defaultFormValues[17];
        this.TRANSACTION_ENTRY_CLEAR_LABEL = this.defaultFormValues[18];
        this.TRANSACTION_ENTRY_UPDATE_LABEL = this.defaultFormValues[19];
        this.TRANSACTION_ENTRY_CREATE_FORM_LABEL = this.defaultFormValues[20];
        this.TRANSACTION_ENTRY_UPDATE_FORM_LABEL = this.defaultFormValues[21];
        this.TRANSACTION_ENTRY_TOTAL_TRANSACTION_GRID = this.defaultFormValues[22];
        this.TRANSACTION_ENTRY_PROJECT_ID = this.defaultFormValues[23];

        this.batchIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.batchId);
        }).mergeMap((token: string) => this.getBatchIdAsObservable(token));

        this.transDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getpayCodeIdAsObservable(token));


        this.transDeductionDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getdeductionCodeIdAsObservable(token));

        this.transBenefitDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
        }).mergeMap((token: string) => this.getbenefitCodeIdAsObservable(token));

        this.employeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.employeeId);
        }).mergeMap((token: string) => this.getemployeeIdAsObservable(token));

        this.projectIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.projectId);
        }).mergeMap((token: string) => this.getProjectIdAsObservable(token));


        this.data = [
            {
                id: 0, batches: { id: 0 }, dimensions: { id: 0 }, batcheId: 0, dimensionsId: 0, entryNumber: "", entryDate: "", description: "", arabicDescription: "", fromDate: "", toDate: "", transactionType: "", CodeId: 0, CodeIdString: "", CodeFactor: "", amount: "", PayRate: 0, PayFactor: "", EmployeeId: "", EmployeeName: "", employeeIndexId: 0, transactionEntryDetail: {

                    employeeMaster: {
                        employeeIndexId: 0
                    },
                    transactionType: 0,
                    codeId: null,
                    amount: null,
                    payRate: null
                }, transcationSubListId: 0
            }
        ]

    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.transactionEntryService.getDepartments().then(data => {
            this.getDepartment = data.result;
            //console.log("Data class options : "+this.getDepartment);
        });

        this.model = {
            id: 0,
            batches: { id: 0 },
            dimensions: { id: 0 },
            entryNumber: '',
            entryDate: '',
            description: '',
            arabicDescription: '',


            transactionEntryDetail: {

                employeeMaster: {
                    employeeIndexId: 0
                },
                transactionType: 0,
                codeId: null,
                amount: null,
                payRate: null
            },
            fromDate: '',
            toDate: ''

        };
    }

    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'transactionType') {
            return (
                row[gridFieldName] === 1 ? 'Pay Code' :
                    row[gridFieldName] === 2 ? 'Deduction' :
                        row[gridFieldName] === 3 ? 'Benefit' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

    getBatchIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.batchDropdownList.filter((id: any) => {
                return query.test(id.batchId);
            })
        );
    }
    getpayCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.payCodeId);
            })
        );
    }

    getdeductionCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.diductionId);
            })
        );
    }

    getbenefitCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.transTypeIdList.filter((id: any) => {
                return query.test(id.benefitId);
            })
        );
    }

    getemployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.employeeIdOption.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    getProjectIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.projectIdDropdownList.filter((id: any) => {
                return query.test(id.dimensionDescription);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {

        console.log('Selected value: ', e);

        if(!this.isUnderUpdate)
        {
                this.model.description = '';
                this.model.arabicDescription = '';
        }
        this.BatchTypeValid = false;
             if (e.item.totalTransactions > 0) {
                this.BatchTypeValid = true;

                // setTimeout(() => {
                //     this.BatchTypeValid = false;
                // }, 2000);

            }

            else {

        this.model.batches = { id: e.item.id }
        this.bId = e.item.id;
        console.log('batches', JSON.stringify(this.model));
        this.model.description = e.item.description;
        this.model.arabicDescription = e.item.arabicDescription;
            }

    }

    typeaheadPayCodeOnSelect(e: any): void {
        console.log("E for Code ID", e);
        this.CodeIdValid = false;
        this.model.transactionEntryDetail['codeId'] = e.item.id;
        this.model.transactionEntryDetail['payRate'] = e.item.payRate;
        this.modelPayRateofCodeId = e.item.amount;
        let i = this.data.length
        i = i - 1;

        if (this.data[i]["id"] == 0 && this.data[i].transactionEntryDetail["employeeMaster"].employeeIndexId == 0) {
            // this.data[i]["id"] = this.model["id"];
            this.data[i]["batcheId"] = this.model.batches['id'];
            this.data[i]["dimensionsId"] = this.model.dimensions['id'];

            this.data[i].batches['id'] = this.model.batches['id'];
            this.data[i].dimensions['id'] = this.model.dimensions['id'];

            this.data[i]["entryNumber"] = this.model["entryNumber"];
            this.data[i]["entryDate"] = this.model['entryDate'];
            this.data[i]["description"] = this.model['description'];
            this.data[i]["arabicDescription"] = this.model['arabicDescription'];
            //    this.data[i]["EmployeeId"] = this.model.transactionEntryDetail["employeeMaster"].employeeIndexId;
            this.data[i]["transactionType"] = this.model.transactionEntryDetail['transactionType'];
            this.data[i].transactionEntryDetail['transactionType'] = this.model.transactionEntryDetail['transactionType'];


            this.data[i].transactionEntryDetail['codeId'] = e.item.id;

            this.data[i]["CodeId"] = e.item.payCodeId;
            // debugger;

            if (this.modelStartDate.jsdate != undefined && this.modelEndDate.jsdate != undefined) {
                this.data[i]["fromDate"] = this.modelStartDate.jsdate;
                this.data[i]["toDate"] = this.modelEndDate.jsdate;
            }
        }

        this.payCodeId = e.item.payCodeId;
        //  this.data[i]["CodeFactor"] = e.item.amount;
        //this.data[i]["PayFactor"] = e.item.amount;
        // this.model.transactionEntryDetail['payFactor'] = e.item.payFactor;
        this.codeDesc = e.item.payCodeTypeDesc;
        this.calculatePayRate();
        this.getAllEmployees(e.item.id);
    }


    getAllEmployees(id) {

        let transactionType = this.model.transactionEntryDetail['transactionType'];

        this.employeeIdOption = [];

        let startDate = this.modelStartDate.date.year + "-" + this.modelStartDate.date.month + "-" + this.modelStartDate.date.day;
        let endDate = this.modelEndDate.date.year + "-" + this.modelEndDate.date.month + "-" + this.modelEndDate.date.day;


        if (transactionType == 1) {
            this.transactionEntryService.getEmployeeIdByPayCode(id).then(data => {
                this.employeeIdOption = data.result.records;
                console.log("Employee Record", this.employeeIdOption)

            }
            );
        }
        else if (transactionType == 2) {
            this.transactionEntryService.getEmployeeIdByDeduction(id, startDate, endDate).then(data => {
                this.employeeIdOption = data.result.records;
                console.log("Employee Record", this.employeeIdOption);
            }
            );
        }
        else if (transactionType == 3) {
            this.transactionEntryService.getEmployeeIdByBenefit(id, startDate, endDate).then(data => {
                this.employeeIdOption = data.result.records;
                console.log("Employee Record", this.employeeIdOption)
            }
            );
        }
    }

    typeaheadDeductionCodeOnSelect(e: TypeaheadMatch): void {
        console.log('deduction', e);
        this.model.transactionEntryDetail['codeId'] = e.item.id;
        this.model.transactionEntryDetail['amount'] = e.item.amount;
        this.codeDesc = e.item.discription;
    }


    typeaheadBenefitCodeOnSelect(e: TypeaheadMatch): void {

        this.model.transactionEntryDetail['codeId'] = e.item.id;
        this.model.transactionEntryDetail['amount'] = e.item.amount;
        this.codeDesc = e.item.desc;
        console.log('benefit', this.codeDesc);
    }

    typeaheadEmployeeOnSelect(e: TypeaheadMatch): void {

        this.model.transactionEntryDetail["employeeMaster"].employeeIndexId = e.item.employeeIndexId;
        this.emdescription = e.item.employeeFirstName;
        // console.log('employee', this.model);
        this.transactionEntryService.getCodeDataByTransactionType(e.item.employeeIndexId, this.model.transactionEntryDetail['transactionType']).then(data => {
            console.log('TypeHead Data', data);
            this.model.transactionEntryDetail['amount'] = null;
            this.model.transactionEntryDetail['payRate'] = null;
            this.CodeId = '';
            this.codeDesc = '';
            this.transTypeIdList = [];
            this.transTypeIdList = data.result.records;
        });
    }

    typeaheadProjectOnSelect(e: TypeaheadMatch): void {

        this.model.dimensions["id"] = e.item.id;

    }
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.transactionEntryService.getlist(this.page, this.searchKeyword).then(pagedData => {
            this.page.pageNumber = pagedData.result.pageNumber;
            this.page.size = pagedData.result.pageSize;
            this.page.sortBy = pagedData.result.sortBy;
            this.page.sortOn = pagedData.result.sortOn;
            this.page.totalElements = pagedData.result.totalCount;

            // console.log('ROws Data', pagedData)
            this.rows = pagedData.result.records;
        });
        this.transactionEntryService.getAllBatch(this.searchKeyword).then(data => {
            this.batchDropdownList = data.result;
        });

        // this.transactionEntryService.getEmployeeId(this.page).then(data => {
        //     this.employeeIdOption = data.result.records;
        //     console.log("Employee Record",this.employeeIdOption)

        // }
        // );

        this.transactionEntryService.getProjectId(this.page).then(data => {
            this.projectIdDropdownList = data.result.records;
        })
    }
    //setting pagination for Second Grid
    setPage1(pageInfo) {
        this.page1.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page1.sortOn = this.page1.sortOn;
        } else {
            this.page1.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page1.sortBy = this.page1.sortBy;
        } else {
            this.page1.sortBy = pageInfo.sortBy;
        }

        this.page1.searchKeyword = '';
        this.transactionEntryService.getTransctionEntryByBatch(this.page1, this.model.id).then(pagedData => {
            console.log("Page Data for 2nd Grid", pagedData)
            this.page1.pageNumber = pagedData.result.pageNumber;
            this.page1.size = pagedData.result.pageSize;
            this.page1.sortBy = pagedData.result.sortBy;
            this.page1.sortOn = pagedData.result.sortOn;
            this.page1.totalElements = pagedData.result.totalCount;

            this.transDetails = pagedData.result.records;
            // console.log('this.page1', this.page1);
            this.serialNoDisplayCalc(this.page1);
        });
    }

    // Amount Calculation
    calculatePayRate() {
        this.model.transactionEntryDetail['payRate'] = parseFloat(Number(this.model.transactionEntryDetail['amount']) * Number(this.modelPayRateofCodeId) + "").toFixed(3);
        const conv = this.model.transactionEntryDetail['payRate'];
        this.model.transactionEntryDetail['payRate'] = Math.round(conv);
        //console.log("aa:", this.model.payRate);
    }

    // Open form for create department
    Create() {
        this.modelPayRateofCodeId = null
        window.scrollTo(0, 600);
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.islifeTimeValidamount = true;
        this.islifeTimeValidpayRate = true;
        this.model.id = 0;
        this.model.description = '';
        this.modelStartDate = null;
        this.modelEndDate = null;
        this.projectId = null;
        this.employeeId = null;
        this.model.transactionEntryDetail['transactionType'] = null;
        this.CodeId = null;
        this.model.transactionEntryDetail['amount'] = null;
        this.model.transactionEntryDetail['payRate'] = null;
        this.ViewBy = "id";
        this.batchId = null;
        this.emdescription = null;
        this.codeDesc = null;
        this.model.arabicDescription = '';
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);

        this.ViewBy = "id";
        this.data = [
            {
                id: 0, batches: { id: 0 }, dimensions: { id: 0 }, batcheId: 0, dimensionsId: 0, entryNumber: "", entryDate: "", description: "", arabicDescription: "", fromDate: "", toDate: "", transactionType: "", CodeId: 0, CodeIdString: "", CodeFactor: "", amount: "", PayRate: 0, PayFactor: "", EmployeeId: "", EmployeeName: "", employeeIndexId: 0, transactionEntryDetail: {

                    employeeMaster: {
                        employeeIndexId: 0
                    },
                    transactionType: 0,
                    codeId: null,
                    amount: null,
                    payRate: null
                }, transcationSubListId: 0
            }
        ]

    }


    // Clear form to reset to default blank
    Clear(f: NgForm) {
        this.modelPayRateofCodeId = null
        if (!this.isUnderUpdate) {
            f.resetForm({ transactionType: 0, viewby: "id" });
            this.ViewBy = "id";
            this.defaultIdvalue = '';
            this.emdescription = null;
            this.islifeTimeValidamount = true;
            this.islifeTimeValidpayRate = true;
        } else {
            f.resetForm({
                transactionType: 0,
                viewby: "id",
                defaultIdvalue: this.defaultIdvalue,
                description: this.model.description,
                arabicDescription: this.model.arabicDescription

            })
            this.emdescription = null;
            this.codeDesc = null;
            this.islifeTimeValidamount = true;
            this.islifeTimeValidpayRate = true;
        }

    }

    //function call for creating new department
    CreateTransactionEntry(f: NgForm, event: Event) {

        console.log(this.data);
        console.log(JSON.stringify(this.data));
        event.preventDefault();


        let removeIndex = [];
        // // this.model.transactionEntryDetail['fromDate'] = this.modelStartDate;
        // // this.model.transactionEntryDetail['toDate'] = this.modelEndDate;
        // this.model.fromDate = this.modelStartDate.jsdate;
        // this.model.toDate = this.modelEndDate.jsdate;
        // //var deptIdx = this.model.id;

        // //Check if the id is available in the model.
        // //If id avalable then update the department, else Add new department.
        // // if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
        // //     this.isConfirmationModalOpen = true;
        // //     this.isDeleteAction = false;
        // // }
        // // else {
        // //Check for duplicate Division Id according to it create new division
        // /*this.transactionEntryService.checkDuplicateDeptId(deptIdx).then(response => {
        //     if (response && response.code == 302 && response.result && response.result.isRepeat) {
        //         this.duplicateWarning = true;
        //         this.message.type = "success";

        //         window.setTimeout(() => {
        //             this.isSuccessMsg = false;
        //             this.isfailureMsg = true;
        //             this.showMsg = true;
        //             window.setTimeout(() => {
        //                 this.showMsg = false;
        //                 this.duplicateWarning = false;
        //             }, 4000);
        //             this.message.text = response.btiMessage.message;
        //         }, 100);
        //         this.codeDesc = '';
        //         this.emdescription = '';
        //     } else {*/
        //         //Call service api for Creating new department
        //         this.model.id = 0;
        //         this.transactionEntryService.createTransaction(this.model).then(data => {
        //             var datacode = data.code;
        //             if (datacode == 201) {
        //                 window.scrollTo(0, 0);
        //                 window.setTimeout(() => {
        //                     this.isSuccessMsg = true;
        //                     this.isfailureMsg = false;
        //                     this.showMsg = true;
        //                     this.hasMessage = false;
        //                     window.setTimeout(() => {
        //                         this.showMsg = false;
        //                         this.hasMsg = false;
        //                     }, 4000);

        //                     this.messageText = data.btiMessage.message;
        //                 }, 100);

        //                 this.hasMsg = true;
        //                 this.isUnderUpdate = false;
        //                 this.showCreateForm = false;
        //                 this.defaultIdvalue = '';
        //                 this.model.id = 0;
        //                 this.model.description = '';
        //                 this.model.arabicDescription = '';
        //                 f.resetForm({ transactionType: 0 });
        //                 this.ViewBy = "id";
        //                 //Refresh the Grid data after adding new department
        //                 this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        //             }
        //         }).catch(error => {
        //             this.hasMsg = true;
        //             window.setTimeout(() => {
        //                 this.isSuccessMsg = false;
        //                 this.isfailureMsg = true;
        //                 this.showMsg = true;
        //                 this.messageText = "Server error. Please contact admin.";
        //             }, 100)
        //         });
        //    // }

        // /*}).catch(error => {
        //     this.hasMsg = true;
        //     window.setTimeout(() => {
        //         this.isSuccessMsg = false;
        //         this.isfailureMsg = true;
        //         this.showMsg = true;
        //         this.messageText = "Server error. Please contact admin.";
        //     }, 100)
        // });*/

        // // }

        //SMT

        // this.model.id = 0;
        for (let i = 0; i < this.data.length; i++) {
            if (this.data[i].employeeIndexId == 0) {
                //this.data.splice(i,1);
                removeIndex.push(i)
            }
        }

        //For Removing null values for Array
        for (var i = removeIndex.length -1; i >= 0; i--)
                 this.data.splice(removeIndex[i],1);

        // for (let i = 0; i < this.data.length; i++) {
        //     if (this.data[i]["id"] == 0) {
        //         saveData.push(this.data[i])
        //     }
        // }

        console.log("Save Data", this.data)
        this.saving = true;
        this.transactionEntryService.createNewTransaction(this.data).then(data => {
         //   debugger;
            var datacode = data.code;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMessage = false;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);

                    this.messageText = data.btiMessage.message;
                }, 100);

                this.hasMsg = true;
                this.isUnderUpdate = false;
                this.showCreateForm = false;
                this.defaultIdvalue = '';
                this.model.id = 0;
                this.model.description = '';
                this.model.arabicDescription = '';
                this.data = [];
                // f.resetForm({ transactionType: 0 });
                this.ViewBy = "id";
                //Refresh the Grid data after adding new department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.saving = false;
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
            this.saving = false;
        });
    }

    Cancel() {
        this.modelPayRateofCodeId = null
        this.showCreateForm = false;
        this.emdescription = null;
        this.codeDesc = null;
        this.islifeTimeValidamount = true;
        this.islifeTimeValidpayRate = true;
    }

    //edit department by row
    edit(row: TransactionEntryModule) {
       // debugger;

        this.CodeId="";
        this.model.transactionEntryDetail["codeId"]="";
        this.model.transactionEntryDetail["transactionType"]=0;
        
        console.log('Edit Row', row);
        this.data = [];
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
        this.rowDataId = row.id;
        this.model.batches = { id: row['id'] }

        this.page2.pageNumber = 0;
        this.page2.size = 5;

        console.log("Page before edit",this.page2)

        this.batchIdForPagination = row.id;

        this.transactionEntryService.getTransctionEntryByBatch(this.page2, row.id).then(data => {
            //this.transDetails = [];
            // console.log("Page Data for 2nd Grid", data)
            // console.log('this.transDetails', this.transDetails)
            this.page1.pageNumber = data.result.pageNumber;
            this.page1.size = data.result.pageSize;
            this.page1.sortBy = data.result.sortBy;
            this.page1.sortOn = data.result.sortOn;
            this.page1.totalElements = data.result.totalCount;


            this.page2.pageNumber = data.result.pageNumber;
            this.page2.size = data.result.pageSize;
            this.page2.sortBy = data.result.sortBy;
            this.page2.sortOn = data.result.sortOn;
            this.page2.totalElements = data.result.totalCount;
            
            console.log("Page after edit",this.page2)
            this.transDetails = data.result.records;
            console.log("EditResult", data.result.records)
            console.log("EditResult Page Size",  data.result.pageSize)
            console.log("EditResult Page Count",  data.result.totalCount)

            this.model.id = row.id;
            this.defaultIdvalue = row['batchId'];
            this.model.description = row.description;
            this.model.arabicDescription = row.arabicDescription;
            this.projectId = this.transDetails[0].projectId;
            this.employeeId = this.transDetails[0].employeeId;
           // this.model.transactionEntryDetail["transactionType"] = this.transDetails[0].trxType;
           // this.CodeId = this.transDetails[0].codeId;
            this.model.transactionEntryDetail["amount"] = this.transDetails[0].amount;
            this.model.transactionEntryDetail["payRate"] = this.transDetails[0].payRate;
            this.showCreateForm = true;
            this.isUnderUpdate = true;
            this.payCodeId = this.transDetails[0].codeId;
            this.model.transactionEntryDetail['codeId'] = this.transDetails[0].codePrimaryId
            // console.log('this.page1', this.page1)
            this.serialNoDisplayCalc(this.page2);
            this.getEditData();

         //   this.page2.size = data.result.pageSize;
          //  this.page2.totalElements = data.result.totalCount;



        });

        this.modelEndDate = this.formatDateFordatePicker(row['payPeriodTo']);
        this.modelStartDate = this.formatDateFordatePicker(row['payPeriodFrom']);

    }

    getEditData() {
        this.data = [];
        var dateStart;
        var dateEnd;
     //   debugger;
        let startDate = this.modelStartDate.date.year + "-" + this.modelStartDate.date.month + "-" + this.modelStartDate.date.day;
        let endDate = this.modelEndDate.date.year + "-" + this.modelEndDate.date.month + "-" + this.modelEndDate.date.day;

        dateStart = new Date(startDate);
        dateEnd = new Date(endDate);


        for (let i = 0; i < this.transDetails.length; i++) {
            let row = new SaveDataEntryModule(this.transDetails[i].transcationPrimaryId, { id: this.model.batches['id'] }, { id: this.model.dimensions['id'] }, 0, 0, this.model["entryNumber"], this.model['entryDate'], this.model['description'], this.model['arabicDescription'], dateStart, dateEnd, "", this.transDetails[i].codeId, "", this.transDetails[i].payFactor, "", this.transDetails[i].payRate, this.transDetails[i].amount, this.transDetails[i].employeeId, this.transDetails[i].employeeName, this.transDetails[i].employeePrimaryId, {
                employeeMaster: {
                    employeeIndexId: this.transDetails[i].employeePrimaryId
                },
                transactionType: this.transDetails[i].trxType,
                codeId: this.transDetails[i].codePrimaryId,
                amount: this.transDetails[i].amount,
                payRate: this.transDetails[i].payRate
            }, this.transDetails[i].transcationSubListId);
            this.data.push(row);
            console.log(this.data)
            this.page1.totalElements = this.data.length;
        }

        setTimeout(() => {
            for (let i = 0; i < this.data.length; i++) {
                const element = document.getElementsByClassName('custom-' + i);
                for (let j = 0; j < element.length; j++) {

                    (<HTMLInputElement>element[j]).setAttribute("disabled", "");
                }

            }
        }, 250);

    }

    // transType(event) {

    //     this.modelPayRateofCodeId = null
    //     let employeeId: any;
    //     let transactionType: number;
    //     employeeId = this.model.transactionEntryDetail["employeeMaster"].employeeIndexId;
    //     transactionType = event;

    //     this.transactionEntryService.getCodeDataByTransactionType(employeeId, transactionType).then(data => {
    //         this.model.transactionEntryDetail['amount'] = null;
    //         this.model.transactionEntryDetail['payRate'] = null;
    //         this.model.transactionEntryDetail['payFactor'] = null;
    //         this.model.transactionEntryDetail['payRate'] = null;
    //         this.CodeId = '';
    //         this.codeDesc = '';
    //         this.transTypeIdList = [];
    //         this.transTypeIdList = data.result.records;

    //     });
    // }

    transType(event) {

        this.TransactionTypeValid = false;

        this.modelPayRateofCodeId = null
        let employeeId: any;
        let transactionType: number;
        employeeId = this.model.transactionEntryDetail["employeeMaster"].employeeIndexId;
        transactionType = event;

        let startDate = this.modelStartDate.date.year + "-" + this.modelStartDate.date.month + "-" + this.modelStartDate.date.day;
        let endDate = this.modelEndDate.date.year + "-" + this.modelEndDate.date.month + "-" + this.modelEndDate.date.day;

        this.transactionEntryService.getAllCodeDataOfTransactionType(transactionType, startDate, endDate).then(data => {
            this.model.transactionEntryDetail['amount'] = null;
            this.model.transactionEntryDetail['payRate'] = null;
            this.model.transactionEntryDetail['payFactor'] = null;
            this.model.transactionEntryDetail['payRate'] = null;
            this.CodeId = '';
            this.codeDesc = '';
            this.transTypeIdList = [];
            this.transTypeIdList = data.result.records;

            // this.transTypeIdList = this.transTypeIdList.filter((el, i, a) => i === a.indexOf(el));
            console.log("transtype", this.transTypeIdList);
            var obj = {};

            for (var i = 0, len = this.transTypeIdList.length; i < len; i++)
                obj[this.transTypeIdList[i]['payCodeId']] = this.transTypeIdList[i];

            this.transTypeIdList = new Array();
            for (var key in obj)
                this.transTypeIdList.push(obj[key]);

        });

    }

    // updateStatus() {
    //     this.closeModal();
    //     //Call service api for updating selected department
    //     this.model.id = this.departmentIdvalue;
    //     this.transactionEntryService.updateDepartment(this.model).then(data => {
    //         var datacode = data.code;
    //         if (datacode == 201) {
    //             //Refresh the Grid data after editing department
    //             this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    //             //Scroll to top after editing department
    //             window.scrollTo(0, 0);
    //             window.setTimeout(() => {
    //                 this.isSuccessMsg = true;
    //                 this.isfailureMsg = false;
    //                 this.showMsg = true;
    //                 window.setTimeout(() => {
    //                     this.showMsg = false;
    //                     this.hasMsg = false;
    //                 }, 4000);
    //                 this.messageText = data.btiMessage.message;
    //                 this.showCreateForm = false;
    //             }, 100);
    //             this.hasMessage = false;
    //             this.hasMsg = true;
    //             window.setTimeout(() => {
    //                 this.showMsg = false;
    //                 this.hasMsg = false;
    //             }, 4000);
    //         }
    //     }).catch(error => {
    //         this.hasMsg = true;
    //         window.setTimeout(() => {
    //             this.isSuccessMsg = false;
    //             this.isfailureMsg = true;
    //             this.showMsg = true;
    //             this.messageText = "Server error. Please contact admin.";
    //         }, 100)
    //     });
    // }

    varifyDelete() {
        // if (this.selected.length > 0) {
        //     this.showCreateForm = true;
        //     this.isDeleteAction = true;
        //     this.isConfirmationModalOpen = true;
        // } else {
        //     this.isSuccessMsg = false;
        //     this.hasMessage = true;
        //     this.message.type = 'error';
        //     this.isfailureMsg = true;
        //     this.showMsg = true;
        //     window.setTimeout(() => {
        //         this.showMsg = false;
        //         this.hasMessage = false;
        //     }, 4000);
        //     this.message.text = 'Please select at least one record to delete.';
        //     window.scrollTo(0, 0);
        // }
        this.showCreateForm = true;
        this.isDeleteAction = true;
        this.isConfirmationModalOpen = true;
    }

    varifyDeleteBatch() {
        if (this.selectedBatches.length > 0) {
            this.showCreateForm = false;
            this.isDeleteBatch = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    deleteBatch() {
        let selectedBatch = [];
        for (let i = 0; i < this.selectedBatches.length; i++) {
            selectedBatch.push(this.selectedBatches[i].id)
        }
        this.transactionEntryService.deleteTransactionEntryBatch(selectedBatch).then(data => {
            var datacode = data.code;
            this.hasMessage = true;
            if (datacode == "302") {
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    //delete department by passing whole object of perticular Department
    delete() {
     //   debugger;
        if (this.isDelFromDatabase) {
            var selectedTransaction = [];
            // for (var i = 0; i < this.selected.length; i++) {
            //     selectedTransaction.push(this.selected[i].transcationSubListId);
            // }

            selectedTransaction.push(this.delId);
            if (this.delId != 0) {
                this.transactionEntryService.deleteTransactionEntryByrow(selectedTransaction).then(data => {
                    var datacode = data.code;
                    // if (datacode == 200) {
                    //     this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                    // }
                    this.transactionEntryService.getTransctionEntryByBatch(this.page, this.rowDataId).then(data => {
                        // this.transDetails = [];
                        this.transDetails = data.result.records;
                    });
                    this.hasMessage = true;
                    this.message.type = "success";
                    //this.message.text = data.btiMessage.message;

                    //  window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMessage = false;
                        }, 4000);
                        this.message.text = "Successfully Deleted";
                    }, 100);

                    //Refresh the Grid data after deletion of department
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                    this.getDataByPagination({ offset:0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                    // this.transactionEntryService.getTransctionEntryByBatch(this.page, this.bId, this.ViewBy).then(data => {
                    //     this.rows = data.result.records;
                    // }
                    // )
                }).catch(error => {
                    this.hasMessage = true;
                    this.message.type = "error";
                    var errorCode = error.status;
                    this.message.text = "Server issue. Please contact admin.";
                });
                this.closeModal();
                this.data.splice(this.delIdFromDatabase, 1);
                this.changeDetectorRef.detectChanges();
                this.data=[...this.data];
                this.page2.totalElements = this.data.length
            }
        }
        else {
            this.data.splice(this.delId, 1);
            this.hasMessage = true;
            this.message.type = "success";

            this.data=[...this.data];
            if(!this.isUnderUpdate)
            this.page2.totalElements = this.data.length
            //this.message.text = data.btiMessage.message;

            //  window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = "Successfully Deleted";
            }, 100);
            this.closeModal();

        }


    }

    // default list on page
    onSelectBatches({ selected }) {
        this.selectedBatches.splice(0, this.selectedBatches.length);
        this.selectedBatches.push(...selected);
    }
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    updateTransactionFilter(event) {
        this.page2.searchKeyword = event.target.value.toLowerCase();
        this.page2.pageNumber = 0;
        this.getDataByPagination({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
       // this.table.offset = 0;
        this.page2.searchKeyword = '';
        // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    // Set default page size
    changePageSize1(event) {
        this.page1.size = event.target.value;
        this.setPage1({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    changePageSize2(event) {
        this.page2.size = event.target.value;
        if (this.isUnderUpdate) {
            this.getDataByPagination({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
        }
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    onStartDateChanged(event: IMyDateModel): void {
        this.modelStartDate = event.jsdate;
        this.startDate = event.epoc;
        //console.log('this.frmStartDate', event);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: 'Invalid End Date.' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.modelEndDate = event.jsdate;
        this.endDate = event.epoc;
        //console.log('this.frmEndDate', this.frmEndDate, event);
        //console.log(this.endDate);
        //console.log(this.frmStartDate);
        if ((this.startDate > this.endDate) && this.endDate != undefined) {
            this.error = { isError: true, errorMessage: ' Invalid End Date' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        if (this.endDate == undefined && this.startDate == undefined) {
            this.error = { isError: false, errorMessage: '' };
        }
        //this.frmEndDate = event.jsdate;
        //this.frmEndDate = event.jsdate;
        //console.log('>>>>>>>>>>', f.controls.['frmEndDate']);
        //console.log('<<<<<<<<<<<<', f.controls['frmStartDate'].value);
        //if(f.controls['frmEndDate'].value.formatted < f.controls['frmStartDate'].value.formatted){
        //this.error={isError:true,errorMessage:' Invalid End Date'};
        // }
    }

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }

    sortBatchResult(sortOn) {
        this.page.sortOn = this.sortColumnNameArray[sortOn];
        if (this.page.sortBy == "DESC") {
            this.page.sortBy = "ASC";
        }
        else {
            this.page.sortBy = "ASC";
        }
        console.log('batch', this.page.sortBy);
        this.transactionEntryService.getTransctionEntryByBatch(this.page, this.rowDataId).then(data => {
            this.transDetails = [];
            this.transDetails = data.result.records;
            console.log('this.transDetails', this.transDetails);
        });

    }

    // checkdecimalamount(digit, varToBeFalse) {
    //     var temp = true;
    //     console.log('digit', digit);
    //     if (digit == '' || digit == null) {
    //         if (varToBeFalse == 'AmountPattern') {
    //             this.AmountPattern = true;
    //         }
    //         if (varToBeFalse == 'PayratePattern') {
    //             this.PayratePattern = true;
    //         }

    //         return;
    //     }

    //     if (digit != null) {
    //         this.tempp = digit.toString().split(".");
    //         if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
    //             temp = false;
    //         } else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
    //             temp = false;
    //         } else {
    //             temp = true;
    //         }
    //     }



    //     if (varToBeFalse == 'AmountPattern') {
    //         this.AmountPattern = temp;
    //     }
    //     if (varToBeFalse == 'PayratePattern') {
    //         this.PayratePattern = temp;
    //     }


    // }this.islifeTimeValidpayRate = true;

    checkdecimalamount(digit) {
        if (digit == '' || digit == null) {
            this.islifeTimeValidamount = true;
            return;
        }
        this.tempp = digit.toString().split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidamount = false;
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidamount = false;
        }
        else {
            this.islifeTimeValidamount = true;
        }
    }
    print() {

        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        printContents = printContents.replace(/\r?\n|\r/g, "");
        printContents = printContents.replace(" ", "");
        printContents = printContents.replace(/<!--.*?-->/g, "");

        // var postedOnes = this.getElementsByIdStartsWith("print-section", "canvas", "canvasdiv-", printContents);
        // var postedOnes1 = this.getElementsByIdStartsWith("print-section", "canvas", "categorychart-", postedOnes);
        // this.getElementsByIdStartsWith("print-section", "canvas", "innerchart-", postedOnes1);

        printContents = document.getElementById('print-section').innerHTML;

        popupWin = window.open('', '_blank', 'top=0,left=0,height=400px,width=800px');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
        <link rel="stylesheet" type="text/css" media="screen,print" href="assets/css/bootstrap.min.css">
          <title>Transaction Entry</title>
          <style>
            .print-tabel{disaplay:block}
          </style>
        </head>
    <body onload="">${printContents}</body>
    <script type="text/javascript">
    window.print();
    if(navigator.userAgent.match(/iPad/i)){
      window.onfocus=function(){window.close(); }
    }else{        
      window.close();
    }
    </script>
      </html>`
        );
        popupWin.document.close();
    }

    printEmployeeDetails() {
        this.doNotPrintHearder = true;
        this.noPrintTable = true;
        this.printDetails = false;

        setTimeout(() => { this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint() {
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }


    checkdecimalpayRate(digit) {
        if (digit == '' || digit == null) {
            this.islifeTimeValidpayRate = true;
            return;
        }
        this.tempp = digit.toString().split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidpayRate = false;
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            this.islifeTimeValidpayRate = false;
        }
        else {
            this.islifeTimeValidpayRate = true;
        }
    }

    // Serial Number DisplayFunction
    serialNoDisplayCalc(page2) {
        // console.log('page2', page2)
        let tempLen: number = 0;
        if (page2.totalElements > page2.size) {
            tempLen = page2.size;
        } else {
            tempLen = page2.totalElements;
        }
        let serialNo: number = 0;
        this.serialNoDisplayArr = [];
        if (page2.pageNumber === 0) {
            while (tempLen > 0) {
                serialNo += 1;
                this.serialNoDisplayArr.push(serialNo);
                tempLen--;
            }
        } else {
            serialNo = 1 + page2.pageNumber * page2.size;
            tempLen = page2.totalElements - page2.pageNumber * page2.size
            while (tempLen > 0) {
                this.serialNoDisplayArr.push(serialNo);
                serialNo += 1;
                tempLen--;
            }
        }
        console.log("Serial", this.serialNoDisplayArr);
        return this.serialNoDisplayArr;

    }

    getName(event, row) {

        let index = row.$$index;

        // this.data[index]["employeeIndexId"] = employeeId;

        for (let i = 0; i < this.employeeIdOption.length; i++) {
            if (this.employeeIdOption[i].employeeIdInString == event) {
                this.data[index]["EmployeeName"] = this.employeeIdOption[i].employeeFirstName;
                this.data[index]["employeeIndexId"] = this.employeeIdOption[i].employeeIndexId;
                this.data[index]["CodeFactor"] = this.employeeIdOption[i].amount;
                this.data[index]["EmployeeId"] = this.employeeIdOption[i].employeeIdInString;

                this.data[index].transactionEntryDetail["employeeMaster"].employeeIndexId = this.employeeIdOption[i].employeeIndexId;
            }
        }

    }

    addRow(index) {
       // debugger;
        let j;

        if (index.$$index == undefined) {
            j = index;
        }

        else {
            j = index.$$index;
    }

        var startDate = "";
        var endDate = "";
        var dateStart;
        var dateEnd;

       
        if (this.modelStartDate.jsdate == undefined && this.modelEndDate.jsdate == undefined) {
            startDate = this.modelStartDate.date.year + "-" + this.modelStartDate.date.month + "-" + this.modelStartDate.date.day;
            endDate = this.modelEndDate.date.year + "-" + this.modelEndDate.date.month + "-" + this.modelEndDate.date.day;

            dateStart = new Date(startDate);
            dateEnd = new Date(endDate);

        }
        else {
            startDate = this.modelStartDate.jsdate;
            endDate = this.modelEndDate.jsdate;
        }


        if (this.isUnderUpdate == true) {

            this.TransactionTypeValid = false;
            this.CodeIdValid = false;

            if (this.model.transactionEntryDetail["transactionType"] == 0) {
                this.TransactionTypeValid = true;
            }
            else if (this.CodeId == 0) {
                this.CodeIdValid = true;
            }
            else{
                let row = new SaveDataEntryModule(0, { id: this.model.batches['id'] }, { id: this.model.dimensions['id'] }, 0, 0, this.model["entryNumber"], this.model['entryDate'], this.model['description'], this.model['arabicDescription'], dateStart, dateEnd, "", this.payCodeId, "", "", "", 0, "", "", "", 0, {
                    employeeMaster: {
                        employeeIndexId: 0
                    },
                    transactionType: this.model.transactionEntryDetail['transactionType'],
                    codeId: this.model.transactionEntryDetail['codeId'],
                    amount: null,
                    payRate: null
                }, 0);
    
              //  alert(this.data.length);
                
                //alert(this.data.length);
                if (this.page2.size == this.data.length) {
                    this.page2.totalElements = this.page2.totalElements + 1
                    this.page2.size = this.data.length + 1;
                }
    
                this.data.push(row);
                this.data=[...this.data];
                //  this.page2.totalElements = this.page2.totalElements + 1
            }
 
            this.serialNoDisplayCalc(this.page2);
            let k = j + 1;
            this.getAllEmployees(this.model.transactionEntryDetail['codeId']);
            setTimeout(() => {
                const element = document.getElementsByClassName('custom-' + k);
                for (let i = 0; i < element.length; i++) {

                    (<HTMLInputElement>element[i]).removeAttribute("disabled");
                    // (<HTMLInputElement>element[i]).focus();// note the type assertion on the element
                }
                setTimeout(() => {
                const empElement = document.getElementsByClassName('empCustom-' + k);
                for (let i = 0; i < empElement.length; i++) {

                    //  (<HTMLInputElement>element[i]).removeAttribute("disabled");
                    (<HTMLInputElement>empElement[i]).focus();// note the type assertion on the element
                }
            }, 250);
            }, 250);

        }

        else {
            let k = j + 1;
            let row = new SaveDataEntryModule(0, { id: this.model.batches['id'] }, { id: this.model.dimensions['id'] }, 0, 0, this.model["entryNumber"], this.model['entryDate'], this.model['description'], this.model['arabicDescription'], dateStart, dateEnd, "", this.payCodeId, "", "", "", 0, "", "", "", 0, {
                employeeMaster: {
                    employeeIndexId: 0
                },
                transactionType: this.model.transactionEntryDetail['transactionType'],
                codeId: this.model.transactionEntryDetail['codeId'],
                amount: null,
                payRate: null
            }, 0);
          //  alert(this.data.length);
            this.data.push(row);
            this.data=[...this.data];
          //  alert(this.data.length);
            this.page2.totalElements = this.data.length;

            setTimeout(() => {
    
                const empElement = document.getElementsByClassName('empCustom-' + k);
                for (let i = 0; i < empElement.length; i++) {
    
                    //  (<HTMLInputElement>element[i]).removeAttribute("disabled");
                    (<HTMLInputElement>empElement[i]).focus();// note the type assertion on the element
                }
            }, 250);
        }

        if (this.isUnderUpdate == true) {
            
        }

        //    this.model.transactionEntryDetail['transactionType'] = 0;
        //    this.CodeId = "";
        console.log(this.data);

       
    }

    cancelRow(index) {
     //   debugger;
        let delIndex = index.$$index;

        let j = index.$$index - 1;

        if (index.transcationSubListId != 0 && index.id != 0) {
            this.delId = index.transcationSubListId;
            this.isDelFromDatabase = true;
            this.delIdFromDatabase = delIndex;

        }

        else {
            this.delId = delIndex;
            this.isDelFromDatabase = false;
        }

        this.varifyDelete();

        //  this.data.splice(delIndex, 1);

        if (this.data.length == 0) {
            this.data = [];
            let row = new SaveDataEntryModule(0, { id: this.model.batches['id'] }, { id: this.model.dimensions['id'] }, 0, 0, this.model["entryNumber"], this.model['entryDate'], this.model['description'], this.model['arabicDescription'], this.modelStartDate.jsdate, this.modelEndDate.jsdate, "", this.payCodeId, "", "", "", 0, "", "", "", 0, {
                employeeMaster: {
                    employeeIndexId: 0
                },
                transactionType: this.model.transactionEntryDetail['transactionType'],
                codeId: this.model.transactionEntryDetail['codeId'],
                amount: null,
                payRate: null
            }, 0);
            this.data.push(row);
        }

        this.serialNoDisplayCalc(this.page2);
    }

    calculateGridPayRate(event, index) {
        console.log("Event", event);
        var value = event.target.value;

        if (event.keyCode == 13) {
         //   debugger
            if (this.data.length - 1 == index) {
                this.addRow(index);
            }
        }
        else {
        //  alert(value);
        let i = this.data.length
        i = i - 1;
        let number = parseFloat(Number(this.data[index]['CodeFactor']) * Number(value) + "").toFixed(3);
        const conv = number;
        //  alert(conv);
        // this.model.transactionEntryDetail['payRate'] = Math.round(+conv);
        this.data[index]['PayRate'] = Math.round(+conv);
        this.data[index]['amount'] = value;

        this.data[index].transactionEntryDetail['amount'] = +value;
        this.data[index].transactionEntryDetail['payRate'] = Math.round(+conv);

        // console.log("aa:", this.model.payRate);
            this.data[index]['PayFactor'] = value;
            const element = document.getElementsByClassName('custom-' + index);
            setTimeout(() => {
                for (let i = 0; i < element.length; i++) {

                    (<HTMLInputElement>element[i]).focus();// note the type assertion on the element
                }
            }, 100);
            // setTimeout(() => {
            //     this.data[index]['PayFactor'] = value;
            //     setTimeout(() => {
            //         const element = document.getElementsByClassName('custom-' +index);
            //     for (let i = 0; i < element.length; i++) {

            //         (<HTMLInputElement>element[i]).focus();// note the type assertion on the element
            //     }
            //     }, 100);

            // }, 1000);

        }

    }


    getCodeByTransactionType(transactionType) {

        let startDate = this.modelStartDate.date.year + "-" + this.modelStartDate.date.month + "-" + this.modelStartDate.date.day;
        let endDate = this.modelEndDate.date.year + "-" + this.modelEndDate.date.month + "-" + this.modelEndDate.date.day;

        this.transactionEntryService.getAllCodeDataOfTransactionType(transactionType, startDate, endDate).then(data => {
            this.model.transactionEntryDetail['amount'] = null;
            this.model.transactionEntryDetail['payRate'] = null;
            this.model.transactionEntryDetail['payFactor'] = null;
            this.model.transactionEntryDetail['payRate'] = null;
            this.CodeId = '';
            this.codeDesc = '';
            this.transTypeIdList = [];
            this.transTypeIdList = data.result.records;

            // this.transTypeIdList = this.transTypeIdList.filter((el, i, a) => i === a.indexOf(el));
            // console.log("transtype",this.transTypeIdList);
            var obj = {};

            for (var i = 0, len = this.transTypeIdList.length; i < len; i++)
                obj[this.transTypeIdList[i]['payCodeId']] = this.transTypeIdList[i];

            this.transTypeIdList = new Array();
            for (var key in obj)
                this.transTypeIdList.push(obj[key]);

        });
    }

    editRow(row) {
        //debugger

        for (let i = 0; i < this.data.length; i++) {
            const element = document.getElementsByClassName('custom-' + i);
            for (let j = 0; j < element.length; j++) {

                (<HTMLInputElement>element[j]).setAttribute("disabled", "");
            }

        }
       
        const el = document.getElementsByClassName('custom-' + row.$$index);
        for (let i = 0; i < el.length; i++) {

            (<HTMLInputElement>el[i]).removeAttribute("disabled");
            (<HTMLInputElement>el[i]).focus(); // note the type assertion on the element
        }

        this.CodeId=row.CodeId;
        this.payCodeId=row.CodeId;
        this.model.transactionEntryDetail["codeId"]=row.transactionEntryDetail["codeId"];
        this.model.transactionEntryDetail["transactionType"]=row.transactionEntryDetail["transactionType"];
         
        console.log(row);
        console.log(row.transactionEntryDetail["codeId"])
        this.getAllEmployees(row.transactionEntryDetail["codeId"]);

    }

    setPage2(pageInfo) {
        this.page2.pageNumber = pageInfo.offset;
        if (this.isUnderUpdate) {
            this.getDataByPagination({ offset: pageInfo.offset, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
        }
        
    }

    getDataByPagination(pageInfo) {
       
        this.page2.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page2.sortOn = this.page.sortOn;
        } else {
            this.page2.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page2.sortBy = this.page.sortBy;
        } else {
            this.page2.sortBy = pageInfo.sortBy;
        }

        // alert(this.batchIdForPagination)
  console.log("page 2",this.page2)
        this.transactionEntryService.getTransctionEntryByBatch(this.page2, this.batchIdForPagination).then(data => {
  

            this.page2.pageNumber = data.result.pageNumber;
            this.page2.size = data.result.pageSize;
            this.page2.sortBy = data.result.sortBy;
            this.page2.sortOn = data.result.sortOn;
            this.page2.totalElements = data.result.totalCount;
            this.transDetails = [];
           // console.log("Page after edit",this.page2)
            this.transDetails = data.result.records;
            this.page2.searchKeyword = '';
            // console.log("EditResult", data.result.records)
            // console.log("EditResult Page Size",  data.result.pageSize)
            // console.log("EditResult Page Count",  data.result.totalCount)

            // this.model.id = row.id;
            // this.defaultIdvalue = row['batchId'];
            // this.model.description = row.description;
            // this.model.arabicDescription = row.arabicDescription;
            // this.projectId = this.transDetails[0].projectId;
            // this.employeeId = this.transDetails[0].employeeId;
            // this.model.transactionEntryDetail["transactionType"] = this.transDetails[0].trxType;
            // this.CodeId = this.transDetails[0].codeId;
            // this.model.transactionEntryDetail["amount"] = this.transDetails[0].amount;
            // this.model.transactionEntryDetail["payRate"] = this.transDetails[0].payRate;
            // this.showCreateForm = true;
            // this.isUnderUpdate = true;
            // this.payCodeId = this.transDetails[0].codeId;
            // this.model.transactionEntryDetail['codeId'] = this.transDetails[0].codePrimaryId
            // console.log('this.page1', this.page1)
            this.serialNoDisplayCalc(this.page2);
            this.getEditData();

         //   this.page2.size = data.result.pageSize;
          //  this.page2.totalElements = data.result.totalCount;

        });

        // this.modelEndDate = this.formatDateFordatePicker(row['payPeriodTo']);
        // this.modelStartDate = this.formatDateFordatePicker(row['payPeriodFrom']);
    }


}