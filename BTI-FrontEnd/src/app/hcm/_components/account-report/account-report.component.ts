import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Router } from '@angular/router';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatePickerModule } from 'ng2-datepicker-bootstrap';
import { SupervisorService } from '../../_services/supervisor/supervisor.service';

import { Page } from '../../../_sharedresource/page';

@Component({
    selector: 'accountReport',
    templateUrl: './account-report.component.html',
    providers: [
        SupervisorService,

    ]
})
export class AccountReportComponent {

    page = new Page();
    moduleCode = 'M-1011';
    screenCode = 'S-1462';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    supervisorId = {};

    searchKeyword = '';

    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;


    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;


    employeeIdList: Observable<any>;
    getEmployee: any[] = [];
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;

    bankDetails: any[] = [];

    employeeId;
    gridLists;
    getScreenDetailArr;
    isConfigure = false;
    isShowHideGrid = false;

    NationalityDescription;
    employeeIndexId;
    EmployeeName;
    tempp:string[]=[];
    islifeTimeValidEmployeepay: boolean = true;
    islifeTimeValidEmployeepercent: boolean = true;


    constructor(private router: Router,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private supervisorService: SupervisorService,
    ) {

        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            //0
            { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LINE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BANK_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CHK_SVG', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'STATUS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MOVE_UP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MOVE_DOWN', 'fieldValue': '', 'helpMessage': '' },
            //11
            { 'fieldName': 'REMOVE_BANK', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PRINT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDNEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '' },


        ];
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.getScreenDetails();

    }
	
    //setting pagination
    setPage(pageInfo) {
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
    }

    getScreenDetails() {
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.screenGridDetail(this.moduleCode, this.screenCode).then(data => {
            this.gridLists = data.result.dtoScreenDetail.girdList;
            this.getScreenDetailArr = data.result;

            for (let i = 0; i < this.gridLists.length; i++) {
                this.gridLists[i]['fieldDetailList'].sort(function (a, b) {
                    var keyA = parseInt(a.colOrder),
                        keyB = parseInt(b.colOrder);
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });

            }


            console.log('this.gridLists');
            console.log(this.gridLists);
            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }

}
