"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var get_screen_detail_service_1 = require("../../_sharedresource/_services/get-screen-detail.service");
var common_1 = require("@angular/common");
var HCMSidebarComponent = (function () {
    function HCMSidebarComponent(getScreenDetailService, location) {
        this.getScreenDetailService = getScreenDetailService;
        this.moduleCode = Constants.hcmModuleCode;
        this.screenCode = "S-1222";
        this.location = location;
        this.defaultFormValues = [
            { 'fieldName': 'MANAGE_DEPARTMENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_DIVISION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_POSITION', 'fildValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_POSITION_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_LOCATION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_SUPERVISOR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_POSITION_PAY_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_POSITION_BUDGET', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_DATE_CONVERTER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_ATTENDANCE_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_DEDUCTION_CODE_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_ACCURAL_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_HEALTH_COVERAGE_TYPE_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_SKILLS_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_BENEFIT_CODE_SETUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_HEALTH_INSURANCE_SETUP', 'fieldValue': '', 'helpMessage': '' }
        ];
    }
    HCMSidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        // getting screen
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
            }
        });
        // setting active language
        var currentLanguage = localStorage.getItem('currentLanguage');
        if (currentLanguage == "1") {
            this.isActive = 1;
        }
        else if (currentLanguage == "2") {
            this.isActive = 2;
        }
        else {
            this.isActive = 1;
        }
    };
    HCMSidebarComponent.prototype.getClass = function (path) {
        if (this.location.path().substr(0, path.length) === path) {
            return "active";
        }
        else {
            return "";
        }
    };
    return HCMSidebarComponent;
}());
HCMSidebarComponent = __decorate([
    core_1.Component({
        selector: 'hcm-sidebar',
        templateUrl: './hcm-sidebar.component.html'
    }),
    __metadata("design:paramtypes", [get_screen_detail_service_1.GetScreenDetailService, common_1.Location])
], HCMSidebarComponent);
exports.HCMSidebarComponent = HCMSidebarComponent;
//# sourceMappingURL=hcm-sidebar.component.js.map