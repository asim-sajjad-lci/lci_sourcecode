import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeNationalityComponent } from './employee-nationality.component';

describe('EmployeeNationalityComponent', () => {
  let component: EmployeeNationalityComponent;
  let fixture: ComponentFixture<EmployeeNationalityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeNationalityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeNationalityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
