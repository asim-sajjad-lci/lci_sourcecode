import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { EmployeeNationalityModule } from '../../_models/employee-nationality/employee-nationality.module';
import { EmployeeNationalityService } from '../../_services/employee-nationality/employee-nationality.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
  selector: 'employeeNationality',
  templateUrl: './employee-nationality.component.html',
  providers: [EmployeeNationalityService, CommonService]
})
export class EmployeeNationalityComponent {
  page = new Page();
  rows = new Array<EmployeeNationalityModule>();
  temp = new Array<EmployeeNationalityModule>();
  selected = [];
  countries = [];
  states = [];
  cities = [];
  modelState = '';
  moduleCode = 'M-1011';
  screenCode = 'S-1448';
  moduleName;
  screenName;
  defaultFormValues: Array<any> = [];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  searchKeyword = '';
  ddPageSize: number = 5;
  model: EmployeeNationalityModule;
  showCreateForm: boolean = false;
  islifeTimeValidEmployerpay: boolean = true;
  islifeTimepercent: boolean = true;
  tempp: string[] = [];
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  employeeNationalityIdvalue: string;
  isShiftcodeFormdisabled: boolean = false;

  dataSource: Observable<any>;
  employeeNationalityIdSearch: any;
	typeaheadLoading: boolean;
  typeaheadNoResults: boolean;

  EMPLOYEE_NATIONALITY_NATIONALITY_ID:any
  EMPLOYEE_NATIONALITY_DESCRIPTION:any
  EMPLOYEE_NATIONALITY_ARABIC_DESCRIPTION:any
  EMPLOYEE_NATIONALITY_ACTION:any
  EMPLOYEE_NATIONALITY_DELETE_LABEL:any
  EMPLOYEE_NATIONALITY_SAVE_LABEL:any
  EMPLOYEE_NATIONALITY_CLEAR_LABEL:any
  EMPLOYEE_NATIONALITY_CREATE_LABEL:any
  EMPLOYEE_NATIONALITY_UPDATE_LABEL:any
  EMPLOYEE_NATIONALITY_CANCEL_LABEL:any
  EMPLOYEE_NATIONALITY_SEARCH:any
  EMPLOYEE_NATIONALITY_UPDATE_FORM_LABEL:any
  EMPLOYEE_NATIONALITY_CREATE_FORM_LABEL:any
  
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;

  constructor(private router: Router,
    private employeeNationalityService: EmployeeNationalityService,
    private getScreenDetailService: GetScreenDetailService,
    private alertService: AlertService,
    private commonService: CommonService) {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'employeeNationalityIndexId';
    this.page.sortBy = 'DESC';
    // default form parameter for department  screen
    this.defaultFormValues = [
      { 'fieldName': 'EMPLOYEE_NATIONALITY_NATIONALITY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_SEARCH', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  },
      { 'fieldName': 'EMPLOYEE_NATIONALITY_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''  }
    ];
    this.EMPLOYEE_NATIONALITY_NATIONALITY_ID = this.defaultFormValues[0];
    this.EMPLOYEE_NATIONALITY_DESCRIPTION = this.defaultFormValues[1];
    this.EMPLOYEE_NATIONALITY_ARABIC_DESCRIPTION = this.defaultFormValues[2];
    this.EMPLOYEE_NATIONALITY_ACTION = this.defaultFormValues[3];
    this.EMPLOYEE_NATIONALITY_DELETE_LABEL = this.defaultFormValues[4];
    this.EMPLOYEE_NATIONALITY_SAVE_LABEL = this.defaultFormValues[5];
    this.EMPLOYEE_NATIONALITY_CLEAR_LABEL = this.defaultFormValues[6];
    this.EMPLOYEE_NATIONALITY_CREATE_LABEL = this.defaultFormValues[7];
    this.EMPLOYEE_NATIONALITY_UPDATE_LABEL = this.defaultFormValues[8];
    this.EMPLOYEE_NATIONALITY_CANCEL_LABEL = this.defaultFormValues[9];
    this.EMPLOYEE_NATIONALITY_SEARCH = this.defaultFormValues[10];
    this.EMPLOYEE_NATIONALITY_UPDATE_FORM_LABEL = this.defaultFormValues[11];
    this.EMPLOYEE_NATIONALITY_CREATE_FORM_LABEL = this.defaultFormValues[12];
    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.model.employeeNationalityId);
    }).mergeMap((token: string) => this.getReportEmployeeNationalityAsObservable(token));
  }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

    //Following shifted from SetPage for better performance
    this.employeeNationalityService.getEmployeeNationalityIdDropDown().then(data => {
      this.employeeNationalityIdSearch = data.result;                
     console.log("Data class options : ", this.employeeNationalityIdSearch);
    });
  }
  // Exiting Id Search on every click
  getReportEmployeeNationalityAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
    return Observable.of(
      this.employeeNationalityIdSearch.filter((employeeNationalityIndexId: any) => {
        return query.test(employeeNationalityIndexId.employeeNationalityId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
   console.log('Selected value: ', e);
   this.model = e.item
   this.model.employeeNationalityIndexId = 0;
  }

  //setting pagination
  setPage(pageInfo) {
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
    } else {
      this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
    } else {
      this.page.sortBy = pageInfo.sortBy;
    }
    this.page.searchKeyword = '';
    this.employeeNationalityService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
      this.page = pagedData.page;
      this.rows = pagedData.data;

    });



  }

  // Open form for create location
  Create() {
    this.showCreateForm = false;
    this.isUnderUpdate = false;
    this.isShiftcodeFormdisabled = false;
    setTimeout(() => {
      this.showCreateForm = true;
      setTimeout(() => {
        window.scrollTo(0, 2000);
      }, 10);
    }, 10);
    this.model = {
     employeeNationalityIndexId: 0,
      employeeNationalityId: '',
      employeeNationalityDescription: '',
      employeeNationalityDescriptionArabic: ''

    };
  }

  // Clear form to reset to default blank
  Clear(f: NgForm) {
    f.resetForm({
      employeeNationalityIndexId: 0,
      employeeNationalityId: '',
      desc: '',
      arabicDesc: '',
      inActive: false,
      shitPremium: 1,
      amount: null,
      percent: null
    });
    this.isShiftcodeFormdisabled = false;
  }


  //function call for creating new location
  createEmployeeNationality(f: NgForm, event: Event) {
    event.preventDefault();
    var locIdx = this.model.employeeNationalityId;

    //Check if theemployeeNationalityIndexId is available in the model.
    //If id avalable then update the location, else Add new location.
    if (this.model.employeeNationalityIndexId> 0 && this.model.employeeNationalityIndexId!= 0 && this.model.employeeNationalityIndexId!= undefined) {
      this.isConfirmationModalOpen = true;
      this.isDeleteAction = false;
    }
    else {
      //Check for duplicate EmployeeNationality Id according to it create new location
      this.employeeNationalityService.checkDuplicateEmployeeNationalityId(locIdx).then(response => {
        if (response && response.code == 302 && response.result && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = 'success';

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;
          }, 100);
        } else {
          //Call service api for Creating new location
          this.employeeNationalityService.createEmployeeNationality(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.hasMessage = false;
                this.showMsg = true;
                window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
                }, 4000);
                this.showCreateForm = false;
                this.messageText = data.btiMessage.message;
                ;
              }, 100);

              this.hasMsg = true;
              f.resetForm();
              //Refresh the Grid data after adding new location
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = 'Server error. Please contact admin.';
            }, 100)
          });
        }

      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = 'Server error. Please contact admin.';
        }, 100)
      });

    }
  }

  //edit department by row
  edit(row: EmployeeNationalityModule) {
    this.showCreateForm = true;
    this.model = Object.assign({}, row);
    this.isUnderUpdate = true;
    this.employeeNationalityIdvalue = this.model.employeeNationalityId;
    setTimeout(() => {
      window.scrollTo(0, 2000);
    }, 10);
  }


  updateStatus() {
    this.closeModal();
    this.model.employeeNationalityId = this.employeeNationalityIdvalue;
    //Call service api for updating selected department

    //Call service api for Creating new location
    this.employeeNationalityService.updateEmployeeNationality(this.model).then(data => {
      var datacode = data.code;
      if (datacode == 201) {
        //Refresh the Grid data after editing department
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        //Scroll to top after editing department
        window.scrollTo(0, 0);
        window.setTimeout(() => {
          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          this.messageText = data.btiMessage.message;
          ;
          this.showCreateForm = false;
        }, 100);
        this.hasMessage = false;
        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = 'Server error. Please contact admin.';
      }, 100)
    });
    /*}*/

  }

  varifyDelete() {
    if (this.selected.length > 0) {
      this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
    } else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;
      this.message.text = 'Please select at least one record to delete.';
      window.scrollTo(0, 0);
    }
  }


  //delete department by passing whole object of perticular Department
  delete() {
    var selectedEmployeeNationalities = [];
    for (var i = 0; i < this.selected.length; i++) {
      selectedEmployeeNationalities.push(this.selected[i].employeeNationalityIndexId);
    }
    this.employeeNationalityService.deleteEmployeeNationality(selectedEmployeeNationalities).then(data => {
      var datacode = data.code;
      if (datacode == 200) {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if(datacode == "302"){
          this.message.type = "error";
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
        } else {
            this.isSuccessMsg = true;
            this.message.type = "success";
            this.isfailureMsg = false;
        }
      //this.message.text = data.btiMessage.message;

      window.scrollTo(0, 0);
      window.setTimeout(() => {
        this.showMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMessage = false;
        }, 4000);
        this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of department
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    }).catch(error => {
      this.hasMessage = true;
      this.message.type = 'error';
      var errorCode = error.status;
      this.message.text = 'Server issue. Please contact admin.    ';
    });
    this.closeModal();
  }

  // default list on page
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  // search department by keyword
  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
    // this.employeeNationalityService.searchEmployeeNationalitylist(this.page, this.searchKeyword).subscribe(pagedData => {
    //     this.page = pagedData.page;
    //     this.rows = pagedData.data;
    //     this.table.offset = 0;
    // });
  }

  // Set default page size
  changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  confirm(): void {
    this.messageText = 'Confirmed!';
    this.delete();
  }

  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }

  CheckNumber(event) {
    if (isNaN(event.target.value) == true) {
      this.model.employeeNationalityIndexId= 0;
      return false;
    }
  }

  keyPress(event: any) {
    const pattern = /^[0-9\-()]+$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  sortColumn(val) {
    if (this.page.sortOn == val) {
      if (this.page.sortBy == 'DESC') {
        this.page.sortBy = 'ASC';
      } else {
        this.page.sortBy = 'DESC';
      }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

}