import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanPackageSetupComponent } from './loan-package-setup.component';

describe('LoanPackageSetupComponent', () => {
  let component: LoanPackageSetupComponent;
  let fixture: ComponentFixture<LoanPackageSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanPackageSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanPackageSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
