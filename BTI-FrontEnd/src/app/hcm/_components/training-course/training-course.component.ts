import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { TrainingCourse } from '../../_models/training-course/training-course';
import { TrainingCourseService } from '../../_services/training-course/training-course.service';
import { TrainingBatchSignupService } from '../../_services/training-batch-signup/training-batch-signup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
import { forEach } from '@angular/router/src/utils/collection';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
@Component({
    selector: 'training-course',
    templateUrl: './training-course.component.html',
    styleUrls: ['./training-course.component.css'],
    providers: [TrainingCourseService,TrainingBatchSignupService,CommonService]
})

// export TrainingCourse component to make it available for other classes
export class TrainingCourseComponent {
    page = new Page();
    rows = new Array<TrainingCourse>();
    temp = new Array<TrainingCourse>();
    selected = [];
    selectedClass = [];
    moduleCode = "M-1011";
    screenCode = "S-1433";
    moduleName;
    screenName;
    defaultFormValues:any = [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    traningCourseId = {};
    searchKeyword = '';
	getTrainingCourse:any[]=[];
    ddPageSize: number = 5;
    model: TrainingCourse;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    isUnderUpdateClass: boolean = false;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    traningCourseIdvalue : string;
	traningCourseIdList: Observable<any>;
	typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
    startDate;
    endDate;
    startDateformatted;
    endDateformatted;
    error:any={isError:false,errorMessage:''};
    exchangeTime='';
    isUnderUpdateIndex;
    islifeTimeValidEmployeeCost: boolean = true;
    islifeTimeValidEmployerCost: boolean = true;
    islifeTimeValidSupplierCost: boolean = true;
    islifeTimeValidInstructorCost: boolean = true;
    updateStartDate;
    updateEndDate;

    tempp:string[]=[];
   
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

            SEARCH:any;
            TRAINING_ID:any;           
            DESCRIPTION:any;
            ARABIC_DESCRIPTION:any;
            DEFAULT_LOCATION:any;
            PREREQUISITE_TRANINING_ID:any;
            COURSE_IN_COMPANY_OFFICE:any;
            EMPLOYEE_COST:any;
            EMPLOYER_COST:any;
            SUPPLIER_COST:any;
            INSTRUCTOR_COST:any;
            CLASS_ID:any;
            CLASS_NAME:any;
            START_DATE:any;
            END_DATE:any;
            START_TIME:any;
            END_TIME:any;
            INSTRUCTOR_NAME:any;
            ENROLLED:any;
            MAXIMUM:any;
            CLASS_LOCATION:any;
            CREATE:any;
            CLASS_ENROLLMENT:any;
            TRANINING_SIGNUP:any;
            CLASS_SKILLS:any;
            DELETE_ROW:any;
            DELETE:any;
            SAVE:any;
            CLEAR:any;
            ACTION:any;
            UPDATE:any;
            ADD_CLASS:any;
            CLOSE:any;
            SELECT:any;
            TRAINING_COURSE:any;
            PREREQUISITE:any;
            CANCEL:any;

    subItems = [];
    subItemsObj = {
        // "isDelete": false,
       "classId": '',
       "className": '',
       "instructorName": '',
       "classLocation": '',
       "maximum": null,
       "enrolled": this.trainingBatchSignupService.getEnrolledEmployees(),
       "startDate" : "",
       "endDate" : "",
       "startTime" : "",
       "endTime" : ""
     }  
    isSubItem = true;

    constructor(
        private router: Router,
        private trainingCourseService: TrainingCourseService,
        private trainingBatchSignupService: TrainingBatchSignupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,private commonService:CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for traningCourse  screen
        this.defaultFormValues = [
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRAINING_ID', 'fieldValue': '', 'helpMessage': '' },           
            { 'fieldName': 'DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEFAULT_LOCATION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PREREQUISITE_TRANINING_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'COURSE_IN_COMPANY_OFFICE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_COST', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYER_COST', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPPLIER_COST', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INSTRUCTOR_COST', 'fieldValue': '', 'helpMessage': '' },

            { 'fieldName': 'CLASS_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'START_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'END_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'START_TIME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'END_TIME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INSTRUCTOR_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ENROLLED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MAXIMUM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_LOCATION', 'fieldValue': '', 'helpMessage': '' },

            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_ENROLLMENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRANINING_SIGNUP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLASS_SKILLS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE_ROW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },

            { 'fieldName': 'ADD_CLASS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLOSE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SELECT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TRAINING_COURSE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PREREQUISITE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '' }
        ];

            this.SEARCH=this.defaultFormValues[0];
            this.TRAINING_ID=this.defaultFormValues[1];           
            this.DESCRIPTION=this.defaultFormValues[2];
            this.ARABIC_DESCRIPTION=this.defaultFormValues[3];
            this.DEFAULT_LOCATION=this.defaultFormValues[4];
            this.PREREQUISITE_TRANINING_ID=this.defaultFormValues[5];
            this.COURSE_IN_COMPANY_OFFICE=this.defaultFormValues[6];
            this.EMPLOYEE_COST=this.defaultFormValues[7];
            this.EMPLOYER_COST=this.defaultFormValues[8];
            this.SUPPLIER_COST=this.defaultFormValues[9];
            this.INSTRUCTOR_COST=this.defaultFormValues[10];
            this.CLASS_ID=this.defaultFormValues[11];
            this.CLASS_NAME=this.defaultFormValues[12];
            this.START_DATE=this.defaultFormValues[13];
            this.END_DATE=this.defaultFormValues[14];
            this.START_TIME=this.defaultFormValues[15];
            this.END_TIME=this.defaultFormValues[16];
            this.INSTRUCTOR_NAME=this.defaultFormValues[17];
            this.ENROLLED=this.defaultFormValues[18];
            this.MAXIMUM=this.defaultFormValues[19];
            this.CLASS_LOCATION=this.defaultFormValues[20];
            this.CREATE=this.defaultFormValues[21];
            this.CLASS_ENROLLMENT=this.defaultFormValues[22];
            this.TRANINING_SIGNUP=this.defaultFormValues[23];
            this.CLASS_SKILLS=this.defaultFormValues[24];
            this.DELETE_ROW=this.defaultFormValues[25];
            this.DELETE=this.defaultFormValues[26];
            this.SAVE=this.defaultFormValues[27];
            this.CLEAR=this.defaultFormValues[28];
            this.ACTION=this.defaultFormValues[29];
            this.UPDATE=this.defaultFormValues[30];
            this.ADD_CLASS=this.defaultFormValues[31];
            this.CLOSE=this.defaultFormValues[32];
            this.SELECT=this.defaultFormValues[33];
            this.TRAINING_COURSE=this.defaultFormValues[34];
            this.PREREQUISITE=this.defaultFormValues[35];
            this.CANCEL=this.defaultFormValues[36];

		this.traningCourseIdList = Observable.create((observer: any) => {
            // Runs on every search
            console.log('this.model.traningId')
            console.log(this.model.traningId)
            observer.next(this.model.traningId);
          }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));
          
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    }

    getReportPositionsAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        
        console.log(query)
        console.log(this.getTrainingCourse)
        return Observable.of(
          this.getTrainingCourse.filter((id: any) => {
            return query.test(id.traningId);
          })
        );
      }
     
      changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
      }
     
      typeaheadOnSelect(e: TypeaheadMatch): void {
        // console.log('Selected value: ', e);
        this.trainingCourseService.getTrainingCourse(e.item.id).then(pagedData => {
            // console.log('pagedData', pagedData);
            this.model = pagedData.result;
            this.model.id = 0;
           
        });
      } 
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
		
        this.page.searchKeyword = '';
        this.trainingCourseService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.getTrainingCourse = pagedData.data;     
            console.log(this.rows);
           
        });
    }

    // Open form for create traningCourse
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.islifeTimeValidEmployeeCost = true;
        this.islifeTimeValidEmployerCost = true;
        this.islifeTimeValidSupplierCost = true;
        this.islifeTimeValidInstructorCost = true;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            traningId: '',
            desc: '',
            arbicDesc : '',
            location:'',
            prerequisiteId:'',
            courseInCompany:false,
            employeeCost:null,
            employerCost:null,
            supplierCost:null,
            instructorCost:null,
            subItems: [],
        };
        this.subItemsObj = {
            // "isDelete": false,
           "classId": '',
           "className": '',
           "instructorName": '',
           "classLocation": '',
           "maximum": null,
           "enrolled": this.trainingBatchSignupService.getEnrolledEmployees(),
           "startDate" : "",
           "endDate" : "",
           "startTime" : "",
           "endTime" : ""
         }  
         this.modelEndDate = '';
         this.modelStartDate = '';
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
        this.islifeTimeValidEmployeeCost = true;
        this.islifeTimeValidEmployerCost = true;
        this.islifeTimeValidSupplierCost = true;
        this.islifeTimeValidInstructorCost = true;
        this.subItemsObj = {
            // "isDelete": false,
           "classId": '',
           "className": '',
           "instructorName": '',
           "classLocation": '',
           "maximum": null,
           "enrolled": this.trainingBatchSignupService.getEnrolledEmployees(),
           "startDate" : "",
           "endDate" : "",
           "startTime" : "",
           "endTime" : ""
         }  
        this.modelEndDate = '';
        this.modelStartDate = '';
    }

    //function call for creating new traningCourse
    CreateTrainingCourse(f: NgForm, event: Event) {
        event.preventDefault();
        var Idx = this.model.traningId;

        console.log("aa:", this.model);

        //Check if the id is available in the model.
        //If id avalable then update the traningCourse, else Add new traningCourse.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.trainingCourseService.checkDuplicateDeptId(Idx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new traningCourse
                    this.trainingCourseService.createTrainingCourse(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            this.subItemsObj = {
                                // "isDelete": false,
                               "classId": '',
                               "className": '',
                               "instructorName": '',
                               "classLocation": '',
                               "maximum": null,
                               "enrolled": this.trainingBatchSignupService.getEnrolledEmployees(),
                               "startDate" : "",
                               "endDate" : "",
                               "startTime" : "",
                               "endTime" : ""
                             }  
                            this.modelEndDate = '';
                            this.modelStartDate = '';
                            //Refresh the Grid data after adding new traningCourse
                            
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }

    //edit traningCourse by row
    edit(row: TrainingCourse) {
        this.showCreateForm = true;
        this.isUnderUpdateClass = false;  
        this.islifeTimeValidEmployeeCost = true;
        this.islifeTimeValidEmployerCost = true;
        this.islifeTimeValidSupplierCost = true;
        this.islifeTimeValidInstructorCost = true;
        console.log(row);
        this.model = Object.assign({},row);
        
        this.isUnderUpdate = true;
        this.traningCourseId = row.id;
        this.traningCourseIdvalue = this.model.traningId;
        setTimeout(() => {
            window.scrollTo(0, 550);
        }, 10);
    }

    //edit Class by row
    editClass(row) {
        console.log(row);
        
        this.updateStartDate = row.startDate;
        this.updateEndDate = row.endDate;
        console.log(this.updateStartDate);
        console.log(this.updateEndDate);

        this.subItemsObj = Object.assign({},row);   
        this.subItemsObj.startDate =this.formatDateFordatePicker(new Date(this.subItemsObj.startDate));
        this.subItemsObj.endDate =this.formatDateFordatePicker(new Date(this.subItemsObj.endDate));   
        this.modelStartDate =this.subItemsObj.startDate;   
        this.modelEndDate =this.subItemsObj.endDate;   
        this.subItemsObj.enrolled =this.subItemsObj.enrolled ? this.subItemsObj.enrolled : 0;   
        console.log(this.subItemsObj.enrolled);
        console.log(row['$$index']);
        this.isUnderUpdateClass = true;        
        this.isUnderUpdateIndex = row['$$index'];        
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }
   
    updateStatus() {
        this.closeModal();
        //Call service api for updating selected traningCourse
        this.model.traningId = this.traningCourseIdvalue;
        this.trainingCourseService.updateTrainingCourse(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing traningCourse
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.showCreateForm = false;
                this.Create();
                //Scroll to top after editing traningCourse
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                this.model = {
                    id: 0,
                    traningId: '',
                    desc: '',
                    arbicDesc : '',
                    location:'',
                    prerequisiteId:'',
                    courseInCompany:false,
                    employeeCost:null,
                    employerCost:null,
                    supplierCost:null,
                    instructorCost:null,
                    subItems: [],
                };
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }
    


    //delete traningCourse by passing whole object of perticular TrainingCourse
    delete() {
        var selectedTrainingCourses = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedTrainingCourses.push(this.selected[i].id);
        }
        this.trainingCourseService.deleteTrainingCourse(selectedTrainingCourses).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of traningCourse
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        console.log(this.selected);
    }
    // default list on page
    onSelectClass({ selected }) {
        this.selectedClass.splice(0, this.selectedClass.length);
        this.selectedClass.push(...selected);
        
    }
    varifyDeleteClass() {
        if (this.selectedClass.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    deleteClass(){

        if (this.selectedClass.length > 0) {
            this.showCreateForm = true;
            this.isDeleteAction = true;

        } else {
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.hasMessage = true;
                this.message.type = 'error';
                this.isfailureMsg = true;
                this.showMsg = true;
                this.message.text = 'Please select at least one record to delete.';
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                
            }, 100);

            return;
        }

        // -----------------------
        let newarr = [];
        for (var i = 0; i < this.model.subItems.length; i++) {          

            for (var j = 0; j < this.selectedClass.length; j++) {
                const index = this.model.subItems.indexOf(this.selectedClass[j]);
    
                if (index !== -1) {
                    this.model.subItems.splice(index, 1);
                }
            }
        }
       // this.model.subItems = newarr;
       
        this.closeModal();
    }

    // search traningCourse by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;       
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
    
    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }


    addSubItem(subItemData: NgForm, subitem){    

        if(!this.isUnderUpdateClass){
            for (let i = 0; i < this.model.subItems.length; i++) {
                const element = this.model.subItems[i];
                if(subitem.classId == element.classId){
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.hasMessage = true;
                        this.message.type = 'error';
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.message.text = 'Class Id already exist.';
                        window.scrollTo(0, 0);
                    }, 100)
                    
                    return false;
                }
            }
        }
       // var date = new Date(this.payScheduleBeggingDate.date.year+"-"+this.payScheduleBeggingDate.date.month+"-"+this.payScheduleBeggingDate.date.day);
         //this.model.payScheduleBeggingDate=date.getTime();
         
        if(this.isUnderUpdateClass){
            console.log(this.updateStartDate);
        console.log(this.updateEndDate);

            this.model.subItems[this.isUnderUpdateIndex]['classId']=subitem.classId;
            this.model.subItems[this.isUnderUpdateIndex]['className'] = subitem.className;
            this.model.subItems[this.isUnderUpdateIndex]['instructorName'] = subitem.instructorName;
            this.model.subItems[this.isUnderUpdateIndex]['classLocation'] = subitem.classLocation;
            this.model.subItems[this.isUnderUpdateIndex]['maximum'] = subitem.maximum;
            this.model.subItems[this.isUnderUpdateIndex]['enrolled'] = subitem.enrolled;
            this.model.subItems[this.isUnderUpdateIndex]['startDate'] = this.frmStartDate ? this.frmStartDate : this.updateStartDate;
            this.model.subItems[this.isUnderUpdateIndex]['endDate'] = this.frmEndDate? this.frmEndDate : this.updateEndDate;
            this.model.subItems[this.isUnderUpdateIndex]['startTime'] = subitem.startTime;
            this.model.subItems[this.isUnderUpdateIndex]['endTime'] = subitem.endTime;
            this.isUnderUpdateClass = false;
            this.isUnderUpdateIndex = "";  
                   
        }else{
            this.subItemsObj.classId=subitem.classId;
            this.subItemsObj.className = subitem.className;
            this.subItemsObj.instructorName = subitem.instructorName;
            this.subItemsObj.classLocation = subitem.classLocation;
            this.subItemsObj.maximum = subitem.maximum;
            this.subItemsObj.enrolled = subitem.enrolled;
            this.subItemsObj.startDate = this.frmStartDate;
            this.subItemsObj.endDate = this.frmEndDate;
            this.subItemsObj.startTime = subitem.startTime;
            this.subItemsObj.endTime = subitem.endTime;
            //this.model.subItems.push(Object.assign({}, this.subItemsObj));
            this.model.subItems.unshift(Object.assign({}, this.subItemsObj))
        }
        console.log(this.model.subItems);
       subItemData.resetForm({
        // "isDelete": false,
       "classId": '',
       "className": '',
       "instructorName": '',
       "classLocation": '',
       "maximum": null,
       "enrolled": this.trainingBatchSignupService.getEnrolledEmployees(),
       "startDate" : "",
       "endDate" : "",
       "startTime" : "",
       "endTime" : ""
     }  );  
      // this.subItemsObj.enrolled = 0;

        if(this.traningCourseIdvalue){
            this.model.traningId = this.traningCourseIdvalue;
            this.trainingCourseService.updateTrainingCourse(this.model).then(data => {
                var datacode = data.code;
                if (datacode == 201) {
                    this.updateStartDate = '';
                    this.updateEndDate = '';
                   
                
                }
            }).catch(error => {
                
            });
        }

        this.subItemsObj = {
            // "isDelete": false,
           "classId": '',
           "className": '',
           "instructorName": '',
           "classLocation": '',
           "maximum": null,
           "enrolled": this.trainingBatchSignupService.getEnrolledEmployees(),
           "startDate" : "",
           "endDate" : "",
           "startTime" : "",
           "endTime" : ""
         }  
        this.modelEndDate = '';
        this.modelStartDate = '';
        
        
        
    }

    varifySubDelete(obj) {
       // this.idx = obj.id;
       this.model.subItems.pop();
        // if( obj.id === undefined){
        //     this.model.subItems.pop();
        // }
        // else if ( obj.id > 0) {
        //     this.isDeleteAction = true;
        //     this.isConfirmationModalOpen = true;
        //   } else {
        //       this.isSuccessMsg = false;
        //       this.hasMessage = true;
        //       this.message.type = 'error';
        //       this.isfailureMsg = true;
        //       this.showMsg = true;
        //       this.message.text = 'Please select at least one record to delete.';
        //       window.scrollTo(0, 0);
        //   }
    }
    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
	onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        this.startDateformatted = event.formatted;
        
        console.log('this.frmStartDate', event);
		 if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
        this.endDateformatted = event.formatted;
        
       console.log('this.frmEndDate', this.frmEndDate, event);

        if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

    formatstartTime(selectedTime){
        this.subItemsObj.startTime = moment(selectedTime).format('HH:mm');
    }
    formatendTime(selectedTime) { 
        
        this.subItemsObj.endTime = moment(selectedTime).format('HH:mm');
      
    }
    _keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
    
        if (!pattern.test(inputChar)) {
          // invalid character, prevent input
          event.preventDefault();
        }
    }

    checkdecimalemployeepay(digit)
    {
        console.log(digit);
        if(digit==null || digit==''){
            this.islifeTimeValidEmployeeCost = true;
            return;
        }
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidEmployeeCost = false;
            console.log(this.islifeTimeValidEmployeeCost);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidEmployeeCost = false;
            console.log(this.islifeTimeValidEmployeeCost);
        }

        else{
            this.islifeTimeValidEmployeeCost = true;
            console.log(this.islifeTimeValidEmployeeCost);
        }

    }

    checkdecimalemployerCost(digit)
    {
        console.log(digit);
        if(digit==null || digit==''){
            this.islifeTimeValidEmployerCost = true;
            return;
        }
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidEmployerCost = false;
            console.log(this.islifeTimeValidEmployerCost);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidEmployerCost = false;
            console.log(this.islifeTimeValidEmployerCost);
        }

        else{
            this.islifeTimeValidEmployerCost = true;
            console.log(this.islifeTimeValidEmployerCost);
        }

    }

    checkdecimalsupplierCost(digit)
    {
        console.log(digit);
        if(digit==null || digit==''){
            this.islifeTimeValidSupplierCost = true;
            return;
        }
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidSupplierCost = false;
            console.log(this.islifeTimeValidSupplierCost);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidSupplierCost = false;
            console.log(this.islifeTimeValidSupplierCost);
        }

        else{
            this.islifeTimeValidSupplierCost = true;
            console.log(this.islifeTimeValidSupplierCost);
        }

    }

    checkdecimalinstructorCost(digit)
    {
        console.log(digit);
        if(digit==null || digit==''){
            this.islifeTimeValidInstructorCost = true;
            return;
        }
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidInstructorCost = false;
            console.log(this.islifeTimeValidInstructorCost);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValidInstructorCost = false;
            console.log(this.islifeTimeValidInstructorCost);
        }

        else{
            this.islifeTimeValidInstructorCost = true;
            console.log(this.islifeTimeValidInstructorCost);
        }

    }
}