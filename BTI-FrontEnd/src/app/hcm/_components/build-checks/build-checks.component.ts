import { Component, ElementRef, OnInit, ViewChild, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { BuildChecks } from '../../_models/build-checks/build-checks.module';
import { BuildChecksService } from '../../_services/build-checks/build-checks.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { BuildCheckStorageService } from '../../../_sharedresource/_services/build-check-storage.service';
import { NgForm } from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { RoutesRecognized } from '@angular/router';
import 'rxjs/add/observable/forkJoin';
import * as cloneDeep from 'lodash/cloneDeep';
import { BuildCheckCommonService } from '../../../_sharedresource/_services/build-check-common.service';

@Component({
    selector: 'buildChecks',
    templateUrl: './build-checks.component.html',
    styleUrls: ['./build-checks.component.css'],
    providers: [BuildChecksService, BuildCheckStorageService]
})
export class BuildChecksComponent implements OnInit, AfterViewChecked {
    page = new Page();
    rows = new Array<BuildChecks>();
    temp = new Array<BuildChecks>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1468';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    searchKeyword = '';
    ddPageSize: number = 5;
    model: BuildChecks;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    lastaccruedDate;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    errorhours = false;
    errorhoursMsg;

    submitted: boolean = false;
    endDateModel: any = null;
    startDateModel: any = null;
    endDateModelInvalid: boolean = false;
    startDateModelInvalid: boolean = true;
    hasBeenCreated: boolean = false;
    defaultExists: boolean = false;
    defaultID: any;

    defChange: boolean = false;
    dfchange: boolean = false;
    efchange: boolean = false;

    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    defaultSource: Observable<any>;
    employeeSourceTo: Observable<any>;
    departmentSourceTo: Observable<any>;
    employeeSourceFrom: Observable<any>;
    departmentSourceFrom: Observable<any>;
    defaultIdsList = [];
    employeesOfAllDepartmentList = [];
    selectedEmployeeDepartment: Array<any> = [];
    allEmployeeIdsList = [];
    selectedAllEmployeeIdsList: Array<any> = [];
    selectedFewEmployeeIdsList: Array<any> = [];
    employeeIdsList = [];
    departmentSelectedList = [];
    employeeSelectedList = [];
    employeeIdsettings = {};
    employeeDepartmentSettings = {};
    fromEmployeeId: string;
    toEmployeeId: string;
    fromDepartmentId: string;
    toDepartmentId: string;
    selectedDep = [];
    unselectedDep = [];
    unselectEmp = [];
    mainDep = [];
    dataRows: any = [];
    pattern = /^[\d]*$/;
    employeeDp = [];
    departmentDp = [];
    selecteddep = [];
    itemList: any
    selectedItems: any
    selectedDepartment: any = [];
    minDate: Date = new Date();
    loaderImg: boolean = false;
    checkEmpValidId: boolean = true;

    // label Variable
    BUILD_CHECKS_CLEAR: any;
    BUILD_CHECKS_REMOVE: any;
    BUILD_CHECKS_BUILD: any;
    BUILD_CHECKS_INCL_BATCHES: any;
    BUILD_CHECKS_INCL_BENEFITS: any;
    BUILD_CHECKS_INCL_DEDUCTIONS: any;
    BUILD_CHECKS_INCL_PAY_CODES: any;
    BUILD_CHECKS_DAILY_MISC: any;
    BUILD_CHECKS_ANNUALLY: any;
    BUILD_CHECKS_SEMIANNUALLY: any;
    BUILD_CHECKS_QUARTERLY: any;
    BUILD_CHECKS_MONTHLY: any;
    BUILD_CHECKS_SEMIMONTHLY: any;
    BUILD_CHECKS_BIWEEKLY: any;
    BUILD_CHECKS_EMP_ID: any;
    BUILD_CHECKS_EMP_DEPT: any;
    BUILD_CHECKS_ALL_LABEL: any;
    BUILD_CHECKS_TO_LABEL: any;
    BUILD_CHECKS_FROM_LABEL: any;
    BUILD_CHECKS_PAY_PERIOD_DATE: any;
    BUILD_CHECKS_PAY_RUN_RANGE: any;
    BUILD_CHECKS_TYPE_PAY_RUN: any;
    BUILD_CHECKS_DESCRIPTION: any;
    BUILD_CHECKS_DEFAULT_ID: any;
    BUILD_CHECKS_EMP_DEPT_SELECTED_LABEL: any;
    BUILD_CHECKS_EMP_DEPT_TO_LABEL: any;
    BUILD_CHECKS_EMP_ID_SELECTED_LABEL: any;
    BUILD_CHECKS_PAY_PERIOD_FROM_LABEL: any;
    BUILD_CHECKS_PAY_PERIOD_TO_LABEL: any;
    BUILD_CHECKS_INCL_PAY_PERIOD: any;
    BUILD_CHECKS_WEEKLY: any;
    BUILD_CHECKS_SAVE: any;
    BUILD_CHECKS_EMPEXCLD_LABEL: any;
    BUILD_CHECKS_SELECTED_EMPLOYEE_LABEL: any;

    payRunTypes = [{ type: "Regular Pay", value: "1" }, { type: "Advance Pay", value: "2" }];
    selectedEmpDepartment: Array<any> = [];
    selectedEmpId: Array<any> = [];
    showSelectedEmployeeDepartment: any[] = [];
    showSelectedEmployeeId: any[] = [];
    employeeIdAccordingToDepartment: any[] = [];
    AllDepartmentList: any[] = [];
    disabled: boolean = false;
    selectErrorDpt: string;
    selectErrorEmp: string
    isSelectedDpt: boolean = false;
    isSelectedEmp: boolean = false;
    clearDisabled: boolean = false;

    constructor(private router: Router,
        private buildChecksService: BuildChecksService,
        private buildCheckStorageService: BuildCheckStorageService,
        private getScreenDetailService: GetScreenDetailService,
        private buildCheckCommonService: BuildCheckCommonService,
        private alertService: AlertService) {
        this.model = {
            id: null,
            defaultId: null,
            description: "",
            checkDate: new Date(0),
            checkByUserId: "",
            typeofPayRun: null,
            dateFrom: new Date(0),
            dateTo: new Date(0),
            weekly: false,
            biweekly: false,
            semimonthly: false,
            monthly: false,
            quarterly: false,
            semiannually: false,
            annually: false,
            dailyMisc: false,
            allEmployees: true,
            fromEmployeeId: null,
            toEmployeeId: null,
            allDepartment: true,
            fromDepartmentId: null,
            toDepartmentId: null,
            dtoEmployeeMasters: [],
            dtoDepartments: []
        };
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.employeeSelectedList = [];
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'BUILD_CHECKS_CLEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_REMOVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_BUILD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_INCL_BATCHES', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_INCL_BENEFITS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_INCL_DEDUCTIONS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_INCL_PAY_CODES', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_DAILY_MISC', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_ANNUALLY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_SEMIANNUALLY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_QUARTERLY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_MONTHLY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_SEMIMONTHLY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_BIWEEKLY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_EMP_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_EMP_DEPT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_ALL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_TO_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_FROM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_PAY_PERIOD_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_PAY_RUN_RANGE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_TYPE_PAY_RUN', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_DEFAULT_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_EMP_DEPT_SELECTED_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_EMP_DEPT_TO_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_EMP_ID_SELECTED_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_PAY_PERIOD_FROM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_PAY_PERIOD_TO_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_INCL_PAY_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_WEEKLY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BUILD_CHECKS_EMPEXCLD_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'BUILD_CHECKS_SELECTED_EMPLOYEE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];

        this.BUILD_CHECKS_CLEAR = this.defaultFormValues[0];
        this.BUILD_CHECKS_REMOVE = this.defaultFormValues[1];
        this.BUILD_CHECKS_BUILD = this.defaultFormValues[2];
        this.BUILD_CHECKS_INCL_BATCHES = this.defaultFormValues[3];
        this.BUILD_CHECKS_INCL_BENEFITS = this.defaultFormValues[4];
        this.BUILD_CHECKS_INCL_DEDUCTIONS = this.defaultFormValues[5];
        this.BUILD_CHECKS_INCL_PAY_CODES = this.defaultFormValues[6];
        this.BUILD_CHECKS_DAILY_MISC = this.defaultFormValues[7];
        this.BUILD_CHECKS_ANNUALLY = this.defaultFormValues[8];
        this.BUILD_CHECKS_SEMIANNUALLY = this.defaultFormValues[9];
        this.BUILD_CHECKS_QUARTERLY = this.defaultFormValues[10];
        this.BUILD_CHECKS_MONTHLY = this.defaultFormValues[11];
        this.BUILD_CHECKS_SEMIMONTHLY = this.defaultFormValues[12];
        this.BUILD_CHECKS_BIWEEKLY = this.defaultFormValues[13];
        this.BUILD_CHECKS_EMP_ID = this.defaultFormValues[14];
        this.BUILD_CHECKS_EMP_DEPT = this.defaultFormValues[15];
        this.BUILD_CHECKS_ALL_LABEL = this.defaultFormValues[16];
        this.BUILD_CHECKS_TO_LABEL = this.defaultFormValues[17];
        this.BUILD_CHECKS_FROM_LABEL = this.defaultFormValues[18];
        this.BUILD_CHECKS_PAY_PERIOD_DATE = this.defaultFormValues[19];
        this.BUILD_CHECKS_PAY_RUN_RANGE = this.defaultFormValues[20];
        this.BUILD_CHECKS_TYPE_PAY_RUN = this.defaultFormValues[21];
        this.BUILD_CHECKS_DESCRIPTION = this.defaultFormValues[22];
        this.BUILD_CHECKS_DEFAULT_ID = this.defaultFormValues[23];
        this.BUILD_CHECKS_EMP_DEPT_SELECTED_LABEL = this.defaultFormValues[24];
        this.BUILD_CHECKS_EMP_DEPT_TO_LABEL = this.defaultFormValues[25];
        this.BUILD_CHECKS_EMP_ID_SELECTED_LABEL = this.defaultFormValues[26];
        this.BUILD_CHECKS_PAY_PERIOD_FROM_LABEL = this.defaultFormValues[27];
        this.BUILD_CHECKS_PAY_PERIOD_TO_LABEL = this.defaultFormValues[28];
        this.BUILD_CHECKS_INCL_PAY_PERIOD = this.defaultFormValues[29];
        this.BUILD_CHECKS_WEEKLY = this.defaultFormValues[30];
        this.BUILD_CHECKS_SAVE = this.defaultFormValues[31];
        this.BUILD_CHECKS_EMPEXCLD_LABEL = this.defaultFormValues[32];
        this.BUILD_CHECKS_SELECTED_EMPLOYEE_LABEL = this.defaultFormValues[33];
        this.defaultSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.id);
        }).mergeMap((token: string) => this.getDefaultAsObservable(token));
    }
    ngOnInit() {
        this.selectErrorDpt = '';
        this.selectErrorEmp = '';
        this.checkEmpValidId = false;
        this.startDateModel = null;
        this.endDateModel = null;
        this.currentLanguage = localStorage.getItem('currentLanguage');
        if ((this.buildCheckCommonService.pathFromBCID === true && this.buildCheckCommonService.modelData != null) ||
            (this.buildCheckCommonService.pathFromBCIB === true && this.buildCheckCommonService.modelData != null) ||
            (this.buildCheckCommonService.pathFromBCSBs === true && this.buildCheckCommonService.modelData != null) ||
            (this.buildCheckCommonService.pathFromBCIPC === true && this.buildCheckCommonService.modelData != null)) {
            this.selectedEmployeeDepartment = [];
            this.selectedFewEmployeeIdsList = [];
            this.buildCheckCommonService.pathFromBCID = false;
            this.buildCheckCommonService.pathFromBCIB = false;
            this.buildCheckCommonService.pathFromBCSBs = false;
            this.buildCheckCommonService.pathFromBCIPC = false;
            // this.loaderImg = true;
            this.buildChecksService.getBuildCheckByDefaultID(this.buildCheckCommonService.defaultId).then(data => {
                console.log('Data for returning from another page', data)
                if (data.result.records.length != 0) {
                    let selected = data.result.records[0];
                    this.buildCheckCommonService.modelData = selected;
                    this.model.id = selected.id;
                    this.model.defaultId = selected.default1.id;
                    this.defaultID = this.buildCheckCommonService.defaultDisplay;
                    this.model.description = selected.description;
                    this.model.checkDate = selected.checkDate;
                    this.model.checkByUserId = selected.checkByUserId;
                    this.model.typeofPayRun = selected.typeofPayRun;
                    this.model.dateFrom = selected.dateFrom;
                    this.startDateModel = this.formatDateFordatePicker(this.model.dateFrom);
                    this.model.dateTo = selected.dateTo;
                    this.endDateModel = this.formatDateFordatePicker(this.model.dateTo);
                    this.startDateModelInvalid = false;
                    this.endDateModelInvalid = false;
                    this.model.weekly = selected.weekly;
                    this.model.biweekly = selected.biweekly;
                    this.model.semimonthly = selected.semimonthly;
                    this.model.monthly = selected.monthly;
                    this.model.quarterly = selected.quarterly;
                    this.model.semiannually = selected.semiannually;
                    this.model.annually = selected.annually;
                    this.model.dailyMisc = selected.dailyMisc;
                    this.model.allEmployees = selected.allEmployees;
                    this.model.allDepartment = selected.allDepertment;
                    this.disabled = selected.disable
                    if (!this.model.allEmployees) {
                        if (selected.dtoEmployeeMasters.length > 0) {
                            this.model.dtoEmployeeMasters = [];
                            for (let i = 0; i < selected.dtoEmployeeMasters.length; i++) {
                                let empObj = {
                                    id: selected.dtoEmployeeMasters[i].employeeIndexId,
                                    itemName: selected.dtoEmployeeMasters[i].employeeId
                                }
                                this.showSelectedEmployeeId.push(empObj);
                                this.model.dtoEmployeeMasters.push(selected.dtoEmployeeMasters[i].employeeIndexId)
                            }
                            this.EmployeeSetting();
                        }
                    } else {
                        this.buildCheckCommonService.listEmployeeId = [];
                        this.buildCheckCommonService.listEmployeeId = selected.dtoEmployeeMasters;
                        this.EmployeeSetting();
                    }

                    if (!this.model.allDepartment) {
                        this.departmentSetting();
                        if (selected.dtoDepartments.length > 0) {
                            this.model.dtoDepartments = [];
                            for (let i = 0; i < selected.dtoDepartments.length; i++) {
                                let deptObj = {
                                    id: selected.dtoDepartments[i].id,
                                    itemName: selected.dtoDepartments[i].departmentId
                                }
                                this.showSelectedEmployeeDepartment.push(deptObj);
                                this.model.dtoDepartments.push(selected.dtoDepartments[i].id);
                                this.employeeIdAccordingToDepartment = this.model.dtoDepartments;
                            }
                            this.employeeListInAfterSaveMode();
                        }
                    } else {
                        this.departmentSetting();
                    }

                }


            });
        } else {
            this.model = {
                id: null,
                defaultId: null,
                description: "",
                checkDate: new Date(0),
                checkByUserId: "",
                typeofPayRun: null,
                dateFrom: new Date(0),
                dateTo: new Date(0),
                weekly: false,
                biweekly: false,
                semimonthly: false,
                monthly: false,
                quarterly: false,
                semiannually: false,
                annually: false,
                dailyMisc: false,
                allEmployees: true,
                fromEmployeeId: null,
                toEmployeeId: null,
                allDepartment: true,
                fromDepartmentId: null,
                toDepartmentId: null,
                dtoEmployeeMasters: [],
                dtoDepartments: []
            };
            this.EmployeeSetting();
        }

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                objDefault['isMandatory'] = objAvailable['isMandatory'];
            }
        });

        this.buildChecksService.getAllDefaultDropDown().then(data => {
            data.result.forEach(row => { this.defaultIdsList.push(row); });
        });
        this.buildChecksService.getAllDepartmentDropDown().then(data => {
            this.AllDepartmentList = [];
            this.model.dtoDepartments = [];
            this.buildCheckCommonService.employeeDepartmentList = data.result;
            for (let i = 0; i < this.buildCheckCommonService.employeeDepartmentList.length; i++) {
                if (this.buildCheckCommonService.employeeDepartmentList[i].departmentId != undefined) {

                    let empdpt = {
                        id: this.buildCheckCommonService.employeeDepartmentList[i].id,
                        itemName: this.buildCheckCommonService.employeeDepartmentList[i].departmentId
                    }
                    this.AllDepartmentList.push(empdpt);
                    this.model.dtoDepartments.push(this.buildCheckCommonService.employeeDepartmentList[i].id);
                }
            }
            console.log("this.AllDepartmentList", this.AllDepartmentList);

            this.departmentSetting();
        });



    }
    departmentSetting() {

        this.employeeDepartmentSettings = {
            singleSelection: false,
            enableCheckAll: true,
            text: "Select Department",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            searchPlaceholderText: "Search Department IDs",
            badgeShowLimit: 2,
            disabled: this.model.allDepartment
        }

    }


    departmentSettingForProcessedData = {
        singleSelection: false,
        enableCheckAll: true,
        text: "Select Department",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        searchPlaceholderText: "Search Department IDs",
        badgeShowLimit: 2,
        disabled: true
    }


    EmployeeSettingForProcessedData = {
        singleSelection: false,
        enableCheckAll: true,
        text: "Select Employee",
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: "myclass custom-class",
        searchPlaceholderText: "Search Employee IDs",
        badgeShowLimit: 2,
        disabled: true
    };

    EmployeeSetting() {
        this.employeeIdsettings = {
            singleSelection: false,
            enableCheckAll: true,
            text: "Select Employee",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class",
            searchPlaceholderText: "Search Employee IDs",
            badgeShowLimit: 2,
            disabled: this.model.allEmployees
        };
    }
    // For Employee Department and Employee Id Function
    onItemSelect(item: any, userCase: any) {
        console.log(this.showSelectedEmployeeDepartment);
        switch (userCase) {
            case 'employeeDepartment':
                this.isSelectedDpt = false;
                this.employeeIdAccordingToDepartment = [];
                console.log('this.model.dtoDepartments', this.model.dtoDepartments)
                this.model.dtoDepartments.push(item.id);
                for (let i = 0; i < this.model.dtoDepartments.length; i++) {
                    this.employeeIdAccordingToDepartment.push(this.model.dtoDepartments[i]);
                }
                console.log('this.model.dtoDepartments', this.model.dtoDepartments)
                this.employeeList();
                break;
            case 'employeeId':
                this.isSelectedEmp = false;
                this.model.dtoEmployeeMasters.push(item.id);
                console.log('this.model.dtoEmployeeMasters', this.model.dtoEmployeeMasters)
                break;
        }
    }
    OnItemDeSelect(item: any, userCase: any) {
        console.log(item);
        switch (userCase) {
            case 'employeeDepartment':
                this.isSelectedDpt = false;
                for (let i = 0; i < this.AllDepartmentList.length; i++) {
                    if (this.employeeIdAccordingToDepartment[i] == item.id) {
                        this.model.dtoDepartments.splice(i, 1);
                        this.employeeIdAccordingToDepartment.splice(i, 1);
                    }
                }
                console.log('this.model.dtoDepartments', this.model.dtoDepartments)
                this.employeeList();
                break;
            case 'employeeId':
                this.isSelectedEmp = false;
                for (let i = 0; i < this.employeesOfAllDepartmentList.length; i++) {
                    if (this.model.dtoEmployeeMasters[i] == item.id) {
                        this.model.dtoEmployeeMasters.splice(i, 1);
                    }

                }
                console.log('this.model.dtoEmployeeMasters', this.model.dtoEmployeeMasters)
                break;
        }

    }
    onSelectAll(items: any, userCase: any) {
        console.log(items);
        switch (userCase) {
            case 'employeeDepartment':
                this.isSelectedDpt = false;
                this.model.dtoDepartments = [];
                for (let i = 0; i < items.length; i++) {
                    this.model.dtoDepartments.push(items[i].id);
                    this.employeeIdAccordingToDepartment.push(items[i].id);
                }
                console.log("this.model.dtoDepartments", this.model.dtoDepartments)
                this.employeeList();
                break;
            case 'employeeId':
                this.isSelectedEmp = false;
                this.model.dtoEmployeeMasters = []
                for (let i = 0; i < items.length; i++) {
                    this.model.dtoEmployeeMasters.push(items[i].id);
                }
                console.log("this.model.dtoEmployeeMasters", this.model.dtoEmployeeMasters)
                break;
        }
    }
    onDeSelectAll(items: any, userCase: any) {
        console.log(items.length);
        switch (userCase) {
            case 'employeeDepartment':
                this.isSelectedDpt = false;
                this.model.dtoDepartments = [];
                this.showSelectedEmployeeDepartment = [];
                this.employeeIdAccordingToDepartment = [];
                this.employeesOfAllDepartmentList = [];
                console.log('this.model.dtoDepartments', this.model.dtoDepartments)
                break;
            case 'employeeId':
                this.isSelectedEmp = false;
                this.model.dtoEmployeeMasters = [];
                this.showSelectedEmployeeId = [];
                console.log('this.model.dtoEmployeeMasters', this.model.dtoEmployeeMasters)
                break;
        }
    }

    // Get Employee By Department
    employeeList() {
        if (this.employeeIdAccordingToDepartment.length) {
            this.buildChecksService.getAllEmployeeByDepartment(this.employeeIdAccordingToDepartment).then(data => {
                this.employeesOfAllDepartmentList = [];
                for (let i = 0; i < data.result.length; i++) {
                    if (data.result[i].employeeId != undefined) {
                        let allEmp = {
                            id: data.result[i].employeeIndexId,
                            itemName: data.result[i].employeeId
                        }
                        this.employeesOfAllDepartmentList.push(allEmp)
                    }
                }
                this.showSelectedEmployeeId = [];

                this.EmployeeSetting();
            });
        }
    }

    employeeListInAfterSaveMode() {
        if (this.employeeIdAccordingToDepartment.length) {
            this.buildChecksService.getAllEmployeeByDepartment(this.employeeIdAccordingToDepartment).then(data => {
                this.employeesOfAllDepartmentList = [];
                for (let i = 0; i < data.result.length; i++) {
                    if (data.result[i].employeeId != undefined) {
                        let allEmp = {
                            id: data.result[i].employeeIndexId,
                            itemName: data.result[i].employeeId
                        }
                        this.employeesOfAllDepartmentList.push(allEmp)
                    }
                }

                this.EmployeeSetting();
            });
        }
    }

    AllEmployeeId() {
        this.isSelectedEmp = false;
        this.model.dtoEmployeeMasters = [];
        this.model.allEmployees = true;
        this.EmployeeSetting();
    }
    FromEmployeeId() {
        this.isSelectedEmp = false;
        this.model.allEmployees = false;
        this.model.dtoEmployeeMasters = [];
        this.EmployeeSetting();
        if (this.model.dtoDepartments.length > 0) {
            this.employeeIdAccordingToDepartment = [];
            for (let i = 0; i < this.model.dtoDepartments.length; i++) {
                this.employeeIdAccordingToDepartment.push(this.model.dtoDepartments[i]);
            }
            this.employeeList();
        }

    }

    DepartmentAll() {
        this.isSelectedDpt = false;
        this.model.dtoDepartments = [];
        this.model.dtoEmployeeMasters = [];
        this.showSelectedEmployeeDepartment = []
        if (this.AllDepartmentList.length) {
            for (let i = 0; i < this.AllDepartmentList.length; i++) {
                this.model.dtoDepartments.push(this.AllDepartmentList[i].id);
                this.employeeIdAccordingToDepartment.push(this.AllDepartmentList[i].id);
            }
        }
        this.employeeList();
        this.model.allDepartment = true;
        this.employeeDepartmentSettings["disabled"] = this.model.allDepartment;
    }
    DepartmentFrom() {
        this.isSelectedDpt = true;
        this.model.dtoDepartments = [];
        this.model.allDepartment = false;
        this.employeesOfAllDepartmentList = [];
        this.showSelectedEmployeeDepartment = [];
        this.showSelectedEmployeeId = [];
        this.employeeDepartmentSettings["disabled"] = this.model.allDepartment;
    }
    ngAfterViewChecked() {
    }

    Clear(f: NgForm) {
        f.resetForm({
            id: null,
            defaultId: null,
            description: "",
            checkDate: new Date(0),
            checkByUserId: "",
            typeofPayRun: null,
            dateFrom: new Date(0),
            dateTo: new Date(0),
            weekly: false,
            biweekly: false,
            semimonthly: false,
            monthly: false,
            quarterly: false,
            semiannually: false,
            annually: false,
            dailyMisc: false,
            allEmployees: true,
            fromEmployeeId: null,
            toEmployeeId: null,
            allDepartment: true,
            fromDepartmentId: null,
            toDepartmentId: null
        });
        this.isSelectedDpt = false;
        this.isSelectedEmp = false;
        this.submitted = false;
        this.defaultExists = false;
        this.hasBeenCreated = false;
        this.endDateModelInvalid = false;
        this.startDateModelInvalid = true;
        this.defChange = false;
        this.dfchange = false;
        this.efchange = false;
        this.checkEmpValidId = false;
        this.employeeDepartmentSettings['disabled'] = this.model.allDepartment;
        this.employeeIdsettings['disabled'] = this.model.allEmployees;
        this.clearDisabled = true
    }

    CreateBuildChecks(f: NgForm) {
        if (!(this.model.weekly || this.model.biweekly || this.model.semimonthly || this.model.monthly ||
            this.model.quarterly || this.model.semiannually || this.model.annually || this.model.dailyMisc)) {
            return;
        }
        if (this.model.allDepartment == false || this.model.allEmployees == false) {
            if (this.model.dtoDepartments.length == 0) {
                this.isSelectedDpt = true;
                this.selectErrorDpt = 'This field is required.';
                return;
            }
            if (this.model.allEmployees == false) {
                if (this.model.dtoEmployeeMasters.length == 0) {
                    this.isSelectedEmp = true;
                    this.selectErrorEmp = 'This field is required.';
                    return;
                }
            }
        }
        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
            this.loaderImg = true;
        } else {
            this.loaderImg = true;
            this.buildChecksService.createBuildChecks(this.model).then(data => {
                var datacode = data.code;
                this.buildCheckCommonService.modelData = data.result;
                this.dataRows = data.result;
                if (datacode == 201) {
                    this.model.typeofPayRun == 1 ? this.getAllForEmployeePaycodeApiCall() : null;
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.messageText = data.btiMessage.message;
                    }, 100);

                    this.hasMsg = true;
                    // this.hasBeenCreated = true;
                    this.model.id = data.result.id;
                    this.loaderImg = false;
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin !";
                }, 100)
            });
        }
    }

    /** New Api Call BuildCheck Paycode */
    getAllForEmployeePaycodeApiCall() {
        let tempMOdel = {
            "id": this.model.id,
            "all": true,
            "sortOn": "id",
            "sortBy": "DESC",
            "searchKeyword": "",
            "pageNumber": 0,
            "pageSize": 5
        }
        this.buildChecksService.getAllForEmployeePaycode(tempMOdel).then(data => {
            console.log('NEW API CALL DATA', data);
        })
    }

    updateStatus(f: NgForm) {
        this.closeModal();
        console.log("Form Valid", f);
        console.log("this.model for update", this.model);
        this.loaderImg = true;
        this.buildChecksService.updateBuildChecks(this.model).then(data => {
            var datacode = data.code;
            this.dataRows = data.result;
            if (datacode == 201) {
                this.model.typeofPayRun == 1 ? this.getAllForEmployeePaycodeApiCall() : null;
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                }, 100);
                this.loaderImg = false;
                this.hasMsg = true;
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin !";
            }, 100)
        });

    }

    verifyDelete() {

        if (this.model.id == 0 || this.model.id == undefined) {
            window.scrollTo(0, 0);
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "The build you are trying to delete does not exist.";
            }, 100);

            return;
        }
        else {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
    }


    //delete department by passing whole object of perticular Department
    delete(f: NgForm) {
        var selectedShiftCodeSetups = [];
        selectedShiftCodeSetups.push(this.model.id);
        this.buildChecksService.deleteBuildChecks(selectedShiftCodeSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
            }
            this.hasMessage = true;
            this.message.type = 'success';

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
        this.Clear(f);
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    onStartDateChanged(event: IMyDateModel): void {

        this.startDateModel = event.jsdate;
        this.model.dateFrom = new Date(0);
        this.model.dateFrom.setUTCSeconds(event.epoc);
        if (event.jsdate == null)
            this.startDateModelInvalid = true;
        else
            this.startDateModelInvalid = false;

        if (this.endDateModel == null) {
            this.endDateModelInvalid = false;
        } else if ((new Date(this.model.dateTo)).valueOf() < event.jsdate.valueOf()) {
            this.endDateModelInvalid = true;
        } else {
            this.endDateModelInvalid = false;
        }
    }

    onEndDateChanged(event: IMyDateModel): void {
        this.endDateModel = event.jsdate;
        this.model.dateTo = new Date(0);
        this.model.dateTo.setUTCSeconds(event.epoc);
        if (event.jsdate == null)
            this.endDateModelInvalid = true;
        else if (this.startDateModel != null) {
            if ((new Date(this.model.dateFrom)).valueOf() > event.jsdate.valueOf()) {
                this.endDateModelInvalid = true;
            }
            else {
                this.endDateModelInvalid = false;
            }
        }

    }
    getDefaultAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.defaultIdsList.filter((id: any) => {
                console.log(id);
                return query.test(id.defaultID);
            })
        );
    }

    changeTypeaheadLoading(e: boolean, typeofSource: any): void {
        this.typeaheadLoading = e;
        if (typeofSource == "defaultId") {
            if (this.defaultExists && !this.defaultIdsList.filter(row => row.defaultID == this.defaultID).length) {
                this.defaultExists = false;
                this.model.description = '';
                this.hasBeenCreated = false;
            }
        }
    }

    typeaheadOnSelect(e: TypeaheadMatch, typeofSource: any): void {
        this.clearDisabled = false;
        this.endDateModelInvalid = false;
        if (typeofSource == "defaultId") {
            this.isSelectedDpt = false;
            this.isSelectedEmp = false;
            this.selectedEmployeeDepartment = [];
            this.selectedAllEmployeeIdsList = [];
            this.model.defaultId = e.item.id;
            this.buildCheckCommonService.defaultId = e.item.id;
            this.buildCheckCommonService.defaultDisplay = e.item.defaultID;
            this.defaultID = e.item.defaultID;
            this.model.description = e.item.desc;
            this.defaultExists = true;
            this.buildChecksService.getBuildCheckByDefaultID(this.model.defaultId).then(data => {
                if (data.result.records.length != 0) {
                    // this.loaderImg = true;
                    let selected = data.result.records[0];
                    this.buildCheckCommonService.modelData = selected;
                    this.model.id = selected.id;
                    this.model.description = selected.description;
                    this.model.checkDate = selected.checkDate;
                    this.model.checkByUserId = selected.checkByUserId;
                    this.model.typeofPayRun = selected.typeofPayRun;
                    this.model.dateFrom = selected.dateFrom;
                    this.startDateModel = this.formatDateFordatePicker(this.model.dateFrom);
                    this.model.dateTo = selected.dateTo;
                    this.endDateModel = this.formatDateFordatePicker(this.model.dateTo);
                    this.startDateModelInvalid = false;
                    this.endDateModelInvalid = false;
                    this.model.weekly = selected.weekly;
                    this.model.biweekly = selected.biweekly;
                    this.model.semimonthly = selected.semimonthly;
                    this.model.monthly = selected.monthly;
                    this.model.quarterly = selected.quarterly;
                    this.model.semiannually = selected.semiannually;
                    this.model.annually = selected.annually;
                    this.model.dailyMisc = selected.dailyMisc;
                    this.model.allEmployees = selected.allEmployees;
                    this.model.allDepartment = selected.allDepertment;
                    this.disabled = selected.disable;
                    if (!this.model.allEmployees) {
                        this.EmployeeSetting();
                        if (selected.dtoEmployeeMasters.length > 0) {
                            this.model.dtoEmployeeMasters = [];
                            this.showSelectedEmployeeId = [];
                            for (let i = 0; i < selected.dtoEmployeeMasters.length; i++) {
                                let empObj = {
                                    id: selected.dtoEmployeeMasters[i].employeeIndexId,
                                    itemName: selected.dtoEmployeeMasters[i].employeeId
                                }
                                this.showSelectedEmployeeId.push(empObj);
                                this.model.dtoEmployeeMasters.push(selected.dtoEmployeeMasters[i].employeeIndexId);
                            }
                        }
                    } else {
                        this.buildCheckCommonService.listEmployeeId = [];
                        this.buildCheckCommonService.listEmployeeId = selected.dtoEmployeeMasters;
                    }

                    if (!this.model.allDepartment) {
                        this.departmentSetting();
                        if (selected.dtoDepartments.length > 0) {
                            this.model.dtoDepartments = [];
                            this.showSelectedEmployeeDepartment = [];
                            for (let i = 0; i < selected.dtoDepartments.length; i++) {
                                let deptObj = {
                                    id: selected.dtoDepartments[i].id,
                                    itemName: selected.dtoDepartments[i].departmentId
                                }
                                this.showSelectedEmployeeDepartment.push(deptObj);
                                this.model.dtoDepartments.push(selected.dtoDepartments[i].id);
                                this.employeeIdAccordingToDepartment = this.model.dtoDepartments;
                            }
                            this.employeeListInAfterSaveMode();
                        }
                    }

                }
                else {
                    this.model.id = null;
                    this.model.checkDate = null;
                    this.model.checkByUserId = null;
                    this.model.typeofPayRun = null;

                    this.model.dateFrom = null;
                    this.startDateModel = null;
                    this.model.dateTo = null;
                    this.endDateModel = null;

                    this.model.weekly = null;
                    this.model.biweekly = null;
                    this.model.semimonthly = null;
                    this.model.monthly = null;
                    this.model.quarterly = null;
                    this.model.semiannually = null;
                    this.model.annually = null;
                    this.model.dailyMisc = null;
                    this.model.allEmployees = true;
                    this.model.fromEmployeeId = null;
                    this.model.toEmployeeId = null;
                    this.model.allDepartment = true;
                    this.model.fromDepartmentId = null;
                    this.model.toDepartmentId = null;
                    this.hasBeenCreated = false;
                    this.isSelectedDpt = false;
                    this.isSelectedEmp = false;
                }
                this.departmentSetting();
                this.EmployeeSetting();

            });
        }
        console.log('this.model.dtoDepartments', this.model.dtoDepartments)
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }

    navigateToPayCodes() {
        Observable.forkJoin([Observable.of(this.buildCheckStorageService.stored(this.model, this.defaultID))]).subscribe(t => {
            localStorage.setItem('model', JSON.stringify(this.model));
            this.router.navigate(['hcm/buildPayrollCheckPayCodes']);
        });
    }

    navigateToBenefits() {
        Observable.forkJoin([Observable.of(this.buildCheckStorageService.stored(this.model, this.defaultID))]).subscribe(t => {
            localStorage.setItem('model', JSON.stringify(this.model));
            this.router.navigate(['hcm/buildPayrollCheckBenefits']);
        });
    }

    navigateToDeductions() {
        Observable.forkJoin([Observable.of(this.buildCheckStorageService.stored(this.model, this.defaultID))]).subscribe(t => {
            localStorage.setItem('model', JSON.stringify(this.model));
            this.router.navigate(['hcm/buildPayrollCheckDeductions']);
        });
    }

    navigateToBatches() {
        Observable.forkJoin([Observable.of(this.buildCheckStorageService.stored(this.model, this.defaultID))]).subscribe(t => {
            localStorage.setItem('model', JSON.stringify(this.model));
            this.router.navigate(['hcm/buildPayrollCheckBatches']);
        });
    }

    CheckValidID(eve: any) {
        if (eve.target.value == "") {
            this.checkEmpValidId = false;
        } else {
            for (let i = 0; i < this.defaultIdsList.length; i++) {
                if (this.defaultIdsList[i].defaultID.toLowerCase().includes(eve.target.value.toLowerCase())) {
                    this.checkEmpValidId = false;
                    break;
                } else {
                    this.checkEmpValidId = true;
                }
            }
        }
    }
}
