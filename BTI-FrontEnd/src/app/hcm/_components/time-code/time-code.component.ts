import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Page} from '../../../_sharedresource/page';
import {Constants} from '../../../_sharedresource/Constants';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Router} from '@angular/router';
import {TimeCode} from '../../_models/time-code/time-code.module';
import {TimeCodeService} from '../../_services/time-code/time-code.service';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {NgForm} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'timeCode',
    templateUrl: './time-code.component.html',
    providers : [TimeCodeService, CommonService]
})
export class TimeCodeComponent {
    page = new Page();
    rows = new Array<TimeCode>();
    temp = new Array<TimeCode>();
    selected = [];
    countries = [];
    states = [];
    cities = [];
    accrualScheduleList = [];
    linkedPayCodeList = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1417';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    timeCodeId;
    searchKeyword = '';
    ddPageSize: number = 5;
    model: TimeCode;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    timeCodeIdvalue: string;
    scheduleId: number;
    scheduleIdSelected: number;
    schedulePrimaryId: number;
    payPrimaryId: number;
    isTimeCodeFormdisabled: boolean = false;
    // LABEL variable
    TIME_CODE_ID: any;
    TIME_CODE_DECRIPTION: any;
    TIME_CODE_ARABIC_DESCRIPTION: any;
    TIME_CODE_ACCRUAL_PERIOD: any;
    TIME_CODE_TIME_TYPE: any;
    TIME_CODE_ACCRUAL_SCHEDULE: any;
    TIME_CODE_SAVE: any;
    TIME_CODE_DELETE: any;
    TIME_CODE_CLEAR: any;
    TIME_CODE_INACTIVE: any;
    TIME_CODE_UPDATE_FORM_LABEL: any;
    TIME_CODE_UPDATE: any;
    TIME_CODE_CREATE_FORM_LABEL: any;
    TIME_CODE_ACTION: any;
    TIME_CODE_SEARCH: any;
    TIME_CODE_CREATE_LABEL: any;
    TIME_CODE_LINKED_PAY_CODE: any;
    TIME_CODE_CANCEL_LABEL: any;
    TIME_CODE_TABLEVIEW: any;

    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
	frequencyArray = ['Weekly', 'Biweekly', 'Semimonthly', 'Monthly', 'Quarterly', 'Semianually', 'Anually'];
	timeTypeArray = ['Hourly', 'Salary', 'Overtime', 'Benefit', 'Absent', 'Other'];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    timecodeIdList: Observable<any>;
    getTimeCode: any[]= [];
    typeaheadLoading: boolean;

    constructor(private router: Router,
                private timeCodeService: TimeCodeService,
                private getScreenDetailService: GetScreenDetailService,
                private alertService: AlertService,
                private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            {'fieldName': 'TIME_CODE_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_DECRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_ACCRUAL_PERIOD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_TIME_TYPE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_ACCRUAL_SCHEDULE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_SAVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_DELETE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_INACTIVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
			{'fieldName': 'TIME_CODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_UPDATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
			{'fieldName': 'TIME_CODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},            
			{'fieldName': 'TIME_CODE_LINKED_PAY_CODE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''}, 
            {'fieldName': 'TIME_CODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},
            {'fieldName': 'TIME_CODE_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''},             
        ];

        this.TIME_CODE_ID = this.defaultFormValues[0];
        this.TIME_CODE_DECRIPTION = this.defaultFormValues[1];
        this.TIME_CODE_ARABIC_DESCRIPTION = this.defaultFormValues[2];
        this.TIME_CODE_ACCRUAL_PERIOD = this.defaultFormValues[3];
        this.TIME_CODE_TIME_TYPE = this.defaultFormValues[4];
        this.TIME_CODE_ACCRUAL_SCHEDULE = this.defaultFormValues[5];
        this.TIME_CODE_SAVE = this.defaultFormValues[6];
        this.TIME_CODE_DELETE = this.defaultFormValues[7];
        this.TIME_CODE_CLEAR = this.defaultFormValues[8];
        this.TIME_CODE_INACTIVE = this.defaultFormValues[9];
        this.TIME_CODE_UPDATE_FORM_LABEL = this.defaultFormValues[10];
        this.TIME_CODE_UPDATE = this.defaultFormValues[11];
        this.TIME_CODE_CREATE_FORM_LABEL = this.defaultFormValues[12];
        this.TIME_CODE_ACTION = this.defaultFormValues[13];
        this.TIME_CODE_SEARCH = this.defaultFormValues[14];
        this.TIME_CODE_CREATE_LABEL = this.defaultFormValues[15];
        this.TIME_CODE_LINKED_PAY_CODE = this.defaultFormValues[16];
        this.TIME_CODE_CANCEL_LABEL = this.defaultFormValues[17];
        this.TIME_CODE_TABLEVIEW = this.defaultFormValues[18];
        this.timecodeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.timeCodeId);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.timeCodeService.getCountry().then(data => {
            this.countries = data.result;
        });
        this.timeCodeService.getAccrualSchedule().then(data => {
            this.accrualScheduleList = data.result.records;
            // console.log('this.accrualScheduleList', this.accrualScheduleList);
        });
        this.timeCodeService.getlinkedPayCode().then(data => {
            this.linkedPayCodeList = data.result;
        });

        // getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        // Following shifted from SetPage for better performance
        this.timeCodeService.getTimecodeId().then(data => {
            this.getTimeCode = data.result.records;
        });
    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'accrualPeriod') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                        row[gridFieldName] === 7 ? 'Annually' : ''
            );
        } else if (gridFieldName === 'timeType') {
            return (
                row[gridFieldName] === 1 ? 'Hourly' :
                    row[gridFieldName] === 2 ? 'Salary' :
                        row[gridFieldName] === 3 ? 'Overtime' :
                            row[gridFieldName] === 4 ? 'Benefit' :
                                row[gridFieldName] === 5 ? 'Absent' :
                                    row[gridFieldName] === 6 ? 'Other' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

    getSuperviserIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getTimeCode.filter((id: any) => {
                return query.test(id.timeCodeId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        // console.log('E', e);
        this.timeCodeService.getTimeCode(e.item.id).then(pagedData => {
            // console.log('PageData', pagedData);
            this.model = pagedData.result;
            this.model.id = 0;
        });
    }

    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if ( pageInfo.sortOn === undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if ( pageInfo.sortBy === undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.timeCodeService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.isTimeCodeFormdisabled = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            payCodeId: 0,
            timeCodeId: '',
            inActive: false,
            desc: '',
            arbicDesc: '',
            accrualPeriod: '',
            timeType: '',
            scheduleId: 0,
            schedulePrimaryId: 0,
            payPrimaryId: 0,
            description: '',
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({timeType: '', accrualPeriod: '', payCodeId: 0, scheduleId: 0});
        this.isTimeCodeFormdisabled = false;
    }

    // function call for creating new location
    CreateTimeCode(f: NgForm, event: Event) {
        event.preventDefault();
        var locIdx = this.model.timeCodeId;

        // Check if the id is available in the model.
        // If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Check for duplicate Location Id according to it create new location
            this.timeCodeService.checkDuplicateTimeCodeId(locIdx).then(response => {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    // Call service api for Creating new location
                    this.timeCodeService.createTimeCode(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.hasMessage = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new location
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    }

    // edit department by row
    edit(row: TimeCode) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.timeCodeId = row.timeCodeId;
        this.isUnderUpdate = true;
        this.timeCodeIdvalue = this.model.timeCodeId;
        this.schedulePrimaryId = this.model.schedulePrimaryId;
        this.model.scheduleId = this.model.schedulePrimaryId;
        this.payPrimaryId = this.model.payPrimaryId;
        this.model.payCodeId = this.model.payPrimaryId;
        this.scheduleIdSelected = this.model.scheduleId;
		// this.scheduleId = this.model.scheduleId;
        this.timeCodeService.getStatesByCountryId(1).then(data => {
            this.states = data.result;
        });
        this.timeCodeService.getCitiesByStateId(1).then(data => {
            this.cities = data.result;
        });

        if (this.model.inActive === true) {
            this.isTimeCodeFormdisabled = true;
        } else {
            this.isTimeCodeFormdisabled = false;
        }
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        this.model.timeCodeId = this.timeCodeId;
        // Call service api for updating selected department
                // Call service api for Creating new location
                this.timeCodeService.updateTimeCode(this.model).then(data => {
                    var datacode = data.code;
                    if (datacode === 201) {
                        // Refresh the Grid data after editing department
                        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                        // Scroll to top after editing department
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            this.messageText = data.btiMessage.message;
                            ;
                            this.showCreateForm = false;
                        }, 100);
                        this.hasMessage = false;
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = 'Server error. Please contact admin.';
                    }, 100)
                });
            /*}*/
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    // delete department by passing whole object of perticular Department
    delete() {
        var selectedLocations = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedLocations.push(this.selected[i].id);
        }
        this.timeCodeService.deleteTimeCode(selectedLocations).then(data => {
            var datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    onCountrySelect(event) {
        this.timeCodeService.getStatesByCountryId(event.target.value).then(data => {
            this.states = data.result;
        });
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.locationService.searchLocationlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    onStateSelect(event) {
        this.timeCodeService.getCitiesByStateId(event.target.value).then(data => {
            this.cities = data.result;
        });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) === true) {
            this.model.timeCodeId = '';
            return false;
        }
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn === val ) {
            if (this.page.sortBy === 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkTimeCodeForm(event) {
        if (event.target.checked === true) {
            this.isTimeCodeFormdisabled = true;
        } else {
            this.isTimeCodeFormdisabled = false;
        }
    }

}
