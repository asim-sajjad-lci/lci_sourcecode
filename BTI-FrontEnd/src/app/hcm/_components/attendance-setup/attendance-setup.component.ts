import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Page} from '../../../_sharedresource/page';
import {Constants} from '../../../_sharedresource/Constants';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {AttendanceSetup} from '../../_models/attendance-setup/attendance-setup.module';
import {AttendanceSetupService} from '../../_services/attendance-setup/attendance-setup.service';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import {NgForm} from '@angular/forms';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';


@Component({
  selector: 'attendanceSetup',
  templateUrl: './attendance-setup.component.html',
  providers: [AttendanceSetupService],
})
export class AttendanceSetupComponent {

    page = new Page();
    rows = new Array<AttendanceSetup>();
    temp = new Array<AttendanceSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1403';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    locationId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: AttendanceSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    lastaccruedDate;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    locationIdvalue : number;
    showDiv: boolean;
    errorhours = false;
    errorhoursMsg;

    dataRows: any = [];
    pattern = /*/^\d*\.?\d{0,3}$/*/ /^[\d]*$/;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;



    constructor(private router: Router,
                private attendanceSetupService: AttendanceSetupService,
                private getScreenDetailService: GetScreenDetailService,
                private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            {'fieldName': 'LOCATION_SEARCH', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'ATTENDANCE_ACCRUAL_TYPE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'ATTENDANCE_CHECKBOX', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'ATTENDANCE_CURRENT_YEAR', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'ATTENDANCE_LAST_DAY_ACCRUED', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'NUMBER_OF_DAYS_WEEK', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'NUMBER_OF_HOURS_IN_DAY', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'NEXT_TRANSX_NUM', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_FAX', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_ACTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'LOCATION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': ''},
        ];

    }

    //public startDateModel: any = { date: { year: 2018, month: 10, day: 9 } };

    ngOnInit() {
        this.showDiv = true;
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.model  = {
            id : 0,
            accrueType: '1',
            attendance: false,
            currentyear: '',
            latDayAcuurate: null,
            numberOfDayInWeek: '',
            numberOfHourInDay: '',
            nextTranscationNum: ''
        };
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    Clear(f: NgForm) {
        f.resetForm({accrueType: '1'});
    }

    CreateAttendanceSetup(f: NgForm, event: Event) {
        this.model.latDayAcuurate = this.lastaccruedDate;
        this.attendanceSetupService.createAttendance(this.model).then(data => {
            var datacode = data.code;
            this.dataRows = data.result;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.showCreateForm = false;
                    this.messageText = data.btiMessage.message;
                }, 100);

                this.hasMsg = true;
                // f.resetForm({accrueType: '1'});

                //Refresh the Grid data after adding new department

            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin !";
            }, 100)
        });


    }

    onEndDateChanged(event: IMyDateModel): void {
        this.lastaccruedDate = event.jsdate;
    }

    hideShowDiv(event){
        if (event.target.value == 1){
            this.showDiv = true;
        } else{
            this.showDiv = false;

        }

    }
	myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };
	keyPress(event: any) {
		//const pattern = /^[0-9\-.()]+$/;
        const pattern = /^[0-9]*$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
    calculateDays(event) {
        //this.model.numberOfHourInDay
        var nod = parseInt(this.model.numberOfDayInWeek) * 24;
        var nohs = parseInt(this.model.numberOfHourInDay);        
        if( nod < nohs ){
            this.errorhours=true;
            //this.errorhoursMsg = 'Number of hours should be not be greater number of days in a week.';
        } else {
            this.errorhours=false;
        }
    }
}
