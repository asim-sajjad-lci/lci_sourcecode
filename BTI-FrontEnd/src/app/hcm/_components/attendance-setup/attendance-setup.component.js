"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var attendance_setup_service_1 = require("../../_services/attendance-setup/attendance-setup.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var AttendanceSetupComponent = (function () {
    function AttendanceSetupComponent(router, attendanceSetupService, getScreenDetailService, alertService) {
        this.router = router;
        this.attendanceSetupService = attendanceSetupService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1403';
        this.message = { 'type': '', 'text': '' };
        this.locationId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.errorhours = false;
        this.pattern = /^[\d]*$/;
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'LOCATION_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_ACCRUAL_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_CHECKBOX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_CURRENT_YEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ATTENDANCE_LAST_DAY_ACCRUED', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NUMBER_OF_DAYS_WEEK', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NUMBER_OF_HOURS_IN_DAY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NEXT_TRANSX_NUM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_FAX', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LOCATION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    //public startDateModel: any = { date: { year: 2018, month: 10, day: 9 } };
    AttendanceSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.showDiv = true;
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.model = {
            id: 0,
            accrueType: '1',
            attendance: false,
            currentyear: '',
            latDayAcuurate: null,
            numberOfDayInWeek: '',
            numberOfHourInDay: '',
            nextTranscationNum: ''
        };
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    AttendanceSetupComponent.prototype.Clear = function (f) {
        f.resetForm({ accrueType: '1' });
    };
    AttendanceSetupComponent.prototype.CreateAttendanceSetup = function (f, event) {
        var _this = this;
        this.model.latDayAcuurate = this.lastaccruedDate;
        this.attendanceSetupService.createAttendance(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.showCreateForm = false;
                    _this.messageText = data.btiMessage.message;
                }, 100);
                _this.hasMsg = true;
                f.resetForm({ accrueType: '1' });
                //Refresh the Grid data after adding new department
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = "Server error. Please contact admin !";
            }, 100);
        });
    };
    AttendanceSetupComponent.prototype.onEndDateChanged = function (event) {
        this.lastaccruedDate = event.jsdate;
    };
    AttendanceSetupComponent.prototype.hideShowDiv = function (event) {
        if (event.target.value == 1) {
            this.showDiv = true;
        }
        else {
            this.showDiv = false;
        }
    };
    AttendanceSetupComponent.prototype.calculateDays = function (event) {
        //this.model.numberOfHourInDay
        var nod = parseInt(this.model.numberOfDayInWeek) * 24;
        var nohs = parseInt(this.model.numberOfHourInDay);
        if (nod < nohs) {
            this.errorhours = true;
            //this.errorhoursMsg = 'Number of hours should be not be greater number of days in a week.';
        }
        else {
            this.errorhours = false;
        }
    };
    return AttendanceSetupComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], AttendanceSetupComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], AttendanceSetupComponent.prototype, "myScrollContainer", void 0);
AttendanceSetupComponent = __decorate([
    core_1.Component({
        selector: 'attendanceSetup',
        templateUrl: './attendance-setup.component.html',
        providers: [attendance_setup_service_1.AttendanceSetupService],
    }),
    __metadata("design:paramtypes", [router_1.Router,
        attendance_setup_service_1.AttendanceSetupService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], AttendanceSetupComponent);
exports.AttendanceSetupComponent = AttendanceSetupComponent;
//# sourceMappingURL=attendance-setup.component.js.map