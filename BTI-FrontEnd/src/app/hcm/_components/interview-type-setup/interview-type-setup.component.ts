import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { InterviewType } from '../../_models/interview-type-setup/InterviewType';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { InterviewTypeSetupService } from '../../_services/interview-type-setup/interviewtypesetup.service';
import { Constants } from '../../../_sharedresource/Constants';
import { NgForm } from '@angular/forms';
import { InterviewTypeDeail } from '../../_models/interview-type-setup/InterviewTypeSetupDetail';

@Component({
  selector: 'app-interview-type-setup',
  templateUrl: './interview-type-setup.component.html',
  providers: [InterviewTypeSetupService]
})

export class InterviewTypeSetupComponent implements OnInit {
    selectedChildDelete: any;
    isDeleteAction1: boolean;
    isConfirmationModalOpen1: boolean;
    selectedInterviewTypeSetups1 = [];
    duplicateWarning: boolean;
    hasMsg: boolean;
    showMsg: boolean;
    ddPageSize: number = 5;
    hasMessage: boolean;
    isConfirmationModalOpen: boolean;
    isDeleteAction: boolean;
    page = new Page();
    rows = new Array<InterviewType>();
    temp = new Array<InterviewType>();
    model: InterviewType;
    selectAll = {select:false};
    moduleCode = "M-1011";
    screenCode = "S-1432";
    defaultFormValues = [];
    availableFormValues = [];
    searchKeyword = '';
    message = { 'type': '', 'text': '' };
    selected = [];
    currentLanguage: any;
    moduleName:any;
    screenName:any;
    interviewTypeId;
    interviewTypeIdvalue : string;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    messageText: string;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    rowsToDelete=[];

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

  constructor(
    private interviewTypeSetupService: InterviewTypeSetupService,
    private getScreenDetailService: GetScreenDetailService) 
    {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
//this.selectedAllSub=false;
        this.defaultFormValues = [
            { 'fieldName': 'INTERVIEW_TYPE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_RANGE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_CATEGORY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_WEIGHT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_SEQUENCE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_SETUP_DETAIL_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_SETUP_DETAIL_CATEGORY_ROW_SEQUENCE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_SETUP_DETAIL_CATEGORY_SEQUENCE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'INTERVIEW_TYPE_SETUP_DETAIL_CATEGORY_WEIGHT', 'fieldValue': '', 'helpMessage': '' }
        ];
   }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');

    this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
        this.moduleName = data.result.moduleName
        this.screenName = data.result.dtoScreenDetail.screenName
        this.availableFormValues = data.result.dtoScreenDetail.fieldList;

        for (var j = 0; j < this.availableFormValues.length; j++) 
        {
            var fieldKey = this.availableFormValues[j]['fieldName'];
            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
            var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
            objDefault['fieldValue'] = objAvailable['fieldValue'];
            objDefault['helpMessage'] = objAvailable['helpMessage'];
            if (objAvailable['listDtoFieldValidationMessage']) {
                objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
            }
        }
    });
  }
  count = 0;
  addSubItem(subItemData: NgForm, subitem){
   this.count = this.count+1;
    let subItemsObj = {
        id :0,
        interviewTypeSetupDetailCategoryWeight:"",
        interviewTypeSetupDetailCategoryRowSequance:this.count,
        interviewTypeSetupDetailDescription:"",
        interviewTypeSetupDetailCategorySequance:"",
        selected:false,
        tempid:"temp"+this.model.dtoInterviewTypeSetupDetail.length + 1
    };
    // debugger
    this.model.dtoInterviewTypeSetupDetail.push(Object.assign({}, subItemsObj));
 }
 deleteRowManuallyIndex = [];
 deleteRows(){
     this.deleteRowManuallyIndex = [];
     this.selectedInterviewTypeSetups1 = [];
     this.model.dtoInterviewTypeSetupDetail.forEach((element,index) => {
        if(element.selected == true){
            if(element.id != 0){
                this.selectedInterviewTypeSetups1.push(element.id);
            }
            
        }else{
            this.deleteRowManuallyIndex.push(element);
        }
    });
    console.log(this.deleteRowManuallyIndex)
    if(this.selectedInterviewTypeSetups1.length != 0){
        this.isDeleteAction1 = true;
        this.isConfirmationModalOpen1 = true;
    }
    if(this.isDeleteAction1 == undefined || this.isDeleteAction1 == false){
        this.model.dtoInterviewTypeSetupDetail = [];
        this.model.dtoInterviewTypeSetupDetail = this.deleteRowManuallyIndex;
    }
 }

 deleteRow(){
    this.interviewTypeSetupService.deleteInterviewTypeSetupDetail(this.selectedInterviewTypeSetups1).then(data => {
        var datacode = data.code;
        if (datacode == 200) {
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        }
        this.isDeleteAction1 = false;
        this.hasMessage = true;
        this.message.type = "success";
        window.scrollTo(0, 0);
        window.setTimeout(() => {
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
            this.showMsg = true;
            if(this.deleteRowManuallyIndex.length != 0){
                this.model.dtoInterviewTypeSetupDetail = [];
                this.model.dtoInterviewTypeSetupDetail = this.deleteRowManuallyIndex;
            }
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = data.btiMessage.message;
            this.closeModal1();
        }, 100);
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }).catch(error => {
        this.hasMessage = true;
        this.message.type = "error";
        var errorCode = error.status;
        this.message.text = "Server issue. Please contact admin.";
    });
    this.closeModal1();
    this.model.dtoInterviewTypeSetupDetail.splice(this.selectedChildDelete, 1);
}

  setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.interviewTypeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
          console.log(pagedData);
        });
    }

    updateFilter(event) {
      this.searchKeyword = event.target.value.toLowerCase();
      this.page.pageNumber = 0;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.table.offset = 0;
    }

    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    onSelect({ selected }) {
      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
    }

    delete() {
        var selectedInterviewTypeSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedInterviewTypeSetups.push(this.selected[i].id);
        }
        this.interviewTypeSetupService.deleteInterviewTypeSetup(selectedInterviewTypeSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.message.type = "success";
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);
                
            }else if(datacode == 201){
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.messageText =  data.result.messageType;
                }, 100)
            }

            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
           
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    
    updateStatus() {
        this.closeModal();

        //Call service api for updating selected department
        this.model.interviewTypeId = this.interviewTypeIdvalue;
        if(this.model.dtoInterviewTypeSetupDetail.length > 0){
            this.interviewTypeSetupService.updateInterviewTypeSetup(this.model).then(data => {
                var datacode = data.code;

                if (datacode == 201) {
                    //Refresh the Grid data after editing department
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                    //Scroll to top after editing department
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        this.messageText = data.btiMessage.message;
                        this.showCreateForm = false;
                    }, 100);
                    this.hasMessage = false;
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });
        }else{
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Please enter atleast one Interview Setup Detail record.";
            }, 100)
        }
    }

    validateModel(){
        this.model.dtoInterviewTypeSetupDetail.forEach(element => {
            let haserror = false;
            if(element.interviewTypeSetupDetailDescription == ""){
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Please enter Interview type setup detail description.";
                }, 100)
                return false;
            }
            if(element.interviewTypeSetupDetailCategoryWeight == ""){
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Please enter Interview type setup detail weight.";
                }, 100)
                return false;
            }
            if(element.interviewTypeSetupDetailCategorySequance == ""){
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Please enter Interview type setup detail sequence.";
                }, 100)
                return false;
            }
        });
    }


    closeModal() {
        this.isDeleteAction1 = false;
        
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
        this.isConfirmationModalOpen1 = false;
    }

    closeModal1() {
        this.isDeleteAction1 = false;
        
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
        this.isConfirmationModalOpen1 = false;
    }

    CreateInteviewTypeSetup(f: NgForm, event: Event) {
        debugger;
        event.preventDefault();
        var interviewTypeSetupIdx = this.model.interviewTypeId;
        
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            this.interviewTypeSetupService.checkDuplicateInterviewSetupId(interviewTypeSetupIdx).then(response => {
              debugger
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } 
                else {
                    if(this.model.dtoInterviewTypeSetupDetail.length > 0){
                    //Call service api for Creating new interview type
                        this.interviewTypeSetupService.createInterviewTypeSetup(this.model).then(data => {
                            var datacode = data.code;
                            if (datacode == 201) {
                                window.scrollTo(0, 0);
                                window.setTimeout(() => {
                                    this.isSuccessMsg = true;
                                    this.isfailureMsg = false;
                                    this.showMsg = true;
                                    this.hasMessage = false;
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.hasMsg = false;
                                    }, 4000);
                                    this.showCreateForm = false;
                                    this.messageText = data.btiMessage.message;
                                }, 100);

                                this.hasMsg = true;
                                f.resetForm();
                                //Refresh the Grid data after adding new department
                                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                            }
                        }).catch(error => {
                            this.hasMsg = true;
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.messageText = "Server error. Please contact admin.";
                            }, 100)
                        });
                    }else{
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Please enter atleast one Interview Setup Detail record.";
                        }, 100)
                    }
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }
    
    // Open form for create interview type setup
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id : 0,
            interviewTypeId : '',
            interviewTypeDescription: '',
            interviewTypeDescriptionArabic: '',
           interviewRange:1, 
            dtoInterviewTypeSetupDetail:[]
        };
        this.model.dtoInterviewTypeSetupDetail = [];
    }
    
    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } 
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    onCheckBoxChange(event){
        if(event.target.checked==true)
            this.rowsToDelete.push(event.target.value);
        if(event.target.checked==false)
            this.rowsToDelete.splice(event.target.value,1);


        console.log(this.rowsToDelete);
    }

    varifyDelete1(data) {
        debugger
       /* console.log("Row to delete Array:",this.rowsToDelete)
        this.selectedInterviewTypeSetups1 = [];
        for(var i=0;i<this.rowsToDelete.length;i++){
            this.selectedChildDelete = this.rowsToDelete[i];
            this.isDeleteAction1 = true;
            this.isConfirmationModalOpen1 = true;
        this.selectedInterviewTypeSetups1.push(this.model.dtoInterviewTypeSetupDetail[this.selectedChildDelete].id);
        }*/
       
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    Clear(f: NgForm) {
        f.resetForm();
    }
    
    selectAllChild(){
        console.log(this.selectAll.select);
        this.model.dtoInterviewTypeSetupDetail.forEach(element => {
            element.selected = !this.selectAll.select;
        });
    }
    selectParent(){
        let count = 0;
        this.model.dtoInterviewTypeSetupDetail.forEach(element => {
            if(element.selected == true){
                count = count + 1;
            }
        });
        if(count == this.model.dtoInterviewTypeSetupDetail.length){
            this.selectAll.select = true;
        }else {
            this.selectAll.select = false;
        }
    }
    edit(row: InterviewType) {
        this.isUnderUpdate = true;
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.interviewTypeId = true;
        this.interviewTypeId = row.interviewTypeId;
        this.interviewTypeIdvalue = this.model.interviewTypeId;
        this.model.dtoInterviewTypeSetupDetail = [];

        row.dtoInterviewTypeSetupDetail.forEach((element, index) => {
            let subItemsObj = {
                id:element.id,
                interviewTypeSetupDetailCategoryWeight:element.interviewTypeSetupDetailCategoryWeight,
                interviewTypeSetupDetailCategoryRowSequance:element.interviewTypeSetupDetailCategoryRowSequance,
                interviewTypeSetupDetailDescription:element.interviewTypeSetupDetailDescription,
                interviewTypeSetupDetailCategorySequance:element.interviewTypeSetupDetailCategoryRowSequance,
                selected:false,
                tempid:"temp"+index
            };
            this.model.dtoInterviewTypeSetupDetail.push(Object.assign({}, subItemsObj));
        });
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }
  
    checkNo(){
        // debugger;
        // if(this.model.interviewRange+''!=""){
        //     if(this.model.interviewRange<=10 && this.model.interviewRange>0){
        //         this.model.interviewRange=this.model.interviewRange;
        //     }else{
        //         this.model.interviewRange=1;
        //     }
        // }
           
    }
}
