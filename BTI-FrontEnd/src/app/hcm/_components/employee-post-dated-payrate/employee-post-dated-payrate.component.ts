import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {AlertService} from '../../../_sharedresource/_services/alert.service';
import {Constants} from '../../../_sharedresource/Constants';
import {EmployeePostDatedPayrateModule} from '../../_models/employee-post-dated-payrate/employee-post-dated-payrate.module';
import {Page} from '../../../_sharedresource/page';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';
import {EmployeePostDatedPayrateService} from '../../_services/employee-post-dated-payrate/employee-post-dated-payrate.service';
import {GetScreenDetailService} from '../../../_sharedresource/_services/get-screen-detail.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-employee-post-dated-payrate',
  templateUrl: './employee-post-dated-payrate.component.html',
  providers : [EmployeePostDatedPayrateService, CommonService, DatePipe]
})
export class EmployeePostDatedPayrateComponent implements OnInit {

    page = new Page();
    rows = new Array<EmployeePostDatedPayrateModule>();
    temp = new Array<EmployeePostDatedPayrateModule>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1450';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = {'type': '', 'text': ''};
    superVisionCode = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: EmployeePostDatedPayrateModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    getEmployee:any[] = [];
    getPayCode:any[] = [];
    employeeIdList: Observable<any>;
    paycodeIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    employeeMaster1:object;
    payCodeMaster1:object;
    tempemployeeId:string;
    employeeDesc: string;
    payCodeDesc: string;
    startDateModel;
    effDate;
    tempp:string[]=[];
    isValidNewPayRate: boolean = true;
    isEmployeeIdExist: boolean = true;
    isBenefitIdExist: boolean = true;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    EMPLOYEE_POST_DATE_SEARCH: any;
    EMPLOYEE_POST_DATE_EMP_ID: any;
    EMPLOYEE_POST_DATE_NAME: any;
    EMPLOYEE_POST_DATE_PAYCODE_ID: any;
    EMPLOYEE_POST_DATE_DESCRIPTION: any;
    EMPLOYEE_POST_DATE_CURRENT_RATE: any;
    EMPLOYEE_POST_DATE_ACTION: any;
    EMPLOYEE_POST_DATE_CREATE_LABEL: any;
    EMPLOYEE_POST_DATE_SAVE_LABEL: any;
    EMPLOYEE_POST_DATE_CLEAR_LABEL: any;
    EMPLOYEE_POST_DATE_CANCEL_LABEL: any;
    EMPLOYEE_POST_DATE_UPDATE_LABEL: any;
    EMPLOYEE_POST_DATE_DELETE_LABEL: any;
    EMPLOYEE_POST_DATE_CREATE_FORM_LABEL: any;
    EMPLOYEE_POST_DATE_UPDATE_FORM_LABEL: any;
    EMPLOYEE_POST_DATE_NEW_RATE: any;
    EMPLOYEE_POST_DATE_EFFECTIVE: any;
    EMPLOYEE_POST_DATE_REASON: any;

    constructor(private router: Router,
                private supervisorService: EmployeePostDatedPayrateService,
                private getScreenDetailService: GetScreenDetailService,
                private alertService: AlertService,
                private commonService: CommonService,
                private datePipe : DatePipe) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            {'fieldName': 'EMPLOYEE_POST_DATE_SEARCH', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_EMP_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_NAME', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_PAYCODE_ID', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_DESCRIPTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_CURRENT_RATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_ACTION', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_NEW_RATE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_EFFECTIVE', 'fieldValue': '', 'helpMessage': ''},
            {'fieldName': 'EMPLOYEE_POST_DATE_REASON', 'fieldValue': '', 'helpMessage': ''}
        ];

        this.EMPLOYEE_POST_DATE_SEARCH = this.defaultFormValues[0];
        this.EMPLOYEE_POST_DATE_EMP_ID = this.defaultFormValues[1];
        this.EMPLOYEE_POST_DATE_NAME = this.defaultFormValues[2];
        this.EMPLOYEE_POST_DATE_PAYCODE_ID = this.defaultFormValues[3];
        this.EMPLOYEE_POST_DATE_DESCRIPTION = this.defaultFormValues[4];
        this.EMPLOYEE_POST_DATE_CURRENT_RATE = this.defaultFormValues[5];
        this.EMPLOYEE_POST_DATE_ACTION = this.defaultFormValues[6];
        this.EMPLOYEE_POST_DATE_CREATE_LABEL = this.defaultFormValues[7];
        this.EMPLOYEE_POST_DATE_SAVE_LABEL = this.defaultFormValues[8];
        this.EMPLOYEE_POST_DATE_CLEAR_LABEL = this.defaultFormValues[9];
        this.EMPLOYEE_POST_DATE_CANCEL_LABEL = this.defaultFormValues[10];
        this.EMPLOYEE_POST_DATE_UPDATE_LABEL = this.defaultFormValues[11];
        this.EMPLOYEE_POST_DATE_DELETE_LABEL = this.defaultFormValues[12];
        this.EMPLOYEE_POST_DATE_CREATE_FORM_LABEL = this.defaultFormValues[13];
        this.EMPLOYEE_POST_DATE_UPDATE_FORM_LABEL = this.defaultFormValues[14];
        this.EMPLOYEE_POST_DATE_NEW_RATE = this.defaultFormValues[15];
        this.EMPLOYEE_POST_DATE_EFFECTIVE = this.defaultFormValues[16];
        this.EMPLOYEE_POST_DATE_REASON = this.defaultFormValues[17];

        this.paycodeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.payCodeMaster1['payCodeId']);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

        this.employeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.employeeMaster1['employeeId']);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.supervisorService.getPayTypeDropdown().then(data => {
            this.getPayCode = data.result.records;
            //console.log("Data class options : "+this.getPayCode);
        });
        this.supervisorService.getEmployee().then(data => {
            this.getEmployee = data.result;
            //console.log("Data class options : "+this.getEmployee);
        });

    }
    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'employeeId') {
            return row['employeeMasterHcm'] && row['employeeMasterHcm']['employeeId'] ? row['employeeMasterHcm']['employeeId'] : '';
        } else if (gridFieldName == 'employeeFirstName') {
            return row['employeeMasterHcm'] && row['employeeMasterHcm']['employeeFirstName'] ? row['employeeMasterHcm']['employeeFirstName'] : '';
        } else if (gridFieldName == 'payCodeId') {
            return row['payCode'] && row['payCode']['payCodeId'] ? row['payCode']['payCodeId'] : '';
        } else if (gridFieldName == 'description') {
            return row['payCode'] && row['payCode']['description'] ? row['payCode']['description'] : '';
        } else if (gridFieldName === 'effectiveDate') {
            return row[gridFieldName] ? this.datePipe.transform(new Date(row[gridFieldName]), 'dd-MM-yyyy'): '';
        } else {
            return row[gridFieldName];
        }
    }

    getEmployeeIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployee.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }
    getSuperviserIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getPayCode.filter((id: any) => {
                return query.test(id.payCodeId);
            })
        );
    }


    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        //this.model.employeeName = '';
       //console.log(e);
    }

    typeaheadOnSelect1(e: TypeaheadMatch): void {
        this.employeeMaster1['employeeId'] = e.value;
        this.isEmployeeIdExist = true;
        for(var i=0;i<this.getEmployee.length;i++){
            if(this.getEmployee[i].employeeId == e.value){
                this.employeeDesc = this.getEmployee[i].employeeFirstName;
                this.employeeMaster1['employeeIndexId'] = this.getEmployee[i].employeeIndexId;
                break;
            } else{
                this.employeeDesc = '';
            }
        }
       //console.log('Selected value: ', e.value);
        //this.model.benefitsId = e.value;
    }

    typeaheadOnSelect2(e: TypeaheadMatch): void {
        this.payCodeMaster1['payCodeId'] = e.value;
        this.isBenefitIdExist = true;
        for(var i=0;i<this.getPayCode.length;i++){
            if(this.getPayCode[i].payCodeId == e.value){
                this.payCodeDesc = this.getPayCode[i].description;
                this.model.currentPayRate = this.getPayCode[i].payRate;
                this.payCodeMaster1['id'] = this.getPayCode[i].id;
                break;
            } else{
                this.payCodeDesc = '';
                this.model.currentPayRate = null;
            }
        }
       //console.log('Selected value: ', e.value);
        //this.model.benefitsId = e.value;
    }

    checkValid1(event){
        if(event.target.value == ''){
            this.isEmployeeIdExist = true;
            this.employeeDesc = '';
        } else{
            for(var i=0;i<this.getEmployee.length;i++){
                if(this.getEmployee[i].employeeId.includes(event.target.value)){
                    this.isEmployeeIdExist = true;
                    this.employeeDesc = this.getEmployee[i].employeeFirstName;
                    this.employeeMaster1['employeeIndexId'] = this.getEmployee[i].employeeIndexId;
                    break;
                } else{
                    this.employeeDesc = '';
                    this.isEmployeeIdExist = false;
                }
            }


        }
    }

    checkValid2(event){
        if(event.target.value == ''){
            this.isBenefitIdExist = true;
            this.model.currentPayRate = null;
            this.payCodeDesc = '';
        } else{
            for(var i=0;i<this.getPayCode.length;i++){
                if(this.getPayCode[i].payCodeId.includes(event.target.value)){
                    this.isBenefitIdExist = true;
                    this.payCodeDesc = this.getPayCode[i].description;
                    this.model.currentPayRate = this.getPayCode[i].payRate;
                    this.payCodeMaster1['id'] = this.getPayCode[i].id;
                    break;
                } else{
                    this.payCodeDesc = '';
                    this.model.currentPayRate = null;
                    this.isBenefitIdExist = false;
                }
            }
        }
    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.supervisorService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Print Screen
    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto,scale=50');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
              <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
              <style>
              @media print{
                .doNotPrint{display:none;!important}
              }
              @page { size: landscape; 
                 margin-top: 25px;
                margin-bottom: 250px;
                margin-right: 0px;
                margin-left: 0px;
                -webkit-transform: scale(0.5);  /* Saf3.1+, Chrome */
                -moz-transform: scale(0.5);  /* FF3.5+ */
                -ms-transform: scale(0.5);  /* IE9 */
                -o-transform: scale(0.5);  /* Opera 10.5+ */
                transform: scale(0.5);  }
            
              
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }
    printEmployeeDetails(){
        this.noPrintTable = true;
        this.printDetails = false;
        
        setTimeout(()=> { window.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint(){
        this.noPrintTable = true;
        this.printDetails = true;
    }
    // Open form for create location
    Create() {
        this.startDateModel = null;
        this.showCreateForm = false;
        this.isUnderUpdate=false;
        this.employeeDesc='';
        this.payCodeDesc='';
        this.isEmployeeIdExist = true;
        this.isBenefitIdExist = true;
        this.isValidNewPayRate = true;
        this.employeeMaster1 = {
            employeeId : '',
            employeeIndexId : 0
        };
        this.payCodeMaster1 = {
            id : 0,
            payCodeId : ''
        }
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            employeeMaster: {
                employeeId : '',
                employeeIndexId : 0
            },
            payCode: {
                id : 0,
                payCodeId : ''
            },
            currentPayRate: null,
            newPayRate: null,
            effectiveDate: null,
            reasonforChange: ''
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
        this.isValidNewPayRate = true;
        this.payCodeDesc = '';
        this.employeeMaster1 = {
            employeeId : '',
            employeeIndexId : 0
        };
        this.payCodeMaster1 = {
            id : 0,
            payCodeId : ''
        }
        if(!this.isUnderUpdate){
            this.employeeDesc = '';
        }
        this.isEmployeeIdExist = true;
        this.isBenefitIdExist = true;
    }


    //function call for creating new location
    CreatePostDated(f: NgForm, event: Event) {
        event.preventDefault();
        this.model.employeeMaster = this.employeeMaster1;
        this.model.payCode = this.payCodeMaster1;
        this.model.effectiveDate = this.effDate;
        var deptIdx = this.model.employeeMaster;

        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.supervisorService.checkDuplicatePostDated(deptIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new department
                    this.supervisorService.createPostDated(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new department
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: EmployeePostDatedPayrateModule) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.startDateModel = this.formatDateFordatePicker(this.model.effectiveDate);
        this.effDate = this.model.effectiveDate;
        this.tempemployeeId = this.model.employeeMaster['employeeId'];
        this.employeeDesc = this.model.employeeMaster['employeeFirstName'];
        this.payCodeDesc = this.model.payCode['description'];
        this.employeeMaster1 = Object.assign( {}, this.model.employeeMaster) ;
        this.payCodeMaster1 = Object.assign( {}, this.model.payCode);
        //this.superVisionCode = row.employeeId['employeeId'];
        this.isUnderUpdate=true;
        //this.superVisionCodevalue = this.model.employeeId['employeeId'];
        this.isEmployeeIdExist = true;
        this.isBenefitIdExist = true;
        this.isValidNewPayRate = true;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }

    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
       // this.model.employeeId['employeeId'] = this.superVisionCodevalue;
        this.supervisorService.updatePostDated(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin !';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedSupervisors = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSupervisors.push(this.selected[i].id);
        }
        this.supervisorService.deleteSupervisor(selectedSupervisors).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message ;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({selected}) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.supervisorService.searchSupervisorlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    /*confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }*/

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    onEndDateChanged(event: IMyDateModel): void {
        this.effDate = event.jsdate;
    }

    checkNewPayRate(digit)
    {
       //console.log(digit);
        if(digit==null || digit==''){
            this.isValidNewPayRate = true;
            return;
        }
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
           
            this.isValidNewPayRate = false;
           //console.log(this.isValidNewPayRate);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
           
            this.isValidNewPayRate = false;
           //console.log(this.isValidNewPayRate);
        }

        else{
            this.isValidNewPayRate = true;
           //console.log(this.isValidNewPayRate);
        }

    }

}
