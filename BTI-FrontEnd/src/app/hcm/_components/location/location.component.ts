import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { Location } from '../../_models/location/location.module';
import { LocationService } from '../../_services/location/location.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'location',
    templateUrl: './location.component.html',
    providers: [LocationService, CommonService]
})
export class LocationComponent {
    page = new Page();
    rows = new Array<Location>();
    temp = new Array<Location>();
    selected = [];
    countries = [];
    states = [];
    cities = [];
    modelState = '';
    moduleCode = 'M-1011';
    screenCode = 'S-1312';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    locationId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: Location;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    country: number;
    state: number;
    city: number;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    locationIdvalue: any;
    phonePattern = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    locationIdList: Observable<any>;
    getLocationId: any[] = [];
    typeaheadLoading: boolean;

    LOCATION_ID: any;
    LOCATION_DESCRIPTION: any;
    LOCATION_ARABIC_DESCRIPTION: any;
    LOCATION_CONTACT_NAME: any;
    LOCATION_ADDRESS: any;
    LOCATION_CITY: any;
    LOCATION_COUNTRY: any;
    LOCATION_PHONE: any;
    LOCATION_FAX: any;
    LOCATION_CREATE_LABEL: any;
    LOCATION_ACTION: any;
    LOCATION_SAVE_LABEL: any;
    LOCATION_CLEAR_LABEL: any;
    LOCATION_CANCEL_LABEL: any;
    LOCATION_UPDATE_LABEL: any;
    LOCATION_DELETE_LABEL: any;
    LOCATION_CREATE_FORM_LABEL: any;
    LOCATION_SEARCH: any;
    LOCATION_STATE: any;
    LOCATION_UPDATE_FORM_LABEL: any;
    LOCATION_TABLEVIEW: any;

    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private locationService: LocationService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_CONTACT_NAME', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_CITY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_PHONE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_FAX', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_STATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'LOCATION_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];
        this.LOCATION_ID = this.defaultFormValues[0];
        this.LOCATION_DESCRIPTION = this.defaultFormValues[1];
        this.LOCATION_ARABIC_DESCRIPTION = this.defaultFormValues[2];
        this.LOCATION_CONTACT_NAME = this.defaultFormValues[3];
        this.LOCATION_ADDRESS = this.defaultFormValues[4];
        this.LOCATION_CITY = this.defaultFormValues[5];
        this.LOCATION_COUNTRY = this.defaultFormValues[6];
        this.LOCATION_PHONE = this.defaultFormValues[7];
        this.LOCATION_FAX = this.defaultFormValues[8];
        this.LOCATION_CREATE_LABEL = this.defaultFormValues[9];
        this.LOCATION_ACTION = this.defaultFormValues[10];
        this.LOCATION_SAVE_LABEL = this.defaultFormValues[11];
        this.LOCATION_CLEAR_LABEL = this.defaultFormValues[12];
        this.LOCATION_CANCEL_LABEL = this.defaultFormValues[13];
        this.LOCATION_UPDATE_LABEL = this.defaultFormValues[14];
        this.LOCATION_DELETE_LABEL = this.defaultFormValues[15];
        this.LOCATION_CREATE_FORM_LABEL = this.defaultFormValues[16];
        this.LOCATION_SEARCH = this.defaultFormValues[17];
        this.LOCATION_STATE = this.defaultFormValues[18];
        this.LOCATION_UPDATE_FORM_LABEL = this.defaultFormValues[19];
        this.LOCATION_TABLEVIEW = this.defaultFormValues[20]
        this.locationIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.locationId);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.locationService.getCountry().then(data => {
            this.countries = data.result;
        });
        // getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


        // Following shifted from SetPage for better performance
        this.locationService.getLocationId().then(data => {
            this.getLocationId = data.result.records;
        });
    }

    getSuperviserIdAsObservable(token: string): Observable<any> {
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getLocationId.filter((id: any) => {
                return query.test(id.locationId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        // Following shifted from SetPage for better performance
        this.locationService.getLocation(e.item.id).then(allData => {
            console.log('allData', allData)
            this.locationService.getStatesByCountryId(allData.result.countryId).then(data => {
                this.states = data.result;
                // console.log(data.result);
                this.locationService.getCitiesByStateId(allData.result.stateId).then(data => {
                    this.cities = data.result;
                    // console.log(data.result);
                    this.model = allData.result;
                    this.model.id = 0;
                    // console.log('this.model', this.model)
                });
            });
        });
    }

    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.locationService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            locationId: '',
            description: '',
            arabicDescription: '',
            contactName: '',
            locationAddress: '',
            cityName: '',
            countryName: '',
            stateName: '',
            cityId: null,
            countryId: null,
            stateId: null,
            phone: '',
            fax: ''
        };
        this.locationService.getCountry().then(data => {
            this.countries = data.result;
            console.log(data.result);
        });
        this.countries.forEach(ele => {
            if (ele.defaultCountry === 1) {
                this.model.countryId = ele.countryId;
            }
        })
        console.log('counytyid', this.model.countryId);
        this.locationService.getStatesByCountryId(this.model.countryId).then(data => {
            this.states = data.result;
            this.model.stateId = this.states[0].stateId;

            this.locationService.getCitiesByStateId(this.model.stateId).then(data => {
                this.cities = data.result;
                this.model.cityId = this.cities[0].cityId;
            });
        });
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ country: 0, state: 0, city: 0 });
    }


    // function call for creating new location
    CreateLocation(f: NgForm, event: Event) {
        event.preventDefault();
        var locIdx = this.model.locationId;

        // Check if the id is available in the model.
        // If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Check for duplicate Location Id according to it create new location
            this.locationService.checkDuplicateLocationId(locIdx).then(response => {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    // Call service api for Creating new location
                    this.locationService.createLocation(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.hasMessage = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();

                            this.states = [];
                            this.cities = [];
                            // Refresh the Grid data after adding new location
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });

        }
    }

    // edit department by row
    edit(row: Location) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.locationId = row.locationId;
        this.isUnderUpdate = true;
        this.locationIdvalue = this.model.locationId;
        this.locationService.getStatesByCountryId(row.countryId).then(data => {
            this.states = data.result;
        });
        this.locationService.getCitiesByStateId(row.stateId).then(data => {
            this.cities = data.result;
        });

        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        this.model.locationId = this.locationIdvalue;
        // Call service api for updating selected department

        // Call service api for Creating new location
        this.locationService.updateLocation(this.model).then(data => {
            var datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.states = [];
                this.cities = [];
                // Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
        /*}*/
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    // delete department by passing whole object of perticular Department
    delete() {
        var selectedLocations = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedLocations.push(this.selected[i].id);
        }
        this.locationService.deleteLocation(selectedLocations).then(data => {
            var datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    onCountrySelect(event) {
        this.model.stateId = 0;
        this.model.cityId = 0;
        this.locationService.getStatesByCountryId(event.target.value).then(data => {
            this.states = data.result;
        });
        this.cities = [];
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.locationService.searchLocationlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    onStateSelect(event) {
        this.model.cityId = 0;
        this.locationService.getCitiesByStateId(event.target.value).then(data => {
            this.cities = data.result;
        });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.locationId = '0';
            return false;
        }
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn === val) {
            if (this.page.sortBy === 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}
