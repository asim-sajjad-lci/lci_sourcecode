import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Router } from '@angular/router';
import { EmployeeDirectDeposit } from '../../_models/employee-direct-deposit/employee-direct-deposit';
import { EmployeeDirectDepositService } from '../../_services/employee-direct-deposit/employee-direct-deposit.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DatePickerModule } from 'ng2-datepicker-bootstrap';
import { SupervisorService } from '../../_services/supervisor/supervisor.service';
import { Page } from '../../../_sharedresource/page';
import {CommonService} from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'employee-direct-deposit',
    templateUrl: './employee-direct-deposit.component.html',
    providers: [
        EmployeeDirectDepositService,
        SupervisorService,
        CommonService,
    ]
})
export class EmployeeDirectDepositComponent {

    page = new Page();
    rows = new Array<EmployeeDirectDeposit>();
    temp = new Array<EmployeeDirectDeposit>();
    selected = [];
    selectedListEmployeeDirectDeposit = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1455';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    supervisorId = {};
    model: EmployeeDirectDeposit;
    searchKeyword = '';
    ddPageSize = 5;
    showCreateForm: boolean = false;
    isUpdateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    isDeleteActionBank: boolean = false;
    employeeIdList: Observable<any>;
    getEmployee: any[] = [];
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    bankDetails: any[] = [];
    employeeId;
    gridLists;
    getScreenDetailArr;
    isConfigure = false;
    isShowHideGrid = false;
    NationalityDescription;
    listEmployeeDirectDeposit = [];
    employeeIndexId;
    EmployeeName;
    tempp: string[] = [];
    islifeTimeValidEmployeepay: boolean = true;
    islifeTimeValidEmployeepercent: boolean = true;
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder: boolean = false;

    EMPLOYEE_ID: any;
    NAME: any;
    LINE: any;
    BANK_NAME: any;
    ACCOUNT_NUMBER: any;
    CHK_SVG: any;
    AMOUNT: any;
    PERCENT: any;
    STATUS: any;
    MOVE_UP: any;
    MOVE_DOWN: any;
    REMOVE_BANK: any;
    DELETE: any;
    SAVE: any;
    PRINT: any;
    CLEAR: any;
    CREATE: any;
    UPDATE: any;
    ACTION: any;
    ADDNEW: any;
    SEARCH: any;
    CANCEL: any;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(private router: Router,
        private employeeDirectDepositService: EmployeeDirectDepositService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private supervisorService: SupervisorService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'LINE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'BANK_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CHK_SVG', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PERCENT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'STATUS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MOVE_UP', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MOVE_DOWN', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'REMOVE_BANK', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'PRINT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ADDNEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '' },
        ];

        this.EMPLOYEE_ID = this.defaultFormValues[0];
        this.NAME = this.defaultFormValues[1];
        this.LINE = this.defaultFormValues[2];
        this.BANK_NAME = this.defaultFormValues[3];
        this.ACCOUNT_NUMBER = this.defaultFormValues[4];
        this.CHK_SVG = this.defaultFormValues[5];
        this.AMOUNT = this.defaultFormValues[6];
        this.PERCENT = this.defaultFormValues[7];
        this.STATUS = this.defaultFormValues[8];
        this.MOVE_UP = this.defaultFormValues[9];
        this.MOVE_DOWN = this.defaultFormValues[10];
        this.REMOVE_BANK = this.defaultFormValues[11];
        this.DELETE = this.defaultFormValues[12];
        this.SAVE = this.defaultFormValues[13];
        this.PRINT = this.defaultFormValues[14];
        this.CLEAR = this.defaultFormValues[15];
        this.CREATE = this.defaultFormValues[16];
        this.UPDATE = this.defaultFormValues[17];
        this.ACTION = this.defaultFormValues[18];
        this.ADDNEW = this.defaultFormValues[19];
        this.SEARCH = this.defaultFormValues[20];
        this.CANCEL = this.defaultFormValues[21];

        this.employeeIdList = Observable.create((observer: any) => {
            observer.next(this.employeeIndexId);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
        this.supervisorService.getEmployee().then(data => {
            this.getEmployee = data.result;
            console.log('this.getEmployee');
            console.log(this.getEmployee);
        });

    }
    getIndex(event) {
        console.log(event);
        return 0;
    }

    addNewCompanyDetails() {

        this.listEmployeeDirectDeposit.push(
            {
                bankName: '',
                accountNumber: null,
                amount: null,
                percent: null,
                status: 0,
                accountSequence: 0,
                employeeMaster: {
                    employeeIndexId: this.employeeId
                }
            }
        );
        console.log(this.listEmployeeDirectDeposit);
    }

    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.islifeTimeValidEmployeepercent = true;
        this.islifeTimeValidEmployeepay = true;
        this.listEmployeeDirectDeposit = [];
        this.addNewCompanyDetails();
        this.selectedListEmployeeDirectDeposit = [];
        setTimeout(() => {
            this.showCreateForm = true;
            this.doNotPrintHearder = false;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.employeeIndexId = '';
        this.EmployeeName = '';
        this.isUpdateForm = false;
        this.model = {
            id: 0,
            listEmployeeDirectDeposit: []
        };
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        this.page.searchKeyword = '';
        this.employeeDirectDepositService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }
    // default list on page
    onSelectListEmployeeDirectDeposit({ selected }) {
        this.selectedListEmployeeDirectDeposit.splice(0, this.selectedListEmployeeDirectDeposit.length);
        this.selectedListEmployeeDirectDeposit.push(...selected);
    }

    // search department by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'employeeId') {
            return row['employeeMaster'] && row['employeeMaster']['employeeId'] ? row['employeeMaster']['employeeId'] : '';
        } else if (gridFieldName === 'employeeFirstName') {
            return row['employeeMaster'] && row['employeeMaster']['employeeFirstName'] ? row['employeeMaster']['employeeFirstName'] : '';
        } else if (gridFieldName === 'bankName') {
            let eid = '';
            for (let i = 0; i < row['listEmployeeDirectDeposit'].length; i++) {
                eid = eid + row['listEmployeeDirectDeposit'][i]['bankName'] + ',';
            }
            return eid;
        } else if (gridFieldName === 'accountNumber') {
            let eid = '';
            for (let i = 0; i < row['listEmployeeDirectDeposit'].length; i++) {
                eid = eid + row['listEmployeeDirectDeposit'][i]['accountNumber'] + ',';
            }
            return eid;
        } else if (gridFieldName === 'amount') {
            let eid = '';
            for (let i = 0; i < row['listEmployeeDirectDeposit'].length; i++) {
                eid = eid + row['listEmployeeDirectDeposit'][i]['amount'] + ',';
            }
            return eid;
        } else if (gridFieldName === 'percent') {
            let eid = '';
            for (let i = 0; i < row['listEmployeeDirectDeposit'].length; i++) {
                eid = eid + row['listEmployeeDirectDeposit'][i]['percent'] + ',';
            }
            return eid;
        } else if (gridFieldName === 'status') {
            let eid = '';
            for (let i = 0; i < row['listEmployeeDirectDeposit'].length; i++) {
                eid = eid + ( row['listEmployeeDirectDeposit'][i]['status'] === 1 ? 'true' : 'false' ) + ',';
            }
            return eid;
        } else if (gridFieldName === 'accountSequence') {
            let eid = '';
            for (let i = 0; i < row['listEmployeeDirectDeposit'].length; i++) {
                eid = eid + ( row['listEmployeeDirectDeposit'][i]['accountSequence']  === 1 ? 'checking' : 'savings' ) + ',';
            }
            return eid;
        } else {
            return row[gridFieldName];
        }
    }

    getEmployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        this.EmployeeName = "";
        return Observable.of(
            this.getEmployee.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        console.log(e);
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        console.log('Selected value: ', e.item);
        if (typeof (e.item.employeeFirstName) != 'undefined')
            this.employeeId = e.item.employeeIndexId;
        this.EmployeeName = e.item.employeeFirstName + ' ' + e.item.employeeMiddleName + ' ' + e.item.employeeLastName;
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        // this.employeeIndexId = '';
        // this.EmployeeName = '';
        this.listEmployeeDirectDeposit = [];
        f.resetForm();
        this.islifeTimeValidEmployeepercent = true;
        this.islifeTimeValidEmployeepay = true;
        this.doNotPrintHearder = false;
    }

    //function call for creating new location
    CreateEmployeeDirectDeposit(f: NgForm, event: Event) {
        console.log(this.model);
        this.doNotPrintHearder = false;

        console.log(this.islifeTimeValidEmployeepay)
        console.log(this.islifeTimeValidEmployeepercent)

        if (!this.islifeTimeValidEmployeepay || !this.islifeTimeValidEmployeepercent) {
            return false;
        }

        event.preventDefault();
        var supIdx = this.model.id;
        this.model.listEmployeeDirectDeposit = [];
        for (let i = 0; i < this.listEmployeeDirectDeposit.length; i++) {
            const element = this.listEmployeeDirectDeposit[i];
            this.model.listEmployeeDirectDeposit.push({
                "bankName": element.bankName,
                "accountNumber": element.accountNumber,
                "amount": parseFloat(element.amount),
                "percent": parseFloat(element.percent),
                "status": element.status,
                "accountSequence": element.accountSequence,
                "employeeMaster": {
                    "employeeIndexId": this.employeeId
                }
            });
        }

        if (!this.employeeId) {
            return false;
        }

        if (this.model.listEmployeeDirectDeposit.length == 0) {
            this.hasMsg = true;
            window.scrollTo(0, 0);
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = 'Please add atleast one bank detail.';
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
                this.duplicateWarning = false;
            }, 4000);

            return false;
        } else {
            console.log(this.model);
            if (this.isUpdateForm) {
                //Check if the id is available in the model.
                //If id avalable then update the location, else Add new location.

                this.isConfirmationModalOpen = true;
                this.isDeleteAction = false;
                this.isDeleteActionBank = false;
            }
            else {
                //Check for duplicate employeeIdcheck Id according to it create
                this.employeeDirectDepositService.employeeIdcheck(this.employeeId).then(res => {
                    if (res && res.result && res.result.isRepeat) {
                        this.duplicateWarning = true;
                        this.message.type = "success";

                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.hasMessage = false;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.duplicateWarning = false;
                            }, 4000);
                            this.message.text = res.btiMessage.message;
                        }, 100);
                    } else {
                        this.employeeDirectDepositService.createEmployeeDirectDeposit(this.model).then(data => {
                            var datacode = data.code;

                            if (datacode == 400) {
                                this.duplicateWarning = true;
                                this.message.type = "success";

                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.hasMessage = false;
                                    this.showMsg = true;
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.duplicateWarning = false;
                                    }, 4000);
                                    this.message.text = data.btiMessage.message;
                                }, 100);
                                return;
                            }

                            if (datacode == 201) {
                                window.scrollTo(0, 0);
                                window.setTimeout(() => {
                                    this.isSuccessMsg = true;
                                    this.isfailureMsg = false;
                                    this.showMsg = true;
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.hasMsg = false;
                                    }, 4000);
                                    this.showCreateForm = false;
                                    this.messageText = data.btiMessage.message;
                                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                                    ;
                                }, 100);
                                this.hasMsg = true;
                                f.resetForm();
                            }
                        }).catch(error => {
                            this.hasMsg = true;
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.messageText = 'Server error. Please contact admin !';
                            }, 100)
                        });
                    }

                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = "Server error. Please contact admin.";
                    }, 100);
                });

            }
        }



    }


    //edit department by row
    edit(row: EmployeeDirectDeposit) {
        console.log(row);

        this.showCreateForm = true;
        this.isUpdateForm = true;
        this.doNotPrintHearder = false;
        this.model = Object.assign({}, row);
        if (row.listEmployeeDirectDeposit && row.listEmployeeDirectDeposit.length) {
            this.listEmployeeDirectDeposit = row.listEmployeeDirectDeposit;
        } else {
            this.listEmployeeDirectDeposit = [];
        }

        if (typeof (row['employeeMaster']['employeeFirstName']) != 'undefined') {
            this.employeeIndexId = row['employeeMaster'].employeeId;
            this.employeeId = row['employeeMaster'].employeeIndexId;
            this.EmployeeName = row['employeeMaster'].employeeFirstName + ' ' + row['employeeMaster'].employeeMiddleName + ' ' + row['employeeMaster'].employeeLastName;

        }

        this.isUnderUpdate = true;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
        this.islifeTimeValidEmployeepercent = true;
        this.islifeTimeValidEmployeepay = true;
    }

    updateStatus() {
        this.closeModal();
        this.doNotPrintHearder = false;
        //Call service api for updating selected department
        //  this.model.id = this.employeeIndexId;
        this.employeeDirectDepositService.updateEmployeeDirectDeposit(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 400) {
                this.duplicateWarning = true;
                this.message.type = "success";

                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.hasMessage = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);
                return;
            }
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                    this.isUpdateForm = false;
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=100%,scale=100');
        popupWin.document.open();
        popupWin.document.write(`
          <html>
            <head>
              <title>Print tab</title>
              <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
              <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
              <style>
              @media print{
                .doNotPrint{display:none;!important}
              }
              @page { size: landscape; 
                 margin-top: 25px;
                margin-bottom: 250px;
                margin-right: 0px;
                margin-left: 0px;
                -webkit-transform: scale(0.5);  /* Saf3.1+, Chrome */
                -moz-transform: scale(0.5);  /* FF3.5+ */
                -ms-transform: scale(0.5);  /* IE9 */
                -o-transform: scale(0.5);  /* Opera 10.5+ */
                transform: scale(0.5);  }
            
              
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
    }

    printEmployeeDetails() {
        this.noPrintTable = true;
        this.printDetails = false;
        this.doNotPrintHearder = true;

        setTimeout(() => { this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint() {
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }



    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isDeleteActionBank = false;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }
    varifyDeleteBank() {
        if (this.selectedListEmployeeDirectDeposit.length > 0) {
            this.showCreateForm = true;
            this.isDeleteAction = false;
            this.isDeleteActionBank = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }



    //delete traningCourse by passing whole object of perticular TrainingCourse
    delete() {
        var selectedEmp = [];
        console.log(this.selected);
        for (var i = 0; i < this.selected.length; i++) {
            if (this.selected[i]['employeeMaster']) {
                selectedEmp.push(this.selected[i]['employeeMaster']['employeeIndexId']);
            }

        }
        console.log(selectedEmp);

        this.employeeDirectDepositService.deleteEmployeeDirectDeposit(selectedEmp).then(data => {
            var datacode = data.code;
            if (datacode == 200) {

            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.message.text = data.btiMessage.message;
            }, 100);



        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }
    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isDeleteActionBank = false;
        this.isConfirmationModalOpen = false;
    }

    _keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
            // invalid character, prevent input
            event.preventDefault();
        }
    }

    moveUp() {
        //console.log(this.selectedListEmployeeDirectDeposit[0]['$$index']);
        if (this.selectedListEmployeeDirectDeposit.length == 1 && this.selectedListEmployeeDirectDeposit[0]['$$index'] != 0) {

            const currentIndex = this.selectedListEmployeeDirectDeposit[0]['$$index'];
            const nextIndex = parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) - 1;

            var b = this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']];
            this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']] = this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) - 1];
            this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']]['$$index'] = currentIndex;

            this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) - 1] = b;
            this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) - 1]['$$index'] = nextIndex;

            const NewData = this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index'])];
            this.selectedListEmployeeDirectDeposit = [];
            this.selectedListEmployeeDirectDeposit.push(NewData);


            this.listEmployeeDirectDeposit = [...this.listEmployeeDirectDeposit]
        } else {
            this.selectedListEmployeeDirectDeposit = [];
        }



    }
    moveDown() {
        //  console.log(this.selectedListEmployeeDirectDeposit);
        if (this.selectedListEmployeeDirectDeposit.length == 1 && parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) + 1 != this.listEmployeeDirectDeposit.length) {
            // var b = this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']];
            // this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']] = this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) + 1];
            // this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) + 1] = b;

            const currentIndex = this.selectedListEmployeeDirectDeposit[0]['$$index'];
            const nextIndex = parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) + 1;

            var b = this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']];
            this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']] = this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) + 1];
            this.listEmployeeDirectDeposit[this.selectedListEmployeeDirectDeposit[0]['$$index']]['$$index'] = currentIndex;

            this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) + 1] = b;
            this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index']) + 1]['$$index'] = nextIndex;

            const NewData = this.listEmployeeDirectDeposit[parseInt(this.selectedListEmployeeDirectDeposit[0]['$$index'])];
            this.selectedListEmployeeDirectDeposit = [];
            this.selectedListEmployeeDirectDeposit.push(NewData);


            this.listEmployeeDirectDeposit = [...this.listEmployeeDirectDeposit]
        } else {
            this.selectedListEmployeeDirectDeposit = [];
        }
    }
    deleteBank() {
        this.closeModal();
        if (this.selectedListEmployeeDirectDeposit.length) {

            if (this.selectedListEmployeeDirectDeposit.length == this.listEmployeeDirectDeposit.length) {
                this.listEmployeeDirectDeposit = [];
            } else {
                for (var j = 0; j < this.listEmployeeDirectDeposit.length; j++) {
                    const index = this.listEmployeeDirectDeposit.indexOf(this.selectedListEmployeeDirectDeposit[j]);

                    if (index !== -1) {
                        this.listEmployeeDirectDeposit.splice(index, 1);
                    }
                }

            }

            this.hasMessage = true;
            this.message.type = "success";
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);

                this.message.text = "Selected Banks Details deleted successfully.";
            }, 100);


            this.selectedListEmployeeDirectDeposit = [];
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }

    }

    checkdecimalemployeepay(digit) {
        console.log(digit);
        if (digit == null || digit == '') {
            this.islifeTimeValidEmployeepay = true;
            return false;
        }
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidEmployeepay = false;
            console.log(this.islifeTimeValidEmployeepay);
            return false;
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidEmployeepay = false;
            console.log(this.islifeTimeValidEmployeepay);
            return false;
        }

        else {
            this.islifeTimeValidEmployeepay = true;
            console.log(this.islifeTimeValidEmployeepay);
        }

    }

    checkdecimalemployeepercent(digit) {
        console.log(digit);
        if (digit == null || digit == '') {
            this.islifeTimeValidEmployeepercent = true;
            return;
        }
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
            console.log("in the condition");
            this.islifeTimeValidEmployeepercent = false;
            console.log(this.islifeTimeValidEmployeepercent);
            return;
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 5))) {
            console.log("in the condition");
            this.islifeTimeValidEmployeepercent = false;
            console.log(this.islifeTimeValidEmployeepercent);
            return;
        }

        else {
            this.islifeTimeValidEmployeepercent = true;
            console.log(this.islifeTimeValidEmployeepercent);
        }

    }

    // search department by keyword
    searchFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }
    sortColumn(val) {
        console.log("val", val);
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }


    }
}
