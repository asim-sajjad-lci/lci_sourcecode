"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var page_1 = require("../../../_sharedresource/page");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var router_1 = require("@angular/router");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var position_setup_service_1 = require("../../_services/position-setup/position-setup.service");
var PositionSetupComponent = (function () {
    function PositionSetupComponent(router, positionSetupService, getScreenDetailService, alertService) {
        this.router = router;
        this.positionSetupService = positionSetupService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1313';
        this.message = { 'type': '', 'text': '' };
        this.positionId = {};
        this.positionClassOptions = [];
        this.skillSetIdOptions = [];
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for position  screen
        this.defaultFormValues = [
            { 'fieldName': 'POSITION_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_REPORT_TO_POSITION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_CLASS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_LONG_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_DEFAULT_SKILL_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_TRAINING_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_PLAN_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' }
        ];
    }
    PositionSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            var _loop_1 = function (j) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            };
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                _loop_1(j);
            }
        });
    };
    // setting pagination
    PositionSetupComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.positionSetupService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.positionSetupService.getPositionClassList().then(function (data) {
                _this.positionClassOptions = data.result;
            });
            _this.positionSetupService.getSkillSetList().then(function (data) {
                _this.skillSetIdOptions = data.result;
            });
        });
    };
    // Open form for create position setup
    PositionSetupComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            positionId: '',
            description: '',
            arabicDescription: '',
            reportToPostion: '',
            positionClassId: '',
            skillSetId: '',
            postionLongDesc: ''
        };
    };
    // edit position class by row
    PositionSetupComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.positionId = row.positionId;
        this.isUnderUpdate = true;
        this.positionIdvalue = this.model.positionId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    PositionSetupComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        // Call service api for updating selected position class
        this.model.positionId = this.positionIdvalue;
        this.positionSetupService.updatePositionSetup(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                _this.setPage({ offset: 0 });
                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    };
    PositionSetupComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    PositionSetupComponent.prototype.navToTraining = function () {
        var _this = this;
        console.log("function is called");
        if (this.selected.length === 1) {
            this.router.navigate(['hcm/positionTraining', this.selected[0].positionId,
                this.selected[0].description, this.selected[0].arabicDescription, this.selected[0].id]);
        }
        else if (this.selected.length > 1) {
            //console.log(this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = true;
                    _this.hasMsg = true;
                }, 100);
                _this.messageText = "Please select only one to navigate";
            }, 100);
        }
        else {
            //console.log("down:-->"+this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = true;
                    _this.hasMsg = true;
                }, 100);
                _this.messageText = "Please select atleast one option";
            }, 100);
        }
    };
    PositionSetupComponent.prototype.navToPlanSetup = function () {
        var _this = this;
        if (this.selected.length === 1) {
            this.router.navigate(['hcm/positionPlanSetup', this.selected[0].positionId,
                this.selected[0].id]);
        }
        else if (this.selected.length > 1) {
            //console.log(this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = true;
                    _this.hasMsg = true;
                }, 100);
                _this.messageText = "Please select only one to navigate";
            }, 100);
        }
        else {
            //console.log("down:-->"+this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = true;
                    _this.hasMsg = true;
                }, 100);
                _this.messageText = "Please select atleast one option";
            }, 100);
        }
    };
    PositionSetupComponent.prototype.navToAttachment = function () {
        this.router.navigate(['hcm/positionAttachment', this.model.positionId,
            this.model.description]);
    };
    PositionSetupComponent.prototype.attachMsg = function () {
        var _this = this;
        window.scrollTo(0, 0);
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        window.setTimeout(function () {
            _this.showMsg = true;
            _this.hasMsg = true;
        }, 100);
        this.messageText = "Please add position setup, than you will be allowed to upload an attachment.";
    };
    // delete position class by passing whole object of perticular position Setup
    PositionSetupComponent.prototype.delete = function () {
        var _this = this;
        var selectedPositionSetup = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedPositionSetup.push(this.selected[i].id);
        }
        this.positionSetupService.deletePositionSetup(selectedPositionSetup).then(function (data) {
            var datacode = data.code;
            if (datacode === 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            // this.message.text = data.btiMessage.message;
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.result.messageType;
            }, 100);
            // Refresh the Grid data after deletion of position Setup
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    PositionSetupComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search position class by keyword
    PositionSetupComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.positionSetupService.searchPositionSetuplist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    PositionSetupComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    // Clear form to reset to default blank
    PositionSetupComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    PositionSetupComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    // function call for creating new positionsetup
    PositionSetupComponent.prototype.CreatePositionSetup = function (f, event) {
        var _this = this;
        event.preventDefault();
        var posIdx = this.model.positionId;
        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            // Check for duplicate positionClass Id according to it create new position class
            this.positionSetupService.checkDuplicatePositionSetupId(posIdx).then(function (response) {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    // Call service api for Creating new position class
                    _this.positionSetupService.createPositionSetup(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new position class
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    return PositionSetupComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], PositionSetupComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], PositionSetupComponent.prototype, "myScrollContainer", void 0);
PositionSetupComponent = __decorate([
    core_1.Component({
        selector: 'positionSetup',
        templateUrl: './position-setup.component.html',
        providers: [position_setup_service_1.PositionSetupService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        position_setup_service_1.PositionSetupService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], PositionSetupComponent);
exports.PositionSetupComponent = PositionSetupComponent;
//# sourceMappingURL=position-setup.component.js.map