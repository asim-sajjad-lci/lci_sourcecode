import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { HealthInsSetup } from '../../_models/health-ins-setup/health-ins-setup.module';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { HealthInsSetupService } from '../../_services/health-ins-setup/health-ins-setup.service';
import { NgForm } from '@angular/forms';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { document } from 'ngx-bootstrap/utils/facade/browser';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'app-health-ins-setup',
    templateUrl: './health-ins-setup.component.html',
    providers: [HealthInsSetupService,CommonService],

})
export class HealthInsSetupComponent implements OnInit {

    page = new Page();
    rows = new Array<HealthInsSetup>();
    temp = new Array<HealthInsSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1416';
    moduleName;
    screenName;
    defaultFormValues: Array<any> = [];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    helthInsuranceId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: HealthInsSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    error: any = { isError: false, errorMessage: '' };
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    healthInsValue: string;
    compInsurances: any[] = [];
    healthCovTypes: any[] = [];
    pattern = /^\d*\.?\d{0,3}$/;
    tempp: string[] = [];
    islifeTimeValidEmployeepay: boolean = true;
    islifeTimeValidEmployerpay: boolean = true;
    islifeTimeValidExpenses: boolean = true;
    islifeTimeValidMaxcoverage: boolean = true;
    empeeAmount: number;
    emperAmount: number;
    isEmperAmount: boolean = true;
    maxCov: number;
    isMaxCov: boolean = true;
    addExpEmpMTP: number;
    isAddExpEmpMTP: boolean = true;
    dataSource: Observable<any>;
    healthInsIdSearch = [];
    typeaheadLoading: boolean;
    HEALTHINSSETUP_SEARCH:any;
    HEALTHINSSETUP_ID:any;
    HEALTHINSSETUP_DESCRIPTION:any;
    HEALTHINSSETUP_ARABIC_DESCRIPTION:any;
    HEALTHINSSETUP_COMPANY_INSURANCE:any;
    HEALTHINSSETUP_GROUP_NUMBER:any;
    HEALTHINSSETUP_COVERAGE_TYPE:any;
    HEALTHINSSETUP_FREQUENCY:any;
    HEALTHINSSETUP_MAXIMUM_AGE:any;
    HEALTHINSSETUP_EMPLOYEE_AGE:any;
    HEALTHINSSETUP_DEPARTMENT_AGE:any;
    HEALTHINSSETUP_EMPLOYEE_PAY:any;
    HEALTHINSSETUP_EMPLOYER_PAY:any;
    HEALTHINSSETUP_ADD_EXP:any;
    HEALTHINSSETUP_ACTION:any;
    HEALTHINSSETUP_CREATE_LABEL:any;
    HEALTHINSSETUP_SAVE_LABEL:any;
    HEALTHINSSETUP_CLEAR_LABEL:any;
    HEALTHINSSETUP_UPDATE_LABEL:any;
    HEALTHINSSETUP_DELETE_LABEL:any;
    HEALTHINSSETUP_CREATE_FORM_LABEL:any;
    HEALTHINSSETUP_UPDATE_FORM_LABEL:any;
    HEALTHINSSETUP_CANCEL_LABEL:any;
    HEALTHINSSETUP_COVERAGE_MAXIMUM:any;
    HEALTHINSSETUP_HID:any;
    HEALTHINSSETUP_HEALTH_TYPE_DESC:any;
    HEALTHINSSETUP_CO_INSURANCE:any;
    HEALTHINSSETUP_TYPE_COVERAGE:any;
    HEALTHINSSETUP_TABLEVIEW:any;
    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    healthInsFrequencys: DropDownModule[] = [
        { id: 1, name: 'Weekly' },
        { id: 2, name: 'Biweekly' },
        { id: 3, name: 'Semimonthly' },
        { id: 4, name: 'Monthly' },
        { id: 5, name: 'Quarterly' },
        { id: 6, name: 'Semiannually' },
        { id: 7, name: 'Annually' },
        { id: 8, name: 'Daily/Miscellaneous' },
    ];

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;


    constructor(private router: Router,
        private healthInsSetupService: HealthInsSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'HEALTHINSSETUP_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_COMPANY_INSURANCE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_GROUP_NUMBER', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_COVERAGE_TYPE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_FREQUENCY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_MAXIMUM_AGE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_EMPLOYEE_AGE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_DEPARTMENT_AGE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_EMPLOYEE_PAY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_EMPLOYER_PAY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_ADD_EXP', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_COVERAGE_MAXIMUM', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_HID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_HEALTH_TYPE_DESC', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_CO_INSURANCE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_TYPE_COVERAGE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSSETUP_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];

        this.HEALTHINSSETUP_SEARCH = this.defaultFormValues[0];
        this.HEALTHINSSETUP_ID = this.defaultFormValues[1];
        this.HEALTHINSSETUP_DESCRIPTION = this.defaultFormValues[2];
        this.HEALTHINSSETUP_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.HEALTHINSSETUP_COMPANY_INSURANCE = this.defaultFormValues[4];
        this.HEALTHINSSETUP_GROUP_NUMBER = this.defaultFormValues[5];
        this.HEALTHINSSETUP_COVERAGE_TYPE = this.defaultFormValues[6];
        this.HEALTHINSSETUP_FREQUENCY = this.defaultFormValues[7];
        this.HEALTHINSSETUP_MAXIMUM_AGE = this.defaultFormValues[8];
        this.HEALTHINSSETUP_EMPLOYEE_AGE = this.defaultFormValues[9];
        this.HEALTHINSSETUP_DEPARTMENT_AGE = this.defaultFormValues[10];
        this.HEALTHINSSETUP_EMPLOYEE_PAY = this.defaultFormValues[11];
        this.HEALTHINSSETUP_EMPLOYER_PAY = this.defaultFormValues[12];
        this.HEALTHINSSETUP_ADD_EXP = this.defaultFormValues[13];
        this.HEALTHINSSETUP_ACTION = this.defaultFormValues[14];
        this.HEALTHINSSETUP_CREATE_LABEL = this.defaultFormValues[15];
        this.HEALTHINSSETUP_SAVE_LABEL = this.defaultFormValues[16];
        this.HEALTHINSSETUP_CLEAR_LABEL = this.defaultFormValues[17];
        this.HEALTHINSSETUP_UPDATE_LABEL = this.defaultFormValues[18];
        this.HEALTHINSSETUP_DELETE_LABEL = this.defaultFormValues[19];
        this.HEALTHINSSETUP_CREATE_FORM_LABEL = this.defaultFormValues[20];
        this.HEALTHINSSETUP_UPDATE_FORM_LABEL = this.defaultFormValues[21];
        this.HEALTHINSSETUP_CANCEL_LABEL = this.defaultFormValues[22];
        this.HEALTHINSSETUP_COVERAGE_MAXIMUM = this.defaultFormValues[23];
        this.HEALTHINSSETUP_HID = this.defaultFormValues[24];
        this.HEALTHINSSETUP_HEALTH_TYPE_DESC = this.defaultFormValues[25];
        this.HEALTHINSSETUP_CO_INSURANCE = this.defaultFormValues[26];
        this.HEALTHINSSETUP_TYPE_COVERAGE = this.defaultFormValues[27];
        this.HEALTHINSSETUP_TABLEVIEW = this.defaultFormValues[28];
        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.helthInsuranceId);
        }).mergeMap((token: string) => this.getHealthInsIdAsObservable(token));

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        this.healthInsSetupService.getcompInsurances().then(data => {
            this.compInsurances = data.result.records;
            console.log(this.compInsurances);
        });

        this.healthInsSetupService.getHealthCovType().then(data => {
            this.healthCovTypes = data.result.records;
        });

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.healthInsSetupService.getHealthIdList().then(data => {
            this.healthInsIdSearch = data.result.records;
            console.log("Data class options : " + data.result.records);
        });

    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'frequency') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                        row[gridFieldName] === 7 ? 'Annually' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }
    // Health Ins Id Search:

    getHealthInsIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.healthInsIdSearch.filter((id: any) => {
                return query.test(id.helthInsuranceId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        // console.log('Selected value: ', e);
        this.healthInsSetupService.getHealthInsById(e.item.id).then(pagedData => {
            // console.log('pagedData: ', pagedData);
            this.model = pagedData.result;
            this.model.id = 0;
        });
    }

    //setting pagination
    setPage(pageInfo) {

        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.healthInsSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            console.log('pagedData: ' + pagedData);

        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.islifeTimeValidEmployeepay = true;
        this.islifeTimeValidEmployerpay = true;
        this.islifeTimeValidExpenses = true;
        this.islifeTimeValidMaxcoverage = true;
        this.isEmperAmount = true;
        this.isMaxCov = true;
        this.isAddExpEmpMTP = true;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            helthInsuranceId: '',
            desc: '',
            arbicDesc: '',
            frequency: 0,
            helthCoverageTypeId: 0,
            helthCoverageDesc: '',
            compnayInsuranceId: 0,
            groupNumber: '',
            maxAge: null,
            minAge: null,
            employeePay: null,
            employerPay: null,
            expenses: null,
            maxCoverage: null,
            helthInsurancePrimaryId: 0,
            compnayInsurancePrimaryId: 0
        };
    }


    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ frequency: 0, helthInsurancePrimaryId: 0, compnayInsurancePrimaryId: 0, maxAge: null, minAge: null, employeePay: null, employerPay: null, expenses: null, maxCoverage: null });
        this.islifeTimeValidEmployeepay = true;
        this.islifeTimeValidEmployerpay = true;
        this.islifeTimeValidExpenses = true;
        this.islifeTimeValidMaxcoverage = true;
        this.isEmperAmount = true;
        this.isMaxCov = true;
        this.isAddExpEmpMTP = true;

    }


    //function call for creating new Health Insurance
    CreateHealthIns(f: NgForm, event: Event) {
        event.preventDefault();
        var locId = this.model.helthInsuranceId;



        //Check if the id is available in the model.
        //If id avalable then update the Health Insurance, else Add new Health Insurance.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            //Check for duplicate Health Id according to it create new Benefit Code
            this.healthInsSetupService.checkDuplicatehealthIns(locId).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                }

                else {
                    //Call service api for Creating new Health Insurance
                    this.healthInsSetupService.createHealthIns(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;

                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Health Insurance
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }

    }


    //edit department by row
    edit(row: HealthInsSetup) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.helthInsuranceId = row.helthInsuranceId;
        this.healthInsValue = this.model.helthInsuranceId;
        this.islifeTimeValidEmployeepay = true;
        this.islifeTimeValidEmployerpay = true;
        this.islifeTimeValidExpenses = true;
        this.islifeTimeValidMaxcoverage = true;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        //Call service api for Updating department
        this.model.helthInsuranceId = this.healthInsValue;
        this.healthInsSetupService.updateHealthIns(this.model).then(data => {
            var datacode = data.code;

            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)

        });

    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedInsurances = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedInsurances.push(this.selected[i].id);
        }
        this.healthInsSetupService.deleteHealthIns(selectedInsurances).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;

    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }


    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.helthInsuranceId = '';
            return false;
        }
    }

    onSelectCompIns(event) {
        this.healthInsSetupService.getcompInsurances().then(data => {
            this.compInsurances = data.result.records;
            console.log("DR", data.result);
        })
    }


    keyPress(event: any) {
        const pattern = /^[0-9\-.()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkAgeRange1(event: any) {
        if (event.target.value.length == 1) {
            this.model.maxAge = 18;
        } else {
            if (event.target.value < 18) {
                this.model.maxAge = 18;
            } else if (event.target.value > 75) {
                this.model.maxAge = 75;
            }
        }

    }

    checkAgeRange2(event: any) {
        if (event.target.value.length == 1) {
            this.model.minAge = 18;
        } else {
            if (event.target.value < 18) {
                this.model.minAge = 18;
            } else if (event.target.value > 75) {
                this.model.minAge = 75;
            }
        }
    }

    checkdecimalemployeepay(digit) {
        console.log(digit);
        digit = digit.toString();
        if (digit == null || digit == '') {
            this.islifeTimeValidEmployeepay = true;
            return;
        }
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidEmployeepay = false;
            console.log(this.islifeTimeValidEmployeepay);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidEmployeepay = false;
            console.log(this.islifeTimeValidEmployeepay);
        }

        else {
            this.islifeTimeValidEmployeepay = true;
            console.log(this.islifeTimeValidEmployeepay);
        }

    }


    checkdecimalemployerpay(digit) {
        console.log(digit);
        digit = digit.toString();
        if (digit == null || digit == '') {
            this.islifeTimeValidEmployerpay = true;
            return;
        }
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidEmployerpay = false;
            this.isEmperAmount = true;
            console.log(this.islifeTimeValidEmployerpay);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidEmployerpay = false;
            this.isEmperAmount = true;
            console.log(this.islifeTimeValidEmployerpay);
        }

        else {
            this.islifeTimeValidEmployerpay = true;
            console.log(this.islifeTimeValidEmployerpay);
        }

    }

    checkdecimalexpenses(digit) {
        console.log(digit);
        digit = digit.toString();
        if (digit == null || digit == '') {
            this.islifeTimeValidExpenses = true;
            return;
        }
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidExpenses = false;
            this.isAddExpEmpMTP = true;
            console.log(this.islifeTimeValidExpenses);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidExpenses = false;
            this.isAddExpEmpMTP = true;
            console.log(this.islifeTimeValidExpenses);
        }

        else {
            this.islifeTimeValidExpenses = true;
            console.log(this.islifeTimeValidExpenses);
        }

    }

    checkdecimalcoverage(digit) {
        console.log(digit);
        digit = digit.toString();
        if (digit == null || digit == '') {
            this.islifeTimeValidMaxcoverage = true;
            return;
        }
        this.tempp = digit.split(".");
        if (this.tempp != null && this.tempp[1] != null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidMaxcoverage = false;
            this.isMaxCov = true;
            console.log(this.islifeTimeValidMaxcoverage);
        }
        else if (this.tempp != null && ((this.tempp[1] == null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))) {
            console.log("in the condition");
            this.islifeTimeValidMaxcoverage = false;
            this.isMaxCov = true;
            console.log(this.islifeTimeValidMaxcoverage);
        }

        else {
            this.islifeTimeValidMaxcoverage = true;
            console.log(this.islifeTimeValidMaxcoverage);
        }

    }

    // Comp. Employer Pay Amount with Employee Pay Amount
    onChangeEmplerAmount(eve) {
        console.log("Event:", event);
        this.empeeAmount = this.model.employeePay;
        this.maxCov = this.model.maxCoverage;
        this.emperAmount = parseFloat(eve);
        if (this.emperAmount === 0 || this.maxCov == 0 || this.emperAmount == 0) {
            this.isMaxCov = true;
            this.isEmperAmount = true;
        }
        else {
            if (this.islifeTimeValidEmployeepay === true &&
                this.islifeTimeValidEmployerpay === true &&
                this.islifeTimeValidMaxcoverage === true) {
                if (this.empeeAmount !== null) {
                    if (this.emperAmount <= this.empeeAmount) {
                        this.isEmperAmount = false

                    } else {
                        this.isEmperAmount = true;
                    }
                } else {
                    this.isEmperAmount = true;
                }
                if (this.maxCov !== null && this.emperAmount !== null) {
                    if (this.emperAmount >= this.maxCov) {
                        this.isMaxCov = false;
                    } else {
                        this.isMaxCov = true;
                    }
                } else {
                    this.isMaxCov = true;
                }
            } else {
                this.isMaxCov = true;
                this.isEmperAmount = true;
            }

        }

    }
    // Comp. Employee Pay Amount with Employer Pay Amount and Must to Pay
    onChangeEmpleeAmount(eve) {
        console.log("Event:", event);
        this.emperAmount = this.model.employerPay;
        this.empeeAmount = parseFloat(eve);
        this.addExpEmpMTP = this.model.expenses;
        if (this.empeeAmount === 0 || this.emperAmount == 0 || this.addExpEmpMTP == 0) {
            this.isAddExpEmpMTP = true;
            this.isAddExpEmpMTP = true;
        }
        else {
            if (this.islifeTimeValidEmployeepay === true &&
                this.islifeTimeValidEmployerpay === true &&
                this.islifeTimeValidExpenses === true) {
                if (this.emperAmount !== null) {
                    if (this.empeeAmount >= this.emperAmount) {
                        this.isEmperAmount = false

                    } else {
                        this.isEmperAmount = true;
                    }

                } else {
                    this.isEmperAmount = true;
                }
                if (this.addExpEmpMTP !== null && this.empeeAmount !== null) {
                    if (this.empeeAmount <= this.addExpEmpMTP) {
                        this.isAddExpEmpMTP = false;

                    } else {
                        this.isAddExpEmpMTP = true;
                    }
                } else {
                    this.isAddExpEmpMTP = true;
                }

            } else {
                this.isEmperAmount = true;
                this.isAddExpEmpMTP = true;
            }

        }
    }
    // comp. Add. Exp. Employee Must To Pay with Employee Amount and Employer Amount
    onChangeEmperMTPAmount(eve) {
        console.log("Event:", event);
        this.empeeAmount = this.model.employeePay;
        this.emperAmount = this.model.employerPay;
        this.addExpEmpMTP = parseFloat(eve);
        if (this.addExpEmpMTP === 0 || this.empeeAmount == 0 || this.emperAmount == 0) {
            this.isAddExpEmpMTP = true;
        } else {
            if (this.islifeTimeValidEmployeepay === true &&
                this.islifeTimeValidEmployerpay === true &&
                this.islifeTimeValidExpenses === true) {
                if (this.empeeAmount !== null && this.emperAmount != null && this.addExpEmpMTP !== null) {
                    if (this.addExpEmpMTP >= (this.empeeAmount)) {
                        this.isAddExpEmpMTP = false

                    } else {
                        this.isAddExpEmpMTP = true;
                    }
                } else {
                    this.isAddExpEmpMTP = true;
                }
            } else {
                this.isAddExpEmpMTP = true;
            }

        }
    }

    // comp. Max Cov. with Employee Amount and Employer Amount

    onChangeMaxCov(eve) {
        console.log("Event:", event);
        this.maxCov = parseFloat(eve);
        this.emperAmount = this.model.employerPay;
        if (this.maxCov === 0 || this.emperAmount == 0) {
            this.isMaxCov = true;

        } else {
            if (this.islifeTimeValidEmployerpay === true &&
                this.islifeTimeValidMaxcoverage === true) {
                if (this.emperAmount !== null && this.maxCov !== null) {
                    if (this.maxCov <= (this.emperAmount)) {
                        this.isMaxCov = false;
                    } else {
                        this.isMaxCov = true;
                    }
                } else {
                    this.isMaxCov = true;
                }

            } else {
                this.isMaxCov = true;
            }
        }
    }

}