"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var deduction_code_setup_service_1 = require("../../_services/deduction-code-setup/deduction-code-setup.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var DeductionCodeSetupComponent = (function () {
    function DeductionCodeSetupComponent(router, deductionCodeSetupService, getScreenDetailService, alertService) {
        this.router = router;
        this.deductionCodeSetupService = deductionCodeSetupService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1406';
        this.message = { 'type': '', 'text': '' };
        this.diductionId = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.error = { isError: false, errorMessage: '' };
        this.methodArray = ["Fixed Amount", "Amount Per Unit", "Percent of Gross Wages", "Percent of Net Wages"];
        this.frequencyArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually", "Daily/Miscellaneous"];
        this.myOptions = {
            // other options...
            dateFormat: 'dd-mm-yyyy',
        };
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'DEDUCTION_CODE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_STARTDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_ENDDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_FREQUENCY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_METHOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_PER_PERIOD', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_PER_YEAR', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'DEDUCTION_CODE_LIFETIME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    DeductionCodeSetupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    DeductionCodeSetupComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.deductionCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    DeductionCodeSetupComponent.prototype.compareTwoDates = function (f) {
        if (new Date(f.controls['frmEndDate'].value) < new Date(f.controls['frmStartDate'].value)) {
            this.error = { isError: true, errorMessage: 'End Date cannot before start date' };
        }
    };
    // Open form for create Deduction Code
    DeductionCodeSetupComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            diductionId: null,
            discription: '',
            arbicDiscription: '',
            startDate: null,
            endDate: null,
            frequency: 0,
            method: 0,
            amount: '',
            transction: true,
            inActive: true,
            perPeriod: '',
            perYear: '',
            lifeTime: ''
        };
    };
    // Clear form to reset to default blank
    DeductionCodeSetupComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new Deduction Code
    DeductionCodeSetupComponent.prototype.CreateDeductionCode = function (f, event) {
        var _this = this;
        this.model.startDate = this.frmStartDate;
        this.model.endDate = this.frmEndDate;
        event.preventDefault();
        var locIdx = this.model.diductionId;
        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.deductionCodeSetupService.checkDuplicateDeductionCodeId(locIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    //Call service api for Creating new Deduction Code
                    _this.deductionCodeSetupService.createDeductionCode(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Deduction Code
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        }
    };
    //edit department by row
    DeductionCodeSetupComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.modelStartDate = this.formatDateFordatePicker(this.model.startDate);
        this.modelEndDate = this.formatDateFordatePicker(this.model.endDate);
        this.frmEndDate = this.model.endDate;
        this.frmStartDate = this.model.startDate;
        this.diductionId = row.diductionId;
        this.isUnderUpdate = true;
        this.diductionIdvalue = this.model.diductionId;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    DeductionCodeSetupComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        this.model.diductionId = this.diductionIdvalue;
        //Call service api for updating selected department
        /*var locIdx = this.model.diductionId;
        this.deductionCodeService.checkDuplicateDeductionCodeId(locIdx).then(response => {
            if (response && response.code == 302 && response.result && response.result.isRepeat) {
                this.duplicateWarning = true;
                this.message.type = 'success';
    
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = response.btiMessage.message + ' !';
                }, 100);
            } else {*/
        //Call service api for Creating new Deduction Code
        this.deductionCodeSetupService.updateDeductionCode(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
        /*}*/
        /*}).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin !';
            }, 100)
        });*/
    };
    DeductionCodeSetupComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    DeductionCodeSetupComponent.prototype.delete = function () {
        var _this = this;
        var selectedDeductionCodes = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedDeductionCodes.push(this.selected[i].id);
        }
        this.deductionCodeSetupService.deleteDeductionCode(selectedDeductionCodes).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message + " !";
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message;
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    };
    // default list on page
    DeductionCodeSetupComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    DeductionCodeSetupComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.deductionCodeSetupService.searchDeductionCodelist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    DeductionCodeSetupComponent.prototype.formatDateFordatePicker = function (strDate) {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        }
        else {
            return null;
        }
    };
    // Set default page size
    DeductionCodeSetupComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    DeductionCodeSetupComponent.prototype.confirm = function () {
        this.messageText = 'Confirmed.';
        //this.modalRef.hide();
        this.delete();
    };
    DeductionCodeSetupComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    DeductionCodeSetupComponent.prototype.onStartDateChanged = function (event) {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
        console.log('this.frmStartDate', event);
    };
    DeductionCodeSetupComponent.prototype.onEndDateChanged = function (event) {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
        console.log('this.frmEndDate', this.frmEndDate, event);
        if (this.startDate > this.endDate) {
            this.error = { isError: true, errorMessage: 'End Date cannot before start date' };
        }
        if (this.startDate <= this.endDate) {
            this.error = { isError: false, errorMessage: '' };
        }
        //this.frmEndDate = event.jsdate;
        //this.frmEndDate = event.jsdate;
        //console.log('>>>>>>>>>>', f.controls.['frmEndDate']);
        //console.log('<<<<<<<<<<<<', f.controls['frmStartDate'].value);
        //if(f.controls['frmEndDate'].value.formatted < f.controls['frmStartDate'].value.formatted){
        //this.error={isError:true,errorMessage:'End Date cannot before start date'};
        // }
    };
    DeductionCodeSetupComponent.prototype.CheckNumber = function (event) {
        if (isNaN(event.target.value) == true) {
            this.model.diductionId = 0;
            return false;
        }
    };
    DeductionCodeSetupComponent.prototype.keyPress = function (event) {
        var pattern = /^[0-9\-.()]+$/;
        var inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    };
    return DeductionCodeSetupComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], DeductionCodeSetupComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], DeductionCodeSetupComponent.prototype, "myScrollContainer", void 0);
__decorate([
    core_1.ViewChild('dp2'),
    __metadata("design:type", core_1.ElementRef)
], DeductionCodeSetupComponent.prototype, "input", void 0);
DeductionCodeSetupComponent = __decorate([
    core_1.Component({
        selector: 'deductionCodeSetup',
        templateUrl: './deduction-code-setup.component.html',
        providers: [deduction_code_setup_service_1.DeductionCodeSetupService],
    }),
    __metadata("design:paramtypes", [router_1.Router,
        deduction_code_setup_service_1.DeductionCodeSetupService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], DeductionCodeSetupComponent);
exports.DeductionCodeSetupComponent = DeductionCodeSetupComponent;
//# sourceMappingURL=deduction-code-setup.component.js.map