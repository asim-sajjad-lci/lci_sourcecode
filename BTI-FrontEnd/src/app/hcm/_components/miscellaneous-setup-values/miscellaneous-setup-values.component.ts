import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { MiscellaneousValuesSetup } from '../../_models/miscellaneous-values/miscellaneous-values';
import { NgForm } from '@angular/forms';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { MiscellaneousValuesService } from '../../_services/miscellaneous-values/miscellaneous-values.service';
import { Constants } from '../../../_sharedresource/Constants';

@Component({
  selector: 'app-miscellaneous-setup-values',
  templateUrl: './miscellaneous-setup-values.component.html',
  styleUrls: ['./miscellaneous-setup-values.component.css'],
  providers: [CommonService, MiscellaneousValuesService]
})
export class MiscellaneousSetupValuesComponent implements OnInit {

  miscId;
  page = new Page();
  model: MiscellaneousValuesSetup;
  rows = new Array<MiscellaneousValuesSetup>();
  ddPageSize: number = 5;
  selected = [];
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  deleteId;
  isUnderUpdate: boolean;
  MiscIdvalue;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;

  constructor(
    private route: ActivatedRoute,
    private service: MiscellaneousValuesService,
    private router: Router,
  ) {
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'id';
    this.page.sortBy = 'DESC';
    
    this.miscId = this.route.snapshot.paramMap.get('id');

  }

  ngOnInit() {
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.model = {
      id: 0,
      miscId: 0,
      valueDescription: '',
      codeName: '',
      valueId: '',
      valueArabicDescription: ''

    };
  }

  CreateMiscellaneousValues(f: NgForm, event: Event) {
    debugger;
    event.preventDefault();
    var miscIdx = this.model.valueId;
    this.model.miscId = this.miscId;

    //Check if the id is available in the model.
    //If id avalable then update the Miscellneous, else Add new Miscellneous.
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
       this.isConfirmationModalOpen = true;
       this.isDeleteAction = false;
    }
    else {
      debugger
      //Check for duplicate Division Id according to it create new division
      this.service.checkDuplicateMiscId(miscIdx).then(response => {
        debugger
        if (response && response.code == 302 && response.result && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = "success";

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;
          }, 100);
        } else {
          //Call service api for Creating new miscellaneous
          this.service.createMiscellaneous(this.model).then(data => {
            debugger;
            var datacode = data.code;
            if (datacode == 201) {
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  this.hasMessage = false;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                  }, 4000);
                 // this. isValueDisable = false;
                //  this.MiscId = data.result.id;
                  this.messageText = data.btiMessage.message;
              }, 100);

              this.hasMsg = true;
                f.resetForm();
              //Refresh the Grid data after adding new department
               this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
          });
        }

      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = "Server error. Please contact admin.";
        }, 100)
      });

    }
  }

  setPage(pageInfo) {
debugger
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
      this.page.sortOn = this.page.sortOn;
    } else {
      this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
      this.page.sortBy = this.page.sortBy;
    } else {
      this.page.sortBy = pageInfo.sortBy;
    }

    this.page.searchKeyword = '';
    this.service.getValuesByMiscId(this.page, this.miscId).subscribe(pagedData => {
debugger;
      this.page = pagedData.page;
      this.rows = pagedData.data;
    });
  }

  varifyDelete(row) {
    this.deleteId = row.id;
    this.isDeleteAction = true;
    this.isConfirmationModalOpen = true;

  }

  //delete department by passing whole object of perticular Department
  delete() {
    debugger;
    var selectedMiscellaneous = [];
    //for (var i = 0; i < this.selected.length; i++) {
    selectedMiscellaneous.push(this.deleteId);
    //}
    this.service.deleteMiscellaneous(selectedMiscellaneous).then(data => {
      var datacode = data.code;
      if (datacode == 200) {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if (datacode == "302") {
        this.message.type = "error";
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
      } else {
        this.isSuccessMsg = true;
        this.message.type = "success";
        this.isfailureMsg = false;
      }
      //this.message.text = data.btiMessage.message;

      window.scrollTo(0, 0);
      window.setTimeout(() => {
        this.showMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMessage = false;
        }, 4000);
        this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of department
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    }).catch(error => {
      this.hasMessage = true;
      this.message.type = "error";
      var errorCode = error.status;
      this.message.text = "Server issue. Please contact admin.";
    });
    this.closeModal();
  }

  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }

  //edit Miscellaneous by row
  edit(row: MiscellaneousValuesSetup) {
    debugger;
    this.model = Object.assign({}, row);
    this.isUnderUpdate = true;
      this.MiscIdvalue = row.valueId;
    //  this.codeIdvalue = this.model.codeId;
    setTimeout(() => {
      window.scrollTo(0, 2000);
    }, 10);
  }

  // Clear form to reset to default blank
  Clear(f: NgForm) {
    f.resetForm();
  }

     // Set default page size
     changePageSize(event) {
      this.page.size = event.target.value;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }


  updateStatus() {
    debugger
  this.closeModal();
  //Call service api for updating selected department
  //this.model.codeId = this.codeIdvalue;
  this.model.valueId = this.MiscIdvalue;
  this.service.updateMiscellaneous(this.model).then(data => {
      debugger
      var datacode = data.code;
      if (datacode == 201) {
          //Refresh the Grid data after editing department
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

          //Scroll to top after editing department
          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.isSuccessMsg = true;
              this.isfailureMsg = false;
              this.showMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
              this.messageText = data.btiMessage.message;
              //this.showCreateForm = false;
          }, 100);
          this.hasMessage = false;
          this.hasMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
          }, 4000);
      }
  }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = "Server error. Please contact admin.";
      }, 100)
  });
}

goToMiscellaneous(){
    // var myurl = `${'AccountDetails'}/${row.id}`;
   // var myurl = `${'hcm/miscellaneoussetupvalues'}/${this.MiscId}`;
   var myurl = 'hcm/miscellaneoussetup';
     this.router.navigateByUrl(myurl);
   
}

}
