import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiscellaneousSetupValuesComponent } from './miscellaneous-setup-values.component';

describe('MiscellaneousSetupValuesComponent', () => {
  let component: MiscellaneousSetupValuesComponent;
  let fixture: ComponentFixture<MiscellaneousSetupValuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiscellaneousSetupValuesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiscellaneousSetupValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
