import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { HealthInsuranceSetup } from '../../_models/health-insurance-setup/health-insurance-setup.module';
import { HealthInsuranceSetupService } from '../../_services/health-insurance-setup/health-insurance-setup.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { IMyDateModel } from 'ngx-mydatepicker';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'healthInsuranceSetup',
    templateUrl: './health-insurance-setup.component.html',
    providers: [HealthInsuranceSetupService,CommonService],
})
export class HealthInsuranceSetupComponent {

    page = new Page();
    rows = new Array<HealthInsuranceSetup>();
    temp = new Array<HealthInsuranceSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1410';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    insCompanyId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: HealthInsuranceSetup;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    insCompanyIdvalue: number;
    getCompany: any[] = [];
    companyIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    countries = [];
    states = [];
    cities = [];
    phonePattern = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
    HEALTHINSURANCESETUP_SEARCH: any
    HEALTHINSURANCESETUP_ID: any
    HEALTHINSURANCESETUP_DESCRIPTION: any
    HEALTHINSURANCESETUP_ARABIC_DESCRIPTION: any
    HEALTHINSURANCESETUP_CONTACT_NAME: any
    HEALTHINSURANCESETUP_ADDRESS: any
    HEALTHINSURANCESETUP_CITY: any
    HEALTHINSURANCESETUP_COUNTRY: any
    HEALTHINSURANCESETUP_PHONE: any
    HEALTHINSURANCESETUP_FAX: any
    HEALTHINSURANCESETUP_ACTION: any
    HEALTHINSURANCESETUP_CREATE_LABEL: any
    HEALTHINSURANCESETUP_SAVE_LABEL: any
    HEALTHINSURANCESETUP_CLEAR_LABEL: any
    HEALTHINSURANCESETUP_CANCEL_LABEL: any
    HEALTHINSURANCESETUP_UPDATE_LABEL: any
    HEALTHINSURANCESETUP_DELETE_LABEL: any
    HEALTHINSURANCESETUP_CREATE_FORM_LABEL: any
    HEALTHINSURANCESETUP_UPDATE_FORM_LABEL: any
    HEALTHINSURANCESETUP_EMAIL: any
    HEALTHINSURANCESETUP_TABLEVIEW: any
    HEALTHINSURANCESETUP_STATE: any
    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private healthInsuranceSetupService: HealthInsuranceSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,private commonService : CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'HEALTHINSURANCESETUP_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_CONTACT_NAME', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_CITY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_PHONE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_FAX', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_EMAIL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'HEALTHINSURANCESETUP_STATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];
        this.HEALTHINSURANCESETUP_SEARCH = this.defaultFormValues[0];
        this.HEALTHINSURANCESETUP_ID = this.defaultFormValues[1];
        this.HEALTHINSURANCESETUP_DESCRIPTION = this.defaultFormValues[2];
        this.HEALTHINSURANCESETUP_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.HEALTHINSURANCESETUP_CONTACT_NAME = this.defaultFormValues[4];
        this.HEALTHINSURANCESETUP_ADDRESS = this.defaultFormValues[5];
        this.HEALTHINSURANCESETUP_CITY = this.defaultFormValues[6];
        this.HEALTHINSURANCESETUP_COUNTRY = this.defaultFormValues[7];
        this.HEALTHINSURANCESETUP_PHONE = this.defaultFormValues[8];
        this.HEALTHINSURANCESETUP_FAX = this.defaultFormValues[9];
        this.HEALTHINSURANCESETUP_ACTION = this.defaultFormValues[10];
        this.HEALTHINSURANCESETUP_CREATE_LABEL = this.defaultFormValues[11];
        this.HEALTHINSURANCESETUP_SAVE_LABEL = this.defaultFormValues[12];
        this.HEALTHINSURANCESETUP_CLEAR_LABEL = this.defaultFormValues[13];
        this.HEALTHINSURANCESETUP_CANCEL_LABEL = this.defaultFormValues[14];
        this.HEALTHINSURANCESETUP_UPDATE_LABEL = this.defaultFormValues[15];
        this.HEALTHINSURANCESETUP_DELETE_LABEL = this.defaultFormValues[16];
        this.HEALTHINSURANCESETUP_CREATE_FORM_LABEL = this.defaultFormValues[17];
        this.HEALTHINSURANCESETUP_UPDATE_FORM_LABEL = this.defaultFormValues[18];
        this.HEALTHINSURANCESETUP_EMAIL = this.defaultFormValues[19];
        this.HEALTHINSURANCESETUP_TABLEVIEW = this.defaultFormValues[20];
        this.HEALTHINSURANCESETUP_STATE = this.defaultFormValues[21];
        this.companyIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.insCompanyId);
        }).mergeMap((token: string) => this.getCompanyIdAsObservable(token));
    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.healthInsuranceSetupService.getCountry().then(data => {
            this.countries = data.result;
            //console.log(data.result);       
        });
        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance        
        

    }
    getCompanyIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getCompany.filter((id: any) => {
                return query.test(id.insCompanyId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        // console.log('Selected value: ', e);
        this.healthInsuranceSetupService.gethealthInsCompanyById(e.item.id).then(pagedData => {
            // console.log('PageData', pagedData);
            this.model = pagedData.result;
            this.model.id = 0;
            this.healthInsuranceSetupService.getStatesByCountryId(this.model.countryId).then(data => {
                this.states = data.result;
                //console.log(data.result);  
                this.healthInsuranceSetupService.getCitiesByStateId(this.model.stateId).then(data => {
                    this.cities = data.result;
                    //console.log(data.result);       
                });     
            });
        });
    }
    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.healthInsuranceSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
        this.healthInsuranceSetupService.getCompany().then(data => {
            this.getCompany = data.result.records;
            //console.log("Data class options : "+this.getCompany);
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            insCompanyId: null,
            desc: '',
            arbicDesc: '',
            address: '',
            contactPerson: '',
            phoneNo: '',
            email: '',
            fax: '',
            cityName: '',
            countryName: '',
            stateName: '',
            cityId: null,
            countryId: null,
            stateId: null,
        };
        this.healthInsuranceSetupService.getCountry().then(data => {
            this.countries = data.result;
            //console.log(data.result);       
        });
        this.countries.forEach(ele => {
            if (ele.defaultCountry == 1) {
                this.model.countryId = ele.countryId;
            }
        });
        this.healthInsuranceSetupService.getStatesByCountryId(this.model.countryId).then(data => {
            this.states = data.result;
            //console.log(data.result);
            this.model.stateId = this.states[0].stateId;

            this.healthInsuranceSetupService.getCitiesByStateId(this.model.stateId).then(data => {
                this.cities = data.result;
                //console.log(data.result);
                this.model.cityId = this.cities[0].cityId;
            });
        });



    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({ country: 0, state: 0, city: 0 });
    }

    //function call for creating new Health Insurance
    CreateHealthInsurance(f: NgForm, event: Event) {
        event.preventDefault();
        var locIdx = this.model.insCompanyId;

        //Check if the id is available in the model.
        //If id avalable then update the Health Insurance, else Add new Health Insurance.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Call service api for Creating new Health Insurance
            this.healthInsuranceSetupService.checkDuplicatehealthIns(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                }
                else {
                    this.healthInsuranceSetupService.createHealthInsurance(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Health Insurance
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }

                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });
        }
    }

    //edit department by row
    edit(row: HealthInsuranceSetup) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.insCompanyId = row.insCompanyId;
        this.isUnderUpdate = true;
        this.insCompanyIdvalue = this.model.insCompanyId;
        this.healthInsuranceSetupService.getStatesByCountryId(1).then(data => {
            this.states = data.result;
            //console.log(data.result);       
        });
        this.healthInsuranceSetupService.getCitiesByStateId(1).then(data => {
            this.cities = data.result;
            //console.log(data.result);       
        });
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }


    updateStatus() {
        this.closeModal();
        this.model.insCompanyId = this.insCompanyIdvalue;
        //Call service api for Creating new location
        this.healthInsuranceSetupService.updateHealthInsurance(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedInsurances = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedInsurances.push(this.selected[i].id);
        }
        this.healthInsuranceSetupService.deleteHealthInsurance(selectedInsurances).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }
    onCountrySelect(event) {
        this.model.stateId = 0;
        this.model.cityId = 0;
        this.healthInsuranceSetupService.getStatesByCountryId(event.target.value).then(data => {
            this.states = data.result;
            //console.log(data.result);       
        });
        this.cities = [];
        /* this.states = this._dataService.getStates()
                      .filter((item)=> item.countryid == countryid);*/
    }
    onStateSelect(event) {
        this.model.cityId = 0;
        this.healthInsuranceSetupService.getCitiesByStateId(event.target.value).then(data => {
            this.cities = data.result;
            //console.log(data.result);       
        });
        /* this.states = this._dataService.getStates()
                      .filter((item)=> item.countryid == countryid);*/
    }
    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.healthInsuranceSetupService.searchHealthInsurancelist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model.insCompanyId = 0;
            return false;
        }
    }

    keyPress(event: any) {
        const pattern = /^[0-9\-()]+$/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}
