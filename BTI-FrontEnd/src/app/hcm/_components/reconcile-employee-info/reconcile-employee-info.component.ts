import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { PaycodeSetupModule } from '../../_models/paycode-setup/paycode-setup.module';
import { PaycodeSetupService } from '../../_services/paycode-setup/paycode-setup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { PayrollCodeModifierModule } from '../../_models/payroll-code-modifier/payroll-code-modifier.module';
import { RoutineUtilitiesService } from '../../_services/routines-utilities/routine-utilities.service';
import { DeductionCodeSetup } from '../../_models/deduction-code-setup/deduction-code-setup.module';
import { BenefitCodeSetup } from '../../_models/benefit-code-setup/benefit-code-setup.module';

@Component({
    selector: 'reconcile-employee-info',
    templateUrl: './reconcile-employee-info.component.html',
    providers: [CommonService, RoutineUtilitiesService]
})

export class ReconcileEmployeeInfoComponent {

    moduleCode = 'M-1011';
    screenCode = 'S-1475';
    defaultFormValues: object[];
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    RECONCILE_EMPLOYEE_INFO_YEAR: any;
    RECONCILE_EMPLOYEE_INFO_RANGE: any;
    RECONCILE_EMPLOYEE_INFO_ALL: any;
    RECONCILE_EMPLOYEE_INFO_FROM: any;
    RECONCILE_EMPLOYEE_INFO_TO: any;
    RECONCILE_EMPLOYEE_INFO_RESTRICTIONS: any;
    RECONCILE_EMPLOYEE_INFO_INSERT: any;
    RECONCILE_EMPLOYEE_INFO_REMOVE: any;
    RECONCILE_EMPLOYEE_INFO_CLEAR: any;
    RECONCILE_EMPLOYEE_INFO_PROCESS: any;
    messageText: string;
    year: any;

    years: DropDownModule[] = [
        { id: 1, name: '2018' }
    ];


    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('fieldName1')

    typeaheadLoading: boolean;

    loaderImg: boolean = true;

    constructor(public commonService: CommonService, private routineService: RoutineUtilitiesService) {
        // this.model = new PayrollCodeModifierModule();

        this.defaultFormValues = [
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_YEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_RANGE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_ALL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_FROM', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_TO', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_RESCTRICTIONS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_INSERT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_REMOVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_CLEAR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'RECONCILE_EMPLOYEE_INFO_PROCESS', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' }
        ];

        this.RECONCILE_EMPLOYEE_INFO_YEAR = this.defaultFormValues[0];
        this.RECONCILE_EMPLOYEE_INFO_RANGE = this.defaultFormValues[1];
        this.RECONCILE_EMPLOYEE_INFO_ALL = this.defaultFormValues[2];
        this.RECONCILE_EMPLOYEE_INFO_FROM = this.defaultFormValues[3];
        this.RECONCILE_EMPLOYEE_INFO_TO = this.defaultFormValues[4];
        this.RECONCILE_EMPLOYEE_INFO_RESTRICTIONS = this.defaultFormValues[5];
        this.RECONCILE_EMPLOYEE_INFO_INSERT = this.defaultFormValues[6];
        this.RECONCILE_EMPLOYEE_INFO_REMOVE = this.defaultFormValues[7];
        this.RECONCILE_EMPLOYEE_INFO_CLEAR = this.defaultFormValues[8];
        this.RECONCILE_EMPLOYEE_INFO_PROCESS = this.defaultFormValues[9];
    }


    ngOnInit() {

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    }

    Clear(f: NgForm) {
        f.resetForm({
            year: ''
        });
    }

}

