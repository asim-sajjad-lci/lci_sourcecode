import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Page } from '../../../_sharedresource/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Router } from '@angular/router';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { TerminationSetupModule } from '../../_models/termination-setup/termination-setup.module';
import { TerminationSetupService } from '../../_services/termination-setup/termination-setup.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

@Component({
    selector: 'terminationSetup',
    templateUrl: './termination-setup.component.html',
    providers: [TerminationSetupService]
})
export class TerminationSetupComponent implements OnInit {

    page = new Page();
    rows = new Array<TerminationSetupModule>();
    temp = new Array<TerminationSetupModule>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1424';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    terminationId = {};
    positionClassOptions = [];
    skillSetIdOptions = [];
    searchKeyword = '';
    ddPageSize: number = 5;
    model: TerminationSetupModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
	idx;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    positionIdvalue: string;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    dataSource: Observable<any>;
    asyncSelected: string;
    typeaheadLoading: boolean;
	typeaheadNoResults: boolean;
	subItems = [];
	subItemsObj= {};
	isSubItem = true;
	checklistitem;
	sublistitem=[];
	sequence;
	personResponsible;
	completeDate;
    constructor(
        private router: Router,
        private positionSetupService: TerminationSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService
    ) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        // default form parameter for position  screen
        this.defaultFormValues = [
            { 'fieldName': 'TERMINATION_SETUP_PREDEFINE_CHECKSUM', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_ARABIC_DECRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_TERMINATION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_TERMINATION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'TERMINATION_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_TRAINING_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_PLAN_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
			{ 'fieldName': 'TERMINATION_SETUP_PREDEFINE_CHECKLIST_ITEM', 'fieldValue': '', 'helpMessage': '' },
			{ 'fieldName': 'TERMINATION_SETUP_CREATE_PREDEFINE_CHECK_LIST_ITEM', 'fieldValue': '', 'helpMessage': '' },
        ];

        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.asyncSelected);
          }).mergeMap((token: string) => this.getStatesAsObservable(token));
    }

    ngOnInit() {

        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        // getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName;
            this.screenName = data.result.dtoScreenDetail.screenName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (let j = 0; j < this.availableFormValues.length; j++) {
                let fieldKey = this.availableFormValues[j]['fieldName'];
                let objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                let objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        //Following shifted from SetPage for better performance
        this.positionSetupService.getTerminationClassList().then(data => {
            this.positionClassOptions = data.result.records;
           //console.log(this.positionClassOptions);
        });
        
    }

    getStatesAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
     
        return Observable.of(
          this.positionClassOptions.filter((id: any) => {
            return query.test(id.positionClassId);
          })
        );
      }
     
      changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
      }
     
      typeaheadOnSelect(e: TypeaheadMatch): void {
       //console.log('Selected value: ', e.value);
      }

    // setting pagination
    setPage(pageInfo) {
        // debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.positionSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            
            // this.positionSetupService.getSkillSetList().then(data => {
            //     this.skillSetIdOptions = data.result.records;
            // });
        });
    }

    // Open form for create position setup
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            terminationId: '',
            terminationDesc: '',
            terminationArbicDesc: '',
			terminationDetailId:0,
			predefinecheckListtypeId:0,
			orientationPredefinedCheckListItemId:0,
			subItems:[]
            
        };
    }

    // edit position class by row
    edit(row: TerminationSetupModule) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.terminationId = row.terminationId;
        this.isUnderUpdate = true;
        this.positionIdvalue = this.model.terminationId;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        // Call service api for updating selected position class
        this.model.terminationId = this.positionIdvalue;
        this.positionSetupService.updatePositionSetup(this.model).then(data => {
            let datacode = data.code;
            if (datacode === 201) {
                // Refresh the Grid data after editing position class
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                // Scroll to top after editing position class
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;

                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    navToTraining() {
       //console.log("function is called");
        if (this.selected.length === 1) {

            this.router.navigate(['hcm/positionTraining', this.selected[0].positionId,
                this.selected[0].description, this.selected[0].arabicDescription, this.selected[0].id]);
        } else if (this.selected.length > 1) {
            //console.log(this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = true;
                }, 100);
                this.messageText = "Please select only one to navigate";
            }, 100);
        } else {
            //console.log("down:-->"+this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = true;
                }, 100);
                this.messageText = "Please select atleast one option";
            }, 100);
        }

    }

    navToPlanSetup() {

        if (this.selected.length === 1) {

            this.router.navigate(['hcm/positionPlanSetup', this.selected[0].positionId,
                this.selected[0].id]);
        } else if (this.selected.length > 1) {
            //console.log(this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = true;
                }, 100);
                this.messageText = "Please select only one to navigate";
            }, 100);
        } else {
            //console.log("down:-->"+this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = true;
                }, 100);
                this.messageText = "Please select atleast one option";
            }, 100);
        }
    }

    navToAttachment() {
        this.router.navigate(['hcm/positionAttachment', this.model.terminationId,
            this.model.terminationDesc]);
    }

    attachMsg() {
        window.scrollTo(0, 0);
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        window.setTimeout(() => {
            this.showMsg = true;
            this.hasMsg = true;
        }, 100);
        this.messageText = "Please save the Position Setup first.";
    }

    // delete position class by passing whole object of perticular position Setup
    delete() {
	
		if(this.idx > 0){

        this.deleteSubItem();

      }else{
		let selectedPositionSetup = [];
        for (let i = 0; i < this.selected.length; i++) {
            selectedPositionSetup.push(this.selected[i].id);
        }
        this.positionSetupService.deletePositionSetup(selectedPositionSetup).then(data => {
            let datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);

                this.message.text = data.result.messageType;
            }, 100);

            // Refresh the Grid data after deletion of position Setup
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            let errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
	  }
        
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search position class by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.positionSetupService.searchPositionSetuplist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }


    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({predefinecheckListtypeId:0});
    }


    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    // function call for creating new positionsetup
    CreatePositionSetup(f: NgForm, event: Event) {
        event.preventDefault();
        let posIdx = this.model.terminationId;

        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Check for duplicate positionClass Id according to it create new position class
            this.positionSetupService.checkDuplicatePositionSetupId(posIdx).then(response => {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    // Call service api for Creating new position class
					console.log(this.model);
                    this.positionSetupService.createPositionSetup(this.model).then(data => {
                        let datacode = data.code;
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.hasMessage = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            // Refresh the Grid data after adding new position class
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });

        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
	checklistbyChanged(id){
		 var id = id;
		 this.model.subItems=[];
		 this.positionSetupService.getTerminationsubitem(id).then(data=>{
			this.sublistitem = data.result.subItems;
			this.sublistitem.forEach(item =>{
			
			this.subItemsObj = {
					  "checklistitem": item.itemDesc,
					  "sequence": null,
					  "personResponsible":null,
					  "completeDate":null
				}
				console.log('obj',this.subItemsObj);
				 this.model.subItems.push(Object.assign({}, this.subItemsObj));
				//console.log('item',item);
			});
			console.log('subitem',this.sublistitem);
		 });
		 
		 var acc = this.positionClassOptions.filter(x => x.id === id)[0];        
		 var accdescription = acc.checklistType;
		  this.subItemsObj = {
          "checklistitem": accdescription,
          "sequence": null,
		  "personResponsible":null,
		  "completeDate":null
        }
        this.model.subItems.push(Object.assign({}, this.subItemsObj));
		//console.log(accdescription);
		 
	}
	
	addSubItem(subItemData: NgForm, subitem){
        this.subItemsObj = {
          "checklistitem": subitem.checklistitem,
          "sequence": subitem.sequence,
		  "personResponsible":subitem.personResponsible,
		  "completeDate":subitem.completeDate
        }
        this.model.subItems.push(Object.assign({}, this.subItemsObj));
 
       //console.log("AA",this.model.subItems);
        subItemData.resetForm();
    }
	addprecheck(precheck,num)
	{
		console.log('num',num);
		this.subItemsObj = {
          "checklistitem": precheck.checklistitem,
          "sequence": precheck.sequence,
		  "personResponsible":precheck.personResponsible,
		  "completeDate":precheck.completeDate
        }
        this.model.subItems[num].push(Object.assign({}, this.subItemsObj[num]));
	}
    // delete Subitems
    deleteRow(num){
        this.model.subItems.splice(num, 1);
    }
	varifySubDelete(obj) {
        this.idx = obj.id;
        if(this.idx === undefined){
            this.model.subItems.pop();
        }
        else if (this.idx > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
          } else {
              this.isSuccessMsg = false;
              this.hasMessage = true;
              this.message.type = 'error';
              this.isfailureMsg = true;
              this.showMsg = true;
              this.message.text = 'Please select at least one record to delete.';
              window.scrollTo(0, 0);
          }
    }
	deleteSubItem() {
        // this.varifySubDelete(id);
            let indx= this.idx;
            var predefinechecklists = [];
          if (indx==null) {
            this.model.subItems.pop();
            } else{
                predefinechecklists.push(indx);
            }
        this.positionSetupService.deleteSubitem(predefinechecklists).then(data => {
            this.model.subItems = data.result.delete[0].list;
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            
            this.hasMessage = true;
            this.message.type = 'success';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                  }, 4000);
                this.message.text = data.btiMessage.message;
               }, 100);
  
            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            // this.model.subItems = this.rows.

  
          }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
          });
          this.closeModal();

      }

}
