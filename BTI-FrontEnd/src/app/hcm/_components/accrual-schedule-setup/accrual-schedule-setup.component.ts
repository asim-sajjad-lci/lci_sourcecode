import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { AccrualScheduleSetup } from '../../_models/accrual-schedule-setup/accrual-schedule-setup.module';
import { AccrualScheduleDetails } from '../../_models/accrual-schedule-details/accrual-schedule-details.module';
import { AccrualScheduleSetupService } from '../../_services/accrual-schedule-setup/accrual-schedule-setup.service';
import { AccrualScheduleDetailService } from '../../_services/accrual-schedule-detail/accrual-schedule-detail.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';

import { forEach } from '@angular/router/src/utils/collection';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
    selector: 'accrualScheduleSetup',
    templateUrl: './accrual-schedule-setup.component.html',
    styles: ['./accrual-schedule-setup.component.css'],
    providers: [AccrualScheduleSetupService, AccrualScheduleDetailService]
})
export class AccrualScheduleSetupComponent {
    page = new Page();
    rows = new Array<AccrualScheduleSetup>();
    selected = [];
    model1: AccrualScheduleSetup;

    model2: AccrualScheduleDetails;
    page2 = new Page();
    rows2 = new Array<AccrualScheduleDetails>();
    selected2 = [];

    moduleCode = 'M-1011';
    screenCode = 'S-1418';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    accrualId = {};
    accrualId2 = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    ddPageSize2: number = 5;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    isDeleteAction2: boolean = false;
    accrualIdvalue: number;
    searchAccrualOpt: boolean = false;
    selectedRowId: number = 0;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private accrualScheduleSetupService: AccrualScheduleSetupService,
        private accrualScheduleDetailService: AccrualScheduleDetailService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'scheduleId';
        this.page.sortBy = 'DESC';

        this.page2.pageNumber = 0;
        this.page2.size = 5;
        this.page2.sortOn = 'scheduleSeniority';
        this.page2.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'ACCURAL_SCHEDULE_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_DECRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_STARTDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_DETAIL_SENIORITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_DETAIL_ACCRUAL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCRUAL_SCHEDULE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_ENDDATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_DETAIL_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_DETAIL_ACCRUAL_BY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_DETAIL_AMOUNT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACCURAL_SCHEDULE_DETAIL_DELETE_ROW', 'fieldValue': '', 'helpMessage': '' }
        ];

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

    }

    // Open form for create Accrual
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model1 = {
            id: 0,
            scheduleId: null,
            desc: '',
            arbicDesc: '',
            startDate: null,
            endDate: null,
            subItems: new Array<AccrualScheduleDetails>()
        };
    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.accrualScheduleSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            /*     
                if(this.rows[0].subItems!=null)
                    this.rows2 = this.rows[0].subItems;
                else
                    this.rows2=new Array<AccrualScheduleDetails>(); */
            /*   this.rows[0].subItems.forEach(element => {
                  var temp = new AccrualScheduleDetails(
                      element.id,
                      element.scheduleId,
                      element.scheduleSeniority,
                      element.accrualSetupId,
                      element.description,
                      element.accrualType,
                      element.hours
                  );
                  this.rows2.push(temp);
              }); */
            /* var temp=new AccrualScheduleDetails(0,0,0,0,'',0,0);
            console.log("ROW 1:", this.rows);
            this.rows.forEach(row => {
                console.log("ROW:", row);
                row.subItems.forEach(subItem => {
                    console.log("SUBITEM:", subItem);
                    temp.accrualSetupId=subItem.accrualSetupId;
                    temp.accrualType=subItem.accrualType;
                    temp.description=subItem.description;
                    temp.hours=subItem.hours;
                    temp.id=subItem.id;
                    temp.scheduleId=subItem.scheduleId;
                    temp.scheduleSeniority=subItem.scheduleSeniority;
                    this.rows2.push(temp);
                    console.log("ADD:", this.rows2);
                })
            }) */
            /* 
                        console.log("ROW 2:", this.rows2); */
            //this.rows2 = this.rows.subItems;
        });
    }

    setPage2(pageInfo) {
        //debugger;
        this.selected2 = []; // remove any selected checkbox on paging
        this.page2.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page2.sortOn = this.page2.sortOn;
        } else {
            this.page2.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page2.sortBy = this.page2.sortBy;
        } else {
            this.page2.sortBy = pageInfo.sortBy;
        }
        this.page2.searchKeyword = '';
        this.accrualScheduleDetailService.getlist(this.page2, this.searchKeyword).subscribe(pagedData => {
            //this.rows2= pagedData.data;
            this.rows2=this.model1.subItems.slice(pagedData.page.pageNumber*pagedData.page.size,(pagedData.page.pageNumber+1)*pagedData.page.size);
            console.log(this.rows2);
            pagedData.page.totalElements = this.model1.subItems.length;
            pagedData.page.totalPages = Math.ceil(pagedData.page.totalElements / pagedData.page.size);
            this.page2 = pagedData.page;
            console.log(this.page2);
        });

    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }


    //function call for creating new Accrual
    CreateAccuralSchedule(f: NgForm, event: Event) {
        event.preventDefault();
        var scheduleId = this.model1.scheduleId;

        //Check if the id is available in the model.
        //If id avalable then update the Accrual, else Add new Accrual.
        if (this.model1.id > 0 && this.model1.id != 0 && this.model1.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            //Call service api for Creating new Accrual
            this.accrualScheduleSetupService.checkDuplicateAccrualId(scheduleId).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message + ' !';
                    }, 100);
                }
                else {
                    this.accrualScheduleSetupService.createAccrualSchedule(this.model1).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Accrual
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin !';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: AccrualScheduleSetup) {
        this.selectedRowId = row.id;
        this.showCreateForm = true;
        this.model1 = Object.assign({}, row);
        if (this.model1.subItems == null)
            this.model1.subItems = new Array<AccrualScheduleDetails>();
        this.accrualId = row.scheduleId;
        this.isUnderUpdate = true;
        this.accrualIdvalue = this.model1.scheduleId;
        this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }
    /* 
    
        updateStatus() {
            if(this.isDeleteAction2)
                this.updateStatus2();
            else 
                this.updateStatus1();
        } */

    updateStatus() {
        this.closeModal();
        this.model1.scheduleId = this.accrualIdvalue;

        //Call service api for Creating new Accrual
        this.accrualScheduleSetupService.updateAccrualSchedule(this.model1).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });

    }

    /* updateStatus2() {
        this.closeModal();
        this.model2.id = this.accrualIdvalue;
        
        //Call service api for Creating new Accrual
        this.accrualScheduleDetailService.updateAccrual(this.model2).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });            

    } */

    varifyDelete() {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    varifyDelete2() {
        if (this.selected2.length > 0) {
            this.isDeleteAction2 = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    delete() {
        if (this.isDeleteAction) {
            this.delete1();
            //console.log("del1");
        }
        else if (this.isDeleteAction2)
            this.delete2();
    }

    //delete department by passing whole object of perticular Department
    delete1() {
        var selectedAccruals = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedAccruals.push(this.selected[i].id);
        }
        console.log(selectedAccruals);
        this.accrualScheduleSetupService.deleteAccrualSchedule(selectedAccruals).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            //this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    //delete department by passing whole object of perticular Department
    delete2() {
        var selectedAccruals = [];
        for (var i = 0; i < this.selected.length; i++) {
            if (this.selected2[i].id > 0 && this.selected2[i].id != 0 && this.selected2[i].id != undefined) {
                selectedAccruals.push(this.selected2[i].id);
            }
            this.model1.subItems = this.model1.subItems.filter(item => item !== this.selected2[i]);
        }
        console.log(selectedAccruals);
        this.accrualScheduleDetailService.deleteAccrual(selectedAccruals).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            this.message.type = 'success';

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            this.updateStatus();

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });

        //Refresh the Grid data after deletion of department
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });

        this.selected2 = [];
        this.closeModal();
    }

    /* addRow2() {
        this.model1.subItems.push(cloneDeep(new AccrualScheduleDetails(0, null, null, null, '', null, null)));
        console.log(this.model1.subItems);
        this.page2.size = this.model1.subItems.length;
        this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
    } */

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        console.log(selected);
    }

    onSelect2({ selected }) {
        this.selected2.splice(0, this.selected2.length);
        this.selected2.push(...selected);
        console.log(selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
        this.table.offset = 0;
        // this.accrualSetupService.searchAccruallist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    updateFilter2(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page2.pageNumber = 0;
        this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
        this.table.offset = 0;
        // this.accrualSetupService.searchAccruallist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    changePageSize2(event) {
        this.page2.size = event.target.value;
        this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isDeleteAction2 = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            this.model1.scheduleId = 0;
            return false;
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    sortColumn2(val) {
        if (this.page2.sortOn == val) {
            if (this.page2.sortBy == 'DESC') {
                this.page2.sortBy = 'ASC';
            } else {
                this.page2.sortBy = 'DESC';
            }
        }
        this.page2.sortOn = val;
        this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
    }

}
