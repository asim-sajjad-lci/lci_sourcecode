import { Component , ElementRef, ViewChild} from '@angular/core';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {ReportManagementService} from '../../../userModule/_services/report-management/report-management.service';
import {Page} from '../../../_sharedresource/page';
import {ReportManagementModel} from '../../../userModule/_models/report-management/report-management';
import {DatatableComponent} from '@swimlane/ngx-datatable';

declare var showReport: any;

@Component({
    selector: 'payroll-statement-report',
    templateUrl: './payroll-statement-report.component.html',
    providers: [ReportManagementService]
})
export class PayrollStatementReportComponent {

    page = new Page();
    rows = new Array<ReportManagementModel>();
    searchKeyword = '';
    moduleCode = 'M-1011';
    screenCode = 'S-1478';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    currentLanguage: any;
    getScreenDetailArr;

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    
    constructor(private getScreenDetailService: GetScreenDetailService,
                private reportManagementService: ReportManagementService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
    }

    ngOnInit() {
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.getScreenDetails();

        this.reportManagementService.getReportMasterList(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            for (let i = 0; i < this.rows.length; i++) {
                if (this.rows[i].reportName.includes(this.screenName)) {
                    let reportid = this.rows[i].id;
                    this.reportManagementService.getEJBIUserCredentials(reportid).then(res => {
                        showReport.func1(res);
                    });
                }
            }
        });
    }

    getScreenDetails() {
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.screenGridDetail(this.moduleCode, this.screenCode).then(data => {      
            this.getScreenDetailArr = data.result;
            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }
}
