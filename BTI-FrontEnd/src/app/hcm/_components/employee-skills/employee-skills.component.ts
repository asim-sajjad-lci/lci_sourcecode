import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { EmployeeSkills } from '../../_models/employee-skills/employee-skills';
import { EmployeeSkillsService } from '../../_services/employee-skills/employee-skills.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { IMyDateModel, INgxMyDpOptions, IMyOptions } from 'ngx-mydatepicker';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
@Component({
    selector: 'employeeSkills',
    templateUrl: './employee-skills.component.html',
    styleUrls: ['./employee-skills.component.css'],
    providers: [EmployeeSkillsService,CommonService]
})

// export Department component to make it available for other classes
export class EmployeeSkillsComponent {
    isErrorMsg: boolean;
    errorMsg: string;
    page = new Page();
    rows = new Array<any>();
    temp = new Array<EmployeeSkills>();
    selected = [];
    moduleCode = "M-1011";
    screenCode = "S-1454";
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    employeeId = {};
    skillSetId = {};
    listSkillsList: any = [];
    searchKeyword = '';
    getEmployeeIds = [];
    getEmployeeId = [];    
    getSkillSetIds: any[] = [];
    ddPageSize: number = 5;
    model: EmployeeSkills;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    isUnderUpdateSId: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    employeeIdValue: string;
    skillSetIdValue: string;
    employeeIdList: Observable<any>;
    skillSetIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    modelemployeeId: any;
    modelskillSetId: any;
    subItemsArray: any = [];
    obtainedValue: any;
    proficiencyValue: any;
    requiredValue: any;
    skillCommentValue: any;
    skillExpirationDateValue: any;
    obtained: any;
    proficiency: any;
    required: any;
    skillComment: any;
    skillExpirationDate: Date;
    skillSetIdObj: object;
    employeeMasterIdObj: object
    skillSetIdStore: any;
    listSkillsObj: any = {};
    editAndSaveArray: any = []
    minDate: Date = new Date();
    noPrintTable: boolean = true;
    printDetails: boolean = true;
    doNotPrintHearder: boolean = false;
    loaderImg: boolean = true;
    checkEmpSkillValidId: boolean = true;
    checkEmpValidId: boolean = true;
    afterFirstSave:boolean = true;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    EMPLOYEE_SKILLS_EMPLOYEE_ID:any;
    EMPLOYEE_SKILLS_SKILL_SET_ID:any;
    EMPLOYEE_SKILLS_SAVE_LABEL:any;
    EMPLOYEE_SKILLS_CREATE_LABEL:any;
    EMPLOYEE_SKILLS_CLEAR_LABEL:any;
    EMPLOYEE_SKILLS_UPDATE_LABEL:any;
    EMPLOYEE_SKILLS_CANCEL_LABEL:any;
    EMPLOYEE_SKILLS_DELETE_LABEL:any;
    EMPLOYEE_SKILLS_PRINT_LABEL:any;
    EMPLOYEE_SKILLS_SKILLS_DESCRIPTION_LABEL:any;
    EMPLOYEE_SKILLS_OBTAINED_LABEL:any;
    EMPLOYEE_SKILLS_PROFICIENCY_LABEL:any;
    EMPLOYEE_SKILLS_REQUIRED_LABEL:any;
    EMPLOYEE_SKILLS_DATE_LABEL:any;
    EMPLOYEE_SKILLS_COMMENT_LABEL:any;
    EMPLOYEE_SKILLS_CREATE_FORM_LABEL:any;
    EMPLOYEE_SKILLS_UPDATE_FORM_LABEL:any;
    EMPLOYEE_SKILLS_SEARCH_LABEL:any;
    EMPLOYEE_SKILLS_ACTION:any;
    EMPLOYEE_DEPENDENTS_ACTION:any;
    ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_ACTION:any;

    constructor(
        private router: Router,
        private employeeSkillsService: EmployeeSkillsService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,private commonService:CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for Employee Skills  screen
        this.defaultFormValues = [
            { 'fieldName': 'EMPLOYEE_SKILLS_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_SKILL_SET_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_PRINT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_SKILLS_DESCRIPTION_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_OBTAINED_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_PROFICIENCY_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_REQUIRED_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_DATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_COMMENT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_SEARCH_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_SKILLS_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'EMPLOYEE_DEPENDENTS_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_ACTION', 'fieldValue': '', 'helpMessage': '' }
        ];

            this.EMPLOYEE_SKILLS_EMPLOYEE_ID=this.defaultFormValues[0];
            this.EMPLOYEE_SKILLS_SKILL_SET_ID=this.defaultFormValues[1];
            this.EMPLOYEE_SKILLS_SAVE_LABEL=this.defaultFormValues[2];
            this.EMPLOYEE_SKILLS_CREATE_LABEL=this.defaultFormValues[3];
            this.EMPLOYEE_SKILLS_CLEAR_LABEL=this.defaultFormValues[4];
            this.EMPLOYEE_SKILLS_UPDATE_LABEL=this.defaultFormValues[5];
            this.EMPLOYEE_SKILLS_CANCEL_LABEL=this.defaultFormValues[6];
            this.EMPLOYEE_SKILLS_DELETE_LABEL=this.defaultFormValues[7];
            this.EMPLOYEE_SKILLS_PRINT_LABEL=this.defaultFormValues[8];
            this.EMPLOYEE_SKILLS_SKILLS_DESCRIPTION_LABEL=this.defaultFormValues[9];
            this.EMPLOYEE_SKILLS_OBTAINED_LABEL=this.defaultFormValues[10];
            this.EMPLOYEE_SKILLS_PROFICIENCY_LABEL=this.defaultFormValues[11];
            this.EMPLOYEE_SKILLS_REQUIRED_LABEL=this.defaultFormValues[12];
            this.EMPLOYEE_SKILLS_DATE_LABEL=this.defaultFormValues[13];
            this.EMPLOYEE_SKILLS_COMMENT_LABEL=this.defaultFormValues[14];
            this.EMPLOYEE_SKILLS_CREATE_FORM_LABEL=this.defaultFormValues[15];
            this.EMPLOYEE_SKILLS_UPDATE_FORM_LABEL=this.defaultFormValues[16];
            this.EMPLOYEE_SKILLS_SEARCH_LABEL=this.defaultFormValues[17];
            this.EMPLOYEE_SKILLS_ACTION=this.defaultFormValues[18];
            this.EMPLOYEE_DEPENDENTS_ACTION=this.defaultFormValues[19];
            this.ACTIVE_EMPLOYEE_POST_DATE_PAY_RATE_ACTION=this.defaultFormValues[20];

        this.employeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.modelemployeeId);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));
        this.skillSetIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.modelskillSetId);
        }).mergeMap((token: string) => this.getSkillSetIdAsObservable(token));
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
    }
    getEmployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployeeIds.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }

    getSkillSetIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getSkillSetIds.filter((id: any) => {
                return query.test(id.skillSetId);
            })
        );
    }
    changeTypeaheadLoading(e: boolean, etype: string): void {
        this.typeaheadLoading = e;
        if (etype == 'skillset') {
            // this.skillSetIdStore = null;
        }
        if (etype == 'employee') {
            // this.model.employeeMaster["employeeId"] = null;
        }
    }

    typeaheadOnSelect(e: TypeaheadMatch, etype: string): void {
        if (etype == 'skillset') {
            this.model.skillSetSetup["id"] = e.item.id;
            // this.skillSetIdStore = e.item.id;
            this.loaderImg = true;
            this.employeeSkillsService.getSkillsBySetupId(e.item.id).then(data => {
                this.listSkillsList = data.result.records.skillIds;
                console.log('this.listSkillsList', this.listSkillsList)
                this.listSkillsList.forEach(element => {
                    element.obtained = false;
                    element.required = false;
                    element.skillExpirationDate = '';
                    element.skillComment = '';
                    element.proficiency = '';
                    element.selected = false;
                });
                this.editAndSaveArray = [];
                for (let i = 0; i < this.listSkillsList.length; i++) {

                    this.editAndSaveArray.push({
                        'id': 0,
                        'skillsId': this.listSkillsList[i].id,
                        'skillDesc': this.listSkillsList[i].skillDesc,
                        'selected': this.listSkillsList[i].selected,
                        'obtained': this.listSkillsList[i].obtained,
                        'proficiency': this.listSkillsList[i].proficiency,
                        'skillExpirationDate': this.listSkillsList[i].skillExpirationDate,
                        'required': this.listSkillsList[i].required,
                        'skillComment': this.listSkillsList[i].skillComment
                    })
                }
                this.loaderImg = false;
                console.log('this.editAndSaveArray', this.editAndSaveArray)
            })
        }
        if (etype == 'employee') {
            this.model.employeeMaster["employeeIndexId"] = e.item.employeeIndexId;
            // this.model.employeeMaster["employeeId"] = e.item.employeeId;
        }
    }
    //setting pagination
    setPage(pageInfo) {
        // Remove previous value to avoid repetation
        this.listSkillsList = [];
        // remove any selected checkbox on paging
        this.selected = [];
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.employeeSkillsService.getEmployee().then(data => {
            this.getEmployeeIds = data.result;
        });
        this.employeeSkillsService.getSkillSetIds("").then(data => {
            this.getSkillSetIds = data.result.records;
        })
        this.page.searchKeyword = '';
        this.employeeSkillsService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            console.log("pagedData:", pagedData);
            this.page.pageNumber = pagedData['result'].pageNumber;
            this.page.size = pagedData['result'].pageSize;
            this.page.sortBy = pagedData['result'].sortBy;
            this.page.sortOn = pagedData['result'].sortOn;
            this.page.totalElements = pagedData['result'].totalCount;
            this.rows = pagedData['result'].records;
            console.log("Page:", this.page);

            console.log("Rows:", this.rows);
        });
    }

    // Open form for create Employee Skills
    Create() {
        this.editAndSaveArray = [];
        this.listSkillsList = []
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.isUnderUpdateSId = false;
        this.afterFirstSave = true;
        setTimeout(() => {
            this.showCreateForm = true;
            this.modelskillSetId = "";
            this.modelemployeeId = "";
            this.doNotPrintHearder = false;
            this.loaderImg = false;
            this.checkEmpSkillValidId = false;
            this.checkEmpValidId = false;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            skillSetSetup: { "id": null },
            employeeMaster: { "employeeIndexId": null },
            listSkills: []
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm, t: NgForm) {
        f.resetForm();
        t.resetForm();
    }

    //function call for creating new Employee Skills
    CreateEmployeeSkills(f: NgForm, event: Event, t) {
        this.afterFirstSave = false;
        let createModel: any = [];
        createModel = t;
        console.log("MOdel Bind:", createModel)
        for (let i = 0; i < createModel.length; i++) {
            if (createModel[i].selected) {
                let Jdate = "";
                if (createModel[i].skillExpirationDate !== null && createModel[i].skillExpirationDate !== "") {
                    if (createModel[i].skillExpirationDate['date']) {
                        Jdate = createModel[i].skillExpirationDate.date;
                        Jdate = Jdate['year'] + "-" + Jdate['month'] + "-" + Jdate['day'];
                    }
                }
                this.model.listSkills.push({
                    "skillsSetup": { "id": createModel[i].skillsId },
                    "obtained": createModel[i].obtained,
                    "proficiency": createModel[i].proficiency,
                    "required": createModel[i].required,
                    "skillComment": createModel[i].skillComment,
                    "skillExpirationDate": Jdate,
                    'selected': createModel[i].selected
                })
            }
        }
        console.log("Inserted Data:", this.model.listSkills);
        event.preventDefault();
        if (this.model.listSkills.length == 0) {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record.';
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 1000);
            window.scrollTo(0, 0);
            return;
        }
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            let payload = {
                employeeId:this.model.employeeMaster['employeeIndexId'],
	            skillId:this.model.skillSetSetup['id']
            }
            // Check for duplicate employeeIndexId according to it create new employeeIndexId
            this.employeeSkillsService.employeeIdcheck(payload).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.scrollTo(0, 400);
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                        this.showCreateForm = true;
                    }, 100);
                } else{
                    //Call service api for Creating new Employee Skills
                    this.employeeSkillsService.createEmpSkill(this.model).then(data => {
                        this.afterFirstSave = true;
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 2000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);
        
                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new Employee Skills
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin !';
                }, 100);
            });
        }
    }

    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
        disableUntil: { day: this.minDate.getDate() - 1, month: this.minDate.getMonth() + 1, year: this.minDate.getFullYear() }
    };
    onDateChanged(event: IMyDateModel): void {
        this.skillExpirationDate = event.jsdate;
    }
    // Formated Date Converter
    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }
    //edit Employee Skills by row
    edit(row: EmployeeSkills) {
        // Remove previous value to avoid repetation
        this.showCreateForm = true;
        this.loaderImg = true;
        this.doNotPrintHearder = false;
        this.checkEmpSkillValidId = false;
        this.checkEmpValidId = false;
        this.model = Object.assign({}, row);
        this.isUnderUpdate = true;
        this.employeeIdValue = this.model['employeeId'];
        this.skillSetIdValue = this.model['skillSetId'];
        console.log("Edit Model:", this.model);
        this.employeeSkillsService.getSkillsById(this.model.id).then(data => {
            this.getEmployeeId = data.result;
            this.loaderImg = false;
            console.log("Get By ID:", this.getEmployeeId)
            this.editAndSaveArray = [];
            for (let i = 0; i < this.getEmployeeId['listSkills'].length; i++) {

                this.editAndSaveArray.push({
                    'skillsId': this.getEmployeeId['listSkills'][i].skillsSetup.id,
                    'skillDesc': this.getEmployeeId['listSkills'][i].skillsSetup.skillDesc,
                    'selected': this.getEmployeeId['listSkills'][i].selected,
                    'obtained': this.getEmployeeId['listSkills'][i].obtained,
                    'proficiency': this.getEmployeeId['listSkills'][i].proficiency,
                    'skillExpirationDate': this.formatDateFordatePicker(this.getEmployeeId['listSkills'][i].skillExpirationDate),
                    'required': this.getEmployeeId['listSkills'][i].required,
                    'skillComment': this.getEmployeeId['listSkills'][i].skillComment
                })
            }
            this.model = {
                id: this.getEmployeeId['id'],
                skillSetSetup: { "id": this.getEmployeeId['skillSetSetup'].id },
                employeeMaster: { "employeeIndexId": this.getEmployeeId['employeeMaster'].employeeIndexId },
                listSkills: []
            }
        });
        setTimeout(() => {
            window.scrollTo(0, 1000);
        }, 10);
    }

    updateStatus(t) {
        this.afterFirstSave = false;
        this.closeModal();
        this.doNotPrintHearder = false;
        console.log("Update Model Data:", this.model);
        //Call service api for updating selected Employee Skills
        this.employeeSkillsService.updateEmployeeSkills(this.model).then(data => {
            this.afterFirstSave = true;
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing Employee Skills
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 1000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 1000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 1000);
            window.scrollTo(0, 0);
        }
    }
    //delete Employee Skills by passing whole object of perticular Department
    delete() {
        var selectedDepartments = [];

        for (var i = 0; i < this.selected.length; i++) {
            selectedDepartments.push(this.selected[i].employeeSkillId);
        }
        this.employeeSkillsService.deleteEmpSkill(selectedDepartments).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 1000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of Employee Skills
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
        console.log("This Selected Value for Delete", this.selected);

    }

    // search Employee Skills by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;

    }

    // Set default page size
    changePageSize(event) {
        // Remove previous value to avoid repetation
        this.listSkillsList = [];
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        console.log("This Selected Value for Delete", this.selected);
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    // Print Screen
    print() {

        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        printContents = printContents.replace(/\r?\n|\r/g, "");
        printContents = printContents.replace(" ", "");
        printContents = printContents.replace(/<!--.*?-->/g, "");
    
        // var postedOnes = this.getElementsByIdStartsWith("print-section", "canvas", "canvasdiv-", printContents);
        // var postedOnes1 = this.getElementsByIdStartsWith("print-section", "canvas", "categorychart-", postedOnes);
        // this.getElementsByIdStartsWith("print-section", "canvas", "innerchart-", postedOnes1);
    
        printContents = document.getElementById('print-section').innerHTML;
    
        popupWin = window.open('', '_blank', 'top=0,left=0,height=400px,width=800px');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
        <link rel="stylesheet" type="text/css" media="screen,print" href="assets/css/bootstrap.min.css">
          <title>Employee Skills</title>
          <style>
            .print-tabel{disaplay:block}
          </style>
        </head>
    <body onload="">${printContents}</body>
    <script type="text/javascript">
    window.print();
    if(navigator.userAgent.match(/iPad/i)){
      window.onfocus=function(){window.close(); }
    }else{        
      window.close();
    }
    </script>
      </html>`
        );
        popupWin.document.close();
    }
    printEmployeeDetails() {
        this.doNotPrintHearder = true;
        this.noPrintTable = true;
        this.printDetails = false;

        setTimeout(() => { this.print(); }, 100);
        setTimeout(() => { this.resetAfterPrint(); }, 100);

    }
    resetAfterPrint() {
        this.noPrintTable = true;
        this.printDetails = true;
        this.doNotPrintHearder = false;
    }

    CheckValidID(eve: any, empId: any) {
        switch(empId){
        case 'employeeId':
        if (eve.target.value == "") {
          this.checkEmpValidId = false;
        } else {
          for (let i = 0; i < this.getEmployeeIds.length; i++) {
            if (this.getEmployeeIds[i].employeeId.includes(eve.target.value)) {
              this.checkEmpValidId = false;
              break;
            } else {
              this.checkEmpValidId = true;
            }
          }
        }
        break;
        case 'empSkillId':
        if(eve.target.value == ""){
          this.checkEmpSkillValidId = false;
        }else{
          for(let i = 0; i < this.getSkillSetIds.length; i++){
            if(this.getSkillSetIds[i].skillSetId.includes(eve.target.value)){
              this.checkEmpSkillValidId = false;
              break;
            }else{
              this.checkEmpSkillValidId = true;
            }
          }
        }
        break;
        }
      }

}