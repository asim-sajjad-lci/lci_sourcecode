import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { Supervisor } from '../../_models/supervisor/supervisor.module';
import { SupervisorService } from '../../_services/supervisor/supervisor.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
    selector: 'supervisor',
    templateUrl: './supervisor.component.html',
    providers: [SupervisorService, CommonService]
})
export class SupervisorComponent {
    page = new Page();
    rows = new Array<Supervisor>();
    temp = new Array<Supervisor>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1315';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    superVisionCode = {};
    searchKeyword = '';
    ddPageSize: number = 5;
    model: Supervisor;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    superVisionCodevalue: string;
    getSuperviser: any[] = [];
    getEmployee: any[] = [];
    superviserIdList: Observable<any>;
    employeeIdList: Observable<any>;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    empolyeId;
    // Label Veriable
    SUPERVISOR_CODE: any;
    SUPERVISOR_DESCRIPTION: any;
    SUPERVISOR_ARABIC_DESCRIPTION: any;
    SUPERVISOR_EMPLOYEE_ID: any;
    SUPERVISOR_EMPLOYEE_NAME: any;
    SUPERVISOR_ACTION: any;
    SUPERVISOR_CREATE_LABEL: any;
    SUPERVISOR_SAVE_LABEL: any;
    SUPERVISOR_CLEAR_LABEL: any;
    SUPERVISOR_CANCEL_LABEL: any;
    SUPERVISOR_UPDATE_LABEL: any;
    SUPERVISOR_DELETE_LABEL: any;
    SUPERVISOR_CREATE_FORM_LABEL: any;
    SUPERVISOR_UPDATE_FORM_LABEL: any;
    SUPERVISOR_SEARCH: any;
    SUPERVISOR_TABLEVIEW: any;
    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private supervisorService: SupervisorService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
		private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'SUPERVISOR_CODE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_EMPLOYEE_NAME', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_ACTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SUPERVISOR_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },

        ];
        this.SUPERVISOR_CODE = this.defaultFormValues[0];
        this.SUPERVISOR_DESCRIPTION = this.defaultFormValues[1];
        this.SUPERVISOR_ARABIC_DESCRIPTION = this.defaultFormValues[2];
        this.SUPERVISOR_EMPLOYEE_ID = this.defaultFormValues[3];
        this.SUPERVISOR_EMPLOYEE_NAME = this.defaultFormValues[4];
        this.SUPERVISOR_ACTION = this.defaultFormValues[5];
        this.SUPERVISOR_CREATE_LABEL = this.defaultFormValues[6];
        this.SUPERVISOR_SAVE_LABEL = this.defaultFormValues[7];
        this.SUPERVISOR_CLEAR_LABEL = this.defaultFormValues[8];
        this.SUPERVISOR_CANCEL_LABEL = this.defaultFormValues[9];
        this.SUPERVISOR_UPDATE_LABEL = this.defaultFormValues[10];
        this.SUPERVISOR_DELETE_LABEL = this.defaultFormValues[11];
        this.SUPERVISOR_CREATE_FORM_LABEL = this.defaultFormValues[12];
        this.SUPERVISOR_UPDATE_FORM_LABEL = this.defaultFormValues[13];
        this.SUPERVISOR_SEARCH = this.defaultFormValues[14];
        this.SUPERVISOR_TABLEVIEW = this.defaultFormValues[15];

        this.superviserIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.superVisionCode);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

        this.employeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.empolyeId);
        }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

    }

    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.supervisorService.getSuperviserId().then(data => {
            this.getSuperviser = data.result;
            // console.log("Data class options : ", data);
        });
        this.supervisorService.getEmployee().then(data => {
            this.getEmployee = data.result;
            //console.log("Data class options : "+this.getEmployee);
        });
    }
	
    getEmployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getEmployee.filter((id: any) => {
                return query.test(id.employeeId);
            })
        );
    }
	
    getSuperviserIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.getSuperviser.filter((id: any) => {
                return query.test(id.superVisionCode);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        this.model.employeeName = '';
        //console.log(e);
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        this.empolyeId = e.item.employeeId
        this.model.employeeId = e.item.employeeIndexId;
        //console.log('Selected value: ', e.item);
        if (typeof (e.item.employeeFirstName) != 'undefined')
            this.model.employeeName = e.item.employeeFirstName + ' ' + e.item.employeeMiddleName + ' ' + e.item.employeeLastName;
    }

    typeaheadOnSelect1(e: TypeaheadMatch): void {
        // console.log('Selected value: ', e);
        this.supervisorService.getSupervisor(e.item.id).then(pagedData => {
            // console.log('Get By Id Data: ', pagedData);
            this.model = pagedData.result;
            // console.log('this.model: ', this.model);
            this.empolyeId = this.model.employeeIds;
            this.model.id = 0;
        });

    }




    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.supervisorService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            superVisionCode: '',
            description: '',
            arabicDescription: '',
            employeeName: '',
            employeeId: null,
            employeeIds: ''
        };
        this.empolyeId = '';
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm();
    }

    //function call for creating new location
    CreateSupervisor(f: NgForm, event: Event) {
        event.preventDefault();
        var supIdx = this.model.superVisionCode;
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            //Check if the id is available in the model.
            //If id avalable then update the location, else Add new location.

            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.supervisorService.checkDuplicateSupervisionCode(supIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new location
                    this.supervisorService.createSupervisor(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new location
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin !';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin !';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: any) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.empolyeId = row.employeeIds;
        //console.log('employeeids',this.empolyeId);
        //console.log('row',row);
        this.superVisionCode = row.superVisionCode;
        this.isUnderUpdate = true;
        this.superVisionCodevalue = this.model.superVisionCode;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
        this.model.superVisionCode = this.superVisionCodevalue;
        this.supervisorService.updateSupervisor(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin !';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    //delete department by passing whole object of perticular Department
    delete() {
        var selectedSupervisors = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSupervisors.push(this.selected[i].id);
        }
        this.supervisorService.deleteSupervisor(selectedSupervisors).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.supervisorService.searchSupervisorlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    /*confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }*/

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    getEmployeeDetail(event) {
        if (event.target.value != "") {
            if (isNaN(event.target.value) == true) {
                this.model.employeeId = 0;
                return false;
            } else {

                this.supervisorService.getEmployeeDetail(event.target.value).then(response => {
                    var datacode = response.code;
                    if (datacode == 404) {
                        this.hasMessage = true;
                        this.message.type = 'success';

                        window.scrollTo(0, 0);
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMessage = false;
                            }, 4000);
                            this.message.text = response.btiMessage.message + ' !';
                        }, 100);
                        this.model.employeeName = '';
                    } else {
                        this.model.employeeName = response.result.records.firstName + " " + response.result.records.lastName;
                    }
                }).catch(error => {
                    this.hasMessage = true;
                    this.message.type = 'error';
                    var errorCode = error.status;
                    this.message.text = 'Server issue. Please contact admin !';
                });
            }
        } else {
            this.model.employeeName = '';
        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

}
