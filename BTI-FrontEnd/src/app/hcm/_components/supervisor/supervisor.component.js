"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../../_sharedresource/page");
var Constants_1 = require("../../../_sharedresource/Constants");
var ngx_datatable_1 = require("@swimlane/ngx-datatable");
var router_1 = require("@angular/router");
var supervisor_service_1 = require("../../_services/supervisor/supervisor.service");
var alert_service_1 = require("../../../_sharedresource/_services/alert.service");
var get_screen_detail_service_1 = require("../../../_sharedresource/_services/get-screen-detail.service");
var SupervisorComponent = (function () {
    function SupervisorComponent(router, supervisorService, getScreenDetailService, alertService) {
        this.router = router;
        this.supervisorService = supervisorService;
        this.getScreenDetailService = getScreenDetailService;
        this.alertService = alertService;
        this.page = new page_1.Page();
        this.rows = new Array();
        this.temp = new Array();
        this.selected = [];
        this.moduleCode = 'M-1011';
        this.screenCode = 'S-1315';
        this.message = { 'type': '', 'text': '' };
        this.superVisionCode = {};
        this.searchKeyword = '';
        this.ddPageSize = 5;
        this.showCreateForm = false;
        this.hasMsg = false;
        this.showMsg = false;
        this.isConfirmationModalOpen = false;
        this.confirmationModalTitle = Constants_1.Constants.confirmationModalTitle;
        this.confirmationModalBody = Constants_1.Constants.confirmationModalBody;
        this.deleteConfirmationText = Constants_1.Constants.deleteConfirmationText;
        this.OkText = Constants_1.Constants.OkText;
        this.CancelText = Constants_1.Constants.CancelText;
        this.isDeleteAction = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'SUPERVISOR_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_CODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_EMPLOYEE_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'SUPERVISOR_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
        ];
    }
    SupervisorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.setPage({ offset: 0 });
        this.currentLanguage = localStorage.getItem('currentLanguage');
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(function (data) {
            _this.moduleName = data.result.moduleName;
            _this.screenName = data.result.dtoScreenDetail.screenName;
            _this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < _this.availableFormValues.length; j++) {
                var fieldKey = _this.availableFormValues[j]['fieldName'];
                var objAvailable = _this.availableFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                var objDefault = _this.defaultFormValues.find(function (x) { return x['fieldName'] === fieldKey; });
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    };
    SupervisorComponent.prototype.setPage = function (pageInfo) {
        var _this = this;
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.supervisorService.getlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
        });
    };
    // Open form for create location
    SupervisorComponent.prototype.Create = function () {
        var _this = this;
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(function () {
            _this.showCreateForm = true;
            setTimeout(function () {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            superVisionCode: '',
            description: '',
            arabicDescription: '',
            employeeName: '',
            employeeId: 0
        };
    };
    // Clear form to reset to default blank
    SupervisorComponent.prototype.Clear = function (f) {
        f.resetForm();
    };
    //function call for creating new location
    SupervisorComponent.prototype.CreateSupervisor = function (f, event) {
        var _this = this;
        event.preventDefault();
        var supIdx = this.model.superVisionCode;
        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.supervisorService.checkDuplicateSupervisionCode(supIdx).then(function (response) {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    _this.duplicateWarning = true;
                    _this.message.type = 'success';
                    window.setTimeout(function () {
                        _this.isSuccessMsg = false;
                        _this.isfailureMsg = true;
                        _this.showMsg = true;
                        window.setTimeout(function () {
                            _this.showMsg = false;
                            _this.duplicateWarning = false;
                        }, 4000);
                        _this.message.text = response.btiMessage.message + ' !';
                    }, 100);
                }
                else {
                    //Call service api for Creating new location
                    _this.supervisorService.createSupervisor(_this.model).then(function (data) {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(function () {
                                _this.isSuccessMsg = true;
                                _this.isfailureMsg = false;
                                _this.showMsg = true;
                                window.setTimeout(function () {
                                    _this.showMsg = false;
                                    _this.hasMsg = false;
                                }, 4000);
                                _this.showCreateForm = false;
                                _this.messageText = data.btiMessage.message;
                                ;
                            }, 100);
                            _this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new location
                            _this.setPage({ offset: 0 });
                        }
                    }).catch(function (error) {
                        _this.hasMsg = true;
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            _this.messageText = 'Server error. Please contact admin !';
                        }, 100);
                    });
                }
            }).catch(function (error) {
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.isSuccessMsg = false;
                    _this.isfailureMsg = true;
                    _this.showMsg = true;
                    _this.messageText = 'Server error. Please contact admin !';
                }, 100);
            });
        }
    };
    //edit department by row
    SupervisorComponent.prototype.edit = function (row) {
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        this.superVisionCode = row.superVisionCode;
        this.isUnderUpdate = true;
        this.superVisionCodevalue = this.model.superVisionCode;
        setTimeout(function () {
            window.scrollTo(0, 2000);
        }, 10);
    };
    SupervisorComponent.prototype.updateStatus = function () {
        var _this = this;
        this.closeModal();
        //Call service api for updating selected department
        this.model.superVisionCode = this.superVisionCodevalue;
        this.supervisorService.updateSupervisor(this.model).then(function (data) {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                _this.setPage({ offset: 0 });
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(function () {
                    _this.isSuccessMsg = true;
                    _this.isfailureMsg = false;
                    _this.showMsg = true;
                    window.setTimeout(function () {
                        _this.showMsg = false;
                        _this.hasMsg = false;
                    }, 4000);
                    _this.messageText = data.btiMessage.message;
                    ;
                    _this.showCreateForm = false;
                }, 100);
                _this.hasMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMsg = false;
                }, 4000);
            }
        }).catch(function (error) {
            _this.hasMsg = true;
            window.setTimeout(function () {
                _this.isSuccessMsg = false;
                _this.isfailureMsg = true;
                _this.showMsg = true;
                _this.messageText = 'Server error. Please contact admin !';
            }, 100);
        });
    };
    SupervisorComponent.prototype.varifyDelete = function () {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        }
        else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    };
    //delete department by passing whole object of perticular Department
    SupervisorComponent.prototype.delete = function () {
        var _this = this;
        var selectedSupervisors = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSupervisors.push(this.selected[i].id);
        }
        this.supervisorService.deleteSupervisor(selectedSupervisors).then(function (data) {
            var datacode = data.code;
            if (datacode == 200) {
                _this.setPage({ offset: 0 });
            }
            _this.hasMessage = true;
            _this.message.type = 'success';
            //this.message.text = data.btiMessage.message + " !";
            window.scrollTo(0, 0);
            window.setTimeout(function () {
                _this.isSuccessMsg = true;
                _this.isfailureMsg = false;
                _this.showMsg = true;
                window.setTimeout(function () {
                    _this.showMsg = false;
                    _this.hasMessage = false;
                }, 4000);
                _this.message.text = data.btiMessage.message + ' !';
            }, 100);
            //Refresh the Grid data after deletion of department
            _this.setPage({ offset: 0 });
        }).catch(function (error) {
            _this.hasMessage = true;
            _this.message.type = 'error';
            var errorCode = error.status;
            _this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    };
    // default list on page
    SupervisorComponent.prototype.onSelect = function (_a) {
        var selected = _a.selected;
        this.selected.splice(0, this.selected.length);
        (_b = this.selected).push.apply(_b, selected);
        var _b;
    };
    // search department by keyword
    SupervisorComponent.prototype.updateFilter = function (event) {
        var _this = this;
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.supervisorService.searchSupervisorlist(this.page, this.searchKeyword).subscribe(function (pagedData) {
            _this.page = pagedData.page;
            _this.rows = pagedData.data;
            _this.table.offset = 0;
        });
    };
    // Set default page size
    SupervisorComponent.prototype.changePageSize = function (event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    };
    /*confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }*/
    SupervisorComponent.prototype.closeModal = function () {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    };
    SupervisorComponent.prototype.getEmployeeDetail = function (event) {
        var _this = this;
        if (event.target.value != "") {
            if (isNaN(event.target.value) == true) {
                this.model.employeeId = 0;
                return false;
            }
            else {
                this.supervisorService.getEmployeeDetail(event.target.value).then(function (response) {
                    var datacode = response.code;
                    if (datacode == 404) {
                        _this.hasMessage = true;
                        _this.message.type = 'success';
                        window.scrollTo(0, 0);
                        window.setTimeout(function () {
                            _this.isSuccessMsg = false;
                            _this.isfailureMsg = true;
                            _this.showMsg = true;
                            window.setTimeout(function () {
                                _this.showMsg = false;
                                _this.hasMessage = false;
                            }, 4000);
                            _this.message.text = response.btiMessage.message + ' !';
                        }, 100);
                        _this.model.employeeName = '';
                    }
                    else {
                        _this.model.employeeName = response.result.records.firstName + " " + response.result.records.lastName;
                    }
                }).catch(function (error) {
                    _this.hasMessage = true;
                    _this.message.type = 'error';
                    var errorCode = error.status;
                    _this.message.text = 'Server issue. Please contact admin !';
                });
            }
        }
        else {
            this.model.employeeName = '';
        }
    };
    return SupervisorComponent;
}());
__decorate([
    core_1.ViewChild(ngx_datatable_1.DatatableComponent),
    __metadata("design:type", ngx_datatable_1.DatatableComponent)
], SupervisorComponent.prototype, "table", void 0);
__decorate([
    core_1.ViewChild('target'),
    __metadata("design:type", core_1.ElementRef)
], SupervisorComponent.prototype, "myScrollContainer", void 0);
SupervisorComponent = __decorate([
    core_1.Component({
        selector: 'supervisor',
        templateUrl: './supervisor.component.html',
        providers: [supervisor_service_1.SupervisorService]
    }),
    __metadata("design:paramtypes", [router_1.Router,
        supervisor_service_1.SupervisorService,
        get_screen_detail_service_1.GetScreenDetailService,
        alert_service_1.AlertService])
], SupervisorComponent);
exports.SupervisorComponent = SupervisorComponent;
//# sourceMappingURL=supervisor.component.js.map