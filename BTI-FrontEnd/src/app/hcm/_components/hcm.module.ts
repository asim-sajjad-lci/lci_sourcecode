
import { NgModule, Directive, ElementRef, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';

import { DepartmentComponent } from './department/department.component';
import { DivisionComponent } from './division/division.component';
import { PositionClassComponent } from './position-class/position-class.component';
import { LocationComponent } from './location/location.component';
import { SupervisorComponent } from './supervisor/supervisor.component';
import { PositionSetupComponent } from './position-setup/position-setup.component';
import { PositionPlanSetupComponent } from './position-plan-setup/position-plan-setup.component';
import { PositionTrainingComponent } from './position-training/position-training.component';
import { PositionAttachmentComponent } from './position-attachment/position-attachment.component';
import { PositionPayCodeComponent } from './position-pay-code/position-pay-code.component';
import { PositionBudgetComponent } from './position-budget/position-budget.component';
import { AttendanceSetupComponent } from './attendance-setup/attendance-setup.component';
import { AttendanceSetupOptionsComponent } from './attendance-setup-options/attendance-setup-options.component';
import { DeductionCodeSetupComponent } from './deduction-code-setup/deduction-code-setup.component';
import { AccrualSetupComponent } from './accrual-setup/accrual-setup.component';
import { HealthCoverageTypeSetupComponent } from './health-coverage-type-setup/health-coverage-type-setup.component';
import { SkillsSetupComponent } from './skills-setup/skills-setup.component';
import { BenefitCodeSetupComponent } from './benefit-code-setup/benefit-code-setup.component';
import { HealthInsuranceSetupComponent } from './health-insurance-setup/health-insurance-setup.component';
import { AccrualMainCodeSetupComponent } from './accrual-setup-main/accrual-setup-main.component';
import { RetirementPlanSetupComponent } from './retirement-plan-setup/retirement-plan-setup.component';
import { TimeCodeComponent } from './time-code/time-code.component';
import { AccrualScheduleSetupComponent } from './accrual-schedule-setup/accrual-schedule-setup.component';
import { SalaryMatrixSetupComponent } from './salary-matrix-setup/salary-matrix-setup.component';
import { ShiftCodeSetupComponent } from './shift-code-setup/shift-code-setup.component';
import { SkillSetSetupComponent } from './skill-set-setup/skill-set-setup.component';
import { MiscellaneousBenefitsComponent } from './miscellaneous-benefits/miscellaneous-benefits.component';
import { TerminationSetupComponent } from './termination-setup/termination-setup.component';
import { LifeInsuranceSetupComponent } from './life-insurance-setup/life-insurance-setup.component';
import { HealthInsSetupComponent } from './health-ins-setup/health-ins-setup.component';
import { PredefineChecklistComponent } from './predefine-checklist/predefine-checklist.component';
import { PaycodeSetupComponent } from './paycode-setup/paycode-setup.component';
import { ExitInterviewComponent } from './exit-interview/exit-interview.component';
import { RequisitionsSetupComponent } from './requisitions-setup/requisitions-setup.component';
import { BenefitPreferencesComponent } from './benefit-preferences/benefit-preferences.component';
import { TrainingCourseComponent } from './training-course/training-course.component';
import { TrainingBatchSignupComponent } from './training-batch-signup/training-batch-signup.component';
import { OrientationSetupComponent } from './orientation-setup/orientation-setup.component';
import { InterviewTypeSetupComponent } from './interview-type-setup/interview-type-setup.component';
import { PayScheduleSetupComponent } from './pay-schedule-setup/pay-schedule-setup.component';
import { ClassSkillComponent } from './class-skill/class-skill.component';
import { ClassEnrollmentComponent } from './class-enrollment/class-enrollment.component';
import { HcmRoutingModule } from './hcm-routing.module';
// import { AccountStatementReportComponent } from './account-statement-report/account-statement-report.component';
// import { AccountReportComponent } from './account-report/account-report.component';

import { SecurePipe } from '../../_sharedresource/secure.pipe';
import { CustomMaxDirective } from '../../_sharedresource/custom-max.directive';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SharedModule } from '../../shared/shared.module';

// import { FieldAccessComponent } from "../../userModule/_components/fields/fieldAccess.component";
// import { FieldByCompanyManagementComponent } from '../../userModule/_components/fields/field-by-company-management.component';
// import { MainHeaderComponent } from '../../_sharedcomponent/main-header.component';
import { EmployeeMasterComponent } from './employee-master/employee-master.component';
import { UploadFileService } from '../../_sharedresource/_services/upload-file.service';
import { BuildCheckStorageService } from '../../_sharedresource/_services/build-check-storage.service';
import { EmployeeDeductionMaintenanceComponent } from './employee-deduction-maintenance/employee-deduction-maintenance.component';
import { EmployeeBenefitMaintenanceComponent } from './employee-benefit-maintenance/employee-benefit-maintenance.component';
import { HealthInsuranceEnrollmentComponent } from './health-insurance-enrollment/health-insurance-enrollment.component';
import { EmployeePayCodeMaintenanceComponent } from './employee-pay-code-maintenance/employee-pay-code-maintenance.component';
import { MiscellaneousBenefitsEnrollmentComponent } from './miscellaneous-benefits-enrollment/miscellaneous-benefits-enrollment.component';
import { EmployeePostDatedPayrateComponent } from './employee-post-dated-payrate/employee-post-dated-payrate.component';
import { EmployeeQuickAssignmentComponent } from './employee-quick-assignment/employee-quick-assignment.component';
import { EmployeeNationalityComponent } from './employee-nationality/employee-nationality.component';
import { EmployeeEducationComponent } from './employee-education/employee-education.component';
import { EmployeeDependentsComponent } from './employee-dependents/employee-dependents.component';
import { EmployeeSkillsComponent } from './employee-skills/employee-skills.component';
import { EmployeeDirectDepositComponent } from './employee-direct-deposit/employee-direct-deposit.component';
import { ReportComponent } from './reports/reports.component';
// import { AccountStatementReportComponent } from './account-statement-report/account-statement-report.component';
// import { AccountReportComponent } from './account-report/account-report.component';
import { EmployeeContactsComponent } from './employee-contacts/employee-contacts.component';
import { EmployeeAddressMasterComponent } from './employee-address-master/employee-address-master.component';
import { EmployeePositionHistoryComponent } from './employee-position-history/employee-position-history.component';
import { BatchesComponent } from './batches/batches.component';
import { TransactionEntryComponent } from './transaction-entry/transaction-entry.component';
import { BuildChecksComponent } from './build-checks/build-checks.component';
import { BuildPayrollCheckComponentPayCode } from './build-payroll-check-paycode/build-payroll-check-paycode.component';
import { BuildPayrollCheckDeductionsComponent } from './build-payroll-check-deductions/build-payroll-check-deductions.component';
import { BuildPayrollCheckBenefitsComponent } from './build-payroll-check-benefits/build-payroll-check-benefits.component';
import { BuildPayrollCheckBatchesComponent } from './build-payroll-check-batches/build-payroll-check-batches.component';
import { ActiveEmployeePostDatedPayrateComponent } from './active-employee-post-dated-payrate/active-employee-post-dated-payrate.component';
import { CalculateChecksComponent } from './calculate-checks/calculate-checks.component';
import { BuildCheckReportComponent } from './buildcheck-report/buildcheck-report.component';
import { BuildPayrollCheckDefaultComponent } from './build-payroll-check-default-setup/build-payroll-check-default-setup.component';
import { BankStatementReportComponent } from './bank-statement-report/bank-statement-report.component';
import { PayrollStatementReportComponent } from './payroll-statement-report/payroll-statement-report.component';
import { PayslipReportComponent } from './payslip-report/payslip-report.component';
import { HCMSidebarComponent } from './hcm-sidebar.component';
import { PayrollCodeModifierComponent } from './payroll-code-modifier/payroll-code-modifier.component';
import { PayrollYearEndClosingComponent } from './payroll-year-end-closing/payroll-year-end-closing.component';
import { ReconcileEmployeeInfoComponent } from './reconcile-employee-info/reconcile-employee-info.component';
import { EmployeeMassUpdateComponent } from './employee-mass-update/employee-mass-update.component';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { MiscellaneousSetupComponent } from './miscellaneous-setup/miscellaneous-setup.component';
import { MiscellaneousSetupValuesComponent } from './miscellaneous-setup-values/miscellaneous-setup-values.component';
import { ProjectSetupComponent } from './project-setup/project-setup.component';
// import { AccountStructureComponent } from './account-structure/account-structure.component';
// import { AccountStructureSegmentComponent } from './account-structure-segment/account-structure-segment.component';
import { LoanAndAdvanceComponent } from './loan-and-advance/loan-and-advance.component';
import { LoanPackageSetupComponent } from './loan-package-setup/loan-package-setup.component';
import { LoanESBSetupComponent } from './loan-esbsetup/loan-esbsetup.component';



@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    HcmRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule,
    TypeaheadModule,
    SharedModule,
    Ng2AutoCompleteModule
    // FieldAccessComponent,
    // FieldByCompanyManagementComponent,
    // MainHeaderComponent,
  ],
  // /*
  declarations: [
    DepartmentComponent,
    DivisionComponent,
    LocationComponent,
    PositionClassComponent,
    SupervisorComponent,
    PositionSetupComponent,
    PositionPlanSetupComponent,
    PositionTrainingComponent,
    PositionAttachmentComponent,
    PositionPayCodeComponent,
    PositionBudgetComponent,
    AttendanceSetupComponent,
    AttendanceSetupOptionsComponent,
    DeductionCodeSetupComponent,
    AccrualSetupComponent,
    AccrualScheduleSetupComponent,
    HealthCoverageTypeSetupComponent,
    SkillsSetupComponent,
    SkillSetSetupComponent,
    BenefitCodeSetupComponent,
    HealthInsuranceSetupComponent,
    TimeCodeComponent,
    SalaryMatrixSetupComponent,
    HealthInsuranceSetupComponent,
    TimeCodeComponent,
    LifeInsuranceSetupComponent,
    AccrualMainCodeSetupComponent,
    RetirementPlanSetupComponent,
    TimeCodeComponent,
    MiscellaneousBenefitsComponent,
    TerminationSetupComponent,
    HealthInsSetupComponent,
    ShiftCodeSetupComponent,
    PredefineChecklistComponent,
    PaycodeSetupComponent,
    ExitInterviewComponent,
    RequisitionsSetupComponent,
    BenefitPreferencesComponent,
    TrainingCourseComponent,
    TrainingBatchSignupComponent,
    OrientationSetupComponent,
    InterviewTypeSetupComponent,
    PayScheduleSetupComponent,
    OrientationSetupComponent,
    ClassSkillComponent,
    ClassEnrollmentComponent,
    AutoFocusDirective,
    EmployeeMasterComponent,
    EmployeeDeductionMaintenanceComponent,
    EmployeeBenefitMaintenanceComponent,
    HealthInsuranceEnrollmentComponent,
    EmployeePayCodeMaintenanceComponent,
    MiscellaneousBenefitsEnrollmentComponent,
    EmployeePostDatedPayrateComponent,
    EmployeeQuickAssignmentComponent,
    EmployeeNationalityComponent,
    EmployeeEducationComponent,
    EmployeeDependentsComponent,
    EmployeeSkillsComponent,
    EmployeeDirectDepositComponent,
    ReportComponent,
    EmployeeContactsComponent,
    BatchesComponent,
    // AccountReportComponent,
    ReportComponent,
    EmployeeAddressMasterComponent,
    EmployeePositionHistoryComponent,
    // AccountStatementReportComponent,
    TransactionEntryComponent,
    BuildChecksComponent,
    BuildPayrollCheckComponentPayCode,
    BuildPayrollCheckDeductionsComponent,
    BuildPayrollCheckBenefitsComponent,
    BuildPayrollCheckBatchesComponent,
    ActiveEmployeePostDatedPayrateComponent,
    CalculateChecksComponent,
    BuildCheckReportComponent,
    BuildPayrollCheckDefaultComponent,
    BankStatementReportComponent,
    PayrollStatementReportComponent,
    PayslipReportComponent,
    CustomMaxDirective,
    SecurePipe,
    HCMSidebarComponent,
    // NoCommaPipe,
    PayrollCodeModifierComponent,
    PayrollYearEndClosingComponent,
    ReconcileEmployeeInfoComponent,
    EmployeeMassUpdateComponent,
    MiscellaneousSetupComponent,
    MiscellaneousSetupValuesComponent,
    ProjectSetupComponent,
    // AccountStructureComponent,
    // AccountStructureSegmentComponent,
    LoanAndAdvanceComponent,
    LoanPackageSetupComponent,
    LoanESBSetupComponent
  ],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
  // */
})
export class HcmModule { }
