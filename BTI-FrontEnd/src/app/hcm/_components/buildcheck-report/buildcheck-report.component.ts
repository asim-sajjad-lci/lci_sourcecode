import { Component, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BuildCheckReportModule } from '../../_models/buildcheck-report/buildcheck-report';
import { BuildCheckReportService } from '../../_services/buildcheck-report/buildcheck-report.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { BuildCheckCommonService } from '../../../_sharedresource/_services/build-check-common.service';
@Component({
    selector: 'buildcheck-report',
    templateUrl: './buildcheck-report.component.html',
    providers: [BuildCheckReportService]
})

// export Department component to make it available for other classes
export class BuildCheckReportComponent {
    page = new Page();
    rows = new Array<BuildCheckReportModule>();
    temp = new Array<BuildCheckReportModule>();
    selected = [];
    moduleCode = "M-1011";
    screenCode = "S-1471";
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    departmentId = {};
    searchKeyword = '';
	getDepartment:any[]=[];
    ddPageSize: number = 5;
    model: BuildCheckReportModule;
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    departmentIdvalue : number;
	batchIdList: Observable<any>;
	typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    modelStartDate;
    modelEndDate;
    frmStartDate;
    frmEndDate;
	startDate;
    endDate;
    employeeId;
    emdescription;
    CodeId;
    codeDesc;
    batchDropdownList=[];
    batchId;
    bId;
    defaultId;
    chkNum;
    auditTrNum;
    empName;
    depId;
    depDesc;
    projectId;
    trType;
    projDesc;
    codeId;
    transDataList:Observable<any>;
    transDeductionDataList:Observable<any>;
    transBenefitDataList:Observable<any>;
    employeeIdList:Observable<any>;
    transTypeIdList=[];
    employeeIdOption=[];
    codeType=["","Pay Code","Deduction","Benefit"]
    isDeleteBatch:boolean=false;
	error:any={isError:false,errorMessage:''};
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    postingTypes: any[] = [];

    constructor(
        private router: Router,
        private buildCheckReportService: BuildCheckReportService,
        private getScreenDetailService: GetScreenDetailService,
        private buildCheckCommonService: BuildCheckCommonService,
        private alertService: AlertService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        //default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'CALCULATE_CHECKS_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAIS_PRINT_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_COMPLETE_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_TRANSECTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_DISPLAY_BY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_CODE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_TRANSECTION_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAIS_PROJECT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_EMPLOYEE_NAME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_AUDIT_TRANSECTION_NUMER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_CHECK_NUMBER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_DEFAULT_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_CREDIT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DEBIT', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_ACCOUNT_DISCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_POSTING_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'CALCULATE_CHECKS_DETAILS_ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '' },
           
            
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_UPDATE', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_EDIT', 'fieldValue': '', 'helpMessage': '' },
            // { 'fieldName': 'ACTIVE _EMPLOYEE_POST_DATE_PAY_RATE_ACTIONL', 'fieldValue': '', 'helpMessage': '' },
        ];
		this.batchIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.batchId);
          }).mergeMap((token: string) => this.getBatchIdAsObservable(token));

          this.transDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
          }).mergeMap((token: string) => this.getpayCodeIdAsObservable(token));


          this.transDeductionDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
          }).mergeMap((token: string) => this.getdeductionCodeIdAsObservable(token));

          this.transBenefitDataList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.CodeId);
          }).mergeMap((token: string) => this.getbenefitCodeIdAsObservable(token));

          this.employeeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.employeeId);
          }).mergeMap((token: string) => this.getemployeeIdAsObservable(token));
    }

    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        let reportDetail = [];
        console.log("this.buildCheckCommonService.buildReport", this.buildCheckCommonService.buildReport);
        reportDetail=JSON.parse(localStorage.getItem('reports'));
        console.log(reportDetail);
        this.rows=reportDetail;
        //Following shifted from SetPage for better performance
        this.buildCheckReportService.getDepartments().then(data => {
            this.getDepartment = data.result;
            //console.log("Data class options : "+this.getDepartment);
        });
        this.defaultId = reportDetail[0].defaultId;
        this.chkNum = reportDetail[0].checkNumber;
        this.auditTrNum = reportDetail[0].auditTransactionNo;
        this.empName = reportDetail[0].employeeFirstName;
        this.employeeId = reportDetail[0].employeeId;
        this.depId = reportDetail[0].departmentId;
        this.depDesc = reportDetail[0].description;
        this.projectId = reportDetail[0].projectId;
        this.depDesc=reportDetail[0].departmentDescription;
        this.projDesc=reportDetail[0].employeeFirstName;
        this.codeId=reportDetail[0].codeId;
        
        // this.trType = reportDetail[0].
        
        this.trType=this.codeType[reportDetail[0].transactionType];
        this.Create();
    }
   getBatchIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
          this.batchDropdownList.filter((id: any) => {
            return query.test(id.batchId);
          })
        );
      }
      getpayCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
          this.transTypeIdList.filter((id: any) => {
            return query.test(id.payCodeId);
          })
        );
      }

      getdeductionCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
          this.transTypeIdList.filter((id: any) => {
            return query.test(id.diductionId);
          })
        );
      }

      getbenefitCodeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
          this.transTypeIdList.filter((id: any) => {
            return query.test(id.benefitId);
          })
        );
      }

      getemployeeIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
          this.employeeIdOption.filter((id: any) => {
            return query.test(id.employeeId);
          })
        );
      }
     
      changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
      }
     
      typeaheadOnSelect(e: TypeaheadMatch): void {
       //console.log('Selected value: ', e.value);
       this.model.batches={id:e.item.id}
       this.bId=e.item.id;
       console.log('batches',JSON.stringify(this.model));
       this.model.description=e.item.description;
       this.model.arabicDescription=e.item.arabicDescription;
       this.buildCheckReportService.getTransctionEntryByBatch(this.page,e.item.id).then(data => 
            {
                this.rows=data.result.records;  
            }
        )
        this.postingTypes = []
        for(let i = 0; i < this.rows.length; i++){
            if(this.rows[i]['postingTypes'] == 1){
                this.postingTypes.push('Pay Code');
            }else if(this.rows[i]['postingTypes'] == 2){
                this.postingTypes.push('Deduction');
            }else{
                this.postingTypes.push('Benefit');
            }
        }
      } 

      typeaheadPayCodeOnSelect(e: TypeaheadMatch): void {

        this.model.transactionEntryDetail['codeId']=e.item.id;
        this.model.transactionEntryDetail['payRate']=e.item.payRate;
        this.model.transactionEntryDetail['amount']= e.item.baseOnPayCodeAmount;
        this.codeDesc = e.item.description;
        console.log('paycode',this.codeDesc);
      }

      typeaheadDeductionCodeOnSelect(e: TypeaheadMatch): void {

        this.model.transactionEntryDetail['codeId']=e.item.id;
        this.model.transactionEntryDetail['amount']=e.item.amount;
        this.codeDesc = e.item.discription;
        console.log('deduction',this.codeDesc);
      }


      typeaheadBenefitCodeOnSelect(e: TypeaheadMatch): void {

        this.model.transactionEntryDetail['codeId']=e.item.id;
        this.model.transactionEntryDetail['amount']=e.item.amount;
        this.codeDesc = e.item.desc;
        console.log('benefit',this.codeDesc);
      }

      typeaheadEmployeeOnSelect(e: TypeaheadMatch): void {

        this.model.transactionEntryDetail["employeeMaster"].employeeIndexId=e.item.employeeIndexId;
        this.emdescription = e.item.employeeFirstName;
        console.log('employee',this.model);
      }
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
		
        this.page.searchKeyword = '';
        // this.transactionEntryService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        // });
        this.buildCheckReportService.getAllBatch(this.searchKeyword).then(data => {
            this.batchDropdownList = data.result;
        });

        this.buildCheckReportService.getEmployeeId(this.page).then(data => 
            {
                this.employeeIdOption = data.result.records;
            }
        );
    }
    backToCalculateCheck(){
        this.router.navigate(['hcm/calculateChecks']);
    }
    // Open form for create department
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            batches:{id:0},
            entryNumber: '',
            entryDate:'',
            description:'',
            arabicDescription:'',

            
            transactionEntryDetail:{

                employeeMaster:{
                    employeeIndexId:0
                },
                transactionType:0,
                codeId:null,
                amount:null,
                fromDate:'',
                toDate:'',
                payRate:null
            },
           

        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        f.resetForm({transactionType:0});
    }

    //function call for creating new department
    CreateTransactionEntry(f: NgForm, event: Event) {
        event.preventDefault();
        this.model.transactionEntryDetail['fromDate']=this.frmStartDate;
        this.model.transactionEntryDetail['toDate'] =this.frmEndDate;
        var deptIdx = this.model.id;

        //Check if the id is available in the model.
        //If id avalable then update the department, else Add new department.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Division Id according to it create new division
            this.buildCheckReportService.checkDuplicateDeptId(deptIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = "success";

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else {
                    //Call service api for Creating new department
                    this.buildCheckReportService.createTransaction(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMessage = false;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            this.buildCheckReportService.getTransctionEntryByBatch(this.page,this.bId).then(data => 
                                {
                                    this.rows=data.result.records;  
                                }
                            )
                            f.resetForm();
                            //Refresh the Grid data after adding new department
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = "Server error. Please contact admin.";
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = "Server error. Please contact admin.";
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: BuildCheckReportModule) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.isUnderUpdate = true;
        this.departmentId = row.id;
        this.departmentIdvalue = this.model.id;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }

    transType(event){
        if(event==1){
            this.buildCheckReportService.getPayCodeId(this.page).then(data => 
                {   
                    this.CodeId='';
                    this.codeDesc='';
                    this.model.transactionEntryDetail['amount']=null;
                    this.model.transactionEntryDetail['payRate']=null;
                    this.transTypeIdList=[];
                    this.transTypeIdList=data.result.records;
                    console.log('transtpe',this.model);
                }
            
            )

            
        }

        if(event==2){
            this.buildCheckReportService.getDeductionCodeId(this.page).then(data => 
                {
                    this.model.transactionEntryDetail['amount']=null;
                    this.model.transactionEntryDetail['payRate']=null;
                    this.CodeId='';
                    this.codeDesc='';
                    this.transTypeIdList=[];
                    this.transTypeIdList=data.result.records;
                }
            
            )

            
        }

        if(event==3){
            this.buildCheckReportService.getBenefitCodeId(this.page).then(data => 
                {
                    this.model.transactionEntryDetail['amount']=null;
                    this.model.transactionEntryDetail['payRate']=null;
                    this.CodeId='';
                    this.codeDesc='';
                    this.transTypeIdList=[];
                    this.transTypeIdList=data.result.records;
                }
            
            )

            
        }

    }

    updateStatus() {
        this.closeModal();
        //Call service api for updating selected department
        this.model.id = this.departmentIdvalue;
        this.buildCheckReportService.updateDepartment(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    this.showCreateForm = false;
                }, 100);
                this.hasMessage = false;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = true;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    varifyDeleteBatch() {
       
            this.showCreateForm = true;
            this.isDeleteBatch = true;
            this.isConfirmationModalOpen = true;
            window.scrollTo(0, 0);
        }
  
        deleteBatch() {
           
            this.buildCheckReportService.deleteTransactionEntryBatch(this.bId).then(data => {
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                    this.buildCheckReportService.getTransctionEntryByBatch(this.page,this.bId).then(data => 
                        {
                            this.rows=data.result.records;  
                        }
                    )
                }
                this.hasMessage = true;
                this.message.type = "success";
                //this.message.text = data.btiMessage.message;
    
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMessage = false;
                    }, 4000);
                    this.message.text = data.btiMessage.message;
                }, 100);
    
                //Refresh the Grid data after deletion of department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.buildCheckReportService.getTransctionEntryByBatch(this.page,this.bId).then(data => 
                    {
                        this.rows=data.result.records;  
                    }
                )
            }).catch(error => {
                this.hasMessage = true;
                this.message.type = "error";
                var errorCode = error.status;
                this.message.text = "Server issue. Please contact admin.";
            });
            this.closeModal();
        }
    
    //delete department by passing whole object of perticular Department
    delete() {
        var selectedTransaction = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedTransaction.push(this.selected[i].id);
        }
        this.buildCheckReportService.deleteTransactionEntryByrow(selectedTransaction).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                this.buildCheckReportService.getTransctionEntryByBatch(this.page,this.bId).then(data => 
                    {
                        this.rows=data.result.records;  
                    }
                )
            }
            this.hasMessage = true;
            this.message.type = "success";
            //this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.buildCheckReportService.getTransctionEntryByBatch(this.page,this.bId).then(data => 
                {
                    this.rows=data.result.records;  
                }
            )
        }).catch(error => {
            this.hasMessage = true;
            this.message.type = "error";
            var errorCode = error.status;
            this.message.text = "Server issue. Please contact admin.";
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword 
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
    
    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd-mm-yyyy',
    };

    onStartDateChanged(event: IMyDateModel): void {
        this.frmStartDate = event.jsdate;
        this.startDate = event.epoc;
       //console.log('this.frmStartDate', event);
		if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:'Invalid End Date.'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
       if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
    }

   onEndDateChanged(event: IMyDateModel): void {
        this.frmEndDate = event.jsdate;
        this.endDate = event.epoc;
      //console.log('this.frmEndDate', this.frmEndDate, event);
	  //console.log(this.endDate);
	  //console.log(this.frmStartDate);
         if((this.startDate > this.endDate) && this.endDate!=undefined){
            this.error={isError:true,errorMessage:' Invalid End Date'};
        }
        if(this.startDate <= this.endDate){
            this.error={isError:false,errorMessage:''};
        }
		if(this.endDate==undefined && this.startDate==undefined){
			 this.error={isError:false,errorMessage:''};
		}
        //this.frmEndDate = event.jsdate;
        //this.frmEndDate = event.jsdate;
        //console.log('>>>>>>>>>>', f.controls.['frmEndDate']);
        //console.log('<<<<<<<<<<<<', f.controls['frmStartDate'].value);
        //if(f.controls['frmEndDate'].value.formatted < f.controls['frmStartDate'].value.formatted){
            //this.error={isError:true,errorMessage:' Invalid End Date'};
        // }
    }

    formatDateFordatePicker(strDate: Date): any {
        if (strDate != null) {
            var setDate = new Date(strDate);
            return { date: { year: setDate.getFullYear(), month: setDate.getMonth()+1, day: setDate.getDate() } };
        } else {
            return null;
        }
    }

}