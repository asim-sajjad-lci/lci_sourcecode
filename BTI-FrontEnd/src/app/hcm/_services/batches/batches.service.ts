/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Batch, getAllBatch } from "../../_models/batches/batches";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class BatchService {
    private headers = new Headers({ 'content-type': 'application/json' });    
    private createBatchUrl = Constants.hcmModuleApiBaseUrl + 'batches/create';
    private updateBatchUrl = Constants.hcmModuleApiBaseUrl + 'batches/update';
    private getAllBatchesUrl = Constants.hcmModuleApiBaseUrl + 'batches/getAll';
    private deleteBatchUrl = Constants.hcmModuleApiBaseUrl + 'batches/delete';
    private checkBatchIdx = Constants.hcmModuleApiBaseUrl + 'batches/batcheIdcheck';
    private getuserByIdUrl = Constants.userModuleApiBaseUrl +'user/getUserDetailByUserId';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new department
    createBatch(batch: Batch) {
        return this.http.post(this.createBatchUrl, JSON.stringify(batch), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateBatch(batch: Batch) {
        return this.http.post(this.updateBatchUrl, JSON.stringify(batch), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getUser(userId) {
        return this.http.post(this.getuserByIdUrl, {userId:userId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteBatch(ids: any) {
        return this.http.put(this.deleteBatchUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    //check for duplicate ID department
    checkDuplicateBatchId(batchId: any) {
        return this.http.post(this.checkBatchIdx, { 'batchId': batchId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getlist(page: Page, searchKeyword): Observable<PagedData<getAllBatch>> {
        return this.http.post(this.getAllBatchesUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getAllPagedData(page, data.json().result));
    }

    private getPagedData(page: Page, data: any): PagedData<Batch> {
        let pagedData = new PagedData<Batch>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let batch = new Batch(
                        jsonObj.id,
                        jsonObj.batchId,
                        jsonObj.description,
                        jsonObj.arabicDescription,
                        jsonObj.transactionType,
                        jsonObj.postingDate,
                        jsonObj.quantityTotal,
                        jsonObj.totalTransactions,
                        jsonObj.approved,
                        jsonObj.userID,
                        jsonObj.approvedDate
                    );
                    pagedData.data.push(batch);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }
    
    private getAllPagedData(page: Page, data: any): PagedData<getAllBatch> {
        let pagedData = new PagedData<getAllBatch>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let getAllbatch = new getAllBatch(
                        jsonObj.id,
                        jsonObj.batchId,
                        jsonObj.description,
                        jsonObj.arabicDescription,
                        jsonObj.transactionType,
                        jsonObj.postingDate,
                        jsonObj.quantityTotal,
                        jsonObj.totalTransactions,
                        jsonObj.approved,
                        jsonObj.userID,
                        jsonObj.approvedDate,
                        jsonObj.status
                    );
                    pagedData.data.push(getAllbatch);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
