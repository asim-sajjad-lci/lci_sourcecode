/**
 * A service class for Attendance Setup Options
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { AttendanceSetupOptions } from "../../_models/attendance-setup-options/attendance-setup-options.module" ;
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class AttendanceSetupOptionsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllAttendanceSetupOptionsUrl = Constants.hcmModuleApiBaseUrl + 'atteandaceOption/getAll';
    private getAllAttendanceSetupOptionsTypeUrl = Constants.hcmModuleApiBaseUrl + 'atteandaceOptionType/getAll';    
    private createAttendanceSetupOptionsUrl = Constants.hcmModuleApiBaseUrl + 'atteandaceOption/create';
    private createAttendanceSetupOptionsTypeUrl = Constants.hcmModuleApiBaseUrl + 'atteandaceOptionType/create';   
    private deleteAttendanceSetupOptionsUrl = Constants.hcmModuleApiBaseUrl + 'atteandaceOption/delete';
	private deleteAttendanceSetupOptionsTypeUrl = Constants.hcmModuleApiBaseUrl + 'atteandaceOptionType/delete';
    
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }

    //add new Attendance Setup Options
    createAttendanceSetupOption(attendance: AttendanceSetupOptions) {
        return this.http.post(this.createAttendanceSetupOptionsUrl, JSON.stringify(attendance), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    createAttendanceSetupOptionType(attendance: AttendanceSetupOptions){
        return this.http.post(this.createAttendanceSetupOptionsTypeUrl, JSON.stringify(attendance), {headers: this.headers})
        .toPromise()
        .then(res =>res.json())
        .catch(this.handleError);
    }

    //delete Attendance Setup Options
    deleteAttendanceOption(ids: any) {
        return this.http.put(this.deleteAttendanceSetupOptionsUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    deleteAttendanceOptionType(ids: any) {
        return this.http.put(this.deleteAttendanceSetupOptionsTypeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page): Observable<PagedData<AttendanceSetupOptions>> {
        return this.http.put(this.getAllAttendanceSetupOptionsUrl, {
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get list option type

    getlistType(page: Page, searchKeyword): Observable<PagedData<AttendanceSetupOptions>> {
        return this.http.post(this.getAllAttendanceSetupOptionsTypeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<AttendanceSetupOptions> {
		console.log(data);
        let pagedData = new PagedData<AttendanceSetupOptions>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let attendanceSetupOptions = new AttendanceSetupOptions(
                        jsonObj.id,
						jsonObj.seqn,
                        jsonObj.reasonIndx,
                        jsonObj.reasonDesc,
                        jsonObj.atteandanceTypeSeqn,
						jsonObj.atteandanceTypeIndx,
						jsonObj.atteandanceTypeDesc,
						jsonObj.isActive
                    );
                    pagedData.data.push(attendanceSetupOptions);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}