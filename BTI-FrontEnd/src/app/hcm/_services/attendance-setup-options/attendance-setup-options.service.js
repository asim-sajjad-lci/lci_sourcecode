"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A service class for department
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/Rx");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var attendance_setup_options_module_1 = require("../../_models/attendance-setup-options/attendance-setup-options.module");
var Constants_1 = require("../../../_sharedresource/Constants");
var AttendanceSetupOptionsService = (function () {
    //initializing parameter for constructor
    function AttendanceSetupOptionsService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/getAll';
        this.searchDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/searchDepartment';
        this.createAttendanceUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'attedance/create';
        this.getDepartmentByDepartmentIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/getDepartmentDetailByDepartmentId';
        this.updateDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/update';
        this.deleteDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/delete';
        this.checkDepartmentIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/departmentIdcheck';
        this.getAllAttendanceSetupOptionsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'atteandaceOption/getAll';
        this.createAttendanceSetupOptionsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'atteandaceOption/create';
        this.updateAttendanceSetupOptionsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'atteandaceOption/update';
        this.deleteAttendanceSetupOptionsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'atteandaceOption/delete';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new department
    AttendanceSetupOptionsService.prototype.createAttendanceSetupOption = function (attendance) {
        return this.http.post(this.createAttendanceSetupOptionsUrl, JSON.stringify(attendance), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    AttendanceSetupOptionsService.prototype.updateAttendanceSetupOption = function (attendanceOption) {
        return this.http.post(this.updateAttendanceSetupOptionsUrl, JSON.stringify(attendanceOption), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    AttendanceSetupOptionsService.prototype.deleteAttendanceOption = function (ids) {
        return this.http.put(this.deleteAttendanceSetupOptionsUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    /*//check for duplicate ID department
    checkDuplicateDeptId(departmentId: any) {
        return this.http.post(this.checkDepartmentIdx, { 'departmentId': departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getDepartment(departmentId: string) {
        return this.http.post(this.getDepartmentByDepartmentIdUrl, { departmentId: departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }*/
    //get list
    AttendanceSetupOptionsService.prototype.getlist = function (page) {
        var _this = this;
        return this.http.put(this.getAllAttendanceSetupOptionsUrl, {
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    /*//get list by search keyword
    searchDepartmentlist(page: Page, searchKeyword): Observable<PagedData<Department>> {
        return this.http.post(this.searchDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }
    */
    //pagination for data
    AttendanceSetupOptionsService.prototype.getPagedData = function (page, data) {
        console.log(data);
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var attendanceSetupOptions = new attendance_setup_options_module_1.AttendanceSetupOptions(jsonObj.id, jsonObj.seqn, jsonObj.reasonIndx, jsonObj.reasonDesc, jsonObj.atteandanceTypeSeqn, jsonObj.atteandanceTypeIndx, jsonObj.atteandanceTypeDesc, jsonObj.ids, jsonObj.isActive);
                    pagedData.data.push(attendanceSetupOptions);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    AttendanceSetupOptionsService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return AttendanceSetupOptionsService;
}());
AttendanceSetupOptionsService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], AttendanceSetupOptionsService);
exports.AttendanceSetupOptionsService = AttendanceSetupOptionsService;
//# sourceMappingURL=attendance-setup-options.service.js.map