import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {AccrualScheduleSetup} from '../../_models/accrual-schedule-setup/accrual-schedule-setup.module';
import { AccrualScheduleDetails } from '../../_models/accrual-schedule-details/accrual-schedule-details.module';

@Injectable()
export class AccrualScheduleSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllAccrualScheduleUrl = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/getAll';
    private searchAccrualScheduleUrl = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/search';
    private createAccrualScheduleUrl = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/create';
    //private getAccrualByAccrualIdUrl = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/getAccrualScheduleById';
    private updateAccrualScheduleUrl = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/update';
    private deleteAccrualScheduleUrl = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/delete';
    private checkAccrualScheduleIdx = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/scheduleIdcheck';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new accrualSetup
    createAccrualSchedule(accrualSetup: AccrualScheduleSetup) {
        return this.http.post(this.createAccrualScheduleUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateAccrualSchedule(accrualSetup: AccrualScheduleSetup) {
        return this.http.post(this.updateAccrualScheduleUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteAccrualSchedule(ids: any) {
        return this.http.put(this.deleteAccrualScheduleUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID Accrual
    checkDuplicateAccrualId(accrualId: any) {
        return this.http.post(this.checkAccrualScheduleIdx, { 'accrualId': accrualId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    /* //get Accrual detail by Id
    getAccrualSchedule(accrualId: string) {
        return this.http.post(this.getAccrualByAccrualIdUrl, { accrualId: accrualId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    } */


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<AccrualScheduleSetup>> {
        return this.http.post(this.getAllAccrualScheduleUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchAccrualSchedulelist(page: Page, searchKeyword): Observable<PagedData<AccrualScheduleSetup>> {
        return this.http.post(this.searchAccrualScheduleUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<AccrualScheduleSetup> {
        let pagedData = new PagedData<AccrualScheduleSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];                                                 
                    //jsonObj.subItems=jsonObj.subItems as Array<AccrualScheduleDetails>;
                    /* jsonObj.subItems.forEach(subItem=>{
                        let subJsonObj = subItem as AccrualScheduleDetails;
                        jsonObj.listSubItems.push(new AccrualScheduleDetails(
                            subJsonObj.id,
                            subJsonObj.scheduleId,
                            subJsonObj.scheduleSeniority,
                            subJsonObj.accrualSetupId,
                            subJsonObj.description,
                            subJsonObj.accrualType,
                            subJsonObj.hours));
                            console.log(jsonObj.listSubItems);
                    }); */
                    /* gridRecords[i].subItems.map(subItem=>{
                        let subJsonObj = subItem as AccrualScheduleDetails;
                        jsonObj.subItems.push(new AccrualScheduleDetails(
                            subJsonObj.id,
                            subJsonObj.scheduleId,
                            subJsonObj.scheduleSeniority,
                            subJsonObj.accrualSetupId,
                            subJsonObj.description,
                            subJsonObj.accrualType,
                            subJsonObj.hours));
                            console.log(jsonObj.listSubItems);
                    }); */
                    let accrual = new AccrualScheduleSetup(
                        jsonObj.id,
                        jsonObj.scheduleId,
                        jsonObj.desc,
                        jsonObj.arbicDesc,
                        jsonObj.startDate,
                        jsonObj.endDate,
                        jsonObj.subItems as Array<AccrualScheduleDetails>
                    );
                    pagedData.data.push(accrual);

                    /* let player = new Person(jsonObject.name, jsonObject.surname, jsonObject.age,
                        new Address(jsonObject.address['first-line'],
                            jsonObject.address['second-line'],
                            jsonObject.address.city)
                    );
                    deferred.resolve(player); */
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
