import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {AccrualScheduleDetails} from '../../_models/accrual-schedule-details/accrual-schedule-details.module';

@Injectable()
export class AccrualScheduleDetailService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllAccrualScheduleDetailUrl = Constants.hcmModuleApiBaseUrl + 'accrualScheduleDetail/getAll';
    private searchAccrualScheduleDetailUrl = Constants.hcmModuleApiBaseUrl + 'accrualScheduleDetail/search';
    private createAccrualScheduleDetailUrl = Constants.hcmModuleApiBaseUrl + 'accrualScheduleDetail/create';
    //private getAccrualScheduleDetailByAccrualScheduleDetailIdUrl = Constants.hcmModuleApiBaseUrl + 'accrualScheduleDetail/getAccrualScheduleDetailById';
    private updateAccrualScheduleDetailUrl = Constants.hcmModuleApiBaseUrl + 'accrualScheduleDetail/update';
    private deleteAccrualScheduleDetailUrl = Constants.hcmModuleApiBaseUrl + 'accrualScheduleDetail/delete';
    //private checkAccrualScheduleDetailIdx = Constants.hcmModuleApiBaseUrl + 'accrualScheduleDetail/scheduleIdcheck';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new accrualSetup
    createAccrual(accrualSetup: AccrualScheduleDetails) {
        return this.http.post(this.createAccrualScheduleDetailUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateAccrual(accrualSetup: AccrualScheduleDetails) {
        return this.http.post(this.updateAccrualScheduleDetailUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteAccrual(ids: any) {
        return this.http.put(this.deleteAccrualScheduleDetailUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    // checkDuplicateAccrualId(accrualId: any) {
    //     return this.http.post(this.checkAccruaSetuplIdx, { 'accrualId': accrualId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    //get department detail by Id
    // getAccrual(accrualId: string) {
    //     return this.http.post(this.getAccrualByAccrualIdUrl, { accrualId: accrualId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<AccrualScheduleDetails>> {
        return this.http.post(this.getAllAccrualScheduleDetailUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchAccruallist(page: Page, searchKeyword): Observable<PagedData<AccrualScheduleDetails>> {
        return this.http.post(this.searchAccrualScheduleDetailUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<AccrualScheduleDetails> {
        let pagedData = new PagedData<AccrualScheduleDetails>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let accrual = new AccrualScheduleDetails(
                        jsonObj.id,
                        jsonObj.scheduleId,
                        jsonObj.scheduleSeniority,
                        jsonObj.accrualSetupId,
                        jsonObj.description,
                        jsonObj.accrualTypeId,
                        jsonObj.hours
                    );
                    pagedData.data.push(accrual);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
