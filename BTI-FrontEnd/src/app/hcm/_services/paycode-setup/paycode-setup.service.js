"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var paycode_setup_module_1 = require("../../_models/paycode-setup/paycode-setup.module");
var PaycodeSetupService = (function () {
    //initializing parameter for constructor
    function PaycodeSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllPaycodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'payCode/search';
        this.searchPaycodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'payCode/search';
        this.createPaycodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'payCode/create';
        this.getSupervisorBySupervisorIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
        this.updatePaycodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'payCode/update';
        this.deletePaycodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'payCode/delete';
        this.checkPaycodeIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'payCode/payCodeIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new location
    PaycodeSetupService.prototype.createPaycode = function (paycode) {
        return this.http.post(this.createPaycodeUrl, JSON.stringify(paycode), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    PaycodeSetupService.prototype.updatePaycode = function (paycode) {
        return this.http.post(this.updatePaycodeUrl, JSON.stringify(paycode), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    PaycodeSetupService.prototype.deletePaycode = function (ids) {
        return this.http.put(this.deletePaycodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    PaycodeSetupService.prototype.checkDuplicatePayCode = function (payCodeId) {
        return this.http.post(this.checkPaycodeIdx, { 'payCodeId': payCodeId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    PaycodeSetupService.prototype.getSupervisor = function (superVisionCode) {
        return this.http.post(this.getSupervisorBySupervisorIdUrl, { superVisionCode: superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    PaycodeSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.getAllPaycodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    PaycodeSetupService.prototype.searchPaycodelist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPaycodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    PaycodeSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var supervisor = new paycode_setup_module_1.PaycodeSetupModule(jsonObj.id, jsonObj.payCodeId, jsonObj.description, jsonObj.arbicDescription, jsonObj.payType, jsonObj.baseOnPayCode, jsonObj.baseOnPayCodeAmount, jsonObj.payFactor, jsonObj.payRate, jsonObj.unitofPay, jsonObj.payperiod, jsonObj.inActive);
                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    PaycodeSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return PaycodeSetupService;
}());
PaycodeSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PaycodeSetupService);
exports.PaycodeSetupService = PaycodeSetupService;
//# sourceMappingURL=paycode-setup.service.js.map