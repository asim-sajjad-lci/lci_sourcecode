import { Injectable } from '@angular/core';

import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { EmployeeDeductionMaintnce } from '../../_models/employee-deduction-maintenance/employee-deduction-maintenance.module';

@Injectable()
export class EmployeeDeductionMaintenanceComponentService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllEmpDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/getAll';
  private createEmpDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/create';
  private duplicateEmpBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/validate';
  // private getEmpDeductionCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/getById';
  private updateEmpDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/update';
  private deleteEmpDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/delete';
  private employeecodeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
  private deductioncodeIdUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/getAllDeductionDropDown';
  private getEmpDeductionCodeByEmployeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/getById';
  private getAllBasedOnPayCodeDropDownUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPaycodeforDeduction';
  private getAlldeductionCodefindCodeByEmployeeIdUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/findCodeByEmployeeId';
  private typeIdDetails = Constants.hcmModuleApiBaseUrl + 'typeFieldForCodes/getAllIds';
  

  //initializing parameter for constructor
  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
  }

  //Create new Emp Deduction Code
  createEmpDeductionCode(deductionCodeSetup: EmployeeDeductionMaintnce) {
    console.log(JSON.stringify(deductionCodeSetup));
    return this.http.post(this.createEmpDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  checkDuplicateBenefitId(benefitId: any) {
    return this.http.post(this.duplicateEmpBenefitCodeUrl, { 
        "employeeMaster": {
            "employeeIndexId":benefitId.employeeMaster.employeeIndexId
        },
        "deductionCode": {
            "id":benefitId.deductionCode.id
        }

     }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

  //update Emp Deduction
  updateEmpDeductionCode(deductionCodeSetup: EmployeeDeductionMaintnce) {
    return this.http.post(this.updateEmpDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get department detail by Id
  getAllBasedOnPayCodeDropDown(payload: any) {
    return this.http.post(this.getAllBasedOnPayCodeDropDownUrl, payload, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }
  
  //get department detail by Id
  getAlldeductionCodefindCodeByEmployeeId(payload: any) {
    return this.http.post(this.getAlldeductionCodefindCodeByEmployeeIdUrl, payload, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //delete Emp Deduction
  deleteEmpDeductionCode(ids: any) {
    return this.http.put(this.deleteEmpDeductionCodeUrl, { 'ids': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  // get Employee detail by Id
  getEmployeeCodeId() {
    return this.http.get(this.employeecodeIdUrl, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  getDeductioncodeId() {
    return this.http.get(this.deductioncodeIdUrl, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get list
  getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeDeductionMaintnce>> {
    return this.http.post(this.getAllEmpDeductionCodeUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //get Employee Education detail by Employee Id
  getlistByEmployeeID(id: number) {
    return this.http.post(this.getEmpDeductionCodeByEmployeeIdUrl, { 'id': id }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<EmployeeDeductionMaintnce> {
    let pagedData = new PagedData<EmployeeDeductionMaintnce>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let empDeductionMaintnce = new EmployeeDeductionMaintnce(
            jsonObj.id,
            jsonObj.employeeMaster,
            jsonObj.deductionCode,
            jsonObj.startDate,
            jsonObj.endDate,
            jsonObj.frequency,
            jsonObj.benefitMethod,
            jsonObj.deductionAmount,
            jsonObj.deductionPercent,
            jsonObj.transactionRequired,
            jsonObj.inactive,
            jsonObj.perPeriord,
            jsonObj.perYear,
            jsonObj.lifeTime,
            jsonObj.dtoPayCode,
            jsonObj.payFactor,
            jsonObj.noOfDays,
            jsonObj.endDateDays,
            jsonObj.deductionTypeId,
            jsonObj.roundOf
          );
          pagedData.data.push(empDeductionMaintnce);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }

   //get type field detail by Id
   getTypeFieldDetails(typeid) {
    return this.http.post(this.typeIdDetails, { id: typeid }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}
  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
