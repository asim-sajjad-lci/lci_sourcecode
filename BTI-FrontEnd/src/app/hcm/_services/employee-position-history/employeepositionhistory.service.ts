/**
 * A service class for employeePositionHistorySeup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
//import { employeePositionHistory } from "../../_models/employee-position-history/employeePositionHistory";
import { employeePosition } from "../../_models/employee-position-history/employeePosition";

import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class EmployeePositionHistoryService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmployeePositionHistoryUrl = Constants.hcmModuleApiBaseUrl + 'employeePositionHistory/getAll';
    private searchEmployeePositionHistoryUrl = Constants.hcmModuleApiBaseUrl + 'employeePositionHistory/searchemployeePositionHistorySeup';
    private createEmployeePositionHistoryUrl = Constants.hcmModuleApiBaseUrl + 'employeePositionHistory/create';
    private updateEmployeePositionHistoryUrl = Constants.hcmModuleApiBaseUrl + 'employeePositionHistory/update';
    private deleteEmployeePositionHistoryUrl = Constants.hcmModuleApiBaseUrl + 'employeePositionHistory/delete';
    private checkEmployeePositionHistoryUrl = Constants.hcmModuleApiBaseUrl +  'employeePositionHistory/EmployeePositionHistoryIdcheck';
    private deleteEmployeePositionHistoryDetailUrl=Constants.hcmModuleApiBaseUrl + 'employeePositionHistoryDetail/delete';

    private getEmployeeListUrl=Constants.hcmModuleApiBaseUrl + 'employeeMaster/employeeMasterDropDownList';
    private getDivisionListUrl=Constants.hcmModuleApiBaseUrl + 'division/getAllDivisionDropDown';
    private getDepartmentListUrl=Constants.hcmModuleApiBaseUrl + 'department/getAllDepartmentDropDown';
    private getPositionListUrl=Constants.hcmModuleApiBaseUrl + 'positionSetup/getAllPostionDropDownList';
    private getSuperVisorListUrl=Constants.hcmModuleApiBaseUrl + 'supervisor/getAllSupervisorDropDownList';
    private getLocationListUrl=Constants.hcmModuleApiBaseUrl + 'location/getAllLocationDropDown';
    private getEmployeePositionReasonUrl=Constants.hcmModuleApiBaseUrl + 'controllerEmployeePositionReason/getAllForDroupDown';
    private getEmployeeListId=Constants.hcmModuleApiBaseUrl + 'employeeMaster/employeeMasterDropDownList';
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        // this.headers.append("tenantid", localStorage.getItem('tenantid'));
        this.headers.append("tenantid", "bti_hr");
    }

    //add new employeePositionHistorySeup
    createEmployeePositionHistory(employeePositionHistorySeup: employeePosition) {
        return this.http.post(this.createEmployeePositionHistoryUrl, JSON.stringify(employeePositionHistorySeup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getEmployee() {
        return this.http.get(this.getEmployeeListId, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    checkDuplicateEmployeePositionHistoryId(employeePositionHistoryId: any) {
        return this.http.post(this.checkEmployeePositionHistoryUrl, { 'employeePositionHistoryId': employeePositionHistoryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit employeePositionHistorySeup
    updateEmployeePositionHistory(employeePositionHistorySeup: employeePosition) {
        return this.http.post(this.updateEmployeePositionHistoryUrl, JSON.stringify(employeePositionHistorySeup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete employeePositionHistorySeup
    deleteEmployeePositionHistory(ids: any) {
        return this.http.put(this.deleteEmployeePositionHistoryUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete EmployeePositionHistoryDetail
    deleteEmployeePositionHistoryDetail(ids: any) {
        return this.http.put(this.deleteEmployeePositionHistoryDetailUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   
    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<employeePosition>> {
        return this.http.post(this.getAllEmployeePositionHistoryUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchEmployeePositionHistorylist(page: Page, searchKeyword): Observable<PagedData<employeePosition>> {
        return this.http.post(this.searchEmployeePositionHistoryUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<employeePosition> {
         let pagedData = new PagedData<employeePosition>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                   
                    let jsonObj = gridRecords[i];
                    let EmployeePositionHistory = new employeePosition(
                        jsonObj.id, 
                        jsonObj.positionEffectiveDate,
                        jsonObj.employeeType,
                        jsonObj.employeeMaster,
                        jsonObj.division,
                        jsonObj.department,
                        jsonObj.position,
                        jsonObj.location,
                        jsonObj.supervisor,
                        jsonObj.dtoEmployeePositionReason,
                        
                    );
                    pagedData.data.push(EmployeePositionHistory);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    // getting Skill set list
 getEmployeeList() {
    return this.http.get(this.getEmployeeListUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getDivisionList() {
    return this.http.get(this.getDivisionListUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getDepartmentList() {
    return this.http.get(this.getDepartmentListUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getPositionList() {
    return this.http.get(this.getPositionListUrl,{ headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getSupervisorList() {
    return this.http.get(this.getSuperVisorListUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getLocationList() {
    return this.http.get(this.getLocationListUrl,{ headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getEmployeePositionReasonList() {
    return this.http.get(this.getEmployeePositionReasonUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}
}