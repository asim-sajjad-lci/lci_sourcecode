"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var skills_setup_module_1 = require("../../_models/skills-setup/skills-setup.module");
var SkillsSetupService = (function () {
    //initializing parameter for constructor
    function SkillsSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllSkillsSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getAll';
        this.searchSkillsSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/searchSkillSteup';
        this.createSkillSteupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/createSkillSteup';
        this.getSkillSteupById = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getSkillSteupById';
        this.updateSkillsSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/update';
        this.deleteSkillsSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/delete';
        this.getSkillsSetupIdcheck = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/skillSteupIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new skillsSetup
    SkillsSetupService.prototype.createSkillSteup = function (skillsSetup) {
        return this.http.post(this.createSkillSteupUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    SkillsSetupService.prototype.updateSkillsSetup = function (skillsSetup) {
        return this.http.post(this.updateSkillsSetupUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    SkillsSetupService.prototype.deleteSkillsSetup = function (ids) {
        return this.http.put(this.deleteSkillsSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    SkillsSetupService.prototype.checkDuplicateSkillsSetupId = function (skillId) {
        return this.http.post(this.getSkillsSetupIdcheck, { 'skillId': skillId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    SkillsSetupService.prototype.getSkillsSetup = function (skillId) {
        return this.http.post(this.getSkillSteupById, { skillId: skillId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    SkillsSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllSkillsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    SkillsSetupService.prototype.searchSkillsSetuplist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchSkillsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    SkillsSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var skillsSetup = new skills_setup_module_1.SkillsSetup(jsonObj.id, jsonObj.skillId, jsonObj.skillDesc, jsonObj.arabicDesc, jsonObj.compensation, jsonObj.frequency, jsonObj.frequencyName);
                    pagedData.data.push(skillsSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    SkillsSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return SkillsSetupService;
}());
SkillsSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], SkillsSetupService);
exports.SkillsSetupService = SkillsSetupService;
//# sourceMappingURL=skills-setup.service.js.map