import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { MiscellaneousBenefits } from '../../_models/miscellaneous-benefits/miscellaneous-benefits.module';

@Injectable()
export class MiscellaneousBenefitsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllMiscellaneousBenefitsUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/getAll';
    private searchMiscellaneousBenefitsUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/searchMiscellaneousId';
    private createMiscellaneousBenefitsUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/create';
    private getMiscellaneousBenefitsById = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/getMiscellaneousBenefitsId';
    private updateMiscellaneousBenefitsUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/update';
    private deleteMiscellaneousBenefitsUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/delete';
    private getMiscellaneousBenefitsIdcheck = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/miscellaneousBenefitsIdcheck';
	//private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAllPostionClassId';
	private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/searchMiscellaneousId';
	
    //getAllBenefitId

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new MiscellaneousBenefits
    createMiscellaneousBenefits(miscellaneousBenefits: MiscellaneousBenefits) {
        return this.http.post(this.createMiscellaneousBenefitsUrl, JSON.stringify(miscellaneousBenefits), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateMiscellaneousBenefits(miscellaneousBenefits: MiscellaneousBenefits) {
        return this.http.post(this.updateMiscellaneousBenefitsUrl, JSON.stringify(miscellaneousBenefits), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteMiscellaneousBenefits(ids: any) {
        return this.http.put(this.deleteMiscellaneousBenefitsUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateMiscellaneousBenefitsId(skillId: any) {
        return this.http.post(this.getMiscellaneousBenefitsIdcheck, { 'benefitsId': skillId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id 
    getMiscellaneousBenefits(skillId: string) {
        return this.http.post(this.getMiscellaneousBenefitsById, { id: skillId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

getPositionClassList() {
        return this.http.post(this.getPositionClassListUrl,{
			"searchKeyword": "",
			"pageNumber": 0,
			"pageSize": 5,
			"sortOn": "id",
			"sortBy": "DESC"
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<MiscellaneousBenefits>> {
        return this.http.post(this.getAllMiscellaneousBenefitsUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchMiscellaneousBenefitslist(page: Page, searchKeyword): Observable<PagedData<MiscellaneousBenefits>> {
        return this.http.post(this.searchMiscellaneousBenefitsUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<MiscellaneousBenefits> {
        let pagedData = new PagedData<MiscellaneousBenefits>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let miscellaneousBenefits = new MiscellaneousBenefits(
                        jsonObj.id,
                        jsonObj.benefitsId,
                        jsonObj.desc,
                        jsonObj.arbicDesc,
                        jsonObj.frequency,
                        jsonObj.startDate,
                        jsonObj.endDate,
						jsonObj.inactive,
						jsonObj.method,
						jsonObj.dudctionAmount,
						jsonObj.dudctionPercent,
						jsonObj.monthlyAmount,
						jsonObj.yearlyAmount,
						jsonObj.lifetimeAmount,
						jsonObj.empluyeeerinactive,
						jsonObj.empluyeeermethod,
						jsonObj.benefitAmount,
						jsonObj.benefitPercent,
						jsonObj.employermonthlyAmount,
						jsonObj.employeryearlyAmount,
						jsonObj.employerlifetimeAmount);
                    pagedData.data.push(miscellaneousBenefits);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
