import { TestBed, inject } from '@angular/core/testing';

import { EmployeeNationalityService } from './employee-nationality.service';

describe('EmployeeNationalityService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeNationalityService]
    });
  });

  it('should be created', inject([EmployeeNationalityService], (service: EmployeeNationalityService) => {
    expect(service).toBeTruthy();
  }));
});
