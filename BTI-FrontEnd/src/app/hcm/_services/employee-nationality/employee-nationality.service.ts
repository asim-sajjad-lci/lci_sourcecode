import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {EmployeeNationalityModule} from '../../_models/employee-nationality/employee-nationality.module';

@Injectable()
export class EmployeeNationalityService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmployeeNationalityUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/getAll';
    private searchEmployeeNationalityUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/searchEmployeeNationalityId';
    private createEmployeeNationalityUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/create';
    private getEmployeeNationalityByEmployeeNationalityIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/getEmployeeNationalitiesById';
    private updateEmployeeNationalityUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/update';
    private deleteEmployeeNationalityUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/delete';
    private checkEmployeeNationalityIdx = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/employeeNationalitiesIdcheck';
    private searchEmployeeNationalityIDUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/searchEmployeeNationalityId';
    private getDropdownEmployeeNationalityIDUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/getAllEmployeeNationalitiesDropDown';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }

    // Emp Nat Search Id
    
    getEmployeeNationalityIdList() {
        return this.http.post(this.searchEmployeeNationalityIDUrl,{'searchKeyword': ''} ,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Emp Nat Search Id
    
    getEmployeeNationalityIdDropDown() {
        return this.http.get(this.getDropdownEmployeeNationalityIDUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //add new EmployeeNationality
    createEmployeeNationality(employeeNationality: EmployeeNationalityModule) {
        return this.http.post(this.createEmployeeNationalityUrl, JSON.stringify(employeeNationality), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateEmployeeNationality(employeeNationality: EmployeeNationalityModule) {
        return this.http.post(this.updateEmployeeNationalityUrl, JSON.stringify(employeeNationality), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteEmployeeNationality(ids: any) {
        return this.http.put(this.deleteEmployeeNationalityUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateEmployeeNationalityId(employeeNationalityId: any) {
        return this.http.post(this.checkEmployeeNationalityIdx, { 'employeeNationalityId': employeeNationalityId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getEmployeeNationality(employeeNationalityId: string) {
        return this.http.post(this.getEmployeeNationalityByEmployeeNationalityIdUrl, { employeeNationalityId: employeeNationalityId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeNationalityModule>> {
        return this.http.post(this.getAllEmployeeNationalityUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchEmployeeNationalitylist(page: Page, searchKeyword): Observable<PagedData<EmployeeNationalityModule>> {
        return this.http.post(this.searchEmployeeNationalityUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeNationalityModule> {
        let pagedData = new PagedData<EmployeeNationalityModule>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let employeeNationalityModule = new EmployeeNationalityModule(
                        jsonObj.employeeNationalityIndexId,
                        jsonObj.employeeNationalityId,
                        jsonObj.employeeNationalityDescription,
                        jsonObj.employeeNationalityDescriptionArabic);
                    pagedData.data.push(employeeNationalityModule);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
