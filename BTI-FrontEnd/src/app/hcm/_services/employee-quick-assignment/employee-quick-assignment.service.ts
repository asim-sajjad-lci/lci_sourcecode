import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {EmployeeQuickAssignmentModule} from '../../_models/employee-quick-assignment/employee-quick-assignment.module';

@Injectable()
export class EmployeeQuickAssignmentService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllQuickAssignmentUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/getAll';
    private searchQuickAssignmentUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/searchSuperVisor';
    private createQuickAssignmentUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/create';
    private getQuickAssignmentByQuickAssignmentIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/getLocationDetailByLocationId';
    private updateQuickAssignmentUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/update';
    private deleteQuickAssignmentUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/delete';
    private updateQuickAssignmentPayRateUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/updateQuickAssignment';
    private checkQuickAssignmentIdx = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/employeeIdCheck';
    private getEmployeeIdDetail = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/searchUserbyId';
    private superviserIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeQuickAssigments/getAllQuickAssignmentDropDownList';
    private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
    
    private checkCodeUsageUrl = Constants.hcmModuleApiBaseUrl + 'employeeDeductionMaintenance/checkForDel?';//employeeId=2&codeType=1&codeId=2
    
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createQuickAssignment(employeeId: number,codeType: number,allEmpCode:boolean,listCodeTypes) {
        return this.http.post(this.updateQuickAssignmentPayRateUrl, {"employeeId":employeeId ,"codeType": codeType,"allEmpCode": allEmpCode,"listCodeTypes":listCodeTypes  }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateQuickAssignment(supervisor: EmployeeQuickAssignmentModule) {
        return this.http.post(this.updateQuickAssignmentUrl, JSON.stringify(supervisor), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteQuickAssignment(ids: any) {
        return this.http.put(this.deleteQuickAssignmentUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getSuperviserId(){
        return this.http.get(this.superviserIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    getEmployee(){
        return this.http.get(this.employeeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    //check for duplicate ID department
    checkDuplicateQuickAssignment(superVisionCode: any) {
        return this.http.post(this.checkQuickAssignmentIdx, { 'employeeMaster': superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getQuickAssignment(superVisionCode: string) {
        return this.http.post(this.getQuickAssignmentByQuickAssignmentIdUrl, { superVisionCode: superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, startIndx, endIndx,param :any): Observable<PagedData<EmployeeQuickAssignmentModule>> {
        return this.http.post(this.getAllQuickAssignmentUrl, {
            "from" : startIndx,
            "toIndex": endIndx,
            'employeeId' : param['employeeId'], 
            'codeType' : param['codeType'],
            'allCompanyCode' : param['allCompanyCode']
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchQuickAssignmentlist(page: Page, searchKeyword): Observable<PagedData<EmployeeQuickAssignmentModule>> {
        return this.http.post(this.searchQuickAssignmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeQuickAssignmentModule> {
        let pagedData = new PagedData<any>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let parentPaycodeAmount=0;

                    if(jsonObj.dtoPayCode){
                        for(let j=0;j<jsonObj.dtoPayCode.length;j++){
                            parentPaycodeAmount=parentPaycodeAmount+jsonObj.dtoPayCode[j].payRate;
                        }
                    }

                    let supervisor = {
                      "index" :    page.pageNumber*page.size + i,
                      "include" : (jsonObj.masterId!=null),
                      "masterId" : jsonObj.masterId,
                      "employeeId" : jsonObj.employeeId,
                      "code": jsonObj.code,
                      "codeId": jsonObj.codeId,
                      "desc": jsonObj.desc,
                      "arabicDesc": jsonObj.arabicDesc,
                      "method" : jsonObj.method,
                      "percent" : jsonObj.percent,
                      "empolyeeMaintananceId": jsonObj.empolyeeMaintananceId,
                      "amount" : jsonObj.amount,
                      "payRate" : jsonObj.payRate,
                      "startDate" : jsonObj.startDate,
                      "endDate" : jsonObj.endDate,
                      "inActive" : jsonObj.active,  
                      "isAmtType": (Math.floor(jsonObj.method/3) == 0),
                      "basedOnpaycode":jsonObj.baseOnPayCode,
                      "payFactor":jsonObj.payFactory,
                      "dtoPayCode":jsonObj.dtoPayCode,
                      "parentPaycodeAmount":parentPaycodeAmount
                    } 
                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    checkCodeUsage(empId,codeId,typeId){
       // employeeId=2&codeType=1&codeId=2
        let url=this.checkCodeUsageUrl+'employeeId='+empId+'&codeType='+typeId+'&codeId='+codeId
        return this.http.get(url, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    /* getPayTypeDropdown() {
        return this.http.post(this.getgetPayTypeDropdown, { "searchKeyword": "" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    } */
}
