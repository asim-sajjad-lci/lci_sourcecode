"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var employee_quick_assignment_service_1 = require("./employee-quick-assignment.service");
describe('EmployeeQuickAssignmentService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [employee_quick_assignment_service_1.EmployeeQuickAssignmentService]
        });
    });
    it('should be created', testing_1.inject([employee_quick_assignment_service_1.EmployeeQuickAssignmentService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=employee-quick-assignment.service.spec.js.map