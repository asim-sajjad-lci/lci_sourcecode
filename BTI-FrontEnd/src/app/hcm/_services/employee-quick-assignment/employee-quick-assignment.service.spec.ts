import { TestBed, inject } from '@angular/core/testing';

import { EmployeeQuickAssignmentService } from './employee-quick-assignment.service';

describe('EmployeeQuickAssignmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeQuickAssignmentService]
    });
  });

  it('should be created', inject([EmployeeQuickAssignmentService], (service: EmployeeQuickAssignmentService) => {
    expect(service).toBeTruthy();
  }));
});
