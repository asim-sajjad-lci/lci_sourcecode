import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { PaycodeSetupModule } from '../../_models/paycode-setup/paycode-setup.module';
import { BenefitCodeSetup } from '../../_models/benefit-code-setup/benefit-code-setup.module';
import { DeductionCodeSetup } from '../../_models/deduction-code-setup/deduction-code-setup.module';

@Injectable()
export class RoutineUtilitiesService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private paycodeurl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPayCodeDropDown';
    private benefitcodeurl = Constants.hcmModuleApiBaseUrl + 'benefitCode/getAllBenefitDropDown';
    private deductioncodeurl = Constants.hcmModuleApiBaseUrl + 'deductionCode/getAllDeductionDropDown';
    private updatepaycodeurl = Constants.hcmModuleApiBaseUrl + 'payCode/update';
    private updatebenecodeurl = Constants.hcmModuleApiBaseUrl + 'benefitCode/update';
    private updatededucodeurl = Constants.hcmModuleApiBaseUrl + 'deductionCode/update';
    private getpaycodeurl = Constants.hcmModuleApiBaseUrl + 'payCode/getById';
    private getbenefitcodeurl = Constants.hcmModuleApiBaseUrl + 'benefitCode/getBenefitCodeById';
    private getdecductioncodeurl = Constants.hcmModuleApiBaseUrl + 'deductionCode/getDeductionCodeById';
    private processclosingurl = Constants.hcmModuleApiBaseUrl + 'routineUtilities/yearEndProcess/';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //get paycodes
    getPayCodes() {
        return this.http.get(this.paycodeurl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get benefit codes
    getBenefitCodes() {
        return this.http.get(this.benefitcodeurl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get deduc codes
    getDeductionCodes() {
        return this.http.get(this.deductioncodeurl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    updatePayCode(payCode: any) {
        return this.http.post(this.updatepaycodeurl, JSON.stringify(payCode), { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    updateBeneCode(benefitCode: any) {
        return this.http.post(this.updatebenecodeurl, JSON.stringify(benefitCode), { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    updateDeduCode(deductionCode: any) {
        return this.http.post(this.updatededucodeurl, JSON.stringify(deductionCode), { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getByPayCodeId(payCode: any) {
        return this.http.post(this.getpaycodeurl, payCode, { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getBenefitCodesById(benefitCode: any) {
        return this.http.post(this.getbenefitcodeurl, benefitCode, { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getDeductionCodeById(deductionCode: any) {
        return this.http.post(this.getdecductioncodeurl, deductionCode, { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    processClosing(year: any) {
        return this.http.get(this.processclosingurl + year, { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
