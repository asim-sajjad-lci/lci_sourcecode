import { TestBed, inject } from '@angular/core/testing';

import { LoanAndAdvanceService } from './loan-and-advance.service';

describe('LoanAndAdvanceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoanAndAdvanceService]
    });
  });

  it('should be created', inject([LoanAndAdvanceService], (service: LoanAndAdvanceService) => {
    expect(service).toBeTruthy();
  }));
});
