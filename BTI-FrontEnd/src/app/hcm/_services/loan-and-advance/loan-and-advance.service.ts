import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { Observable } from 'rxjs';
import { PagedData } from '../../../_sharedresource/paged-data';
import { DatePipe } from '@angular/common';

@Injectable()
export class LoanAndAdvanceService {

  private headers = new Headers({ 'content-type': 'application/json' });

  private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'loanPayment/getAllEmployeeList';
 private getEmployeeDetailsById = Constants.hcmModuleApiBaseUrl + 'loanPayment/getEmployeeDetailsById';
  private saveLoan = Constants.hcmModuleApiBaseUrl + 'loanPayment/create';
  private getAllLoan = Constants.hcmModuleApiBaseUrl + 'loanPayment/viewLoans';
   private loadDetailsByLoadId = Constants.hcmModuleApiBaseUrl + 'loanPayment/getLoanDetailsByLoanId';
   private getRemainingAmountDetails = Constants.hcmModuleApiBaseUrl + 'loanPayment/getAllRemainingsByEmpId';


  constructor(private http: Http) { 
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
    console.log('Header: ', this.headers)
  }

  getEmployee(){
		return this.http.post(this.employeeIdUrl,{}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
  }

  getLoanDetails(id){
		return this.http.post(this.loadDetailsByLoadId,{"loanId":id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}

  getEmployeeDetails(id){
		return this.http.post(this.getEmployeeDetailsById,{	"employeeIdx":id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
  }

  saveLoanAndPayment(model){
   
    console.log(JSON.stringify(model));
    return this.http.post(this.saveLoan, JSON.stringify(model), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  getlist(page: Page, searchKeyword): Observable<PagedData<Object>> {
   
    return this.http.post(this.getAllLoan,{
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
    },
    { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<Object> {
    
    let pagedData = new PagedData<Object>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let date = new DatePipe('en-US').transform(gridRecords[i].transactionDate, 'yyyy-MM-dd')
          let obj ={
            "id":jsonObj.loanId,
            "employeeId" : jsonObj.employeeName,
            "refId" : jsonObj.refId,
            "transactionDate" : date,
            "loanAmt" : jsonObj.loanAmt,
        };
          pagedData.data.push(obj);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }


  getRemaininglist(id,page: Page,): Observable<PagedData<Object>> {
  
    return this.http.post(this.getRemainingAmountDetails,{
      'employeeIndexId': id,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
    },
    { headers: this.headers }).map(data => this.getPagedDataForRemaining(page,data.json().result));
  
  }

  private getPagedDataForRemaining( page: Page, data: any): PagedData<Object> {
    
    let pagedData = new PagedData<Object>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
      //    let date = new DatePipe('en-US').transform(gridRecords[i].transactionDate, 'yyyy-MM-dd')
          let obj ={
            "refId":jsonObj.refId,
            "deductionAmount" : jsonObj.deductionAmount,
            "month" : jsonObj.monthYear,
        };
          pagedData.data.push(obj);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }
  
   //error handler
   private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
}


}
