"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var accrual_setup_module_1 = require("../../_models/accrual-setup/accrual-setup.module");
var AccrualSetupService = (function () {
    //private checkAccrualIdx = Constants.hcmModuleApiBaseUrl + 'accrual/accrualIdcheck';
    //initializing parameter for constructor
    function AccrualSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accurual/getAll';
        this.searchAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accurual/search';
        this.createAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accurual/create';
        //private getAccrualByAccrualIdUrl = Constants.hcmModuleApiBaseUrl + 'accrual/getAccrualDetailByAccrualId';
        this.updateAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accurual/update';
        this.deleteAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accurual/delete';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new accrualSetup
    AccrualSetupService.prototype.createAccrual = function (accrualSetup) {
        return this.http.post(this.createAccrualUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    AccrualSetupService.prototype.updateAccrual = function (accrualSetup) {
        return this.http.post(this.updateAccrualUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    AccrualSetupService.prototype.deleteAccrual = function (ids) {
        return this.http.put(this.deleteAccrualUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    // checkDuplicateAccrualId(accrualId: any) {
    //     return this.http.post(this.checkAccruaSetuplIdx, { 'accrualId': accrualId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    //get department detail by Id
    // getAccrual(accrualId: string) {
    //     return this.http.post(this.getAccrualByAccrualIdUrl, { accrualId: accrualId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    //get list
    AccrualSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllAccrualUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    AccrualSetupService.prototype.searchAccruallist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchAccrualUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    AccrualSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var accrual = new accrual_setup_module_1.AccrualSetup(jsonObj.id, jsonObj.desc, jsonObj.type);
                    pagedData.data.push(accrual);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    AccrualSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return AccrualSetupService;
}());
AccrualSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], AccrualSetupService);
exports.AccrualSetupService = AccrualSetupService;
//# sourceMappingURL=accrual-setup.service.js.map