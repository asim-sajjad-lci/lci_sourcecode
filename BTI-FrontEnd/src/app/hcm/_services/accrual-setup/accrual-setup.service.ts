import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {AccrualSetup} from '../../_models/accrual-setup/accrual-setup.module';

@Injectable()
export class AccrualSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllAccrualUrl = Constants.hcmModuleApiBaseUrl + 'accurualType/getAll';
    private searchAccrualUrl = Constants.hcmModuleApiBaseUrl + 'accurualType/search';
    private createAccrualUrl = Constants.hcmModuleApiBaseUrl + 'accurualType/create';
    //private getAccrualByAccrualIdUrl = Constants.hcmModuleApiBaseUrl + 'accurualType/getAccrualDetailByAccrualId';
    private updateAccrualUrl = Constants.hcmModuleApiBaseUrl + 'accurualType/update';
    private deleteAccrualUrl = Constants.hcmModuleApiBaseUrl + 'accurualType/delete';
    private checkAccrualDesc = Constants.hcmModuleApiBaseUrl + 'accurualType/accrualDescriptionCheck';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new accrualSetup
    createAccrual(accrualSetup: AccrualSetup) {
        return this.http.post(this.createAccrualUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateAccrual(accrualSetup: AccrualSetup) {
        return this.http.post(this.updateAccrualUrl, JSON.stringify(accrualSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteAccrual(ids: any) {
        return this.http.put(this.deleteAccrualUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    // checkDuplicateAccrualId(accrualId: any) {
    //     return this.http.post(this.checkAccruaSetuplIdx, { 'accrualId': accrualId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    //get department detail by Id
    // getAccrual(accrualId: string) {
    //     return this.http.post(this.getAccrualByAccrualIdUrl, { accrualId: accrualId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<AccrualSetup>> {
        return this.http.post(this.getAllAccrualUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchAccruallist(page: Page, searchKeyword): Observable<PagedData<AccrualSetup>> {
        return this.http.post(this.searchAccrualUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<AccrualSetup> {
        let pagedData = new PagedData<AccrualSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let accrual = new AccrualSetup(
                        jsonObj.id,
                        jsonObj.desc,
                        jsonObj.type);
                    pagedData.data.push(accrual);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    checkDuplicateDesc(accrualDesc: any) {
        return this.http.post(this.checkAccrualDesc, { 'desc': accrualDesc }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
}
