"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var accrual_module_1 = require("../../_models/accrual/accrual.module");
var AccrualService = (function () {
    //initializing parameter for constructor
    function AccrualService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accrual/getAll';
        this.searchAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accrual/searchAccrual';
        this.createAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accrual/create';
        this.getAccrualByAccrualIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accrual/getAccrualDetailByAccrualId';
        this.updateAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accrual/update';
        this.deleteAccrualUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'accrual/delete';
        this.checkAccrualIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'accrual/accrualIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new location
    AccrualService.prototype.createAccrual = function (accrual) {
        return this.http.post(this.createAccrualUrl, JSON.stringify(accrual), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    AccrualService.prototype.updateAccrual = function (accrual) {
        return this.http.post(this.updateAccrualUrl, JSON.stringify(accrual), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    AccrualService.prototype.deleteAccrual = function (ids) {
        return this.http.put(this.deleteLocationUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    LocationService.prototype.checkDuplicateLocationId = function (locationId) {
        return this.http.post(this.checkLocationIdx, { 'locationId': locationId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    LocationService.prototype.getLocation = function (locationId) {
        return this.http.post(this.getLocationByLocationIdUrl, { locationId: locationId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    LocationService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllLocationUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    LocationService.prototype.searchLocationlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchLocationUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    LocationService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var location_1 = new location_module_1.Location(jsonObj.id, jsonObj.locationId, jsonObj.description, jsonObj.arabicDescription, jsonObj.contactName, jsonObj.locationAddress, jsonObj.city, jsonObj.country, jsonObj.phone, jsonObj.fax);
                    pagedData.data.push(location_1);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    LocationService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return LocationService;
}());
LocationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], LocationService);
exports.LocationService = LocationService;
//# sourceMappingURL=location.service.js.map