import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { OrientationSetup } from '../../_models/orientation-setup/orientation-setup.module';

@Injectable()
export class OrientationSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllOrientationSetupUrl = Constants.hcmModuleApiBaseUrl + 'orientationSetup/getAll';
    private searchOrientationSetuppUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/searchSkillSteup';
    private createOrientationSetupUrl = Constants.hcmModuleApiBaseUrl + 'orientationSetup/create';
    private getOrientationSetupById = Constants.hcmModuleApiBaseUrl + 'skillSetDescDetail/getById';
    private updateOrientationSetupUrl = Constants.hcmModuleApiBaseUrl + 'orientationSetup/update';
    private deleteOrientationSetupUrl = Constants.hcmModuleApiBaseUrl + 'orientationSetup/delete';
    private getOrientationSetupIdcheck = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/skillSteupIdcheck';
    private getOrientationSetupListUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getAll';
    private getOrientationSetupByIdUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getSkillSteupById';
	private getPredfinedCheckListUrl = Constants.hcmModuleApiBaseUrl + 'orientationPredefinedCheckListItem/getAllList';
    private getTerminationprechecksubitem = Constants.hcmModuleApiBaseUrl + 'predefinedCheckListSetup/getOrientaionChekListSetupById';
	private deleteOrientationSetupsubitemUrl = Constants.hcmModuleApiBaseUrl + 'orientationSetupDetail/delete';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new skillsSetup
    createOrientationSetup(OrientationSetup: OrientationSetup) {
        return this.http.post(this.createOrientationSetupUrl, JSON.stringify(OrientationSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateOrientationSetup(OrientationSetup: OrientationSetup) {
        return this.http.post(this.updateOrientationSetupUrl, JSON.stringify(OrientationSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteOrientationSetup(ids: any) {
        return this.http.put(this.deleteOrientationSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	deleteOrientationSetupsubitem(ids: any) {
        return this.http.put(this.deleteOrientationSetupsubitemUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getPredefinedChilist(){
		 return this.http.get(this.getPredfinedCheckListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
    //check for duplicate ID department
    checkDuplicateOrientationSetupId(OrientationSetup: any) {
        return this.http.post(this.getOrientationSetupIdcheck, { 'OrientationSetup': OrientationSetup }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
   /* getSkillsSetup(skillId: string) {
        return this.http.post(this.getSkillSteupById, { skillId: skillId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }*/

     // getting Skill set list
   /*  getSkillSetList() {
        return this.http.post(this.getSkillSetListUrl,{ "searchKeyword" : "", "sortOn" : "skillId",
        "sortBy" : "DESC",
       "pageNumber":"0",
       "pageSize":"0"}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }*/
	
	/*getSkillSetupById(skillSetIndexId){
		return this.http.post(this.getSkillSetupByIdUrl,{ id:skillSetIndexId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}*/


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<OrientationSetup>> {
        return this.http.post(this.getAllOrientationSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    /*searchOrientationSetuplist(page: Page, searchKeyword): Observable<PagedData<OrientationSetup>> {
        return this.http.post(this.searchOrientationSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }*/

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<OrientationSetup> {
        let pagedData = new PagedData<OrientationSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let orientationSetup = new OrientationSetup(
                        jsonObj.id,
                        jsonObj.orientationId,
                        jsonObj.orientationDesc,
                        jsonObj.orientationArbicDesc,
						jsonObj.orientationPredefinedCheckListItemId,
						jsonObj.subItems,
                        );
                    pagedData.data.push(orientationSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
	getTerminationsubitem(id:any){
		return this.http.post(this.getTerminationprechecksubitem, {'id':id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}
}
