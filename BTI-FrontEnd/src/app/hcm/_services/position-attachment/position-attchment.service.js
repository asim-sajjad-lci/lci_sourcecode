"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var position_attachment_module_1 = require("../../_models/position-attachment/position-attachment.module");
var PositionAttchmentService = (function () {
    //private checkPositionAttachIdx = Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionPlanIdcheck';
    function PositionAttchmentService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.positionURL = 'positionAttachmentSetup';
        this.getAllPositionAttachUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
        this.getAttachmentByIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/attachment';
        this.searchPositionAttachUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPositionSetup';
        this.createPositionAttachUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPosition';
        this.getAllAttachmentByPositionIdIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAllAttachmentByPositionId';
        this.updatePositionAttachUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
        this.deletePositionAttachUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }
    // getAttachmentById(id: number) {
    //     return this.http.put(this.getAttachmentByIdUrl, { id: id }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    PositionAttchmentService.prototype.getAttachmentById = function (id) {
        return this.http.get(this.getAttachmentByIdUrl + "/" + id, { headers: this.headers, responseType: http_1.ResponseContentType.Blob })
            .map(function (res) {
            //const contentDisposition = res.headers.get('content-disposition') || '';              
            // const matches = /filename=([^;]+)/ig.exec(contentDisposition);
            // console.log(matches);
            // const fileName = (matches[1] || 'untitled').trim();
            // console.log(fileName);                
            //return new Blob([res.blob()], { type: 'application/octet-stream' })
            return res.url;
        })
            .catch(this.handleError);
    };
    // get position class detail by Id
    PositionAttchmentService.prototype.getPositionClass = function (page, positionIds) {
        var _this = this;
        return this.http.put(this.getAllAttachmentByPositionIdIdUrl, { positionIds: positionIds }, { headers: this.headers })
            .map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    PositionAttchmentService.prototype.createPositionAttachment = function (positionPlanSetup, file) {
        var formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('DtoPositionSetup', JSON.stringify(positionPlanSetup));
        this.headers.delete('Content-Type');
        return this.http.post(this.createPositionAttachUrl, formData, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // update for edit position class
    PositionAttchmentService.prototype.updatePositionAttachment = function (positionPlanSetup, file) {
        var prop = "$$index";
        delete positionPlanSetup[prop];
        console.log(positionPlanSetup);
        var formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('DtoPositionSetup', JSON.stringify(positionPlanSetup));
        this.headers.delete('Content-Type');
        return this.http.post(this.updatePositionAttachUrl, formData, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // delete position class
    PositionAttchmentService.prototype.deletePositionAttachment = function (ids) {
        return this.http.put(this.deletePositionAttachUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // get list by search keyword
    PositionAttchmentService.prototype.searchPositionAttachmentlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPositionAttachUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // get list
    PositionAttchmentService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllPositionAttachUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // pagination for data
    PositionAttchmentService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var positionAttachment = new position_attachment_module_1.PositionAttachmentModule(jsonObj.id, jsonObj.positionIds, jsonObj.documentAttachmenDesc, jsonObj.attachmentType, jsonObj.attachmentDate, jsonObj.attachmentTime, jsonObj.userId, jsonObj.documentAttachmentName);
                    pagedData.data.push(positionAttachment);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    // error handler
    PositionAttchmentService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return PositionAttchmentService;
}());
PositionAttchmentService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PositionAttchmentService);
exports.PositionAttchmentService = PositionAttchmentService;
//# sourceMappingURL=position-attchment.service.js.map