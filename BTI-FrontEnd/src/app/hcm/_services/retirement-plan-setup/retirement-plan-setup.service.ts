import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { RetirementPlanSetup } from '../../_models/retirement-plan-setup/retirement-plan-setup.module';

@Injectable()
export class RetirementPlanSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'retirementPlanSetup/getAll';
    private searchDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'retirementPlanSetup/getAll';
    private createDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'retirementPlanSetup/create';
    private getDeductionCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'retirementPlanSetup/getretirementPlanId';
    private updateDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'retirementPlanSetup/update';
    private deleteDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'retirementPlanSetup/delete';
    private checkDeductionCodeIdx = Constants.hcmModuleApiBaseUrl + 'retirementPlanSetup/retirementPlanIdcheck';
	private getAllcompInsurancesUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getAllCompanyInsurance';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new Deduction Code
    createDeductionCode(deductionCodeSetup: RetirementPlanSetup) {
        console.log(JSON.stringify(deductionCodeSetup));
        return this.http.post(this.createDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateDeductionCode(deductionCodeSetup: RetirementPlanSetup) {
        return this.http.post(this.updateDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteDeductionCode(ids: any) {
        return this.http.put(this.deleteDeductionCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getDeductionCode(deductionCodeId: string) {
        return this.http.post(this.getDeductionCodeByIdUrl, { deductionCodeId: deductionCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getcompInsurances(){
        return this.http.get(this.getAllcompInsurancesUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<RetirementPlanSetup>> {
        return this.http.post(this.getAllDeductionCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchDeductionCodelist(page: Page, searchKeyword): Observable<PagedData<RetirementPlanSetup>> {
        return this.http.post(this.searchDeductionCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    checkDuplicateDeductionCodeId(deductionCodeId: any) {
        return this.http.post(this.checkDeductionCodeIdx, { 'retirementPlanId': deductionCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<RetirementPlanSetup> {
        let pagedData = new PagedData<RetirementPlanSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let deductionCode = new RetirementPlanSetup(
                        jsonObj.id,
                        jsonObj.retirementPlanId,
                        jsonObj.retirementPlanDescription,
                        jsonObj.retirementPlanArbicDescription,
                        jsonObj.retirementPlanAccountNumber,
                        jsonObj.retirementPlanMatchPercent,
                        jsonObj.retirementPlanMaxPercent,
                        jsonObj.retirementPlanMaxAmount,
						jsonObj.bonus,
                        jsonObj.loans,
                        jsonObj.watingMethod,
						jsonObj.watingPeriods,
						jsonObj.retirementAge,
						jsonObj.retirementContribution,
						jsonObj.retirementPlanAmount,
						jsonObj.retirementPlanPercent,
						jsonObj.retirementFrequency,
						jsonObj.helthInsuranceId,
						jsonObj.retirementEntranceDate,
                        jsonObj.subItems,
                        jsonObj.insCompanyId
                        
                    );
                    pagedData.data.push(deductionCode);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    } 
}
