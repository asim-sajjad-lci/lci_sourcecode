import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {Supervisor} from '../../_models/supervisor/supervisor.module';

@Injectable()
export class SupervisorService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/getAll';
    private searchSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/searchSuperVisor';
    private createSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/create';
    private getSupervisorBySupervisorIdUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/getById';
    private updateSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/update';
    private deleteSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/delete';
    private checkSupervisorIdx = Constants.hcmModuleApiBaseUrl + 'supervisor/supervisorCodeCheck';
    private getEmployeeIdDetail = Constants.hcmModuleApiBaseUrl + 'supervisor/searchUserbyId';
	private superviserIdUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/getAllSupervisorDropDownList';
	private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
	private getUserDetailByUserIdUrl = Constants.userModuleApiBaseUrl + 'user/getUserDetailByUserId';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createSupervisor(supervisor: Supervisor) {
        return this.http.post(this.createSupervisorUrl, JSON.stringify(supervisor), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateSupervisor(supervisor: Supervisor) {
        return this.http.post(this.updateSupervisorUrl, JSON.stringify(supervisor), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteSupervisor(ids: any) {
        return this.http.put(this.deleteSupervisorUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getSuperviserId(){
		return this.http.get(this.superviserIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getUserDetailByUserId(id){
		return this.http.post(this.getUserDetailByUserIdUrl,{"userId": id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
          

	
	}
	getEmployee(){
		return this.http.get(this.employeeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
    //check for duplicate ID department
    checkDuplicateSupervisionCode(superVisionCode: any) {
        return this.http.post(this.checkSupervisorIdx, { 'superVisionCode': superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getSupervisor(superVisionCode: string) {
        return this.http.post(this.getSupervisorBySupervisorIdUrl, { id: superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<Supervisor>> {
        return this.http.post(this.getAllSupervisorUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchSupervisorlist(page: Page, searchKeyword): Observable<PagedData<Supervisor>> {
        return this.http.post(this.searchSupervisorUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<Supervisor> {
        let pagedData = new PagedData<Supervisor>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let supervisor = new Supervisor(
                        jsonObj.id,
                        jsonObj.superVisionCode,
                        jsonObj.description,
                        jsonObj.arabicDescription,
                        jsonObj.employeeName,
                        jsonObj.employeeId,
                        jsonObj.employeeIds
                );
                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
}
