"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var supervisor_module_1 = require("../../_models/supervisor/supervisor.module");
var SupervisorService = (function () {
    //initializing parameter for constructor
    function SupervisorService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllSupervisorUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'supervisor/getAll';
        this.searchSupervisorUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'supervisor/searchSuperVisor';
        this.createSupervisorUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'supervisor/create';
        this.getSupervisorBySupervisorIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
        this.updateSupervisorUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'supervisor/update';
        this.deleteSupervisorUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'supervisor/delete';
        this.checkSupervisorIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'supervisor/supervisorCodeCheck';
        this.getEmployeeIdDetail = Constants_1.Constants.hcmModuleApiBaseUrl + 'supervisor/searchUserbyId';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new location
    SupervisorService.prototype.createSupervisor = function (supervisor) {
        return this.http.post(this.createSupervisorUrl, JSON.stringify(supervisor), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    SupervisorService.prototype.updateSupervisor = function (supervisor) {
        return this.http.post(this.updateSupervisorUrl, JSON.stringify(supervisor), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    SupervisorService.prototype.deleteSupervisor = function (ids) {
        return this.http.put(this.deleteSupervisorUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    SupervisorService.prototype.checkDuplicateSupervisionCode = function (superVisionCode) {
        return this.http.post(this.checkSupervisorIdx, { 'superVisionCode': superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    SupervisorService.prototype.getSupervisor = function (superVisionCode) {
        return this.http.post(this.getSupervisorBySupervisorIdUrl, { superVisionCode: superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    SupervisorService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllSupervisorUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    SupervisorService.prototype.searchSupervisorlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchSupervisorUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    SupervisorService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var supervisor = new supervisor_module_1.Supervisor(jsonObj.id, jsonObj.superVisionCode, jsonObj.description, jsonObj.arabicDescription, jsonObj.employeeName, jsonObj.employeeId);
                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    SupervisorService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    /*//get department detail by Id
    getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .subscribe(data => {
                return data.json();
            });

    }*/
    SupervisorService.prototype.getEmployeeDetail = function (employeeId) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    return SupervisorService;
}());
SupervisorService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], SupervisorService);
exports.SupervisorService = SupervisorService;
//# sourceMappingURL=supervisor.service.js.map