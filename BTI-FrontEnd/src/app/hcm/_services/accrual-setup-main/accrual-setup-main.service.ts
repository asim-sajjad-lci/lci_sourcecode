import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { AccrualSetupMain } from '../../_models/accrual-setup-main/accrual-setup-main.module';

@Injectable()
export class AccrualCodeSetupMainService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'accrualSetup/getAll';
    private searchDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'accrualSetup/getAll';
    private createDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'accrualSetup/create';
    private getDeductionCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'accrualSetup/getaccuralId';
    private updateDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'accrualSetup/update';
    private deleteDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'accrualSetup/delete';
    private checkDeductionCodeIdx = Constants.hcmModuleApiBaseUrl + '/accrualSetup/accuralidcheck ';
	private typereasoncode= Constants.hcmModuleApiBaseUrl + 'atteandaceOption/getAll';
	private getaccrualby= Constants.hcmModuleApiBaseUrl + 'accurualType/getAllAccuralTypes';
	private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/searchIds';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new Deduction Code
    createDeductionCode(deductionCodeSetup: AccrualSetupMain) {
        console.log(JSON.stringify(deductionCodeSetup));
        return this.http.post(this.createDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

	getPositionClassList() {
        return this.http.post(this.getPositionClassListUrl,{
				"searchKeyword": "",
				"sortOn" : "",
				"sortBy" : "DESC",
				"pageNumber":"0",
				"pageSize":"2"
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update for edit department
    updateDeductionCode(deductionCodeSetup: AccrualSetupMain) {
        return this.http.post(this.updateDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteDeductionCode(ids: any) {
        return this.http.put(this.deleteDeductionCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getDeductionCode(deductionCodeId: number) {
        return this.http.post(this.getDeductionCodeByIdUrl, { id: deductionCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	//get accrual by
	getaccrualbymethod(){
		return this.http.post(this.getaccrualby, { "searchKeyword" :""}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	//get type and reason
	typereasoncodemethod() {
        return this.http.put(this.typereasoncode, { "searchKeyword" :""}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<AccrualSetupMain>> {
        return this.http.post(this.getAllDeductionCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchDeductionCodelist(page: Page, searchKeyword): Observable<PagedData<AccrualSetupMain>> {
        return this.http.post(this.searchDeductionCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    checkDuplicateDeductionCodeId(deductionCodeId: any) {
        return this.http.post(this.checkDeductionCodeIdx, { 'accuralId': deductionCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<AccrualSetupMain> {
        let pagedData = new PagedData<AccrualSetupMain>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let deductionCode = new AccrualSetupMain(
                        jsonObj.id,
                        jsonObj.accuralId,
                        jsonObj.desc,
                        jsonObj.arbic,
                        jsonObj.numHour,
                        jsonObj.maxAccrualHour,
                        jsonObj.maxHour,
						jsonObj.accrualTyepId,
                        jsonObj.workHour,
                        jsonObj.numDay,
                        jsonObj.period,
						jsonObj.reasons,
						jsonObj.type,
						
                        
                    );
                    pagedData.data.push(deductionCode);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    } 
}
