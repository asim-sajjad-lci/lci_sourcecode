/**
 * A service class for benefit-preferences
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BenefitPreferences } from "../../_models/benefit-preferences/benefit-preferences";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class BenefitPreferencesService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private createBenefitPreferencesUrl = Constants.hcmModuleApiBaseUrl + 'benefitPreferences/create';
    private getBenefitPreferencesByBenefitPreferencesIdUrl = Constants.hcmModuleApiBaseUrl + 'benefitPreferences/getById';
    private updateBenefitPreferencesUrl = Constants.hcmModuleApiBaseUrl + 'benefitPreferences/update';
    private deleteBenefitPreferencesUrl = Constants.hcmModuleApiBaseUrl + 'benefitPreferences/delete';
    private getAllUrl = Constants.hcmModuleApiBaseUrl + 'benefitPreferences/getAll';
    private getIdsUrl = Constants.hcmModuleApiBaseUrl + 'benefitPreferences/getIds';
   

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



   

    //add new BenefitPreferences
    createBenefitPreferences(benefitPreferences: BenefitPreferences) {
        return this.http.post(this.createBenefitPreferencesUrl, JSON.stringify(benefitPreferences), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit benefitPreferences
    updateBenefitPreferences(benefitPreferences: BenefitPreferences) {
        return this.http.post(this.updateBenefitPreferencesUrl, JSON.stringify(benefitPreferences), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get benefitPreferences detail by Id
    getBenefitPreferences(benefitPreferencesId: string) {
        return this.http.post(this.getBenefitPreferencesByBenefitPreferencesIdUrl, { benefitPreferencesId: benefitPreferencesId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //get benefitPreferences getAll
     getAllBenefitPreferences(data) {
        return this.http.post(this.getAllUrl, data, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }



    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}