/**
 * A service class for benefit-preferences
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { SettingFamilyLeave } from "../../_models/benefit-preferences/setting-family-leave";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class SettingFamilyLeaveService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private createSettingFamilyLeaveUrl = Constants.hcmModuleApiBaseUrl + 'settingFamilyLeave/create';
    private getSettingFamilyLeaveBySettingFamilyLeaveIdUrl = Constants.hcmModuleApiBaseUrl + 'settingFamilyLeave/getById';
    private updateSettingFamilyLeaveUrl = Constants.hcmModuleApiBaseUrl + 'settingFamilyLeave/update';
    private deleteSettingFamilyLeaveUrl = Constants.hcmModuleApiBaseUrl + 'settingFamilyLeave/delete';
    private getAllUrl = Constants.hcmModuleApiBaseUrl + 'settingFamilyLeave/getAll';
    private getIdsUrl = Constants.hcmModuleApiBaseUrl + 'settingFamilyLeave/getIds';
   

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



   

    //add new SettingFamilyLeave
    createSettingFamilyLeave(settingFamilyLeave: SettingFamilyLeave) {
        return this.http.post(this.createSettingFamilyLeaveUrl, JSON.stringify(settingFamilyLeave), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit settingFamilyLeave
    updateSettingFamilyLeave(settingFamilyLeave: SettingFamilyLeave) {
        return this.http.post(this.updateSettingFamilyLeaveUrl, JSON.stringify(settingFamilyLeave), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get settingFamilyLeave detail by Id
    getSettingFamilyLeave(settingFamilyLeaveId: string) {
        return this.http.post(this.getSettingFamilyLeaveBySettingFamilyLeaveIdUrl, { settingFamilyLeaveId: settingFamilyLeaveId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //get settingFamilyLeave getAll
     getAllSettingFamilyLeave(data) {
        return this.http.post(this.getAllUrl, data, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }



    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}