/**
 * A service class for buildChecks
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BuildChecks } from '../../_models/build-checks/build-checks.module';
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class BuildChecksService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllBuildChecksUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/getAll';
    //private searchBuildChecksUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/searchBuildChecks';
    // private createBuildChecksUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/create';
    private createBuildChecksUrl = Constants.hcmModuleApiDirectBaseUrl + 'buildChecks/create';
    // private createDefaultUrl = Constants.hcmModuleApiBaseUrl + 'default/create';
    private createDefaultUrl = Constants.hcmModuleApiDirectBaseUrl + 'default/create';
    private getBuildChecksByBuildChecksIdUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/getById';
    private getBuildChecksByDefaultIdUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/getByDefaultId';
    // private updateBuildChecksUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/update';
    private updateBuildChecksUrl = Constants.hcmModuleApiDirectBaseUrl + 'buildChecks/update';
    private deleteBuildChecksUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/deleteBuild';
    //private checkBuildChecksIdx = Constants.hcmModuleApiBaseUrl + 'buildChecks/buildChecksIdcheck';
    private getAlEmployeeDropDownURL = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
    private getAllDepartmentDropDownURL = Constants.hcmModuleApiBaseUrl + 'department/getAllDepartmentDropDown';
    private getAllDefaultDropDownURL = Constants.hcmModuleApiBaseUrl + 'default/getAllDefaultDropDown';
    private getEmployeeByDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllByEmployeeForDepartmentList'
    private getAllForEmployeePaycodeUrl = Constants.hcmModuleApiBaseUrl + 'buildPayrollCheckByPayCodes/getAllForEmployeePaycode'

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }

    //add new buildChecks
    createBuildChecks(buildChecks: BuildChecks) {
        return this.http.post(this.createBuildChecksUrl,{
            "description":buildChecks.description,
            "checkDate":buildChecks.checkDate,
            "checkByUserId":buildChecks.checkByUserId,
            "typeofPayRun":buildChecks.typeofPayRun,
            "dateFrom":buildChecks.dateFrom,
            "dateTo":buildChecks.dateTo,
            "weekly":buildChecks.weekly,
            "biweekly":buildChecks.biweekly,
            "semimonthly":buildChecks.semimonthly,
            "monthly":buildChecks.monthly,
            "quarterly":buildChecks.quarterly,
            "semiannually":buildChecks.semiannually,
            "annually":buildChecks.annually,
            "dailyMisc":buildChecks.dailyMisc,
            "allEmployees":buildChecks.allEmployees,
            "fromEmployeeId":buildChecks.fromEmployeeId,
            "toEmployeeId":buildChecks.toEmployeeId,
            "allDepertment":buildChecks.allDepartment,
            "fromDepartmentId":buildChecks.fromDepartmentId,
            "toDepartmentId":buildChecks.toDepartmentId,
            "default1":{
                "id": buildChecks.defaultId
            },
            "listEmployeeId":buildChecks.dtoEmployeeMasters,
            "listDepartmentId":buildChecks.dtoDepartments
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    buildBuildChecks(default1: BuildChecks) {
        return this.http.post(this.createDefaultUrl, {
            "defaultID": default1.id,
            "desc": default1.description,
            "arabicDesc": ""
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAllDefaultDropDown() {
        return this.http.get(this.getAllDefaultDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAllDepartmentDropDown() {
        return this.http.get(this.getAllDepartmentDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAllEmployeeDropDown() {
        return this.http.get(this.getAlEmployeeDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getAllEmployeeByDepartment(ids) {
        return this.http.post(this.getEmployeeByDepartmentUrl,{ids:ids}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAllForEmployeePaycode(empPaycodeModel:any) {
        return this.http.post(this.getAllForEmployeePaycodeUrl,{empPaycodeModel}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit buildChecks
    updateBuildChecks(buildChecks: BuildChecks) {
        console.log("buildChecks", buildChecks)
        return this.http.post(this.updateBuildChecksUrl, {
            "id"    :           buildChecks.id,
            "description"   :   buildChecks.description,
            "checkDate" :       buildChecks.checkDate,
            "checkByUserId" :   buildChecks.checkByUserId,
            "typeofPayRun"  :   buildChecks.typeofPayRun,
            "dateFrom"  :       buildChecks.dateFrom,
            "dateTo"    :       buildChecks.dateTo,
            "weekly"    :       buildChecks.weekly,
            "biweekly"  :       buildChecks.biweekly,
            "semimonthly"   :   buildChecks.semimonthly,
            "monthly"   :       buildChecks.monthly,
            "quarterly" :       buildChecks.quarterly,
            "semiannually"  :   buildChecks.semiannually,
            "annually"  :       buildChecks.annually,
            "dailyMisc" :       buildChecks.dailyMisc,
            "allEmployees"  :   buildChecks.allEmployees,
            "fromEmployeeId":   buildChecks.fromEmployeeId,
            "toEmployeeId"  :   buildChecks.toEmployeeId,
            "allDepertment" :   buildChecks.allDepartment,
            "fromDepartmentId": buildChecks.fromDepartmentId,
            "toDepartmentId":   buildChecks.toDepartmentId,
            "default1"  :{
                "id"    :   buildChecks.defaultId
            },
            "listEmployeeId":buildChecks.dtoEmployeeMasters,
            "listDepartmentId":buildChecks.dtoDepartments
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete buildChecks
    deleteBuildChecks(ids: any) {
        return this.http.put(this.deleteBuildChecksUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID buildChecks
    /* checkDuplicateDeptId(buildChecksId: any) {
        return this.http.post(this.checkBuildChecksIdx, { 'buildChecksId': buildChecksId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    } */

    //get buildChecks detail by Id
    getBuildChecks(buildChecksId: string) {
        return this.http.post(this.getBuildChecksByBuildChecksIdUrl, { buildChecksId: buildChecksId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //get buildChecks detail by Id
    getBuildCheckByDefaultID(defaultPrimaryId: number) {
        return this.http.post(this.getBuildChecksByDefaultIdUrl, { "id": defaultPrimaryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get list
    /* getlist(page: Page, searchKeyword): Observable<PagedData<BuildChecks>> {
        return this.http.put(this.getAllBuildChecksUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    } */

    //get list by search keyword
    /* searchBuildCheckslist(page: Page, searchKeyword): Observable<PagedData<BuildChecks>> {
        return this.http.post(this.searchBuildChecksUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    } */

    //pagination for data
    /* private getPagedData(page: Page, data: any): PagedData<BuildChecks> {
        let pagedData = new PagedData<BuildChecks>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let buildChecks = new BuildChecks(
                        jsonObj.id,
                        jsonObj.buildChecksId,
                        jsonObj.buildChecksDescription,
                        jsonObj.arabicBuildChecksDescription
                    );
                    pagedData.data.push(buildChecks);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    } */

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}