import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {TerminationSetupModule} from '../../_models/termination-setup/termination-setup.module';
import {Page} from '../../../_sharedresource/page';
import {Observable} from 'rxjs/Rx';
import {PagedData} from '../../../_sharedresource/paged-data';
import {PositionClassModule} from '../../_models/position-class/position-class.module';

@Injectable()
export class TerminationSetupService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private positionURL = 'terminationSetup';
    private getAllPositionSetupUrl = Constants.hcmModuleApiBaseUrl + 'terminationSetup/getAll';
    private searchPositionSetupUrl = Constants.hcmModuleApiBaseUrl + 'terminationSetup/getAll';
   private createPositionSetupUrl = Constants.hcmModuleApiBaseUrl +  'terminationSetup/create';
   private getPositionSetupByPositionSetupIdUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionSetupDetailByPositionSetupId';
    private updatePositionSetupUrl = Constants.hcmModuleApiBaseUrl + 'terminationSetup/update';
    private deletePositionSetupUrl = Constants.hcmModuleApiBaseUrl + 'terminationSetup/delete';
    private checkPositionSetupIdx = Constants.hcmModuleApiBaseUrl + 'terminationSetup/terminationIdcheck';
    private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'terminationSetup/getAll';
   //private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAllPostionClassId';
    private getTerminationprechecksubitem = Constants.hcmModuleApiBaseUrl + 'orientationPredefinedCheckListItem/getById';
   private getTerminationClassListUrl = Constants.hcmModuleApiBaseUrl + 'orientationPredefinedCheckListItem/getAllList';
   private deletesubitemurl= Constants.hcmModuleApiBaseUrl + 'terminationSetupDetails/delete';

    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // get position setup detail by Id
    getPositionSetup(terminationSetupId: string) {
        return this.http.post(this.getPositionSetupByPositionSetupIdUrl, { terminationSetupId: terminationSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // check for duplicate ID position setup
    checkDuplicatePositionSetupId(terminationSetupId: any) {
        return this.http.post(this.checkPositionSetupIdx, { 'terminationId': terminationSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // add new position setup
    createPositionSetup(terminationSetup: TerminationSetupModule) {
        return this.http.post(this.createPositionSetupUrl, JSON.stringify(terminationSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit position setup
    updatePositionSetup(terminationSetup: TerminationSetupModule) {
        return this.http.post(this.updatePositionSetupUrl, JSON.stringify(terminationSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getTerminationsubitem(id:any){
		return this.http.post(this.getTerminationprechecksubitem, {'id':id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}
	deleteSubitem(ids:any){
		return this.http.put(this.deletesubitemurl, { 'ids': ids }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
	
	}

    // delete position setup
    deletePositionSetup(ids: any) {
        return this.http.put(this.deletePositionSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    searchPositionSetuplist(page: Page, searchKeyword): Observable<PagedData<TerminationSetupModule>> {
        return this.http.post(this.searchPositionSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get list
    getlist(page: Page, searchKeyword): Observable<PagedData<TerminationSetupModule>> {
        return this.http.post(this.getAllPositionSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<TerminationSetupModule> {
        let pagedData = new PagedData<TerminationSetupModule>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let terminationSetup = new TerminationSetupModule(
                        jsonObj.id,
                        jsonObj.terminationId,
                        jsonObj.terminationDesc,
                        jsonObj.terminationArbicDesc,
						jsonObj.terminationDetailId,
						jsonObj.predefinecheckListtypeId,
						jsonObj.orientationPredefinedCheckListItemId,
						jsonObj.subItems
                    );
                    pagedData.data.push(terminationSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // getting Position Class list
    getTerminationClassList() {
        return this.http.get(this.getTerminationClassListUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    /*// get list
    getPositionClassList(page: Page, searchKeyword): Observable<PagedData<PositionSetupModule>> {
        return this.http.put(this.getPositionClassListUrl, {
            'searchKeyword': searchKeyword
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }*/

    // getting Skill set list
    // getSkillSetList() {
    //     return this.http.get(this.getSkillSetListUrl, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
