import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { LifeInsuranceSetup } from '../../_models/life-insurance-setup/life-insurance-setup.module';
import { Constants } from '../../../_sharedresource/Constants';

@Injectable()
export class LifeInsuranceSetupService { 

  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllLifeInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'lifeInsuranceSetup/getAll';
  private createLifeInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'lifeInsuranceSetup/create';
  private updateLifeInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'lifeInsuranceSetup/update';
  private deleteLifeInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'lifeInsuranceSetup/delete';
  private searchLifeInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'lifeInsurance/search';

  private getAllcompInsurancesUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getAllCompanyInsurance';

  private checklifeInsuranceIdUrl = Constants.hcmModuleApiBaseUrl + 'lifeInsuranceSetup/getLifeInsuranceSetupId';
  // initializing parameter for constructor
  constructor(private http: Http) {
      var userData = JSON.parse(localStorage.getItem('currentUser'));
      this.headers.append('session', userData.session);
      this.headers.append('userid', userData.userId);
      var currentLanguage = localStorage.getItem('currentLanguage') ?
          localStorage.getItem('currentLanguage') : "1";
      this.headers.append("langid", currentLanguage);
      this.headers.append("tenantid", localStorage.getItem('tenantid'));
      console.log('Header: ', this.headers)
  }


  //add new location
  createLifeInsurance(lifeInsurance: LifeInsuranceSetup) {
      return this.http.post(this.createLifeInsuranceUrl, JSON.stringify(lifeInsurance), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

  //update for edit department
  updateLifeInsurance(lifeInsurance: LifeInsuranceSetup) {
      return this.http.post(this.updateLifeInsuranceUrl, JSON.stringify(lifeInsurance), { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  //delete department
  deleteLifeInsurance(ids: any) {
      return this.http.put(this.deleteLifeInsuranceUrl, { 'ids': ids }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }


  //get list



    getcompInsurances(){
        return this.http.get(this.getAllcompInsurancesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    
  
  getlist(page: Page, searchKeyword): Observable<PagedData<LifeInsuranceSetup>> {
      return this.http.post(this.getAllLifeInsuranceUrl, {
          'searchKeyword': searchKeyword,
          'pageNumber': page.pageNumber,
          'pageSize': page.size,
          'sortOn': page.sortOn,
          'sortBy': page.sortBy
      }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //get list by search keyword
  searchLifeInsurancelist(page: Page, searchKeyword): Observable<PagedData<LifeInsuranceSetup>> {
      return this.http.post(this.searchLifeInsuranceUrl, {
          'searchKeyword': searchKeyword,
          'pageNumber': page.pageNumber,
          'pageSize': page.size
      }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    checkDuplicateLifeInsId(lifeInsuraceId: any) {
        return this.http.post(this.checklifeInsuranceIdUrl, { 'lifeInsuraceId': lifeInsuraceId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<LifeInsuranceSetup> {
      let pagedData = new PagedData<LifeInsuranceSetup>();
      if (data) {
          var gridRecords = data.records;
          page.totalElements = data.totalCount;
          if (gridRecords && gridRecords.length > 0) {
              for (let i = 0; i < gridRecords.length; i++) {
                  let jsonObj = gridRecords[i];
                  let lifeInsurance = new LifeInsuranceSetup(
                      jsonObj.id,
                      jsonObj.lifeInsuraceId,
                      jsonObj.lifeInsuraceDescription,
                      jsonObj.lifeInsuraceDescriptionArabic,
                      jsonObj.healthInsuranceGroupNumber,
                      jsonObj.lifeInsuranceFrequency,
                      jsonObj.amount,
                      jsonObj.spouseAmount,
                      jsonObj.childAmount,
                      jsonObj.coverageTotalAmount,
                      jsonObj.employeePay,
                      jsonObj.startDate,
                      jsonObj.endDate,
                      jsonObj.insuranceType,
                      jsonObj.healthInsuranceId);
                  pagedData.data.push(lifeInsurance);
              }
          }
      }
      page.totalPages = page.totalElements / page.size;
      let start = page.pageNumber * page.size;
      let end = Math.min((start + page.size), page.totalElements);
      pagedData.page = page;
      return pagedData;
  }

  //error handler
  private handleError(error: any): Promise<any> {
      return Promise.reject(error.message || error);
  }
}



