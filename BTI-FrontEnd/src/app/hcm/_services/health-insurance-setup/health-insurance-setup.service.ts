import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { HealthInsuranceSetup } from '../../_models/health-insurance-setup/health-insurance-setup.module';

@Injectable()
export class HealthInsuranceSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllHealthInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getAll';
    private createHealthInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/create';
    private updateHealthInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/update';
    private deleteHealthInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/delete';
    private searchHealthInsuranceUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/search';
    // private getLocationByLocationIdUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getLocationDetailByLocationId';
    // private checkLocationIdx = Constants.hcmModuleApiBaseUrl + 'helthInsurance/locationIdcheck';
	private cheakHealthInsuranceIdUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/helthInsuranceIdcheck'
    private getAllCountriesUrl = Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
    private getAllStatesByCountryIdUrl = Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
    private getAllCitiesByStateIdUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
    private getByHealthInsCompIdUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getById';
	private getCompanyUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getAllCompanyInsurance';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createHealthInsurance(healthInsurance: HealthInsuranceSetup) {
        return this.http.post(this.createHealthInsuranceUrl, JSON.stringify(healthInsurance), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateHealthInsurance(healthInsurance: HealthInsuranceSetup) {
        return this.http.post(this.updateHealthInsuranceUrl, JSON.stringify(healthInsurance), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteHealthInsurance(ids: any) {
        return this.http.put(this.deleteHealthInsuranceUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	checkDuplicatehealthIns(helthInsuranceId: any){
      return this.http.post(this.cheakHealthInsuranceIdUrl, { 'insCompanyId': helthInsuranceId}, { headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);

  }
    //get list
    getCountry() {
        return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getCompany() {
		return this.http.get(this.getCompanyUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
    getStatesByCountryId(countryId: number) {
        return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCitiesByStateId(stateId: number) {
        return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    gethealthInsCompanyById(id: any) {
        return this.http.post(this.getByHealthInsCompIdUrl, { id: id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getlist(page: Page, searchKeyword): Observable<PagedData<HealthInsuranceSetup>> {
        return this.http.post(this.getAllHealthInsuranceUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchHealthInsurancelist(page: Page, searchKeyword): Observable<PagedData<HealthInsuranceSetup>> {
        return this.http.post(this.searchHealthInsuranceUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<HealthInsuranceSetup> {
        let pagedData = new PagedData<HealthInsuranceSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let healthInsurance = new HealthInsuranceSetup(
                        jsonObj.id,
                        jsonObj.insCompanyId,
                        jsonObj.desc,
                        jsonObj.arbicDesc,
                        jsonObj.address,
                        jsonObj.contactPerson,
						jsonObj.phoneNo,
                        jsonObj.email,
                        jsonObj.fax,
                        jsonObj.cityName,
                        jsonObj.countryName,
                        jsonObj.stateName,
                        jsonObj.cityId,
                        jsonObj.countryId,
                        jsonObj.stateId
                        );
                    pagedData.data.push(healthInsurance);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
