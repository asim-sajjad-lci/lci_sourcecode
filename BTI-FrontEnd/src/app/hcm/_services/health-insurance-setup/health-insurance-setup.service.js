"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var health_insurance_setup_module_1 = require("../../_models/health-insurance-setup/health-insurance-setup.module");
var HealthInsuranceSetupService = (function () {
    // private getLocationByLocationIdUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getLocationDetailByLocationId';
    // private checkLocationIdx = Constants.hcmModuleApiBaseUrl + 'helthInsurance/locationIdcheck';
    //initializing parameter for constructor
    function HealthInsuranceSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllHealthInsuranceUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthInsurance/getAll';
        this.createHealthInsuranceUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthInsurance/create';
        this.updateHealthInsuranceUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthInsurance/update';
        this.deleteHealthInsuranceUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthInsurance/delete';
        this.searchHealthInsuranceUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthInsurance/search';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new location
    HealthInsuranceSetupService.prototype.createHealthInsurance = function (healthInsurance) {
        return this.http.post(this.createHealthInsuranceUrl, JSON.stringify(healthInsurance), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    HealthInsuranceSetupService.prototype.updateHealthInsurance = function (healthInsurance) {
        return this.http.post(this.updateHealthInsuranceUrl, JSON.stringify(healthInsurance), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    HealthInsuranceSetupService.prototype.deleteHealthInsurance = function (ids) {
        return this.http.put(this.deleteHealthInsuranceUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    // checkDuplicateLocationId(locationId: any) {
    //     return this.http.post(this.checkLocationIdx, { 'locationId': locationId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    //get department detail by Id
    // getLocation(locationId: string) {
    //     return this.http.post(this.getLocationByLocationIdUrl, { locationId: locationId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    //get list
    HealthInsuranceSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllHealthInsuranceUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    HealthInsuranceSetupService.prototype.searchHealthInsurancelist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchHealthInsuranceUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    HealthInsuranceSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var healthInsurance = new health_insurance_setup_module_1.HealthInsuranceSetup(jsonObj.id, jsonObj.insCompanyId, jsonObj.desc, jsonObj.arbicDesc, jsonObj.address, jsonObj.city, jsonObj.contactPerson, jsonObj.phoneNo, jsonObj.email, jsonObj.fax);
                    pagedData.data.push(healthInsurance);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    HealthInsuranceSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return HealthInsuranceSetupService;
}());
HealthInsuranceSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], HealthInsuranceSetupService);
exports.HealthInsuranceSetupService = HealthInsuranceSetupService;
//# sourceMappingURL=health-insurance-setup.service.js.map