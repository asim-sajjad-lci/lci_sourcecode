import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {ExitInterview} from '../../_models/exit-interview/exit-interview.module';

@Injectable()
export class ExitInterviewService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllExitInterviewUrl = Constants.hcmModuleApiBaseUrl + 'exitInterview/getAll';
    private searchExitInterviewUrl = Constants.hcmModuleApiBaseUrl + 'exitInterview/search';
    private createExitInterviewUrl = Constants.hcmModuleApiBaseUrl + 'exitInterview/create';
    private getExitInterviewByIdUrl = Constants.hcmModuleApiBaseUrl + 'exitInterview/getById';
    private updateExitInterviewUrl = Constants.hcmModuleApiBaseUrl + 'exitInterview/update';
    private deleteExitInterviewUrl = Constants.hcmModuleApiBaseUrl + 'exitInterview/delete';
    private checkExitInterviewIdx = Constants.hcmModuleApiBaseUrl + 'exitInterview/exitInterviewIdcheck';
    private getAllCountriesUrl = Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
    private getAllStatesByCountryIdUrl = Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
    private getAllCitiesByStateIdUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
    private getAllAccrualSchedule = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/getAllIds';
    private getAllLinkedPayCode = Constants.hcmModuleApiBaseUrl + 'payCode/searchPayCodeId';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createExitInterview(exitInterview: ExitInterview) {
        return this.http.post(this.createExitInterviewUrl, JSON.stringify(exitInterview), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateExitInterview(exitInterview: ExitInterview) {
        return this.http.post(this.updateExitInterviewUrl, JSON.stringify(exitInterview), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteExitInterview(ids: any) {
        return this.http.put(this.deleteExitInterviewUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateExitInterviewId(exitInterviewId: any) {
        return this.http.post(this.checkExitInterviewIdx, { 'exitInterviewId': exitInterviewId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getExitInterview(exitInterviewId: string) {
        return this.http.post(this.getExitInterviewByIdUrl, { exitInterviewId: exitInterviewId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCountry() {
        return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    getAccrualSchedule() {
		return this.http.post(this.getAllAccrualSchedule, { "searchKeyword": "" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
       
    }

	getlinkedPayCode(){
		return this.http.post(this.getAllLinkedPayCode, { "searchKeyword": "" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}
    getStatesByCountryId(countryId: number) {
        return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCitiesByStateId(stateId: number) {
        return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<ExitInterview>> {
        return this.http.post(this.getAllExitInterviewUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchExitInterviewlist(page: Page, searchKeyword): Observable<PagedData<ExitInterview>> {
        return this.http.post(this.searchExitInterviewUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<ExitInterview> {
        let pagedData = new PagedData<ExitInterview>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let exitInterview = new ExitInterview(
                        jsonObj.id,
                        jsonObj.exitInterviewId,
                        jsonObj.inActive,
                        jsonObj.desc,
                        jsonObj.arbicDesc);
                    pagedData.data.push(exitInterview);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
