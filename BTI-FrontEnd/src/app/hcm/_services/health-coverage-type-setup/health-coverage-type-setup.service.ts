import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {HealthCoverageTypeSetup} from '../../_models/health-coverage-type-setup/health-coverage-type-setup.module';

@Injectable()
export class HealthCoverageTypeSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllHealthCoverageTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/getAll';
    private createHealthCoverageTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/create';
    private updateHealthCoverageTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/update';
    private deleteHealthCoverageTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/delete';
    private searchHealthCoverageTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/search';
    private getHelthCoverageTypeId = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/getHelthCoverageTypeId';
    private HelthCoverageTypeIdcheck = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/HelthCoverageTypeIdcheck';
    private healthIdUrl = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/getAllHealthCoverageType';
    private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAllPostionClassId';
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new healthCoverageTypeSetup
    createHealthCoverageTypeSetup(healthCoverageTypeSetup: HealthCoverageTypeSetup) {
        return this.http.post(this.createHealthCoverageTypeSetupUrl, JSON.stringify(healthCoverageTypeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateHealthCoverageTypeSetup(healthCoverageTypeSetup: HealthCoverageTypeSetup) {
        return this.http.post(this.updateHealthCoverageTypeSetupUrl, JSON.stringify(healthCoverageTypeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteHealthCoverageTypeSetup(ids: any) {
        return this.http.put(this.deleteHealthCoverageTypeSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateHealthCoverageTypeSetupId(healthCoverageTypeSetupId: any) {
        return this.http.post(this.HelthCoverageTypeIdcheck, { 'helthCoverageId': healthCoverageTypeSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getHealthCoverageTypeSetup(healthCoverageTypeSetupId: string) {
        return this.http.post(this.getHelthCoverageTypeId, { id: healthCoverageTypeSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting Position Class list
    getPositionClassList() {
        return this.http.get(this.getPositionClassListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<HealthCoverageTypeSetup>> {
        return this.http.post(this.getAllHealthCoverageTypeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchlist(page: Page, searchKeyword): Observable<PagedData<HealthCoverageTypeSetup>> {
        return this.http.post(this.searchHealthCoverageTypeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<HealthCoverageTypeSetup> {
        let pagedData = new PagedData<HealthCoverageTypeSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let healthCoverageTypeSetup = new HealthCoverageTypeSetup(
                        jsonObj.id,
                        jsonObj.helthCoverageId,
                        jsonObj.desc,
                        jsonObj.arbicDesc);
                    pagedData.data.push(healthCoverageTypeSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getHealthId(){
        return this.http.get(this.healthIdUrl,  { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
}
