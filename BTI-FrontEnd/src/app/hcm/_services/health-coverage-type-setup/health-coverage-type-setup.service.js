"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var health_coverage_type_setup_module_1 = require("../../_models/health-coverage-type-setup/health-coverage-type-setup.module");
var HealthCoverageTypeSetupService = (function () {
    //initializing parameter for constructor
    function HealthCoverageTypeSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllHealthCoverageTypeSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthCoverageType/getAll';
        this.createHealthCoverageTypeSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthCoverageType/create';
        this.updateHealthCoverageTypeSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthCoverageType/update';
        this.deleteHealthCoverageTypeSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthCoverageType/delete';
        this.searchHealthCoverageTypeSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthCoverageType/search';
        this.getHelthCoverageTypeId = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthCoverageType/getHelthCoverageTypeId';
        this.HelthCoverageTypeIdcheck = Constants_1.Constants.hcmModuleApiBaseUrl + 'helthCoverageType/HelthCoverageTypeIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new healthCoverageTypeSetup
    HealthCoverageTypeSetupService.prototype.createHealthCoverageTypeSetup = function (healthCoverageTypeSetup) {
        return this.http.post(this.createHealthCoverageTypeSetupUrl, JSON.stringify(healthCoverageTypeSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    HealthCoverageTypeSetupService.prototype.updateHealthCoverageTypeSetup = function (healthCoverageTypeSetup) {
        return this.http.post(this.updateHealthCoverageTypeSetupUrl, JSON.stringify(healthCoverageTypeSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    HealthCoverageTypeSetupService.prototype.deleteHealthCoverageTypeSetup = function (ids) {
        return this.http.put(this.deleteHealthCoverageTypeSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    HealthCoverageTypeSetupService.prototype.checkDuplicateHealthCoverageTypeSetupId = function (healthCoverageTypeSetupId) {
        return this.http.post(this.HelthCoverageTypeIdcheck, { 'healthCoverageTypeSetupId': healthCoverageTypeSetupId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    HealthCoverageTypeSetupService.prototype.getHealthCoverageTypeSetup = function (healthCoverageTypeSetupId) {
        return this.http.post(this.getHelthCoverageTypeId, { healthCoverageTypeSetupId: healthCoverageTypeSetupId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    HealthCoverageTypeSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllHealthCoverageTypeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    HealthCoverageTypeSetupService.prototype.searchlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchHealthCoverageTypeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    HealthCoverageTypeSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var healthCoverageTypeSetup = new health_coverage_type_setup_module_1.HealthCoverageTypeSetup(jsonObj.id, jsonObj.helthCoverageId, jsonObj.desc, jsonObj.arbicDesc);
                    pagedData.data.push(healthCoverageTypeSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    HealthCoverageTypeSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return HealthCoverageTypeSetupService;
}());
HealthCoverageTypeSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], HealthCoverageTypeSetupService);
exports.HealthCoverageTypeSetupService = HealthCoverageTypeSetupService;
//# sourceMappingURL=health-coverage-type-setup.service.js.map