import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { BenefitCodeSetup } from "../../_models/benefit-code-setup/benefit-code-setup.module";

@Injectable()
export class BenefitCodeSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/getAll';
    private searchBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/search';
    private createBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/create';
    private getBenefitByBenefitIdUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/getBenefitCodeById';
    private updateBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/update';
    private deleteBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/delete';
    private checkBenefitIdx = Constants.hcmModuleApiBaseUrl + 'benefitCode/benefitCodeIdcheck';
    private benefitcodeIdUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/searchAllBenefitId';
    private getAllBasedOnPayCodeDropDownUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPaycodeforDeduction';

    private typeIdDetails = Constants.hcmModuleApiBaseUrl + 'typeFieldForCodes/getAllIds';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new Deduction Code
    createBenefitCodeSetup(benefitCode: BenefitCodeSetup) {
        console.log(JSON.stringify(benefitCode));
        return this.http.post(this.createBenefitCodeUrl, JSON.stringify(benefitCode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateBenefitCode(benefitCode: BenefitCodeSetup) {
        return this.http.post(this.updateBenefitCodeUrl, JSON.stringify(benefitCode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteBenefitCode(ids: any) {
        return this.http.put(this.deleteBenefitCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getBenefitCode(benefitId: any) {
        return this.http.post(this.getBenefitByBenefitIdUrl, { id: benefitId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<BenefitCodeSetup>> {
        return this.http.post(this.getAllBenefitCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get department detail by Id
    getAllBasedOnPayCodeDropDown(payload: any) {
        return this.http.post(this.getAllBasedOnPayCodeDropDownUrl, payload, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list by search keyword
    searchBenefitCodelist(page: Page, searchKeyword): Observable<PagedData<BenefitCodeSetup>> {
        return this.http.post(this.searchBenefitCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    checkDuplicateBenefitId(benefitId: any) {
        return this.http.post(this.checkBenefitIdx, { 'benefitId': benefitId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<BenefitCodeSetup> {
        let pagedData = new PagedData<BenefitCodeSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let benefitCodeSetup = new BenefitCodeSetup(
                        jsonObj.id,
                        jsonObj.benefitId,
                        jsonObj.desc,
                        jsonObj.arbicDesc,
                        jsonObj.startDate,
                        jsonObj.endDate,
                        jsonObj.frequency,
                        jsonObj.method,
                        jsonObj.amount,
                        jsonObj.percent,
                        jsonObj.transction,
                        jsonObj.inActive,
                        jsonObj.perPeriod,
                        jsonObj.perYear,
                        jsonObj.lifeTime,
                        jsonObj.dtoPayCode,
                        jsonObj.payFactor,
                        jsonObj.customDate,
                        jsonObj.noOfDays,
                        jsonObj.endDateDays,
                        jsonObj.benefitTypeId,
                        jsonObj.roundOf
                    );
                    pagedData.data.push(benefitCodeSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getBenefitcodeId() {
        return this.http.post(this.benefitcodeIdUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

           //get type field detail by Id
           getTypeFieldDetails(typeid) {
            return this.http.post(this.typeIdDetails, { id: typeid }, { headers: this.headers })
                .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
        }
}
