"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var benefit_code_setup_module_1 = require("../../_models/benefit-code-setup/benefit-code-setup.module");
var BenefitCodeSetupService = (function () {
    //initializing parameter for constructor
    function BenefitCodeSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllBenefitCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'benefitCode/getAll';
        this.searchBenefitCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'benefitCode/search';
        this.createBenefitCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'benefitCode/create';
        this.getBenefitByBenefitIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'benefitCode/getBenefitCodeById';
        this.updateBenefitCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'benefitCode/update';
        this.deleteBenefitCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'benefitCode/delete';
        this.checkBenefitIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'benefitCode/benefitCodeIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new Deduction Code
    BenefitCodeSetupService.prototype.createBenefitCodeSetup = function (benefitCode) {
        console.log(JSON.stringify(benefitCode));
        return this.http.post(this.createBenefitCodeUrl, JSON.stringify(benefitCode), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    BenefitCodeSetupService.prototype.updateBenefitCode = function (benefitCode) {
        return this.http.post(this.updateBenefitCodeUrl, JSON.stringify(benefitCode), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    BenefitCodeSetupService.prototype.deleteBenefitCode = function (ids) {
        return this.http.put(this.deleteBenefitCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    BenefitCodeSetupService.prototype.getBenefitCode = function (benefitId) {
        return this.http.post(this.getBenefitByBenefitIdUrl, { benefitId: benefitId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    BenefitCodeSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllBenefitCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    BenefitCodeSetupService.prototype.searchBenefitCodelist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchBenefitCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    BenefitCodeSetupService.prototype.checkDuplicateBenefitId = function (benefitId) {
        return this.http.post(this.checkBenefitIdx, { 'benefitId': benefitId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //pagination for data
    BenefitCodeSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var benefitCodeSetup = new benefit_code_setup_module_1.BenefitCodeSetup(jsonObj.id, jsonObj.benefitId, jsonObj.desc, jsonObj.arbicDesc, jsonObj.startDate, jsonObj.endDate, jsonObj.frequency, jsonObj.method, jsonObj.amount, jsonObj.transction, jsonObj.inActive, jsonObj.perPeriod, jsonObj.perYear, jsonObj.lifeTime);
                    pagedData.data.push(benefitCodeSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    BenefitCodeSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return BenefitCodeSetupService;
}());
BenefitCodeSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], BenefitCodeSetupService);
exports.BenefitCodeSetupService = BenefitCodeSetupService;
//# sourceMappingURL=benefit-code-setup.service.js.map