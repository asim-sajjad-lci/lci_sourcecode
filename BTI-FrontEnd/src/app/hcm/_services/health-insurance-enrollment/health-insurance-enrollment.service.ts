import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {HealthInsuranceEnrollment} from '../../_models/health-insurance-enrollment/health-insurance-enrollment.module';

@Injectable()
export class HealthInsuranceEnrollmentService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllHealthInsuranceEnrollmentUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/getAll';
    private searchHealthInsuranceEnrollmentUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/searchSuperVisor';
    private createHealthInsuranceEnrollmentUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/create';
    private getHealthInsuranceEnrollmentByHealthInsuranceEnrollmentIdUrl = Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
    private updateHealthInsuranceEnrollmentUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/update';
    private deleteHealthInsuranceEnrollmentUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/delete';
    private checkHealthInsuranceEnrollmentIdx = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/healthInsuranceEnrollmentCodeCheck';
    private getByHealthInsuranceEnrollmentIDUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/getById';
    private getEmployeeIdDetail = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/searchUserbyId';
	private superviserIdUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/getAllHealthInsuranceEnrollmentDropDownList';
	private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/getAllHealthInsuranceEnrollmentDropDownList';
	private getAllEmployeeAddressMasterDropDownUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/getAllEmployeeAddressMasterDropDown';
	private getAllEmployeeNationalitiesDropDownUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/getAllEmployeeNationalitiesDropDown';
    private gethealthInsuranceIdDropDownUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/getAllHelthInsuranceDropDown';
    private employeeIdcheckUrl = Constants.hcmModuleApiBaseUrl + 'healthInsuranceEnrollment/employeeIdHealthInsuranceIdcheck';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createHealthInsuranceEnrollment(healthInsuranceEnrollment: HealthInsuranceEnrollment) {
        return this.http.post(this.createHealthInsuranceEnrollmentUrl, JSON.stringify(healthInsuranceEnrollment), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //add new location
      createHealthInsuranceEnrollmentAttachment(healthInsuranceEnrollment: HealthInsuranceEnrollment,file: File) {
        let formData: FormData = new FormData();
        formData.append('file', file, file.name);
        formData.append('DtoHealthInsuranceEnrollment', JSON.stringify(healthInsuranceEnrollment));
        this.headers.delete('Content-Type');

        return this.http.post(this.createHealthInsuranceEnrollmentUrl, formData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateHealthInsuranceEnrollment(healthInsuranceEnrollment: HealthInsuranceEnrollment) {
        return this.http.post(this.updateHealthInsuranceEnrollmentUrl, JSON.stringify(healthInsuranceEnrollment), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteHealthInsuranceEnrollment(ids: any) {
        return this.http.put(this.deleteHealthInsuranceEnrollmentUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getSuperviserId(){
		return this.http.get(this.superviserIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getEmployee(){
		return this.http.get(this.employeeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllEmployeeAddressMasterDropDown(){
		return this.http.get(this.getAllEmployeeAddressMasterDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllEmployeeNationalitiesDropDown(){
		return this.http.get(this.getAllEmployeeNationalitiesDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	gethealthInsuranceId(){
		return this.http.get(this.gethealthInsuranceIdDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
    //check for duplicate ID department
    getEmpHealthInsEnrolmntById(employeeId: any) {
        return this.http.post(this.getByHealthInsuranceEnrollmentIDUrl, { id: employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update for edit employeeDirectDeposit
    employeeIdcheck(payload) {
        return this.http.post(this.employeeIdcheckUrl, payload, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get department detail by Id
    getHealthInsuranceEnrollment(employeeId: string) {
        return this.http.post(this.getHealthInsuranceEnrollmentByHealthInsuranceEnrollmentIdUrl, { employeeId: employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<HealthInsuranceEnrollment>> {
        return this.http.post(this.getAllHealthInsuranceEnrollmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchHealthInsuranceEnrollmentlist(page: Page, searchKeyword): Observable<PagedData<HealthInsuranceEnrollment>> {
        return this.http.post(this.searchHealthInsuranceEnrollmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<HealthInsuranceEnrollment> {
        let pagedData = new PagedData<HealthInsuranceEnrollment>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    // let healthInsuranceEnrollment = new HealthInsuranceEnrollment(
                    //     jsonObj.id,
                    //     jsonObj.employeeId,
                    //     jsonObj.description,
                    //     jsonObj.arabicDescription,
                    //     jsonObj.employeeName,
                    //     jsonObj.employeeId);
                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
}
