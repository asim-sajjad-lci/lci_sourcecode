import { TestBed, inject } from '@angular/core/testing';

import { AccountStatementReportService } from './account-statement-report.service';

describe('AccountStatementReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountStatementReportService]
    });
  });

  it('should be created', inject([AccountStatementReportService], (service: AccountStatementReportService) => {
    expect(service).toBeTruthy();
  }));
});
