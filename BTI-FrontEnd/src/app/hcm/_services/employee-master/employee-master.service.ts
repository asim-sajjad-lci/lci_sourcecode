import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {EmployeeMaster} from '../../_models/employee-master/employee-master.module';

@Injectable()
export class EmployeeMasterService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmployeeMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAll';
    private searchEmployeeMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/searchSuperVisor';
    private createEmployeeMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/create';
    private getEmployeeMasterByEmployeeMasterIdUrl = Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
    private updateEmployeeMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/update';
    private deleteEmployeeMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/delete';
    private checkEmployeeMasterIdx = Constants.hcmModuleApiBaseUrl + 'employeeMaster/employeeIdcheck';
    private getEmployeeIdDetail = Constants.hcmModuleApiBaseUrl + 'employeeMaster/searchUserbyId';
	private superviserIdUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/getAllSupervisorDropDownList';
	private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
	private getAllEmployeeAddressMasterDropDownUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/getAllEmployeeAddressMasterDropDown';
	private getAllEmployeeNationalitiesDropDownUrl = Constants.hcmModuleApiBaseUrl + 'employeeNationalities/getAllEmployeeNationalitiesDropDown';
	private getgetAllUserDetailDropDownUrl = Constants.userModuleApiBaseUrl + 'user/getAllUserDetail';
    private getAllSupervisorDropDownListURL = Constants.hcmModuleApiBaseUrl + 'supervisor/getAllSupervisorDropDownList';
    private employeeIdcheckurl = Constants.hcmModuleApiBaseUrl +'employeeMaster/employeeIdcheck';
    private getEmployeeDetailsById = Constants.hcmModuleApiBaseUrl +'employeeMaster/getEmployeeByEmployeeId';
    //initializing parameter for constructor
    constructor(private http: Http) {
       // localStorage.setItem('tenantid','bti_hr')
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createEmployeeMaster(employeeMaster: EmployeeMaster) {
        return this.http.post(this.createEmployeeMasterUrl, JSON.stringify(employeeMaster), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //add new location
      createEmployeeMasterAttachment(employeeMaster: EmployeeMaster,file: File) {
        let formData: FormData = new FormData();      
        if(file && file.name){
            formData.append('file', file, file.name);
        }   
        formData.append('DtoEmployeeMaster', JSON.stringify(employeeMaster));
        this.headers.delete('Content-Type');

        return this.http.post(this.createEmployeeMasterUrl, formData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateEmployeeMaster(employeeMaster,file: File) {
        let formData: FormData = new FormData();
        console.log(file);
      
            formData.append('file', file, file.name);
             
        formData.append('DtoEmployeeMaster', JSON.stringify(employeeMaster));
        this.headers.delete('Content-Type');

        return this.http.post(this.updateEmployeeMasterUrl, formData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    // //update for edit department
    // updateEmployeeMaster(employeeMaster: EmployeeMaster) {
    //     return this.http.post(this.updateEmployeeMasterUrl, JSON.stringify(employeeMaster), { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    //delete department
    deleteEmployeeMaster(ids: any) {
        return this.http.put(this.deleteEmployeeMasterUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getSuperviserId(){
		return this.http.get(this.superviserIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getEmployee(){
		return this.http.get(this.employeeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllSupervisorDropDownList(){
		return this.http.get(this.getAllSupervisorDropDownListURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllEmployeeAddressMasterDropDown(){
		return this.http.get(this.getAllEmployeeAddressMasterDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllEmployeeNationalitiesDropDown(){
		return this.http.get(this.getAllEmployeeNationalitiesDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllUserDetailDropDown(){
		return this.http.get(this.getgetAllUserDetailDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
    //check for duplicate ID department
    checkDuplicateEmployeeCode(employeeId: any) {
        return this.http.post(this.checkEmployeeMasterIdx, { 'employeeId': employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getEmployeeMaster(employeeId: string) {
        return this.http.post(this.getEmployeeMasterByEmployeeMasterIdUrl, { employeeId: employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    validateEmployee(employeeId:string){
        return this.http.post(this.employeeIdcheckurl, { employeeId: employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeMaster>> {
        return this.http.post(this.getAllEmployeeMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchEmployeeMasterlist(page: Page, searchKeyword): Observable<PagedData<EmployeeMaster>> {
        return this.http.post(this.searchEmployeeMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeMaster> {
        let pagedData = new PagedData<EmployeeMaster>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    // let employeeMaster = new EmployeeMaster(
                    //     jsonObj.id,
                    //     jsonObj.employeeId,
                    //     jsonObj.description,
                    //     jsonObj.arabicDescription,
                    //     jsonObj.employeeName,
                    //     jsonObj.employeeId);
                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getEmployeeDetailsByEmployeeId(employeeIndexId: any) {
        return this.http.post(this.getEmployeeDetailsById, { 'employeeIndexId': employeeIndexId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
}
