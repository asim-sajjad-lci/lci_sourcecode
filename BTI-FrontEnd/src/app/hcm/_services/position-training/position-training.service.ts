import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {PositionTrainingModule} from '../../_models/position-training/position-training.module';

@Injectable()
export class PositionTrainingService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private positionURL = 'positionCourseToLink';
    private getAllPositionTrainingUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
    private searchPositionTrainingUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/search';
    private createPositionTrainingUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/create';
    private getPositionTrainingByPositionTrainingIdUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionPlanSetupId';
    private updatePositionTrainingUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
    private deletePositionTrainingUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
    private checkPositionTrainingIdx = Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionPlanIdcheck';
    private getTrainingListUrl = Constants.hcmModuleApiBaseUrl + 'trainingCourseDetail/getAllIds';
    private getCourseListUrl = Constants.hcmModuleApiBaseUrl + 'traningCourse/getAllTraningCourseId';

  constructor(private http: Http) {
      let userData = JSON.parse(localStorage.getItem('currentUser'));
      this.headers.append('session', userData.session);
      this.headers.append('userid', userData.userId);
      let currentLanguage = localStorage.getItem('currentLanguage') ?
          localStorage.getItem('currentLanguage') : '1';
      this.headers.append('langid', currentLanguage);
      this.headers.append('tenantid', localStorage.getItem('tenantid'));
  }

    // get position class detail by Id
    getPositionClass(positionPlanId: string) {
      return this.http.post(this.getPositionTrainingByPositionTrainingIdUrl, { positionPlanId: positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // check for duplicate ID position class
    checkDuplicatePositionPlanSetupId(positionPlanId: any) {
        return this.http.post(this.checkPositionTrainingIdx, { 'positionPlanId': positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	 getTrainingList() {
        return this.http.post(this.getTrainingListUrl,{ "searchKeyword" : "",
				"sortOn" : "", 
				"sortBy" : "DESC",
				"pageNumber":"0",
				"pageSize":"10"    }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	 getCourseList() {
        return this.http.post(this.getCourseListUrl,{ "searchKeyword" : "",
				"sortOn" : "", 
				"sortBy" : "DESC",
				"pageNumber":"0",
				"pageSize":"10"    }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // add new position class
    createPositionPlanSetup(positionPlanSetup: PositionTrainingModule) {
        return this.http.post(this.createPositionTrainingUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit position class
    updatePositionClass(positionPlanSetup: PositionTrainingModule) {
        return this.http.post(this.updatePositionTrainingUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete position class
    deletePositionClass(ids: any) {
        return this.http.put(this.deletePositionTrainingUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    searchPositionClasslist(page: Page, searchKeyword): Observable<PagedData<PositionTrainingModule>> {
        return this.http.post(this.searchPositionTrainingUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get list
    getlist(page: Page, searchKeyword, positionId): Observable<PagedData<PositionTrainingModule>> {
        return this.http.post(this.getAllPositionTrainingUrl, {
            'id': positionId,
			'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<PositionTrainingModule> {
        let pagedData = new PagedData<PositionTrainingModule>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let positionPlanSetup = new PositionTrainingModule(
                        jsonObj.id,
                        jsonObj.positionId,
                        jsonObj.positionTrainingDesc,
                        jsonObj.positionTrainingArbicDesc,
                        jsonObj.traningCourseId,
                        jsonObj.traningClassId,
                        jsonObj.courseDesc,
                        jsonObj.classDesc,
                        jsonObj.traningClassIdName,
                        jsonObj.traningCourseIdName,
                        jsonObj.traningClassName
                    );
                    pagedData.data.push(positionPlanSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }


}
