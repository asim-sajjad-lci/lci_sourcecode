"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var position_training_module_1 = require("../../_models/position-training/position-training.module");
var PositionTrainingService = (function () {
    function PositionTrainingService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.positionURL = 'positionCourseToLink';
        this.getAllPositionTrainingUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAllPositionId';
        this.searchPositionTrainingUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/search';
        this.createPositionTrainingUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/create';
        this.getPositionTrainingByPositionTrainingIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionPlanSetupId';
        this.updatePositionTrainingUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
        this.deletePositionTrainingUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
        this.checkPositionTrainingIdx = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionPlanIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }
    // get position class detail by Id
    PositionTrainingService.prototype.getPositionClass = function (positionPlanId) {
        return this.http.post(this.getPositionTrainingByPositionTrainingIdUrl, { positionPlanId: positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // check for duplicate ID position class
    PositionTrainingService.prototype.checkDuplicatePositionPlanSetupId = function (positionPlanId) {
        return this.http.post(this.checkPositionTrainingIdx, { 'positionPlanId': positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // add new position class
    PositionTrainingService.prototype.createPositionPlanSetup = function (positionPlanSetup) {
        return this.http.post(this.createPositionTrainingUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // update for edit position class
    PositionTrainingService.prototype.updatePositionClass = function (positionPlanSetup) {
        return this.http.post(this.updatePositionTrainingUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // delete position class
    PositionTrainingService.prototype.deletePositionClass = function (ids) {
        return this.http.put(this.deletePositionTrainingUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // get list by search keyword
    PositionTrainingService.prototype.searchPositionClasslist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPositionTrainingUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // get list
    PositionTrainingService.prototype.getlist = function (page, searchKeyword, positionId) {
        var _this = this;
        return this.http.put(this.getAllPositionTrainingUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'positionId': positionId
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // pagination for data
    PositionTrainingService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var positionPlanSetup = new position_training_module_1.PositionTrainingModule(jsonObj.id, jsonObj.positionId, jsonObj.positionTrainingDesc, jsonObj.positionTrainingArbicDesc, jsonObj.traningCourseId, jsonObj.traningClassId, jsonObj.courseDesc, jsonObj.classDesc);
                    pagedData.data.push(positionPlanSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    // error handler
    PositionTrainingService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return PositionTrainingService;
}());
PositionTrainingService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PositionTrainingService);
exports.PositionTrainingService = PositionTrainingService;
//# sourceMappingURL=position-training.service.js.map