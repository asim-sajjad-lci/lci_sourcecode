/**
 * A service class for employeeDirectDeposit
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { EmployeeDirectDeposit } from "../../_models/employee-direct-deposit/employee-direct-deposit";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class EmployeeDirectDepositService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmployeeDirectDepositUrl = Constants.hcmModuleApiBaseUrl + 'employeeDirectDeposit/getAll';

    private createEmployeeDirectDepositUrl = Constants.hcmModuleApiBaseUrl + 'employeeDirectDeposit/create';

    private updateEmployeeDirectDepositUrl = Constants.hcmModuleApiBaseUrl + 'employeeDirectDeposit/update';
    private deleteEmployeeDirectDepositUrl = Constants.hcmModuleApiBaseUrl + 'employeeDirectDeposit/delete';
    private employeeIdcheckUrl = Constants.hcmModuleApiBaseUrl + 'employeeDirectDeposit/employeeIdcheck';
 




    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
    //add new employeeDirectDeposit
    createEmployeeDirectDeposit(employeeDirectDeposit: EmployeeDirectDeposit) {
        return this.http.post(this.createEmployeeDirectDepositUrl, JSON.stringify(employeeDirectDeposit), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit employeeDirectDeposit
    updateEmployeeDirectDeposit(employeeDirectDeposit: EmployeeDirectDeposit) {
        return this.http.post(this.updateEmployeeDirectDepositUrl, JSON.stringify(employeeDirectDeposit), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update for edit employeeDirectDeposit
    employeeIdcheck(id) {
        return this.http.post(this.employeeIdcheckUrl, {"id" :id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete employeeDirectDeposit
    deleteEmployeeDirectDeposit(ids: any) {
        return this.http.put(this.deleteEmployeeDirectDepositUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeDirectDeposit>> {
        return this.http.post(this.getAllEmployeeDirectDepositUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

  

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeDirectDeposit> {
        let pagedData = new PagedData<EmployeeDirectDeposit>();
        if (data) {
            var gridRecords = data.records;

            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    // let employeeDirectDeposit = new EmployeeDirectDeposit(
                    //     jsonObj.id,
                    //     jsonObj.employeeDirectDepositId,
                    //     jsonObj.employeeDirectDepositDescription,
                    //     jsonObj.arabicEmployeeDirectDepositDescription,
                    //     jsonObj.employeeDirectDepositAddress,
                    //     jsonObj.phoneNumber,
                    //     jsonObj.fax,
                    //     jsonObj.email,
                    //     jsonObj.cityName,
                    //     jsonObj.countryName,
                    //     jsonObj.stateName,
                    //     jsonObj.cityId,
                    //     jsonObj.countryId,
                    //     jsonObj.stateId,
                    // );
                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}