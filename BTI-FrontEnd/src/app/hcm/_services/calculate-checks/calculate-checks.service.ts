/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions,ResponseOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { CalculateChecksModule } from "../../_models/calculate-checks/calculate-checks";
import { Constants } from '../../../_sharedresource/Constants';
import { CompanyService } from '../../../userModule/_services/companymanagement/company.service';
declare var $: any;


@Injectable()
export class CalculateChecksService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllCalculateChecksUrl = Constants.hcmModuleApiBaseUrl + 'transactionEntry/getAllForCalculateCheck';
    private searchDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/searchDepartment';
    private createDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/create';
    private getDepartmentByDepartmentIdUrl = Constants.hcmModuleApiBaseUrl + 'department/getDepartmentDetailByDepartmentId';
    private updateDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/update';
    private deleteDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/delete';
    private checkDepartmentIdx = Constants.hcmModuleApiBaseUrl + 'department/departmentIdcheck';
    private getDepatmentId = Constants.hcmModuleApiBaseUrl + 'department/getAllDepartmentDropDown';
    private getAllBatchDropDownUrl = Constants.hcmModuleApiBaseUrl + 'batches/getAllBatcheDropDown';
    private getDefaultIdUrl = Constants.hcmModuleApiBaseUrl + 'default/searchDefaultId';
    private payCodeIdUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPayCodeId';
    private deductionCodeIdUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/searchdiductionId';
    private benefitCodeIdUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/searchAllBenefitId';
    private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeId';
    private createCalculateChecksUrl = Constants.hcmModuleApiBaseUrl + 'calculateChecks/create';
    private deleteTransactionEntryUrl = Constants.hcmModuleApiBaseUrl + 'transactionEntry/deleteBatchRow';
    private deleteTransactionEntryBatchUrl = Constants.hcmModuleApiBaseUrl + 'transactionEntry/deleteBatch';
    private getAllDefaultIdUrl = Constants.hcmModuleApiBaseUrl + 'calculateChecks/getAllByDefaultId';
    private distributionUrl = Constants.hcmModuleApiBaseUrl + 'calculateChecks/getDistribrution';
    // private processUrl = Constants.hcmModuleApiBaseUrl + 'calculateChecks/process';
    private processUrl = Constants.hcmModuleApiDirectBaseUrl + 'calculateChecks/process';
    private userDetailsByUserIdUrl = Constants.userModuleApiBaseUrl + 'user/getUserDetailByUserId';
    private getAllDisctrubutionUrl = Constants.hcmModuleApiBaseUrl + 'default/getAllForCalculateCheck';
    private getAllsearchDefaultIdForCalculateChecksUrl = Constants.hcmModuleApiBaseUrl + 'default/searchDefaultIdForCalculateChecks';
    private getAllProcessedDefaultDataUrl = Constants.hcmModuleApiBaseUrl + 'calculateChecks/getAllByDefaultIdForProcess';
    private generateCSVFileUrl = Constants.hcmModuleApiBaseUrl + 'calculateChecks/generateCSVFile';
    private generateCSVFileUrlReport = Constants.hcmModuleApiBaseUrl + 'calculateChecks/generateCSVFileBeforePosting';
    private generateAndEmailPayslipUrl = Constants.hcmModuleApiDirectBaseUrl + 'calculateChecks/generateAndEmailPayslip';
    private getAllDefaultIdUrlWithoutPaging = Constants.hcmModuleApiBaseUrl + 'calculateChecks/getAllByDefaultIdWithoutPagination';
    private fileName;
    //initializing parameter for constructor 
    constructor(private http: Http, private companyService:CompanyService) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));

        if(localStorage.getItem('tenantid') != ''){
            this.companyService.getOneCompanyByTenatedId(localStorage.getItem('tenantid')).subscribe(data =>{
              
              this.companyService.getCurrentCompanyName().subscribe(companyName =>{
                this.fileName = companyName;
              });
            });
            
          }
}

    //add new department
    createCalculateCheck(calculatechecks: CalculateChecksModule) {
        return this.http.post(this.createCalculateChecksUrl, JSON.stringify(calculatechecks), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    process(ids,postedDate,id,type) {
        return this.http.post(this.processUrl, {ids:ids,id:id,type:type,
            postingDate:postedDate,
            "pageNumber" : 0, 
            "pageSize" :5,
            "sortBy":"DESC",
            "sortOn":"id"}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    distribution(ids,id) {
        return this.http.post(this.distributionUrl, {ids:ids,
            "pageNumber" : 0, 
            "pageSize" :5,
            "sortBy":"DESC",
            "sortOn":"id","id":id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateDepartment(department: CalculateChecksModule) {
        return this.http.post(this.updateDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteTransactionEntryByrow(ids: any) {
        return this.http.put(this.deleteTransactionEntryUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    deleteTransactionEntryBatch(id: any) {
        return this.http.put(this.deleteTransactionEntryBatchUrl, { 'id': id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getDepartments(){
		 return this.http.get(this.getDepatmentId, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
    }
    
    // GetAll Distributions
    getAllDistributions(page:Page, searchKeyword){
        return this.http.post(this.getAllDisctrubutionUrl,
            {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy

            }, { headers: this.headers })
           .toPromise()
           .then(res => res.json())
           .catch(this.handleError);
   
   }

//    getAllsearchDefaultIdForCalculateChecksUrl
   getAllUnprocessedData(page:Page){
   return this.http.post(this.getAllsearchDefaultIdForCalculateChecksUrl,
        {
        'searchKeyword': '',
        'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy

        }, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

}

    //check for duplicate ID department
    checkDuplicateDeptId(departmentId: any) {
        return this.http.post(this.checkDepartmentIdx, { 'departmentId': departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getDefaultID(id:any,flag,page: Page) {
        return this.http.post(this.getAllDefaultIdUrl, {
            'id': id,
            'all':flag,
            'sortOn':page.sortOn,
            'sortBy':page.sortBy,
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    getDefaultIDwithoutPaging(id) {
        return this.http.post(this.getAllDefaultIdUrlWithoutPaging, {
            'id': id,
            'all':true,
            'sortOn':'',
            'sortBy':'',
            'searchKeyword': '',
        }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
    // GET Processed Data
    getAllProcessedDefaultID(id,flag, page:Page) {
        console.log("Page for 2nd grid", page);
        return this.http.post(this.getAllProcessedDefaultDataUrl, { 
            'id': id,
            'all':flag,
            'sortOn':page.sortOn,
            'sortBy':page.sortBy,
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getDepartment(departmentId: string) {
        return this.http.post(this.getDepartmentByDepartmentIdUrl, { departmentId: departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword) {
        return this.http.post(this.getAllCalculateChecksUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //get list by search keyword
    searchDepartmentlist(page: Page, searchKeyword): Observable<PagedData<CalculateChecksModule>> {
        return this.http.post(this.searchDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    getAllBatch(searchKeyword){
        return this.http.post(this.getAllBatchDropDownUrl, {
            'searchKeyword': searchKeyword,
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    getDefaultId(){
        return this.http.post(this.getDefaultIdUrl, {
            'searchKeyword': '',
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    getPayCodeId(page: Page){
        return this.http.post(this.payCodeIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }


    getDeductionCodeId(page: Page){
        return this.http.post(this.deductionCodeIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }


    getBenefitCodeId(page: Page){
        return this.http.post(this.benefitCodeIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    getEmployeeId(page: Page){
        return this.http.post(this.employeeIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    // UserDetails By Userid
    getUserDetailsByUserId(id: any){
        return this.http.post(this.userDetailsByUserIdUrl, {"userId":id}, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<CalculateChecksModule> {
        let pagedData = new PagedData<CalculateChecksModule>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let department = new CalculateChecksModule(
                        jsonObj.list,
                        
                        
                    );
                    pagedData.data.push(department);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    // getAllProcessedDefaultID(id,flag, page:Page) {
    //     console.log("Page for 2nd grid", page);
    //     return this.http.post(this.getAllProcessedDefaultDataUrl, { 
    //         'id': id,
    //         'all':flag,
    //         'sortOn':page.sortOn,
    //         'sortBy':page.sortBy,
    //         'searchKeyword': '',
    //         'pageNumber': page.pageNumber,
    //         'pageSize': page.size
    //     }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }


    generateCSVFile(id, postingDate) {
        // alert ('requested');
        // this.headers.append('Accept','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        // this.headers.append('id', id);
        // alert ("postingDate: " + postingDate);
        if (postingDate == null || postingDate == "") {
            alert('Please select Posting Date first!');
            return;
        }

        const options = {headers: this.headers, params: {'id' : id, 'postingDate' : postingDate}};
        return this.http.get(this.generateCSVFileUrl, options).subscribe(data => {
            console.log(data.text());
            this.SaveFile(this.fileName + '_Payroll.csv','text/csv;charset=UTF-8', data.text());
            // alert ('response saved');
        },error => {
            console.log(error);
        });
        /* .toPromise()
        .then(res =>{
            alert ('response: ' + res);
            this.SaveFile('BAAN Payroll.csv','text/csv', res)
            alert ('response saved');
        } ) */
        //.catch(this.handleError);
    }

    generateAndEmailPayslip(id) {
        const options = {'id' : id};
        return this.http.post(this.generateAndEmailPayslipUrl, options, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    SaveFile(name, type, data) {
        data = "\ufeff" + data;
        if (data != null && navigator.msSaveBlob)
            return navigator.msSaveBlob(new Blob([data], { type: type }), name);

        var a = $("<a style='display: none;'/>");
        var url = window.URL.createObjectURL(new Blob([data], { type: type }));
        a.attr("href", url);
        a.attr("download", name);
        $("body").append(a);
        a[0].click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }

    generateCSVFileAli(defaultId: string, languageId) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        var params = {
            'languageId':1
          }
        return Observable.create(observer => {
            
            let xhr = new XMLHttpRequest();
            //xhr.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
            xhr.open('GET',this.generateCSVFileUrlReport, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.setRequestHeader('Accept','text/csv'), 
            xhr.setRequestHeader('session',userData.session), 
            xhr.setRequestHeader('userid', userData.userId), 
            xhr.setRequestHeader('tenantid', localStorage.getItem('tenantid')), 
            xhr.setRequestHeader('langid',languageId), 
            xhr.setRequestHeader('defaultId',defaultId), 
            xhr.responseType='blob';
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(params));
        });
       


       /*  // alert ('requested');
        // this.headers.append('Accept','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        // this.headers.append('id', id);
        const options = {headers: this.headers};
        return this.http.get(this.generateCSVFileUrlReport, options).subscribe(data => {
            console.log(data);
            alert ('response: ' + data);
            this.SaveFile('BAAN Payroll.csv','text/csv', data.text());
            alert ('response saved');
        },error => {
            console.log(error);
        });
        /* .toPromise()
        .then(res =>{
            alert ('response: ' + res);
            this.SaveFile('BAAN Payroll.csv','text/csv', res)
            alert ('response saved');
        } ) */
        //.catch(this.handleError); */
    }

}