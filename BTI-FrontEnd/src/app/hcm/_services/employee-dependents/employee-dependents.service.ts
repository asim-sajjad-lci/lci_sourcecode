import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { EmployeeDependents } from '../../_models/employee-dependents/employee-dependents.module';

@Injectable()
export class EmployeeDependentsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmployeeDependentUrl = Constants.hcmModuleApiBaseUrl + 'employeeDependent/getAll';
    private searchSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/searchSkillSteup';
    private createEmployeeDependentUrl = Constants.hcmModuleApiBaseUrl + 'employeeDependent/create';
    //private getSkillSteupById = Constants.hcmModuleApiBaseUrl + 'skillSetDescDetail/getById';
    private updateEmployeeDependentUrl = Constants.hcmModuleApiBaseUrl + 'employeeDependent/update';
    private deleteEmployeeDependentUrl = Constants.hcmModuleApiBaseUrl + 'employeeDependent/delete';
    private getSkillsSetupIdcheck = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/skillSteupIdcheck';
    //private getSkillSetListUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getAll';
    //private getSkillSetupByIdUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getSkillSteupById';
   // private getSetIdUrl =  Constants.hcmModuleApiBaseUrl + 'skillSetSteup/searchSkillSetSetupId';
    private getRelationDropdownUrl = Constants.hcmModuleApiBaseUrl + 'employeeDependents/getAllemployeeDependentsDropDownList';
    private getEmployeeIdListUrl =  Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
    private getEmployeeIdDependentListUrl =  Constants.hcmModuleApiBaseUrl + 'employeeDependent/getAllByEmployeeId';
    
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new skillsSetup
    createEmployeeDependent(skillsSetup: EmployeeDependents) {
        return this.http.post(this.createEmployeeDependentUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateEmployeeDependent(skillsSetup: EmployeeDependents) {
        return this.http.post(this.updateEmployeeDependentUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    // getSetId() {
    //     return this.http.post(this.getSetIdUrl, {"searchKeyword":""}, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    getRelationDropdown() {
        return this.http.get(this.getRelationDropdownUrl,  { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getEmployeeDependent(page: Page,id: any) {
        return this.http.post(this.getEmployeeIdDependentListUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            id:id
        } ,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //delete department
    deleteEmployeeDependent(ids: any) {
        return this.http.put(this.deleteEmployeeDependentUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateSkillsSetupId(skillSetId: any) {
        return this.http.post(this.getSkillsSetupIdcheck, { 'skillSetId': skillSetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    // getSkillsSetup(skillId: string) {
    //     return this.http.post(this.getSkillSteupById, { skillId: skillId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

     // getting Skill set list
     getEmployeeIdList() {
        return this.http.get(this.getEmployeeIdListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	// getSkillSetupById(skillSetIndexId){
	// 	return this.http.post(this.getSkillSetupByIdUrl,{ id:skillSetIndexId}, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
	// }


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeDependents>> {
        return this.http.post(this.getAllEmployeeDependentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    // searchSkillsSetuplist(page: Page, searchKeyword): Observable<PagedData<EmployeeDependents>> {
    //     return this.http.post(this.searchSkillsSetupUrl, {
    //         'searchKeyword': searchKeyword,
    //         'pageNumber': page.pageNumber,
    //         'pageSize': page.size
    //     }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    // }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeDependents> {
        let pagedData = new PagedData<EmployeeDependents>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    
                    // let jsonObj = gridRecords[i];
                    // let skillsSetup = new EmployeeDependents(
                       
                    //     jsonObj.listEmployeeDependent,
                       
                    // );
                    pagedData.data.push(gridRecords[i]);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
