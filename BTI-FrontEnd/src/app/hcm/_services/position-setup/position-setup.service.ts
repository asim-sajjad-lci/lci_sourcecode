import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {PositionSetupModule} from '../../_models/position-setup/position-setup.module';
import {Page} from '../../../_sharedresource/page';
import {Observable} from 'rxjs/Rx';
import {PagedData} from '../../../_sharedresource/paged-data';
import {PositionClassModule} from '../../_models/position-class/position-class.module';

@Injectable()
export class PositionSetupService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private positionURL = 'positionSetup';
    private getAllPositionSetupUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
    private searchPositionSetupUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPosition';
    private createPositionSetupUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPosition';
    private getPositionSetupByPositionSetupIdUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionId';
    private updatePositionSetupUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
    private deletePositionSetupUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
    private checkPositionSetupIdx = Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionIdcheck';
    //private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAll';
    private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAllPostionClassId';
    private getPositionIdUrl = Constants.hcmModuleApiBaseUrl + 'positionSetup/searchAllPostionIds' 
   // private getSkillSetListUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/searchSkillSetId';
    private getSkillSetListUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/searchSkillSetSetupId';
    private getAllPostionDropDownListUrl = Constants.hcmModuleApiBaseUrl + 'positionSetup/getAllPostionDropDownList';

    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // get position setup detail by Id
    getPositionSetup(positionSetupId: string) {
        return this.http.post(this.getPositionSetupByPositionSetupIdUrl, { id: positionSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // check for duplicate ID position setup
    checkDuplicatePositionSetupId(positionSetupId: any) {
        return this.http.post(this.checkPositionSetupIdx, { 'positionId': positionSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // add new position setup
    createPositionSetup(positionSetup: PositionSetupModule) {
        return this.http.post(this.createPositionSetupUrl, JSON.stringify(positionSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit position setup
    updatePositionSetup(positionSetup: PositionSetupModule) {
        return this.http.post(this.updatePositionSetupUrl, JSON.stringify(positionSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete position setup
    deletePositionSetup(ids: any) {
        return this.http.put(this.deletePositionSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    searchPositionSetuplist(page: Page, searchKeyword): Observable<PagedData<PositionSetupModule>> {
        return this.http.post(this.searchPositionSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PositionSetupModule>> {
        return this.http.post(this.getAllPositionSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<PositionSetupModule> {
        let pagedData = new PagedData<PositionSetupModule>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let positionSetup = new PositionSetupModule(
                        jsonObj.id,
                        jsonObj.positionId,
                        jsonObj.description,
                        jsonObj.arabicDescription,
                        jsonObj.reportToPostion,
                        jsonObj.positionClassId,
                        jsonObj.skillSetId,
                        jsonObj.postionLongDesc,
                        jsonObj.positionClassPrimaryId,
                        jsonObj.skillsetPrimaryId
                    );
                    pagedData.data.push(positionSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // getting Position Class list
    getPositionClassList() {
        return this.http.get(this.getPositionClassListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    /*// get list
    getPositionClassList(page: Page, searchKeyword): Observable<PagedData<PositionSetupModule>> {
        return this.http.put(this.getPositionClassListUrl, {
            'searchKeyword': searchKeyword
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }*/

    // getting Skill set list
    getSkillSetList() {
        return this.http.post(this.getSkillSetListUrl,{"searchKeyword":""}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getPositionIdList() {
        return this.http.post(this.getPositionIdUrl,{"searchKeyword":""}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getAllPostionDropDownList() {
        return this.http.get(this.getAllPostionDropDownListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
