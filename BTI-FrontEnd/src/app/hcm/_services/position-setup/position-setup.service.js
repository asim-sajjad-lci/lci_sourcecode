"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var position_setup_module_1 = require("../../_models/position-setup/position-setup.module");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var PositionSetupService = (function () {
    function PositionSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.positionURL = 'positionSetup';
        this.getAllPositionSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
        this.searchPositionSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPosition';
        this.createPositionSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPosition';
        this.getPositionSetupByPositionSetupIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionSetupDetailByPositionSetupId';
        this.updatePositionSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
        this.deletePositionSetupUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
        this.checkPositionSetupIdx = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionIdcheck';
        this.getPositionClassListUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/getAll';
        this.getSkillSetListUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getSkillSetList';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }
    // get position setup detail by Id
    PositionSetupService.prototype.getPositionSetup = function (positionSetupId) {
        return this.http.post(this.getPositionSetupByPositionSetupIdUrl, { positionSetupId: positionSetupId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // check for duplicate ID position setup
    PositionSetupService.prototype.checkDuplicatePositionSetupId = function (positionSetupId) {
        return this.http.post(this.checkPositionSetupIdx, { 'positionId': positionSetupId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // add new position setup
    PositionSetupService.prototype.createPositionSetup = function (positionSetup) {
        return this.http.post(this.createPositionSetupUrl, JSON.stringify(positionSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // update for edit position setup
    PositionSetupService.prototype.updatePositionSetup = function (positionSetup) {
        return this.http.post(this.updatePositionSetupUrl, JSON.stringify(positionSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // delete position setup
    PositionSetupService.prototype.deletePositionSetup = function (ids) {
        return this.http.put(this.deletePositionSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // get list by search keyword
    PositionSetupService.prototype.searchPositionSetuplist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPositionSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // get list
    PositionSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllPositionSetupUrl, {
            // 'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // pagination for data
    PositionSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var positionSetup = new position_setup_module_1.PositionSetupModule(jsonObj.id, jsonObj.positionId, jsonObj.description, jsonObj.arabicDescription, jsonObj.reportToPostion, jsonObj.positionClassId, jsonObj.skillSetId, jsonObj.postionLongDesc);
                    pagedData.data.push(positionSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    // getting Position Class list
    PositionSetupService.prototype.getPositionClassList = function () {
        return this.http.get(this.getPositionClassListUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    /*// get list
    getPositionClassList(page: Page, searchKeyword): Observable<PagedData<PositionSetupModule>> {
        return this.http.put(this.getPositionClassListUrl, {
            'searchKeyword': searchKeyword
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }*/
    // getting Skill set list
    PositionSetupService.prototype.getSkillSetList = function () {
        return this.http.get(this.getSkillSetListUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // error handler
    PositionSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return PositionSetupService;
}());
PositionSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PositionSetupService);
exports.PositionSetupService = PositionSetupService;
//# sourceMappingURL=position-setup.service.js.map