/**
 * A service class for interviewTypeSeup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { InterviewType } from "../../_models/interview-type-setup/InterviewType";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class InterviewTypeSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllInterviewTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'interviewTypeSetup/getAll';
    private searchInterviewTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'interviewTypeSetup/searchinterviewTypeSeup';
    private createInterviewTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'interviewTypeSetup/create';
    private updateInterviewTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'interviewTypeSetup/update';
    private deleteInterviewTypeSetupUrl = Constants.hcmModuleApiBaseUrl + 'interviewTypeSetup/delete';
    private checkInterviewTypeSetupUrl = Constants.hcmModuleApiBaseUrl +  'interviewTypeSetup/interviewTypeSetupIdcheck';
    private deleteInterviewTypeSetupDetailUrl=Constants.hcmModuleApiBaseUrl + 'interviewTypeSetupDetail/delete';
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new interviewTypeSeup
    createInterviewTypeSetup(interviewTypeSeup: InterviewType) {
        return this.http.post(this.createInterviewTypeSetupUrl, JSON.stringify(interviewTypeSeup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    checkDuplicateInterviewSetupId(interviewTypeId: any) {
        return this.http.post(this.checkInterviewTypeSetupUrl, { 'interviewTypeId': interviewTypeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit interviewTypeSeup
    updateInterviewTypeSetup(interviewTypeSeup: InterviewType) {
        return this.http.post(this.updateInterviewTypeSetupUrl, JSON.stringify(interviewTypeSeup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete interviewTypeSeup
    deleteInterviewTypeSetup(ids: any) {
        return this.http.put(this.deleteInterviewTypeSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete interviewTypeSetupDetail
    deleteInterviewTypeSetupDetail(ids: any) {
        return this.http.put(this.deleteInterviewTypeSetupDetailUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   
    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<InterviewType>> {
        return this.http.post(this.getAllInterviewTypeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchInterviewTypeSetuplist(page: Page, searchKeyword): Observable<PagedData<InterviewType>> {
        return this.http.post(this.searchInterviewTypeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<InterviewType> {
        let pagedData = new PagedData<InterviewType>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let interviewTypeSetup = new InterviewType(
                        jsonObj.id,
                        jsonObj.interviewTypeId,
                        jsonObj.interviewTypeDescription,
                        jsonObj.interviewTypeDescriptionArabic,
                        jsonObj.interviewRange,
                        jsonObj.dtoInterviewTypeSetupDetail,
                        
                    );
                    pagedData.data.push(interviewTypeSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}