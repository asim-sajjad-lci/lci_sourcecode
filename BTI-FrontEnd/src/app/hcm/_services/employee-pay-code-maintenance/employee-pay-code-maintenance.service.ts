import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { EmployeePayCodeMaintenanceModule } from '../../_models/employee-pay-code-maintenance/employee-pay-code-maintenance.module';

@Injectable()
export class EmployeePayCodeMaintenanceService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllEmpPayCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/getAll';
  private getcheckDuplicatePaycodeMaintenance = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/CheckDuplicateByEmployeeIdAndPayCodeId';
  private createEmpPayCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/create';
  private employeecodeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
  private paycodeIdUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPayCodeDropDown';
  private paycodeTypeIdUrl = Constants.hcmModuleApiBaseUrl + 'payCodeType/getAllPayCodeTypeDropDown';
  private basedOnPayCodeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/getAllEmployeePaycodeMainteneceDropDownList';
  private getEmpPayCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/getById';
  private updateEmpPayCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/update';
  private deleteEmpPayCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/delete';
  private getBasedOnPayCodeRecordsUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/getAllEmployeePaycodeMainteneceDropDownList';
  private getEmpPayCodeRefreshedUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/findBasedOnCodeByEmployeeIds';
  private typeIdDetails = Constants.hcmModuleApiBaseUrl + 'typeFieldForCodes/getAllIds';
  //initializing parameter for constructor
  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
    localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
  }

  //Create new Emp Pay Code
  createEmpPayCode(payCodeSetup: EmployeePayCodeMaintenanceModule) {
    console.log(JSON.stringify(payCodeSetup));
    return this.http.post(this.createEmpPayCodeUrl, JSON.stringify(payCodeSetup), { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  // update Employee Pay COde
  updateEmpPaycodeMntnce(paycode: EmployeePayCodeMaintenanceModule) {
    return this.http.post(this.updateEmpPayCodeByIdUrl, JSON.stringify(paycode), { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  //delete Emp Pay code Mntnce
  deleteEmpPaycode(ids: any) {
    return this.http.put(this.deleteEmpPayCodeByIdUrl, { 'ids': ids }, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }


  // get Employee detail by Id
  getEmployeeCodeId() {
    return this.http.get(this.employeecodeIdUrl, {headers: this.headers})
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  // Pay Code Id
  getPaycodeId(){
    return this.http.get(this.paycodeIdUrl, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  // Pay Code Type Id
  getPaycodeTypeId(){
    return this.http.get(this.paycodeTypeIdUrl, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  //Based On Pay Code Id
  getBasedOnPayCodeId(){
    return this.http.get(this.basedOnPayCodeIdUrl, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
  }

  getBasedOnPayCodeRecords(){
        return this.http.get(this.getBasedOnPayCodeRecordsUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
  }

  // Get Data by Id
  getEmpPayCodeById(id = 0) {
		return this.http.post(this.getEmpPayCodeByIdUrl, {"id":id}, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
       
  }

  // Get Data by Id
  getEmpPayCodeRefreshed(payload:any) {
		return this.http.post(this.getEmpPayCodeRefreshedUrl, payload, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
       
  }

  //get list 
  getlist(page: Page, searchKeyword): Observable<PagedData<EmployeePayCodeMaintenanceModule>> {
    return this.http.post(this.getAllEmpPayCodeUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': "",
      'sortBy': ""
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

   //pagination for data
   private getPagedData(page: Page, data: any): PagedData<EmployeePayCodeMaintenanceModule> {
    let pagedData = new PagedData<EmployeePayCodeMaintenanceModule>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let empPayCodeMaintnce = new EmployeePayCodeMaintenanceModule(
            jsonObj.id,
            jsonObj.employeeMaster,
            jsonObj.payCode,
            jsonObj.payCodeType,
            jsonObj.baseOnPayCode,
            jsonObj.amount,
            jsonObj.payFactory,
            jsonObj.inactive,
            jsonObj.payPeriod,                        
            jsonObj.unitOfPay,
            jsonObj.payRate,
            jsonObj.baseOnPayCodeId,
            jsonObj.payTypeId,
            jsonObj.roundOf,
          );
          pagedData.data.push(empPayCodeMaintnce);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }
  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  checkDuplicateMiscellaneousBenefitsId(payload: any) {
        return this.http.post(this.getcheckDuplicatePaycodeMaintenance, payload, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //get type field detail by Id
     getTypeFieldDetails(typeid) {
      return this.http.post(this.typeIdDetails, { id: typeid }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

}
