import { TestBed, inject } from '@angular/core/testing';

import { EmployeeEducationService } from './employee-education.service';

describe('EmployeeEducationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeEducationService]
    });
  });

  it('should be created', inject([EmployeeEducationService], (service: EmployeeEducationService) => {
    expect(service).toBeTruthy();
  }));
});
