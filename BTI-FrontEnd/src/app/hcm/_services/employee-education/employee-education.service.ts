import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { EmployeeEducationModule } from '../../_models/employee-education/employee-education.module';

@Injectable()
export class EmployeeEducationService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllEmployeeEducationUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/getAll';
  private searchEmployeeEducationUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/searchEmployeeEducationId';
  private createEmployeeEducationUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/create';
  private getEmployeeEducationByEmployeeEducationIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/getById';
  private updateEmployeeEducationUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/update';
  private deleteEmployeeEducationUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/delete';
  private checkEmployeeEducationIdx = Constants.hcmModuleApiBaseUrl + 'employeeEducation/employeeEducationIdcheck';
  private searchEmployeeEducationIDUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/searchEmployeeEducationId';
  private getEmployeeEducationByEmployeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeEducation/getByEmployeeId';
  
  //initializing parameter for constructor
  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
    console.log('Header: ', this.headers)
  }

  // Employee Education Search Id

  getEmployeeEducationIdList() {
    return this.http.post(this.searchEmployeeEducationIDUrl, { 'searchKeyword': '' }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //add new EmployeeEducation
  createEmployeeEducation(employeeEducation: Array<EmployeeEducationModule>) {
    return this.http.post(this.createEmployeeEducationUrl, JSON.stringify({ 'listEmployeeEducation' : employeeEducation}), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //update for edit Employee Education
  updateEmployeeEducation(employeeEducation: Array<EmployeeEducationModule>) {
    return this.http.post(this.updateEmployeeEducationUrl, JSON.stringify({ 'listEmployeeEducation' : employeeEducation}), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //delete Employee Education
  deleteEmployeeEducation(ids: any) {
    return this.http.put(this.deleteEmployeeEducationUrl, { 'ids': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //check for duplicate ID Employee Education
  checkDuplicateEmployeeEducationId(employeeEducationId: any) {
    return this.http.post(this.checkEmployeeEducationIdx, { 'employeeEducationId': employeeEducationId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get Employee Education detail by Id
  getEmployeeEducation(employeeEducationId: string) {
    return this.http.post(this.getEmployeeEducationByEmployeeEducationIdUrl, { 'id': employeeEducationId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get Employee Education detail by Id
  getlistByID(id: string) {
    return this.http.post(this.getEmployeeEducationByEmployeeEducationIdUrl, { 'id': id }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get Employee Education detail by Employee Id
  getlistByEmployeeID(id: number) {
    return this.http.post(this.getEmployeeEducationByEmployeeIdUrl, { 'id': id }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get list
  getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeEducationModule>> {
    return this.http.post(this.getAllEmployeeEducationUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //get list by search keyword
  searchEmployeeEducationlist(page: Page, searchKeyword): Observable<PagedData<EmployeeEducationModule>> {
    return this.http.post(this.searchEmployeeEducationUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<EmployeeEducationModule> {
    let pagedData = new PagedData<EmployeeEducationModule>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let employeeEducationModule = new EmployeeEducationModule(
            jsonObj.id,
            jsonObj.employeeMaster,
            jsonObj.schoolName,
            jsonObj.major,
            jsonObj.endYear,
            jsonObj.startYear,
            jsonObj.degree,
            jsonObj.gpa,
            jsonObj.comments,
            jsonObj.listEmployeeEducation);
          pagedData.data.push(employeeEducationModule);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }

  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
