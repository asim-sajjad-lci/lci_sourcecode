import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { skillSetSetup } from '../../_models/skill-set-setup/skill-set-setup.module';

@Injectable()
export class SkillSetSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/getAll';
    private searchSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/searchSkillSteup';
    private createSkillSteupUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/create';
    private getSkillSteupById = Constants.hcmModuleApiBaseUrl + 'skillSetDescDetail/getById';
    private updateSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/update';
    private deleteSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/delete';
    private getSkillsSetupIdcheck = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/skillSteupIdcheck';
    private getSkillSetListUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getAll';
    private getSkillSetupByIdUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getSkillSteupById';
    private getSetIdUrl =  Constants.hcmModuleApiBaseUrl + 'skillSetSteup/searchSkillSetSetupId';
    
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new skillsSetup
    createSkillSteup(skillsSetup: skillSetSetup) {
        return this.http.post(this.createSkillSteupUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateSkillsSetup(skillsSetup: skillSetSetup) {
        return this.http.post(this.updateSkillsSetupUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getSetId() {
        return this.http.post(this.getSetIdUrl, {"searchKeyword":""}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteSkillsSetup(ids: any) {
        return this.http.put(this.deleteSkillsSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateSkillsSetupId(skillSetId: any) {
        return this.http.post(this.getSkillsSetupIdcheck, { 'skillSetId': skillSetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getSkillsSetup(skillId: string) {
        return this.http.post(this.getSkillSteupById, { id: skillId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     // getting Skill set list
     getSkillSetList() {
        return this.http.post(this.getSkillSetListUrl,{ "searchKeyword" : "", "sortOn" : "skillId",
        "sortBy" : "DESC",
       "pageNumber":"0",
       "pageSize":"0"}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getSkillSetupById(skillSetIndexId){
		return this.http.post(this.getSkillSetupByIdUrl,{ id:skillSetIndexId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<skillSetSetup>> {
        return this.http.post(this.getAllSkillsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchSkillsSetuplist(page: Page, searchKeyword): Observable<PagedData<skillSetSetup>> {
        return this.http.post(this.searchSkillsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<skillSetSetup> {
        let pagedData = new PagedData<skillSetSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let skillsSetup = new skillSetSetup(
                        jsonObj.id,
                        jsonObj.skillId,
                        jsonObj.skillSetId,
                        jsonObj.skillSetDiscription,
                        jsonObj.arabicDiscription,
                        jsonObj.skillSetSeqn,
                        jsonObj.skillRequired,
                        jsonObj.comment,
                        jsonObj.skillSetDescId,
                        jsonObj.skillSetIndexId,
                        jsonObj.skillIdParam,
                        jsonObj.listSkillSteup,
                        jsonObj.skillsIds
                    );
                    pagedData.data.push(skillsSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
