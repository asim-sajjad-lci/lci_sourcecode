/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BuildPayrollCheckBatch } from "../../_models/build-payroll-check-batches/build-payroll-check-batches";
import { Constants } from '../../../_sharedresource/Constants';
import { identifierModuleUrl } from '@angular/compiler';


@Injectable()
export class BuildPayrollCheckBatchService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private createPayrollCheckBatchesUrl = Constants.hcmModuleApiBaseUrl + 'buildPayrollCheckByBatches/create';
    private updateBatchUrl = Constants.hcmModuleApiBaseUrl + 'batches/update';
    private getAllBatchesUrl = Constants.hcmModuleApiBaseUrl + 'buildPayrollCheckByBatches/getAll';
    private getAllBatchesByDateUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/getAllForFromDateToDate';
    private deleteBatchUrl = Constants.hcmModuleApiBaseUrl + 'batches/delete';
    private checkBatchIdx = Constants.hcmModuleApiBaseUrl + 'batches/batcheIdcheck';
    private getAllbatchesUrl = Constants.hcmModuleApiBaseUrl + 'buildPayrollCheckByBatches/getAll';
    private getalluserUrl = Constants.userModuleApiBaseUrl +'user/getAllUserDetail';
    private batchMarkUnmarkUrl = Constants.hcmModuleApiBaseUrl +'transactionEntry/buildcheckBatchMarkUnMark';
    private batchNewApiUrl = Constants.hcmModuleApiBaseUrl +'transactionEntry/checkAndUnCheckedFromBatchBuildCheck';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new department
    createPayrollCheckBatches(buildChecks: number, batch: Array<BuildPayrollCheckBatch>) {
        return this.http.post(this.createPayrollCheckBatchesUrl, {
            "listBuildChecks": [{
                "buildChecks": {
                    "id": buildChecks
                }
            }],
            "batches": batch
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateBatch(buildChecks: number, batch: Array<BuildPayrollCheckBatch>) {
        return this.http.post(this.updateBatchUrl, {
            "listBuildChecks": [{
                "buildChecks": {
                    "id": buildChecks
                }
            }],
            "batches": batch
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getUser() {
        return this.http.get(this.getalluserUrl,  { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    batchMarkUnmark(batchId:any) {
        return this.http.post(this.batchMarkUnmarkUrl, {batchId},  { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    batchNewApi(batchId:any) {
        return this.http.post(this.batchNewApiUrl, {batchId},  { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteBatch(ids: any) {
        return this.http.put(this.deleteBatchUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateBatchId(batchId: any) {
        return this.http.post(this.checkBatchIdx, { 'batchId': batchId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    searchBatches(id:any,page: Page, searchKeyword:any): any {
        return this.http.post(this.getAllbatchesUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': 5,
            'sortOn': 'id',
            'sortBy': 'ASC',
            'id':id
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
    getAll(id:any,page: Page, searchKeyword:any) {
            return this.http.post(this.getAllbatchesUrl, {
                'searchKeyword': searchKeyword,
                'pageNumber': page.pageNumber,
                'pageSize': page.size,
                'sortOn': page.sortOn,
                'sortBy': page.sortBy,
                'id':id
            }, { headers: this.headers }).toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        
    }
    //get list
    getlistByDate(id: number,fromDate: string, toDate: string): Observable<Array<BuildPayrollCheckBatch>> {
        return this.http.post(this.getAllBatchesByDateUrl, {
            'id': id,
            'dateFrom':fromDate,
            'dateTo':toDate
            /* 'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy */
        }, { headers: this.headers }).map(data => this.getData(data.json()));
    }

    //pagination for data
    private getData(data: any): Array<BuildPayrollCheckBatch> {
        let res = new Array<BuildPayrollCheckBatch>();
        if (data) {
            var gridRecords = data.result.records;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let batch = new BuildPayrollCheckBatch(
                        jsonObj.batchPrimaryId,
                        jsonObj.batchId,
                        jsonObj.description,
                        jsonObj.userID,
                        jsonObj.status,
                        (jsonObj.buildChecksId != null)
                    );
                    res.push(batch);
                }
            }
        }
        return res;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}