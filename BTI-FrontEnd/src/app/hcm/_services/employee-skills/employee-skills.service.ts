/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { EmployeeSkills } from "../../_models/employee-skills/employee-skills";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class EmployeeSkillsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmpSkillUrl = Constants.hcmModuleApiBaseUrl + 'employeeSkills/getAll';
    private createEmpSkillUrl = Constants.hcmModuleApiBaseUrl + 'employeeSkills/create';
    private checkDuplicateEmpSkillUrl = Constants.hcmModuleApiBaseUrl + 'employeeSkills/employeeIdandSkillIdcheck';
    private updateEmpSkillUrl = Constants.hcmModuleApiBaseUrl + 'employeeSkills/update';
    private deleteEmpSkillUrl = Constants.hcmModuleApiBaseUrl + 'employeeSkills/deleteEmployeeSkill';
    private employeecodeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/employeeMasterDropDownList';
    private skillSetIdUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/getIds';
    private getSkillsBySkillSetIdUrl = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/getById';
    private getSkillsSetByIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeSkills/getById';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new department
    createEmpSkill(department: EmployeeSkills) {
        return this.http.post(this.createEmpSkillUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Check Duplicate
    employeeIdcheck(payload) {
        return this.http.post(this.checkDuplicateEmpSkillUrl, payload, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateEmployeeSkills(employeeSkills: EmployeeSkills) {
        return this.http.post(this.updateEmpSkillUrl, JSON.stringify(employeeSkills), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteEmpSkill(ids: any) {
        return this.http.put(this.deleteEmpSkillUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getEmployee() {
        return this.http.get(this.employeecodeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    getSkillSetIds(searchKeyword: any) {
        return this.http.post(this.skillSetIdUrl, { 'searchkeyword': searchKeyword }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

    //get Emp Skils  detail by Id
    getSkillsById(skillSetId: number) {
        return this.http.post(this.getSkillsSetByIdUrl, { id: skillSetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   
    //get department detail by Id
    getSkillsBySetupId(skillSetId: number) {
        return this.http.post(this.getSkillsBySkillSetIdUrl, { id: skillSetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<any>> {
        return this.http.post(this.getAllEmpSkillUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => data.json());
    }
    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeSkills> {
        let pagedData = new PagedData<EmployeeSkills>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let employeeSkills = new EmployeeSkills(
                        jsonObj.id,
                        jsonObj.skillSetSetup,
                        jsonObj.employeeMaster,
                        jsonObj.listSkills
                    );
                    pagedData.data.push(employeeSkills);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}