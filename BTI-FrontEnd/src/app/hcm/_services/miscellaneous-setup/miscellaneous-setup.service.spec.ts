import { TestBed, inject } from '@angular/core/testing';

import { MiscellaneousSetupService } from './miscellaneous-setup.service';

describe('MiscellaneousSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MiscellaneousSetupService]
    });
  });

  it('should be created', inject([MiscellaneousSetupService], (service: MiscellaneousSetupService) => {
    expect(service).toBeTruthy();
  }));
});
