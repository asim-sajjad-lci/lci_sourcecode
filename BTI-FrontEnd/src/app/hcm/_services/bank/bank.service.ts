import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {BankModel} from '../../_models/bank/bank.model';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class BankService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private bankURL = 'bank';
    private getAllBankDropDownListUrl = Constants.hcmModuleApiBaseUrl + this.bankURL + '/getAllBankDropDownList';

    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    getAllPostionDropDownList() {
        return this.http.get(this.getAllBankDropDownListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}

