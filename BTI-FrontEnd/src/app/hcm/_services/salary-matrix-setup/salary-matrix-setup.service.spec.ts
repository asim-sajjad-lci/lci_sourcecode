import { TestBed, inject } from '@angular/core/testing';

import { SalaryMatrixSetupService } from './salary-matrix-setup.service';

describe('SalaryMatrixSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalaryMatrixSetupService]
    });
  });

  it('should be created', inject([SalaryMatrixSetupService], (service: SalaryMatrixSetupService) => {
    expect(service).toBeTruthy();
  }));
});
