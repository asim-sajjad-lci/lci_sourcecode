import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { SalaryMatrixSetupModule } from '../../_models/salary-matrix-setup/salary-matrix-setup.module';

@Injectable()
export class SalaryMatrixSetupService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllSalaryMatrixSetupUrl = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/getAll';
  private searchSalaryMatrixSetupIdUrl = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/getSalaryMatrixSetupId';
  private earchSalaryMatrixSetupUrl=Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/searchsalaryMatrixSetup';
  private createSalaryMatrixSetupUrl = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/create';
  private getSalaryMatrixSetupBysalaryMatrixSetupIdUrl = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/getSalaryMatrixSetupId';
  private updateSalaryMatrixSetupUrl = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/update';
  private deleteSalaryMatrixSetupUrl = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/delete';
  private checkSalaryMatrixSetupIdx = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/salaryMatrixSetupIdcheck';
  private matrixIdUrl = Constants.hcmModuleApiBaseUrl + 'salaryMatrixSetup/searchSalaryMatrixId';


  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
    console.log('Header: ', this.headers)
  }

  //add new salaryMatrixSetup
  createSalaryMatrixSetup(salMatrixSetup: SalaryMatrixSetupModule) {
    return this.http.post(this.createSalaryMatrixSetupUrl, JSON.stringify(salMatrixSetup), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //update for edit department
  updateSalaryMatrixSetup(salMatrixSetup: SalaryMatrixSetupModule) {
    return this.http.post(this.updateSalaryMatrixSetupUrl, JSON.stringify(salMatrixSetup), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //delete department
  deleteSalaryMatrixSetup(ids: any) {
    return this.http.put(this.deleteSalaryMatrixSetupUrl, { 'ids': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //check for duplicate ID department
  checkDuplicateSalaryMatrixSetupId(salaryMatrixSetupId: any) {
    return this.http.post(this.checkSalaryMatrixSetupIdx, { 'salaryMatrixSetupId': salaryMatrixSetupId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //check for duplicate ID department
  getSalaryMatrixSetupById(salaryMatrixSetupId: any) {
    return this.http.post(this.getSalaryMatrixSetupBysalaryMatrixSetupIdUrl, { id: salaryMatrixSetupId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get list
  getlist(page: Page, searchKeyword): Observable<PagedData<SalaryMatrixSetupModule>> {
    return this.http.post(this.getAllSalaryMatrixSetupUrl, {
        'searchKeyword': searchKeyword,
        'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
}

  //get list by id
  searchSalaryMatrixSetuplist(page: Page, searchKeyword): Observable<PagedData<SalaryMatrixSetupModule>> {
    return this.http.post(this.searchSalaryMatrixSetupIdUrl, {
      'ids': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }


  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<SalaryMatrixSetupModule> {
    let pagedData = new PagedData<SalaryMatrixSetupModule>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let salaryMatrixSetup = new SalaryMatrixSetupModule(
            jsonObj.id,
            jsonObj.salaryMatrixId,
            jsonObj.description,
            jsonObj.arabicSalaryMatrixDescription,
            jsonObj.payUnit,
            jsonObj.totalRow,
            jsonObj.totalRowValues,
            jsonObj.totalColumns,
            jsonObj.listSalaryMatrixRow,
            jsonObj.listSalaryMatrixColSetup
          );
            console.log(salaryMatrixSetup);
          pagedData.data.push(salaryMatrixSetup);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }

  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  getMatrixId(){
        return this.http.post(this.matrixIdUrl,{'searchKeyword' : ''} ,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
  }

}
