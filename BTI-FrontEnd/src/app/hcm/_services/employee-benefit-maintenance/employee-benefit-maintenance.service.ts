import { Injectable } from '@angular/core';

import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { EmployeeBenefitMaintenance } from '../../_models/employee-benefit-maintenance/employee-benefit-maintenance.module';

@Injectable()
export class EmployeeBenefitMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmpBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeBenefitMaintenance/getAllData';
    private createEmpBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeBenefitMaintenance/create';
    private duplicateEmpBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeBenefitMaintenance/validate';
    // private getEmpDeductionCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeBenefitMaintenance/getById';
    private updateEmpBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeBenefitMaintenance/update';
    private deleteEmpBenefitCodeUrl = Constants.hcmModuleApiBaseUrl + 'employeeBenefitMaintenance/delete';
    private employeecodeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
    private benefitcodeIdUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/getAllBenefitDropDown';
    private getEmpBenefitCodeByEmployeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeBenefitMaintenance/getById';
    private getAllBasedOnPayCodeDropDownUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPaycodeforDeduction';
    private getAlldeductionCodefindCodeByEmployeeIdUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/findCodeByEmployeeId';
    private typeIdDetails = Constants.hcmModuleApiBaseUrl + 'typeFieldForCodes/getAllIds';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //Create new Emp Deduction Code
    createEmpBenefitCode(benefitCodeSetup: EmployeeBenefitMaintenance) {
        console.log(JSON.stringify(benefitCodeSetup));
        return this.http.post(this.createEmpBenefitCodeUrl, JSON.stringify(benefitCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    checkDuplicateBenefitId(benefitId: any) {
        return this.http.post(this.duplicateEmpBenefitCodeUrl, { 
            "employeeMasters": {
                "employeeIndexId":benefitId.employeeMaster.employeeIndexId
            },
            "benefitCode": {
                "id":benefitId.benefitCode.id
            }

         }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update Emp Deduction
    updateEmpBenefitCode(benefitCodeSetup: EmployeeBenefitMaintenance) {
        return this.http.post(this.updateEmpBenefitCodeUrl, JSON.stringify(benefitCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete Emp Deduction
    deleteEmpBenefitCode(ids: any) {
        return this.http.put(this.deleteEmpBenefitCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get Employee detail by Id
    getEmployeeCodeId() {
        return this.http.get(this.employeecodeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getBenefitcodeId() {
        return this.http.get(this.benefitcodeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeBenefitMaintenance>> {
        return this.http.post(this.getAllEmpBenefitCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get Employee Education detail by Employee Id
    getlistByEmployeeID(id: number) {
        return this.http.post(this.getEmpBenefitCodeByEmployeeIdUrl, { 'id': id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
  getAlldeductionCodefindCodeByEmployeeId(payload: any) {
    return this.http.post(this.getAlldeductionCodefindCodeByEmployeeIdUrl, payload, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

    //get department detail by Id
    getAllBasedOnPayCodeDropDown(payload: any) {
        return this.http.post(this.getAllBasedOnPayCodeDropDownUrl, payload, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeBenefitMaintenance> {
        let pagedData = new PagedData<EmployeeBenefitMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let empDeductionMaintnce = new EmployeeBenefitMaintenance(
                        jsonObj.id,
                        jsonObj.employeeMaster,
                        jsonObj.benefitCode,
                        jsonObj.startDate,
                        jsonObj.endDate,
                        jsonObj.frequency,
                        jsonObj.benefitMethod,
                        jsonObj.benefitAmount,
                        jsonObj.benefitPercent,
                        jsonObj.transactionRequired,
                        jsonObj.inactive,
                        jsonObj.perPeriord,
                        jsonObj.perYear,
                        jsonObj.lifeTime,
                        jsonObj.dtoPayCode,
                        jsonObj.payFactor,
                        jsonObj.noOfDays,
                        jsonObj.endDateDays,
                        jsonObj.benefitTypeId,
                        jsonObj.roundOf 

                    );
                    pagedData.data.push(empDeductionMaintnce);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

     //get type field detail by Id
     getTypeFieldDetails(typeid) {
        return this.http.post(this.typeIdDetails, { id: typeid }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
