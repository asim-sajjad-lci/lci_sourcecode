"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var position_plan_setup_module_1 = require("../../_models/position-plan-setup/position-plan-setup.module");
var PositionPlanSetupService = (function () {
    function PositionPlanSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.positionURL = 'positionPlanSetup';
        this.getAllPositionPlanUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAllPostionId';
        this.searchPositionPlanUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPositionPlan';
        this.createPositionPlanUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPositionPlanSetup';
        this.getPositionPlanByPositionPlanIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionPlanSetupId';
        this.updatePositionPlanUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
        this.deletePositionPlanUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
        this.checkPositionPlanIdx = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionPlanIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }
    // get position class detail by Id
    PositionPlanSetupService.prototype.getPositionClass = function (positionPlanId) {
        return this.http.post(this.getPositionPlanByPositionPlanIdUrl, { positionPlanId: positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // check for duplicate ID position class
    PositionPlanSetupService.prototype.checkDuplicatePositionPlanSetupId = function (positionPlanId) {
        return this.http.post(this.checkPositionPlanIdx, { 'positionPlanId': positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // add new position class
    PositionPlanSetupService.prototype.createPositionPlanSetup = function (positionPlanSetup) {
        return this.http.post(this.createPositionPlanUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // update for edit position class
    PositionPlanSetupService.prototype.updatePositionClass = function (positionPlanSetup) {
        return this.http.post(this.updatePositionPlanUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // delete position class
    PositionPlanSetupService.prototype.deletePositionClass = function (ids) {
        return this.http.put(this.deletePositionPlanUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // get list by search keyword
    PositionPlanSetupService.prototype.searchPositionClasslist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPositionPlanUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // get list
    PositionPlanSetupService.prototype.getlist = function (page, searchKeyword, positionId) {
        var _this = this;
        return this.http.put(this.getAllPositionPlanUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'positionId': positionId
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // pagination for data
    PositionPlanSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var positionPlanSetup = new position_plan_setup_module_1.PositionPlanSetupModule(jsonObj.id, jsonObj.positionId, jsonObj.positionPlanId, jsonObj.positionPlanDesc, jsonObj.positionPlanArbicDesc, jsonObj.positionPlanStart, jsonObj.positionPlanEnd, jsonObj.inactive);
                    pagedData.data.push(positionPlanSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    // error handler
    PositionPlanSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return PositionPlanSetupService;
}());
PositionPlanSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PositionPlanSetupService);
exports.PositionPlanSetupService = PositionPlanSetupService;
//# sourceMappingURL=position-plan-setup.service.js.map