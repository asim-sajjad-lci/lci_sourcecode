import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {PositionPlanSetupModule} from '../../_models/position-plan-setup/position-plan-setup.module';

@Injectable()
export class PositionPlanSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private positionURL = 'positionPlanSetup';
    private getAllPositionPlanUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
    private searchPositionPlanUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPositionPlan';
    private createPositionPlanUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPositionPlanSetup';
    private getPositionPlanByPositionPlanIdUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionPlanSetupId';
    private updatePositionPlanUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
    private deletePositionPlanUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
    private checkPositionPlanIdx = Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionPlanIdcheck';

  constructor(private http: Http) {
      let userData = JSON.parse(localStorage.getItem('currentUser'));
      this.headers.append('session', userData.session);
      this.headers.append('userid', userData.userId);
      let currentLanguage = localStorage.getItem('currentLanguage') ?
          localStorage.getItem('currentLanguage') : '1';
      this.headers.append('langid', currentLanguage);
      this.headers.append('tenantid', localStorage.getItem('tenantid'));
  }

    // get position class detail by Id
    getPositionClass(positionPlanId: string) {
        return this.http.post(this.getPositionPlanByPositionPlanIdUrl, { positionPlanId: positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // check for duplicate ID position class
    checkDuplicatePositionPlanSetupId(positionPlanId: any) {
        return this.http.post(this.checkPositionPlanIdx, { 'positionPlanId': positionPlanId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // add new position class
    createPositionPlanSetup(positionPlanSetup: PositionPlanSetupModule) {
        return this.http.post(this.createPositionPlanUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit position class
    updatePositionClass(positionPlanSetup: PositionPlanSetupModule) {
        return this.http.post(this.updatePositionPlanUrl, JSON.stringify(positionPlanSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete position class
    deletePositionClass(ids: any) {
        return this.http.put(this.deletePositionPlanUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    searchPositionClasslist(page: Page, searchKeyword): Observable<PagedData<PositionPlanSetupModule>> {
        return this.http.post(this.searchPositionPlanUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get list
    getlist(page: Page, searchKeyword, positionId): Observable<PagedData<PositionPlanSetupModule>> {
        return this.http.post(this.getAllPositionPlanUrl, {
			'id' : positionId,
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'positionId': positionId
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<PositionPlanSetupModule> {
        let pagedData = new PagedData<PositionPlanSetupModule>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let positionPlanSetup = new PositionPlanSetupModule(
                        jsonObj.id,
                        jsonObj.positionId,
                        jsonObj.positionPlanId,
                        jsonObj.positionPlanDesc,
                        jsonObj.positionPlanArbicDesc,
                        jsonObj.positionPlanStart,
                        jsonObj.positionPlanEnd,
                        jsonObj.inactive
                    );
                    pagedData.data.push(positionPlanSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }



}
