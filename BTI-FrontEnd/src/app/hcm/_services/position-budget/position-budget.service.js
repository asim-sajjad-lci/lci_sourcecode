"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var position_budget_module_1 = require("../../_models/position-budget/position-budget.module");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var PositionBudgetService = (function () {
    function PositionBudgetService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.positionURL = 'hrPositionBudget';
        this.getAllPositionBudgetUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
        this.searchPositionBudgetUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPositionBudget';
        this.createPositionBudgetUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPositionBudget';
        this.getPositionBudgetIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionBudgetId';
        this.updatePositionBudgetUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
        this.deletePositionBudgetUrl = Constants_1.Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
        this.getPositionPlanListUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanSetup/getAll';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }
    // get position budget schedule by Id
    PositionBudgetService.prototype.getPositionBudget = function (positionBudgetId) {
        return this.http.post(this.getPositionBudgetIdUrl, { positionBudgetId: positionBudgetId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // getting Position Class list
    PositionBudgetService.prototype.getPositionPlanList = function () {
        return this.http.put(this.getPositionPlanListUrl, {}, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // add new position budget
    PositionBudgetService.prototype.createPositionBudget = function (positionBudget) {
        return this.http.post(this.createPositionBudgetUrl, JSON.stringify(positionBudget), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // update for edit position budget
    PositionBudgetService.prototype.updatePositionBudget = function (positionBudget) {
        return this.http.post(this.updatePositionBudgetUrl, JSON.stringify(positionBudget), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // delete position budget
    PositionBudgetService.prototype.deletePositionBudget = function (ids) {
        return this.http.put(this.deletePositionBudgetUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // get list by search keyword
    PositionBudgetService.prototype.searchPositionBudgetlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPositionBudgetUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // get list
    PositionBudgetService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllPositionBudgetUrl, {
            // 'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // pagination for data
    PositionBudgetService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var positionBudget = new position_budget_module_1.PositionBudgetModule(jsonObj.id, jsonObj.positionPlanId, jsonObj.description, jsonObj.budgetScheduleStart, jsonObj.budgetScheduleEnd, jsonObj.budgetAmount, jsonObj.budgetDesc, jsonObj.budgetArbicDesc);
                    pagedData.data.push(positionBudget);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    // error handler
    PositionBudgetService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return PositionBudgetService;
}());
PositionBudgetService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PositionBudgetService);
exports.PositionBudgetService = PositionBudgetService;
//# sourceMappingURL=position-budget.service.js.map