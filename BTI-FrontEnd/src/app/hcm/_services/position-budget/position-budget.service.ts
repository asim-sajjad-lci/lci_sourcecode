import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {PositionBudgetModule} from '../../_models/position-budget/position-budget.module';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Page} from '../../../_sharedresource/page';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PositionBudgetService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private positionURL = 'hrPositionBudget';
    private getAllPositionBudgetUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
    private searchPositionBudgetUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPositionBudget';
    private createPositionBudgetUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPositionBudget';
    private getPositionBudgetIdUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getPositionBudgetId';
    private updatePositionBudgetUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
    private deletePositionBudgetUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
    private getPositionPlanListUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanSetup/getAll';
    private getsearchPositionPlanIdListUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanSetup/searchPositionPlanId';

    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }


    // get position budget schedule by Id
    getPositionBudget(positionBudgetId: string) {
        return this.http.post(this.getPositionBudgetIdUrl, { positionBudgetId: positionBudgetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // getting Position Class list
    getPositionPlanList() {
        return this.http.put(this.getPositionPlanListUrl, {}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
// getting Position Class list
    getsearchPositionPlanIdList() {
        return this.http.post(this.getsearchPositionPlanIdListUrl, {"searchKeyword": ""}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // add new position budget
    createPositionBudget(positionBudget: PositionBudgetModule) {
        return this.http.post(this.createPositionBudgetUrl, JSON.stringify(positionBudget), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit position budget
    updatePositionBudget(positionBudget: PositionBudgetModule) {
        return this.http.post(this.updatePositionBudgetUrl, JSON.stringify(positionBudget), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete position budget
    deletePositionBudget(ids: any) {
        return this.http.put(this.deletePositionBudgetUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    searchPositionBudgetlist(page: Page, searchKeyword): Observable<PagedData<PositionBudgetModule>> {
        return this.http.post(this.searchPositionBudgetUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PositionBudgetModule>> {
        return this.http.post(this.getAllPositionBudgetUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<PositionBudgetModule> {
        let pagedData = new PagedData<PositionBudgetModule>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let positionBudget = new PositionBudgetModule(
                        jsonObj.id,
                        jsonObj.positionPlanId,
                        jsonObj.description,
                        jsonObj.budgetScheduleStart,
                        jsonObj.budgetScheduleEnd,
                        jsonObj.budgetAmount,
                        jsonObj.budgetDesc,
                        jsonObj.budgetArbicDesc,
                        jsonObj.positionPlanPrimaryId,
                    );
                    pagedData.data.push(positionBudget);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

}
