/**
 * A service class for trainingBatchSignup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { TrainingBatchSignup } from "../../_models/training-batch-signup/training-batch-signup";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class TrainingBatchSignupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllTrainingBatchSignupUrl = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/getAll';
    private searchTrainingBatchSignupUrl = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/searchTrainingBatchSignup';
    private createTrainingBatchSignupUrl = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/create';
    private getTrainingBatchSignupByTrainingBatchSignupIdUrl = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/getTrainingBatchSignupDetailByTrainingBatchSignupId';
    private updateTrainingBatchSignupUrl = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/update';
    private deleteTrainingBatchSignupUrl = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/delete';

    //Training Course Class Index ID droupdown
    private getTrainingCourseAllIds = Constants.hcmModuleApiBaseUrl + 'trainingCourseDetail/getAllIds';
    //Training Course Detail Index ID droupdown
    private getAllTraningCourseId = Constants.hcmModuleApiBaseUrl + 'traningCourse/getAllTraningCourseId';
    //Employee Index ID droupdown
    private getAllEmployeeMasterDropDownList = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
    private getBatchSighnUpFromTrainingIdURL = Constants.hcmModuleApiBaseUrl + 'traningCourse/getTraningCourseById';
    private getTraningBatchSighUpByClassIdURL = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/getSighnUpFromTrainingId';
    private getTrainingCourseDetailTraningIdURL = Constants.hcmModuleApiBaseUrl + 'trainingCourseDetail/getAllByTraningId';
    private getClassIdByTrainingIdUrl = Constants.hcmModuleApiBaseUrl + 'trainingCourseDetail/getAllByTraningId';
    private getTraningIdByStatusUrl = Constants.hcmModuleApiBaseUrl + 'trainingBatchSignup/getTraningIdByStatus';

    public enrolledEmployees = 0;

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new trainingBatchSignup
    createTrainingBatchSignup(trainingBatchSignup: TrainingBatchSignup) {
        return this.http.post(this.createTrainingBatchSignupUrl, JSON.stringify(trainingBatchSignup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //get class id
    getClassIdByTrainingId(id) {
        return this.http.post(this.getClassIdByTrainingIdUrl, { 'id': id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update for edit trainingBatchSignup
    updateTrainingBatchSignup(trainingBatchSignup: TrainingBatchSignup) {
        return this.http.post(this.updateTrainingBatchSignupUrl, JSON.stringify(trainingBatchSignup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete trainingBatchSignup
    deleteTrainingBatchSignup(ids: any) {
        return this.http.put(this.deleteTrainingBatchSignupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getTrainingCourse(page: Page, searchKeyword) {
        return this.http.post(this.getTrainingCourseAllIds, { "searchKeyword": "", "pageNumber": 0, "pageSize": 5, "sortOn": "id", "sortBy": "DESC" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    
    getAllTraningCourse(page: Page, searchKeyword) {

        return this.http.post(this.getAllTraningCourseId, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    getAllEmployeeMaster() {
        return this.http.get(this.getAllEmployeeMasterDropDownList, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }





    //get trainingBatchSignup detail by Id
    getTrainingBatchSignup(trainingBatchSignupId: string) {
        return this.http.post(this.getTrainingBatchSignupByTrainingBatchSignupIdUrl, { trainingBatchSignupId: trainingBatchSignupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<TrainingBatchSignup>> {
        return this.http.post(this.getAllTrainingBatchSignupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }
    //get list
    getBatchSighnUpFromTrainingId(page: Page, id): Observable<PagedData<TrainingBatchSignup>> {
        return this.http.post(this.getBatchSighnUpFromTrainingIdURL, {
            'id': id
        }, { headers: this.headers }).map(data => this.getPagedCourseData(page, data.json().result.records.subItems));
    }
    //get list
    getTraningIdByStatus(page: Page, Status, id): Observable<PagedData<TrainingBatchSignup>> {
        return this.http.post(this.getTraningIdByStatusUrl, {
            'trainingClassStatus': Status,
            'id': id,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedCourseData(page, data.json().result.records.subItems));
    }
    //get list
    getTraningBatchSighUpByClassId(page: Page, id): Observable<Array<TrainingBatchSignup>> {
        return this.http.post(this.getTraningBatchSighUpByClassIdURL, {
            'id': id,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => data.json().result.records);
    }
    //get list
    getTrainingCourseDetailTraningId(id) {
        return this.http.post(this.getTrainingCourseDetailTraningIdURL, {
            'id': id
        }, { headers: this.headers }).map(data => data.json().result);
    }

    //get list by search keyword
    searchTrainingBatchSignuplist(page: Page, searchKeyword): Observable<PagedData<TrainingBatchSignup>> {
        return this.http.post(this.searchTrainingBatchSignupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<TrainingBatchSignup> {
        let pagedData = new PagedData<TrainingBatchSignup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    // let trainingBatchSignup = new TrainingBatchSignup(
                    //     jsonObj.id,
                    //     jsonObj.traningId,
                    //     jsonObj.desc,
                    //     jsonObj.arbicDesc,
                    //     jsonObj.location,
                    //     jsonObj.prerequisiteId,
                    //     jsonObj.courseInCompany,
                    //     jsonObj.employeeCost,
                    //     jsonObj.employerCost,
                    //     jsonObj.supplierCost,
                    //     jsonObj.instructorCost,
                    // );
                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //pagination for data
    private getPagedCourseData(page: Page, data: any): PagedData<TrainingBatchSignup> {
        let pagedData = new PagedData<TrainingBatchSignup>();
        if (data) {
            var gridRecords = data;
            page.totalElements = data.length;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];

                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    public setEnrolledEmployees(value) {
        this.enrolledEmployees = value;
    }
    public getEnrolledEmployees() {
        return this.enrolledEmployees;
    }
}