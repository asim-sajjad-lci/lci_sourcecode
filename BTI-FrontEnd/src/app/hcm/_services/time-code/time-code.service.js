"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var location_module_1 = require("../../_models/location/location.module");
var LocationService = (function () {
    //initializing parameter for constructor
    function LocationService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllLocationUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/getAll';
        this.searchLocationUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/searchLocation';
        this.createLocationUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/create';
        this.getLocationByLocationIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
        this.updateLocationUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/update';
        this.deleteLocationUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/delete';
        this.checkLocationIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/locationIdcheck';
        this.getAllCountriesUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
        this.getAllStatesByCountryIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
        this.getAllCitiesByStateIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new location
    LocationService.prototype.createLocation = function (location) {
        return this.http.post(this.createLocationUrl, JSON.stringify(location), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    LocationService.prototype.updateLocation = function (location) {
        return this.http.post(this.updateLocationUrl, JSON.stringify(location), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    LocationService.prototype.deleteLocation = function (ids) {
        return this.http.put(this.deleteLocationUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    LocationService.prototype.checkDuplicateLocationId = function (locationId) {
        return this.http.post(this.checkLocationIdx, { 'locationId': locationId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    LocationService.prototype.getLocation = function (locationId) {
        return this.http.post(this.getLocationByLocationIdUrl, { locationId: locationId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    LocationService.prototype.getCountry = function () {
        return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    LocationService.prototype.getStatesByCountryId = function (countryId) {
        return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    LocationService.prototype.getCitiesByStateId = function (stateId) {
        return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    LocationService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllLocationUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    LocationService.prototype.searchLocationlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchLocationUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    LocationService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var location_1 = new location_module_1.Location(jsonObj.id, jsonObj.locationId, jsonObj.description, jsonObj.arabicDescription, jsonObj.contactName, jsonObj.locationAddress, jsonObj.cityName, jsonObj.countryName, jsonObj.stateName, jsonObj.cityId, jsonObj.countryId, jsonObj.stateId, jsonObj.phone, jsonObj.fax);
                    pagedData.data.push(location_1);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    LocationService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return LocationService;
}());
LocationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], LocationService);
exports.LocationService = LocationService;
//# sourceMappingURL=location.service.js.map