import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {TimeCode} from '../../_models/time-code/time-code.module';

@Injectable()
export class TimeCodeService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllTimeCodeUrl = Constants.hcmModuleApiBaseUrl + 'timeCode/getAll';
    private searchTimeCodeUrl = Constants.hcmModuleApiBaseUrl + 'timeCode/search';
    private createTimeCodeUrl = Constants.hcmModuleApiBaseUrl + 'timeCode/create';
    private getTimeCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'timeCode/getById';
    private updateTimeCodeUrl = Constants.hcmModuleApiBaseUrl + 'timeCode/update';
    private deleteTimeCodeUrl = Constants.hcmModuleApiBaseUrl + 'timeCode/delete';
    private checkTimeCodeIdx = Constants.hcmModuleApiBaseUrl + 'timeCode/timeCodeIdcheck';
    private getAllCountriesUrl = Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
    private getAllStatesByCountryIdUrl = Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
    private getAllCitiesByStateIdUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
    private getAllAccrualSchedule = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/getAllIds';
    private getAllLinkedPayCode = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPayCodeDropDown';
    private timecodeIdUrl = Constants.hcmModuleApiBaseUrl + 'timeCode/searchAllTimeCodeId';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createTimeCode(timeCode: TimeCode) {
        return this.http.post(this.createTimeCodeUrl, JSON.stringify(timeCode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateTimeCode(timeCode: TimeCode) {
        return this.http.post(this.updateTimeCodeUrl, JSON.stringify(timeCode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteTimeCode(ids: any) {
        return this.http.put(this.deleteTimeCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateTimeCodeId(timeCodeId: any) {
        return this.http.post(this.checkTimeCodeIdx, { 'timeCodeId': timeCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getTimeCode(timeCodeId: string) {
        return this.http.post(this.getTimeCodeByIdUrl, { id: timeCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCountry() {
        return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    getAccrualSchedule() {
		return this.http.post(this.getAllAccrualSchedule, { 
        "searchKeyword": "",
        "pageNumber": 0,
        "pageSize": 5,
        "sortOn": "id",
        "sortBy": "DESC" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
       
    }

	getlinkedPayCode(){
		return this.http.get(this.getAllLinkedPayCode,  { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}
    getStatesByCountryId(countryId: number) {
        return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCitiesByStateId(stateId: number) {
        return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<TimeCode>> {
        return this.http.post(this.getAllTimeCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchTimeCodelist(page: Page, searchKeyword): Observable<PagedData<TimeCode>> {
        return this.http.post(this.searchTimeCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<TimeCode> {
        let pagedData = new PagedData<TimeCode>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let timeCode = new TimeCode(
                        jsonObj.id,
                        jsonObj.payCodeId,
                        jsonObj.timeCodeId,
                        jsonObj.inActive,
                        jsonObj.desc,
                        jsonObj.arbicDesc,
                        jsonObj.accrualPeriod,                        
                        jsonObj.timeType,
						jsonObj.scheduleId,
                        jsonObj.schedulePrimaryId,
                        jsonObj.payPrimaryId,
						jsonObj.description);
                    pagedData.data.push(timeCode);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getTimecodeId(){
        return this.http.post(this.timecodeIdUrl, {'searchKeyword' : ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
}
