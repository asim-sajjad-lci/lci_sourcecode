"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A service class for department
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/Rx");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var department_1 = require("../../_models/department/department");
var Constants_1 = require("../../../_sharedresource/Constants");
var DepartmentService = (function () {
    //initializing parameter for constructor 
    function DepartmentService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/getAll';
        this.searchDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/searchDepartment';
        this.createDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/create';
        this.getDepartmentByDepartmentIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/getDepartmentDetailByDepartmentId';
        this.updateDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/update';
        this.deleteDepartmentUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/delete';
        this.checkDepartmentIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'department/departmentIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new department
    DepartmentService.prototype.createDepartment = function (department) {
        return this.http.post(this.createDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    DepartmentService.prototype.updateDepartment = function (department) {
        return this.http.post(this.updateDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    DepartmentService.prototype.deleteDepartment = function (ids) {
        return this.http.put(this.deleteDepartmentUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    DepartmentService.prototype.checkDuplicateDeptId = function (departmentId) {
        return this.http.post(this.checkDepartmentIdx, { 'departmentId': departmentId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    DepartmentService.prototype.getDepartment = function (departmentId) {
        return this.http.post(this.getDepartmentByDepartmentIdUrl, { departmentId: departmentId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    DepartmentService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    DepartmentService.prototype.searchDepartmentlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    DepartmentService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var department = new department_1.Department(jsonObj.id, jsonObj.departmentId, jsonObj.departmentDescription, jsonObj.arabicDepartmentDescription);
                    pagedData.data.push(department);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    DepartmentService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return DepartmentService;
}());
DepartmentService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], DepartmentService);
exports.DepartmentService = DepartmentService;
//# sourceMappingURL=department.service.js.map