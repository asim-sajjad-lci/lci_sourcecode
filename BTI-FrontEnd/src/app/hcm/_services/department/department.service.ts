/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Department } from "../../_models/department/department";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class DepartmentService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/getAll';
    private searchDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/searchDepartment';
    private createDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/create';
    private getDepartmentByDepartmentIdUrl = Constants.hcmModuleApiBaseUrl + 'department/getDepartmentById';
    private updateDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/update';
    private deleteDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/delete';
    private checkDepartmentIdx = Constants.hcmModuleApiBaseUrl + 'department/departmentIdcheck';
	private getDepatmentId = Constants.hcmModuleApiBaseUrl + 'department/getAllDepartmentDropDown';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new department
    createDepartment(department: Department) {
        return this.http.post(this.createDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateDepartment(department: Department) {
        return this.http.post(this.updateDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteDepartment(ids: any) {
        return this.http.put(this.deleteDepartmentUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getDepartments(){
		 return this.http.get(this.getDepatmentId, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}

    //check for duplicate ID department
    checkDuplicateDeptId(departmentId: any) {
        return this.http.post(this.checkDepartmentIdx, { 'departmentId': departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getDepartment(departmentId: string) {
        return this.http.post(this.getDepartmentByDepartmentIdUrl, { id: departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<Department>> {
        return this.http.post(this.getAllDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchDepartmentlist(page: Page, searchKeyword): Observable<PagedData<Department>> {
        return this.http.post(this.searchDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<Department> {
        let pagedData = new PagedData<Department>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let department = new Department(
                        jsonObj.id,
                        jsonObj.departmentId,
                        jsonObj.departmentDescription,
                        jsonObj.arabicDepartmentDescription
                    );
                    pagedData.data.push(department);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}