import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {PositionPayCode} from '../../_models/position-pay-code/position-pay-code.module';

@Injectable()
export class PositionPayCodeService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllPositionPaymentCodeUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/getAll';
    private searchPositionPayCodeUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/searchSuperVisor';
    private createPositionPaymentCodeUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/createPositionPayCode';
    private updatePositionPayCodeUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/update';
    private deletePositionPayCodeUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/delete';
    private getPositionPlanUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanSetup/getAll';
    private getPositionDetailUrl = Constants.hcmModuleApiBaseUrl + 'positionPlanSetup/searchPositionPlan1';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }

    //add new location
    createPositionPayCode(positionpaycode: PositionPayCode) {
        return this.http.post(this.createPositionPaymentCodeUrl, JSON.stringify(positionpaycode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updatePositionPayCode(positionpaycode: PositionPayCode) {
        return this.http.post(this.updatePositionPayCodeUrl, JSON.stringify(positionpaycode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deletePositionPayCode(ids: any) {
        return this.http.put(this.deletePositionPayCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PositionPayCode>> {
        return this.http.post(this.getAllPositionPaymentCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchPositionPayCodelist(page: Page, searchKeyword): Observable<PagedData<PositionPayCode>> {
        return this.http.post(this.searchPositionPayCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<PositionPayCode> {
        let pagedData = new PagedData<PositionPayCode>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let supervisor = new PositionPayCode(
                        jsonObj.id,
                        jsonObj.positionPlanId,
                        jsonObj.codeType,
                        jsonObj.payCodeId,
                        jsonObj.payRate,
                        jsonObj.positionPlanDesc,
                        jsonObj.positiondescription,
                        jsonObj.positionId,
                        jsonObj.positionPlan);

                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getPositionPlan() {
        return this.http.post(this.getPositionPlanUrl, {}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getPositionDetail(Id:any) {
        return this.http.post(this.getPositionDetailUrl, {'searchKeyword': Id }, { headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
}
