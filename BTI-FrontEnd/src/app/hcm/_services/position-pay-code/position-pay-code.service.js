"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var position_pay_code_module_1 = require("../../_models/position-pay-code/position-pay-code.module");
var PositionPayCodeService = (function () {
    //initializing parameter for constructor
    function PositionPayCodeService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllPositionPaymentCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/getAll';
        this.searchPositionPayCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/searchSuperVisor';
        this.createPositionPaymentCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/createPositionPayCode';
        this.updatePositionPayCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/update';
        this.deletePositionPayCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanPayCode/delete';
        this.getPositionPlanUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanSetup/getAll';
        this.getPositionDetailUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionPlanSetup/searchPositionPlan1';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new location
    PositionPayCodeService.prototype.createPositionPayCode = function (positionpaycode) {
        return this.http.post(this.createPositionPaymentCodeUrl, JSON.stringify(positionpaycode), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    PositionPayCodeService.prototype.updatePositionPayCode = function (positionpaycode) {
        return this.http.post(this.updatePositionPayCodeUrl, JSON.stringify(positionpaycode), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    PositionPayCodeService.prototype.deletePositionPayCode = function (ids) {
        return this.http.put(this.deletePositionPayCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    /*//check for duplicate ID department
    checkDuplicateSupervisionCode(superVisionCode: any) {
        return this.http.post(this.checkSupervisorIdx, { 'superVisionCode': superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }*/
    /* //get department detail by Id
     getSupervisor(superVisionCode: string) {
         return this.http.post(this.getSupervisorBySupervisorIdUrl, { superVisionCode: superVisionCode }, { headers: this.headers })
             .toPromise()
             .then(res => res.json())
             .catch(this.handleError);
     }*/
    //get list
    PositionPayCodeService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllPositionPaymentCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    PositionPayCodeService.prototype.searchPositionPayCodelist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPositionPayCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    PositionPayCodeService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var supervisor = new position_pay_code_module_1.PositionPayCode(jsonObj.id, jsonObj.positionPlanId, jsonObj.codeType, jsonObj.payCodeId, jsonObj.payRate, jsonObj.positionPlanDesc, jsonObj.positiondescription, jsonObj.positionId, jsonObj.positionPlan);
                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    PositionPayCodeService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    /*//get department detail by Id
    getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .subscribe(data => {
                return data.json();
            });

    }*/
    /*getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }*/
    PositionPayCodeService.prototype.getPositionPlan = function () {
        return this.http.put(this.getPositionPlanUrl, {}, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    PositionPayCodeService.prototype.getPositionDetail = function (Id) {
        return this.http.post(this.getPositionDetailUrl, { 'searchKeyword': Id }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    return PositionPayCodeService;
}());
PositionPayCodeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PositionPayCodeService);
exports.PositionPayCodeService = PositionPayCodeService;
//# sourceMappingURL=position-pay-code.service.js.map