import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {ShiftCodeSetupModule} from '../../_models/shift-code-setup/shift-code-setup.module';

@Injectable()
export class ShiftCodeSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllShiftCodeSetupUrl = Constants.hcmModuleApiBaseUrl + 'shiftCode/getAll';
    private searchShiftCodeSetupUrl = Constants.hcmModuleApiBaseUrl + 'shiftCode/searchShiftCodeId';
    private createShiftCodeSetupUrl = Constants.hcmModuleApiBaseUrl + 'shiftCode/create';
    private getShiftCodeSetupByShiftCodeSetupIdUrl = Constants.hcmModuleApiBaseUrl + 'shiftCode/getById';
    private updateShiftCodeSetupUrl = Constants.hcmModuleApiBaseUrl + 'shiftCode/update';
    private deleteShiftCodeSetupUrl = Constants.hcmModuleApiBaseUrl + 'shiftCode/delete';
    private checkShiftCodeSetupIdx = Constants.hcmModuleApiBaseUrl + 'shiftCode/shiftCodecheck';
    private searchShiftCodeIDUrl = Constants.hcmModuleApiBaseUrl + 'shiftCode/searchShiftCodeId';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }

    // Shift Code Search Id
    
    getShiftCodeIdList() {
        return this.http.post(this.searchShiftCodeIDUrl,{'searchKeyword': ''} ,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //add new ShiftCodeSetup
    createShiftCodeSetup(shiftCodeSetup: ShiftCodeSetupModule) {
        return this.http.post(this.createShiftCodeSetupUrl, JSON.stringify(shiftCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateShiftCodeSetup(shiftCodeSetup: ShiftCodeSetupModule) {
        return this.http.post(this.updateShiftCodeSetupUrl, JSON.stringify(shiftCodeSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteShiftCodeSetup(ids: any) {
        return this.http.put(this.deleteShiftCodeSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateShiftCodeSetupId(shiftCodeId: any) {
        return this.http.post(this.checkShiftCodeSetupIdx, { 'shiftCodeId': shiftCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getShiftCodeSetup(shiftCodeSetupId: string) {
        return this.http.post(this.getShiftCodeSetupByShiftCodeSetupIdUrl, { shiftCodeSetupId: shiftCodeSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<ShiftCodeSetupModule>> {
        return this.http.post(this.getAllShiftCodeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchShiftCodeSetuplist(page: Page, searchKeyword): Observable<PagedData<ShiftCodeSetupModule>> {
        return this.http.post(this.searchShiftCodeSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<ShiftCodeSetupModule> {
        let pagedData = new PagedData<ShiftCodeSetupModule>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let shiftCodeSetupModule = new ShiftCodeSetupModule(
                        jsonObj.id,
                        jsonObj.shiftCodeId,
                        jsonObj.desc,
                        jsonObj.arabicDesc,
                        jsonObj.inActive,
                        jsonObj.shitPremium,
                        jsonObj.amount,
                        jsonObj.percent);
                    pagedData.data.push(shiftCodeSetupModule);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
