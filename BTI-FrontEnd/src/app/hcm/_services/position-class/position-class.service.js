"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/Rx");
var Constants_1 = require("../../../_sharedresource/Constants");
var position_class_module_1 = require("../../_models/position-class/position-class.module");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var PositionClassService = (function () {
    function PositionClassService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllPositionClassUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/getAll';
        this.searchPositionClassUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/searchPositionClass';
        this.createPositionClassUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/create';
        this.getPositionClassByPositionClassIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/getPositionClassDetailByPositionClassId';
        this.updatePositionClassUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/update';
        this.deletePositionClassUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/delete';
        this.checkPositionClassIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'positionClass/positionClassIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }
    // get position class detail by Id
    PositionClassService.prototype.getPositionClass = function (positionClassId) {
        return this.http.post(this.getPositionClassByPositionClassIdUrl, { positionClassId: positionClassId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // check for duplicate ID position class
    PositionClassService.prototype.checkDuplicatePositionClassId = function (positionClassId) {
        return this.http.post(this.checkPositionClassIdx, { 'positionClassId': positionClassId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // add new position class
    PositionClassService.prototype.createPositionClass = function (positionClass) {
        return this.http.post(this.createPositionClassUrl, JSON.stringify(positionClass), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // update for edit position class
    PositionClassService.prototype.updatePositionClass = function (positionClass) {
        return this.http.post(this.updatePositionClassUrl, JSON.stringify(positionClass), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // delete position class
    PositionClassService.prototype.deletePositionClass = function (ids) {
        return this.http.put(this.deletePositionClassUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // get list by search keyword
    PositionClassService.prototype.searchPositionClasslist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchPositionClassUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // get list
    PositionClassService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllPositionClassUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    // pagination for data
    PositionClassService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var positionClass = new position_class_module_1.PositionClassModule(jsonObj.id, jsonObj.positionClassId, jsonObj.positionClassDescription, jsonObj.arabicPositionClassDescription);
                    pagedData.data.push(positionClass);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    // error handler
    PositionClassService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return PositionClassService;
}());
PositionClassService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PositionClassService);
exports.PositionClassService = PositionClassService;
//# sourceMappingURL=position-class.service.js.map