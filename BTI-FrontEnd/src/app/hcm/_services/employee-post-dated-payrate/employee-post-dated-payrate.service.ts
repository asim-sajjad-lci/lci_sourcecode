import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {EmployeePostDatedPayrateModule} from '../../_models/employee-post-dated-payrate/employee-post-dated-payrate.module';

@Injectable()
export class EmployeePostDatedPayrateService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/getAll';
    private searchSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/searchSuperVisor';
    private createSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/create';
    private getSupervisorBySupervisorIdUrl = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/getLocationDetailByLocationId';
    private updateSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/update';
    private deleteSupervisorUrl = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/delete';
    private checkSupervisorIdx = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/employeeIdCheck';
    private getEmployeeIdDetail = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/searchUserbyId';
    private superviserIdUrl = Constants.hcmModuleApiBaseUrl + 'employeePostDatedPayRates/getAllSupervisorDropDownList';
    private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
    private getgetPayTypeDropdown = Constants.hcmModuleApiBaseUrl + 'payCode/searchAllPayCodeId';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createPostDated(supervisor: EmployeePostDatedPayrateModule) {
        return this.http.post(this.createSupervisorUrl, JSON.stringify(supervisor), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updatePostDated(supervisor: EmployeePostDatedPayrateModule) {
        return this.http.post(this.updateSupervisorUrl, JSON.stringify(supervisor), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteSupervisor(ids: any) {
        return this.http.put(this.deleteSupervisorUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getSuperviserId(){
        return this.http.get(this.superviserIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    getEmployee(){
        return this.http.get(this.employeeIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    //check for duplicate ID department
    checkDuplicatePostDated(superVisionCode: any) {
        return this.http.post(this.checkSupervisorIdx, { 'employeeMaster': superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getSupervisor(superVisionCode: string) {
        return this.http.post(this.getSupervisorBySupervisorIdUrl, { superVisionCode: superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<EmployeePostDatedPayrateModule>> {
        return this.http.post(this.getAllSupervisorUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchSupervisorlist(page: Page, searchKeyword): Observable<PagedData<EmployeePostDatedPayrateModule>> {
        return this.http.post(this.searchSupervisorUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeePostDatedPayrateModule> {
        let pagedData = new PagedData<EmployeePostDatedPayrateModule>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let supervisor = new EmployeePostDatedPayrateModule(
                        jsonObj.id,
                        jsonObj.employeeMaster,
                        jsonObj.payCode,
                        jsonObj.currentPayRate,
                        jsonObj.newPayRate,
                        jsonObj.effectiveDate,
                        jsonObj.reasonforChange);
                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getEmployeeDetail(employeeId: any) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getPayTypeDropdown() {
        return this.http.post(this.getgetPayTypeDropdown, { "searchKeyword": "" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
}
