import { TestBed, inject } from '@angular/core/testing';

import { EmployeePostDatedPayrateService } from './employee-post-dated-payrate.service';

describe('EmployeePostDatedPayrateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeePostDatedPayrateService]
    });
  });

  it('should be created', inject([EmployeePostDatedPayrateService], (service: EmployeePostDatedPayrateService) => {
    expect(service).toBeTruthy();
  }));
});
