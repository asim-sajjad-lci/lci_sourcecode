"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var employee_post_dated_payrate_service_1 = require("./employee-post-dated-payrate.service");
describe('EmployeePostDatedPayrateService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [employee_post_dated_payrate_service_1.EmployeePostDatedPayrateService]
        });
    });
    it('should be created', testing_1.inject([employee_post_dated_payrate_service_1.EmployeePostDatedPayrateService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=employee-post-dated-payrate.service.spec.js.map