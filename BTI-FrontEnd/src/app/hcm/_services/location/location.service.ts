import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {Location} from '../../_models/location/location.module';

@Injectable()
export class LocationService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllLocationUrl = Constants.hcmModuleApiBaseUrl + 'location/getAll';
    private searchLocationUrl = Constants.hcmModuleApiBaseUrl + 'location/searchLocation';
    private createLocationUrl = Constants.hcmModuleApiBaseUrl + 'location/create';
    private getLocationByLocationIdUrl = Constants.hcmModuleApiBaseUrl + 'location/getLocationById';
    private updateLocationUrl = Constants.hcmModuleApiBaseUrl + 'location/update';
    private deleteLocationUrl = Constants.hcmModuleApiBaseUrl + 'location/delete';
    private checkLocationIdx = Constants.hcmModuleApiBaseUrl + 'location/locationIdcheck';
    private getAllCountriesUrl = Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
    private getAllStatesByCountryIdUrl = Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
    private getAllCitiesByStateIdUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
    private locationIdUrl = Constants.hcmModuleApiBaseUrl + 'location/searchAllLocationId';
    private getAllLocationDropDownUrl = Constants.hcmModuleApiBaseUrl + 'location/getAllLocationDropDown';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createLocation(location: Location) {
        return this.http.post(this.createLocationUrl, JSON.stringify(location), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateLocation(location: Location) {
        return this.http.post(this.updateLocationUrl, JSON.stringify(location), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteLocation(ids: any) {
        return this.http.put(this.deleteLocationUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateLocationId(locationId: any) {
        return this.http.post(this.checkLocationIdx, { 'locationId': locationId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getLocation(locationId: string) {
        return this.http.post(this.getLocationByLocationIdUrl, { id: locationId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCountry() {
        return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getStatesByCountryId(countryId: number) {
        return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCitiesByStateId(stateId: number) {
        return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<Location>> {
        return this.http.post(this.getAllLocationUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchLocationlist(page: Page, searchKeyword): Observable<PagedData<Location>> {
        return this.http.post(this.searchLocationUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<Location> {
        let pagedData = new PagedData<Location>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let location = new Location(
                        jsonObj.id,
                        jsonObj.locationId,
                        jsonObj.description,
                        jsonObj.arabicDescription,
                        jsonObj.contactName,
                        jsonObj.locationAddress,
                        jsonObj.cityName,
                        jsonObj.countryName,
                        jsonObj.stateName,
                        jsonObj.cityId,
                        jsonObj.countryId,
                        jsonObj.stateId,
                        jsonObj.phone,
                        jsonObj.fax);
                    pagedData.data.push(location);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getLocationId(){
        return this.http.post(this.locationIdUrl, {'searchKeyword' : ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    getAllLocationDropDownId(){
        return this.http.get(this.getAllLocationDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
}
