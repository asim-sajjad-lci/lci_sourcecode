import { Injectable } from '@angular/core';
import { PredefineChecklist } from '../../_models/predefine-checklist/predefine-checklist.module';
import { Observable } from 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Http, Headers } from '@angular/http';
import { Constants } from '../../../_sharedresource/Constants';
import { Page } from '../../../_sharedresource/page';

@Injectable()
export class PredefineChecklistService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllPredefinedCkLstUrl = Constants.hcmModuleApiBaseUrl + 'orientationPredefinedCheckListItem/getAll';
  private createPredefinedCkLstUrl = Constants.hcmModuleApiBaseUrl + 'orientationPredefinedCheckListItem/create';
  private updatePredefinedCkLstUrl = Constants.hcmModuleApiBaseUrl + 'orientationPredefinedCheckListItem/update';
  private deletePredefinedCkLstUrl = Constants.hcmModuleApiBaseUrl + 'orientationPredefinedCheckListItem/delete';
  private deletesubitemPredefinedCkLstUrl= Constants.hcmModuleApiBaseUrl + 'predefinedCheckListSetup/delete';

  
  // initializing parameter for constructor
  constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


  //add new Health Insurance
  createPredefinedCkLst(predefinedCkLstSetup: PredefineChecklist) {
      return this.http.post(this.createPredefinedCkLstUrl, JSON.stringify(predefinedCkLstSetup), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

  //update for edit department
  updatePredefinedCkLst(predefinedCkLstSetup: PredefineChecklist) {
      return this.http.post(this.updatePredefinedCkLstUrl, JSON.stringify(predefinedCkLstSetup), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

  //delete department
  deletePredefinedCkLst(ids: any) {
      return this.http.put(this.deletePredefinedCkLstUrl, { 'ids': ids }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    deleteSubitemPredefinedCkLst(ids: any) {
        return this.http.put(this.deletesubitemPredefinedCkLstUrl, { 'ids': ids }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
      }

 
  //get list
//   latestgetlist(page:Page, searchKeyword): Observable<PagedData<PredefineChecklist>> {
//     return this.http.post(this.getAllPredefinedCkLstUrl, {
//         'searchKeyword': searchKeyword,
//     }, {headers: this.headers}).map(data => this.getPagedData(page, data.json().result));
//   }



    
  
  getlist(page: Page, searchKeyword): Observable<PagedData<PredefineChecklist>> {
      return this.http.post(this.getAllPredefinedCkLstUrl, {
          'searchKeyword': searchKeyword,
          'pageNumber': page.pageNumber,
          'pageSize': page.size,
          'sortOn': page.sortOn,
          'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }


  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<PredefineChecklist> {
      let pagedData = new PagedData<PredefineChecklist>();
      if (data) {
          var gridRecords = data.records;
          page.totalElements = data.totalCount;
          if (gridRecords && gridRecords.length > 0) {
              for (let i = 0; i < gridRecords.length; i++) {
                  let jsonObj = gridRecords[i];
                  let PredefinedCkLst = new PredefineChecklist(
                    jsonObj.id,
                    jsonObj.preDefineCheckListType,
                    jsonObj.preDefineCheckListItemDescription,
                    jsonObj.preDefineCheckListItemDescriptionArabic,
                    jsonObj.subItems);
                  pagedData.data.push(PredefinedCkLst);
                }
            }
        }
      page.totalPages = page.totalElements / page.size;
      let start = page.pageNumber * page.size;
      let end = Math.min((start + page.size), page.totalElements);
      pagedData.page = page;
      return pagedData;
    }

  //error handler
  private handleError(error: any): Promise<any> {
      return Promise.reject(error.message || error);
    }
}