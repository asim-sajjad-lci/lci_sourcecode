import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class LoanESBSetupService {

  private headers = new Headers({ 'content-type': 'application/json' });

  private typeIdDetails = Constants.hcmModuleApiBaseUrl + 'typeFieldForCodes/getAllIds';
  private saveLoanPackage = Constants.hcmModuleApiBaseUrl + 'packageESB/create';
  private getLoanPackage = Constants.hcmModuleApiBaseUrl + 'packageESB/getByTypeId';

  //initializing parameter for constructor
  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
    console.log('Header: ', this.headers)
  }


  //get type field detail by Id
  getTypeFieldDetails(typeid) {
    return this.http.post(this.typeIdDetails, { id: typeid }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get data by type Id
  getTypeData(typeid) {
    return this.http.post(this.getLoanPackage, { typeId: typeid }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  saveLoanPackageSetup(model) {

    console.log(JSON.stringify(model));
    return this.http.post(this.saveLoanPackage, JSON.stringify(model), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
