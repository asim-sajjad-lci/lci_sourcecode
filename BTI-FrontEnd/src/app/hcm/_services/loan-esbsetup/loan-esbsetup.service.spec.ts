import { TestBed, inject } from '@angular/core/testing';

import { LoanESBSetupService } from './loan-esbsetup.service';

describe('LoanESBSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoanESBSetupService]
    });
  });

  it('should be created', inject([LoanESBSetupService], (service: LoanESBSetupService) => {
    expect(service).toBeTruthy();
  }));
});
