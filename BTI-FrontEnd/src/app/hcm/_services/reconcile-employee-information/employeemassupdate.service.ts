/**
 * A service class for employeeAddressMaster
 */

import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { EmployeeAddressMaster } from "../../_models/employee-address-master/employeeAddressMaster";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class EmployeeAddressMasterService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllEmployeeAddressMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/getAll';
    private searchEmployeeAddressMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/getAll';
    private createEmployeeAddressMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/create';
    private updateEmployeeAddressMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/update';
    private deleteEmployeeAddressMasterUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/delete';
    private getEmployeeAddressMasterByIdUrl = Constants.hcmModuleApiBaseUrl +  'employeeAddressMaster/getById';
    private addressIdCheckUrl = Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/addressIdcheck';
    private AddressIdEmployeeAddressMasterUrl=Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/getAllEmployeeAddressMasterDropDown';
   
    //initializing parameter for constructor 

    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new emploeeAddressMaster
    createEmploeeAddressMaster(emploeeAddressMaster: EmployeeAddressMaster) {
        return this.http.post(this.createEmployeeAddressMasterUrl, JSON.stringify(emploeeAddressMaster), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAddressList() {
        return this.http.get(this.AddressIdEmployeeAddressMasterUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update emploeeAddressMaster
    updateEmploeeAddressMaster(emploeeAddressMaster: EmployeeAddressMaster) {
        return this.http.post(this.updateEmployeeAddressMasterUrl, JSON.stringify(emploeeAddressMaster), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete interviewTypeSeup
    deleteEmploeeAddressMaster(ids: any) {
        return this.http.put(this.deleteEmployeeAddressMasterUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<EmployeeAddressMaster>> {
        return this.http.post(this.getAllEmployeeAddressMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<EmployeeAddressMaster> {
        let pagedData = new PagedData<EmployeeAddressMaster>();
        //debugger;
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let emploeeAddressMaster = new EmployeeAddressMaster(
                        jsonObj.employeeAddressIndexId,
                        jsonObj.addressId,
                        jsonObj.address1,
                        jsonObj.address2,
                        jsonObj.personalEmail,
                        jsonObj.businessEmail,
                        jsonObj.addressArabic1,
                        jsonObj.addressArabic2,    
                        jsonObj.pOBox,
                        jsonObj.postCode,
                        jsonObj.personalPhone,
                        jsonObj.businessPhone,
                        jsonObj.otherPhone,
                        jsonObj.locationLinkGoogleMap,
                        jsonObj.city,
                        jsonObj.cityArabic,
                        jsonObj.country,
                        jsonObj.countryArabic,
                        jsonObj.ext,
                        jsonObj.countryId,
                        jsonObj.stateId,
                        jsonObj.cityId,
                        jsonObj.cityName,
                        jsonObj.countryName,
                        jsonObj.stateName,
                    );
                    pagedData.data.push(emploeeAddressMaster);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}