/**
 * A service class for division
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Division } from "../../_models/division/division";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class DivisionService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/getAll';
    private searchDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/searchDivision';
    private createDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/create';
    private getDivisionByDivisionIdUrl = Constants.hcmModuleApiBaseUrl + 'division/getDivisionById';
    private updateDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/update';
    private deleteDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/delete';
    private getCountryListUrl = Constants.hcmModuleApiBaseUrl + 'division/getCountryList';
    private getStateListUrl = Constants.hcmModuleApiBaseUrl + 'getStateListByCountryId';
    private getCityListUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityList';
    private getCountryCodeUrl = Constants.hcmModuleApiBaseUrl + 'getCountryCodeByCountryId';
    private checkDivisionIdx = Constants.hcmModuleApiBaseUrl + 'division/divisionIdcheck';
    
    private getAllCountriesUrl = Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
    private getAllStatesByCountryIdUrl = Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
    private getAllCitiesByStateIdUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
    private getAllDivisionDropDownURL = Constants.hcmModuleApiBaseUrl + 'division/getAllDivisionDropDown';


    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    // getting Division Class list
    getDivisionClassList() {
        return this.http.get(this.getAllDivisionDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting city list
    getCityList() {
        return this.http.get(this.getCityListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getCountry() {
        return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getStatesByCountryId(countryId: number) {
        return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCitiesByStateId(stateId: number) {
        return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //getting state list
    getStateList(countryId: any) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting country code list
    getCountryCode(countryId: any) {
        return this.http.post(this.getCountryCodeUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check division Id, if match return true else false
    checkDivisionId(divisionId: any) {
        return this.http.post(this.checkDivisionIdx, { 'divisionId': divisionId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    

    //getting country list
    getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //add new division
    createDivision(division: Division) {
        return this.http.post(this.createDivisionUrl, JSON.stringify(division), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit division
    updateDivision(division: Division) {
        return this.http.post(this.updateDivisionUrl, JSON.stringify(division), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete division
    deleteDivision(ids: any) {
        return this.http.put(this.deleteDivisionUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get division detail by Id
    getDivision(divisionId: any) {
        return this.http.post(this.getDivisionByDivisionIdUrl, { id: divisionId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<Division>> {
        return this.http.post(this.getAllDivisionUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list
    searchDivisionlist(page: Page, searchKeyword): Observable<PagedData<Division>> {
        return this.http.post(this.searchDivisionUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<Division> {
        let pagedData = new PagedData<Division>();
        if (data) {
            var gridRecords = data.records;

            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let division = new Division(
                        jsonObj.id,
                        jsonObj.divisionId,
                        jsonObj.divisionDescription,
                        jsonObj.arabicDivisionDescription,
                        jsonObj.divisionAddress,
                        jsonObj.phoneNumber,
                        jsonObj.fax,
                        jsonObj.email,
                        jsonObj.cityName,
                        jsonObj.countryName,
                        jsonObj.stateName,
                        jsonObj.cityId,
                        jsonObj.countryId,
                        jsonObj.stateId,
                    );
                    pagedData.data.push(division);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}