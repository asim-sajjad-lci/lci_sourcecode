"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A service class for division
 */
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
require("rxjs/Rx");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var division_1 = require("../../_models/division/division");
var Constants_1 = require("../../../_sharedresource/Constants");
var DivisionService = (function () {
    //initializing parameter for constructor 
    function DivisionService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllDivisionUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'division/getAll';
        this.searchDivisionUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'division/searchDivision';
        this.createDivisionUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'division/create';
        this.getDivisionByDivisionIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'division/getDivisionDetailByDivisionId';
        this.updateDivisionUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'division/update';
        this.deleteDivisionUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'division/delete';
        this.getCountryListUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'getCountryList';
        this.getStateListUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'getStateListByCountryId';
        this.getCityListUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'hrCity/getCityList';
        this.getCountryCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'getCountryCodeByCountryId';
        this.checkDivisionIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'division/divisionIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
    //getting city list
    DivisionService.prototype.getCityList = function () {
        return this.http.get(this.getCityListUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //getting state list
    DivisionService.prototype.getStateList = function (countryId) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //getting country code list
    DivisionService.prototype.getCountryCode = function (countryId) {
        return this.http.post(this.getCountryCodeUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check division Id, if match return true else false
    DivisionService.prototype.checkDivisionId = function (divisionId) {
        return this.http.post(this.checkDivisionIdx, { 'divisionId': divisionId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //getting country list
    DivisionService.prototype.getCountryList = function () {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //add new division
    DivisionService.prototype.createDivision = function (division) {
        return this.http.post(this.createDivisionUrl, JSON.stringify(division), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit division
    DivisionService.prototype.updateDivision = function (division) {
        return this.http.post(this.updateDivisionUrl, JSON.stringify(division), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete division
    DivisionService.prototype.deleteDivision = function (ids) {
        return this.http.put(this.deleteDivisionUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get division detail by Id
    DivisionService.prototype.getDivision = function (divisionId) {
        return this.http.post(this.getDivisionByDivisionIdUrl, { divisionId: divisionId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    DivisionService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllDivisionUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list
    DivisionService.prototype.searchDivisionlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchDivisionUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    DivisionService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var division = new division_1.Division(jsonObj.id, jsonObj.divisionId, jsonObj.divisionDescription, jsonObj.arabicDivisionDescription, jsonObj.divisionAddress, jsonObj.city, jsonObj.phoneNumber, jsonObj.fax, jsonObj.email);
                    pagedData.data.push(division);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    DivisionService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return DivisionService;
}());
DivisionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], DivisionService);
exports.DivisionService = DivisionService;
//# sourceMappingURL=division.service.js.map