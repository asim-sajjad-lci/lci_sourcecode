import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {ActivitiMaster} from '../../_models/activiti-master/activiti-master.module';

@Injectable()
export class ActivitiMasterService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllActivitiMasterUrl = Constants.hcmModuleApiBaseUrl + 'activitiMaster/getAll';
    private searchActivitiMasterUrl = Constants.hcmModuleApiBaseUrl + 'activitiMaster/searchSuperVisor';
    private createActivitiMasterUrl = Constants.hcmModuleApiBaseUrl + 'activitiMaster/create';
    private getActivitiMasterByActivitiMasterIdUrl = Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
    private updateActivitiMasterUrl = Constants.hcmModuleApiBaseUrl + 'activitiMaster/update';
    private deleteActivitiMasterUrl = Constants.hcmModuleApiBaseUrl + 'activitiMaster/delete';
    private checkActivitiMasterIdx = Constants.hcmModuleApiBaseUrl + 'activitiMaster/activitiIdcheck';
    private getActivitiIdDetail = Constants.hcmModuleApiBaseUrl + 'activitiMaster/searchUserbyId';
	private superviserIdUrl = Constants.hcmModuleApiBaseUrl + 'supervisor/getAllSupervisorDropDownList';
	private activitiIdUrl = Constants.hcmModuleApiBaseUrl + 'activitiMaster/getAllActivitiMasterDropDownList';
	private getAllActivitiAddressMasterDropDownUrl = Constants.hcmModuleApiBaseUrl + 'activitiAddressMaster/getAllActivitiAddressMasterDropDown';
	private getAllActivitiNationalitiesDropDownUrl = Constants.hcmModuleApiBaseUrl + 'activitiNationalities/getAllActivitiNationalitiesDropDown';
	private getgetAllUserDetailDropDownUrl = Constants.userModuleApiBaseUrl + 'user/getAllUserDetail';
	private getAllSupervisorDropDownListURL = Constants.hcmModuleApiBaseUrl + 'supervisor/getAllSupervisorDropDownList';
    //initializing parameter for constructor
    constructor(private http: Http) {
       // localStorage.setItem('tenantid','bti_hr')
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createActivitiMaster(activitiMaster: ActivitiMaster) {
        return this.http.post(this.createActivitiMasterUrl, JSON.stringify(activitiMaster), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //add new location
      createActivitiMasterAttachment(activitiMaster: ActivitiMaster,file: File) {
        let formData: FormData = new FormData();      
        if(file && file.name){
            formData.append('file', file, file.name);
        }   
        formData.append('DtoActivitiMaster', JSON.stringify(activitiMaster));
        this.headers.delete('Content-Type');

        return this.http.post(this.createActivitiMasterUrl, formData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateActivitiMaster(activitiMaster,file: File) {
        let formData: FormData = new FormData();
        console.log(file);
      
            formData.append('file', file, file.name);
             
        formData.append('DtoActivitiMaster', JSON.stringify(activitiMaster));
        this.headers.delete('Content-Type');

        return this.http.post(this.updateActivitiMasterUrl, formData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    // //update for edit department
    // updateActivitiMaster(activitiMaster: ActivitiMaster) {
    //     return this.http.post(this.updateActivitiMasterUrl, JSON.stringify(activitiMaster), { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    //delete department
    deleteActivitiMaster(ids: any) {
        return this.http.put(this.deleteActivitiMasterUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getSuperviserId(){
		return this.http.get(this.superviserIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getActiviti(){
		return this.http.get(this.activitiIdUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllSupervisorDropDownList(){
		return this.http.get(this.getAllSupervisorDropDownListURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllActivitiAddressMasterDropDown(){
		return this.http.get(this.getAllActivitiAddressMasterDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllActivitiNationalitiesDropDown(){
		return this.http.get(this.getAllActivitiNationalitiesDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllUserDetailDropDown(){
		return this.http.get(this.getgetAllUserDetailDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
    //check for duplicate ID department
    checkDuplicateActivitiCode(activitiId: any) {
        return this.http.post(this.checkActivitiMasterIdx, { 'activitiId': activitiId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getActivitiMaster(activitiId: string) {
        return this.http.post(this.getActivitiMasterByActivitiMasterIdUrl, { activitiId: activitiId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<ActivitiMaster>> {
        return this.http.post(this.getAllActivitiMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchActivitiMasterlist(page: Page, searchKeyword): Observable<PagedData<ActivitiMaster>> {
        return this.http.post(this.searchActivitiMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<ActivitiMaster> {
        let pagedData = new PagedData<ActivitiMaster>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    // let activitiMaster = new ActivitiMaster(
                    //     jsonObj.id,
                    //     jsonObj.activitiId,
                    //     jsonObj.description,
                    //     jsonObj.arabicDescription,
                    //     jsonObj.activitiName,
                    //     jsonObj.activitiId);
                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getActivitiDetail(activitiId: any) {
        return this.http.post(this.getActivitiIdDetail, { 'searchKeyword': activitiId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
}
