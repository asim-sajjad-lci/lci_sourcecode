"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var EmployeeMasterService = (function () {
    //initializing parameter for constructor
    function EmployeeMasterService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllEmployeeMasterUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAll';
        this.searchEmployeeMasterUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/searchSuperVisor';
        this.createEmployeeMasterUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/create';
        this.getEmployeeMasterByEmployeeMasterIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
        this.updateEmployeeMasterUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/update';
        this.deleteEmployeeMasterUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/delete';
        this.checkEmployeeMasterIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/employeeMasterCodeCheck';
        this.getEmployeeIdDetail = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/searchUserbyId';
        this.superviserIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
        this.employeeIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
        this.getAllEmployeeAddressMasterDropDownUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeAddressMaster/getAllEmployeeAddressMasterDropDown';
        this.getAllEmployeeNationalitiesDropDownUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeNationalities/getAllEmployeeNationalitiesDropDown';
        this.getgetAllUserDetailDropDownUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'user/getAllUserDetail';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new location
    EmployeeMasterService.prototype.createEmployeeMaster = function (employeeMaster) {
        return this.http.post(this.createEmployeeMasterUrl, JSON.stringify(employeeMaster), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //add new location
    EmployeeMasterService.prototype.createEmployeeMasterAttachment = function (employeeMaster, file) {
        var formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('DtoEmployeeMaster', JSON.stringify(employeeMaster));
        this.headers.delete('Content-Type');
        return this.http.post(this.createEmployeeMasterUrl, formData, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    EmployeeMasterService.prototype.updateEmployeeMaster = function (employeeMaster) {
        return this.http.post(this.updateEmployeeMasterUrl, JSON.stringify(employeeMaster), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    EmployeeMasterService.prototype.deleteEmployeeMaster = function (ids) {
        return this.http.put(this.deleteEmployeeMasterUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    EmployeeMasterService.prototype.getSuperviserId = function () {
        return this.http.get(this.superviserIdUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    EmployeeMasterService.prototype.getEmployee = function () {
        return this.http.get(this.employeeIdUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    EmployeeMasterService.prototype.getAllEmployeeAddressMasterDropDown = function () {
        return this.http.get(this.getAllEmployeeAddressMasterDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    EmployeeMasterService.prototype.getAllEmployeeNationalitiesDropDown = function () {
        return this.http.get(this.getAllEmployeeNationalitiesDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    EmployeeMasterService.prototype.getAllUserDetailDropDown = function () {
        return this.http.get(this.getgetAllUserDetailDropDownUrl, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    EmployeeMasterService.prototype.checkDuplicateEmployeeCode = function (employeeId) {
        return this.http.post(this.checkEmployeeMasterIdx, { 'employeeId': employeeId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    EmployeeMasterService.prototype.getEmployeeMaster = function (employeeId) {
        return this.http.post(this.getEmployeeMasterByEmployeeMasterIdUrl, { employeeId: employeeId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    EmployeeMasterService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.getAllEmployeeMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    EmployeeMasterService.prototype.searchEmployeeMasterlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchEmployeeMasterUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    EmployeeMasterService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    // let employeeMaster = new EmployeeMaster(
                    //     jsonObj.id,
                    //     jsonObj.employeeId,
                    //     jsonObj.description,
                    //     jsonObj.arabicDescription,
                    //     jsonObj.employeeName,
                    //     jsonObj.employeeId);
                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    EmployeeMasterService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    EmployeeMasterService.prototype.getEmployeeDetail = function (employeeId) {
        return this.http.post(this.getEmployeeIdDetail, { 'searchKeyword': employeeId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    return EmployeeMasterService;
}());
EmployeeMasterService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], EmployeeMasterService);
exports.EmployeeMasterService = EmployeeMasterService;
//# sourceMappingURL=employee-master.service.js.map