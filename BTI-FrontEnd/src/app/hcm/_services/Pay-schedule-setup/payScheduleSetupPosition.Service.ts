/**
 * A service class for PayScheduleSetupPosition
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { PayScheduleSetupPosition } from "../../_models/pay-schedule-setup/payScheduleSetupPosition";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class PayScheduleSetupPositionService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllPayScheduleSetupPositionUrl = Constants.hcmModuleApiBaseUrl + 'positionSetup/getAllPostionDropDownList';
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    
    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PayScheduleSetupPosition>> {
        return this.http.get(this.getAllPayScheduleSetupPositionUrl, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<PayScheduleSetupPosition> {
        let pagedData = new PagedData<PayScheduleSetupPosition>();
        if (data) {
            var gridRecords = data;
            page.totalElements = data.length;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let payScheduleSetupPosition = new PayScheduleSetupPosition(
                        jsonObj.id,
                        jsonObj.description,
                        jsonObj.arabicDescription,
                        jsonObj.positionId
                    );
                    pagedData.data.push(payScheduleSetupPosition);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}