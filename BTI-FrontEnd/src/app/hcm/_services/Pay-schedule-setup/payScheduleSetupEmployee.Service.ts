/**
 * A service class for PayScheduleSetupEmployee
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { PayScheduleSetupEmployee } from "../../_models/pay-schedule-setup/payScheduleSetupEmployee";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class PayScheduleSetupEmployeeService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllPayScheduleSetupEmployeeUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PayScheduleSetupEmployee>> {
        return this.http.get(this.getAllPayScheduleSetupEmployeeUrl, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<PayScheduleSetupEmployee> {
        let pagedData = new PagedData<PayScheduleSetupEmployee>();
        if (data) {
            var gridRecords = data;
            page.totalElements = data.length;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let payScheduleSetupEmployee = new PayScheduleSetupEmployee(
                        jsonObj.employeeIndexId,
                        jsonObj.employeeId,
                        jsonObj.employeeLastName,
                        jsonObj.employeeFirstName,
                        jsonObj.employeeMiddleName
                    );
                    pagedData.data.push(payScheduleSetupEmployee);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}