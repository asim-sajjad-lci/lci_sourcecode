/**
 * A service class for PayScheduleSetup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { PayScheduleSetup } from "../../_models/pay-schedule-setup/payScheduleSetup";
import { Constants } from '../../../_sharedresource/Constants';
import { debounce } from 'rxjs/operators/debounce';


@Injectable()
export class PayScheduleSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllPayScheduleSetupUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetup/getAll';
    private getByIdPayScheduleSetupUrl = Constants.hcmModuleApiBaseUrl +'PayScheduleSetup/getById';
    private createPayScheduleSetupUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetup/create';
    private updatePayScheduleSetupUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetup/update';
    private deletePayScheduleSetupUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetup/delete';
    private checkPayScheduleSetupUrl = Constants.hcmModuleApiBaseUrl +  'PayScheduleSetup/paycodeIdcheck';
    private searchPayScheduleSetupUrl = Constants.hcmModuleApiBaseUrl +'PayScheduleSetup/search';
    private payscheduleIdUrl = Constants.hcmModuleApiBaseUrl +'PayScheduleSetup/searchPayScheduleId';
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new PayScheduleSetup
    createPayScheduleSetup(payScheduleSetup: PayScheduleSetup) {
        return this.http.post(this.createPayScheduleSetupUrl, JSON.stringify(payScheduleSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    checkDuplicatePayScheduleSetupId(PayScheduleId: any) {
        return this.http.post(this.checkPayScheduleSetupUrl, { 'payScheduleId': PayScheduleId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getPayScheduleSetupById(payload) {
        return this.http.post(this.getByIdPayScheduleSetupUrl, payload, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit PayScheduleSetup
    updatePayScheduleSetup(PayScheduleSetup: PayScheduleSetup) {
        //debugger;
        return this.http.post(this.updatePayScheduleSetupUrl, JSON.stringify(PayScheduleSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete PayScheduleSetup
    deletePayScheduleSetup(ids: any) {
        return this.http.put(this.deletePayScheduleSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   
    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PayScheduleSetup>> {
        return this.http.post(this.getAllPayScheduleSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchPayScheduleSetuplist(page: Page, searchKeyword): Observable<PagedData<PayScheduleSetup>> {
        return this.http.post(this.searchPayScheduleSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<PayScheduleSetup> {
        let pagedData = new PagedData<PayScheduleSetup>();
        console.log(pagedData)
        //debugger
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    console.log(jsonObj);
                    let payScheduleSetup = new PayScheduleSetup(
                        jsonObj.id,
                        jsonObj.payScheduleIndexId,
                        jsonObj.payScheduleId,
                        jsonObj.payScheduleDescription,
                        jsonObj.payScheduleDescriptionArabic,
                        jsonObj.paySchedulePayPeriod,
                        jsonObj.payScheduleBeggingDate,
                        jsonObj.payScheduleYear,
                        jsonObj.subItems,
                        jsonObj.payScheduleLocation,
                        jsonObj.payScheduleSetupPositions,
                        jsonObj.payScheduleSetupDepartments,
                        jsonObj.payScheduleSetupEmployees
                    );
                    pagedData.data.push(payScheduleSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getPayScheduleId(){
        return this.http.post(this.payscheduleIdUrl, {'searchKeyword' : ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
}