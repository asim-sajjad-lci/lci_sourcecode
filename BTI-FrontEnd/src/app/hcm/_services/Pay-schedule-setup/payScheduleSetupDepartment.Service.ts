/**
 * A service class for payScheduleSetupDepartment
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { PayScheduleSetupDepartment } from "../../_models/pay-schedule-setup/payScheduleSetupDepartment";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class payScheduleSetupDepartmentService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllpayScheduleSetupDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/getAllDepartmentDropDown';
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

 
    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PayScheduleSetupDepartment>> {
        return this.http.get(this.getAllpayScheduleSetupDepartmentUrl, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }


    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<PayScheduleSetupDepartment> {
        let pagedData = new PagedData<PayScheduleSetupDepartment>();
        if (data) {
            //debugger;
            var gridRecords = data;
            page.totalElements = data.length;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let payScheduleSetupDepartment = new PayScheduleSetupDepartment(
                        jsonObj.id,
                        jsonObj.departmentId,
                        jsonObj.departmentDescription,
                        jsonObj.arabicDepartmentDescription
                    );
                    pagedData.data.push(payScheduleSetupDepartment);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}