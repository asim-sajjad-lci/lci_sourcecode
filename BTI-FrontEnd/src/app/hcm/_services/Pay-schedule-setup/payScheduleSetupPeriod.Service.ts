/**
 * A service class for PayScheduleSetupPeriod
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { PayScheduleSetupPeriod } from "../../_models/pay-schedule-setup/payScheduleSetupPeriod";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class PayScheduleSetupPeriodService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllPayScheduleSetupPeriodUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetupSchedulePeriods/getAll';
    private getByIdPayScheduleSetupPeriodUrl = Constants.hcmModuleApiBaseUrl +'PayScheduleSetupSchedulePeriods/getById';
    private createPayScheduleSetupPeriodUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetupSchedulePeriods/create';
    private updatePayScheduleSetupPeriodUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetupSchedulePeriods/update';
    private deletePayScheduleSetupPeriodUrl = Constants.hcmModuleApiBaseUrl + 'PayScheduleSetupSchedulePeriods/delete';
    private searchPayScheduleSetupPeriodUrl = Constants.hcmModuleApiBaseUrl +'PayScheduleSetupPeriod/search';
    
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new PayScheduleSetupPeriod
    createPayScheduleSetupPeriod(PayScheduleSetupPeriod: PayScheduleSetupPeriod) {
        return this.http.post(this.createPayScheduleSetupPeriodUrl, JSON.stringify(PayScheduleSetupPeriod), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit PayScheduleSetupPeriod
    updatePayScheduleSetupPeriod(PayScheduleSetupPeriod: PayScheduleSetupPeriod) {
        return this.http.post(this.updatePayScheduleSetupPeriodUrl, JSON.stringify(PayScheduleSetupPeriod), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete PayScheduleSetupPeriod
    deletePayScheduleSetupPeriod(ids: any) {
        return this.http.put(this.deletePayScheduleSetupPeriodUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   
    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PayScheduleSetupPeriod>> {
        return this.http.post(this.getAllPayScheduleSetupPeriodUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchPayScheduleSetupPeriodlist(page: Page, searchKeyword): Observable<PagedData<PayScheduleSetupPeriod>> {
        return this.http.post(this.searchPayScheduleSetupPeriodUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<PayScheduleSetupPeriod> {
        let pagedData = new PagedData<PayScheduleSetupPeriod>();
        if (data) {
            var gridRecords = data;
            page.totalElements = data.length;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let payScheduleSetupPeriod = new PayScheduleSetupPeriod(
                        jsonObj.id,
                        jsonObj.payscheduleSetupId,
                        jsonObj.periodId,
                        jsonObj.year,
                        jsonObj.periodName,
                        jsonObj.isRepeat,
                        jsonObj.periodStartDate,
                        jsonObj.periodEndDate
                    );
                    pagedData.data.push(payScheduleSetupPeriod);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}