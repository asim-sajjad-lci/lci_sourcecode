import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {RequisitionsSetup} from '../../_models/requisitions-setup/requisitions-setup.module';

@Injectable()
export class RequisitionsSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllRequisitionsSetupUrl = Constants.hcmModuleApiBaseUrl + 'requisition/getAll';
    private searchRequisitionsSetupUrl = Constants.hcmModuleApiBaseUrl + 'requisition/search';
    private createRequisitionsSetupUrl = Constants.hcmModuleApiBaseUrl + 'requisition/create';
    private getRequisitionsSetupByIdUrl = Constants.hcmModuleApiBaseUrl + 'requisition/getById';
    private updateRequisitionsSetupUrl = Constants.hcmModuleApiBaseUrl + 'requisition/update';
    private deleteRequisitionsSetupUrl = Constants.hcmModuleApiBaseUrl + 'requisition/delete';
    private checkRequisitionsSetupIdx = Constants.hcmModuleApiBaseUrl + 'requisition/requisitionsSetupIdcheck';
    private getAllCountriesUrl = Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
    private getAllStatesByCountryIdUrl = Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
    private getAllCitiesByStateIdUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
    private getAllAccrualSchedule = Constants.hcmModuleApiBaseUrl + 'accrualSchedule/getAllIds';
    private getAllLinkedPayCode = Constants.hcmModuleApiBaseUrl + 'payCode/searchPayCodeId';
    private getAllDivisionDropDownURL = Constants.hcmModuleApiBaseUrl + 'division/getAllDivisionDropDown';
    private getAllDepartmentDropDownURL = Constants.hcmModuleApiBaseUrl + 'department/getAllDepartmentDropDown';
    private getAllPositionDropDownURL = Constants.hcmModuleApiBaseUrl + 'positionSetup/getAllPostionDropDownList';
	private getAllSupervisorDropDownURL = Constants.hcmModuleApiBaseUrl + 'supervisor/getAllSupervisorDropDownList';
	
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createRequisitionsSetup(requisitionsSetup: RequisitionsSetup) {
        return this.http.post(this.createRequisitionsSetupUrl, JSON.stringify(requisitionsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateRequisitionsSetup(requisitionsSetup: RequisitionsSetup) {
        return this.http.post(this.updateRequisitionsSetupUrl, JSON.stringify(requisitionsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteRequisitionsSetup(ids: any) {
        return this.http.put(this.deleteRequisitionsSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateRequisitionsSetupId(requisitionsSetupId: any) {
        return this.http.post(this.checkRequisitionsSetupIdx, { 'requisitionsSetupId': requisitionsSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getRequisitionsSetup(requisitionsSetupId: string) {
        return this.http.post(this.getRequisitionsSetupByIdUrl, { requisitionsSetupId: requisitionsSetupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCountry() {
        return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    getAllDivisionDropDown() {
        return this.http.get(this.getAllDivisionDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    getAllDepartmentDropDown() {
        return this.http.get(this.getAllDepartmentDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    getAccrualSchedule() {
		return this.http.post(this.getAllAccrualSchedule, { "searchKeyword": "" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
       
    }

	getlinkedPayCode(){
		return this.http.post(this.getAllLinkedPayCode, { "searchKeyword": "" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}
    getStatesByCountryId(countryId: number) {
        return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCitiesByStateId(stateId: number) {
        return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getAllPositionDropDown() {
        return this.http.get(this.getAllPositionDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getAllSupervisorDropDown() {
        return this.http.get(this.getAllSupervisorDropDownURL, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getlist(page: Page, searchKeyword): Observable<PagedData<RequisitionsSetup>> {
        return this.http.post(this.getAllRequisitionsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchRequisitionsSetuplist(page: Page, searchKeyword): Observable<PagedData<RequisitionsSetup>> {
        return this.http.post(this.searchRequisitionsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<RequisitionsSetup> {
        let pagedData = new PagedData<RequisitionsSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let requisitionsSetup = new RequisitionsSetup(
                        jsonObj.id,
						jsonObj.requisitionNo,
						jsonObj.stats,
						jsonObj.internalPostDate,
						jsonObj.internalCloseDate,
						jsonObj.openingDate,
						jsonObj.recruiterName,
						jsonObj.companyId,
						jsonObj.managerName,
						jsonObj.jobPosting,
						jsonObj.advertisingField1,
						jsonObj.advertisingField2,
						jsonObj.advertisingField3,
						jsonObj.advertisingField4,
						jsonObj.positionsAvailable,
						jsonObj.positionsFilled,
						jsonObj.applicantsApplied,
						jsonObj.applicantsInterviewed,
						jsonObj.costadvirs,
						jsonObj.costrecri,
						jsonObj.departmentId,
						jsonObj.divisionId,
						jsonObj.supervisorId,
						jsonObj.positionId);
                    pagedData.data.push(requisitionsSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
