import { TestBed, inject } from '@angular/core/testing';

import { MiscellaneousBenefitsEnrollmentService } from './miscellaneous-benefits-enrollment.service';

describe('MiscellaneousBenefitsEnrollmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MiscellaneousBenefitsEnrollmentService]
    });
  });

  it('should be created', inject([MiscellaneousBenefitsEnrollmentService], (service: MiscellaneousBenefitsEnrollmentService) => {
    expect(service).toBeTruthy();
  }));
});
