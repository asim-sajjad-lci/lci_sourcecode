"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var miscellaneous_benefits_enrollment_service_1 = require("./miscellaneous-benefits-enrollment.service");
describe('MiscellaneousBenefitsEnrollmentService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [miscellaneous_benefits_enrollment_service_1.MiscellaneousBenefitsEnrollmentService]
        });
    });
    it('should be created', testing_1.inject([miscellaneous_benefits_enrollment_service_1.MiscellaneousBenefitsEnrollmentService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=miscellaneous-benefits-enrollment.service.spec.js.map