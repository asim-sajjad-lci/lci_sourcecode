"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var miscellaneous_benefits_module_1 = require("../../_models/miscellaneous-benefits/miscellaneous-benefits.module");
var MiscellaneousBenefitsService = (function () {
    //getAllBenefitId
    //initializing parameter for constructor
    function MiscellaneousBenefitsService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllMiscellaneousBenefitsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/getAll';
        this.searchMiscellaneousBenefitsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/searchMiscellaneousId';
        this.createMiscellaneousBenefitsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/create';
        this.getMiscellaneousBenefitsById = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/getMiscellaneousBenefitsId';
        this.updateMiscellaneousBenefitsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/update';
        this.deleteMiscellaneousBenefitsUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/delete';
        this.getMiscellaneousBenefitsIdcheck = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/miscellaneousBenefitsIdcheck';
        //private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAllPostionClassId';
        this.getPositionClassListUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'miscellaneousBenefits/searchMiscellaneousId';
        this.getAllEmployeeMasterDropDownList = Constants_1.Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeMasterDropDownList';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new MiscellaneousBenefits
    MiscellaneousBenefitsService.prototype.createMiscellaneousBenefits = function (miscellaneousBenefits) {
        return this.http.post(this.createMiscellaneousBenefitsUrl, JSON.stringify(miscellaneousBenefits), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    MiscellaneousBenefitsService.prototype.updateMiscellaneousBenefits = function (miscellaneousBenefits) {
        return this.http.post(this.updateMiscellaneousBenefitsUrl, JSON.stringify(miscellaneousBenefits), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    MiscellaneousBenefitsService.prototype.deleteMiscellaneousBenefits = function (ids) {
        return this.http.put(this.deleteMiscellaneousBenefitsUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //check for duplicate ID department
    MiscellaneousBenefitsService.prototype.checkDuplicateMiscellaneousBenefitsId = function (skillId) {
        return this.http.post(this.getMiscellaneousBenefitsIdcheck, { 'benefitsId': skillId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    MiscellaneousBenefitsService.prototype.getMiscellaneousBenefits = function (skillId) {
        return this.http.post(this.getMiscellaneousBenefitsById, { 'benefitsId': skillId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    MiscellaneousBenefitsService.prototype.getPositionClassList = function () {
        return this.http.post(this.getPositionClassListUrl, {
            "searchKeyword": "",
            "pageNumber": 0,
            "pageSize": 5,
            "sortOn": "id",
            "sortBy": "DESC"
        }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    MiscellaneousBenefitsService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.getAllMiscellaneousBenefitsUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    MiscellaneousBenefitsService.prototype.searchMiscellaneousBenefitslist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchMiscellaneousBenefitsUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //pagination for data
    MiscellaneousBenefitsService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var miscellaneousBenefits = new miscellaneous_benefits_module_1.MiscellaneousBenefits(jsonObj.id, jsonObj.benefitsId, jsonObj.desc, jsonObj.arbicDesc, jsonObj.frequency, jsonObj.startDate, jsonObj.endDate, jsonObj.inactive, jsonObj.method, jsonObj.dudctionAmount, jsonObj.dudctionPercent, jsonObj.monthlyAmount, jsonObj.yearlyAmount, jsonObj.lifetimeAmount, jsonObj.empluyeeerinactive, jsonObj.empluyeeermethod, jsonObj.benefitAmount, jsonObj.benefitPercent, jsonObj.employermonthlyAmount, jsonObj.employeryearlyAmount, jsonObj.employerlifetimeAmount);
                    pagedData.data.push(miscellaneousBenefits);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    MiscellaneousBenefitsService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    MiscellaneousBenefitsService.prototype.getAllEmployeeMaster = function () {
        return this.http.get(this.getAllEmployeeMasterDropDownList, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    return MiscellaneousBenefitsService;
}());
MiscellaneousBenefitsService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], MiscellaneousBenefitsService);
exports.MiscellaneousBenefitsService = MiscellaneousBenefitsService;
//# sourceMappingURL=miscellaneous-benefits-enrollment.service.js.map