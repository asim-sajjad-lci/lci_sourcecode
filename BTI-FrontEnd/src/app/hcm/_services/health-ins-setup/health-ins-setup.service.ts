import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { HealthInsSetup } from '../../_models/health-ins-setup/health-ins-setup.module';
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class HealthInsSetupService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllHealthInsUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/search';
  private createHealthInsUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/create';
  private updateHealthInsUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/update';
  private deleteHealthInsUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/delete';
  private getAllHealthCovType = Constants.hcmModuleApiBaseUrl + 'helthCoverageType/getAllHealthCoverageType';
  private checkDuplicateHealthInsUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/helthInsurancecheck'
  private getByIdHealthInsUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/getById'

  private getAllcompInsurancesUrl = Constants.hcmModuleApiBaseUrl + 'helthInsurance/getAllCompanyInsurance';
  private searchHealthInsIdUrl = Constants.hcmModuleApiBaseUrl + 'HelthInsuranceSetUp/searchHelthInsuranceId'

  
  // initializing parameter for constructor
  constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }

  // Search Health Ins ID:
  
  getHealthIdList() {
    return this.http.post(this.searchHealthInsIdUrl, { "searchKeyword" : ""}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}
  //add new Health Insurance
  createHealthIns(healthInsSetup: HealthInsSetup) {
      return this.http.post(this.createHealthInsUrl, JSON.stringify(healthInsSetup), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

  // check Duplicate id

  checkDuplicatehealthIns(helthInsuranceId: any){
      return this.http.post(this.checkDuplicateHealthInsUrl, { 'helthInsuranceId': helthInsuranceId}, { headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);

  }

  getHealthInsById(helthInsuranceId: any){
      return this.http.post(this.getByIdHealthInsUrl, { id: helthInsuranceId}, { headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);

  }

  //update for edit department
  updateHealthIns(healthInsSetup: HealthInsSetup) {
      return this.http.post(this.updateHealthInsUrl, JSON.stringify(healthInsSetup), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

  //delete department
  deleteHealthIns(ids: any) {
      return this.http.put(this.deleteHealthInsUrl, { 'ids': ids }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

 
  //get list



  getcompInsurances(){
        return this.http.get(this.getAllcompInsurancesUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    getHealthCovType(){
        return this.http.get(this.getAllHealthCovType, { headers: this.headers})
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);

    }

    
  
  getlist(page: Page, searchKeyword): Observable<PagedData<HealthInsSetup>> {
      return this.http.post(this.getAllHealthInsUrl, {
          'searchKeyword': searchKeyword,
          'pageNumber': page.pageNumber,
          'pageSize': page.size,
          'sortOn': page.sortOn,
          'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

  //get list by search keyword
    //   searchHealthInslist(page: Page, searchKeyword): Observable<PagedData<HealthInsSetup>> {
    //       return this.http.post(this.getAllHealthInsUrl, {
    //           'searchKeyword': searchKeyword,
    //           'pageNumber': page.pageNumber,
    //           'pageSize': page.size
    //         }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    //     }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<HealthInsSetup> {
      let pagedData = new PagedData<HealthInsSetup>();
      if (data) {
          var gridRecords = data.records;
          page.totalElements = data.totalCount;
          if (gridRecords && gridRecords.length > 0) {
              for (let i = 0; i < gridRecords.length; i++) {
                  let jsonObj = gridRecords[i];
                  let healthIns = new HealthInsSetup(
                      jsonObj.id,
                      jsonObj.helthInsuranceId,
                      jsonObj.desc,
                      jsonObj.arbicDesc,
                      jsonObj.frequency,
                      jsonObj.helthCoverageTypeId,
                      jsonObj.helthCoverageDesc,
                      jsonObj.compnayInsuranceId,
                      jsonObj.groupNumber,
                      jsonObj.maxAge,
                      jsonObj.minAge,
                      jsonObj.employeePay,
                      jsonObj.employerPay,
                      jsonObj.expenses,
                      jsonObj.maxCoverage,
                      jsonObj.helthInsurancePrimaryId,
                      jsonObj.compnayInsurancePrimaryId);
                    pagedData.data.push(healthIns);
                }
            }
        }
      page.totalPages = page.totalElements / page.size;
      let start = page.pageNumber * page.size;
      let end = Math.min((start + page.size), page.totalElements);
      pagedData.page = page;
      return pagedData;
    }

  //error handler
  private handleError(error: any): Promise<any> {
      return Promise.reject(error.message || error);
    }
}