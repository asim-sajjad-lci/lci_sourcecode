import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { SkillsSetup } from '../../_models/skills-setup/skills-setup.module';

@Injectable()
export class SkillsSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getAll';
    private searchSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/searchSkillSteup';
    private createSkillSteupUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/createSkillSteup';
    private getSkillSteupById = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/getSkillSteupById';
    private updateSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/update';
    private deleteSkillsSetupUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/delete';
    private getSkillsSetupIdcheck = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/skillSteupIdcheck';
    private skillIdUrl = Constants.hcmModuleApiBaseUrl + 'hrSkillSteup/searchAllSkillId';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new skillsSetup
    createSkillSteup(skillsSetup: SkillsSetup) {
        return this.http.post(this.createSkillSteupUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateSkillsSetup(skillsSetup: SkillsSetup) {
        return this.http.post(this.updateSkillsSetupUrl, JSON.stringify(skillsSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteSkillsSetup(ids: any) {
        return this.http.put(this.deleteSkillsSetupUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateSkillsSetupId(skillId: any) {
        return this.http.post(this.getSkillsSetupIdcheck, { 'skillId': skillId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getSkillsSetup(skillId: string) {
        return this.http.post(this.getSkillSteupById, { id: skillId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<SkillsSetup>> {
        return this.http.post(this.getAllSkillsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchSkillsSetuplist(page: Page, searchKeyword): Observable<PagedData<SkillsSetup>> {
        return this.http.post(this.searchSkillsSetupUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<SkillsSetup> {
        let pagedData = new PagedData<SkillsSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let skillsSetup = new SkillsSetup(
                        jsonObj.id,
                        jsonObj.skillId,
                        jsonObj.skillDesc,
                        jsonObj.arabicDesc,
                        jsonObj.compensation,
                        jsonObj.frequency,
						jsonObj.frequencyName);
                    pagedData.data.push(skillsSetup);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getSkillId(){
        return this.http.post(this.skillIdUrl, {'searchKeyword' : ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

}
