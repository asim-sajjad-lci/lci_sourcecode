import { TestBed, inject } from '@angular/core/testing';

import { LoanPackageSetupService } from './loan-package-setup.service';

describe('LoanPackageSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoanPackageSetupService]
    });
  });

  it('should be created', inject([LoanPackageSetupService], (service: LoanPackageSetupService) => {
    expect(service).toBeTruthy();
  }));
});
