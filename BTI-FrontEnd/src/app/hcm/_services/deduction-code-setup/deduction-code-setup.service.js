"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Constants_1 = require("../../../_sharedresource/Constants");
var http_1 = require("@angular/http");
var paged_data_1 = require("../../../_sharedresource/paged-data");
var deduction_code_setup_module_1 = require("../../_models/deduction-code-setup/deduction-code-setup.module");
var DeductionCodeSetupService = (function () {
    //initializing parameter for constructor
    function DeductionCodeSetupService(http) {
        this.http = http;
        this.headers = new http_1.Headers({ 'content-type': 'application/json' });
        this.getAllDeductionCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'deductionCode/getAll';
        this.searchDeductionCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'deductionCode/search';
        this.createDeductionCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'deductionCode/create';
        this.getDeductionCodeByIdUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'deductionCode/getDeductionCodeById';
        this.updateDeductionCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'deductionCode/update';
        this.deleteDeductionCodeUrl = Constants_1.Constants.hcmModuleApiBaseUrl + 'deductionCode/delete';
        this.checkDeductionCodeIdx = Constants_1.Constants.hcmModuleApiBaseUrl + 'deductionCode/deductionIdcheck';
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers);
    }
    //add new Deduction Code
    DeductionCodeSetupService.prototype.createDeductionCode = function (deductionCodeSetup) {
        console.log(JSON.stringify(deductionCodeSetup));
        return this.http.post(this.createDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //update for edit department
    DeductionCodeSetupService.prototype.updateDeductionCode = function (deductionCodeSetup) {
        return this.http.post(this.updateDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //delete department
    DeductionCodeSetupService.prototype.deleteDeductionCode = function (ids) {
        return this.http.put(this.deleteDeductionCodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get department detail by Id
    DeductionCodeSetupService.prototype.getDeductionCode = function (deductionCodeId) {
        return this.http.post(this.getDeductionCodeByIdUrl, { deductionCodeId: deductionCodeId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //get list
    DeductionCodeSetupService.prototype.getlist = function (page, searchKeyword) {
        var _this = this;
        return this.http.put(this.getAllDeductionCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    //get list by search keyword
    DeductionCodeSetupService.prototype.searchDeductionCodelist = function (page, searchKeyword) {
        var _this = this;
        return this.http.post(this.searchDeductionCodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(function (data) { return _this.getPagedData(page, data.json().result); });
    };
    DeductionCodeSetupService.prototype.checkDuplicateDeductionCodeId = function (deductionCodeId) {
        return this.http.post(this.checkDeductionCodeIdx, { 'diductionId': deductionCodeId }, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    //pagination for data
    DeductionCodeSetupService.prototype.getPagedData = function (page, data) {
        var pagedData = new paged_data_1.PagedData();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (var i = 0; i < gridRecords.length; i++) {
                    var jsonObj = gridRecords[i];
                    var deductionCode = new deduction_code_setup_module_1.DeductionCodeSetup(jsonObj.id, jsonObj.diductionId, jsonObj.discription, jsonObj.arbicDiscription, jsonObj.startDate, jsonObj.endDate, jsonObj.frequency, jsonObj.method, jsonObj.amount, jsonObj.transction, jsonObj.inActive, jsonObj.perPeriod, jsonObj.perYear, jsonObj.lifeTime);
                    pagedData.data.push(deductionCode);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        var start = page.pageNumber * page.size;
        var end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    };
    //error handler
    DeductionCodeSetupService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return DeductionCodeSetupService;
}());
DeductionCodeSetupService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], DeductionCodeSetupService);
exports.DeductionCodeSetupService = DeductionCodeSetupService;
//# sourceMappingURL=deduction-code-setup.service.js.map