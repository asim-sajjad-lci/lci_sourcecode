import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { BuildCheckPayrollCodes } from '../../_models/build-check-payroll-codes/build-check-payroll-codes.module';

@Injectable()
export class BuildPayrollCheckDeductionsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'buildPayrollCheckByDeductions/getAllByDates';
    private createDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'buildPayrollCheckByDeductions/create';
    private deleteDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'buildChecks/changeFlagDeduction';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new Deduction Code
    createDeductionCode(buildChecks:number,BuildCheckPayrollCodes: Array<BuildCheckPayrollCodes>) {
        console.log(JSON.stringify(BuildCheckPayrollCodes));
        return this.http.post(this.createDeductionCodeUrl,{
            "listBuildChecks":[{
                "buildChecks":  {
                    "id":   buildChecks
                }
            }],
            "deductionCode":    BuildCheckPayrollCodes
        } , { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(id: number, startDate:any, endDate:any): Observable<Array<BuildCheckPayrollCodes>> {
        return this.http.post(this.getAllDeductionCodeUrl, {
            'id': id,
            'startDate': startDate,
            'endDate': endDate
            /* 'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy */
        }, { headers: this.headers }).map(data => this.getData(data.json()));
    }
    //Delete API list
    deleteTransDeduction(payload: any): Observable<any> {
        return this.http.post(this.deleteDeductionCodeUrl, payload, { headers: this.headers }).map(data => data.json());
    }

    //pagination for data
    private getData(data: any): Array<BuildCheckPayrollCodes> {
        let res = new Array<BuildCheckPayrollCodes>();
        if (data) {
            var gridRecords = data.result.records;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let deductionCode = new BuildCheckPayrollCodes(
                        jsonObj.deductionCodeParimaryId, 
                        jsonObj.diductionId,
                        jsonObj.buildChecksId
                    );
                    res.push(deductionCode);
                }
            }
        }
        return res;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
