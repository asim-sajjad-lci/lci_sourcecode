import { TestBed, inject } from '@angular/core/testing';

import { ProjectSetupService } from './project-setup.service';

describe('ProjectSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProjectSetupService]
    });
  });

  it('should be created', inject([ProjectSetupService], (service: ProjectSetupService) => {
    expect(service).toBeTruthy();
  }));
});
