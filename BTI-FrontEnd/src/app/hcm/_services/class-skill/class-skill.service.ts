/**
 * A service class for controllerTraningCourseClassSkill
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { ClassSkill } from "../../_models/class-skill/class-skill";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class ClassSkillService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllClassSkillUrl = Constants.hcmModuleApiBaseUrl + 'controllerTraningCourseClassSkill/getAll';
    private searchClassSkillUrl = Constants.hcmModuleApiBaseUrl + 'controllerTraningCourseClassSkill/searchClassSkill';
    private createClassSkillUrl = Constants.hcmModuleApiBaseUrl + 'controllerTraningCourseClassSkill/create';
    private getClassSkillByClassSkillIdUrl = Constants.hcmModuleApiBaseUrl + 'controllerTraningCourseClassSkill/getClassSkillDetailByClassSkillId';
    private updateClassSkillUrl = Constants.hcmModuleApiBaseUrl + 'controllerTraningCourseClassSkill/update';
    private deleteClassSkillUrl = Constants.hcmModuleApiBaseUrl + 'controllerTraningCourseClassSkill/delete';
    private skillSetSteupgetByIdURL = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/getById';

    //Training Course Class Index ID droupdown
    private getTrainingCourseAllIds = Constants.hcmModuleApiBaseUrl + 'trainingCourseDetail/getAllIds';
    //Training Course Detail Index ID droupdown
    private getAllTraningCourseId = Constants.hcmModuleApiBaseUrl + 'traningCourse/getAllTraningCourseId';
    //Employee Index ID droupdown
	private getskillSetSteupDropDownList = Constants.hcmModuleApiBaseUrl + 'skillSetSteup/getIds';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new controllerTraningCourseClassSkill
    createClassSkill(controllerTraningCourseClassSkill: ClassSkill) {
        return this.http.post(this.createClassSkillUrl, JSON.stringify(controllerTraningCourseClassSkill), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit controllerTraningCourseClassSkill
    updateClassSkill(controllerTraningCourseClassSkill: ClassSkill) {
        return this.http.post(this.updateClassSkillUrl, JSON.stringify(controllerTraningCourseClassSkill), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete controllerTraningCourseClassSkill
    deleteClassSkill(ids: any) {
        return this.http.put(this.deleteClassSkillUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getTrainingCourse(page: Page, searchKeyword){
        return this.http.post(this.getTrainingCourseAllIds, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
    }
    getALLClasses(page: Page, searchKeyword){
    return this.http.post(this.getAllClassSkillUrl, {
        'searchKeyword': searchKeyword,
        'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }
    getSkillSetSteupgetById(page:Page,id){
      
        return this.http.post(this.skillSetSteupgetByIdURL,{'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy, 'id': id}, { headers: this.headers }).map(data =>  data.json().result);
    }
	getAllTraningCourse(page: Page, searchKeyword){
		 
        return this.http.post(this.getAllTraningCourseId, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}
	getAllSkillSetSetup(searchKeyword){
		 return this.http.post(this.getskillSetSteupDropDownList,{searchKeyword:searchKeyword}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}

    

    //get controllerTraningCourseClassSkill detail by Id
    getClassSkill(controllerTraningCourseClassSkillId: string) {
        return this.http.post(this.getClassSkillByClassSkillIdUrl, { controllerTraningCourseClassSkillId: controllerTraningCourseClassSkillId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<ClassSkill>> {
        return this.http.post(this.getAllClassSkillUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchClassSkilllist(page: Page, searchKeyword): Observable<PagedData<ClassSkill>> {
        return this.http.post(this.searchClassSkillUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<ClassSkill> {
        let pagedData = new PagedData<ClassSkill>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    // let controllerTraningCourseClassSkill = new ClassSkill(
                    //     jsonObj.id,
                    //     jsonObj.traningId,
                    //     jsonObj.desc,
                    //     jsonObj.arbicDesc,
                    //     jsonObj.location,
                    //     jsonObj.prerequisiteId,
                    //     jsonObj.courseInCompany,
                    //     jsonObj.employeeCost,
                    //     jsonObj.employerCost,
                    //     jsonObj.supplierCost,
                    //     jsonObj.instructorCost,
                    // );
                    pagedData.data.push(jsonObj);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}