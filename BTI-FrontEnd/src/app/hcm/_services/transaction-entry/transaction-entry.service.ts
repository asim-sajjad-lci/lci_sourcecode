/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { TransactionEntryModule,SaveDataEntryModule } from "../../_models/transaction-entry/transaction-entry";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class TransactionEntryService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'batches/getAllForTransectionEntry';
    private searchDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/searchDepartment';
    private createDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/create';
    private getDepartmentByDepartmentIdUrl = Constants.hcmModuleApiBaseUrl + 'department/getDepartmentDetailByDepartmentId';
    private updateDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/update';
    private deleteDepartmentUrl = Constants.hcmModuleApiBaseUrl + 'department/delete';
    private checkDepartmentIdx = Constants.hcmModuleApiBaseUrl + 'department/departmentIdcheck';
    private getDepatmentId = Constants.hcmModuleApiBaseUrl + 'department/getAllDepartmentDropDown';
    private getAllBatchDropDownUrl = Constants.hcmModuleApiBaseUrl + 'batches/getAllBatcheDropDown';
    private getTransactionEntryByBatchIdUrl = Constants.hcmModuleApiBaseUrl + 'transactionEntry/searchByBatchId';
    private payCodeIdUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPayCodeId';
    private deductionCodeIdUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/searchdiductionId';
    private benefitCodeIdUrl = Constants.hcmModuleApiBaseUrl + 'benefitCode/searchAllBenefitId';
    private employeeIdUrl = Constants.hcmModuleApiBaseUrl + 'employeeMaster/getAllEmployeeId';
    private createTransactionEntryUrl = Constants.hcmModuleApiBaseUrl + 'transactionEntry/create';
    private deleteTransactionEntryUrl = Constants.hcmModuleApiBaseUrl + 'transactionEntry/deleteBatchRow';
    private deleteTransactionEntryBatchUrl = Constants.hcmModuleApiBaseUrl + 'transactionEntry/deleteBatch';
    private getProjectIdUrl = Constants.hcmModuleApiBaseUrl + 'payrollSetup/getAllDroupdownDimension';
    private getCodeDataByTransactionTypeUrl = Constants.hcmModuleApiBaseUrl + 'employeePayCodeMaintenance/findCodeByEmployeeId';

    //SMT
    private getAllCodeDataOfTransactionTypeUrl = Constants.hcmModuleApiBaseUrl + 'employeeByTransactionType/search';
    private employeeIdUrlByPayCode = Constants.hcmModuleApiBaseUrl + 'employeeByTransactionType/findByPayCode';
    private employeeIdUrlByDeduction = Constants.hcmModuleApiBaseUrl + 'employeeByTransactionType/findByDeductionCode';
    private employeeIdUrlByBenefit = Constants.hcmModuleApiBaseUrl + 'employeeByTransactionType/findByBenefitCode';

    private createNewTransactionEntryUrl = Constants.hcmModuleApiBaseUrl + 'newTransactionEntry/create';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new department
    createTransaction(transaction: TransactionEntryModule) {
        return this.http.post(this.createTransactionEntryUrl, JSON.stringify(transaction), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //add new department
     createNewTransaction(transaction: SaveDataEntryModule[]) {
        return this.http.post(this.createNewTransactionEntryUrl, JSON.stringify(transaction), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updateDepartment(department: TransactionEntryModule) {
        return this.http.post(this.updateDepartmentUrl, JSON.stringify(department), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deleteTransactionEntryByrow(ids: any) {
        return this.http.put(this.deleteTransactionEntryUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    deleteTransactionEntryBatch(ids: any) {
        return this.http.put(this.deleteTransactionEntryBatchUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getDepartments(){
		 return this.http.get(this.getDepatmentId, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
    }
    
    getCodeDataByTransactionType(id: any,type:number){
        return this.http.post(this.getCodeDataByTransactionTypeUrl, { 'id': id,'type':type}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAllCodeDataOfTransactionType(type:number,startDate,endDate){
        return this.http.post(this.getAllCodeDataOfTransactionTypeUrl, {'type':type,"startDate": startDate,"endDate": endDate}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicateDeptId(departmentId: any) {
        return this.http.post(this.checkDepartmentIdx, { 'departmentId': departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getDepartment(departmentId: string) {
        return this.http.post(this.getDepartmentByDepartmentIdUrl, { departmentId: departmentId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): any {
        return this.http.post(this.getAllDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //get list by search keyword
    searchDepartmentlist(page: Page, searchKeyword): Observable<PagedData<TransactionEntryModule>> {
        return this.http.post(this.searchDepartmentUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    getAllBatch(searchKeyword){
        return this.http.post(this.getAllBatchDropDownUrl, {
            'searchKeyword': searchKeyword,
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    getTransctionEntryByBatch(page: Page,id){
       
        return this.http.post(this.getTransactionEntryByBatchIdUrl, {
            'searchKeyword': page.searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            'id':id,
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    getPayCodeId(page: Page){
        return this.http.post(this.payCodeIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    getProjectId(page: Page){
        return this.http.post(this.getProjectIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }



    getDeductionCodeId(page: Page){
        return this.http.post(this.deductionCodeIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }


    getBenefitCodeId(page: Page){
        return this.http.post(this.benefitCodeIdUrl, {
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    getEmployeeId(page: Page){
        return this.http.post(this.employeeIdUrl, {
            'all':true,
            'searchKeyword': '',
            'pageNumber': page.pageNumber,
            'pageSize': 5000000,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            
           
        }, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    getEmployeeIdByPayCode(id:number){
        return this.http.post(this.employeeIdUrlByPayCode, {"id":id}, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    getEmployeeIdByDeduction(id:number,startDate:string,endDate:string){
        return this.http.post(this.employeeIdUrlByDeduction, {"id":id ,"startDate": startDate,"endDate": endDate}, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }


    getEmployeeIdByBenefit(id:number,startDate:string,endDate:string){
        return this.http.post(this.employeeIdUrlByBenefit, {"id":id ,"startDate": startDate,"endDate": endDate}, { headers: this.headers }).toPromise()
        .then(res => res.json())
        .catch(this.handleError);  
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<TransactionEntryModule> {
        let pagedData = new PagedData<TransactionEntryModule>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let transEntry = new TransactionEntryModule(
                        jsonObj.id,
                        jsonObj.batches,
                        jsonObj.dimensions,
                        jsonObj.entryNumber,
                        jsonObj.entryDate,
                        jsonObj.description,
                        jsonObj.arabicDescription,
                        
                        jsonObj.transactionEntryDetail,
                        jsonObj.fromDate,
                        jsonObj.toDate
                        
                    );
                    pagedData.data.push(transEntry);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    private getPagedData1(page: Page, data: any): PagedData<Object> {
        let pagedData = new PagedData<Object>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let transEntry = {
                        "transcationPrimaryId" : jsonObj.transcationPrimaryId,
                        "transcationNumber" : jsonObj.transcationNumber,
                        "employeeId" : jsonObj.employeeId,
                        "employeeName" : jsonObj.employeeName,
                        "trxType" : jsonObj.trxType,
                        "amount" : jsonObj.amount,
                        "payRate" : jsonObj.payRate,
                        "status" : jsonObj.status,
                        "projectId" : jsonObj.projectId,
                        "codeId" : jsonObj.codeId,
                        "departmentId" : jsonObj.departmentId
                    };
                    pagedData.data.push(transEntry);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}