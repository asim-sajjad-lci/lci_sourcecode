/**
 * A service class for traningCourse
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { TrainingCourse } from "../../_models/training-course/training-course";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class TrainingCourseService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllTrainingCourseUrl = Constants.hcmModuleApiBaseUrl + 'traningCourse/getAll';
    private searchTrainingCourseUrl = Constants.hcmModuleApiBaseUrl + 'traningCourse/searchTrainingCourse';
    private createTrainingCourseUrl = Constants.hcmModuleApiBaseUrl + 'traningCourse/create';
    private getTrainingCourseByTrainingCourseIdUrl = Constants.hcmModuleApiBaseUrl + 'traningCourse/getTraningCourseById1';
    private updateTrainingCourseUrl = Constants.hcmModuleApiBaseUrl + 'traningCourse/update';
    private deleteTrainingCourseUrl = Constants.hcmModuleApiBaseUrl + 'traningCourse/delete';
    private checkTrainingCourseIdx = Constants.hcmModuleApiBaseUrl + 'traningCourse/traningIdcheck';
	private getDepatmentId = Constants.hcmModuleApiBaseUrl + 'traningCourse/getAllTrainingCourseDropDown';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new traningCourse
    createTrainingCourse(traningCourse: TrainingCourse) {
        return this.http.post(this.createTrainingCourseUrl, JSON.stringify(traningCourse), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit traningCourse
    updateTrainingCourse(traningCourse: TrainingCourse) {
        return this.http.post(this.updateTrainingCourseUrl, JSON.stringify(traningCourse), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete traningCourse
    deleteTrainingCourse(ids: any) {
        return this.http.put(this.deleteTrainingCourseUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getTrainingCourses(){
		 return this.http.get(this.getDepatmentId, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}

    //check for duplicate ID traningCourse
    checkDuplicateDeptId(traningCourseId: any) {
        return this.http.post(this.checkTrainingCourseIdx, { 'traningId': traningCourseId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get traningCourse detail by Id
    getTrainingCourse(traningCourseId: string) {
        return this.http.post(this.getTrainingCourseByTrainingCourseIdUrl, { id: traningCourseId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<TrainingCourse>> {
        return this.http.post(this.getAllTrainingCourseUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchTrainingCourselist(page: Page, searchKeyword): Observable<PagedData<TrainingCourse>> {
        return this.http.post(this.searchTrainingCourseUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<TrainingCourse> {
        let pagedData = new PagedData<TrainingCourse>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let traningCourse = new TrainingCourse(
                        jsonObj.id,
                        jsonObj.traningId,
                        jsonObj.desc,
                        jsonObj.arbicDesc,
                        jsonObj.location,
                        jsonObj.prerequisiteId,
                        jsonObj.courseInCompany,
                        jsonObj.employeeCost,
                        jsonObj.employerCost,
                        jsonObj.supplierCost,
                        jsonObj.instructorCost,
                        jsonObj.subItems
                    );
                    pagedData.data.push(traningCourse);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}