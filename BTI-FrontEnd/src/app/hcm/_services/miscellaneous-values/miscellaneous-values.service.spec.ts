import { TestBed, inject } from '@angular/core/testing';

import { MiscellaneousValuesService } from './miscellaneous-values.service';

describe('MiscellaneousValuesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MiscellaneousValuesService]
    });
  });

  it('should be created', inject([MiscellaneousValuesService], (service: MiscellaneousValuesService) => {
    expect(service).toBeTruthy();
  }));
});
