import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { MiscellaneousValuesSetup } from '../../_models/miscellaneous-values/miscellaneous-values';
import { Page } from '../../../_sharedresource/page';
import { Observable } from 'rxjs';
import { PagedData } from '../../../_sharedresource/paged-data';

@Injectable()
export class MiscellaneousValuesService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private createMiscllaneousUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/values/create';
  private getByMisclaneousIdx = Constants.hcmModuleApiBaseUrl + 'miscellaneous/values/getByMiscellaneousId';
  private deleteMiscellaneousUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/values/delete';
  private checkMisclaneousIdx = Constants.hcmModuleApiBaseUrl + 'miscellaneous/values/valuesIdCheck';
  private updateMiscllaneousUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/values/update';

  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
   }

   //add new Miscellaneous Values
  createMiscellaneous(miscellaneousValues: MiscellaneousValuesSetup) {
    return this.http.post(this.createMiscllaneousUrl, JSON.stringify(miscellaneousValues), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

    // //gwt Values by Miscellaneous ID
    // getValuesByMiscId(page: Page,miscId: any) {
    //   return this.http.post(this.getByMisclaneousIdx, {page: Page, 'miscId': miscId }, { headers: this.headers })
    //     .toPromise()
    //     .then(res => res.json())
    //     .catch(this.handleError);
    // }

    //get Values by Miscellaneous ID
    getValuesByMiscId(page: Page,miscId: any): Observable<PagedData<MiscellaneousValuesSetup>> {
    return this.http.post(this.getByMisclaneousIdx, {
      "miscId":miscId,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<MiscellaneousValuesSetup> {
    debugger;
    let pagedData = new PagedData<MiscellaneousValuesSetup>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let miscellaneous = new MiscellaneousValuesSetup(
            jsonObj.id,
            jsonObj.valueId,
            jsonObj.valueDescription,
            jsonObj.valueArabicDescription,
            jsonObj.miscId,
            jsonObj.codeName
          );
          pagedData.data.push(miscellaneous);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }
  
 //delete Miscellaneous
 deleteMiscellaneous(ids: any) {
  return this.http.post(this.deleteMiscellaneousUrl, { 'ids': ids }, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
}

 //check for duplicate ID Miscellaneous
 checkDuplicateMiscId(valueId: any) {
  return this.http.post(this.checkMisclaneousIdx, { 'valueId': valueId }, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
}

 //update for edit Miscellaneous
 updateMiscellaneous(miscellaneous: MiscellaneousValuesSetup) {
  return this.http.post(this.updateMiscllaneousUrl, JSON.stringify(miscellaneous), { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError);
}

  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
