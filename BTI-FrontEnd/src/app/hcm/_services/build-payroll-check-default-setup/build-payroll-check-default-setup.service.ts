/**
 * A service class for department
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';

import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { BuildPayrollCheckDefault } from "../../_models/build-payroll-chek-default-setup/build-payroll-chek-default-setup";
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class BuildPayRollService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllDefaultUrl = Constants.hcmModuleApiBaseUrl + 'default/getAll';
    private createDefaultIdUrl = Constants.hcmModuleApiBaseUrl + 'default/create';
    private updateDefaultUrl = Constants.hcmModuleApiBaseUrl + 'default/update';
    private deleteDefaultUrl = Constants.hcmModuleApiBaseUrl + 'default/delete';
    private checkDefaultIdx = Constants.hcmModuleApiBaseUrl + 'default/defaultIdcheck';
	private getDefaultId = Constants.hcmModuleApiBaseUrl + 'default/getAllDefaultDropDown ';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new default
    createDefaultId(defaultId: BuildPayrollCheckDefault) {
        return this.http.post(this.createDefaultIdUrl, JSON.stringify(defaultId), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit default
    updateDefault(defaultId: BuildPayrollCheckDefault) {
        return this.http.post(this.updateDefaultUrl, JSON.stringify(defaultId), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete default
    deleteDefault(ids: any) {
        return this.http.put(this.deleteDefaultUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getDefaultIdList(){
		 return this.http.get(this.getDefaultId, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	
	}

    //check for duplicate ID default
    checkDuplicateDefaultId(defaultID: any) {
        return this.http.post(this.checkDefaultIdx, { 'defaultID': defaultID }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<BuildPayrollCheckDefault>> {
        return this.http.post(this.getAllDefaultUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

   

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<BuildPayrollCheckDefault> {
        let pagedData = new PagedData<BuildPayrollCheckDefault>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let department = new BuildPayrollCheckDefault(
                        jsonObj.id,
                        jsonObj.defaultID,
                        jsonObj.desc,
                        jsonObj.arabicDesc
                    );
                    pagedData.data.push(department);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}