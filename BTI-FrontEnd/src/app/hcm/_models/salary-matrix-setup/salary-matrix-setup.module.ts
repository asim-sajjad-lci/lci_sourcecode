import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SalaryMatrixRowModule} from './salary-matrix-row.module';
import {SalaryMatrixColumnModule} from './salary-matrix-column.module';
import {SalaryMatrixRowValueModule} from './salary-matrix-row-value.module';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SalaryMatrixSetupModule { 
  id:number;
  salaryMatrixId:string;
  description: string;
  arabicSalaryMatrixDescription: string;
  payUnit: string;
  totalRow: number;
  totalRowValues: number;
  totalColumns: number;
  listSalaryMatrixRow: Array<SalaryMatrixRowModule>;
  listSalaryMatrixColSetup:Array<SalaryMatrixColumnModule>;

  constructor(id:number, salaryMatrixId:string, description: string,
    arabicSalaryMatrixDescription: string, payUnit: string,
    totalRow: number, totalRowValues: number,
    totalColumns: number,listSalaryMatrixRow:Array<SalaryMatrixRowModule>,listSalaryMatrixColSetup:Array<SalaryMatrixColumnModule>) {
    
    this.id=id;
    this.salaryMatrixId=salaryMatrixId;
    this.description=description;
    this.arabicSalaryMatrixDescription=arabicSalaryMatrixDescription;
    this.payUnit=payUnit;
    this.totalRow=totalRow;
    this.totalColumns=totalColumns;     
    this.totalRowValues=totalRowValues;
    this.listSalaryMatrixRow=listSalaryMatrixRow;
    this.listSalaryMatrixColSetup=listSalaryMatrixColSetup;

  }
}
