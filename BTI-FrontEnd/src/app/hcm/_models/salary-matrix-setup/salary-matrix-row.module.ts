import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SalaryMatrixRowValueModule} from './salary-matrix-row-value.module';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: []
})
export class SalaryMatrixRowModule {
    id:number;
    desc: string;
    arbic: string;
    listSalaryMatrixRowValue:Array<SalaryMatrixRowValueModule>;

    constructor(desc: string, arbic: string,listSalaryMatrixRowValue:Array<SalaryMatrixRowValueModule>) {
        this.desc = desc;
        this.arbic = arbic;
        this.listSalaryMatrixRowValue=listSalaryMatrixRowValue;
    }

    delete(indx: number){
        this.listSalaryMatrixRowValue[indx].delete();   
    }

}
