import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SalaryMatrixRowValueModule { 
    id:number;
    sequence:number;
    desc: string;
    amount:number;
    deleted: boolean =false;

    constructor(sequence:number,amount:number, desc: string) {
      this.sequence=sequence;
      this.amount=amount;
      this.desc=desc;
    }

  getAmount(){
    return this.amount;
  }

  getDesc(){
    return this.desc;
  }
  
  delete(){
      this.deleted=true;
  }
}
