import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SalaryMatrixColumnModule { 
    colSalaryMatrixDescription:string;
    arabicColSalaryMatrixDescription:string;

  constructor(colSalaryMatrixDescription:string,arabicColSalaryMatrixDescription:string) {
    this.arabicColSalaryMatrixDescription=arabicColSalaryMatrixDescription;
    this.colSalaryMatrixDescription=colSalaryMatrixDescription;
  }
}
