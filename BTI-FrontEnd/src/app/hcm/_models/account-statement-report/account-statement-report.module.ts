export class AccountStatementReportModule {
  include: boolean;
  masterId: number;
  employeeId: number;
  code: string;
  codeId: number;
  desc: string;
  arabicDesc: string;
  method: number;
  percent: number;
  amount: number;
  startDate: Date;
  endDate: Date;
  inActive: boolean;
  isAmtType:boolean;
  amtInvalid:boolean=false;
  endDateInvalid:boolean=false;
  startDateInvalid:boolean=false;

  constructor(
    masterId: number,
    employeeId: number,
    code: string,
    codeId: number,
    desc: string,
    arabicDesc: string,
    method: number,
    percent: number,
    amount: number,
    startDate: Date,
    endDate: Date,
    inActive: boolean) {
      
    this.masterId = masterId;
    this.employeeId = employeeId;
    this.code = code;
    this.codeId = codeId;
    this.desc = desc;
    this.arabicDesc = arabicDesc;
    this.method = method;
    this.percent = percent;
    this.amount = amount;
    this.startDate = startDate;
    this.endDate = endDate;
    this.inActive = inActive;
  }  
}
