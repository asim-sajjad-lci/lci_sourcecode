/**
 * A model for ClassSkill 
 */

export class ClassSkill {
    id: number;
    skillDesc: string;
    skillArabicDesc: string;
    skillSequnce : number;
    trainingCourse:object;
    trainingClass:object;
    skillsSetup:object[];
    skillSetSetup:object;
   
    

    //initializing ClassSkill parameters
    constructor(
        id: number, 
        skillDesc: string,
        skillArabicDesc: string,
        skillSequnce : number,
        trainingCourse:object,
        trainingClass:object,
        skillsSetup:object[],
        skillSetSetup:object,
    ) {
        this.id = id;
        this.skillDesc = skillDesc;
        this.skillArabicDesc = skillArabicDesc;
        this.skillSequnce = skillSequnce;
        this.trainingCourse = trainingCourse;
        this.trainingClass = trainingClass;
        this.skillsSetup = skillsSetup;
        this.skillSetSetup = skillSetSetup;     
        
    }
}