
export class EmployeeDeductionMaintnce {
    id: number;
    employeeMaster: object;
    deductionCode: object;
    startDate: Date;
    endDate: Date;
    frequency: number;
    benefitMethod: number;
    deductionAmount: number;
    deductionPercent: number;
    transactionRequired: boolean;
    inactive: boolean;
    perPeriord: number;
    perYear: number;
    lifeTime: number;
    dtoPayCode: any;
    payFactor: any;
    noOfDays:any;
    endDateDays:any;
    deductionTypeId:number;
    roundOf: number;

    constructor(id: number, employeeMaster: object, deductionCode: object,
        startDate: Date, endDate: Date, frequency: number, benefitMethod: number,
        deductionAmount: number, deductionPercent: number, transactionRequired: boolean,
        inactive: boolean, perPeriord: number, perYear: number, lifeTime: number, dtoPayCode: any, payFactor: any,noOfDays:any,endDateDays:any, deductionTypeId:number,
        roundOf: number,) {
        this.id = id;
        this.employeeMaster = employeeMaster;
        this.deductionCode = deductionCode;
        this.startDate = startDate;
        this.endDate = endDate;
        this.frequency = frequency;
        this.benefitMethod = benefitMethod;
        this.deductionAmount = deductionAmount;
        this.deductionPercent = deductionPercent;
        this.transactionRequired = transactionRequired;
        this.inactive = inactive;
        this.perPeriord = perPeriord;
        this.perYear = perYear;
        this.lifeTime = lifeTime;
        this.dtoPayCode = dtoPayCode;
        this.payFactor = payFactor;
        this.noOfDays = noOfDays;
        this.endDateDays = endDateDays;
        this.deductionTypeId = deductionTypeId;
        this.roundOf = roundOf;
    }

}


