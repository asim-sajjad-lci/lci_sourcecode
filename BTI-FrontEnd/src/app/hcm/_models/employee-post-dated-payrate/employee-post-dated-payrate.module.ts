export class EmployeePostDatedPayrateModule {
  id: number;
  employeeMaster: object;
  payCode: object;
  currentPayRate: number;
  newPayRate: number;
  effectiveDate: Date;
  reasonforChange: string;

  constructor(id: number, employeeMaster: object, payCode: object, currentPayRate: number, newPayRate: number, effectiveDate: Date,
              reasonforChange: string){
          this.id = id;
          this.employeeMaster = employeeMaster;
          this.payCode = payCode;
          this.currentPayRate = currentPayRate;
          this.newPayRate = newPayRate;
          this.effectiveDate = effectiveDate;
          this.reasonforChange = reasonforChange;
  }
}
