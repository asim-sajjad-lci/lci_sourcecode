/**
 * A model for Batches 
 */
export class Batch {
    id: number;
    batchId: string;
    description: string;
    arabicDescription:string
    transactionType: any;
    postingDate: any;
    totalTransactions:number;
    quantityTotal:number;
    approved: boolean;
    userID: string;
    approvedDate:string;

    //initializing department parameters
    constructor(id: number, batchId: string, description: string, arabicDescription:string,
                transactionType:any,postingDate: any,totalTransactions :number,
                quantityTotal: number,approved: boolean,userID: string,approvedDate:string) {
        this.id = id;
        this.batchId = batchId;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.transactionType = transactionType;
        this.postingDate = postingDate;
        this.totalTransactions = totalTransactions;        
        this.quantityTotal = quantityTotal;
        this.approved = approved;
        this.userID= userID;
        this.approvedDate = approvedDate;
    }
}

export class getAllBatch {
    id: number;
    batchId: string;
    description: string;
    arabicDescription:string
    transactionType: any;
    postingDate: Date;
    totalTransactions:number;
    quantityTotal:number;
    approved: boolean;
    userID: string;
    approvedDate:string;
    status:number

    //initializing department parameters
    constructor(id: number, batchId: string, description: string, arabicDescription:string,
                transactionType:any,postingDate: Date,totalTransactions :number,
                quantityTotal: number,approved: boolean,userID: string,approvedDate:string, status:number) {
        this.id = id;
        this.batchId = batchId;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.transactionType = transactionType;
        this.postingDate = postingDate;
        this.totalTransactions = totalTransactions;        
        this.quantityTotal = quantityTotal;
        this.approved = approved;
        this.userID= userID;
        this.approvedDate = approvedDate;
        this.status = status;
    }
}