
export class PositionClassModule {
    id: number;
    positionClassId: string;
    positionClassDescription: string;
    arabicPositionClassDescription: string;

    // initializing Position Class parameters
    constructor(id: number, positionClassId: string, positionClassDescription: string,
                arabicPositionClassDescription: string) {
        this.id = id;
        this.positionClassId = positionClassId;
        this.arabicPositionClassDescription = arabicPositionClassDescription;
        this.positionClassDescription = positionClassDescription;
    }
}
