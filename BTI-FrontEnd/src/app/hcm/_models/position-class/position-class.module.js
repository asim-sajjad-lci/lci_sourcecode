"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PositionClassModule = (function () {
    // initializing Position Class parameters
    function PositionClassModule(id, positionClassId, positionClassDescription, arabicPositionClassDescription) {
        this.id = id;
        this.positionClassId = positionClassId;
        this.arabicPositionClassDescription = arabicPositionClassDescription;
        this.positionClassDescription = positionClassDescription;
    }
    return PositionClassModule;
}());
exports.PositionClassModule = PositionClassModule;
//# sourceMappingURL=position-class.module.js.map