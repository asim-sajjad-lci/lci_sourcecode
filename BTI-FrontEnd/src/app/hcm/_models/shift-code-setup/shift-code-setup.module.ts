
export class ShiftCodeSetupModule {

    id: number;
    shiftCodeId: string;
    desc: string;
    arabicDesc: string;
    inActive: boolean;
    shitPremium: number;
    amount: number;
    percent: number;


    constructor(id: number, shiftCodeId: string, desc: string,
        arabicDesc: string, inActive: boolean, shitPremium: number,
        amount: number, percent: number) {
        this.id = id;
        this.shiftCodeId = shiftCodeId;
        this.desc = desc;
        this.arabicDesc = arabicDesc;
        this.inActive = inActive;
        this.shitPremium = shitPremium;
        this.amount =  amount;
        this.percent=percent;
    }


}


