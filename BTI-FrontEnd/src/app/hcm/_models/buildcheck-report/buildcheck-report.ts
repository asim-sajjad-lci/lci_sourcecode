/**
 * A model for department 
 */
export class BuildCheckReportModule {
    id: number;
    batches:Object;
    entryNumber: string;
    entryDate: string;
    description:string;
    arabicDescription:string;
    
    transactionEntryDetail:Object;
    
    //initializing department parameters
    constructor(id: number,
        batches:Object,entryNumber: string,entryDate: string,description:string,arabicDescription:string,
       transactionEntryDetail:Object,) {
        this.id = id;
        this.batches = batches;
        this.entryNumber = entryNumber;
        this.entryDate = entryDate;
        this.description=description;
        this.arabicDescription=arabicDescription;
       
        this.transactionEntryDetail=transactionEntryDetail;
        
    }
}