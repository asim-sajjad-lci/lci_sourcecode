import { InterviewTypeDeail } from "./InterviewTypeSetupDetail";

export class InterviewType {
    id: number;
    interviewTypeId: string;
    interviewTypeDescription: string;
    interviewTypeDescriptionArabic: string;
    interviewRange: number;
    dtoInterviewTypeSetupDetail;
    constructor(id : number,interviewTypeId: string,interviewTypeDescription: string,
        interviewTypeDescriptionArabic: string, interviewRange:number,
        dtoInterviewTypeSetupDetail
    ) {
        this.id = id;
        this.interviewTypeId = interviewTypeId;
        this.interviewTypeDescription = interviewTypeDescription;
        this.interviewTypeDescriptionArabic = interviewTypeDescriptionArabic;
        this.interviewRange = interviewRange;
        this.dtoInterviewTypeSetupDetail = dtoInterviewTypeSetupDetail;
    }
}
