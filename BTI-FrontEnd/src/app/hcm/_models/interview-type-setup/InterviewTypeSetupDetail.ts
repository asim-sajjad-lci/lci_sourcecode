export class InterviewTypeDeail {
    id: any;
    interviewTypeSetupDetailCategoryWeight:any;
    interviewTypeSetupDetailCategoryRowSequance:any;
    interviewTypeSetupDetailDescription:any;
    interviewDetailId:any;
    interviewTypeSetupDetailCategorySequance:any;
    
    constructor(id: any, 
        interviewDetailId: any, 
        interviewTypeSetupDetailCategoryRowSequance:any,
        interviewTypeSetupDetailDescription:any,
        interviewTypeSetupDetailCategoryWeight:any,
        interviewTypeSetupDetailCategorySequance:any
        ) {
        this.id = id;
        this.interviewTypeSetupDetailCategoryWeight = interviewTypeSetupDetailCategoryWeight;
        this.interviewTypeSetupDetailCategoryRowSequance = interviewTypeSetupDetailCategoryRowSequance;
        this.interviewTypeSetupDetailDescription = interviewTypeSetupDetailDescription;
        this.interviewDetailId = interviewDetailId;
        this.interviewTypeSetupDetailCategorySequance=interviewTypeSetupDetailCategorySequance;
    }
}
