/**
 * A model for department 
 */
export class Department {
    id: number;
    departmentId: string;
    departmentDescription: string;
    arabicDepartmentDescription: string;

    //initializing department parameters
    constructor(id: number, departmentId: string, departmentDescription: string,
        arabicDepartmentDescription: string) {
        this.id = id;
        this.departmentId = departmentId;
        this.arabicDepartmentDescription = arabicDepartmentDescription;
        this.departmentDescription = departmentDescription;
    }
}