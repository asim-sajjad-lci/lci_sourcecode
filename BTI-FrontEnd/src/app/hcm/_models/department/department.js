"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A model for department
 */
var Department = (function () {
    //initializing department parameters
    function Department(id, departmentId, departmentDescription, arabicDepartmentDescription) {
        this.id = id;
        this.departmentId = departmentId;
        this.arabicDepartmentDescription = arabicDepartmentDescription;
        this.departmentDescription = departmentDescription;
    }
    return Department;
}());
exports.Department = Department;
//# sourceMappingURL=department.js.map