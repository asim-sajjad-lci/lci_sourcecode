import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeMaster } from '../employee-master/employee-master.module';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class EmployeeEducationModule {
  id: number;
  employeeMaster: EmployeeMaster;
  schoolName: string;
  major: string;
  endYear: string;
  startYear: string;
  degree: string;
  gpa: number;
  comments: string;
  listEmployeeEducation: Array<EmployeeEducationModule>;

  constructor(id: number, employeeMaster: EmployeeMaster, schoolName: string, 
    major: string, endYear: string, startYear: string, degree: string, 
    gpa: number, comments: string, listEmployeeEducation: Array<EmployeeEducationModule>) {
      this.id = id;
      this.employeeMaster = employeeMaster;
      this.schoolName = schoolName;
      this.major = major;
      this.endYear = endYear;
      this.startYear = startYear;
      this.degree =  degree;
      this.gpa=gpa;
      this.comments=comments;
      this.listEmployeeEducation=listEmployeeEducation;
  }
  
 }
