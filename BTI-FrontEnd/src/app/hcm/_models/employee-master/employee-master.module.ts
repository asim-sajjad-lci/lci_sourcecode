
export class EmployeeMaster {
    employeeIndexId: number;
    employeeId: string;
    employeeTitle: string;
    employeeLastName: string;
    employeeFirstName: string;
    employeeMiddleName: string;
    employeeLastNameArabic: string;
    employeeFirstNameArabic: string;
    employeeMiddleNameArabic: string;
    employeeHireDate: number;
    employeeAdjustHireDate: number;
    employeeLastWorkDate: number;
    employeeInactiveDate: number;
    employeeInactiveReason: string;
    employeeType: number;
    employeeGender: string;
    employeeMaritalStatus: string;
    employeeBirthDate: number;
    employeeUserIdInSystem: number;
    employeeWorkHourYearly: number;
    employeeCitizen: number;
    employeeInactive: number;
    employeeImmigration: number;
    divisionId: number;
    departmentId: number;
    projectId: number;
    locationId: number;
    supervisorId: number;
    positionId: number;
    employeeNationalitiesId: number;
    employeeAddressIndexId: number;

    employeeTitleArabic: string;

    employeeSmoker: number;
    idNumber: String;
    expire: number;
    expireHijri: string;
    passportNumber: String;
    expires: number;
    expiresHijri: string;

    employeeBirthDateH: string;

    employeeNotes: string;
    employeeEmail: string;
    employeeBankAccountNumber: string;
    bankId: number;


    constructor(
        employeeIndexId: number,
        employeeId: string,
        employeeTitle: string,
        employeeLastName: string,
        employeeFirstName: string,
        employeeMiddleName: string,
        employeeLastNameArabic: string,
        employeeFirstNameArabic: string,
        employeeMiddleNameArabic: string,
        employeeHireDate: number,
        employeeAdjustHireDate: number,
        employeeLastWorkDate: number,
        employeeInactiveDate: number,
        employeeInactiveReason: string,
        employeeType: number,
        employeeGender: string,
        employeeMaritalStatus: string,
        employeeBirthDate: number,
        employeeUserIdInSystem: number,
        employeeWorkHourYearly: number,
        employeeCitizen: number,
        employeeInactive: number,
        employeeImmigration: number,
        divisionId: number,
        departmentId: number,
        projectId: number,
        locationId: number,
        supervisorId: number,
        positionId: number,
        employeeNationalitiesId: number,
        employeeAddressIndexId: number,

        employeeTitleArabic: string,
        employeeSmoker: number,
        idNumber: String,
        expire: number,
        passportNumber: String,
        expires: number,
        employeeBirthDateH: string,
        expireHijri: string,
        expiresHijri: string,

        employeeNotes: string,
        employeeEmail: string,
        employeeBankAccountNumber: string,
        bankId: number

    ) {
        this.employeeIndexId = employeeIndexId;
        this.employeeId = employeeId;
        this.employeeTitle = employeeTitle;
        this.employeeLastName = employeeLastName;
        this.employeeFirstName = employeeFirstName;
        this.employeeMiddleName = employeeMiddleName;
        this.employeeLastNameArabic = employeeLastNameArabic;
        this.employeeFirstNameArabic = employeeFirstNameArabic;
        this.employeeMiddleNameArabic = employeeMiddleNameArabic;
        this.employeeHireDate = employeeHireDate;
        this.employeeAdjustHireDate = employeeAdjustHireDate;
        this.employeeLastWorkDate = employeeLastWorkDate;
        this.employeeInactiveDate = employeeInactiveDate;
        this.employeeInactiveReason = employeeInactiveReason;
        this.employeeType = employeeType;
        this.employeeGender = employeeGender;
        this.employeeMaritalStatus = employeeMaritalStatus;
        this.employeeBirthDate = employeeBirthDate;
        this.employeeUserIdInSystem = employeeUserIdInSystem;
        this.employeeWorkHourYearly = employeeWorkHourYearly;
        this.employeeCitizen = employeeCitizen;
        this.employeeInactive = employeeInactive;
        this.employeeImmigration = employeeImmigration;
        this.divisionId = divisionId;
        this.departmentId = departmentId;
        this.projectId = projectId;
        this.locationId = locationId;
        this.supervisorId = supervisorId;
        this.positionId = positionId;
        this.employeeNationalitiesId = employeeNationalitiesId;
        this.employeeAddressIndexId = employeeAddressIndexId;

        this.employeeTitleArabic = employeeTitleArabic;

        this.employeeSmoker = employeeSmoker;

        this.idNumber = idNumber;
        this.expire = expire;
        this.passportNumber = passportNumber;
        this.expires = expires;
        this.employeeBirthDateH = employeeBirthDateH;
        this.expireHijri = expireHijri;
        this.expiresHijri = expiresHijri;

        this.employeeNotes = employeeNotes;
        this.employeeEmail = employeeEmail;
        this.employeeBankAccountNumber = employeeBankAccountNumber;
        this.bankId = bankId;
    }


}


export class EmployeeMiscellaneousMaster {
    id:number;
  //  employeeIndexId: number;
  //  miscIndxId: number;
  include: boolean;
  masterId: number;
    codeId: string;
    codeName: string;
    valueIndexId:string;
    dtoValuesList: any[];

    constructor( 
        id:number,
       // employeeIndexId: number,
      //  miscIndxId: number,
      masterId: number,
        codeId: string,
        codeName: string,
        valueIndexId:string,
        dtoValuesList: any[]

    ) {
        this.id = id;
     //   this.employeeIndexId = employeeIndexId;
      //  this.miscIndxId = miscIndxId;
      this.masterId = masterId;
        this.codeId = codeId;
        this.codeName = codeName;
        this.valueIndexId = valueIndexId;
        this.dtoValuesList = dtoValuesList;

    }


}

export class EmployeeMiscellaneousMasterSave {
    miscId:number;
    valueIndexId: number;
  employeeId: number;
  id:number;
  isDeleted:boolean;

    constructor( 
        miscId:number,
        valueIndexId: number,
        employeeId: number,
        id:number,
        isDeleted:boolean
    ) {
        this.miscId = miscId;
        this.valueIndexId = valueIndexId;
        this.employeeId = employeeId;
        this.id = id;
        this.isDeleted = isDeleted;
}


}

