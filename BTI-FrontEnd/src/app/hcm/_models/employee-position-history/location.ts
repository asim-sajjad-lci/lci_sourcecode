export class location {
    id: any;
    locationId:any;
    description:string;
    arabicDescription:string;

    constructor(id: any,locationId:any,description:string,arabicDescription:string) {
        this.id = id;
        this.locationId=locationId;
        this.description=description;
        this.arabicDescription=arabicDescription;
       }
}