export class employeePosition {
    id:number;
    positionEffectiveDate: any;
    employeeType;
    employeeMaster;
    division;
    department;
    position;
    location;
    supervisor;
    dtoEmployeePositionReason;
  

    constructor(id: number, positionEffectiveDate: Date, employeeType,
        employeeMaster, division,
        department, position, location,supervisor,dtoEmployeePositionReason
    ) {
        this.id = id;
        this.positionEffectiveDate = positionEffectiveDate;
        this.employeeType = employeeType;
        this.employeeMaster = employeeMaster;
        this.division = division;
        this.department = department;
        this.position = position;
        this.location = location;
        this.supervisor = supervisor;
        this.dtoEmployeePositionReason=dtoEmployeePositionReason;
        
    }
}