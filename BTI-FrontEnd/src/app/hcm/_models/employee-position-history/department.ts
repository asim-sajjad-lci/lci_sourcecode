export class department {
    id: any;
    departmentId:any;
    departmentDescription:string;
    arabicDepartmentDescription:string;

    constructor(id: any,departmentId:string,departmentDescription:string,arabicDepartmentDescription:string) {
        this.id = id;
        this.departmentId=departmentId;
        this.departmentDescription=departmentDescription;
        this.arabicDepartmentDescription=arabicDepartmentDescription;
       }
}