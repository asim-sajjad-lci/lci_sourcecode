export class position {
    id: any;
    positionId: any;
    description:string;
    arabicDescription:string;

    constructor(id: any,positionId:any,description:string,arabicDescription:string) {
        this.id = id;
        this.positionId=positionId;
        this.description=description;
        this.arabicDescription=arabicDescription;
       }
}