export class dtoEmployeePositionReason {
    id: any;
    positionReason: string;

    constructor(id: number,positionReason:string) {
        this.id = id;
        this.positionReason=positionReason;
       }
}