
export class PredefineChecklist { 
  id: number;
  preDefineCheckListType: number;
  preDefineCheckListItemDescription: string;
  preDefineCheckListItemDescriptionArabic: string;
  subItems;


  // initializing Predefine Checklist Parameter
  constructor(id: number, preDefineCheckListType: number, preDefineCheckListItemDescription: string, 
    preDefineCheckListItemDescriptionArabic: string, subItems) {
      this.id = id;
      this.preDefineCheckListType = preDefineCheckListType;
      this.preDefineCheckListItemDescription = preDefineCheckListItemDescription;
      this.preDefineCheckListItemDescriptionArabic = preDefineCheckListItemDescriptionArabic;
      this.subItems = subItems;
  }
}

