/**
 * A model for Miscellaneous 
 */
export class MiscellaneousSetup {
   id: number;
   codeId: string;
   codeName: string;
   codeArabicName: string;
   dimension:boolean;

   //initializing Miscellaneous parameters
   constructor(id: number, codeId: string, codeName: string,
      codeArabicName: string,dimension:boolean) {
       this.id = id;
       this.codeId = codeId;
       this.codeArabicName = codeArabicName;
       this.codeName = codeName;
       this.dimension = dimension;
   }
}