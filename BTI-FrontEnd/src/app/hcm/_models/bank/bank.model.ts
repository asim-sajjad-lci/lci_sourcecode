export class BankModel {
    id: number;
    bankName: string;
    bankDescription: string;
    bankDescriptionArabic:string
    swiftCode: string;
    accountNumber: string;

    //initializing department parameters
    constructor(id: number, bankName: string, bankDescription: string, bankDescriptionArabic:string,
        swiftCode: string, accountNumber: string
    ) {
        this.id = id;
        this.bankName = bankName;
        this.bankDescription = bankDescription;
        this.bankDescriptionArabic = bankDescriptionArabic;
        this.swiftCode = swiftCode;
        this.accountNumber = accountNumber;
    }
}

