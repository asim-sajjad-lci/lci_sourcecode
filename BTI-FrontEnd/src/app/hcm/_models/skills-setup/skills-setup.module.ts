export class SkillsSetup {
    id: number;
    skillId: number;
    skillDesc: string;
    arabicDesc: string;
    compensation: number;
    frequency: number;
    frequencyName: string;


    constructor(id: number, skillId: number, skillDesc: string,
        arabicDesc: string, compensation: number, frequency: number, frequencyName: string) {
        this.id = id;
        this.skillId = skillId;
        this.skillDesc = skillDesc;
        this.arabicDesc = arabicDesc;
        this.compensation = compensation;
        this.frequency = frequency;
        this.frequencyName = frequencyName;
    }

}


