"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AttendanceSetupOptions = (function () {
    function AttendanceSetupOptions(id, seqn, reasonIndx, reasonDesc, atteandanceTypeIndx, atteandanceTypeSeqn, atteandanceTypeDesc, ids, isActive) {
        this.id = id;
        this.seqn = seqn;
        this.reasonIndx = reasonIndx;
        this.reasonDesc = reasonDesc;
        this.atteandanceTypeIndx = atteandanceTypeIndx;
        this.atteandanceTypeSeqn = atteandanceTypeSeqn;
        this.atteandanceTypeDesc = atteandanceTypeDesc;
        this.ids = ids;
        this.isActive = isActive;
    }
    return AttendanceSetupOptions;
}());
exports.AttendanceSetupOptions = AttendanceSetupOptions;
//# sourceMappingURL=attendance-setup-options.module.js.map