
export class AttendanceSetupOptions {
    id: number;
    seqn: number;
    reasonIndx: number;
    reasonDesc: string;
    atteandanceTypeIndx: number;
    atteandanceTypeSeqn: number;
    atteandanceTypeDesc: string;
    isActive: boolean;


    constructor(id: number, seqn: number, reasonIndx: number, reasonDesc: string,
        atteandanceTypeIndx: number, atteandanceTypeSeqn: number, atteandanceTypeDesc:string, isActive: boolean) {
        this.id = id;
        this.seqn = seqn;
        this.reasonIndx = reasonIndx;
        this.reasonDesc = reasonDesc;
        this.atteandanceTypeIndx = atteandanceTypeIndx;
        this.atteandanceTypeSeqn = atteandanceTypeSeqn;
        this.atteandanceTypeDesc = atteandanceTypeDesc;
        this.isActive = isActive;
        }

}


