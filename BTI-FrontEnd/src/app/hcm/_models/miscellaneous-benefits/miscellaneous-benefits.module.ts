export class MiscellaneousBenefits {
    id: number;
    benefitsId: number;
    desc: string;
    arbicDesc: string;
    frequency: number;
    startDate: Date;
    endDate: Date;
    inactive: boolean;
    method: number;
    dudctionAmount: number;
    dudctionPercent: number;
    monthlyAmount: number;
    yearlyAmount: number;
    lifetimeAmount: number;
    empluyeeerinactive: boolean;
    empluyeeermethod: number;
    benefitAmount: number;
    benefitPercent: number;
    employermonthlyAmount: number;
    employeryearlyAmount: number;
    employerlifetimeAmount: number;

    constructor(
        id: number,
        benefitsId: number,
        desc: string,
        arbicDesc: string,
        frequency: number,
        startDate: Date,
        endDate: Date,
        inactive: boolean,
        method: number,
        dudctionAmount: number,
        dudctionPercent: number,
        monthlyAmount: number,
        yearlyAmount: number,
        lifetimeAmount: number,
        empluyeeerinactive: boolean,
        empluyeeermethod: number,
        benefitAmount: number,
        benefitPercent: number,
        employermonthlyAmount: number,
        employeryearlyAmount: number,
        employerlifetimeAmount: number
    ) {
        this.id = id;
        this.benefitsId = benefitsId;
        this.desc = desc;
        this.arbicDesc = arbicDesc;
        this.frequency = frequency;
        this.startDate = startDate;
		this.inactive=inactive;
        this.endDate = endDate;
        this.method = method;
        this.dudctionAmount = dudctionAmount;
        this.dudctionPercent = dudctionPercent;
        this.monthlyAmount = monthlyAmount;
        this.yearlyAmount = yearlyAmount;
        this.lifetimeAmount = lifetimeAmount;
        this.empluyeeerinactive = empluyeeerinactive;
        this.empluyeeermethod = empluyeeermethod;
        this.benefitAmount = benefitAmount;
        this.benefitPercent = benefitPercent;
        this.employermonthlyAmount = employermonthlyAmount;
        this.employeryearlyAmount = employeryearlyAmount;
        this.employerlifetimeAmount = employerlifetimeAmount;
    }

}
