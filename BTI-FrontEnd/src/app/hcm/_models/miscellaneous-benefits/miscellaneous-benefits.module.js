"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SkillsSetup = (function () {
    function SkillsSetup(id, skillId, skillDesc, arabicDesc, compensation, frequency, frequencyName) {
        this.id = id;
        this.skillId = skillId;
        this.skillDesc = skillDesc;
        this.arabicDesc = arabicDesc;
        this.compensation = compensation;
        this.frequency = frequency;
        this.frequencyName = frequencyName;
    }
    return SkillsSetup;
}());
exports.SkillsSetup = SkillsSetup;
//# sourceMappingURL=skills-setup.module.js.map