export class skillSetSetup {
    id: number;
    skillId:string;
    skillSetId: string;
    skillSetDiscription: string;
    arabicDiscription: string;
    skillSetSeqn : string;
    skillRequired : boolean;
    comment: string;
    skillSetDescId: any;
    skillSetIndexId:any;
    skillIdParam:string;
    listSkillSteup:object[];
    skillsIds:string;
   
    constructor(id: number,skillId:string, skillSetId: string, skillSetDiscription: string,
        arabicDiscription: string ,skillSetSeqn : string,skillRequired : boolean, comment : string,skillSetDescId: any, skillSetIndexId:any, skillIdParam:string,listSkillSteup:object[],skillsIds:string) {
        this.id = id;
        this.skillId = skillId;
        this.skillSetId = skillSetId;
        this.skillSetDiscription = skillSetDiscription;
        this.arabicDiscription = arabicDiscription;
        this.skillSetSeqn=skillSetSeqn;
        this.skillRequired=skillRequired;
        this.comment=comment;
        this.skillSetDescId=skillSetDescId;
        this.skillSetIndexId=skillSetIndexId;
        this.skillIdParam=skillIdParam;
        this.listSkillSteup=listSkillSteup;
        this.skillsIds = skillsIds;
    }

}


