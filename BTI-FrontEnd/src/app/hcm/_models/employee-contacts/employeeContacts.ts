/**
 * A model for employee address master 
 */
export class EmployeeAddressMaster {

    id: number;
    employeeDepedentSequence: string;
    employeeContactName: string;
    employeeContactNameArabic: string;
    employeeContactSequence: number;
    employeeContactNameRelationship: string;
    employeeContactHomePhone: string;
    employeeContactMobilePhone: string;    
    employeeContactWorkPhone: string;
    employeeContactAddress: string;
    employeeContactWorkPhoneExt: string;    
    employeeMaster;

    //initializing employee address master parameters

    constructor( id: number,  employeeDepedentSequence: string, employeeContactName: string, employeeContactNameArabic: string, 
        employeeContactSequence: number, employeeContactNameRelationship: string, employeeContactHomePhone: string, 
        employeeContactMobilePhone: string, employeeContactWorkPhone: string, employeeContactAddress: string, 
        employeeContactWorkPhoneExt: string, employeeMaster) {
        
            this.id = id;
            this.employeeDepedentSequence = employeeDepedentSequence;
            this.employeeContactName = employeeContactName;
            this.employeeContactNameArabic = employeeContactNameArabic;
            this.employeeContactSequence = employeeContactSequence;
            this.employeeContactNameRelationship = employeeContactNameRelationship;
            this.employeeContactHomePhone = employeeContactHomePhone;
            this.employeeContactMobilePhone = employeeContactMobilePhone;
            this.employeeContactWorkPhone = employeeContactWorkPhone;
            this.employeeContactAddress = employeeContactAddress;
            this.employeeContactWorkPhoneExt = employeeContactWorkPhoneExt;
            this.employeeMaster = employeeMaster;

        }
}