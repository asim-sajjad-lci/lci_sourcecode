"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PositionPlanSetupModule = (function () {
    // initializing Position Plan Setup parameters
    function PositionPlanSetupModule(id, positionId, positionPlanId, positionPlanDesc, positionPlanArbicDesc, positionPlanStart, positionPlanEnd, inactive) {
        this.id = id;
        this.positionId = positionId;
        this.positionPlanId = positionPlanId;
        this.positionPlanDesc = positionPlanDesc;
        this.positionPlanArbicDesc = positionPlanArbicDesc;
        this.positionPlanStart = positionPlanStart;
        this.positionPlanEnd = positionPlanEnd;
        this.inactive = inactive;
    }
    return PositionPlanSetupModule;
}());
exports.PositionPlanSetupModule = PositionPlanSetupModule;
//# sourceMappingURL=position-plan-setup.module.js.map