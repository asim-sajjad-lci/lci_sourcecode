export class PositionPlanSetupModule {
    id: number;
    positionPlanId: string;
    positionId: string;
    positionPlanDesc: string;
    positionPlanArbicDesc: string;
    positionPlanStart: Date;
    positionPlanEnd: Date;
    inactive: boolean;

    // initializing Position Plan Setup parameters
    constructor(id: number, positionId: string, positionPlanId: string, positionPlanDesc: string,
                positionPlanArbicDesc: string, positionPlanStart: Date, positionPlanEnd: Date, inactive: boolean) {
        this.id = id;
        this.positionId = positionId;
        this.positionPlanId = positionPlanId;
        this.positionPlanDesc = positionPlanDesc;
        this.positionPlanArbicDesc = positionPlanArbicDesc;
        this.positionPlanStart = positionPlanStart;
        this.positionPlanEnd = positionPlanEnd;
        this.inactive = inactive;
    }
}
