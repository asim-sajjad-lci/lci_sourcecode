
export class BuildCheckPayrollCodes {
    include: boolean =false;
    id: number;
    codeId: string;
    buildChecksId:number;

    constructor( id: number, codeId: string, buildChecksId:number) {
            this.id = id;
            this.codeId = codeId;
            this.buildChecksId = buildChecksId;
    }

}


