
export class RetirementPlanSetup {
        id: number;
        retirementPlanId: string;
        retirementPlanDescription: string;
        retirementPlanArbicDescription: string;
        retirementPlanAccountNumber: number;
        retirementPlanMatchPercent: number;
        retirementPlanMaxPercent: number;
		retirementPlanMaxAmount:number;
        bonus:boolean;
		loans:boolean;
		watingMethod:number;
		
        watingPeriods: number;
		retirementAge:number;
        retirementContribution: number;
		retirementPlanAmount:number;
		retirementPlanPercent:number;
		retirementFrequency:number;
		helthInsuranceId:number;
        retirementEntranceDate: string;
        subItems;
        insCompanyId: string;

        constructor(id: number, retirementPlanId: string, retirementPlanDescription: string, retirementPlanArbicDescription: string,
                retirementPlanAccountNumber: number, retirementPlanMatchPercent: number, retirementPlanMaxPercent: number,retirementPlanMaxAmount:number,bonus:boolean,
                loans:boolean,watingMethod:number, watingPeriods: number,retirementAge:number,retirementContribution: number,retirementPlanAmount:number,retirementPlanPercent:number,retirementFrequency:number,helthInsuranceId:number,
                    retirementEntranceDate: string,subItems,insCompanyId: string) {
                this.id = id;
                this.retirementPlanId = retirementPlanId;
                this.retirementPlanDescription = retirementPlanDescription;
                this.retirementPlanArbicDescription = retirementPlanArbicDescription;

                this.retirementPlanAccountNumber = retirementPlanAccountNumber;
                this.retirementPlanMatchPercent = retirementPlanMatchPercent;
                this.retirementPlanMaxPercent = retirementPlanMaxPercent;
				this.retirementPlanMaxAmount=retirementPlanMaxAmount;
                

                this.bonus = bonus;
                this.loans = loans;
				this.watingMethod = watingMethod;
				this.watingPeriods = watingPeriods;
				this.retirementAge = retirementAge;
				this.retirementContribution = retirementContribution;
				this.retirementPlanAmount = retirementPlanAmount
				this.retirementPlanPercent = retirementPlanPercent;
				this.retirementFrequency = retirementFrequency;
				this.helthInsuranceId = helthInsuranceId;
                this.retirementEntranceDate = retirementEntranceDate;
                this.subItems = subItems;
                this.insCompanyId = insCompanyId;
        }

}


