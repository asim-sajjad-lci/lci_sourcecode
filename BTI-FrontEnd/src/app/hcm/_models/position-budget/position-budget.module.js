"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PositionBudgetModule = (function () {
    // initializing Position Plan Setup parameters
    function PositionBudgetModule(id, positionPlanId, description, budgetScheduleStart, budgetScheduleEnd, budgetAmount, budgetDesc, budgetArbicDesc) {
        this.id = id;
        this.positionPlanId = positionPlanId;
        this.description = description;
        this.budgetScheduleStart = budgetScheduleStart;
        this.budgetScheduleEnd = budgetScheduleEnd;
        this.budgetAmount = budgetAmount;
        this.budgetDesc = budgetDesc;
        this.budgetArbicDesc = budgetArbicDesc;
    }
    return PositionBudgetModule;
}());
exports.PositionBudgetModule = PositionBudgetModule;
//# sourceMappingURL=position-budget.module.js.map