
export class PositionBudgetModule {
    id: number;
    positionPlanId: string;
    budgetScheduleStart: Date;
    budgetScheduleEnd: Date;
    budgetDesc: string;
    budgetArbicDesc: string;
    description: string;
    budgetAmount: number;
    positionPlanPrimaryId: string;

    // initializing Position Plan Setup parameters
    constructor(id: number, positionPlanId: string,
                description: string, budgetScheduleStart: Date, budgetScheduleEnd: Date,
                budgetAmount: number, budgetDesc: string, budgetArbicDesc: string, positionPlanPrimaryId: string) {
        this.id = id;
        this.positionPlanId = positionPlanId;
        this.description = description;
        this.budgetScheduleStart = budgetScheduleStart;
        this.budgetScheduleEnd = budgetScheduleEnd;
        this.budgetAmount = budgetAmount;
        this.budgetDesc = budgetDesc;
        this.budgetArbicDesc = budgetArbicDesc;
        this.positionPlanPrimaryId = positionPlanPrimaryId;
    }
}
