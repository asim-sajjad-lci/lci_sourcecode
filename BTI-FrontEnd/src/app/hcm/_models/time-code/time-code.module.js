"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Location = (function () {
    function Location(id, locationId, description, arabicDescription, contactName, locationAddress, cityName, countryName, stateName, cityId, countryId, stateId, phone, fax) {
        this.id = id;
        this.locationId = locationId;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.contactName = contactName;
        this.locationAddress = locationAddress;
        this.cityName = cityName;
        this.countryName = countryName;
        this.stateName = stateName;
        this.cityId = cityId;
        this.countryId = countryId;
        this.stateId = stateId;
        this.phone = phone;
        this.fax = fax;
    }
    return Location;
}());
exports.Location = Location;
//# sourceMappingURL=location.module.js.map