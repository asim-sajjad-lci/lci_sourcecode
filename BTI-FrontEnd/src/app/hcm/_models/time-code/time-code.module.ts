
export class TimeCode {
    id: number;
    payCodeId: number;
    timeCodeId: string;
    inActive: boolean;
    desc: string;
    arbicDesc: string;
    accrualPeriod: string;
    timeType: string;
	scheduleId: number;
    schedulePrimaryId: number;
    payPrimaryId: number;
	description:string;


    constructor(id: number, payCodeId: number, timeCodeId: string,
                inActive: boolean, desc: string, arbicDesc: string,
                accrualPeriod: string, timeType: string, scheduleId: number, schedulePrimaryId: number, payPrimaryId: number,description:string) {
        this.id = id;
        this.payCodeId = payCodeId;
        this.timeCodeId = timeCodeId;
        this.inActive = inActive;
        this.desc = desc;
        this.arbicDesc =  arbicDesc;
        this.accrualPeriod =  accrualPeriod;
        this.timeType = timeType;  
        this.scheduleId = scheduleId;  
        this.schedulePrimaryId = schedulePrimaryId;  
        this.payPrimaryId = payPrimaryId;
		this.description=description;
    }


}


