/**
 * A model for department 
 */
export class TransactionEntryModule {
    id: number;
    batches:Object;
    dimensions:Object;
    entryNumber: string;
    entryDate: string;
    description:string;
    arabicDescription:string;
    transactionEntryDetail:Object;
    fromDate:string;
    toDate:string;
    
    //initializing department parameters
    constructor(id: number,
        batches:Object,dimensions:Object,entryNumber: string,entryDate: string,description:string,arabicDescription:string,
       transactionEntryDetail:Object,fromDate:string,toDate:string) {
        this.id = id;
        this.batches = batches;
        this.dimensions = dimensions;
        this.entryNumber = entryNumber;
        this.entryDate = entryDate;
        this.description=description;
        this.arabicDescription=arabicDescription;
        this.transactionEntryDetail=transactionEntryDetail;
        this.fromDate = fromDate;
        this.toDate = toDate;
        
    }
}

export class SaveDataEntryModule {
    id: number;
    batches: Object;
    dimensions: Object;
    batcheId: number;
    dimensionsId: number;
    entryNumber: string;
    entryDate: string;
    description: string;
    arabicDescription: string;
    fromDate: string;
    toDate: string;
    transactionType: string;
    CodeId: number;
    CodeIdString: string;
    CodeFactor: string;
    amount: string;
    PayRate: number;
    PayFactor: string;
    EmployeeId: string;
    EmployeeName: string;
    employeeIndexId:number;
    transactionEntryDetail: Object;
    transcationSubListId:number;

    //initializing department parameters
    constructor(id: number, batches: Object,dimensions: Object,batcheId: number, dimensionsId: number, entryNumber: string, entryDate: string, description: string, arabicDescription: string,
        fromDate: string, toDate: string, transactionType: string, CodeId: number, CodeIdString: string,CodeFactor:string,amount: string, PayRate: number, PayFactor: string, EmployeeId: string, EmployeeName: string, employeeIndexId: number, transactionEntryDetail: Object,transcationSubListId:number) {
        this.id = id;
        this.batches = batches;
        this.dimensions = dimensions;
        this.batcheId = batcheId;
        this.dimensionsId = dimensionsId;
        this.entryNumber = entryNumber;
        this.entryDate = entryDate;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.EmployeeId = EmployeeId;
        this.transactionType = transactionType;
        this.CodeId = CodeId;
        this.CodeIdString = CodeIdString
        this.amount = amount;
        this.PayRate = PayRate;
        this.PayFactor = PayFactor;
        this.EmployeeName = EmployeeName;
        this.CodeFactor = CodeFactor;
        this.employeeIndexId = employeeIndexId;
        this.transactionEntryDetail = transactionEntryDetail;
        this.transcationSubListId = transcationSubListId;

    }
}
