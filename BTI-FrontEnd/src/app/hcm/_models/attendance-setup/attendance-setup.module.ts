
export class AttendanceSetup {
    id: number;
    accrueType: string;
    attendance: boolean;
    currentyear: string;
    latDayAcuurate: Date;
    numberOfDayInWeek: string;
    numberOfHourInDay: string;
    nextTranscationNum: string;


    constructor(id: number, accrueType: string, attendance: boolean,
                currentyear: string, latDayAcuurate: Date, numberOfDayInWeek: string,
                numberOfHourInDay: string, nextTranscationNum: string) {
        this.id = id;
        this.accrueType = accrueType;
        this.attendance = attendance;
        this.currentyear = currentyear;
        this.latDayAcuurate = latDayAcuurate;
        this.numberOfDayInWeek = numberOfDayInWeek;
        this.numberOfHourInDay = numberOfHourInDay;
        this.nextTranscationNum = nextTranscationNum;
        }

}


