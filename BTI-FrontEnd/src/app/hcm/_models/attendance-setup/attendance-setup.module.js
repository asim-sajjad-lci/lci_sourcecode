"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AttendanceSetup = (function () {
    function AttendanceSetup(id, accrueType, attendance, currentyear, latDayAcuurate, numberOfDayInWeek, numberOfHourInDay, nextTranscationNum) {
        this.id = id;
        this.accrueType = accrueType;
        this.attendance = attendance;
        this.currentyear = currentyear;
        this.latDayAcuurate = latDayAcuurate;
        this.numberOfDayInWeek = numberOfDayInWeek;
        this.numberOfHourInDay = numberOfHourInDay;
        this.nextTranscationNum = nextTranscationNum;
    }
    return AttendanceSetup;
}());
exports.AttendanceSetup = AttendanceSetup;
//# sourceMappingURL=attendance-setup.module.js.map