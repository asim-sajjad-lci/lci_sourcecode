/**
 * A model for department 
 */
export class EmployeeSkills {
    id: number;
    skillSetSetup: object;
    employeeMaster: object;
    listSkills: Array<any>;
    //initializing department parameters
    constructor(id: number, skillSetSetup: object, employeeMaster: object,listSkills) {
        this.id = id;
        this.skillSetSetup = skillSetSetup;
        this.employeeMaster = employeeMaster;
        this.listSkills = listSkills;
    }
}