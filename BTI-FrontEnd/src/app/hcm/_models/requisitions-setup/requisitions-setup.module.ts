
export class RequisitionsSetup {
    id: number;
	requisitionNo : number;
    stats: number;
    internalPostDate: string;
    internalCloseDate: string;
    openingDate: string;
    recruiterName: string;
    companyId: number;
    managerName: string;
	jobPosting: number;
    advertisingField1: string;
    advertisingField2: string;
    advertisingField3: string;
    advertisingField4: string;
    positionsAvailable: number;
    positionsFilled: number;
    applicantsApplied: number;
    applicantsInterviewed: number;
    costadvirs: number;
    costrecri: number;
    departmentId: number;
    divisionId: number;
    supervisorId: number;
    positionId: number;


    constructor(id: number,requisitionNo:number,stats: number,internalPostDate: string,internalCloseDate: string,openingDate: string,recruiterName: string,companyId: number,managerName: string,jobPosting: number,advertisingField1: string,advertisingField2: string,advertisingField3: string,advertisingField4: string,positionsAvailable: number,positionsFilled: number,applicantsApplied: number,applicantsInterviewed: number,costadvirs: number,costrecri:number,departmentId: number,divisionId: number,supervisorId: number,positionId: number) {
        	this.id= id;
			this.requisitionNo=requisitionNo;
			this.stats= stats;
			this.internalPostDate= internalPostDate;
			this.internalCloseDate= internalCloseDate;
			this.openingDate= openingDate;
			this.recruiterName= recruiterName;
			this.companyId= companyId;
			this.managerName= managerName;
			this.jobPosting= jobPosting;
			this.advertisingField1= advertisingField1;
			this.advertisingField2= advertisingField2;
			this.advertisingField3= advertisingField3;
			this.advertisingField4= advertisingField4;
			this.positionsAvailable= positionsAvailable;
			this.positionsFilled= positionsFilled;
			this.applicantsApplied= applicantsApplied;
			this.applicantsInterviewed= applicantsInterviewed;
			this.costadvirs= costadvirs;
			this.costrecri= costrecri;
			this.departmentId= departmentId;
			this.divisionId= divisionId;
			this.supervisorId= supervisorId;
			this.positionId= positionId;
    }


}


