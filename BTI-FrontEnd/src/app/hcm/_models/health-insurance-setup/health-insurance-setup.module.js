"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HealthInsuranceSetup = (function () {
    function HealthInsuranceSetup(id, insCompanyId, desc, arbicDesc, address, city, contactPerson, phoneNo, email, fax) {
        this.id = id;
        this.insCompanyId = insCompanyId;
        this.desc = desc;
        this.arbicDesc = arbicDesc;
        this.address = address;
        this.city = city;
        this.contactPerson = contactPerson;
        this.phoneNo = phoneNo;
        this.email = email;
        this.fax = fax;
    }
    return HealthInsuranceSetup;
}());
exports.HealthInsuranceSetup = HealthInsuranceSetup;
//# sourceMappingURL=health-insurance-setup.module.js.map