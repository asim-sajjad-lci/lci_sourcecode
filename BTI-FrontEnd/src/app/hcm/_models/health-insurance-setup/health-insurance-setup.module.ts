export class HealthInsuranceSetup {
    id: number;
    insCompanyId: number;
    desc: string;
    arbicDesc: string;
    address: string;
	contactPerson: string;
	phoneNo: string;
	email: string;
	fax: string;
    cityName: string;
    countryName: string;
    stateName: string;    
    cityId: number;
    countryId: number;
    stateId: number;  
  

    constructor(id: number, insCompanyId: number, desc: string, arbicDesc: string,
        address: string, contactPerson: string, phoneNo: string, email: string, fax: string, cityName: string, countryName: string,stateName:string,cityId:number,countryId:number,stateId:number,  
         ) {
        this.id = id;
        this.insCompanyId = insCompanyId;
        this.desc = desc;
        this.arbicDesc = arbicDesc;
        this.address = address;
		this.contactPerson = contactPerson;
		this.phoneNo = phoneNo;
		 this.email = email;
        this.fax = fax;
        this.cityName =  cityName;
        this.countryName =  countryName;
        this.stateName = stateName;    
        this.cityId = cityId;
        this.countryId =  countryId;
        this.stateId = stateId;
        
        
       
    }
}
