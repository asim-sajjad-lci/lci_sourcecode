export class PositionAttachmentModule {
    id: number;
    positionIds: string;
    documentAttachmenDesc: string;
    attachmentType: number;
   

    // initializing Position Training parameters
    constructor(id: number, positionIds: string, 
                documentAttachmenDesc: string, attachmentType: number ) {
        this.id = id;
        this.positionIds = positionIds;
        this.documentAttachmenDesc = documentAttachmenDesc;
        this.attachmentType = attachmentType;
       
    }
}
