"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PositionAttachmentModule = (function () {
    // initializing Position Training parameters
    function PositionAttachmentModule(id, positionIds, documentAttachmenDesc, attachmentType, attachmentDate, attachmentTime, userId, documentAttachmentName) {
        this.id = id;
        this.positionIds = positionIds;
        this.documentAttachmenDesc = documentAttachmenDesc;
        this.attachmentType = attachmentType;
        this.attachmentDate = attachmentDate;
        this.attachmentTime = attachmentTime;
        this.userId = userId;
        this.documentAttachmentName = documentAttachmentName;
    }
    return PositionAttachmentModule;
}());
exports.PositionAttachmentModule = PositionAttachmentModule;
//# sourceMappingURL=position-attachment.module.js.map