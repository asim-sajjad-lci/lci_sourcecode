"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DeductionCodeSetup = (function () {
    function DeductionCodeSetup(id, deductionCodeId, description, arabicDescription, startDate, endDate, frequency, method, amount, transction, inActive, perPeriod, perYear, lifeTime) {
        this.id = id;
        this.diductionId = deductionCodeId;
        this.discription = description;
        this.arbicDiscription = arabicDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.frequency = frequency;
        this.method = method;
        this.amount = amount;
        this.transction = transction;
        this.inActive = inActive;
        this.perPeriod = perPeriod;
        this.perYear = perYear;
        this.lifeTime = lifeTime;
    }
    return DeductionCodeSetup;
}());
exports.DeductionCodeSetup = DeductionCodeSetup;
//# sourceMappingURL=deduction-code-setup.module.js.map