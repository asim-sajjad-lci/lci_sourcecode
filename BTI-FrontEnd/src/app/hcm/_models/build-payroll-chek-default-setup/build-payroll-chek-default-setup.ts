/**
 * A model for department 
 */
export class BuildPayrollCheckDefault {
    id: number;
    defaultID: string;
    desc: string;
    arabicDesc: string;

    //initializing department parameters
    constructor(id: number, defaultID: string, desc: string,
        arabicDesc: string) {
        this.id = id;
        this.defaultID = defaultID;
        this.desc = desc;
        this.arabicDesc = arabicDesc;
    }
}