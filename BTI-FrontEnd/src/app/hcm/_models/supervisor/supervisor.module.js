"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Supervisor = (function () {
    function Supervisor(id, superVisionCode, description, arabicDescription, employeeName, employeeId) {
        this.id = id;
        this.superVisionCode = superVisionCode;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.employeeName = employeeName;
        this.employeeId = employeeId;
    }
    return Supervisor;
}());
exports.Supervisor = Supervisor;
//# sourceMappingURL=supervisor.module.js.map