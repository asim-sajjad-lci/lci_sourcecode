
export class Supervisor {
    id: number;
    superVisionCode: string;
    description: string;
    arabicDescription: string;
    employeeName: string;
    employeeId: number;
    employeeIds:string;


    constructor(id: number, superVisionCode: string, description: string,
                arabicDescription: string, employeeName: string, employeeId: number,employeeIds:string) {
        this.id = id;
        this.superVisionCode = superVisionCode;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.employeeName = employeeName;
        this.employeeId = employeeId;
        this.employeeIds=employeeIds;

    }


}


