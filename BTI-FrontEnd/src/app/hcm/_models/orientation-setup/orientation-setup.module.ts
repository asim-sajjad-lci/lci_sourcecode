export class OrientationSetup {
    id: number;
    orientationId:string;
    orientationDesc: string;
    orientationArbicDesc: string;
	orientationPredefinedCheckListItemId:number;
    subItems:any[];
	
   
    constructor(id: number,orientationId:string, orientationDesc: string, 
        orientationArbicDesc: string,orientationPredefinedCheckListItemId:number,subItems:any[] ) {
        this.id = id;
        this.orientationId = orientationId;
        this.orientationDesc = orientationDesc;
        this.orientationArbicDesc = orientationArbicDesc;
		this.orientationPredefinedCheckListItemId = orientationPredefinedCheckListItemId;
		this.subItems = subItems;
        
    }

}


