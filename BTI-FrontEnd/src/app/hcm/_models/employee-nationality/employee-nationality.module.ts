import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class EmployeeNationalityModule { 
  employeeNationalityIndexId: number;
  employeeNationalityId: string;
  employeeNationalityDescription: string;
  employeeNationalityDescriptionArabic: string;


  constructor(employeeNationalityIndexId: number, employeeNationalityId: string, employeeNationalityDescription: string, employeeNationalityDescriptionArabic: string) {
      this.employeeNationalityIndexId = employeeNationalityIndexId;
      this.employeeNationalityId=employeeNationalityId;
      this.employeeNationalityDescription = employeeNationalityDescription;
      this.employeeNationalityDescriptionArabic = employeeNationalityDescriptionArabic;
  }
}
