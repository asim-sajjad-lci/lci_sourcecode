export class EmployeeBenefitMaintenance {
  id: number;
  employeeMaster: object;
  benefitCode: object;
  startDate: Date;
  endDate: Date;
  frequency: number;
  benefitMethod: number;
  benefitAmount: number;
  benefitPercent: number;
  transactionRequired: boolean;
  inactive: boolean;
  perPeriord: number;
  perYear: number;
  lifeTime: number;
  dtoPayCode: any;
  payFactor: any;
  noOfDays: any;
  endDateDays: any;
  benefitTypeId:number;
  roundOf: number;

  constructor(id: number, employeeMaster: object, benefitCode: object,
    startDate: Date, endDate: Date, frequency: number, benefitMethod: number,
    benefitAmount: number, benefitPercent: number, transactionRequired: boolean,
    inactive: boolean, perPeriord: number, perYear: number, lifeTime: number, dtoPayCode: any, payFactor: any, noOfDays: any,
    endDateDays: any, benefitTypeId:number,
    roundOf: number,) {
    this.id = id;
    this.employeeMaster = employeeMaster;
    this.benefitCode = benefitCode;
    this.startDate = startDate;
    this.endDate = endDate;
    this.frequency = frequency;
    this.benefitMethod = benefitMethod;
    this.benefitAmount = benefitAmount;
    this.benefitPercent = benefitPercent;
    this.transactionRequired = transactionRequired;
    this.inactive = inactive;
    this.perPeriord = perPeriord;
    this.perYear = perYear;
    this.lifeTime = lifeTime;
    this.dtoPayCode = dtoPayCode;
    this.payFactor = payFactor;
    this.noOfDays = noOfDays;
    this.endDateDays = endDateDays;
    this.benefitTypeId = benefitTypeId;
    this.roundOf = roundOf;
  }
}
