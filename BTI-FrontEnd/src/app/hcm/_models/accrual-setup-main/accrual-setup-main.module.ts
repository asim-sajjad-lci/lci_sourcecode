
export class AccrualSetupMain {
    id: number;
    accuralId: string;
    desc: string;
    arbic: string;
    numHour: number;
    maxAccrualHour: number;
    maxHour: number;
    accrualTyepId:number;
    workHour: number;
    numDay: number;
    period: number;
    reasons:number;
    type:number;


    constructor(id: number, accuralId: string, desc: string, arbic: string,
            numHour: number, maxAccrualHour: number, maxHour: number,accrualTyepId:number,workHour: number,
            numDay: number, period: number,reasons:number,type:number) {
            this.id = id;
            this.accuralId = accuralId;
            this.desc = desc;
            this.arbic = arbic;

            this.numHour = numHour;
            this.maxAccrualHour = maxAccrualHour;
            this.maxHour = maxHour;
            this.accrualTyepId=accrualTyepId;
            this.workHour = workHour;

            this.numDay = numDay;
            this.period = period;
            this.reasons=reasons;
            this.type=type;
            
    }

}


