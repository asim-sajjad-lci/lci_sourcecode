/**
 * A model for employee address master 
 */
export class EmployeeAddressMaster {

    employeeAddressIndexId: number;
    addressId: string;
    address1: string;
    address2: string;
    personalEmail: string;
    businessEmail: string;
    addressArabic1: string;
    addressArabic2: string;    
    pOBox: string;
    postCode: string;
    personalPhone: string;    
    businessPhone: string;
    otherPhone: string;
    locationLinkGoogleMap: string;
    city:string;
    cityArabic:string;
    country:string;
    countryArabic:string;
    ext:string;
    countryId:number;
    stateId:number;
    cityId:number;
    cityName: string;
    countryName: string;
    stateName: string;

    // initializing employee address master parameters

    constructor( employeeAddressIndexId: number, addressId: string, address1: string, address2: string,
        personalEmail: string, businessEmail: string, addressArabic1: string, addressArabic2: string, 
        pOBox: string, postCode: string, personalPhone: string, businessPhone: string, otherPhone: string, 
        locationLinkGoogleMap: string, city:string, cityArabic:string, country:string, countryArabic:string, ext:string,
                 countryId:number,stateId:number,cityId:number, cityName: string, countryName: string, stateName: string) {
        this.employeeAddressIndexId = employeeAddressIndexId;
        this.addressId = addressId;
        this.address1 = address1;
        this.address2 = address2;
        this.personalEmail = personalEmail;
        this.businessEmail = businessEmail;
        this.addressArabic1 = addressArabic1;
        this.addressArabic2 = addressArabic2;
        this.pOBox = pOBox;
        this.postCode =  postCode;
        this.personalPhone =  personalPhone;
        this.businessPhone = businessPhone;
        this.otherPhone = otherPhone;
        this.locationLinkGoogleMap =  locationLinkGoogleMap;
        this.city = city;
        this.cityArabic = cityArabic;
        this.country = country;
        this.countryArabic = countryArabic;
        this.ext = ext;
        this.countryId = countryId;
        this.stateId = stateId;
        this.cityId = cityId;
        this.cityName = cityName;
        this.countryName = countryName;
        this.stateName = stateName;
    }
}