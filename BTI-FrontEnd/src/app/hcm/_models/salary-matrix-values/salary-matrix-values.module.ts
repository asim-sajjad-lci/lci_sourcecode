import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SalaryMatrixValues { 
 
  rowSeq: number[];
  rowAmt: number[];
  rowDesc:string[];
  rowArabicDesc:string[];
  colDesc:string[];
  colArabicDesc:string[];
  


  constructor(rowSeq: number[],rowAmt: number[],rowDesc:string[],
    rowArabicDesc:string[],colDesc:string[],
    colArabicDesc:string[]) {

    this.rowSeq=rowSeq;
    this.rowAmt=rowAmt;
    this.rowDesc=rowDesc;
    this.rowArabicDesc=rowArabicDesc;
    this.colDesc=colDesc;
    this.colArabicDesc=colArabicDesc;

  }
}
