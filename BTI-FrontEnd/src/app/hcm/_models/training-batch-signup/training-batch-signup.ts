/**
 * A model for Training Course 
 */

export class TrainingBatchSignup {
    id: number;
    selectedEmployes: string;
    completeDate: string;
    courseComments : string;
    trainingClassStatus:number;
    employeeMaster:object[];
    trainingClass:object;
    dtoTrainingCourseDetail:object[];
    

    //initializing Training Course parameters
    constructor(
        id: number, 
        selectedEmployes: string,
        completeDate: string,
        courseComments : string,
        trainingClassStatus:number,
        employeeMaster:object[],
        trainingClass:object,
        dtoTrainingCourseDetail:object[]
    ) {
        this.id = id;
        this.selectedEmployes = selectedEmployes;
        this.completeDate = completeDate;
        this.courseComments = courseComments;
        this.trainingClassStatus = trainingClassStatus;
        this.employeeMaster = employeeMaster;
        this.trainingClass = trainingClass;
        this.dtoTrainingCourseDetail = dtoTrainingCourseDetail;     
        
    }
}