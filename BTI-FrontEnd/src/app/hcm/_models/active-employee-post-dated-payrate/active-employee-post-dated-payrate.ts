0/**
 * A model for department 
 */
export class ActiveEmployeePostDatedPayRateModule {
    id: number;
    ranges: string;
    fromDate: Date;
    toDate: Date;
    select: boolean;
    fromemployeeIndexId: number;
    toemployeeIndexId: number;
    fromPayCodeId: number;
    toPayCodeId: number;
    fromReasion: number;
    toReasion: number;
    active: boolean;

    //initializing department parameters
    constructor(id: number,
        ranges: string, fromDate: Date, toDate: Date, select: boolean, fromemployeeIndexId: number, toemployeeIndexId: number, 
        fromPayCodeId: number, toPayCodeId: number, fromReasion: number, toReasion: number) {
        this.id = id;
        this.ranges = ranges;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.select = select;
        this.fromemployeeIndexId = fromemployeeIndexId;
        this.toemployeeIndexId = toemployeeIndexId;
        this.fromPayCodeId = fromPayCodeId;
        this.toPayCodeId = toPayCodeId;
        this.fromReasion = fromReasion;
        this.toReasion = toReasion;
        this.active = false;

    }
}

export class ActiveEmployeePostDatedPayRateModulepayload {
    listActivateEmployeePostDatePayRate = [];


    //initializing department parameters
    constructor(listActivateEmployeePostDatePayRate) {
        this.listActivateEmployeePostDatePayRate = listActivateEmployeePostDatePayRate;


    }
}