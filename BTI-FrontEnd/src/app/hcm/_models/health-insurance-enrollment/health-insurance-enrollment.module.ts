
export class HealthInsuranceEnrollment {
    id:number;
    status:number;
	eligibilityDate:number;
	benefitStartDate:number;
	benefitEndDate:number;
	overrideCost:boolean;
	insuredAmount:number;
	costToEmployee:number;
	costToEmployeer:number;
	additionalCost:number;
	policyNumber:string;
	employeeMaster:{
        employeeIndexId : number;
        employeeId:any
    };
    helthInsurance:{
        id:number;
    }

    constructor(
        id:number,
        status:number,
        eligibilityDate:number,
        benefitStartDate:number,
        benefitEndDate:number,
        overrideCost:boolean,
        insuredAmount:number,
        costToEmployee:number,
        costToEmployeer:number,
        additionalCost:number,
        policyNumber:string,
        employeeMaster:{
            employeeIndexId : number;
            employeeId:any
        },
        helthInsurance:{
            id:number;
        }
    
    ) {
        this.id = id;
        this.status = status;
        this.eligibilityDate= eligibilityDate;
        this.benefitStartDate= benefitStartDate;
        this.benefitEndDate= benefitEndDate;
        this.overrideCost= overrideCost;
        this.insuredAmount= insuredAmount;
        this.costToEmployee= costToEmployee;
        this.costToEmployeer= costToEmployeer;
        this.additionalCost= additionalCost;
        this.policyNumber= policyNumber;
        this.employeeMaster= employeeMaster;
        this.helthInsurance= helthInsurance;
        
    }


}


