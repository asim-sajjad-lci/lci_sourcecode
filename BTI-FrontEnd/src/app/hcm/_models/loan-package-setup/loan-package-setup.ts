export class LoanPackageSetupModel {
  
   typeId: number;
   dtoMultiSelectList: any[];
  
   constructor(typeId: number, dtoMultiSelectList: any[]) {

      this.typeId = typeId;
      this.dtoMultiSelectList = dtoMultiSelectList;

   }
}