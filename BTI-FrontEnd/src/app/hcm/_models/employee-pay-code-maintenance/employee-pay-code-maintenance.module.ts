export class EmployeePayCodeMaintenanceModule {
    id: number;
    employeeMaster:object;
    payCode:object;
    payCodeType:object;
    baseOnPayCode:any;
    amount: any;
    payFactory: any;
    inactive: boolean;
    payPeriod: string;
    unitOfPay:string;
    payRate:any;
    baseOnPayCodeId:any;
    payTypeId: number;
    roundOf: number;

    constructor(id: number, employeeMaster: object, payCode: object, payCodeType:object,
        baseOnPayCode:any, amount: any, payFactory: any, 
        inactive: boolean, payPeriod: string, unitOfPay: string, payRate:any, baseOnPayCodeId:any,payTypeId: number,
        roundOf: number) {
        this.id = id;
        this.employeeMaster = employeeMaster;
        this.payCode = payCode;
        this.payCodeType = payCodeType;
        this.baseOnPayCode = baseOnPayCode;
        this.amount = amount;
        this.payFactory = payFactory;
        this.inactive = inactive;
        this.payPeriod = payPeriod;
        this.unitOfPay = unitOfPay;
        this.payRate = payRate;
        this.baseOnPayCodeId = baseOnPayCodeId;
        this.payTypeId = payTypeId;
        this.roundOf = roundOf;

    }

}
