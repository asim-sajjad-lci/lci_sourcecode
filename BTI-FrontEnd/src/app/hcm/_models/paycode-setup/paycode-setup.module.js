"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PaycodeSetupModule = (function () {
    function PaycodeSetupModule(id, payCodeId, description, arbicDescription, payType, baseOnPayCode, baseOnPayCodeAmount, payFactor, payRate, unitofPay, payperiod, inActive) {
        this.id = id;
        this.payCodeId = payCodeId;
        this.description = description;
        this.arbicDescription = arbicDescription;
        this.payType = payType;
        this.baseOnPayCode = baseOnPayCode;
        this.baseOnPayCodeAmount = baseOnPayCodeAmount;
        this.payFactor = payFactor;
        this.payRate = payRate;
        this.unitofPay = unitofPay;
        this.payperiod = payperiod;
        this.inActive = inActive;
    }
    return PaycodeSetupModule;
}());
exports.PaycodeSetupModule = PaycodeSetupModule;
//# sourceMappingURL=paycode-setup.module.js.map