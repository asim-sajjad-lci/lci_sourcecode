
export class PaycodeSetupModule {
    id: number;
    payCodeId: string;
    description: string;
    arbicDescription: string;
    payCodeTypeId: string;
    baseOnPayCode: any;
    baseOnPayCode1: any;
    baseOnPayCodeAmount: any;
    payFactor: any;
    payRate: any;
    unitofPay: string;
    payperiod: string;
    inActive: boolean;
    payCodeTypeDesc: string;
    baseOnPayCodeId:any;
    payTypeId: number;
    roundOf: number;

    constructor(id: number, payCodeId: string, description: string,
                arbicDescription: string, payCodeTypeId: string, baseOnPayCode: any, baseOnPayCode1: any,
                baseOnPayCodeAmount: any, payFactor: any, payRate: any,
                unitofPay: string, payperiod: string, inActive: boolean, payCodeTypeDesc: string, baseOnPayCodeId:any,payTypeId: number,roundOf: number) {
                this.id = id;
                this.payCodeId = payCodeId;
                this.description = description;
                this.arbicDescription = arbicDescription;
                this.payCodeTypeId = payCodeTypeId;
                this.baseOnPayCode = baseOnPayCode;
                this.baseOnPayCode1 = baseOnPayCode1;
                this.baseOnPayCodeAmount = baseOnPayCodeAmount;
                this.payFactor = payFactor;
                this.payRate = payRate;
                this.unitofPay = unitofPay;
                this.payperiod = payperiod;
                this.payCodeTypeDesc = payCodeTypeDesc;
                this.inActive =  inActive;
                this.payCodeTypeDesc = payCodeTypeDesc;
                this.baseOnPayCodeId = baseOnPayCodeId;
                this.payTypeId = payTypeId;
                this.roundOf = roundOf;

    }


}


