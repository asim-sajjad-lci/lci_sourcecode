/**
 * A model for Batches 
 */
export class BuildPayrollCheckBatch {
    include:boolean;
    id: number;
    batchId: any;
    description: string;
    userId: string;
    status: number;
    //initializing department parameters
    constructor(id: number, batchId: any, description: string,userId: string,status:number, include: boolean) {
        this.id = id;
        this.batchId = batchId;
        this.description = description;
        this.userId = userId;
        this.status = status;
        this.include=include;
    }
}