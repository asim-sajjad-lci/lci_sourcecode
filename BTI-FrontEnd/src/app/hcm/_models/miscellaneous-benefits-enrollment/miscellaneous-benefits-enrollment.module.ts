export class MiscellaneousBenefitsEnrollmentModule {
    id: number;
    frequency: number;
    startDate: Date;
    endDate: Date;
    inActive: boolean;
    method: number;
    dudctionAmount: number;
    dudctionPercent: number;
    monthlyAmount: number;
    yearlyAmount: number;
    lifetimeAmount: number;
    empluyeeerinactive: boolean;
    empluyeeermethod: number;
    benefitAmount: number;
    benefitPercent: number;
    employermonthlyAmount: number;
    employeryearlyAmount: number;
    employerlifetimeAmount: number;
    employeeMaster: object;
    dtoMiscellaneousBenefits: object;
    status: number;

    constructor(
        id: number,
        frequency: number,
        startDate: Date,
        endDate: Date,
        inActive: boolean,
        method: number,
        dudctionAmount: number,
        dudctionPercent: number,
        monthlyAmount: number,
        yearlyAmount: number,
        lifetimeAmount: number,
        empluyeeerinactive: boolean,
        empluyeeermethod: number,
        benefitAmount: number,
        benefitPercent: number,
        employermonthlyAmount: number,
        employeryearlyAmount: number,
        employerlifetimeAmount: number,
        employeeMaster: object,
        dtoMiscellaneousBenefits: object,
        status: number,
    ) {
        this.id = id;
        this.frequency = frequency;
        this.startDate = startDate;
        this.inActive=  inActive;
        this.endDate = endDate;
        this.method = method;
        this.dudctionAmount = dudctionAmount;
        this.dudctionPercent = dudctionPercent;
        this.monthlyAmount = monthlyAmount;
        this.yearlyAmount = yearlyAmount;
        this.lifetimeAmount = lifetimeAmount;
        this.empluyeeerinactive = empluyeeerinactive;
        this.empluyeeermethod = empluyeeermethod;
        this.benefitAmount = benefitAmount;
        this.benefitPercent = benefitPercent;
        this.employermonthlyAmount = employermonthlyAmount;
        this.employeryearlyAmount = employeryearlyAmount;
        this.employerlifetimeAmount = employerlifetimeAmount;
        this.employeeMaster = employeeMaster;
        this.dtoMiscellaneousBenefits = dtoMiscellaneousBenefits;
        this.status = status ;
    }
}
