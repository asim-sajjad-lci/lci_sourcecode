"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MiscellaneousBenefitsEnrollmentModule = (function () {
    function MiscellaneousBenefitsEnrollmentModule(id, frequency, startDate, endDate, inActive, method, dudctionAmount, dudctionPercent, monthlyAmount, yearlyAmount, lifetimeAmount, empluyeeerinactive, empluyeeermethod, benefitAmount, benefitPercent, employermonthlyAmount, employeryearlyAmount, employerlifetimeAmount, employeeMaster, dtoMiscellaneousBenefits, status) {
        this.id = id;
        this.frequency = frequency;
        this.startDate = startDate;
        this.inActive = inActive;
        this.endDate = endDate;
        this.method = method;
        this.dudctionAmount = dudctionAmount;
        this.dudctionPercent = dudctionPercent;
        this.monthlyAmount = monthlyAmount;
        this.yearlyAmount = yearlyAmount;
        this.lifetimeAmount = lifetimeAmount;
        this.empluyeeerinactive = empluyeeerinactive;
        this.empluyeeermethod = empluyeeermethod;
        this.benefitAmount = benefitAmount;
        this.benefitPercent = benefitPercent;
        this.employermonthlyAmount = employermonthlyAmount;
        this.employeryearlyAmount = employeryearlyAmount;
        this.employerlifetimeAmount = employerlifetimeAmount;
        this.employeeMaster = employeeMaster;
        this.dtoMiscellaneousBenefits = dtoMiscellaneousBenefits;
        this.status = status;
    }
    return MiscellaneousBenefitsEnrollmentModule;
}());
exports.MiscellaneousBenefitsEnrollmentModule = MiscellaneousBenefitsEnrollmentModule;
//# sourceMappingURL=miscellaneous-benefits-enrollment.module.js.map