import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class EmployeeQuickAssignmentModule {
  index: number;
  include: boolean;
  masterId: number;
  employeeId: number;
  code: string;
  codeId: number;
  desc: string;
  arabicDesc: string;
  method: number;
  percent: number;
  amount: number;
  payRate: number;
  empolyeeMaintananceId: any;
  startDate: Date;
  endDate: Date;
  inActive: boolean;
  isAmtType:boolean;
  amtInvalid:boolean=false;
  endDateInvalid:boolean=false;
  startDateInvalid:boolean=false;

  constructor(masterId: number,
    employeeId: number,
    code: string,
    codeId: number,
    desc: string,
    arabicDesc: string,
    method: number,
    percent: number,
    amount: number,
    payRate:number,
    empolyeeMaintananceId: any,
    startDate: Date,
    endDate: Date,
    inActive: boolean) {
    this.masterId = masterId;
    this.employeeId = employeeId;
    this.code = code;
    this.codeId = codeId;
    this.desc = desc;
    this.arabicDesc = arabicDesc;
    this.method = method;
    this.percent = percent;
    this.amount = amount;
    this.payRate = payRate;
    this.empolyeeMaintananceId = empolyeeMaintananceId;
    this.startDate = startDate;
    this.endDate = endDate;
    this.inActive = inActive;
  }
}
