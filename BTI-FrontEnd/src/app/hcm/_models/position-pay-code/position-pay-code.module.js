"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PositionPayCode = (function () {
    function PositionPayCode(id, positionPlanId, codeType, payCodeId, payRate, positionPlanDesc, positiondescription, positionId, positionPlan) {
        this.id = id;
        this.positionPlanId = positionPlanId;
        this.codeType = codeType;
        this.payCodeId = payCodeId;
        this.payRate = payRate;
        this.positionPlanDesc = positionPlanDesc;
        this.positiondescription = positiondescription;
        this.positionId = positionId;
        this.positionPlan = positionPlan;
    }
    return PositionPayCode;
}());
exports.PositionPayCode = PositionPayCode;
//# sourceMappingURL=position-pay-code.module.js.map