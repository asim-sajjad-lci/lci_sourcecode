
export class ActivitiMaster {
    activitiIndexId: number;
    activitiId:string;
    activitiTitle:string;
    activitiLastName:string;
    activitiFirstName:string;
    activitiMiddleName:string;
    activitiLastNameArabic:string;
    activitiFirstNameArabic:string;
    activitiMiddleNameArabic:string;    
    activitiHireDate:number;
    activitiAdjustHireDate:number;
    activitiLastWorkDate:number;
    activitiInactiveDate:number;
    activitiInactiveReason:string;
    activitiType:number;
    activitiGender:string;
    activitiMaritalStatus:string;
    activitiBirthDate:number;
    activitiUserIdInSystem:number;
    activitiWorkHourYearly:number;
    activitiCitizen:number;
    activitiInactive:number;
    activitiImmigration:number;
    divisionId:number;
    departmentId:number;
    locationId:number;
    supervisorId:number;
    positionId:number;
    activitiNationalitiesId:number;
    activitiAddressIndexId:number;

    activitiTitleArabic:string;
   
    activitiSmoker:number;



    constructor(
        activitiIndexId:number,
        activitiId:string,
        activitiTitle:string,
        activitiLastName:string,
        activitiFirstName:string,
        activitiMiddleName:string,
        activitiLastNameArabic:string,
        activitiFirstNameArabic:string,
        activitiMiddleNameArabic:string,      
        activitiHireDate:number,
        activitiAdjustHireDate:number,
        activitiLastWorkDate:number,
        activitiInactiveDate:number,
        activitiInactiveReason:string,
        activitiType:number,
        activitiGender:string,
        activitiMaritalStatus:string,
        activitiBirthDate:number,
        activitiUserIdInSystem:number,
        activitiWorkHourYearly:number,
        activitiCitizen:number,
        activitiInactive:number,
        activitiImmigration:number,
        divisionId:number,
        departmentId:number,
        locationId:number,
        supervisorId:number,
        positionId:number,
        activitiNationalitiesId:number,
        activitiAddressIndexId:number,
    
        activitiTitleArabic:string,
        activitiSmoker:number,
      
    ) {
        this.activitiIndexId = activitiIndexId;
        this.activitiId=activitiId;
        this.activitiTitle=activitiTitle;
        this.activitiLastName=activitiLastName;
        this.activitiFirstName=activitiFirstName;
        this.activitiMiddleName=activitiMiddleName;
        this.activitiLastNameArabic=activitiLastNameArabic;
        this.activitiFirstNameArabic=activitiFirstNameArabic;
        this.activitiMiddleNameArabic=activitiMiddleNameArabic;
        this.activitiHireDate=activitiHireDate;
        this.activitiAdjustHireDate=activitiAdjustHireDate;
        this.activitiLastWorkDate=activitiLastWorkDate;
        this.activitiInactiveDate=activitiInactiveDate;
        this.activitiInactiveReason=activitiInactiveReason;
        this.activitiType=activitiType;
        this.activitiGender=activitiGender;
        this.activitiMaritalStatus=activitiMaritalStatus;
        this.activitiBirthDate=activitiBirthDate,
        this.activitiUserIdInSystem=activitiUserIdInSystem;
        this.activitiWorkHourYearly=activitiWorkHourYearly;
        this.activitiCitizen=activitiCitizen;
        this.activitiInactive=activitiInactive;
        this.activitiImmigration=activitiImmigration;
        this.divisionId=divisionId;
        this.departmentId=departmentId;
        this.locationId=locationId;
        this.supervisorId=supervisorId;
        this.positionId=positionId;
        this.activitiNationalitiesId=activitiNationalitiesId;
        this.activitiAddressIndexId=activitiAddressIndexId;

        this.activitiTitleArabic = activitiTitleArabic;
      
        this.activitiSmoker= activitiSmoker;
      
      

    }


}


