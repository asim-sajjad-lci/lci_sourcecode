"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmployeeMaster = (function () {
    function EmployeeMaster(id, employeeId, employeeTitle, employeeLastName, employeeFirstName, employeeMiddleName, employeeLastNameArabic, employeeFirstNameArabic, employeeMiddleNameArabic, employeeHireDate, employeeAdjustHireDate, employeeLastWorkDate, employeeInactiveDate, employeeInactiveReason, employeeType, employeeGender, employeeMaritalStatus, employeeBirthDate, employeeUserIdInSystem, employeeWorkHourYearly, employeeCitizen, employeeInactive, employeeImmigration, divisionId, departmentId, locationId, supervisorId, positionId, employeeNationalitiesId, employeeAddressMasterId, employeeTitleArabic, employeeSmoker) {
        this.id = id;
        this.employeeId = employeeId;
        this.employeeTitle = employeeTitle;
        this.employeeLastName = employeeLastName;
        this.employeeFirstName = employeeFirstName;
        this.employeeMiddleName = employeeMiddleName;
        this.employeeLastNameArabic = employeeLastNameArabic;
        this.employeeFirstNameArabic = employeeFirstNameArabic;
        this.employeeMiddleNameArabic = employeeMiddleNameArabic;
        this.employeeHireDate = employeeHireDate;
        this.employeeAdjustHireDate = employeeAdjustHireDate;
        this.employeeLastWorkDate = employeeLastWorkDate;
        this.employeeInactiveDate = employeeInactiveDate;
        this.employeeInactiveReason = employeeInactiveReason;
        this.employeeType = employeeType;
        this.employeeGender = employeeGender;
        this.employeeMaritalStatus = employeeMaritalStatus;
        this.employeeBirthDate = employeeBirthDate,
            this.employeeUserIdInSystem = employeeUserIdInSystem;
        this.employeeWorkHourYearly = employeeWorkHourYearly;
        this.employeeCitizen = employeeCitizen;
        this.employeeInactive = employeeInactive;
        this.employeeImmigration = employeeImmigration;
        this.divisionId = divisionId;
        this.departmentId = departmentId;
        this.locationId = locationId;
        this.supervisorId = supervisorId;
        this.positionId = positionId;
        this.employeeNationalitiesId = employeeNationalitiesId;
        this.employeeAddressMasterId = employeeAddressMasterId;
        this.employeeTitleArabic = employeeTitleArabic;
        this.employeeSmoker = employeeSmoker;
    }
    return EmployeeMaster;
}());
exports.EmployeeMaster = EmployeeMaster;
//# sourceMappingURL=employee-master.module.js.map