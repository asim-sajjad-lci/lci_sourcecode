export class PayScheduleSetupLocation {
    id: number;
    description: any;
    locationId:any;
    arabicDescription:any;
    constructor(id : number,
        description: any,
        locationId:any,
        arabicDescription:any
    ) {
        this.id = id;
        this.locationId = locationId;
        this.description = description;
       this.arabicDescription = arabicDescription;
       }
}
