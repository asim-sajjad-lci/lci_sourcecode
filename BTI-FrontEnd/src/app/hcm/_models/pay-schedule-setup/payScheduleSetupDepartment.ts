export class PayScheduleSetupDepartment {
    id: number;
    departmentId: string;
    departmentDescription: string;
    arabicDepartmentDescription:any

    constructor(id : number,
        departmentId: string,
        departmentDescription: string,
        arabicDepartmentDescription:any
    ) {
        this.id = id;
        this.departmentId = departmentId;
        this.departmentDescription = departmentDescription;
        this.arabicDepartmentDescription = arabicDepartmentDescription;
    }
}
