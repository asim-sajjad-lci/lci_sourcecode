export class PayScheduleSetupPosition {
    
    id: number;
    description: any;
    arabicDescription: any;
    positionId:any;
   
    constructor(id : number,
        description: any,
        arabicDescription: any,
        positionId: any) {
        this.id = id;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.positionId = positionId;
      }
}
