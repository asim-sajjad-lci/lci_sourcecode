export class PayScheduleSetup {
    id:any;
    payScheduleIndexId: any;
    payScheduleId: any;
    payScheduleDescription: any;
    payScheduleDescriptionArabic: any;
    paySchedulePayPeriod:any;
    payScheduleBeggingDate:any;
    payScheduleYear:any;
    subItems:any[];
    payScheduleLocation:any[];
    payScheduleSetupPositions:any[];
    payScheduleSetupDepartments:any[];
    payScheduleSetupEmployees:any[];
    constructor(id : number,
        payScheduleIndexId:number,
        payScheduleId: any,
        payScheduleDescription: any,
        payScheduleDescriptionArabic: any,
        paySchedulePayPeriod:any,
        payScheduleBeggingDate:any,
        payScheduleYear:any,
        subItems:any[],
        payScheduleLocation:any[],
        payScheduleSetupPositions:any[],
        payScheduleSetupDepartments:any[],
        payScheduleSetupEmployees:any[]

    ) {
        this.id = id;
        this.payScheduleIndexId = payScheduleIndexId;
        this.payScheduleId = payScheduleId;
        this.payScheduleDescription = payScheduleDescription;
        this.payScheduleDescriptionArabic = payScheduleDescriptionArabic;
        this.paySchedulePayPeriod = paySchedulePayPeriod;
        this.payScheduleBeggingDate = payScheduleBeggingDate;
        this.payScheduleYear = payScheduleYear;
        this.subItems=subItems;
        this.payScheduleLocation = payScheduleLocation;
        this.payScheduleSetupPositions = payScheduleSetupPositions;
        this.payScheduleSetupDepartments = payScheduleSetupDepartments;
        this.payScheduleSetupEmployees = payScheduleSetupEmployees
       }
}
