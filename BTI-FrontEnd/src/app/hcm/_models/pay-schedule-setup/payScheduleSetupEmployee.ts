export class PayScheduleSetupEmployee {
    id: any;
    employeeId:any;
    employeeLastName: any;
    employeeFirstName: any;
    employeeMiddleName:any;
    constructor(id : any,
        employeeId:any,
        employeeLastName: any,
        employeeFirstName: any,
        employeeMiddleName:any
    ) {
        this.id = id;
        this.employeeId = employeeId;
        this.employeeLastName = employeeLastName;
        this.employeeFirstName = employeeFirstName;
        this.employeeMiddleName = employeeMiddleName;
       }
}
