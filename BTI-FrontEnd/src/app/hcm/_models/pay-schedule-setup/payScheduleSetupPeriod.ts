export class PayScheduleSetupPeriod {
    id: number;
    payscheduleSetupId: any;
    periodId: any;
    year: any;
    periodName:any;
    isRepeat:any;
    periodStartDate:any;
    periodEndDate:any;

    
    constructor(id : number,payscheduleSetupId: string,periodId: string,
        year: string,
        periodName:string,
        isRepeat:string,
        periodStartDate:string,
        periodEndDate:string
    ) {
        this.id = id;
        this.payscheduleSetupId = payscheduleSetupId;
        this.periodId = periodId;
        this.year = year;
        this.periodName = periodName;
        this.isRepeat=isRepeat;
        this.periodStartDate=periodStartDate;
        this.periodEndDate=periodEndDate;
       }
}
