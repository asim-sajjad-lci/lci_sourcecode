"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccrualSetup = (function () {
    function AccrualSetup(id, desc, type) {
        this.id = id;
        this.desc = desc;
        this.type = type;
    }
    return AccrualSetup;
}());
exports.AccrualSetup = AccrualSetup;
//# sourceMappingURL=accrual-setup.module.js.map