import { AccrualScheduleDetails } from '../accrual-schedule-details/accrual-schedule-details.module'

export class AccrualScheduleSetup {
        id: number;
        scheduleId: number;
        desc: string;
        arbicDesc: string;
        startDate: Date;
        endDate: Date;
        subItems: Array<AccrualScheduleDetails>;

        constructor(id: number, scheduleId: number, desc: string, arbicDesc: string,
                startDate: Date, endDate: Date, subItems: Array<AccrualScheduleDetails>) {
                this.id = id;
                this.scheduleId = scheduleId;
                this.desc = desc;
                this.arbicDesc = arbicDesc;
                this.startDate = startDate;
                this.endDate = endDate;
                this.subItems=subItems;
                
                /* 
                this.subItems=subItems;
                console.log("SUBITEM CONST: ",this.subItems); */
                /* subItems.forEach(subItem => {
                        console.log("SUBITEM INIT: ", subItems);
                        var temp = new AccrualScheduleDetails(
                                subItem.id,
                                subItem.scheduleId,
                                subItem.scheduleSeniority,
                                subItem.accrualSetupId,
                                subItem.description,
                                subItem.accrualType,
                                subItem.hours
                        );
                        this.subItems.push(temp);
                        console.log("SUBITEM CONST: ", this.subItems);
                }); */
        }
}

