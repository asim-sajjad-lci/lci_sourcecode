"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A model for division
 */
var Division = (function () {
    //initializing division parameters
    function Division(id, divisionId, divisionDescription, arabicDivisionDescription, divisionAddress, city, phoneNumber, fax, email) {
        this.id = id;
        this.divisionId = divisionId;
        this.divisionDescription = divisionDescription;
        this.arabicDivisionDescription = arabicDivisionDescription;
        this.divisionAddress = divisionAddress;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.fax = fax;
        this.email = email;
    }
    return Division;
}());
exports.Division = Division;
//# sourceMappingURL=division.js.map