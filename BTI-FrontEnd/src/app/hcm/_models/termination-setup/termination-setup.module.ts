export class TerminationSetupModule {
    id: number;
    terminationId: string;
    terminationDesc: string;
    terminationArbicDesc: string;
	terminationDetailId:number;
	predefinecheckListtypeId:number;
	orientationPredefinedCheckListItemId:number;
	subItems;

    // initializing Termination Class parameters
    constructor(id: number, terminationId: string, terminationDesc: string,
                terminationArbicDesc: string,terminationDetailId:number,predefinecheckListtypeId:number,orientationPredefinedCheckListItemId:number,subItems) {
        this.id = id;
        this.terminationId = terminationId;
        this.terminationDesc = terminationDesc;
        this.terminationArbicDesc = terminationArbicDesc;
		this.terminationDetailId=terminationDetailId;
		this.predefinecheckListtypeId=predefinecheckListtypeId;
		this.orientationPredefinedCheckListItemId=orientationPredefinedCheckListItemId;
		this.subItems=subItems;
    }
}
