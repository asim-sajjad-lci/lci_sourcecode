"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BenefitCodeSetup = (function () {
    function BenefitCodeSetup(id, benefitCodeId, description, arabicDescription, startDate, endDate, frequency, method, amount, transction, inActive, perPeriod, perYear, lifeTime) {
        this.id = id;
        this.benefitId = benefitCodeId;
        this.desc = description;
        this.arbicDesc = arabicDescription;
        this.startDate = startDate;
        this.endDate = endDate;
        this.frequency = frequency;
        this.method = method;
        this.amount = amount;
        this.transction = transction;
        this.inActive = inActive;
        this.perPeriod = perPeriod;
        this.perYear = perYear;
        this.lifeTime = lifeTime;
    }
    return BenefitCodeSetup;
}());
exports.BenefitCodeSetup = BenefitCodeSetup;
//# sourceMappingURL=benefit-code-setup.module.js.map