
export class BenefitCodeSetup {
        id: number;
        benefitId: number;
        desc: string;
        arbicDesc: string;
        startDate: Date;
        endDate: Date;
        frequency: number;
        method: number;
        amount: any;
        percent: string
        transction: boolean;
        inActive: boolean;
        perPeriod: string;
        perYear: string;
        lifeTime: string;
        dtoPayCode: any;
        payFactor: any;
        customDate: boolean;
        noOfDays: any;
        endDateDays: any;
        benefitTypeId:number;
        roundOf: number;

        constructor(id: number, benefitCodeId: number, description: string, arabicDescription: string,
                startDate: Date, endDate: Date, frequency: number, method: number,
                amount: any, percent: string, transction: boolean, inActive: boolean, perPeriod: string,
                perYear: string, lifeTime: string, dtoPayCode: any, payFactor: any, customDate: boolean,
                noOfDays: any,
                endDateDays: any, benefitTypeId:number,roundOf: number) {
                this.id = id;
                this.benefitId = benefitCodeId;
                this.desc = description;
                this.arbicDesc = arabicDescription;

                this.startDate = startDate;
                this.endDate = endDate;
                this.frequency = frequency;
                this.method = method;

                this.amount = amount;
                this.percent = percent;
                this.transction = transction;
                this.inActive = inActive;
                this.perPeriod = perPeriod;

                this.perYear = perYear;
                this.lifeTime = lifeTime;
                this.dtoPayCode = dtoPayCode
                this.payFactor = payFactor
                this.customDate = customDate;
                this.noOfDays = noOfDays;
                this.endDateDays = endDateDays;
                this.benefitTypeId = benefitTypeId;
                this.roundOf = roundOf;
        }

}


