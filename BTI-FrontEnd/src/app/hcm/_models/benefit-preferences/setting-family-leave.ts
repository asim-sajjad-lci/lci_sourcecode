/**
 * A model for SettingFamilyLeave 
 */
export class SettingFamilyLeave {
    id: number;
    method: number;
    isThisPeriod: number;
    periodStart: string;
   
    

    //initializing SettingFamilyLeave parameters
    constructor(
        id: number,
        method: number,
        isThisPeriod: number,
        periodStart: string
    
    ) {

        this.id = id;
        this.method = method;
        this.isThisPeriod= isThisPeriod;
        this.periodStart= periodStart;
       
   
        
    }
}