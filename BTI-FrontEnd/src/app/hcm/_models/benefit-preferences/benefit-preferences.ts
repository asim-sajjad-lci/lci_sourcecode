/**
 * A model for BenefitPreferences 
 */
export class BenefitPreferences {
    id: number;
    postEstimatedDate: number;
    postPaymentDates: number;
    workingdaysWeek: number;
    workingHoursDay: number;
    eligibilityDate: number;
    paperworkDeadlines: number;
      
    

    //initializing BenefitPreferences parameters
    constructor(
        id: number,
        postEstimatedDate: number,
        postPaymentDates: number,
        workingdaysWeek: number,
        workingHoursDay: number,
        eligibilityDate: number,
        paperworkDeadlines: number
    
    ) {

        this.id = id;
        this.postEstimatedDate = postEstimatedDate;
        this.postPaymentDates= postPaymentDates;
        this.workingdaysWeek= workingdaysWeek;
        this.workingHoursDay= workingHoursDay;
        this.eligibilityDate= eligibilityDate;
        this.paperworkDeadlines= paperworkDeadlines;
   
        
    }
}