import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class BuildChecks {
  id: number;
  defaultId: number;
  description: string;
  checkDate: Date;
  checkByUserId: string;
  typeofPayRun: number;
  dateFrom: Date;
  dateTo: Date;
  weekly: boolean;
  biweekly: boolean;
  semimonthly: boolean;
  monthly: boolean;
  quarterly: boolean;
  semiannually: boolean;
  annually: boolean;
  dailyMisc: boolean;
  allEmployees: boolean;
  fromEmployeeId: number;
  toEmployeeId: number;
  allDepartment: boolean;
  fromDepartmentId: number;
  toDepartmentId: number;
  dtoEmployeeMasters: Array<any>;
  dtoDepartments: Array<any>;


  constructor(id: number, defaultId: number, description: string, checkDate: Date, checkByUserId: string,
    typeofPayRun: number, dateFrom: Date, dateTo: Date, weekly: boolean, biweekly: boolean, semimonthly: boolean,
    monthly: boolean, quarterly: boolean, semiannually: boolean, annually: boolean, dailyMisc: boolean, 
    allEmployees: boolean, fromEmployeeId: number, toEmployeeId: number, allDepartment: boolean, fromDepartmentId: number,
    toDepartmentId: number, dtoEmployeeMasters: Array<any>, dtoDepartments: Array<any>) {
    this.id = id;
    this.defaultId = defaultId;
    this.description = description;
    this.checkDate = checkDate;
    this.checkByUserId = checkByUserId;
    this.typeofPayRun = typeofPayRun;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.weekly = weekly;
    this.biweekly = biweekly;
    this.semimonthly = semimonthly;
    this.monthly = monthly;
    this.quarterly = quarterly;
    this.semiannually = semiannually;
    this.annually = annually;
    this.dailyMisc = dailyMisc;
    this.allEmployees = allEmployees;
    this.fromEmployeeId = fromEmployeeId;
    this.toEmployeeId = toEmployeeId;
    this.allDepartment = allDepartment;
    this.fromDepartmentId = fromDepartmentId;
    this.toDepartmentId = toDepartmentId;
    this.dtoEmployeeMasters = dtoEmployeeMasters;
    this.dtoDepartments = dtoDepartments;
  }

}
