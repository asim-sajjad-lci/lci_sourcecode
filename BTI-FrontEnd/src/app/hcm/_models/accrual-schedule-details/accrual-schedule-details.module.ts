export class AccrualScheduleDetails {
        id:number;
        scheduleSeniority: number;
        accrualSetupId: number;
        description: string;
        accrualTypeId: number;
        hours: number;


        constructor(id:number, scheduleId:number, scheduleSeniority: number, accrualSetupId: number, description: string,
                accrualTypeId: number, hours: number) {
                this.id=id;
                this.scheduleSeniority = scheduleSeniority;
                this.accrualSetupId = accrualSetupId;
                this.description = description;
                this.accrualTypeId = accrualTypeId;
                this.hours = hours;

        }
}

