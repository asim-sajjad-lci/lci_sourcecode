"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PositionSetupModule = (function () {
    // initializing Position Class parameters
    function PositionSetupModule(id, positionId, description, arabicDescription, reportToPostion, positionClassId, skillSetId, postionLongDesc) {
        this.id = id;
        this.positionId = positionId;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.reportToPostion = reportToPostion;
        this.positionClassId = positionClassId;
        this.skillSetId = skillSetId;
        this.postionLongDesc = postionLongDesc;
    }
    return PositionSetupModule;
}());
exports.PositionSetupModule = PositionSetupModule;
//# sourceMappingURL=position-setup.module.js.map