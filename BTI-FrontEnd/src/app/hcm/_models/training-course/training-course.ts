/**
 * A model for Training Course 
 */
import { ClassDetails } from '../class-details/class-details'
export class TrainingCourse {
    id: number;
    traningId: string;
    desc: string;
    arbicDesc : string;
    location:string;
    prerequisiteId:string;
    courseInCompany:boolean;
    employeeCost:number;
    employerCost:number;
    supplierCost:number;
    instructorCost:number;
    subItems: Array<ClassDetails>;

    //initializing Training Course parameters
    constructor(
        id: number, 
        traningId: string,
        desc: string,
        arbicDesc : string,
        location:string,
        prerequisiteId:string,
        courseInCompany:boolean,
        employeeCost:number,
        employerCost:number,
        supplierCost:number,
        instructorCost:number,
        subItems: ClassDetails[]
    ) {
        this.id = id;
        this.traningId = traningId;
        this.desc = desc;
        this.arbicDesc = arbicDesc;
        this.location = location;
        this.prerequisiteId = prerequisiteId;
        this.courseInCompany = courseInCompany;
        this.employeeCost = employeeCost;
        this.employerCost = employerCost;
        this.supplierCost = supplierCost;
        this.instructorCost = instructorCost;
        this.subItems = subItems;
        
    }
}