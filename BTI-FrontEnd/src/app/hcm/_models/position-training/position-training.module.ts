export class PositionTrainingModule {
    id: number;
    positionId: string;
    positionTrainingDesc: string;
    positionTrainingArbicDesc: string;
    traningCourseId: number;
    traningClassId: number;
    courseDesc: string;
    classDesc: string;
	traningClassIdName:string;
	traningCourseIdName:string;
	traningClassName:string;


    // initializing Position Training parameters
    constructor(id: number, positionId: string, positionTrainingDesc: string,
                positionTrainingArbicDesc: string, traningCourseId: number, traningClassId: number,
                courseDesc: string, classDesc: string, traningClassIdName: string, traningCourseIdName: string, traningClassName: string) {
        this.id = id;
        this.positionId = positionId;
        this.positionTrainingDesc = positionTrainingDesc;
        this.positionTrainingArbicDesc = positionTrainingArbicDesc;
        this.traningCourseId = traningCourseId;
        this.traningClassId = traningClassId;
        this.courseDesc = courseDesc;
        this.classDesc = classDesc;
        this.traningClassIdName = traningClassIdName;
        this.traningCourseIdName = traningCourseIdName;
        this.traningClassName = traningClassName;
    }
}
