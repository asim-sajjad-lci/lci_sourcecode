"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PositionTrainingModule = (function () {
    // initializing Position Training parameters
    function PositionTrainingModule(id, positionId, positionTrainingDesc, positionTrainingArbicDesc, traningCourseId, traningClassId, courseDesc, classDesc) {
        this.id = id;
        this.positionId = positionId;
        this.positionTrainingDesc = positionTrainingDesc;
        this.positionTrainingArbicDesc = positionTrainingArbicDesc;
        this.traningCourseId = traningCourseId;
        this.traningClassId = traningClassId;
        this.courseDesc = courseDesc;
        this.classDesc = classDesc;
    }
    return PositionTrainingModule;
}());
exports.PositionTrainingModule = PositionTrainingModule;
//# sourceMappingURL=position-training.module.js.map