export class HealthInsSetup {
    id: number;
    helthInsuranceId: string;
    desc: string;
    arbicDesc: string;
    frequency: number;
    helthCoverageTypeId: number;
    helthCoverageDesc: string;
    compnayInsuranceId: number;
    groupNumber: string;
    maxAge: number;
    minAge: number;
    employeePay: number;
    employerPay: number;
    expenses: number;
    maxCoverage: number;
    helthInsurancePrimaryId: number;
    compnayInsurancePrimaryId: number;


  
    // initializing Life Insurance Parameter
    constructor(id: number, helthInsuranceId: string, desc: string, arbicDesc: string,
        frequency: number, helthCoverageTypeId: number, helthCoverageDesc: string, compnayInsuranceId: number, groupNumber: string, maxAge: number, 
        minAge: number, employeePay: number, employerPay: number, expenses: number, maxCoverage: number,
                helthInsurancePrimaryId: number,compnayInsurancePrimaryId: number) {
        this.id = id;
        this.helthInsuranceId = helthInsuranceId;
        this.desc = desc;
        this.arbicDesc = arbicDesc;
        this.frequency = frequency
        this.helthCoverageTypeId = helthCoverageTypeId;
        this.helthCoverageDesc = helthCoverageDesc;
        this.compnayInsuranceId = compnayInsuranceId;
        this.groupNumber = groupNumber;
        this.maxAge = maxAge;
        this.minAge = minAge;
        this.employeePay = employeePay;
        this.employerPay = employerPay;
        this.expenses = expenses;
        this.maxCoverage = maxCoverage;
        this.helthInsurancePrimaryId = helthInsurancePrimaryId;
        this.compnayInsurancePrimaryId = compnayInsurancePrimaryId;
    }
  }
  
  