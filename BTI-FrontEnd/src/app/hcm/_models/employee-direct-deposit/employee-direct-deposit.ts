/**
 * A model for employeeDirectDeposit 
 */


export class EmployeeDirectDeposit {
    id: number;
    listEmployeeDirectDeposit: Array<object>


    //initializing employeeDirectDeposit parameters
    constructor(
        id: number,
        listEmployeeDirectDeposit: Array<object>
    ) {
        this.id = id;
        this.listEmployeeDirectDeposit = listEmployeeDirectDeposit;

    }
}