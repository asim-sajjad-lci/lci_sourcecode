
export class ExitInterview {
    id: number;
    exitInterviewId: string;
    inActive: boolean;
    desc: string;
    arbicDesc: string;


    constructor(id: number, exitInterviewId: string, inActive: boolean, desc: string, arbicDesc: string) {
        this.id = id;
        this.exitInterviewId = exitInterviewId;
        this.inActive = inActive;
        this.desc = desc;
        this.arbicDesc =  arbicDesc;
    }


}


