"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HealthCoverageTypeSetup = (function () {
    function HealthCoverageTypeSetup(id, helthCoverageId, desc, arbicDesc) {
        this.id = id;
        this.helthCoverageId = helthCoverageId;
        this.desc = desc;
        this.arbicDesc = arbicDesc;
    }
    return HealthCoverageTypeSetup;
}());
exports.HealthCoverageTypeSetup = HealthCoverageTypeSetup;
//# sourceMappingURL=health-coverage-type-setup.module.js.map