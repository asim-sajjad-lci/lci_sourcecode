export class HealthCoverageTypeSetup {
    id: number;
    helthCoverageId: string;
    desc: string;
    arbicDesc: string;

    constructor(id: number, helthCoverageId: string, desc: string, arbicDesc: string) {
        this.id = id;
        this.helthCoverageId = helthCoverageId;
        this.desc = desc;
        this.arbicDesc = arbicDesc;
    }

}
