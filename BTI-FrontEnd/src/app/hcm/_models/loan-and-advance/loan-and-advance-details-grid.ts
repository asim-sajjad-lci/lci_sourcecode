export class LoanAndAdvancePaymentsDetailsGrid {

   loanDetailsId:number;
   monthYear:string;
   deductionAmount:string;
   isPostpone:boolean;
   status:number;
   isPaid:boolean;
   constructor(loanDetailsId:number,monthYear:string,deductionAmount:string,isPostpone:boolean, status:number,isPaid:boolean){

      this.loanDetailsId = loanDetailsId;
      this.monthYear = monthYear;
      this.deductionAmount = deductionAmount;
      this.isPostpone = isPostpone;
      this.status = status;
      this.isPaid = isPaid;
   }
}
