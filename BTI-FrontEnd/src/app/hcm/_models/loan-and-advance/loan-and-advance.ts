import { LoanAndAdvancePaymentsDetailsGrid } from "./loan-and-advance-details-grid";

export class LoanAndAdvancePayments {
   id:number;
   refId:string;
   transDate:string;
   employeeId: string;
   loanAmt:number;
   deductionPerMonthDefault:number;
   numOfMonths:number;
   startingFromMonth:string;
   startingFromYear:string;
   employeeName:string;
   hiringDate:string;
   position:string;
   site:string;
   totPackage:string;
   prevLoan:string;
   totDeduction:string;
   totRemaining:string;
   currentESB:string;
   transactionDate:string;
   startDate:string;
   endDate:string;
   employeeIdx:number;
   loanDetailsList:Array<LoanAndAdvancePaymentsDetailsGrid>

   constructor( id:number,refId:string,transDate:string,employeeId: string,loanAmt:number,deductionPerMonthDefault:number,numOfMonths:number,
      startingFromMonth:string,startingFromYear:string,employeeName:string,hiringDate:string,position:string,site:string,totPackage:string,prevLoan:string,totDeduction:string,totRemaining:string,currentESB:string,loanDetailsList:Array<LoanAndAdvancePaymentsDetailsGrid>) {
         this.id = id;
         this.refId = refId;
         this.transDate = transDate;
         this.employeeId = employeeId;
         this.loanAmt = loanAmt;
         this.deductionPerMonthDefault = deductionPerMonthDefault;
         this.numOfMonths = numOfMonths;
         this.startingFromMonth = startingFromMonth;
         this.startingFromYear = startingFromYear;
         this.employeeName = employeeName;
         this.hiringDate = hiringDate;
         this.position = position;
         this.site = site;
         this.totPackage = totPackage;
         this.prevLoan = prevLoan;
         this.totDeduction = totDeduction;
         this.totRemaining = totRemaining;
         this.currentESB = currentESB;
         this.loanDetailsList = loanDetailsList;
   }
}