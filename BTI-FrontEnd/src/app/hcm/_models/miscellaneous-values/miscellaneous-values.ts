/**
 * A model for Miscellaneous Values
 */
export class MiscellaneousValuesSetup {
   id: number;
   valueId: string;
   valueDescription: string;
   valueArabicDescription: string;
   miscId:number;
   codeName:string;

   //initializing Miscellaneous Values parameters
   constructor(id: number, valueId: string, valueDescription: string,
      valueArabicDescription: string,miscId:number,codeName:string) {
       this.id = id;
       this.valueId = valueId;
       this.valueDescription = valueDescription;
       this.valueArabicDescription = valueArabicDescription;
       this.miscId = miscId;
       this.codeName = codeName;
   }
}