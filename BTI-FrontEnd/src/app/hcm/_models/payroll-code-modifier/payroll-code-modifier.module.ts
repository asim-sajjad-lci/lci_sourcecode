/**
 * A model for Payroll Code Modifier 
 */
export interface PayrollCodeModifierModule {
    id: any;
    newCode: any;
}