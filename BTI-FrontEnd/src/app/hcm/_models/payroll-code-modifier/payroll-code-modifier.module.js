"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PayrollCodeModifierModule = (function () {
    // initializing Position Training parameters
    function PayrollCodeModifierModule(id, codeType, oldCode, newCode) {
        this.id = id;
        this.oldCode = oldCode;
        this.newCode = newCode;
        this.codeType = codeType;
    }
    return PayrollCodeModifierModule;
}());
exports.PayrollCodeModifierModule = PayrollCodeModifierModule;
//# sourceMappingURL=position-attachment.module.js.map