export class ClassDetails {
       // id:number;
        classId : string;
        className : string;
        instructorName : string;
        classLocation : string;
        maximum : number;
        enrolled : number;


        constructor(
               // id:number,
                classId : string,
                className : string,
                instructorName : string,
                classLocation : string,
                maximum : number,
                enrolled : number
        ) {
                //this.id=id;
                this.classId=classId;
                this.className = className;
                this.instructorName = instructorName;
                this.classLocation = classLocation;
                this.maximum = maximum;
                this.enrolled = enrolled;

        }
}

