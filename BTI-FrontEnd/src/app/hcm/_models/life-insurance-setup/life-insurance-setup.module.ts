/**
 * A model for department 
 */
export class LifeInsuranceSetup {
  id: number;
  lifeInsuraceId: string;
  lifeInsuraceDescription: string;
  lifeInsuraceDescriptionArabic: string;
  healthInsuranceGroupNumber: string;
  lifeInsuranceFrequency: number;
  amount: number;
  spouseAmount: number;
  childAmount: number;
  coverageTotalAmount: number;
  employeePay: number;
  startDate: Date;
  endDate: Date;
  insuranceType: number;
  healthInsuranceId: number;
  


  // initializing Life Insurance Parameter
  constructor(id: number, lifeInsuraceId: string, lifeInsuraceDescription: string, lifeInsuraceDescriptionArabic: string,
      healthInsuranceGroupNumber: string, lifeInsuranceFrequency: number, amount: number,
      spouseAmount: number, childAmount: number, coverageTotalAmount: number, employeePay: number,startDate: 
      Date, endDate: Date, insuranceType: number, healthInsuranceId:number,) {
      this.id = id;
      this.lifeInsuraceId = lifeInsuraceId;
      this.lifeInsuraceDescription = lifeInsuraceDescription;
      this.lifeInsuraceDescriptionArabic = lifeInsuraceDescriptionArabic;
      this.healthInsuranceGroupNumber = healthInsuranceGroupNumber;
      this.lifeInsuranceFrequency =  lifeInsuranceFrequency;
      this.amount = amount;
      this.spouseAmount = spouseAmount
      this.childAmount = childAmount;
      this.coverageTotalAmount = coverageTotalAmount;
      this.employeePay = employeePay;
      this.startDate = startDate;
      this.endDate = endDate;  
      this.insuranceType =  insuranceType;
      this.healthInsuranceId = healthInsuranceId; 
      
  }
}

