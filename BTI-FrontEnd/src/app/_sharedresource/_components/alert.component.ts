import { Component, Input, ViewContainerRef } from '@angular/core';

import { AlertService, Alert, AlertType } from '../_services/alert.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'alert',
    templateUrl: './alert.component.html'
})

export class AlertComponent {
    message: any;
    messageArray;
    messageObject;
    isVisible: boolean = true;

    @Input() id: string;

    alerts: Alert[] = [];

    constructor(
        private alertService: AlertService,
        public toastr: ToastsManager, vcr: ViewContainerRef
        ) {
            this.toastr.setRootViewContainerRef(vcr);
         }

    ngOnInit() {
        this.alertService.getMessage().subscribe(message => {
            this.isVisible = true;
            this.message = message;
            if (message) {
                this.messageObject = message.text.message;
                // converting json object to array
                this.messageArray = Object.keys(message.text.message).map(key => message.text.message[key]);
                
                if (message.type == "success") {
                    setTimeout(() => {
                        this.isVisible = false;
                    }, 6000);
                }
            }

        });

        this.alertService.getAlert(this.id).subscribe((alert: Alert) => {
            debugger;
            if (!alert.message) {
                // clear alerts when an empty alert is received
                this.alerts = [];
                return;
            }

            // add alert to array
            this.toastr.success(alert.message);
            this.alerts.push(alert);
            setTimeout(() => {
                                   this.removeAlert(alert);
                                }, 5000);
        });
    }

    removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }

        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-danger';
            case AlertType.Info:
                return 'alert alert-info';
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }
}