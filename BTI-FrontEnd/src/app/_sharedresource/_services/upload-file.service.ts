import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PositionSetupModule } from '../../hcm/_models/position-setup/position-setup.module';
import { PositionAttachmentModule } from '../../hcm/_models/position-attachment/position-attachment.module';

@Injectable()
export class UploadFileService {
    positionModelData:PositionSetupModule;
    positionAttchModelData:any;
    modelClear:boolean;

    positionData(positionData:any){
        this.positionModelData = positionData;
    }

    positionAttachData(positionAttach:PositionAttachmentModule, attachmentFile:any, isAttachedFile:boolean){

        this.positionAttchModelData = { 
            positionAttach: positionAttach, 
            attachmentFile: attachmentFile,
            isAttachedFile: isAttachedFile,
        };
    }
}
