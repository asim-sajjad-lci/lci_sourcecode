import { Injectable } from '@angular/core';

@Injectable()
export class BuildCheckCommonService {
  defaultId:number = 0;
  defaultIdDetailsForDistribution:object = {};
  batchesForProcess =   [];
  defaultDisplay:string = null;
  employeeDepartmentList:Array<any>;
  selectedEmployeeDepartment:Array<any>;
  modelData:any = null;
  pathFromBCID:boolean = false;
  pathFromBCIB:boolean = false;
  pathFromBCSBs:boolean = false;
  pathFromBCIPC:boolean = false;
  listEmployeeId: Array<any> = [];
  buildReport:any;

}
