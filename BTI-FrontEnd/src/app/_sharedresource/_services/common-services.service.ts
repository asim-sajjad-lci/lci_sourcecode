/**
 * A service class for Common Services
 */
import { Injectable, NgModuleFactoryLoader } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../_sharedresource/paged-data';
import { Page } from '../../_sharedresource/page';
import { Constants } from '../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { Button } from 'selenium-webdriver';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import { AuditTrial } from '../../financialModule/_models/general-ledger-configuration-setup/audit-trial';

declare var $: any;
@Injectable()
export class CommonService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getCompanyListUrl = Constants.userModuleApiBaseUrl + 'companyListByUserId';
    private getCountryListUrl = Constants.financialModuleApiBaseUrl + 'getCountryList';
    private getStateListByCountryIdUrl = Constants.financialModuleApiBaseUrl + 'getStateListByCountryId';
    private getCityListByStateIdUrl = Constants.financialModuleApiBaseUrl + 'getCityListByStateId';
    private getCountryCodeByCountryIdUrl = Constants.financialModuleApiBaseUrl + 'getCountryCodeByCountryId';
    private getGlAccountNumberListUrl = Constants.financialModuleApiBaseUrl + 'getGlAccountNumberList';
    private getGlAccountNumberListNewUrl = Constants.financialModuleApiBaseUrl + 'getGlAccountNumberListNew';
    private searchGlAccountNumberListNewUrl = Constants.financialModuleApiBaseUrl + 'searchGlAccountNumberListNew';
    private getMainAccountNumberUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
    private getDimensionValuesUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
    private getGlAccountNumberDetailByIdUrl = Constants.financialModuleApiBaseUrl + 'getGlAccountNumberDetailById';
    private getPriceLevelListUrl = Constants.financialModuleApiBaseUrl + 'getPriceLevelList';
    private checkGlAccountNumberUrl = Constants.financialModuleApiBaseUrl + 'checkGlAccountNumber';
    private companyListByUserIdUrl = Constants.userModuleApiBaseUrl + 'companyListByUserId';
    private createNewGlAccountNumberUrl = Constants.financialModuleApiBaseUrl + 'account/payables/createNewGlAccountNumber';
    private getAuditTrialCodeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchAuditTrialCode';
    private getGLNextJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getGLNextJournalEntry';
    private getUniqueReceiptNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getUniqueReceiptNumber';
    private getPaymentTermListUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchPaymentTermSetup';
    private getPaymentMethodTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getPaymentMethodType';
    private getShippingMethodListUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchShipmentMethodSetup';
    private getCreditCardSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchCreditCardSetup';
    private getCustomerByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/maintenanceGetById';
    private getSalesmanMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchSalesmanMaintenance';
    private getSalesmanMaintenanceByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/salesmanMaintenanceGetById';
    private getSalesTerritorySetupUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/search';
    private getSalesTerritorySetupByIdUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/getById';
    private getVatDetailSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchVatDetailSetup';
    private getExchangeTableSetupByCurrencyIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getExchangeTableSetupByCurrencyId';
    private getCurrencyExchangeDetailUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencyExchangeDetail';
    private getAllBatchesByTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBatchByTransactionTypeId';
    private getAllBatchesByPaymentMethodUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBatchByPaymentMethod';
    private getARBatchesByTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getBatchByTransactionTypeId';
    private getAPBatchesByTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getBatchByTransactionTypeId';
    private getCheckBookMaintenanceByCheckbookIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCheckBookMaintenanceByCheckbookId';
    private getActiveFAGeneralMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getActiveFAGeneralMaintenance';
    private searchActiveMainAccountSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchActiveMainAccountSetup';
    private getCustomerMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveCustomerMaintenance';
    private getAccountStructureSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountStructureSetup';
    private getCheckbookMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';
    private getDistributionAccountTypesListUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getDistributionAccountTypesList';
    private getVendorMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/find/Activevender';
    private getActiveSalesmanMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveSalesmanMaintenance';
    private getvendormaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/find/vender';
    private searchCurrencySetupsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
    private getvendormaintenanceByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/getOne/vender';
    private updateActiveSessionUrl = Constants.financialModuleApiBaseUrl + 'updateActiveSession';
    private getCurrencySetupDetailsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencySetupDetails';
    private checkTransactionDateIsValidUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/checkTransactionDateIsValid';
    private getVatDetailSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getVatDetailSetupById';
    private getAuditTrialByTransactionTypeUrl = Constants.financialModuleApiBaseUrl + '/getAuditTrialByTransactionType';
    private openWorkflowURL = Constants.userModuleApiBaseUrl + 'login/openWorkflow'

    userData: any = {};
    currentLanguage = "";
    dynamicHeader: any;
    changedValue;
    hasMsg: boolean = false;
    showMsg: boolean = false;
    isSuccessMsg: boolean = false;
    isfailureMsg: boolean = false;
    messageText;
    gridLists;
    moduleName;
    screenName;
    availableFormValues: [object];
    getScreenDetailArr;
    mandatory: boolean;
    isShowHideGrid = false;

    //initializing parameter for constructor
    constructor(private http: Http, private getScreenDetailService: GetScreenDetailService) {
        if (localStorage.getItem('currentUser')) {
            this.userData = JSON.parse(localStorage.getItem('currentUser'));
            this.headers.append('session', this.userData.session);
            this.headers.append('userid', this.userData.userId);
            this.currentLanguage = localStorage.getItem('currentLanguage') ?
                localStorage.getItem('currentLanguage') : "1";
            this.headers.append("langid", this.currentLanguage);
            this.headers.append("tenantid", localStorage.getItem('tenantid'));
        }

    }

    getDynamicHeader(TenantId: string) {
        var headers = new Headers({ 'content-type': 'application/json' });
        headers.append('session', this.userData.session);
        headers.append('userid', this.userData.userId);
        this.currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        headers.append("langid", this.currentLanguage);
        headers.append("tenantid", TenantId);
        this.dynamicHeader = headers;
    }

    //getting company list
    getCompanyList() {
        return this.http.post(this.getCompanyListUrl, { 'userId': this.userData.userId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get CountryList
    getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting details of  StateListByCountryId 
    getStateListByCountryId(countryId: string) {
        return this.http.post(this.getStateListByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting details of  CityListByStateId 
    getCityListByStateId(stateId: string) {
        return this.http.post(this.getCityListByStateIdUrl, { stateId: stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting details of  CountryCodeByCountryId 
    getCountryCodeByCountryId(countryId: string) {
        return this.http.post(this.getCountryCodeByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAccountStructureSetup() {
        return this.http.get(this.getAccountStructureSetupUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get GlAccountNumberList
    getGlAccountNumberList() {
        return this.http.get(this.getGlAccountNumberListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //get GlAccountNumberList
    getGlAccountNumberListNew() {
        return this.http.get(this.getGlAccountNumberListNewUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get GlAccountNumberList
    searchGlAccountNumberListNew(page:Page,searchKeyword) {
        return this.http.post(this.searchGlAccountNumberListNewUrl,{ 'searchKeyword': searchKeyword, 'pageNumber': page.pageNumber, 'pageSize': page.size } ,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //get GlAccountNumberList
    getGlAccountNumberListByCompany(companyId: string) {
        var headers = new Headers({ 'content-type': 'application/json' });
        headers.append('session', this.userData.session);
        headers.append('userid', this.userData.userId);
        this.currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        headers.append("langid", this.currentLanguage);
        headers.append("tenantid", companyId);
        return this.http.get(this.getGlAccountNumberListNewUrl, { headers: headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

        //get GlAccountNumberListNew
        getGlAccountNumberListByCompanyNew(companyId: string) {
            var headers = new Headers({ 'content-type': 'application/json' });
            headers.append('session', this.userData.session);
            headers.append('userid', this.userData.userId);
            this.currentLanguage = localStorage.getItem('currentLanguage') ?
                localStorage.getItem('currentLanguage') : "1";
            headers.append("langid", this.currentLanguage);
            headers.append("tenantid", companyId);
            return this.http.get(this.getGlAccountNumberListNewUrl, { headers: headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting details of  MainAccountNumber 
    getMainAccountNumber(mainAccountNumber: string) {
        return this.http.post(this.getMainAccountNumberUrl, { mainAccountNumber: mainAccountNumber }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting details of Dimension Values
    getDimensionValues(dimensionValue: string, segmentNumber: number) {
        return this.http
            .post(this.getDimensionValuesUrl, { 'dimensionValue': dimensionValue, 'segmentNumber': segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }


    //getting details of  GlAccountNumber By Id
    getGlAccountNumberDetailById(accountTableRowIndex: string) {
        return this.http.post(this.getGlAccountNumberDetailByIdUrl, { accountTableRowIndex: accountTableRowIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Gl AccountNumber Detail By Dynamic CompanyId
    getGlAccountNumberDetailByDynamicCompanyId(accountTableRowIndex: string, companyId: string) {
        var headers = new Headers({ 'content-type': 'application/json' });
        headers.append('session', this.userData.session);
        headers.append('userid', this.userData.userId);
        this.currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        headers.append("langid", this.currentLanguage);
        headers.append("tenantid", companyId);

        return this.http.post(this.getGlAccountNumberDetailByIdUrl, { accountTableRowIndex: accountTableRowIndex }, { headers: headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check Transaction Date Is Valid
    checkTransactionDateIsValid(RequestedData: any) {
        return this.http.post(this.checkTransactionDateIsValidUrl, JSON.stringify(RequestedData), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Price Level List
    getPriceLevelList() {
        return this.http.get(this.getPriceLevelListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting details of  GlAccountNumber 
    checkGlAccountNumber(accountNumber: string) {
        return this.http.post(this.checkGlAccountNumberUrl, { accountNumber: accountNumber }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting details of  companyList By UserId 
    companyListByUserId(userId: string) {
        return this.http.post(this.companyListByUserIdUrl, { userId: userId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Currency Setup detials
    getCurrencySetup() {
        return this.http.post(this.searchCurrencySetupsUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get vendor maintenance
    Getvendormaintenance() {
        return this.http.post(this.getvendormaintenanceUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get vendor maintenance By venderId
    GetvendormaintenanceById(venderId: string) {
        return this.http.post(this.getvendormaintenanceByIdUrl, { venderId: venderId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Active Main Account Setup
    searchActiveMainAccountSetup() {
        return this.http.post(this.searchActiveMainAccountSetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }



    //getting details of  NewGlAccountNumber 
    createNewGlAccountNumber(accountNumberList: string[]) {

        return this.http.post(this.createNewGlAccountNumberUrl, { accountNumberList: accountNumberList }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Audit Trial Code
    getAuditTrialCode() {
        return this.http.post(this.getAuditTrialCodeUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get GL Next JournalEntry
    getGLNextJournalEntry() {
        return this.http.get(this.getGLNextJournalEntryUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //Get Unique Receipt Number
    GetUniqueReceiptNumber() {
        return this.http.get(this.getUniqueReceiptNumberUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    // //Get Audit Trial Code
    // GetCheckbookMaintenanceList() {
    //     return this.http.post(this.getAuditTrialCodeUrl,{ 'searchKeyword': ''}, { headers: this.headers })
    //     .toPromise()
    //     .then(res => res.json())
    //     .catch(this.handleError);
    //  }

    //Get checkbook Maintenance
    GetCheckbookMaintenance() {
        return this.http.post(this.getCheckbookMaintenanceUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //Get Payment Term List
    GetPaymentTermList() {
        return this.http.post(this.getPaymentTermListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //Get Shipping Method List
    GetShippingMethodList() {
        return this.http.post(this.getShippingMethodListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Payment Method
    GetPaymentMethod() {
        return this.http.get(this.getPaymentMethodTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Credit Card List    
    GetCreditCardSetup() {
        return this.http.post(this.getCreditCardSetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get checkbook Maintenance
    GetCheckbookMaintenanceByDynamicTenantid(TenantId: string) {
        // var headers = new Headers({ 'content-type': 'application/json' });
        // headers.append('session', this.userData.session);
        // headers.append('userid', this.userData.userId);
        // this.currentLanguage=localStorage.getItem('currentLanguage')?
        //                     localStorage.getItem('currentLanguage'):"1";
        // headers.append("langid", this.currentLanguage);
        // headers.append("tenantid", TenantId);
        this.getDynamicHeader(TenantId);
        return this.http.post(this.getCheckbookMaintenanceUrl, { 'searchKeyword': '' }, { headers: this.dynamicHeader })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Customer List
    GetCustomerList() {
        return this.http.post(this.getCustomerMaintenanceListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Customer By Id
    GetCustomerById(customerId: string) {
        return this.http.post(this.getCustomerByIdUrl, { customerId: customerId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Salesman Maintenance
    GetSalesmanMaintenanceList() {
        return this.http.post(this.getSalesmanMaintenanceListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Active Salesman Maintenance
    GetActiveSalesmanMaintenanceList() {
        return this.http.post(this.getActiveSalesmanMaintenanceListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Salesman Maintenance By Id
    GetSalesmanMaintenanceById(salesmanId: string) {
        return this.http.post(this.getSalesmanMaintenanceByIdUrl, { salesmanId: salesmanId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Sales Territory Setup
    GetSalesTerritorySetupList() {
        return this.http.post(this.getSalesTerritorySetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Sales Territory Setup By Id
    GetSalesTerritorySetupById(salesTerritoryId: string) {
        return this.http.post(this.getSalesTerritorySetupByIdUrl, { salesTerritoryId: salesTerritoryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get VatDetail Setup
    GetVatDetailSetup() {
        return this.http.post(this.getVatDetailSetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Distribution Account Type List
    GetDistributionAccountTypesList() {
        return this.http.get(this.getDistributionAccountTypesListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting ExchangeTableSetup By CurrencyId
    getExchangeTableSetupByCurrencyId(currencyId: string) {
        return this.http
            .post(this.getExchangeTableSetupByCurrencyIdUrl, { currencyId: currencyId, 'pageNumber': 0, 'pageSize': 1000 }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Exchange Table SetupByCurrencyId
    GetCurrencyExchangeDetail(exchangeId: string) {

        return this.http
            .post(this.getCurrencyExchangeDetailUrl, { 'exchangeId': exchangeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get All BatchesList By TransactionType
    getAllBatchesListByTransactionType(transactionTypeId: number) {
        return this.http.post(this.getAllBatchesByTransactionTypeUrl, { 'transactionTypeId': transactionTypeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

    //get All Batches By Payment Method 
    getAllBatchesListByPaymentMethod(sourceDocumentId: number){
        return this.http.post(this.getAllBatchesByPaymentMethodUrl, { 'sourceDocumentID': sourceDocumentId }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //get AR BatchesList By TransactionType
    getARBatchesListByTransactionType(transactionTypeId: number) {
        return this.http.post(this.getARBatchesByTransactionTypeUrl, { 'transactionTypeId': transactionTypeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

    //get AR BatchesList By TransactionType
    getAPBatchesByTransactionType(transactionTypeId: number) {
        return this.http.post(this.getAPBatchesByTransactionTypeUrl, { 'transactionTypeId': transactionTypeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }
    //Get Checkbook List
    GetCheckBookMaintenanceByCheckbookId(checkBookId: string) {
        return this.http.post(this.getCheckBookMaintenanceByCheckbookIdUrl, { checkBookId: checkBookId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    GetCheckbookMaintenanceByCheckbookIdAndDynamicTenantid(checkBookId: string, TenantId: string) {
        // var headers = new Headers({ 'content-type': 'application/json' });
        // headers.append('session', this.userData.session);
        // headers.append('userid', this.userData.userId);
        // this.currentLanguage=localStorage.getItem('currentLanguage')?
        //                     localStorage.getItem('currentLanguage'):"1";
        // headers.append("langid", this.currentLanguage);
        // headers.append("tenantid", TenantId);
        this.getDynamicHeader(TenantId);
        return this.http.post(this.getCheckBookMaintenanceByCheckbookIdUrl, { checkBookId: checkBookId }, { headers: this.dynamicHeader })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Vendor List
    GetVendorList() {
        return this.http.post(this.getVendorMaintenanceListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Active FA General Maintenance
    GetActiveFAGeneralMaintenance() {
        return this.http.post(this.getActiveFAGeneralMaintenanceUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }



    //Get Active Session
    UpdateActiveSession() {

        return this.http.post(this.updateActiveSessionUrl, { 'userId': this.userData.userId, 'session': this.userData.session }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getCurrencySetupDetail(submitInfo) {
        return this.http.post(this.getCurrencySetupDetailsUrl, submitInfo, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get VatDetail Setup By VatId
    GetVatDetailSetupById(vatScheduleId: string) {
        return this.http.post(this.getVatDetailSetupByIdUrl, { vatScheduleId: vatScheduleId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    addCommas(nStr, seprator) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + seprator + '$2');
        }

        return x1 + x2;
    }

    closeMultiselect() {
        $(".c-btn").trigger("click")
    }
    closeMultiselectWithId(elemId) {
        console.log(elemId);
        //console.log( $("#" + elemId).find(".c-btn"));
        $("#" + elemId).find(".c-btn").trigger("click")
    }
    changeInputNumber(elemId) {

        var el = $("#" + elemId);
        el.prop('type', 'number');
        el.prop('step', '.001');
        el.prop("autocomplete", false); // remove autocomplete (optional)
        el.on('keyup', function (e) {
            var amt = $(this).val().split(".");
            if (amt.length == 1 && amt[0].length >= 20) {
                $(this).val(amt[0].slice(0, 20))
            }
            else {
                if (amt.length > 1 && amt[1].length >= 3) {
                    $(this).val(amt[0] + "." + amt[1].slice(0, 3))

                }
                if (amt.length > 1 && amt[0].length >= 20) {
                    $(this).val(amt[0].slice(0, 20) + "." + amt[1])

                }
            }
        });
        el.on('keydown', function (e) {
            var amt = $(this).val().split(".")
            var allowedKeyCodesArr = [9, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8, 37, 39, 109, 189, 46, 110, 190];  // allowed keys
            if ($.inArray(e.keyCode, allowedKeyCodesArr) === -1 && (e.keyCode != 17 && e.keyCode != 86)) {  // if event key is not in array and its not Ctrl+V (paste) return false;
                e.preventDefault();
            } else if ($(this).val().length >= 20 && e.keyCode != 8 && e.keyCode != 37 && e.keyCode != 39) {
                e.preventDefault()
            }

            else if ($.trim($(this).val()).indexOf('.') > -1 && $.inArray(e.keyCode, [110, 190]) != -1) {  // if float decimal exists and key is not backspace return fasle;
                e.preventDefault();
            } else {
                return true;
            };
        }).on('paste', function (e) {  // on paste
            var pastedTxt = e.originalEvent.clipboardData.getData('Text').replace(/[^0-9.]/g, '');  // get event text and filter out letter characters
            if ($.isNumeric(pastedTxt)) {  // if filtered value is numeric
                e.originalEvent.target.value = pastedTxt;
                e.preventDefault();
            } else {  // else 
                e.originalEvent.target.value = ""; // replace input with blank (optional)
                e.preventDefault();  // retur false
            };
        });
    }
    maxlenghtOnNumber(event) {
        var amt = event.split(".")
        if (amt.length == 1 && amt[0].length >= 6) {
            event = amt[0].slice(0, 6)
        }
        else {
            if (amt.length > 1 && amt[1].length >= 3) {
                event = amt[0] + "." + amt[1].slice(0, 3)
            }
            if (amt.length > 1 && amt[0].length >= 6) {
                event = amt[0].slice(0, 6) + "." + amt[1]
            }
        }
        return event
    }
    changeInputText(elemId) {
        $("#" + elemId).prop('type', 'text');
        $("#" + elemId).prop('step', '');
    }

    checkIsNumeric(inputtxt) {

        console.log(inputtxt)
        if (inputtxt != 'undefined' || inputtxt != '' || inputtxt != null || inputtxt != undefined) {
            return true
        } else {
            return false;
        }

    }

    formatAmount(data, inputValue) {
        if (data.status = "FOUND") {
            var originalinputValue = inputValue
            this.changedValue = data.result.currencySymbol
            if (data.result.includeSpaceAfterCurrencySymbol == true && data.result.displayCurrencySymbol == "2") {
                this.changedValue = " " + this.changedValue
            }
            if (data.result.includeSpaceAfterCurrencySymbol == true) {
                this.changedValue = this.changedValue + " "
            }
            if (data.result.displayCurrencySymbol == "1") {
                inputValue = this.changedValue + inputValue
            }
            if (data.result.displayCurrencySymbol == "2") {
                inputValue = inputValue + this.changedValue
            }
            if (originalinputValue < 0) {
                inputValue = inputValue.replace('-', "");
                if (data.result.negativeSymbol == 1) {
                    inputValue = '(' + inputValue + ')'
                } else {
                    if (data.result.displayNegativeSymbolSign == "1") {
                        inputValue = data.result.negativeSymbolValue + inputValue
                    }
                    if (data.result.displayNegativeSymbolSign == "2") {
                        inputValue = inputValue + data.result.negativeSymbolValue
                    }
                }

            }
            if (data.result.separatorsDecimal == "1") {
                inputValue = inputValue.toString().replace(/\./g, '.');
            }
            if (data.result.separatorsDecimal == "2") {
                inputValue = inputValue.toString().replace(/\./g, ',');
            }
            if (data.result.separatorsDecimal == "3") {
                inputValue = inputValue.toString().replace(/\./g, ' ');
            }
            if (data.result.separatorsThousands == "1") {
                inputValue = this.addCommas(inputValue, '.')
            }
            if (data.result.separatorsThousands == "2") {
                inputValue = this.addCommas(inputValue, ',')
            }
            if (data.result.separatorsThousands == "3") {
                inputValue = this.addCommas(inputValue, ' ')
            }
            return inputValue

        }
        else {
            this.hasMsg = true;
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.messageText = data.btiMessage.message;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 2000);
            return false;
        }
    }

    ExportData(Url: string): Observable<Object[]> {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        return Observable.create(observer => {

            let xhr = new XMLHttpRequest();
            xhr.open('GET', Url, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.setRequestHeader('Accept', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
                xhr.setRequestHeader('session', userData.session),
                xhr.setRequestHeader('userid', userData.userId),
                xhr.setRequestHeader('tenantid', localStorage.getItem('tenantid')),
                xhr.setRequestHeader('langid', '1'),
                xhr.responseType = 'blob';
            xhr.onreadystatechange = function () {

                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send();

        });
    }

    ExportDataForUpdate(Url: string, languageId: string): Observable<Object[]> {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        return Observable.create(observer => {

            var params = {
                'languageId': languageId
            }
            let xhr = new XMLHttpRequest();
            xhr.open('Post', Url, true);
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.setRequestHeader('Accept', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
                xhr.setRequestHeader('session', userData.session),
                xhr.setRequestHeader('userid', userData.userId),
                xhr.setRequestHeader('tenantid', localStorage.getItem('tenantid')),
                xhr.setRequestHeader('langid', '1'),

                xhr.responseType = 'blob';
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        var blob = new Blob([xhr.response], { type: contentType });
                        observer.next(blob);
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            xhr.send(JSON.stringify(params));
        });
    }

    allowDrop(ev, sourceGridIndex, sourceIndex, sourceColOrder) {
        ev.preventDefault();
        console.log('sourceIndex : ' + sourceIndex);
        // console.log('sourceFieldName : '+sourceFieldName);
        // console.log('sourceColOrder : '+sourceColOrder);
    }

    drag(ev, sourceGridIndex, sourceIndex, sourceColOrder) {
        ev.dataTransfer.setData("sourceGridIndex", sourceGridIndex);
        ev.dataTransfer.setData("sourceIndex", sourceIndex);
        ev.dataTransfer.setData("sourceColOrder", sourceColOrder);
    }

    drop(ev, destGridIndex, destIndex, destColOrder) {
        ev.preventDefault();
        var sourceGridIndex = ev.dataTransfer.getData("sourceGridIndex");
        var sourceIndex = ev.dataTransfer.getData("sourceIndex");
        var sourceColOrder = ev.dataTransfer.getData("sourceColOrder");

        this.gridLists[destGridIndex]['fieldDetailList'][destIndex]['colOrder'] = sourceColOrder.toString();
        this.gridLists[sourceGridIndex]['fieldDetailList'][parseInt(sourceIndex)]['colOrder'] = destColOrder.toString();
        this.gridLists[sourceGridIndex]['fieldDetailList'].sort((a, b) => {
            if (parseInt(a.colOrder) < parseInt(b.colOrder)) return -1;
            else if (parseInt(a.colOrder) > parseInt(b.colOrder)) return 1;
            else return 0;
        });
        console.log(this.gridLists);
    }

    markAsHideShow(gridIndex, index, value) {
        if (value) {
            this.gridLists[gridIndex]['fieldDetailList'][index]['isVisible'] = 1;
        } else {
            this.gridLists[gridIndex]['fieldDetailList'][index]['isVisible'] = 0;
        }

    }

    getScreenDetails(moduleCode, screenCode, defaultFormValues) {
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.screenGridDetail(moduleCode, screenCode).then(data => {

            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.gridLists = data.result.dtoScreenDetail.girdList;
            this.getScreenDetailArr = data.result;

            for (let i = 0; i < this.gridLists.length; i++) {
                this.gridLists[i]['fieldDetailList'].sort(function (a, b) {
                    var keyA = parseInt(a.colOrder),
                        keyB = parseInt(b.colOrder);
                    // Compare the 2 dates
                    if (keyA < keyB) return -1;
                    if (keyA > keyB) return 1;
                    return 0;
                });
            }

            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                this.mandatory = this.availableFormValues[j]['isMandatory'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['isMandatory'] = objAvailable['isMandatory'];
                if (objAvailable['listDtoFieldValidationMessage'] && objAvailable['isMandatory']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                objDefault['isMandatory'] = objAvailable['isMandatory'];
            }
        });
    }

    resetGrid(gridIndex, moduleCode, screenCode, defaultFormValues) {
        this.getScreenDetailService.getResetGrid({ gridId: this.gridLists[gridIndex]['gridId'] }).then(data => {
            console.log('getResetGrid Field:');
            console.log(data);
            this.getScreenDetails(moduleCode, screenCode, defaultFormValues);
        });
        this.isShowHideGrid = false;
    }

    saveConfiguration(gridIndex, moduleCode, screenCode, defaultFormValues) {
        this.isShowHideGrid = false;
        let PostData = [];
        var columnList = {};

        for (let index = 0; index < this.gridLists[gridIndex]['fieldDetailList'].length; index++) {
            const element = this.gridLists[gridIndex]['fieldDetailList'][index];
            columnList = {
                gridId: this.gridLists[gridIndex]['gridId'],
                fieldId: element['fieldId'],
                isVisible: element.isVisible,
                colOrder: element.colOrder
            }
            PostData.push(columnList);
        }
        console.log("change column visible status");
        console.log(PostData);
        if (PostData.length) {
            this.getScreenDetailService.getchangeColumnVisibleStatus(PostData).then(data => {
                console.log('getchangeColumnVisibleStatus Field:');
                console.log(data);
                this.getScreenDetails(moduleCode, screenCode, defaultFormValues);
            });
        }
    }

    getAuditTrialByTransactionType(auditTrial: AuditTrial) {
        return this.http.post(this.getAuditTrialByTransactionTypeUrl, auditTrial, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    openWorkflow() {
        var form = document.createElement("form");
        form.setAttribute("method", "get");
        form.setAttribute("action", this.openWorkflowURL);
        form.setAttribute("target", "view");
        document.head.setAttribute('userid', this.headers.get('userid'));
        document.head.setAttribute('session', this.headers.get('session'));
        document.head.setAttribute('tenantid', this.headers.get('tenantid'));


        var useridHhiddenField = document.createElement("input"); 
        useridHhiddenField.setAttribute("type", "hidden");
        useridHhiddenField.setAttribute("name", "client");
        useridHhiddenField.setAttribute("value", this.headers.get('algoras'));
        form.appendChild(useridHhiddenField);

        var useridHhiddenField = document.createElement("input"); 
        useridHhiddenField.setAttribute("type", "hidden");
        useridHhiddenField.setAttribute("name", "userid");
        useridHhiddenField.setAttribute("value", this.headers.get('userid'));
        form.appendChild(useridHhiddenField);

        var sessionHiddenField = document.createElement("input"); 
        sessionHiddenField.setAttribute("type", "hidden");
        sessionHiddenField.setAttribute("name", "session");
        sessionHiddenField.setAttribute("value", this.headers.get('session'));
        form.appendChild(sessionHiddenField);

        var tenantidHhiddenField = document.createElement("input"); 
        tenantidHhiddenField.setAttribute("type", "hidden");
        tenantidHhiddenField.setAttribute("name", "tenantid");
        tenantidHhiddenField.setAttribute("value", this.headers.get('tenantid'));
        form.appendChild(tenantidHhiddenField);

        document.body.appendChild(form);

        window.open('', 'view');

        form.submit();

    }

}
