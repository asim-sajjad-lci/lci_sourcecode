import { TestBed, inject } from '@angular/core/testing';

import { BuildCheckStorageService } from './build-check-storage.service';

describe('BuildCheckStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BuildCheckStorageService]
    });
  });

  it('should be created', inject([BuildCheckStorageService], (service: BuildCheckStorageService) => {
    expect(service).toBeTruthy();
  }));
});
