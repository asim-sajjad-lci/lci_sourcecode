import { Injectable } from '@angular/core';
import { BuildChecks } from '../../hcm/_models/build-checks/build-checks.module';
import * as cloneDeep from 'lodash/cloneDeep';

@Injectable()
export class BuildCheckStorageService {
  current:BuildChecks;
  defaultId:any;

  stored(buildCheck:BuildChecks, id:string){
    this.current = cloneDeep(buildCheck);
    this.defaultId = id;
    return true;
  }

  getStoredModule(): BuildChecks{
    console.log('current',this.current);
    return this.current;
    
  }

  getDefaultID(): any{
    return this.defaultId;
  }

  getBuildCheckID():number{
    return this.current.id;
  }

}
