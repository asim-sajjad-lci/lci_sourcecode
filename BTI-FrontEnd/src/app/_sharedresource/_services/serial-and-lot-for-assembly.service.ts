import { Injectable } from '@angular/core';

@Injectable()
export class SerialAndLotForAssemblyService {
  assemblyModel:any;
  serialScreenData:any;
  lotScreenData:any;
  siteId:any;
  assemblyOTY:any;
  moveFromSerialScreen:boolean = false;
}
