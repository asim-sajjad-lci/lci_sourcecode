import { Validator, AbstractControl, NG_VALIDATORS, Validators } from "@angular/forms";
import { Directive } from "@angular/core";


@Directive({
    selector: '[dateValidation]',
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: DateValidator,
        multi: true
    }]
})
export class DateValidator  implements Validator{
    validate(c: AbstractControl): {[key: string]: any} | null {
        const regex = /((0|1|2)\d{1}|[30])[/]((01|02|03|04|05|06|07|08|09|10|11|12))[/]((13|14|15)\d{2})/g;

       
        return (regex.test(c.value))?null:{'inValidDate':true};
    }
}