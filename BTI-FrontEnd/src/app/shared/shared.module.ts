import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LooseCurrencyPipe } from '../_sharedcomponent/looseCurrency.pipe';
import { NoCommaPipe } from "../_sharedresource/no-comma.pipe";
import { CreateMainAccountComponent } from '../_sharedcomponent/create-main-account/create-main-account.component';
import { FormsModule } from '@angular/forms';
import { CreateMainAccountPopupComponent } from '../_sharedcomponent/create-main-account-popup/create-main-account-popup.component';
import { SelectModule } from 'ng2-select';
import { SearchPopupComponent } from '../_sharedcomponent/search-popup/search-popup.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SelectModule,
    NgxDatatableModule
  ],
  declarations: [
    LooseCurrencyPipe,
    NoCommaPipe,
    CreateMainAccountComponent,
    CreateMainAccountPopupComponent,
    SearchPopupComponent

  ],
  exports: [
    LooseCurrencyPipe,
    NoCommaPipe,
    CreateMainAccountComponent,
    CreateMainAccountPopupComponent,
    SearchPopupComponent
  ],
})
export class SharedModule { }
