import { location } from './hcm/_models/employee-position-history/location';
import { Location } from '@angular/common';
import { Component, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {Constants} from './_sharedresource/Constants';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[]
}) 

export class AppComponent {
  msg:string;
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private getCommonConstantListUrl = Constants.userModuleApiBaseUrl+'getCommononConstantList';

  
  constructor( @Inject(DOCUMENT) private document: any,private http: Http,private router: Router,private location:Location,  private renderer: Renderer2,
  ) {
     var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1"; 
      this.headers.append("langid", currentLanguage);
        if(localStorage.getItem('currentUser') != '')
        {
            var userData = JSON.parse(localStorage.getItem('currentUser'));
            if(userData != null){
                this.headers.append('session', userData.session);
                this.headers.append('userid', userData.userId);
                this.headers.append("tenantid", localStorage.getItem('tenantid'))
                if(this.location.path() == '/login' ||this.location.path() == ''  )
                {
                  
                  this.router.navigate(["/dashboard"]);

                }else {
                  
                  this.router.navigate([this.location.path()]);
                }
                
            }
        }
        else{
          this.router.navigate(['login']);
        }
  }

  ngOnInit() {
    var currentLanguage = localStorage.getItem('currentLanguage');
    var languageOrientation = localStorage.getItem('languageOrientation');
    if (languageOrientation == 'LTR') {
      this.document.getElementById('admin-theme').setAttribute('href', 'assets/css/AdminLTE.min.css');
      this.document.getElementById('main-theme').setAttribute('href', 'assets/css/main.css');

      this.renderer.addClass(document.body, 'englishLang');
      this.renderer.removeClass(document.body, 'ArabicLang');
    }
    else if (languageOrientation == "RTL") {
      this.document.getElementById('admin-theme').setAttribute('href', 'assets/css/AdminLTE-rtl.min.css');
      this.document.getElementById('main-theme').setAttribute('href', 'assets/css/main-rtl.css');

      this.renderer.removeClass(document.body, 'englishLang');
  this.renderer.addClass(document.body, 'ArabicLang');
    }
    else {

      localStorage.setItem('currentLanguage', '1');
      localStorage.setItem('languageOrientation', 'LTR');
      this.document.getElementById('admin-theme').setAttribute('href', 'assets/css/AdminLTE.min.css');
      this.document.getElementById('main-theme').setAttribute('href', 'assets/css/main.css');
    }

  
    }
  
  
   getCommonConstantList() {
        return this.http
            .get(this.getCommonConstantListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   
   getAllConstantList()
    {
         this.getCommonConstantList().then(data => {
            Constants.atATimeText = data.result.atATimeText;
            Constants.selectedText=data.result.selectedText;
            Constants.totalText=data.result.totalText;
            Constants.deleteConfirmationText=data.result.deleteConfirmationText;
            Constants.serverErrorText=data.result.serverErrorText;
            Constants.deleteCompanyAssociatedMessage=data.result.deleteCompanyAssociatedMessage;
            Constants.deleteSuccessMessage=data.result.deleteSuccessMessage;
            Constants.butText=data.result.butText;
            Constants.ConfirmPasswordText=data.result.ConfirmPasswordText;
            Constants.alertmessage=data.result.alertmessage;
            Constants.validationMessage=data.result.validationMessage;
            Constants.selectAll=data.result.selectAll;
            Constants.unselectAll=data.result.unselectAll;
            Constants.selectUser=data.result.selectUser;
            Constants.selectDays=data.result.selectDays;
            Constants.selectRoles=data.result.selectRoles;
            Constants.selectCompany=data.result.selectCompany;
            Constants.selectUserGroup=data.result.selectUserGroup;
            Constants.select=data.result.select;
            Constants.activeUsers=data.result.activeUsers;
            Constants.inactiveUsers=data.result.inactiveUsers;
            Constants.userGroups=data.result.userGroups;
            Constants.roleGroups=data.result.roleGroups;
            Constants.roles=data.result.roles;
            Constants.passwordPolicy=data.result.passwordPolicy;
            Constants.invalidPassword=data.result.invalidPassword;
            Constants.requiredValid=data.result.requiredValid;
            Constants.tableViewtext=data.result.tableViewtext;
            Constants.msgText=data.result.msgText;
            Constants.firstdateGreaterMsg=data.result.firstdateGreaterMsg;
            Constants.lastdateGreaterMsg=data.result.lastdateGreaterMsg;
            Constants.quarter=data.result.quarter;
            Constants.firstQuarter=data.result.firstQuarter;
            Constants.secondQuarter=data.result.secondQuarter;
            Constants.thirdQuarter=data.result.thirdQuarter;
            Constants.forthQuarter=data.result.forthQuarter;
            Constants.selectAccount=data.result.selectAccount;
            Constants.accountNumberMissing=data.result.accountNumberMissing;
            Constants.confirmationModalTitle=data.result.confirmationModalTitle;
            Constants.confirmationModalBody=data.result.confirmationModalBody;
            Constants.deleteConfirmationModalBody=data.result.deleteConfirmationModalBody;
            Constants.OkText=data.result.OkText;
            Constants.CancelText=data.result.CancelText;
            Constants.EmptyMessage=data.result.EmptyMessage;
            Constants.fillAllFields=data.result.fillAllFields;
            Constants.ValidationToFromValue=data.result.ValidationToFromValue;
                Constants.minimumRange=data.result.minimumRange;
                Constants.maximumRange=data.result.maximumRange;
                Constants.fromPeriod=data.result.fromPeriod;
                Constants.toPeriod=data.result.toPeriod;
                Constants.toValue=data.result.toValue;
                Constants.toGreaterThan=data.result.toGreaterThan;
                Constants.toLessThan=data.result.toLessThan;
                Constants.onePeriod=data.result.onePeriod;
                Constants.twoPeriod=data.result.twoPeriod;
                Constants.lessThan=data.result.lessThan;
                Constants.greaterThan=data.result.greaterThan;
                Constants.maxrangeGreater=data.result.maxrangeGreater;
                Constants.greaterorEqualValue=data.result.greaterorEqualValue;
                Constants.minrange=data.result.minrange;
                Constants.agingPeriod=data.result.agingPeriod;
                Constants.sevenAgingPeriod=data.result.sevenAgingPeriod;
                Constants.missingAccount=data.result.missingAccount;
                Constants.yearAndSeries=data.result.yearAndSeries;
                Constants.oneRadio=data.result.oneRadio;
                Constants.InvalidDate=data.result.InvalidDate;
                Constants.InvalidMonth=data.result.InvalidMonth;
                Constants.InvalidYear=data.result.InvalidYear;
                Constants.InvalidDay=data.result.InvalidDay;
                Constants.mustBe=data.result.mustBe;
                Constants.footervalue="jhsajdasjhd"; 
        });
    }  

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}
