
import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginService } from '../../loginModule/_services/login.service';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../_sharedresource/_services/alert.service';
import {Constants} from '../../_sharedresource/Constants';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    providers: [LoginService]
})
//export to make it available for other classes
export class ForgotPasswordComponent {
    moduleCode = Constants.userModuleCode;
    screenCode = "S-1001";
    screenName;
    moduleName;
    defaultFormValues: any = [];
    availableFormValues: any = [];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    username;
    email;
    password;
    isActive: number;
    currentLanguage: string;
    arrLanguage:[object];
   

    constructor(
        private router: Router,
        private loginService: LoginService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService
    ) {
        //defaultFormValues for forgotpassword screen
        this.defaultFormValues = [
            { 'fieldName': 'FORGOT_PASS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
            { 'fieldName': 'EMAIL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
            { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
            { 'fieldName': 'SUBMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
        ];
    }

    // Screen initialization 
    ngOnInit() {

    this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
     {
        this.arrLanguage = data
     });  
      this.currentLanguage = localStorage.getItem('currentLanguage');
    // getting forgot password screen
     this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
            this.moduleName=data.result.moduleName;
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                if (objDefault) {
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        //setting active language
        var currentLanguage = localStorage.getItem('currentLanguage');
        if (currentLanguage == "1") {
            this.isActive = 1;
        }
        else if (currentLanguage == "2") {
            this.isActive = 2;
        }
        else {
            this.isActive = 1;
        }
    }

    //function call for sending email with password
    getPassword() {
        this.loginService.forgotPassword(this.model.email).then(data => {
            var code = data.code;
            if (code == "200") {
                window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                }, 100);
                        this.hasMsg = true;
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                        this.router.navigate(['login']);
                }, 4000);
                }
            else if (code == "404") {
                window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                }, 100);
                        this.hasMsg = true;
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
            } else {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.hasMsg = true;
            }
        return false; 
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }

       ChangeLanguage() {
        localStorage.setItem('currentLanguage',  this.currentLanguage);
        this.getScreenDetailService.getLanguageById(this.currentLanguage).then(data => {
              localStorage.setItem('languageOrientation',  data.result.languageOrientation);
               window.location.reload();
         });
     }
}