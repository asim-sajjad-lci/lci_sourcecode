
import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginService } from '../../loginModule/_services/login.service';
import { ResetPassword } from '../_models/resetpassword/resetpassword';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../_sharedresource/Constants';

@Component({
  selector: 'resetpassword',
  templateUrl: './reset-password.component.html',
  providers: [LoginService]
  
})
export class ResetPasswordComponent {
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1027";
  screenName;
  moduleName;
  defaultFormValues: [object];
  availableFormValues: [object];
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  model: any = {};
  userId: string;
  password:string;
  newpassword :string;
  confirmpassword:string;
  passwordPolicy=Constants.passwordPolicy;
  invalidPassword=Constants.invalidPassword;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private loginService: LoginService,
    private getScreenDetailService: GetScreenDetailService,
  ) {
    //defaultFormValues for reset password
    this.defaultFormValues = [
            { 'fieldName': 'RESET_PASSWORD_LABEL', 'fieldValue': '', 'helpMessage': '' ,'listDtoFieldValidationMessage':'' },
            { 'fieldName': 'RESET_PASSWORD_NEW_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage':'' },
            { 'fieldName': 'RESET_PASSWORD_CONFIRM_PASSWORD', 'fieldValue': '', 'helpMessage': '' , 'listDtoFieldValidationMessage':'' },
            { 'fieldName': 'RESET_PASSWORD_UPDATE_PASSWORD_BUTTON', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage':'' },
    ];
  }

  // Screen initialization
  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
    this.model.userId = params['userId'];
      });
   
      //getting screen for login
      this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
      this.screenName=data.result.dtoScreenDetail.screenName
      this.moduleName=data.result.moduleName;
      this.availableFormValues = data.result.dtoScreenDetail.fieldList;
      for (var j = 0; j < this.availableFormValues.length; j++) {
        var fieldKey = this.availableFormValues[j]['fieldName'];
        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
        var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
        objDefault['fieldValue'] = objAvailable['fieldValue'];
        objDefault['helpMessage'] = objAvailable['helpMessage'];
        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
      }
    });
  }

//function call for reset password
resetPassword()
     {
    if(this.model.password != this.model.confirmpassword)
      {
          window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "confirm password does not match";
          }, 100);
                this.hasMsg = true;
          window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
          }, 4000);
    return false;
      }
      this.loginService.resetPassword(this.model.userId, this.model.password).then(data => {
            var code = data.code;
          if (code == "200") {
                  window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                  }, 100);
                        this.hasMsg = true;
                  window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                        location.href='dashboard';
                  }, 4000)
                  }
                }).catch(error => {
                 this.hasMsg = true;
                 window.setTimeout(() => {
                 this.isSuccessMsg = false;
                 this.isfailureMsg = true;
                 this.showMsg = true;
                 this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                 }, 100)
                 });
                 }
                 }