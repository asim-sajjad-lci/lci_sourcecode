
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { LoginService } from '../../loginModule/_services/login.service';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../_sharedresource/_services/alert.service';
import {Constants} from '../../_sharedresource/Constants';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  providers: [LoginService]
  
})
export class LoginComponent {
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1000";
  screenName;
  defaultFormValues: any = [];
  availableFormValues: any = [];
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  model: any = {};
  MyIP;
  userId: number;
  otp: string;
  isResetPassword:string;
  isActive: number;
  currentLanguage: string;
  arrLanguage:[object];
  atATimeText=Constants.atATimeText;
  constructor(
    private router: Router,
    private loginService: LoginService,
    private getScreenDetailService: GetScreenDetailService,
    private cookieService: CookieService,
    private alertService: AlertService
  ) {
    //defaultFormValues for login screen
    this.defaultFormValues = [
      { 'fieldName': 'LOGIN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'REMEMBER_ME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'FORGOT_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'LOGIN_BUTTON', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' }
    ];
  }

  // Screen initialization 
  ngOnInit() {

    //getting data from cookie
    const cookieUsernameExists: boolean = this.cookieService.check('username');
    const cookiePasswordExists: boolean = this.cookieService.check('password');
    
     if(cookieUsernameExists && cookiePasswordExists)
     {
       this.model.remember=true;
       this.model.username = this.cookieService.get('username');
       this.model.password = this.cookieService.get('password');
     }
     this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
     {
              this.arrLanguage = data
     });
  
     this.currentLanguage = localStorage.getItem('currentLanguage');
    //getting screen for login
    this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
      
    this.screenName=data.result.dtoScreenDetail.screenName
    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
      for (var j = 0; j < this.availableFormValues.length; j++) {
        var fieldKey = this.availableFormValues[j]['fieldName'];
        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
        var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
        objDefault['fieldValue'] = objAvailable['fieldValue'];
        objDefault['helpMessage'] = objAvailable['helpMessage'];
        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
      }
    });

  


  //setting active language
  var currentLanguage = localStorage.getItem('currentLanguage');
  if (currentLanguage == "1") {
      this.isActive = 1;
    }
  else if (currentLanguage == "2") {
      this.isActive = 2;
    }
  else {
      this.isActive = 1;
    }

  }

 //check Ip for system
 checkMyIp()
 {
   this.loginService.checkMyIp().then(data => {
         this.MyIP=data.result;
    });
 }

  //function call for loggedin user
  Login() {
    this.loginService.loginUserForOTP(this.model.username, this.model.password).then(data => {
        if(data.result != undefined && data.result != 'undefined')
        {
          this.userId = data.result.userId;
          this.otp = data.result.otp;
          this.isResetPassword=data.result.isResetPassword;
         if (this.model.remember) {
          // storing data in cookie
           this.cookieService.set( 'username', this.model.username );
           this.cookieService.set( 'password', this.model.password );
          }
          else{
            const cookieUsernameExists: boolean = this.cookieService.check('username');
            const cookiePasswordExists: boolean = this.cookieService.check('password');
             if(cookieUsernameExists && cookiePasswordExists)
             {
               this.cookieService.delete('username');
               this.cookieService.delete('password');
             }
          }
          // storing credential
          var userCredential={'username':this.model.username,'password':this.model.password};
          localStorage.setItem('userCredential',JSON.stringify(userCredential));
          this.router.navigate(['verifyotp', this.userId, this.otp,this.isResetPassword]);  
        //  if(data.result.isResetPassword== 'Y')
        //  {
        //   this.router.navigate(['resetpassword', this.userId]);
        //   this.router.navigate(['verifyotp', this.userId, this.otp,this.isResetPassword]); 
        //  }
         }
        else
        {
          this.hasMsg = true;
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = data.btiMessage.message;
        return false;
        }
    }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
     });
    }

     ChangeLanguage() {
        localStorage.setItem('currentLanguage',  this.currentLanguage);
        this.getScreenDetailService.getLanguageById(this.currentLanguage).then(data => {
              localStorage.setItem('languageOrientation',  data.result.languageOrientation);
               window.location.reload();
         });
     }

  //event fire for language change
  // ChangeLanguage(event) {
  //   event.preventDefault();
  //   var currentLanguage = event.target.attributes[0].value;
  //   if (currentLanguage == "1") {
  //     this.isActive = 1;
  //   }
  //   else if (currentLanguage == "2") {
  //     this.isActive = 2;
  //   }
  //   else {
  //     this.isActive = 1;
  //   }
  //   localStorage.setItem('currentLanguage', currentLanguage);
  //   window.location.reload();
  // }
}