import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginService } from '../../loginModule/_services/login.service';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../_sharedresource/_services/alert.service';
import {Constants} from '../../_sharedresource/Constants';

@Component({
  selector: 'verify-otp',
  templateUrl: './verify-otp.component.html',
  providers: [LoginService]
})
export class VerifyOtpComponent {
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1002";
  screenName;
  defaultFormValues: [object];
  availableFormValues: [object];
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  model: any = {};
  username;
  password;
  isResetPassword;
  isActive: number;
   currentLanguage: string;
  arrLanguage:[object];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private loginService: LoginService,
    private getScreenDetailService: GetScreenDetailService,
    private alertService: AlertService
  ) {
    //defaultFormValues for verify OTP screen
    this.defaultFormValues = [
      { 'fieldName': 'OTP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'ENTER_OTP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'RESEND', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'SUBMIT_BUTTON', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
    ];
  }

  //for initializing screen
  ngOnInit() {

     this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
     {
        this.arrLanguage = data
     });

     this.currentLanguage = localStorage.getItem('currentLanguage');
  // getting screen
    this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
      this.screenName=data.result.dtoScreenDetail.screenName
      this.availableFormValues = data.result.dtoScreenDetail.fieldList;
      for (var j = 0; j < this.availableFormValues.length; j++) {
        var fieldKey = this.availableFormValues[j]['fieldName'];
        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
        var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
        objDefault['fieldValue'] = objAvailable['fieldValue'];
        objDefault['helpMessage'] = objAvailable['helpMessage'];
        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
      }
    });

    // subscribe to router event
    this.route.params.subscribe((params: Params) => {
      this.model.userId = params['userId'];
      this.model.otp = params['otp'];
      this.isResetPassword = params['isResetPassword'];
    });

    // setting username and password to resend
    var userCredential = JSON.parse(localStorage.getItem('userCredential'));
    this.username=userCredential.username;
    this.password=userCredential.password;

    // setting active language
    var currentLanguage = localStorage.getItem('currentLanguage');
    if (currentLanguage == "1") {
      this.isActive = 1;
    }
    else if (currentLanguage == "2") {
      this.isActive = 2;
    }
    else {
      this.isActive = 1;
    }
  }

 //function call for verify otp
  verifyOTP() {
     
   this.loginService.verifyOtpAuthentication(this.model.userId, this.model.otp).then(data => {
     var code = data.code;
      
      if (code == "200") {
         localStorage.setItem('currentUser', JSON.stringify(data.result));
         localStorage.setItem('isResetPassword', this.isResetPassword);
         localStorage.setItem('tenantid', '');
         
         if(data.result.role == 'USER')
         {
            this.router.navigate(["selectcompany"]);
         }
         else{
           if(this.isResetPassword == 'Y')
           {
            this.router.navigate(['resetpassword', data.result.userId]);  
           }
           else
           {
              location.href='dashboard';
           }
           
         }
          // if(this.isResetPassword == 'Y')
          //  {
          //   this.router.navigate(['resetpassword', data.result.userId]);  
          //  }
          //  else
          //  {
          //    location.href='dashboard';
          //  }
      }
      else {
         if(data.result.otpMaxLimitReached != undefined && data.result.otpMaxLimitReached == true)
         {
           this.isSuccessMsg = false;
           this.isfailureMsg = true;
           this.hasMsg = true;
           this.showMsg = true;
           this.messageText =data.btiMessage.message;
            window.setTimeout(() => {
                this.router.navigate(['login']);
            }, 2000);
         }
          // if(data.btiMessage.messageShort == "INVALID_OTP")
          // {
          //   this.router.navigate(['login']);
          // }
           localStorage.setItem('currentUser', JSON.stringify(data.result));
           this.isSuccessMsg = false;
           this.isfailureMsg = true;
           this.hasMsg = true;
           this.showMsg = true;
           this.messageText =data.btiMessage.message;
            window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
            }, 2000);
           if(this.isResetPassword == 'Y')
           {
            this.router.navigate(['resetpassword', data.result.userId]);  
           }
      }
       });
  }
       
     //function call for resending otp
     Resend() {
      this.loginService.loginUserForOTP(this.username, this.password).then(data => {
            var code = data.code;
        
                  window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.model.otp = data.result.otp;
                        this.messageText = data.btiMessage.message;
                  }, 100);
                        this.hasMsg = true;
                  window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                       }, 4000)
                  
      }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
      }, 100)

    });
  }

     ChangeLanguage() {
        localStorage.setItem('currentLanguage',  this.currentLanguage);
        this.getScreenDetailService.getLanguageById(this.currentLanguage).then(data => {
              localStorage.setItem('languageOrientation',  data.result.languageOrientation);
               window.location.reload();
         });
     }

  

}