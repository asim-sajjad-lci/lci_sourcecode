import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SelectCompanyService } from '../../loginModule/_services/selectCompany.service';
import { SettingsService } from '../../userModule/_services/settings/settings.service';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../_sharedresource/Constants';
declare var $:any
@Component({
  selector: 'selectCompany',
  templateUrl: './selectCompany.component.html',
  providers: [SelectCompanyService,SettingsService]
  
})

export class SelectCompanyComponent {
    screenCode = "S-1227";
    screenName;
    ddl_MultiSelectCompany = [];
    ddlCompanySetting = {};
    defaultFormValues: [object];
    moduleCode = Constants.userModuleCode;
    selectCompany = Constants.selectCompany;
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    search=Constants.search;
    availableFormValues: [object];
    arrLanguage:[object];
    currentLanguage: string;
    companyId:string;
    SelectedCompany = [];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    isResetPassword:string;
    private MyIP;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private selectCompanyService: SelectCompanyService,
        private settingsService: SettingsService,
        private getScreenDetailService: GetScreenDetailService,
     ) {
            var userData = JSON.parse(localStorage.getItem('currentUser'));
            this.isResetPassword =localStorage.getItem('isResetPassword');
             this.defaultFormValues = [
                { 'fieldName': 'SELECT_COM_PROCEED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'SELECT_COM_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' }
                ];
     }


    ngOnInit() {
        this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
        {
           this.arrLanguage = data
        });

        this.currentLanguage = localStorage.getItem('currentLanguage');

            this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
            }
            });


             this.ddlCompanySetting = { 
              singleSelection: true, 
              text:this.selectCompany,
              selectAllText: this.selectAll,
              unSelectAllText: this.unselectAll,
              enableSearchFilter: true,
              classes:"myclass custom-class",              
                searchPlaceholderText:this.search,
            };  

              this.selectCompanyService.getCompanyList().then(data => {
                  if(data.result)
                  {
                        for(var i=0;i<data.result.length;i++)
                        {
                            this.ddl_MultiSelectCompany.push({ "id": data.result[i].companyId, "itemName": data.result[i].name })
                        }
                  }
               
           });
        }

    Proceed()
    {
        if(this.SelectedCompany.length > 0)
        {
            this.selectCompanyService.checkCompanyAccess(this.SelectedCompany[0].id).then(data => {
              var datacode = data.code;
                if (datacode == 200) {
                  localStorage.setItem('tenantid', data.result.companyTenantId);
                  if(this.isResetPassword == 'Y')
                  {
                   this.router.navigate(['resetpassword', data.result.userId]);  
                  }
                  else
                  {
                    location.href='dashboard';
                  }
            }
            else{
                   this.hasMsg = true;
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = data.btiMessage.message+' !!';
                  window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                  }, 3000);
            }
        }).catch(error => {
            this.hasMsg = true;
                   window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
        });
    }
    else{
            this.hasMsg = true;
            this.isSuccessMsg = true;
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = "Please Select Company !!"
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 3000);
         }  
    }
        
    cancel() {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
      this.settingsService.logOutBeforeCompanySelection(userData.userId).then(data => {
        localStorage.setItem('currentUser', '');
        this.router.navigate(['login']);
    });
  }
  
    onItemSelect(item:any){
     this.SelectedCompany=[];
     this.SelectedCompany.push({"id":item.id,"itemName":item.itemName});
     $( ".c-btn" ).trigger( "click" );
    }
    OnItemDeSelect(item:any){
     this.SelectedCompany=[];
    }
    onSelectAll(items: any){
    }
    onDeSelectAll(items: any){
        
    }
}
       
        


     
