/**
 * A model for reset password
 */
export class ResetPassword {
    userId: string;
    password: string;
    confirmpassword:string;
    
    //initializing reset password parameters
    constructor(userId:string, password: string,confirmpassword:string) 
      {
            this.userId = userId;
            this.password = password;
            this.confirmpassword = confirmpassword;
      }
}