import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { TrialBalanceReportService } from '../../../_services/reports/trial-balance-report.service';
import { AccountStatementReportService } from '../../../_services/reports/account-statement-report.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { IAccountStatementReport, IMaster, IDetail } from '../../../_models/reports/AccountStatementReport';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../_sharedresource/Constants';
import {ModalModule, Modal} from "ng2-modal";
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { LooseCurrencyPipe } from '../../../../_sharedcomponent/looseCurrency.pipe';

@Component({
    templateUrl: './account-statement-report.component.html',
    providers:[TrialBalanceReportService, AccountStatementReportService, CommonService]
})

//export to make it available for other classes
export class AccountStatementReportComponent {
    screenCode;
    moduleCode = Constants.financialModuleCode;
    screenName;
    moduleName;
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    startDate;
	endDate;
    currentDateTime;
    accountNumberList = [{ id: '0', text:'--Select--' }];
    accountNumber;
    description;
    accountCategory;
    beginningBalance = 0.0;
    totalDebit;
    totalCredit;
    totalBalance;


    defaultFormValues: Array<object>;
    availableFormValues: [object];

    res : IAccountStatementReport;
    masterList: IMaster[];
    detailList: IDetail[];
    selectedMaster: IMaster;


    ACT_STMT_RPT_HEADING : any = { 'fieldName': 'ACT_STMT_RPT_HEADING', 'fieldValue': 'Account Statement Report', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_START_DATE : any = { 'fieldName': 'ACT_STMT_RPT_START_DATE', 'fieldValue': 'From', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_END_DATE : any = { 'fieldName': 'ACT_STMT_RPT_END_DATE', 'fieldValue': 'To', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_GET_DATA : any = { 'fieldName': 'ACT_STMT_RPT_GET_DATA', 'fieldValue': 'Get Data', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };

    ACT_STMT_RPT_ACCOUNT_NUMBER : any = { 'fieldName': 'ACT_STMT_RPT_ACCOUNT_NUMBER', 'fieldValue': 'Account Number', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_ACCOUNT_DESCRIPTION : any = { 'fieldName': 'ACT_STMT_RPT_ACCOUNT_DESCRIPTION', 'fieldValue': 'Account Description', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_ACCOUNT_CATEGORY : any = { 'fieldName': 'ACT_STMT_RPT_ACCOUNT_CATEGORY', 'fieldValue': 'Account Category', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };

    ACT_STMT_RPT_BEGINNING_BALANCE : any = { 'fieldName': 'ACT_STMT_RPT_BEGINNING_BALANCE', 'fieldValue': 'Beginning Balance', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    
    ACT_STMT_RPT_TH_JOURNAL_NO : any = { 'fieldName': 'ACT_STMT_RPT_TH_JOURNAL_NO', 'fieldValue': 'Journal Number', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_TH_JOURNAL_DATE : any = { 'fieldName': 'ACT_STMT_RPT_TH_JOURNAL_DATE', 'fieldValue': 'Journal Date', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_TH_DIST_REF : any = { 'fieldName': 'ACT_STMT_RPT_TH_DIST_REF', 'fieldValue': 'Distribuation Reference', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_TH_DEBIT_AMT : any = { 'fieldName': 'ACT_STMT_RPT_TH_DEBIT_AMT', 'fieldValue': 'Debit Amount', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_TH_CREDIT_AMT : any = { 'fieldName': 'ACT_STMT_RPT_TH_CREDIT_AMT', 'fieldValue': 'Credit Amount', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_TH_BALANCE : any = { 'fieldName': 'ACT_STMT_RPT_TH_BALANCE', 'fieldValue': 'Balance', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };

    ACT_STMT_RPT_BTN_PRINT : any = { 'fieldName': 'ACT_STMT_RPT_BTN_PRINT', 'fieldValue': 'Print', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_BTN_CLOSE : any = { 'fieldName': 'ACT_STMT_RPT_BTN_CLOSE', 'fieldValue': 'Close', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_LBL_PRINT_DATE : any = { 'fieldName': 'ACT_STMT_RPT_LBL_PRINT_DATE', 'fieldValue': 'Print Date', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    ACT_STMT_RPT_LBL_PRINT_TIME : any = { 'fieldName': 'ACT_STMT_RPT_LBL_PRINT_TIME', 'fieldValue': 'Print Time', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };

    ACT_STMT_RPT_LBL_TOTAL : any = { 'fieldName': 'ACT_STMT_RPT_LBL_TOTAL', 'fieldValue': 'Total', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };

    
    private myOptions1: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy'
    };
	private myOptions2: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy'
    };
   
    today = new Date();
    year=this.today.getFullYear();
    private  Object = { 
         date: { year: 2018, month: 10, day: 9 }
      //  date: { year: this.today.getFullYear(), month: this.today.getMonth() + 1+1, day: this.today.getDate() }
     };

     

        arrAccountStatementReport:any={
            "printDate": "",
            "printTime": "",
            "startDate": "",
            "endDate": "",
        "accountStatmentMasterRecordList": [
            {
                "accountNumber": "",
                "description": "",
                "accountCategory": "",
                "beginningBalance": "",
                "totalDebitAmount": "",
                "accountStatmentDetailRecordList": [
                    {
                        "journalNo": "",
                        "journalDate": "",
                        "distributionReference": "",
                        "debitAmount": "",
                        "creditAmount": "",
                        "balance": ""
                    }
                ]
            }
        ]
       }

       arrAccountStatementRecordList: any[] = [{
                "accountNumber": "",
                "description": "",
                "accountCategory": "",
                "beginningBalance": "",
                "totalDebitAmount": "",
                "accountStatmentDetailRecordList": [
                    {
                        "journalNo": "",
                        "journalDate": "",
                        "distributionReference": "",
                        "debitAmount": "",
                        "creditAmount": "",
                        "balance": ""
                    }
                ]
            }]
       
       accountStatementRecord: any = {
                "accountNumber": "",
                "description": "",
                "accountCategory": "",
                "beginningBalance": "",
                "totalDebitAmount": "",
                "accountStatmentDetailRecordList": [
                    {
                        "journalNo": "",
                        "journalDate": "",
                        "distributionReference": "",
                        "debitAmount": "",
                        "creditAmount": "",
                        "balance": ""
                    }
                ]
            }
       
    @ViewChild(DatatableComponent) table: DatatableComponent;
       constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private commonService: CommonService,
        private accountStatementReportService: AccountStatementReportService,
        )
        {
           
        }


        

        ngOnInit() 
        {
            this.startDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
            this.endDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
            
            this.currentDateTime = this.today.getDate() + "/" + this.today.getMonth() + "/" + this.today.getFullYear() + " " + this.today.getHours() + ":" + this.today.getMinutes() + ":" + this.today.getSeconds();
            this.getViewScreenDetail();
        }

    getViewScreenDetail()
    {
         this.screenCode = "S-1262";
         this.defaultFormValues = [
                { 'fieldName': 'ACT_STMT_RPT_HEADING', 'fieldValue': 'Account Statement Report', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               
                { 'fieldName': 'ACT_STMT_RPT_START_DATE', 'fieldValue': 'From', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_END_DATE', 'fieldValue': 'To', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_GET_DATA', 'fieldValue': 'Get Data', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
              

                { 'fieldName': 'ACT_STMT_RPT_ACCOUNT_NUMBER', 'fieldValue': 'Account Number', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_ACCOUNT_DESCRIPTION', 'fieldValue': 'Account Description', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_ACCOUNT_CATEGORY', 'fieldValue': 'Account Category', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },

                { 'fieldName': 'ACT_STMT_RPT_BEGINNING_BALANCE', 'fieldValue': 'Beginning Balance', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },

                { 'fieldName': 'ACT_STMT_RPT_TH_JOURNAL_NO', 'fieldValue': 'Journal Number', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_TH_JOURNAL_DATE', 'fieldValue': 'Journal Date', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_TH_DIST_REF', 'fieldValue': 'Distribuation Reference', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_TH_DEBIT_AMT', 'fieldValue': 'Debit Amount', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_TH_CREDIT_AMT', 'fieldValue': 'Credit Amount', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_TH_BALANCE', 'fieldValue': 'Balance', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },

                { 'fieldName': 'ACT_STMT_RPT_LBL_PRINT_DATE', 'fieldValue': 'Print Date', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_LBL_PRINT_TIME', 'fieldValue': 'Print Time', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_LBL_TOTAL', 'fieldValue': 'Total', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },

                { 'fieldName': 'ACT_STMT_RPT_BTN_PRINT', 'fieldValue': 'Print', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACT_STMT_RPT_BTN_CLOSE', 'fieldValue': 'Print', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];


         
        this.getScreenDetail(this.screenCode);
    }
        
    getScreenDetail(screenCode)
    {

        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }

                this.ACT_STMT_RPT_HEADING = this.defaultFormValues[0];
                this.ACT_STMT_RPT_START_DATE = this.defaultFormValues[1];
                this.ACT_STMT_RPT_END_DATE = this.defaultFormValues[2];
                this.ACT_STMT_RPT_GET_DATA = this.defaultFormValues[3];

                this.ACT_STMT_RPT_ACCOUNT_NUMBER = this.defaultFormValues[4];
                this.ACT_STMT_RPT_ACCOUNT_DESCRIPTION = this.defaultFormValues[5];
                this.ACT_STMT_RPT_ACCOUNT_CATEGORY = this.defaultFormValues[6];

                this.ACT_STMT_RPT_BEGINNING_BALANCE = this.defaultFormValues[7];
                
                this.ACT_STMT_RPT_TH_JOURNAL_NO = this.defaultFormValues[8];
                this.ACT_STMT_RPT_TH_JOURNAL_DATE = this.defaultFormValues[9];
                this.ACT_STMT_RPT_TH_DIST_REF = this.defaultFormValues[10];
                this.ACT_STMT_RPT_TH_DEBIT_AMT = this.defaultFormValues[11];
                this.ACT_STMT_RPT_TH_CREDIT_AMT = this.defaultFormValues[12];
                this.ACT_STMT_RPT_TH_BALANCE = this.defaultFormValues[13];

                this.ACT_STMT_RPT_LBL_PRINT_DATE = this.defaultFormValues[14];
                this.ACT_STMT_RPT_LBL_PRINT_TIME = this.defaultFormValues[15];
                this.ACT_STMT_RPT_LBL_TOTAL = this.defaultFormValues[16];

                this.ACT_STMT_RPT_BTN_PRINT = this.defaultFormValues[17];
                this.ACT_STMT_RPT_BTN_CLOSE = this.defaultFormValues[18];

        });
    }

        GetAccountStatementReport(startDate: string,endDate:string)
        {
            //  this.startDate=this.startDate.formatted;
            //  this.endDate=this.endDate.formatted;
             if(this.startDate.formatted == undefined)
             {
               var startDateData = this.startDate;
               if(startDateData.date != undefined)
               {
                  this.startDate = startDateData.date.day +'/'+ startDateData.date.month +'/'+ startDateData.date.year;
               }
             }
             else
             {
                this.startDate=this.startDate.formatted;
             }

             if(this.endDate.formatted == undefined)
             {
               var endDateData = this.endDate;
               if(endDateData.date != undefined)
               {
                  this.endDate = endDateData.date.day +'/'+ endDateData.date.month +'/'+ endDateData.date.year;
               }
             }
             else
             {
                this.endDate=this.endDate.formatted;
             }
            this.accountStatementReportService.GetAccountStatementReport(this.startDate,this.endDate).then(data => {
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                this.res = data.result;
                this.masterList = this.res.accountStatmentMasterRecordList;
                console.log(this.masterList);
                this.accountNumberList = [{id: '0', text:'--Select--' }];
                for(let i = 0 ; i< this.masterList.length;i++){
                    this.accountNumberList.push({id:this.masterList[i].accountNumber , text: this.masterList[i].accountNumber + ':'+this.masterList[i].description});
                }
                this.selectedMaster = this.res.accountStatmentMasterRecordList[0];

                this.description = this.selectedMaster.description;
                this.accountCategory = this.selectedMaster.accountCategory;
                this.beginningBalance = (this.selectedMaster.beginningBalance) ? this.selectedMaster.beginningBalance : 0.0;
                this.accountNumber = this.selectedMaster.accountNumber;
                this.accountStatementRecord = this.selectedMaster;
                this.totalDebit = (this.selectedMaster.totalDebitAmount) ? this.selectedMaster.totalDebitAmount: 0.0;
                this.totalCredit = (this.selectedMaster.totalCreditAmount) ? this.selectedMaster.totalCreditAmount : 0.0;
                this.totalBalance = (this.selectedMaster.totalBalance) ? this.selectedMaster.totalBalance : 0.0;
                // this.

                // this.formatCurrency();
            //    this.arrAccountStatementReport= data.result;
            //    this.accountStatementRecord = this.arrAccountStatementReport.accountStatmentMasterRecordList[0];
            }
            });
         }


         getAccountStatementDetails(event){
            console.log(event);
            var selectedAccountNumber = event.target.value;
            console.log(selectedAccountNumber);
            for (let account of this.masterList){
                console.log(account.accountNumber);
                if (account.accountNumber == selectedAccountNumber){
                    // alert(selectedAccountNumber + " macthed with " + account.accountNumber);
                    this.selectedMaster = account;
                    this.description = account.description;
                    this.accountCategory = account.accountCategory;
                    this.beginningBalance = (account.beginningBalance) ? account.beginningBalance : 0.0 ;
                    this.accountNumber = account.accountNumber;
                    this.accountStatementRecord = account;
                    this.totalDebit = (account.totalDebitAmount) ? account.totalDebitAmount : 0.0;
                    this.totalCredit = (account.totalCreditAmount) ? account.totalCreditAmount : 0.0;
                    this.totalBalance = (account.totalBalance) ? account.totalBalance : 0.0;
                }
            }
            // this.formatCurrency();
         }

         getAccountStatmentDetilsNew(event){
             console.log(event);
             this.getAccountStatementDetails({target: {value: event.id}});
         }

        clearMaintenanceDate(){
		    this.startDate='';
        }
        clearAddedDate(){
            this.endDate='';
        }

        getCopyOfOptions(): INgxMyDpOptions {
            return JSON.parse(JSON.stringify(this.myOptions2));
        } 
    
        getCopyOfOption(): INgxMyDpOptions {
            return JSON.parse(JSON.stringify(this.myOptions1));
        } 

        print(): void {
                let printContents, popupWin;
                printContents = document.getElementById('AccountStatementReportPrintSection').innerHTML;
                popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
                popupWin.document.open();
                popupWin.document.write(`
                <html>
                    <head>
                    <title>Algoras</title>
                    <style>
                    //........Customized style.......
                    </style>
                    </head>
                <body onload="window.print();window.close()">${printContents}</body>
                </html>`
                );
                popupWin.document.close();   
        }


    openModel(myModal)
    {
        myModal.open();
    }

    closeModal(myModal)
    {
        myModal.close();
        //  this.isDeleteAction=false;
        //  this.isConfirmationModalOpen = false;
        //  this.isModalOpen = false;
    }

    openSearchModal(event){
        if(event.target.value === '?' || event.target.value === '؟'){
            document.getElementById('searchPopupButton').click();
        }
    }
    
    receiveMessage(event){
        console.log(event);
        let accountNumber  =  event.accountNumber.replace(/\s/g, "");
        console.log(accountNumber);
        this.getAccountStatementDetails({target: {value: accountNumber}});
    }
    
}