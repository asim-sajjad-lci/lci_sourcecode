import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { TrialBalanceReportService } from '../../_services/reports/trial-balance-report.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../_sharedresource/Autofocus';
import { Constants } from '../../../_sharedresource/Constants';

@Component({
    templateUrl: './trial-balance-report.component.html',
    providers:[TrialBalanceReportService]
})

//export to make it available for other classes
export class TrialBalanceReportComponent {
    screenCode;
    screenName;
    moduleCode = Constants.financialModuleCode;
    moduleName;
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    startDate;
	endDate;
    check:boolean;
    

    fld_header : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_startDate : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_endDate : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_getData : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_accountNumber : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_accountDescription : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_beginningBalance : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_debit : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_credit : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_netChange : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_endingBalance : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_print : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_printDate : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_printTime : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_total : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_accounts : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_close : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };

    defaultFormValues: Array<object>;
    availableFormValues: [object];

    private myOptions1: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy'
    };
	private myOptions2: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy'
    };
   
    today = new Date();
    year=this.today.getFullYear();
    private  Object = { 
         date: { year: 2018, month: 10, day: 9 }
      //  date: { year: this.today.getFullYear(), month: this.today.getMonth() + 1+1, day: this.today.getDate() }
        
     };
        arrTrialBalanceReport:any={
            "startDate": "",
            "endDate": "",
            "totalBeginningBalance": "",
            "totalEndingBalance": "",
            "totalDebit": "",
            "totalCredit": "",
            "totalNetChange": "",
            "totalAccounts": "",
            "trailBalanceReportList": [
                {
                    "accountNumber": "",
                    "accountDescription": "",
                    "beginningBalance": "",
                    "endingBalance": "",
                    "debit": "",
                    "credit": "",
                    "netChange": ""
                }]
       }
    @ViewChild(DatatableComponent) table: DatatableComponent;
       constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private trialBalanceReportService: TrialBalanceReportService,
        )
        {
           
        }

        ngOnInit() 
        {
            this.startDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
         this.endDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
            this.getViewScreenDetail();
        }

    getViewScreenDetail()
    {
         this.screenCode = "S-1261";
         this.defaultFormValues = [

               { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_START_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_END_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_GET_DATA', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },

               { 'fieldName': 'TB_RPT_ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_ACCOUNT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_BEGINNING_BALANCE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_DEBIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               
               { 'fieldName': 'TB_RPT_CREDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_NET_CHARGE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_ENDING_BALANCE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PRINT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },

               { 'fieldName': 'TB_RPT_PRINT_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PRINT_TIME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_TOTAL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_ACCOUNTS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
              
               { 'fieldName': 'TB_RPT_CLOSE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode);



    }
        
    getScreenDetail(screenCode)
    {

        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }

                this.fld_header = this.defaultFormValues[0];
                this.fld_startDate = this.defaultFormValues[1];
                this.fld_endDate = this.defaultFormValues[2];
                this.fld_getData = this.defaultFormValues[3];

                this.fld_accountNumber = this.defaultFormValues[4];
                this.fld_accountDescription = this.defaultFormValues[5];
                this.fld_beginningBalance = this.defaultFormValues[6];
                this.fld_debit = this.defaultFormValues[7];

                this.fld_credit = this.defaultFormValues[8];
                this.fld_netChange = this.defaultFormValues[9];
                this.fld_endingBalance = this.defaultFormValues[10];
                this.fld_print = this.defaultFormValues[11];
                
                this.fld_printDate = this.defaultFormValues[12];
                this.fld_printTime = this.defaultFormValues[13];
                this.fld_total = this.defaultFormValues[14];
                this.fld_accounts = this.defaultFormValues[15];

                this.fld_close = this.defaultFormValues[16];
        });
        }
        
        GetTrialBalanceReport(startDate: string,endDate:string)
        {

            //  this.startDate=this.startDate.formatted;
            //  this.endDate=this.endDate.formatted;
             if(this.startDate.formatted == undefined)
             {
               var startDateData = this.startDate;
               if(startDateData.date != undefined)
               {
                  this.startDate = startDateData.date.day +'/'+ startDateData.date.month +'/'+ startDateData.date.year;
               }
             }
             else
             {
                this.startDate=this.startDate.formatted;
             }

             if(this.endDate.formatted == undefined)
             {
               var endDateData = this.endDate;
               if(endDateData.date != undefined)
               {
                  this.endDate = endDateData.date.day +'/'+ endDateData.date.month +'/'+ endDateData.date.year;
               }
             }
             else
             {
                this.endDate=this.endDate.formatted;
             }
            this.trialBalanceReportService.GetTrailBalanceReport(this.startDate,this.endDate).then(data => {
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
               this.arrTrialBalanceReport=data.result;
            }
            });
         }

        clearMaintenanceDate(){
		this.startDate='';
        }
        clearAddedDate(){
            this.endDate='';
        }

        getCopyOfOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.myOptions2));
        } 
    
        getCopyOfOption(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.myOptions1));
        } 

        print(): void {
                let printContents, popupWin;
                printContents = document.getElementById('TrialBalanceReportPrintSection').innerHTML;
                popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
                popupWin.document.open();
                popupWin.document.write(`
                <html>
                    <head>
                    <title>Algoras</title>
                    <style>
                    //........Customized style.......
                    </style>
                    </head>
                <body onload="window.print();window.close()">${printContents}</body>
                </html>`
                );
                popupWin.document.close();   
        }


    openModel(myModal)
    {
        myModal.open();
    }

    closeModal(myModal)
    {
        myModal.close();
        //  this.isDeleteAction=false;
        //  this.isConfirmationModalOpen = false;
        //  this.isModalOpen = false;
    }

    changeWidth(event, check){
        console.log(event);
        if(event.target.id == "Landscape"){
            this.check = true;
        }else if(event.target.id == "Portrait"){
            this.check = false;
        }
    }

}