import {Component, ElementRef, ViewChild} from '@angular/core';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import {ReportManagementService} from '../../../../userModule/_services/report-management/report-management.service';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {Page} from '../../../../_sharedresource/page';
import {ReportManagementModel} from '../../../../userModule/_models/report-management/report-management';

declare var dashboard: any;

@Component({
    selector: 'dashboard-report',
    templateUrl: './dashboard-report.component.html',
    providers: [ReportManagementService]
})
export class DashboardReportComponent {

    page = new Page();
    rows = new Array<ReportManagementModel>();
    moduleCode = 'M-1001';
    screenCode = 'S-PLD-1025';
    moduleName;
    screenName;
    defaultFormValues: [object];
    availableFormValues: [object];
    currentLanguage: any;
    getScreenDetailArr;
    reports: any[]= [];
    searchKeyword = '';
    containedReports: any[] = [];
    resultList: [object];
    moduleId: string;
    screenCategoryList: any[]= [];
    screenMenuList: any[]= [];

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private getScreenDetailService: GetScreenDetailService,
                private reportManagementService: ReportManagementService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
    }

    ngOnInit() {
        this.currentLanguage = localStorage.getItem('currentLanguage');
        this.getScreenDetails();
        var tanent_id = localStorage.getItem('tenantid');
        var dashboard_obj = '';
        var user = 'admin';
        var pass = 'admin';

        // alert("tanent_id: " + tanent_id);
        if (tanent_id == 'c_1002_finacialmodule') {
            user = 'CAREER_USER';
            pass = 'career';
            // dashboard_obj = 'd167f482758e.dsb';
        } else if (tanent_id == 'c_1003_finacialmodule') {
            user = 'OTC_USER';
            pass = 'otc'
            // dashboard_obj = 'd1685a54e72f.dsb';
        }
        dashboard_obj = 'd1692e910f12.dsb';

        // alert("dashboard_obj: " + dashboard_obj);

        dashboard.func1(dashboard_obj, user, pass);
    }

    getScreenDetails() {
        //getting screen labels, help messages and validation messages
        this.getScreenDetailService.screenGridDetail(this.moduleCode, this.screenCode).then(data => {
            this.getScreenDetailArr = data.result;
            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });
    }
}
