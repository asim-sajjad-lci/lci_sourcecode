import { Component } from "@angular/core";
import { ProfitAndLossService } from "../../../_services/reports/profitAndLoss.service";
import { ProfitAndLossReport } from "../../../_models/reports/ProfitAndLossReport";
import { GetScreenDetailService } from "../../../../_sharedresource/_services/get-screen-detail.service";
import { Constants } from "../../../../_sharedresource/Constants";
import { CompanyService } from "../../../../userModule/_services/companymanagement/company.service";




@Component({
    templateUrl:'./profit&loss-report.component.html',
    providers:[]
})
export class ProfitAndLossReportComponent{
    

    math:Math = Math;
    companyName:string;
    TotalprofitAndLoss:number = 0;

    reportcategories:any ={};

    screenCode;
    defaultFormValues:Array<Object>;
    availableFormValues: [object];
    screenName;
    moduleCode = Constants.financialModuleCode;
    moduleName;
    startDate;
    endDate;
    check:boolean;
    userData;
    today = new Date();
    year=this.today.getFullYear();

    fld_header : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_Year : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_priod : any = { 'fieldName': 'TB_RPT_TRIAL_BALANCE_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_getData : any = { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_GET_DATA', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_accountCategory : any = { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_ACCOUNT_CATEGORY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_accountDescription : any = { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_ACCOUNT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_periodTransaction : any = { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_PERIOD_TRANSACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_totalAccountTransactionYTD: any = { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_TOTAL_ACCOUNT_TRANSACTION_YTD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_NetIncome: any = { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_NET_INCOME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    fld_print : any = { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_PRINT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' };
    

   profitAndLossReport:ProfitAndLossReport = new ProfitAndLossReport(0,0);

    constructor(private profitAndLossService:ProfitAndLossService,private getScreenDetailService: GetScreenDetailService,private companyService:CompanyService){

        this.getViewScreenDetail();
    }

    ngOnInit() 
    {
        this.companyService.getOneCompanyByTenatedId(localStorage.getItem('tenantid')).subscribe(data => {
            this.companyService.getCurrentCompanyName().subscribe(companyName => {
                this.companyName = companyName;
            })
        });
        this.startDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
        this.endDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
        this.userData = JSON.parse(localStorage.getItem('currentUser'));
        this.userData =  this.userData.userId;
    }
    getViewScreenDetail()
    {
         this.screenCode = "S-4003";
         this.defaultFormValues = [

               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_REPORT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_YEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_PERIOD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_GET_DATA', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },

               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_ACCOUNT_CATEGORY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_ACCOUNT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_PERIOD_TRANSACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_TOTAL_ACCOUNT_TRANSACTION_YTD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_NET_INCOME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'TB_RPT_PROFITL_AND_LOSS_PRINT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode);



    }

    getScreenDetail(screenCode)
    {

        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }

                this.fld_header = this.defaultFormValues[0];
                this.fld_Year = this.defaultFormValues[1];
                this.fld_priod = this.defaultFormValues[2];
                this.fld_getData = this.defaultFormValues[3];

                this.fld_accountCategory = this.defaultFormValues[4];
                this.fld_accountDescription = this.defaultFormValues[5];
                this.fld_periodTransaction = this.defaultFormValues[6];
                this.fld_totalAccountTransactionYTD = this.defaultFormValues[7];
                this.fld_NetIncome = this.defaultFormValues[8];

                this.fld_print = this.defaultFormValues[9];
        });
        }

    onPeriodChange(event){
        this.profitAndLossReport.period = event.target.value;
    }

    onYearChange(event){
        this.profitAndLossReport.year = event.target.value;
    }

    GetProfitAndLossReport(){
        if(this.profitAndLossReport.period != undefined){
            this.profitAndLossService.GetProfitAndLossReport(this.profitAndLossReport.year ,this.profitAndLossReport.period).then( data =>{
                this.reportcategories = data.result
                
            });
        }else{
            
        }

    }

    
    openModel(myModal)
    {
        myModal.open();
    }

    closeModal(myModal)
    {
        myModal.close();
    }


    print(): void {

        let printContents, popupWin;
        printContents = document.getElementById('ProfitAndLossReportPrintSection').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
        <html>
            <head>
            <title>Algoras</title>
            <style>
            //........Customized style.......
            </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
        </html>`
        );
        popupWin.document.close();   
}
 

changeWidth(event, check){
    console.log(event);
    if(event.target.id == "Landscape"){
        this.check = true;
    }else if(event.target.id == "Portrait"){
        this.check = false;
    }
}
 
}