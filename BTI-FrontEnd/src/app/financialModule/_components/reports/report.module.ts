import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { ReportRoutingModule } from './report-routing.module';
import { TrialBalanceReportComponent } from './trial-balance-report.component';
import { AccountStatementReportComponent } from './account-statement-report/account-statement-report.component';
import {SelectModule} from 'ng2-select';
import {ModalModule} from "ng2-modal";
import { SharedModule } from "../../../shared/shared.module";
import { ProfitAndLossReportComponent } from './profit&loss-report/profit&loss-report.component';
import { BalanceSheetReportComponent } from './balanceSheetReport/balance-sheet-report.component';
import { DashboardReportComponent } from './dashboard-report/dashboard-report.component';
import { TrialBalanceReportNewComponent } from './trial-balance-report-new.component';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    SelectModule,
    ReportRoutingModule,
    FormsModule,
    NgxMyDatePickerModule,
    ModalModule,
    SharedModule
  ],
  declarations: [
    AccountStatementReportComponent, 
    TrialBalanceReportComponent,
    TrialBalanceReportNewComponent,
    AutoFocusDirective,
    ProfitAndLossReportComponent,
    BalanceSheetReportComponent, 
    DashboardReportComponent]
})
export class ReportModule { }
