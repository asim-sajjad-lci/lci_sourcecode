import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrialBalanceReportComponent } from './trial-balance-report.component';
import { TrialBalanceReportNewComponent } from './trial-balance-report-new.component';
import { AccountStatementReportComponent } from './account-statement-report/account-statement-report.component';
import { ProfitAndLossReportComponent } from './profit&loss-report/profit&loss-report.component';
import { BalanceSheetReportComponent } from './balanceSheetReport/balance-sheet-report.component';
import { DashboardReportComponent } from './dashboard-report/dashboard-report.component';

const routes: Routes = [
  { path: '', component: AccountStatementReportComponent },
  { path: 'trialbalance', component: TrialBalanceReportComponent },
  { path: 'trialbalance_new', component: TrialBalanceReportNewComponent },
  { path: 'accountstatementreport', component: AccountStatementReportComponent },
  { path: 'profitandlossreport', component: ProfitAndLossReportComponent },
  { path: 'BalancesheetReport', component: BalanceSheetReportComponent },
  { path: 'dashboardreport', component: DashboardReportComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportRoutingModule { }
