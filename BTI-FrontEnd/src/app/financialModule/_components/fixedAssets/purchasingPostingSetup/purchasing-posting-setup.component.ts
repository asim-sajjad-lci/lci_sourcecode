import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {fixedAssetCompanySetup} from '../../../../financialModule/_models/fixedAssets/fixedAssetCompanySetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {PurchasingPostingSetupService} from '../../../../financialModule/_services/fixedAssets/purchasingPostSetup.service';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 
import * as moment from 'moment';
@Component({
   selector: 'purchasing-posting-setup',
   templateUrl: './purchasing-posting-setup.component.html', 
    providers: [PurchasingPostingSetupService,AccountStructureService,CommonService],
})

export class PurchasingPostingSetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	isScreenLock
	// Serachable Select Box Step1
	ArrAccountNoId = [];
    accountTableRowIndex : any = [];
    ddlAccountNoIdSetting = {};
	ArrClassId = [];
    classId : any = [];
    ddlClassIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<fixedAssetCompanySetup>();
    temp = new Array<fixedAssetCompanySetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
	deleteConfirmationText = Constants.deleteConfirmationText;
	search=Constants.search;
	isModalOpen:boolean=false;
	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	tempVar;
	fetchedData;
	getClass;
	getAccountList;
	mode;
	arrSegment= [];
	accountNumberList =[];
	accountDescription=[];
	accountNumberIndex=[];
	accountNumberTitleLabel:string;
	createAccountNumberLabel:string;
	btnCancelLabel:string;
	faPostingSetup = {
		'classId': '',
		'accountTableRowIndex': '',
	}
	
	constructor(private router: Router,private route: ActivatedRoute,
		private purchasingPostingSetupService: PurchasingPostingSetupService,
		private accountStructureService:AccountStructureService,
		private commonService:CommonService,
		private getScreenDetailService: GetScreenDetailService,public toastr: ToastsManager,	vcr: ViewContainerRef,) {

		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.tempVar ='';
		this.fetchedData =[];
		this.getClass =[];
		this.getAccountList =[];
		this.mode ="add";
		this.accountNumberTitleLabel=Constants.accountNumberTitle;
		this.createAccountNumberLabel=Constants.createAccountNumber;
		this.btnCancelLabel=Constants.btnCancelText;
			this.toastr.setRootViewContainerRef(vcr);
		this.purchasingPostingSetupService.getClass().subscribe(pagedData => {
            this.getClass = pagedData.records;
			                // Searchable Select Box Step3
                for(var i=0;i< pagedData.records.length;i++)
                {
                    this.ArrClassId.push({ "id": pagedData.records[i].classId, "itemName": pagedData.records[i].classId})
                }
			    // Searchable Select Box Step3 End
        });
		
		this.getAccountNumberList();
		/* for edit */
			// this.purchasingPostingSetupService.getData().then(data => {
			// 	this.fetchedData = data.result;
			// 	if(this.fetchedData.classId){
			// 		this.faPostingSetup = {
			// 			'classId': this.fetchedData.classId,
			// 			'accountTableRowIndex': this.fetchedData.accountTableRowIndex,
			// 		}
			// 	}
			// });
		/* for edit ends*/
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_FA_PUR_POST_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_PUR_POST_ACC_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_PUR_POST_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_PUR_POST_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				];

			this.getScreenDetailService.ValidateScreen("S-1214").then(res=>
				{
					this.isScreenLock = res;
				});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1214").then(data => {
				
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
		
	}
	getAccountNumberList()
	{
		this.purchasingPostingSetupService.getAccountList().then(pagedData => {
            this.getAccountList = pagedData.result;
			                // Searchable Select Box Step3
                for(var i=0;i< pagedData.result.length;i++)
                {
                    this.ArrAccountNoId.push({ "id": pagedData.result[i].accountTableRowIndex, "itemName": pagedData.result[i].accountNumber})
                }
			    // Searchable Select Box Step3 End
        });
	}

     getPurchasePostingAccountSetupByClassId(classId)
	{
		this.purchasingPostingSetupService.getPurchasePostingAccountSetupByClassId(classId).then(data => {
            this.getAccountList = data.result;
			this.accountTableRowIndex=[];
			var selectedAccount = this.ArrAccountNoId.find(x => x.id ==   data.result.accountTableRowIndex);
			this.accountTableRowIndex.push({ "id": selectedAccount.id, "itemName": selectedAccount.itemName})
			//this.accountTableRowIndex.push({ "id": data.result.accountTableRowIndex, "itemName": data.result.accountNumber})
			                
        });
	}
    
   
	getAccountNumber(classId){
		// this.classId[0].id;
		// this.classId[0].itemName;
		this.getPurchasePostingAccountSetupByClassId(this.classId[0].id);
		
	}
	    // Serachable Select Box Step4
    getAccountNoByID(item:any){
		this.faPostingSetup.accountTableRowIndex=item.id;
		this.commonService.closeMultiselectWithId("accountTableRowIndex")
    }
    OnAccountNoDeSelect(item:any){
		this.faPostingSetup.accountTableRowIndex='';
		this.commonService.closeMultiselectWithId("accountTableRowIndex")
    }

	getClassByID(item:any){
		this.faPostingSetup.classId=item.id;
		this.commonService.closeMultiselectWithId("classId")
    }
    OnClassIdDeSelect(item:any){
		this.faPostingSetup.classId='';
		this.commonService.closeMultiselectWithId("classId")
    }
    // Serachable Select Box Step4 ends
		
	save(FaPurchasing:NgForm){
		this.btndisabled=true;
		let submittedData ={
			"classId":this.faPostingSetup.classId,
			"accountTableRowIndex" : this.faPostingSetup.accountTableRowIndex
		};
	
		this.purchasingPostingSetupService.save(submittedData).then(data => {
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
	}

	clear(FaPurchasing:NgForm){
    this.accountTableRowIndex=[];
	this.classId=[];
	}

	getSegmentCount()
	{
		 this.accountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}
    
	fetchFirstAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		
		if(segmentNumber)
		{
			this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {
                
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberIndex[segmentIndex])
					{
						this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountDescription.push(result.mainAccountDescription);
						this.accountNumberIndex.push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
                    	var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
						this.accountNumberIndex[m]='0';
						this.accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
						}
						if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountDescription = [];
					this.accountNumberIndex = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
                    {
						var ctrlId='';
                        if(m >  0)
                        {
                            ctrlId = 'txtSegment_'+m;
                        }
                        else{
                            ctrlId='txtfirstSegment_'+m;
                        }
                        var segment = <HTMLInputElement>document.getElementById(ctrlId);
                        segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
			this.accountDescription = [];
			this.accountNumberIndex = [];

			var currentSegment=segmentIndex;
			for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
			{
				var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
				segment.value = '';
				segment.disabled = true;
			}
		}
	}
	fetchOtherAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberIndex.indexOf(segment.value);
		
			if(segmentNumber)
			{
				this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
					let status = pagedData.status;
    				let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberIndex[segmentIndex])
						{
							this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountDescription.push(result.dimensionDescription);
							this.accountNumberIndex.push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
						segment.value = '';
						this.accountNumberIndex[segmentIndex]='0';
						this.accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberIndex[currentSegment]='0';
				this.accountDescription[currentSegment]='0';
			}
	}

	createNewGlAccountNumber()
	{
		
		var tempAccountNumberList=[];
		var accountDesc = this.accountDescription.join();
		accountDesc=accountDesc.replace(/,/g , " ");
        accountDesc=accountDesc.replace("0"," ");
		tempAccountNumberList.push({'accountNumberIndex':this.accountNumberIndex,'accountDescription':accountDesc})
		this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {
			
			if(data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED')
			{
				this.toastr.success(data.btiMessage.message);
				this.getAccountNumberList();
				this.closeModal();
			}
			else{
				this.toastr.warning(data.btiMessage.message);
			}
		 });
		
	}
	ngOnInit() {

			this.getSegmentCount();
		           // Serachable Select Box Step2
		   this.ddlAccountNoIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 this.ddlClassIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
	}	

	openAccountWindow()
	{
		this.accountNumberIndex=[];
		this.accountDescription=[];
		this.isModalOpen = true;
	}
	closeModal()
    {
         this.isModalOpen = false;
    }
			
			/** If Screen is Lock then prevent user to perform any action.
			*  This function also cover Role Management Write acceess functionality */
			LockScreen(writeAccess)
			{
				if(!writeAccess)
				{
					return true
				}
				else if(this.btndisabled)
				{
					return true
				}
				else if(this.isScreenLock)
				{
					return true;
				}
				else{
					return false;
				}
			}
}