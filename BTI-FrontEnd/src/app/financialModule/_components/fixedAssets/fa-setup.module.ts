import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { CompanySetupComponent } from '../FixedAssets/companySetup/company-set-up.component';
import { StructureSetupComponent } from '../FixedAssets/structureSetup/structure-setup.component';
import { BookSetupComponent } from '../FixedAssets/bookSetup/book-setup.component';
import { RetirementSetupComponent } from '../FixedAssets/retirementSetup/retirement-setup.component';
import { FaCalenderSetupComponent } from '../FixedAssets/fa-calender-setup/fa-calender-setup.component';
import { CalenderQuarterSetupComponent } from '../FixedAssets/calender-quarter-setup/calender-quarter-setup.component';
import { LocationComponent } from '../FixedAssets/locationSetup/location.component';
import { physicalLocationComponent } from '../FixedAssets/physicalLocationSetup/physical-location.component';
import { InsuranceClassSetupComponent } from '../FixedAssets/insuranceClassSetup/insuranceClassSetup.component';
import { BookClassSetupComponent} from '../FixedAssets/bookClassSetup/bookClassSetup.component';
import { ClassSetup } from '../FixedAssets/classSetup/class-setup.component';
import { LeaseCompanySetupComponent } from '../FixedAssets/leaseCompanySetup/lease-company-setup.component';
import { PurchasingPostingSetupComponent } from '../FixedAssets/purchasingPostingSetup/purchasing-posting-setup.component';
import { AccountGroupSetupComponent } from '../FixedAssets/accountGroupSetup/account-group-setup.component';
import { AccountsReceivableRoutingModule } from './fa-setup-routing.module';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    AccountsReceivableRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule
  ],
  declarations: [
    CompanySetupComponent,
    StructureSetupComponent,BookSetupComponent,RetirementSetupComponent,
    FaCalenderSetupComponent,CalenderQuarterSetupComponent,LocationComponent,physicalLocationComponent
    ,InsuranceClassSetupComponent,
    BookClassSetupComponent,ClassSetup,LeaseCompanySetupComponent,PurchasingPostingSetupComponent,
    AccountGroupSetupComponent,AutoFocusDirective
    ]
})
export class AccountsReceivableModule { }
