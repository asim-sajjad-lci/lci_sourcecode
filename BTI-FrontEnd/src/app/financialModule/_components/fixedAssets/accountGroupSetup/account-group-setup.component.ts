import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {fixedAssetCompanySetup} from '../../../../financialModule/_models/fixedAssets/fixedAssetCompanySetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import {AccountGroupSetupService} from '../../../../financialModule/_services/fixedAssets/accountGroupPostSetup.service';
import { AccountGroupSetup} from '../../../../financialModule/_models/fixedAssets/accountGroupSetup';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import {AccountReceivableClassGLSetupService} from '../../../../financialModule/_services/accountReceivables/accountReceivablesClassGLSetup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
   selector: 'account-group-setup',
   templateUrl: './account-group-setup.component.html',
    providers: [AccountReceivableClassGLSetupService,AccountGroupSetupService,AccountStructureService],
})

export class AccountGroupSetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues:any= [];
	gridDefaultFormValues:any= [];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	accountNumberList =[];
	AccountTypeList=[];
	page = new Page();
	rows = new Array<AccountGroupSetup>();
    temp = new Array<AccountGroupSetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	arrSegment= [];
	btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
	EmptyMessage = Constants.EmptyMessage;
	sameAccountNumberMsg = Constants.sameAccountNumberMsg;
	btndisabled:boolean=false;
	isScreenLock
    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	tempVar;
	fetchedData;
	getClass;
	getAccountList;
	mode;
	//accountNumberList;
	accountGroup = {
		'accountGroupId': '',
		'description': '',
		'descriptionArabic': '',
		'accountNumberList':''
	}
	
	constructor(private router: Router,private route: ActivatedRoute,private accountGroupSetupService: AccountGroupSetupService,private getScreenDetailService: GetScreenDetailService,public toastr: ToastsManager,vcr: ViewContainerRef,
	private AccountStructureService:AccountStructureService,private AccountReceivableClassGLSetupService: AccountReceivableClassGLSetupService) 
	{
		this.toastr.setRootViewContainerRef(vcr);
		this.add = true;
		let vm = this;
		this.disableId = false;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.tempVar ='';
		this.fetchedData =[];
		this.getClass =[];
		this.getAccountList =[];
		this.accountNumberList =[];
		this.mode ="add";
		
	
       this.getAccountType();
    
		// this.accountGroupSetupService.getAccountType().then(pagedData => {
        //     this.getAccountList = pagedData.result;
		// 	for(let i=0; i<this.getAccountList.length; i++){
		// 		let temp = {
		// 			 "accountNumber":"",
		// 			 "accountTypeId": this.getAccountList[i]["typeId"],
		// 			 "typeValue": this.getAccountList[i]["typeValue"],
		// 			 "accountDescription":"",
		// 			 "accountNumber1":"",
		// 			 "accountNumber2":"",
		// 			 "accountNumber3":"",
		// 			 "accountNumber4":"",
		// 			 "accountDescription1":"",
		// 			 "accountDescription2":"",
		// 			 "accountDescription3":"",
		// 			 "accountDescription4":"",
		// 			 "disabled2":true,
		// 			 "disabled3":true,
		// 			 "disabled4":true
		// 		}
		// 		this.accountNumberList.push(temp);
		// 	}
        // });
		
		/* for edit */
			// this.accountGroupSetupService.getEditRecord().then(data => {
			// 	this.fetchedData = data.result;
			// 	if(this.fetchedData.accountGroupId){
			// 		this.accountGroup = {
			// 		'accountGroupId': this.fetchedData.accountGroupId,
			// 		'description': this.fetchedData.description,
			// 		'descriptionArabic': this.fetchedData.descriptionArabic,
			// 		'accountNumberList':this.fetchedData.accountNumberList
			// 		};
			// 		for(let i=0;i<this.fetchedData.accountNumberList.length;i++){
			// 			let j = this.fetchedData.accountNumberList[i]["accountType"]-1;
			// 			var splitAccountNumber = this.fetchedData.accountNumberList[i]["accountNumber"].split("-");
					
			// 			var splitDesription = this.fetchedData.accountNumberList[i]["accountDescription"].split(" ");
						
						
			// 			// binding here
			// 			this.accountNumberList[j]["accountNumber"] = this.fetchedData.accountNumberList[i]["accountNumber"];
			// 			this.accountNumberList[j]["accountDescription"] = this.fetchedData.accountNumberList[i]["accountDescription"];
						
			// 			if(splitAccountNumber[0]){
			// 				this.accountNumberList[j]["accountNumber1"] = splitAccountNumber[0];
			// 			}
			// 			if(splitAccountNumber[1]){
			// 				this.accountNumberList[j]["accountNumber2"] = splitAccountNumber[1];
			// 				this.accountNumberList[j]["disabled2"] = false;
			// 			}
			// 			if(splitAccountNumber[2]){
			// 				this.accountNumberList[j]["accountNumber3"] = splitAccountNumber[2];
			// 				this.accountNumberList[j]["disabled3"] = false;
			// 			}
			// 			if(splitAccountNumber[3]){
			// 				this.accountNumberList[j]["accountNumber4"] = splitAccountNumber[3];
			// 				this.accountNumberList[j]["disabled4"] = false;
			// 			}								
			// 		}
			// 	}					
			// });
		/* for edit ends*/
			this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_ACC_GP_ACC_GP_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_GP_DESC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_GP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_GP_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_GP_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_GP_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
		];

		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1224").then(data => {
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */

		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_ACC_GP_ACC_GP_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_GP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_GP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_GP_ACC_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_GP_ACC_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_GP_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_GP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_GP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
				];

			this.getScreenDetailService.ValidateScreen("S-1213").then(res=>
				{
					this.isScreenLock = res;
				});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1213").then(data => {
				
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */
		
	}
     getAccountType()
	 {
		
		   this.accountGroupSetupService.getAccountType().then(pagedData => {
			this.accountNumberList=[];
            this.getAccountList = pagedData.result;
			for(let i=0; i<this.getAccountList.length; i++){
				let temp={
					// "accountNumber":"",
					 "accountType": this.getAccountList[i]["typeId"],
					 "typeValue": this.getAccountList[i]["typeValue"],
					 "accountDescription":[],
					 "accountNumberIndex":[]
				}
				this.accountNumberList.push(temp);
			}
        });
	 }
	 updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.accountGroupSetupService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
			this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

	getSegmentCount()
	{
		 this.AccountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}

	save(accountGroupSetup:NgForm){
		// let submittedData = {
		// 	'accountGroupId': this.accountGroup.accountGroupId,
		// 	'description': this.accountGroup.description,
		// 	'descriptionArabic': this.accountGroup.descriptionArabic,
		// }
		// submittedData["accountNumberList"] = [];
		// for(let i=0;i<this.accountNumberList.length;i++){
		// 	if(this.accountNumberList[i]["accountNumber"])
		// 	{
		// 		submittedData["accountNumberList"].push({
		// 			"accountNumber": this.accountNumberList[i]["accountNumber"],
		// 			"accountTypeId":this.accountNumberList[i]["accountTypeId"],
		// 			"accountDescription":this.accountNumberList[i]["accountDescription"].trim()
		// 		});				
		// 	}
		// }
		this.btndisabled=true;
		 let submittedData = {
			'accountGroupId': this.accountGroup.accountGroupId,
			'description': this.accountGroup.description,
			'descriptionArabic': this.accountGroup.descriptionArabic,
		}
		submittedData["accountNumberList"] = [];
		var accountNumberIndexArr=[];
		var tempAccountNumberList=[];
		for(let i=0;i<this.accountNumberList.length;i++){
			if(this.accountNumberList[i]["accountNumberIndex"])
			{
				var accountDesc = this.accountNumberList[i]["accountDescription"].join();
				accountDesc=accountDesc.replace(/,/g , " ");
				accountDesc=accountDesc.replace("0"," ");
				accountDesc=accountDesc.replace(0," ");
				if(this.accountNumberList[i]["accountNumberIndex"].length > 0)
				{
					tempAccountNumberList.push({
					"accountType":this.accountNumberList[i]["accountType"],
					"accountDescription": accountDesc,
					"accountNumberIndex":this.accountNumberList[i]["accountNumberIndex"]
				});	
				}
			}
		 }
		 submittedData["accountNumberList"] = tempAccountNumberList;
            
            //  this.accountNumberList = tempAccountNumberList;
			//  this.model.customerClassId=this.selectedCustomer[0].id;
	    if(this.add){
		this.accountGroupSetupService.save(submittedData).then(data => {
			
			window.scrollTo(0, 0);
			this.showBtns=false;
				var datacode = data.code;
				this.btndisabled=false;
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					accountGroupSetup.resetForm();
					this.ClearForm(accountGroupSetup);
					this.setPage({ offset: 0 });
					
					for(var i=0;i<this.accountNumberList.length;i++)
					{
						if(this.accountNumberList[i]["accountNumberIndex"]["arrSegment"])
						{
						var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
						 for(var k=0;k<currentRowSegment.length;k++)
						 {
							  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
							  if(k > 0)
							  {
								   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
								   segment.disabled=true;
							  }
							  else{
								segment.disabled=false;  	
							  }
							  segment.value = '';
						 }
						}
			
						this.accountNumberList[i]["accountDescription"] =[];	
						this.accountNumberList[i]["accountNumberIndex"] =[];
				}	
				}, 100);
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}
			else{
				
			this.accountGroupSetupService.save(submittedData).then(data => {
				window.scrollTo(0, 0);
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				this.showBtns=false;
				accountGroupSetup.resetForm();
				this.ClearForm(accountGroupSetup);
				for(var i=0;i<this.accountNumberList.length;i++)
				{
					if(this.accountNumberList[i]["accountNumberIndex"]["arrSegment"])
					{
					var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
					 for(var k=0;k<currentRowSegment.length;k++)
					 {
						  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
						  if(k > 0)
						  {
							   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
							   segment.disabled=true;
						  }
						  else{
							segment.disabled=false;  	
						  }
						  segment.value = '';
					 }
					}
		
					this.accountNumberList[i]["accountDescription"] =[];	
					this.accountNumberList[i]["accountNumberIndex"] =[];
			}	
			this.setPage({ offset: 0 });
				this.disableId = false;
				this.add = true;
				window.setTimeout(() => {
					
				
					this.showMsg = false;
					this.hasMsg = false;
				
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = "Already Exist";
			  this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		}
	}

	edit(row : any){
		this.add = false;
		this.disableId = true;
		this.showBtns=true;
		this.accountGroupSetupService.getAccountGroupById(row.accountGroupId).then(data => {
			//this.records = data.result;	
			this.accountGroup.accountGroupId = data.result.accountGroupId;
			this.accountGroup.description = data.result.description;
			this.accountGroup.descriptionArabic = data.result.descriptionArabic;
		//	this.accountNumberList =data.result.accountNumberList;
		// });
		// this.AccountReceivableClassGLSetupService.getAccountRecievableClassGlSetupByCustomerClassId(row.accountGroupId).then(data => {
		//   this.accountNumberList=data.result.accountNumberList;
		for(var i=0;i<this.accountNumberList.length;i++)
		   {
			  
			  var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
			  if(currentRowSegment)
			  {
			  	for(var k=0;k< currentRowSegment.length;k++)
			  	{
			  	  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
				  if(k>0)
				  {
				    segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
					if(segment)
					{
						segment.disabled=true;
					}
				  }
				  else{
					if(segment)
					{
						segment.disabled=false;
					}
				  }
				  segment.value = '';
				}
				this.accountNumberList[i]["accountDescription"] =[];
				this.accountNumberList[i]["accountNumberIndex"] =[];
			    this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = [];
			  }
					
					 
			   for(var j =0;j<data.result.accountNumberList.length;j++)
			   {
				   if(this.accountNumberList[i]["accountType"] == data.result.accountNumberList[j]["accountType"])
				   {
					 this.accountNumberList[i]["accountDescription"] = data.result.accountNumberList[j]["accountDescription"].split(',');
					 this.accountNumberList[i]["accountNumberIndex"] = data.result.accountNumberList[j]["accountNumberIndex"];
					 this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = data.result.accountNumberList[j]["accountNumber"].split('-');
					 var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
					
					 for(var k=0;k<currentRowSegment.length;k++)
					 {
						  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
						  if(k > 0)
						  {
							   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
						  }
						  segment.disabled=false;
						  if(currentRowSegment[k] == 0)
						  {
							  segment.value = '';
						  }	
						  else{
							segment.value = currentRowSegment[k];
						  }
					 }
				   }
			   }
		   }
		//   for(var i=0;i<this.accountNumberList.length;i++)
		//    {
			   
		// 	  var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
		// 	  if(currentRowSegment)
		// 	  {
		// 	  	for(var k=0;k< currentRowSegment.length;k++)
		// 	  	{
		// 	  	  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
		// 			if(segment)
		// 			{
		// 	  	  if(k > 0)
		// 	  	  {
		// 	  		   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
		// 	  		   segment.disabled= true;
		// 	  	  }
		// 	  	  else{
		// 	  		  segment.disabled=false;
		// 	  	  }
     	// 	  	  segment.value = '';
		// 	      }
		// 		  }
		// 		  this.accountNumberList[i]["accountDescription"] =[];
		// 		  this.accountNumberList[i]["accountNumberIndex"] =[];
		// 	      this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = [];
		// 	  }
					
					 
		// 	   for(var j =0;j<data.result.accountNumberList.length;j++)
		// 	   {
		// 		   if(this.accountNumberList[i]["accountType"] == data.result.accountNumberList[j]["accountType"])
		// 		   {
		// 			 this.accountNumberList[i]["accountDescription"] = data.result.accountNumberList[j]["accountDescription"].split(',');
		// 			 this.accountNumberList[i]["accountNumberIndex"] = data.result.accountNumberList[j]["accountNumberIndex"];
		// 			 this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = data.result.accountNumberList[j]["accountNumber"].split('-');
		// 			 var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
				
		// 			 for(var k=0;k<currentRowSegment.length;k++)
		// 			 {
		// 				  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
		// 				  if(k > 0)
		// 				  {
		// 					   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
		// 				  }
		// 				  segment.disabled=false;
		// 				  if(currentRowSegment[k] == 0)
		// 				  {
		// 					  segment.value = '';
		// 				  }	
		// 				  else{
		// 					segment.value = currentRowSegment[k];
		// 				  }
		// 			 }
		// 		   }
		// 		   	   }
		//    }
		});
			// this.accountGroup = {
			// 	'accountGroupId': this.records.accountGroupId,
			// 	'description': this.records.description,
			// 	'descriptionArabic': this.records.descriptionArabic
			//   };
		// });
	}
    
	fetchFirstAccountIndexInfo(i,segmentIndex){
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+segmentIndex);
		var segmentNumber=segment.value;

		var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));

		if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		
		if(segmentNumber)
		{
			this.AccountReceivableClassGLSetupService.firstTextApi(segmentNumber).subscribe(pagedData => {
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberList[i].accountNumberIndex[segmentIndex])
					{
						this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountNumberList[i].accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountNumberList[i]["accountDescription"].push(result.mainAccountDescription);
						this.accountNumberList[i]["accountNumberIndex"].push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
						this.accountNumberList[i].accountNumberIndex[m]='0';
						this.accountNumberList[i].accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
						}
						if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountNumberList[i]["accountDescription"] = [];
					this.accountNumberList[i]["accountNumberIndex"] = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
					{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
						if(m == 0)
						{
							segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+m);
						}
						segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
				this.accountNumberList[i]["accountDescription"] = [];
				this.accountNumberList[i]["accountNumberIndex"] = [];

				var currentSegment=segmentIndex;
				for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
				{
					var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
					segment.value = '';
					segment.disabled = true;
				}
		}
	}
	fetchOtherAccountIndexInfo(i,segmentIndex){
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
		var segmentNumber=segment.value;
		var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));
		if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberList[i].accountNumberIndex.indexOf(segment.value);
		if(acctIndex > -1)
		{
			this.toastr.warning(this.sameAccountNumberMsg);
			segment.value = '';
		}
		else{
			if(segmentNumber)
			{
				this.AccountReceivableClassGLSetupService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
					let status = pagedData.status;
					let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberList[i].accountNumberIndex[segmentIndex] || this.accountNumberList[i].accountNumberIndex[segmentIndex] == '0')
						{
							this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountNumberList[i].accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountNumberList[i]["accountDescription"].push(result.dimensionDescription);
							this.accountNumberList[i]["accountNumberIndex"].push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
						segment.value = '';
						this.accountNumberList[i].accountNumberIndex[segmentIndex]='0';
						this.accountNumberList[i].accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberList[i].accountNumberIndex[currentSegment]='0';
				this.accountNumberList[i].accountDescription[currentSegment]='0';
			}
		}
	}
	
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.accountGroupSetupService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

	getAccountTypeArray()
	{
        this.AccountTypeList = [{
			"accountType": '',
			"accountDescription": "",	
			"accountNumberIndex": []
     	}]
	}



	ClearForm(accountGroupSetup:NgForm){
		this.accountGroup.description='';
		this.accountGroup.descriptionArabic='';
	    for(var i=0;i<this.accountNumberList.length;i++)
		{
			if(this.accountNumberList[i]["accountNumberIndex"]["arrSegment"])
			{
			var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
			 for(var k=0;k<currentRowSegment.length;k++)
			 {
				  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
				  if(k > 0)
				  {
					   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
					   segment.disabled=true;
				  }
				  else{
					segment.disabled=false;  	
				  }
				  segment.value = '';
			 }
			}

			this.accountNumberList[i]["accountDescription"] =[];	
			this.accountNumberList[i]["accountNumberIndex"] =[];
    }	     
	}

	
    Cancel(accountGroupSetup)
    {
    this.add=true;
	this.ClearForm(accountGroupSetup);
    this.accountGroup.accountGroupId= '';
	this.disableId=false;
    }

	ngOnInit() {
		this.accountNumberList=[];
        this.getSegmentCount();
		this.setPage({ offset: 0 });
	}	
	
	 changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	
			/** If Screen is Lock then prevent user to perform any action.
			*  This function also cover Role Management Write acceess functionality */
			LockScreen(writeAccess)
			{
				if(!writeAccess)
				{
					return true
				}
				else if(this.btndisabled)
				{
					return true
				}
				else if(this.isScreenLock)
				{
					return true;
				}
				else{
					return false;
				}
			}
			
}