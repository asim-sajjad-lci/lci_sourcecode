import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { InsuranceClassSetup } from '../../../../financialModule/_models/fixedAssets/insuranceClassSetup';
import { InsuranceClassSetupService } from '../../../../financialModule/_services/fixedAssets/insuranceClassSetup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<retirement-setup></retirement-setup>',
    templateUrl: './insuranceClassSetup.component.html',
    styles: ["user.component.css"],
    providers:[InsuranceClassSetupService]
})

//export to make it available for other classes
export class InsuranceClassSetupComponent {
    page = new Page();
    rows = new Array<InsuranceClassSetupService>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    insuranceclassid;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean=false;
    cardTypeStatus =[];
    // Serachable Select Box Step1
	ArrInsuranceClassId = [];
    insuranceClassId : any = [];
    ddlInsuranceClassIdSetting = {};
    // Serachable Select Box Step1 End
    
    btndisabled:boolean=false;
    isScreenLock;
  
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private InsuranceClassSetupService: InsuranceClassSetupService,
        private getScreenDetailService: GetScreenDetailService){
     
        this.page.pageNumber = 0;
        this.page.size = 5;
    }

    ngOnInit() {
                this.getAddScreenDetail();
                this.getInsuranceClassId();
                // this.setPage({ offset: 0 });
           // Serachable Select Box Step2
		   this.ddlInsuranceClassIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class"
         }; 
		 // Serachable Select Box Step2
         
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1185";
           // default form parameter for adding exchange structureSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_INS_CLASS_INS_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_INS_CLASS_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_INS_CLASS_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_INS_CLASS_INFLATION_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_INS_CLASS_DEP_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_INS_CLASS_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_INS_CLASS_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            ];
         this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
			{
				this.isScreenLock = res;
			});
         this.getScreenDetail(this.screenCode,'Add');
    }

    getScreenDetail(screenCode,ArrayType)
    {
       
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
         
        });
    }

     getInsuranceClassId()
   {
       
              this.InsuranceClassSetupService.getdropdownInsuranceClassId().then(data => {
            //   this.insuranceclassid = data.result.records;
            //   this.insuranceclassid.splice(0,{ "insClassId": "", "insuranceYear": this.select });
                // Searchable Select Box Step3
                for(var i=0;i< data.result.records.length;i++)
                {
                    this.ArrInsuranceClassId.push({ "id": data.result.records[i].insClassId, "itemName": data.result.records[i].insClassId})
                }
			    // Searchable Select Box Step3 End
       });
   }

    // Serachable Select Box Step4
    getInsuranceClassByID(item:any){
        this.model.insuranceClassId=item.id;
        // this.model.insuranceClassId.push({'id':item.id,'itemName':item.itemName});
    }
    OnInsuranceClassIdDeSelect(item:any){
        this.model.insuranceClassId='';
    }
    // Serachable Select Box Step4 ends


    saveInsuranceClassSetup(f: NgForm)
    {
        this.btndisabled=true; 
        this.InsuranceClassSetupService.createInsuranceClassSetup(this.model).then(data => {
            var datacode = data.code;
            this.btndisabled=false;
            this.insuranceClassId=[];
              if (datacode == 201) {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
            f.resetForm();
         }).catch(error => {
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
            this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
         });
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
         
    	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
         
    }

