import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {physicalLocationSetup} from '../../../../financialModule/_models/fixedAssets/physicalLocationSetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {physicalLocationService} from '../../../../financialModule/_services/fixedAssets/physicalLocation.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'physical-location',
   templateUrl: './physical-location.component.html', 
    providers: [physicalLocationService,CommonService],
})

export class physicalLocationComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
    locations;
    locationID;
	myHtml;
	btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
	// Serachable Select Box Step1
	ArrLocationId = [];
    locationId : any = [];
    ddlLocationIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<physicalLocationSetup>();
    temp = new Array<physicalLocationSetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	tableViewtext=Constants.tableViewtext;
	EmptyMessage = Constants.EmptyMessage;
	search=Constants.search;
	btndisabled:boolean=false;
	isScreenLock

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	physicalLocation = {
    'physicalLocationId': '',
    'descriptionPrimary': '',
    'descriptionSecondary': '',
    'locationId': '',
  };
	
	constructor(private router: Router,private route: ActivatedRoute,private physicalLocationService: physicalLocationService,private getScreenDetailService: GetScreenDetailService, private commonService:CommonService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.disableId = false;
		this.locationID=[];
		this.physicalLocationService.searchLocation().subscribe(pagedData => {
            // this.locations = pagedData.records;
			            // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrLocationId.push({ "id": pagedData.records[i].locationIndex, "itemName": pagedData.records[i].locationId})
              }
			    // Searchable Select Box Step3 End
        });
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_PHY_LOCATION_SETUP_PHY_LOCATION_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_PHY_LOCATION_SETUP_DESC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_PHY_LOCATION_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_PHY_LOCATION_SETUP_LOCATION_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_PHY_LOCATION_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_PHY_LOCATION_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_PHY_LOCATION_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
		];

		
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1146").then(data => {
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_PHY_LOCATION_SETUP_PHY_LOCATION_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_PHY_LOCATION_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_PHY_LOCATION_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_PHY_LOCATION_SETUP_LOCATION_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_PHY_LOCATION_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_PHY_LOCATION_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				
				];

		this.getScreenDetailService.ValidateScreen("S-1145").then(res=>
			{
				this.isScreenLock = res;
			});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1145").then(data => {
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
		
	}		
		//Is id already exist
		IsIdAlreadyExist(){
			if(this.physicalLocation.physicalLocationId != '' && this.physicalLocation.physicalLocationId != undefined && this.physicalLocation.physicalLocationId != 'undefined')
			  {
				this.physicalLocationService.getPhysicalLocationById(this.physicalLocation.physicalLocationId).then(data => {
				   var datacode = data.code;
				   if (data.status == "NOT_FOUND" || data.btiMessage.messageShort =='RECORD_NOT_FOUND') {
						  return true;
					  }
					  else{
						  var txtId = <HTMLInputElement> document.getElementById("ddl_insClassId");
						  txtId.focus();
						  this.isSuccessMsg = false;
						  this.isfailureMsg = true;
						  this.showMsg = true;
						  this.hasMsg = true;
						  this.messageText = data.btiMessage.message;
						  this.setPage({ offset: 0 });
						  window.setTimeout(() => {
						  this.showMsg = false;
						  this.hasMsg = false;
							}, 4000);
							window.scrollTo(0,0);
							this.physicalLocation.physicalLocationId='';
							return false;
					  }
		  
					  }).catch(error => {
					  window.setTimeout(() => {
					  this.isSuccessMsg = false;
					  this.isfailureMsg = true;
					  this.showMsg = true;
					  this.hasMsg = true;
					  this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
				  }, 100)
			  });
		  }
		}
		
		 updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.physicalLocationService.searchPhysicalLocation(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

	save(physicalLocationSetup:NgForm){
		this.btndisabled=true;
		if(this.add){
			let submittedData = {
				'physicalLocationId': this.physicalLocation.physicalLocationId,
				'descriptionPrimary': this.physicalLocation.descriptionPrimary,
				'descriptionSecondary': this.physicalLocation.descriptionSecondary,
				'locationIndex': this.physicalLocation.locationId,
			}
			this.physicalLocationService.createPhysicalLocation(submittedData).then(data => {
			this.showBtns=false;
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				this.locationId=[];
			    physicalLocationSetup.resetForm();
				this.setPage({ offset: 0 });
				this.physicalLocation.locationId='';
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			  //this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		}else{
			let submittedData = {
				'physicalLocationId': this.physicalLocation.physicalLocationId,
				'descriptionPrimary': this.physicalLocation.descriptionPrimary,
				'descriptionSecondary': this.physicalLocation.descriptionSecondary,
				'locationIndex': this.physicalLocation.locationId,
			}
			this.physicalLocationService.editPhysicalLocation(submittedData).then(data => {
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				this.showBtns=false;
				this.locationId=[];
			    physicalLocationSetup.resetForm();
				this.setPage({ offset: 0 });
				this.physicalLocation.locationId='';
				this.disableId = false;
				this.add = true;
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			  //this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		}
	}
	
	edit(row : any){
		this.locationId=[];
		this.add = false;
		this.disableId = true;
		this.showBtns=true;
		this.physicalLocationService.getPhysicalLocationById(row.physicalLocationId).then(data => {
			this.records = data.result;	
			var selectedLocation=this.ArrLocationId.find(x => x.id ==  data.result.locationIndex);
			this.locationId.push({'id':selectedLocation.id,'itemName':selectedLocation.itemName});		
			this.physicalLocation = {
				'physicalLocationId': this.records.physicalLocationId,
				'descriptionPrimary': this.records.descriptionPrimary,
				'descriptionSecondary': this.records.descriptionSecondary,
				'locationId': this.records.locationIndex,
			  };
		});
	}
	ClearForm(physicalLocationSetup:NgForm){
		  this.locationId=[];
	      physicalLocationSetup.resetForm();
		  var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		  selectedValue.value=this.records.physicalLocationId;
		  this.physicalLocation.physicalLocationId = this.records.physicalLocationId;
				
			 if(!this.add){
				this.myHtml = "border-vanish";
			 }else{
				this.myHtml = "";
			 }
	}
	
	Cancel(physicalLocationSetup)
    {
    this.add=true;
    this.ClearForm(physicalLocationSetup);
    this.physicalLocation.physicalLocationId= '';
	this.disableId=false;
    }

	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.physicalLocationService.searchPhysicalLocation(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	// Serachable Select Box Step4
	getLocationByID(item:any){
		this.physicalLocation.locationId=item.id;
		this.commonService.closeMultiselectWithId("locationId")
	}
	OnLocationIdDeSelect(item:any){
		this.physicalLocation.locationId='';
		this.commonService.closeMultiselectWithId("locationId")

	}
	// Serachable Select Box Step4 ends

	ngOnInit() {
		   // Serachable Select Box Step2
		   this.ddlLocationIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
	}		
	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
		
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
}