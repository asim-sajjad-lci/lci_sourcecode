import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { BookSetup } from '../../../../financialModule/_models/fixedAssets/book-setup';
import { BookSetupService } from '../../../../financialModule/_services/fixedAssets/bookSetup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

@Component({
    selector: '<book-setup></book-setup>',
    templateUrl: './book-setup.component.html',
    styles: ["user.component.css"],
    providers:[BookSetupService]
})

//export to make it available for other classes
export class BookSetupComponent {
    page = new Page();
    rows = new Array<BookSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    ArrDepreciationPeriodTypes =[];
    calenderoption=[];
    calenderString=[];
    temp=[];
    disableId;
    myHtml;
    add;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    EmptyMessage = Constants.EmptyMessage;
    totalText = Constants.totalText;
    btndisabled:boolean=false;
    years;
    isScreenLock;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private bookSetupService: BookSetupService,
        private getScreenDetailService: GetScreenDetailService){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.disableId = false;
        this.add = true;
    }

    ngOnInit() {
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.getCalenderTypeIndex();
                this.getYears();
                this.setPage({ offset: 0 });
                        this.bookSetupService.getDepreciationPeriodTypesList().then(data => {
                        this.ArrDepreciationPeriodTypes = data.result;
                        this.ArrDepreciationPeriodTypes.splice(0, 0, { "typeId": "", "typeValue": this.select });
                        this.model.depreciationPeriodId='';
                  });
    }

    getYears()
    {
        this.bookSetupService.getYears().then(pagedData => {
            this.years = pagedData.result;
            this.model.currentYear='';
		});
    }

  //getCalenderTypeIndex
getCalenderTypeIndex()
{
    this.bookSetupService.getCalenderTypeIndex().then(data => {
            this.calenderoption = data.result;
            if(data.btiMessage.messageShort !='RECORD_NOT_FOUND')
            {
                this.temp.push(this.calenderoption);
            }
            // this.calenderoption.splice(0, 0, { "calenderId": "", "calenderIndex": this.select });
            // this.model.faCalendarSetupIndexId='';
            //  this.model.calenderIndex='';
 });
}
 resetMe(f)
    {
        this.model.bookDescription= '';
        this.model.bookDescriptionArabic= '';
        this.model.currentYear='';
        this.model.depreciationPeriodId='';
        this.model.faCalendarSetupIndexId='';
        this.model.bookIndexId='';
        this.model.calendarIndex='';
        this.model.calendarId='';
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.bookId='';
		 }
    }

    Cancel(f)
    {
    this.add=true;
    this.model.bookId= '';
    this.resetMe(f);
    this.disableId=false;
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1121";
           // default form parameter for adding exchange structureSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_BOOK_SETUP_BOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_BOOK_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_SETUP_ARABIC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_BOOK_SETUP_CALENDAR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_SETUP_CURRENT_FISCAL_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_SETUP_DEPRECIATION_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
           
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1122";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_BOOK_SETUP_BOOK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_SETUP_ARABIC_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_SETUP_CURRENT_FISCAL_YEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_SETUP_DEPRECIATION_PERIOD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
             
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }
    // getYears()
    // {
    //     var min = new Date().getFullYear() - 5,
    //     max = min + 10,
    //     select = <HTMLSelectElement>document.getElementById('ddlcurrentYear');

    //     for (var i = min; i<=max; i++){
    //        var opt = <HTMLOptionElement>document.createElement('option');
    //        opt.value = i.toString();
    //        opt.innerHTML = i.toString();
    //        select.appendChild(opt);
    //     }
    // }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.bookSetupService.searchBookSetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

    getScreenDetail(screenCode,ArrayType)
    {
     
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.bookSetupService.searchBookSetup(this.page, this.searchKeyword).subscribe(pagedData => {
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
 
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    
    // getStructureSetupById(row: any)
    getBookSetupById(row: any)
    {
        this.showBtns=true;
        this.add = false;
        this.isModify=true;
         this.disableId = true;
         this.myHtml = "border-vanish";
         this.bookSetupService.getBookSetupByBookIndexId(row.bookIndexId).then(data => {
                    this.model = data.result; 
                    
         });
    }
      //Is id already exist
      IsIdAlreadyExist(){
    if(this.model.bookIndexId != '' && this.model.bookIndexId != undefined && this.model.bookIndexId != 'undefined')
      {
          this.bookSetupService.getBookSetupByBookIndexId(this.model.bookIndexId).then(data => {
           var datacode = data.code;
           if (data.status == "NOT_FOUND") {
                  return true;
              }
              else{
                  var txtId = <HTMLInputElement> document.getElementById("txtBookId");
                  txtId.focus();
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.hasMsg = true;
                  this.messageText = data.btiMessage.message;
                  this.setPage({ offset: 0 });
                  window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.model.bookIndexId='';
                return false;
              }
  
              }).catch(error => {
              window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
          }, 100)
      });
  }
  }
    savebookSetup(f: NgForm)
    {
        this.btndisabled=true;
        //this.model.faCalendarSetupIndexId = this.model.faCalendarSetupIndexId.calendarIndex;
        if(!this.add)
        {
             this.disableId = false;
             this.bookSetupService.updateBookSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 200) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.model.bookId=null;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.disableId = false;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
             this.bookSetupService.createBookSetup(this.model).then(data => {
                this.showBtns=false;
                this.disableId = true;
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                  else{
                     this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }

                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
	// nameInputFormatter = (result: { calendarId: string }) => result.calendarId
	// formatter = (result: { calendarId: string }) => result.calendarId
	// search = (text$: Observable<string>) =>
    // text$
    //   .debounceTime(200)
    //   .distinctUntilChanged()
    //   .map(term => term.length < 1 ? []
    //     : this.calenderoption.filter(v => v.calendarId.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
}