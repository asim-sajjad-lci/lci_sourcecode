import { Component,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CalenderQuarterSetup } from '../../../../financialModule/_models/fixedAssets/calender-quarter-setup';
import { CalenderQuarterSetupService } from '../../../../financialModule/_services/fixedAssets/calender-quarter-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 

@Component({
    selector: '<calender-quarter-setup></calender-quarter-setup>',
    templateUrl: './calender-quarter-setup.component.html',
    styles: ["user.component.css"],
    providers:[CalenderQuarterSetupService]
})

//export to make it available for other classes
export class CalenderQuarterSetupComponent {
    page = new Page();
    rows = new Array<CalenderQuarterSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    isScreenLock;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    quarter=Constants.quarter;
    firstQuarter=Constants.firstQuarter;
    secondQuarter=Constants.secondQuarter;
    thirdQuarter=Constants.thirdQuarter;
    forthQuarter=Constants.forthQuarter;
    InvalidDate=Constants.InvalidDate;
    InvalidMonth=Constants.InvalidMonth;
    InvalidYear=Constants.InvalidYear;
    InvalidDay=Constants.InvalidDay;
    mustBe=Constants.mustBe;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean=false;
    cardTypeStatus =[];
    showCalander:boolean=false;
    calenderIdShow;
    editData;
    finalMonth :string;
    finalDate :string;
    fullDate :string;
    arrYears :any;
    btndisabled:boolean=false;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private calenderQuarterSetupService: CalenderQuarterSetupService,
        private toastr: ToastsManager,
        vcr: ViewContainerRef,
        private getScreenDetailService: GetScreenDetailService){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.getCalendarQuarterSetup();
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
                this.getAddScreenDetail();
                this. getYears();
            
    }


    getAddScreenDetail()
    {
         this.screenCode = "S-1105";
           // default form parameter for adding exchange structureSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_CAL_QUAR_CAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CAL_QUAR_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CAL_QUAR_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CAL_QUAR_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CAL_QUAR_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CAL_QUAR_START_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CAL_QUAR_MID_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CAL_QUAR_END_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CAL_QUAR_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CAL_QUAR_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }
    getYears()
    {
        this.arrYears=[];
        var min = new Date().getFullYear() - 5,
        max = min + 10;
        
        for (var i = min; i<=max; i++){
            this.arrYears.push({'year':i});
        }
    }

   parseDate(str) {
     var mdy = str.split('/');
     return new Date(mdy[2],mdy[0] , mdy[1]);
   }

   daydiff(first, second) {
     return Math.round((second-first)/(1000*60*60*24));
   }
      AddSubstractDay(Mydate,numberOfDay, Type) {
         //date = new Date();
        if (Type == '+') {
            Mydate.setDate(Mydate.getDate() + numberOfDay);
        }
        else {
            Mydate.setDate(Mydate.getDate() - numberOfDay);
        }
       
        if (Mydate.getMonth() < 10) {
            this.finalMonth = '0' + (Mydate.getMonth() + parseInt('1'));
        }
        else {
            this.finalMonth = (Mydate.getMonth() + parseInt('1')).toString();
        }
        if (Mydate.getDate() < 10) {
            this.finalDate = '0' + (Mydate.getDate() - parseInt('1'));
        }
        else {
            this.finalDate = (Mydate.getDate() - parseInt('1')).toString();
        }
        // this.fullDate = date.getFullYear() + '-' + this.finalMonth + '-' + this.finalDate;
        this.fullDate = this.finalDate + '/' +this.finalMonth + '/' + Mydate.getFullYear();
        return this.fullDate;
    }
    getQuarterDetail(event)
    {
       this.showCalander=true;
       this.model.quarterCalendar = [];
       var CurrentYear =event.target.value;
       this.model.year1=CurrentYear;
       var date = new Date();
       var quarterIndex = 0;
       for(var i=0;i< 12;i++)  
       {
          var firstDay = new Date(CurrentYear, i, 1);
          var lastDay = new Date(CurrentYear, i + 1, 0);
          var firstDayWithSlashes = "";
          var lastDayWithSlashes = "";
          var tempfirstDayWithSlashes = "";
          var templastDayWithSlashes = "";
          var midDayWithSlashes = "";

          var quarter;
         
          if (firstDay.getMonth() % 3 == 0 )
          {
              
            //   var midDate= (firstDay.getTime()  + lastDay.getTime()) / 2;
              quarterIndex = quarterIndex + 1;
              firstDayWithSlashes = (firstDay.getDate()) + '/' + (firstDay.getMonth() + 1 ) + '/' + firstDay.getFullYear();
              tempfirstDayWithSlashes = ((firstDay.getMonth() + 1 )) + '/' + (firstDay.getDate()) + '/' + firstDay.getFullYear();
              
              if(quarterIndex == 3)
              {
                lastDayWithSlashes = (lastDay.getDate()-1) + '/' + (lastDay.getMonth() + 3 ) + '/' + lastDay.getFullYear();
                templastDayWithSlashes = (lastDay.getMonth() + 3 ) + '/' + (lastDay.getDate()-1) + '/' + lastDay.getFullYear();

              }
              else
              {
                lastDayWithSlashes = (lastDay.getDate()) + '/' + (lastDay.getMonth() + 3 ) + '/' + lastDay.getFullYear();
                templastDayWithSlashes = (lastDay.getMonth() + 3 ) + '/' + (lastDay.getDate()) + '/' + lastDay.getFullYear();
              }
             
              var daydiff=this.daydiff(this.parseDate(tempfirstDayWithSlashes),this.parseDate(templastDayWithSlashes)) + parseInt("1");
             
              var AddDays =Math.round(parseInt(daydiff.toString()) / 2);
            
              var startdateData = tempfirstDayWithSlashes.split('/');
              midDayWithSlashes = this.AddSubstractDay(firstDay,AddDays,'+')
              this.model.quarterCalendar.push({"quarterIndex": quarterIndex ,"startDate":firstDayWithSlashes,"endDate":lastDayWithSlashes,"midDate": midDayWithSlashes});      
               
          }
       }
       
    }
    getScreenDetail(screenCode,ArrayType)
    {
       
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

     
    // setPage(pageInfo) {
    //     this.selected = []; // remove any selected checkbox on paging
    //     this.page.pageNumber = pageInfo.offset;
    //     this.calenderQuarterSetupService.searchFaCalendarSetup(this.page, this.searchKeyword).subscribe(pagedData => {
    //        this.page = pagedData.page;
    //        this.rows = pagedData.data;
    //     });
    // }
 
    // changePageSize(event) {
    //     this.page.size = event.target.value;
    //     this.setPage({ offset: 0 });
    // }
  
 getCalendarQuarterSetup()
   {
        this.calenderIdShow=false;
       
        this.calenderQuarterSetupService.getCalendarQuarterSetup().then(data => {
             this.editData = data.result;
       
        if(this.editData)
         {
            this.model = data.result;
            if(data.result.calendarId!=null){
            this.calenderIdShow=true;
            this.showCalander=true;
            }
            
         }
      });
   }
    resetForm(f){
    this.model.calendarDescription='';
    this.model.calendarDescriptionArabic='';
    this.model.year1='';
    this.model.monthlyCalendar='';
     
 
   }
    
    // getCalendarQuarterSetupById(row: any)
    getCalendarQuarterSetupById(row: any)
    {
        this.showCalander=true;
        this.isModify=true;
         this.calenderQuarterSetupService.getCalendarQuarterSetupById(row.calendarIndex).then(data => {
                    this.model = data.result; 
         });
    }
    saveCalenderQuarterSetup(f: NgForm)
    {
        this.btndisabled=true;
        if(this.isModify)
        {
             this.calenderQuarterSetupService.createCalendarQuarterSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                       
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
              
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
             this.calenderQuarterSetupService.createCalendarQuarterSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                       
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                  else{
                     this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
               
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }


    }
    getDaysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
    }

    validateDate(Id)
    {
        var txtEndDate=<HTMLInputElement>document.getElementById('txtEndDate'+Id);
        var quarterDate=txtEndDate.value;

         // regular expression to match required date format
        var  datePattern = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

        if(quarterDate != '') {
          if(quarterDate.match(datePattern)) {

            var arrQuarterDate=quarterDate.split('/')
            // day value between 1 and 31
            var daysInMonth = this.getDaysInMonth(parseInt(arrQuarterDate[1]),parseInt(arrQuarterDate[2]));
            if(parseInt(arrQuarterDate[0]) < 1 || parseInt(arrQuarterDate[0]) > daysInMonth) {
               this.toastr.warning(this.InvalidDay + arrQuarterDate[0]);	
               txtEndDate.value = '';
               return false;
            }

            // month value between 1 and 12
            if(parseInt(arrQuarterDate[1]) < 1 || parseInt(arrQuarterDate[1]) > 12) {
              this.toastr.warning(this.InvalidMonth + arrQuarterDate[1]);	
              txtEndDate.value = '';
            //   form.startdate.focus();
              return false;
            }
            // year value between 1902 and 2017
            if(parseInt(arrQuarterDate[2]) != this.model.year1) {
              this.toastr.warning(this.InvalidYear + arrQuarterDate[2] + this.mustBe + this.model.year1);	
              txtEndDate.value = '';
              return false;
            }

            var tempQuarterDate = arrQuarterDate[1] + '/' + arrQuarterDate[0] + '/' + arrQuarterDate[2];
            var date = new Date(this.model.year1,parseInt(arrQuarterDate[1]),parseInt(arrQuarterDate[0]));
            var newdate = new Date(date);
            newdate.setDate(newdate.getDate() + 1); // minus the date
            var nextQuarterStartDate = this.addDays(tempQuarterDate,1);

            var arrstartDate=this.model.quarterCalendar[Id - 1]["startDate"].split('/');
            var tempStartDate=arrstartDate[1] + '/' + arrstartDate[0] + '/' + arrstartDate[2];
           
            var daydiff=this.daydiff(this.parseDate(tempStartDate),this.parseDate(tempQuarterDate)) + parseInt("1");
            var AddDays =Math.round(parseInt(daydiff.toString()) / 2);
            var midDate=this.addDays(tempStartDate,AddDays);

            var arrNextEndDate=this.model.quarterCalendar[Id]["endDate"].split('/');
            var tempNextEndDate=arrNextEndDate[1] + '/' + arrNextEndDate[0] + '/' + arrNextEndDate[2];

            var arrNextStartDate = nextQuarterStartDate.split('/');
            var tempNextStartDate=arrNextStartDate[1] + '/' + arrNextStartDate[0] + '/' + arrNextStartDate[2];


            var daydiff=this.daydiff(this.parseDate(tempNextStartDate),this.parseDate(tempNextEndDate)) + parseInt("1");
            var AddDays =Math.round(parseInt(daydiff.toString()) / 2);
            var nextMidDate=this.addDays(tempNextStartDate,AddDays);

            this.model.quarterCalendar[Id]["startDate"]=nextQuarterStartDate;
            this.model.quarterCalendar[Id-1]["endDate"]=quarterDate;
            this.model.quarterCalendar[Id-1]["midDate"]=midDate;

             this.model.quarterCalendar[Id]["midDate"]=nextMidDate;
          } else {
            this.toastr.warning(this.InvalidDate + quarterDate);
            txtEndDate.value = '';
            return false;
          }
    }
}

addDays(currentdate,n){
    var t = new Date(currentdate);
    t.setDate(t.getDate() + n); 
    var month = "0"+(t.getMonth()+1);
    var date = "0"+t.getDate();
    month = month.slice(-2);
    date = date.slice(-2);
    date = date +"/"+month +"/"+t.getFullYear();
    return date;
}
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
}