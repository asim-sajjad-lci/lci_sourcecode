import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { BookClassSetup } from '../../../../financialModule/_models/fixedAssets/bookClassSetup';
import { BookClassSetupService } from '../../../../financialModule/_services/fixedAssets/bookClassSetup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<bookClassSetup></bookClassSetup>',
    templateUrl: './bookClassSetup.component.html',
    styles: ["user.component.css"],
    providers:[BookClassSetupService,CommonService]
})

//export to make it available for other classes
export class BookClassSetupComponent {
    page = new Page();
    rows = new Array<BookClassSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    bookIDoptions;
    classIDoptions;
    AmortizationCodeListoptions;
    convention;
	sw;
	am;
	dep;
    specialDepreciationAllowanceoptions;
    bookDescription:string;
    descriptionArabic:string;
    yearlyDepreciatedRate:number;
    depreciatedDate:string;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean=false;
    salvageEstimatecheck: number=0;
    salvageEstimate: number=0;
    cardTypeStatus =[];
    // Serachable Select Box Step1
	ArrBookId = [];
    bookId : any = [];
    ddlBookIdSetting = {};
    ArrClassId = [];
    classId : any = [];
    ddlClassIdSetting = {};
	// Serachable Select Box Step1 End
    checkIfOthersAreSelected:boolean=true;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    AmountText = Constants.AmountText;
    PercentageText = Constants.PercentageText;
    YesText = Constants.YesText;
    NoText = Constants.NoText;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private bookClassSetupService: BookClassSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.model.amortizationAmountAllowance = 'percentage';
    }

    ngOnInit() {

        // Serachable Select Box Step2
		   this.ddlBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 
          this.ddlClassIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         };
		 // Serachable Select Box Step2
                this.salvageEstimate;
                this.salvageEstimatecheck;
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });
                this.getbookIdList();
                this.bookIDoptions="";
                this.getclassIdList();
                this.specialDepreciationAllowanceoptions="";
                this.classIDoptions="";
                this.bookDescription="";
                this.descriptionArabic="";
                this.yearlyDepreciatedRate;
               
                this.bookClassSetupService.getConventionList().then(data => {
                    this.convention = data.result;
                    this.convention.splice(0,0,{ "typeId": "", "name": this.select });
                    this.model.averagingConvention='';
                });
                this.bookClassSetupService.getSwitchOverList().then(data => {
                    this.sw = data.result;
                    this.sw.splice(0,0,{ "typeId": "", "name": this.select });
                    this.model.swtch='';
                });
                this.bookClassSetupService.getamortizationCodeList().then(data => {
                    this.am = data.result;
                    this.am.splice(0,0,{ "typeId": "", "name": this.select });
                    this.model.amortizationCode='';
                });
                this.bookClassSetupService.getDepList().then(data => {
                    this.dep = data.result;
                    this.dep.splice(0,0,{ "typeId": "", "name": this.select });
                    this.model.depreciationMethodId='';
                });
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1201";
           // default form parameter for adding BookClassSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_BOOK_CLASS_BOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_BOOK_CLASS_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_AR_BOOK_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_BOOK_CLASS_AR_CLASS_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_DEP_METHOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_AVG_CONVENTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_SWITCHOVER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_ORIG_LIFE_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_SP_DEP_ALLOWANCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_AMOR_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_AMOR_AMT_PCT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_INI_ALLOW_PCT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_SALVAGE_EST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_BOOK_CLASS_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1202";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_BOOK_CLASS_BOOK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_CLASS_CLASS_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_CLASS_DEP_METHOD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_CLASS_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_CLASS_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_BOOK_CLASS_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    // setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.bookClassSetupService.searchBookClassSetup(this.page, this.searchKeyword).subscribe(pagedData => {
           
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.bookClassSetupService.searchBookClassSetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
    
    resetMe(f){
    f.resetForm();
    this.bookId=[];
    this.classId=[];
    }
    
        Cancel(f)
    {
    this.isModify=false;
    this.resetMe(f);
    }

    getbookIdList()
   {
              this.bookClassSetupService.getBookIDSetup().then(data => {
            //   this.bookIDoptions = data.result.records;
            //   this.bookIDoptions.splice(0,0,{ "bookIndexId": "", "bookDescription": this.select });
            //   this.model.bookId='';
              // Searchable Select Box Step3
             
              if(data.btiMessage.messageShort !='RECORD_NOT_FOUND' && data.result.records.length > 0)
              {
                for(var i=0;i< data.result.records.length;i++)
                {
                  this.ArrBookId.push({ "id": data.result.records[i].bookIndexId, "itemName": data.result.records[i].bookId})
                }
              }
			    // Searchable Select Box Step3 End
       });
   }

   getclassIdList()
   {
        this.bookClassSetupService.getClassIDSetup().then(data => {
            if(data.btiMessage.messageShort == 'RECORD_GET_SUCCESSFULLY')
            {
               for(var i=0;i< data.result.records.length;i++)
               {
                 this.ArrClassId.push({ "id": data.result.records[i].classId, "itemName": data.result.records[i].description})
               }
            }
         });
   }

    // Serachable Select Box Step4
    getBookByID(item:any){
       
        this.showBtns=true;
        this.model.bookId=item.itemName;
        this.bookId.push({'id':item.itemName});
	    this.bookClassSetupService.getBookSetupByBookIndexId(item.id).then(data => {
           
			 this.bookDescription = data.result.bookDescription;
        });
        this.commonService.closeMultiselectWithId("bookId")
        
    }
    OnBookIdDeSelect(item:any){
        this.model.bookDescription='';
        this.commonService.closeMultiselectWithId("bookId")
    }
    // Serachable Select Box Step4 ends

    // Serachable Select Box Step4
    getClassByID(item:any){
        this.model.classId=item.id;
        this.classId.push({'id':item.id});
	    this.bookClassSetupService.getData(item.id).then(data => {
			 this.descriptionArabic = data.result.descriptionArabic;
        });
        this.commonService.closeMultiselectWithId("classId")
    }
    OnClassIdDeSelect(item:any){
        this.model.descriptionArabic='';
        this.commonService.closeMultiselectWithId("classId")
    }
    // Serachable Select Box Step4 ends

checkSelected(e) {
   if(this.model.specialDepreciationAllowance=='yes'){

        this.checkIfOthersAreSelected = false;
   }
   else{
        this.model.specialDepreciationAllowancePercent='';
        this.checkIfOthersAreSelected = true;
   }
}

ChangeAmountAllowance()
{
    if(this.model.amortizationAmountAllowance == 'percentage')
    {
        this.model.amortizationAmount='';
    }
    else if(this.model.amortizationAmountAllowance == 'amount')
    {
        this.model.amortizationPercentage='';
    }
}
GetbookSetupInfo(event){
     var bookId = this.model.bookId;
     this.bookClassSetupService.getBookSetupByBookIndexId(bookId).then(data => {
      this.bookDescription = data.result.bookDescription;
     });
}
GetclassSetupInfo(event){
     var classId= event.target.value;
     this.bookClassSetupService.getData(classId).then(data => {
     this.descriptionArabic = data.result.descriptionArabic;
   });
}

//    GetAccountSetupInfo(event){
//         var assetId= event.target.value;
//         this.bookClassSetupService.getDeprRate(assetId).then(data => {
//         this.yearlyDepreciatedRate = data.result.yearlyDepreciatedRate;
    
//       });
//    }
 
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    
    // getBookClassSetupById(row: any)
    getClassSetupById(row: any)
    {
       
        this.isModify=true;
         this.bookClassSetupService.getBookClassSetupById(row.bookIndex).then(data => {
            this.model = data.result; 
            // this.classId.push({'id':data.result.classId,'itemName':data.result.description});
            this.bookId=[];
            var selectedBook = this.ArrBookId.find(x => x.itemName ==   data.result.bookId);
            this.bookId.push({'id':selectedBook.id,'itemName':selectedBook.itemName});
            this.bookDescription = data.result.bookDescriptionArabic;
            this.classId=[];
            var selectedClass = this.ArrClassId.find(x => x.id ==   data.result.classId);
            this.classId.push({'id':selectedClass.id,'itemName':selectedClass.itemName});
            this.descriptionArabic = data.result.classSetupDescriptionArabic;

            if(this.model.amortizationAmount)
            {
                this.model.amortizationAmountAllowance = 'amount';
            }
            else 
            {
                this.model.amortizationAmountAllowance = 'percentage';
            }

            if(this.model.specialDepreciationAllowance)
            {
                this.model.specialDepreciationAllowance = 'yes';
            }
            else{
                this.model.specialDepreciationAllowance = 'no';
            }
            if(this.model.salvageEstimate){

                this.model.salvageEstimatecheck = true;
            }

         });
    }
    saveBookClassSetup(f: NgForm)
    {
       
        this.btndisabled=true;
        var active = 0;
        if(this.model.salvageEstimatecheck){
			active = 1;
        }
        if(!this.model.salvageEstimatecheck){
		    this.model.salvageEstimate = '';
		}
            
        this.model.salvageEstimatecheck= active;

        if(this.model.specialDepreciationAllowance=='yes'){
            
            this.model.specialDepreciationAllowance = 1;
        }
        else{
            this.model.specialDepreciationAllowance = 0;
        }

            // this.model.salvageEstimate=this.salvageEstimate;
        if(this.isModify)
        {
            this.bookClassSetupService.updateBookClassSetup(this.model).then(data => {
                this.bookId=[];
                this.classId=[];
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                if (datacode == 200) {
                    this.isModify=false;
                    this.model.bookId=null;
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.showBtns=false;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
           
            this.model.specialDepreciationAllowance = parseInt(this.model.specialDepreciationAllowance)
            
             this.bookClassSetupService.saveBookClassSetup(this.model).then(data => {
                 this.showBtns=false;
                 this.bookId=[];
                 this.classId=[];
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        this.btndisabled=false;
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
            
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
}