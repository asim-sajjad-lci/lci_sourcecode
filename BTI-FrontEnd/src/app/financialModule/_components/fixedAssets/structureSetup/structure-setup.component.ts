import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { StructureSetup } from '../../../../financialModule/_models/fixedAssets/structure-setup';
import { StructureSetupService } from '../../../../financialModule/_services/fixedAssets/structure-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<structure-setup></structure-setup>',
    templateUrl: './structure-setup.component.html',
    styles: ["user.component.css"],
    providers:[StructureSetupService]
})

//export to make it available for other classes
export class StructureSetupComponent {
    page = new Page();
    rows = new Array<StructureSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    cardTypeStatus =[];
    add;
    oldstructureId;
    myHtml;
    disableId;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private structureSetupService: StructureSetupService,
        private getScreenDetailService: GetScreenDetailService){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.disableId = false;
        this.add = true;
    }

    ngOnInit() {
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1125";
           // default form parameter for adding exchange structureSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_STR_SETUP_STRUCTURE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_STR_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_STR_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_STR_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_STR_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1126";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_STR_SETUP_STRUCTURE_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_STR_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_STR_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_STR_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_STR_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_STR_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
             
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

//for reseting data
    clearForm(){
        this.model.structureDescription= '';
        this.model.structureDescriptionArabic= '';
       
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.structureId='';
		 }
    }

    

    Cancel()
    {
    this.add=true;
    this.model.structureId= '';
    this.clearForm();
    this.disableId=false;
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.structureSetupService.searchStructureSetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }      
  
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.structureSetupService.searchStructureSetup(this.page, this.searchKeyword).subscribe(pagedData => {
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
 
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    
    // getStructureSetupById(row: any)
    getStructureSetupById(row: any)
    {
        this.showBtns=true;
        this.add = false;
        this.disableId = true;
        this.myHtml = "border-vanish";
        this.structureSetupService.getStructureSetupById(row.structureIndex).then(data => {
        this.model = data.result; 
        });
    }
    
    saveStrutureSetUp(f: NgForm)
    {
        this.myHtml = "";
        this.btndisabled=true;
        if(!this.add)
        {
            
             this.structureSetupService.updateStructureSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.add = true;
                        this.messageText = data.btiMessage.message;
                        this.disableId = false;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
             this.structureSetupService.createStructureSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.showBtns=false;
                this.disableId = false;
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                    else{
                     this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }

                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
}