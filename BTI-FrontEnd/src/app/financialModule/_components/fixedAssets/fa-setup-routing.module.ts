import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanySetupComponent } from '../FixedAssets/companySetup/company-set-up.component';
import { StructureSetupComponent } from '../FixedAssets/structureSetup/structure-setup.component';
import { BookSetupComponent } from '../FixedAssets/bookSetup/book-setup.component';
import { RetirementSetupComponent } from '../FixedAssets/retirementSetup/retirement-setup.component';
import { FaCalenderSetupComponent } from '../FixedAssets/fa-calender-setup/fa-calender-setup.component';
import { CalenderQuarterSetupComponent } from '../FixedAssets/calender-quarter-setup/calender-quarter-setup.component';
import { LocationComponent } from '../FixedAssets/locationSetup/location.component';
import { physicalLocationComponent } from '../FixedAssets/physicalLocationSetup/physical-location.component';
import { InsuranceClassSetupComponent } from '../FixedAssets/insuranceClassSetup/insuranceClassSetup.component';
import { BookClassSetupComponent} from '../FixedAssets/bookClassSetup/bookClassSetup.component';
import { ClassSetup } from '../FixedAssets/classSetup/class-setup.component';
import { LeaseCompanySetupComponent } from '../FixedAssets/leaseCompanySetup/lease-company-setup.component';
import { PurchasingPostingSetupComponent } from '../FixedAssets/purchasingPostingSetup/purchasing-posting-setup.component';
import { AccountGroupSetupComponent } from '../FixedAssets/accountGroupSetup/account-group-setup.component';

const routes: Routes = [
  { path: '', component: CompanySetupComponent },
  { path: 'facompanysetup', component:  CompanySetupComponent },
   { path: 'structuresetup', component:  StructureSetupComponent },
   { path:'booksetup', component: BookSetupComponent},
   { path: 'retirementsetup', component:  RetirementSetupComponent },
   { path:'facalendarsetup', component:FaCalenderSetupComponent},
   { path:'calendarquartersetup', component:CalenderQuarterSetupComponent},
   { path:'fixedassetscalendersetup', component:FaCalenderSetupComponent},
   {path:'locationsetup', component:LocationComponent},
   {path:'physicallocationsetup', component:physicalLocationComponent},
   {path:'insuranceclasssetup', component:InsuranceClassSetupComponent},
   {path:'bookclasssetup', component:BookClassSetupComponent},
   {path:'classsetup', component:ClassSetup},
   {path:'leasecompanysetup', component:LeaseCompanySetupComponent},
   {path:'purchasingpostingsetup', component:PurchasingPostingSetupComponent},
   {path:'accountgroupsetup', component:AccountGroupSetupComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountsReceivableRoutingModule { }
