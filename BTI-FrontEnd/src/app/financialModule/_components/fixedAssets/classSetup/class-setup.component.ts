import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { classSetupMaintenance } from '../../../../financialModule/_models/fixedAssets/classSetupMaintenance';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {ClassSetupService} from '../../../../financialModule/_services/fixedAssets/classSetup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'class-setup',
   templateUrl: './class-setup.component.html', 
    providers: [ClassSetupService,CommonService],
})

export class ClassSetup implements OnInit {
	customerId;
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	// Serachable Select Box Step1
	ArrInsuranceClass = [];
    faInsuranceClassIndexId : any = [];
    ddlInsuranceClassSetting = {};
	// Serachable Select Box Step1 End

	// Serachable Select Box Step1
	ArrAccountGroupId = [];
    faAccountGroupId : any = [];
    ddlAccountGroupIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<classSetupMaintenance>();
    temp = new Array<classSetupMaintenance>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
	tableViewtext=Constants.tableViewtext;
	search=Constants.search;
    deleteConfirmationText = Constants.deleteConfirmationText;
	btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
	EmptyMessage = Constants.EmptyMessage;
	btndisabled:boolean=false;
	isScreenLock;
    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	insuranceClass;
	accountGroup;
	myHtml;
	classSetupObject = {
    'classId': '',
    'description': '',
    'descriptionArabic': '',
    'faAccountGroupId': '',
    'faInsuranceClassIndexId': '',
  };
	
	constructor(private router: Router,private route: ActivatedRoute,private classSetupService: ClassSetupService,private getScreenDetailService: GetScreenDetailService, private commonService:CommonService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.disableId = false;
		this.insuranceClass=[];
		this.accountGroup=[];
		this.classSetupService.getinsuranceClass().subscribe(pagedData => {
            // this.insuranceClass = pagedData.records;
				 // Serachable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrInsuranceClass.push({ "id": pagedData.records[i].insuranceClassIndex, "itemName": pagedData.records[i].insuranceDescriptionPrimary })
              }
			    // Serachable Select Box Step3 End
        });

		
		this.classSetupService.getAccountGroup().then(pagedData => {
			
			this.accountGroup.push(pagedData.result.records);
			for(var i=0;i< pagedData.result.records.length;i++)
              {
                this.ArrAccountGroupId.push({ "id": pagedData.result.records[i].accountGroupIndex, "itemName": pagedData.result.records[i].accountGroupId })
              }
        });
		
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
				{ 'fieldName': 'MANAGE_CLASS_SETUP_CLASS_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_CLASS_SETUP_DESC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_CLASS_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_CLASS_SETUP_GROUP_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_CLASS_SETUP_INS_CLASS_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_CLASS_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_CLASS_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_CLASS_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
			];
		
			this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1198").then(data => {
				
				this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
					var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
					var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
				}
			});
		/* for grid binding ends */
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_CLASS_SETUP_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CLASS_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CLASS_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CLASS_SETUP_ACC_GROUP_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CLASS_SETUP_INS_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CLASS_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CLASS_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' }
				];

				this.getScreenDetailService.ValidateScreen("S-1197").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1197").then(data => {
				
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */	
		
	}

	 // Serachable Select Box Step4
		getInsuranceClassByID(item:any){
		//this.faInsuranceClassIndexId=item.id;
		this.faInsuranceClassIndexId=[];
		this.faInsuranceClassIndexId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("faInsuranceClassIndexId")
		}
		OnInsuranceClassDeSelect(item:any){
			this.faInsuranceClassIndexId=[];
			this.commonService.closeMultiselectWithId("faInsuranceClassIndexId")
		}
		// Serachable Select Box Step4 ends

		// Serachable Select Box Step4
		getAccountGroupByID(item:any){
		 //this.faAccountGroupId=item.id;
		 this.faAccountGroupId=[];
		 this.faAccountGroupId.push({'id':item.id,'itemName':item.itemName});
		 this.commonService.closeMultiselectWithId("faAccountGroupId");
		}
		OnAccountGroupIdDeSelect(item:any){
			this.faAccountGroupId=[];
			this.commonService.closeMultiselectWithId("faAccountGroupId");
		}
		// Serachable Select Box Step4 ends

	
	save(classSetup:NgForm){
		this.btndisabled=true;
			if(this.add){
				let submittedData = {
				 'classId': this.classSetupObject.classId,
				'description': this.classSetupObject.description,
				'descriptionArabic': this.classSetupObject.descriptionArabic,
				'faAccountGroupIndex': this.faAccountGroupId[0].id,
				'faInsuranceClassIndexId': this.faInsuranceClassIndexId[0].id,
				}
				
				this.classSetupService.save(submittedData).then(data => {
					this.showBtns=false;
					window.scrollTo(0,0);
					this.btndisabled=false;
				var datacode = data.code;
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					classSetup.resetForm();
				// 	this.classSetupObject = {
				// 	'classId': '',
				// 	'description': '',
				// 	'descriptionArabic': '',
				// 	'faAccountGroupId': '',
				// 	'faInsuranceClassIndexId': '',
				//   };
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					this.faInsuranceClassIndexId=[];
					this.faAccountGroupId=[];
				}, 100);
					this.setPage({ offset: 0 });
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  //this.setPage({ offset: 0 });
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}else{
			let submittedData = {
				 'classId': this.classSetupObject.classId,
				'description': this.classSetupObject.description,
				'descriptionArabic': this.classSetupObject.descriptionArabic,
				'faAccountGroupIndex': this.faAccountGroupId[0].id,
				'faInsuranceClassIndexId': this.faInsuranceClassIndexId[0].id,
				}
				
				this.classSetupService.edit(submittedData).then(data => {
					this.btndisabled=false;
					window.scrollTo(0,0);
				var datacode = data.code;
				if (datacode == 200) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					classSetup.resetForm();
				// 	this.classSetupObject = {
				// 	'classId': '',
				// 	'description': '',
				// 	'descriptionArabic': '',
				// 	'faAccountGroupId': '',
				// 	'faInsuranceClassIndexId': '',
				//   };
				  this.add= true;
				  this.disableId=false;
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					this.faInsuranceClassIndexId=[];
					this.faAccountGroupId=[];
					this.showBtns=false;
				}, 100);
					this.setPage({ offset: 0 });
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  //this.setPage({ offset: 0 });
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}
	}
	
	 updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.classSetupService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }


	edit(row : any){
		this.showBtns=true;
		this.faInsuranceClassIndexId=[];
		this.faAccountGroupId=[];
		this.add = false;
		this.disableId = true;
		this.classSetupService.getData(row.classId).then(data => {
			this.records = data.result;
			var	selectedClass = this.ArrInsuranceClass.find(x => x.id ==  data.result.faInsuranceClassIndexId);
			this.faInsuranceClassIndexId.push({'id':selectedClass.id,'itemName':selectedClass.itemName}); 

			var	selectedAccountGroup = this.ArrAccountGroupId.find(x => x.id ==  data.result.faAccountGroupId);
            if(selectedAccountGroup){
				
				this.faAccountGroupId.push({'id':selectedAccountGroup.id,'itemName':selectedAccountGroup.itemName});
			}
			
			this.classSetupObject = {
				'classId': this.records.classId,
				'description': this.records.description,
				'descriptionArabic': this.records.descriptionArabic,
				'faAccountGroupId': this.records.faAccountGroupId,
				'faInsuranceClassIndexId': this.records.faInsuranceClassIndexId,
			  };					
		});
	}
	//Is id already exist
	IsIdAlreadyExist(){
		if(this.classSetupObject.classId != '' && this.classSetupObject.classId != undefined && this.classSetupObject.classId != 'undefined')
		  {
			  this.classSetupService.getData(this.classSetupObject.classId).then(data => {
			   var datacode = data.code;
			   if (data.status == "NOT_FOUND") {
					  return true;
				  }
				  else{
					  var txtId = <HTMLInputElement> document.getElementById("ddl_insClassId");
					  txtId.focus();
					  this.isSuccessMsg = false;
					  this.isfailureMsg = true;
					  this.showMsg = true;
					  this.hasMsg = true;
					  this.messageText = data.btiMessage.message;
					  this.setPage({ offset: 0 });
					  window.setTimeout(() => {
					  this.showMsg = false;
					  this.hasMsg = false;
						}, 4000);
						window.scrollTo(0,0);
						this.classSetupObject.classId='';
						return false;
				  }
	  
				  }).catch(error => {
				  window.setTimeout(() => {
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
			  }, 100)
		  });
	  }
	  }
	ClearForm(classSetup:NgForm){
	      classSetup.resetForm();
		  this.faInsuranceClassIndexId=[];
		  var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		  selectedValue.value=this.records.classId;
		  this.classSetupObject.classId = this.records.classId;
				
			 if(!this.add){
				this.myHtml = "border-vanish";
			 }else{
				this.myHtml = "";
			 }
	}

    Cancel(classSetup)
    {
    this.add=true;
	this.ClearForm(classSetup);
    this.classSetupObject.classId= '';
	this.disableId=false;
    }

	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.classSetupService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	ngOnInit() {

		 // Serachable Select Box Step2
		   this.ddlInsuranceClassSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2

		// Serachable Select Box Step2
		   this.ddlAccountGroupIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
	}

	
    	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
}