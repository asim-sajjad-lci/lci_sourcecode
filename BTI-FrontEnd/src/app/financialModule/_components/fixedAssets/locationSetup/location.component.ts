import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {locationSetup} from '../../../../financialModule/_models/fixedAssets/locationSetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import {LocationService} from '../../../../financialModule/_services/fixedAssets/location.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'location',
   templateUrl: './location.component.html', 
    providers: [LocationService],
})

export class LocationComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	formCountries;
	formStates;
	formCities;
	locationId;
	countries;
	states;
	cities;
	editCountries;
	editStates;
	editCities;
	myHtml;
	btnCancelText=Constants.btnCancelText;
	showBtns:boolean=false;
	page = new Page();
	rows = new Array<locationSetup>();
    temp = new Array<locationSetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	tableViewtext=Constants.tableViewtext;
	EmptyMessage = Constants.EmptyMessage;
	btndisabled:boolean=false;
	isScreenLock;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	
	constructor(private router: Router,private route: ActivatedRoute,private locationService: LocationService,private getScreenDetailService: GetScreenDetailService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.disableId = false;
		this.formCountries='';
		this.formStates='';
		this.formCities='';
		this.locationService.getCountryList()
			.then(data => {
				this.countries = data.result;
				this.countries.splice(0,1);
		});
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_LOCATION_SETUP_LOCATION_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_LOCATION_SETUP_CITY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_LOCATION_SETUP_STATE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_LOCATION_SETUP_COUNTRY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_LOCATION_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_LOCATION_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_LOCATION_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
		];
		this.getScreenDetailService.ValidateScreen("S-1142").then(res=>
			{
				this.isScreenLock = res;
			});
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1142").then(data => {
			this.moduleName = data.result.moduleName;
			this.screenName = data.result.dtoScreenDetail.screenName;
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_LOCATION_SETUP_LOCATION_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LOCATION_SETUP_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LOCATION_SETUP_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LOCATION_SETUP_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LOCATION_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LOCATION_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				
				];
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1141").then(data => {
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
	}	

	 updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.locationService.searchLocation(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
	
	selected(event){
		let countryID = this.formCountries;
		this.locationService.getStateList(countryID).then(data => {
			this.states=[];
			this.cities=[];
			this.formStates='';
			this.formCities='';
			this.states = data.result;
        });
	}
	
	selectedState(event){
		let stateID = this.formStates;
		this.locationService.getCityList(stateID).then(data => {
			this.cities=[];
			this.formCities='';
			this.cities = data.result;
        });
	}
		//Is id already exist
		IsIdAlreadyExist(){
			if(this.locationId != '' && this.locationId != undefined && this.locationId != 'undefined')
			  {
				this.locationService.getLocationById(this.locationId).then(data => {
				   var datacode = data.code;
				   if (data.status == "NOT_FOUND") {
						  return true;
					  }
					  else{
						  var txtId = <HTMLInputElement> document.getElementById("ddl_insClassId");
						  txtId.focus();
						  this.isSuccessMsg = false;
						  this.isfailureMsg = true;
						  this.showMsg = true;
						  this.hasMsg = true;
						  this.messageText = data.btiMessage.message;
						  this.setPage({ offset: 0 });
						  window.setTimeout(() => {
						  this.showMsg = false;
						  this.hasMsg = false;
							}, 4000);
							window.scrollTo(0,0);
							this.locationId='';
							return false;
					  }
		  
					  }).catch(error => {
					  window.setTimeout(() => {
					  this.isSuccessMsg = false;
					  this.isfailureMsg = true;
					  this.showMsg = true;
					  this.hasMsg = true;
					  this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
				  }, 100)
			  });
		  }
		  }
	 save(location: NgForm){
		this.btndisabled=true;
		if(this.add){
			for(let i=0; i<this.countries.length; i++){
				if(this.formCountries == this.countries[i].countryId){
					var countryName = this.countries[i]["countryName"];
				}
			}
						  
		for(let i=0; i<this.states.length; i++){
			if(this.formStates == this.states[i].stateId){
				var stateName = this.states[i]["stateName"];
			}
		}
			
		for(let i=0; i<this.cities.length; i++){
			if(this.formCities == this.cities[i].cityId){
				var cityName = this.cities[i]["cityName"];
			}
		}
		let submittedData = 
		{
			"locationId":this.locationId,
			"stateId":this.formStates,
			"cityId":this.formCities,
			"countryId":this.formCountries,
			"state": stateName,
			"city":cityName,
			"country":countryName
		}
		this.locationService.createLocation(submittedData).then(data => {
			this.showBtns=false;
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
			    location.resetForm();
				this.setPage({ offset: 0 });
				this.formCountries='';
				this.formCities ='';
				this.formStates='';
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			  this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		
		}
		else{
			for(let i=0; i<this.countries.length; i++){
				if(this.formCountries == this.countries[i].countryId){
					var countryName = this.countries[i]["countryName"];
				}
			}
						  
		for(let i=0; i<this.states.length; i++){
			if(this.formStates == this.states[i].stateId){
				var stateName = this.states[i]["stateName"];
			}
		}
			
		for(let i=0; i<this.cities.length; i++){
			if(this.formCities == this.cities[i].cityId){
				var cityName = this.cities[i]["cityName"];
			}
		}
		let submittedData = 
		{
			"locationId":this.locationId,
			"stateId":this.formStates,
			"cityId":this.formCities,
			"countryId":this.formCountries,
			"state": stateName,
			"city":cityName,
			"country":countryName
		}
		this.locationService.editLocation(submittedData).then(data => {
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				this.showBtns=false;
			    location.resetForm();
				this.setPage({ offset: 0 });
				this.formCountries='';
				this.formCities ='';
				this.formStates='';
				this.disableId = false;
				this.add = true;
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			  this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		}
	 }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.locationService.searchLocation(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	edit(row: any){
		this.add = false;
		this.disableId = true;
		this.showBtns=true;
		this.locationService.getLocationById(row.locationId).then(data => {
			this.records = data.result;
			this.locationId = this.records.locationId;
			let countryID = this.records.countryId;
			this.locationService.getStateList(countryID).then(data => {
				this.states = data.result;
			});
			
			let stateID = this.records.stateId;
			this.locationService.getCityList(stateID).then(data => {
				this.cities = data.result;
			});
			
			this.locationService.getCountryList()
			.then(data => {
				this.editCountries = data.result;
				this.editCountries.splice(0,1);
				for(let i=0; i<this.editCountries.length; i++){
					if(this.records.countryId == this.editCountries[i].countryId){
						//this.formCountries =this.editCountries[i];
						this.formCountries = this.editCountries[i]["countryId"];
					}
			}
			});
			  
			this.locationService.getStateList(this.records.countryId).then(data => {
				this.editStates = data.result;
				//this.formStates = {"stateId": '',"stateName": ""}
				for(let i=0; i<this.editStates.length; i++){
					if(this.records.stateId == this.editStates[i].stateId){
						this.formStates = this.editStates[i]["stateId"];
					}
				}
			});
			this.locationService.getCityList(this.records.stateId).then(data => {
				this.editCities = data.result;
				//this.formCities = {"cityId": '',"cityName": ""}
				for(let i=0; i<this.editCities.length; i++){
					if(this.records.cityId == this.editCities[i].cityId){
						this.formCities = this.editCities[i]["cityId"];
					}
				}
			});
		});
	}
	
	ClearForm(location:NgForm){
	      location.resetForm();
		  var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		  selectedValue.value=this.records.locationId;
		 this.locationId = this.records.locationId;
				
			 if(!this.add){
				this.myHtml = "border-vanish";
			 }else{
				this.myHtml = "";
			 }
	}
	
	Cancel(location)
    {
    this.add=true;
    this.ClearForm(location);
    this.locationId= '';
	this.disableId=false;
    }

	ngOnInit() {
	}		
	
	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
}