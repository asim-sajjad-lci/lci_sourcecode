import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {fixedAssetCompanySetup} from '../../../../financialModule/_models/fixedAssets/fixedAssetCompanySetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {CompanySetupService} from '../../../../financialModule/_services/fixedAssets/companySetup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'fixes-asset-company-set-up',
   templateUrl: './company-set-up.component.html', 
    providers: [CompanySetupService,CommonService],
})

export class CompanySetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues:any= [];
	gridDefaultFormValues:any= [];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	isScreenLock;
	editData;
	condition;
	myHtml;
	// Serachable Select Box Step1
	ArrCompanyBookId = [];
    companyBook : any = [];
    ddlCompanyBookIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<fixedAssetCompanySetup>();
    temp = new Array<fixedAssetCompanySetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
	totalText=Constants.totalText;
	search=Constants.search;
    deleteConfirmationText = Constants.deleteConfirmationText;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	books;
	CompanyBook;
	fixedAssetCompanySetup = {
    'description': '',
    'arabicDescription': '',
    'requireAssetAccount': false,
    'addBookInfo': false,
    'assetLabelFormAssetID': false,
    'postInDetails': false,
    'postPayableManagementThroughFa': false,
    'postPurchaseOrderProFa': false,
  };
	
	constructor(private router: Router,private route: ActivatedRoute,private companySetupService: CompanySetupService,private getScreenDetailService: GetScreenDetailService, private commonService:CommonService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
        this.CompanyBook = [{'id':'','itemName':''}];
		this.disableId = false;
		this.condition = false;

		
	//	this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_FA_COMPANY_SETUP_COMPANY_BOOK', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_COMPANY_SETUP_DESC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_COMPANY_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_COMPANY_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_COMPANY_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_COMPANY_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
		];
		
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1110").then(data => {
			
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_COMPANY_BOOK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_OPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_ASSET_ACC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_AUTO_BOOK_INFO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_DEF_ASSET_LABEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_POST_DETAIL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_PURCHG_OPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_POST_PAYABLE_MGT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_POST_PURCHAGE_ORDER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_COMPANY_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				];

				this.getScreenDetailService.ValidateScreen("S-1109").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1109").then(data => {
					
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
		
	}

	ngOnInit() {
		this.getBookDetail();
		// Serachable Select Box Step2
		this.ddlCompanyBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
	}	


	getBookDetail()
	{
		this.companySetupService.searchBooks().then(data => {
			this.CompanyBook=[];
			this.CompanyBook.push({ "id": '', "itemName": this.select})
			if(data.btiMessage.messageShort !='RECORD_NOT_FOUND')
			{
				if(data.result.records.length>0){
                	for(var i=0;i<data.result.records.length;i++)
					{
				 	 this.ArrCompanyBookId.push({ "id": data.result.records[i].bookIndexId, "itemName": data.result.records[i].bookId})
					}
				}
			}
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
	}
	getCompanyData()
	{
			this.CompanyBook=[];
			this.companySetupService.getList().then(data => {
            this.editData = data.result;
			if(this.editData){
				this.condition = true;
				this.add = false;
				  if(this.editData.requireAssetAccount=="1"){
						var req = true;
					}else{
						var req = false;
					}
					
					if(this.editData.autoAddBookInformation=="1"){
						var bookInfo = true;
					}else{
						var bookInfo = false;
					}
					
					if(this.editData.defaultAssetLabel=="1"){
						var assetLabel = true;
					}else{
						var assetLabel = false;
					}
					
					if(this.editData.postDetails=="1"){
						var postDetail = true;
					}else{
						var postDetail = false;
					}
					if(this.editData.postPayableManagement=="1"){
						var postPayMan = true;
					}else{
						var postPayMan = false;
					}
					if(this.editData.postPurchaseOrderProcessing=="1"){
						var postPurOrdPro = true;
					}else{
						var postPurOrdPro = false;
					}
					this.fixedAssetCompanySetup = {
						'description': this.editData.bookDescription,
						'arabicDescription': this.editData.bookDescriptionArabic,
						'requireAssetAccount': req,
						'addBookInfo': bookInfo,
						'assetLabelFormAssetID': assetLabel,
						'postInDetails': postDetail,
						'postPayableManagementThroughFa': postPayMan,
						'postPurchaseOrderProFa': postPurOrdPro,
					  };

					  var selectedCompanyBook = this.ArrCompanyBookId.find(x => x.id ==   data.result.bookIndexId);
				      this.CompanyBook.push({ "id": selectedCompanyBook.id, "itemName": selectedCompanyBook.itemName})
					
			}
        });
	}

	 // Serachable Select Box Step4
	getCompanyBookByID(item:any){
		this.CompanyBook=[];
		this.CompanyBook.push({ "id": item.id, "itemName": item.itemName})
		if(item.id)
		{
		  this.companySetupService.getFixedAssetCompanySetupByBookId(item.id).then(data => {
			  
			     if(data.btiMessage.messageShort=='RECORD_NOT_FOUND')
				 {
					this.companySetupService.getBookSetupByBookIndexId(item.id).then(data => {
					  this.fixedAssetCompanySetup = {
						'description': data.result.bookDescription,
						'arabicDescription': data.result.bookDescriptionArabic,
						'requireAssetAccount': false,
						'addBookInfo': false,
						'assetLabelFormAssetID': false,
						'postInDetails': false,
						'postPayableManagementThroughFa': false,
						'postPurchaseOrderProFa': false,
					  };
      	  			});
				 }
				 else{
  					// this.fixedAssetCompanySetup.requireAssetAccount=data.result.requireAssetAccount;
					// this.fixedAssetCompanySetup.addBookInfo=data.result.autoAddBookInformation;
					// this.fixedAssetCompanySetup.assetLabelFormAssetID=data.result.defaultAssetLabel;
					// this.fixedAssetCompanySetup.postInDetails=data.result.postDetails;
					// this.fixedAssetCompanySetup.postPayableManagementThroughFa=data.result.postPayableManagement;
					// this.fixedAssetCompanySetup.postPurchaseOrderProFa=data.result.postPurchaseOrderProcessing;
					// this.fixedAssetCompanySetup.description=data.result.bookDescription;
					// this.fixedAssetCompanySetup.arabicDescription=data.result.bookDescriptionArabic;
					if(data.result.requireAssetAccount=="1"){
						var req = true;
					}else{
						var req = false;
					}
					
					if(data.result.autoAddBookInformation=="1"){
						var bookInfo = true;
					}else{
						var bookInfo = false;
					}
					
					if(data.result.defaultAssetLabel=="1"){
						var assetLabel = true;
					}else{
						var assetLabel = false;
					}
					
					if(data.result.postDetails=="1"){
						var postDetail = true;
					}else{
						var postDetail = false;
					}
					if(data.result.postPayableManagement=="1"){
						var postPayMan = true;
					}else{
						var postPayMan = false;
					}
					if(data.result.postPurchaseOrderProcessing=="1"){
						var postPurOrdPro = true;
					}else{
						var postPurOrdPro = false;
					}
					this.fixedAssetCompanySetup = {
						'description': data.result.bookDescription,
						'arabicDescription': data.result.bookDescriptionArabic,
						'requireAssetAccount': req,
						'addBookInfo': bookInfo,
						'assetLabelFormAssetID': assetLabel,
						'postInDetails': postDetail,
						'postPayableManagementThroughFa': postPayMan,
						'postPurchaseOrderProFa': postPurOrdPro,
					  };
				 }
      	  });
	   }
	   
	   this.commonService.closeMultiselectWithId("companyBook")
	}
	OnCompanyBookIdDeSelect(item:any){
		this.CompanyBook=[];
		this.fixedAssetCompanySetup.description='';
		this.fixedAssetCompanySetup.arabicDescription='';
		this.commonService.closeMultiselectWithId("companyBook")
	}
	// Serachable Select Box Step4 ends
	
	save(assetCompanySetup:NgForm){
			if(this.fixedAssetCompanySetup.postInDetails){
				var postInDetails = "1";
			}else{
				var postInDetails = "0";
			}
			
			if(this.fixedAssetCompanySetup.postPayableManagementThroughFa){
				var postPayableManagement = "1";
			}else{
				var postPayableManagement = "0";
			}
			
			if(this.fixedAssetCompanySetup.postPurchaseOrderProFa){
				var postPurchaseOrder = "1";
			}else{
				var postPurchaseOrder = "0";
			}
			
			if(this.fixedAssetCompanySetup.requireAssetAccount){
				var requiresssetAccount = "1";
			}else{
				var requiresssetAccount = "0";
			}
			
			if(this.fixedAssetCompanySetup.addBookInfo){
				var autoAddBookInformation = "1";
			}else{
				var autoAddBookInformation = "0";
			}
			
			if(this.fixedAssetCompanySetup.assetLabelFormAssetID){
				var defaultAssetLabel = "1";
			}else{
				var defaultAssetLabel = "0";
			}
			let submittedData = {						
				"bookIndexId":this.CompanyBook[0].id,
				"bookDescription":this.fixedAssetCompanySetup.description,
				"bookDescriptionArabic":this.fixedAssetCompanySetup.arabicDescription,
				"postDetails":postInDetails,
				"postPayableManagement":postPayableManagement,
				"postPurchaseOrderProcessing":postPurchaseOrder,
				"autoAddBookInformation":autoAddBookInformation,
				"defaultAssetLabel":defaultAssetLabel,
				"requireAssetAccount":requiresssetAccount
			}
			
			this.companySetupService.createFACompanySetup(submittedData).then(data => {
				// this.companyBook=[];
				var datacode = data.code;
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
				}, 100);
			 }else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
			}
		   });
	}
	
	
	
	
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	clearForm(assetCompanySetup:NgForm){
		this.CompanyBook=[];
		this.fixedAssetCompanySetup = {
			'description': '',
			'arabicDescription': '',
			'requireAssetAccount': false,
			'addBookInfo': false,
			'assetLabelFormAssetID': false,
			'postInDetails': false,
			'postPayableManagementThroughFa': false,
			'postPurchaseOrderProFa': false,
		};
		 
		if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
		 }

		 //this.getCompanyData();
	}
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.companySetupService.searchFACompanySetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
		
        });
    }
	
	
    	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
	
}