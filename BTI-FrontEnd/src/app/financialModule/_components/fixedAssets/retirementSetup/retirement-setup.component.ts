import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { RetirementSetup } from '../../../../financialModule/_models/fixedAssets/retirement/retirement-setup';
import { RetirementSetupService } from '../../../../financialModule/_services/fixedAssets/retirement/retirement.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<retirement-setup></retirement-setup>',
    templateUrl: './retirement-setup.component.html',
    styles: ["user.component.css"],
    providers:[RetirementSetupService]
})

//export to make it available for other classes
export class RetirementSetupComponent {
    page = new Page();
    rows = new Array<RetirementSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    cardTypeStatus =[];
    myHtml;
    add;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    isScreenLock;
  
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private retirementSetupService: RetirementSetupService,
        private getScreenDetailService: GetScreenDetailService){
     
         this.page.pageNumber = 0;
        this.page.size = 5;
    }

    ngOnInit() {
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1113";
           // default form parameter for adding exchange structureSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_RET_SETUP_RETIREMENT_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_RET_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_RET_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_RET_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_RET_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1114";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_RET_SETUP_RETIREMENT_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_RET_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_RET_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_RET_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_RET_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_RET_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    resetMe(f)
    {
        this.model.descriptionPrimary= '';
        this.model.descriptionSecondary= '';
      
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.faRetirementId='';
		 }
    }

    
    Cancel(f)
    {
    this.isModify=false;
    this.model.faRetirementId= '';
    this.resetMe(f);
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.retirementSetupService.searchRetirementSetup(this.page, this.searchKeyword).subscribe(pagedData => {
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
 
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

     updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.retirementSetupService.searchRetirementSetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }      
    // getRetirementSetupById(row: any)
    getRetirementSetupById(row: any)
    {
        this.showBtns=true;
        this.isModify=true;
        this.retirementSetupService.getRetirementSetupById(row.faRetirementId).then(data => {
            this.model = data.result; 
         });
    }
    //Is id already exist
		IsIdAlreadyExist(){
			if(this.model.faRetirementId != '' && this.model.faRetirementId != undefined && this.model.faRetirementId != 'undefined')
			  {
				this.retirementSetupService.getRetirementSetupById(this.model.faRetirementId).then(data => {
				   var datacode = data.code;
				   if (data.status == "NOT_FOUND" || data.btiMessage.messageShort =='RECORD_NOT_FOUND') {
						  return true;
					  }
					  else{
						  var txtId = <HTMLInputElement> document.getElementById("faRetirementIdText");
						  txtId.focus();
						  this.isSuccessMsg = false;
						  this.isfailureMsg = true;
						  this.showMsg = true;
						  this.hasMsg = true;
						  this.messageText = data.btiMessage.message;
						  this.setPage({ offset: 0 });
						  window.setTimeout(() => {
						  this.showMsg = false;
						  this.hasMsg = false;
							}, 4000);
							window.scrollTo(0,0);
							this.model.faRetirementId='';
							return false;
					  }
		  
					  }).catch(error => {
					  window.setTimeout(() => {
					  this.isSuccessMsg = false;
					  this.isfailureMsg = true;
					  this.showMsg = true;
					  this.hasMsg = true;
					  this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
				  }, 100)
			  });
		  }
		  }
    saveRetirementSetUp(f: NgForm)
    {
        this.btndisabled=true;
        if(this.isModify)
        {
             this.retirementSetupService.updateRetirementSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
             this.retirementSetupService.createRetirementSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.showBtns=false;
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }

     /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
           LockScreen(writeAccess)
           {
               if(!writeAccess)
               {
                   return true
               }
               else if(this.btndisabled)
               {
                   return true
               }
               else if(this.isScreenLock)
               {
                   return true;
               }
               else{
                   return false;
               }
           }
}