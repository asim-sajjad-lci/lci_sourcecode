import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {AccountRecievables} from '../../../../financialModule/_models/accounts-receivables-setup/accounts-receivables-class-setup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {LeaseCompanySetupService} from '../../../../financialModule/_services/fixedAssets/lease-company-setup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
import { leaseCompanySetup } from '../../../../financialModule/_models/fixedAssets/leaseCompanySetup';

@Component({
   selector: 'lease-company-setup',
   templateUrl: './lease-company-setup.component.html', 
    providers: [LeaseCompanySetupService,CommonService],
})

export class LeaseCompanySetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	venderNamePrimary:string;
	isScreenLock
    // Serachable Select Box Step1
	ArrVendorId = [];
    vendorId : any = [];
    ddlVendorIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<leaseCompanySetup>();
    temp = new Array<leaseCompanySetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	btnCancelText=Constants.btnCancelText;
	search=Constants.search;
    showBtns:boolean=false;
	EmptyMessage = Constants.EmptyMessage;
	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	vendors;
	editData;
	condition;
	myHtml;
	
	leaseCom=
	{
		'companyIndex':'',
		'companyId' : '',
		'vendorId' : '',
		'vendorName':'',
		'companyName':''
	}
	
	constructor(private router: Router,private route: ActivatedRoute,private leaseCompanySetupService: LeaseCompanySetupService,private getScreenDetailService: GetScreenDetailService,private commonService:CommonService) {
		 this.disableId = false;
        this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.vendors=[];
		this.condition = false;
		
		// fetch vendor records
		this.leaseCompanySetupService.searchVendors().then(data => {
            //this.vendors = data.result.records;
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrVendorId.push({ "id": data.result.records[i].venderId, "itemName": data.result.records[i].venderId})
              }
        });
		
		/* for edit data */
		this.leaseCompanySetupService.getList(this.page).subscribe(pagedData => {
            this.editData = pagedData.data;	
			if(this.editData){
				this.condition=true;	
				this.leaseCom=
					{
						'companyIndex':this.editData.companyIndex,
						'companyId' : this.editData.companyId,
						'vendorId' : this.editData.vendorId,
						'vendorName': this.editData.vendorName,
						'companyName':this.editData.companyName
					};
				}			
        });
	
		/* for edit data ends here */
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
				{ 'fieldName': 'MANAGE_LEASE_COMPANY_COMPANY_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_LEASE_COMPANY_COMPANY_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_LEASE_COMPANY_VENDOR_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_LEASE_COMPANY_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_LEASE_COMPANY_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_LEASE_COMPANY_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				
			];
		
			this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1222").then(data => {
				
				this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
					var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
					var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
				}
			});
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_LEASE_COMPANY_COMPANY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LEASE_COMPANY_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LEASE_COMPANY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_LEASE_COMPANY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },	
				{ 'fieldName': 'ADD_LEASE_COMPANY_COMPANY_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
				];

		this.getScreenDetailService.ValidateScreen("S-1175").then(res=>
			{
				this.isScreenLock = res;
			});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1175").then(data => {
	
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
	}
		// Serachable Select Box Step4
   getVendorIdByID(item:any){
     this.leaseCom.vendorId=item.id;
	  this.vendorId.push({'id':item.id});
	    this.leaseCompanySetupService.getVendorNameByID(item.id).then(data => {
			 this.venderNamePrimary = data.result.venderNamePrimary;
		});
	this.commonService.closeMultiselectWithId("vendorId");
 }
 OnVendorIdDeSelect(item:any){
	 this.venderNamePrimary='';
	 this.commonService.closeMultiselectWithId("vendorId");
 }

//  updateFilter(event) {
//         this.searchKeyword = event.target.value.toLowerCase();
//         this.page.pageNumber = 0;
//         this.page.size = this.ddPageSize;
//         this.leaseCompanySetupService.search(this.page, this.searchKeyword).subscribe(pagedData => {
//             this.page = pagedData.page;
//             this.rows = pagedData.data;
//             this.table.offset = 0;
//        }, error => {
//             this.hasMsg = true;
//         window.setTimeout(() => {
//             this.isSuccessMsg = false;
//             this.isfailureMsg = true;
//             this.showMsg = true;
//             this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
//         }, 100)
//         });
//     }

    	edit(row : any){
		this.showBtns=true;
		this.vendorId=[];
	    this.add = false;
         this.disableId = true;
		this.leaseCompanySetupService.getEdit(row.companyIndex).then(data => {
			this.records = data.result;
			
			var	selectedVendor = this.ArrVendorId.find(x => x.id ==  data.result.vendorId);
			this.vendorId.push({'id':selectedVendor.id,'itemName':selectedVendor.itemName}); 
            this.venderNamePrimary = data.result.venderNamePrimary;
			this.leaseCompanySetupService.getVendorNameByID(data.result.vendorId).then(data => {
				 this.venderNamePrimary = data.result.venderNamePrimary;
			});
			this.leaseCom = {
				'companyIndex': this.records.companyIndex,
				'companyId': this.records.companyId,
				'vendorId': this.records.vendorId,
				'vendorName': this.records.vendorName,
				'companyName': this.records.companyName,
			  };					
		});
	}

	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.leaseCompanySetupService.getList(this.page).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	
	save(leaseCompanySetup: NgForm){
		this.btndisabled=true;
		if(this.add){
		let submittedData = {
			'companyId' : this.leaseCom.companyId,
			'vendorId' : this.leaseCom.vendorId,
			'companyName' : this.leaseCom.companyName,
			'vendorName':this.leaseCom.vendorName
		}
		this.leaseCompanySetupService.create(submittedData).then(data => {
			this.showBtns=false;
			this.disableId = false;
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				leaseCompanySetup.resetForm();
				this.vendorId=[];
				//this.condition=true;
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				//  	this.messageText = data.btiMessage.message;
				// }, 100);
			   this.setPage({ offset: 0 });
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
			  this.setPage({ offset: 0 });
		}
	   });
	}else{
		let submittedData = {
			'companyIndex':this.leaseCom.companyIndex,
			'companyId' : this.leaseCom.companyId,
			'vendorId' : this.leaseCom.vendorId,
			'companyName' : this.leaseCom.companyName,
			'vendorName':this.leaseCom.vendorName
		}
		this.leaseCompanySetupService.update(submittedData).then(data => {
			window.scrollTo(0,0);
				var datacode = data.code;
				this.btndisabled=false;
				if (datacode == 200) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					leaseCompanySetup.resetForm();
					this.vendorId=[];
					this.leaseCom=
					{
						'companyIndex':'',
						'companyId' : '',
						'vendorId' : '',
						'vendorName':'',
						'companyName':''
					}
				  this.add= true;
				  this.disableId=false;
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					this.disableId = false;
					this.showBtns=false;
				}, 100);
					this.setPage({ offset: 0 });
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}
	}

	clearForm(leaseCompanySetup:NgForm){
		this.leaseCom.companyName='';
		this.venderNamePrimary='';
		  this.vendorId=[];
		//   var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		//   selectedValue.value=this.editData.companyId;
		//   this.leaseCom.companyId = this.editData.companyId;
				
		// 	 if(this.condition){
		// 		this.myHtml = "border-vanish";
		// 	 }else{
		// 		this.myHtml = "";
		// 	 }
	}

	Cancel(leaseCompanySetup)
    {
    this.add=true;
    this.clearForm(leaseCompanySetup);
	this.leaseCom.companyId= '';
	this.disableId=false;
    }
	
	ngOnInit() {

		  this.venderNamePrimary="";
          // Serachable Select Box Step2
		   this.ddlVendorIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
         }; 
		
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
	}	
	}	
	
	

