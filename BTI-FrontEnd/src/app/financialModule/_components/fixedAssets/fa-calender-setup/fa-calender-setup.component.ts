import { Component,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FixedAssetsCalenderSetup } from '../../../../financialModule/_models/fixedAssets/fixed-assets-calender-setup';
import { FaCalenderSetupService } from '../../../../financialModule/_services/fixedAssets/faCalenderSetup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 

@Component({
    selector: '<fixed-assets-calender-setup></fixed-assets-calender-setup>',
    templateUrl: './fa-calender-setup.component.html',
    styles: ["user.component.css"],
    providers:[FaCalenderSetupService]
})

//export to make it available for other classes
export class FaCalenderSetupComponent {
    page = new Page();
    rows = new Array<FixedAssetsCalenderSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    InvalidDay=Constants.InvalidDay;
    InvalidMonth=Constants.InvalidMonth;
    InvalidYear=Constants.InvalidYear;
    mustBe=Constants.mustBe;
    InvalidDate=Constants.InvalidDate;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean=false;
    showCalander:boolean=false;
    cardTypeStatus =[];
    private hideElement: boolean = false;
    calenderIdShow;
    calenderIdDisplay;
    oldcalendarId;
    editData;
    btndisabled:boolean=false;
    isScreenLock;
  
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private faCalenderSetupService: FaCalenderSetupService,
        private toastr: ToastsManager,
        vcr: ViewContainerRef,
        private getScreenDetailService: GetScreenDetailService){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.getCalendarMonthlySetup();
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
                this.getAddScreenDetail();
               
                //this.getMonthDetail(2020);
                this.getYears();
               
                
    }
    
    getYears()
    {
        var min = new Date().getFullYear() - 5,
        max = min + 20,
        select = document.getElementById('ddlYear');

        for (var i = min; i<=max; i++){
           var opt = <HTMLOptionElement>document.createElement('option');
           opt.value = i.toString();
           opt.innerHTML = i.toString();
           select.appendChild(opt);
        }
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1101";
           // default form parameter for adding exchange structureSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_FA_CAL_SETUP_CAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_FA_CAL_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_FA_CAL_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_FA_CAL_SETUP_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_FA_CAL_SETUP_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_FA_CAL_SETUP_START_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_FA_CAL_SETUP_END_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_FA_CAL_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_FA_CAL_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            
            ];
            this.getScreenDetailService.ValidateScreen("S-1101").then(res=>
                {
                    this.isScreenLock = res;
                });
            this.getScreenDetail(this.screenCode,'Add');
    }

    getMonthDetail(event)
    {
       this.model.monthlyCalendar = [];
       var CurrentYear =event.target.value;
       this.model.year1=CurrentYear;
       var date = new Date();
       for(var i=0;i< 12;i++)  
       {
          var firstDay = new Date(CurrentYear, i, 1);
          var lastDay = new Date(CurrentYear, i + 1, 0);

          var firstDayWithSlashes = (firstDay.getDate()) + '/' + (firstDay.getMonth() + 1) + '/' + firstDay.getFullYear();
          var lastDayWithSlashes = (lastDay.getDate()) + '/' + (lastDay.getMonth() + 1) + '/' + lastDay.getFullYear();
        
          this.model.monthlyCalendar.push({"periodIndex":i + 1 ,"startDate":firstDayWithSlashes,"endDate":lastDayWithSlashes});
       }
       this.showCalander=true;
    
    }

  

    getScreenDetail(screenCode,ArrayType)
    {

        this.getScreenDetailService.getScreenDetail(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

 getCalendarMonthlySetup()
   {
       this.faCalenderSetupService.getCalendarMonthlySetup().then(data => {
		this.editData = data.result;
         if(this.editData)
         {
             this.model = data.result;
             if(data.result.calendarId!=null){
            this.calenderIdShow=true;
            this.showCalander=true;
            this.calenderIdDisplay=data.result.calendarId;
         
            
            }
         }
      });
   }
 
 resetForm(f){
    this.model.calendarDescription='';
    this.model.calendarDescriptionArabic='';
    this.model.year1='';
    this.model.monthlyCalendar='';
    
 
   }
    
    // getStructureSetupById(row: any)
    getCalendarMonthlySetupById(row: any)
    {
        this.isModify=true;
        this.showCalander=true;
         this.faCalenderSetupService.getCalendarMonthlySetupById(row.calendarIndex).then(data => {
            this.model = data.result; 
         });
    }
    saveCalenderMonthlySetup(f: NgForm)
    {
        this.btndisabled=true;
        if(this.isModify)
        {
             this.faCalenderSetupService.createCalendarMonthlySetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
             this.faCalenderSetupService.createCalendarMonthlySetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                       this.getCalendarMonthlySetup();
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                    else{
                     this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                      
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }

             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }


    }

    validateDate(Id)
    {
        var txtEndDate=<HTMLInputElement>document.getElementById('txtEndDate'+Id);
        var quarterDate=txtEndDate.value;

         // regular expression to match required date format
        var  datePattern = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

        if(quarterDate != '') {
          if(quarterDate.match(datePattern)) {

            var arrQuarterDate=quarterDate.split('/')
            // day value between 1 and 31
            var daysInMonth = this.getDaysInMonth(parseInt(arrQuarterDate[1]),parseInt(arrQuarterDate[2]));
            if(parseInt(arrQuarterDate[0]) < 1 || parseInt(arrQuarterDate[0]) > daysInMonth) {
               this.toastr.warning(this.InvalidDay + arrQuarterDate[0]);	
               txtEndDate.value = '';
               return false;
            }

            // month value between 1 and 12
            if(parseInt(arrQuarterDate[1]) < 1 || parseInt(arrQuarterDate[1]) > 12) {
              this.toastr.warning(this.InvalidMonth + arrQuarterDate[1]);	
              txtEndDate.value = '';
            //   form.startdate.focus();
              return false;
            }
            // year value between 1902 and 2017
            if(parseInt(arrQuarterDate[2]) != this.model.year1) {
              this.toastr.warning(this.InvalidYear+ arrQuarterDate[2] + this.mustBe + this.model.year1);	
              txtEndDate.value = '';
              return false;
            }

            var tempQuarterDate = arrQuarterDate[1] + '/' + arrQuarterDate[0] + '/' + arrQuarterDate[2];
            var date = new Date(this.model.year1,parseInt(arrQuarterDate[1]),parseInt(arrQuarterDate[0]));
            var newdate = new Date(date);
            newdate.setDate(newdate.getDate() + 1); // minus the date
            var nextQuarterStartDate = this.addDays(tempQuarterDate,1);

            // var arrstartDate=this.model.quarterCalendar[Id - 1]["startDate"].split('/');
            // var tempStartDate=arrstartDate[1] + '/' + arrstartDate[0] + '/' + arrstartDate[2];
            // 
            // var daydiff=this.daydiff(this.parseDate(tempStartDate),this.parseDate(tempQuarterDate)) + parseInt("1");
            // var AddDays =Math.round(parseInt(daydiff.toString()) / 2);
            // var midDate=this.addDays(tempStartDate,AddDays);

            // var arrNextEndDate=this.model.quarterCalendar[Id]["endDate"].split('/');
            // var tempNextEndDate=arrNextEndDate[1] + '/' + arrNextEndDate[0] + '/' + arrNextEndDate[2];

            // var arrNextStartDate = nextQuarterStartDate.split('/');
            // var tempNextStartDate=arrNextStartDate[1] + '/' + arrNextStartDate[0] + '/' + arrNextStartDate[2];


            // var daydiff=this.daydiff(this.parseDate(tempNextStartDate),this.parseDate(tempNextEndDate)) + parseInt("1");
            // var AddDays =Math.round(parseInt(daydiff.toString()) / 2);
            // var nextMidDate=this.addDays(tempNextStartDate,AddDays);

            this.model.monthlyCalendar[Id]["startDate"]=nextQuarterStartDate;
            this.model.monthlyCalendar[Id-1]["endDate"]=quarterDate;
           // this.model.quarterCalendar[Id-1]["midDate"]=midDate;

          //   this.model.quarterCalendar[Id]["midDate"]=nextMidDate;
          } else {
            this.toastr.warning(this.InvalidDate + quarterDate);
            txtEndDate.value = '';
            return false;
          }
    }
}


   parseDate(str) {
     var mdy = str.split('/');
     return new Date(mdy[2],mdy[0] , mdy[1]);
   }

    daydiff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }

    getDaysInMonth(month,year) {
        return new Date(year, month, 0).getDate();
       }

    addDays(currentdate,n){
       var t = new Date(currentdate);
       t.setDate(t.getDate() + n); 
       var month = "0"+(t.getMonth()+1);
       var date = "0"+t.getDate();
       month = month.slice(-2);
       date = date.slice(-2);
       date = date +"/"+month +"/"+t.getFullYear();
       return date;
    }

    	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
}