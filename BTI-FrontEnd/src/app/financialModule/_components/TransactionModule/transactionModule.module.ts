import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import {SelectModule} from 'ng2-select';
import { JournalEntrySetupComponent } from './generalLedger/JournalEntry/journal-entry.component';
import { BatchSetupComponent } from './generalLedger/GL-Batches/gl-batches.component';
import { ClearingEntryComponent } from './generalLedger/ClearingEntry/clearingEntry.component';
import { CashReceiptComponent } from './generalLedger/CashReceiptEntry/cash-receipt-entry.component';
import { BankTransferComponent } from './generalLedger/BankTransferEntry/bank-transfer-entry.component';
import { ARTransactionEntryComponent } from './accountReceivables/TransactionEntry/ar-transaction-entry.component';
import { ARCashReceiptComponent } from './accountReceivables/CashReceiptEntry/cash-receipt-entry.component';
import { ArBatchSetupComponent } from './accountReceivables/BatchSetup/batch-setup.component';
import { APTransactionEntryComponent } from './accountPayables/TransactionEntry/ap-transaction-entry.component';
import { APBatchSetupComponent } from './accountPayables/BatchSetup/batch-setup.component';
import { ManualPaymentEntryComponent } from './accountPayables/ManualPaymentEntry/manual-payment-entry.component';
import { ApplyArTransactionComponent } from './accountReceivables/ApplyARTransaction/apply-ar-transaction.component';
import { ApplyAPTransactionComponent } from './accountPayables/ApplyAPTransaction/apply-ap-transaction.component';
import { TransactionRoutingModule } from './transactionModule-routing.module';
import {ModalModule} from "ng2-modal";
import { SharedModule } from "../../../shared/shared.module";
import { PaymentVaoucherComponent } from './generalLedger/payment-vaoucher/payment-vaoucher.component';
import { AccountStructureService } from '../../_services/general-ledger-setup/accounts-structure-setup.service';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    SelectModule,
    AngularMultiSelectModule,
    TransactionRoutingModule,    
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule,
    ModalModule,
    SharedModule
  ],
  declarations: [
    JournalEntrySetupComponent,BatchSetupComponent,ClearingEntryComponent,
    CashReceiptComponent, BankTransferComponent,ARTransactionEntryComponent,
    ARCashReceiptComponent,ArBatchSetupComponent
    ,APTransactionEntryComponent,APBatchSetupComponent,
    ManualPaymentEntryComponent, ApplyArTransactionComponent,
    ApplyAPTransactionComponent,AutoFocusDirective, PaymentVaoucherComponent
    ],
    providers: [AccountStructureService]
})
export class TransactionModule { }
