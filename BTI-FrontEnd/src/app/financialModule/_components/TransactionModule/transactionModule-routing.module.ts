import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JournalEntrySetupComponent } from './generalLedger/JournalEntry/journal-entry.component';
import { BatchSetupComponent } from './generalLedger/GL-Batches/gl-batches.component';
import { ClearingEntryComponent } from './generalLedger/ClearingEntry/clearingEntry.component';
import { CashReceiptComponent } from './generalLedger/CashReceiptEntry/cash-receipt-entry.component';
import { BankTransferComponent } from './generalLedger/BankTransferEntry/bank-transfer-entry.component';
import { ARTransactionEntryComponent } from './accountReceivables/TransactionEntry/ar-transaction-entry.component';
import { ARCashReceiptComponent } from './accountReceivables/CashReceiptEntry/cash-receipt-entry.component';
import { ArBatchSetupComponent } from './accountReceivables/BatchSetup/batch-setup.component';
import { APTransactionEntryComponent } from './accountPayables/TransactionEntry/ap-transaction-entry.component';
import { APBatchSetupComponent } from './accountPayables/BatchSetup/batch-setup.component';
import { ManualPaymentEntryComponent } from './accountPayables/ManualPaymentEntry/manual-payment-entry.component';
import { ApplyArTransactionComponent } from './accountReceivables/ApplyARTransaction/apply-ar-transaction.component';
import { ApplyAPTransactionComponent } from './accountPayables/ApplyAPTransaction/apply-ap-transaction.component';
import { PaymentVaoucherComponent } from './generalLedger/payment-vaoucher/payment-vaoucher.component';

const routes: Routes = [
  { path: '', component: JournalEntrySetupComponent },
  {path:'journalentrySetup',component:JournalEntrySetupComponent},
  {path:'batchsetup',component:BatchSetupComponent},
  {path:'clearingentry',component:ClearingEntryComponent},
  {path:'cashreceipt',component:CashReceiptComponent},
  {path:'banktransferentry',component:BankTransferComponent},
  {path:'accountreceivablestransaction',component:ARTransactionEntryComponent},
  {path:'arcashreceipt',component:ARCashReceiptComponent},
  {path:'arbatchsetup',component:ArBatchSetupComponent},
  {path:'accountpayablestransaction',component:APTransactionEntryComponent},
  {path:'accountpayablesbatch',component:APBatchSetupComponent},
  {path:'manualpaymententry',component:ManualPaymentEntryComponent},
  {path:'applyartransaction',component:ApplyArTransactionComponent},
  {path:'applyaptransaction',component:ApplyAPTransactionComponent},
  {path:'paymentvaoucher',component:PaymentVaoucherComponent}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransactionRoutingModule { }
