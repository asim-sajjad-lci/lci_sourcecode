import { Component, ViewChild, ViewContainerRef, ViewEncapsulation, ElementRef, QueryList, ContentChildren } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { JournalEntrySetup } from "../../../../../financialModule/_models/transactionModule/generalLedger/JournalEntry";
import { JournalEntryService } from '../../../../../financialModule/_services/transactionModule/generalLedger/JournalEntry.service';
import { BatchSetupService } from '../../../../../financialModule/_services/transactionModule/generalLedger/batchSetUp.service';
import { AccountStructureService } from '../../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';

import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import { Observable } from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { ModalModule, Modal } from "ng2-modal";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { PostedJournalEntry } from '../../../../_models/transactionModule/generalLedger/PostedJournalEntry';
// import { NgSelectComponent } from '@ng-select/ng-select';
import { SelectComponent } from 'ng2-select';

// import { Overlay, overlayConfigFactory } from 'angular2-modal';
@Component({
    selector: '<JournalEntry></JournalEntry>',
    templateUrl: './journal-entry.component.html',
    providers: [JournalEntryService, CommonService, BatchSetupService, AccountStructureService]
})

//export to make it available for other classes
export class JournalEntrySetupComponent {

   
    @ViewChild('selectAccountNumber') selectAccountNumber;

    //posted Journal Entry vars
    postedJournalEntry: any = {};
    accountNumberDescriptionArabic: string[] = [];
    accountNumberDescription: string[] = [];

    currentLanguage;
    dispalyArabicDescriptiopn: boolean;
    displayEnglishDescription: boolean;

    pageUnposted = new Page();
    rowsUnposted = new Array<JournalEntrySetup>();
    unPostedselected = [];
    unPostedsearchKeyword = '';
    ddPageSizeUnposted: number = 5;
    ddPageSizePosted: number = 5;

    selectedText = Constants.selectedText;
    totalText = Constants.totalText;

    pageposted = new Page();
    rowsposted = new Array<PostedJournalEntry>();
    Postedselected = [];
    postedsearchKeyword = '';

    selectedJournalEntryDetail: any = {};
    selectedModel: any = [];

    isGroupAction: boolean = false;
    atATimeText = Constants.atATimeText;

    moduleCode = Constants.financialModuleCode;
    currentTransactionType = Constants.batchTransacitonType.JOURNAL_ENTRY;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    //isScreenLock:boolean;
    copyScreenModuleName = '';
    correctScreenModuleName = '';
    exchangeRateScreenModuleName = '';
    defaultAddFormValues: Array<object>;
    defaultCopyFormValues: Array<object>;
    defaultCorrectFormValues: Array<object>;
    defaultExchangeRateFormValues: Array<object>;
    defaultPrintFormValues: Array<object>;
    defaultUnpostedJournalEntryTable: Array<object>;
    defaultPostedJournalEntryTable: Array<object>;
    defaultVirwPostedJournalEntryTable: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select = Constants.select;
    btnCancelText = Constants.btnCancelText;
    close = Constants.close;
    tableViewtext = Constants.tableViewtext;
    isModify: boolean = false;
    IsAddJournalEntryDetails: boolean = false;
    isIntercompany: boolean = true;
    isReversing: number = 0;
    public items: Array<any> = [];
    emptyArray = [{ 'id': '0', 'text': this.select }];
    arrAuditTrial = this.emptyArray;
    arrBatchList = this.emptyArray;
    arrCurrency = this.emptyArray;
    arrCompany = this.emptyArray;
    arrEditCompany = this.emptyArray;
    arrYearsList = this.emptyArray;
    arrJVEntryNumber = this.emptyArray;
    arrJVEntryYear = this.emptyArray;
    arrAccountNumber = [];
    arrEditAccountNumber = [];
    accountNumber: string[] = [];
    accountTableRow: any = { 'id': '', 'text': '' };
    accountTableRowEdit: any = { 'id': '', 'text': '' };
    intercompany: any = { 'id': '', 'text': '' };
    intereditcompany: any = { 'id': '', 'text': '' };
    debitAmount: number = 0;
    creditAmount: number = 0;
    debitEditAmount: number = 0;
    creditEditAmount: number = 0;
    distributionDescription: '';
    distributionEditDescription: '';
    accountDescriptionTitle = Constants.accountDescription;
    accountDescription: string;
    accountDescriptionEdit: string;
    editAccountDescriptionDisplay: string;
    arrAccountDescription: string[];
    arrAccountDescriptionArabic: string[];
    isCredit: boolean = false;
    totalJournalEntryDebit: number = 0;
    totalJournalEntryCredit: number = 0;
    amtDifference: number = 0;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isCopyJournalEntry: boolean = false;
    isPostJournalEntry: boolean = false;
    isCorrectJournalEntry: boolean = false;
    isDeleteAction: boolean = false;
    isConfirmationModalOpen: boolean = false;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    typicalBalanceType: string;
    txtDebitAmount;
    txtCreditAmount;
    txtDistributionDescription;
    showBtns: boolean = false;

    accountNumberTitleLabel: string;
    createAccountNumberLabel: string;
    btnCancelLabel: string;

    btndisabled: boolean = false;

    accountNumberIndex = [];
    isModalOpen: boolean = false;
    arrSegment = [];

    hoverdAccountNumber: string;
    accountDescirptionDisplay: string;

    loaderImg:boolean = false;

    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };

    myOptions2: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: { year: 0, month: 0, day: 0 }
    };

    activeBatch: any = this.emptyArray;
    activeAuditTrial: any = this.emptyArray;
    activeCurrency: any = this.emptyArray;
    activeCompany: any = this.emptyArray;
    activeAccountNumber: any = this.emptyArray;
    activeEditCompany: any = this.emptyArray;
    activeEditAccountNumber: any = this.emptyArray;
    activeJournalEntry: any = this.emptyArray;
    arrActionList = this.emptyArray;
    arrCorrectYearsList = this.emptyArray;
    arrCorrectJVEntryNumber = this.emptyArray;
    arrExchangeDetail = this.emptyArray;
    activeYears = this.emptyArray;
    activeCorrectYears = this.emptyArray;
    activeAction = this.emptyArray;
    activeJVEntryNumber = this.emptyArray;
    activeCorrectJVEntryNumber = this.emptyArray;
    activeExchangeDetail = this.emptyArray;

    arrJournalEntry = [];
    today = new Date();
    year = this.today.getFullYear();
    private Object = {
        date: { year: 2018, month: 10, day: 9 }
        //  date: { year: this.today.getFullYear(), month: this.today.getMonth() + 1+1, day: this.today.getDate() }

    };
    arrjournalEntryDetailsList: any = [];
    nextJournalEntry: string;
    isPostAllowed: boolean = true;
    isPostEvt: boolean = false;
    isCreditAllowed: boolean = false;
    isDebitAllowed: boolean = false;

    arrjournalEntryReport: any = {
        "id": "",
        "journalID": "",
        "userId": "",
        "glBatchId": "",
        "glBatchComment": "",
        "transactionType": "",
        "transactionDate": "",
        "transactionReversingDate": "",
        "sourceDocumentId": "",
        "sourceDocument": "",
        "transactionReference": "",
        "transactionReferenceArabic": "",
        "totalDistributions": "",
        "totalDebit": "",
        "totalCredit": "",
        "dtoGLReportDetailList": [
            {
                "debitAmount": "",
                "creditAmount": "",
                "journalSequence": "",
                "accountNumber": "",
                "accountDescription": ""
            },
            {
                "debitAmount": "",
                "creditAmount": "",
                "journalSequence": "",
                "accountNumber": "",
                "accountDescription": ""

            }
        ]
    }


    userData: any;

    PRT_HEADING: any = { 'fieldName': 'PRT_HEADING', 'fieldValue': 'Journal Entry', 'helpMessage': 'Journal Entry', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_BATCH_ID: any = { 'fieldName': 'PRT_BATCH_ID', 'fieldValue': 'Batch ID', 'helpMessage': 'Batch ID', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_BATCH_COMMENT: any = { 'fieldName': 'PRT_BATCH_COMMENT', 'fieldValue': 'Batch Comment', 'helpMessage': 'Batch Comment', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };

    PRT_TH_JOURNAL_ENTRY: any = { 'fieldName': 'PRT_TH_JOURNAL_ENTRY', 'fieldValue': 'Journal Entry', 'helpMessage': 'Journal Entry', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_TH_TRANSACTION_TYPE: any = { 'fieldName': 'PRT_TH_TRANSACTION_TYPE', 'fieldValue': 'Transaction Type', 'helpMessage': 'Transaction Type', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_TH_TRANSACTION_DATE: any = { 'fieldName': 'PRT_TH_TRANSACTION_DATE', 'fieldValue': 'Transaction Date', 'helpMessage': 'Transaction Date', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_TH_REVERSING_DATE: any = { 'fieldName': 'PRT_TH_REVERSING_DATE', 'fieldValue': 'Reversing Date', 'helpMessage': 'Reversing Date', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_TH_SOURCE_DOCUMENT: any = { 'fieldName': 'PRT_TH_SOURCE_DOCUMENT', 'fieldValue': 'Source Document', 'helpMessage': 'Source Document', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_TH_TRANSACTION_REFERENCE: any = { 'fieldName': 'PRT_TH_TRANSACTION_REFERENCE', 'fieldValue': 'Transaction Reference', 'helpMessage': 'Transaction Reference', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };

    PRT_ACCOUNT_NUMBER: any = { 'fieldName': 'PRT_ACCOUNT_NUMBER', 'fieldValue': 'Account Number', 'helpMessage': 'Account Number', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_ACCOUNT_DESCRIPTION: any = { 'fieldName': 'PRT_ACCOUNT_DESCRIPTION', 'fieldValue': 'Account Description', 'helpMessage': 'Account Description', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_DEBIT: any = { 'fieldName': 'PRT_DEBIT', 'fieldValue': 'Debit', 'helpMessage': 'Debit', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_CREDIT: any = { 'fieldName': 'PRT_CREDIT', 'fieldValue': 'Credit', 'helpMessage': 'Credit', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };

    PRT_TOTAL: any = { 'fieldName': 'PRT_TOTAL', 'fieldValue': 'Total', 'helpMessage': 'Total', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_TOTAL_DISTRIBUTION: any = { 'fieldName': 'PRT_TOTAL_DISTRIBUTION', 'fieldValue': 'Total Distribution', 'helpMessage': 'Total Distribution', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_BTN_PRINT: any = { 'fieldName': 'PRT_BTN_PRINT', 'fieldValue': 'Print', 'helpMessage': 'Print', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };
    PRT_BTN_CLOSE: any = { 'fieldName': 'PRT_BTN_CLOSE', 'fieldValue': 'Close', 'helpMessage': 'Close', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' };

    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private journalEntryService: JournalEntryService,
        private commonService: CommonService,
        private batchSetupService: BatchSetupService,
        public toastr: ToastsManager,
        private accountStructureService: AccountStructureService,
        vcr: ViewContainerRef
    ) {
        this.pageUnposted.pageNumber = 0;
        this.pageUnposted.size = 5;
        this.setPageUnPosted({ offset: 0 });
        this.setPagePosted({ offset: 0 });
        this.toastr.setRootViewContainerRef(vcr);
        this.getCompanyList();
        this.userData = JSON.parse(localStorage.getItem('currentUser'));
        if (this.userData.companyCount > 1) {
            this.isIntercompany = false;
            this.model.interCompany = true;

        }
        else {
            this.model.interCompany = false;
            this.bindAccountNumber(0);
            this.bindEditAccountNumber(0);
        }
        this.getCompanyEditList();
        this.accountNumberTitleLabel = Constants.accountNumberTitle;
        this.createAccountNumberLabel = Constants.createAccountNumber;
        this.btnCancelLabel = Constants.btnCancelText;
        this.currentLanguage = localStorage.getItem('currentLanguage');
        if (this.currentLanguage == 2) {
            this.dispalyArabicDescriptiopn = true;
            this.displayEnglishDescription = !this.dispalyArabicDescriptiopn;
        } else {
            this.displayEnglishDescription = true;
            this.dispalyArabicDescriptiopn = !this.displayEnglishDescription;
        }
    }

    ngOnInit() {

        //this.getBatchList()
        this.getAuditTrialCode();
        this.getCurrencySetupList();
        this.getGLNextJournalEntry();
        this.getJournalIdList();
        this.GetCopyScreenDetail();
        this.GetCorrectScreenDetail();
        this.GetYearsList();
        this.GetActionList();
        this.GetCorrectYearsList();
        this.GetExchangeRateScreenDetail();
        this.getSegmentCount();
        this.getUnpostedScreenDetail();
        this.getpostedScreenDetail();
        this.getViewPostedScreenDetail();
        this.model.transactionDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.model.transactionType = "1";
        this.getAddScreenDetail();
        this.GetPrintScreenDetail();
    }
    //let date = Date.
    // setting pagination
    //Add by ali
    //unposted JournalEntry
    setPageUnPosted(pageInfo) {
        this.unPostedselected = []; // remove any selected checkbox on paging
        this.pageUnposted.pageNumber = pageInfo.offset;
        this.journalEntryService.getAllUnpostedJournalEntry(this.pageUnposted, this.unPostedsearchKeyword).subscribe(pagedData => {
            this.pageUnposted = pagedData.page;
            this.rowsUnposted = pagedData.data;
            console.log(this.rowsUnposted);
        });
    }
    // Set default page size
    //add by ali
    changePageSizeUnPosted(event) {
        this.pageUnposted.size = event.target.value;
        this.setPageUnPosted({ offset: 0 });
    }

    // setting pagination
    //Add by ali
    //posted JournalEntry
    setPagePosted(pageInfo) {
        this.Postedselected = []; // remove any selected checkbox on paging
        this.pageposted.pageNumber = pageInfo.offset;
        this.journalEntryService.getAllPostedJournalEntry(this.pageposted, this.postedsearchKeyword).subscribe(pagedData => {
            this.pageposted = pagedData.page;
            this.rowsposted = pagedData.data;
            console.log(this.rowsposted);
        });
    }

    // Set default page size
    //add by ali
    changePageSizePosted(event) {
        this.pageposted.size = event.target.value;
        this.setPagePosted({ offset: 0 });
    }

    // delete one or multiple journal
    delete() {

        var selectedRoles = [];
        for (var i = 0; i < this.unPostedselected.length; i++) {
            selectedRoles.push(this.unPostedselected[i].id);
        }


        this.journalEntryService.DeleteJournalEntries(selectedRoles).then(data => {
            window.scrollTo(0, 0);
            var datacode = data.code;
            console.log(data);
            if (datacode == 302) {
                this.setPageUnPosted({ offset: 0 });
                this.hasMsg = true;
                this.closeModal();
                this.closeModal();
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                    this.messageText = Constants.deleteSuccessMessage;
                }, 4000);
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
            } else {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            }



        }).catch(error => {

        });

    }

    // update table data upon the search key word
    updateFilterUnposted(event) {

        this.unPostedsearchKeyword = event.target.value.toLowerCase();
        this.pageUnposted.pageNumber = 0;
        this.pageUnposted.size = this.ddPageSizeUnposted;
        this.journalEntryService.getAllUnpostedJournalEntry(this.pageUnposted, this.unPostedsearchKeyword).subscribe(pagedData => {
            this.hasMsg = false;
            this.showMsg = false;
            this.pageUnposted = pagedData.page;
            this.rowsUnposted = pagedData.data;
            this.table.offset = 0;
            console.log(pagedData);
        }, error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = Constants.serverErrorText;
            }, 100)
        });
    }

    // update table data upon the search key word
    updateFilterPosted(event) {

        this.postedsearchKeyword = event.target.value.toLowerCase();
        this.pageposted.pageNumber = 0;
        this.pageposted.size = this.ddPageSizePosted;
        this.journalEntryService.getAllPostedJournalEntry(this.pageposted, this.postedsearchKeyword).subscribe(pagedData => {
            this.hasMsg = false;
            this.showMsg = false;
            console.log(pagedData);
            this.pageposted = pagedData.page;
            this.rowsposted = pagedData.data;
            this.table.offset = 0;
        }, error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = Constants.serverErrorText;
            }, 100)
        });


    }

    getUnpostedScreenDetail() {
        this.screenCode = "S-4000";
        this.defaultUnpostedJournalEntryTable = [
            { 'fieldName': 'Unposted_Journal_Entry', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_Description', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_Debit', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_Credit', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_Entry_transaction_Date', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ACTION_Unposted_Journal_Entry', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_Description_Arabic', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_DELETE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'Unposted_Journal_Entry_Tab', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' }
        ];

        this.getScreenDetail(this.screenCode, 'UnpostedTable');
    }

    getpostedScreenDetail() {
        this.screenCode = "S-4001";
        this.defaultPostedJournalEntryTable = [
            { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Entry', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Description', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Description_Arabic', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Entry_transaction_Post_Date', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Entry_transaction_Date', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Debit', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Credit', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ACTION_posted_Journal_Entry', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'posted_Journal_Entry_Tab', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' }


        ];

        this.getScreenDetail(this.screenCode, 'postedTable');
    }


    getViewPostedScreenDetail() {
        this.screenCode = "S-1268";
        this.defaultVirwPostedJournalEntryTable = [
            { 'fieldName': 'JOURNAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'REFRENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ARABIC_REFRENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'TRANSACTION_SOURCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'TRANSACTION_CURRENCY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'TRANSACTION_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'TRANSACTION_POSTED_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' }


        ];

        this.getScreenDetail(this.screenCode, 'ViewPoste');
    }

    getAddScreenDetail() {
        this.screenCode = "S-1229";
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_JE_JOURNAL_ENTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_INTERCOMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_REFERENCE_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_JOURNAL_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_STANDARD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_REVERSING', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_TRANSACTION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_REVERSING_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_SOURCE_DOC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_CURRENCY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_CO_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_ACCOUNT_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_DIS_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_DEBIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_TOTAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_DIFFERENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_COPY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_CORRECT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'ADD_JE_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' }
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res => {
            this.isScreenLock = res;
        });
        this.getScreenDetail(this.screenCode, 'Add');
    }

    GetCopyScreenDetail() {
        this.screenCode = "S-1230";
        this.defaultCopyFormValues = [
            { 'fieldName': 'COPY_JE_ORIGINAL_JV_ENTRY_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'COPY_JE_ORIGINAL_JV_ENTRY_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'COPY_JE_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'COPY_JE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
        ];
        this.getScreenDetail(this.screenCode, 'Copy');
    }

    GetCorrectScreenDetail() {
        this.screenCode = "S-1231";
        this.defaultCorrectFormValues = [
            { 'fieldName': 'CORRECT_JE_ACTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'CORRECT_JE_ORIGINAL_JV_ENTRY_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'CORRECT_JE_ORIGINAL_JV_ENTRY_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'CORRECT_JE_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'CORRECT_JE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
        ];

        this.getScreenDetail(this.screenCode, 'Correct');
    }

    GetExchangeRateScreenDetail() {
        this.screenCode = "S-1238";
        this.defaultExchangeRateFormValues = [
            { 'fieldName': 'EX_TABLE_DET_JOURNAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'EX_TABLE_DET_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'EX_TABLE_DET_EX_TABLE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'EX_TABLE_DET_EX_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'EX_TABLE_DET_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'EX_TABLE_DET_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
        ];
        this.getScreenDetail(this.screenCode, 'ExchangeRate');
    }

    GetPrintScreenDetail() {
        this.screenCode = "S-1263";
        this.defaultPrintFormValues = [
            { 'fieldName': 'PRT_HEADING', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_BATCH_COMMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },

            { 'fieldName': 'PRT_TH_JOURNAL_ENTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_TH_TRANSACTION_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_TH_TRANSACTION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_TH_REVERSING_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_TH_SOURCE_DOCUMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_TH_TRANSACTION_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },

            { 'fieldName': 'PRT_ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_ACCOUNT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_DEBIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },

            { 'fieldName': 'PRT_TOTAL', 'fieldValue': 'Total', 'helpMessage': 'Total', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_TOTAL_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_BTN_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
            { 'fieldName': 'PRT_BTN_CLOSE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
        ];
        this.getScreenDetail(this.screenCode, 'Print');

        this.PRT_HEADING = this.defaultPrintFormValues[0];
        this.PRT_BATCH_ID = this.defaultPrintFormValues[1];
        this.PRT_BATCH_COMMENT = this.defaultPrintFormValues[2];

        this.PRT_TH_JOURNAL_ENTRY = this.defaultPrintFormValues[3];
        this.PRT_TH_TRANSACTION_TYPE = this.defaultPrintFormValues[4];
        this.PRT_TH_TRANSACTION_DATE = this.defaultPrintFormValues[5];
        this.PRT_TH_REVERSING_DATE = this.defaultPrintFormValues[6];
        this.PRT_TH_SOURCE_DOCUMENT = this.defaultPrintFormValues[7];
        this.PRT_TH_TRANSACTION_REFERENCE = this.defaultPrintFormValues[8];

        this.PRT_ACCOUNT_NUMBER = this.defaultPrintFormValues[9];
        this.PRT_ACCOUNT_DESCRIPTION = this.defaultPrintFormValues[10];
        this.PRT_DEBIT = this.defaultPrintFormValues[11];
        this.PRT_CREDIT = this.defaultPrintFormValues[12];

        this.PRT_TOTAL = this.defaultPrintFormValues[13];
        this.PRT_TOTAL_DISTRIBUTION = this.defaultPrintFormValues[14];
        this.PRT_BTN_PRINT = this.defaultPrintFormValues[15];
        this.PRT_BTN_CLOSE = this.defaultPrintFormValues[16];


    }
    GetExchangeTableSetupByCurrencyId(currencyID: string) {

        this.journalEntryService.getExchangeTableSetupByCurrencyId(currencyID).then(data => {
            this.arrExchangeDetail = [{ 'id': '0', 'text': this.select }];
            this.bindSelectedItem(this.emptyArray[0], 'ExchangeDetail')
            //this.model.exchangeExpirationDate='';
            //this.model.exchangeRate='';
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                for (var i = 0; i < data.result.records.length; i++) {
                    this.arrExchangeDetail.push({ 'id': data.result.records[i].exchangeTableIndex, 'text': data.result.records[i].exchangeId });
                }
                var defaultExchangeCurrency: any = [];
                defaultExchangeCurrency = data.result.records.find(x => x.default == true);
                this.bindSelectedItem({ 'id': defaultExchangeCurrency.exchangeTableIndex, 'text': defaultExchangeCurrency.exchangeId }, 'ExchangeDetail')

            }
        });
    }

    onDateChanged(event: IMyDateModel): void {
        var RequestedData = {
            "transactionDate": event.formatted,
            "moduleName": "GL",
            "originId": Constants.massCloseOriginType.GL_JOURNAL_ENTRY
        }
        this.commonService.checkTransactionDateIsValid(RequestedData).then(data => {
            if (data.btiMessage.messageShort == "INVALID_TRANSACTION_DATE") {
                this.model.transactionDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });
    }

    GetCurrencyExchangeDetail(exchangeId: string) {

        this.journalEntryService.GetCurrencyExchangeDetail(exchangeId).then(data => {
            if (data.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND") {
                if (!this.model.exchangeRate) {
                    this.model.exchangeRate = data.result.exchangeRate;
                }
                this.model.exchangeExpirationDate = data.result.exchangeExpirationDate;
            }
        });
    }

    getScreenDetail(screenCode, ArrayType) {

        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName = data.result.dtoScreenDetail.screenName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;



            if (ArrayType == 'Add') {
                this.moduleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];

                }
            }
            else if (ArrayType == 'Copy') {
                this.copyScreenModuleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultCopyFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if (ArrayType == 'Correct') {
                this.correctScreenModuleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultCorrectFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if (ArrayType == 'ExchangeRate') {
                this.exchangeRateScreenModuleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultExchangeRateFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if (ArrayType == 'Print') {
                this.exchangeRateScreenModuleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultPrintFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];



                }
            } else if (ArrayType == 'UnpostedTable') {
                this.moduleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultUnpostedJournalEntryTable.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];

                }

            } else if (ArrayType == 'postedTable') {
                this.moduleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultPostedJournalEntryTable.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];

                }
            } else if (ArrayType == 'ViewPoste') {
                console.log(data);
                this.moduleName = data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultPostedJournalEntryTable.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];

                }
            }
        });
    }
    getAuditTrialCode() {
        this.commonService.getAuditTrialCode().then(data => {
            //  this.arrAuditTrial=[];
            this.arrAuditTrial = [{ 'id': '0', 'text': this.select }];
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                for (var i = 0; i < data.result.records.length; i++) {
                    this.arrAuditTrial.push({ 'id': data.result.records[i].seriesIndex, 'text': data.result.records[i].sourceDocument });
                }
            }
        });
    }
    getBatchList() {
        this.commonService.getAllBatchesListByTransactionType(this.currentTransactionType).then(data => {
            this.arrBatchList = [{ 'id': '0', 'text': this.select }];
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                for (var i = 0; i < data.result.length; i++) {
                    this.arrBatchList.push({ 'id': data.result[i].batchId, 'text': data.result[i].batchId });
                }
            }
        });
    }

    // get Batches By Payment method
    getBatchListByPaymentMethod(sourceDocumentId: number, selectedBatchId?) {
        this.bindSelectedItem({ 'id': '0', 'text': '--Select--' },'Batch');
        this.commonService.getAllBatchesListByPaymentMethod(sourceDocumentId).then(data => {
            this.arrBatchList = [{ 'id': '0', 'text': this.select }];
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                for (let i = 0; i < data.result.length; i++) {
                    this.arrBatchList.push({ 'id': data.result[i].batchId, 'text': data.result[i].batchId });
                }
            }
            if (selectedBatchId) {
                let currentSelectedBatch = this.arrBatchList.find(x => x.id == selectedBatchId);
                this.bindSelectedItem(currentSelectedBatch,'Batch');
            }
        });
    }

    getPostedJournalEntryByID(journalID: string) {
        console.log(journalID);
        this.journalEntryService.getPostedJournalById(journalID).subscribe(data => {
            console.log(data);
            this.postedJournalEntry = data.result;
        });
    }

    getPostedJournalEntryByIDAndAuditTrial(journalEntry) {
        this.journalEntryService.getPostedJournalByIdAndAudit(journalEntry).subscribe(data => {
            console.log(data);
            this.postedJournalEntry = data.result;
        });
        console.log(journalEntry);
    }

    getCurrencySetupList() {
        this.commonService.getCurrencySetup().then(data => {
            this.arrCurrency = [{ 'id': '0', 'text': this.select }];
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                var fatchData = data.result;
                for (var i = 0; i < data.result.records.length; i++) {
                    this.arrCurrency.push({ 'id': data.result.records[i].currencyId, 'text': data.result.records[i].currencyId });
                }
                var defaultCurrency: any = [];
                defaultCurrency = data.result.records.find(x => x.defaultCurrency == true);
                console.log(defaultCurrency);
                this.bindSelectedItem({ 'id': defaultCurrency.currencyId, 'text': defaultCurrency.currencyId }, 'Currency');
            }
        });
    }


    getCompanyEditList() {
        this.commonService.getCompanyList().then(data => {

            this.arrEditCompany = [{ 'id': '0', 'text': this.select }];
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {

                var fatchData = data.result;
                for (var i = 0; i < data.result.length; i++) {

                    this.arrEditCompany.push({ 'id': data.result[i].companyId + '*' + data.result[i].tenantId, 'text': data.result[i].name });
                }
                var tenantid = localStorage.getItem('tenantid')
                var SelectedComp = fatchData.find(x => x.tenantId == tenantid);
                this.bindSelectedItem({ 'id': SelectedComp.companyId + '*' + SelectedComp.tenantId, 'text': SelectedComp.name }, 'EditCompany')
            }
        });

    }

    getCompanyList() {
        this.commonService.getCompanyList().then(data => {
            this.arrCompany = [{ 'id': '0', 'text': this.select }];

            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                var userData = JSON.parse(localStorage.getItem('currentUser'));
                var fatchData = data.result;
                for (var i = 0; i < data.result.length; i++) {
                    this.arrCompany.push({ 'id': data.result[i].companyId + '*' + data.result[i].tenantId, 'text': data.result[i].name });

                }
                var tenantid = localStorage.getItem('tenantid')
                var SelectedComp = fatchData.find(x => x.tenantId == tenantid);
                console.log(SelectedComp);
                this.bindSelectedItem({ 'id': SelectedComp.companyId + '*' + SelectedComp.tenantId, 'text': SelectedComp.name }, 'Company');

            }
        });
    }

    GetYearsList() {
        this.journalEntryService.getYearsList().then(data => {
            this.arrYearsList = [{ 'id': '0', 'text': this.select }];
            for (var i = 0; i < data.result.length; i++) {
                this.arrYearsList.push({ 'id': data.result[i].toString(), 'text': data.result[i].toString() });
            }
        });
    }

    GetJournalEntryByYear(year: any) {

        this.journalEntryService.getJournalEntryByYear(year).then(data => {
            this.arrJVEntryNumber = [{ 'id': '0', 'text': this.select }];

            if (data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST') {
                for (var i = 0; i < data.result.length; i++) {
                    this.arrJVEntryNumber.push({ 'id': data.result[i].journalID, 'text': data.result[i].journalID });
                }
            }

        });
    }

    GetActionList() {
        this.journalEntryService.actionList().then(data => {
            this.arrActionList = [{ 'id': '0', 'text': this.select }];
            for (var i = 0; i < data.result.length; i++) {
                this.arrActionList.push({ 'id': data.result[i].actiontype, 'text': data.result[i].action });
            }
        });
    }

    GetCorrectYearsList() {
        this.journalEntryService.getYearsList().then(data => {
            this.arrCorrectYearsList = [{ 'id': '0', 'text': this.select }];
            for (var i = 0; i < data.result.length; i++) {
                this.arrCorrectYearsList.push({ 'id': data.result[i], 'text': data.result[i] });
            }
        });
    }

    GetCorrectJournalEntryIdByYear(year: any) {

        this.journalEntryService.getCorrectJournalEntryIdByYear(year).then(data => {
            this.arrCorrectJVEntryNumber = [{ 'id': '0', 'text': this.select }];
            if (data.btiMessage.messageShort != 'RECORD_NOT_FOUND') {
                for (var i = 0; i < data.result.length; i++) {
                    this.arrCorrectJVEntryNumber.push({ 'id': data.result[i].journalID, 'text': data.result[i].journalID });
                }
            }
        });
    }

    validateAmount(amountType: string, thisevt) {

        if (amountType == 'debitAmount' && thisevt.debitAmount == '') {
            this.debitAmount = 0;
            //    this.isDebitAllowed=false;
            //    this.isCreditAllowed=true;
        }
        else if (amountType == 'creditAmount' && thisevt.creditAmount == '') {
            this.creditAmount = 0;
            // this.isCreditAllowed=false;
            // this.isDebitAllowed=true;
        }
        if (amountType == 'debitAmount') {
            if (this.debitAmount > 0) {
                this.creditAmount = 0;
            }
        }
        else if (amountType == 'creditAmount') {
            if (this.creditAmount > 0) {
                this.debitAmount = 0;
            }
        }

    }

    validateEditAmount(amountType: string, thisevt) {

        if (amountType == 'debitEditAmount' && thisevt.debitEditAmount == '') {
            this.debitEditAmount = 0;
            //    this.isDebitAllowed=false;
            //    this.isCreditAllowed=true;
        }
        else if (amountType == 'creditEditAmount' && thisevt.creditEditAmount == '') {
            this.creditEditAmount = 0;
            // this.isCreditAllowed=false;
            // this.isDebitAllowed=true;
        }
        if (amountType == 'debitEditAmount') {
            if (this.debitEditAmount > 0) {
                this.creditEditAmount = 0;
            }
        }
        else if (amountType == 'creditEditAmount') {
            if (this.creditEditAmount > 0) {
                this.debitEditAmount = 0;
            }
        }

    }

    GetJournalEntryByJournalId(JournalId,sourceDocument?) {

        if (JournalId != '0') {
            this.loaderImg = true;
            this.journalEntryService.GetJournalEntryByJournalIdAndAudit(JournalId,sourceDocument).then(data => {


                if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                    this.isModify = true;
                    this.showBtns = true;
                    this.loaderImg = false;
                    this.model = data.result;
                    this.companyVisiblity();
                    this.arrjournalEntryDetailsList = data.result.journalEntryDetailsList;
                    this.model.journalEntryDetailsList = data.result.journalEntryDetailsList;
                    this.model.transactionType = this.model.transactionType.toString();
                    if (data.result.transactionDate) {
                        this.model.transactionDate = data.result.transactionDate;
                    }

                    if (this.model.transactionType == "1") {
                        this.isReversing = 0;

                        var transactionDate = this.model.transactionDate;
                        var transactionDateData = transactionDate.split('/');
                        this.model.transactionDate = { "date": { "year": parseInt(transactionDateData[2]), "month": parseInt(transactionDateData[1]), "day": parseInt(transactionDateData[0]) } };
                        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };

                    }
                    else {
                        this.isReversing = 1
                        var transactionDate = this.model.transactionDate;
                        var transactionDateData = transactionDate.split('/');
                        this.model.transactionDate = { "date": { "year": parseInt(transactionDateData[2]), "month": parseInt(transactionDateData[1]), "day": parseInt(transactionDateData[0]) } };

                        if (data.result.transactionReversingDate) {
                            this.model.transactionReversingDate = data.result.transactionReversingDate;
                        }
                        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
                        // var transactionReversingDate = this.model.transactionReversingDate;
                        // var transactionReversingDateData = transactionReversingDate.split('/');
                        // this.model.transactionReversingDate = {"date":{"year":parseInt(transactionReversingDateData[2]),"month":parseInt(transactionReversingDateData[1]),"day":parseInt(transactionReversingDateData[0])}};

                    }
/*                     if (data.result.glBatchId) {
                        var selectedbatch = this.arrBatchList.find(x => x.id == data.result.glBatchId);
                        this.bindSelectedItem(selectedbatch, 'Batch');
                    } */
                    var sourceDocument = this.arrAuditTrial.find(x => x.id == data.result.sourceDocumentId);
                    this.bindSelectedItem(sourceDocument, 'AuditTrial',data.result.glBatchId);
                    var sourceCurrency = this.arrCurrency.find(x => x.id == data.result.currencyID);
                    this.bindSelectedItem(sourceCurrency, 'Currency');

                    window.setTimeout(() => {

                        var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id == data.result.exchangeTableIndex);
                        this.bindSelectedItem(selectedExchangeDetail, 'ExchangeDetail');
                    }, 2000);

                    this.arrjournalEntryDetailsList = [];
                    // this.model.totalJournalEntryCredit = 0;
                    // this.model.totalJournalEntryDebit = 0;
                    //console.log(data.result.totalJournalEntryCredit);
                    this.model.totalJournalEntryCredit = parseFloat(data.result.totalJournalEntryCredit.toString());
                    this.model.totalJournalEntryDebit = parseFloat(data.result.totalJournalEntryDebit.toString());
                    this.amtDifference = this.model.amountDifference;


                    for (var i = 0; i < data.result.journalEntryDetailsList.length; i++) {

                        // var selectedCompany = this.arrCompany.find(x => x.id.split('*')[0] ==   data.result.journalEntryDetailsList[i].intercompanyId);
                        // if(selectedCompany)
                        if (this.arrCompany.length > 1) {
                            this.arrjournalEntryDetailsList.push({
                                "accountTableRowIndex": data.result.journalEntryDetailsList[i].accountTableRowIndex,
                                'accountNumber': data.result.journalEntryDetailsList[i].accountNumber,
                                "intercompanyId": data.result.journalEntryDetailsList[i].intercompanyId,
                                "interCompanyName": data.result.journalEntryDetailsList[i].companyName,
                                "debitAmount": data.result.journalEntryDetailsList[i].debitAmount,
                                "creditAmount": data.result.journalEntryDetailsList[i].creditAmount,
                                "distributionDescription": data.result.journalEntryDetailsList[i].distributionDescription,
                                "isCredit": data.result.journalEntryDetailsList[i].isCredit,
                                "journalSequence": data.result.journalEntryDetailsList[i].journalSequence,
                                "accountDescription": data.result.journalEntryDetailsList[i].accountDescription
                            }
                            );
                        }
                        else {
                            this.arrjournalEntryDetailsList.push({
                                "accountTableRowIndex": data.result.journalEntryDetailsList[i].accountTableRowIndex,
                                'accountNumber': data.result.journalEntryDetailsList[i].accountNumber,
                                "intercompanyId": '',
                                "interCompanyName": '',
                                "debitAmount": data.result.journalEntryDetailsList[i].debitAmount,
                                "creditAmount": data.result.journalEntryDetailsList[i].creditAmount,
                                "distributionDescription": data.result.journalEntryDetailsList[i].distributionDescription,
                                "isCredit": data.result.journalEntryDetailsList[i].isCredit,
                                "journalSequence": data.result.journalEntryDetailsList[i].journalSequence,
                                "accountDescription": data.result.journalEntryDetailsList[i].accountDescription
                            }
                            );
                        }

                        this.debitAmount = data.result.journalEntryDetailsList[i].debitAmount;
                        if (!this.debitAmount) {
                            this.debitAmount = 0;
                        }
                        this.creditAmount = data.result.journalEntryDetailsList[i].creditAmount;
                        if (!this.creditAmount) {
                            this.creditAmount = 0;
                        }
                        // if(!this.model.totalJournalEntryCredit)
                        // {
                        //     this.model.totalJournalEntryCredit = 0;
                        // }
                        // if(!this.model.totalJournalEntryDebit)
                        // {
                        //     this.model.totalJournalEntryDebit = 0;
                        // }
                        //     this.model.totalJournalEntryCredit = parseFloat(this.model.totalJournalEntryCredit.toString()) +   parseFloat(this.creditAmount.toString());
                        //     this.model.totalJournalEntryDebit = parseFloat(this.model.totalJournalEntryDebit.toString()) +   parseFloat(this.debitAmount.toString());
                        // this.amtDifference=parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());
                        // this.amtDifference = this.DecimalFormater(this.amtDifference);
                        // if(this.model.totalJournalEntryDebit >= this.model.totalJournalEntryCredit)
                        // {
                        //     this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString());
                        // }
                        // else 
                        // {
                        //     this.amtDifference=parseInt(this.model.totalJournalEntryCredit.toString()) - parseInt(this.model.totalJournalEntryDebit.toString());
                        // }


                        this.debitAmount = 0;
                        this.creditAmount = 0;
                    }
                    if (this.amtDifference == 0) {
                        this.isPostAllowed = false;
                    }
                    else {
                        this.isPostAllowed = true;
                    }
                }
                else {
                    this.isModify = false;
                    //this.clear(f);
                }
            });
        }
        else {
            // this.getGLNextJournalEntry();
            this.isModify = false;
            this.loaderImg = false;
            //this.clear(f);
        }
    }

    getCopyOfOptions(): INgxMyDpOptions {
        return JSON.parse(JSON.stringify(this.myOptions2));
    }

    // optional date changed callback
    transactionDate(event: IMyDateModel): void {
        // date selected
        this.model.transactionDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.myOptions2.disableUntil = { year: event.date.year, month: event.date.month, day: event.date.day - 1 }
    }
    transactionReversingDate(event: IMyDateModel): void {
        this.model.transactionDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.myOptions2.disableUntil = { year: event.date.year, month: event.date.month, day: event.date.day - 1 }
    }

    clearTransactionDate() {
        this.model.transactionDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
    }

    cleartransactionReversingDate() {
        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
    }

    companyVisiblity() {
        if (this.userData.companyCount > 1) {
            this.model.interCompany = true;
        }
        else {
            this.model.interCompany = false;
        }
    }

    AddJournalEntryDetails(event) {

        window.event.stopPropagation();
        
        this.IsAddJournalEntryDetails = true;
        if ((this.creditAmount || this.debitAmount) && this.accountTableRow.id && this.distributionDescription && (this.intercompany.id || this.isIntercompany)) {

            if (this.creditAmount) {
                this.isCredit = true;
            }
            else {
                this.isCredit = false;
            }
            if (!this.debitAmount) {
                this.debitAmount = 0;
            }
            if (!this.creditAmount) {
                this.creditAmount = 0;
            }

            if(this.debitAmount != 0 && this.creditAmount != 0){
                this.debitAmount = 0;
            }

            var companyId = this.intercompany.id.split('*');
            this.arrjournalEntryDetailsList.push({
                "accountTableRowIndex": this.accountTableRow.id,
                'accountNumber': this.accountDescirptionDisplay, //  this.accountTableRow.text,
                "intercompanyId": companyId[0],
                "interCompanyName": this.intercompany.text,
                "debitAmount": this.debitAmount,
                "creditAmount": this.creditAmount,
                "distributionDescription": this.distributionDescription,
                "isCredit": this.isCredit,
                "accountDescription": this.accountDescription
            }
            );
            this.IsAddJournalEntryDetails = false;
            //this.getCompanyList();
            //this.activeCompany=[this.emptyArray[0]];
            //  this.bindSelectedItem(this.emptyArray[0],'AccountNumber');

            this.activeAccountNumber = [this.emptyArray[0]];
            this.accountDescirptionDisplay = '';
            //this.arrAccountNumber=[{'id':'0','text':this.select}];
            //this.arrEditAccountNumber=[{'id':'0','text':this.select}];

            if (!this.model.totalJournalEntryCredit) {
                this.model.totalJournalEntryCredit = 0;
            }
            if (!this.model.totalJournalEntryDebit) {
                this.model.totalJournalEntryDebit = 0;
            }

            this.model.totalJournalEntryCredit = parseFloat(this.model.totalJournalEntryCredit.toString()) + parseFloat(this.creditAmount.toString());
            this.model.totalJournalEntryDebit = parseFloat(this.model.totalJournalEntryDebit.toString()) + parseFloat(this.debitAmount.toString());
            this.amtDifference = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());
            this.amtDifference = this.DecimalFormater(this.amtDifference);
            // this.model.totalJournalEntryCredit = this.DecimalFormater(this.model.totalJournalEntryCredit);
            // this.model.totalJournalEntryDebit = this.DecimalFormater( this.model.totalJournalEntryDebit);

            // if(this.model.totalJournalEntryDebit >= this.model.totalJournalEntryCredit)
            // {
            //     this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString());
            // }
            // else 
            // {
            //     this.amtDifference=parseInt(this.model.totalJournalEntryCredit.toString()) - parseInt(this.model.totalJournalEntryDebit.toString());
            // }
            // this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString())
            this.debitAmount = 0;
            this.creditAmount = 0;
            this.distributionDescription = '';
            this.isCredit = false;
            this.accountDescription = '';

            if (parseInt(this.amtDifference.toString()) > 0) {
                this.isPostAllowed = true;
            }
            else {
                this.isPostAllowed = false;
            }
            console.log(this.selectAccountNumber);
            var el = this.selectAccountNumber.element.nativeElement.querySelector('div.ui-select-container');
            
            console.log(el);
            if (el) { el.focus(); }
        }
        else {
            // alert(Constants.msgText);
        }

    }

    resetMe() {
        this.bindSelectedItem({ 'id': '0', 'text': this.select }, 'Company');
        // this.bindSelectedItem({'id':'','text':''},'AccountNumber');
    }


    RemoveJournalEntryDetails(RowIndex, tempArr: any) {

        this.model.totalJournalEntryCredit = parseFloat(this.model.totalJournalEntryCredit.toString()) - parseFloat(tempArr.creditAmount.toString());
        this.model.totalJournalEntryDebit = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(tempArr.debitAmount.toString());
        // this.amtDifference=parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString())
        //this.model.totalJournalEntryDebit = newTotalCreditAmmount;

        this.amtDifference = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());
        if (this.model.totalJournalEntryDebit >= this.model.totalJournalEntryCredit) {
            this.amtDifference = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());
        }
        else {
            this.amtDifference = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());
        }

        this.amtDifference = this.DecimalFormater(this.amtDifference);

        //    this.model.totalJournalEntryCredit = this.DecimalFormater(this.model.totalJournalEntryCredit);
        //    this.model.totalJournalEntryDebit = this.DecimalFormater( this.model.totalJournalEntryDebit);
        this.arrjournalEntryDetailsList.splice(RowIndex, 1);

        if (parseInt(this.amtDifference.toString()) > 0) {
            this.isPostAllowed = true;
        }
        else {
            this.isPostAllowed = false;
        }
    }

    SaveUpdateJournalSetup(f: NgForm, myModal) {
        this.isPostEvt = false;
        if (f.valid && this.activeCurrency[0].id != '0' && this.activeAuditTrial[0].id != '0' && this.arrjournalEntryDetailsList.length > 0 && this.activeExchangeDetail[0].id != '0') {
            this.btndisabled = true;
            this.model.journalEntryDetailsList = this.arrjournalEntryDetailsList;
            this.model.currencyID = this.activeCurrency[0].id;
            this.model.sourceDocumentId = this.activeAuditTrial[0].id;
            this.model.glBatchId = this.activeBatch[0].id;
            this.model.exchangeTableIndex = this.activeExchangeDetail[0].id;
            if (this.isCorrectJournalEntry) {
                this.model.correction = true;
            }
            else {
                this.model.correction = false;
            }

            if (this.model.transactionDate.formatted == undefined) {
                var transactionDate = this.model.transactionDate;
                if (transactionDate.date != undefined) {
                    this.model.transactionDate = transactionDate.date.day + '/' + transactionDate.date.month + '/' + transactionDate.date.year;
                }
            }
            else {
                this.model.transactionDate = this.model.transactionDate.formatted;
            }


            if (this.model.transactionReversingDate.formatted == undefined) {
                var transactionReversingDate = this.model.transactionReversingDate;
                if (transactionReversingDate.date != undefined) {
                    this.model.transactionReversingDate = transactionReversingDate.date.day + '/' + transactionReversingDate.date.month + '/' + transactionReversingDate.date.year;
                }
            }
            else {
                this.model.transactionReversingDate = this.model.transactionReversingDate.formatted;
            }


            this.model.transactionSource = "JV";
            this.loaderImg = true;
            window.scrollTo(0,0);
            this.journalEntryService.SaveUpdateJournalSetup(this.model, this.isModify).then(data => {
                window.scrollTo(0,0);
                this.btndisabled = false;
                window.scrollTo(0, 0);
                var datacode = data.code;
                if (datacode == 201 || datacode == 200) {

                    this.setPageUnPosted({ offset: 0 });
                    this.setPagePosted({ offset: 0 });
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.isModify = false;
                    this.showBtns = false;
                    this.loaderImg = false;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.bindSelectedItem(this.emptyArray[0], 'JournalEntry');
                    this.getCurrencySetupList();

                }
                else {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.loaderImg = false;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    
                }
                f.resetForm();
                this.clear(f);
                this.getJournalIdList();
                this.loaderImg = false;

            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"', '').replace('"', '');
                }, 100)
            });
        }
        else {

            if (this.activeExchangeDetail[0].id == '0') {
                myModal.open();
            }
        }
        this.btndisabled = false;
    }


    DeleteJournalEntry(f: NgForm) {

        if (!this.model.journalID) {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.defaultAddFormValues[0]['listDtoFieldValidationMessage'][0]['validationMessage'];
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);

        }
        else {
            this.journalEntryService.DeleteJournalEntry(this.model.journalID).then(data => {
                window.scrollTo(0, 0);
                this.closeModal();
                if (data.btiMessage.messageShort = "RECORD_DELETED_SUCCESSFULLY") {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.isModify = false;
                    this.showBtns = false;
                    this.bindSelectedItem(this.emptyArray[0], 'JournalEntry');
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                this.clear(f);
                this.getJournalIdList();

            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"', '').replace('"', '');
                }, 100)
            });
        }
    }
    ConvertDecimal(x) {
        return Number.parseFloat(x).toFixed(2);
    }
    CopyJournalEntrySetup(f2: NgForm, copyModal) {

        if (f2.valid && this.activeJVEntryNumber[0].id != '0' && this.activeYears[0].id != '0') {
            this.journalEntryService.CopyJournalEntry(this.activeJVEntryNumber[0].id, this.activeYears[0].id).then(data => {

                copyModal.close();
                this.clearCopyModal(f2);
                if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {

                    this.getGLNextJournalEntry();
                    this.isCopyJournalEntry = true;
                    this.isModify = false;
                    this.isCorrectJournalEntry = false;
                    this.isPostEvt = false;
                    this.model = data.result;
                    this.companyVisiblity();
                    this.model.originalJournalID = data.result.journalID;
                    this.arrjournalEntryDetailsList = data.result.journalEntryDetailsList;
                    this.model.journalEntryDetailsList = data.result.journalEntryDetailsList;
                    this.model.transactionType = this.model.transactionType.toString();
                    if (this.model.transactionType == "1") {
                        this.isReversing = 0;
                        if (data.result.transactionDate) {
                            this.model.transactionDate = data.result.transactionDate;
                        }

                        var transactionDate = this.model.transactionDate;
                        var transactionDateData = transactionDate.split('/');
                        this.model.transactionDate = { "date": { "year": parseInt(transactionDateData[2]), "month": parseInt(transactionDateData[1]), "day": parseInt(transactionDateData[0]) } };
                        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
                    }
                    else {
                        this.isReversing = 1
                        if (data.result.transactionDate) {
                            this.model.transactionDate = data.result.transactionDate;
                        }

                        var transactionDate = this.model.transactionDate;
                        var transactionDateData = transactionDate.split('/');
                        this.model.transactionDate = { "date": { "year": parseInt(transactionDateData[2]), "month": parseInt(transactionDateData[1]), "day": parseInt(transactionDateData[0]) } };

                        if (data.result.transactionReversingDate) {
                            this.model.transactionReversingDate = data.result.transactionReversingDate;
                        }

                        if (data.result.transactionReversingDate != '') {
                            var transactionReversingDate = this.model.transactionReversingDate;
                            var transactionReversingDateData = transactionReversingDate.split('/');
                            this.model.transactionReversingDate = { "date": { "year": parseInt(transactionReversingDateData[2]), "month": parseInt(transactionReversingDateData[1]), "day": parseInt(transactionReversingDateData[0]) } };
                        }

                    }

                    if (data.result.glBatchId) {
                        var selectedbatch = this.arrBatchList.find(x => x.id == data.result.glBatchId);
                        this.bindSelectedItem(selectedbatch, 'Batch');
                    }
                    else {
                        this.bindSelectedItem(this.emptyArray[0], 'Batch');
                    }

                    var sourceDocument = this.arrAuditTrial.find(x => x.id == data.result.sourceDocumentId);
                    this.bindSelectedItem(sourceDocument, 'AuditTrial');

                    var sourceCurrency = this.arrCurrency.find(x => x.id == data.result.currencyID);
                    this.bindSelectedItem(sourceCurrency, 'Currency');

                    window.setTimeout(() => {
                        var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id == data.result.exchangeTableIndex);
                        this.bindSelectedItem(selectedExchangeDetail, 'ExchangeDetail');
                    }, 2000);

                    this.arrjournalEntryDetailsList = [];
                    for (var i = 0; i < data.result.journalEntryDetailsList.length; i++) {

                        var selectedCompany = this.arrCompany.find(x => x.id.split('*')[0] == data.result.journalEntryDetailsList[i].intercompanyId);
                        if (selectedCompany) {
                            this.arrjournalEntryDetailsList.push({
                                "accountTableRowIndex": data.result.journalEntryDetailsList[i].accountTableRowIndex,
                                'accountNumber': data.result.journalEntryDetailsList[i].accountNumber,
                                "intercompanyId": data.result.journalEntryDetailsList[i].intercompanyId,
                                "interCompanyName": selectedCompany.text,
                                "debitAmount": data.result.journalEntryDetailsList[i].debitAmount,
                                "creditAmount": data.result.journalEntryDetailsList[i].creditAmount,
                                "distributionDescription": data.result.journalEntryDetailsList[i].distributionDescription,
                                "isCredit": data.result.journalEntryDetailsList[i].isCredit
                            }
                            );
                        }
                        else {
                            this.arrjournalEntryDetailsList.push({
                                "accountTableRowIndex": data.result.journalEntryDetailsList[i].accountTableRowIndex,
                                'accountNumber': data.result.journalEntryDetailsList[i].accountNumber,
                                "intercompanyId": '',
                                "interCompanyName": '',
                                "debitAmount": data.result.journalEntryDetailsList[i].debitAmount,
                                "creditAmount": data.result.journalEntryDetailsList[i].creditAmount,
                                "distributionDescription": data.result.journalEntryDetailsList[i].distributionDescription,
                                "isCredit": data.result.journalEntryDetailsList[i].isCredit
                            }
                            );
                        }

                        this.debitAmount = data.result.journalEntryDetailsList[i].debitAmount;
                        if (!this.debitAmount) {
                            this.debitAmount = 0;
                        }
                        this.creditAmount = data.result.journalEntryDetailsList[i].creditAmount;
                        if (!this.creditAmount) {
                            this.creditAmount = 0;
                        }
                        if (!this.model.totalJournalEntryCredit) {
                            this.model.totalJournalEntryCredit = 0;
                        }
                        if (!this.model.totalJournalEntryDebit) {
                            this.model.totalJournalEntryDebit = 0;
                        }

                        // this.model.totalJournalEntryCredit = parseInt(this.model.totalJournalEntryCredit.toString()) +   parseInt(this.creditAmount.toString());
                        // this.model.totalJournalEntryDebit = parseInt(this.model.totalJournalEntryDebit.toString()) +   parseInt(this.debitAmount.toString());
                        // this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString());

                        this.amtDifference = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());

                        this.amtDifference = this.DecimalFormater(this.amtDifference);
                        // var s = this.ConvertDecimal(this.amtDifference);
                        // if(this.model.totalJournalEntryDebit >= this.model.totalJournalEntryCredit)
                        // {
                        //     this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString());
                        // }
                        // else 
                        // {
                        //     this.amtDifference=parseInt(this.model.totalJournalEntryCredit.toString()) - parseInt(this.model.totalJournalEntryDebit.toString());
                        // }

                        this.debitAmount = 0;
                        this.creditAmount = 0;

                        if (this.amtDifference == 0) {
                            this.isPostAllowed = false;
                        }
                        else {
                            this.isPostAllowed = true;
                        }
                    }
                }

                f2.resetForm();
                this.companyVisiblity();
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"', '').replace('"', '');
                }, 100)
            });
        }
    }

    SaveCorrectJournalEntrySetup(f3: NgForm, correctJournalEntry) {

        if (f3.valid && this.activeCorrectJVEntryNumber[0].id != '0' && this.activeCorrectYears[0].id != '0' && this.activeAction[0].id != '0') {
            this.journalEntryService.savecorrectJournalEntry(this.activeCorrectJVEntryNumber[0].id, this.activeCorrectYears[0].id, this.activeAction[0].id).then(data => {
                correctJournalEntry.close();
                this.clearCopyModal(f3);
                if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {

                    this.isModify = false;
                    this.isCorrectJournalEntry = true;
                    this.isCopyJournalEntry = false;
                    this.isPostEvt = false;
                    this.model = data.result;
                    this.companyVisiblity();
                    this.getGLNextJournalEntry();
                    this.model.originalJournalID = data.result.journalID;
                    this.arrjournalEntryDetailsList = data.result.journalEntryDetailsList;
                    this.model.journalEntryDetailsList = data.result.journalEntryDetailsList;
                    this.model.transactionType = this.model.transactionType.toString();
                    if (this.model.transactionType == "1") {
                        this.isReversing = 0;
                        if (data.result.transactionDate) {
                            this.model.transactionDate = data.result.transactionDate;
                        }

                        var transactionDate = this.model.transactionDate;
                        var transactionDateData = transactionDate.split('/');
                        this.model.transactionDate = { "date": { "year": parseInt(transactionDateData[2]), "month": parseInt(transactionDateData[1]), "day": parseInt(transactionDateData[0]) } };
                        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
                    }
                    else {
                        this.isReversing = 1
                        if (data.result.transactionDate) {
                            this.model.transactionDate = data.result.transactionDate;
                        }

                        var transactionDate = this.model.transactionDate;
                        var transactionDateData = transactionDate.split('/');
                        this.model.transactionDate = { "date": { "year": parseInt(transactionDateData[2]), "month": parseInt(transactionDateData[1]), "day": parseInt(transactionDateData[0]) } };

                        if (data.result.transactionReversingDate) {
                            this.model.transactionReversingDate = data.result.transactionReversingDate;
                        }

                        if (data.result.transactionReversingDate != '') {
                            var transactionReversingDate = this.model.transactionReversingDate;
                            var transactionReversingDateData = transactionReversingDate.split('/');
                            this.model.transactionReversingDate = { "date": { "year": parseInt(transactionReversingDateData[2]), "month": parseInt(transactionReversingDateData[1]), "day": parseInt(transactionReversingDateData[0]) } };
                        }
                    }

                    if (data.result.glBatchId) {
                        var selectedbatch = this.arrBatchList.find(x => x.id == data.result.glBatchId);
                        this.bindSelectedItem(selectedbatch, 'Batch');
                    }
                    else {
                        this.bindSelectedItem(this.emptyArray[0], 'Batch');
                    }

                    var sourceDocument = this.arrAuditTrial.find(x => x.id == data.result.sourceDocumentId);
                    this.bindSelectedItem(sourceDocument, 'AuditTrial');

                    var sourceCurrency = this.arrCurrency.find(x => x.id == data.result.currencyID);
                    this.bindSelectedItem(sourceCurrency, 'Currency');


                    window.setTimeout(() => {

                        var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id == data.result.exchangeTableIndex);
                        this.bindSelectedItem(selectedExchangeDetail, 'ExchangeDetail');
                    }, 2000);

                    this.arrjournalEntryDetailsList = [];
                    for (var i = 0; i < data.result.journalEntryDetailsList.length; i++) {

                        var selectedCompany = this.arrCompany.find(x => x.id.split('*')[0] == data.result.journalEntryDetailsList[i].intercompanyId);
                        if (selectedCompany) {
                            this.arrjournalEntryDetailsList.push({
                                "accountTableRowIndex": data.result.journalEntryDetailsList[i].accountTableRowIndex,
                                'accountNumber': data.result.journalEntryDetailsList[i].accountNumber,
                                "intercompanyId": data.result.journalEntryDetailsList[i].intercompanyId,
                                "interCompanyName": selectedCompany.text,
                                "debitAmount": data.result.journalEntryDetailsList[i].debitAmount,
                                "creditAmount": data.result.journalEntryDetailsList[i].creditAmount,
                                "distributionDescription": data.result.journalEntryDetailsList[i].distributionDescription,
                                "isCredit": data.result.journalEntryDetailsList[i].isCredit
                            }
                            );
                        }
                        else {
                            this.arrjournalEntryDetailsList.push({
                                "accountTableRowIndex": data.result.journalEntryDetailsList[i].accountTableRowIndex,
                                'accountNumber': data.result.journalEntryDetailsList[i].accountNumber,
                                "intercompanyId": '',
                                "interCompanyName": '',
                                "debitAmount": data.result.journalEntryDetailsList[i].debitAmount,
                                "creditAmount": data.result.journalEntryDetailsList[i].creditAmount,
                                "distributionDescription": data.result.journalEntryDetailsList[i].distributionDescription,
                                "isCredit": data.result.journalEntryDetailsList[i].isCredit
                            }
                            );
                        }

                        this.debitAmount = data.result.journalEntryDetailsList[i].debitAmount;
                        if (!this.debitAmount) {
                            this.debitAmount = 0;
                        }
                        this.creditAmount = data.result.journalEntryDetailsList[i].creditAmount;
                        if (!this.creditAmount) {
                            this.creditAmount = 0;
                        }
                        if (!this.model.totalJournalEntryCredit) {
                            this.model.totalJournalEntryCredit = 0;
                        }
                        if (!this.model.totalJournalEntryDebit) {
                            this.model.totalJournalEntryDebit = 0;
                        }

                        this.amtDifference = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());
                        this.amtDifference = this.DecimalFormater(this.amtDifference);
                        // if(this.model.totalJournalEntryDebit >= this.model.totalJournalEntryCredit)
                        // {
                        //     this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString());
                        // }
                        // else 
                        // {
                        //     this.amtDifference=parseInt(this.model.totalJournalEntryCredit.toString()) - parseInt(this.model.totalJournalEntryDebit.toString());
                        // }

                        this.debitAmount = 0;
                        this.creditAmount = 0;

                        if (this.amtDifference == 0) {
                            this.isPostAllowed = false;
                        }
                        else {
                            this.isPostAllowed = true;
                        }
                    }
                }
                correctJournalEntry.close();
                f3.resetForm();
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"', '').replace('"', '');
                }, 100)
            });
        }

    }
    PostJournalSetup(f: NgForm, myModal) {

        this.isPostEvt = true;
        if (f.valid && (parseFloat(this.amtDifference.toString()) > 0 || parseFloat(this.amtDifference.toString()) < 0)) {
            this.btndisabled = true;
            window.scrollTo(0, 0);
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.btndisabled = false;
            this.messageText = this.defaultAddFormValues[22]['listDtoFieldValidationMessage'][1]['validationMessage'];
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
        }
        else if (f.valid && this.activeBatch[0].id != '0') {
            window.scrollTo(0, 0);
            this.btndisabled = false;
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.defaultAddFormValues[22]['listDtoFieldValidationMessage'][0]['validationMessage'];
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
        }
        else {
            if (f.valid && this.activeCurrency[0].id != '0' && this.activeAuditTrial[0].id != '0' && this.arrjournalEntryDetailsList.length > 0 && this.activeExchangeDetail[0].id != '0') {
                this.btndisabled = true;
                if (this.isCorrectJournalEntry) {
                    this.model.correction = true;
                }
                else {
                    this.model.correction = false;
                }
                this.isCopyJournalEntry = false;

                this.isCorrectJournalEntry = false;
                var journalID = this.model.journalID;
                var originalJournalID = this.model.originalJournalID;
                //    this.model.originalJournalID=this.model.journalID;
                //    this.getGLNextJournalEntry();
                this.model.journalEntryDetailsList = this.arrjournalEntryDetailsList;
                this.model.activeCurrency = this.activeCurrency[0].id;
                this.model.sourceDocumentId = this.activeAuditTrial[0].id;
                this.model.exchangeTableIndex = this.activeExchangeDetail[0].id;
                //this.model.glBatchId=this.activeBatch[0].id;
                this.model.glBatchId = '';

                if (this.model.transactionDate.formatted == undefined) {
                    var transactionDate = this.model.transactionDate;
                    if (transactionDate.date != undefined) {
                        this.model.transactionDate = transactionDate.date.day + '/' + transactionDate.date.month + '/' + transactionDate.date.year;
                    }
                }
                else {
                    this.model.transactionDate = this.model.transactionDate.formatted;
                }

                if (this.model.transactionReversingDate.formatted == undefined) {
                    var transactionReversingDate = this.model.transactionReversingDate;
                    if (transactionReversingDate.date != undefined) {
                        this.model.transactionReversingDate = transactionReversingDate.date.day + '/' + transactionReversingDate.date.month + '/' + transactionReversingDate.date.year;
                    }
                }
                else {
                    this.model.transactionReversingDate = this.model.transactionReversingDate.formatted;
                }
                this.loaderImg = true;
                window.scroll(0,0);
                window.setTimeout(() => {
                    
                    this.journalEntryService.SaveUpdateJournalSetup(this.model, this.isModify).then(data => {
                        this.btndisabled = false;
                        window.scrollTo(0, 0);
                        var datacode = data.code;
                        if (datacode == 201) {
                            this.journalEntryService.PostJournalSetup(this.model).then(data => {
                                this.btndisabled = false;
                                window.scrollTo(0, 0);
                                var datacode = data.code;
                                if (datacode == 201) {
                                    this.isSuccessMsg = true;
                                    this.isfailureMsg = false;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = data.btiMessage.message;
                                    this.isModify = false;
                                    this.showBtns = false;
                                    this.loaderImg = false;
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.hasMsg = false;
                                    }, 4000);
                                }
                                else {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = data.btiMessage.message;
                                    this.loaderImg = false;
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.hasMsg = false;
                                    }, 4000);
                                }
                                f.resetForm();
                                this.bindSelectedItem(this.emptyArray[0], 'JournalEntry');
                                this.clear(f);
                                this.isPostEvt = false;
                                this.isCopyJournalEntry = false;
                                this.isCorrectJournalEntry = false;
                                this.getJournalIdList();
                                
                            }).then(data => {
                                this.setPageUnPosted({ offset: 0 });
                                this.setPagePosted({ offset: 0 });
                                this.loaderImg = false;
                                this.getCurrencySetupList();
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"', '').replace('"', '');
                                    this.loaderImg = false;
                                }, 100)
                            });
                        }
                    });

                }, 1000);
            }
            else {
                this.loaderImg = false;
                if (this.activeExchangeDetail[0].id == '0') {
                    myModal.open();
                }
            }
        }
        this.btndisabled = false;
    }

    private value: any = {};
    isIntercompanyChange(event) {
    }


    bindEditAccountNumber(companyId) {

        console.log(companyId);
        if (companyId == 0) {
            this.arrEditAccountNumber = [{ 'id': '0', 'text': this.select }];

            this.commonService.getGlAccountNumberListNew().then(data => {

                if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                    this.arrEditAccountNumber = [{ 'id': '0', 'text': this.select }];
                    for (var i = 0; i < data.result.length; i++) {

                        this.arrEditAccountNumber.push({
                            'id': data.result[i].accountTableRowIndex, 'text':

                                data.result[i].accountNumber /*+ '<*>' + (data.result[i].description ? data.result[i].description : '')
                                + '<*>' + (data.result[i].descriptionArabic ? data.result[i].descriptionArabic : '')*/
                        });
                    }
                }
            }, error => { });
        }
        else {
            this.arrEditAccountNumber = [{ 'id': '0', 'text': this.select }];

            companyId = companyId.split('*');
            this.commonService.getGlAccountNumberListByCompanyNew(companyId[1]).then(data => {

                this.arrEditAccountNumber = [{ 'id': '0', 'text': this.select }];

                if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                    for (var i = 0; i < data.result.length; i++) {
                        this.arrEditAccountNumber.push({
                            'id': data.result[i].accountTableRowIndex, 'text':
                                data.result[i].accountNumber /*+ '<*> ' + (data.result[i].description ? data.result[i].description : '<*> ')
                                + '<*>  ' + (data.result[i].descriptionArabic ? data.result[i].descriptionArabic : '')*/
                        });
                    }
                }
            });
        }
    }


    bindAccountNumber(companyId) {

        if (companyId == 0) {
            this.arrAccountNumber = [{ 'id': '0', 'text': this.select }];
            this.commonService.getGlAccountNumberListNew().then(data => {
                this.arrAccountNumber = [{ 'id': '0', 'text': this.select }];

                if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                    for (var i = 0; i < data.result.length; i++) {
                        // this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                        this.arrAccountNumber.push({
                            'id': data.result[i].accountTableRowIndex, 'text':

                                (data.result[i].accountNumber ? data.result[i].accountNumber : '') /*+ '<*>' + (data.result[i].accountDescription ? data.result[i].accountDescription : '')
                                + '<*>' + (data.result[i].accountDescriptionArabic ? data.result[i].accountDescriptionArabic : '')*/
                        });


                    }
                }
            });
        }
        else {
            this.arrAccountNumber = [{ 'id': '0', 'text': this.select }];
            companyId = companyId.split('*');

            this.commonService.getGlAccountNumberListByCompanyNew(companyId[1]).then(data => {
                this.arrAccountNumber = [{ 'id': '0', 'text': this.select }];


                if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                    for (var i = 0; i < data.result.length; i++) {
                        // this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                        this.arrAccountNumber.push({
                            'id': data.result[i].accountTableRowIndex, 'text':
                                (data.result[i].accountNumber ? data.result[i].accountNumber : '') /*+ '<*>' + (data.result[i].accountDescription ? data.result[i].accountDescription : '')
                                + '<*>' + (data.result[i].accountDescriptionArabic ? data.result[i].accountDescriptionArabic : '')*/
                        });

                    }
                }
            });
        }

    }

    getGLNextJournalEntry() {
        this.model.journalID = '9999';
        console.log(this.model.journalID);
        /*             this.commonService.getGLNextJournalEntry().then(data => {
                       //this.nextJournalEntry=data.result.nextJournalEntry;
                       this.model.journalID=data.result.nextJournalEntry;
                    }); */
    }

    getJournalIdList() {
        this.journalEntryService.getJournalIdList().then(data => {
            this.arrJournalEntry = [{ 'id': '0', 'text': this.select }];

            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                for (var i = 0; i < data.result.length; i++) {
                    this.arrJournalEntry.push({ 'id': data.result[i].journalID, 'text': data.result[i].journalID });
                }
            }
        });
    }

    varifyDelete() {
        if (this.unPostedselected.length > 0) {
            this.isGroupAction = true;
            this.isDeleteAction = false;
        } else {
            this.isDeleteAction = true;
            this.isGroupAction = false;
        }

        this.confirmationModalBody = this.deleteConfirmationText;
        this.isConfirmationModalOpen = true;
    }



    public bindSelectedItem(value: any, type: string,batchId?): void {
        if (type == 'Batch') {
            this.value = value;
            if (value.text) {
                this.model.batchID = this.activeBatch[0].id;
                this.activeBatch = [value];
            }
        }
        else if (type == 'AuditTrial') {
            this.value = value;
            if (value.text) {
                this.activeAuditTrial = [value];
                this.getBatchListByPaymentMethod(value.id,batchId);
            }
        }
        else if (type == 'Currency') {

            this.value = value;

            if (value.text)
            // {
            //     this.activeCurrency=[value];
            //     this.model.currencyID=this.activeCurrency[0].id;

            //     this.GetExchangeTableSetupByCurrencyId(value.id);
            // }
            {
                this.activeCurrency = [value];
                this.model.currencyID = this.activeCurrency[0].id;
                this.activeExchangeDetail = [this.emptyArray[0]];
                this.arrExchangeDetail = [];
                this.model.exchangeRate = '';
                this.model.exchangeExpirationDate = '';
                if (value.id != "0") {
                    this.GetExchangeTableSetupByCurrencyId(value.id);
                }
            }
        }
        else if (type == 'JournalEntry') {
            this.value = value;
            if (value.text) {
                this.activeJournalEntry = [value];

                this.GetJournalEntryByJournalId(value.id)

            }
        }
        else if (type == 'Company') {
            console.log(value);
            this.activeAccountNumber = this.emptyArray;
            this.accountDescirptionDisplay = '';
            this.value = value;
            if (value.id) {
                this.intercompany = value;
                this.bindAccountNumber(value.id);
            }
            else {
                this.arrAccountNumber = [];
            }
            if (value.text) {

                this.activeCompany = [value];
            }
        }
        else if (type == 'AccountNumber') {
            console.log(value);
            this.accountTableRow = value;
            this.value = value;
            if (value.text) {
                this.activeAccountNumber = [value];

            }

            // alert('value: ' + value);
            // alert('this.activeAccountNumber: ' + this.activeAccountNumber[0].id);

            if (this.activeAccountNumber[0].text != this.select) {

                var actNumber = this.activeAccountNumber[0].text.split("<*>");

                // this.accountDescirptionDisplay = this.activeAccountNumber[0].text;
                this.accountDescirptionDisplay = this.activeAccountNumber[0].text;

                ///this.activeAccountNumber = [{ 'id': this.activeAccountNumber[0].id, 'text': this.select }];
            } else {
                this.accountDescirptionDisplay = '';
            }
            console.log(JSON.stringify(this.activeAccountNumber));

            var accountTableRowIndex = '';
            accountTableRowIndex = this.activeAccountNumber[0].id;
            console.log(accountTableRowIndex);
            var companyId = this.activeCompany[0].id.split('*');

            this.commonService.getGlAccountNumberDetailByDynamicCompanyId(accountTableRowIndex, companyId[1]).then(data => {

                if (data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST') {
                    this.typicalBalanceType = data.result.typicalBalanceType;
                    this.txtDebitAmount = <HTMLInputElement>document.getElementById("txtDebitAmount");
                    this.txtCreditAmount = <HTMLInputElement>document.getElementById("txtCreditAmount");
                    this.txtDistributionDescription = <HTMLInputElement> document.getElementById("txtDistributionDescription");
                    this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
                        if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                            this.accountDescription = data.result.accountDescription;
                        }
                        else {
                            this.accountDescription = data.btiMessage.message;
                        }


                    });
                    this.txtDistributionDescription.focus();
//                    if (this.typicalBalanceType == "Debit") {
//                        this.txtDebitAmount.focus();
//                    }
//                    else {
//                        this.txtCreditAmount.focus();
//                    }
                }
                else if (data.btiMessage.messageShort == 'MAIN_ACCOUNT_TRANSACTION_NOT_ALLOWED') {
                    this.activeAccountNumber = [{ 'id': '0', 'text': this.select }];
                    this.accountDescirptionDisplay = '';
                    this.accountDescription = '';
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            });
        }
        else if (type == 'YearsList') {

            this.value = value;
            if (value.id) {
                this.activeYears = [value];
                this.GetJournalEntryByYear(value.id);
            }
        }
        else if (type == 'JVEntryNumber') {
            this.value = value;
            if (value.id) {
                this.activeJVEntryNumber = [value];
                // this.GetJournalEntryByYear(value.id);  
            }
        }
        else if (type == 'ActionList') {

            this.value = value;
            if (value.id) {
                this.activeAction = [value];
                // this.GetCorrectJournalEntryIdByYear(value.id);  
            }
        }
        else if (type == 'CorrectYearsList') {

            this.value = value;
            if (value.id) {
                this.activeCorrectYears = [value];
                this.GetCorrectJournalEntryIdByYear(value.id);
            }
        }
        else if (type == 'CorrectJVEntryNumber') {
            this.value = value;
            if (value.id) {
                this.activeCorrectJVEntryNumber = [value];
            }
        }
        else if (type == 'ExchangeDetail') {

            this.value = value;
            if (value.id) {
                this.activeExchangeDetail = [value];
                this.GetCurrencyExchangeDetail(value.text);
            }
        } else if (type == "EditCompany") {
            this.activeEditAccountNumber = this.emptyArray;
            this.editAccountDescriptionDisplay = '';
            this.value = value;
            if (value.id) {
                this.intereditcompany = value;

                this.bindEditAccountNumber(value.id);
            }
            else {
                this.arrEditAccountNumber = [];
            }
            if (value.text) {
                this.activeEditCompany = [value];
            }
            console.log(this.activeEditCompany);
        } else if (type == "EditAccountNumber") {
            this.accountTableRowEdit = value;
            this.value = value;
            if (value.text) {
                this.activeEditAccountNumber = [value];

            }

            if (this.activeEditAccountNumber[0].text != this.select) {

                var actNumber = this.activeEditAccountNumber[0].text.split(":");

                // this.accountDescirptionDisplay = this.activeAccountNumber[0].text;
                this.editAccountDescriptionDisplay = actNumber[0];

                this.activeEditAccountNumber = [{ 'id': this.activeEditAccountNumber[0].id, 'text': this.select }];
            } else {
                this.editAccountDescriptionDisplay = '';
            }
            console.log(JSON.stringify(this.activeEditAccountNumber));

            var accountTableRowIndex = '';
            accountTableRowIndex = this.activeEditAccountNumber[0].id;
            console.log(accountTableRowIndex);
            console.log(this.activeEditAccountNumber)



            var companyId = this.activeEditCompany[0].id.split('*');
            this.commonService.getGlAccountNumberDetailByDynamicCompanyId(accountTableRowIndex, companyId[1]).then(data => {

                if (data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST') {
                    this.typicalBalanceType = data.result.typicalBalanceType;
                    this.txtDebitAmount = <HTMLInputElement>document.getElementById("txtDebitAmount");
                    this.txtCreditAmount = <HTMLInputElement>document.getElementById("txtCreditAmount");
                    this.accountDescriptionEdit = data.result.accountDescription;
                    if (this.typicalBalanceType == "Debit") {
                        this.txtDebitAmount.focus();
                    }
                    else {
                        this.txtCreditAmount.focus();
                    }
                }
                else if (data.btiMessage.messageShort == 'MAIN_ACCOUNT_TRANSACTION_NOT_ALLOWED') {
                    this.activeEditAccountNumber = [{ 'id': '0', 'text': this.select }];
                    this.editAccountDescriptionDisplay = '';
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            });
        }
    }

    clear(f) {
        this.IsAddJournalEntryDetails = false;
        this.companyVisiblity();
        if (!this.isModify) {
            this.model.journalID = '';
            this.getGLNextJournalEntry();
        }
        else {
            this.model.journalID = this.activeJournalEntry[0].id;
        }
        this.isPostEvt = false;
        this.isReversing = 0;
        this.amtDifference = 0;
        this.isCopyJournalEntry = false;
        this.isCorrectJournalEntry = false;
        this.model.interCompany = true;
        this.model.journalDescription = '';
        this.model.journalDescriptionArabic = '';
        this.model.transactionType = "1";
        this.arrjournalEntryDetailsList = [];
        this.bindSelectedItem(this.emptyArray[0], 'Batch');
        this.bindSelectedItem(this.emptyArray[0], 'AuditTrial');
        this.bindSelectedItem(this.emptyArray[0], 'Currency');
        this.bindSelectedItem(this.emptyArray[0], 'ExchangeDetail');
        this.model.exchangeRate = '';
        this.model.exchangeExpirationDate = '';
        this.debitAmount = 0;
        this.creditAmount = 0;
        this.model.totalJournalEntryCredit = 0;
        this.model.totalJournalEntryDebit = 0;
        this.amtDifference = 0;
        this.model.transactionDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.model.transactionReversingDate = { "date": { "year": this.today.getFullYear(), "month": this.today.getMonth() + 1, "day": this.today.getDate() } };
        this.clearCopyModal(f);
        this.clearCorrectModal(f);

        var chkInterCompany = <HTMLInputElement>document.getElementById("chkInterCompany")
        if (this.userData.companyCount > 1) {
            this.isIntercompany = false;
            chkInterCompany.checked = true;
        }
        else {
            this.model.interCompany = false;
            chkInterCompany.checked = false;
        }
    }

    Cancel(f) {
        this.clear(f);
        f.resetForm();
        this.isModify = false;
        this.isPostEvt = false;
        this.bindSelectedItem(this.emptyArray[0], 'JournalEntry');
        this.getGLNextJournalEntry();

    }

    clearCopyModal(f) {
        this.bindSelectedItem(this.emptyArray[0], 'YearsList')
        this.bindSelectedItem(this.emptyArray[0], 'JVEntryNumber')
    }
    clearCorrectModal(f) {

        this.bindSelectedItem(this.emptyArray[0], 'ActionList')
        this.bindSelectedItem(this.emptyArray[0], 'CorrectYearsList')
        this.bindSelectedItem(this.emptyArray[0], 'CorrectJVEntryNumber')
    }


    getGlAccountNumberDetail(myModal) {
        var accountTableRowIndex = '';
        accountTableRowIndex = this.activeAccountNumber[0].id
        this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                this.accountDescription = data.result.accountDescription;
            }
            else {
                this.accountDescription = data.btiMessage.message;
            }
            myModal.open();

        });
    }

    GetReport(journalID: string) {

        this.journalEntryService.GetReport(journalID).then(data => {
            if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                this.arrjournalEntryReport = data.result;
            }
        });
    }


    // print()
    // {
    //     window.print();
    // }
    print(): void {
        let printContents, popupWin;
        printContents = document.getElementById('journalEntryReportInner').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
            <html>
                <head>
                <title>Print tab</title>
                <style>
                //........Customized style.......
                </style>
                </head>
            <body onload="window.print();window.close()">${printContents}</body>
            </html>`
        );
        popupWin.document.close();
    }

    printPosted(): void {
        let printContents, popupWin;
        printContents = document.getElementById('journalEntryReportPosted').innerHTML;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
        <html>
            <head>
            <title>Print Posted Journal </title>
            </head>
        <body onload="window.print();window.close()">
            <div >
            <img src="../../../../../../assets/img/logo.png" >
            </div>
            ${printContents}
            
        </body>
        </html>`
        );
        popupWin.document.close();
    }
    openModel(myModal, action, f: NgForm) {

        if (action == 'journalEntryReport') {
            this.GetReport(this.model.journalID)
        }
        if (action == 'correctJournalEntry') {
            this.clearCorrectModal(f);
        }
        if (action == 'copyJournalEntry') {
            this.clearCopyModal(f);
        }
        myModal.open();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
        this.isModalOpen = false;
        this.isGroupAction = false;
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }


    DecimalFormater(amt) {
        if (!Number.isInteger(amt)) {
            var amt = amt.toString().split(".")
            if (amt.length > 1 && amt[1].length >= 3) {
                amt = amt[0] + "." + amt[1].slice(0, 2)
            }
            else {
                amt = amt[0] + "." + amt[1];
            }
        }
        return amt;
    }


    openAccountWindow() {
        this.accountNumberIndex = [];
        this.arrAccountDescription = [];
        this.arrAccountDescriptionArabic = [];
        this.isModalOpen = true;
    }

    closeModalWindow(model) {
        model.close();
    }

    getSegmentCount() {
        this.accountStructureService.getAccountStructureSetup().then(data => {
            this.arrSegment = [];
            for (var i = 0; i < data.result.segmentNumber; i++) {
                this.arrSegment.push({ 'segmentNumber': i, 'isReadOnly': "true" });
            }
        });
    }

    createNewGlAccountNumber() {
        var tempAccountNumberList = [];
        var accountDesc = this.arrAccountDescription.join("-");
        var accounDescArabic = this.arrAccountDescriptionArabic.join("-");
        var accounNumber = this.accountNumber.join(" - ");
        console.log(this.accountNumber);
        //accountDesc=accountDesc.replace(/,/g , " ");
        //accountDesc=accountDesc.replace("0"," ");
        tempAccountNumberList.push({ 'accountNumber': accounNumber, 'accountNumberIndex': this.accountNumberIndex, 'accountDescription': accountDesc, 'accountDescriptionArabic': accounDescArabic })
        this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {

            if (data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED') {
                this.toastr.success(data.btiMessage.message);
                // this.getAccountNumber();
                this.bindAccountNumber(this.intercompany.id)
                this.bindEditAccountNumber(this.intercompany.id);
                this.closeModal();
            }
            else {
                this.toastr.warning(data.btiMessage.message);
            }
        });

    }

    refreshAccountNumber(event) {
        console.log(event);
        if (event) {
            this.bindAccountNumber(this.intercompany.id);
            this.bindAccountNumber(0);
            this.bindEditAccountNumber(0);
        }
    }

    fetchFirstAccountIndexInfo(segmentIndex) {
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_' + segmentIndex);
        var segmentNumber = segment.value;

        if (this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber) {
            return false;
        }

        if (segmentNumber) {
            this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {

                let status = pagedData.status;
                let result = pagedData.result;
                if (status == "FOUND") {
                    if (this.accountNumberIndex[segmentIndex]) {
                        this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
                        this.arrAccountDescription[segmentIndex] = result.mainAccountDescription;
                        this.arrAccountDescriptionArabic[segmentIndex] = result.mainAccountDescriptionArabic
                        this.accountNumber[segmentIndex] = result.mainAccountNumber;
                    }
                    else {
                        this.arrAccountDescription.push(result.mainAccountDescription);
                        this.accountNumberIndex.push(result.actIndx.toString());
                        this.arrAccountDescriptionArabic.push(result.mainAccountDescriptionArabic);
                        this.accountNumber.push(result.mainAccountNumber);
                    }

                    for (var m = segmentIndex + 1; m < this.arrSegment.length; m++) {
                        var segment = <HTMLInputElement>document.getElementById('txtSegment_' + m);
                        this.accountNumberIndex[m] = '0';
                        this.arrAccountDescription[m] = ' ';
                        this.arrAccountDescriptionArabic[m] = ' ';
                        this.accountNumber[m] = ' ';
                        if (currentSegment != m) {
                            segment.disabled = false;
                        }
                        if (m == segmentIndex + 1) {
                            segment.focus();
                        }
                    }
                } else {
                    this.arrAccountDescription = [];
                    this.arrAccountDescriptionArabic = [];
                    this.accountNumberIndex = [];
                    this.accountNumber = [];
                    var currentSegment = segmentIndex;
                    for (var m = currentSegment; m < this.arrSegment.length; m++) {
                        var ctrlId = '';
                        if (m > 0) {
                            ctrlId = 'txtSegment_' + m;
                        }
                        else {
                            ctrlId = 'txtfirstSegment_' + m;
                        }
                        var segment = <HTMLInputElement>document.getElementById(ctrlId);
                        segment.value = '';
                    }
                    this.toastr.warning(pagedData.btiMessage.message);
                }
            });
        }
        else {
            this.arrAccountDescription = [];
            this.arrAccountDescriptionArabic = [];
            this.accountNumberIndex = [];
            this.accountNumber = [];
            var currentSegment = segmentIndex;
            for (var m = currentSegment + 1; m < this.arrSegment.length; m++) {
                var segment = <HTMLInputElement>document.getElementById('txtSegment_' + m);
                segment.value = '';
                segment.disabled = true;
            }
        }
    }

    fetchOtherAccountIndexInfo(segmentIndex) {

        var segment = <HTMLInputElement>document.getElementById('txtSegment_' + segmentIndex);
        var segmentNumber = segment.value;

        if (this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber) {
            return false;
        }
        var acctIndex = this.accountNumberIndex.indexOf(segment.value);

        if (segmentNumber) {
            this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
                let status = pagedData.status;
                let result = pagedData.result;
                if (status == "FOUND") {
                    if (this.accountNumberIndex[segmentIndex]) {
                        this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
                        this.arrAccountDescription[segmentIndex] = result.dimensionDescription;
                        this.arrAccountDescriptionArabic[segmentIndex] = result.dimensionDescriptionArabic;
                        this.accountNumber[segmentIndex] = result.dimensionValue;
                    }
                    else {
                        this.arrAccountDescription.push(result.dimensionDescription);
                        this.accountNumberIndex.push(result.dimInxValue.toString());
                        this.accountNumberIndex.push(result.dimInxValue.toString());
                        this.accountNumber.push(result.dimensionValue);
                    }
                } else {
                    var segment = <HTMLInputElement>document.getElementById('txtSegment_' + segmentIndex);
                    segment.value = '';
                    this.accountNumberIndex[segmentIndex] = ' ';
                    this.arrAccountDescription[segmentIndex] = ' ';
                    this.arrAccountDescriptionArabic[segmentIndex] = ' ';
                    this.accountNumber[segmentIndex] = '';
                    this.toastr.warning(pagedData.btiMessage.message);
                }
            });
        }
        else {
            var currentSegment = segmentIndex;
            this.accountNumberIndex[currentSegment] = '0';
            this.arrAccountDescription[currentSegment] = ' ';
            this.arrAccountDescriptionArabic[currentSegment] = ' ';
            this.accountNumber[currentSegment] = ' ';
        }
    }

    /*
    getAccountNumber(){
           if(companyId == 0)
           {
            this.commonService.getGlAccountNumberList().then(data => {
                this.arrAccountNumber=[{'id':'0','text':this.select}];
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    for(var i=0;i<data.result.length;i++)	
                    {
                        this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                    }
                }
            });
           }
           else{
               companyId = companyId.split('*');
            this.commonService.getGlAccountNumberListByCompany(companyId[1]).then(data => {
                this.arrAccountNumber=[{'id':'0','text':this.select}];
                
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    for(var i=0;i<data.result.length;i++)	
                    {
                        this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                    }
                }
            });
           }
/*       
        this.vatDetailSetupService.getGlAccountNumberList().then(data => {
            // Serachable Select Box Step3
             this.ArraccountRowId = [];
			 for(var i=0;i< data.result.length;i++)
              {
                this.ArraccountRowId.push({ "id": data.result[i].accountTableRowIndex, "itemName": data.result[i].accountNumber})
              }
			    // Serachable Select Box Step3 End
                        // this.ArrAccountNumberList = data.result;
                        // this.ArrAccountNumberList.splice(0, 0, { "accountTableRowIndex": "", "accountNumber": this.select });
                        // this.model.accountRowId='';
                  });
                     * /
    }
                */
    setHoverdAccountNumber(event) {
        if (this.hoverdAccountNumber != event.path[0].innerText) {
            this.hoverdAccountNumber = event.path[0].innerText;
        }
    }

    displayCurrentSelectedJournalEntry(journalindex: any) {
        this.selectedJournalEntryDetail = this.arrjournalEntryDetailsList[journalindex];
        if (this.selectedJournalEntryDetail.interCompanyName) {

            this.activeEditCompany = [this.arrEditCompany.find(x => x.text == this.selectedJournalEntryDetail.interCompanyName)];
        } else {

            this.activeEditCompany = this.emptyArray;
        }

        if (this.selectedJournalEntryDetail.accountTableRowIndex) {

            this.activeEditAccountNumber = [this.arrEditAccountNumber.find(x => x.id == this.selectedJournalEntryDetail.accountTableRowIndex)];
        } else {

            this.activeEditAccountNumber = this.emptyArray;
        }



        this.debitEditAmount = this.selectedJournalEntryDetail.debitAmount;
        this.creditEditAmount = this.selectedJournalEntryDetail.creditAmount;
        this.distributionEditDescription = this.selectedJournalEntryDetail.distributionDescription;
        this.accountDescriptionEdit = this.selectedJournalEntryDetail.accountDescription;
        this.accountTableRowEdit = this.activeEditAccountNumber[0];
        var accountNum = this.accountTableRowEdit.text.split(":");
        this.editAccountDescriptionDisplay = accountNum[0];

    }

    updatedJournalEntryDetails() {

        if ((this.debitEditAmount || this.creditEditAmount) && this.accountTableRowEdit.id && this.distributionEditDescription && (this.intercompany.id || this.isIntercompany)) {


            if (this.creditEditAmount) {
                this.isCredit = true;
            }
            else {
                this.isCredit = false;
            }

            this.model.totalJournalEntryCredit = parseFloat(this.model.totalJournalEntryCredit.toString()) - parseFloat(this.selectedJournalEntryDetail.creditAmount.toString());
            this.model.totalJournalEntryDebit = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.selectedJournalEntryDetail.debitAmount.toString());

            var companyId = this.intereditcompany.id.split('*');
            console.log(this.intereditcompany);
            this.selectedJournalEntryDetail.intercompanyId = companyId[0];
            this.selectedJournalEntryDetail.interCompanyName = this.intereditcompany.text;
            this.selectedJournalEntryDetail.accountNumber = this.editAccountDescriptionDisplay;
            this.selectedJournalEntryDetail.accountTableRowIndex = this.accountTableRowEdit.id;
            this.selectedJournalEntryDetail.distributionDescription = this.distributionEditDescription;
            this.selectedJournalEntryDetail.debitAmount = this.debitEditAmount;
            this.selectedJournalEntryDetail.creditAmount = this.creditEditAmount;
            this.selectedJournalEntryDetail.accountDescription = this.accountDescriptionEdit;

            //var journalEntryDetailIndex = this.arrjournalEntryDetailsList.indexOf(this.selectedJournalEntryDetail);
            //this.RemoveJournalEntryDetails(journalEntryDetailIndex,this.selectedJournalEntryDetail);


            //this.model.totalJournalEntryDebit = newTotalCreditAmmount;




            this.model.totalJournalEntryCredit = parseFloat(this.model.totalJournalEntryCredit.toString()) + parseFloat(this.selectedJournalEntryDetail.creditAmount.toString());
            this.model.totalJournalEntryDebit = parseFloat(this.model.totalJournalEntryDebit.toString()) + parseFloat(this.selectedJournalEntryDetail.debitAmount.toString());

            this.amtDifference = parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());
            this.amtDifference = this.DecimalFormater(this.amtDifference);

            /*                      this.arrjournalEntryDetailsList.push({
                                     "accountTableRowIndex":this.accountTableRowEdit.id,
                                     'accountNumber':this.editAccountDescriptionDisplay, //  this.accountTableRow.text,
                                     "intercompanyId":companyId[0],
                                     "interCompanyName":this.intercompany.text,
                                     "debitAmount":this.debitEditAmount,
                                     "creditAmount":this.creditEditAmount,
                                     "distributionDescription":this.distributionEditDescription,
                                     "isCredit": this.isCredit,
                                     "journalSequence":this.selectedJournalEntryDetail.journalSequence    
                                 }
                                 ); */

            //this.getCompanyEditList();


            //this.activeEditAccountNumber=[this.emptyArray[0]];
            // this.editAccountDescriptionDisplay = '';
            //this.arrEditAccountNumber=[this.emptyArray[0]];

            if (!this.debitAmount) {
                this.debitAmount = 0;
            }
            if (!this.creditAmount) {
                this.creditAmount = 0;
            }
            if (!this.model.totalJournalEntryCredit) {
                this.model.totalJournalEntryCredit = 0;
            }
            if (!this.model.totalJournalEntryDebit) {
                this.model.totalJournalEntryDebit = 0;
            }

            //this.model.totalJournalEntryCredit = parseFloat(this.model.totalJournalEntryCredit.toString()) +   parseFloat(this.creditEditAmount.toString());
            //this.model.totalJournalEntryDebit = parseFloat(this.model.totalJournalEntryDebit.toString()) +   parseFloat(this.debitEditAmount.toString());
            //var newTotalJournalEntryDebit =  parseFloat(this.model.totalJournalEntryDebit.toString()) +   parseFloat(this.debitEditAmount.toString());
            //this.model.totalJournalEntryDebit = newTotalJournalEntryDebit;

            //this.amtDifference=parseFloat(this.model.totalJournalEntryDebit.toString()) - parseFloat(this.model.totalJournalEntryCredit.toString());


            this.debitEditAmount = 0;
            this.creditEditAmount = 0;
            this.distributionEditDescription = '';
            this.isCredit = false;

            if (parseInt(this.amtDifference.toString()) > 0) {
                this.isPostAllowed = true;
            }
            else {
                this.isPostAllowed = false;
            }

        }
        else {
            // alert(Constants.msgText);
        }
    }


    openSearchModal(event) {
        if (event.target.value === '?' || event.target.value === '؟') {
            document.getElementById('searchPopupButton').click();
            window.setTimeout(() => { document.getElementById('SearchAccountNumber').focus() }, 500);
        } else if (event.target.value === '+' || event.target.value === '+') {
            document.getElementById('createAccountNumberPopup').click();
        }
    }

    receiveMessage(event) {
        console.log(this.arrAccountNumber);
        let selectedItem = this.arrAccountNumber.find(x => x.id == event.accountTableRowIndex);
        console.log(selectedItem);
        this.bindSelectedItem(selectedItem, 'AccountNumber');
    }





}