import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ClearingEntry } from '../../../../../financialModule/_models/transactionModule/generalLedger/clearingEntry';
import { ClearingEntryService } from '../../../../../financialModule/_services/transactionModule/generalLedger/clearingEntry.service';
import { JournalEntryService } from '../../../../../financialModule/_services/transactionModule/generalLedger/JournalEntry.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { BatchSetupService } from '../../../../../financialModule/_services/transactionModule/generalLedger/batchSetUp.service';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgForm } from '@angular/forms';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: './clearingEntry.component.html',
    providers:[ClearingEntryService,CommonService,BatchSetupService,JournalEntryService]
})

//export to make it available for other classes
export class ClearingEntryComponent {
    rows = new Array<ClearingEntry>();
    moduleCode = Constants.financialModuleCode;
    currentTransactionType=Constants.batchTransacitonType.CLEARING_ENTRY;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    availableFormValues: [object];
    defaultAddFormValues: Array<object>;
    isModify:boolean=false;
    IsAddJournalEntryDetails:boolean=false;
    model: any = {};
    arrJournalEntry = [];
    arrAuditTrial = [];
    arrBatchList = [];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    select=Constants.select;
    btnCancelText=Constants.btnCancelText;
    close=Constants.close;
    accountTableRow:any={'id':'','text':''};
    accountTableRowOffset:any={'id':'','text':''};
    distributionDescription:'';
    accountDescriptionTitle=Constants.accountDescription;
    today = new Date();
    isPostEvt:boolean = false;
    showBtns:boolean=false;
    
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isDeleteAction : boolean = false;
    isConfirmationModalOpen:boolean=false;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;

    btndisabled:boolean=false;

    year=this.today.getFullYear();
    private  Object = { 
         date: { year: 2018, month: 10, day: 9 }
     };
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy'
    };
    //private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
     onDateChanged(event: IMyDateModel): void {
       
    }
    emptyArray=[{'id':'0','text':this.select}];
    activeAccountNumber:any=this.emptyArray;
    activeAccountNumberOffset:any=this.emptyArray;
    arrAccountNumber=[];
    arrjournalEntryDetailsList:any=[];
    accountDescription:string;
    activeJournalEntry:any=this.emptyArray;
    activeBatch:any=this.emptyArray;
    activeAuditTrial:any=this.emptyArray;

    @ViewChild(DatatableComponent) table: DatatableComponent;
       constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private clearingEntryService: ClearingEntryService,
        private commonService: CommonService,
        private batchSetupService: BatchSetupService,
        private journalEntryService:JournalEntryService
        )
        {
            
        }

    ngOnInit() 
            {
                this.getGLNextJournalEntry();
                this.getAddScreenDetail();
                this.getBatchList()
                this.getAuditTrialCode();
                this.getJournalIdList();
                this.bindAccountNumber();
                this.model.balanceType="0";
                this.model.transactionDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
            }

    getAddScreenDetail()
            {
                this.screenCode = "S-1232";
                this.defaultAddFormValues = [
                    { 'fieldName': 'CLEARING_ENTRY_JOURNAL_ENTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                    { 'fieldName': 'CLEARING_ENTRY_TRANSACTION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_BALANCE_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_YEAR_TO_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                    { 'fieldName': 'CLEARING_ENTRY_PERIOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_SOURCE_DOC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_ACC_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_ACC_NUMBER_OFFSET', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_DIS_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_VOID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CLEARING_ENTRY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                ];
                this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                    {
                        this.isScreenLock = res;
                    });
        this.getScreenDetail(this.screenCode,'Add');
    }  

    getScreenDetail(screenCode,ArrayType)
    {
      
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }          
        });
    }

    getGLNextJournalEntry()
    {
        this.commonService.getGLNextJournalEntry().then(data => {
           this.model.journalID=data.result.nextJournalEntry;
        });
    }
    
    SaveUpdateClearingJournalEntry(f: NgForm) {
        this.btndisabled=true;
        this.isPostEvt=false;
        if(f.valid && this.arrjournalEntryDetailsList.length > 0 && this.activeAuditTrial[0].id != '0')
        {
           if(this.isModify)
           {
             this.model.journalID=this.activeJournalEntry[0].id;
           }
           this.model.journalEntryDetailsList=this.arrjournalEntryDetailsList;
           this.model.sourceDocumentId=this.activeAuditTrial[0].id;
           this.model.glBatchId=this.activeBatch[0].id;
            
           if(this.model.transactionDate.formatted == undefined)
           {
              var transactionDate = this.model.transactionDate;
              if(transactionDate.date != undefined)
              {
                 this.model.transactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
              }
           }
           else
           {
               this.model.transactionDate = this.model.transactionDate.formatted;
           }
          // this.model.transactionDate = this.model.transactionDate.formatted;
             
            this.clearingEntryService.SaveUpdateClearingJournalEntry(this.model,this.isModify).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 201) {
                    this.isModify=false;
                    this.model.bankId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.model.balanceType="0";
                        this.isModify=false;
                        this.showBtns=true;
                        this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                        //this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
              
                 
                this.model.balanceType = this.model.balanceType.toString();
                
                this.model.transactionDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
                this.IsAddJournalEntryDetails=false;
                this.getGLNextJournalEntry();
                this.getJournalIdList();
                this.activeBatch=this.emptyArray;
                this.activeAuditTrial=this.emptyArray;
                this.activeAccountNumber=this.emptyArray;
                this.activeAccountNumberOffset=this.emptyArray;
                this.arrjournalEntryDetailsList=[];
                f.resetForm();
                this.model.balanceType="0";
            }).catch(error => {
                   
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        this.btndisabled=false;
    }    


       // Delete Clearing Journal Entry
    DeleteClearingJournalEntry(f: NgForm)
    {
        
           if(!this.model.journalID)
           {
               this.isSuccessMsg = false;
               this.isfailureMsg = true;
               this.showMsg = true;
               this.hasMsg = true;
               this.messageText = this.defaultAddFormValues[0]['listDtoFieldValidationMessage'][0]['validationMessage'];
               window.setTimeout(() => {
                 this.showMsg = false;
                 this.hasMsg = false;
               }, 4000);
               
           }
           else{
               this.clearingEntryService.DeleteClearingJournalEntry(this.model.journalID).then(data =>
                   {
                       window.scrollTo(0,0);
                       this.closeModal();
                       if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                               this.isSuccessMsg = true;
                               this.isfailureMsg = false;
                               this.showMsg = true;
                               this.hasMsg = true;
                               this.messageText = data.btiMessage.message;
                               this.isModify=false;
                               this.showBtns=false;
                               this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                               window.setTimeout(() => {
                                 this.showMsg = false;
                                 this.hasMsg = false;
                           }, 4000);
                       }
                       else{
                               this.isSuccessMsg = false;
                               this.isfailureMsg = true;
                               this.showMsg = true;
                               this.hasMsg = true;
                               this.messageText = data.btiMessage.message;
                               window.setTimeout(() => {
                                 this.showMsg = false;
                                 this.hasMsg = false;
                               }, 4000);
                       }
                       this.getJournalIdList();
                       f.resetForm();
                    })
           }
        this.clear(); 
       }

    PostClearingJournalEntry(f: NgForm) {
    this.isPostEvt=true;
    this.btndisabled=true;
    if(f.valid && this.activeBatch[0].id != '0')
    {
            window.scrollTo(0,0);
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.btndisabled=false;
            this.messageText =this.defaultAddFormValues[13]['listDtoFieldValidationMessage'][0]['validationMessage'];
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
    }        
    else if(f.valid && this.arrjournalEntryDetailsList.length > 0 && this.activeAuditTrial[0].id != '0')
    {
        if(this.isModify)
        {
          this.model.journalID=this.activeJournalEntry[0].id;
        }
        this.model.journalEntryDetailsList=this.arrjournalEntryDetailsList;
        this.model.sourceDocumentId=this.activeAuditTrial[0].id;
        this.model.glBatchId=this.activeBatch[0].id;
        
        if(this.model.transactionDate.formatted == undefined)
        {
           var transactionDate = this.model.transactionDate;
           if(transactionDate.date != undefined)
           {
              this.model.transactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
           }
        }
        else
        {
            this.model.transactionDate = this.model.transactionDate.formatted;
        }
         this.clearingEntryService.SaveUpdateClearingJournalEntry(this.model,this.isModify).then(data => {
             window.scrollTo(0,0);
             var datacode = data.code;
             this.btndisabled=false;
             if (datacode == 201) {
         this.clearingEntryService.PostClearingJournalEntry(this.model).then(data => {
             window.scrollTo(0,0);
             var datacode = data.code;
             this.btndisabled=false;
             if (datacode == 200) {
                 this.model.bankId=null;
                     this.isSuccessMsg = true;
                     this.isfailureMsg = false;
                     this.hasMsg = true;
                     this.showMsg = true;
                     this.messageText = data.btiMessage.message;
                     this.isModify=false;
                     this.showBtns=false;
                     //this.setPage({ offset: 0 });
                     window.setTimeout(() => {
                     this.showMsg = false;
                     this.hasMsg = false;
                 }, 4000);
             }
             f.resetForm();
             this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
             this.model.transactionDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
             this.getGLNextJournalEntry();
             this.getJournalIdList();
             this.activeBatch=this.emptyArray;
             this.activeAuditTrial=this.emptyArray;
             this.activeAccountNumber=this.emptyArray;
             this.activeAccountNumberOffset=this.emptyArray;
             this.arrjournalEntryDetailsList=[];
             this.isPostEvt=false;
             
         }).catch(error => {
             window.setTimeout(() => {
                 this.isSuccessMsg = false;
                 this.isfailureMsg = true;
                 this.showMsg = true;
                 this.hasMsg = true;
                 this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
             }, 100)
         });
         }
            })
        }
        this.btndisabled=false;
 }    
    

    getAuditTrialCode(){
        this.commonService.getAuditTrialCode().then(data => {
            this.arrAuditTrial=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
        	    for(var i=0;i<data.result.records.length;i++)	
        	    {   
    	    	    this.arrAuditTrial.push({'id':data.result.records[i].seriesIndex,'text':data.result.records[i].sourceDocument});
                }
            }
        });
    }

    GetClearingJournalEntryByJournalId()
    {
       if(this.activeJournalEntry[0].id != '0')
       {
        this.clearingEntryService.GetClearingJournalEntryByJournalId(this.activeJournalEntry[0].id).then(data => {
            
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                this.isModify=true;
                this.showBtns=true;
                this.model=data.result;
                this.arrjournalEntryDetailsList=data.result.journalEntryDetailsList;
                this.model.journalEntryDetailsList=data.result.journalEntryDetailsList;
                
                if(this.model.balanceType)
                {
                    this.model.balanceType="1";
                }
                else{
                    this.model.balanceType="0";
                }

                if(data.result.transactionDate) {
                    this.model.transactionDate=data.result.transactionDate;
                }

                var transactionDate = this.model.transactionDate;
                var transactionDateData = transactionDate.split('/');
                this.model.transactionDate = {"date":{"year":parseInt(transactionDateData[2]),"month":parseInt(transactionDateData[1]),"day":parseInt(transactionDateData[0])}};
                
                if(data.result.glBatchId)
                {
                    var selectedbatch = this.arrBatchList.find(x => x.id ==   data.result.glBatchId);
                    this.bindSelectedItem(selectedbatch,'Batch');
                }

                var sourceDocument = this.arrAuditTrial.find(x => x.id ==   data.result.sourceDocumentId);
                this.bindSelectedItem(sourceDocument,'AuditTrial');
                
                this.arrjournalEntryDetailsList=[];
                for(var i=0;i<data.result.journalEntryDetailsList.length;i++)
                {
                    var selectedAccountNumber = this.arrAccountNumber.find(x => x.id ==   data.result.journalEntryDetailsList[i].accountTableRowIndex);
                    var selectedAccountNumberOffSetValue = this.arrAccountNumber.find(x => x.id ==   data.result.journalEntryDetailsList[i].accountTableRowIndexOffset);
                    this.arrjournalEntryDetailsList.push({
                        "accountTableRowIndex": data.result.journalEntryDetailsList[i].accountTableRowIndex,
                        'accountNumber':selectedAccountNumber.text,
                        "accountTableRowIndexOffset":data.result.journalEntryDetailsList[i].accountTableRowIndexOffset,
                        "accountTableRowIndexOffsetValue":selectedAccountNumberOffSetValue.text,
                        "distributionDescription":data.result.journalEntryDetailsList[i].distributionDescription}
                    );
                }
                
                
                // this.bindSelectedItem(this.emptyArray[0],'Company');
                // this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
            }
            else{
                 this.isModify=false;
                // this.model={};
                 this.arrjournalEntryDetailsList=[];
                 this.bindSelectedItem(this.emptyArray[0],'Batch');
                 this.bindSelectedItem(this.emptyArray[0],'AuditTrial');
                 this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
                 this.bindSelectedItem(this.emptyArray[0],'AccountNumberOffset');
                 this.getGLNextJournalEntry();
            }
        });
    }
    else{
     this.getGLNextJournalEntry();
    }
    }

    getGlAccountNumberDetail(actionType:string,myModal)
    {
        var accountTableRowIndex='';
        if(actionType == 'AccountNumber')
        {
            accountTableRowIndex=this.activeAccountNumber[0].id
        }
        else if(actionType == 'AccountNumberOffset')
        {
            accountTableRowIndex=this.activeAccountNumberOffset[0].id
        }
        this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                this.accountDescription=data.result.accountDescription;
            }
            else{
                this.accountDescription=data.btiMessage.message;
            }
            myModal.open();
        });
    }

    getJournalIdList(){
        
        this.clearingEntryService.getJournalIdListForUpdateClearingJVEntry().then(data => {
            this.arrJournalEntry=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
        	    for(var i=0;i<data.result.length;i++)	
    	        {
    		        this.arrJournalEntry.push(data.result[i].journalID);
                }
            }
     });
    }
    
    getBatchList(){
        this.commonService.getAllBatchesListByTransactionType(this.currentTransactionType).then(data => {
            this.arrBatchList=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrBatchList.push({'id':data.result[i].batchId,'text':data.result[i].batchId});
                }
            }
        });
    }
    
    bindAccountNumber()
    {
        this.clearingEntryService.getAccountNumberForClearingJVEntry().then(data => {
            this.arrAccountNumber=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                }
            }
        });
    }
    AddJournalEntryDetails()
    {
      this.IsAddJournalEntryDetails=true;
       if(this.activeAccountNumber[0].id !='0' && this.activeAccountNumberOffset[0].id !='0' && this.distributionDescription)
       {
            this.arrjournalEntryDetailsList.push({
                "accountTableRowIndex":this.activeAccountNumber[0].id,
                'accountNumber':this.activeAccountNumber[0].text,
                "accountTableRowIndexOffset":this.activeAccountNumberOffset[0].id,
                "accountTableRowIndexOffsetValue":this.activeAccountNumberOffset[0].text,
                "distributionDescription":this.distributionDescription}
            );
            this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
            this.bindSelectedItem(this.emptyArray[0],'AccountNumberOffset');
            this.distributionDescription='';
            this.IsAddJournalEntryDetails=false;
       }
       else{
         //  alert('Please fill all detail');
       }
       
    }
    RemoveJournalEntryDetails(RowIndex,tempArr:any)
    {
        this.arrjournalEntryDetailsList.splice(RowIndex, 1);
    }
    private value:any = {};
    private item:any = {};
       public onSelectItem(item:any,type:string):void {
            if(type == 'AccountNumber')
            {
                this.item = item;
            }
            else if(type == 'AccountNumberOffset')
            {
                this.item = item;
            }
       }
       
        public onItemRemoved(value:any,type:string):void {
            if(type == 'AccountNumber')
            {
                this.value = value;
            }
            else if(type == 'AccountNumberOffset')
            {
                this.value = value;
            }
          
        }
       
        public bindSelectedItem(value:any,type:string):void {
            if(type == 'Batch')
            {
                this.value = value;
                if(value.text)
                {
                    this.activeBatch=[value];
                }
            }
            else if(type == 'AuditTrial')
            {
                this.value = value;
                if(value.text)
                {
                    this.activeAuditTrial=[value];
                }
            }
            if(type == 'JournalEntry')
            {
                this.value = value;
                if(value.text)
                {
                    this.activeJournalEntry=[value];
                }
                this.GetClearingJournalEntryByJournalId();
            }
            else if(type == 'AccountNumber')
            {
                this.accountTableRow=value;
                this.value = value;
                if(value.text)
                {
                    this.activeAccountNumber=[value];
                }
            }
            else if(type == 'AccountNumberOffset')
            {
                this.accountTableRowOffset=value;
                this.value = value;
                if(value.text)
                {
                    this.activeAccountNumberOffset=[value];
                }
            }
        }
        
        varifyDelete()
    {
       this.isDeleteAction=true;
       this.confirmationModalBody = this.deleteConfirmationText;
       this.isConfirmationModalOpen = true;
    }


    closeModal()
    {
         this.isDeleteAction=false;
         this.isConfirmationModalOpen = false;
    } 

        clear(){
            
            if(!this.isModify)
            {
                this.model.journalID=[];
                this.getGLNextJournalEntry();
            }
            else{
                this.model.journalID=this.activeJournalEntry[0].id;
            }
            
            this.model.balanceType = this.model.balanceType.toString();
            this.model.balanceType="0";
            this.model.transactionDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
            this.isPostEvt=false;
            this.IsAddJournalEntryDetails=false;
            this.arrjournalEntryDetailsList=[];
            this.model.transactionDescription='';
            this.distributionDescription='';
            //this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
            this.bindSelectedItem(this.emptyArray[0],'Batch');
            this.bindSelectedItem(this.emptyArray[0],'AuditTrial');
            this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
            this.bindSelectedItem(this.emptyArray[0],'AccountNumberOffset');
        } 

        Cancel(f){
        this.clear();
        //f.resetForm();
        this.isModify=false;
        this.isPostEvt=false;
        this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
        this.getGLNextJournalEntry();
    }
        print()
        {
            window.print();
        }
        print00(): void {
            let printContents, popupWin;
            printContents = document.getElementById('FrmClearingEntry').innerHTML;
            popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
            popupWin.document.open();
            popupWin.document.write(`
            <html>
                <head>
                <title>Print tab</title>
                <style>
                //........Customized style.......
                </style>
                </head>
            <body onload="window.print();window.close()">${printContents}</body>
            </html>`
        );
            popupWin.document.close();
}
onlyDecimalNumberKey(event) {
    return this.getScreenDetailService.onlyDecimalNumberKey(event);
}
   
}