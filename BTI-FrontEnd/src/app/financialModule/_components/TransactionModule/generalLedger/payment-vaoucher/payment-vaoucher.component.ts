import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { paymentVoucher } from '../../../../_services/transactionModule/generalLedger/paymentVoucher.service';
import { stringify } from '@angular/core/src/util';
import { NgForm } from '@angular/forms';
import { AuditTrialService } from '../../../../_services/general-ledger-configuration-setup/audit-trial.service';
import { Constants } from '../../../../../_sharedresource/Constants';
import { TrimWhitespace } from '../../../../../_sharedresource/TrimWhitespace.directive';
import { NgFor } from '@angular/common';
import { IMyDateModel } from 'ngx-mydatepicker';
import { elementsFromPoint, selectRowsBetween } from '@swimlane/ngx-datatable/release/utils';
import { id } from '@swimlane/ngx-charts/release/utils';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { from } from 'rxjs/observable/from';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { AccountStructureService } from '../../../../_services/general-ledger-setup/accounts-structure-setup.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { forEach } from '@angular/router/src/utils/collection';
import { AuditTrial } from '../../../../_models/general-ledger-configuration-setup/audit-trial';

@Component({
  animations: [],
  selector: 'app-payment-vaoucher',
  templateUrl: './payment-vaoucher.component.html',
  styleUrls: ['./payment-vaoucher.component.css'],
  providers: [CommonService, paymentVoucher, AuditTrialService, AccountStructureService]
})
export class PaymentVaoucherComponent implements OnInit {
  
  @ViewChild('selectAccountNumber') selectAccountNumber;
  
  accountDescription: string;

  audtiTrial: string;


  //Screen Detail 
  screenCode = "";
  defaultAddFormPaymentVoucher = [];
  moduleName = "";
  screenName = "";
  availableFormValues = [];
  moduleCode = Constants.financialModuleCode;
  select = Constants.select;
  //
  btnDisabled: boolean = false;

  date;
  companyID: string;
  //
  today = new Date();
  year = this.today.getFullYear();
  isUpdate: boolean = false;
  isSave: boolean = true;

  // model object for payment voucher
  model: any = { "totalPaymentVoucherCredit": 0, "totalPaymentVoucherDebit": 0 };
  totalPaymentVoucherDebit: number;
  // variable for data selecting
  //paymentType
  arrPaymentType = [];
  activePaymentType = [{ 'id': '0', 'text': this.select }];
  //Checkbook ID
  arrCheckbookList = [];
  activeCheckbook = [{ 'id': '0', 'text': this.select }];

  //Account Number
  arrAccountNumber = [];
  activeAccountNumber = [{ 'id': '0', 'text': this.select }];
  //Payment Voucher IDs
  arrPaymentVoucherIDs = [];
  activePaymentVoucherID = [{ 'id': '0', 'text': '--Select--' }];

  // credit card number
  arrCreditCard = [{ 'id': '0', 'text': this.select }];
  activeCreditCard: any = [{ 'id': '0', 'text': this.select }];

  //Currency Exchange Header
  arrExchangeHeader = [];
  activeExchangeHeader = [{ 'id': '0', 'text': this.select }];
  //Currency exchange Detail
  arrExchangeDetail = [];
  activeExchangeDetail = [{ 'id': '0', 'text': this.select }];

  //Curerncy
  arrCurrency = [];
  activeCurrency = [];

  //batch id
  arrBatchIDs = [];
  activeBatchID = [{ 'id': '0', 'text': this.select }];

  //Company 
  arrCompany = [];
  activeCompany = [{ 'id': '0', 'text': this.select }];

  arrDebitAccountNumber = [];
  activeDebitAccountNumber = [{ 'id': '0', 'text': this.select }];

  //=================================//
  interCompany;
  isIntercompany: boolean = true;

  // Payment voucher Header


  isModify: boolean = true;

  // Payment Detail
  distribution: string;
  debitAmount: number;
  VATAmount: number;
  arrDebitVoucherList = [];


  //For error & success Msg (Form)
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  showMsg: boolean = false;
  hasMsg: boolean = false;
  messageText: string;

  //For error for detail
  isAddPaymentVaoucher: boolean = false;
  currentTransactionType = Constants.batchTransacitonType.PAYMENT_VOUCHER;

  //For Payment type field
  isCash: boolean = true;
  isCheck: boolean = false;
  userData: any;
  isCreditCard: boolean = false;
  isBankTransfer: boolean = false;

  // new account Number variable 
  arrAccountDescription: string[];
  arrAccountDescriptionArabic: string[];
  accountNumber: string[] = [];
  accountNumberIndex = [];
  arrSegment = [];
  currentLanguage;
  dispalyArabicDescriptiopn: boolean;
  displayEnglishDescription: boolean;
  isArrDebitAccountNumber: boolean;
  isArrAccountNumber: boolean;

  constructor(private getScreenDetailService: GetScreenDetailService,
    private commoneService: CommonService,
    private paymentVoucherSrvice: paymentVoucher,
    private auditTrialService: AuditTrialService,
    private accountStructureService: AccountStructureService,
    public toastr: ToastsManager,
    private commonService: CommonService,
    vcr: ViewContainerRef) {


    this.accountNumberIndex = [];
    this.arrAccountDescription = [];
    this.arrAccountDescriptionArabic = [];
    this.toastr.setRootViewContainerRef(vcr);
    this.userData = JSON.parse(localStorage.getItem('currentUser'));
    if (this.userData.companyCount > 1) {
      this.isIntercompany = false;
      this.model.interCompany = true;
      this.getCompanyList();
    }
    else {
      this.model.interCompany = false;

    }
    this.currentLanguage = localStorage.getItem('currentLanguage');
    if (this.currentLanguage == 2) {
      this.dispalyArabicDescriptiopn = true;
      this.displayEnglishDescription = !this.dispalyArabicDescriptiopn;
    } else {
      this.displayEnglishDescription = true;
      this.dispalyArabicDescriptiopn = !this.displayEnglishDescription;
    }

    this.companyID = localStorage.getItem('tenantid');

  }


  getPaymentVoucherFormDetail() {
    this.screenCode = "S-1267";
    this.defaultAddFormPaymentVoucher = [
      { 'fieldName': 'PAYMENT_VOUCHER_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_PAYMENT_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_Bank_Name', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_Check_Number', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_DESCRIPTION_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_DATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_CURRENCY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_COMPANY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_DEBIT_ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_DEBIT_DECRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_DEBIT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_TOTAl_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_DELETE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_POST', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_SAVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_CANCEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_Clear', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_UPDATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' },
      { 'fieldName': 'PAYMENT_VOUCHER_REMAINING_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '' }
    ];

    this.getScreenDetail(this.screenCode, 'FORM');
  }

  getScreenDetail(screenCode, ArrayType) {

    this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
      this.screenName = data.result.dtoScreenDetail.screenName;
      this.availableFormValues = data.result.dtoScreenDetail.fieldList;
      console.log(data);

      if (ArrayType == 'FORM') {
        this.moduleName = data.result.moduleName;
        for (var j = 0; j < this.availableFormValues.length; j++) {
          var fieldKey = this.availableFormValues[j]['fieldName'];
          var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
          var objDefault = this.defaultAddFormPaymentVoucher.find(x => x['fieldName'] === fieldKey);
          objDefault['fieldValue'] = objAvailable['fieldValue'];
          objDefault['helpMessage'] = objAvailable['helpMessage'];
          objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
          objDefault['deleteAccess'] = objAvailable['deleteAccess'];
          objDefault['readAccess'] = objAvailable['readAccess'];
          objDefault['writeAccess'] = objAvailable['writeAccess'];

        }
      }
    }).then(data => console.log(this.defaultAddFormPaymentVoucher));

  }

  ngOnInit() {
    this.getPaymentMethod();
    this.getAccountNumberList();
    this.getCurrencySetupList();
    this.getCompanyList();
    this.getNextPaymentVoucherID();
    this.getAllPaymentVoucerIDs();
    this.GetCheckbookMaintenance();
    //this.getBatchList();
    this.getPaymentVoucherFormDetail();
    this.getSegmentCount();
    this.GetCreditCardSetup();
    this.GetCreditCardSetup();
  }

  //getting List of options
  //1-payment type
  getPaymentMethod() {
    this.arrPaymentType = [{ 'id': '0', 'text': this.select }];
    this.activePaymentType = [{ 'id': '0', 'text': this.select }];
    this.commoneService.GetPaymentMethod().then(data => {
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        this.arrPaymentType = [{ 'id': '0', 'text': this.select }];
        for (let PaymentType of data.result) {
          this.arrPaymentType.push({ 'id': PaymentType.typeId, 'text': PaymentType.name });

        }
      }
    });
  }
  // Get Checkbook Maintenance
  GetCheckbookMaintenance() {
    this.commoneService.GetCheckbookMaintenance().then(data => {
      this.arrCheckbookList = [{ 'id': '0', 'text': this.select }];
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        for (var i = 0; i < data.result.records.length; i++) {
          this.arrCheckbookList.push({ 'id': data.result.records[i].checkBookId, 'text': data.result.records[i].checkBookId });
        }
      }
    });
  }
  //2-Account Number
  getAccountNumberList() {
    this.activeAccountNumber = [{ 'id': '0', 'text': this.select }];
    this.commoneService.getGlAccountNumberListNew().then(data => {
      this.arrAccountNumber = [{ 'id': '0', 'text': this.select }];
      this.arrDebitAccountNumber = [{ 'id': '0', 'text': this.select }];
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        for (var i = 0; i < data.result.length; i++) {
          // this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
          this.arrAccountNumber.push({
            'id': data.result[i].accountTableRowIndex, 'text':

              (data.result[i].accountNumber ? data.result[i].accountNumber : '') /*+ '<*>' + (data.result[i].accountDescription ? data.result[i].accountDescription : '')
              + '<*>' + (data.result[i].accountDescriptionArabic ? data.result[i].accountDescriptionArabic : '')*/
          });

          this.arrDebitAccountNumber.push({
            'id': data.result[i].accountTableRowIndex, 'text':

              (data.result[i].accountNumber ? data.result[i].accountNumber : '')/* + '<*>' + (data.result[i].accountDescription ? data.result[i].accountDescription : '')
              + '<*>' + (data.result[i].accountDescriptionArabic ? data.result[i].accountDescriptionArabic : '')*/
          });


        }
      }
    });
  }
  //3-Currency Setup 
  getCurrencySetupList() {
    this.activeCurrency = [{ 'id': '0', 'text': this.select }]
    this.commonService.getCurrencySetup().then(data => {
      this.arrCurrency = [{ 'id': '0', 'text': this.select }];
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        var fatchData = data.result;
        for (var i = 0; i < data.result.records.length; i++) {
          this.arrCurrency.push({ 'id': data.result.records[i].currencyId, 'text': data.result.records[i].currencyId });
        }
        var defaultCurrency: any = [];
        defaultCurrency = data.result.records.find(x => x.defaultCurrency == true);
        console.log(defaultCurrency);
        if (defaultCurrency) {
          this.changeCurrency({ 'id': defaultCurrency.currencyId, 'text': defaultCurrency.currencyId });
        }

      }
    });
  }

  //4-getExchange Rate
  GetExchangeTableSetupByCurrencyId(currencyID: string) {
    this.arrExchangeDetail = [{ 'id': '0', 'text': this.select }];
    this.activeExchangeDetail = [{ 'id': '0', 'text': this.select }];
    this.commoneService.getExchangeTableSetupByCurrencyId(currencyID).then(data => {
      this.arrExchangeDetail = [{ 'id': '0', 'text': this.select }];
      this.activeExchangeDetail = [{ 'id': '0', 'text': this.select }];
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        for (let exchangeDetail of data.result.records) {
          this.arrExchangeDetail.push({ 'id': exchangeDetail.exchangeTableIndex, 'text': exchangeDetail.exchangeId });
          if (exchangeDetail.default) {
            this.changeExchangeTableID({ 'id': exchangeDetail.exchangeTableIndex, 'text': exchangeDetail.exchangeId });
          }
        }
      }

    });
  }

  //5-get Exchange Detail
  GetCurrencyExchangeDetail(exchangeId: string) {
    this.commoneService.GetCurrencyExchangeDetail(exchangeId).then(data => {
      if (data.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND") {
        this.model.exchangeRate = data.result.exchangeRate;

        this.model.exchangeExpirationDate = data.result.exchangeExpirationDate;
      }
    });
  }
  //6-get Company List
  getCompanyList() {
    this.activeCompany = [{ 'id': '0', 'text': this.select }];
    this.commoneService.getCompanyList().then(data => {
      this.arrCompany = [{ 'id': '0', 'text': this.select }];
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        for (let company of data.result) {
          this.arrCompany.push({ 'id': company.companyId, 'text': company.name })
        }
        var tenantid = localStorage.getItem('tenantid')
        var SelectedComp = data.result.find(x => x.tenantId == tenantid);
        this.changeCompany({ 'id': SelectedComp.companyId, 'text': SelectedComp.name });
      }
    });
  }
  // 7-get Next PAyment Voucher ID
  getNextPaymentVoucherID() {
    this.paymentVoucherSrvice.getNextPaymentVoucherID().then(data => {
      this.model.paymentVoucherID = data.result.paymentVoucherID;
    })
  }


  //8- get All Payment Voucher IDs
  getAllPaymentVoucerIDs() {
    this.arrPaymentVoucherIDs = [{ 'id': '0', 'text': this.select }];
    this.paymentVoucherSrvice.getPaymentVoucerIDs().then(data => {
      this.arrPaymentVoucherIDs = [{ 'id': '0', 'text': this.select }];
      for (let PaymentID of data.result.ids) {
        this.arrPaymentVoucherIDs.push({ 'id': PaymentID, 'text': 'PV-' + PaymentID });
      }
    });
  }

  //9- get all audit series
  getBatchList() {
    this.activeBatchID = [{ 'id': '0', 'text': this.select }]
    this.commoneService.getAllBatchesListByTransactionType(this.currentTransactionType).then(data => {
      console.log(data);
      this.arrBatchIDs = [{ 'id': '0', 'text': this.select }];
      for (let batch of data.result) {
        this.arrBatchIDs.push({ 'id': batch.batchId, 'text': batch.description });
      }
    });


  }

  // 10- get all credit card
  GetCreditCardSetup() {
    this.commonService.GetCreditCardSetup().then(data => {

      this.arrCreditCard = [{ 'id': '0', 'text': this.select }];
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        for (var i = 0; i < data.result.records.length; i++) {
          this.arrCreditCard.push({ 'id': data.result.records[i].cardIndex, 'text': data.result.records[i].cardId });
        }
      }
    });
  }

  // get Batches By Payment method
  getBatchListByPaymentMethod(sourceDocumentId: number, selectedBatchId?) {
    this.cahngeBatchID({ 'id': '0', 'text': '--Select--' });
    this.commonService.getAllBatchesListByPaymentMethod(sourceDocumentId).then(data => {
      this.arrBatchIDs = [{ 'id': '0', 'text': this.select }];
      if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
        for (let i = 0; i < data.result.length; i++) {
          this.arrBatchIDs.push({ 'id': data.result[i].batchId, 'text': data.result[i].batchId });
        }
      }
      if (selectedBatchId) {
        let currentSelectedBatch = this.arrBatchIDs.find(x => x.id == selectedBatchId);
        this.cahngeBatchID(currentSelectedBatch);
      }
    });
  }

  changeCurrency(event) {
    this.model.exchangeRate = '';
    this.model.exchangeExpirationDate = '';
    this.activeCurrency = [event];
    this.model.currencyID = this.activeCurrency[0].id;
    this.GetExchangeTableSetupByCurrencyId(this.activeCurrency[0].id);
  }

  changeCreditAccountNumber(data) {
    this.activeAccountNumber = [data];
  }

  changeBankName(event) {
    this.activeCheckbook = [event];
  }


  changeCreditAccountNumsber(event) {
    this.activeAccountNumber = [event];
    this.model.accountTableRowIndex = event.id;
  }

  cahngeCreditCardId(event) {
    console.log(event);
    this.activeCreditCard = [event];
    this.model.creditCardId = event.text;
  }

  changeCompany(event) {
    this.activeCompany = [event];
  }

  changeDebitAccountNumber(event) {
    console.log(event);
    console.log(this.activeCompany);
    let accountTableRowIndex = event.id;

    this.commoneService.getGlAccountNumberDetailByDynamicCompanyId(accountTableRowIndex, this.companyID).then(data => {
      console.log(data);
      if (data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST') {
        this.commoneService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
          if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
            this.accountDescription = data.result.accountDescription.split("<*>")[0];
            document.getElementById("distribution").focus();
          }
          else {
            this.accountDescription = data.btiMessage.message;
          }


        });
      }
      else if (data.btiMessage.messageShort == 'MAIN_ACCOUNT_TRANSACTION_NOT_ALLOWED') {
        this.activeAccountNumber = [{ 'id': '0', 'text': this.select }];
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.hasMsg = true;
        this.messageText = data.btiMessage.message;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
      }

      this.activeDebitAccountNumber = [event];
    });
  }

  changeExchangeTableID(event) {
    this.activeExchangeDetail = [event];
    this.model.exchangeTableIndex = this.activeExchangeDetail[0].id;
    this.GetCurrencyExchangeDetail(this.activeExchangeDetail[0].text);
  }
  cahngeBatchID(event) {
    this.activeBatchID = [event];
  }

  changePaymentType(event, selectedBatchId?) {
    let audit = new AuditTrial();
    audit.sourceCode = 'PV';
    audit.seriesNumber = event.id;
    this.commonService.getAuditTrialByTransactionType(audit).then(data => {
      this.model.auditTrial = data.result.seriesIndex;
      this.audtiTrial = data.result.sourceDocument + ':' + data.result.sourceDocument;
      console.log(data);
      console.log(event);
      console.log(selectedBatchId);
      this.getBatchListByPaymentMethod(data.result.seriesIndex, selectedBatchId);
    });
    this.activePaymentType = [event];

    if (this.activePaymentType[0].id == '1' || this.activePaymentType[0].id == '0') {
      this.isCash = true;
      this.isCheck = !this.isCash;
      this.isCreditCard = !this.isCash;
      this.isBankTransfer = !this.isCash;
    } else if (this.activePaymentType[0].id == '2') {
      this.isCheck = true;
      this.isCash = this.isCreditCard = this.isBankTransfer = !this.isCheck;
    } else if (this.activePaymentType[0].id == '3') { // credit card
      this.isCreditCard = true;
      this.isCash = this.isCheck = this.isBankTransfer = !this.isCreditCard;
    } else if (this.activePaymentType[0].id == '4') { // bank transfer
      this.isBankTransfer = true;
      this.isCash = this.isCreditCard = this.isCheck = !this.isBankTransfer;
    }
    this.model.bankName = undefined;
    this.activeAccountNumber = [{ 'id': '0', 'text': this.select }];
    this.activeCheckbook = [{ 'id': '0', 'text': this.select }];
  }

  resetPaymentFields() {
    this.model
  }

  changeCheckbookID(event) {
    this.activeCheckbook = [event];
    this.paymentVoucherSrvice.getCheckbookIDAccount(event.id).subscribe(data => {
      console.log(data.json());

      let dataJson = data.json();

      if (dataJson.code == 302) {
        this.model.bankName = dataJson.result.bankName;
        this.model.checkbookID = dataJson.result.checkBookId;
        this.activeAccountNumber = [{ 'id': dataJson.result.accountRowIDIndex, 'text': dataJson.result.accountNumber }]
      } else {

      }

    })
  }

  changePaymnetVoucherID(event) {
    console.log(this.activePaymentVoucherID);
    console.log(event);
    this.activePaymentVoucherID = [{ 'id': event.id + '', 'text': event.text + '' }];
    console.log(this.activePaymentVoucherID);

    this.isUpdate = true;
    this.isSave = !this.isUpdate;
    this.btnDisabled = false;
    this.paymentVoucherSrvice.getPaymentVoucherById(event.id).then(data => {
      console.log(data);
      this.model.paymentVoucherID = data.result.paymentVoucherID;
      let currenctPayment = this.arrPaymentType.find(x => x.id == data.result.paymentTypeID);
      this.changePaymentType(currenctPayment, data.result.glBatcheId);
      let currentAccountNumber = this.arrAccountNumber.find(x => x.id == data.result.accountTableRowIndex);
      this.changeCreditAccountNumber(currentAccountNumber);
      this.model.voucherDescription = data.result.voucherDescription;
      this.model.voucherDescriptionArabic = data.result.voucherDescriptionArabic;
      var transactionDateTime = data.result.transactionDate;

      var transactionDateData = transactionDateTime.split(' ');
      let transactionDate = transactionDateData[0];
      transactionDate = transactionDate.split('-')

      this.model.transactionDate = { "date": { "year": parseInt(transactionDate[0]), "month": parseInt(transactionDate[1]), "day": parseInt(transactionDate[2]) } };
      this.date = [parseInt(transactionDate[0]) + "-" + parseInt(transactionDate[1]) + "-" + parseInt(transactionDate[2])];

      let sourceCurrency = this.arrCurrency.find(x => x.id == data.result.currencyID);
      this.activeCurrency = [sourceCurrency];
      this.GetExchangeTableSetupByCurrencyId(sourceCurrency.id);
      window.setTimeout(() => {
        let exchaangeTableID = this.arrExchangeDetail.find(x => x.id == data.result.exchangeTableIndex);
        this.activeExchangeDetail = [exchaangeTableID];

      }, 2000);
      window.setTimeout(() => {
        this.GetCurrencyExchangeDetail(this.activeExchangeDetail[0].text);
      }, 3000);

      if (this.activePaymentType[0].id == '2' && data.result.checkbookID != undefined) {
        let sourceCheckbookID = this.arrCheckbookList.find(x => x.id == data.result.checkbookID);

        this.changeCheckbookID(sourceCheckbookID);
        this.model.checkNumber = data.result.checkNumber;
      } else if (this.activePaymentType[0].id == '3') {
        this.model.creditCardExpireMonth = data.result.creditCardExpireMonth;
        this.model.creditCardExpireYear = data.result.creditCardExpireYear;
        let currentCreditCard = this.arrCreditCard.find(x => x.text == data.result.creditCardId);
        this.activeCreditCard = [currentCreditCard];
        this.model.creditCardNumber = data.result.creditCardNumber;
        this.model.creditCardId = data.result.creditCardId;
      } else if (this.activePaymentType[0].id == '4') {
        let sourceCheckbookID = this.arrCheckbookList.find(x => x.id == data.result.checkbookID);

        this.changeCheckbookID(sourceCheckbookID);
      }

      this.model.voucherDescription = data.result.voucherDescription;
      this.model.voucherDescriptionArabic = data.result.voucherDescriptionArabic;
      this.model.exchangeRate = data.result.exchangeRate;
      this.model.currencyID = data.result.currencyID;
      this.arrDebitVoucherList = data.result.voucherDetailList;
      this.model.totalPaymentVoucherCredit = data.result.originalTotalPaymentVoucherCredit;
      this.model.totalPaymentVoucherDebit = data.result.originalTotalPaymentVoucherDebit;
      this.model.exchangeTableIndex = data.result.exchangeTableIndex;
      this.model.accountTableRowIndex = data.result.accountTableRowIndex;
      /*  this.isModify = false;
        let sourceCurrency = this.arrCurrency.find(x => x.id == data.result.currencyID);
        this.activeCurrency = [sourceCurrency];
        
        this.GetExchangeTableSetupByCurrencyId(sourceCurrency.id);
        window.setTimeout(() => {
          let exchaangeTableID = this.arrExchangeDetail.find(x => x.id == data.result.exchangeTableIndex);
          this.activeExchangeDetail = [exchaangeTableID];
  
        }, 2000);
        window.setTimeout(() => {
          this.GetCurrencyExchangeDetail(this.activeExchangeDetail[0].text);
        }, 3000);
        this.btnDisabled = false;
        let sourcePayment = this.arrPaymentType.find(x => x.id == data.result.paymentTypeID);
        this.changePaymentType(sourcePayment);
        if (this.activePaymentType[0].id == '2' && data.result.checkbookID != undefined) {
          let sourceCheckbookID = this.arrCheckbookList.find(x => x.id == data.result.checkbookID);
  
          this.changeCheckbookID(sourceCheckbookID);
          this.model.checkNumber = data.result.checkNumber;
        }
        let sourceBatch = this.arrBatchIDs.find(x => x.id == data.result.glBatcheId);
        //this.activeBatchID = [sourceBatch];
        //this.cahngeBatchID(sourceBatch);
        this.model.glBatcheId = data.result.glBatcheId;
        var transactionDateTime = data.result.transactionDate;
  
        var transactionDateData = transactionDateTime.split(' ');
        let transactionDate = transactionDateData[0];
        transactionDate = transactionDate.split('-')
  
        this.model.transactionDate = { "date": { "year": parseInt(transactionDate[0]), "month": parseInt(transactionDate[1]), "day": parseInt(transactionDate[2]) } };
        this.date = [parseInt(transactionDate[0]) + "-" + parseInt(transactionDate[1]) + "-" + parseInt(transactionDate[2])];
        this.model.voucherDescription = data.result.voucherDescription;
        this.model.voucherDescriptionArabic = data.result.voucherDescriptionArabic;
        this.model.exchangeRate = data.result.exchangeRate;
        this.model.currencyID = data.result.currencyID;
        this.arrDebitVoucherList = data.result.voucherDetailList;
        this.model.totalPaymentVoucherCredit = data.result.originalTotalPaymentVoucherCredit;
        this.model.totalPaymentVoucherDebit = data.result.originalTotalPaymentVoucherDebit;
        this.model.exchangeTableIndex = data.result.exchangeTableIndex;
        /* let sourceAccountNumber = this.arrAccountNumber.find(x => x.id == data.result.accountTableRowIndex);
        this.changeCreditAccountNumber(sourceAccountNumber); */
      /*this.activeAccountNumber = [{ 'id': data.result.accountTableRowIndex, 'text': data.result.accountNumber }];
      this.model.accountTableRowIndex = data.result.accountTableRowIndex;*/
    });
    //console.log(this.model);
  }


  renderCheckBookID(checkbookType: string, checkbookID: string) {
    if (checkbookType == "BANK") {
      // call services to bind bank name and account;
      this.commoneService.GetCheckBookMaintenanceByCheckbookId(checkbookID).then(data => {

      });
    } else if (checkbookType == "CREDIT_CARD") {
      // call services to bind credit card account number
    }
  }

  resetForm(f: NgForm) {
    this.isUpdate = false;
    this.isSave = !this.isUpdate;
    this.getNextPaymentVoucherID();
    this.activePaymentVoucherID = [{ 'id': '0', 'text': this.select }];
    this.activeAccountNumber = [{ 'id': '0', 'text': this.select }];
    this.model = { "totalPaymentVoucherCredit": 0, "totalPaymentVoucherDebit": 0 };
    this.activeCurrency = [{ 'id': '0', 'text': this.select }];
    this.activeExchangeDetail = [{ 'id': '0', 'text': this.select }];
    this.arrDebitVoucherList = [];
    this.activeBatchID = [{ 'id': '0', 'text': this.select }];
    this.activePaymentType = [{ 'id': '0', 'text': this.select }];
    f.resetForm();
    this.isModify = true;
    this.getCurrencySetupList();
  }

  openModel(myModal) {
    myModal.open();
  }

  getPaymentVoucherById() {

  }

  onDateChanged(event: IMyDateModel): void {
    var RequestedData = {
      "transactionDate": event.formatted,
      "moduleName": "GL",
      "originId": Constants.massCloseOriginType.GL_JOURNAL_ENTRY
    }
    /*     this.commoneService.checkTransactionDateIsValid(RequestedData).then(data => {
          if (data.btiMessage.messageShort == "INVALID_TRANSACTION_DATE") {
            this.model.transactionDate = {};
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = data.btiMessage.message;
            window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
            }, 4000);
          }
        }); */
  }

  addPaymentVoucherDetail() {
    let accountNumber = this.activeDebitAccountNumber[0];
    console.log(accountNumber.text.split("<*>"));
    let company = this.activeCompany[0];
    this.isAddPaymentVaoucher = true;
    if (this.debitAmount && this.distribution && this.activeCompany && (this.activeDebitAccountNumber[0].id != '0')) {
      this.arrDebitVoucherList.push({
        "companyName": company.text,
        "companyID": company.id,
        "interCompanyIDMultiCompanyTransaction": company.id,
        "accountTableRowIndex": accountNumber.id,
        "accountNumber": accountNumber.text.split("<*>")[0],
        "distributionDescription": this.distribution,
        "debitAmount": this.debitAmount,
        "originalDebitAmount": this.debitAmount,
        "accountDecription": this.accountDescription
      });
      this.accountDescription = '';
      if (!this.model.totalPaymentVoucherDebit) {
        this.model.totalPaymentVoucherDebit = 0;
      }
      this.model.totalPaymentVoucherDebit = parseFloat(this.model.totalPaymentVoucherDebit.toString()) + parseFloat(this.debitAmount.toString());
      this.isAddPaymentVaoucher = false;
      this.debitAmount = undefined;
      this.distribution = undefined;
      this.activeDebitAccountNumber = [{ 'id': '0', 'text': this.select }];
      console.log(this.selectAccountNumber);
      var el = this.selectAccountNumber.element.nativeElement.querySelector('div.ui-select-container');
      el.focus();
    }

  }

  RemovepaymentVoucherDetial(index, PVDetail) {
    this.model.totalPaymentVoucherDebit = this.model.totalPaymentVoucherDebit - PVDetail.debitAmount;
    this.arrDebitVoucherList.splice(index, 1);
  }

  savePaymentVoucher(f: NgForm, myModal) {
    if (this.fieldsValidation() && (this.model.totalPaymentVoucherDebit == this.model.totalPaymentVoucherCredit) && this.activeBatchID[0].id != '0') {
      var transactionDate = this.model.transactionDate;
      if (transactionDate.date != undefined) {
        this.model.transactionDate = transactionDate.date.year + '-' + transactionDate.date.month + '-' + transactionDate.date.day;
      }
      this.model.voucherDetailList = this.arrDebitVoucherList;
      this.model.glBatcheId = this.activeBatchID[0].id;
      this.model.paymentTypeID = this.activePaymentType[0].id;
      this.model.accountTableRowIndex = this.activeAccountNumber[0].id;
      if (this.activePaymentType[0].id == '2') {
        this.model.checkbookID = this.activeCheckbook[0].id;
      } else if (this.activePaymentType[0].id == '3') {
        this.model.creditCardId = this.activeCreditCard[0].id;
      } else if (this.activePaymentType[0].id == '4') { // banktransafer
        this.model.checkbookID = this.activeCheckbook[0].id;
      }
      this.model.currencyID = this.activeCurrency[0].id;
      this.model.exchangeTableIndex = this.activeExchangeDetail[0].id;
      console.log(this.model);
      this.paymentVoucherSrvice.savePaymentVoucher(this.model).then(data => {
        window.scrollTo(0, 0);
        this.resetForm(f);
        var datacode = data.code;
        if (datacode == 201) {

          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          this.hasMsg = true;
          this.messageText = data.btiMessage.message;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          f.resetForm();
          window.scrollTo(0, 0);
          this.getCurrencySetupList();
          this.isCash = true;
          this.isCreditCard = false;
          this.isCheck = false;
        }
        else {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.hasMsg = true;
          this.messageText = data.btiMessage.message;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          window.scrollTo(0, 0);
        }
      }).then(data => {
        this.getAllPaymentVoucerIDs();
      })
    } else {
      if (this.activeExchangeDetail[0].id == '0') {
        myModal.open();
      } else if ((this.model.totalPaymentVoucherDebit != this.model.totalPaymentVoucherCredit)) {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.hasMsg = true;
        this.messageText = "Debit And Credit Must Be Equal";
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
        window.scrollTo(0, 0);
      }
    }
    console.log(this.model);
  }
  updatePaymentVoucher(f: NgForm, myModel) {
    console.log(this.model);
    console.log(this.fieldsValidation());
    if (this.fieldsValidation() && (this.model.totalPaymentVoucherDebit == this.model.totalPaymentVoucherCredit) && this.activeBatchID[0].id != '0') {
      this.model.voucherDetailList = this.arrDebitVoucherList;
      this.model.glBatcheId = this.activeBatchID[0].id;
      this.model.paymentTypeID = this.activePaymentType[0].id;
      this.model.accountTableRowIndex = this.activeAccountNumber[0].id;
      console.log(this.activePaymentType);
      if (this.activePaymentType[0].id == '2') {
        this.model.checkbookID = this.activeCheckbook[0].id;
      }
      this.model.currencyID = this.activeCurrency[0].id;
      this.model.exchangeTableIndex = this.activeExchangeDetail[0].id;
      this.model.voucherDetailList = this.arrDebitVoucherList
      var transactionDate = this.model.transactionDate;
      console.log(transactionDate.date);
      this.model.transactionDate = transactionDate.date.year + '-' + transactionDate.date.month + '-' + transactionDate.date.day;
      this.paymentVoucherSrvice.updatePaymentVoucherById(this.model).subscribe(data => {
        let datajson = data.json();
        window.scrollTo(0, 0);
        this.resetForm(f);
        var datacode = datajson.code;
        if (datacode == 201) {

          this.isSuccessMsg = true;
          this.isfailureMsg = false;
          this.showMsg = true;
          this.hasMsg = true;
          this.messageText = datajson.btiMessage.message;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
        }
        else {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.hasMsg = true;
          this.messageText = datajson.btiMessage.message;
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
        }
        f.resetForm();
      });
    } else {
      if (this.activeExchangeDetail[0].id == '0') {
        myModel.open();
      } else if ((this.model.totalPaymentVoucherDebit != this.model.totalPaymentVoucherCredit)) {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.hasMsg = true;
        this.messageText = "Debit And Credit Must Be Equal";
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
      }
    }

  }
  post(f: NgForm) {
    if (this.fieldsValidation() && (this.model.totalPaymentVoucherDebit == this.model.totalPaymentVoucherCredit)) {
      if (this.activeBatchID[0].id == '0') {
        var transactionDate = this.model.transactionDate;
        if (transactionDate.date != undefined) {
          this.model.transactionDate = transactionDate.date.year + '-' + transactionDate.date.month + '-' + transactionDate.date.day;
        }
        this.model.voucherDetailList = this.arrDebitVoucherList;
        this.model.activeBatchID = this.activeBatchID[0].id;
        this.model.paymentTypeID = this.activePaymentType[0].id;
        if (this.activePaymentType[0].id == '2') {
          this.model.checkbookID = this.activeCheckbook[0].id;
        } else if (this.activePaymentType[0].id == '3') {
          this.model.creditCardId = this.activeCreditCard[0].id;
        } else if (this.activePaymentType[0].id == '4') {
          this.model.checkbookID = this.activeCheckbook[0].id;
        }
        if (this.model.totalPaymentVoucherCredit == this.model.totalPaymentVoucherDebit) {
          this.paymentVoucherSrvice.postPaymentVoucher(this.model).subscribe(data => {
            let datajson = data.json();
            if (datajson.code == 302) {
              this.isSuccessMsg = true;
              this.isfailureMsg = false;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = datajson.btiMessage.message;
              window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
              }, 4000);
              this.resetForm(f);
              this.getAllPaymentVoucerIDs();
              window.scrollTo(0, 0);
              this.isCash = true;
              this.isCreditCard = false;
              this.isCheck = false;
            } else {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = datajson.btiMessage.message;
              window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
              }, 4000);
              window.scrollTo(0, 0);
            }

          });
          this.getCurrencySetupList();
        } else {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.hasMsg = true;
          this.messageText = "The debit amount must be equal credit amount";
          window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
          }, 4000);
          window.scrollTo(0, 0);
        }
      } else {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.hasMsg = true;
        this.messageText = "Remove The Batch before Posting";
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
        window.scrollTo(0, 0);
      }

    } else {
      console.log("here");
    }
  }

  delete(f: NgForm) {
    let paymentVoucherID = this.activePaymentVoucherID[0].id;
    this.paymentVoucherSrvice.deletePaymentVoucherById(paymentVoucherID).subscribe(data => {
      let datajson = data.json();
      var datacode = datajson.code;
      if (datacode == 302) {
        this.isSuccessMsg = true;
        this.isfailureMsg = false;
        this.showMsg = true;
        this.hasMsg = true;
        this.messageText = datajson.btiMessage.message;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
        window.scrollTo(0, 0);
        this.resetForm(f);
        this.getAllPaymentVoucerIDs();
      }
      else {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.hasMsg = true;
        this.messageText = datajson.btiMessage.message
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
        }, 4000);
        window.scrollTo(0, 0);
      }
    })
  }

  printPV() {
    let printContents, popupWin;
    printContents = document.getElementById('printPaymentVaoucher').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
    <html>
        <head>
        <title>Print Payment Voucher </title>
        <link rel="stylesheet" href="../../../../../../assets/css/bootstrap.min.css"  media="print">
        <link rel="stylesheet" href="../../../../../../assets/css/font-awesome.min.css" media="screen,print" >
        
        <link id="admin-theme" rel="stylesheet" href="../../../../../../assets/css/AdminLTE.min.css" media="screen,print" >
        <link rel="stylesheet" href="../../../../../../assets/js/plugins/pace/pace.min.css" media="screen,print">
        <link rel="stylesheet" href="../../../../../../assets/css/accordian.css" media="screen,print"> 	
        <link rel="stylesheet" href="../../../../../../assets/css/angular-toastr.css" media="screen,print" />
        <link href="../../../../../../assets/css/ex-dialog.css" type="text/css" rel="stylesheet"  media="screen,print"/>
        </head>
    <body onload="window.print();window.close()">
        <div  class="row" style=" margin-bottom: 20px">
          <div class="col col-xs-12">
            <img src="../../../../../../assets/img/logo.png" >
          </div>
        </div>
        ${printContents}
        
    </body>
    </html>`
    );
    popupWin.document.close();
  }

  fieldsValidation(): boolean {
    let isValid: boolean = false;
    //  && this.activeAccountNumber[0].id != '0'
    if (this.activePaymentType[0].id != '0' && this.model.voucherDescription != undefined && this.arrDebitVoucherList.length > 0
      && this.model.voucherDescriptionArabic != undefined && this.model.transactionDate != undefined && this.activeCurrency[0].id != '0'
      && this.activeExchangeDetail[0].id != '0' && this.model.totalPaymentVoucherCredit != undefined && this.model.transactionDate != undefined) {
      if (this.activePaymentType[0].id == '1') {
        if (this.activeAccountNumber[0].id != '0') {
          isValid = true;
        }
      } else if (this.activePaymentType[0].id == '2') {
        if (this.activeCheckbook[0].id != '0') {
          isValid = true;
        }
      } else if (this.activePaymentType[0].id == '3') {
        if (this.activeCreditCard[0].id != '0') {
          isValid = true;
        }
      } else if (this.activePaymentType[0].id == '4') {
        if (this.activeCheckbook[0].id != '0') {
          isValid = true
        }
      }
    }
    return isValid;
  }

  postValidation(): boolean {
    return true;
  }


  createNewGlAccountNumber() {
    var tempAccountNumberList = [];
    var accountDesc = this.arrAccountDescription.join("-");
    var accounDescArabic = this.arrAccountDescriptionArabic.join("-");
    var accounNumber = this.accountNumber.join(" - ");
    console.log(this.accountNumber);
    //accountDesc=accountDesc.replace(/,/g , " ");
    //accountDesc=accountDesc.replace("0"," ");
    tempAccountNumberList.push({ 'accountNumber': accounNumber, 'accountNumberIndex': this.accountNumberIndex, 'accountDescription': accountDesc, 'accountDescriptionArabic': accounDescArabic })
    this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {

      if (data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED') {
        this.toastr.success(data.btiMessage.message);
        // this.getAccountNumber();
        this.getAccountNumberList();
        this.closeModal();

      }
      else {
        this.toastr.warning(data.btiMessage.message);
      }
    });
  }

  fetchFirstAccountIndexInfo(segmentIndex) {
    var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_' + segmentIndex);
    var segmentNumber = segment.value;

    if (this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber) {
      return false;
    }

    if (segmentNumber) {
      this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {

        let status = pagedData.status;
        let result = pagedData.result;
        console.log(pagedData);
        if (status == "FOUND") {
          if (this.accountNumberIndex[segmentIndex]) {
            this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
            this.arrAccountDescription[segmentIndex] = result.mainAccountDescription;
            this.arrAccountDescriptionArabic[segmentIndex] = result.mainAccountDescriptionArabic
            this.accountNumber[segmentIndex] = result.mainAccountNumber;
          }
          else {
            this.arrAccountDescription.push(result.mainAccountDescription);
            this.accountNumberIndex.push(result.actIndx.toString());
            this.arrAccountDescriptionArabic.push(result.mainAccountDescriptionArabic);
            this.accountNumber.push(result.mainAccountNumber);
          }

          for (var m = segmentIndex + 1; m < this.arrSegment.length; m++) {
            var segment = <HTMLInputElement>document.getElementById('txtSegment_' + m);
            this.accountNumberIndex[m] = '0';
            this.arrAccountDescription[m] = ' ';
            this.arrAccountDescriptionArabic[m] = ' ';
            this.accountNumber[m] = ' ';
            if (currentSegment != m) {
              segment.disabled = false;
            }
            if (m == segmentIndex + 1) {
              segment.focus();
            }
          }
        } else {
          this.arrAccountDescription = [];
          this.arrAccountDescriptionArabic = [];
          this.accountNumberIndex = [];
          this.accountNumber = [];
          var currentSegment = segmentIndex;
          for (var m = currentSegment; m < this.arrSegment.length; m++) {
            var ctrlId = '';
            if (m > 0) {
              ctrlId = 'txtSegment_' + m;
            }
            else {
              ctrlId = 'txtfirstSegment_' + m;
            }
            var segment = <HTMLInputElement>document.getElementById(ctrlId);
            segment.value = '';
          }
          this.toastr.warning(pagedData.btiMessage.message);
        }
      });
    }
    else {
      this.arrAccountDescription = [];
      this.arrAccountDescriptionArabic = [];
      this.accountNumberIndex = [];
      this.accountNumber = [];
      var currentSegment = segmentIndex;
      for (var m = currentSegment + 1; m < this.arrSegment.length; m++) {
        var segment = <HTMLInputElement>document.getElementById('txtSegment_' + m);
        segment.value = '';
        segment.disabled = true;
      }
    }
  }

  fetchOtherAccountIndexInfo(segmentIndex) {
    var segment = <HTMLInputElement>document.getElementById('txtSegment_' + segmentIndex);
    var segmentNumber = segment.value;

    if (this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber) {
      return false;
    }
    var acctIndex = this.accountNumberIndex.indexOf(segment.value);

    if (segmentNumber) {
      this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
        let status = pagedData.status;
        let result = pagedData.result;
        if (status == "FOUND") {
          if (this.accountNumberIndex[segmentIndex]) {
            this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
            this.arrAccountDescription[segmentIndex] = result.dimensionDescription;
            this.arrAccountDescriptionArabic[segmentIndex] = result.dimensionDescriptionArabic;
            this.accountNumber[segmentIndex] = result.dimensionValue;
          }
          else {
            this.arrAccountDescription.push(result.dimensionDescription);
            this.accountNumberIndex.push(result.dimInxValue.toString());
            this.accountNumberIndex.push(result.dimInxValue.toString());
            this.accountNumber.push(result.dimensionValue);
          }
        } else {
          var segment = <HTMLInputElement>document.getElementById('txtSegment_' + segmentIndex);
          segment.value = '';
          this.accountNumberIndex[segmentIndex] = ' ';
          this.arrAccountDescription[segmentIndex] = ' ';
          this.arrAccountDescriptionArabic[segmentIndex] = ' ';
          this.accountNumber[segmentIndex] = '';
          this.toastr.warning(pagedData.btiMessage.message);
        }
      });
    }
    else {
      var currentSegment = segmentIndex;
      this.accountNumberIndex[currentSegment] = '0';
      this.arrAccountDescription[currentSegment] = ' ';
      this.arrAccountDescriptionArabic[currentSegment] = ' ';
      this.accountNumber[currentSegment] = ' ';
    }
  }

  closeModal() {
    this.getSegmentCount();
    this.accountNumberIndex = [];
    this.arrAccountDescription = [];
    this.arrAccountDescriptionArabic = [];
    this.accountNumberIndex = [];
    this.accountNumber = [];
  }

  getSegmentCount() {
    this.accountStructureService.getAccountStructureSetup().then(data => {
      this.arrSegment = [];
      for (var i = 0; i < data.result.segmentNumber; i++) {
        this.arrSegment.push({ 'segmentNumber': i, 'isReadOnly': "true" });
      }
    });
  }


  openSearchModal(event) {
    console.log()
    if (event.target.value === '?' || event.target.value === '؟') {
      if (event.path[2].id == 'accountNumberDebit') {
        this.isArrDebitAccountNumber = true;
        this.isArrAccountNumber = !this.isArrDebitAccountNumber;
      } else if (event.path[2].id == 'accountNumber') {
        this.isArrAccountNumber = true;
        this.isArrDebitAccountNumber = !this.isArrAccountNumber;
      }
      document.getElementById('searchPopupButton').click();
      window.setTimeout(() => { document.getElementById('SearchAccountNumber').focus() }, 500);
    } else if (event.target.value === '+' || event.target.value === '+') {
      document.getElementById('createAccountNumberPopup').click();
    }
  }

  receiveMessage(event) {
    let selectedItem;
    if (this.isArrAccountNumber) {
      selectedItem = this.arrAccountNumber.find(x => x.id == event.accountTableRowIndex);
      this.changeCreditAccountNumber(selectedItem);
    } else if (this.isArrDebitAccountNumber) {
      selectedItem = this.arrDebitAccountNumber.find(x => x.id == event.accountTableRowIndex);
      this.changeDebitAccountNumber(selectedItem);
    }
    this.isArrAccountNumber = false;
    this.isArrDebitAccountNumber = false;
    console.log(selectedItem);
  }

  refreshAccountNumber(event) {
    if (event) {
      this.getAccountNumberList();
    }
  }

  checkMonthValue() {
    if (this.model.creditCardExpireMonth <= 0 || this.model.creditCardExpireMonth > 12) {
      this.model.creditCardExpireMonth = '';
    }
  }

  checkYearValue(){
    if((this.model.creditCardExpireYear >= 2100) || (this.model.creditCardExpireYear <= 1990)){
      this.model.creditCardExpireYear = '';
    }
  }

  onlyDecimalNumberKey(event) {
    return this.getScreenDetailService.onlyDecimalNumberKey(event);
  }

}
