import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentVaoucherComponent } from './payment-vaoucher.component';

describe('PaymentVaoucherComponent', () => {
  let component: PaymentVaoucherComponent;
  let fixture: ComponentFixture<PaymentVaoucherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentVaoucherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentVaoucherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
