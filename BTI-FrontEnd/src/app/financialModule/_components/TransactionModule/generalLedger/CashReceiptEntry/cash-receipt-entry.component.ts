import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { CashReceiptSetup } from '../../../../../financialModule/_models/transactionModule/generalLedger/cashReceiptEntry';
import { CashReceiptService } from '../../../../../financialModule/_services/transactionModule/generalLedger/cashReceiptEntry.service';
import { BatchSetupService } from '../../../../../financialModule/_services/transactionModule/generalLedger/batchSetUp.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { AuditTrial } from '../../../../_models/general-ledger-configuration-setup/audit-trial';
declare var $: any
@Component({
    templateUrl: './cash-receipt-entry.component.html',
    providers:[CashReceiptService,CommonService,BatchSetupService]
})

//export to make it available for other classes
export class CashReceiptComponent {
    page = new Page();
    rows = new Array<CashReceiptSetup>();
    moduleCode = Constants.financialModuleCode;
    currentTransactionType=Constants.batchTransacitonType.CASH_RECEIPT_ENTRY;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    select=Constants.select;
    emptyArray=[{'id':'0','text':this.select}];    
    defaultAddFormValues: Array<object>;
    defaultExchangeRateFormValues: Array<object>;
    defaultDistributionFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    distributionScreenModuleName = '';
    exchangeRateScreenModuleName = '';
    availableFormValues: [object];
    searchKeyword = '';
    selected = [];
    isApprovedCheck: number=0;
    arrTransactionType = this.emptyArray;
    messageText;
    hasMsg = false;
    showMsg = false;
    isApproved:boolean=false;
    isSuccessMsg;
    isfailureMsg;
    creditCardExpireYear;
    ddPageSize = 5;
    model: any = {};
    audtiTrial: string;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    btnCancelText=Constants.btnCancelText;
    isModify:boolean=false;
    isPostEvt:boolean=false;
    showBtns:boolean=false;
    arrReceiptType:any=[];
    arrPaymentMethod:any=[];
    arrExchangeDetail = this.emptyArray;
    // Searchable Dropdown
    activeBatch:any=this.emptyArray;
    activeReceiptNumber:any=this.emptyArray;
    activeAccountNumber:any=this.emptyArray;
    activeAccountNumberDebit:any=this.emptyArray;
    activeCurrency:any=this.emptyArray;
    activeVat:any=this.emptyArray;
    activeCheckbook:any=this.emptyArray;
    activeCustomer:any=this.emptyArray;
    activeVendor:any=this.emptyArray;
    activeExchangeDetail=this.emptyArray;
    activeCreditCard:any=this.emptyArray;
    
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isDeleteAction : boolean = false;
    isConfirmationModalOpen:boolean=false;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    vendorName:string;
    arrBatchList =  this.emptyArray;
    arrReceiptNumber=[];
    arrAccountNumber=[];
    arrAccountNumberDebit = [];
    arrCurrency=[];
    arrCheckbook=[];
    arrCustomer=[];
    arrVendor=[];
    arrVat=[];
    arrCreditCard=[];
    checkBookDescription:string;
    customerName:string;
    vendorId:string;
    accountDescription:string;
    accountDebitDescription:string;
    vatPercentage:number;
    tempvatPercentage:number;
    tempRecieptAmount:number = 0;
    totalReceipt:number;
    selectedCurrency;
    editReceiptAmount;
    unFrmtReceiptAmount;
    unFrmtTotalReceipt;
    unFrmtVatAmount;
    editVatAmount:number = 0;
    calculatedVatAmount;
    isArrAccountNumber:boolean;
    isArrAccountNumberDebit:boolean;
    btndisabled:boolean=false;
    arrDistributionEntry:any={
        "receiptNumber": "",
        "checkBookId": "",
        "depositorName": "",
        "transactionType": "",
        "accountDetailDebit": {
            "accountDescription": "",
            "debitAmount": '',
            "accountNumber": ""
        },
        "accountDetailVat": {
            "accountDescription": "",
            "accountNumber": "",
            "vatAmount":''
        },
        "accountDetailCredit": {
            "accountDescription": "",
            "accountNumber": "",
            "creditAmount":''
        }
    }
    private value:any = {};
    
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
    
    private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
    onDateChanged(event: IMyDateModel): void {
        var RequestedData =  {
            "transactionDate":event.formatted,
            "moduleName":"GL",
            "originId":Constants.massCloseOriginType.GL_Cash_Receipt_Entry
        }
        
        this.commonService.checkTransactionDateIsValid(RequestedData).then(data => {
            
            if(data.btiMessage.messageShort == "INVALID_TRANSACTION_DATE")
            {
                this.model.receiptDate = '';
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });
    }
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private cashReceiptService: CashReceiptService,
        private commonService: CommonService,
        private batchSetupService: BatchSetupService)
        {
            
        }
        
        ngOnInit() 
        {
            this.GetAddScreenDetail();
            this.GetExchangeRateScreenDetail();
            this.GetUniqueReceiptNumber();
            this.GetRecieptNumber();
            this.GetCreditCardSetup();
            this.GetRecieptType();
            //this.getBatchList();
            this.GetPaymentMethod();
            this.getGlAccountNumberList();
            this.GetCurrencySetup();
            this.GetCheckbookMaintenance();
            this.GetVatDetailSetup();
            this.GetCustomerList();
            this.GetVendorList();
            this.GetDistributionScreenDetail();
            this.unFrmtTotalReceipt = 0;
            this.unFrmtReceiptAmount = 0;
            this. changePaymentMethod({value:1});
            
        }
        
        GetAddScreenDetail()
        {
            this.screenCode = "S-1233";
            this.defaultAddFormValues = [
                { 'fieldName': 'CASH_RECP_ENTRY_RECEIPT_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_RECEIPT_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_RECEIPT_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_DEPOSITOR_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_TAX_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_PAYMENT_METHOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_CHECK_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_BANK_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_CREDIT_CARD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_CREDIT_CARD_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_EXPIRE_DATE_MM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_EXPIRE_DATE_YY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_TAX_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_TOTAL_RECEIPT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_APPLY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'CASH_RECP_ENTRY_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
            this.getScreenDetail(this.screenCode,'Add');
        }
        
        getScreenDetail(screenCode,ArrayType)
        {
            
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => 
                {
                    this.screenName=data.result.dtoScreenDetail.screenName;
                    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                    if(ArrayType == 'Add')
                    {
                        this.moduleName=data.result.moduleName;
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                    else if(ArrayType == 'Distribution')
                    {
                        this.distributionScreenModuleName=data.result.moduleName;
                        for (var j = 0; j < this.availableFormValues.length; j++) 
                        {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultDistributionFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                    else if(ArrayType == 'ExchangeRate'){
                        this.exchangeRateScreenModuleName=data.result.moduleName;
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultExchangeRateFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }        
                });
            }
            
            GetUniqueReceiptNumber()
            {
                this.commonService.GetUniqueReceiptNumber().then(data => {
                    this.model.receiptNumber=data.result;
                });
            }
            
            GetDistributionScreenDetail()
            {
                this.screenCode = "S-1234";
                this.defaultDistributionFormValues = [
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_RECEIPT_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_TRAN_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_DEPOSITOR_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_ACCOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_ACCOUNT_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_DEBIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_DIS_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'CASH_RECP_ENTRY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                ];
                this.getScreenDetail(this.screenCode,'Distribution');
            }
            
            GetExchangeRateScreenDetail()
            {
                this.screenCode = "S-1238";
                this.defaultExchangeRateFormValues = [
                    { 'fieldName': 'EX_TABLE_DET_JOURNAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'EX_TABLE_DET_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'EX_TABLE_DET_EX_TABLE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'EX_TABLE_DET_EX_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'EX_TABLE_DET_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'EX_TABLE_DET_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                ];
                this.getScreenDetail(this.screenCode,'ExchangeRate');
            }
            
            // Get Reciept Number
            GetRecieptNumber()
            {
                
                this.cashReceiptService.GetRecieptNumber().then(data => {
                    this.arrReceiptNumber=[{'id':'0','text':this.select}];
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.length;i++)	
                        {
                            this.arrReceiptNumber.push({'id':data.result[i],'text':data.result[i]});
                        }
                    }
                });
            }
            
            // get Batch List
            getBatchList(){
                this.commonService.getAllBatchesListByTransactionType(this.currentTransactionType).then(data => {
                    this.arrBatchList=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.length;i++)	
                        {
                            this.arrBatchList.push({'id':data.result[i].batchId,'text':data.result[i].batchId});
                        }
                    }
                });
            }

            // get Batches By Payment method
            getBatchListByPaymentMethod(sourceDocumentId: number, currentBatch?){
                this.commonService.getAllBatchesListByPaymentMethod(sourceDocumentId).then(data =>{
                    this.arrBatchList = [{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND"){
                        for(let i = 0 ; i< data.result.length;i++){
                            this.arrBatchList.push({'id':data.result[i].batchId,'text':data.result[i].batchId});
                        }
                        if(currentBatch){
                            let activeSelectedBatch = this.arrBatchList.find(x => x.id == currentBatch);
                            this.bindSelectedItem(activeSelectedBatch,'Batch');
                        }
                    }
                });
            }
            
            // Get Reciept Type
            GetRecieptType()
            {
                this.cashReceiptService.GetReceiptType().then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.arrReceiptType = data.result;
                        
                        this.arrReceiptType.splice(0, 0, { "typeId": "", "name": this.select });
                        this.model.receiptType='3';
                    }
                });
            }
            
            //Get Payment Method
            GetPaymentMethod()
            {
                this.cashReceiptService.GetPaymentMethod().then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.arrPaymentMethod = data.result;
                        this.model.paymentMethod='1';
                    }
                });
            }

            
            // Get Credit Card Setup List
            GetCreditCardSetup()
            {
                this.commonService.GetCreditCardSetup().then(data => {
                    
                    this.arrCreditCard=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrCreditCard.push({'id':data.result.records[i].cardIndex,'text':data.result.records[i].cardId});
                        }
                    }
                });
            }
            
            //Bind Account Number
            getGlAccountNumberList()
            {
                this.arrAccountNumber = [{ 'id': '0', 'text': this.select }];
                this.arrAccountNumberDebit = [{ 'id': '0', 'text': this.select }];
                this.commonService.getGlAccountNumberListNew().then(data => {
                    this.arrAccountNumber = [{ 'id': '0', 'text': this.select }];
                    this.arrAccountNumberDebit = [{ 'id': '0', 'text': this.select }];
                    if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                        for (var i = 0; i < data.result.length; i++) {
                            // this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                            this.arrAccountNumber.push({
                                'id': data.result[i].accountTableRowIndex, 'text':
    
                                    (data.result[i].accountNumber ? data.result[i].accountNumber : '')/*+ '<*>' + (data.result[i].accountDescription ? data.result[i].accountDescription : '')
                                    + '<*>' + (data.result[i].accountDescriptionArabic ? data.result[i].accountDescriptionArabic : '')*/
                            });
    
                            this.arrAccountNumberDebit.push({
                                'id': data.result[i].accountTableRowIndex, 'text':
    
                                    (data.result[i].accountNumber ? data.result[i].accountNumber : '')/*+ '<*>' + (data.result[i].accountDescription ? data.result[i].accountDescription : '')
                                    + '<*>' + (data.result[i].accountDescriptionArabic ? data.result[i].accountDescriptionArabic : '')*/
                            });
                        }
                    }
                });
            }
            // Get Currency Setup List
            GetCurrencySetup()
            {
                this.commonService.getCurrencySetup().then(data => {
                    this.arrCurrency = [{ 'id': '0', 'text': this.select }];
                    if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                        var fatchData = data.result;
                        for (var i = 0; i < data.result.records.length; i++) {
                            this.arrCurrency.push({ 'id': data.result.records[i].currencyId, 'text': data.result.records[i].currencyId });
                        }
                        var defaultCurrency: any = [];
                        defaultCurrency = data.result.records.find(x => x.defaultCurrency == true);
                        console.log(defaultCurrency);
                        this.bindSelectedItem({ 'id': defaultCurrency.currencyId, 'text': defaultCurrency.currencyId }, 'Currency');
                    }
                });
            }
            //Get checkbook Maintenance
            GetCheckbookMaintenance(){
                {
                    this.commonService.GetCheckbookMaintenance().then(data => {
                        this.arrCheckbook=[{'id':'0','text':this.select}];
                        
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrCheckbook.push({'id':data.result.records[i].checkBookId,'text':data.result.records[i].checkBookId});
                            }
                        }
                    });
                }
            }
            
            // Get Customer List
            GetCustomerList()
            {
                this.commonService.GetCustomerList().then(data => {
                    this.arrCustomer=[{'id':'0','text':this.select}];
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrCustomer.push({'id':data.result.records[i].customerId,'text':data.result.records[i].customerId});
                        }
                    }
                });
            }
            
            // Get Vendor List
            GetVendorList()
            {
                this.commonService.GetVendorList().then(data => {
                    this.arrVendor=[{'id':'0','text':this.select}];
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrVendor.push({'id':data.result.records[i].venderId,'text':data.result.records[i].venderId});
                        }
                    }
                });
            }
            
            GetCheckBookMaintenanceByCheckbookId(checkBookId)
            {
                this.cashReceiptService.GetCheckBookMaintenanceByCheckbookId(checkBookId).then(data => {
                    this.checkBookDescription=data.result.checkbookDescription;
                });
            }
            
            
            GetCustomerByCustomerId(customerId)
            {
                this.cashReceiptService.maintenanceGetById(customerId).then(data => {
                    this.customerName=data.result.name;
                });
            }
            
            GetVendorNameByVenderId(venderId)
            {
                this.commonService.GetvendormaintenanceById(venderId).then(data => {
                    this.vendorName=data.result.venderNamePrimary;
                });
            }
            
            GetAccountDescription(accountTableRowIndex)
            {
                this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.accountDescription=data.result.accountDescription;
                    }
                });
            }

            GetAccountDebitDescription(accountTableRowIndex) {
                this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
                    if (data.btiMessage.messageShort != "RECORD_NOT_FOUND") {
                        this.accountDebitDescription = data.result.accountDescription;
                    }
                    else {
                        this.accountDebitDescription = data.btiMessage.message;
                    }


                });
            }
            
            GetCashReceiptEntryByReceiptNumber(receiptNumber)
            {
                if(receiptNumber != '0')
                {
                    this.cashReceiptService.getCashReceiptEntryByReceiptNumber(receiptNumber).then(data => {
                        
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        { 
                            console.log(data);
                            this.isModify=true;
                            this.showBtns=true;
                            this.isPostEvt=false;
                            this.model=data.result;
                            this.changePaymentMethod({value:this.model.paymentMethod},data.result.batchID);
                            this.totalReceipt = this.model.receiptAmount;
                            this.calculatedVatAmount = data.result.vatAmount
                            this.unFrmtReceiptAmount = data.result.receiptAmount
                            this.selectedCurrency = data.result.currencyID
                            this.formatreceiptAmount();
                            this.formatTotalAmount();
                            this.formatVatAmount();
                            if(data.result.receiptDate) {
                                var receiptDate = data.result.receiptDate;
                                var receiptDateData = receiptDate.split('/');
                                this.model.receiptDate = {"date":{"year":parseInt(receiptDateData[2]),"month":parseInt(receiptDateData[1]),"day":parseInt(receiptDateData[0])}};
                            }
                            
                            if(data.result.accountTableRowIndex)
                            {
                                
                                var selectedAccountNumber = this.arrAccountNumber.find(x => x.id ==   data.result.accountTableRowIndex);
                                this.bindSelectedItem(selectedAccountNumber,'AccountNumber');
                            }
                            
                            //if(data.result.batchID)
                            //{
                                
                            //    var selectedBatch = this.arrBatchList.find(x => x.id ==   data.result.batchID);
                            //    this.bindSelectedItem(selectedBatch,'Batch');
                            //}
                            // else{
                            //     this.bindSelectedItem(this.emptyArray[0],'Batch');
                            // }
                            
                            var selectedCheckbook = this.arrCheckbook.find(x => x.id ==   data.result.checkBookId);
                            if(selectedCheckbook){
                                this.bindSelectedItem(selectedCheckbook,'Checkbook');
                            }
                            
                            
                            if(data.result.paymentMethod == "3")
                            {
                                var selectedCreditCard = this.arrCreditCard.find(x => x.id ==   data.result.creditCardID);
                                this.bindSelectedItem(selectedCreditCard,'CreditCard');
                            }else if(data.result.paymentMethod == "1"){
                                var selectedAccountNumberDebit = this.arrAccountNumberDebit.find(x => x.id ==   data.result.accountTableRowIndexDebit);
                                this.bindSelectedItem(selectedAccountNumberDebit,'accountNumberDebit');
                            }
                            var sourceCurrency = this.arrCurrency.find(x => x.id ==   data.result.currencyID);
                            this.bindSelectedItem(sourceCurrency,'Currency');
                            var exchangeTableIndex = data.result.exchangeTableIndex;
                            
                            window.setTimeout(() => {
                                var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id ==  exchangeTableIndex);
                                this.bindSelectedItem(selectedExchangeDetail,'ExchangeDetail');
                            }, 2000);
                            
                            if(data.result.customerId)
                            {
                                var selectedCustomer = this.arrCustomer.find(x => x.id ==   data.result.customerId);
                                this.bindSelectedItem(selectedCustomer,'Customer');
                            }
                            
                            if(data.result.vendorId)
                            {
                                var selectedVendor = this.arrVendor.find(x => x.id ==   data.result.vendorId);
                                this.bindSelectedItem(selectedVendor,'Vendor');
                            }
                            
                            
                            var selectedVat = this.arrVat.find(x => x.id ==   data.result.vatScheduleID);
                            if(selectedVat){
                                this.bindSelectedItem(selectedVat,'Vat');
                                this.GetVatDetailSetupById(selectedVat.id);
                            }
                            
                            
                            
                            window.setTimeout(() => {
                                this.CalculateTaxAmount('TaxSchedule');
                            }, 500);
                            
                            
                        }
                        else{
                            this.isModify=false;
                        }
                    });
                }
                else{
                    // this.getGLNextJournalEntry();
                    this.isModify=false;
                    this.model.receiptType='';
                    this.model.paymentMethod='1';;
                    this.model={};
                    this.customerName='';
                    this.vendorId='';
                    this.accountDescription='';
                    this.checkBookDescription='';
                    this.totalReceipt=0;
                    this.model.receiptDate='';
                    this.GetRecieptNumber();
                    this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
                    this.bindSelectedItem(this.emptyArray[0],'Currency');
                    this.bindSelectedItem(this.emptyArray[0],'Customer');
                    this.bindSelectedItem(this.emptyArray[0],'Checkbook');
                    this.bindSelectedItem(this.emptyArray[0],'Vat');
                    this.bindSelectedItem(this.emptyArray[0],'Vendor');
                }
            }
            
            //Get Vat Detail Setup
            GetVatDetailSetup()
            {
                this.commonService.GetVatDetailSetup().then(data => {
                    this.arrVat=[{'id':'0','text':this.select}];
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrVat.push({'id':data.result.records[i].vatScheduleId,'text':data.result.records[i].vatDescription});
                        }
                    }
                });
            }
            
            GetVatDetailSetupById(vatScheduleId)
            {
                this.cashReceiptService.GetVatDetailSetupById(vatScheduleId).then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.tempvatPercentage = this.vatPercentage;
                        this.vatPercentage=data.result.basperct;     
                        
                        this.CalculateTaxAmount('TaxSchedule');
                    }
                });
            }
            GetExchangeTableSetupByCurrencyId(currencyId: string){
                this.commonService.getExchangeTableSetupByCurrencyId(currencyId).then(data => {
                    this.arrExchangeDetail=[{'id':'0','text':this.select}];
                    this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail')
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrExchangeDetail.push({'id':data.result.records[i].exchangeTableIndex,'text':data.result.records[i].exchangeId});
                            if(data.result.records[i].default){
                                this.bindSelectedItem({'id':data.result.records[i].exchangeTableIndex,'text':data.result.records[i].exchangeId},'ExchangeDetail');
                            }
                        }
                    }
                });
            }
            
            GetCurrencyExchangeDetail(exchangeId: string){
                this.commonService.GetCurrencyExchangeDetail(exchangeId).then(data => {
                    
                    if(data.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND")
                    {
                        if(!this.model.exchangeRate)
                        {
                            this.model.exchangeRate=data.result.exchangeRate;
                        }      
                        
                        this.model.exchangeExpirationDate=data.result.exchangeExpirationDate;
                    }
                });
            }
            
            GetCashReceiptDistributionByReceiptNumber(receiptNumber: string){
                this.cashReceiptService.GetCashReceiptDistributionByReceiptNumber(receiptNumber).then(data => {
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.arrDistributionEntry=data.result;
                    }
                });
            }
            
            CalculateTaxAmount(actionType)
            {
                if(!this.model.vatAmount)
                {
                    this.model.vatAmount=0;
                }
                if(!this.model.receiptAmount)
                {
                    this.model.receiptAmount=0;
                }
                if(!this.totalReceipt)
                {
                    this.totalReceipt=0;
                }
                //this.formatreceiptAmount()
                if(!this.vatPercentage)
                {
                    this.vatPercentage=0;
                }
                if (!this.unFrmtReceiptAmount)
                {
                    this.model.receiptAmount = 0;
                }
                else{
                    this.model.receiptAmount = this.unFrmtReceiptAmount;
                }
                if(actionType == 'TaxSchedule')
                {
                   
                    if((!this.calculatedVatAmount || !this.unFrmtVatAmount) || this.tempvatPercentage !=this.vatPercentage || this.tempRecieptAmount != this.model.receiptAmount)
                    {
                        if(this.vatPercentage > 0)
                        {
                            this.model.vatAmount = this.model.receiptAmount*this.vatPercentage/100;
                            var amt = this.model.vatAmount;
                            if(!Number.isInteger(amt))
                            {
                                
                                amt = amt.toString().split(".");
                                if(amt.length > 1 && amt[1].length >= 1){
                                    this.calculatedVatAmount = amt[0]+"."+amt[1].slice(0, 3)
                                }else {
                                    this.calculatedVatAmount = amt;
                                }
                            }
                            else{
                                if(amt == 0){
                                    this.calculatedVatAmount = amt;
                                    this.model.vatAmount = amt;
                                }else{
                                    this.calculatedVatAmount = this.model.vatAmount;
                                }
                                
                            }
                            
                            // if(amt.length > 1 && amt[1].length >= 3){
                            //     this.calculatedVatAmount = amt[0]+"."+amt[1].slice(0, 3)
                            // }else{
                            //     this.calculatedVatAmount = this.model.vatAmount
                            // }
                            
                        }
                        else{
                            this.model.vatAmount =0;
                            this.calculatedVatAmount = this.model.vatAmount;
                        }
                    }
                    var vatAmount = '0';
                    if (this.calculatedVatAmount){
                        vatAmount = this.calculatedVatAmount
                    }
                    else if(this.unFrmtVatAmount){
                        vatAmount = this.unFrmtVatAmount
                    }else{
                        vatAmount = '0'
                    }
                    this.unFrmtVatAmount = vatAmount;
                    this.totalReceipt= parseFloat(this.model.receiptAmount) + parseFloat(vatAmount);
                    this.tempRecieptAmount=this.model.receiptAmount;
                    //  this.model.vatAmount = this.model.receiptAmount*this.vatPercentage/100;
                    //this.totalReceipt= parseFloat(this.model.receiptAmount) + parseFloat(this.model.vatAmount);
                    this.unFrmtTotalReceipt = this.totalReceipt;
                }
                
                else if(actionType == 'ReceiptAmount')
                {
                    if(!this.model.vatAmount)
                    {
                        if(this.vatPercentage > 0)
                        {
                            
                            this.model.vatAmount = this.model.receiptAmount*this.vatPercentage/100;
                            var amt = this.model.vatAmount;
                            if(!Number.isInteger(amt))
                            {
                                amt = amt.split(".")
                                if(amt.length > 1 && amt[1].length >= 3){
                                    this.calculatedVatAmount = amt[0]+"."+amt[1].slice(0, 3)
                                }
                            }
                            else{
                                this.calculatedVatAmount = this.model.vatAmount
                            }
                            // amt = amt.split(".")
                            // if(amt.length > 1 && amt[1].length >= 3){
                            //     this.calculatedVatAmount = amt[0]+"."+amt[1].slice(0, 3)
                            // }else{
                            //     this.calculatedVatAmount = this.model.vatAmount
                            // }
                        }
                        else{
                            this.model.vatAmount =0;
                            this.calculatedVatAmount =  this.model.vatAmount
                        }
                    }
                    
                    var vatAmount = '0';
                    if (this.calculatedVatAmount){
                        vatAmount = this.calculatedVatAmount
                    }
                    else if(this.unFrmtVatAmount){
                        vatAmount = this.unFrmtVatAmount
                    }else{
                        vatAmount = '0'
                    }
                    this.totalReceipt= parseFloat(this.model.receiptAmount) + parseFloat(vatAmount);
                    if(this.model.vatScheduleID)
                    {
                        this.GetVatDetailSetupById(this.model.vatScheduleID);
                    }
                    this.unFrmtTotalReceipt = this.totalReceipt;
                }
                else if(actionType == 'TaxAmount')
                {
                    if(!this.model.vatAmount)
                    {
                        if(this.vatPercentage > 0)
                        {
                            this.model.vatAmount = this.model.receiptAmount*this.vatPercentage/100;
                            var amt = this.model.vatAmount;
                            if(!Number.isInteger(amt))
                            {
                                amt = amt.split(".")
                                if(amt.length > 1 && amt[1].length >= 3){
                                    this.calculatedVatAmount = amt[0]+"."+amt[1].slice(0, 3)
                                }
                            }
                            else{
                                this.calculatedVatAmount = this.model.vatAmount
                            }
                            // amt = amt.split(".")
                            // if(amt.length > 1 && amt[1].length >= 3){
                            //     this.calculatedVatAmount = amt[0]+"."+amt[1].slice(0, 3)
                            // }else{
                            //     this.calculatedVatAmount = this.model.vatAmount
                            // }
                        }
                        else{
                            this.model.vatAmount =0;
                            this.calculatedVatAmount =  this.model.vatAmount
                        }
                    }
                    
                    if (this.calculatedVatAmount){
                        vatAmount = this.calculatedVatAmount
                    }
                    else if(this.unFrmtVatAmount){
                        vatAmount = this.unFrmtVatAmount
                    }else{
                        vatAmount = '0'
                    }
                    this.totalReceipt= parseFloat(this.unFrmtReceiptAmount) + parseFloat(vatAmount);
                    this.totalReceipt = Math.round(this.totalReceipt * 100) / 100;
                    this.unFrmtTotalReceipt = this.totalReceipt;
                }
                if(this.model.receiptAmount != undefined){
                    this.formatreceiptAmount();
                }if(this.model.vatAmount != undefined){
                    this.formatVatAmount();
                }if(this.totalReceipt != undefined){
                    this.formatTotalAmount();
                }
            }
            
            public bindSelectedItem(value:any,type:string):void {
                if(type == 'ReceiptNumber')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeReceiptNumber=[value];
                    }
                    this.model.receiptNumber=this.activeReceiptNumber[0].id;
                    this.GetCashReceiptEntryByReceiptNumber(this.model.receiptNumber);
                }
                if(type == 'Batch')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeBatch=[value];
                    }
                    this.model.batchID=this.activeBatch[0].id;
                }
                if(type == 'AccountNumber')
                {
                    console.log(value);
                    this.value = value;
                    if(value.text)
                    {
                        this.activeAccountNumber=[value];
                    }
                    this.model.accountTableRowIndex=this.activeAccountNumber[0].id;
                    if(this.model.accountTableRowIndex != "0")
                    {
                        this.GetAccountDescription(this.model.accountTableRowIndex);
                    }
                }
                else if(type == 'Currency')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCurrency=[value];
                        this.model.currencyID=this.activeCurrency[0].id;
                        this.activeExchangeDetail= [this.emptyArray[0]];
                        this.arrExchangeDetail=[];
                        this.model.exchangeRate = '';
                        this.model.exchangeExpirationDate = '';
                        if(value.id != "0")
                        {
                            
                            this.selectedCurrency = value.id
                            if(!this.model.receiptAmount)
                            {
                                this.model.receiptAmount=0;
                            }
                            if(!this.model.vatAmount)
                            {
                                this.model.vatAmount=0;
                            }
                            if(!this.totalReceipt)
                            {
                                this.totalReceipt=0;
                            }
                            this.formatreceiptAmount();
                            this.formatVatAmount();
                            this.formatTotalAmount();
                            // if(this.model.receiptAmount || this.model.receiptAmount == '0'){
                            //     
                            // }if(this.model.vatAmount || this.model.vatAmount == '0'){
                            //     this.formatVatAmount();
                            // }if(this.totalReceipt || this.totalReceipt == 0){
                            //     this.formatTotalAmount();
                            // }
                            this.GetExchangeTableSetupByCurrencyId(value.id);
                        }
                    }
                }
                else if(type == 'Customer')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCustomer=[value];
                        this.model.customerId=this.activeCustomer[0].id;
                        
                        if(this.model.customerId != "0")
                        {
                            this.GetCustomerByCustomerId(this.model.customerId);
                        }
                    }
                }
                else if(type == 'Vendor')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeVendor=[value];
                        this.model.vendorId=this.activeVendor[0].id;
                        
                        if(this.model.vendorId != "0")
                        {
                            this.GetVendorNameByVenderId(this.model.vendorId);
                        }
                    }
                }
                
                else if(type == 'Checkbook')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCheckbook=[value];
                        this.model.checkBookId=this.activeCheckbook[0].id;
                        if(this.model.checkBookId != "0")
                        {
                            this.GetCheckBookMaintenanceByCheckbookId(this.model.checkBookId);
                        }
                    }
                }
                else if(type == 'Vat')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeVat=[value];
                        this.model.vatScheduleID=this.activeVat[0].id;
                        if(this.model.vatScheduleID != "0")
                        {
                            this.GetVatDetailSetupById(this.model.vatScheduleID);
                        }else{
                            this.calculatedVatAmount = 0;
                            this.model.vatAmount = 0;
                            this.vatPercentage = 0;
                            this.unFrmtVatAmount = 0;
                            this.CalculateTaxAmount('TaxSchedule');
                        }
                        
                    }
                }
                else if(type == 'ExchangeDetail')
                {
                    this.value = value;
                    if(value.id)
                    {
                        this.activeExchangeDetail=[value];
                        this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                        if(this.model.exchangeTableIndex != "0")
                        {
                            this.GetCurrencyExchangeDetail(value.text);
                        }
                        else{
                            this.model.exchangeRate == '';
                            this.model.exchangeExpirationDate == '';
                        }
                    }
                }
                else if(type == 'CreditCard')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCreditCard=[value];
                    }
                    this.model.creditCardID=this.activeCreditCard[0].id;
                }else if(type == 'accountNumberDebit'){
                    this.value = value;
                    if(value.text){
                        this.activeAccountNumberDebit = [value]
                    }
                    this.model.accountTableRowIndexDebit = this.activeAccountNumberDebit[0].id;
                    if(this.model.accountTableRowIndexDebit != '0'){
                        this.GetAccountDebitDescription(this.model.accountTableRowIndexDebit);
                    }

                }
            }
            
            // Save Update Receipt Setup
            SaveUpdateCashReceipt(f: NgForm,myModal) {
                
                this.btndisabled=true;
                // if(!this.editVatAmount)
                // {
                //     this.editVatAmount = 0;
                // }
                if(this.model.receiptType == 3){
                    if(f.valid && this.activeAccountNumber[0].id != '0' && this.activeCurrency[0].id != '0' &&  this.activeExchangeDetail[0].id != '0')
                    {
                        if((this.activeAccountNumberDebit[0].id == '0' && this.activeCreditCard[0].id == '0' && this.activeCheckbook[0] == '0') ) 
                        {
                            return false;
                        }else if(this.model.paymentMethod == "2" &&  this.model.checkBookId == '0'){
                            return false;
                        }
                        
                        if(this.model.receiptDate.formatted == undefined)
                        {
                            var receiptDate = this.model.receiptDate;
                            if(receiptDate.date != undefined)
                            {
                                this.model.receiptDate = receiptDate.date.day +'/'+ receiptDate.date.month +'/'+ receiptDate.date.year;
                            }
                        }
                        else
                        {
                            this.model.receiptDate = this.model.receiptDate.formatted;
                        }
                        this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                        if(this.activeCreditCard[0].id == '0')
                        {
                            this.model.creditCardID = '0';
                        }
                        this.model.receiptAmount = this.unFrmtReceiptAmount;
                        this.model.vatAmount = this.editVatAmount
                        
                        this.cashReceiptService.saveUpdateCashReceiptEntry(this.model,this.isModify).then(data => {
                            window.scrollTo(0,0);
                            var datacode = data.code;
                            this.btndisabled=false;
                            if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY'){
                                this.tempRecieptAmount = 0;
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.isModify=false;
                                this.showBtns=false;
                                
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.GetCurrencySetup();
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            f.resetForm();
                            this.clear(f);
                            this.bindSelectedItem(this.emptyArray[0],'ReceiptNumber');    
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                        
                    }
                    else{
                        
                        if(this.activeExchangeDetail[0].id == '0')
                        {
                            myModal.open();
                        }
                    }
                    this.btndisabled=false;
                }
                else if(this.model.receiptType == 2){
                    // this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
                    // this.accountDescription='';
                    if(f.valid  && this.activeCurrency[0].id != '0' && this.activeVendor[0].id != '0'
                    && this.activeVat[0].id != '0' && this.activeCheckbook[0].id != '0' && this.activeExchangeDetail[0].id != '0')
                    {
                        if(this.model.paymentMethod == "3"  && this.activeCreditCard[0].id == '0')
                        {
                            return false;
                        }
                        
                        if(this.model.receiptDate.formatted == undefined)
                        {
                            var receiptDate = this.model.receiptDate;
                            if(receiptDate.date != undefined)
                            {
                                this.model.receiptDate = receiptDate.date.day +'/'+ receiptDate.date.month +'/'+ receiptDate.date.year;
                            }
                        }
                        else
                        {
                            this.model.receiptDate = this.model.receiptDate.formatted;
                        }
                        this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                        if(this.activeCreditCard[0].id == '0')
                        {
                            this.model.creditCardID = '0';
                        }
                        this.model.receiptAmount = this.unFrmtReceiptAmount;
                        this.model.vatAmount = this.editVatAmount
                        
                        this.cashReceiptService.saveUpdateCashReceiptEntry(this.model,this.isModify).then(data => {
                            window.scrollTo(0,0);
                            var datacode = data.code;
                            this.btndisabled=false;
                            if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.isModify=false;
                                this.showBtns=false;
                                
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            f.resetForm();
                            this.clear(f);
                            this.bindSelectedItem(this.emptyArray[0],'ReceiptNumber');    
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                        
                    }
                    else{
                        
                        if(this.activeExchangeDetail[0].id == '0')
                        {
                            myModal.open();
                        }
                    }
                }
                else if(this.model.receiptType == 1){
                    // this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
                    // this.accountDescription='';
                    if(f.valid  && this.activeCurrency[0].id != '0' && this.activeCustomer[0].id != '0'
                      && this.activeExchangeDetail[0].id != '0')
                    {
                        if(this.model.paymentMethod == "3"  && this.activeCreditCard[0].id == '0')
                        {
                            return false;
                        }
                        
                        if(this.model.receiptDate.formatted == undefined)
                        {
                            var receiptDate = this.model.receiptDate;
                            if(receiptDate.date != undefined)
                            {
                                this.model.receiptDate = receiptDate.date.day +'/'+ receiptDate.date.month +'/'+ receiptDate.date.year;
                            }
                        }
                        else
                        {
                            this.model.receiptDate = this.model.receiptDate.formatted;
                        }
                        this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                        if(this.activeCreditCard[0].id == '0')
                        {
                            this.model.creditCardID = '0';
                        }
                        this.model.receiptAmount = this.unFrmtReceiptAmount;
                        this.model.vatAmount = this.editVatAmount
                        
                        this.cashReceiptService.saveUpdateCashReceiptEntry(this.model,this.isModify).then(data => {
                            window.scrollTo(0,0);
                            var datacode = data.code;
                            this.btndisabled=false;
                            if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.isModify=false;
                                this.showBtns=false;
                                
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            f.resetForm();
                            this.clear(f);
                            this.bindSelectedItem(this.emptyArray[0],'ReceiptNumber');    
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                        
                    }
                    else{
                        
                        if(this.activeExchangeDetail[0].id == '0')
                        {
                            myModal.open();
                        }
                    }
                }
                this.btndisabled=false;
            }
            
            // Delete Journal Entry
            DeleteCashReciept(f: NgForm)
            {
                if(!this.model.receiptNumber)
                {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = this.defaultAddFormValues[0]['listDtoFieldValidationMessage'][0]['validationMessage'];
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    
                }
                else{
                    this.cashReceiptService.DeleteCashReciept(this.model.receiptNumber).then(data =>
                        {
                            window.scrollTo(0,0);
                            this.closeModal();
                            if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.isModify=false;
                                this.showBtns=false;
                                this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            f.resetForm();
                            this.clear(f);                 
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                    }
                }
                
                // Post Cash Receipt
                PostCashReceipt(f: NgForm,myModal) {
                    this.btndisabled=true;
                    this.isPostEvt=true;
                    if(f.valid && this.activeBatch[0].id != '0')
                    {
                        window.scrollTo(0,0);
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText =this.defaultAddFormValues[24]['listDtoFieldValidationMessage'][0]['validationMessage'];
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                        
                        if(f.valid  && this.activeAccountNumber[0].id != '0' 
                        && this.activeCurrency[0].id != '0' && this.activeExchangeDetail[0].id != '0')
                        {
                            
                            if(this.model.paymentMethod == "1"  && this.activeAccountNumberDebit[0].id == '0')
                            {
                                return false;
                            }else if(this.model.paymentMethod == "2" &&  this.model.checkBookId == '0'){
                                return false;
                            }
                            
                            this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                            if(this.model.receiptDate.formatted == undefined)
                            {
                                var receiptDate = this.model.receiptDate;
                                if(receiptDate.date != undefined)
                                {
                                    this.model.receiptDate = receiptDate.date.day +'/'+ receiptDate.date.month +'/'+ receiptDate.date.year;
                                }
                            }
                            else
                            {
                                this.model.receiptDate = this.model.receiptDate.formatted;
                            }
                            
                            this.model.batchID='';
                            if(this.activeCreditCard[0].id == '0')
                            {
                                this.model.creditCardID = '0';
                            }
                            this.model.receiptAmount = this.unFrmtReceiptAmount;
                            this.model.vatAmount = this.editVatAmount
                            this.cashReceiptService.saveUpdateCashReceiptEntry(this.model,this.isModify).then(data => {
                                var datacode = data.code;
                                this.btndisabled=false;
                                if (datacode == 200) {
                                    this.cashReceiptService.PostCashReceiptEntry(this.model).then(data => {
                                        window.scrollTo(0,0);
                                        var datacode = data.code;
                                        this.btndisabled=false;
                                        
                                        if (datacode == 200) {
                                            this.isSuccessMsg = true;
                                            this.isfailureMsg = false;
                                            this.showMsg = true;
                                            this.hasMsg = true;
                                            this.messageText = data.btiMessage.message;
                                            this.tempRecieptAmount = 0;
                                            this.isModify=false;
                                            this.showBtns=false;
                                            this.clear(f);
                                            f.resetForm();
                                            window.setTimeout(() => {
                                                this.showMsg = false;
                                                this.hasMsg = false;
                                            }, 4000);
                                            this.GetCurrencySetup();
                                        }
                                        else{
                                            this.isSuccessMsg = false;
                                            this.isfailureMsg = true;
                                            this.isModify=true;
                                            this.showMsg = true;
                                            this.hasMsg = true;
                                            this.messageText = data.btiMessage.message;
                                            window.setTimeout(() => {
                                                this.showMsg = false;
                                                this.hasMsg = false;
                                            }, 4000);
                                        }
                                    }).catch(error => {
                                        window.setTimeout(() => {
                                            this.isSuccessMsg = false;
                                            this.isfailureMsg = true;
                                            this.showMsg = true;
                                            this.hasMsg = true;
                                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                                        }, 100)
                                    });
                                }
                                
                            })
                            
                            
                        }
                        else{
                            if(this.activeExchangeDetail[0].id == '0')
                            {
                                myModal.open();
                            }
                        }
                    }
                    this.btndisabled=false;
                }
                
                varifyDelete()
                {
                    this.isDeleteAction=true;
                    this.confirmationModalBody = this.deleteConfirmationText;
                    this.isConfirmationModalOpen = true;
                }
                
                
                closeModal()
                {
                    this.isDeleteAction=false;
                    this.isConfirmationModalOpen = false;
                }
                
                SaveCashReceiptEntryDistribution (myModel){
                    this.isPostEvt=false;
                    // 
                    window.scrollTo(0,0);
                    this.cashReceiptService.SaveCashReceiptDistribution(this.model.receiptNumber).then(data => {
                        if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY') {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            
                            this.arrDistributionEntry={
                                "receiptNumber": "",
                                "checkBookId": "",
                                "depositorName": "",
                                "transactionType": "",
                                "accountDetailDebit": {
                                    "accountDescription": "",
                                    "debitAmount": '',
                                    "accountNumber": ""
                                },
                                "accountDetailVat": {
                                    "accountDescription": "",
                                    "accountNumber": "",
                                    "vatAmount":''
                                },
                                "accountDetailCredit": {
                                    "accountDescription": "",
                                    "accountNumber": "",
                                    "creditAmount":''
                                }
                            }
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        myModel.close();
                    });
                }    
                
                openModel(myModal,action)
                {
                    
                    if(action == 'cashReceiptEntryDistribution')
                    {
                        
                        this.GetCashReceiptDistributionByReceiptNumber(this.model.receiptNumber);
                    }
                    myModal.open();
                }   
                
                clear(f){
                    
                    if(!this.isModify)
                    {
                        this.model.receiptNumber='';
                        this.GetUniqueReceiptNumber();
                    }
                    else{
                        this.model.receiptNumber=this.activeReceiptNumber[0].id;
                        
                    }
                    this.isPostEvt=false;
                    //f.resetForm();
                    this.GetRecieptType();
                    this.GetPaymentMethod();
                    this.GetCreditCardSetup();
                    this.GetRecieptNumber();
                    //this.GetUniqueReceiptNumber();
                    this.model.receiptDescription='';
                    this.checkBookDescription='';
                    this.model.receiptDate='';
                    this.model.depositorName='';
                    this.customerName='';
                    this.model.vendorName='';
                    this.accountDescription='';
                    this.model.receiptAmount='';
                    this.totalReceipt=0;
                    this.model.vatAmount='';
                    this.model.checkNumber='';
                    this.model.bankName='';
                    this.model.creditCardNumber='';
                    this.model.creditCardExpireMonth='';
                    this.model.creditCardExpireYear='';
                    this.bindSelectedItem(this.emptyArray[0],'Batch');
                    //this.bindSelectedItem(this.emptyArray[0],'ReceiptNumber');
                    this.bindSelectedItem(this.emptyArray[0],'CreditCard');
                    this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
                    this.bindSelectedItem(this.emptyArray[0],'accountNumberDebit');
                    this.bindSelectedItem(this.emptyArray[0],'Currency');
                    this.bindSelectedItem(this.emptyArray[0],'Customer');
                    this.bindSelectedItem(this.emptyArray[0],'Checkbook');
                    this.bindSelectedItem(this.emptyArray[0],'Vat');
                    this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail');
                    this.bindSelectedItem(this.emptyArray[0],'CreditCard');
                    var drpPaymentTypeId= <HTMLSelectElement>document.getElementById("paymentMethod");
                    drpPaymentTypeId.selectedIndex = 0;
                    this.model.paymentTypeId='1';
                    this.unFrmtReceiptAmount = '0';
                    this.unFrmtTotalReceipt = '0';
                    this.unFrmtVatAmount = '0';
                    this.accountDebitDescription = '';
                    this. changePaymentMethod({value:1});
                }    
                
                Cancel(f){
                    this.clear(f);
                    f.resetForm();
                    this.isModify=false;
                    this.isPostEvt=false;
                    this.bindSelectedItem(this.emptyArray[0],'ReceiptNumber');
                    this.GetUniqueReceiptNumber();
                    this. changePaymentMethod({value:1});
                } 
                
                print()
                {
                    window.print();
                }
                
                onlyDecimalNumberKey(event) {
                    return this.getScreenDetailService.onlyDecimalNumberKey(event);
                }
                
                //receiptAmount
                //add Symbols
                formatreceiptAmount(){
                    
                    this.commonService.changeInputText("receiptAmount")
                    var inputValue = 0;
                    if (!this.unFrmtReceiptAmount){
                        inputValue = 0
                        this.unFrmtReceiptAmount = 0;
                    }else{
                        inputValue = this.unFrmtReceiptAmount
                    }
                    this.editReceiptAmount = inputValue
                    if (this.selectedCurrency !=undefined ){
                        let submitInfo = {
                            'currencyId':this.selectedCurrency
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            
                            if(!this.editVatAmount) {
                                this.formatVatAmount();
                            }
                            if(this.totalReceipt){
                                this.formatTotalAmount()
                            }
                            this.model.receiptAmount = this.commonService.formatAmount(data, inputValue)
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                        
                        
                    }else{
                        this.formatreceiptAmountChanged()
                    }
                    
                    
                }
                //Change To Original Value of receiptAmount
                formatreceiptAmountChanged(){
                    this.commonService.changeInputNumber("receiptAmount")
                    this.model.receiptAmount = this.unFrmtReceiptAmount
                    
                    
                }
                // get Original receiptAmount
                getreceiptAmountValue(event){
                    if(event){
                         var amt = event.split(".")
                        if(amt.length == 1 && amt[0].length >= 20){
                            event = amt[0].slice(0, 20)
                        }
                        else{
                            if(amt.length > 1 && amt[1].length >= 3){
                                event = amt[0]+"."+amt[1].slice(0, 3)
                            }
                            if(amt.length > 1 && amt[0].length >= 20){
                                event = amt[0].slice(0, 20)+"."+amt[1]
                            }
                        } 
                        this.unFrmtReceiptAmount = event
                    }
                }
                //End receiptAmount
                
                formatTotalAmount(){
                    this.commonService.changeInputText("totalReceipt")
                    var inputValue = 0;
                    inputValue = this.unFrmtTotalReceipt
                    
                    if (this.selectedCurrency != undefined ){
                        let submitInfo = {
                            'currencyId':this.selectedCurrency
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.totalReceipt = this.commonService.formatAmount(data, inputValue)
                            //this.unFrmtTotalReceipt =inputValue;
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                        
                        
                    }else{
                        this.totalReceipt = this.totalReceipt
                    }
                    
                    
                }
                //receiptAmount
                //add Symbols
                formatVatAmount(){
                    
                    this.commonService.changeInputText("vatAmount")
                    var inputValue = 0;
                    if(this.calculatedVatAmount && !this.unFrmtVatAmount){
                        inputValue = this.calculatedVatAmount
                    }else if(this.unFrmtVatAmount){
                        inputValue = this.unFrmtVatAmount
                    }else{
                        inputValue = 0
                    }
                    this.editVatAmount = inputValue
                    if (this.selectedCurrency !=undefined ){
                        let submitInfo = {
                            'currencyId':this.selectedCurrency
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.vatAmount = this.commonService.formatAmount(data, inputValue)
                            if(this.totalReceipt){
                                this.formatTotalAmount()
                            }
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                        
                        
                    }else{
                        this.formatvatAmountChanged()
                    }
                    
                    
                }
                //Change To Original Value of vat Amount 
                formatvatAmountChanged(){
                    
                    this.commonService.changeInputNumber("vatAmount")
                    var vatAmount = '0'
                    if (this.calculatedVatAmount && !this.unFrmtVatAmount){
                        vatAmount = this.calculatedVatAmount
                    }
                    else if(this.unFrmtVatAmount){
                        vatAmount = this.unFrmtVatAmount
                    }else{
                        vatAmount = '0'
                    }
                    
                    this.model.vatAmount = vatAmount
                    
                    
                }
                // get Original receiptAmount
                getvatAmountValue(event){
                    
                    this.calculatedVatAmount = ''
                    if(event){
                        var amt = event.split(".");
                        if(amt.length == 1 && amt[0].length >= 15){
                            event = amt[0].slice(0, 15)
                        }
                        else{
                            if(amt.length > 1 && amt[1].length >= 3){
                                event = amt[0]+"."+amt[1].slice(0, 3)
                            }
                            if(amt.length > 1 && amt[0].length >= 15){
                                event = amt[0].slice(0, 15)+"."+amt[1]
                            }
                        }
                        this.unFrmtVatAmount = event
                    }
                }
                //End receiptAmount
                
                checkMonthValue(){
                    if(this.model.creditCardExpireMonth<=0||this.model.creditCardExpireMonth>12){
                        this.model.creditCardExpireMonth='';
                    }
                }

                changePaymentMethod(paymentMethod, batchId?){
                    this.bindSelectedItem({'id':'0','text':'--Select--'},'Batch');
                    let audit = new AuditTrial();
                    audit.sourceCode = 'RV';
                    audit.seriesNumber = paymentMethod.value;
                    this.commonService.getAuditTrialByTransactionType(audit).then(data => {
                        console.log(data);
                        this.model.auditTrial = data.result.seriesIndex;
                        this.audtiTrial = data.result.sourceDocument +':'+data.result.sourceDocument;
                        this.getBatchListByPaymentMethod(data.result.seriesIndex,batchId);
                        
                    });

                    if(paymentMethod.value == 1){
                        this.model.checkNumber = undefined;
                        this.model.bankName = undefined;
                        this.activeCheckbook = this.emptyArray;
                        this.model.checkBookId  = undefined;
    
                        this.activeCreditCard = this.emptyArray;
                        this.model.creditCardNumber = undefined;
                        this.model.creditCardExpireMonth = undefined;
                        this.model.creditCardExpireYear = undefined;
                        this.model.creditCardID = undefined;
                    }else if(paymentMethod.value == 2){
                        this.activeAccountNumberDebit = this.emptyArray;
                        this.accountDebitDescription = undefined;
                        this.model.accountTableRowIndexDebit = undefined;

                        this.activeCreditCard = this.emptyArray;
                        this.model.creditCardNumber = undefined;
                        this.model.creditCardExpireMonth = undefined;
                        this.model.creditCardExpireYear = undefined;
                        this.model.creditCardID = undefined;
                    }else if(paymentMethod.value == 3){
                        this.activeAccountNumberDebit = this.emptyArray;
                        this.accountDebitDescription = undefined;
                        this.model.accountTableRowIndexDebit = undefined;
    
                        this.model.checkNumber = undefined;
                        this.model.bankName = undefined;
                        this.activeCheckbook = this.emptyArray;
                        this.model.checkBookId  = undefined;
                    }
                }
                openSearchModal(event){
                    console.log(event);
                    console.log(event.path[2].id);

                    if(event.target.value === '?' || event.target.value === '؟'){
                        document.getElementById('searchPopupButton').click();
                        window.setTimeout(()=> {document.getElementById('SearchAccountNumber').focus()},500);
                        if(event.path[2].id == 'accountNumberDebit'){
                            this.isArrAccountNumberDebit = true;
                            this.isArrAccountNumber = !this.isArrAccountNumberDebit;
                        }else if(event.path[2].id == 'accountNumber'){
                            this.isArrAccountNumber = true;
                            this.isArrAccountNumberDebit = !this.isArrAccountNumber;
                        }
                    }else if(event.target.value === '+' || event.target.value === '+'){
                        document.getElementById('createAccountNumberPopup').click();
                    }
                }

                receiveMessage(event){
                    console.log(this.arrAccountNumber, this.arrAccountNumberDebit);
                    console.log(event);
                    let selectedItem ;
                    console.log(this.isArrAccountNumber);
                    console.log(this.isArrAccountNumberDebit);
                    if(this.isArrAccountNumber){
                        selectedItem = this.arrAccountNumber.find(x => x.id == event.accountTableRowIndex );
                        this.bindSelectedItem(selectedItem,'AccountNumber');
                    }else if(this.isArrAccountNumberDebit){
                        selectedItem = this.arrAccountNumberDebit.find(x => x.id == event.accountTableRowIndex );
                        this.bindSelectedItem(selectedItem,'accountNumberDebit');
                    }
                    this.isArrAccountNumber = false;
                    this.isArrAccountNumberDebit = false;
                    console.log(selectedItem);
                    
                }
            }
            
            
            
            
            
            
            