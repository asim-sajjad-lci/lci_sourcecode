import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { BankTransferEntrySetup } from '../../../../../financialModule/_models/transactionModule/generalLedger/bankTransferEntry';
import { BankTransferEntryService } from '../../../../../financialModule/_services/transactionModule/generalLedger/bankTransferEntry.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
    templateUrl: './bank-transfer-entry.component.html',
    providers:[BankTransferEntryService,CommonService]
})

//export to make it available for other classes
export class BankTransferComponent {
    page = new Page();
    rows = new Array<BankTransferEntrySetup>();
    moduleCode = Constants.financialModuleCode;
    currentTransactionType=Constants.batchTransacitonType.BANK_TRANSFER;
    screenCode;
    screenName;
    moduleName;
    chargesScreenModuleName = '';
    distributionScreenModuleName = '';
    select=Constants.select;
    emptyArray=[{'id':'0','text':this.select}];    
    defaultAddFormValues: Array<object>;
    defaultDistributionFormValues: Array<object>;
    defaultChargesFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    arrCurrency = this.emptyArray;
    arrExchange=[];
    arrExchangeDetail= this.emptyArray;
    arrBankTransferNumberList = this.emptyArray;
    arrBatchList =  this.emptyArray;
    arrCompany= this.emptyArray;
    arrCheckbook = this.emptyArray;
    arrCheckbookTo = this.emptyArray;
    arrAccountNumber = this.emptyArray;
    searchKeyword = '';
    selected = [];
    messageText;
    hasMsg = false;
    showMsg = false;
    isApproved:boolean=false;
    isChargeNumberExist:boolean=false;
    msgChargeNumberExist:string='';
    isSuccessMsg;
    isfailureMsg;
    ddPageSize = 5;
    model: any = {};
    atATimeText=Constants.atATimeText;
    close=Constants.close;
    tableViewtext=Constants.tableViewtext;
    btnCancelText=Constants.btnCancelText;
    isModify:boolean=false;
    showBtns:boolean=false;
    isConfirmationModalOpen:boolean=false;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isDeleteAction : boolean = false;
    disableBankTrsansferList : boolean = false;
    selectedCurrency;
    unFrmtTransferAmountFrom;
    unFrmtTransferAmountTo;
    unFrmtReceiptAmount;
    btndisabled:boolean=false;
    isScreenLock;
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
   
    private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
     onDateChanged(event: IMyDateModel): void {
        var RequestedData =  {
            "transactionDate":event.formatted,
            "moduleName":"GL",
            "originId":Constants.massCloseOriginType.GL_Bank_Transfer_Entry
        }
        this.commonService.checkTransactionDateIsValid(RequestedData).then(data => {
            
            if(data.btiMessage.messageShort == "INVALID_TRANSACTION_DATE")
            {
                this.model.transferDate = '';
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });
    }
    isPostEvt:boolean = false;
    checkBookCurrencyFrom:string;
    checkBookCurrencyTo:string;
    rateCalcMethodFrom:string;
    rateCalcMethodTo:string;
    checkbookFromAmount:string;
    checkbookToAmount:string;
    accountDescription:string;
    activeBatch:any=this.emptyArray;
    activeBankTransferNumberList:any=this.emptyArray;
    activeCurrency:any=this.emptyArray;
    activeExchangeDetail:any=this.emptyArray;
    activeCompanyFrom:any=this.emptyArray;
    activeCompanyTo:any=this.emptyArray;
    activeCheckbookFrom:any=this.emptyArray;
    activeAccountNumber:any=this.emptyArray;
    activeCheckbookTo:any=this.emptyArray;
    arrDistributionEntry:any={
        "bankTransferNumber":"",
        "currencyID":"",
        "transferFromBank":"",
        "transferToBank":"",
        "transferAmount":"",
        "originalAmount":"",
        "accountTableRowIndexFrom":"",
        "accountTableRowIndexTo":"",
        "accountDetailDebit":{
            "accountDescription": "",
            "debitAmount": "",
            "accountNumber": ""
        },
        "accountDetailCredit": {
            "accountDescription": "",
            "accountNumber": "",
            "creditAmount": ""
        },
        "accountDetailCharge": {
            "accountDescription": "",
            "debitAmount": "",
            "accountNumber": ""
        }


    }

    @ViewChild(DatatableComponent) table: DatatableComponent;
       constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private commonService: CommonService,
        private bankTransferEntryService: BankTransferEntryService,
        )
        {
            var userData = JSON.parse(localStorage.getItem('currentUser'));
            
        }

    ngOnInit() 
    {
        this.model.bankTransferNumber=''
        this.GetAddScreenDetail();
        this.getBatchList();
        this.getCompanyList();
        this.getCurrencySetupList();
        this.GetDistributionScreenDetail();
        this.GetChargesScreenDetail();
        this.GetBankTranferNumbersList();
       
    }

    GetAddScreenDetail()
    {
        this.screenCode = "S-1240";
        this.defaultAddFormValues = [
            { 'fieldName': 'BANK_TRF_BANK_TRF_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_TRF_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_TRANSACTION_CURRENCY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_EXCHANGE_TABLE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_TRF_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_COMPANY_ID_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_ID_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_CURRENCY_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_COMMENT_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_EXC_RATE_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_RATE_CALC_METHOD_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_AMOUNT_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_AMT_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_COM_GRID_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_ID_GRID_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_AMOUNT_GRID_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_MULTIPLY_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_DIVIDE_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_TRF_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_COMPANY_ID_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_ID_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_CURRENCY_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_COMMENT_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_EXC_RATE_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_RATE_CALC_METHOD_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_AMOUNT_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_AMT_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_COM_GRID_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHECKBOOK_ID_GRID_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_AMOUNT_GRID_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_MULTIPLY_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_DIVIDE_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'BANK_TRF_CHARGES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
        this.getScreenDetail(this.screenCode,'Add');
    }
    
    GetDistributionScreenDetail()
        {
            this.screenCode = "S-1241";
            this.defaultDistributionFormValues = [
                { 'fieldName':'BANK_TRF_DIS_BANL_TRF_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_TRF_FROM_BANK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_TRF_TO_BANK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_TRF_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_ORIGINAL_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_ACCOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_ACCOUNT_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_DEBIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_DIS_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
        this.getScreenDetail(this.screenCode,'Distribution');
    }
    
    GetChargesScreenDetail()
        {
            this.screenCode = "S-1242";
            this.defaultChargesFormValues = [
                { 'fieldName':'BANK_TRF_CHG_COMPANY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_CHG_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_COMMENTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_CHG_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName':'BANK_TRF_CHG_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            ];
        this.getScreenDetail(this.screenCode,'Charges');
    }

    getScreenDetail(screenCode,ArrayType)
    {
      
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;    
            if(ArrayType == 'Add'){
                this.moduleName=data.result.moduleName;
              for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
             }   
            else if(ArrayType == 'Distribution'){
                this.distributionScreenModuleName=data.result.moduleName;
             for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultDistributionFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if(ArrayType == 'Charges'){
                this.chargesScreenModuleName=data.result.moduleName;
             for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultChargesFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });       
    }

    getCurrencySetupList()
    {
        this.commonService.getCurrencySetup().then(data => {
            this.arrCurrency=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.records.length;i++)	
                {
                    this.arrCurrency.push({'id':data.result.records[i].currencyId,'text':data.result.records[i].currencyDescription});
                }
            }
        });
    }

    getBatchList(){
        this.commonService.getAllBatchesListByTransactionType(this.currentTransactionType).then(data => {
            this.arrBatchList=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrBatchList.push({'id':data.result[i].batchId,'text':data.result[i].batchId});
                }
            }
            
        });
    }
    GetBankTranferNumbersList(){
        this.bankTransferEntryService.GetBankTranferNumbersList().then(data => {
           this.bindSelectedItemFrom(this.emptyArray[0],'BankTransferNumber')
            this.arrBankTransferNumberList=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrBankTransferNumberList.push({'id':data.result[i].bankTransferNumber.toString(),'text':data.result[i].bankTransferNumber.toString()});
                }
            }
			
		 });
    }  

    
    GetExchangeTableSetupByCurrencyId(currencyId: string,type:string){
        
        if(currencyId !='0')
        {
            this.commonService.getExchangeTableSetupByCurrencyId(currencyId).then(data => {
                
                this.arrExchangeDetail=[{'id':'0','text':this.select}];
                
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                 this.arrExchange=data.result.records;
                 this.activeExchangeDetail=[this.emptyArray[0]]
                //  if(type == 'From')
                //  {
                    
                //      //this.bindSelectedItemFrom(this.emptyArray[0],'ExchangeDetail')
                //  }
                //  else
                //  {
                //     // this.bindSelectedItemTo(this.emptyArray[0],'ExchangeDetail')
                //  }

                var selectedExchange = this.arrExchange.find(x => x.currencyId ==   currencyId);

                 
                 if(selectedExchange)
                 {
                    if(selectedExchange.rateCalcMethod == 'multiply')
                    {
                        this.model.rateCalculateMethodFrom="1";
                        this.model.rateCalculateMethodTo="1";
                    }
                    else
                    {
                        this.model.rateCalculateMethodFrom="2";
                        this.model.rateCalculateMethodTo="2";
                    }
                 }
               
                  for(var i=0;i<data.result.records.length;i++)	
                  {
                    this.arrExchangeDetail.push({'id':data.result.records[i].exchangeTableIndex,'text':data.result.records[i].exchangeId});
                  }
                }
         });
        }
        else{
            
            this.arrExchange=[];
            this.arrExchangeDetail=[];
            if(type == 'From')
            {
                this.bindSelectedItemFrom(this.emptyArray[0],'ExchangeDetail')
            }
            else
            {
                this.bindSelectedItemTo(this.emptyArray[0],'ExchangeDetail')
            }
        }
       
    }

     BindExchangeDetailCurrencyId(currencyId: string){
        
        if(currencyId !='0')
        {
            this.commonService.getExchangeTableSetupByCurrencyId(currencyId).then(data => {
                
                this.arrExchangeDetail=[{'id':'0','text':this.select}];
                
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                 this.arrExchange=data.result.records;
                 this.activeExchangeDetail=[this.emptyArray[0]]

                 var selectedExchange = this.arrExchange.find(x => x.currencyId ==   currencyId);
                  for(var i=0;i<data.result.records.length;i++)	
                  {
                    this.arrExchangeDetail.push({'id':data.result.records[i].exchangeTableIndex,'text':data.result.records[i].exchangeId});
                  }
                }
         });
        }
      
       
    }

    GetCurrencyExchangeDetail(exchangeId: string){
        
        // if(this.model.exchangeTableID)
        // {
        //     exchangeId = this.model.exchangeTableID;
        // }
        // if(this.activeExchangeDetail[0].id != "0")
        // {
        //     exchangeId = this.activeExchangeDetail[0].id ;
        // }
        if(exchangeId !='0')
        {
            this.commonService.GetCurrencyExchangeDetail(exchangeId).then(data => {
                
                 if(data.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND")
                 {
                     if(!this.model.exchangeRateFrom)
                     {
                         this.model.exchangeRateFrom=data.result.exchangeRate;
                         this.model.exchangeRateTo=data.result.exchangeRate;
                     }   
                     this.CalculateToAmount();
                     this.CalculateFromAmount();   
                 }
                 else{
                    
                    this.model.rateCalculateMethodFrom="1";
                    this.model.rateCalculateMethodTo="1";
                    this.model.exchangeRateFrom='';
                    this.model.exchangeRateTo='';
                    // this.model.transferAmountFrom='';
                    // this.model.transferAmountTo='';
                    this.checkbookFromAmount='';
                    this.checkbookToAmount='';
                    this.CalculateToAmount();
                    this.CalculateFromAmount();
                 }
             });
        }
        else{
            
            this.model.rateCalculateMethodFrom="1";
            this.model.rateCalculateMethodTo="1";
            this.model.exchangeRateFrom='';
            this.model.exchangeRateTo='';
            // this.model.transferAmountFrom='';
            // this.model.transferAmountTo='';           
            this.checkbookFromAmount='';
            this.checkbookToAmount='';
           
        }
        // else{
        //     this.model.exchangeRateFrom='';
        //     this.model.exchangeRateTo='';
        // }
       
    }

    GetCheckbookMaintenanceByDynamicTenantid(TenantId,type:string){
        TenantId = TenantId.split('*');
        if(TenantId[1])
        {
            this.commonService.GetCheckbookMaintenanceByDynamicTenantid(TenantId[1]).then(data => {
                 if(type == 'From')
                 {
                     this.arrCheckbook=[{'id':'0','text':this.select}];
                     this.model.companyTransferFrom=TenantId[0];
                     if(data.btiMessage.messageShort == "RECORD_GET_SUCCESSFULLY")
                     {
                         for(var i=0;i<data.result.records.length;i++)	
                         {
                           this.arrCheckbook.push({'id':data.result.records[i].checkBookId,'text':data.result.records[i].checkBookId});
                         }   
                     }
                     else{
                        this.arrCheckbook=[{'id':'0','text':this.select}];
                        this.bindSelectedItemFrom(this.emptyArray[0],'Checkbook');
                     }
                 }
                 else
                 {
                     this.arrCheckbookTo=[{'id':'0','text':this.select}];
                     this.model.companyTransferTo=TenantId[0];
                     if(data.btiMessage.messageShort == "RECORD_GET_SUCCESSFULLY")
                     {
                      
                         for(var i=0;i<data.result.records.length;i++)	
                         {
                           this.arrCheckbookTo.push({'id':data.result.records[i].checkBookId,'text':data.result.records[i].checkBookId});
                         }   
                     }
                     else{
                        this.arrCheckbookTo=[{'id':'0','text':this.select}];
                        this.bindSelectedItemTo(this.emptyArray[0],'CheckbookTo');
                     }
                 }
             });
        }
        else{
            if(type == 'From')
            {
                this.arrCheckbook=[{'id':'0','text':this.select}];
                this.bindSelectedItemFrom(this.emptyArray[0],'Checkbook');
            }
            else
            {
                this.arrCheckbookTo=[{'id':'0','text':this.select}];
                this.bindSelectedItemTo(this.emptyArray[0],'CheckbookTo');
            }
        }
    }

    GetCheckBookMaintenanceByCheckbookId(checkBookId,type:String)
    {
        this.commonService.GetCheckBookMaintenanceByCheckbookId(checkBookId).then(data => {
           
            if(type=='From')
            {
                this.checkBookCurrencyFrom=data.result.currencyId;
            }
            else{
                this.checkBookCurrencyTo=data.result.currencyId;
            }
        });
    }

    GetCheckbookMaintenanceByCheckbookIdAndDynamicTenantid(checkBookId,TenantId,type:String,)
    {
        
        TenantId = TenantId.split('*');
        if(TenantId[1])
        {
           this.commonService.GetCheckbookMaintenanceByCheckbookIdAndDynamicTenantid(checkBookId,TenantId[1]).then(data => {
            
            if(type=='From')
             {
                 this.checkBookCurrencyFrom=data.result.currencyId;
             }
             else{
                 this.checkBookCurrencyTo=data.result.currencyId;
             }
            });
        }
    }

    


    getCompanyList()
    {
        this.commonService.getCompanyList().then(data => {
            this.arrCompany=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrCompany.push({'id':data.result[i].companyId+'*'+data.result[i].tenantId,'text':data.result[i].name});
                }
            }
     });
    }

    GetBankTransferEntry(bankTransferNumber)
    {
       
        if(bankTransferNumber != '0')
        {
            this.bankTransferEntryService.GetBankTransferEntry(bankTransferNumber).then(data => {
                this.disableBankTrsansferList=true;
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                   
                    this.isModify=true;
                    this.showBtns=true;
                    this.model=data.result;
                    this.formattransferAmountFrom()
                    this.selectedCurrency = data.result.currencyID
                    var chkrateCalculateMethodFrom_1=<HTMLInputElement>document.getElementById("rateCalculateMethodFrom_1");
                    var chkrateCalculateMethodFrom_2=<HTMLInputElement>document.getElementById("rateCalculateMethodFrom_2");
                    
                    if(this.model.rateCalculateMethodFrom == 1)
                    {
                        chkrateCalculateMethodFrom_1.checked = true;
                        chkrateCalculateMethodFrom_2.checked = false;
                    }                   
                    else{
                        chkrateCalculateMethodFrom_1.checked = false;
                        chkrateCalculateMethodFrom_2.checked = true;
                    }
       
                    var chkrateCalculateMethodTo_1=<HTMLInputElement>document.getElementById("rateCalculateMethodTo_1");
                    var chkrateCalculateMethodTo_2=<HTMLInputElement>document.getElementById("rateCalculateMethodTo_2");
                    if(this.model.rateCalculateMethodTo == 1)
                    {
                        chkrateCalculateMethodTo_1.checked = true;
                        chkrateCalculateMethodTo_2.checked = false;
                    }
                    else{
                        chkrateCalculateMethodTo_1.checked = false;
                        chkrateCalculateMethodTo_2.checked = true;
                    }

                    this.model.rateCalculateMethodFrom = this.model.rateCalculateMethodFrom.toString();
                    this.model.rateCalculateMethodTo = this.model.rateCalculateMethodTo.toString();
                    if(data.result.transferDate) {
                        this.model.transferDate=data.result.transferDate;
                    }
                    var transferDate = this.model.transferDate;
                    var transferDateData = transferDate.split('/');
                    this.model.transferDate = {"date":{"year":parseInt(transferDateData[2]),"month":parseInt(transferDateData[1]),"day":parseInt(transferDateData[0])}};
                   
                    if(data.result.batchID)
                    {
                        var selectedbatch = this.arrBatchList.find(x => x.id ==   data.result.batchID);
                        this.bindSelectedItemFrom(selectedbatch,'Batch');
                    }

                    this.unFrmtTransferAmountFrom=this.model.transferAmountFrom;
                    this.unFrmtTransferAmountTo=this.model.transferAmountTo;
                    
                    
                    var selectedCurrency = this.arrCurrency.find(x => x.id ==   data.result.currencyID);
                   // this.bindSelectedItemFrom(selectedCurrency,'Currency');
                   
                    if(selectedCurrency)
                    {
                        this.activeCurrency=[selectedCurrency]
                        this.formattransferAmountFrom();
                        this.formattransferAmountTo();
                        
                    }
                    var selectedCompanyTo = this.arrCompany.find(x => x.id.split('*')[0] ==   data.result.companyTransferTo);
                    if(selectedCompanyTo)
                    {
                        this.bindSelectedItemTo(selectedCompanyTo,'CompanyTo');
                        this.GetCheckbookMaintenanceByDynamicTenantid(selectedCompanyTo.id,'To');
                    }
                   
                    var selectedCompanyFrom = this.arrCompany.find(x => x.id.split('*')[0] ==   data.result.companyTransferFrom);
                    if(selectedCompanyFrom)
                    {
                        this.bindSelectedItemFrom(selectedCompanyFrom,'CompanyFrom');
                        this.GetCheckbookMaintenanceByDynamicTenantid(selectedCompanyFrom.id,'From');
                    }
                    
                   
                    this.BindExchangeDetailCurrencyId(data.result.currencyID)
                    
                    window.setTimeout(() => {
                       
                     //   this.arrExchange=[];
                      //  this.arrExchangeDetail=[{'id':'0','text':this.select}];
                        // this.commonService.getExchangeTableSetupByCurrencyId(data.result.currencyID).then(tempdata => {
                        //     if(tempdata.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        //       {
                        //           
                        //        this.arrExchange=tempdata.result.records;
                        //         for(var i=0;i<tempdata.result.records.length;i++)	
                        //         {
                        //           this.arrExchangeDetail.push({'id':tempdata.result.records[i].exchangeTableIndex,'text':tempdata.result.records[i].exchangeId});
                        //         }
                        //         
                        //         var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id ==   data.result.exchangeTableID);
                        //         this.activeExchangeDetail=[selectedExchangeDetail];
                        //         this.GetCurrencyExchangeDetail(selectedExchangeDetail.text);
                        //       }
                        //     });
                        var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id ==   data.result.exchangeTableID);
                        if(selectedExchangeDetail)
                        {
                            this.activeExchangeDetail=[selectedExchangeDetail];
                            this.GetCurrencyExchangeDetail(selectedExchangeDetail.text);
                        }
                        
                        var selectedCheckbookIDFrom = this.arrCheckbook.find(x => x.id.split('*')[0] ==   data.result.checkbookIDFrom);
                        if(selectedCheckbookIDFrom)
                        {
                            this.bindSelectedItemFrom(selectedCheckbookIDFrom,'Checkbook');
                        }
                        var selectedCheckbookIDTo = this.arrCheckbookTo.find(x => x.id.split('*')[0] ==   data.result.checkbookIDTo);
                        if(selectedCheckbookIDTo)
                        {
                            this.bindSelectedItemTo(selectedCheckbookIDTo,'CheckbookTo');
                        }
                      
                    }, 1000);
                    
                    
                    this.GetCheckbookAmount('From');
                    this.GetCheckbookAmount('To');
                    this.disableBankTrsansferList=false;
                }
                else{
                    this.isModify=false;
                }
            });
        }
        else{
            // this.getGLNextJournalEntry();
            this.isModify=false;
            this.clear(null);
        }
    }

    GetBankTransferDistrubutionByBankTransferNumber(bankTransferNumber: string){
        
        this.bankTransferEntryService.GetBankTransferDistrubutionByBankTransferNumber(bankTransferNumber).then(data => {
           
            if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
            {
                this.arrDistributionEntry=data.result;
            }
        });
    }

    bindAccountNumber()
    {
        this.commonService.getGlAccountNumberList().then(data => {
            this.arrAccountNumber=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                }
            }
        });
    }
    getAccountNumberDesc()
    {
        this.commonService.getGlAccountNumberDetailById(this.activeAccountNumber[0].id).then(data => {
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                this.accountDescription=data.result.accountDescription;
            }
            else{
                this.accountDescription= '';
            }
        });
    }
    SaveUpdateBankTransferEntrySetup(f: NgForm) {
    this.isPostEvt=false;
    
    this.model.exchangeTableID=this.activeExchangeDetail[0].id;
    if(f.valid  && this.activeCheckbookTo[0].id != '0' 
    && this.activeCurrency[0].id != '0' && this.activeCompanyTo[0].id != '0' 
    && this.activeCompanyFrom[0].id != '0'  && this.activeCheckbookFrom[0].id != '0' && this.activeExchangeDetail[0].id != '0')
    {
        this.btndisabled=true;
        if(this.model.transferDate.formatted == undefined)
        {
           var transferDate = this.model.transferDate;
           if(transferDate.date != undefined)
           {
              this.model.transferDate = transferDate.date.day +'/'+ transferDate.date.month +'/'+ transferDate.date.year;
           }
        }
        else
        {
            this.model.transferDate = this.model.transferDate.formatted;
        }
        
        this.model.transferAmountFrom=this.unFrmtTransferAmountFrom;
        this.model.transferAmountTo=this.unFrmtTransferAmountTo;
        this.bankTransferEntryService.SaveUpdateBankTransferEntry(this.model,this.isModify).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                   
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.isModify = false;
                    this.clear(f);
                    this.showBtns=false;
                    this.GetBankTranferNumbersList();
                    window.setTimeout(() => {
                       this.showMsg = false;
                       this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                }
               
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        this.btndisabled=false;
    }
    
 varifyDelete()
    {
       this.isDeleteAction=true;
       this.confirmationModalBody = this.deleteConfirmationText;
       this.isConfirmationModalOpen = true;
    }


closeModal()
    {
         this.isDeleteAction=false;
         this.isConfirmationModalOpen = false;
    }

    DeleteChargesEntry(f3: NgForm,chargeEntry)
    {
        
    if(this.model.chargeNumber)
    {
        this.bankTransferEntryService.DeleteBankTransferEntryCharges(this.model.chargeNumber).then(data =>
            {
                window.scrollTo(0,0);
                if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                          this.showMsg = false;
                          this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                          this.showMsg = false;
                          this.hasMsg = false;
                        }, 4000);
                } 
            });
           this.clearChargesForm(f3);
           chargeEntry.close();  
    }
     
    }
    
    ValidateChargeNumber()
    {
        this.bankTransferEntryService.GetBankTransferChargesByChargeNumber(this.model.chargeNumber).then(data =>
        {
            if (data.btiMessage.messageShort=="CHARGE_NUMBER_ALREADY_EXIST") {
                if(data.result.bankTransferNumber == this.model.bankTransferNumber)
                {
                    this.isChargeNumberExist=false;
                }
                else{
                    this.isChargeNumberExist=true;
                    this.msgChargeNumberExist=data.btiMessage.message;
                }
            } 
            else{
                this.isChargeNumberExist=false;
                this.msgChargeNumberExist='';
            }
        });
    }

    GetBankTransferChargesByBankTransferNumber()
    {
       
        this.bankTransferEntryService.GetBankTransferChargesByBankTransferNumber(this.model.bankTransferNumber).then(data =>
        {
            if (data.btiMessage.messageShort=="RECORD_GET_SUCCESSFULLY") {
                this.model.chargeNumber = data.result.chargeNumber;
                this.isChargeNumberExist = true;
                this.commonService.getGlAccountNumberList().then(Acctdata => {
                    this.arrAccountNumber=[{'id':'0','text':this.select}];
                    if(Acctdata.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<Acctdata.result.length;i++)	
                        {
                            this.arrAccountNumber.push({'id':Acctdata.result[i].accountTableRowIndex,'text':Acctdata.result[i].accountNumber});
                        }
                        
                        var selectedAccount = this.arrAccountNumber.find(x => x.id ==   data.result.accountNumber);
                        if(selectedAccount)
                        {
                            this.activeAccountNumber=[selectedAccount];
                            this.getAccountNumberDesc();
                        }
                    }
                    
                });
                
                // if(data.result.chargeAmount)
                // {
                //     this.isChargeNumberExist = true;
                // }
                this.model.chargeAmount =  data.result.chargeAmount;
                this.unFrmtReceiptAmount = data.result.chargeAmount;
                
                this.formatreceiptAmount();
                
                this.model.chargeComments =  data.result.chargeComments;
            } 
           
        });
    }

  
    DeleteBankTransferEntry(f: NgForm)
    {
        
        if(!this.model.bankTransferNumber)
        {
            this.closeModal();
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.defaultAddFormValues[0]['listDtoFieldValidationMessage'][0]['validationMessage'];
            window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
            }, 4000);
            
        }
        else{
            this.bankTransferEntryService.DeleteBankTransferEntry(this.model.bankTransferNumber).then(data =>
                {
                    window.scrollTo(0,0);
                    if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.isModify=false;
                            this.showBtns=false;
                            this.isConfirmationModalOpen=false;
                            this.bindSelectedItemFrom(this.emptyArray[0],'BankTransferNumber');
                            window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                            }, 4000);
                    }
                    this.clear(f);
                    this.GetBankTranferNumbersList();
                   
                 }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
        }
    }


    PostBankTransferEntrySetup(f: NgForm) {
        this.isPostEvt=true;
        
        if(f.valid && this.activeBatch[0].id != '0')
         {
            this.btndisabled=true;
             window.scrollTo(0,0);
             this.isSuccessMsg = false;
             this.isfailureMsg = true;
             this.showMsg = true;
             this.hasMsg = true;
             this.btndisabled=false;
             this.messageText =this.defaultAddFormValues[35]['listDtoFieldValidationMessage'][0]['validationMessage'];
             window.setTimeout(() => {
                 this.showMsg = false;
                 this.hasMsg = false;
             }, 4000);
         }
         else{
        this.isPostEvt=false;
        this.model.batchID = '';
       
        this.model.exchangeTableID=this.activeExchangeDetail[0].id;
        
        if(f.valid  && this.activeCheckbookTo[0].id != '0' 
        && this.activeCurrency[0].id != '0' && this.activeCompanyTo[0].id != '0' 
        && this.activeCompanyFrom[0].id != '0'  && this.activeCheckbookFrom[0].id != '0' && this.activeExchangeDetail[0].id != '0')
        {
            this.btndisabled=true;
            if(this.model.transferDate.formatted == undefined)
            {
               var transferDate = this.model.transferDate;
               if(transferDate.date != undefined)
               {
                  this.model.transferDate = transferDate.date.day +'/'+ transferDate.date.month +'/'+ transferDate.date.year;
               }
            }
            else
            {
                this.model.transferDate = this.model.transferDate.formatted;
            }
            this.model.transferAmountFrom=this.unFrmtTransferAmountFrom;
            this.model.transferAmountTo=this.unFrmtTransferAmountTo;
            this.bankTransferEntryService.SaveUpdateBankTransferEntry(this.model,this.isModify).then(data => {
                var datacode = data.code;
                this.btndisabled=false;
                 if (datacode == 200) {
             this.bankTransferEntryService.PostBankTransferEntry(this.model).then(data => {
                    window.scrollTo(0,0);
                    this.btndisabled=false;
                    if (data.btiMessage.messageShort == 'RECORD_POST_SUCCESSFULLY') {
                       
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.isModify=false;
                        this.clear(f);
                        this.showBtns=false;
                        this.GetBankTranferNumbersList();
                        window.setTimeout(() => {
                           this.showMsg = false;
                           this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                    }
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                }); 
                 }
            });
            
            }
        }
        this.btndisabled=false;
    }

    print()
    {
        window.print();
    }
    CalculateFromAmount()
    {
        var calcMethod=this.model.rateCalculateMethodFrom;
        var totalAmount=this.unFrmtTransferAmountFrom;
        var exchangeRate=this.model.exchangeRateFrom;
        var checkbookFromAmount = 0;
        if(calcMethod && totalAmount && exchangeRate)
        {
            if(calcMethod == '1')
            {
                checkbookFromAmount = totalAmount * exchangeRate;
            }
            else{
                checkbookFromAmount = totalAmount / exchangeRate;
            }

            this.checkbookFromAmount=checkbookFromAmount.toFixed(3);
            
            this.formatCheckBookAmount('From');
        }
        else{
            this.checkbookFromAmount='0';
        }
    }
    CalculateToAmount()
    {
        
        var calcMethod=this.model.rateCalculateMethodTo;
        var totalAmount=this.unFrmtTransferAmountTo
        var exchangeRate=this.model.exchangeRateTo;
        var checkbookToAmount = 0;
        if(calcMethod && totalAmount && exchangeRate)
        {
            if(calcMethod == '1')
            {
                checkbookToAmount = totalAmount * exchangeRate;
            }
            else{
                checkbookToAmount = totalAmount / exchangeRate;
            }

            this.checkbookToAmount=checkbookToAmount.toFixed(3);
            
            this.formatCheckBookAmount('To');
        }
        else{
            this.checkbookToAmount='0';
        }
    }
    
    GetCheckbookAmount(type:string)
    {
        
        if(type == 'From')
        {
            this.CalculateFromAmount();
            // console.log(this.unFrmtTransferAmountFrom + "original")
            // console.log(this.model.transferAmountFrom + "from value")
            this.model.transferAmountTo= this.unFrmtTransferAmountFrom;
            this.unFrmtTransferAmountTo= this.model.transferAmountTo;
            this.formattransferAmountTo();
            this.formattransferAmountFrom()
            // this.formattransferAmountTo();
            this.CalculateToAmount();
        }
        else{
            this.CalculateToAmount();
            // console.log(this.unFrmtTransferAmountTo + "original to")
            // console.log(this.model.transferAmountTo + "from value To")
            this.model.transferAmountFrom=this.unFrmtTransferAmountTo;
            this.unFrmtTransferAmountFrom= this.model.transferAmountFrom;
            this.formattransferAmountFrom();
            this.formattransferAmountTo();
            // this.formattransferAmountFrom();
            this.CalculateFromAmount();
            
        }
       
       
    }
    DistributionEntrySetup(myModal) {
        
     this.bankTransferEntryService.SaveBankTransferDistribution(this.model.bankTransferNumber,this.arrDistributionEntry.accountTableRowIndexFrom,this.arrDistributionEntry.accountTableRowIndexTo).then(data => {
         if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY') {
                    window.scrollTo(0,0);
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.arrDistributionEntry={
                    "bankTransferNumber":"",
                    "currencyID":"",
                    "transferFromBank":"",
                    "transferToBank":"",
                    "transferAmount":"",
                    "originalAmount":"",
                    "accountTableRowIndexFrom":"",
                    "accountTableRowIndexTo":"",
                    "accountDetailDebit":{
                        "accountDescription": "",
                        "debitAmount": "",
                        "accountNumber": ""
                    },
                    "accountDetailCredit": {
                        "accountDescription": "",
                        "accountNumber": "",
                        "creditAmount": ""
                    },
                    "accountDetailCharge": {
                        "accountDescription": "",
                        "debitAmount": "",
                        "accountNumber": ""
                    }
                }
         }
        else{
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
        }
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
        }, 4000);
        myModal.close();
     });
      
    }

    ChargeEntrySetup(f3:NgForm,chargeEntry) {
       if(!this.isChargeNumberExist)
        {
            this.model.chargeAmount=this.unFrmtReceiptAmount
            let submitChargesEntry={
                "bankTransferNumber" :this.model.bankTransferNumber,
                "chargeAmount" :this.model.chargeAmount,
                "chargeComments":this.model.chargeComments,
                "chargeNumber":this.model.chargeNumber,
                "checkbookID" :this.activeCheckbookFrom[0].id,
                "accountNumber" :this.activeAccountNumber[0].id 
                };
                
                this.bankTransferEntryService.SaveBankTransferCharges(submitChargesEntry).then(data => {
                 if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY') {
                            window.scrollTo(0,0);
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            // this.model=data.result;
                 }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                }
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                chargeEntry.close();
                this.clearChargesForm(f3);
             }); 
        }
             
    }


    clear(f:NgForm){
      
        // if(f == null)
        // {
             this.model.exchangeTableID=0;
             this.model.transferDate='';
             this.model.checkBookCurrencyFrom='';
             this.model.commentFrom='';
             this.model.exchangeRateFrom='';
             this.model.transferAmountFrom='';
             this.model.checkbookFromAmount='';
             this.model.checkBookCurrencyTo='';
             this.model.commentTo='';
             this.model.exchangeRateTo='';
             this.model.transferAmountTo='';
             this.model.checkbookToAmountAmount='';
             this.model.checkBookCurrencyTo='';
             this.model.checkbookFromAmount='';
             this.model.checkbookToAmount='';
             this.model.checkbookToAmount='';
             this.model.rateCalcMethodFrom="1";
             this.model.rateCalcMethodTo="1";
             this.model.rateCalculateMethodTo="1";
             this.model.rateCalculateMethodFrom="1";
             
        // }
        // else{
        //     f.resetForm();
        // }
        if(f != null)
        {
            f.resetForm();
        }
        var chkrateCalculateMethodFrom=<HTMLInputElement>document.getElementById("rateCalculateMethodFrom_1");
        if(chkrateCalculateMethodFrom)
        {
           chkrateCalculateMethodFrom.checked = true;
        }
        

        var chkrateCalculateMethodTo=<HTMLInputElement>document.getElementById("rateCalculateMethodTo_1");
        if(chkrateCalculateMethodFrom)
        {
           chkrateCalculateMethodTo.checked = true;
        }
        
       this.model.bankTransferNumber=this.activeBankTransferNumberList[0].id;
        if(!this.isModify)
        {
            this.model.bankTransferNumber='';
        }
        this.CalculateFromAmount();
        this.model.transferAmountTo=this.model.transferAmountFrom;
        this.CalculateToAmount();
        this.bindSelectedItemTo(this.emptyArray[0],'CheckbookTo');
        this.bindSelectedItemTo(this.emptyArray[0],'CompanyTo');
        this.bindSelectedItemFrom(this.emptyArray[0],'Batch');
      //  this.bindSelectedItemFrom(this.emptyArray[0],'BankTransferNumber');
        this.bindSelectedItemFrom(this.emptyArray[0],'Checkbook');
        this.bindSelectedItemFrom(this.emptyArray[0],'CompanyFrom');
        this.bindSelectedItemFrom(this.emptyArray[0],'Currency');
        this.bindSelectedItemFrom(this.emptyArray[0],'ExchangeDetail');
        var txt_BankTranfer = <HTMLInputElement>document.getElementById("bankTransferNumber");
        txt_BankTranfer.value=this.activeBankTransferNumberList[0].id;
    }
    Cancel(f){
        f.resetForm();
        this.isModify=false;
        this.model.rateCalcMethodFrom="1";
        this.model.rateCalcMethodTo="1";
        this.model.bankTransferNumber=this.model.bankTransferNumber;
        this.bindSelectedItemTo(this.emptyArray[0],'CheckbookTo');
        this.bindSelectedItemTo(this.emptyArray[0],'CompanyTo');
        this.bindSelectedItemFrom(this.emptyArray[0],'Batch');
        this.bindSelectedItemFrom(this.emptyArray[0],'BankTransferNumber');
        this.bindSelectedItemFrom(this.emptyArray[0],'Checkbook');
        this.bindSelectedItemFrom(this.emptyArray[0],'CompanyFrom');
        this.bindSelectedItemFrom(this.emptyArray[0],'Currency');
        this.bindSelectedItemFrom(this.emptyArray[0],'ExchangeDetail');
    }

    clearChargesForm(f3){
        this.bindSelectedItemFrom(this.emptyArray[0],'AccountNumber');
        this.accountDescription='';
        this.isChargeNumberExist=false;
        this.msgChargeNumberExist='';
        f3.resetForm();
    }
    private value:any = {};
    public bindSelectedItemFrom(value:any,type:string):void {
      
        if(type == 'Batch')
        {
            this.value = value;
            this.activeBatch=[value];
            if(value.text)
            {
                this.model.batchID=value.id;
            }
        }
        if(type == 'BankTransferNumber')
        {
            this.value = value;
            this.activeBankTransferNumberList=[value];
            
            if(value.text)
            {
                if(value.id != '0')
                {
                    this.model.bankTransferNumber=value.id;
                    this.GetBankTransferEntry(value.id);
                }
            }
           
        }
        if(type == 'Currency')
        {
            this.value = value;
            this.activeCurrency=[value];
            if(value.id != '0')
            {
                this.model.currencyID=this.activeCurrency[0].id;
                this.selectedCurrency = value.id
                this.formattransferAmountFrom();
                this.formattransferAmountTo();
                this.GetExchangeTableSetupByCurrencyId(value.id,'From');
            }
            else{
                this.formattransferAmountFrom();
                this.formattransferAmountTo();
            }
            // if(value.text)
            // {
            //     this.model.currencyID=this.activeCurrency[0].id;
            //     this.GetExchangeTableSetupByCurrencyId(value.id,'From');
            // }
        }
        else if(type == 'ExchangeDetail')
        {
            
            this.value = value;
           
            this.activeExchangeDetail=[value];
            if(value.text)
            {
                // this.model.exchangeTableID=this.activeExchangeDetail[0].id;
                var selectedExchange = this.arrExchange.find(x => x.exchangeTableIndex ==   value.id);
                
                if(selectedExchange)
                {
                    
                    if(selectedExchange.rateCalcMethod == 'multiply')
                    {
                        this.model.rateCalculateMethodFrom="1";
                        this.model.rateCalculateMethodTo="1";
                    }
                    else
                    {
                        this.model.rateCalculateMethodFrom="2";
                        this.model.rateCalculateMethodTo="2";
                    }
                }
                this.model.exchangeRateFrom=0;
                this.model.exchangeRateTo=0;
                this.GetCurrencyExchangeDetail(value.text);
            }
        }
        else if(type == 'CompanyFrom')
        {
            if(value.text)
            {
                
                this.arrCheckbook=[{'id':'0','text':this.select}];
                this.activeCheckbookFrom=[this.emptyArray[0]];
                this.GetCheckbookMaintenanceByDynamicTenantid(value.id,'From');
            }
            else{
                this.arrCheckbook=[{'id':'0','text':this.select}];
            }
            if(value.text)
            {
                this.activeCompanyFrom=[value];
            }
        }
        else if(type == 'Checkbook')
        {
            this.value = value;
            this.activeCheckbookFrom=[value];
            if(value.text)
            {
                this.model.checkbookIDFrom=this.activeCheckbookFrom[0].id;
                if(this.model.checkbookIDFrom != "0")
                {
                    
                    // var selectedCompanyBook = this.arrCompany.find(x => x.id ==   );
                    this.GetCheckbookMaintenanceByCheckbookIdAndDynamicTenantid(this.model.checkbookIDFrom,this.activeCompanyFrom[0].id,'From');
                //   this.GetCheckBookMaintenanceByCheckbookId(this.model.checkbookIDFrom,'From');
                }
                else{
                    this.checkBookCurrencyFrom='';
                }
            }
        }
        if(type == 'AccountNumber')
        {            
            this.value = value;
            if(value.text)
            {
                this.activeAccountNumber=[value];
                this.getAccountNumberDesc();
            }
        }
    }

    public bindSelectedItemTo(value:any,type:string):void {
       
        if(type == 'CompanyTo')
        {
            
            if(value.text)
            {
                
                this.arrCheckbookTo=[{'id':'0','text':this.select}];
                this.activeCheckbookTo=[this.emptyArray[0]];
                this.GetCheckbookMaintenanceByDynamicTenantid(value.id,'To');
            }
            else{
                this.activeCheckbookTo=[this.emptyArray[0]];
                this.arrCheckbookTo=[{'id':'0','text':this.select}];
            }
            if(value.text)
            {
                this.activeCompanyTo=[value];
            }
        }
        else if(type == 'CheckbookTo')
        {
           
            this.value = value;
            this.activeCheckbookTo=[value];
            if(value.text)
            {
                this.model.checkbookIDTo=this.activeCheckbookTo[0].id;
               
                if(this.model.checkbookIDTo != "0")
                {
                    
                    this.GetCheckbookMaintenanceByCheckbookIdAndDynamicTenantid(this.model.checkbookIDTo,this.activeCompanyTo[0].id,'To');
                  //this.GetCheckBookMaintenanceByCheckbookId(this.model.checkbookIDTo,'To');
                }
                else{
                    this.checkBookCurrencyTo='';
                }
            }
        }
    }
   
    openModel(myModal,action)
    {
        if(action == 'distributionEntrySetup')
        {
           
           this.GetBankTransferDistrubutionByBankTransferNumber(this.model.bankTransferNumber);
        }
        else{
            this.isChargeNumberExist=false;
            this.msgChargeNumberExist='';
            this.bindAccountNumber();
            this.GetBankTransferChargesByBankTransferNumber();
        }
       myModal.open();
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
    // From Transfer Amount
    // on Blur Add Symbols
    formattransferAmountFrom(){
        if(this.unFrmtTransferAmountFrom)
        {
            var inputValue = this.unFrmtTransferAmountFrom;
            this.model.transferAmountFrom = this.unFrmtTransferAmountFrom;
            this.commonService.changeInputText("transferAmountFrom")
           if (this.selectedCurrency !=undefined ){
                let submitInfo = {
                    'currencyId':this.selectedCurrency
                };
                this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                    this.model.transferAmountFrom = this.commonService.formatAmount(data, inputValue)
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                    }, 100)
                });
            }else{
                this.formattransferAmountFromChanged('From')
            }
        }
       
		
	
    }
    // On Focus change value to unFormated
	formattransferAmountFromChanged(actionType:string ){
        this.commonService.changeInputNumber("transferAmountFrom")
        this.model.transferAmountFrom = this.unFrmtTransferAmountFrom
    }

    // get value on change without formating
	gettransferAmountFrom(event){
        this.unFrmtTransferAmountFrom = event
	}
 ///////// end From Transfer Amount /////////

 // To Transfer Amount
    // on Blur Add Symbols
    formattransferAmountTo(){
        
        if(this.unFrmtTransferAmountTo)
        {
            var inputValue = this.unFrmtTransferAmountTo;
            this.model.transferAmountTo = this.unFrmtTransferAmountTo;

            this.commonService.changeInputText("transferAmountTo")
            if (this.selectedCurrency !=undefined ){
                let submitInfo = {
                    'currencyId':this.selectedCurrency
                };
                this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                    this.model.transferAmountTo = this.commonService.formatAmount(data, inputValue)
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                    }, 100)
                });
                
            }else{
                this.formattransferAmountFromChanged('To')
            }

        }
    }
    // On Focus change value to unFormated
	formattransferAmountToChanged(actionType:string ){
        this.commonService.changeInputNumber("transferAmountTo")
        this.model.transferAmountTo = this.unFrmtTransferAmountTo
    
    }

    // get value on change without formating
	gettransferAmountTo(event){
		this.unFrmtTransferAmountTo = event
    }
    
    formatCheckBookAmount(type:string){
        
        if(type == "From")
        {
            if(this.checkbookFromAmount)
            {
                var inputValue = this.checkbookFromAmount;
                this.checkbookFromAmount = this.checkbookFromAmount;
    
                this.commonService.changeInputText("transferAmountTo")
                if (this.selectedCurrency !=undefined ){
                    let submitInfo = {
                        'currencyId':this.selectedCurrency
                    };
                    this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                        this.checkbookFromAmount = this.commonService.formatAmount(data, inputValue)
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                        }, 100)
                    });
                    
                }
             }
        }
        else{
            if(this.checkbookToAmount)
            {
                var inputValue = this.checkbookToAmount;
                this.checkbookToAmount = this.checkbookToAmount;
    
                this.commonService.changeInputText("transferAmountTo")
                if (this.selectedCurrency !=undefined ){
                    let submitInfo = {
                        'currencyId':this.selectedCurrency
                    };
                    this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                        this.checkbookToAmount = this.commonService.formatAmount(data, inputValue)
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                        }, 100)
                    });
                    
                }
        }
       
    }
}


 //Change To Original Value of receiptAmount
 formatreceiptAmountChanged(){
    this.commonService.changeInputNumber("chargeAmount");
    this.model.chargeAmount = this.unFrmtReceiptAmount
}
// get Original receiptAmount
getreceiptAmountValue(event){
    this.unFrmtReceiptAmount = event
}

formatreceiptAmount(){
    
    
    if(this.unFrmtReceiptAmount)
    {
        this.commonService.changeInputText("chargeAmount")
        var inputValue = this.unFrmtReceiptAmount;
        
        this.model.chargeAmount = this.unFrmtReceiptAmount;
        if (this.activeCurrency[0].id !='0' ){
            let submitInfo = {
                'currencyId':this.activeCurrency[0].id
            };
            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                this.model.chargeAmount = this.commonService.formatAmount(data, inputValue)
                
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                }, 100)
            });
            
            
        }else{
            this.formatreceiptAmountChanged()
        }
}
}
  /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
       LockScreen(writeAccess)
       {
           if(!writeAccess)
           {
               return true
           }
           else if(this.btndisabled)
           {
               return true
           }
           else if(this.isScreenLock)
           {
               return true;
           }
           else{
               return false;
           }
       }
 ///////// end From Transfer Amount /////////
} 