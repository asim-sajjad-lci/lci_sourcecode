import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { ManualPaymentEntry } from '../../../../../financialModule/_models/transactionModule/accountPayables/manualPaymentEntry';
import { ManualPaymentEntryService } from '../../../../../financialModule/_services/transactionModule/accountPayables/manualPaymentEntry.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
    templateUrl:'./manual-payment-entry.component.html',
    providers:[ManualPaymentEntryService,CommonService]
})

//export to make it available for other classes
export class ManualPaymentEntryComponent {
    page = new Page();
    rows = new Array<ManualPaymentEntryService>();
    moduleCode = Constants.financialModuleCode;
    accountDescriptionTitle=Constants.accountDescription;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    distributionScreenModuleName = '';
    exchangeRateScreenModuleName = '';
    select=Constants.select;
    emptyArray=[{'id':'0','text':this.select}];    
    defaultAddFormValues: Array<object>;
    defaultDistributionFormValues: Array<object>;
    defaultExchangeRateFormValues: Array<object>;
    availableFormValues: [object];
    searchKeyword = '';
    selected = [];
    messageText;
    hasMsg = false;
    PopUpmessageText;
    PopUphasMsg = false;
    showMsg = false;
    isPostEvt:boolean=false;
    isSuccessMsg;
    isfailureMsg;
    ddPageSize = 5;
    model: any = {};
    atATimeText=Constants.atATimeText;
    close=Constants.close;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    showBtns:boolean=false; 
    /* For Cash Receipt Dropdown */
    activeManualPaymentNumber:any=this.emptyArray;
    arrManualPaymentNumber=[];
    arrDistributionAccountTypes=[];
    /* For Customer Dropdown */
    activeVendor:any=this.emptyArray;
    arrVendor=[];
    venderName:string;
    distributionAccountTypeCode:string;
    distributionAccountType:string;
    /* For Batch List Dropdown */
    activeBatch:any=this.emptyArray;
    arrBatchList =  this.emptyArray;
    currentTransactionType=Constants.batchTransacitonType.AP_ENTER_VOID_RECEIPT;
    /* For Receipt Type */
    arrManualPaymentType:any=[];
    /* For Checkbook ID List */
    activeCheckbook:any=this.emptyArray;
    activeAccountNumber:any=this.emptyArray;
    accountTableRow:any={'id':'','text':''};
    arrCheckbook=[];
    checkBookDescription:string;
    /* For Currency List */
    activeCurrency:any=this.emptyArray;
    arrCurrency=[];
    arrExchange=[];
    arrExchangeDetail = this.emptyArray;
    /* For Credit Card */
    activeCreditCard:any=this.emptyArray;
    activeExchangeDetail=this.emptyArray;
    arrCreditCard=[];
    
    /* For Distribution Screen */
    arrAccountNumber=[];
    IsAddDistributionDetails:boolean=false;
    debitAmount:number = 0;
    creditAmount:number = 0;
    distributionDescription:'';
    accountDescription:string;
    isCredit:boolean=false;
    btnCancelText=Constants.btnCancelText;
    selectedCurrency;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isDeleteAction : boolean = false;
    isConfirmationModalOpen:boolean=false;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    unFrmtReceiptAmount;
    
    btndisabled:boolean=false;
    arrAPDistributionEntry:any={
        "manualPaymentNumber":"",
        "vendorId":"",
        "vendorName":"",
        "currencyID":"",
        "transactionType":"",
        "functionalAmount":"",
        "originalAmount":"",
        "listDtoAPDistributionDetail":[]
    }
    
    private value:any = {};
    
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
    
    private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
    onDateChanged(event: IMyDateModel): void {
        var RequestedData =  {
            "transactionDate":event.formatted,
            "moduleName":"AP",
            "originId":Constants.massCloseOriginType.AP_Manual_Payment_Entry
        }
        this.commonService.checkTransactionDateIsValid(RequestedData).then(data => {
            if(data.btiMessage.messageShort == "INVALID_TRANSACTION_DATE")
            {
                this.model.manualPaymentCreateDate = '';
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });   
    }
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private commonService: CommonService,
        
        private manualPaymentEntryService: ManualPaymentEntryService,
    )
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
    }
    
    ngOnInit() 
    {
        this.GetAddScreenDetail();
        this.GetDistributionScreenDetail();
        this.GetExchangeRateScreenDetail();
        this.GetVendorList();
        this.GetBatchList();
        this.GetUniqueManualPaymentNumber();
        this.GetAllManualPaymentNumber();
        this.GetManualPaymentType();
        this.GetCheckbookMaintenance();
        this.GetCurrencySetup();
        this.GetCreditCardSetup();
        this.BindAccountNumber();
        this.GetDistributionAccountTypesList();
    }
    
    GetAddScreenDetail()
    {
        this.screenCode = "S-1255";
        this.defaultAddFormValues = [
            {'fieldName': 'AP_MAN_PAY_PAYMENT_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_COMMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_TRANSACTION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_PAYMENT_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_CHECK_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_CREDIT_CARD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_CREDIT_CARD_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_EXPIRE_DATE_MM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_EXPIRE_DATE_YY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_MAN_PAY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
        this.getScreenDetail(this.screenCode,'Add');
    }
    
    GetDistributionScreenDetail()
    {
        this.screenCode = "S-1256";
        this.defaultDistributionFormValues = [
            { 'fieldName':'AP_MAN_PAY_DIS_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_VENDOR_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_TRANSACTION_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_TRANSACTION_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_FUNC_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_ORIG_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_ACC_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_DIS_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_DEBIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_MAN_PAY_DIS_DEFAULT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
        this.getScreenDetail(this.screenCode,'Distribution');
    } 
    
    GetExchangeRateScreenDetail()
    {
        this.screenCode = "S-1238";
        this.defaultExchangeRateFormValues = [
            { 'fieldName': 'EX_TABLE_DET_JOURNAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_EX_TABLE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_EX_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
        this.getScreenDetail(this.screenCode,'ExchangeRate');
    }
    
    getScreenDetail(screenCode,ArrayType)
    {
        
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;    
            if(ArrayType == 'Add'){
                this.moduleName=data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if(ArrayType == 'Distribution'){
                this.distributionScreenModuleName=data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultDistributionFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if(ArrayType == 'ExchangeRate'){
                this.exchangeRateScreenModuleName=data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultExchangeRateFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }
    
    //Get vendor maintenance By venderId
    GetvendormaintenanceById(venderId)
    {
        this.commonService.GetvendormaintenanceById(venderId).then(data => {
            this.venderName=data.result.name;
        });
    }
    
    // Get Cash Reciept Number
    GetAllManualPaymentNumber()
    {
        
        this.manualPaymentEntryService.GetAllManualPaymentNumber().then(data => {
            this.arrManualPaymentNumber=[{'id':'0','text':this.select}];
            
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrManualPaymentNumber.push({'id':data.result[i],'text':data.result[i]});
                }
            }
        });
    }
    
    //Get Distribution Account Type List
    GetDistributionAccountTypesList()
    {
        this.manualPaymentEntryService.GetDistributionAccountTypesList().then(data => {
            this.arrDistributionAccountTypes=data.result;
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                this.arrDistributionAccountTypes.splice(0, 0, { "typeId": "", "typeCode": this.select });
                this.distributionAccountType='';
            }
        });
    }
    
    getGlAccountNumberDetail(myModal)
    {
        var accountTableRowIndex='';
        accountTableRowIndex=this.activeAccountNumber[0].id
        this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                this.accountDescription=data.result.accountDescription;
            }
            else{
                this.accountDescription=data.btiMessage.message;
            }
            myModal.open();
        });
    }
    
    GetGlAccountNumberDetailById(accountTableRowIndex:string)
    {
        this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
            if(data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST')
            {
                this.accountDescription=data.result.accountDescription;
            }
            else if(data.btiMessage.messageShort == 'MAIN_ACCOUNT_TRANSACTION_NOT_ALLOWED')
            {
                this.activeAccountNumber = [{'id':'0','text':this.select}];
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.PopUphasMsg = true;
                this.PopUpmessageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.PopUphasMsg = false;
                }, 2000);
            }
            else{
                this.accountDescription=data.btiMessage.message;
            }
        });
    }
    validateAmount(amountType:string,thisevt)
    {
        
        if(amountType=='debitAmount' && thisevt.debitAmount==''){
            this.debitAmount=0;
            //    this.isDebitAllowed=false;
            //    this.isCreditAllowed=true;
        }
        else if(amountType=='creditAmount' && thisevt.creditAmount==''){
            this.creditAmount=0;
            // this.isCreditAllowed=false;
            // this.isDebitAllowed=true;
        }
        if(amountType=='debitAmount')
        {
            if(this.debitAmount > 0)
            {
                this.creditAmount=0;
            }
        }
        else if(amountType=='creditAmount')
        {
            if(this.creditAmount > 0)
            {
                this.debitAmount=0;
            }
        }
        
    }
    
    ChangeDistributionAccountType(evt)
    {
        
        var selectedIndex = evt.target.selectedIndex
        this.distributionAccountTypeCode=evt.target[selectedIndex].text
        // var listbox = <HTMLSelectElement>document.getElementById("distributionAccountType");
        
        // this.distributionAccountTypeCode=this.options[this.selectedIndex].text
        
        
        /// var selectedIndex = listbox.selectedIndex;
        //  var selValue = listbox.options[selIndex].value;
        //  var selectedText = listbox.options[selectedIndex].text;  
        // this.distributionAccountTypeCode=selectedText;
    }
    
    GetUniqueManualPaymentNumber()
    {
        this.commonService.GetUniqueReceiptNumber().then(data => {
            this.model.manualPaymentNumber=data.result;
        });
    }
    
    AddDistributionDetails()
    {
        this.IsAddDistributionDetails=true;
        if((this.creditAmount || this.debitAmount) && this.activeAccountNumber[0].id !='0' && this.distributionAccountType && this.distributionDescription)
        {
            if(this.creditAmount)
            {
                this.isCredit=true;
            }
            else{
                this.isCredit=false;
            }
            
            this.arrAPDistributionEntry.listDtoAPDistributionDetail.push(
                {
                    "accountNumber":this.activeAccountNumber[0].text,
                    "accountDescription":this.accountDescription,
                    "type":this.distributionAccountTypeCode,
                    "typeId":this.distributionAccountType,
                    "distributionReference":this.distributionDescription,
                    "debitAmount":this.debitAmount,
                    "creditAmount":this.creditAmount,
                    "accountTableRowIndex":this.activeAccountNumber[0].id
                }
            );
            this.IsAddDistributionDetails=false;
            this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
            if(!this.debitAmount)
            {
                this.debitAmount = 0;
            }
            if(!this.creditAmount)
            {
                this.creditAmount = 0;
            }
            
            // this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString())
            this.debitAmount=0;
            this.creditAmount=0;
            this.distributionDescription='';
            this.distributionAccountType='';
            
        }
        else{
            // alert(Constants.msgText);
        }
        
    }
    RemoveDistributionDetails(RowIndex)
    {
        this.arrAPDistributionEntry.listDtoAPDistributionDetail.splice(RowIndex, 1);
        if(this.arrAPDistributionEntry.listDtoAPDistributionDetail == 0)
        {
            this.IsAddDistributionDetails=true;
        }
    }
    
    BindAccountNumber()
    {
        this.commonService.getGlAccountNumberList().then(data => {
            this.arrAccountNumber=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                }
            }
        });
    }
    
    // Get Customer List
    GetVendorList()
    {
        this.commonService.Getvendormaintenance().then(data => {
            this.arrVendor=[{'id':'0','text':this.select}];
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.records.length;i++)	
                {
                    this.arrVendor.push({'id':data.result.records[i].venderId,'text':data.result.records[i].venderId
                });
            }
        }
    });
}

// get Batch List
GetBatchList(){
    this.commonService.getAPBatchesByTransactionType(this.currentTransactionType).then(data => {
        this.arrBatchList=[{'id':'0','text':this.select}];
        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
        {
            for(var i=0;i<data.result.length;i++)	
            {
                this.arrBatchList.push({'id':data.result[i].batchId,'text':data.result[i].batchId});
            }
        }
    });
}

//Get Manual Payment Type
GetManualPaymentType()
{
    this.manualPaymentEntryService.GetManualPaymentType().then(data => {
        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
        {
            this.arrManualPaymentType = data.result;
            this.arrManualPaymentType.splice(0, 0, { "typeId": "", "name": this.select });
            this.model.manualPaymentType='2';
        }
    });
}

//Get checkbook Maintenance
GetCheckbookMaintenance(){
    {
        this.commonService.GetCheckbookMaintenance().then(data => {
            this.arrCheckbook=[{'id':'0','text':this.select}];
            
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.records.length;i++)	
                {
                    this.arrCheckbook.push({'id':data.result.records[i].checkBookId,'text':data.result.records[i].checkBookId});
                }
            }
        });
    }
}

// Get Currency Setup List
GetCurrencySetup()
{
    this.commonService.getCurrencySetup().then(data => {
        this.arrCurrency=[{'id':'0','text':this.select}];
        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
        {
            for(var i=0;i<data.result.records.length;i++)	
            {
                this.arrCurrency.push({'id':data.result.records[i].currencyId,'text':data.result.records[i].currencyId});
            }
        }
    });
}

GetCheckBookMaintenanceByCheckbookId(checkBookId)
{
    this.commonService.GetCheckBookMaintenanceByCheckbookId(checkBookId).then(data => {
        this.checkBookDescription=data.result.checkbookDescription;
    });
}

GetExchangeTableSetupByCurrencyId(currencyId: string){
    this.commonService.getExchangeTableSetupByCurrencyId(currencyId).then(data => {
        this.arrExchangeDetail=[{'id':'0','text':this.select}];
        this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail')
        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
        {
            for(var i=0;i<data.result.records.length;i++)	
            {
                this.arrExchangeDetail.push({'id':data.result.records[i].exchangeTableIndex,'text':data.result.records[i].exchangeId});
            }
        }
    });
}

GetCurrencyExchangeDetail(exchangeId: string){
    
    this.commonService.GetCurrencyExchangeDetail(exchangeId).then(data => {
        
        if(data.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND")
        {
            if(!this.model.exchangeTableRate)
            {
                this.model.exchangeTableRate=data.result.exchangeRate;
                
            }    
            // if(!this.model.exchangeExpirationDate)
            // {  
            this.model.exchangeExpirationDate=data.result.exchangeExpirationDate;
            //     }
        }
    });
}


// Get Credit Card Setup List
GetCreditCardSetup()
{
    this.commonService.GetCreditCardSetup().then(data => {
        this.arrCreditCard=[{'id':'0','text':this.select}];
        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
        {
            for(var i=0;i<data.result.records.length;i++)	
            {
                this.arrCreditCard.push({'id':data.result.records[i].cardIndex,'text':data.result.records[i].cardId});
            }
        }
    });
}



public bindSelectedItem(value:any,type:string):void {
    if(type == 'ManualPaymentNumber')
    {
        this.value = value;
        if(value.text)
        {
            this.activeManualPaymentNumber=[value];
        }
        if(value.id != '0')
        {
            this.model.manualPaymentNumber=this.activeManualPaymentNumber[0].id;
            this.GetCashReceiptEntryByCashReceiptNumber(this.model.manualPaymentNumber);
        }
        else{
            this.GetUniqueManualPaymentNumber();
        }
        
    }
    else if(type == 'Batch')
    {
        this.value = value;
        if(value.text)
        {
            this.activeBatch=[value];
        }
        this.model.batchID=this.activeBatch[0].id;
    }
    else if(type == 'Vendor')
    {
        this.value = value;
        if(value.text)
        {
            this.activeVendor=[value];
            this.model.vendorID=this.activeVendor[0].id;
            
            if(this.model.vendorID != "0")
            {
                this.GetVendorByVendorId(this.model.vendorID);
            }
        }
    }
    else if(type == 'Checkbook')
    {
        this.value = value;
        if(value.text)
        {
            this.activeCheckbook=[value];
            this.model.checkBookId=this.activeCheckbook[0].id;
            if(this.model.checkBookId != "0")
            {
                this.GetCheckBookMaintenanceByCheckbookId(this.model.checkBookId);
            }
        }
    }
    else if(type == 'Currency')
    {
        this.value = value;
        if(value.text)
        {
            this.activeCurrency=[value];
            this.model.currencyID=this.activeCurrency[0].id;
            this.activeExchangeDetail= [this.emptyArray[0]];
            this.arrExchangeDetail=[];
            this.model.exchangeTableRate = '';
            this.model.exchangeExpirationDate = '';
            if(value.id != "0")
            {
                if(this.model.manualPaymentAmount){
                    this.formatreceiptAmount();
                }
                this.selectedCurrency = value.id
                this.GetExchangeTableSetupByCurrencyId(value.id);
            }else{
                this.model.manualPaymentAmount = this.unFrmtReceiptAmount;
            }
        }
    }
    else if(type == 'CreditCard')
    {
        this.value = value;
        if(value.text)
        {
            this.activeCreditCard=[value];
        }
        this.model.creditCardID=this.activeCreditCard[0].id;
    }
    else if(type == 'ExchangeDetail')
    {
        this.value = value;
        // this.activeExchangeDetail=[value];
        
        // if(this.activeExchangeDetail[0].id != "0")
        // {
        //     this.GetCurrencyExchangeDetail(value.id);
        // }
        // else{
        //     this.model.exchangeTableRate == '';
        //     this.model.exchangeExpirationDate == '';
        // }
        
        if(value.id)
        {
            this.model.exchangeTableIndex=value.id;
            this.activeExchangeDetail=[value];
            if(this.activeExchangeDetail[0].id != "0")
            {
                this.GetCurrencyExchangeDetail(value.text);
            }
            else{
                this.model.exchangeTableRate == '';
                this.model.exchangeExpirationDate == '';
            }
        }
    }
    else if(type == 'AccountNumber')
    {
        this.accountTableRow=value;
        this.value = value;
        if(value.text)
        {
            this.activeAccountNumber=[value];
            this.GetGlAccountNumberDetailById(value.id)
        }
    }
}

SaveUpdateManualPaymentEntry(f,myModal){
    
    if(f.valid &&  this.activeExchangeDetail[0].id != '0' && this.activeVendor[0].id != '0' && this.activeCurrency[0].id != '0' && this.activeCheckbook[0].id != '0')
    {
        this.btndisabled=true;
        this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
        if(this.model.manualPaymentCreateDate.formatted == undefined)
        {
            var manualPaymentCreateDate = this.model.manualPaymentCreateDate;
            if(manualPaymentCreateDate.date != undefined)
            {
                this.model.manualPaymentCreateDate = manualPaymentCreateDate.date.day +'/'+ manualPaymentCreateDate.date.month +'/'+ manualPaymentCreateDate.date.year;
            }
        }
        else
        {
            this.model.manualPaymentCreateDate = this.model.manualPaymentCreateDate.formatted;
        }
        if(this.activeCreditCard[0].id == '0')
        {
            this.model.creditCardID = '';
        }
        
        this.model.manualPaymentAmount=this.unFrmtReceiptAmount;
        this.manualPaymentEntryService.SaveUpdateManualPaymentEntry(this.model,this.isModify).then(data => {
            this.btndisabled=false;
            this.isPostEvt=false;
            window.scrollTo(0,0);
            if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                this.isModify=false;
                this.showBtns=false;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
            else{
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
            f.resetForm();
            this.clear(f);
            this.GetAllManualPaymentNumber();
            this.bindSelectedItem(this.emptyArray[0],'ManualPaymentNumber');
            
        }).catch(error => {
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    else{
        
        if(this.activeExchangeDetail[0].id == '0')
        {
            myModal.open();
        }
    }   
    this.btndisabled=false;     
}

GetCashReceiptEntryByCashReceiptNumber(manualPaymentNumber)
{
    
    if(manualPaymentNumber != '0')
    {
        this.manualPaymentEntryService.GetManualPaymentEntryByManualPaymentNumber(manualPaymentNumber).then(data => {
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                this.isModify=true;
                this.showBtns=true;
                this.isPostEvt=false;
                this.model=data.result;
                this.unFrmtReceiptAmount = this.model.manualPaymentAmount;
                if(data.result.manualPaymentCreateDate) {
                    var manualPaymentCreateDate = data.result.manualPaymentCreateDate;
                    var receiptDateData = manualPaymentCreateDate.split('/');
                    this.model.manualPaymentCreateDate = {"date":{"year":parseInt(receiptDateData[2]),"month":parseInt(receiptDateData[1]),"day":parseInt(receiptDateData[0])}};
                } 
                
                if(data.result.batchID)
                {
                    var selectedBatch = this.arrBatchList.find(x => x.id ==   data.result.batchID);
                    this.bindSelectedItem(selectedBatch,'Batch');
                }
                else{
                    this.bindSelectedItem(this.emptyArray[0],'Batch');
                }
                
                var selectedCustomer = this.arrVendor.find(x => x.id ==   data.result.vendorID);
                this.bindSelectedItem(selectedCustomer,'Vendor');   
                
                var selectedCheckbook = this.arrCheckbook.find(x => x.id ==   data.result.checkBookId);
                this.bindSelectedItem(selectedCheckbook,'Checkbook');
                
                var sourceCurrency = this.arrCurrency.find(x => x.id ==   data.result.currencyID);
                this.selectedCurrency =  data.result.currencyID
                // this.bindSelectedItem(sourceCurrency,'Currency');
                if(sourceCurrency)
                {
                    this.activeCurrency=[sourceCurrency]
                    this.formatreceiptAmount();
                }
                
                if(data.result.creditCardID)
                {
                    var selectedCreditCardId = this.arrCreditCard.find(x => x.id ==   data.result.creditCardID);
                    this.bindSelectedItem(selectedCreditCardId,'CreditCard'); 
                }
                
                var exchangeTableIndex = data.result.exchangeTableIndex;
                window.setTimeout(() => {
                    this.commonService.getExchangeTableSetupByCurrencyId(data.result.currencyID).then(tempdata => {
                        this.arrExchange=tempdata.result.records;
                        if(tempdata.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<tempdata.result.records.length;i++)	
                            {
                                this.arrExchangeDetail.push({'id':tempdata.result.records[i].exchangeTableIndex,'text':tempdata.result.records[i].exchangeId});
                            }
                            
                            var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id ==   data.result.exchangeTableIndex);
                            this.activeExchangeDetail=[selectedExchangeDetail];
                            
                            this.GetCurrencyExchangeDetail(selectedExchangeDetail.text);
                        }
                    });
                }, 2000);
                // window.setTimeout(() => {
                //     
                
                //     var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id ==  exchangeTableIndex);
                //     this.model.exchangeTableIndex=selectedExchangeDetail.id;
                
                //     this.activeExchangeDetail=[selectedExchangeDetail];
                
                //     this.commonService.GetCurrencyExchangeDetail(selectedExchangeDetail.text).then(fatchdata => {
                //        if(fatchdata.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND")
                //        {
                
                //            this.model.exchangeExpirationDate=fatchdata.result.exchangeExpirationDate;
                //        }
                //      });
                //     }, 2000);
            }
            else{
                this.isModify=false;
            }   
        });
    }
    else{
        // this.getGLNextJournalEntry();
        this.isModify=false;
        this.model.manualPaymentNumber='';
        this.model={};
        this.venderName='';
        this.model.manualPaymentCreateDate='';
        this.GetAllManualPaymentNumber();
        this.bindSelectedItem(this.emptyArray[0],'ManualPaymentNumber');
        this.bindSelectedItem(this.emptyArray[0],'Vendor');
        this.bindSelectedItem(this.emptyArray[0],'Batch');
        this.bindSelectedItem(this.emptyArray[0],'Currency');
        this.bindSelectedItem(this.emptyArray[0],'Checkbook');
        this.bindSelectedItem(this.emptyArray[0],'CreditCard'); 
    }
}

GetVendorByVendorId(vendorID)
{
    this.commonService.GetvendormaintenanceById(vendorID).then(data => {
        this.venderName=data.result.venderNamePrimary;
    });
}

GetManualPaymentDistributionByManualPaymentNumber(manualPaymentNumber:string){
    this.manualPaymentEntryService.GetManualPaymentDistributionByManualPaymentNumber(manualPaymentNumber).then(data => {
        if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
        {
            this.arrAPDistributionEntry=data.result;
        }
    });
}

APDistributionEntrySetup(myModal){    
    this.manualPaymentEntryService.SaveManualPaymentDistribution(this.arrAPDistributionEntry).then(data => {
        
        if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY') {
            
            window.scrollTo(0,0);
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = data.btiMessage.message;
            this.arrAPDistributionEntry={
                "manualPaymentNumber":"",
                "vendorId":"",
                "vendorName":"",
                "currencyID":"",
                "transactionType":"",
                "functionalAmount":"",
                "originalAmount":"",
                "listDtoAPDistributionDetail":[]
            }
        }
        else{
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = data.btiMessage.message;
        }
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
        }, 4000);
        myModal.close();
    });
    
}

// Delete Manual Payment Entry
DeleteManualPaymentEntry(f: NgForm)
{
    
    if(!this.model.manualPaymentNumber)
    {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.hasMsg = true;
        this.messageText = this.defaultAddFormValues[0]['listDtoFieldValidationMessage'][0]['validationMessage'];
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
        }, 4000);
        
    }
    else{
        
        this.manualPaymentEntryService.DeleteManualPaymentEntry(this.model.manualPaymentNumber).then(data =>
            {
                window.scrollTo(0,0);
                this.closeModal();
                if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.isModify=false;
                    this.showBtns=false;
                    this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                    this.clear(f);
                    
                    this.GetAllManualPaymentNumber();
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                this.clear(f);                 
                
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
    }
    
    
    // Delete AR Cash Receipt Entry Distribution
    DeleteManualPaymentDistribution(myModal)
    {
        this.manualPaymentEntryService.DeleteManualPaymentDistribution(this.model.manualPaymentNumber).then(data =>
            {
                window.scrollTo(0,0);
                if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.isModify=false;
                    this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
            myModal.close();
        }
        
        
        // Post Manual Payment Entry
        PostManualPaymentEntry(f: NgForm,myModal) {
            this.isPostEvt=true;
            
            if(f.valid && this.activeBatch[0].id !='0')
            {
                
                window.scrollTo(0,0);
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText =this.defaultAddFormValues[18]['listDtoFieldValidationMessage'][0]['validationMessage'];
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
            else{
                
                if(f.valid &&  this.activeExchangeDetail[0].id != '0' && this.activeVendor[0].id != '0' && this.activeCurrency[0].id != '0' && this.activeCheckbook[0].id != '0')
                {
                    this.btndisabled=true;
                    this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                    if(this.model.manualPaymentCreateDate.formatted == undefined)
                    {
                        var manualPaymentCreateDate = this.model.manualPaymentCreateDate;
                        if(manualPaymentCreateDate.date != undefined)
                        {
                            this.model.manualPaymentCreateDate = manualPaymentCreateDate.date.day +'/'+ manualPaymentCreateDate.date.month +'/'+ manualPaymentCreateDate.date.year;
                        }
                    }
                    else
                    {
                        this.model.manualPaymentCreateDate = this.model.manualPaymentCreateDate.formatted;
                    }
                    if(this.activeCreditCard[0].id == '0')
                    {
                        this.model.creditCardID = '';
                    }
                    
                    this.model.manualPaymentAmount=this.unFrmtReceiptAmount;
                    this.manualPaymentEntryService.SaveUpdateManualPaymentEntry(this.model,this.isModify).then(data => {
                        this.manualPaymentEntryService.PostManualPaymentEntry(this.model).then(data => {
                            this.btndisabled=false;
                            window.scrollTo(0,0);
                            var datacode = data.code;
                            if (datacode == 200) {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.model.manualPaymentNumber='';
                                this.isModify = false;
                                this.showBtns=false;
                                
                                this.clear(f);
                                this.GetAllManualPaymentNumber();
                                this.model.manualPaymentAmount='';
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                    })
                }
                else{
                    if(this.activeExchangeDetail[0].id == '0')
                    {
                        myModal.open();
                    }
                }
            }
            this.btndisabled=false;   
        }
        
        
        
        
        openModel(myModal,action)
        {
            
            if(action == 'apCashReceiptEntryDistribution')
            {
                this.GetManualPaymentDistributionByManualPaymentNumber(this.model.manualPaymentNumber);
            }
            myModal.open();
        }
        
        varifyDelete()
        {
            this.isDeleteAction=true;
            this.confirmationModalBody = this.deleteConfirmationText;
            this.isConfirmationModalOpen = true;
        }
        
        
        closeModal()
        {
            this.isDeleteAction=false;
            this.isConfirmationModalOpen = false;
        }
        
        print()
        {
            window.print();
        }
        
        clear(f){
            if(!this.isModify)
            {
                this.model.manualPaymentNumber='';
                this.GetUniqueManualPaymentNumber();
            }
            else{
                this.model.manualPaymentNumber=this.activeManualPaymentNumber[0].id;
                
            }
            //this.isPostEvt=false;
            //this.isModify=false;
            //f.resetForm();
            this.model.manualPaymentDescription='';
            this.model.manualPaymentCreateDate='';
            this.venderName='';
            this.model.manualPaymentType='';
            this.model.manualPaymentAmount='';
            this.model.checkNumber='';
            this.model.creditCardNumber='';
            this.model.creditCardExpireMonth='';
            this.model.creditCardExpireYear='';
            this.model.exchangeTableRate='';
            this.model.exchangeExpirationDate='';
            this.activeManualPaymentNumber=[this.emptyArray[0]];
            //this.bindSelectedItem(this.emptyArray[0],'ManualPaymentNumber');
            this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail');
            this.bindSelectedItem(this.emptyArray[0],'Vendor');
            this.bindSelectedItem(this.emptyArray[0],'Batch');
            this.bindSelectedItem(this.emptyArray[0],'Currency');
            this.bindSelectedItem(this.emptyArray[0],'Checkbook');
            this.bindSelectedItem(this.emptyArray[0],'CreditCard');
        }
        
        Cancel(f){
            
            this.clear(f);
            f.resetForm();
            this.isModify=false;
            this.isPostEvt=false;
            this.bindSelectedItem(this.emptyArray[0],'ManualPaymentNumber');
            this.GetUniqueManualPaymentNumber();
        }
        
        onlyDecimalNumberKey(event) {
            return this.getScreenDetailService.onlyDecimalNumberKey(event);
        }
        
        //Change To Original Value of receiptAmount
        formatreceiptAmountChanged(){
            this.commonService.changeInputNumber("manualPaymentAmount");
            this.model.manualPaymentAmount = this.unFrmtReceiptAmount
        }
        // get Original receiptAmount
        getreceiptAmountValue(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
            }
            this.unFrmtReceiptAmount = event
        }
        
        formatreceiptAmount(){
            if(this.unFrmtReceiptAmount)
            {
                this.commonService.changeInputText("manualPaymentAmount")
                var inputValue = this.unFrmtReceiptAmount;
                this.model.manualPaymentAmount = this.unFrmtReceiptAmount;
                if (this.selectedCurrency !=undefined ){
                    let submitInfo = {
                        'currencyId':this.selectedCurrency
                    };
                    this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                        this.model.manualPaymentAmount = this.commonService.formatAmount(data, inputValue)
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                        }, 100)
                    });
                    
                    
                }else{
                    this.formatreceiptAmountChanged()
                }
                
                
            }
        }
        
        checkMonthValue(){
            if(this.model.creditCardExpireMonth<=0||this.model.creditCardExpireMonth>12){
                this.model.creditCardExpireMonth='';
            }
        }
        
        
        GetManualPaymentDistribution(f,ModalExchangeRateDetail,ModalDistribution){
            if(f.valid &&  this.activeExchangeDetail[0].id != '0' && this.activeVendor[0].id != '0' && this.activeCurrency[0].id != '0' && this.activeCheckbook[0].id != '0')
            {
                this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                if(this.model.manualPaymentCreateDate.formatted == undefined)
                {
                    var manualPaymentCreateDate = this.model.manualPaymentCreateDate;
                    if(manualPaymentCreateDate.date != undefined)
                    {
                        this.model.manualPaymentCreateDate = manualPaymentCreateDate.date.day +'/'+ manualPaymentCreateDate.date.month +'/'+ manualPaymentCreateDate.date.year;
                    }
                }
                else
                {
                    this.model.manualPaymentCreateDate = this.model.manualPaymentCreateDate.formatted;
                }
                if(this.activeCreditCard[0].id == '0')
                {
                    this.model.creditCardID = '';
                }
                
                this.model.manualPaymentAmount=this.unFrmtReceiptAmount;
                Object.assign(this.model, {'buttonType':'NEW_DISTRIBUTION'});
                this.manualPaymentEntryService.GetManualPaymentDistribution(this.model).then(data => {
                    ModalDistribution.open();
                    if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
                    {
                        this.arrAPDistributionEntry=data.result;
                    }
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            else{
                if(this.activeExchangeDetail[0].id == '0')
                {
                    ModalExchangeRateDetail.open();
                }
            }   
            this.btndisabled=false;     
        }
        
        /**
        * Get Default Distribution 
        */
        GetDefaultDistribution()
        {
            this.model.buttonType='DEFAULT_DISTRIBUTION'
            this.manualPaymentEntryService.GetManualPaymentDistribution(this.model).then(data => {
                if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
                {
                    this.IsAddDistributionDetails=false;
                    this.arrAPDistributionEntry=data.result;
                }
            });
        }
        
    }    