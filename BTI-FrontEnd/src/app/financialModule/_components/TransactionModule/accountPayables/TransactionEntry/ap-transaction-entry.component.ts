import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { TransactionEntrySetup } from '../../../../../financialModule/_models/transactionModule/accountPayables/transactionEntry';
import { TransactionEntryService } from '../../../../../financialModule/_services/transactionModule/accountPayables/transactionEntry.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
    templateUrl:'./ap-transaction-entry.component.html',
    providers:[TransactionEntryService,CommonService]
})

//export to make it available for other classes
export class APTransactionEntryComponent {
    page = new Page();
    rows = new Array<TransactionEntrySetup>();
    moduleCode = Constants.financialModuleCode;
    currentTransactionType=Constants.batchTransacitonType.AP_ENTER_VOID_TRANSACTION;
    screenCode;
    screenName;
    moduleName;
    distributionRequestData;
    distributionScreenModuleName = '';
    exchangeRateScreenModuleName = '';
    select=Constants.select;
    emptyArray=[{'id':'0','text':this.select}];    
    defaultAddFormValues: Array<object>;
    defaultExchangeRateFormValues: Array<object>;
    defaultDistributionFormValues: Array<object>;
    availableFormValues: [object];
    arrTransactionType=[];
    searchKeyword = '';
    selected = [];
    remainingAmount:number=0;
    tempRemainingAmount:number=0;
    tradeDiscountPercent:number = 0;
    miscVatPercent:number = 0;
    freightVatPercent:number  = 0;
    vatPercent:number = 0;
    accountTableRow:any={'id':'','text':''};
    activeVendor:any=this.emptyArray;
    activeAccountNumber:any=this.emptyArray;
    activeSalesman:any=this.emptyArray;
    activeSalesTerritory:any=this.emptyArray;
    activeCurrency:any=this.emptyArray;
    activeVat:any=this.emptyArray;
    activeBatch:any=this.emptyArray;
    activeCheckbook:any=this.emptyArray;
    activeShippingMethod:any=this.emptyArray;
    activePaymentTerm:any=this.emptyArray;
    activeCreditCard:any=this.emptyArray;
    activeExchangeDetail=this.emptyArray;
    activeTransactionNumberList=this.emptyArray;
    isScreenLock;
    private value:any = {};
    vendorName:string;
    venderNamePrimary:string;
    salesmanName:string;
    territoryName:string;
    dynamicTransactionTypeLabel:string;
    
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isDeleteAction : boolean = false;
    isConfirmationModalOpen:boolean=false;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    
    arrVendor=[];
    arrSalesman=[];
    arrSalesTerritory=[];
    arrCurrency=[];
    arrCheckbook=[];
    arrVat=[];
    arrTransactionNumberList=[];
    arrBatchList =  this.emptyArray;
    arrPaymentMethod:any=[];
    arrShippingMethod=[];
    arrPaymentTermSetup=[];
    arrCreditCard=[];
    arrExchangeDetail = this.emptyArray;
    arrAccountNumber=[];
    arrDistributionAccountTypes=[];
    distributionAccountTypeCode:string;
    distributionAccountType:string;
    IsAddDistributionDetails:boolean=false;
    debitAmount:number = 0;
    creditAmount:number = 0;
    distributionDescription:'';
    accountDescription:string;
    isCredit:boolean=false;
    paymentTransactionTypeId:number;
    btndisabled:boolean=false;
    arrAPDistributionEntry:any={
        "vendorId":"",
        "vendorName":"",
        "currencyID":"",
        "transactionType":"",
        "functionalAmount":"",
        "originalAmount":"",
        "listDtoAPDistributionDetail":[]
    }
    messageText;
    hasMsg = false;
    showMsg = false;
    PopUpmessageText;
    PopUphasMsg = false;
    isSuccessMsg;
    isfailureMsg;
    ddPageSize = 5;
    model: any = {};
    atATimeText=Constants.atATimeText;
    close=Constants.close;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    
    unFrmtPurchasesAmount;
    unFrmtTradeDiscount;
    unFrmtMiscellaneous;
    unFrmtFreightAmount;
    unFrmtVATAmount;
    unFrmtTotalAmount;
    unFrmtPaymentAmount;
    
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
    
    
    private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
    onDateChanged(event: IMyDateModel): void {
        var RequestedData =  {
            "transactionDate":event.formatted,
            "moduleName":"AP",
            "originId":Constants.massCloseOriginType.AP_Transaction_Entry
        }
        this.commonService.checkTransactionDateIsValid(RequestedData).then(data => {
            if(data.btiMessage.messageShort == "INVALID_TRANSACTION_DATE")
            {
                this.model.apTransactionDate = '';
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });   
    }
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private transactionEntryService: TransactionEntryService,
        private commonService: CommonService,
    )
    {
        // var userData = JSON.parse(localStorage.getItem('currentUser'));
    }
    
    ngOnInit() 
    {
        this.GetAddScreenDetail();
        this.GetAPTransactionType();
        this.GetExchangeRateScreenDetail();
        this.GetDistributionScreenDetail();
        this.GetVendorList();
        this.GetBatchList();
        this.GetVatDetailSetup();
        this.GetPaymentTermList();
        this.GetShippingMethodList();
        this.GetPaymentMethod();
        this.GetCurrencySetup();
        this.GetCheckbookMaintenance();
        this.GetCreditCardSetup();
        this.BindAccountNumber();
        this.GetDistributionAccountTypesList();
        this.GetInitialValues();
    }
    
    GetAddScreenDetail()
    {
        this.screenCode = "S-1253";
        this.defaultAddFormValues = [
            {'fieldName': 'AP_TR_ENTRY_TRANS_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_TRANS_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_TRANS_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_PAYMENT_TERM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_SHIPPING_METHOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_PURCHASE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_TRADE_DISCOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_MISCELLANEOUS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_FREIGHT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_VAT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_TOTAL_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_PAYMENT_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_CHECK_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_CREDIT_CARD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_CREDIT_CARD_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_EXPIRE_DATE_MM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_EXPIRE_DATE_YY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AP_TR_ENTRY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
        this.getScreenDetail(this.screenCode,'Add');
    }
    
    GetDistributionScreenDetail()
    {
        this.screenCode = "S-1254";
        this.defaultDistributionFormValues = [
            { 'fieldName':'AP_TR_ENTRY_DIS_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_VENDOR_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_TRANS_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_TRANS_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_FUNCT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_ORIGINAL_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_DIS_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_DEBIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AP_TR_ENTRY_DIS_DEFAULT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetail(this.screenCode,'Distribution');
        } 
        GetExchangeRateScreenDetail()
        {
            this.screenCode = "S-1238";
            this.defaultExchangeRateFormValues = [
                { 'fieldName': 'EX_TABLE_DET_JOURNAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'EX_TABLE_DET_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'EX_TABLE_DET_EX_TABLE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'EX_TABLE_DET_EX_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'EX_TABLE_DET_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'EX_TABLE_DET_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            ];
            this.getScreenDetail(this.screenCode,'ExchangeRate');
        }
        
        getScreenDetail(screenCode,ArrayType)
        {
            
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                this.screenName=data.result.dtoScreenDetail.screenName;
                
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;    
                if(ArrayType == 'Add'){
                    this.moduleName=data.result.moduleName;
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
                else if(ArrayType == 'Distribution'){
                    this.distributionScreenModuleName=data.result.moduleName;
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultDistributionFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
                else if(ArrayType == 'ExchangeRate'){
                    this.exchangeRateScreenModuleName=data.result.moduleName;
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultExchangeRateFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
            });
        }
        
        GetInitialValues(){
            this.model.purchasesAmount='0';
            this.model.apTransactionTradeDiscount='0';
            this.model.apTransactionMiscellaneous='0';
            this.model.apTransactionFreightAmount='0';
            this.model.apTransactionVATAmount='0';
            this.model.apTransactionTotalAmount='0';
            this.model.paymentAmount='0';
        }
        
        ChangeTransactionTypeLabel(evt)
        {
            this.dynamicTransactionTypeLabel=evt.target[evt.target.selectedIndex].text;
        }
        // get Batch List
        GetBatchList(){
            this.commonService.getAPBatchesByTransactionType(this.currentTransactionType).then(data => {
                this.arrBatchList=[{'id':'0','text':this.select}];
                
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    for(var i=0;i<data.result.length;i++)	
                    {
                        this.arrBatchList.push({'id':data.result[i].batchId,'text':data.result[i].batchId});
                    }
                }
            });
        }
        
        //Get Vat Detail Setup
        GetVatDetailSetup()
        {
            this.commonService.GetVatDetailSetup().then(data => {
                this.arrVat=[{'id':'0','text':this.select}];
                
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    for(var i=0;i<data.result.records.length;i++)	
                    {
                        this.arrVat.push({'id':data.result.records[i].vatScheduleId,'text':data.result.records[i].vatDescription});
                    }
                }
            });
        }
        
        //Get AR Transaction Type
        GetAPTransactionType()
        {
            
            this.arrTransactionType=[];
            this.transactionEntryService.GetTransactionType().then(data => {
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    
                    this.paymentTransactionTypeId=0;
                    var selectedTransactionType = data.result.find(x => x.code == 'PMT');
                    if(selectedTransactionType)
                    {
                        this.paymentTransactionTypeId = selectedTransactionType.typeId;
                    }
                    this.arrTransactionType = [];
                    for(var i=0; i < data.result.length;i++)
                    {
                        if(data.result[i].code != 'PMT' && data.result[i].code != 'SCP')
                        {
                            this.arrTransactionType.push({ "typeId":data.result[i].typeId, "name": data.result[i].name });
                        }
                    }
                    this.dynamicTransactionTypeLabel=this.arrTransactionType[0].name;
                    this.arrTransactionType.splice(0, 0, { "typeId": "", "name": this.select });
                    this.model.apTransactionType='';
                }
            });
        }
        
        //getTransactionNumber
        GetTransactionNumber(value,myform)
        {
            myform = <HTMLFormElement>document.getElementById('APTransactionEntry');
            if(value != '')
            {
                this.clear(myform,0);
                // this.GetARTransactionType();
                this.isModify=false;
                this.transactionEntryService.GetTransactionNumber(value).then(data => {
                    this.model.apTransactionNumber=data.result;
                });
            }
            else{
                this.activeTransactionNumberList=[this.emptyArray[0]];
                this.arrTransactionNumberList=this.emptyArray;
                this.model.apTransactionNumber='';
            }
            
        }
        
        GetDistributionDetailByTransactionNumber(TransactionNumber)
        {
            this.transactionEntryService.GetDistributionDetailByTransactionNumber(TransactionNumber).then(data => {
                if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
                {
                    this.arrAPDistributionEntry=data.result;
                }
            });
        }
        
        /**
        * Distribution detail by Transaction
        * @param Transaction - Pass Transaction Entry Data  
        */
        GetDistributionDetailByTransaction(objTransaction)
        {
            
            this.transactionEntryService.GetDistributionDetailByTransaction(objTransaction).then(data => {
                if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
                {
                    this.arrAPDistributionEntry=data.result;
                }
            });
        }
        
        /**
        * Get Default Distribution 
        */
        GetDefaultDistribution()
        {
            
            if(this.distributionRequestData)
            {
                this.distributionRequestData.buttonType='DEFAULT_DISTRIBUTION'
                this.transactionEntryService.GetDistributionDetailByTransaction(this.distributionRequestData).then(data => {
                    if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
                    {
                        this.arrAPDistributionEntry=data.result;
                    }
                });
            }
        }
        
        GetTransactionEntryByTransactionNumber(TransactionNumber)
        {
            this.transactionEntryService.GetTransactionEntryByTransactionNumber(TransactionNumber).then(data => {
                this.isModify=true;
                this.model =data.result;
                this.unFrmtPurchasesAmount=this.model.purchasesAmount;
                this.unFrmtMiscellaneous=this.model.apTransactionMiscellaneous;
                this.unFrmtTradeDiscount=this.model.apTransactionTradeDiscount;
                this.unFrmtFreightAmount = this.model.apTransactionFreightAmount;
                this.unFrmtVATAmount = this.model.apTransactionVATAmount;
                this.unFrmtTotalAmount=this.model.apTransactionTotalAmount;
                this.unFrmtPaymentAmount=this.model.paymentAmount;
                if(!this.unFrmtPaymentAmount)
                {
                    this.unFrmtPaymentAmount = 0;
                }
                this.model.apTransactionMiscellaneous=this.model.apTransactionMiscellaneous.toString();
                this.model.apTransactionFreightAmount=this.model.apTransactionFreightAmount.toString();
                var apTransactionDebitMemoAmount=this.model.apTransactionDebitMemoAmount; // DR
                var apTransactionFinanceChargeAmount=this.model.apTransactionFinanceChargeAmount; // FIN
                var apTransactionWarrantyAmount=this.model.apTransactionWarrantyAmount; // WRN
                var apTransactionCreditMemoAmount=this.model.apTransactionCreditMemoAmount; // CR
                var apTransactionCashAmount=this.model.apTransactionCashAmount; 
                var apTransactionCheckAmount=this.model.apTransactionCheckAmount; 
                var apTransactionCreditCardAmount=this.model.apTransactionCreditCardAmount; // NA
                var apServiceRepairAmount=this.model.apServiceRepairAmount; // SVC
                var apReturnAmount=this.model.apReturnAmount; // RTN
                var purchasesAmount=this.model.purchasesAmount; // SLS
                
                
                
                var transactionType= this.model.apTransactionNumber.split('-');
                if(transactionType[0]== 'FIN')
                {
                    this.model.purchasesAmount=apTransactionFinanceChargeAmount;
                }
                else if(transactionType[0]== 'WRN')
                {
                    this.model.purchasesAmount=apTransactionWarrantyAmount;
                }
                else if(transactionType[0]== 'CRM')
                {
                    this.model.purchasesAmount=apTransactionCreditMemoAmount;
                }
                else if(transactionType[0]== 'RTN')
                {
                    this.model.purchasesAmount=apReturnAmount;
                }
                else if(transactionType[0]== 'DR')
                {
                    this.model.purchasesAmount=apTransactionDebitMemoAmount;
                }
                else if(transactionType[0]== 'SVC')
                {
                    this.model.purchasesAmount=apServiceRepairAmount;
                }
                else if(transactionType[0]== 'INV')
                {
                    this.model.purchasesAmount=purchasesAmount;
                }
                
                var apTransactionDate = this.model.apTransactionDate;
                var apTransactionDateData = apTransactionDate.split('/');
                this.model.apTransactionDate = {"date":{"year":parseInt(apTransactionDateData[2]),"month":parseInt(apTransactionDateData[1]),"day":parseInt(apTransactionDateData[0])}};
                
                
                var selectedVendor = this.arrVendor.find(x => x.id ==   data.result.vendorID);
                this.activeVendor=[selectedVendor];
                this.transactionEntryService.GetVendorAccountDetailByVendorId(selectedVendor.id).then(data => {
                    this.vendorName=data.result.vendorName;
                    this.tradeDiscountPercent=data.result.tradeDiscountPercent;
                    this.miscVatPercent=data.result.miscVatPercent;
                    this.freightVatPercent=data.result.freightVatPercent;
                    this.vatPercent=data.result.vatPercent;
                });
                
                var selectedShippingMethod = this.arrShippingMethod.find(x => x.id ==   data.result.shippingMethodId);
                this.bindSelectedItem(selectedShippingMethod,'ShippingMethod');
                
                var selectedPaymentTerm = this.arrPaymentTermSetup.find(x => x.id ==   data.result.paymentTermsID);
                this.bindSelectedItem(selectedPaymentTerm,'PaymentTerm');
                //this.activePaymentTerm=[selectedPaymentTerm];
                
                
                // this.activeSalesman=[selectedSalesMan];
                
                var selectedCheckBook = this.arrCheckbook.find(x => x.id ==  data.result.checkbookID );
                //this.bindSelectedItem(selectedCheckBook,'Checkbook');
                this.activeCheckbook=[selectedCheckBook];
                
                var selectedCurrency = this.arrCurrency.find(x => x.id ==  data.result.currencyID );
                //this.bindSelectedItem(selectedCurrency,'Currency');
                if(selectedCurrency)
                {
                    this.activeCurrency=[selectedCurrency];
                    this.formatSaleAmount();
                    this.formatMiscAmount();
                    this.formatTradeAmount();
                    this.formatFreightAmount();
                    this.formatVatAmount();
                    this.formatTotalAmount();
                    this.formatPaymentAmount();
                    this.GetExchangeTableSetupByCurrencyId(selectedCurrency.id);
                }
                
                
                
                var selectedVat = this.arrVat.find(x => x.id ==  data.result.vatScheduleID );
                //this.bindSelectedItem(selectedVat,'Vat');
                this.activeVat=[selectedVat];
                
                var selectedcreditCardID = this.arrCreditCard.find(x => x.id ==  data.result.creditCardID );
                //this.bindSelectedItem(selectedBatch,'Batch');
                this.activeCreditCard=[selectedcreditCardID]; 
                
                if(data.result.batchID)
                {
                    var selectedBatch = this.arrBatchList.find(x => x.id ==  data.result.batchID );
                    //this.bindSelectedItem(selectedBatch,'Batch');
                    this.activeBatch=[selectedBatch];
                }
                
                window.setTimeout(() => {
                    
                    var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id ==  data.result.exchangeTableIndex );
                    this.activeExchangeDetail=[selectedExchangeDetail];
                    
                    if(selectedExchangeDetail)
                    {
                        this.GetCurrencyExchangeDetail(selectedExchangeDetail.text);
                    }
                    
                }, 1000);
            });
        }
        GetTransactionNumberByTransactionType(evt)
        {
            this.GetTransactionNumber(evt.target.value,myform);
            if(evt.target.value == ''){
                this.dynamicTransactionTypeLabel=this.arrTransactionType[1].name;
            }
            else{
                this.dynamicTransactionTypeLabel=evt.target[evt.target.selectedIndex].text;
            }
            if(evt.target.value != '')
            {
                this.model.apTransactionType = evt.target.value;
                this.transactionEntryService.GetTransactionNumberByTransactionType(evt.target.value).then(data => {
                    // this.arrTransactionNumberList=data.result;
                    
                    this.arrTransactionNumberList=[{'id':'0','text':this.select}];
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.length;i++)	
                        {
                            this.arrTransactionNumberList.push({'id':data.result[i],'text':data.result[i]});
                        }
                    }
                    else{
                        this.activeTransactionNumberList=[{'id':'0','text':this.select}]
                    }
                }); 
            }
            
            else{
                
                var myform = <HTMLFormElement>document.getElementById('APTransactionEntry');
                if(myform)
                {
                    this.clear(myform,0)
                    
                }
            }
        }
        
        PaymentTypeChange(evt)
        {
            var PaymentMethodId=evt.target.value;
            if(PaymentMethodId == 1)
            {
                this.model.checkNumber='';
                this.model.creditCardExpireMonth='';
                this.model.creditCardExpireYear='';
                this.model.creditCardNumber='';
                this.bindSelectedItem(this.emptyArray[0],'CreditCard');
            }
            else if(PaymentMethodId == 2)
            {
                this.model.creditCardExpireMonth='';
                this.model.creditCardExpireYear='';
                this.model.creditCardNumber='';
                this.bindSelectedItem(this.emptyArray[0],'CreditCard');
            }
            else if(PaymentMethodId == 3){
                this.model.checkNumber='';
            }
        }
        
        openDistribution(f:NgForm,myModal,ExchangeModal)
        {
            //  myModal.open();
            if(f.valid  && this.activeVendor[0].id != '0'  
            && this.activeCurrency[0].id != '0'  && this.activePaymentTerm[0].id != '0' 
            && this.activeShippingMethod[0].id != '0' && this.activeVat[0].id != '0' 
            && this.activeCheckbook[0].id != '0' && this.activeExchangeDetail[0].id != '0' && this.model.exchangeTableRate != '')
            {
                //  this.btndisabled=true;
                if(this.model.apTransactionDate.formatted == undefined)
                {
                    var transactionDate = this.model.apTransactionDate;
                    if(transactionDate.date != undefined)
                    {
                        this.model.apTransactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
                    }
                }
                else
                {
                    this.model.apTransactionDate = this.model.apTransactionDate.formatted;
                }
                
                
                var checkbookID =this.model.checkbookID 
                if(!this.model.checkbookID)
                {
                    checkbookID=0;
                }
                var checkNumber =this.model.checkNumber 
                if(!this.model.checkNumber)
                {
                    checkNumber=0;
                }
                
                var cashReceiptNumber =this.model.cashReceiptNumber 
                if(!this.model.cashReceiptNumber)
                {
                    cashReceiptNumber=0;
                }
                var creditCardID =this.model.creditCardID 
                if(!this.model.creditCardID)
                {
                    creditCardID=0;
                }
                
                var creditCardNumber =this.model.creditCardNumber 
                if(!this.model.creditCardNumber)
                {
                    creditCardNumber=0;
                }
                
                var creditCardExpireYear =this.model.creditCardExpireYear 
                if(!this.model.creditCardExpireYear)
                {
                    creditCardExpireYear=0;
                }
                
                var creditCardExpireMonth =this.model.creditCardExpireMonth 
                if(!this.model.creditCardExpireMonth)
                {
                    creditCardExpireMonth=0;
                }
                
                var apTransactionStatus =this.model.apTransactionStatus 
                if(!this.model.apTransactionStatus)
                {
                    apTransactionStatus=0;
                }
                
                var venderNameArabic =this.model.venderNameArabic 
                if(!this.model.venderNameArabic)
                {
                    venderNameArabic='';
                }
                
                var isPayment=true;
                if(this.model.apTransactionTotalAmount == 0)
                {
                    isPayment=false;
                }
                
                var exchangeTableIndex= '0';
                if(this.activeExchangeDetail[0].id != '0')
                {
                    exchangeTableIndex = this.activeExchangeDetail[0].id
                }
                
                
                var apTransactionDebitMemoAmount="0"; // DR
                var apTransactionFinanceChargeAmount="0"; // FIN
                var apTransactionWarrantyAmount="0"; // WRN
                var apTransactionCreditMemoAmount="0"; // CR
                var apTransactionCashAmount="0"; 
                var apTransactionCheckAmount="0"; 
                var apTransactionCreditCardAmount="0"; // NA
                var apServiceRepairAmount="0"; // SVC
                var apReturnAmount="0"; // RTN
                var purchasesAmount="0"; // SLS
                
                this.model.purchasesAmount=this.unFrmtPurchasesAmount;
                this.model.apTransactionMiscellaneous=this.unFrmtMiscellaneous;
                this.model.apTransactionTradeDiscount=this.unFrmtTradeDiscount;
                this.model.apTransactionFreightAmount=this.unFrmtFreightAmount;
                this.model.apTransactionTotalAmount=this.unFrmtTotalAmount;
                this.model.apTransactionVATAmount=this.unFrmtVATAmount;
                this.model.paymentAmount=this.unFrmtPaymentAmount;
                
                var transactionType= this.model.apTransactionNumber.split('-');
                if(transactionType[0]== 'FIN')
                {
                    apTransactionFinanceChargeAmount=this.model.purchasesAmount;
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apReturnAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'WRN')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount=this.model.purchasesAmount;
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apReturnAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'CRM')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    apTransactionCreditMemoAmount=this.model.purchasesAmount;
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apReturnAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'RTN')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apReturnAmount=this.model.purchasesAmount;
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apTransactionWarrantyAmount="0";
                    purchasesAmount="0";
                }
                
                else if(transactionType[0]== 'DR')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount=this.model.purchasesAmount;
                    apReturnAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apTransactionWarrantyAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'SVC')
                {
                    apTransactionFinanceChargeAmount="0";
                    apServiceRepairAmount=this.model.purchasesAmount;
                    apReturnAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'INV')
                {
                    apTransactionFinanceChargeAmount="0";
                    purchasesAmount=this.model.purchasesAmount;
                    apReturnAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    apServiceRepairAmount="0";
                }
                
                this.distributionRequestData={
                    "apTransactionNumber": this.model.apTransactionNumber,
                    "apTransactionType": this.model.apTransactionType,
                    "batchID": this.model.batchID,
                    "apTransactionDescription":this.model.apTransactionDescription,
                    "apTransactionDate": this.model.apTransactionDate,
                    "vendorID": this.activeVendor[0].id,
                    "vendorName":this.vendorName,
                    "vendorNameArabic":venderNameArabic,
                    "documentNumberOfVendor ":this.activeVendor[0].id,
                    "currencyID": this.model.currencyID,
                    "paymentTermsID": this.model.paymentTermsID,
                    "apTransactionCost": this.model.apTransactionCost,
                    "purchasesAmount": purchasesAmount,
                    "apTransactionTradeDiscount": this.model.apTransactionTradeDiscount,
                    "apTransactionFreightAmount": this.model.apTransactionFreightAmount,
                    "apTransactionMiscellaneous": this.model.apTransactionMiscellaneous,
                    "apTransactionVATAmount": this.model.apTransactionVATAmount,
                    "apTransactionFinanceChargeAmount": apTransactionFinanceChargeAmount,
                    "apTransactionCreditMemoAmount": apTransactionCreditMemoAmount,
                    "apTransactionTotalAmount": this.model.apTransactionTotalAmount,
                    "apTransactionCashAmount":0,
                    "apTransactionCheckAmount": apTransactionCheckAmount,
                    "apTransactionCreditCardAmount":apTransactionCreditCardAmount,
                    "apTransactionDebitMemoAmount": apTransactionDebitMemoAmount,
                    "apServiceRepairAmount":apServiceRepairAmount,
                    "apTransactionWarrantyAmount": apTransactionWarrantyAmount,
                    "apReturnAmount":apReturnAmount,
                    "checkbookID":checkbookID,
                    "checkNumber": checkNumber,
                    "cashReceiptNumber":cashReceiptNumber,
                    "creditCardID":creditCardID,
                    "creditCardNumber":creditCardNumber,
                    "creditCardExpireYear": creditCardExpireYear,
                    "creditCardExpireMonth":creditCardExpireMonth,
                    "apTransactionStatus": apTransactionStatus,
                    "exchangeTableIndex":exchangeTableIndex,
                    "exchangeTableRate":this.model.exchangeTableRate,
                    "transactionVoid":0,
                    "shippingMethodId": this.activeShippingMethod[0].id,
                    "vatScheduleID":this.activeVat[0].id,
                    "paymentTypeId":this.model.paymentTypeId,
                    "isPayment":isPayment,
                    "paymentAmount":this.model.paymentAmount,
                    "paymentTransactionTypeId": this.paymentTransactionTypeId,
                    "buttonType": "NEW_DISTRIBUTION"
                };
                if(this.activeCreditCard[0].id == '0')
                {
                    this.model.creditCardID = '';
                }
                
                if(this.model.apTransactionNumber)
                {
                    this.IsAddDistributionDetails=false;
                    this.GetDistributionDetailByTransaction(this.distributionRequestData)
                    this.formatSaleAmount();
                    this.formatMiscAmount();
                    this.formatTradeAmount();
                    this.formatFreightAmount();
                    this.formatVatAmount();
                    this.formatTotalAmount();
                    this.formatPaymentAmount();
                }
                myModal.open();
            }
            else{
                
                if(this.activeExchangeDetail[0].id == '0')
                {
                    ExchangeModal.open();
                }
            }
            
        }
        
        SaveAPTransactionEntry(f :NgForm,myModal){
            if(f.valid  && this.activeVendor[0].id != '0'  
            && this.activeCurrency[0].id != '0'  && this.activePaymentTerm[0].id != '0' 
            && this.activeShippingMethod[0].id != '0' && this.activeVat[0].id != '0' 
            && this.activeCheckbook[0].id != '0' && this.activeExchangeDetail[0].id != '0' && this.model.exchangeTableRate != '')
            {
                this.btndisabled=true;
                if(this.model.apTransactionDate.formatted == undefined)
                {
                    var transactionDate = this.model.apTransactionDate;
                    if(transactionDate.date != undefined)
                    {
                        this.model.apTransactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
                    }
                }
                else
                {
                    this.model.apTransactionDate = this.model.apTransactionDate.formatted;
                }
                
                
                var checkbookID =this.model.checkbookID 
                if(!this.model.checkbookID)
                {
                    checkbookID=0;
                }
                var checkNumber =this.model.checkNumber 
                if(!this.model.checkNumber)
                {
                    checkNumber=0;
                }
                
                var cashReceiptNumber =this.model.cashReceiptNumber 
                if(!this.model.cashReceiptNumber)
                {
                    cashReceiptNumber=0;
                }
                var creditCardID =this.model.creditCardID 
                if(!this.model.creditCardID)
                {
                    creditCardID=0;
                }
                
                var creditCardNumber =this.model.creditCardNumber 
                if(!this.model.creditCardNumber)
                {
                    creditCardNumber=0;
                }
                
                var creditCardExpireYear =this.model.creditCardExpireYear 
                if(!this.model.creditCardExpireYear)
                {
                    creditCardExpireYear=0;
                }
                
                var creditCardExpireMonth =this.model.creditCardExpireMonth 
                if(!this.model.creditCardExpireMonth)
                {
                    creditCardExpireMonth=0;
                }
                
                var apTransactionStatus =this.model.apTransactionStatus 
                if(!this.model.apTransactionStatus)
                {
                    apTransactionStatus=0;
                }
                
                var venderNameArabic =this.model.venderNameArabic 
                if(!this.model.venderNameArabic)
                {
                    venderNameArabic='';
                }
                
                var isPayment=true;
                if(this.model.apTransactionTotalAmount == 0)
                {
                    isPayment=false;
                }
                
                var exchangeTableIndex= '0';
                if(this.activeExchangeDetail[0].id != '0')
                {
                    exchangeTableIndex = this.activeExchangeDetail[0].id
                }
                
                
                var apTransactionDebitMemoAmount="0"; // DR
                var apTransactionFinanceChargeAmount="0"; // FIN
                var apTransactionWarrantyAmount="0"; // WRN
                var apTransactionCreditMemoAmount="0"; // CR
                var apTransactionCashAmount="0"; 
                var apTransactionCheckAmount="0"; 
                var apTransactionCreditCardAmount="0"; // NA
                var apServiceRepairAmount="0"; // SVC
                var apReturnAmount="0"; // RTN
                var purchasesAmount="0"; // SLS
                
                this.model.purchasesAmount=this.unFrmtPurchasesAmount;
                this.model.apTransactionMiscellaneous=this.unFrmtMiscellaneous;
                this.model.apTransactionTradeDiscount=this.unFrmtTradeDiscount;
                this.model.apTransactionFreightAmount=this.unFrmtFreightAmount;
                this.model.apTransactionTotalAmount=this.unFrmtTotalAmount;
                this.model.apTransactionVATAmount=this.unFrmtVATAmount;
                this.model.paymentAmount=this.unFrmtPaymentAmount;
                
                var transactionType= this.model.apTransactionNumber.split('-');
                if(transactionType[0]== 'FIN')
                {
                    apTransactionFinanceChargeAmount=this.model.purchasesAmount;
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apReturnAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'WRN')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount=this.model.purchasesAmount;
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apReturnAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'CRM')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    apTransactionCreditMemoAmount=this.model.purchasesAmount;
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apReturnAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'RTN')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apReturnAmount=this.model.purchasesAmount;
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apTransactionWarrantyAmount="0";
                    purchasesAmount="0";
                }
                
                else if(transactionType[0]== 'DR')
                {
                    apTransactionFinanceChargeAmount="0";
                    apTransactionDebitMemoAmount=this.model.purchasesAmount;
                    apReturnAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apServiceRepairAmount="0";
                    apTransactionWarrantyAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'SVC')
                {
                    apTransactionFinanceChargeAmount="0";
                    apServiceRepairAmount=this.model.purchasesAmount;
                    apReturnAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    purchasesAmount="0";
                }
                else if(transactionType[0]== 'INV')
                {
                    apTransactionFinanceChargeAmount="0";
                    purchasesAmount=this.model.purchasesAmount;
                    apReturnAmount="0";
                    apTransactionCreditMemoAmount="0";
                    apTransactionCashAmount="0";
                    apTransactionCheckAmount="0";
                    apTransactionCreditCardAmount="0";
                    apTransactionDebitMemoAmount="0";
                    apTransactionWarrantyAmount="0";
                    apServiceRepairAmount="0";
                }
                
                var requestedData={
                    "apTransactionNumber": this.model.apTransactionNumber,
                    "apTransactionType": this.model.apTransactionType,
                    "batchID": this.model.batchID,
                    "apTransactionDescription":this.model.apTransactionDescription,
                    "apTransactionDate": this.model.apTransactionDate,
                    "vendorID": this.activeVendor[0].id,
                    "vendorName":this.vendorName,
                    "vendorNameArabic":venderNameArabic,
                    "documentNumberOfVendor ":this.activeVendor[0].id,
                    "currencyID": this.model.currencyID,
                    "paymentTermsID": this.model.paymentTermsID,
                    "apTransactionCost": this.model.apTransactionCost,
                    "purchasesAmount": purchasesAmount,
                    "apTransactionTradeDiscount": this.model.apTransactionTradeDiscount,
                    "apTransactionFreightAmount": this.model.apTransactionFreightAmount,
                    "apTransactionMiscellaneous": this.model.apTransactionMiscellaneous,
                    "apTransactionVATAmount": this.model.apTransactionVATAmount,
                    "apTransactionFinanceChargeAmount": apTransactionFinanceChargeAmount,
                    "apTransactionCreditMemoAmount": apTransactionCreditMemoAmount,
                    "apTransactionTotalAmount": this.model.apTransactionTotalAmount,
                    "apTransactionCashAmount":0,
                    "apTransactionCheckAmount": apTransactionCheckAmount,
                    "apTransactionCreditCardAmount":apTransactionCreditCardAmount,
                    "apTransactionDebitMemoAmount": apTransactionDebitMemoAmount,
                    "apServiceRepairAmount":apServiceRepairAmount,
                    "apTransactionWarrantyAmount": apTransactionWarrantyAmount,
                    "apReturnAmount":apReturnAmount,
                    "checkbookID":checkbookID,
                    "checkNumber": checkNumber,
                    "cashReceiptNumber":cashReceiptNumber,
                    "creditCardID":creditCardID,
                    "creditCardNumber":creditCardNumber,
                    "creditCardExpireYear": creditCardExpireYear,
                    "creditCardExpireMonth":creditCardExpireMonth,
                    "apTransactionStatus": apTransactionStatus,
                    "exchangeTableIndex":exchangeTableIndex,
                    "exchangeTableRate":this.model.exchangeTableRate,
                    "transactionVoid":0,
                    "shippingMethodId": this.activeShippingMethod[0].id,
                    "vatScheduleID":this.activeVat[0].id,
                    "paymentTypeId":this.model.paymentTypeId,
                    "isPayment":isPayment,
                    "paymentAmount":this.model.paymentAmount,
                    "paymentTransactionTypeId": this.paymentTransactionTypeId
                };
                if(this.activeCreditCard[0].id == '0')
                {
                    this.model.creditCardID = '';
                }
                this.transactionEntryService.SaveUpdateTransactionEntry(requestedData,this.isModify).then(data => {
                    
                    window.scrollTo(0,0);
                    var datacode = data.code;
                    this.btndisabled=false;
                    if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.isModify=false;
                        this.clear(f,1);
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
                
            }
            else{
                
                if(this.activeExchangeDetail[0].id == '0' || this.model.exchangeTableRate == '')
                {
                    myModal.open();
                }
            }
            
            this.btndisabled=false;
        }
        
        Amount(paymentAmount){
            if(this.model.apTransactionTotalAmount >=paymentAmount){
                var paymentAmount1=this.model.paymentAmount;
            }
            else{
                window.scrollTo(0,0);
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = "Total Amount should be greater than Payment Amount";
                this.model.paymentAmount='';
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }
        
        PostAPTransactionEntry(f :NgForm,myModal){
            
            this.btndisabled=true;
            if(f.valid && this.activeBatch[0].id !='0')
            {
                window.scrollTo(0,0);
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText =this.defaultAddFormValues[27]['listDtoFieldValidationMessage'][0]['validationMessage'];
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
            else{
                
                
                if(f.valid  && this.activeVendor[0].id != '0'  
                && this.activeCurrency[0].id != '0'  && this.activePaymentTerm[0].id != '0' 
                && this.activeShippingMethod[0].id != '0' && this.activeVat[0].id != '0' 
                && this.activeCheckbook[0].id != '0' && this.activeExchangeDetail[0].id != '0')
                {
                    
                    if(this.model.apTransactionDate.formatted == undefined)
                    {
                        var transactionDate = this.model.apTransactionDate;
                        if(transactionDate.date != undefined)
                        {
                            this.model.apTransactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
                        }
                    }
                    else
                    {
                        this.model.apTransactionDate = this.model.apTransactionDate.formatted;
                    }
                    
                    
                    var checkbookID =this.model.checkbookID 
                    if(!this.model.checkbookID)
                    {
                        checkbookID=0;
                    }
                    var checkNumber =this.model.checkNumber 
                    if(!this.model.checkNumber)
                    {
                        checkNumber=0;
                    }
                    
                    var cashReceiptNumber =this.model.cashReceiptNumber 
                    if(!this.model.cashReceiptNumber)
                    {
                        cashReceiptNumber=0;
                    }
                    var creditCardID =this.model.creditCardID 
                    if(!this.model.creditCardID)
                    {
                        creditCardID=0;
                    }
                    
                    var creditCardNumber =this.model.creditCardNumber 
                    if(!this.model.creditCardNumber)
                    {
                        creditCardNumber=0;
                    }
                    
                    var creditCardExpireYear =this.model.creditCardExpireYear 
                    if(!this.model.creditCardExpireYear)
                    {
                        creditCardExpireYear=0;
                    }
                    
                    var creditCardExpireMonth =this.model.creditCardExpireMonth 
                    if(!this.model.creditCardExpireMonth)
                    {
                        creditCardExpireMonth=0;
                    }
                    
                    var apTransactionStatus =this.model.apTransactionStatus 
                    if(!this.model.apTransactionStatus)
                    {
                        apTransactionStatus=0;
                    }
                    
                    var venderNameArabic =this.model.venderNameArabic 
                    if(!this.model.venderNameArabic)
                    {
                        venderNameArabic='';
                    }
                    
                    var isPayment=true;
                    if(this.model.apTransactionTotalAmount == 0)
                    {
                        isPayment=false;
                    }
                    
                    var exchangeTableIndex= '0';
                    if(this.activeExchangeDetail[0].id != '0')
                    {
                        exchangeTableIndex = this.activeExchangeDetail[0].id
                    }
                    
                    
                    var apTransactionDebitMemoAmount="0"; // DR
                    var apTransactionFinanceChargeAmount="0"; // FIN
                    var apTransactionWarrantyAmount="0"; // WRN
                    var apTransactionCreditMemoAmount="0"; // CR
                    var apTransactionCashAmount="0"; 
                    var apTransactionCheckAmount="0"; 
                    var apTransactionCreditCardAmount="0"; // NA
                    var apServiceRepairAmount="0"; // SVC
                    var apReturnAmount="0"; // RTN
                    var purchasesAmount="0"; // SLS
                    
                    this.model.purchasesAmount=this.unFrmtPurchasesAmount;
                    this.model.apTransactionMiscellaneous=this.unFrmtMiscellaneous;
                    this.model.apTransactionTradeDiscount=this.unFrmtTradeDiscount;
                    this.model.apTransactionFreightAmount=this.unFrmtFreightAmount;
                    this.model.apTransactionTotalAmount=this.unFrmtTotalAmount;
                    this.model.apTransactionVATAmount=this.unFrmtVATAmount;
                    this.model.paymentAmount=this.unFrmtPaymentAmount;
                    
                    
                    var transactionType= this.model.apTransactionNumber.split('-');
                    if(transactionType[0]== 'FIN')
                    {
                        apTransactionFinanceChargeAmount=this.model.purchasesAmount;
                        apTransactionDebitMemoAmount="0";
                        apTransactionWarrantyAmount="0";
                        apTransactionCreditMemoAmount="0";
                        apTransactionCashAmount="0";
                        apTransactionCheckAmount="0";
                        apTransactionCreditCardAmount="0";
                        apServiceRepairAmount="0";
                        apReturnAmount="0";
                        purchasesAmount="0";
                    }
                    else if(transactionType[0]== 'WRN')
                    {
                        apTransactionFinanceChargeAmount="0";
                        apTransactionDebitMemoAmount="0";
                        apTransactionWarrantyAmount=this.model.purchasesAmount;
                        apTransactionCreditMemoAmount="0";
                        apTransactionCashAmount="0";
                        apTransactionCheckAmount="0";
                        apTransactionCreditCardAmount="0";
                        apServiceRepairAmount="0";
                        apReturnAmount="0";
                        purchasesAmount="0";
                    }
                    else if(transactionType[0]== 'CRM')
                    {
                        apTransactionFinanceChargeAmount="0";
                        apTransactionDebitMemoAmount="0";
                        apTransactionWarrantyAmount="0";
                        apTransactionCreditMemoAmount=this.model.purchasesAmount;
                        apTransactionCashAmount="0";
                        apTransactionCheckAmount="0";
                        apTransactionCreditCardAmount="0";
                        apServiceRepairAmount="0";
                        apReturnAmount="0";
                        purchasesAmount="0";
                    }
                    else if(transactionType[0]== 'RTN')
                    {
                        apTransactionFinanceChargeAmount="0";
                        apTransactionDebitMemoAmount="0";
                        apReturnAmount=this.model.purchasesAmount;
                        apTransactionCreditMemoAmount="0";
                        apTransactionCashAmount="0";
                        apTransactionCheckAmount="0";
                        apTransactionCreditCardAmount="0";
                        apServiceRepairAmount="0";
                        apTransactionWarrantyAmount="0";
                        purchasesAmount="0";
                    }
                    
                    else if(transactionType[0]== 'DR')
                    {
                        apTransactionFinanceChargeAmount="0";
                        apTransactionDebitMemoAmount=this.model.purchasesAmount;
                        apReturnAmount="0";
                        apTransactionCreditMemoAmount="0";
                        apTransactionCashAmount="0";
                        apTransactionCheckAmount="0";
                        apTransactionCreditCardAmount="0";
                        apServiceRepairAmount="0";
                        apTransactionWarrantyAmount="0";
                        purchasesAmount="0";
                    }
                    else if(transactionType[0]== 'SVC')
                    {
                        apTransactionFinanceChargeAmount="0";
                        apServiceRepairAmount=this.model.purchasesAmount;
                        apReturnAmount="0";
                        apTransactionCreditMemoAmount="0";
                        apTransactionCashAmount="0";
                        apTransactionCheckAmount="0";
                        apTransactionCreditCardAmount="0";
                        apTransactionDebitMemoAmount="0";
                        apTransactionWarrantyAmount="0";
                        purchasesAmount="0";
                    }
                    else if(transactionType[0]== 'INV')
                    {
                        apTransactionFinanceChargeAmount="0";
                        purchasesAmount=this.model.purchasesAmount;
                        apReturnAmount="0";
                        apTransactionCreditMemoAmount="0";
                        apTransactionCashAmount="0";
                        apTransactionCheckAmount="0";
                        apTransactionCreditCardAmount="0";
                        apTransactionDebitMemoAmount="0";
                        apTransactionWarrantyAmount="0";
                        apServiceRepairAmount="0";
                    }
                    
                    var requestedData={
                        "apTransactionNumber": this.model.apTransactionNumber,
                        "apTransactionType": this.model.apTransactionType,
                        "batchID": this.model.batchID,
                        "apTransactionDescription":this.model.apTransactionDescription,
                        "apTransactionDate": this.model.apTransactionDate,
                        "vendorID": this.activeVendor[0].id,
                        "vendorName":this.vendorName,
                        "vendorNameArabic":venderNameArabic,
                        "documentNumberOfVendor ":this.activeVendor[0].id,
                        "currencyID": this.model.currencyID,
                        "paymentTermsID": this.model.paymentTermsID,
                        "apTransactionCost": this.model.apTransactionCost,
                        "purchasesAmount": purchasesAmount,
                        "apTransactionTradeDiscount": this.model.apTransactionTradeDiscount,
                        "apTransactionFreightAmount": this.model.apTransactionFreightAmount,
                        "apTransactionMiscellaneous": this.model.apTransactionMiscellaneous,
                        "apTransactionVATAmount": this.model.apTransactionVATAmount,
                        "apTransactionFinanceChargeAmount": apTransactionFinanceChargeAmount,
                        "apTransactionCreditMemoAmount": apTransactionCreditMemoAmount,
                        "apTransactionTotalAmount": this.model.apTransactionTotalAmount,
                        "apTransactionCashAmount":0,
                        "apTransactionCheckAmount": apTransactionCheckAmount,
                        "apTransactionCreditCardAmount":apTransactionCreditCardAmount,
                        "apTransactionDebitMemoAmount": apTransactionDebitMemoAmount,
                        "apServiceRepairAmount":this.model.apServiceRepairAmount,
                        "apTransactionWarrantyAmount": apTransactionWarrantyAmount,
                        "apReturnAmount":this.model.apReturnAmount,
                        "checkbookID":checkbookID,
                        "checkNumber": checkNumber,
                        "cashReceiptNumber":cashReceiptNumber,
                        "creditCardID":creditCardID,
                        "creditCardNumber":creditCardNumber,
                        "creditCardExpireYear": creditCardExpireYear,
                        "creditCardExpireMonth":creditCardExpireMonth,
                        "apTransactionStatus": apTransactionStatus,
                        "exchangeTableIndex":exchangeTableIndex,
                        "exchangeTableRate":this.model.exchangeTableRate,
                        "transactionVoid":0,
                        "shippingMethodId": this.activeShippingMethod[0].id,
                        "vatScheduleID":this.activeVat[0].id,
                        "paymentTypeId":this.model.paymentTypeId,
                        "isPayment":isPayment,
                        "paymentAmount":this.model.paymentAmount,
                        "paymentTransactionTypeId": this.paymentTransactionTypeId
                    };
                    
                    if(this.activeCreditCard[0].id == '0')
                    {
                        this.model.creditCardID = '';
                    } 
                    
                    this.transactionEntryService.SaveUpdateTransactionEntry(requestedData,this.isModify).then(data => {
                        
                        if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') 
                        {
                            
                            this.transactionEntryService.PostAPTransactionEntry(requestedData).then(data => {
                                window.scrollTo(0,0);
                                
                                this.btndisabled=false;
                                if (data.btiMessage.messageShort == 'RECORD_POST_SUCCESSFULLY') {
                                    this.isSuccessMsg = true;
                                    this.isfailureMsg = false;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = data.btiMessage.message;
                                    this.isModify=false;
                                    this.clear(f,1);
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.hasMsg = false;
                                    }, 4000);
                                }
                                else{
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = data.btiMessage.message;
                                    window.setTimeout(() => {
                                        this.showMsg = false;
                                        this.hasMsg = false;
                                    }, 4000);
                                }
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                                }, 100)
                            });
                            
                        }
                    })
                    
                    this.btndisabled=false;
                    
                }
                else{
                    
                    if(this.activeExchangeDetail[0].id == '0')
                    {
                        myModal.open();
                    }
                }
            }
            
            this.btndisabled=false;
        }
        
        // Delete AP Transaction Entry
        DeleteAPTransactionEntry(f:NgForm)
        {
            
            this.transactionEntryService.DeleteTransactionEntry(this.model.apTransactionNumber).then(data =>
                {
                    
                    window.scrollTo(0,0);
                    this.closeModal();
                    if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.isModify=false;
                        this.clear(f,1);
                        //this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            
            // Delete AP Transaction Entry Distribution
            DeleteAPTransactionEntryDistribution(myModal)
            {
                
                this.transactionEntryService.DeleteTransactionEntryDistribution(this.model.apTransactionNumber).then(data =>
                    {
                        
                        window.scrollTo(0,0);
                        if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.isModify=false;
                            this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                    myModal.close();
                }
                
                // Get Vendor List
                GetVendorList()
                {
                    this.commonService.Getvendormaintenance().then(data => {
                        this.arrVendor=[{'id':'0','text':this.select}];
                        
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrVendor.push({'id':data.result.records[i].venderId,'text':data.result.records[i].venderId});
                            }
                        }
                    });
                }
                
                calculateAmount()
                {
                    
                    var tradeDiscountPercent =  this.tradeDiscountPercent ;
                    var totalCost = this.model.apTransactionCost;
                    var totalSale  = this.model.purchasesAmount;
                    if(totalSale)
                    {
                        var tradeDiscount = (totalSale * tradeDiscountPercent / 100).toFixed(2);
                        
                        this.remainingAmount = totalSale- parseFloat(tradeDiscount);
                        
                        var miscVatPercent = this.miscVatPercent;
                        var miscellaneousAmount = ( this.remainingAmount*miscVatPercent/100).toFixed(2);
                        
                        var freightVatPercent = this.freightVatPercent
                        var FreightAmount = ( this.remainingAmount * freightVatPercent/100).toFixed(2);
                        
                        var vatPercent=this.vatPercent;
                        var vatAmount= ( this.remainingAmount * vatPercent/100).toFixed(2);
                        
                        this.model.apTransactionTradeDiscount = parseFloat(tradeDiscount).toFixed(2);
                        this.model.apTransactionMiscellaneous = parseFloat(miscellaneousAmount).toFixed(2);
                        this.model.apTransactionFreightAmount = parseFloat(FreightAmount).toFixed(2);
                        this.model.apTransactionVATAmount =    parseFloat(vatAmount).toFixed(2);
                        var apTransactionTotalAmount=  this.remainingAmount + parseFloat(tradeDiscount)+parseFloat(miscellaneousAmount) + parseFloat(FreightAmount)+parseFloat(vatAmount);
                        this.model.apTransactionTotalAmount=apTransactionTotalAmount.toFixed(2);
                        //this.model.paymentAmount=apTransactionTotalAmount.toFixed(2);
                        //    this.model.apTransactionTotalAmount = remainingAmount + tradeDiscount + miscellaneousAmount + FreightAmount + vatAmount;
                        
                    }
                    
                }
                calculateTotalAmount145()
                {
                    var totalSale  = parseFloat(this.model.purchasesAmount);
                    var apTransactionTradeDiscount = parseFloat(this.model.apTransactionTradeDiscount);
                    var apTransactionMiscellaneous = parseFloat(this.model.apTransactionMiscellaneous);
                    var apTransactionFreightAmount = parseFloat(this.model.apTransactionFreightAmount);
                    var apTransactionVATAmount = parseFloat(this.model.apTransactionVATAmount);
                    var apTransactionTotalAmount=this.remainingAmount + apTransactionTradeDiscount + apTransactionMiscellaneous+apTransactionFreightAmount + apTransactionVATAmount;
                    this.model.apTransactionTotalAmount=apTransactionTotalAmount;
                    //this.model.paymentAmount=apTransactionTotalAmount;
                }
                
                
                calculateTotalAmount(action:string)
                {
                    
                    if(action == 'PurchasesAmount')
                    {
                        this.formatSaleAmount();
                    }
                    else if(action == 'TradeDiscount')
                    {
                        this.formatTradeAmount();
                    }
                    else if(action == 'Miscellaneous')
                    {
                        this.formatMiscAmount();
                    }
                    if(action == 'FreightAmount')
                    {
                        this.formatFreightAmount();
                    }
                    else if(action == 'VATAmount')
                    {
                        this.formatVatAmount();
                    }
                    else if(action == 'TotalAmount')
                    {
                        this.formatTotalAmount();
                    }
                    
                    if(!this.unFrmtPurchasesAmount)
                    {
                        this.unFrmtPurchasesAmount = '0';
                    } 
                    if(!this.unFrmtTradeDiscount)
                    {
                        this.unFrmtTradeDiscount = '0';
                    }
                    if(!this.unFrmtMiscellaneous)
                    {
                        this.unFrmtMiscellaneous = '0';
                    }
                    if(!this.unFrmtFreightAmount)
                    {
                        this.unFrmtFreightAmount = '0';
                    } 
                    if(!this.unFrmtVATAmount)
                    {
                        this.unFrmtVATAmount = '0';
                    } 
                    if(!this.unFrmtTotalAmount)
                    {
                        this.unFrmtTotalAmount = '0';
                    }
                    if(!this.unFrmtPaymentAmount)
                    {
                        this.unFrmtPaymentAmount = '0';
                    }
                    
                    var totalSale  = parseFloat(this.unFrmtPurchasesAmount);
                    if(totalSale)
                    {
                        var apTransactionTradeDiscount=0;
                        if(this.model.apTransactionTradeDiscount)
                        {
                            apTransactionTradeDiscount = parseFloat(this.unFrmtTradeDiscount);
                        }
                        this.tempRemainingAmount=this.remainingAmount;
                        this.remainingAmount=totalSale-apTransactionTradeDiscount;
                        var vatPercent=this.vatPercent;
                       // var vatAmount=  this.unFrmtVATAmount;
                       this.unFrmtVATAmount = "0"
                       let vatAmount="0";
                       
                        
                        if(action == '')
                        {
                            if(this.vatPercent > 0)
                            {
                                vatAmount= ( this.remainingAmount * vatPercent/100).toFixed(2);
                            }
                        }
                        else{
                            
                            // if(vatAmount == '0' && this.vatPercent > 0)
                            // if(this.unFrmtVATAmount && parseFloat(this.unFrmtVATAmount) == 0)
                            // {
                            //     vatAmount= ( this.remainingAmount * vatPercent/100).toFixed(2);
                            // }
                            if((this.unFrmtVATAmount && parseFloat(this.unFrmtVATAmount) == 0)||(this.tempRemainingAmount != this.remainingAmount))
                            {
                                vatAmount= ( this.remainingAmount * vatPercent/100).toFixed(2);
                            }
                        }
                        
                        var apTransactionFreightAmount = 0;
                        var FreightAmount = 0;
                        var FreightAmountTotal = 0;
                        
                        var apTransactionMiscellaneous = 0;
                        var miscellaneousAmount = 0;
                        var miscellaneousAmountTotal = 0;
                        
                        if(this.unFrmtMiscellaneous && this.unFrmtMiscellaneous !='0')
                        {
                            apTransactionMiscellaneous = parseFloat(this.unFrmtMiscellaneous);
                            miscellaneousAmount = apTransactionMiscellaneous * this.miscVatPercent/100;
                            miscellaneousAmountTotal = apTransactionMiscellaneous + miscellaneousAmount;
                        }
                        
                        if(this.unFrmtFreightAmount && this.unFrmtFreightAmount !='0')
                        {
                            apTransactionFreightAmount = parseFloat(this.unFrmtFreightAmount);
                            FreightAmount = apTransactionFreightAmount * this.freightVatPercent/100;
                            FreightAmountTotal = apTransactionFreightAmount + FreightAmount;
                        }
                       // this.unFrmtVATAmount = parseFloat(vatAmount) + miscellaneousAmountTotal+FreightAmountTotal;
                        this.unFrmtVATAmount = parseFloat(vatAmount) + miscellaneousAmount+FreightAmount;
                        this.unFrmtVATAmount = parseFloat( this.unFrmtVATAmount).toFixed(2);
                        this.formatVatAmount();
                        var apTransactionTotalAmount=this.remainingAmount + parseFloat(vatAmount);
                        apTransactionTotalAmount = Math.round(apTransactionTotalAmount * 100) / 100;
                        this.unFrmtTotalAmount = apTransactionTotalAmount;
                        this.formatTotalAmount();
                        //this.model.paymentAmount=apTransactionTotalAmount;
                    }
                    else
                    {
                        this.unFrmtPurchasesAmount = 0;
                        this.unFrmtVATAmount = 0;
                        this.unFrmtMiscellaneous=0;
                        this.unFrmtFreightAmount=0;
                        this.unFrmtTotalAmount = 0;
                        this.unFrmtPaymentAmount=0;
                        //  this.model.purchasesAmount = 0;
                        // this.model.apTransactionVATAmount = 0;
                        // this.model.apTransactionMiscellaneous = 0;
                        // this.model.apTransactionFreightAmount = 0;
                        // this.model.apTransactionTotalAmount = 0;
                        // this.model.paymentAmount = 0;
                    }
                }
                // Get Salesman Maintenance
                GetSalesmanMaintenanceList()
                {
                    this.commonService.GetSalesmanMaintenanceList().then(data => {
                        this.arrSalesman =[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrSalesman.push({'id':data.result.records[i].salesmanId,'text':data.result.records[i].salesmanId});
                            }
                        }
                    });
                }
                
                //Get Salesman Maintenance By Id
                GetSalesmanMaintenanceById(salesmanId: string) {
                    this.commonService.GetSalesmanMaintenanceById(salesmanId).then(data => {
                        this.salesmanName=data.result.salesmanFirstName;
                    });
                }
                
                // Get Sales Territory Setup List
                GetSalesTerritorySetupList()
                {
                    this.commonService.GetSalesTerritorySetupList().then(data => {
                        
                        this.arrSalesTerritory =[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrSalesTerritory.push({'id':data.result.records[i].salesTerritoryId,'text':data.result.records[i].salesTerritoryId});
                            }
                        }
                    });
                }
                
                // Get Payment Term List
                GetPaymentTermList()
                {
                    this.commonService.GetPaymentTermList().then(data => {
                        this.arrPaymentTermSetup=[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrPaymentTermSetup.push({'id':data.result.records[i].paymentTermId,'text':data.result.records[i].description});
                            }
                        }
                    });
                }
                
                
                // Get Credit Card Setup List
                GetCreditCardSetup()
                {
                    this.commonService.GetCreditCardSetup().then(data => {
                        this.arrCreditCard=[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrCreditCard.push({'id':data.result.records[i].cardId,'text':data.result.records[i].cardId});
                            }
                        }
                    });
                }
                
                BindAccountNumber()
                {
                    this.commonService.getGlAccountNumberList().then(data => {
                        this.arrAccountNumber=[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.length;i++)	
                            {
                                this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                            }
                        }
                    });
                }
                
                //Get Distribution Account Type List
                GetDistributionAccountTypesList()
                {
                    this.transactionEntryService.GetDistributionAccountTypesList().then(data => {
                        this.arrDistributionAccountTypes=data.result;
                        
                        this.arrDistributionAccountTypes.splice(0, 0, { "typeId": "", "typeCode": this.select });
                        this.distributionAccountType='';
                    });
                }
                GetExchangeTableSetupByCurrencyId(currencyId: string){
                    this.commonService.getExchangeTableSetupByCurrencyId(currencyId).then(data => {
                        
                        this.arrExchangeDetail=[{'id':'0','text':this.select}];
                        // this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail')
                        
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrExchangeDetail.push({'id':data.result.records[i].exchangeTableIndex,'text':data.result.records[i].exchangeId});
                            }
                        }
                    });
                }
                
                GetCurrencyExchangeDetail(exchangeId: string){
                    this.commonService.GetCurrencyExchangeDetail(exchangeId).then(data => {
                        
                        if(data.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND")
                        {
                            
                            if(!this.model.exchangeTableRate)
                            {
                                this.model.exchangeTableRate=data.result.exchangeRate;
                            }      
                            this.model.exchangeExpirationDate=data.result.exchangeExpirationDate;
                        }
                        else{
                            this.model.exchangeTableRate='';
                            this.model.exchangeExpirationDate='';
                        }
                    });
                }
                
                // Get Shipping Method List
                GetShippingMethodList()
                {
                    this.commonService.GetShippingMethodList().then(data => {
                        this.arrShippingMethod=[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrShippingMethod.push({'id':data.result.records[i].shipmentMethodId,'text':data.result.records[i].shipmentDescription});
                            }
                        }
                    });
                }
                
                // Get Checkbook Maintenance
                GetCheckbookMaintenance()
                {
                    this.commonService.GetCheckbookMaintenance().then(data => {
                        this.arrCheckbook=[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrCheckbook.push({'id':data.result.records[i].checkBookId,'text':data.result.records[i].checkBookId});
                            }
                        }
                    });
                }
                
                // Get Currency Setup List
                GetCurrencySetup()
                {
                    this.commonService.getCurrencySetup().then(data => {
                        this.arrCurrency=[{'id':'0','text':this.select}];
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            for(var i=0;i<data.result.records.length;i++)	
                            {
                                this.arrCurrency.push({'id':data.result.records[i].currencyId,'text':data.result.records[i].currencyId});
                            }
                        }
                    });
                }
                
                //Get Payment Method
                GetPaymentMethod()
                {
                    this.commonService.GetPaymentMethod().then(data => {
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            this.arrPaymentMethod = data.result;
                            this.model.paymentTypeId='1';
                        }
                    });
                }
                
                //Get Sales Territory Setup By Id
                GetSalesTerritorySetupById(salesmanId: string) {
                    this.commonService.GetSalesTerritorySetupById(salesmanId).then(data => {
                        this.salesmanName=data.result.salesmanFirstName;
                    });
                }
                
                getGlAccountNumberDetail(myModal)
                {
                    
                    var accountTableRowIndex='';
                    accountTableRowIndex=this.activeAccountNumber[0].id
                    this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            this.accountDescription=data.result.accountDescription;
                        }
                        else{
                            this.accountDescription=data.btiMessage.message;
                        }
                        myModal.open();
                    });
                }
                
                GetGlAccountNumberDetailById(accountTableRowIndex:string)
                {
                    this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
                        if(data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST')
                        {
                            this.accountDescription=data.result.accountDescription;
                        }
                        else if(data.btiMessage.messageShort == 'MAIN_ACCOUNT_TRANSACTION_NOT_ALLOWED')
                        {
                            this.activeAccountNumber = [{'id':'0','text':this.select}];
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.PopUphasMsg = true;
                            this.PopUpmessageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.PopUphasMsg = false;
                            }, 2000);
                        }
                        else{
                            this.accountDescription=data.btiMessage.message;
                        }
                    });
                }
                
                // Get Vendor By VendorId
                GetVendorByVendorId(venderId)
                {
                    this.unFrmtPurchasesAmount = 0;
                    this.unFrmtVATAmount = 0;
                    this.unFrmtMiscellaneous=0;
                    this.unFrmtFreightAmount=0;
                    this.unFrmtTotalAmount = 0;
                    this.unFrmtPaymentAmount=0;
                    
                    this.transactionEntryService.GetVendorAccountDetailByVendorId(venderId).then(data => {
                        
                        this.vendorName=data.result.vendorName;
                        var selectedShippingMethod = this.arrShippingMethod.find(x => x.id ==   data.result.shipmentMethodId);
                        this.bindSelectedItem(selectedShippingMethod,'ShippingMethod');
                        
                        var selectedPaymentTerm = this.arrPaymentTermSetup.find(x => x.id ==   data.result.paymentTermId);
                        if(selectedPaymentTerm)
                        {
                            this.bindSelectedItem(selectedPaymentTerm,'PaymentTerm');
                        }
                        
                        
                        var selectedCheckBook = this.arrCheckbook.find(x => x.id ==  data.result.checkBookId );
                        this.bindSelectedItem(selectedCheckBook,'Checkbook');
                        
                        var selectedCurrency = this.arrCurrency.find(x => x.id ==  data.result.currencyId );
                        if(selectedCurrency)
                        {
                            this.bindSelectedItem(selectedCurrency,'Currency');
                        }
                        
                        
                        var selectedVat = this.arrVat.find(x => x.id ==  data.result.vatScheduleId );
                        if(selectedVat)
                        {
                            this.bindSelectedItem(selectedVat,'Vat');
                        }
                        
                        this.tradeDiscountPercent=data.result.tradeDiscountPercent;
                        this.miscVatPercent=data.result.miscVatPercent;
                        this.freightVatPercent=data.result.freightVatPercent;
                        this.vatPercent=data.result.vatPercent;
                        this.model.apTransactionTradeDiscount= data.result.tradeDiscountPercent;
                        this.unFrmtTradeDiscount = this.model.apTransactionTradeDiscount;
                        if(selectedCurrency)
                        {
                            this.formatSaleAmount();
                            this.formatTradeAmount();
                            this.formatMiscAmount();
                            this.formatVatAmount();
                            this.formatTotalAmount();
                            this.formatPaymentAmount();
                        }
                        //  this.model.apTransactionMiscellaneous= data.result.tradeDiscountPercent;
                        //  this.model.apTransactionFreightAmount= data.result.tradeDiscountPercent;
                        //  this.model.apTransactionVATAmount= data.result.tradeDiscountPercent;
                        
                        this.calculateTotalAmount('');
                        
                        //  this.model.apTransactionTradeDiscount= data.result.tradeDiscountPercent;
                        //  this.model.apTransactionMiscellaneous= data.result.tradeDiscountPercent;
                        //  this.model.apTransactionFreightAmount= data.result.tradeDiscountPercent;
                        //  this.model.apTransactionVATAmount= data.result.tradeDiscountPercent;
                        
                        // this.calculateAmount();
                        
                    });
                }
                
                validateAmount(amountType:string,thisevt)
                {
                    
                    if(amountType=='debitAmount' && thisevt.debitAmount==''){
                        this.debitAmount=0;
                        //    this.isDebitAllowed=false;
                        //    this.isCreditAllowed=true;
                    }
                    else if(amountType=='creditAmount' && thisevt.creditAmount==''){
                        this.creditAmount=0;
                        // this.isCreditAllowed=false;
                        // this.isDebitAllowed=true;
                    }
                    if(amountType=='debitAmount')
                    {
                        if(this.debitAmount > 0)
                        {
                            this.creditAmount=0;
                        }
                    }
                    else if(amountType=='creditAmount')
                    {
                        if(this.creditAmount > 0)
                        {
                            this.debitAmount=0;
                        }
                    }
                    
                }
                
                ChangeDistributionAccountType(evt)
                {
                    
                    var selectedIndex = evt.target.selectedIndex
                    this.distributionAccountTypeCode=evt.target[selectedIndex].text
                    
                }
                
                AddDistributionDetails()
                {
                    this.IsAddDistributionDetails=true;
                    if((this.creditAmount || this.debitAmount) && this.activeAccountNumber[0].id !='0' && this.distributionAccountType && this.distributionDescription)
                    {
                        
                        if(this.creditAmount)
                        {
                            this.isCredit=true;
                        }
                        else{
                            this.isCredit=false;
                        }
                        
                        this.arrAPDistributionEntry.listDtoAPDistributionDetail.push(
                            {
                                "accountNumber":this.activeAccountNumber[0].text,
                                "accountDescription":this.accountDescription,
                                "type":this.distributionAccountTypeCode,
                                "typeId":this.distributionAccountType,
                                "distributionReference":this.distributionDescription,
                                "debitAmount":this.debitAmount,
                                "creditAmount":this.creditAmount,
                                "accountTableRowIndex":this.activeAccountNumber[0].id
                            }
                        );
                        this.IsAddDistributionDetails=false;
                        this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
                        if(!this.debitAmount)
                        {
                            this.debitAmount = 0;
                        }
                        if(!this.creditAmount)
                        {
                            this.creditAmount = 0;
                        }
                        
                        // this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString())
                        this.debitAmount=0;
                        this.creditAmount=0;
                        this.distributionDescription='';
                        this.distributionAccountType='';
                        
                    }
                    else{
                        // alert(Constants.msgText);
                    }
                    
                }
                
                RemoveDistributionDetails(RowIndex)
                {
                    this.arrAPDistributionEntry.listDtoAPDistributionDetail.splice(RowIndex, 1);
                    if(this.arrAPDistributionEntry.listDtoAPDistributionDetail.length == 0)
                    {
                        this.IsAddDistributionDetails=true;
                    }
                }
                
                GetVatDetailSetupById(vatScheduleId)
                {
                    this.commonService.GetVatDetailSetupById(vatScheduleId).then(data => {
                        
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            this.vatPercent = data.result.basperct;            
                            this.calculateTotalAmount('');
                        }
                    });
                }
                
                public bindSelectedItem(value:any,type:string):void {
                    
                    if(type == 'Vendor')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeVendor=[value];
                            this.model.venderId=this.activeVendor[0].id;
                            
                            if(this.model.venderId != "0")
                            {
                                this.GetVendorByVendorId(this.model.venderId);
                            }
                            else{
                                this.venderNamePrimary='';
                                this.tradeDiscountPercent=0;
                                this.miscVatPercent=0;
                                this.freightVatPercent=0;
                                this.vatPercent=0;
                                this.model.apTransactionTradeDiscount = 0;
                                this.model.apTransactionMiscellaneous = 0;
                                this.model.apTransactionFreightAmount = 0;
                                this.model.apTransactionTotalAmount = 0;
                            }
                        }
                    }
                    else if(type == 'Salesman')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeSalesman=[value];
                            this.model.salesmanID=this.activeSalesman[0].id;
                            
                            if(this.model.salesmanID != "0")
                            {
                                this.GetSalesmanMaintenanceById(this.model.salesmanID);
                            }
                            else{
                                this.salesmanName='';
                            }
                        }
                    }
                    else if(type == 'SalesTerritory')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeSalesTerritory=[value];
                            this.model.territoryId =this.activeSalesTerritory[0].id;
                        }
                    }
                    else if(type == 'Batch')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeBatch=[value];
                        }
                        this.model.batchID=this.activeBatch[0].id;
                    }
                    else if(type == 'Currency')
                    {
                        this.value = value;
                        
                        if(value.text)
                        {
                            this.activeCurrency=[value];
                            this.model.currencyID=this.activeCurrency[0].id;
                            this.activeExchangeDetail= [this.emptyArray[0]];
                            this.arrExchangeDetail=[];
                            this.model.exchangeTableRate = '';
                            this.model.exchangeExpirationDate = '';
                            if(value.id != "0")
                            {
                                this.formatSaleAmount();
                                this.formatTradeAmount();
                                this.formatMiscAmount();
                                this.formatVatAmount();
                                this.formatFreightAmount();
                                this.formatTotalAmount();
                                this.formatPaymentAmount();
                                this.GetExchangeTableSetupByCurrencyId(value.id);
                            }
                            else{
                                this.model.purchasesAmount=this.unFrmtPurchasesAmount;
                                this.model.apTransactionMiscellaneous=this.unFrmtMiscellaneous;
                                this.model.apTransactionTradeDiscount=this.unFrmtTradeDiscount;
                            }
                        }
                    }
                    else if(type == 'Vat')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeVat=[value];
                            this.model.vatScheduleID=this.activeVat[0].id;
                            this.GetVatDetailSetupById(this.model.vatScheduleID)
                        }
                    }
                    else if(type == 'PaymentTerm')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activePaymentTerm=[value];
                            this.model.paymentTermsID=this.activePaymentTerm[0].id;
                        }
                    }
                    else if(type == 'ShippingMethod')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeShippingMethod=[value];
                            this.model.shippingMethodId=this.activeShippingMethod[0].id;
                        }
                    }
                    else if(type == 'Checkbook')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeCheckbook=[value];
                            this.model.checkbookID=this.activeCheckbook[0].id;
                        }
                    }
                    else if(type == 'CreditCard')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeCreditCard=[value];
                            this.model.creditCardID=this.activeCreditCard[0].id;
                        }
                    }
                    else if(type == 'ExchangeDetail')
                    {
                        
                        this.value = value;
                        this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                        if(value.id)
                        {
                            this.activeExchangeDetail=[value];
                            if(this.activeExchangeDetail[0].id != "0")
                            {
                                
                                this.GetCurrencyExchangeDetail(value.text);
                            }
                            else{
                                this.model.exchangeRate == '';
                                this.model.exchangeExpirationDate == '';
                            }
                        }
                    }
                    else if(type == 'TransactionNumberList')
                    {
                        this.value = value;
                        if(value.text)
                        {
                            this.activeTransactionNumberList=[value];
                            this.model.apTransactionNumber=this.activeTransactionNumberList[0].id;
                            
                            if( this.model.apTransactionNumber != '0')
                            {
                                this.GetTransactionEntryByTransactionNumber(this.model.apTransactionNumber)
                            }
                            else{
                                this.clear('',0);
                            }
                            
                        }
                        
                    }
                    else if(type == 'AccountNumber')
                    {
                        this.accountTableRow=value;
                        this.value = value;
                        if(value.text)
                        {
                            this.activeAccountNumber=[value];
                            this.GetGlAccountNumberDetailById(value.id)
                        }
                    }
                    
                }
                
                varifyDelete()
                {
                    this.isDeleteAction=true;
                    this.confirmationModalBody = this.deleteConfirmationText;
                    this.isConfirmationModalOpen = true;
                }
                
                
                closeModal()
                {
                    this.isDeleteAction=false;
                    this.isConfirmationModalOpen = false;
                }
                
                clear(f,type:number){
                    
                    if(type == 1)
                    {
                        f.resetForm();
                        this.model.arTransactionType='';
                        this.model.paymentTypeId='1';
                    }
                    this.isModify=false;
                    //f.resetForm();
                    this.model.apTransactionType='';
                    this.model.apTransactionNumber='';
                    this.bindSelectedItem(this.emptyArray[0],'CreditCard');
                    this.bindSelectedItem(this.emptyArray[0],'Vat');
                    this.bindSelectedItem(this.emptyArray[0],'Vendor');
                    this.bindSelectedItem(this.emptyArray[0],'Currency');
                    this.bindSelectedItem(this.emptyArray[0],'ShippingMethod');
                    this.bindSelectedItem(this.emptyArray[0],'Batch');
                    this.bindSelectedItem(this.emptyArray[0],'PaymentTerm');
                    this.bindSelectedItem(this.emptyArray[0],'Salesman');
                    this.bindSelectedItem(this.emptyArray[0],'SalesTerritory');
                    this.bindSelectedItem(this.emptyArray[0],'Checkbook');
                    this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail');
                    this.arrTransactionNumberList=[{'id':'0','text':this.select}];
                    this.activeTransactionNumberList=[{'id':'0','text':this.select}]
                    var drpPaymentTypeId= <HTMLSelectElement>document.getElementById("paymentTypeId");
                    drpPaymentTypeId.selectedIndex = 0;
                    this.model.paymentTypeId='1';
                    this.model.exchangeTableRate='';
                    this.model.exchangeExpirationDate='';
                    this.vendorName='';
                    this.model.apTransactionDescription='';
                    //this.model.apTransactionType='';
                    this.model.apTransactionDate='';
                    
                    this.model.purchasesAmount=0;
                    this.model.apTransactionTradeDiscount=0;
                    this.model.apTransactionMiscellaneous=0;
                    this.model.apTransactionFreightAmount=0;
                    this.model.apTransactionVATAmount=0;
                    this.model.apTransactionTotalAmount=0;
                    this.model.paymentAmount=0;
                    this.arrAPDistributionEntry={
                        "vendorId":"",
                        "vendorName":"",
                        "currencyID": "",
                        "transactionType": "",
                        "transactionNumber": "",
                        "functionalAmount": 0,
                        "originalAmount": 0,
                        "listDtoAPDistributionDetail":[]
                    }
                    
                    this.activeTransactionNumberList=[{'id':'0','text':this.select}]
                }
                
                SaveDistributionDetail(myModal)
                {
                    if(this.isModify)
                    {
                        this.transactionEntryService.SaveDistributionDetail(this.arrAPDistributionEntry).then(data => {
                            window.scrollTo(0,0);
                            var datacode = data.code;
                            if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.arrAPDistributionEntry={
                                    "vendorId":"",
                                    "vendorName":"",
                                    "currencyID": "",
                                    "transactionType": "",
                                    "transactionNumber": "",
                                    "functionalAmount": 0,
                                    "originalAmount": 0,
                                    "listDtoAPDistributionDetail":[]
                                }
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                    }
                    
                    myModal.close();
                }
                
                
                
                print()
                {
                    window.print();
                }
                
                openModel(myModal,actionType)
                {
                    if(actionType == 'distributionEntry')
                    {
                        if(this.model.apTransactionNumber)
                        {
                            this.IsAddDistributionDetails=false;
                            this.GetDistributionDetailByTransactionNumber(this.model.apTransactionNumber)
                        }
                        
                    }
                    myModal.open();
                }
                
                onlyDecimalNumberKey(event) {
                    return this.getScreenDetailService.onlyDecimalNumberKey(event);
                }
                
                //Change To Original Value of receiptAmount
                formatAmountChanged(Type:string){
                    
                    if(Type == 'PurchasesAmount')
                    { 
                        this.commonService.changeInputNumber("purchasesAmount")
                        this.model.purchasesAmount = this.unFrmtPurchasesAmount
                    }
                    else if(Type == 'FreightAmount')
                    {
                        this.commonService.changeInputNumber("apTransactionFreightAmount")
                        this.model.apTransactionFreightAmount = this.unFrmtFreightAmount
                    }
                    else if(Type == 'TradeDiscount')
                    {
                        this.commonService.changeInputNumber("apTransactionTradeDiscount")
                        this.model.apTransactionTradeDiscount = this.unFrmtTradeDiscount
                    }
                    else if(Type == 'Miscellaneous')
                    {
                        this.commonService.changeInputNumber("apTransactionMiscellaneous")
                        this.model.apTransactionMiscellaneous = this.unFrmtMiscellaneous
                    }
                    else if(Type == 'VATAmount')
                    {
                        this.commonService.changeInputNumber("apTransactionVATAmount")
                        this.model.apTransactionVATAmount = this.unFrmtVATAmount
                    }
                    //  else if(Type == 'TotalAmount')
                    //  {
                    //     this.model.apTransactionTotalAmount = this.unFrmtTotalAmount
                    //  }
                    else if(Type == 'PaymentAmount')
                    {
                        this.commonService.changeInputNumber("paymentAmount")
                        this.model.paymentAmount = this.unFrmtPaymentAmount
                    }
                    // this.commonService.changeInputNumber("arTransactionCost") ;
                    
                }
                // get Original receiptAmount
                getAmountValue(Type:string,event){
                    if(event && event.indexOf('.') > -1)
                    {
                    var amt = event.split(".")
                    if(amt.length == 1 && amt[0].length >= 6){
                        event = amt[0].slice(0, 6)
                    }
                    else{
                        if(amt.length > 1 && amt[1].length >= 3){
                            event = amt[0]+"."+amt[1].slice(0, 3)
                        }
                        if(amt.length > 1 && amt[0].length >= 6){
                            event = amt[0].slice(0, 6)+"."+amt[1]
                        }
                    }
                    }
                  
                    if(Type == 'PurchasesAmount')
                    {
                        this.unFrmtPurchasesAmount = event
                    }
                    else if(Type == 'TradeDiscount')
                    {
                        this.unFrmtTradeDiscount = event
                    }
                    else if(Type == 'Miscellaneous')
                    {
                        this.unFrmtMiscellaneous = event
                    }
                    else if(Type == 'FreightAmount')
                    {
                        this.unFrmtFreightAmount = event
                    }
                    else if(Type == 'VATAmount')
                    {
                        this.unFrmtVATAmount = event
                    }
                    else if(Type == 'TotalAmount')
                    {
                        this.unFrmtTotalAmount = event;
                    }
                    else if(Type == 'PaymentAmount')
                    {
                        this.unFrmtPaymentAmount = event;
                        if(this.model.apTransactionTotalAmount < event){
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = "Total Amount should be greater than Payment Amount";
                            window.scrollTo(0,0);
                            this.model.paymentAmount='';
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            window.scrollTo(0,0);
                        }
                    }
                }
                
                
                formatSaleAmount(){
                    if(this.unFrmtPurchasesAmount || this.unFrmtPurchasesAmount == 0)
                    {
                        this.commonService.changeInputText("purchasesAmount")
                        var inputValue = this.unFrmtPurchasesAmount;
                        this.model.purchasesAmount = this.unFrmtPurchasesAmount;
                        if (this.activeCurrency[0].id !='0' ){
                            let submitInfo = {
                                'currencyId':this.activeCurrency[0].text
                            };
                            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                                this.model.purchasesAmount = this.commonService.formatAmount(data, inputValue)
                                
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                                }, 100)
                            });
                        }else{
                            this.formatAmountChanged('PurchasesAmount')
                        }
                    }
                }
                
                formatTradeAmount(){
                    if(this.unFrmtTradeDiscount || this.unFrmtTradeDiscount == 0)
                    {
                        this.commonService.changeInputText("apTransactionTradeDiscount")
                        var inputValue = this.unFrmtTradeDiscount;
                        
                        this.model.apTransactionTradeDiscount = this.unFrmtTradeDiscount;
                        if (this.activeCurrency[0].id !='0' ){
                            let submitInfo = {
                                'currencyId':this.activeCurrency[0].text
                            };
                            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                                this.model.apTransactionTradeDiscount = this.commonService.formatAmount(data, inputValue)
                                
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                                }, 100)
                            });
                        }else{
                            this.formatAmountChanged('TradeDiscount')
                        }
                    }
                }
                
                formatMiscAmount(){
                    if(this.unFrmtMiscellaneous || this.unFrmtMiscellaneous == 0)
                    {
                        this.commonService.changeInputText("apTransactionMiscellaneous")
                        var inputValue = this.unFrmtMiscellaneous;
                        
                        this.model.apTransactionMiscellaneous = this.unFrmtMiscellaneous;
                        if (this.activeCurrency[0].id !='0' ){
                            let submitInfo = {
                                'currencyId':this.activeCurrency[0].text
                            };
                            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                                this.model.apTransactionMiscellaneous = this.commonService.formatAmount(data, inputValue)
                                
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                                }, 100)
                            });
                        }else{
                            this.formatAmountChanged('Miscellaneous')
                        }
                    }
                }
                
                formatFreightAmount(){
                    
                    if(this.unFrmtFreightAmount || this.unFrmtFreightAmount == 0)
                    {
                        this.commonService.changeInputText("apTransactionFreightAmount")
                        var inputValue = this.unFrmtFreightAmount;
                        
                        this.model.apTransactionFreightAmount = this.unFrmtFreightAmount;
                        if (this.activeCurrency[0].id !='0' ){
                            let submitInfo = {
                                'currencyId':this.activeCurrency[0].text
                            };
                            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                                this.model.apTransactionFreightAmount = this.commonService.formatAmount(data, inputValue)
                                
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                                }, 100)
                            });
                        }else{
                            this.formatAmountChanged('FreightAmount')
                        }
                    }
                }
                
                formatVatAmount(){
                    
                    if(this.unFrmtVATAmount || this.unFrmtVATAmount == 0 )
                    {
                        this.commonService.changeInputText("apTransactionVATAmount")
                        var inputValue = this.unFrmtVATAmount;
                        
                        this.model.apTransactionVATAmount = this.unFrmtVATAmount;
                        if (this.activeCurrency[0].id !='0' ){
                            let submitInfo = {
                                'currencyId':this.activeCurrency[0].text
                            };
                            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                                this.model.apTransactionVATAmount = this.commonService.formatAmount(data, inputValue)
                                
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                                }, 100)
                            });
                        }else{
                            this.formatAmountChanged('VATAmount')
                        }
                    }
                }
                
                formatTotalAmount(){
                    if(this.unFrmtTotalAmount || this.unFrmtTotalAmount == 0)
                    {
                        this.commonService.changeInputText("apTransactionTotalAmount")
                        var inputValue = this.unFrmtTotalAmount;
                        
                        this.model.apTransactionTotalAmount = this.unFrmtTotalAmount;
                        if (this.activeCurrency[0].id !='0' ){
                            let submitInfo = {
                                'currencyId':this.activeCurrency[0].text
                            };
                            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                                this.model.apTransactionTotalAmount = this.commonService.formatAmount(data, inputValue)
                                
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                                }, 100)
                            });
                        }else{
                            this.formatAmountChanged('TotalAmount')
                        }
                    }
                }
                
                formatPaymentAmount(){
                    if(this.unFrmtPaymentAmount || this.unFrmtPaymentAmount == 0)
                    {
                        var unFrmtPaymentAmount = this.unFrmtPaymentAmount;
                        var apTransactionTotalAmount = this.unFrmtTotalAmount;
                        this.commonService.changeInputText("paymentAmount")
                        var inputValue = this.unFrmtPaymentAmount;
                        this.model.paymentAmount = this.unFrmtPaymentAmount;
                        
                        if(parseFloat(unFrmtPaymentAmount)>apTransactionTotalAmount){
                            this.model.paymentAmount = apTransactionTotalAmount;
                            inputValue = apTransactionTotalAmount;
                            this.unFrmtPaymentAmount = apTransactionTotalAmount;
                        }
                        if (this.activeCurrency[0].id !='0' ){
                            let submitInfo = {
                                'currencyId':this.activeCurrency[0].text
                            };
                            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                                this.model.paymentAmount = this.commonService.formatAmount(data, inputValue)
                            }).catch(error => {
                                window.setTimeout(() => {
                                    this.isSuccessMsg = false;
                                    this.isfailureMsg = true;
                                    this.showMsg = true;
                                    this.hasMsg = true;
                                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                                }, 100)
                            });
                        }else{
                            this.formatAmountChanged('PaymentAmount')
                        }
                    }
                }
                
                checkMonthValue(){
                    if(this.model.creditCardExpireMonth<=0||this.model.creditCardExpireMonth>12){
                        this.model.creditCardExpireMonth='';
                    }
                }
                
            }