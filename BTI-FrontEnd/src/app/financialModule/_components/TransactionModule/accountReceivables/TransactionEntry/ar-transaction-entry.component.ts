import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { TransactionEntrySetup } from '../../../../../financialModule/_models/transactionModule/accountReceivables/transactionEntry';
import { TransactionEntryService } from '../../../../../financialModule/_services/transactionModule/accountReceivables/transactionEntry.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
    templateUrl:'./ar-transaction-entry.component.html',
    providers:[TransactionEntryService,CommonService]
})

//export to make it available for other classes
export class ARTransactionEntryComponent {
    page = new Page();
    rows = new Array<TransactionEntrySetup>();
    moduleCode = Constants.financialModuleCode;
    currentTransactionType=Constants.batchTransacitonType.AR_TransferEntry;
    distributionRequestData;
    isScreenLock;
    screenCode;
    screenName;
    moduleName;
    distributionScreenModuleName = '';
    exchangeRateScreenModuleName = '';
    select=Constants.select;
    emptyArray=[{'id':'0','text':this.select}];    
    defaultAddFormValues: Array<object>;
    defaultExchangeRateFormValues: Array<object>;
    defaultDistributionFormValues: Array<object>;
    availableFormValues: [object];
    arrTransactionType=[];
    searchKeyword = '';
    selected = [];
    remainingAmount:number=0;
    tempRemainingAmount:number=0;
    tradeDiscountPercent:number = 0;
    miscVatPercent:number = 0;
    freightVatPercent:number  = 0;
    vatPercent:number = 0;
    accountTableRow:any={'id':'','text':''};
    activeCustomer:any=this.emptyArray;
    activeAccountNumber:any=this.emptyArray;
    activeSalesman:any=this.emptyArray;
    activeSalesTerritory:any=this.emptyArray;
    activeCurrency:any=this.emptyArray;
    activeVat:any=this.emptyArray;
    activeBatch:any=this.emptyArray;
    activeCheckbook:any=this.emptyArray;
    activeShippingMethod:any=this.emptyArray;
    activePaymentTerm:any=this.emptyArray;
    activeCreditCard:any=this.emptyArray;
    activeExchangeDetail=this.emptyArray;
    activeTransactionNumberList=this.emptyArray;
    private value:any = {};
    customerName:string;
    salesmanName:string;
    territoryName:string;
    dynamicTransactionTypeLabel:string;
    arrCustomer=[];
    arrSalesman=[];
    arrSalesTerritory=[];
    arrCurrency=[];
    arrCheckbook=[];
    arrVat=[];
    arrTransactionNumberList=[];
    arrBatchList =  this.emptyArray;
    arrPaymentMethod:any=[];
    arrShippingMethod=[];
    arrPaymentTermSetup=[];
    arrCreditCard=[];
    arrExchangeDetail = this.emptyArray;
    arrAccountNumber=[];
    arrDistributionAccountTypes=[];
    distributionAccountTypeCode:string;
    distributionAccountType:string;
    IsAddDistributionDetails:boolean=false;
    debitAmount:number = 0;
    creditAmount:number = 0;
    distributionDescription:'';
    accountDescription:string;
    isCredit:boolean=false;
    paymentTransactionTypeId:number;
    
    btndisabled:boolean=false;
    arrARDistributionEntry:any={
        "customerId":"",
        "customerName":"",
        "currencyID":"",
        "transactionType":"",
        "functionalAmount":"",
        "originalAmount":"",
        "listDtoARDistributionDetail":[]
    }
    messageText;
    hasMsg = false;
    showMsg = false;
    PopUpmessageText;
    PopUphasMsg = false;
    isSuccessMsg;
    isfailureMsg;
    ddPageSize = 5;
    model: any = {};
    atATimeText=Constants.atATimeText;
    close=Constants.close;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isDeleteAction : boolean = false;
    isConfirmationModalOpen:boolean=false;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
    
    unFrmtCostAmount;
    unFrmtSalesAmount;
    unFrmtTradeDiscount;
    unFrmtMiscellaneous;
    unFrmtFreightAmount;
    unFrmtVATAmount;
    unFrmtTotalAmount;
    unFrmtPaymentAmount;
    
    private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
    onDateChanged(event: IMyDateModel): void {
        var RequestedData =  {
            "transactionDate":event.formatted,
            "moduleName":"AR",
            "originId":Constants.massCloseOriginType.AR_Transaction_Entry
        }
        this.commonService.checkTransactionDateIsValid(RequestedData).then(data => {
            
            if(data.btiMessage.messageShort == "INVALID_TRANSACTION_DATE")
            {
                this.model.arTransactionDate = '';
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        });   
    }
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private transactionEntryService: TransactionEntryService,
        private commonService: CommonService,
    )
    {
        // var userData = JSON.parse(localStorage.getItem('currentUser'));
    }
    
    ngOnInit() 
    {
        this.GetAddScreenDetail();
        this.GetARTransactionType();
        this.GetExchangeRateScreenDetail();
        this.GetDistributionScreenDetail();
        this.GetCustomerList();
        this.GetActiveSalesmanMaintenanceList();
        this.GetBatchList();
        this.GetVatDetailSetup();
        this.GetPaymentTermList();
        this.GetSalesTerritorySetupList();
        this.GetShippingMethodList();
        this.GetPaymentMethod();
        this.GetCurrencySetup();
        this.GetCheckbookMaintenance();
        this.GetCreditCardSetup();
        this.BindAccountNumber();
        this.GetDistributionAccountTypesList();
        this.GetInitialValues();
    }
    
    GetAddScreenDetail()
    {
        this.screenCode = "S-1244";
        this.defaultAddFormValues = [
            {'fieldName': 'AR_TR_ENTRY_TRANS_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_TRANS_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_TRANS_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_SALESMAN_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_TERRITORY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_PAYMENT_TERM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_SHIPPING_METHOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_COST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_SALES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_TRADE_DISCOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_MISCELLANEOUS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_FREIGHT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_VAT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_TOTAL_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_PAYMENT_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_CHECK_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_CREDIT_CARD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_CREDIT_CARD_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_EXPIRE_DATE_MM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_EXPIRE_DATE_YY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_COMMISSIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            {'fieldName': 'AR_TR_ENTRY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
        this.getScreenDetail(this.screenCode,'Add');
    }
    GetExchangeRateScreenDetail()
    {
        this.screenCode = "S-1238";
        this.defaultExchangeRateFormValues = [
            { 'fieldName': 'EX_TABLE_DET_JOURNAL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_EX_TABLE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_EX_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_EXPIRE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'EX_TABLE_DET_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
        this.getScreenDetail(this.screenCode,'ExchangeRate');
    }
    GetDistributionScreenDetail()
    {
        this.screenCode = "S-1245";
        this.defaultDistributionFormValues = [
            { 'fieldName':'AR_TR_ENTRY_DIS_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_CUSTOMER_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_TRANS_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_TRANS_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_FUNCT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_ORIGINAL_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_DIS_REFERENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_DEBIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName':'AR_TR_ENTRY_DIS_DEFAULT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            
        ];
        this.getScreenDetail(this.screenCode,'Distribution');
    } 
    
    getScreenDetail(screenCode,ArrayType)
    {
        
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;    
            if(ArrayType == 'Add'){
                this.moduleName=data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if(ArrayType == 'Distribution'){
                this.distributionScreenModuleName=data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultDistributionFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else if(ArrayType == 'ExchangeRate'){
                this.exchangeRateScreenModuleName=data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultExchangeRateFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                    objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }    
            
        });
    }
    
    GetInitialValues(){
        this.model.arTransactionCost='0';
        this.model.arTransactionSalesAmount='0';
        this.model.arTransactionTradeDiscount='0';
        this.model.arTransactionMiscellaneous='0';
        this.model.arTransactionFreightAmount='0';
        this.model.arTransactionVATAmount='0';
        this.model.arTransactionTotalAmount='0';
        this.model.paymentAmount='0';
    }
    
    ChangeTransactionTypeLabel(evt)
    {
        this.dynamicTransactionTypeLabel=evt.target[evt.target.selectedIndex].text;
    }
    // get Batch List
    GetBatchList(){
        this.commonService.getARBatchesListByTransactionType(this.currentTransactionType).then(data => {
            this.arrBatchList=[{'id':'0','text':this.select}];
            
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.length;i++)	
                {
                    this.arrBatchList.push({'id':data.result[i].batchId,'text':data.result[i].batchId});
                }
            }
        });
    }
    
    
    
    //Get Vat Detail Setup
    GetVatDetailSetup()
    {
        this.commonService.GetVatDetailSetup().then(data => {
            this.arrVat=[{'id':'0','text':this.select}];
            
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                for(var i=0;i<data.result.records.length;i++)	
                {
                    this.arrVat.push({'id':data.result.records[i].vatScheduleId,'text':data.result.records[i].vatDescription});
                }
            }
        });
    }
    
    //Get AR Transaction Type
    GetARTransactionType()
    {    
        // var myform = <HTMLFormElement>document.getElementById('ARTransactionEntry');
        // if(myform)
        // {
        //     this.clear(myform,0)
        
        // }  
        this.arrTransactionType=[];
        this.transactionEntryService.GetTransactionType().then(data => {
            if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
            {
                
                this.paymentTransactionTypeId=0;
                var selectedTransactionType = data.result.find(x => x.code == 'PMT');
                if(selectedTransactionType)
                {
                    this.paymentTransactionTypeId = selectedTransactionType.typeId;
                }
                this.arrTransactionType = [];
                for(var i=0; i < data.result.length;i++)
                {
                    if(data.result[i].code != 'PMT' && data.result[i].code != 'SCP')
                    {
                        this.arrTransactionType.push({ "typeId":data.result[i].typeId, "name": data.result[i].name });
                    }
                }
                this.dynamicTransactionTypeLabel=this.arrTransactionType[0].name;
                this.arrTransactionType.splice(0, 0, { "typeId": "", "name": this.select });
                this.model.arTransactionType='';
            }
        });
    }
    
    //getTransactionNumber
    GetTransactionNumber(value,myform)
    {
        myform = <HTMLFormElement>document.getElementById('ARTransactionEntry');
        
        if(value != '')
        {
            // myform.reset();
            this.clear(myform,0);
            // this.GetARTransactionType();
            this.isModify=false;
            this.transactionEntryService.GetTransactionNumber(value).then(data => {
                this.model.arTransactionNumber=data.result;
            });
        }
        else{
            this.activeTransactionNumberList=[this.emptyArray[0]];
            this.arrTransactionNumberList=this.emptyArray;
            this.model.arTransactionNumber='';
        }
        
    }
    
    GetDistributionDetailByTransactionNumber(TransactionNumber)
    {
        this.transactionEntryService.GetDistributionDetailByTransactionNumber(TransactionNumber).then(data => {
            
            if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
            {
                this.arrARDistributionEntry=data.result;
            }
        });
    }
    
    /**
    * Distribution detail by Transaction
    * @param Transaction - Pass Transaction Entry Data  
    */
    GetDistributionDetailByTransaction(objTransaction)
    {
        this.IsAddDistributionDetails=false;
        this.transactionEntryService.GetDistributionDetailByTransaction(objTransaction).then(data => {
            
            if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
            {
                this.arrARDistributionEntry=data.result;
            }
        });
    }
    
    /**
    * Get Default Distribution 
    */
    GetDefaultDistribution()
    {
        
        if(this.distributionRequestData)
        {
            this.distributionRequestData.buttonType='DEFAULT_DISTRIBUTION'
            this.transactionEntryService.GetDistributionDetailByTransaction(this.distributionRequestData).then(data => {
                if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
                {
                    this.arrARDistributionEntry=data.result;
                }
            });
        }
    }
    
    GetTransactionEntryByTransactionNumber(TransactionNumber)
    {
        this.transactionEntryService.GetTransactionEntryByTransactionNumber(TransactionNumber).then(data => {
            this.isModify=true;
            this.model =data.result;
            this.unFrmtCostAmount = this.model.arTransactionCost;
            this.unFrmtSalesAmount=this.model.arTransactionSalesAmount;
            this.unFrmtMiscellaneous=this.model.arTransactionMiscellaneous;
            this.unFrmtTradeDiscount=this.model.arTransactionTradeDiscount;
            this.unFrmtFreightAmount = this.model.arTransactionFreightAmount;
            this.unFrmtVATAmount = this.model.arTransactionVATAmount;
            this.unFrmtTotalAmount=this.model.arTransactionTotalAmount;
            this.unFrmtPaymentAmount=this.model.paymentAmount;
            
            var arTransactionDebitMemoAmount=this.model.arTransactionDebitMemoAmount; // DR
            var arTransactionFinanceChargeAmount=this.model.arTransactionFinanceChargeAmount; // FIN
            var arTransactionWarrantyAmount=this.model.arTransactionWarrantyAmount; // WRN
            var arTransactionCreditMemoAmount=this.model.arTransactionCreditMemoAmount; // CR
            var arTransactionCashAmount=this.model.arTransactionCashAmount; 
            var arTransactionCheckAmount=this.model.arTransactionCheckAmount; 
            var arTransactionCreditCardAmount=this.model.arTransactionCreditCardAmount; // NA
            var arServiceRepairAmount=this.model.arServiceRepairAmount; // SVC
            var arReturnAmount=this.model.arReturnAmount; // RTN
            var arTransactionSalesAmount=this.model.arTransactionSalesAmount; // SLS
            
            var transactionType= this.model.arTransactionNumber.split('-');
            if(transactionType[0]== 'FIN')
            {
                this.model.arTransactionSalesAmount=arTransactionFinanceChargeAmount;
            }
            else if(transactionType[0]== 'WRN')
            {
                this.model.arTransactionSalesAmount=arTransactionWarrantyAmount;
            }
            else if(transactionType[0]== 'CR')
            {
                this.model.arTransactionSalesAmount=arTransactionCreditMemoAmount;
            }
            else if(transactionType[0]== 'RTN')
            {
                this.model.arTransactionSalesAmount=arReturnAmount;
            }
            else if(transactionType[0]== 'DR')
            {
                this.model.arTransactionSalesAmount=arTransactionDebitMemoAmount;
            }
            else if(transactionType[0]== 'SVC')
            {
                this.model.arTransactionSalesAmount=arServiceRepairAmount;
            }
            else if(transactionType[0]== 'SLS')
            {
                this.model.arTransactionSalesAmount=arTransactionSalesAmount;
            }
            
            var arTransactionDate = this.model.arTransactionDate;
            var arTransactionDateData = arTransactionDate.split('/');
            this.model.arTransactionDate = {"date":{"year":parseInt(arTransactionDateData[2]),"month":parseInt(arTransactionDateData[1]),"day":parseInt(arTransactionDateData[0])}};
            
            
            var selectedCustomer = this.arrCustomer.find(x => x.id ==   data.result.customerID);
            this.activeCustomer=[selectedCustomer];
            this.transactionEntryService.GetCustomerAccountDetailByCustomerId(selectedCustomer.id).then(data => {
                this.customerName=data.result.name;
                this.tradeDiscountPercent=data.result.tradeDiscountPercent;
                this.miscVatPercent=data.result.miscVatPercent;
                this.freightVatPercent=data.result.freightVatPercent;
                this.vatPercent=data.result.vatPercent;
            });
            //  this.bindSelectedItem(selectedCustomer,'Customer');
            
            var selectedcreditCardID = this.arrCreditCard.find(x => x.id ==  data.result.creditCardID );
            //this.bindSelectedItem(selectedBatch,'Batch');
            this.activeCreditCard=[selectedcreditCardID]; 
            
            var selectedShippingMethod = this.arrShippingMethod.find(x => x.id ==   data.result.shippingMethodID);
            this.bindSelectedItem(selectedShippingMethod,'ShippingMethod');
            // this.activeShippingMethod=[selectedShippingMethod];
            
            var selectedPaymentTerm = this.arrPaymentTermSetup.find(x => x.id ==   data.result.paymentTermsID);
            // this.bindSelectedItem(selectedPaymentTerm,'PaymentTerm');
            this.activePaymentTerm=[selectedPaymentTerm];
            
            var selectedSalesMan = this.arrSalesman.find(x => x.id ==   data.result.salesmanID);
            this.bindSelectedItem(selectedSalesMan,'Salesman');
            // this.activeSalesman=[selectedSalesMan];
            
            if(data.result.territoryId)
            {
                var selectedSalesTerritory = this.arrSalesTerritory.find(x => x.id ==   data.result.territoryId);
                //this.bindSelectedItem(selectedSalesTerritory,'SalesTerritory');
                this.activeSalesTerritory=[selectedSalesTerritory];
            }
            var selectedCheckBook = this.arrCheckbook.find(x => x.id ==  data.result.checkbookID );
            //this.bindSelectedItem(selectedCheckBook,'Checkbook');
            this.activeCheckbook=[selectedCheckBook];
            var selectedCurrency = this.arrCurrency.find(x => x.id ==  data.result.currencyID );
            //this.bindSelectedItem(selectedCurrency,'Currency');
            if(selectedCurrency)
            {
                this.activeCurrency=[selectedCurrency];
                this.formatCostAmount();
                this.formatSaleAmount();
                this.formatMiscAmount();
                this.formatTradeAmount();
                this.formatFreightAmount();
                this.formatVatAmount();
                this.formatTotalAmount();
                this.formatPaymentAmount();
                this.GetExchangeTableSetupByCurrencyId(selectedCurrency.id);
            }
            
            
            var selectedVat = this.arrVat.find(x => x.id ==  data.result.vatScheduleID );
            //this.bindSelectedItem(selectedVat,'Vat');
            this.activeVat=[selectedVat];
            
            if(data.result.batchID)
            {
                var selectedBatch = this.arrBatchList.find(x => x.id ==  data.result.batchID );
                this.activeBatch=[selectedBatch];
            }
            
            window.setTimeout(() => {
                //     var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id ==  data.result.exchangeTableIndex );
                //     this.activeExchangeDetail=[selectedExchangeDetail];
                //     this.GetCurrencyExchangeDetail(selectedExchangeDetail.id);
                //  }, 1000);
                
                var selectedExchangeDetail = this.arrExchangeDetail.find(x => x.id == data.result.exchangeTableIndex);
                this.model.exchangeTableIndex=selectedExchangeDetail.id;
                
                this.activeExchangeDetail=[selectedExchangeDetail];
                if(selectedExchangeDetail)
                {
                    this.commonService.GetCurrencyExchangeDetail(selectedExchangeDetail.id).then(fatchdata => {
                        if(fatchdata.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND")
                        {
                            this.model.exchangeExpirationDate=fatchdata.result.exchangeExpirationDate;
                        }
                    });
                }
                
                // this.bindSelectedItem(selectedExchangeDetail,'ExchangeDetail');
            }, 2000);
        });
    }
    GetTransactionNumberListByTransactionType(evt)
    {
        this.GetTransactionNumber(evt.target.value,myform);
        
        if(evt.target.value == ''){
            this.dynamicTransactionTypeLabel=this.arrTransactionType[1].name;
            
        }
        else{
            this.dynamicTransactionTypeLabel=evt.target[evt.target.selectedIndex].text;
        }
        
        if(evt.target.value != '')
        {
            this.model.arTransactionType = evt.target.value;
            this.transactionEntryService.GetTransactionNumberListByTransactionType(evt.target.value).then(data => {
                // this.arrTransactionNumberList=data.result;
                this.arrTransactionNumberList=[{'id':'0','text':this.select}];
                
                if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                {
                    for(var i=0;i<data.result.length;i++)	
                    {
                        this.arrTransactionNumberList.push({'id':data.result[i],'text':data.result[i]});
                    }
                }
                else{
                    this.activeTransactionNumberList=[{'id':'0','text':this.select}]
                }
            });
        }
        else{
            
            var myform = <HTMLFormElement>document.getElementById('ARTransactionEntry');
            if(myform)
            {
                this.clear(myform,0)
                
            }
        }
        
    }
    
    PaymentTypeChange(evt)
    {
        var PaymentMethodId=evt.target.value;
        if(PaymentMethodId == 1)
        {
            this.model.checkNumber='';
            this.model.creditCardExpireMonth='';
            this.model.creditCardExpireYear='';
            this.model.creditCardNumber='';
            this.bindSelectedItem(this.emptyArray[0],'CreditCard');
        }
        else if(PaymentMethodId == 2)
        {
            this.model.creditCardExpireMonth='';
            this.model.creditCardExpireYear='';
            this.model.creditCardNumber='';
            this.bindSelectedItem(this.emptyArray[0],'CreditCard');
        }
        else if(PaymentMethodId == 3){
            this.model.checkNumber='';
        }
    }
    
    SaveARTransactionEntry(f :NgForm,myModal){
        this.btndisabled=true;
        if(f.valid  && this.activeCustomer[0].id != '0'  
        && this.activeCurrency[0].id != '0' && this.activeSalesman[0].id != '0' && this.activeSalesTerritory[0].id != '0' 
        && this.activePaymentTerm[0].id != '0'  && this.activeShippingMethod[0].id != '0' && this.activeVat[0].id != '0' 
        && this.activeCheckbook[0].id != '0' && this.activeExchangeDetail[0].id != '0')
        {
            
            if(this.model.arTransactionDate.formatted == undefined)
            {
                var transactionDate = this.model.arTransactionDate;
                if(transactionDate.date != undefined)
                {
                    this.model.arTransactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
                }
            }
            else
            {
                this.model.arTransactionDate = this.model.arTransactionDate.formatted;
            }
            
            
            var checkbookID =this.model.checkbookID 
            if(!this.model.checkbookID)
            {
                checkbookID=0;
            }
            var checkNumber =this.model.checkNumber 
            if(!this.model.checkNumber)
            {
                checkNumber=0;
            }
            
            var cashReceiptNumber =this.model.cashReceiptNumber 
            if(!this.model.cashReceiptNumber)
            {
                cashReceiptNumber=0;
            }
            var creditCardID =this.model.creditCardID 
            if(!this.model.creditCardID)
            {
                creditCardID=0;
            }
            
            var creditCardNumber =this.model.creditCardNumber 
            if(!this.model.creditCardNumber)
            {
                creditCardNumber=0;
            }
            
            var creditCardExpireYear =this.model.creditCardExpireYear 
            if(!this.model.creditCardExpireYear)
            {
                creditCardExpireYear=0;
            }
            
            var creditCardExpireMonth =this.model.creditCardExpireMonth 
            if(!this.model.creditCardExpireMonth)
            {
                creditCardExpireMonth=0;
            }
            
            var arTransactionStatus =this.model.arTransactionStatus 
            if(!this.model.arTransactionStatus)
            {
                arTransactionStatus=0;
            }
            
            var customerNameArabic =this.model.customerNameArabic 
            if(!this.model.customerNameArabic)
            {
                customerNameArabic='';
            }
            
            var isPayment=true;
            if(this.model.arTransactionTotalAmount == 0)
            {
                isPayment=false;
            }
            
            var exchangeTableIndex= '0';
            if(this.activeExchangeDetail[0].id != '0')
            {
                exchangeTableIndex = this.activeExchangeDetail[0].id
            }
            
            
            var arTransactionDebitMemoAmount="0"; // DR
            var arTransactionFinanceChargeAmount="0"; // FIN
            var arTransactionWarrantyAmount="0"; // WRN
            var arTransactionCreditMemoAmount="0"; // CR
            var arTransactionCashAmount="0"; 
            var arTransactionCheckAmount="0"; 
            var arTransactionCreditCardAmount="0"; // NA
            var arServiceRepairAmount="0"; // SVC
            var arReturnAmount="0"; // RTN
            var arTransactionSalesAmount="0"; // SLS
            
            this.model.arTransactionCost=this.unFrmtCostAmount;
            this.model.arTransactionSalesAmount=this.unFrmtSalesAmount;
            this.model.arTransactionMiscellaneous=this.unFrmtMiscellaneous;
            this.model.arTransactionTradeDiscount=this.unFrmtTradeDiscount;
            this.model.arTransactionFreightAmount=this.unFrmtFreightAmount;
            this.model.arTransactionTotalAmount=this.unFrmtTotalAmount;
            this.model.arTransactionVATAmount=this.unFrmtVATAmount;
            this.model.paymentAmount=this.unFrmtPaymentAmount;
            
            var transactionType= this.model.arTransactionNumber.split('-');
            if(transactionType[0]== 'FIN')
            {
                arTransactionFinanceChargeAmount=this.model.arTransactionSalesAmount;
                arTransactionDebitMemoAmount="0";
                arTransactionWarrantyAmount="0";
                arTransactionCreditMemoAmount="0";
                arTransactionCashAmount="0";
                arTransactionCheckAmount="0";
                arTransactionCreditCardAmount="0";
                arServiceRepairAmount="0";
                arReturnAmount="0";
                arTransactionSalesAmount="0";
            }
            else if(transactionType[0]== 'WRN')
            {
                arTransactionFinanceChargeAmount="0";
                arTransactionDebitMemoAmount="0";
                arTransactionWarrantyAmount=this.model.arTransactionSalesAmount;
                arTransactionCreditMemoAmount="0";
                arTransactionCashAmount="0";
                arTransactionCheckAmount="0";
                arTransactionCreditCardAmount="0";
                arServiceRepairAmount="0";
                arReturnAmount="0";
                arTransactionSalesAmount="0";
            }
            else if(transactionType[0]== 'CR')
            {
                arTransactionFinanceChargeAmount="0";
                arTransactionDebitMemoAmount="0";
                arTransactionWarrantyAmount="0";
                arTransactionCreditMemoAmount=this.model.arTransactionSalesAmount;
                arTransactionCashAmount="0";
                arTransactionCheckAmount="0";
                arTransactionCreditCardAmount="0";
                arServiceRepairAmount="0";
                arReturnAmount="0";
                arTransactionSalesAmount="0";
            }
            else if(transactionType[0]== 'RTN')
            {
                arTransactionFinanceChargeAmount="0";
                arTransactionDebitMemoAmount="0";
                arReturnAmount=this.model.arTransactionSalesAmount;
                arTransactionCreditMemoAmount="0";
                arTransactionCashAmount="0";
                arTransactionCheckAmount="0";
                arTransactionCreditCardAmount="0";
                arServiceRepairAmount="0";
                arTransactionWarrantyAmount="0";
                arTransactionSalesAmount="0";
            }
            
            else if(transactionType[0]== 'DR')
            {
                arTransactionFinanceChargeAmount="0";
                arTransactionDebitMemoAmount=this.model.arTransactionSalesAmount;
                arReturnAmount="0";
                arTransactionCreditMemoAmount="0";
                arTransactionCashAmount="0";
                arTransactionCheckAmount="0";
                arTransactionCreditCardAmount="0";
                arServiceRepairAmount="0";
                arTransactionWarrantyAmount="0";
                arTransactionSalesAmount="0";
            }
            else if(transactionType[0]== 'SVC')
            {
                arTransactionFinanceChargeAmount="0";
                arServiceRepairAmount=this.model.arTransactionSalesAmount;
                arReturnAmount="0";
                arTransactionCreditMemoAmount="0";
                arTransactionCashAmount="0";
                arTransactionCheckAmount="0";
                arTransactionCreditCardAmount="0";
                arTransactionDebitMemoAmount="0";
                arTransactionWarrantyAmount="0";
                arTransactionSalesAmount="0";
            }
            else if(transactionType[0]== 'SLS')
            {
                arTransactionFinanceChargeAmount="0";
                arTransactionSalesAmount=this.model.arTransactionSalesAmount;
                arReturnAmount="0";
                arTransactionCreditMemoAmount="0";
                arTransactionCashAmount="0";
                arTransactionCheckAmount="0";
                arTransactionCreditCardAmount="0";
                arTransactionDebitMemoAmount="0";
                arTransactionWarrantyAmount="0";
                arServiceRepairAmount="0";
            }
            
            
            
            var requestedData={
                "arTransactionNumber": this.model.arTransactionNumber,
                "arTransactionType": this.model.arTransactionType,
                "batchID": this.model.batchID,
                "arTransactionDescription":this.model.arTransactionDescription,
                "arTransactionDate": this.model.arTransactionDate,
                "customerID": this.model.customerID,
                "customerName":this.customerName,
                "customerNameArabic": customerNameArabic,
                "currencyID": this.model.currencyID,
                "paymentTermsID": this.model.paymentTermsID,
                "arTransactionCost": this.model.arTransactionCost,
                "arTransactionSalesAmount": arTransactionSalesAmount,
                "arTransactionTradeDiscount": this.model.arTransactionTradeDiscount,
                "arTransactionFreightAmount": this.model.arTransactionFreightAmount,
                "arTransactionMiscellaneous": this.model.arTransactionMiscellaneous,
                "arTransactionVATAmount": this.model.arTransactionVATAmount,
                "arTransactionDebitMemoAmount": arTransactionDebitMemoAmount,
                "arTransactionFinanceChargeAmount": arTransactionFinanceChargeAmount,
                "arTransactionWarrantyAmount": arTransactionWarrantyAmount,
                "arTransactionCreditMemoAmount": arTransactionCreditMemoAmount,
                "arTransactionTotalAmount": this.model.arTransactionTotalAmount,
                "arTransactionCashAmount":0,
                "arTransactionCheckAmount": arTransactionCheckAmount,
                "arTransactionCreditCardAmount":arTransactionCreditCardAmount,
                "salesmanID": this.model.salesmanID,
                "checkbookID":checkbookID,
                "checkNumber": checkNumber,
                "cashReceiptNumber":cashReceiptNumber,
                "creditCardID":creditCardID,
                "creditCardNumber":creditCardNumber,
                "creditCardExpireYear": creditCardExpireYear,
                "creditCardExpireMonth":creditCardExpireMonth,
                "arTransactionStatus": arTransactionStatus,
                "exchangeTableIndex":exchangeTableIndex,
                "exchangeTableRate":this.model.exchangeTableRate,
                "transactionVoid":0,
                "shippingMethodID":this.model.shippingMethodID,
                "vatScheduleID":this.model.vatScheduleID,
                "paymentTypeId":this.model.paymentTypeId,
                "territoryId":this.model.territoryId,
                "isPayment":isPayment,
                "paymentAmount":this.model.paymentAmount,
                "arServiceRepairAmount":this.model.arServiceRepairAmount,
                "arReturnAmount":this.model.arReturnAmount,
                "paymentTransactionTypeId": this.paymentTransactionTypeId
            };
            if(this.activeCreditCard[0].id == '0')
            {
                this.model.creditCardID = '';
            }
            
            this.transactionEntryService.SaveUpdateTransactionEntry(requestedData,this.isModify).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.isModify=false;
                    this.clear(f,1);
                    //this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    //this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        else{
            
            if(this.activeExchangeDetail[0].id == '0')
            {
                myModal.open();
            }
        }
        this.btndisabled=false;
    }
    
    Amount(paymentAmount){
        if(this.model.arTransactionTotalAmount >= paymentAmount){
            var paymentAmount1=this.model.paymentAmount;
        }
        else{
            
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = "Total Amount should be greater than Payment Amount";
            window.scrollTo(0,0);
            this.model.paymentAmount='';
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
            window.scrollTo(0,0);
        }
    }
    
    PostARTransactionEntry(f :NgForm,myModal){
        this.btndisabled=true;
        if(f.valid && this.activeBatch[0].id !='0')
        {
            window.scrollTo(0,0);
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText =this.defaultAddFormValues[31]['listDtoFieldValidationMessage'][0]['validationMessage'];
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
        }
        else{
            
            
            if(f.valid  && this.activeCustomer[0].id != '0'  
            && this.activeCurrency[0].id != '0' && this.activeSalesman[0].id != '0' && this.activeSalesTerritory[0].id != '0' 
            && this.activePaymentTerm[0].id != '0'  && this.activeShippingMethod[0].id != '0' && this.activeVat[0].id != '0' 
            && this.activeCheckbook[0].id != '0' && this.activeExchangeDetail[0].id != '0')
            {
                
                if(this.model.arTransactionDate.formatted == undefined)
                {
                    var transactionDate = this.model.arTransactionDate;
                    if(transactionDate.date != undefined)
                    {
                        this.model.arTransactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
                    }
                }
                else
                {
                    this.model.arTransactionDate = this.model.arTransactionDate.formatted;
                }
                
                
                var checkbookID =this.model.checkbookID 
                if(!this.model.checkbookID)
                {
                    checkbookID=0;
                }
                var checkNumber =this.model.checkNumber 
                if(!this.model.checkNumber)
                {
                    checkNumber=0;
                }
                
                var cashReceiptNumber =this.model.cashReceiptNumber 
                if(!this.model.cashReceiptNumber)
                {
                    cashReceiptNumber=0;
                }
                var creditCardID =this.model.creditCardID 
                if(!this.model.creditCardID)
                {
                    creditCardID=0;
                }
                
                var creditCardNumber =this.model.creditCardNumber 
                if(!this.model.creditCardNumber)
                {
                    creditCardNumber=0;
                }
                
                var creditCardExpireYear =this.model.creditCardExpireYear 
                if(!this.model.creditCardExpireYear)
                {
                    creditCardExpireYear=0;
                }
                
                var creditCardExpireMonth =this.model.creditCardExpireMonth 
                if(!this.model.creditCardExpireMonth)
                {
                    creditCardExpireMonth=0;
                }
                
                var arTransactionStatus =this.model.arTransactionStatus 
                if(!this.model.arTransactionStatus)
                {
                    arTransactionStatus=0;
                }
                
                var customerNameArabic =this.model.customerNameArabic 
                if(!this.model.customerNameArabic)
                {
                    customerNameArabic='';
                }
                
                var isPayment=true;
                if(this.model.arTransactionTotalAmount == 0)
                {
                    isPayment=false;
                }
                
                var exchangeTableIndex= '0';
                if(this.activeExchangeDetail[0].id != '0')
                {
                    exchangeTableIndex = this.activeExchangeDetail[0].id
                }
                
                
                var arTransactionDebitMemoAmount="0"; // DR
                var arTransactionFinanceChargeAmount="0"; // FIN
                var arTransactionWarrantyAmount="0"; // WRN
                var arTransactionCreditMemoAmount="0"; // CR
                var arTransactionCashAmount="0"; 
                var arTransactionCheckAmount="0"; 
                var arTransactionCreditCardAmount="0"; // NA
                var arServiceRepairAmount="0"; // SVC
                var arReturnAmount="0"; // RTN
                var arTransactionSalesAmount="0"; // SLS
                
                this.model.arTransactionCost=this.unFrmtCostAmount;
                this.model.arTransactionSalesAmount=this.unFrmtSalesAmount;
                this.model.arTransactionMiscellaneous=this.unFrmtMiscellaneous;
                this.model.arTransactionTradeDiscount=this.unFrmtTradeDiscount;
                this.model.arTransactionFreightAmount=this.unFrmtFreightAmount;
                this.model.arTransactionTotalAmount=this.unFrmtTotalAmount;
                this.model.arTransactionVATAmount=this.unFrmtVATAmount;
                this.model.paymentAmount=this.unFrmtPaymentAmount;
                
                var transactionType= this.model.arTransactionNumber.split('-');
                if(transactionType[0]== 'FIN')
                {
                    arTransactionFinanceChargeAmount=this.model.arTransactionSalesAmount;
                    arTransactionDebitMemoAmount="0";
                    arTransactionWarrantyAmount="0";
                    arTransactionCreditMemoAmount="0";
                    arTransactionCashAmount="0";
                    arTransactionCheckAmount="0";
                    arTransactionCreditCardAmount="0";
                    arServiceRepairAmount="0";
                    arReturnAmount="0";
                    arTransactionSalesAmount="0";
                }
                else if(transactionType[0]== 'WRN')
                {
                    arTransactionFinanceChargeAmount="0";
                    arTransactionDebitMemoAmount="0";
                    arTransactionWarrantyAmount=this.model.arTransactionSalesAmount;
                    arTransactionCreditMemoAmount="0";
                    arTransactionCashAmount="0";
                    arTransactionCheckAmount="0";
                    arTransactionCreditCardAmount="0";
                    arServiceRepairAmount="0";
                    arReturnAmount="0";
                    arTransactionSalesAmount="0";
                }
                else if(transactionType[0]== 'CR')
                {
                    arTransactionFinanceChargeAmount="0";
                    arTransactionDebitMemoAmount="0";
                    arTransactionWarrantyAmount="0";
                    arTransactionCreditMemoAmount=this.model.arTransactionSalesAmount;
                    arTransactionCashAmount="0";
                    arTransactionCheckAmount="0";
                    arTransactionCreditCardAmount="0";
                    arServiceRepairAmount="0";
                    arReturnAmount="0";
                    arTransactionSalesAmount="0";
                }
                else if(transactionType[0]== 'RTN')
                {
                    arTransactionFinanceChargeAmount="0";
                    arTransactionDebitMemoAmount="0";
                    arReturnAmount=this.model.arTransactionSalesAmount;
                    arTransactionCreditMemoAmount="0";
                    arTransactionCashAmount="0";
                    arTransactionCheckAmount="0";
                    arTransactionCreditCardAmount="0";
                    arServiceRepairAmount="0";
                    arTransactionWarrantyAmount="0";
                    arTransactionSalesAmount="0";
                }
                
                else if(transactionType[0]== 'DR')
                {
                    arTransactionFinanceChargeAmount="0";
                    arTransactionDebitMemoAmount=this.model.arTransactionSalesAmount;
                    arReturnAmount="0";
                    arTransactionCreditMemoAmount="0";
                    arTransactionCashAmount="0";
                    arTransactionCheckAmount="0";
                    arTransactionCreditCardAmount="0";
                    arServiceRepairAmount="0";
                    arTransactionWarrantyAmount="0";
                    arTransactionSalesAmount="0";
                }
                else if(transactionType[0]== 'SVC')
                {
                    arTransactionFinanceChargeAmount="0";
                    arServiceRepairAmount=this.model.arTransactionSalesAmount;
                    arReturnAmount="0";
                    arTransactionCreditMemoAmount="0";
                    arTransactionCashAmount="0";
                    arTransactionCheckAmount="0";
                    arTransactionCreditCardAmount="0";
                    arTransactionDebitMemoAmount="0";
                    arTransactionWarrantyAmount="0";
                    arTransactionSalesAmount="0";
                }
                else if(transactionType[0]== 'SLS')
                {
                    arTransactionFinanceChargeAmount="0";
                    arTransactionSalesAmount=this.model.arTransactionSalesAmount;
                    arReturnAmount="0";
                    arTransactionCreditMemoAmount="0";
                    arTransactionCashAmount="0";
                    arTransactionCheckAmount="0";
                    arTransactionCreditCardAmount="0";
                    arTransactionDebitMemoAmount="0";
                    arTransactionWarrantyAmount="0";
                    arServiceRepairAmount="0";
                }
                
                var requestedData={
                    "arTransactionNumber": this.model.arTransactionNumber,
                    "arTransactionType": this.model.arTransactionType,
                    "batchID": this.model.batchID,
                    "arTransactionDescription":this.model.arTransactionDescription,
                    "arTransactionDate": this.model.arTransactionDate,
                    "customerID": this.model.customerID,
                    "customerName":this.customerName,
                    "customerNameArabic": customerNameArabic,
                    "currencyID": this.model.currencyID,
                    "paymentTermsID": this.model.paymentTermsID,
                    "arTransactionCost": this.model.arTransactionCost,
                    "arTransactionSalesAmount": arTransactionSalesAmount,
                    "arTransactionTradeDiscount": this.model.arTransactionTradeDiscount,
                    "arTransactionFreightAmount": this.model.arTransactionFreightAmount,
                    "arTransactionMiscellaneous": this.model.arTransactionMiscellaneous,
                    "arTransactionVATAmount": this.model.arTransactionVATAmount,
                    "arTransactionDebitMemoAmount": arTransactionDebitMemoAmount,
                    "arTransactionFinanceChargeAmount": arTransactionFinanceChargeAmount,
                    "arTransactionWarrantyAmount": arTransactionWarrantyAmount,
                    "arTransactionCreditMemoAmount": arTransactionCreditMemoAmount,
                    "arTransactionTotalAmount": this.model.arTransactionTotalAmount,
                    "arTransactionCashAmount":0,
                    "arTransactionCheckAmount": arTransactionCheckAmount,
                    "arTransactionCreditCardAmount":arTransactionCreditCardAmount,
                    "salesmanID": this.model.salesmanID,
                    "checkbookID":checkbookID,
                    "checkNumber": checkNumber,
                    "cashReceiptNumber":cashReceiptNumber,
                    "creditCardID":creditCardID,
                    "creditCardNumber":creditCardNumber,
                    "creditCardExpireYear": creditCardExpireYear,
                    "creditCardExpireMonth":creditCardExpireMonth,
                    "arTransactionStatus": arTransactionStatus,
                    "exchangeTableIndex":exchangeTableIndex,
                    "exchangeTableRate":this.model.exchangeTableRate,
                    "transactionVoid":0,
                    "shippingMethodID":this.model.shippingMethodID,
                    "vatScheduleID":this.model.vatScheduleID,
                    "paymentTypeId":this.model.paymentTypeId,
                    "territoryId":this.model.territoryId,
                    "isPayment":isPayment,
                    "paymentAmount":this.model.paymentAmount,
                    "arServiceRepairAmount":this.model.arServiceRepairAmount,
                    "arReturnAmount":this.model.arReturnAmount,
                    "paymentTransactionTypeId": this.paymentTransactionTypeId
                };
                
                if(this.activeCreditCard[0].id == '0')
                {
                    this.model.creditCardID = '';
                }
                
                this.transactionEntryService.SaveUpdateTransactionEntry(requestedData,this.isModify).then(data => {
                    
                    if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') 
                    {
                        
                        this.transactionEntryService.PostARTransactionEntry(requestedData).then(data => {
                            window.scrollTo(0,0);
                            this.btndisabled=false;
                            if (data.btiMessage.messageShort == 'RECORD_POST_SUCCESSFULLY') {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.isModify=false;
                                this.clear(f,1);
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                        
                    }
                })
                
                
            }
            else{
                
                if(this.activeExchangeDetail[0].id == '0')
                {
                    myModal.open();
                }
            }
        }
        this.btndisabled=false;
    }
    
    // Delete AR Transaction Entry
    DeleteArTransactionEntry(f:NgForm)
    {
        this.transactionEntryService.DeleteTransactionEntry(this.model.arTransactionNumber).then(data =>
            {
                
                window.scrollTo(0,0);
                this.closeModal();
                if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.isModify=false;
                    this.clear(f,1);
                    //this.bindSelectedItem(this.emptyArray[0],'JournalEntry');
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        // Delete AR Transaction Entry Distribution
        DeleteArTransactionEntryDistribution(myModal)
        {
            
            this.transactionEntryService.DeleteTransactionEntryDistribution(this.model.arTransactionNumber).then(data =>
                {
                    
                    window.scrollTo(0,0);
                    
                    if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.isModify=false;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
                myModal.close();
            }
            
            // Get Customer List
            GetCustomerList()
            {
                this.commonService.GetCustomerList().then(data => {
                    this.arrCustomer=[{'id':'0','text':this.select}];
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrCustomer.push({'id':data.result.records[i].customerId,'text':data.result.records[i].customerId});
                        }
                    }
                });
            }
            // Get Customer By CustomerId
            GetCustomerByCustomerId(customerId)
            {
                this.unFrmtVATAmount = 0;
                this.unFrmtMiscellaneous=0;
                this.unFrmtFreightAmount=0;
                this.unFrmtTotalAmount = 0;
                this.unFrmtPaymentAmount=0;
                this.unFrmtCostAmount = 0;
                this.unFrmtSalesAmount = 0;
                this.unFrmtTradeDiscount = 0;
                
                this.transactionEntryService.GetCustomerAccountDetailByCustomerId(customerId).then(data => {
                    
                    this.customerName=data.result.name;
                    var selectedShippingMethod = this.arrShippingMethod.find(x => x.id ==   data.result.shipmentMethodId);
                    if(selectedShippingMethod)
                    {
                        this.bindSelectedItem(selectedShippingMethod,'ShippingMethod');
                    }
                    
                    var selectedPaymentTerm = this.arrPaymentTermSetup.find(x => x.id ==   data.result.paymentTermId);
                    if(selectedPaymentTerm)
                    {
                        this.bindSelectedItem(selectedPaymentTerm,'PaymentTerm');
                    }
                    
                    var selectedSalesMan = this.arrSalesman.find(x => x.id ==   data.result.salesmanId);
                    if(selectedSalesMan)
                    {
                        this.bindSelectedItem(selectedSalesMan,'Salesman');
                    }
                    
                    var selectedSalesTerritory = this.arrSalesTerritory.find(x => x.id ==   data.result.salesTerritoryId);
                    if(selectedSalesTerritory)
                    {
                        this.bindSelectedItem(selectedSalesTerritory,'SalesTerritory');
                    }
                    
                    var selectedCheckBook = this.arrCheckbook.find(x => x.id ==  data.result.checkbookId );
                    if(selectedCheckBook)
                    {
                        this.bindSelectedItem(selectedCheckBook,'Checkbook');
                    }
                    
                    
                    var selectedCurrency = this.arrCurrency.find(x => x.id ==  data.result.currencyId );
                    if(selectedCurrency)
                    {
                        this.bindSelectedItem(selectedCurrency,'Currency');
                    }
                    
                    var selectedVat = this.arrVat.find(x => x.id ==  data.result.vatId );
                    if(selectedVat)
                    {
                        this.bindSelectedItem(selectedVat,'Vat');
                    }
                    
                    this.tradeDiscountPercent=data.result.tradeDiscountPercent;
                    this.miscVatPercent=data.result.miscVatPercent;
                    this.freightVatPercent=data.result.freightVatPercent;
                    this.vatPercent=data.result.vatPercent;
                    this.model.arTransactionTradeDiscount= data.result.tradeDiscountPercent;
                    this.unFrmtTradeDiscount = this.model.arTransactionTradeDiscount;
                    if(selectedCurrency)
                    {
                        
                        this.formatCostAmount();
                        this.formatSaleAmount();
                        this.formatMiscAmount();
                        this.formatTradeAmount();
                        this.formatFreightAmount();
                        this.formatVatAmount();
                        this.formatTotalAmount();
                        this.formatPaymentAmount();
                        
                    }
                    //  this.model.arTransactionMiscellaneous= data.result.tradeDiscountPercent;
                    //  this.model.arTransactionFreightAmount= data.result.tradeDiscountPercent;
                    //  this.model.arTransactionVATAmount= data.result.tradeDiscountPercent;
                    
                    this.calculateTotalAmount('');
                    
                });
            }
            
            calculateTotalAmount(action:string)
            {
                if(action == 'Cost')
                {
                    this.formatCostAmount();
                }
                else if(action == 'SalesAmount')
                {
                    this.formatSaleAmount();
                }
                else if(action == 'TradeDiscount')
                {
                    this.formatTradeAmount();
                }
                else if(action == 'Miscellaneous')
                {
                    this.formatMiscAmount();
                }
                if(action == 'FreightAmount')
                {
                    this.formatFreightAmount();
                }
                else if(action == 'VATAmount')
                {
                    
                    this.formatVatAmount();
                }
                else if(action == 'TotalAmount')
                {
                    this.formatTotalAmount();
                }
                
                if(!this.unFrmtCostAmount)
                {
                    this.unFrmtCostAmount = '0';
                } 
                if(!this.unFrmtSalesAmount)
                {
                    this.unFrmtSalesAmount = '0';
                } 
                if(!this.unFrmtTradeDiscount)
                {
                    this.unFrmtTradeDiscount = '0';
                }
                if(!this.unFrmtMiscellaneous)
                {
                    this.unFrmtMiscellaneous = '0';
                }
                if(!this.unFrmtFreightAmount)
                {
                    this.unFrmtFreightAmount = '0';
                } 
                if(!this.unFrmtVATAmount)
                {
                    this.unFrmtVATAmount = '0';
                } 
                if(!this.unFrmtTotalAmount)
                {
                    this.unFrmtTotalAmount = '0';
                }
                if(!this.unFrmtPaymentAmount)
                {
                    this.unFrmtPaymentAmount = '0';
                }
                var totalSale  = parseFloat(this.unFrmtSalesAmount);
                if(totalSale)
                {
                    
                    var arTransactionTradeDiscount=0;
                    if(this.unFrmtTradeDiscount || this.unFrmtTradeDiscount == 0)
                    {
                        arTransactionTradeDiscount = parseFloat(this.unFrmtTradeDiscount);
                    }
                    this.tempRemainingAmount=this.remainingAmount;
                    this.remainingAmount=totalSale-arTransactionTradeDiscount;
                    
                    var vatPercent=this.vatPercent;
                    // var vatAmount=  this.model.arTransactionVATAmount;
                  //  var vatAmount=  this.unFrmtVATAmount;
                    this.unFrmtVATAmount = "0"
                    let vatAmount="0";
                    
                    if(action == '')
                    {
                        if(this.vatPercent > 0)
                        {
                            vatAmount= ( this.remainingAmount * vatPercent/100).toFixed(2);
                        }
                    }
                    else{
                        
                        if((this.unFrmtVATAmount && parseFloat(this.unFrmtVATAmount) == 0)||(this.tempRemainingAmount != this.remainingAmount))
                        {
                            vatAmount= ( this.remainingAmount * vatPercent/100).toFixed(2);
                        }
                    }
                    
                    var arTransactionFreightAmount = 0;
                    var FreightAmount = 0;
                    var FreightAmountTotal = 0;
                    
                    var arTransactionMiscellaneous = 0;
                    var miscellaneousAmount = 0;
                    var miscellaneousAmountTotal = 0;
                    
                    if(this.unFrmtMiscellaneous && this.unFrmtMiscellaneous !='0')
                    {
                        arTransactionMiscellaneous = parseFloat(this.unFrmtMiscellaneous);
                        miscellaneousAmount = arTransactionMiscellaneous * this.miscVatPercent/100;
                        miscellaneousAmountTotal = arTransactionMiscellaneous + miscellaneousAmount;
                    }
                    
                    
                    if(this.unFrmtFreightAmount && this.unFrmtFreightAmount !='0')
                    {
                        arTransactionFreightAmount = parseFloat(this.unFrmtFreightAmount);
                        FreightAmount = arTransactionFreightAmount * this.freightVatPercent/100;
                        FreightAmountTotal = arTransactionFreightAmount + FreightAmount;
                    }
                    // this.model.arTransactionVATAmount =    parseFloat(vatAmount).toFixed(2);
                   // this.unFrmtVATAmount = parseFloat(vatAmount) + miscellaneousAmountTotal+FreightAmountTotal;
                   this.unFrmtVATAmount = parseFloat(vatAmount) + miscellaneousAmount+FreightAmount;
                    this.unFrmtVATAmount = parseFloat( this.unFrmtVATAmount).toFixed(2);
                    this.formatVatAmount();
                    var arTransactionTotalAmount=this.remainingAmount + parseFloat(vatAmount);
                    arTransactionTotalAmount = Math.round(arTransactionTotalAmount * 100) / 100;
                    this.unFrmtTotalAmount = arTransactionTotalAmount;
                    this.formatTotalAmount();
                    this.formatPaymentAmount();
                    //this.model.paymentAmount=arTransactionTotalAmount;
                }
                else
                {
                    // this.model.arTransactionVATAmount = 0;
                    // this.model.arTransactionSalesAmount = 0;
                    // this.model.arTransactionMiscellaneous = 0;
                    this.unFrmtSalesAmount = 0;
                    this.unFrmtMiscellaneous=0;
                    this.unFrmtFreightAmount = 0;
                    this.unFrmtTotalAmount=0;
                    this.unFrmtPaymentAmount = 0;
                    
                    // this.model.arTransactionFreightAmount = 0;
                    // this.model.arTransactionTotalAmount = 0;
                    // this.model.paymentAmount = 0;
                }
            }
            // Get Salesman Maintenance
            GetActiveSalesmanMaintenanceList()
            {
                this.commonService.GetActiveSalesmanMaintenanceList().then(data => {
                    this.arrSalesman =[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrSalesman.push({'id':data.result.records[i].salesmanId,'text':data.result.records[i].salesmanId});
                        }
                    }
                });
            }
            
            //Get Salesman Maintenance By Id
            GetSalesmanMaintenanceById(salesmanId: string) {
                this.commonService.GetSalesmanMaintenanceById(salesmanId).then(data => {
                    this.salesmanName=data.result.salesmanFirstName;
                });
            }
            
            // Get Sales Territory Setup List
            GetSalesTerritorySetupList()
            {
                this.commonService.GetSalesTerritorySetupList().then(data => {
                    
                    this.arrSalesTerritory =[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrSalesTerritory.push({'id':data.result.records[i].salesTerritoryId,'text':data.result.records[i].salesTerritoryId});
                        }
                    }
                });
            }
            
            // Get Payment Term List
            GetPaymentTermList()
            {
                this.commonService.GetPaymentTermList().then(data => {
                    this.arrPaymentTermSetup=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrPaymentTermSetup.push({'id':data.result.records[i].paymentTermId,'text':data.result.records[i].description});
                        }
                    }
                });
            }
            
            
            // Get Credit Card Setup List
            GetCreditCardSetup()
            {
                this.commonService.GetCreditCardSetup().then(data => {
                    this.arrCreditCard=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrCreditCard.push({'id':data.result.records[i].cardId,'text':data.result.records[i].cardId});
                        }
                    }
                });
            }
            
            BindAccountNumber()
            {
                this.commonService.getGlAccountNumberList().then(data => {
                    this.arrAccountNumber=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.length;i++)	
                        {
                            this.arrAccountNumber.push({'id':data.result[i].accountTableRowIndex,'text':data.result[i].accountNumber});
                        }
                    }
                });
            }
            
            //Get Distribution Account Type List
            GetDistributionAccountTypesList()
            {
                this.commonService.GetDistributionAccountTypesList().then(data => {
                    this.arrDistributionAccountTypes=data.result;
                    
                    this.arrDistributionAccountTypes.splice(0, 0, { "typeId": "", "typeCode": this.select });
                    this.distributionAccountType='';
                });
            }
            GetExchangeTableSetupByCurrencyId(currencyId: string){
                this.commonService.getExchangeTableSetupByCurrencyId(currencyId).then(data => {
                    
                    this.arrExchangeDetail=[{'id':'0','text':this.select}];
                    // this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail')
                    
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrExchangeDetail.push({'id':data.result.records[i].exchangeTableIndex,'text':data.result.records[i].exchangeId});
                        }
                    }
                });
            }
            
            GetCurrencyExchangeDetail(exchangeId: string){
                this.commonService.GetCurrencyExchangeDetail(exchangeId).then(data => {
                    
                    if(data.btiMessage.messageShort != "EXCHANGE_ID_NOT_FOUND")
                    {
                        
                        // if(!this.model.exchangeTableRate)
                        // {
                        
                        // }     
                        this.model.exchangeTableRate=data.result.exchangeRate; 
                        this.model.exchangeExpirationDate=data.result.exchangeExpirationDate;
                    }
                    else{
                        this.model.exchangeTableRate='';
                        this.model.exchangeExpirationDate='';
                    }
                });
            }
            
            // Get Shipping Method List
            GetShippingMethodList()
            {
                this.commonService.GetShippingMethodList().then(data => {
                    this.arrShippingMethod=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrShippingMethod.push({'id':data.result.records[i].shipmentMethodId,'text':data.result.records[i].shipmentDescription});
                        }
                    }
                });
            }
            
            // Get Checkbook Maintenance
            GetCheckbookMaintenance()
            {
                this.commonService.GetCheckbookMaintenance().then(data => {
                    this.arrCheckbook=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrCheckbook.push({'id':data.result.records[i].checkBookId,'text':data.result.records[i].checkBookId});
                        }
                    }
                });
            }
            
            // Get Currency Setup List
            GetCurrencySetup()
            {
                this.commonService.getCurrencySetup().then(data => {
                    this.arrCurrency=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrCurrency.push({'id':data.result.records[i].currencyId,'text':data.result.records[i].currencyId});
                        }
                    }
                });
            }
            
            //Get Payment Method
            GetPaymentMethod()
            {
                this.commonService.GetPaymentMethod().then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.arrPaymentMethod = data.result;
                        //   this.arrPaymentMethod.splice(0, 0, { "typeId": "", "name": this.select });
                        this.model.paymentTypeId='1';
                    }
                });
            }
            
            //Get Sales Territory Setup By Id
            GetSalesTerritorySetupById(salesmanId: string) {
                this.commonService.GetSalesTerritorySetupById(salesmanId).then(data => {
                    this.salesmanName=data.result.salesmanFirstName;
                });
            }
            
            getGlAccountNumberDetail(myModal)
            {
                var accountTableRowIndex='';
                accountTableRowIndex=this.activeAccountNumber[0].id
                this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.accountDescription=data.result.accountDescription;
                    }
                    else{
                        this.accountDescription=data.btiMessage.message;
                    }
                    myModal.open();
                });
            }
            
            GetGlAccountNumberDetailById(accountTableRowIndex:string)
            {
                this.commonService.getGlAccountNumberDetailById(accountTableRowIndex).then(data => {
                    if(data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST')
                    {
                        this.accountDescription=data.result.accountDescription;
                    }
                    else if(data.btiMessage.messageShort == 'MAIN_ACCOUNT_TRANSACTION_NOT_ALLOWED')
                    {
                        this.activeAccountNumber = [{'id':'0','text':this.select}];
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.PopUphasMsg = true;
                        this.PopUpmessageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.PopUphasMsg = false;
                        }, 2000);
                    }
                    else{
                        this.accountDescription=data.btiMessage.message;
                    }
                });
            }
            validateAmount(amountType:string,thisevt)
            {
                
                if(amountType=='debitAmount' && thisevt.debitAmount==''){
                    this.debitAmount=0;
                    //    this.isDebitAllowed=false;
                    //    this.isCreditAllowed=true;
                }
                else if(amountType=='creditAmount' && thisevt.creditAmount==''){
                    this.creditAmount=0;
                    // this.isCreditAllowed=false;
                    // this.isDebitAllowed=true;
                }
                if(amountType=='debitAmount')
                {
                    if(this.debitAmount > 0)
                    {
                        this.creditAmount=0;
                    }
                }
                else if(amountType=='creditAmount')
                {
                    if(this.creditAmount > 0)
                    {
                        this.debitAmount=0;
                    }
                }
                
            }
            
            ChangeDistributionAccountType(evt)
            {
                
                var selectedIndex = evt.target.selectedIndex
                this.distributionAccountTypeCode=evt.target[selectedIndex].text
                
            }
            
            AddDistributionDetails()
            {
                this.IsAddDistributionDetails=true;
                if((this.creditAmount || this.debitAmount) && this.activeAccountNumber[0].id !='0' && this.distributionDescription && this.distributionAccountType)
                {
                    if(this.creditAmount)
                    {
                        this.isCredit=true;
                    }
                    else{
                        this.isCredit=false;
                    }
                    
                    this.arrARDistributionEntry.listDtoARDistributionDetail.push(
                        {
                            "accountNumber":this.activeAccountNumber[0].text,
                            "accountDescription":this.accountDescription,
                            "type":this.distributionAccountTypeCode,
                            "typeId":this.distributionAccountType,
                            "distributionReference":this.distributionDescription,
                            "debitAmount":this.debitAmount,
                            "creditAmount":this.creditAmount,
                            "accountTableRowIndex":this.activeAccountNumber[0].id
                        }
                    );
                    this.IsAddDistributionDetails=false;
                    this.bindSelectedItem(this.emptyArray[0],'AccountNumber');
                    if(!this.debitAmount)
                    {
                        this.debitAmount = 0;
                    }
                    if(!this.creditAmount)
                    {
                        this.creditAmount = 0;
                    }
                    
                    // this.amtDifference=parseInt(this.model.totalJournalEntryDebit.toString()) - parseInt(this.model.totalJournalEntryCredit.toString())
                    this.debitAmount=0;
                    this.creditAmount=0;
                    this.distributionDescription='';
                    this.distributionAccountType='';
                    
                }
                else{
                    // alert(Constants.msgText);
                }
                
            }
            
            RemoveDistributionDetails(RowIndex)
            {
                
                this.arrARDistributionEntry.listDtoARDistributionDetail.splice(RowIndex, 1);
                if(this.arrARDistributionEntry.listDtoARDistributionDetail.length == 0)
                {
                    this.IsAddDistributionDetails=true;
                }
            }
            
            GetVatDetailSetupById(vatScheduleId)
            {
                this.commonService.GetVatDetailSetupById(vatScheduleId).then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.vatPercent = data.result.basperct;            
                        this.calculateTotalAmount('');
                    }
                });
            }
            
            public bindSelectedItem(value:any,type:string):void {
                
                if(type == 'Customer')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCustomer=[value];
                        this.model.customerID=this.activeCustomer[0].id;
                        
                        if(this.model.customerID != "0")
                        {
                            this.GetCustomerByCustomerId(this.model.customerID);
                        }
                        else{
                            this.customerName='';
                            this.tradeDiscountPercent=0;
                            this.miscVatPercent=0;
                            this.freightVatPercent=0;
                            this.vatPercent=0;
                            this.model.arTransactionTradeDiscount = 0;
                            this.model.arTransactionMiscellaneous = 0;
                            this.model.arTransactionFreightAmount = 0;
                            this.model.arTransactionTotalAmount = 0;
                        }
                    }
                }
                else if(type == 'Salesman')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeSalesman=[value];
                        this.model.salesmanID=this.activeSalesman[0].id;
                        
                        if(this.model.salesmanID != "0")
                        {
                            this.GetSalesmanMaintenanceById(this.model.salesmanID);
                        }
                        else{
                            this.salesmanName='';
                        }
                    }
                }
                else if(type == 'SalesTerritory')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeSalesTerritory=[value];
                        this.model.territoryId =this.activeSalesTerritory[0].id;
                    }
                }
                else if(type == 'Batch')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeBatch=[value];
                    }
                    this.model.batchID=this.activeBatch[0].id;
                }
                else if(type == 'Currency')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCurrency=[value];
                        this.model.currencyID=this.activeCurrency[0].id;
                        this.activeExchangeDetail= [this.emptyArray[0]];
                        this.arrExchangeDetail=[];
                        this.model.exchangeTableRate = '';
                        this.model.exchangeExpirationDate = '';
                        if(value.id != "0")
                        {
                            this.formatCostAmount();
                            this.formatSaleAmount();
                            this.formatTradeAmount();
                            this.formatMiscAmount();
                            this.formatFreightAmount();
                            this.formatVatAmount();
                            this.formatTotalAmount();
                            this.formatPaymentAmount();
                            this.GetExchangeTableSetupByCurrencyId(value.id);
                        }
                        else{
                            this.model.arTransactionCost=this.unFrmtCostAmount;
                            this.model.arTransactionSalesAmount=this.unFrmtSalesAmount;
                            this.model.arTransactionMiscellaneous=this.unFrmtMiscellaneous;
                            this.model.arTransactionTradeDiscount=this.unFrmtTradeDiscount;
                        }
                    }
                }
                else if(type == 'Vat')
                {
                    
                    this.value = value;
                    if(value.text)
                    {
                        this.activeVat=[value];
                        this.model.vatScheduleID=this.activeVat[0].id;
                        this.GetVatDetailSetupById(this.model.vatScheduleID)
                    }
                }
                else if(type == 'PaymentTerm')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activePaymentTerm=[value];
                        this.model.paymentTermsID=this.activePaymentTerm[0].id;
                    }
                }
                else if(type == 'ShippingMethod')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeShippingMethod=[value];
                        this.model.shippingMethodID=this.activeShippingMethod[0].id;
                    }
                }
                else if(type == 'Checkbook')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCheckbook=[value];
                        this.model.checkbookID=this.activeCheckbook[0].id;
                    }
                }
                else if(type == 'CreditCard')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeCreditCard=[value];
                        this.model.creditCardID=this.activeCreditCard[0].id;
                    }
                }
                else if(type == 'ExchangeDetail')
                {
                    
                    this.value = value;
                    // this.model.exchangeTableIndex=this.activeExchangeDetail[0].id;
                    // if(value.id)
                    // {
                    //     this.activeExchangeDetail=[value];
                    //     if(this.activeExchangeDetail[0].id != "0")
                    //     {
                    //         this.GetCurrencyExchangeDetail(value.id);
                    //     }
                    //     else{
                    //         this.model.exchangeRate == '';
                    //         this.model.exchangeExpirationDate == '';
                    //     }
                    // }
                    
                    if(value.id)
                    {
                        this.model.exchangeTableIndex=value.id;
                        this.activeExchangeDetail=[value];
                        if(this.activeExchangeDetail[0].id != "0")
                        {
                            this.GetCurrencyExchangeDetail(value.text);
                        }
                        else{
                            this.model.exchangeTableRate == '';
                            this.model.exchangeExpirationDate == '';
                        }
                    }
                }
                else if(type == 'TransactionNumberList')
                {
                    this.value = value;
                    if(value.text)
                    {
                        this.activeTransactionNumberList=[value];
                        this.model.arTransactionNumber=this.activeTransactionNumberList[0].id;
                        if( this.model.arTransactionNumber != '0')
                        {
                            this.GetTransactionEntryByTransactionNumber(this.model.arTransactionNumber)
                        }
                        else{
                            this.arrTransactionNumberList=[];
                            this.clear('',0);
                        }
                    }
                    
                }
                else if(type == 'AccountNumber')
                {
                    this.accountTableRow=value;
                    this.value = value;
                    if(value.text)
                    {
                        this.activeAccountNumber=[value];
                        this.GetGlAccountNumberDetailById(value.id)
                    }
                }
                
            }
            
            varifyDelete()
            {
                this.isDeleteAction=true;
                this.confirmationModalBody = this.deleteConfirmationText;
                this.isConfirmationModalOpen = true;
            }
            
            
            closeModal()
            {
                this.isDeleteAction=false;
                this.isConfirmationModalOpen = false;
            }
            
            clear(f,type:number){
                if(type == 1)
                {
                    f.resetForm();
                    this.model.arTransactionType='';
                    this.model.paymentTypeId='1';
                }
                this.isModify=false;
                this.model.arTransactionType='';
                this.model.arTransactionNumber='';
                this.bindSelectedItem(this.emptyArray[0],'CreditCard');
                this.bindSelectedItem(this.emptyArray[0],'Vat');
                this.bindSelectedItem(this.emptyArray[0],'Customer');
                this.bindSelectedItem(this.emptyArray[0],'Currency');
                this.bindSelectedItem(this.emptyArray[0],'ShippingMethod');
                this.bindSelectedItem(this.emptyArray[0],'Batch');
                this.bindSelectedItem(this.emptyArray[0],'PaymentTerm');
                this.bindSelectedItem(this.emptyArray[0],'Salesman');
                this.bindSelectedItem(this.emptyArray[0],'SalesTerritory');
                this.bindSelectedItem(this.emptyArray[0],'Checkbook');
                this.bindSelectedItem(this.emptyArray[0],'ExchangeDetail');
                var drpPaymentTypeId= <HTMLSelectElement>document.getElementById("paymentTypeId");
                drpPaymentTypeId.selectedIndex = 0;
                this.model.arTransactionDate='';
                this.model.arTransactionDescription='';
                this.model.paymentTypeId='1';
                this.model.exchangeTableRate='';
                this.model.exchangeExpirationDate='';
                //this.model.arTransactionType='';
                this.model.arTransactionCost=0;
                this.model.arTransactionSalesAmount=0;
                this.model.arTransactionTradeDiscount=0;
                this.model.arTransactionMiscellaneous=0;
                this.model.arTransactionFreightAmount=0;
                this.model.arTransactionVATAmount=0;
                this.model.arTransactionTotalAmount=0;
                this.model.paymentAmount=0;
                this.arrARDistributionEntry={
                    "customerId": "",
                    "customerName": "",
                    "currencyID": "",
                    "transactionType": "",
                    "transactionNumber": "",
                    "functionalAmount": 0,
                    "originalAmount": 0,
                    "listDtoARDistributionDetail":[]
                }
                
                this.activeTransactionNumberList=[{'id':'0','text':this.select}]
            }
            
            SaveDistributionDetail(myModal)
            {
                
                if(this.isModify)
                {
                    this.transactionEntryService.SaveDistributionDetail(this.arrARDistributionEntry).then(data => {
                        window.scrollTo(0,0);
                        var datacode = data.code;
                        if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.arrARDistributionEntry={
                                "customerId": "",
                                "customerName": "",
                                "currencyID": "",
                                "transactionType": "",
                                "transactionNumber": "",
                                "functionalAmount": 0,
                                "originalAmount": 0,
                                "listDtoARDistributionDetail":[]
                            }
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
                myModal.close();
            }
            
            print()
            {
                window.print();
            }
            
            openModel(myModal,actionType)
            {
                if(actionType == 'distributionEntry')
                {
                    if(this.model.arTransactionNumber)
                    {
                        this.IsAddDistributionDetails=false;
                        this.GetDistributionDetailByTransactionNumber(this.model.arTransactionNumber)
                    }
                }
                myModal.open();
            }
            
            openDistribution(f:NgForm,myModal,ExchangeModal)
            {
                //myModal.open();
                if(f.valid  && this.activeCustomer[0].id != '0'  
                && this.activeCurrency[0].id != '0' && this.activeSalesman[0].id != '0' && this.activeSalesTerritory[0].id != '0' 
                && this.activePaymentTerm[0].id != '0'  && this.activeShippingMethod[0].id != '0' && this.activeVat[0].id != '0' 
                && this.activeCheckbook[0].id != '0' && this.activeExchangeDetail[0].id != '0')
                {
                    
                    if(this.model.arTransactionDate.formatted == undefined)
                    {
                        var transactionDate = this.model.arTransactionDate;
                        if(transactionDate.date != undefined)
                        {
                            this.model.arTransactionDate = transactionDate.date.day +'/'+ transactionDate.date.month +'/'+ transactionDate.date.year;
                        }
                    }
                    else
                    {
                        this.model.arTransactionDate = this.model.arTransactionDate.formatted;
                    }
                    
                    
                    var checkbookID =this.model.checkbookID 
                    if(!this.model.checkbookID)
                    {
                        checkbookID=0;
                    }
                    var checkNumber =this.model.checkNumber 
                    if(!this.model.checkNumber)
                    {
                        checkNumber=0;
                    }
                    
                    var cashReceiptNumber =this.model.cashReceiptNumber 
                    if(!this.model.cashReceiptNumber)
                    {
                        cashReceiptNumber=0;
                    }
                    var creditCardID =this.model.creditCardID 
                    if(!this.model.creditCardID)
                    {
                        creditCardID=0;
                    }
                    
                    var creditCardNumber =this.model.creditCardNumber 
                    if(!this.model.creditCardNumber)
                    {
                        creditCardNumber=0;
                    }
                    
                    var creditCardExpireYear =this.model.creditCardExpireYear 
                    if(!this.model.creditCardExpireYear)
                    {
                        creditCardExpireYear=0;
                    }
                    
                    var creditCardExpireMonth =this.model.creditCardExpireMonth 
                    if(!this.model.creditCardExpireMonth)
                    {
                        creditCardExpireMonth=0;
                    }
                    
                    var arTransactionStatus =this.model.arTransactionStatus 
                    if(!this.model.arTransactionStatus)
                    {
                        arTransactionStatus=0;
                    }
                    
                    var customerNameArabic =this.model.customerNameArabic 
                    if(!this.model.customerNameArabic)
                    {
                        customerNameArabic='';
                    }
                    
                    var isPayment=true;
                    if(this.model.arTransactionTotalAmount == 0)
                    {
                        isPayment=false;
                    }
                    
                    var exchangeTableIndex= '0';
                    if(this.activeExchangeDetail[0].id != '0')
                    {
                        exchangeTableIndex = this.activeExchangeDetail[0].id
                    }
                    
                    
                    var arTransactionDebitMemoAmount="0"; // DR
                    var arTransactionFinanceChargeAmount="0"; // FIN
                    var arTransactionWarrantyAmount="0"; // WRN
                    var arTransactionCreditMemoAmount="0"; // CR
                    var arTransactionCashAmount="0"; 
                    var arTransactionCheckAmount="0"; 
                    var arTransactionCreditCardAmount="0"; // NA
                    var arServiceRepairAmount="0"; // SVC
                    var arReturnAmount="0"; // RTN
                    var arTransactionSalesAmount="0"; // SLS
                    
                    this.model.arTransactionCost=this.unFrmtCostAmount;
                    this.model.arTransactionSalesAmount=this.unFrmtSalesAmount;
                    this.model.arTransactionMiscellaneous=this.unFrmtMiscellaneous;
                    this.model.arTransactionTradeDiscount=this.unFrmtTradeDiscount;
                    this.model.arTransactionFreightAmount=this.unFrmtFreightAmount;
                    this.model.arTransactionTotalAmount=this.unFrmtTotalAmount;
                    this.model.arTransactionVATAmount=this.unFrmtVATAmount;
                    this.model.paymentAmount=this.unFrmtPaymentAmount;
                    
                    var transactionType= this.model.arTransactionNumber.split('-');
                    if(transactionType[0]== 'FIN')
                    {
                        arTransactionFinanceChargeAmount=this.model.arTransactionSalesAmount;
                        arTransactionDebitMemoAmount="0";
                        arTransactionWarrantyAmount="0";
                        arTransactionCreditMemoAmount="0";
                        arTransactionCashAmount="0";
                        arTransactionCheckAmount="0";
                        arTransactionCreditCardAmount="0";
                        arServiceRepairAmount="0";
                        arReturnAmount="0";
                        arTransactionSalesAmount="0";
                    }
                    else if(transactionType[0]== 'WRN')
                    {
                        arTransactionFinanceChargeAmount="0";
                        arTransactionDebitMemoAmount="0";
                        arTransactionWarrantyAmount=this.model.arTransactionSalesAmount;
                        arTransactionCreditMemoAmount="0";
                        arTransactionCashAmount="0";
                        arTransactionCheckAmount="0";
                        arTransactionCreditCardAmount="0";
                        arServiceRepairAmount="0";
                        arReturnAmount="0";
                        arTransactionSalesAmount="0";
                    }
                    else if(transactionType[0]== 'CR')
                    {
                        arTransactionFinanceChargeAmount="0";
                        arTransactionDebitMemoAmount="0";
                        arTransactionWarrantyAmount="0";
                        arTransactionCreditMemoAmount=this.model.arTransactionSalesAmount;
                        arTransactionCashAmount="0";
                        arTransactionCheckAmount="0";
                        arTransactionCreditCardAmount="0";
                        arServiceRepairAmount="0";
                        arReturnAmount="0";
                        arTransactionSalesAmount="0";
                    }
                    else if(transactionType[0]== 'RTN')
                    {
                        arTransactionFinanceChargeAmount="0";
                        arTransactionDebitMemoAmount="0";
                        arReturnAmount=this.model.arTransactionSalesAmount;
                        arTransactionCreditMemoAmount="0";
                        arTransactionCashAmount="0";
                        arTransactionCheckAmount="0";
                        arTransactionCreditCardAmount="0";
                        arServiceRepairAmount="0";
                        arTransactionWarrantyAmount="0";
                        arTransactionSalesAmount="0";
                    }
                    
                    else if(transactionType[0]== 'DR')
                    {
                        arTransactionFinanceChargeAmount="0";
                        arTransactionDebitMemoAmount=this.model.arTransactionSalesAmount;
                        arReturnAmount="0";
                        arTransactionCreditMemoAmount="0";
                        arTransactionCashAmount="0";
                        arTransactionCheckAmount="0";
                        arTransactionCreditCardAmount="0";
                        arServiceRepairAmount="0";
                        arTransactionWarrantyAmount="0";
                        arTransactionSalesAmount="0";
                    }
                    else if(transactionType[0]== 'SVC')
                    {
                        arTransactionFinanceChargeAmount="0";
                        arServiceRepairAmount=this.model.arTransactionSalesAmount;
                        arReturnAmount="0";
                        arTransactionCreditMemoAmount="0";
                        arTransactionCashAmount="0";
                        arTransactionCheckAmount="0";
                        arTransactionCreditCardAmount="0";
                        arTransactionDebitMemoAmount="0";
                        arTransactionWarrantyAmount="0";
                        arTransactionSalesAmount="0";
                    }
                    else if(transactionType[0]== 'SLS')
                    {
                        arTransactionFinanceChargeAmount="0";
                        arTransactionSalesAmount=this.model.arTransactionSalesAmount;
                        arReturnAmount="0";
                        arTransactionCreditMemoAmount="0";
                        arTransactionCashAmount="0";
                        arTransactionCheckAmount="0";
                        arTransactionCreditCardAmount="0";
                        arTransactionDebitMemoAmount="0";
                        arTransactionWarrantyAmount="0";
                        arServiceRepairAmount="0";
                    }
                    
                    this.distributionRequestData={
                        "arTransactionNumber": this.model.arTransactionNumber,
                        "arTransactionType": this.model.arTransactionType,
                        "batchID": this.model.batchID,
                        "arTransactionDescription":this.model.arTransactionDescription,
                        "arTransactionDate": this.model.arTransactionDate,
                        "customerID": this.model.customerID,
                        "customerName":this.customerName,
                        "customerNameArabic": customerNameArabic,
                        "currencyID": this.model.currencyID,
                        "paymentTermsID": this.model.paymentTermsID,
                        "arTransactionCost": this.model.arTransactionCost,
                        "arTransactionSalesAmount": arTransactionSalesAmount,
                        "arTransactionTradeDiscount": this.model.arTransactionTradeDiscount,
                        "arTransactionFreightAmount": this.model.arTransactionFreightAmount,
                        "arTransactionMiscellaneous": this.model.arTransactionMiscellaneous,
                        "arTransactionVATAmount": this.model.arTransactionVATAmount,
                        "arTransactionDebitMemoAmount": arTransactionDebitMemoAmount,
                        "arTransactionFinanceChargeAmount": arTransactionFinanceChargeAmount,
                        "arTransactionWarrantyAmount": arTransactionWarrantyAmount,
                        "arTransactionCreditMemoAmount": arTransactionCreditMemoAmount,
                        "arTransactionTotalAmount": this.model.arTransactionTotalAmount,
                        "arTransactionCashAmount":0,
                        "arTransactionCheckAmount": arTransactionCheckAmount,
                        "arTransactionCreditCardAmount":arTransactionCreditCardAmount,
                        "salesmanID": this.model.salesmanID,
                        "checkbookID":checkbookID,
                        "checkNumber": checkNumber,
                        "cashReceiptNumber":cashReceiptNumber,
                        "creditCardID":creditCardID,
                        "creditCardNumber":creditCardNumber,
                        "creditCardExpireYear": creditCardExpireYear,
                        "creditCardExpireMonth":creditCardExpireMonth,
                        "arTransactionStatus": arTransactionStatus,
                        "exchangeTableIndex":exchangeTableIndex,
                        "exchangeTableRate":this.model.exchangeTableRate,
                        "transactionVoid":0,
                        "shippingMethodID":this.model.shippingMethodID,
                        "vatScheduleID":this.model.vatScheduleID,
                        "paymentTypeId":this.model.paymentTypeId,
                        "territoryId":this.model.territoryId,
                        "isPayment":isPayment,
                        "paymentAmount":this.model.paymentAmount,
                        "arServiceRepairAmount":this.model.arServiceRepairAmount,
                        "arReturnAmount":this.model.arReturnAmount,
                        "paymentTransactionTypeId": this.paymentTransactionTypeId,
                        "buttonType":'NEW_DISTRIBUTION'
                    };
                    if(this.activeCreditCard[0].id == '0')
                    {
                        this.model.creditCardID = '';
                    }
                    
                    if(this.model.arTransactionNumber)
                    {
                        this.IsAddDistributionDetails=false;
                        this.GetDistributionDetailByTransaction(this.distributionRequestData);
                        this.formatCostAmount();
                        this.formatSaleAmount();
                        this.formatTradeAmount();
                        this.formatMiscAmount();
                        this.formatFreightAmount();
                        this.formatVatAmount();
                        this.formatTotalAmount();
                        this.formatPaymentAmount();
                    }
                    myModal.open();
                }
                else{
                    
                    if(this.activeExchangeDetail[0].id == '0')
                    {
                        ExchangeModal.open();
                    }
                }
                
            }
            
            onlyDecimalNumberKey(event) {
                return this.getScreenDetailService.onlyDecimalNumberKey(event);
            }
            
            //Change To Original Value of receiptAmount
            formatAmountChanged(Type:string){
              //  this.commonService.changeInputNumber("paymentAmount")
                if(Type == 'Cost')
                {
                    this.commonService.changeInputNumber("arTransactionCost")
                    this.model.arTransactionCost = this.unFrmtCostAmount
                }
                else if(Type == 'SalesAmount')
                {
                    this.commonService.changeInputNumber("arTransactionSalesAmount")
                    this.model.arTransactionSalesAmount = this.unFrmtSalesAmount
                }
                else if(Type == 'FreightAmount')
                {
                    this.commonService.changeInputNumber("arTransactionFreightAmount")
                    this.model.arTransactionFreightAmount = this.unFrmtFreightAmount
                }
                else if(Type == 'TradeDiscount')
                {
                    this.commonService.changeInputNumber("arTransactionTradeDiscount")
                    this.model.arTransactionTradeDiscount = this.unFrmtTradeDiscount
                }
                else if(Type == 'Miscellaneous')
                {
                    this.commonService.changeInputNumber("arTransactionMiscellaneous")
                    this.model.arTransactionMiscellaneous = this.unFrmtMiscellaneous
                }
                else if(Type == 'VATAmount')
                {
                    this.commonService.changeInputNumber("arTransactionVATAmount")
                    this.model.arTransactionVATAmount = this.unFrmtVATAmount
                }
                //  else if(Type == 'TotalAmount')
                //  {
                //     this.model.arTransactionTotalAmount = this.unFrmtTotalAmount
                //  }
                else if(Type == 'PaymentAmount')
                {
                    this.commonService.changeInputNumber("paymentAmount")
                    this.model.paymentAmount = this.unFrmtPaymentAmount
                }
                // this.commonService.changeInputNumber("arTransactionCost") ;
                
            }
            // get Original receiptAmount
            getAmountValue(Type:string,event){
                if(event && event.indexOf('.') > -1)
                {
                var amt = event.split(".")
                if(amt.length == 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)
                }
                else{
                    if(amt.length > 1 && amt[1].length >= 3){
                        event = amt[0]+"."+amt[1].slice(0, 3)
                    }
                    if(amt.length > 1 && amt[0].length >= 6){
                        event = amt[0].slice(0, 6)+"."+amt[1]
                    }
                }
                }
               
                if(Type == 'Cost')
                {
                    this.unFrmtCostAmount = event
                }
                else if(Type == 'SalesAmount')
                {
                    this.unFrmtSalesAmount = event
                }
                else if(Type == 'TradeDiscount')
                {
                    this.unFrmtTradeDiscount = event
                }
                else if(Type == 'Miscellaneous')
                {
                    this.unFrmtMiscellaneous = event
                }
                else if(Type == 'FreightAmount')
                {
                    this.unFrmtFreightAmount = event
                }
                else if(Type == 'VATAmount')
                {
                    this.unFrmtVATAmount = event
                }
                else if(Type == 'TotalAmount')
                {
                    this.unFrmtTotalAmount = event;
                }
                else if(Type == 'PaymentAmount')
                {
                    this.unFrmtPaymentAmount = event;
                    if(this.model.arTransactionTotalAmount < event){
                        
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = "Total Amount should be greater than Payment Amount";
                        window.scrollTo(0,0);
                        this.model.paymentAmount='';
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        window.scrollTo(0,0);
                    }
                }
            }
            
            formatCostAmount(){
                if(this.unFrmtCostAmount || this.unFrmtCostAmount == 0)
                {
                    this.commonService.changeInputText("arTransactionCost")
                    var inputValue = this.unFrmtCostAmount;
                    this.model.arTransactionCost = this.unFrmtCostAmount;
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.arTransactionCost = this.commonService.formatAmount(data, inputValue)
                            
                        });
                    }else{
                        this.formatAmountChanged('Cost')
                    }
                }
            }
            
            formatSaleAmount(){
                if(this.unFrmtSalesAmount || this.unFrmtSalesAmount == 0)
                {
                    this.commonService.changeInputText("arTransactionSalesAmount")
                    var inputValue = this.unFrmtSalesAmount;
                    
                    this.model.arTransactionSalesAmount = this.unFrmtSalesAmount;
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.arTransactionSalesAmount = this.commonService.formatAmount(data, inputValue)
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                    }else{
                        this.formatAmountChanged('SalesAmount')
                    }
                }
            }
            
            formatTradeAmount(){
                if(this.unFrmtTradeDiscount || this.unFrmtTradeDiscount == 0)
                {
                    this.commonService.changeInputText("arTransactionTradeDiscount")
                    var inputValue = this.unFrmtTradeDiscount;
                    
                    this.model.arTransactionTradeDiscount = this.unFrmtTradeDiscount;
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.arTransactionTradeDiscount = this.commonService.formatAmount(data, inputValue)
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                    }else{
                        this.formatAmountChanged('TradeDiscount')
                    }
                }
            }
            
            formatMiscAmount(){
                if(this.unFrmtMiscellaneous || this.unFrmtMiscellaneous == 0)
                {
                    this.commonService.changeInputText("arTransactionMiscellaneous")
                    var inputValue = this.unFrmtMiscellaneous;
                    
                    this.model.arTransactionMiscellaneous = this.unFrmtMiscellaneous;
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.arTransactionMiscellaneous = this.commonService.formatAmount(data, inputValue)
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                    }else{
                        this.formatAmountChanged('Miscellaneous')
                    }
                }
            }
            
            formatFreightAmount(){
                
                if(this.unFrmtFreightAmount || this.unFrmtFreightAmount == 0)
                {
                    this.commonService.changeInputText("arTransactionFreightAmount")
                    var inputValue = this.unFrmtFreightAmount;
                    
                    this.model.arTransactionFreightAmount = this.unFrmtFreightAmount;
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.arTransactionFreightAmount = this.commonService.formatAmount(data, inputValue)
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                    }else{
                        this.formatAmountChanged('FreightAmount')
                    }
                }
            }
            
            formatVatAmount(){
                
                if(this.unFrmtVATAmount || this.unFrmtVATAmount == 0)
                {
                    this.commonService.changeInputText("arTransactionVATAmount")
                    var inputValue = this.unFrmtVATAmount;
                    
                    this.model.arTransactionVATAmount = this.unFrmtVATAmount;
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.arTransactionVATAmount = this.commonService.formatAmount(data, inputValue)
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                    }else{
                        this.formatAmountChanged('VATAmount')
                    }
                }
            }
            
            formatTotalAmount(){
                if(this.unFrmtTotalAmount || this.unFrmtTotalAmount == 0)
                {
                    this.commonService.changeInputText("arTransactionTotalAmount")
                    var inputValue = this.unFrmtTotalAmount;
                    
                    this.model.arTransactionTotalAmount = this.unFrmtTotalAmount;
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.arTransactionTotalAmount = this.commonService.formatAmount(data, inputValue)
                            
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                    }else{
                        this.formatAmountChanged('TotalAmount')
                    }
                }
            }
            
            formatPaymentAmount(){
                if(this.unFrmtPaymentAmount || this.unFrmtPaymentAmount == 0)
                {
                    var unFrmtPaymentAmount = this.unFrmtPaymentAmount;
                    var arTransactionTotalAmount = this.unFrmtTotalAmount;
                    this.commonService.changeInputText("paymentAmount")
                    var inputValue = this.unFrmtPaymentAmount;
                    this.model.paymentAmount = this.unFrmtPaymentAmount;
                    if(parseFloat(unFrmtPaymentAmount)>arTransactionTotalAmount){
                        this.model.paymentAmount = arTransactionTotalAmount;
                        inputValue = arTransactionTotalAmount;
                        this.unFrmtPaymentAmount = arTransactionTotalAmount;
                    }
                    
                    if (this.activeCurrency[0].id !='0' ){
                        let submitInfo = {
                            'currencyId':this.activeCurrency[0].text
                        };
                        this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                            this.model.paymentAmount = this.commonService.formatAmount(data, inputValue)
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                            }, 100)
                        });
                    }else{
                        this.formatAmountChanged('PaymentAmount')
                    }
                }
            }
            
            checkMonthValue(){
                if(this.model.creditCardExpireMonth<=0||this.model.creditCardExpireMonth>12){
                    this.model.creditCardExpireMonth='';
                }
            }
            
        }