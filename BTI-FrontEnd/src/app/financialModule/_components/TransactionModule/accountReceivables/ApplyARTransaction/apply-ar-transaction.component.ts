import { Component, ViewChild,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { ApplyARTransactionService } from '../../../../_services/transactionModule/accountReceivables/applyArTransaction.service';
import { Constants } from '../../../../../_sharedresource/Constants';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';  

@Component({
    templateUrl: './apply-ar-transaction.component.html',
    providers:[ApplyARTransactionService,CommonService]
})

//export to make it available for other classes
export class ApplyArTransactionComponent {
    screenCode;
    screenName;
    moduleName;
    moduleCode = Constants.financialModuleCode;
    messageText;
    defaultAddFormValues: Array<object>;
    availableFormValues: [object];
    hasMsg = false;
    showMsg = false;
    select=Constants.select;
    isScreenLock;
    private value:any = {};
    emptyArray=[{'id':'0','text':this.select}];    
    activeCustomer:any=this.emptyArray;
    activePaymentNumber:any=this.emptyArray;
    customerName:string;
    arrCustomer=[];
    arrDocumentType=[];
    arrPaymentNumber=[];
    isSuccessMsg;
    isfailureMsg;
    startDate;
    endDate;
    documentType:string;
    applyTxnIndex=[];
    private myOptions1: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy'
    };
	private myOptions2: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy'
    };
    
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
    
    
    private  Object = { date: { year: 2018, month: 10, day: 9 } };
    
    onDateChanged(event: IMyDateModel): void {
        
    }
    ModelApplyTransaction:any={
        "paymentNumber":"",
        "postingDate":"",
        "applyDate":"",
        "functionalAmount":"",
        "originalAmount":"",
        "customerID":"",
        "arYTDOpenTransactionsList":
        [
        ]
    }
    
    tempOpenTransactionsList={'Total':0,'originalAmount':0,'tempArray':[]};
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private commonService: CommonService,
        public toastr: ToastsManager,
        vcr: ViewContainerRef,
        private applyARTransactionService: ApplyARTransactionService)
        {
            this.toastr.setRootViewContainerRef(vcr);
        }
        
        ngOnInit() 
        {
            //  this.startDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
            //  this.endDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
            this.getAddScreenDetail();
            this.GetCustomerList();
            this.GetDocumentType();
        }
        
        getAddScreenDetail()
        {
            this.screenCode = "S-1252";
            this.defaultAddFormValues = [
                { 'fieldName': 'APPLY_AR_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_CUSTOMER_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                { 'fieldName': 'APPLY_AR_DOC_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_DOC_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_APPLY_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                { 'fieldName': 'APPLY_AR_POSTING_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_FUNC_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_ORIGINAL_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_REDISPLAY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_SELECT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_APPLY_TO_DOC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_DUE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_AMOUNT_REMAINING', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_ORI_TRANS_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_APPLY_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_APPLY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_UNAPPLY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                { 'fieldName': 'APPLY_AR_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                
            ];
            this.getScreenDetail(this.screenCode);
        }
        getScreenDetail(screenCode)
        {
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
                this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                    this.screenName=data.result.dtoScreenDetail.screenName;
                    this.moduleName=data.result.moduleName;
                    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                    
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                });
            }
            // Get Customer List
            GetCustomerList()
            {
                this.commonService.GetCustomerList().then(data => {
                    this.arrCustomer=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.records.length;i++)	
                        {
                            this.arrCustomer.push({'id':data.result.records[i].customerId,'text':data.result.records[i].customerId});
                        }
                    }
                });
            }
            
            // Get Document Type List
            GetDocumentType()
            {
                this.applyARTransactionService.GetDocumentType().then(data => {
                    this.arrDocumentType=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.arrDocumentType = data.result;
                        this.arrDocumentType.splice(0, 0, { "typeId": "", "name": this.select });
                        this.documentType='';
                    }
                });
            }
            // Get DocumentNumber By Customer And DocType
            GetDocumentNumberByCustomerAndDocType(evt)
            {
                
                this.applyARTransactionService.GetDocumentNumberByCustomerAndDocType(this.ModelApplyTransaction.customerID,evt.target.value).then(data => {
                    
                    this.arrPaymentNumber=[{'id':'0','text':this.select}];
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        for(var i=0;i<data.result.length;i++)	
                        {
                            this.arrPaymentNumber.push({'id':data.result[i].paymentNumber,'text':data.result[i].paymentNumber});
                        }
                    }
                });
            }
            
            GetOpenTransactionsByCustomerId()
            {
                
                this.ModelApplyTransaction.arYTDOpenTransactionsList=[];          
                this.applyARTransactionService.GetOpenTransactionsByCustomerId(this.ModelApplyTransaction.customerID).then(data => {
                    if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                    {
                        this.tempOpenTransactionsList.tempArray = [];
                        this.ModelApplyTransaction.arYTDOpenTransactionsList = data.result.records;
                        
                        for(var i=0;i<data.result.records.length;i++)
                        {
                            var Total = this.tempOpenTransactionsList.Total;
                            this.tempOpenTransactionsList.Total = Total + data.result.records[i].applyAmount;
                            this.tempOpenTransactionsList.tempArray.push(data.result.records[i].applyAmount);
                        }
                        
                    }
                });
            }
            
            GetOpenTransactionsByPostingDate(f:NgForm)
            {
                if(f.valid)
                {
                    if(this.ModelApplyTransaction.postingDate.formatted == undefined)
                    {
                        var postingDateData = this.ModelApplyTransaction.postingDate;
                        if(postingDateData.date != undefined)
                        {
                            this.ModelApplyTransaction.postingDate = postingDateData.date.day +'/'+ postingDateData.date.month +'/'+ postingDateData.date.year;
                        }
                    }
                    else
                    {
                        this.ModelApplyTransaction.postingDate = this.ModelApplyTransaction.postingDate.formatted;
                    }
                    this.applyARTransactionService.GetOpenTransactionsByPostingDate(this.ModelApplyTransaction.customerID,this.ModelApplyTransaction.postingDate,this.ModelApplyTransaction.paymentNumber).then(data => {
                        
                        if(data.btiMessage.messageShort != "RECORD_NOT_FOUND")
                        {
                            this.ModelApplyTransaction.arYTDOpenTransactionsList = data.result.records;
                        }
                        else{
                            this.tempOpenTransactionsList.tempArray=[];
                            this.ModelApplyTransaction.arYTDOpenTransactionsList = [];
                        }
                    });
                }
            }
            
            GetAmountByPaymentNumber()
            {
                
                this.applyARTransactionService.GetAmountByPaymentNumber(this.ModelApplyTransaction.paymentNumber).then(data => {
                    
                    this.ModelApplyTransaction.functionalAmount=data.result.functionalAmount;
                    this.ModelApplyTransaction.originalAmount=data.result.originalAmount;
                    this.tempOpenTransactionsList.originalAmount = data.result.originalAmount;
                    var postingDate = data.result.postingDate;
                    if(postingDate)
                    {
                        var postingDateObj = postingDate.split('/');
                        this.ModelApplyTransaction.postingDate = {"date":{"year":parseInt(postingDateObj[2]),"month":parseInt(postingDateObj[1]),"day":parseInt(postingDateObj[0])}};
                    }
                });
            }
            
            clearApplyDate(){
                this.ModelApplyTransaction.applyDate = '';
            }
            clearPostingDate(){
                this.ModelApplyTransaction.postingDate = '';
            }
            
            getCopyOfOptions(): INgxMyDpOptions {
                return JSON.parse(JSON.stringify(this.myOptions2));
            } 
            
            getCopyOfOption(): INgxMyDpOptions {
                return JSON.parse(JSON.stringify(this.myOptions1));
            } 
            
            GetCustomerByCustomerId(customerId)
            {
                this.commonService.GetCustomerById(customerId).then(data => {
                    
                    this.customerName=data.result.name;
                });
            }
            
            CalculateAmount(idx:number,val:any)
            {
                
                var originalAmount= this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].arTransactionTotalAmount;
                var baseApplyAmount=this.tempOpenTransactionsList.tempArray[idx];
                var txt_ApplyAmount= <HTMLInputElement>document.getElementById('applyAmount_'+idx)
                var applyAmount = 0;
                
                if(txt_ApplyAmount.value !='')
                {
                    applyAmount = parseFloat(txt_ApplyAmount.value);
                }
                if(applyAmount > 0)
                {
                    if(applyAmount < baseApplyAmount)
                    {
                        // this.toastr.warning('Apply Amount Should be greater than Old Amount');	
                        this.toastr.warning(this.defaultAddFormValues[15]['listDtoFieldValidationMessage'][2]['validationMessage']);							
                        this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].applyAmount = baseApplyAmount;
                        txt_ApplyAmount.value = baseApplyAmount;
                    }
                    else{
                        
                        var applyAmountTotal=0;
                        this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].applyAmount = applyAmount;
                        for(var i=0;i<this.ModelApplyTransaction.arYTDOpenTransactionsList.length;i++)
                        { 
                            var diffInAmount = 0;
                            diffInAmount = this.ModelApplyTransaction.arYTDOpenTransactionsList[i].applyAmount - this.tempOpenTransactionsList.tempArray[i];
                            
                            // applyAmountTotal= applyAmountTotal + this.ModelApplyTransaction.arYTDOpenTransactionsList[i].applyAmount;
                            applyAmountTotal= applyAmountTotal + diffInAmount;
                            if(applyAmountTotal > this.tempOpenTransactionsList.originalAmount)
                            {
                                this.toastr.warning(this.defaultAddFormValues[15]['listDtoFieldValidationMessage'][3]['validationMessage'] +' ' +this.tempOpenTransactionsList.originalAmount);							
                                //this.toastr.warning('Apply Amount total should be  less than ' + this.tempOpenTransactionsList.originalAmount);	
                                this.ModelApplyTransaction.arYTDOpenTransactionsList[i].applyAmount = this.tempOpenTransactionsList.tempArray[i];
                                txt_ApplyAmount.value = this.tempOpenTransactionsList.tempArray[i];
                                return false;
                            }
                        }
                        
                        var txt_RemainingAmount= <HTMLSpanElement>document.getElementById('lblremainingAmount_'+idx)
                        var remainingAmount = 0;
                        if(txt_RemainingAmount.innerHTML !='')
                        {
                            remainingAmount = parseFloat(txt_RemainingAmount.innerHTML);
                        }
                        // this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].remainingAmount = remainingAmount;
                        if(applyAmount <= originalAmount )
                        {
                            remainingAmount = originalAmount - applyAmount
                            remainingAmount = Math.round(remainingAmount * 100) / 100;
                            this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].remainingAmount = remainingAmount;
                        }
                        else{
                            this.toastr.warning(this.defaultAddFormValues[15]['listDtoFieldValidationMessage'][1]['validationMessage']);				
                            this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].applyAmount = 0;
                            txt_ApplyAmount.value = "0";
                        }
                    }
                }
                
            }
            
            updateTransactionStatus(evt,idx)
            {
                var txnNumber=this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].arTransactionNumber
                var chkIndex = this.applyTxnIndex.indexOf(txnNumber);
                if(evt.target.checked)
                {
                    this.applyTxnIndex.push(txnNumber)
                }          
                else{
                    this.applyTxnIndex.splice(txnNumber,1);
                }
                //this.ModelApplyTransaction.arYTDOpenTransactionsList[idx].applyAmount
            }
            
            UnApplyTransaction(f:NgForm)
            {
                
                var tempArYTDOpenTransactionsList=[];
                for(var i=0;i<this.applyTxnIndex.length;i++)
                {
                    tempArYTDOpenTransactionsList.push({'arTransactionNumber':this.applyTxnIndex[i]})
                }
                var requestData ={
                    "paymentNumber":this.ModelApplyTransaction.paymentNumber,
                    "customerID":this.ModelApplyTransaction.customerID,
                    "arYTDOpenTransactionsList":tempArYTDOpenTransactionsList
                }
                this.applyARTransactionService.UnApplyTransaction(requestData).then(data => {
                    window.scrollTo(0,0);
                    
                    var datacode = data.code;
                    if (data.btiMessage.messageShort == 'RECORD_UPDATED_SUCCESSFULLY') {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        f.resetForm();
                        this.ResetForm();
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
                
            }
            
            
            ApplyTransaction(f:NgForm)
            {
                
                if(f.valid)
                {
                    var tempArYTDOpenTransactionsList=[];
                    for(var i=0;i<this.applyTxnIndex.length;i++)
                    {
                        for(var j=0;j< this.ModelApplyTransaction.arYTDOpenTransactionsList.length;j++)
                        {
                            if(this.ModelApplyTransaction.arYTDOpenTransactionsList[j].arTransactionNumber == this.applyTxnIndex[i])
                            {
                                tempArYTDOpenTransactionsList.push(this.ModelApplyTransaction.arYTDOpenTransactionsList[j])
                                break;
                            }
                        }
                        
                    }
                    
                    if(this.ModelApplyTransaction.postingDate.formatted == undefined)
                    {
                        var postingDateData = this.ModelApplyTransaction.postingDate;
                        if(postingDateData.date != undefined)
                        {
                            this.ModelApplyTransaction.postingDate = postingDateData.date.day +'/'+ postingDateData.date.month +'/'+ postingDateData.date.year;
                        }
                    }
                    else
                    {
                        this.ModelApplyTransaction.postingDate = this.ModelApplyTransaction.postingDate.formatted;
                    }
                    
                    if(this.ModelApplyTransaction.applyDate.formatted == undefined)
                    {
                        var applyDateData = this.ModelApplyTransaction.applyDate;
                        if(applyDateData.date != undefined)
                        {
                            this.ModelApplyTransaction.applyDate = applyDateData.date.day +'/'+ applyDateData.date.month +'/'+ applyDateData.date.year;
                        }
                    }
                    else
                    {
                        this.ModelApplyTransaction.applyDate = this.ModelApplyTransaction.applyDate.formatted;
                    }
                    
                    var requestData ={
                        "paymentNumber":this.ModelApplyTransaction.paymentNumber,
                        "postingDate":this.ModelApplyTransaction.postingDate,
                        "applyDate": this.ModelApplyTransaction.applyDate,
                        "functionalAmount":this.ModelApplyTransaction.functionalAmount,
                        "originalAmount":this.ModelApplyTransaction.originalAmount,
                        "customerID":this.ModelApplyTransaction.customerID,
                        "arYTDOpenTransactionsList":tempArYTDOpenTransactionsList
                    }
                    
                    
                    
                    this.applyARTransactionService.ApplyTransaction(requestData).then(data => {
                        window.scrollTo(0,0);
                        
                        var datacode = data.code;
                        if (data.btiMessage.messageShort == 'RECORD_SAVE_SUCCESSFULLY' || data.btiMessage.messageShort =='RECORD_UPDATED_SUCCESSFULLY') {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            // this.applyTxnIndex=[];
                            // this.ModelApplyTransaction={
                            //     "paymentNumber":"",
                            //      "postingDate":"",
                            //      "applyDate":"",
                            //      "functionalAmount":"",
                            //      "originalAmount":"",
                            //      "customerID":"",
                            //      "arYTDOpenTransactionsList":
                            //      [
                            //      ]
                            // };
                            // this.tempOpenTransactionsList={'Total':0,'originalAmount':0,'tempArray':[]};
                            // f.resetForm();
                            //this.bindSelectedItem(this.emptyArray[0],'Customer');
                            //this.bindSelectedItem(this.emptyArray[0],'PaymentNumber');
                            f.resetForm();
                            this.ResetForm();
                            
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
                
            }
            
            ResetForm()
            {
                
                this.applyTxnIndex=[];
                this.ModelApplyTransaction={
                    "paymentNumber":"",
                    "postingDate":"",
                    "applyDate":"",
                    "functionalAmount":"",
                    "originalAmount":"",
                    "customerID":"",
                    "arYTDOpenTransactionsList":
                    [
                    ]
                };
                this.documentType='';
                this.tempOpenTransactionsList={'Total':0,'originalAmount':0,'tempArray':[]};
                this.ModelApplyTransaction.applyDate='';
                this.ModelApplyTransaction.postingDate='';
                this.bindSelectedItem(this.emptyArray[0],'Customer');
                this.bindSelectedItem(this.emptyArray[0],'PaymentNumber');
                //f.resetForm();
            }
            
            public bindSelectedItem(value:any,type:string):void {
                
                if(type == 'Customer')
                {
                    this.value = value;
                    this.activeCustomer=[value];
                    if(value.text)
                    {
                        this.ModelApplyTransaction.customerID=this.activeCustomer[0].id;
                        this.activePaymentNumber=[{'id':'0','text':this.select}];
                        this.arrPaymentNumber=[];
                        this.documentType = '';
                        if(this.ModelApplyTransaction.customerID != "0")
                        {
                            this.GetCustomerByCustomerId(this.ModelApplyTransaction.customerID);
                            this.GetOpenTransactionsByCustomerId();
                        }
                        else{
                            this.customerName='';
                        }
                    }
                }
                else if(type == 'PaymentNumber')
                {
                    this.value = value;
                    this.activePaymentNumber=[value];
                    if(value.text)
                    {
                        this.ModelApplyTransaction.paymentNumber=this.activePaymentNumber[0].id;
                        if(this.activePaymentNumber[0].id != '0')
                        {
                            this.GetAmountByPaymentNumber();
                        }
                    }
                }
            }
            
            onlyDecimalNumberKey(event) {
                return this.getScreenDetailService.onlyDecimalNumberKey(event);
            }
        }