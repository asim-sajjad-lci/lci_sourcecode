import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { BatchSetup } from "../../../../../financialModule/_models/transactionModule/accountReceivables/batchSetup";
import { BatchSetupService } from '../../../../../financialModule/_services/transactionModule/accountReceivables/batchSetup.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

@Component({
    templateUrl: './batch-setup.component.html',
    providers:[BatchSetupService]
})

//export to make it available for other classes
export class ArBatchSetupComponent {
    page = new Page();
    rows = new Array<BatchSetup>();
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    select=Constants.select;
    emptyArray=[{'id':'0','text':this.select}];    
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    searchKeyword = '';
    selected = [];
    isApprovedCheck: number=0;
    arrTransactionType = this.emptyArray;
    messageText;
    hasMsg = false;
    showMsg = false;
    isApproved:boolean=false;
    isSuccessMsg;
    isfailureMsg;
    ddPageSize = 5;
    model: any = {};
    Searchselected = [];
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    btnCancelText=Constants.btnCancelText;
    isModify:boolean=false;
    showBtns:boolean=false;

    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    isDeleteAction : boolean = false;
    isConfirmationModalOpen:boolean=false;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    EmptyMessage = Constants.EmptyMessage;
    totalText = Constants.totalText;
    
    btndisabled:boolean=false;
    today = new Date();
    private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year: this.today.getFullYear(), month: this.today.getMonth() + 1, day: this.today.getDate()-1 }
    };

    year=this.today.getFullYear();
    private  Object = { 
         date: { year: 2018, month: 10, day: 9 }
     };
   
    activeTransactionType:any=this.emptyArray;
  
    onDateChanged(event: IMyDateModel): void {
  
    }

    @ViewChild(DatatableComponent) table: DatatableComponent;
       constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private batchSetupService: BatchSetupService,)
        {
            var userData = JSON.parse(localStorage.getItem('currentUser'));
        }

    ngOnInit() 
    {
        this.isApproved;
        this.setPage({ offset: 0 });
        this.getAllBatch({ offset: 0 });
        this.getAddScreenDetail();
        this.getViewScreenDetail();
        this.getTransactionType();
        this.isApprovedCheck;
        this.model.postingDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
    }

    getAddScreenDetail()
    {
        this.screenCode = "S-1249";
        this.defaultAddFormValues = [
            { 'fieldName': 'AR_BATCH_BATCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
            { 'fieldName': 'AR_BATCH_TRANSACTION_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_POSTING_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_TOTAL_TRANSACTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
            { 'fieldName': 'AR_BATCH_QUANITY_TOTAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_APPROVED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_USER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_APPROVED_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_DELETE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_POST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'AR_BATCH_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
           
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
        this.getScreenDetail(this.screenCode,'Add');
    }
    
    getViewScreenDetail()
    {
         this.screenCode = "S-1250";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_AR_BATCH_BATCH_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_TRANS_TYPE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_POSTING_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_TOTAL_TRANS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_QUANTITY_TOTAL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_AR_BATCH_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
              
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
      
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    getTransactionType(){
        this.batchSetupService.GetAllBatchesTransaction().then(data => {
            this.arrTransactionType=[{'id':'0','text':this.select}];
			for(var i=0;i<data.result.length;i++)	
			{
				this.arrTransactionType.push({'id':data.result[i].typeId,'text':data.result[i].transactionType});
			}
		 });
    }

    CreateBatchSetup(f: NgForm) {
        this.btndisabled=true;
        if(f.valid && this.activeTransactionType[0].id != '0')
        {
           if (this.isModify) {

            if(this.model.postingDate.formatted == undefined)
            {
               var postingDate = this.model.postingDate;
               if(postingDate.date != undefined)
               {
                  this.model.postingDate = postingDate.date.day +'/'+ postingDate.date.month +'/'+ postingDate.date.year;
               }
            }
            else
            {
                this.model.postingDate = this.model.postingDate.formatted;
            }

            this.batchSetupService.SaveUpdateBatches(this.model,this.isModify).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                 this.messageText = data.btiMessage.message;
                 
                  this.hasMsg = true;
                  this.showMsg = true;
                 if (data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST') {
                       this.isSuccessMsg = false;
                       this.isfailureMsg = true;
                 }
                 else{
                        this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showBtns=false;
                        this.getAllBatch({ offset: 0 });
                       
                 }
                  window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                    }, 4000);
                    f.resetForm();
                    this.clear();
                    this.setPage({ offset: 0 });
                this.activeTransactionType=this.emptyArray;
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        else {

            if(this.model.postingDate.formatted == undefined)
            {
               var postingDate = this.model.postingDate;
               if(postingDate.date != undefined)
               {
                  this.model.postingDate = postingDate.date.day +'/'+ postingDate.date.month +'/'+ postingDate.date.year;
               }
            }
            else
            {
                this.model.postingDate = this.model.postingDate.formatted;
            }

            this.batchSetupService.SaveUpdateBatches(this.model,this.isModify).then(data => {
                this.showBtns=false;
                this.btndisabled=false;
                window.scrollTo(0,0);
                var datacode = data.code;
                 this.hasMsg = true;
                  this.showMsg = true;
                   this.messageText = data.btiMessage.message;
                 if (data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST') {
                       this.isSuccessMsg = false;
                       this.isfailureMsg = true;
                 }
                 else{
                        this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showBtns=false;
                        this.getAllBatch({ offset: 0 });
                        f.resetForm();
                        this.clear();
                        this.setPage({ offset: 0 });
                        this.activeTransactionType=this.emptyArray;
                 }
                window.setTimeout(() => {
                     this.showMsg = false;
                     this.hasMsg = false;
                 }, 4000);
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
    }
    this.btndisabled=false;
}

PostBatchSetup(f:NgForm) 
        { 
            this.btndisabled=true;  
            if(f.valid)
            {

                if(this.model.postingDate.formatted == undefined)
                {
                    var postingDate = this.model.postingDate;
                    if(postingDate.date != undefined)
                    {
                        this.model.postingDate = postingDate.date.day +'/'+ postingDate.date.month +'/'+ postingDate.date.year;
                    }
                }
                else
                {
                    this.model.postingDate = this.model.postingDate.formatted;
                }
                this.batchSetupService.SaveUpdateBatches(this.model,this.isModify).then(data => {
                this.batchSetupService.PostBatchTransaction(this.model.batchId,this.model.transactionTypeId).then(data =>
                    {
                        this.btndisabled=false;
                        window.scrollTo(0,0);
                        var datacode = data.code;
                        if (data.btiMessage.messageShort=='RECORD_POST_SUCCESSFULLY') {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.batchSetupService.GetAllBatches(this.page,this.searchKeyword).subscribe(pagedData => {
                                    this.page = pagedData.page;
                                    this.rows = pagedData.data;
                                });
                                this.isModify=false;
                                f.resetForm();
                                this.clear();
                                window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                    })
                    .catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                    });
                })
            }
            else{
                
            }
            this.btndisabled=false;
        }

    // Delete Clearing Journal Entry
     DeleteBatches(f: NgForm)
     {
         
        if(!this.model.batchId)
        {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.defaultAddFormValues[0]['listDtoFieldValidationMessage'][0]['validationMessage'];
            window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
            }, 4000);
            
        }
        else{
            this.batchSetupService.DeleteBatches(this.model.batchId,this.model.transactionTypeId).then(data =>
            {
                    window.scrollTo(0,0);
                    this.closeModal();
                    if (data.btiMessage.messageShort="RECORD_DELETED_SUCCESSFULLY") {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.isModify=false;
                            this.getAllBatch({ offset: 0 });
                            window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                            }, 4000);
                    }
                    this.activeTransactionType=this.emptyArray;
                    f.resetForm();
                   
                 }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
        }
     }

getbatchById(row: any)
    {
        
         this.showBtns=true;
         this.isModify=true;
         window.scrollTo(0,0);
         this.batchSetupService.GetBatchesByBatchId(row.batchId,row.transactionTypeId).then(data => {
              this.model = data.result;
              
              this.activeTransactionType=[{'id':this.model.transactionTypeId,'text':this.model.transactionType}];
              this.getBatchTotalTrasactionsByTransactionType(this.model.batchId,this.model.transactionTypeId);
              var postingDate = this.model.postingDate;
              var postingDateData = postingDate.split('/');
              this.model.postingDate = {"date":{"year":parseInt(postingDateData[2]),"month":parseInt(postingDateData[1]),"day":parseInt(postingDateData[0])}};
         });
    }
    
    //setting pagination
    getAllBatch(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.batchSetupService.GetAllBatches(this.page,this.searchKeyword).subscribe(pagedData => {
             this.page = pagedData.page;
             this.rows = pagedData.data;
        });
    }

    getBatchTotalTrasactionsByTransactionType(batchId,transactionTypeId)
    {
        this.batchSetupService.GetBatchTotalTrasactionsByTransactionType(batchId,transactionTypeId).then(data => {
             //this.model = data.result;
             this.model.totalTransactions=data.result.totalTransactions;
             this.model.quantityTotal=data.result.quantityTotal;

        });
    }

    changePageSize(event) {
        this.page.size = event.target.value;
        this.getAllBatch({ offset: 0 });
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.batchSetupService.GetAllBatches(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

    private value:any = {};

    public bindSelectedItem(value:any):void {
        this.value = value;
        if(value.text)
        {
            this.model.transactionTypeId=value.id;
            this.activeTransactionType=[value];
            this.getBatchTotalTrasactionsByTransactionType(this.model.batchId,value.id);
        }
    }
    
    setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.batchSetupService.GetAllBatches(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
    
    varifyDelete()
    {
       this.isDeleteAction=true;
       this.confirmationModalBody = this.deleteConfirmationText;
       this.isConfirmationModalOpen = true;
    }


    closeModal()
    {
         this.isDeleteAction=false;
         this.isConfirmationModalOpen = false;
    }

    clear()
    {
        if(!this.isModify)
        {
            this.model.batchId='';
        }
        this.model.description='';
        this.activeTransactionType=this.emptyArray;
        this.model.postingDate = {"date":{"year":this.today.getFullYear(),"month":this.today.getMonth() + 1,"day":this.today.getDate()}};
        this.model.totalTransactions= '';
        this.model.quantityTotal= '';
    }

    cancel(f)
    {
        this.isModify=false;
        this.model.batchId= '';
        this.clear();
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
}