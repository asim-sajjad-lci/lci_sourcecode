import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountPayablesSetup } from '../../../../financialModule/_models/accounts-payables/accounts-payables-setup';
import { AccountPayablesService } from '../../../../financialModule/_services/accounts-payables-setup/accounts-payables-setup.service';
import { VatDetailSetupService } from '../../../../financialModule/_services/company-setup/vat-detail-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../_sharedresource/Constants';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';

@Component({
    selector: '<accounts-payables></accounts-payables>',
    templateUrl: './accounts-payables-setup.component.html',
    styles: ["user.component.css"],
    providers:[AccountPayablesService,VatDetailSetupService,CommonService]
})

//export to make it available for other classes
export class AccountPayablesSetupComponent {
    page = new Page();
    rows = new Array<AccountPayablesService>();
    ArrBankId = [];
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    AccountReceivableSetup = {};
    id: string;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean=false;
    ArrCheckFormatType;
    ArrApplyBy=[];
    // Serachable Select Box Step1
	ArrCheckbookId = [];
    checkbookId : any = [];
    ddlCheckbookIdSetting = {};
	// Serachable Select Box Step1 End
    exchangeDate;
	exchangeExpireDate;
	exchangeTime='';
    checkbookoptions;
    checkFormatOptions;
    ageingByOptions;
    minRange:string;
    maxRange:string;
    currentFrom:string;
    currentTo:string;
    OldMaxLimit:number;
    isEdit: boolean =false;
    agingIndex:number;
    pmPeriodSetupsList=[];
    ArrApplyByOption=[];
    ageUnappliedCreditAmounts: boolean=false;
    deleteUnpostedPrintedDocuments:boolean=false;
    apTrackingDiscountAvailable: boolean=false;
    overrideVoucherNumberTransactionEntry:boolean=false;
    printHistoricalAgedTrialBalance:boolean=false;
	editData;
    purchaseVatDetailOption;
    arrVatDetailId=[];
    purchaseVatScheduleId: any = [];
    freightVatScheduleId: any = [];
    miscVatScheduleId: any = [];
    fillAllFields=Constants.fillAllFields;
    minimumRange=Constants.minimumRange;
    maximumRange=Constants.maximumRange;
    search=Constants.search;
    fromPeriod=Constants.fromPeriod;
    toPeriod=Constants.toPeriod;
    toValue=Constants.toValue;
    greaterThan=Constants.greaterThan;
    lessThan=Constants.lessThan;
    onePeriod=Constants.onePeriod;
    twoPeriod=Constants.twoPeriod;
    maxrangeGreater=Constants.maxrangeGreater;
    minrange=Constants.minrange;
    agingPeriod=Constants.agingPeriod;
    sevenAgingPeriod=Constants.sevenAgingPeriod;

    btndisabled:boolean=false;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private vatDetailSetupService: VatDetailSetupService,
        private accountPayablesService:AccountPayablesService,
        private commonService:CommonService
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
       this.model.ageingBy=1;
    }

     ngOnInit() {
                // this.getAccountPayableSetup();
                this.getVatDetail();
                this.getAddScreenDetail();
                this.getmasterAgingListAccountReceivable();
                this.getCheckbookList();
                this.getCheckFormatType();
                this.getApplyByoption();  
                	/* for edit data */
	
	                
		/* for edit data ends here */
                // Serachable Select Box Step2
                    this.ddlCheckbookIdSetting = { 
                    singleSelection: true, 
                    text:this.select,
                    enableSearchFilter: true,
                    classes:"myclass custom-class",
                    searchPlaceholderText:this.search,
                }; 
                 window.setTimeout(() => {
                    this.getPayableSetup();
                }, 400);
                // Serachable Select Box Step2  
        
        
    }

     getCheckFormatType(){
                    this.accountPayablesService.getCheckFormatType().then(data => {
                    this.ArrCheckFormatType = data.result;
                    this.ArrCheckFormatType.splice(0,{ "name": "", "typeId": this.select });
                    this.model.checkFormat='';
                });
     }            

    getCheckbookList()
   {
              this.accountPayablesService.getListForCheckbook().then(data => {
            //   this.checkbookoptions = data.result.records;
            //   this.checkbookoptions.splice(0, 0, { "checkBookId": "", "checkbookDescription": this.select });
            //   this.model.checkbookId='';
               // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrCheckbookId.push({ "id": data.result.records[i].checkBookId, "itemName": data.result.records[i].checkBookId})
              }
			    // Searchable Select Box Step3 End
       });
   }

    // Serachable Select Box Step4
    getCheckbookByID(item:any){
       // this.model.checkbookId=item.id;
        this.checkbookId=[];
        this.checkbookId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("checkbookId")
    }
    OnCheckbookIdDeSelect(item:any){
        this.checkbookId=[];
        this.commonService.closeMultiselectWithId("checkbookId")
    }
  // Serachable Select Box Step4 ends

    getmasterAgingListAccountReceivable()
   {
       this.accountPayablesService.getmasterAgingListAccountReceivable().then(data => {
              this.ageingByOptions = data.result;
              this.ArrApplyBy.splice(0, 0, { "name": "", "typeId": this.select });
       });
   }

   getApplyByoption()
    {
       this.accountPayablesService.getdefaultListAccountReceivable().then(data => {
         this.ArrApplyByOption = data.result;
       });
    }

     getVatDetail()
    {
        this.vatDetailSetupService.getVatDetailSetup().subscribe(data => {
           	 for(var i=0;i< data.records.length;i++)
              {
                this.arrVatDetailId.push({ "id": data.records[i].vatScheduleId, "itemName": data.records[i].vatDescription})
              }
        });
    }
    
    generateAccountArray()
    {
        
       if(this.minRange == '' || this.maxRange == '' || this.currentFrom == '' || this.currentTo == '')
        {
            window.scrollTo(0,0);
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.fillAllFields;
            window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
            }, 4000);
        }
        else
        {

            this.pmPeriodSetupsList.push({"childCustId":this.minRange,
	        "childCustNamePrimary":this.maxRange,
	        "chilsCustNameSecondary":this.currentFrom,
	        "custChildBalance":this.currentTo 
             });
             this.minRange = '';
             this.maxRange = '';
             this.currentFrom = '';
             this.currentTo = '';
              this.pmPeriodSetupsList = this.pmPeriodSetupsList;
        }
    }

     RemoveArrValue(RowIndex)
    {
        
        var s=[];
        s=this.pmPeriodSetupsList;
        this.pmPeriodSetupsList.splice(RowIndex, 1);
         
        //this.pmPeriodSetupsList = this.pmPeriodSetupsList ;

    }


    getPayableSetup()
    {
        
        	this.accountPayablesService.getList().then(data => {
                
            this.freightVatScheduleId=[];
            this.miscVatScheduleId=[];
            this.purchaseVatScheduleId=[];
            this.editData = data.result;	
			if(this.editData){
				this.model = this.editData;
                
                var	selectedCheckbook = this.ArrCheckbookId.find(x => x.id ==  data.result.checkBookBankId);
			    this.checkbookId.push({'id':selectedCheckbook.id,'itemName':selectedCheckbook.itemName});

                var	selectedfreightVatSchedule = this.arrVatDetailId.find(x => x.id ==  data.result.freightVatScheduleId);
			    this.freightVatScheduleId.push({'id':selectedfreightVatSchedule.id,'itemName':selectedfreightVatSchedule.itemName});

                var	selectedmiscVatSchedule = this.arrVatDetailId.find(x => x.id ==  data.result.miscVatScheduleId);
			    this.miscVatScheduleId.push({'id':selectedmiscVatSchedule.id,'itemName':selectedmiscVatSchedule.itemName});

                var	selectedPurchaseVatSchedule = this.arrVatDetailId.find(x => x.id ==  data.result.purchaseVatScheduleId);
			    this.purchaseVatScheduleId.push({'id':selectedPurchaseVatSchedule.id,'itemName':selectedPurchaseVatSchedule.itemName});

                this.pmPeriodSetupsList = this.model.pmPeriodSetupsList;
			}			
        });
    }

     // set sales Vat ScheduleId selected Value
    getPurchaseVatDetailByID(item:any){
        this.purchaseVatScheduleId=[];
        this.purchaseVatScheduleId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("purchaseVatScheduleId")
    }

    // set sales Vat ScheduleId DeSelected 
    OnPurchaseVatDetailDeSelect(item:any){
        this.purchaseVatScheduleId=[];
        this.commonService.closeMultiselectWithId("purchaseVatScheduleId")
    }

       // set Freight ScheduleId selected Value
    onSelectFreightVatScheduleId(item:any){
        this.freightVatScheduleId=[];
        this.freightVatScheduleId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("freightVatScheduleId")
    }

    // set  Freight ScheduleId DeSelected 
    OnDeSelectFreightVatScheduleId(item:any){
        this.freightVatScheduleId=[];
        this.commonService.closeMultiselectWithId("freightVatScheduleId")
    }

      // set Misc Vat ScheduleId selected Value
    onSelectMiscVatScheduleId(item:any){
        this.miscVatScheduleId=[];
        this.miscVatScheduleId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("")
    }

    // set Misc Vat ScheduleId DeSelected 
    OnDeSelectMiscVatScheduleId(item:any){
        this.miscVatScheduleId=[];
        this.commonService.closeMultiselectWithId("")
    }
   
    validateRange(actionType)
    {
        if(this.isEdit)
        {
            return false;
        }
        if(this.pmPeriodSetupsList.length == 7)
        {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.sevenAgingPeriod;
             window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
              }, 4000);
          return false;
        }

        if(this.OldMaxLimit == 1000)
        {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.agingPeriod;
             window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
              }, 4000);
           return false;
        }
        if(actionType == 'minRange')
        {
            
            if(!this.minRange)
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.minimumRange;
              //  document.getElementById("txtminRange").focus();
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                return false;
            }
            
            if(this.pmPeriodSetupsList.length > 0 && (parseInt(this.minRange) < this.OldMaxLimit || this.OldMaxLimit < parseInt(this.minRange)) )
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.minrange + this.OldMaxLimit;
               // document.getElementById("txtminRange").focus();
                this.minRange =this.OldMaxLimit.toString();
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                 return false;
            }
        }
        else if(actionType == 'maxRange')
        {
            if(!this.maxRange)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.maximumRange;
             //  document.getElementById("txtmaxRange").focus();
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
            if(this.pmPeriodSetupsList.length > 0 && parseInt(this.maxRange) <  parseInt(this.minRange))
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.maxrangeGreater + this.minRange;
            //  document.getElementById("txtmaxRange").focus();
              this.maxRange ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
        }
        else if(actionType == 'currentFrom')
        {
            if(!this.currentFrom)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.fromPeriod;
            //  document.getElementById("txtcurrentFrom").focus();
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
            
            if(parseInt(this.currentFrom) <  parseInt(this.minRange))
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.greaterThan + this.minRange;
              //  document.getElementById("txtcurrentFrom").focus();
                this.currentFrom ="";
                 window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
            }, 4000);
                return false;
            }
            else if(parseInt(this.currentFrom) >  parseInt(this.maxRange))
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.lessThan + this.maxRange;
            //  document.getElementById("txtcurrentFrom").focus();
              this.currentFrom ="";
               window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
            }, 4000);
              return false;
            }
        }
        else if(actionType == 'currentTo')
        { if(!this.currentTo)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.toPeriod;
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
              
            }

            if(parseInt(this.currentTo) >  999)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.toValue + this.maxRange;
             // document.getElementById("txtcurrentTo").focus();
              this.currentTo ="999";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
            if(parseInt(this.currentTo) <  parseInt(this.currentFrom))
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.greaterThan + this.maxRange;
               // document.getElementById("txtcurrentTo").focus();
                this.currentTo ="";
                 window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                return false;
            }
            else if(parseInt(this.currentTo) >  parseInt(this.maxRange))
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.lessThan + this.maxRange;
             // document.getElementById("txtcurrentTo").focus();
              this.currentTo ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }

            this.pmPeriodSetupsList.push({"periodDescription":this.minRange +'-' + this.maxRange,"periodNoofDays":this.currentFrom,"periodEnd":this.currentTo})
            this.OldMaxLimit = parseInt(this.currentTo) + 1;
            this.minRange = "";
            this.maxRange = "";
            this.currentFrom = "";
            this.currentTo = "";

           
        }

    }
    getAddScreenDetail()
    {
         this.screenCode = "S-1153";
         this.defaultAddFormValues = [
                    { 'fieldName': 'ADD_ACC_PAY_AGING_PERIODS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_DUE_DATE_AGING', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                    { 'fieldName': 'ADD_ACC_PAY_TRANSACTION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_CURRENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                    { 'fieldName': 'ADD_ACC_PAY_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_0-30_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_31-60_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_60_OVER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_PASSWORDS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_REMOVE_VENDOR_HOLD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_EXCEED_MAX_INV_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_EXCEED_MAX_WROFF_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_APPLY_BY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_DUE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_TRANSACTION_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_ALLOW_DUPLICATE_INVOICE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_YES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_RECU_TRANSACTION_ONLY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_CHECK_FORMAT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_OPTIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_PRI_HIS_TRIAL_BAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_OVERRIDE_VOU_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_DEL_UNPOSTED_DOC_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_TRACKING_DIS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_AGE_UNAPP_CREDIT_AMT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_DEF_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_PURCHASE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_FREIGHT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_MISCELLANEOUS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_USER_DEFINE_FIELDS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_USER_DEFINE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                   
         ];
    this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
        {
            this.isScreenLock = res;
        });
         this.getScreenDetail(this.screenCode,'Add');
    }
    
    ModifyAgingPeriod(idx:number,item:any)
    {
       
        var range =item.periodDescription.split('-');
        this.minRange = range[0];
        this.maxRange=range[1];
        this.currentFrom = item.periodNoofDays;
        this.currentTo=item.periodEnd;
        this.agingIndex=idx;
        this.isEdit=true;
    }

    UpdateAgingPeriod()
    {
       
        this.pmPeriodSetupsList[this.agingIndex]["periodDescription"] = this.minRange +'-' + this.maxRange ;
        this.pmPeriodSetupsList[this.agingIndex]["periodNoofDays"] = this.currentFrom ;
        this.pmPeriodSetupsList[this.agingIndex]["periodEnd"] = this.currentTo ;
        this.OldMaxLimit = parseInt(this.currentTo) + 1;
        this.minRange = "";
        this.maxRange = "";
        this.currentFrom = "";
        this.currentTo = "";
        this.agingIndex=0;
        this.isEdit=false;
    }
    CancelAgingPeriod()
    {
       
        this.minRange = "";
        this.maxRange = "";
        this.currentFrom = "";
        this.currentTo = "";
        this.agingIndex=0;
        this.isEdit=false;
    }

    
  
    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
           
        });
    }

  
//    getAccountPayableSetup()
//    {
//      this.accountPayablesService.getAccountPayableSetup().then(data => {
//          if(data.code != 404)
//          {
//             this.model = data.result;
//          }
//       });
//    }
     
    redirect(){
     this.router.navigate(['accountpayablesetup/accountpayablesoptionsetup']);
    }

     Clear(f){
         this.checkbookId=[];
         this.purchaseVatScheduleId=[];
         this.freightVatScheduleId=[];
         this.miscVatScheduleId=[];
         this.pmPeriodSetupsList=[];
         this.model.ageingBy=[];
         this.minRange='';
         this.maxRange='';
         this.currentFrom='';
         this.currentTo='';
         this.model.printHistoricalAgedTrialBalance='';
         this.model.overrideVoucherNumberTransactionEntry='';
         this.model.deleteUnpostedPrintedDocuments='';
         this.model.apTrackingDiscountAvailable='';
         this.model.ageUnappliedCreditAmounts='';
       f.resetForm();
     }
     

     CreateAccountPayables(f: NgForm) {
        this.btndisabled=true;
       if(this.model.ageingBy == undefined || this.model.ageingBy == 'undefined')
       {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.hasMsg = true;
            this.showMsg = true;
            this.btndisabled=false;
            this.messageText = this.defaultAddFormValues[0]['listDtoFieldValidationMessage'][0]['validationMessage'];
            window.scrollTo(0,0);
            window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
            }, 4000);
          
       }
      
         if(this.model.printHistoricalAgedTrialBalance){
				var openMaintenance = "1";
			}else{
				var openMaintenance = "0";
			}
			if(this.model.overrideVoucherNumberTransactionEntry){
				var transaction = "1";
			}else{
				var transaction = "0";
			}
			
			if(this.model.deleteUnpostedPrintedDocuments){
				var fisacalYear = "1";
			}else{
				var fisacalYear = "0";
			}
			
			if(this.model.apTrackingDiscountAvailable){
				var distribution = "1";
			}else{
				var distribution = "0";
			}
            if(this.model.ageUnappliedCreditAmounts){
				var CreditAmounts = "1";
			}else{
				var CreditAmounts = "0";
			}
		
    //   
        // this.model.printHistoricalAgedTrialBalance=openMaintenance;
        // this.model.overrideVoucherNumberTransactionEntry=transaction;
        // this.model.deleteUnpostedPrintedDocuments=fisacalYear;
        // this.model.apTrackingDiscountAvailable=distribution;
        // this.model.ageUnappliedCreditAmounts=CreditAmounts;


    //    this.model.ageUnappliedCreditAmounts=this.ageUnappliedCreditAmounts;
    //    this.model.deleteUnpostedPrintedDocuments=this.deleteUnpostedPrintedDocuments;
    //    this.model.apTrackingDiscountAvailable=this.apTrackingDiscountAvailable;
    //    this.model.overrideVoucherNumberTransactionEntry=this.overrideVoucherNumberTransactionEntry;
    //    this.model.printHistoricalAgedTrialBalance=this.printHistoricalAgedTrialBalance;
    //
       this.model.checkBookBankId=this.checkbookId[0].id;
       this.model.miscVatScheduleId=this.miscVatScheduleId[0].id;
       this.model.purchaseVatScheduleId=this.purchaseVatScheduleId[0].id;
       this.model.freightVatScheduleId=this.freightVatScheduleId[0].id;

       this.model.pmPeriodSetupsList=this.pmPeriodSetupsList;
         if(this.model.ageingBy == 1)
        { 
            if(this.model.pmPeriodSetupsList.length < 1)
            {
              window.scrollTo(0,0);
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.btndisabled=false;
              this.messageText = this.onePeriod;
              this.currentTo ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                  return false;
            }
          

        }
        else if(this.model.ageingBy == 2){
            if(this.model.pmPeriodSetupsList.length < 2)
            {
              window.scrollTo(0,0);
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.btndisabled=false;
              this.messageText = this.twoPeriod;
              this.currentTo ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                 return false;
            }
           
        } 
       if (this.isModify) {
            this.accountPayablesService.updateAccountPayablesSetup(this.model).then(data => {
                this.checkbookId=[];
                this.btndisabled=false;
                var datacode = data.code;
                if (datacode == 200) {
                    this.isModify=false;
                    // this.model.bankId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                         window.scrollTo(0,0);
                         this.getPayableSetup();
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        else {
            this.accountPayablesService.addAccountPayablesSetup(this.model).then(data => {
                this.checkbookId=[];
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 200) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                         window.scrollTo(0,0);
                        this.getPayableSetup();
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        // this.getAccountPayableSetup();

                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        this.btndisabled=false;
    }

    oonlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

    /** If Screen is Lock then prevent user to perform any action.
    *  This function also cover Role Management Write acceess functionality */
    LockScreen(writeAccess)
    {
        if(!writeAccess)
        {
            return true
        }
        else if(this.btndisabled)
        {
            return true
        }
        else if(this.isScreenLock)
        {
            return true;
        }
        else{
            return false;
        }
    }
    
}
