import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountPayablesOptionSetup } from '../../../../financialModule/_models/accounts-payables/accounts-payables-option-setup';
import { AccountPayableOptionService } from '../../../../financialModule/_services/accounts-payables-setup/accounts-payables-option-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../_sharedresource/Constants';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';

@Component({
    selector: '<accounts-receivables></accounts-receivables>',
    templateUrl: './account-payables-option-setup.component.html',
    styles: ["user.component.css"],
    providers:[AccountPayableOptionService]
})

//export to make it available for other classes
export class AccountPayableOptionSetupComponent {
    page = new Page();
    rows = new Array<AccountPayableOptionService>();
    ArrBankId = [];
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    AccountPayableOptionSetup = {};
    id: string;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean=false;
    ArrApplyByOption=[];
    exchangeDate;
	exchangeExpireDate;
	exchangeTime='';
    checkbookoptions;
    ageingByOptions;
    minRange:string;
    maxRange:string;
    currentFrom:string;
    currentTo:string;
    OldMaxLimit:number;
    rmDocumentTypeList = [];
    lastDateBalanceForwardAge: boolean =false;
    compoundFinanceCharge: boolean =false;
    arTrackingDiscountAvailable: boolean =false;
    deleteUnpostedPrintedDocuments: boolean =false;
    printHistoricalAgedTrialBalance: boolean =false;
    arPayCommissionsInvoicePay: boolean =false;
    documentType:string;
    documentTypeDescription:string;
    documentTypeDescriptionArabic:string;
    documentNumberLastNumber:string;
    documentCode:string;
    isLatNumberDisable: boolean =false;
    fillAllFields= Constants.fillAllFields;
    isScreenLock;
    btndisabled:boolean=false;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private AccountPayableOptionService:AccountPayableOptionService,
         
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
    }

    ngOnInit() {
       this.getAddScreenDetail();
       this.getAccountRecievableSetup();
    }
    
    getAddScreenDetail()
    {
         this.screenCode = "S-1181";
         this.defaultAddFormValues = [
                    { 'fieldName': 'ADD_ACC_PAY_OPTION_NEXT_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_OPTION_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_OPTION_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_OPTION_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_OPTION_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_PAY_OPTION_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
         ];
         this.getScreenDetail(this.screenCode);
    }
    
    validateArrValue(ActionType)
    {
        if(ActionType == "documentNumberLastNumber")
        {
            if(this.documentType != "" && this.documentTypeDescription != "" && this.documentNumberLastNumber != "" && this.documentCode != "" && this.documentCode != "") 
            {
                this.rmDocumentTypeList.push({"apDocumentTypeId":"","documentTypeDescription": this.documentTypeDescription,"documentTypeDescriptionArabic": this.documentTypeDescriptionArabic,"documentNumberLastNumber": this.documentNumberLastNumber,"documentCode": this.documentCode,"documentType": this.documentType});
                this.isLatNumberDisable=false;
                this.documentType="";
                this.documentTypeDescription = "";
                this.documentTypeDescriptionArabic = "";
                this.documentCode = "";
                this.documentNumberLastNumber = "";
            }
            else{
                
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.fillAllFields;
                 window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
            }
        }
        else if(ActionType == "documentCode")
        {
            var getSameCodeList=this.rmDocumentTypeList.filter( obj => obj.documentCode.toString().toLowerCase() === this.documentCode.toString().toLowerCase())
                                            .map( obj => obj.documentNumberLastNumber);
            if(getSameCodeList.length > 0)
            {
                this.isLatNumberDisable=true;
                var getSameCodeListDesc= getSameCodeList.reverse();
                var maxNextNumber= getSameCodeListDesc[0];
                this.documentNumberLastNumber =(parseInt(maxNextNumber) + 1).toString();
            }
            else{
                 this.isLatNumberDisable=false;
                 this.documentNumberLastNumber = '1';
            }
        }
    }
    getScreenDetail(screenCode)
    {
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
             for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
    }

    //setting pagination
    clear(){
        this.documentType=''
        this.documentTypeDescription='';
        this.documentCode='';
        this.documentNumberLastNumber='';
        this.rmDocumentTypeList=[]
    }
   
   getAccountRecievableSetup()
   {
     this.AccountPayableOptionService.getAccountPayablesOptionSetup().then(data => {
         if(data.code != 404)
        {
            this.model.rmDocumentTypeList = data.result.records;
            this.rmDocumentTypeList = data.result;
        }
      });
   }
    
    CreateAccountPayable() {
        this.btndisabled=true;
        if(this.rmDocumentTypeList.length>0){
           this.model =this.rmDocumentTypeList;
           this.AccountPayableOptionService.saveAccountPayablesSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 200 || datacode == 201) {
                        this.isModify=false;
                        this.model.bankId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.getAccountRecievableSetup();
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
           else{
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.btndisabled=false;
                this.messageText = this.fillAllFields;
                 window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
        }
        this.btndisabled=false;
    }

    RemoveArrValue(RowIndex)
    {
        this.rmDocumentTypeList.splice(RowIndex, 1);
    }

    clearStartDate(){
		this.exchangeDate ='';
	}
	clearStartDate1(){
		this.exchangeExpireDate ='';
	}
	formatstartTime(selectedTime){
		this.exchangeTime = moment(selectedTime).format('HH:mm a');
    }
    
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

     /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
       LockScreen(writeAccess)
       {
           if(!writeAccess)
           {
               return true
           }
           else if(this.btndisabled)
           {
               return true
           }
           else if(this.isScreenLock)
           {
               return true;
           }
           else{
               return false;
           }
       }
}
