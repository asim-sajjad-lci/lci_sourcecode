
import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { AccountPayablesSetupComponent } from './accounts-payables-setup/accounts-payables-setup.component';
import { AccountPayablesClassSetupComponent } from './accounts-payables-class/accounts-payables-class-setup.component';
import { AccountPayableOptionSetupComponent } from './account-payables-option-setup/account-payables-option-setup.component';
import { AccountPayablesClassAccountSetupComponent } from './account-payables-class-account-setup/account-payables-class-account-setup.component';
import { AccountsPayablesRoutingModule } from './accounts-payables-setup-routing.module';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    AccountsPayablesRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule
  ],
  declarations: [AccountPayablesSetupComponent,AccountPayablesClassSetupComponent,AccountPayableOptionSetupComponent,AccountPayablesClassAccountSetupComponent,AutoFocusDirective]
})
export class AccountsPayablesModule { }
