import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountPayablesSetupComponent } from './accounts-payables-setup/accounts-payables-setup.component';
import { AccountPayablesClassSetupComponent } from './accounts-payables-class/accounts-payables-class-setup.component';
import { AccountPayableOptionSetupComponent } from './account-payables-option-setup/account-payables-option-setup.component';
import { AccountPayablesClassAccountSetupComponent } from './account-payables-class-account-setup/account-payables-class-account-setup.component';
const routes: Routes = [
   { path: '', component: AccountPayablesSetupComponent },
   {path:'accountpayablesclasssetup', component:AccountPayablesClassSetupComponent},
   {path:'accountpayablesoptionsetup', component:AccountPayableOptionSetupComponent},
   {path:'accountpayablesclassaccountsetup',component:AccountPayablesClassAccountSetupComponent},
   {path:'accountpayablesclassaccountsetup/:vendorClassId',component:AccountPayablesClassAccountSetupComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountsPayablesRoutingModule { }
