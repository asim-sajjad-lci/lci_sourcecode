import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountPayablesClassSetup } from '../../../../financialModule/_models/accounts-payables/account-payables-class-setup/account-payables-class-setup';
import { AccountPayablesClassSetupService } from '../../../../financialModule/_services/accounts-payables-setup/account-payables-class-setup/account-payables-class-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<account-payables-class-setup></account-payables-class-setup>',
    templateUrl: './accounts-payables-class-setup.component.html',
    styles: ["user.component.css"],
    providers:[AccountPayablesClassSetupService,CommonService]
})

//export to make it available for other classes
export class AccountPayablesClassSetupComponent {
    page = new Page();
    rows = new Array<AccountPayablesClassSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean=false;
    currencyoptions;
    checkbookoptions;
    shippingoptions;
    paymentoptions;
    vatoptions;
    disableButton;
    maximumInvoiceAmountOption:boolean=false;
    isMaxAmountOption:boolean=true;
    isCreditLimitOption:boolean=true;
    isMinAmountOption:boolean=true;
    oldClassId : string;   
    disableId;
    myHtml;
    add;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    currentvendorClassId;
    // Serachable Select Box Step1
	ArrCheckBookId = [];
    checkBookId : any = [];
    ddlCheckBookIdSetting = {};

    ArrCurrencyId = [];
    currencyId : any = [];
    ddlCurrencyIdSetting = {};

    ArrShipmentMethodId = [];
    shipmentMethodId : any = [];
    ddlShipmentMethodIDSetting = {};

    ArrPaymentTermId = [];
    paymentTermId : any = [];
    ddlPaymentTermIDSetting = {};

    ArrVatScheduleId = [];
    vatScheduleId : any = [];
    ddlVatScheduleIDSetting = {};
	// Serachable Select Box Step1 End
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    valueToChange;
	changedValue;
	selectedCurrency;
	unFrmtMaximumInvoiceAmountValue;
	unFrmtminimumChargeAmount;
    unFrmtCreditLimitAmount;
    unFrmtTradeDiscountPercent;
    frmtMaximumInvoiceAmountValue;
	frmtminimumChargeAmount;
	frmtCreditLimitAmount;
    inputValue;
    editMaximumInvoiceAmountValue;
    editMinimumChargeAmount;
    editCreditLimitAmount;
    editTradeDiscountPercent;
    btndisabled:boolean=false;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private accountPayablesClassSetupService: AccountPayablesClassSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService){

        this.disableButton = true;
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.disableId = false;
        this.add = true;
		this.model.checkBookId='';
		this.model.currencyId='';
		this.model.paymentTermId='';
    }

    ngOnInit() {
       this.getAddScreenDetail();
       this.getViewScreenDetail();
       this.setPage({ offset: 0 });
       this.getCurrencyList();
       this.getCheckbookList();
       this.getShippingMethodList();
       this.getPaymentTermList();
       this.getVatList();
       this.model.maximumInvoiceAmount="0";
       this.model.minimumCharge="0";
       this.model.creditLimit="0";
            // Serachable Select Box Step2
		   this.ddlCheckBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 

           this.ddlCurrencyIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 

           this.ddlShipmentMethodIDSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 

           this.ddlPaymentTermIDSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 

          this.ddlVatScheduleIDSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 
         
		 // Serachable Select Box Step2
    }
    

    getAddScreenDetail()
    {
         this.screenCode = "S-1161";
           // default form parameter for adding exchange creditCardSetup set up
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MAX_INV_AMT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MAX_INV_AMT_NO_MAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MAX_INV_AMT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MIN_PAYMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MIN_PAYMENT_NO_MIN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MIN_PAYMENT_PERCENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MIN_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
           //{ 'fieldName': 'ADD_ACC_PAY_CLASS_CREDIT_LIMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
           { 'fieldName': 'ADD_ACC_PAY_CLASS_CREDIT_LIMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
          
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CREDIT_LIMIT_NO_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CREDIT_LIMIT_UNLIMITED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CREDIT_LIMIT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_TRADE_DIS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MIN_ORDER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_SHIP_METHOD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_MAINTAIN_HISTORY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CALENDAR_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_FISCAL_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_TRANSACTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_USER_DEFINE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_ACCOUNTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_ACC_PAY_CLASS_PAYMENT_TERM_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
           
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1162";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_ACC_PAY_CLASS_CLASS_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_ACC_PAY_CLASS_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_ACC_PAY_CLASS_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_ACC_PAY_CLASS_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_ACC_PAY_CLASS_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_ACC_PAY_CLASS_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
              
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    	updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.accountPayablesClassSetupService.searchAccountPayableClassSetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }


handleInvoiceOptionChange(evt,ActionType)
{
      var optionValue = evt.target.value;
    if(ActionType == 'inamount')
      {
        this.model.maximumInvoiceAmount=optionValue;
        if (optionValue == 0) {
            this.isMaxAmountOption = true;
            this.model.maximumInvoiceAmountValue=null;
        }
        else{
             this.isMaxAmountOption = false;
        }  
      
      }
      else if(ActionType == 'minimumCharge')
      {
        this.model.minimumCharge=optionValue;
        if (optionValue == 0) {
            this.isMinAmountOption = true;
            this.model.minimumChargeAmount= null;
        }
        else{
             this.isMinAmountOption = false;
        }
      }
      else if(ActionType == 'creditLimit')
      {
        this.model.creditLimit=optionValue;
        if (optionValue == 0 || optionValue == 1) {
            this.isCreditLimitOption = true;
            this.model.creditLimitAmount= null;
        }
        else{
             this.isCreditLimitOption = false;
        }
      }
      
}

handleInvoiceOptionDBValue()
{
       var optionValue = this.model.maximumInvoiceAmount;
        if (optionValue) {
            this.maximumInvoiceAmountOption= true;
            this.isMaxAmountOption = true;
        }
        else{
             this.maximumInvoiceAmountOption= false;
             this.isMaxAmountOption = false;
        }
        
     
        optionValue = this.model.creditLimit;
        this.model.creditLimit=optionValue;
        if (optionValue == 0 || optionValue == 1) {
            this.isCreditLimitOption = true;
        }
        else{
             this.isCreditLimitOption = false;
        }
      
}
getCurrencyList()
{
     this.accountPayablesClassSetupService.getListForCurrency().then(data => {
            this.currencyoptions = data.result.records;
            // this.currencyoptions.splice(0, 0, {"currencyId": this.select });
            // this.model.currencyId='';
              // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrCurrencyId.push({ "id": data.result.records[i].currencyId, "itemName": data.result.records[i].currencyId})
              }
			    // Searchable Select Box Step3 End
    });
}

getVatList()
{
     this.accountPayablesClassSetupService.getListForVatId().then(data => {
            this.vatoptions = data.result.records;
            // this.vatoptions.splice(0, 0, { "vatScheduleId": this.select });
            // this.model.vatScheduleId='';
                // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrVatScheduleId.push({ "id": data.result.records[i].vatScheduleId, "itemName": data.result.records[i].vatScheduleId})
              }
			    // Searchable Select Box Step3 End
    });
}
//getShippingMethodList

getShippingMethodList()
{
     this.accountPayablesClassSetupService.getListForShippingMethod().then(data => {
            this.shippingoptions = data.result.records;
            // this.shippingoptions.splice(0, 0, { "shipmentMethodId": this.select });
            // this.model.shipmentMethodId='';
                // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrShipmentMethodId.push({ "id": data.result.records[i].shipmentMethodId, "itemName": data.result.records[i].shipmentMethodId})
              }
			    // Searchable Select Box Step3 End
    });
}

//getPaymentTermList
getPaymentTermList()
{
     this.accountPayablesClassSetupService.getPaymentTermList().then(data => {
            this.paymentoptions = data.result.records;
            // this.paymentoptions.splice(0, 0, { "paymentTermId": this.select });
            // this.model.paymentTermId='';
               // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrPaymentTermId.push({ "id": data.result.records[i].paymentTermId, "itemName": data.result.records[i].paymentTermId})
              }
			    // Searchable Select Box Step3 End
    });
}

getCheckbookList()
{
     this.accountPayablesClassSetupService.getListForCheckbook().then(data => {
            this.checkbookoptions = data.result.records;
            // this.currencyoptions.splice(0,0, { "checkBookId": this.select });
            // this.model.checkBookId='';
              // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrCheckBookId.push({ "id": data.result.records[i].checkBookId, "itemName": data.result.records[i].checkBookId})
              }
			    // Searchable Select Box Step3 End
    });
}

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.accountPayablesClassSetupService.searchAccountPayableClassSetup(this.page, this.searchKeyword).subscribe(pagedData => {
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
 
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
    resetMe(f)
    {
                    this.model.classDescription='';
                    this.model.classDescriptionArabic='';
                    this.model.creditLimit='0';
                    this.model.creditLimitAmount='';
                    this.currencyId=[];
                    this.model.maximumInvoiceAmountValue='';
                    
                    this.model.maximumInvoiceAmount='0';
                    this.model.minimumCharge='0';
                    this.model.minimumChargeAmount='';
                    this.model.minimumOrderAmount='';
                    this.model.tradeDiscountPercent='';
                    this.model.openMaintenanceHistoryCalendarYear='';
                    this.model.openMaintenanceHistoryDistribution='';
                    this.model.openMaintenanceHistoryFiscalYear='';
                    this.model.openMaintenanceHistoryTransaction='';
                    this.model.userDefine1='';
                    this.model.userDefine2='';
                    this.model.userDefine3='';
                    this.vatScheduleId=[];
                    this.shipmentMethodId=[];
                    this.checkBookId=[];
                    this.paymentTermId=[];
                    this.model.accTableId='';
                    this.unFrmtCreditLimitAmount = '';
                    this.unFrmtMaximumInvoiceAmountValue = '';
                    this.unFrmtminimumChargeAmount = '';
                    this.unFrmtTradeDiscountPercent = '';
                    this.editTradeDiscountPercent = '';
                    this.editCreditLimitAmount = '';
                    this.editMinimumChargeAmount = '';
                    this.editMaximumInvoiceAmountValue = '';
                    this.selectedCurrency = undefined;
                    this.model.minimumChargeAmount = '';
                    this.model.maximumInvoiceAmountValue = '';
                    this.model.creditLimitAmount = '';
                    this.model.tradeDiscountPercent = '';

                    



 
     if(!this.add==true){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.vendorClassId='';
		 }
    }

    Cancel(f)
    {
    this.add=true;
    this.disableButton = true;
    this.model.vendorClassId= '';
    this.resetMe(f);
    this.disableId=false;
    }
    
    // getAcountPayableClassSetupGetById(row: any)
  getAcountPayableClassSetupGetById(row: any)
    {
         this.showBtns=true;
         this.add = false;
         this.isModify=true;
         this.disableId = true;
         this.myHtml = "border-vanish";
         this.disableButton = false;
         this.checkBookId=[];
         this.currencyId=[];
         this.shipmentMethodId=[];
         this.paymentTermId=[];
         this.vatScheduleId=[];
        this.accountPayablesClassSetupService.getAcountPayableClassSetupGetById(row.vendorClassId).then(data => {
        this.model = data.result;
        
        this.currentvendorClassId =row.vendorClassId;
        this.checkBookId.push({'id':data.result.checkBookId,'itemName':data.result.checkBookId}); 
        this.currencyId.push({'id':data.result.currencyId,'itemName':data.result.currencyId});
        this.selectedCurrency = data.result.currencyId
        this.shipmentMethodId.push({'id':data.result.shipmentMethodId,'itemName':data.result.shipmentMethodId});
        this.paymentTermId.push({'id':data.result.paymentTermId,'itemName':data.result.paymentTermId});
        this.vatScheduleId.push({'id':data.result.vatScheduleId,'itemName':data.result.vatScheduleId});
        if (data.result.maximumInvoiceAmountValue){
            this.editMaximumInvoiceAmountValue = data.result.maximumInvoiceAmountValue
            this.formatMaximumInvoiceAmountValue();
        }
        if(data.result.minimumChargeAmount){
           this.editMinimumChargeAmount =  data.result.minimumChargeAmount
           this.formatminimumChargeAmount()
        }
        if(data.result.creditLimit)
        {
            this.formatCreditLimitAmount();
            this.editCreditLimitAmount = data.result.creditLimitAmount
            this.model.creditLimit=data.result.creditLimit.toString();

        }
        if(data.result.tradeDiscountPercent){
            this.editTradeDiscountPercent = data.result.tradeDiscountPercent
            this.formatTradeDiscountPercent();
        }
        if(data.result.maximumInvoiceAmount)
        {
            this.model.maximumInvoiceAmount=data.result.maximumInvoiceAmount.toString();
        }
      
        if(data.result.minimumCharge)
        {
            this.model.minimumCharge=data.result.minimumCharge.toString();
            
        }
        
        if(parseInt(this.model.maximumInvoiceAmount))
            {
              this.maximumInvoiceAmountOption=true;
              this.isMaxAmountOption = false;
            }
            else
            {
                this.maximumInvoiceAmountOption=false;
                this.isMaxAmountOption = true;
            }
            if(parseInt(this.model.minimumCharge))
            {
              this.isMinAmountOption = false;
             
            }
            else
            {
                this.isMinAmountOption = true;
            }

            if(parseInt(this.model.creditLimit) == 0 || parseInt(this.model.creditLimit) == 1 )
            {
              this.isCreditLimitOption = true;
            }
            else
            {
                this.isCreditLimitOption = false;
            }
         });
    }

     // Serachable Select Box Step4
    getCheckBookByID(item:any){
        this.model.checkBookId=item.id;
        this.commonService.closeMultiselectWithId("checkBookId")
    }
    OnCheckBookIdDeSelect(item:any){
        this.model.checkBookId='';
        this.commonService.closeMultiselectWithId("checkBookId")
    }

    // getCurrencyByID(item:any){
    //     this.model.currencyId=item.id;
    // }
    getCurrencyByID(item:any){
        this.selectedCurrency = this.model.currencyId=item.id;
        this.commonService.closeMultiselectWithId("currencyId")
		if(this.model.maximumInvoiceAmountValue){
			this.formatMaximumInvoiceAmountValue()
		}
		if(this.model.minimumChargeAmount){
			this.formatminimumChargeAmount()
		}
		if(this.model.creditLimitAmount){
			this.formatCreditLimitAmount()
        }
        if(this.model.tradeDiscountPercent){
			this.formatTradeDiscountPercent()
        }
    }

    formatMaximumInvoiceAmountValue(){
        
         this.commonService.changeInputText("maximumInvoiceAmountValue")
        var inputValue = 0;
        if(this.isModify){
            if(this.editMaximumInvoiceAmountValue == 'undefined' && this.unFrmtMaximumInvoiceAmountValue){
                inputValue = this.unFrmtMaximumInvoiceAmountValue
            }
           else if(this.editMaximumInvoiceAmountValue == 'undefined'){
                inputValue = this.model.maximumInvoiceAmountValue
            }else{
                inputValue = this.editMaximumInvoiceAmountValue
            }
			this.unFrmtMaximumInvoiceAmountValue = inputValue
		}else{
			 inputValue = this.unFrmtMaximumInvoiceAmountValue
		}
        
        
		if (this.selectedCurrency !=undefined ){
            let submitInfo = {
				'currencyId':this.selectedCurrency
			};
			this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
				this.model.maximumInvoiceAmountValue = this.commonService.formatAmount(data, inputValue)
			});
			
			
		}else{
			this.formatMaximumInvoiceAmountValueChanged()
		}
		
	
	}
	formatMaximumInvoiceAmountValueChanged(){
        this.commonService.changeInputNumber("maximumInvoiceAmountValue")
		if(this.isModify){
            if(this.editMaximumInvoiceAmountValue == 'undefined')
            {
                this.model.maximumInvoiceAmountValue = this.model.maximumInvoiceAmountValue
            }else{
                this.model.maximumInvoiceAmountValue = this.editMaximumInvoiceAmountValue
            }
			
		}else{
			this.model.maximumInvoiceAmountValue = this.unFrmtMaximumInvoiceAmountValue
		}
		
		
	}
	getMaximumInvoiceAmountValue(event){
        this.editMaximumInvoiceAmountValue = 'undefined'
        if(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
            }
        }
        
		this.unFrmtMaximumInvoiceAmountValue = event
	}

	//minimumChargeAmount
	//add Symbols
	formatminimumChargeAmount(){
        this.commonService.changeInputText("minimumChargeAmount")
		var inputValue = 0;
		if(this.isModify){
            if(this.editMinimumChargeAmount == 'undefined' && this.unFrmtminimumChargeAmount){
                inputValue = this.unFrmtminimumChargeAmount
            }
            else if(this.editMinimumChargeAmount == 'undefined'){
                inputValue = this.model.minimumChargeAmount
            }else{
                inputValue = this.editMinimumChargeAmount
            }
            this.unFrmtminimumChargeAmount = inputValue
		}else{
			inputValue = this.unFrmtminimumChargeAmount
		}
	    
            if (this.selectedCurrency !=undefined ){
            let submitInfo = {
                'currencyId':this.selectedCurrency
            }
			this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
				this.model.minimumChargeAmount = this.commonService.formatAmount(data, inputValue)
			});
			 }else{
                this.formatminimumChargeAmountChanged()
            }
        
	}
	//Change To Original Value of minimumChargeAmount
	formatminimumChargeAmountChanged(){
        this.commonService.changeInputNumber("minimumChargeAmount")
		if(this.isModify){
            if(this.editMinimumChargeAmount == 'undefined'){
                this.model.minimumChargeAmount = this.model.minimumChargeAmount
            }else{
                this.model.minimumChargeAmount = this.editMinimumChargeAmount
            }
			
		}else{
			
		this.model.minimumChargeAmount = this.unFrmtminimumChargeAmount
		}
		
	}
	// get Original Minimum Charge Amount
	getminimumChargeAmountValue(event){
        this.editMinimumChargeAmount = 'undefined'
        if(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
            }
        }
        
		this.unFrmtminimumChargeAmount = event
	}
	//End MinimumChargeAmount

	//Credit Limit Amount
	//add Symbols
	formatCreditLimitAmount(){
		this.commonService.changeInputText("creditLimitAmount")
        var inputValue = 0;
        if(this.isModify){
			if(this.editCreditLimitAmount == 'undefined' && this.unFrmtCreditLimitAmount){
                inputValue = this.unFrmtCreditLimitAmount
            }else if(this.editCreditLimitAmount == 'undefined'){
                inputValue = this.model.creditLimitAmount
            }
            else{
                inputValue = this.editCreditLimitAmount
            }
            this.unFrmtCreditLimitAmount = inputValue
		}else{
			inputValue = this.unFrmtCreditLimitAmount
		}
		
		if (this.selectedCurrency !=undefined ){
			let submitInfo = {
				'currencyId':this.selectedCurrency
			};
			this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
				this.model.creditLimitAmount = this.commonService.formatAmount(data, inputValue)
			}).catch(error => {
				window.setTimeout(() => {
					this.isSuccessMsg = false;
					this.isfailureMsg = true;
					this.showMsg = true;
					this.hasMsg = true;
					this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
				}, 100)
			});
			
			
		}else{
			this.formatCreditLimitAmountChanged()
		}
		
	
	}
	//Change To Original Value of Credit Limit Amount
	formatCreditLimitAmountChanged(){
		this.commonService.changeInputNumber("creditLimitAmount")
		if(this.isModify){
            if(this.editCreditLimitAmount == 'undefined' ){
                this.model.creditLimitAmount = this.model.creditLimitAmount
            }else{
                this.model.creditLimitAmount = this.editCreditLimitAmount
            }
			
		}else{
			this.model.creditLimitAmount = this.unFrmtCreditLimitAmount
		}
		
		
	}
	// get Original Credit Limit Amount
	getCreditLimitAmountValue(event){
        this.editCreditLimitAmount = 'undefined'
        if(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
            }
        }
        
		this.unFrmtCreditLimitAmount = event
	}
	//End CreditLimitAmount
    getTradeDiscountPercent(event){
		this.editTradeDiscountPercent = 'undefined'
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		this.unFrmtTradeDiscountPercent = event

	}
	formatTradeDiscountPercent(){
		this.commonService.changeInputText("tradeDiscountPercent")
		var inputValue = 0;
		if(this.isModify){
			if (this.editTradeDiscountPercent == 'undefined' && this.unFrmtTradeDiscountPercent ){
				inputValue = this.unFrmtTradeDiscountPercent
			}
			else if(this.editTradeDiscountPercent == 'undefined'){
                inputValue = this.model.tradeDiscountPercent
            }else{
                inputValue = this.editTradeDiscountPercent
            }
			this.unFrmtTradeDiscountPercent = inputValue
		}else{
			inputValue = this.unFrmtTradeDiscountPercent
		}
		
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.model.tradeDiscountPercent = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatTradeDiscountPercentChanged()
			}
		

	}
	formatTradeDiscountPercentChanged(){
		this.commonService.changeInputNumber("tradeDiscountPercent")
		if(this.isModify){
			if(this.model == 'undefined'){
                this.model.tradeDiscountPercent = this.model.tradeDiscountPercent
            }else{
                this.model.tradeDiscountPercent = this.editTradeDiscountPercent
            }
		}else{
			this.model.tradeDiscountPercent = this.unFrmtTradeDiscountPercent
		}
	}
    OnCurrencyIdDeSelect(item:any){
        this.formatMaximumInvoiceAmountValueChanged();
		this.formatCreditLimitAmountChanged();
        this.formatminimumChargeAmountChanged();
        this.formatTradeDiscountPercentChanged();
        this.model.currencyId='';
        this.commonService.closeMultiselectWithId("currencyId")
    }

        getShipmentMethodByID(item:any){
        this.model.shipmentMethodId=item.id;
        this.commonService.closeMultiselectWithId("shipmentMethodId")
    }
        OnShipmentMethodIdDeSelect(item:any){
        this.model.shipmentMethodId='';
        this.commonService.closeMultiselectWithId("shipmentMethodId")
    }

    getPaymentTermByID(item:any){
    this.model.paymentTermId=item.id;
    this.commonService.closeMultiselectWithId("paymentTermId")
    }
    OnPaymentTermIdDeSelect(item:any){
        this.model.paymentTermId='';
        this.commonService.closeMultiselectWithId("paymentTermId")
    }

    getVatScheduleByID(item:any){
    this.model.vatScheduleId=item.id;
    this.commonService.closeMultiselectWithId("vatScheduleId")
    }
    OnVatScheduleIdDeSelect(item:any){
        this.model.vatScheduleId='';
        this.commonService.closeMultiselectWithId("vatScheduleId")
    }
    // Serachable Select Box Step4 ends

     //Is id already exist
    IsIdAlreadyExist(){
        if(this.model.vendorClassId != '' && this.model.vendorClassId != undefined && this.model.vendorClassId != 'undefined')
        {
            this.accountPayablesClassSetupService.getAcountPayableClassSetupGetById(this.model.vendorClassId).then(data => {
                var datacode = data.code;
                if (data.status == "NOT_FOUND") {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("txtvendorClassId");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.model.vendorClassId='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    }
    saveAccountPayblesClass(f: NgForm)
    {
        this.btndisabled=true;
        if(this.checkBookId.length!=0 && this.currencyId.length!=0 && this.paymentTermId.length !=0){

        
          if(this.model.openMaintenanceHistoryCalendarYear){
				var openMaintenance = "1";
			}else{
				var openMaintenance = "0";
			}
			if(this.model.openMaintenanceHistoryTransaction){
				var transaction = "1";
			}else{
				var transaction = "0";
			}
			
			if(this.model.openMaintenanceHistoryFiscalYear){
				var fisacalYear = "1";
			}else{
				var fisacalYear = "0";
			}
			
			if(this.model.openMaintenanceHistoryDistribution){
				var distribution = "1";
			}else{
				var distribution = "0";
			}
		
        this.model.maximumInvoiceAmountValue = this.unFrmtMaximumInvoiceAmountValue
        this.model.minimumChargeAmount = this.unFrmtminimumChargeAmount
        this.model.creditLimitAmount = this.unFrmtCreditLimitAmount
        this.model.tradeDiscountPercent = this.unFrmtTradeDiscountPercent
        this.model.openMaintenanceHistoryCalendarYear=openMaintenance;
        this.model.openMaintenanceHistoryTransaction=transaction;
        this.model.openMaintenanceHistoryFiscalYear=fisacalYear;
        this.model.openMaintenanceHistoryDistribution=distribution;
        
        if(!this.add)
        {
               
             this.accountPayablesClassSetupService.updateAccountPayableClassSetup(this.model).then(data => {
                 
                 this.checkBookId=[];
                 this.currencyId=[];
                 this.shipmentMethodId=[];
                 this.paymentTermId=[];
                 this.vatScheduleId=[];
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 200) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.disableButton = true;
                        this.disableId = false;
                        this.showBtns=false;
                        this.model.maximumInvoiceAmountValue='';
                        this.model.minimumChargeAmount='';
                        this.model.creditLimitAmount='';
                        
                        this.setPage({ offset: 0 });
                        
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
            
            
             this.accountPayablesClassSetupService.createAccountPayablesClassSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.checkBookId=[];
                this.showBtns=false;
                this.disableId = false;
                this.currencyId=[];
                this.shipmentMethodId=[];
                this.paymentTermId=[];
                this.vatScheduleId=[];
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 200) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.model.maximumInvoiceAmountValue='';
                        this.model.minimumChargeAmount='';
                        this.model.creditLimitAmount='';
                        this.setPage({ offset: 0 });
                  window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         }
         this.btndisabled=false;
    }
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

    redirect1(currentvendorClassId){
        
        this.router.navigate(['accountpayablesetup/accountpayablesclassaccountsetup',this.currentvendorClassId]);
    }

     /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
       LockScreen(writeAccess)
       {
           if(!writeAccess)
           {
               return true
           }
           else if(this.btndisabled)
           {
               return true
           }
           else if(this.isScreenLock)
           {
               return true;
           }
           else{
               return false;
           }
       }
}