import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {fixedAssetCompanySetup} from '../../../../financialModule/_models/fixedAssets/fixedAssetCompanySetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import {AccountPayableClassAccountSetupService} from '../../../../financialModule/_services/accounts-payables-setup/account-payables-class-account-setup.service';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import {AccountReceivableClassGLSetupService} from '../../../../financialModule/_services/accountReceivables/accountReceivablesClassGLSetup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
   selector: 'account-payables-class-account-setup',
   templateUrl: './account-payables-class-account-setup.component.html',
   providers: [AccountPayableClassAccountSetupService,AccountStructureService,AccountReceivableClassGLSetupService,CommonService],
})

export class AccountPayablesClassAccountSetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	isScreenLock;
	select=Constants.select; 
	moduleName;
    screenName;
	descriptionn;
	descriptionArabic;
	// Serachable Select Box Step1
	ArrClassId = [];
    classId : any = [];
    ddlClassIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<fixedAssetCompanySetup>();
    temp = new Array<fixedAssetCompanySetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
	currentvendorClassId;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
	totalText=Constants.totalText;
	search=Constants.search;
	deleteConfirmationText = Constants.deleteConfirmationText;
	missingAccount = Constants.missingAccount;
	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	tempVar;
	fetchedData;
	getClass;
	getAccountList;
	mode;
	accountNumberList;
	description;
	arabicDescription;
	accountIndex;
	dimIndex2;
	dimIndex3;
	dimIndex4;
	arrSegment= [];
	AccountTypeList=[];
	sameAccountNumberMsg =Constants.sameAccountNumberMsg;
		accountPayables = 
		{
			'classId': '',
		}
	
	
	constructor(private router: Router,private route: ActivatedRoute,private accountPayableClassAccountSetupService: AccountPayableClassAccountSetupService,private getScreenDetailService: GetScreenDetailService,public toastr: ToastsManager,
		vcr: ViewContainerRef,
		private AccountStructureService:AccountStructureService,
		private AccountReceivableClassGLSetupService: AccountReceivableClassGLSetupService,
		private commonService:CommonService) {
			this.toastr.setRootViewContainerRef(vcr);
			let vm = this;
			this.page.pageNumber = 0;
			this.page.size = 5;	
			this.tempVar ='';
			this.fetchedData =[];
			this.getClass =[];
			this.getAccountList =[];
			this.accountNumberList =[];
			this.mode ="add";
				
		this.getAccountType();
		
		this.accountPayableClassAccountSetupService.getClass().subscribe(pagedData => {
			// Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrClassId.push({ "id": pagedData.records[i].vendorClassId, "itemName": pagedData.records[i].vendorClassId})
              }
			// Searchable Select Box Step3 End
        });
		
		
		/* for edit ends*/
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_PAY_CLASS_ACC_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
				];
				this.getScreenDetailService.ValidateScreen("S-1211").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1211").then(data => {
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */	
	}


	ngOnInit()
	{
		this.route.params.subscribe((params: Params) => {
				this.currentvendorClassId = params['vendorClassId'];
				if(this.currentvendorClassId)
				{
					this.GetClassAccountDetails(this.currentvendorClassId);
				}
				
		 });

		

		// Serachable Select Box Step2
			this.ddlClassIdSetting = { 
			singleSelection: true, 
			text:this.select,
			enableSearchFilter: true,
			disabled:this.currentvendorClassId,
			classes:"myclass custom-class",
			searchPlaceholderText:this.search,
        }; 
		this.getSegmentCount();
		// Serachable Select Box Step2
	}	
    getAccountType(){
       this.accountPayableClassAccountSetupService.getAccountType().then(pagedData => {
            this.AccountTypeList = pagedData.result;
			for(let i=0; i<this.AccountTypeList.length; i++){
				let temp={
					 "accountType": this.AccountTypeList[i]["typeId"],
					 "typeValue": this.AccountTypeList[i]["typeValue"],
					 "accountDescription":[],
					 "accountNumberIndex":[]
				}
				this.accountNumberList.push(temp);
			}
        });
	}
	

	getSegmentCount()
	{
		 this.AccountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}
	
	save(accountPayablesClassAccountSetup:NgForm){
		this.btndisabled=true;
		let submittedData = {
			'classId': this.classId[0].id
		}
		var accountNumberIndexArr=[];
            var tempAccountNumberList=[];
		    for(let i=0;i<this.accountNumberList.length;i++){
				if(this.accountNumberList[i]["accountNumberIndex"])
				{
					var accountDesc = this.accountNumberList[i]["accountDescription"].join();
					accountDesc=accountDesc.replace(/,/g , " ");
					accountDesc=accountDesc.replace("0"," ");
					accountDesc=accountDesc.replace(0," ");
					if(this.accountNumberList[i]["accountNumberIndex"].length > 0)
					{
						tempAccountNumberList.push({
						"accountType":this.accountNumberList[i]["accountType"],
						"accountDescription": accountDesc,
						"accountNumberIndex":this.accountNumberList[i]["accountNumberIndex"],
						"transactionType":0
					});	
					}
				}
			}
			
			if(tempAccountNumberList.length == 0)
			{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.btndisabled=false;
				  this.messageText = this.missingAccount;
					window.setTimeout(() => {
							this.showMsg = false;
							this.hasMsg = false;
						}, 4000);
				return false;
			}
		submittedData["accountNumberList"] = tempAccountNumberList
		this.accountPayableClassAccountSetupService.save(submittedData).then(data => {
				var datacode = data.code;
                this.btndisabled=false; 
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					if(this.currentvendorClassId == undefined || this.currentvendorClassId == 'undefined')
					{	accountPayablesClassAccountSetup.resetForm();
						this.Clear(accountPayablesClassAccountSetup);
					}
				
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
				}, 100);
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
			this.btndisabled=false;
	}
	
	
	fetchFirstAccountIndexInfo(i,segmentIndex){
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+segmentIndex);
		var segmentNumber=segment.value;
		var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));
		if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		if(segmentNumber)
		{
			this.AccountReceivableClassGLSetupService.firstTextApi(segmentNumber).subscribe(pagedData => {
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberList[i].accountNumberIndex[segmentIndex] || this.accountNumberList[i].accountNumberIndex[segmentIndex] == '0')
					{
						this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountNumberList[i].accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountNumberList[i]["accountDescription"].push(result.mainAccountDescription);
						this.accountNumberList[i]["accountNumberIndex"].push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
						this.accountNumberList[i].accountNumberIndex[m]='0';
						this.accountNumberList[i].accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
						}
						if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountNumberList[i]["accountDescription"] = [];
					this.accountNumberList[i]["accountNumberIndex"] = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
					{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
						if(m == 0)
						{
							segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+m);
						}
						segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
				this.accountNumberList[i]["accountDescription"] = [];
				this.accountNumberList[i]["accountNumberIndex"] = [];
				var currentSegment=segmentIndex;
				for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
				{
					var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
					segment.value = '';
					segment.disabled = true;
				}
		}
	}
	fetchOtherAccountIndexInfo(i,segmentIndex){
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
		var segmentNumber=segment.value;
		var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));
		if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberList[i].accountNumberIndex.indexOf(segment.value);
		if(acctIndex > -1)
		{
			this.toastr.warning(this.sameAccountNumberMsg);
			segment.value = '';
		}
		else{
			if(segmentNumber)
			{
                this.AccountReceivableClassGLSetupService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
					let status = pagedData.status;
    				let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberList[i].accountNumberIndex[segmentIndex] || this.accountNumberList[i].accountNumberIndex[segmentIndex] == '0')
						{
							this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountNumberList[i].accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountNumberList[i]["accountDescription"].push(result.dimensionDescription);
							this.accountNumberList[i]["accountNumberIndex"].push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
						segment.value = '';
						this.accountNumberList[i].accountNumberIndex[segmentIndex]='0';
						this.accountNumberList[i].accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberList[i].accountNumberIndex[currentSegment]='0';
				this.accountNumberList[i].accountDescription[currentSegment]='0';
			}
		}
	}

	GetClassAccountDetails(currentvendorClassId){
		this.accountPayableClassAccountSetupService.getClassDescription(this.currentvendorClassId).then(pagedData => {
			
		
		///	this.model =pagedData.result;
		//var classId = pagedData.result;
		   // var selectedBook = this.ArrBookId.find(x => x.id ==   data.result.bookId);
			this.classId.push({'id':pagedData.result.vendorClassId,'itemName':pagedData.result.vendorClassId});
			this.getClassDescription(this.currentvendorClassId)
		
       	});
	}

	Clear(f:NgForm){
		
		if(this.currentvendorClassId == undefined || this.currentvendorClassId == 'undefined')
		{
			this.description="";
			this.classId=[];
			this.arabicDescription="";
			this.getAccountType();
			this.AccountTypeList=[];
			this.accountNumberList = [];
		    f.resetForm();
		}
		this.getAccountType();
		this.AccountTypeList=[];
		this.accountNumberList = [];
	}
	
	clearForm(accountPayablesClassAccountSetup:NgForm){
		accountPayablesClassAccountSetup.resetForm();
		this.description="";
		this.classId=[];
		this.arabicDescription="";
		
		    for(var i=0;i<this.accountNumberList.length;i++)
		       	{
					if(this.accountNumberList[i]["accountNumberIndex"]["arrSegment"])
						{
							var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
			 				for(var k=0;k<currentRowSegment.length;k++)
			 					{
				  					var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
				  				if(k > 0)
				  				{
									segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
									segment.disabled=true;
				  				}
				  				else{
									segment.disabled=false;  	
				  				}
				  				segment.value = '';
							}
						}	
					this.accountNumberList[i]["accountDescription"] =[];	
					this.accountNumberList[i]["accountNumberIndex"] =[];
    			}
			//this.accountNumberList = [];	
	}
	
	getDesc(){
		this.accountPayableClassAccountSetupService.getClassDescription(this.accountPayables.classId).then(pagedData => {
		this.description = pagedData.result.classDescription;
		this.arabicDescription = pagedData.result.classDescriptionArabic;
       	});
	}

	getClassDescription(classId:string)
	{
		
		 this.accountPayableClassAccountSetupService.getClassDescription(classId).then(pagedData => {
			this.description = pagedData.result.classDescription;
			this.arabicDescription = pagedData.result.classDescriptionArabic;
			
		});
		this.accountPayableClassAccountSetupService.getAccountPayableClassSetup(classId).then(pagedData => {
			
			
		  for(var i=0;i<this.accountNumberList.length;i++)
		   {
			  var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
			  if(currentRowSegment)
			  {
			  	for(var k=0;k< currentRowSegment.length;k++)
			  	{
			  	  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
				  if(k>0)
				  {
				    segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
					if(segment)
					{
						segment.disabled=true;
					}
				  }
				  else{
					if(segment)
					{
						segment.disabled=false;
					}
				  }
				  segment.value = '';
				}
			  }
			 
			  	this.accountNumberList[i]["accountDescription"] =[];
				this.accountNumberList[i]["accountNumberIndex"] =[];
			    this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = [];
			  if(pagedData.btiMessage.messageShort != 'RECORD_NOT_FOUND')
			  {
			   for(var j =0;j<pagedData.result.accountNumberList.length;j++)
			   {
				   
				   if(this.accountNumberList[i]["accountType"] == pagedData.result.accountNumberList[j]["accountType"])
				   {
					 this.accountNumberList[i]["accountDescription"] = pagedData.result.accountNumberList[j]["accountDescription"].split(',');
					 this.accountNumberList[i]["accountNumberIndex"] = pagedData.result.accountNumberList[j]["accountNumberIndex"];
					 this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = pagedData.result.accountNumberList[j]["accountNumber"].split('-');
					 var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
					
					 for(var k=0;k<currentRowSegment.length;k++)
					 {
						  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
						  if(k > 0)
						  {
							   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
						  }
						  segment.disabled=false;
						  if(currentRowSegment[k] == 0)
						  {
							  segment.value = '';
						  }	
						  else{
							segment.value = currentRowSegment[k];
						  }
					 }
				   }
			   }
			  }
			  else{
				  
			  }
		   }
		});
	}
	// Serachable Select Box Step4
	getClassByID(item:any){
	   this.classId=[];
       this.classId.push({'id':item.id,'itemName':item.id});
	  this.getClassDescription(item.id);
	  this.commonService.closeMultiselectWithId("classId")
    }
	OnClassIdDeSelect(item:any){
	   this.description ='';
	   this.arabicDescription ='';
	    for(var i=0;i<this.accountNumberList.length;i++)
		{
			if(this.accountNumberList[i]["accountNumberIndex"]["arrSegment"])
			{
			var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
			 for(var k=0;k<currentRowSegment.length;k++)
			 {
				  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
				  if(k > 0)
				  {
					   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
					   segment.disabled=true;
				  }
				  else{
					segment.disabled=false;  	
				  }
				  segment.value = '';
			 }
			}
			this.accountNumberList[i]["accountDescription"] =[];	
			this.accountNumberList[i]["accountNumberIndex"] =[];
	}
	this.commonService.closeMultiselectWithId("classId")
	}

	 /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
	   LockScreen(writeAccess)
	   {
		   if(!writeAccess)
		   {
			   return true
		   }
		   else if(this.btndisabled)
		   {
			   return true
		   }
		   else if(this.isScreenLock)
		   {
			   return true;
		   }
		   else{
			   return false;
		   }
	   }
	// Serachable Select Box Step4 End 
}