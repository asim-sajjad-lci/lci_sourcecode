
import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { AccountReceivableSetupComponent } from './accountsReceivablesSetup/accounts-receivables-setup.component';
import { AccountsReceivablesClassComponent } from './accountsReceivablesClassSetup/accounts-receivables-class-setup.component';
import { AccountReceivableOptionSetupComponent } from './accountReceivableOptionSetup/account-receivable-option-setup.component';
import { AccountReceivableClassGLSetupComponent } from './accountReceivablesClassGLSetup/account-receivables-classGL-setup.component';
import { AccountsReceivableRoutingModule } from './accounts-receivables-setup-routing.module';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    AccountsReceivableRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule
  ],
  declarations: [AccountReceivableSetupComponent,AccountsReceivablesClassComponent,AccountReceivableOptionSetupComponent,AccountReceivableClassGLSetupComponent,AutoFocusDirective]
})
export class AccountsReceivableModule { }
