import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountReceivableSetup } from '../../../../financialModule/_models/accounts-receivables-setup/accounts-receivables-setup';
import { VatDetailSetupService } from '../../../../financialModule/_services/company-setup/vat-detail-setup.service';
import { AccountReceivableService } from '../../../../financialModule/_services/accounts-receivables-setup/accounts-receivables-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../_sharedresource/Constants';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';

@Component({
    selector: '<accounts-receivables></accounts-receivables>',
    templateUrl: './accounts-receivables-setup.component.html',
    styles: ["user.component.css"],
    providers:[AccountReceivableService,VatDetailSetupService,CommonService]
})

//export to make it available for other classes
export class AccountReceivableSetupComponent {
    page = new Page();
    rows = new Array<AccountReceivableSetup>();
    ArrBankId = [];
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    AccountReceivableSetup = {};
    id: string;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean=false;
    ArrApplyByOption=[];
    exchangeDate;
	exchangeExpireDate;
	exchangeTime='';
    checkbookoptions;
    salesVatDetailOption;
    ageingByOptions;
    minRange:string;
    maxRange:string;
    currentFrom:string;
    currentTo:string;
    isEdit: boolean =false;
    agingIndex:number;
    OldMaxLimit:number;
    dtoRMPeriodSetupsList = [];
    lastDateBalanceForwardAge: boolean =false;
     compoundFinanceCharge: boolean =false;
     arTrackingDiscountAvailable: boolean =false;
     deleteUnpostedPrintedDocuments: boolean =false;
     printHistoricalAgedTrialBalance: boolean =false;
    arPayCommissionsInvoicePay: boolean =false;
        // Serachable Select Box Step1
	ArrCheckBookId = [];
    arrVatDetailId=[];
    checkbookId : any = [];
    salesVatScheduleId: any = [];
    freightVatScheduleId: any = [];
    miscVatScheduleId: any = [];
    ddlCheckBookIdSetting = {};
      // Serachable Select Box Step1 ends
     onePeriod= Constants.onePeriod;
     twoPeriod= Constants.twoPeriod;
     toGreaterThan= Constants.toGreaterThan;
     toValue= Constants.toValue;
     toPeriod= Constants.toPeriod;
     lessThan= Constants.lessThan;
     greaterThan= Constants.greaterThan;
     fromPeriod= Constants.fromPeriod;
     maxrangeGreater= Constants.maxrangeGreater;
     search=Constants.search;
     maximumRange= Constants.maximumRange;
     minrange= Constants.minrange;
     minimumRange= Constants.minimumRange;
     agingPeriod= Constants.agingPeriod;
     sevenAgingPeriod= Constants.sevenAgingPeriod;
     toLessThan= Constants.toLessThan;

     btndisabled:boolean=false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private vatDetailSetupService: VatDetailSetupService,
        private accountReceivableService:AccountReceivableService,
        private commonService:CommonService
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
         this.model.ageingBy=1;
    }

    ngOnInit() {
        this.getAddScreenDetail();
        this.getCheckbookList();
        this.getVatDetail();
        window.setTimeout(() => {
            this.getAccountRecievableSetup();
        }, 500);
    
       this.getmasterAgingListAccountReceivable();
       this.getApplyByoption();

       this.ddlCheckBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 
    }

    
    
    
   getCheckbookList()
   {
       this.accountReceivableService.getListForCheckbook().then(data => {
              this.checkbookoptions = data.result.records;
            //   this.checkbookoptions.splice(0, 0, { "checkBookId": "", "checkbookDescription": this.select });
                  // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrCheckBookId.push({ "id": data.result.records[i].checkBookId, "itemName": data.result.records[i].checkBookId})
              }
			    // Searchable Select Box Step3 End
       });
   }
   getmasterAgingListAccountReceivable()
   {
       this.accountReceivableService.getmasterAgingListAccountReceivable().then(data => {
              this.ageingByOptions = data.result;
       });
   }

        // Serachable Select Box Step4
    getCheckBookByID(item:any){
        this.checkbookId=[];
        this.checkbookId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("checkbookId")
        
    }
    OnCheckBookIdDeSelect(item:any){
        this.checkbookId=[];
        this.commonService.closeMultiselectWithId("checkbookId")
    }


    getApplyByoption()
    {
       this.accountReceivableService.getdefaultListAccountReceivable().then(data => {
         this.ArrApplyByOption = data.result;
       });
    }

    getVatDetail()
    {
        
        this.vatDetailSetupService.getVatDetailSetup().subscribe(data => {
             
             this.salesVatDetailOption=data.records;
            
           	 for(var i=0;i< data.records.length;i++)
              {
                this.arrVatDetailId.push({ "id": data.records[i].vatScheduleId, "itemName": data.records[i].vatDescription})
              }
        });
    }
    
    RemoveArrValue(RowIndex)
    {
        this.dtoRMPeriodSetupsList.splice(RowIndex, 1);
    }

     // set sales Vat ScheduleId selected Value
    getSaleVatDetailByID(item:any){
        this.salesVatScheduleId=[];
        this.salesVatScheduleId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("salesVatScheduleId")
    }

    // set sales Vat ScheduleId DeSelected 
    OnSaleVatDetailDeSelect(item:any){
        this.salesVatScheduleId=[];
        this.commonService.closeMultiselectWithId("salesVatScheduleId")
    }

       // set Freight ScheduleId selected Value
    onSelectFreightVatScheduleId(item:any){
        this.freightVatScheduleId=[];
        this.freightVatScheduleId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("freightVatScheduleId");
    }

    // set  Freight ScheduleId DeSelected 
    OnDeSelectFreightVatScheduleId(item:any){
        this.freightVatScheduleId=[];
        this.commonService.closeMultiselectWithId("freightVatScheduleId");
    }

      // set Misc Vat ScheduleId selected Value
    onSelectMiscVatScheduleId(item:any){
        this.miscVatScheduleId=[];
        this.miscVatScheduleId.push({ "id": item.id, "itemName": item.itemName})
        this.commonService.closeMultiselectWithId("miscVatScheduleId")
    }

    // set Misc Vat ScheduleId DeSelected 
    OnDeSelectMiscVatScheduleId(item:any){
        this.miscVatScheduleId=[];
        this.commonService.closeMultiselectWithId("miscVatScheduleId")
    }

    validateRange(actionType)
    {
        this.btndisabled=false;
        if(this.isEdit)
        {
            return false;
        }
        if(this.dtoRMPeriodSetupsList.length == 7)
        {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.sevenAgingPeriod;
             window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
              }, 4000);
          return false;
        }

        if(this.OldMaxLimit == 1000)
        {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = this.agingPeriod;
             window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
              }, 4000);
           return false;
        }
        if(actionType == 'minRange')
        {
            if(!this.minRange)
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.minimumRange;
              //  document.getElementById("txtminRange").focus();
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                return false;
            }
            if(this.dtoRMPeriodSetupsList.length > 0 && this.OldMaxLimit <  parseInt(this.minRange))
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.minrange + this.OldMaxLimit;
                //document.getElementById("txtminRange").focus();
                this.minRange =this.OldMaxLimit.toString();
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                 return false;
            }
        }
        else if(actionType == 'maxRange')
        {
            if(!this.maxRange)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.maximumRange;
             //  document.getElementById("txtmaxRange").focus();
                window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
            if(this.dtoRMPeriodSetupsList.length > 0 && parseInt(this.maxRange) <  parseInt(this.minRange))
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.maxrangeGreater + this.minRange;
              //document.getElementById("txtmaxRange").focus();
              this.maxRange ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
        }
        else if(actionType == 'currentFrom')
        {
            if(!this.currentFrom)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.fromPeriod;
             // document.getElementById("txtcurrentFrom").focus();
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
            
            if(parseInt(this.currentFrom) <  parseInt(this.minRange))
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.greaterThan + this.minRange;
              //  document.getElementById("txtcurrentFrom").focus();
                this.currentFrom ="";
                 window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
            }, 4000);
                return false;
            }
            else if(parseInt(this.currentFrom) >  parseInt(this.maxRange))
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.lessThan + this.maxRange;
           //   document.getElementById("txtcurrentFrom").focus();
              this.currentFrom ="";
               window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
            }, 4000);
              return false;
            }
        }
        else if(actionType == 'currentTo')
        {
            if(!this.currentTo)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.toPeriod;
              //document.getElementById("txtcurrentTo").focus();
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
              
            }

            if(parseInt(this.currentTo) >  999)
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.toValue + this.maxRange;
             // document.getElementById("txtcurrentTo").focus();
              this.currentTo ="999";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }
            if(parseInt(this.currentTo) <  parseInt(this.currentFrom))
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.toGreaterThan + this.maxRange;
             //   document.getElementById("txtcurrentTo").focus();
                this.currentTo ="";
                 window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                return false;
            }
            else if(parseInt(this.currentTo) >  parseInt(this.maxRange))
            {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.toLessThan + this.maxRange;
            //  document.getElementById("txtcurrentTo").focus();
              this.currentTo ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
              return false;
            }

       
            this.dtoRMPeriodSetupsList.push({"periodDescription":this.minRange +'-' + this.maxRange,"periodNoofDays":this.currentFrom,"periodEnd":this.currentTo})
            this.OldMaxLimit = parseInt(this.currentTo) + 1;
            this.minRange = "";
            this.maxRange = "";
            this.currentFrom = "";
            this.currentTo = "";

           
        }

    }

    ModifyAgingPeriod(idx:number,item:any)
    {
        this.btndisabled=false;
        var range =item.periodDescription.split('-');
        this.minRange = range[0];
        this.maxRange=range[1];
        this.currentFrom = item.periodNoofDays;
        this.currentTo=item.periodEnd;
        this.agingIndex=idx;
        this.isEdit=true;
    }

    UpdateAgingPeriod()
    {
        this.btndisabled=false;
        this.dtoRMPeriodSetupsList[this.agingIndex]["periodDescription"] = this.minRange +'-' + this.maxRange ;
        this.dtoRMPeriodSetupsList[this.agingIndex]["periodNoofDays"] = this.currentFrom ;
        this.dtoRMPeriodSetupsList[this.agingIndex]["periodEnd"] = this.currentTo ;
        this.OldMaxLimit = parseInt(this.currentTo) + 1;
        this.minRange = "";
        this.maxRange = "";
        this.currentFrom = "";
        this.currentTo = "";
        this.agingIndex=0;
        this.isEdit=false;
    }
    CancelAgingPeriod()
    {
        this.btndisabled=false;
        this.minRange = "";
        this.maxRange = "";
        this.currentFrom = "";
        this.currentTo = "";
        this.agingIndex=0;
        this.isEdit=false;
    }

    
    getAddScreenDetail()
    {
         this.screenCode = "S-1149";
         this.defaultAddFormValues = [
                    { 'fieldName': 'ADD_ACC_REC_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_DEF_PRICE_LEVEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                    { 'fieldName': 'ADD_ACC_REC_PASSWORDS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_EXCEED_CREDIT_LIMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_REMOVE_CUSTOMER_HOLD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' },
                    { 'fieldName': 'ADD_ACC_REC_EXCEED_MAX_WRITEOFF', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_WAIVE_FINANCE_CHARGE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_APPLY_BY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_DUE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_TRANSACTION_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_OPTIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_PRI_HIS_TRIAL_BAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_COM_FIN_CHARGE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_DEL_UNPOSTED_DOC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_TRACKING_DIS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_PAY_COMM_AFTER_INVOICE_PAY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_DEF_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_SALES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_FREIGHT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_MISCELLANEOUS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_DATES_LAST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_LAST_FINANCE_CHARGE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_LAST_DATE_STATEMENT_PRINTED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_DEL_UNPOSTED_DOC_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_LAST_DATE_BAL_FORWARD_AGE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_USER_DEFINE_FIELDS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_USER_DEFINE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_AGING_PERIODS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_DUE_DATE_AGING', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_TRANSACTION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_CURRENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_0-30_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_31-60_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_60_OVER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':''},
         ];
         this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
         this.getScreenDetail(this.screenCode,'Add');
    }
    
    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    onlastDateBalanceForwardAgeChanged(event: IMyDateModel): void {
         this.model.lastDateBalanceForwardAge='';
    }
    
     ondeleteUnpostedPrintedDocumentDateChanged(event: IMyDateModel): void {
         this.model.deleteUnpostedPrintedDocumentDate='';
    }
    onlastDateStatementPrintedChanged(event: IMyDateModel): void {
         this.model.lastDateStatementPrinted='';
    }
    
     onlastFinanceChargeDateChanged(event: IMyDateModel): void {
         this.model.lastFinanceChargeDate='';
    }

    //setting pagination
   
   getAccountRecievableSetup()
   {
       
     this.checkbookId=[];
     this.freightVatScheduleId=[];
     this.miscVatScheduleId=[];
     this.salesVatScheduleId=[];
     this.accountReceivableService.getAccountRecievableSetup().then(data => {
         
         
         if(data.code == 302)
         {
            
            var selectedBook = this.ArrCheckBookId.find(x => x.id ==  data.result.checkbookId);
            this.checkbookId.push({ "id": selectedBook.id, "itemName": selectedBook.itemName});

            var	selectedfreightVatSchedule = this.arrVatDetailId.find(x => x.id ==  data.result.freightVatScheduleId);
			this.freightVatScheduleId.push({'id':selectedfreightVatSchedule.id,'itemName':selectedfreightVatSchedule.itemName});

            var	selectedmiscVatSchedule = this.arrVatDetailId.find(x => x.id ==  data.result.miscVatScheduleId);
			this.miscVatScheduleId.push({'id':selectedmiscVatSchedule.id,'itemName':selectedmiscVatSchedule.itemName});

            var	selectedSaleVatSchedule = this.arrVatDetailId.find(x => x.id ==  data.result.salesVatScheduleId);
			this.salesVatScheduleId.push({'id':selectedSaleVatSchedule.id,'itemName':selectedSaleVatSchedule.itemName});

            this.model = data.result;
            this.dtoRMPeriodSetupsList=data.result.dtoRMPeriodSetupsList;

            if(data.result.lastDateBalanceForwardAge) {
              this.model.lastDateBalanceForwardAge=data.result.lastDateBalanceForwardAge;
            }
            if(data.result.lastFinanceChargeDate) {
              this.model.lastFinanceChargeDate =data.result.lastFinanceChargeDate;
            }
            if(data.result.lastDateStatementPrinted) {
              this.model.lastDateStatementPrinted = data.result.lastDateStatementPrinted;
            }
            if(data.result.deleteUnpostedPrintedDocumentDate) {
              this.model.deleteUnpostedPrintedDocumentDate =data.result.deleteUnpostedPrintedDocumentDate;
            }
            var lastDateBalanceForwardAge = this.model.lastDateBalanceForwardAge;
            var lastDateBalanceForwardAgeData = lastDateBalanceForwardAge.split('/');
            this.model.lastDateBalanceForwardAge = {"date":{"year":parseInt(lastDateBalanceForwardAgeData[2]),"month":parseInt(lastDateBalanceForwardAgeData[1]),"day":parseInt(lastDateBalanceForwardAgeData[0])}};

            var lastFinanceChargeDate = this.model.lastFinanceChargeDate;
            var lastFinanceChargeDateData = lastFinanceChargeDate.split('/');
            this.model.lastFinanceChargeDate = {"date":{"year":parseInt(lastFinanceChargeDateData[2]),"month":parseInt(lastFinanceChargeDateData[1]),"day":parseInt(lastFinanceChargeDateData[0])}};
            
            var lastDateStatementPrinted = this.model.lastDateStatementPrinted;
            var lastDateStatementPrintedData = lastDateStatementPrinted.split('/');
            this.model.lastDateStatementPrinted = {"date":{"year":parseInt(lastDateStatementPrintedData[2]),"month":parseInt(lastDateStatementPrintedData[1]),"day":parseInt(lastDateStatementPrintedData[0])}};

            var deleteUnpostedPrintedDocumentDate = this.model.deleteUnpostedPrintedDocumentDate;
            var deleteUnpostedPrintedDocumentDateData = deleteUnpostedPrintedDocumentDate.split('/');
            this.model.deleteUnpostedPrintedDocumentDate = {"date":{"year":parseInt(deleteUnpostedPrintedDocumentDateData[2]),"month":parseInt(deleteUnpostedPrintedDocumentDateData[1]),"day":parseInt(deleteUnpostedPrintedDocumentDateData[0])}};

             this.arPayCommissionsInvoicePay=data.result.arPayCommissionsInvoicePay;
             this.compoundFinanceCharge=data.result.compoundFinanceCharge;
             this.arTrackingDiscountAvailable=data.result.arTrackingDiscountAvailable;
             this.deleteUnpostedPrintedDocuments=data.result.deleteUnpostedPrintedDocuments;
             this.model.printHistoricalAgedTrialBalance=data.result.printHistoricalAgedTrialBalance;
                
         }
      });
   }
  DateBalanceOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year:0, month: 0, day: 0 }
   };
   deleteUnpostedPrintedDocumentDateOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year:0, month: 0, day: 0 }
    };
    lastDateStatementPrintedOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year:0, month: 0, day: 0 }
    };
    lastFinanceChargeDateOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year:0, month: 0, day: 0 }
    };

    
    CreateAccountReceivable(f: NgForm) {
     
        this.btndisabled=true;
       if(this.model.lastDateBalanceForwardAge.formatted == undefined)
       {
          var lastDateBalanceForwardAge = this.model.lastDateBalanceForwardAge;
          if(lastDateBalanceForwardAge.date != undefined)
          {
             this.model.lastDateBalanceForwardAge = lastDateBalanceForwardAge.date.day +'/'+ lastDateBalanceForwardAge.date.month +'/'+ lastDateBalanceForwardAge.date.year;
          }
         
       }
       else
       {
             this.model.lastDateBalanceForwardAge = this.model.lastDateBalanceForwardAge.formatted;
       }

       if(this.model.lastDateStatementPrinted.formatted == undefined)
       {
          var lastDateStatementPrinted = this.model.lastDateStatementPrinted;
          if(lastDateStatementPrinted.date != undefined)
          {
             this.model.lastDateStatementPrinted = lastDateStatementPrinted.date.day +'/'+ lastDateStatementPrinted.date.month +'/'+ lastDateStatementPrinted.date.year;
          }
       }
       else
       {
             this.model.lastDateStatementPrinted = this.model.lastDateStatementPrinted.formatted;
       }

       if(this.model.deleteUnpostedPrintedDocumentDate.formatted == undefined)
       {
          var deleteUnpostedPrintedDocumentDate = this.model.deleteUnpostedPrintedDocumentDate;
          if(deleteUnpostedPrintedDocumentDate.date != undefined)
          {
             this.model.deleteUnpostedPrintedDocumentDate = deleteUnpostedPrintedDocumentDate.date.day +'/'+ deleteUnpostedPrintedDocumentDate.date.month +'/'+ deleteUnpostedPrintedDocumentDate.date.year;
          }
       }
       else
       {
             this.model.deleteUnpostedPrintedDocumentDate = this.model.deleteUnpostedPrintedDocumentDate.formatted;
       }

       if(this.model.lastFinanceChargeDate.formatted == undefined)
       {
          var lastFinanceChargeDate = this.model.lastFinanceChargeDate;
          if(lastFinanceChargeDate.date != undefined)
          {
             this.model.lastFinanceChargeDate = lastFinanceChargeDate.date.day +'/'+ lastFinanceChargeDate.date.month +'/'+ lastFinanceChargeDate.date.year;
          }
         
       }
       else
       {
             this.model.lastFinanceChargeDate = this.model.lastFinanceChargeDate.formatted;
       }
       this.model.dtoRMPeriodSetupsList=this.dtoRMPeriodSetupsList;
       this.model.arPayCommissionsInvoicePay=this.arPayCommissionsInvoicePay;
       this.model.compoundFinanceCharge=this.compoundFinanceCharge;
       this.model.arTrackingDiscountAvailable=this.arTrackingDiscountAvailable;
       this.model.deleteUnpostedPrintedDocuments=this.deleteUnpostedPrintedDocuments;
       this.model.printHistoricalAgedTrialBalance=this.printHistoricalAgedTrialBalance;
       
       this.model.checkbookId=this.checkbookId[0].id;
       this.model.miscVatScheduleId=this.miscVatScheduleId[0].id;
       this.model.salesVatScheduleId=this.salesVatScheduleId[0].id;
       this.model.freightVatScheduleId=this.freightVatScheduleId[0].id;
       
        if(this.model.ageingBy == 1)
        { 
            if(this.model.dtoRMPeriodSetupsList.length < 1)
            {
              window.scrollTo(0,0);
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.onePeriod;
              this.currentTo ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                  return false;
            }
          

        }
        else if(this.model.ageingBy == 2){
            if(this.model.dtoRMPeriodSetupsList.length < 2)
            {
              window.scrollTo(0,0);
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.twoPeriod;
              this.currentTo ="";
               window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                }, 4000);
                 return false;
            }
           
        }
       this.accountReceivableService.saveAccountRecievableSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 200 || datacode == 201) {
                    this.isModify=false;
                    this.model.bankId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.getAccountRecievableSetup();
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        
    }
    
    redirect(){
         this.router.navigate(['accountreceivablesetup/accountreceivableoptionsetup']);
    }
    
    
    Clear(f){
        this.salesVatScheduleId=[];
        this.freightVatScheduleId=[];
        this.miscVatScheduleId=[];
        this.checkbookId=[];
        this.dtoRMPeriodSetupsList=[];
        this.model.ageingBy=[];
        this.btndisabled=false;
     f.resetForm();
    }
    
    clearStartDate(){
		this.exchangeDate ='';
	}
	clearStartDate1(){
		this.exchangeExpireDate ='';
	}
	formatstartTime(selectedTime){
		this.exchangeTime = moment(selectedTime).format('HH:mm a');
    }
    
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

    /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
	   LockScreen(writeAccess)
	   {
		   if(!writeAccess)
		   {
			   return true
		   }
		   else if(this.btndisabled)
		   {
			   return true
		   }
		   else if(this.isScreenLock)
		   {
			   return true;
		   }
		   else{
			   return false;
		   }
	   }
	


}
