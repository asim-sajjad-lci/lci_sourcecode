import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountReceivableOptionSetup } from '../../../../financialModule/_models/accounts-receivables-setup/accounts-receivable-option-setup';
import { AccountReceivableOptionService } from '../../../../financialModule/_services/accounts-receivables-option-setup/accounts-receivables-option-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../_sharedresource/Constants';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';

@Component({
    selector: '<accounts-receivables></accounts-receivables>',
    templateUrl: './account-receivable-option-setup.component.html',
    styles: ["user.component.css"],
    providers:[AccountReceivableOptionService]
})

//export to make it available for other classes
export class AccountReceivableOptionSetupComponent {
    page = new Page();
    rows = new Array<AccountReceivableOptionSetup>();
    ArrBankId = [];
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    AccountReceivableOptionSetup = {};
    id: string;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    isScreenLock;
    atATimeText=Constants.atATimeText;
    msgText=Constants.msgText;
    isModify:boolean=false;
    ArrApplyByOption=[];
    exchangeDate;
	exchangeExpireDate;
	exchangeTime='';
    checkbookoptions;
    ageingByOptions;
    minRange:string;
    maxRange:string;
    currentFrom:string;
    currentTo:string;
    OldMaxLimit:number;
    rmDocumentTypeList = [];
    lastDateBalanceForwardAge: boolean =false;
    compoundFinanceCharge: boolean =false;
    arTrackingDiscountAvailable: boolean =false;
    deleteUnpostedPrintedDocuments: boolean =false;
    printHistoricalAgedTrialBalance: boolean =false;
    arPayCommissionsInvoicePay: boolean =false;
    docType:string;
    documentTypeDescription:string;
    documentTypeDescriptionArabic:string;
    documentCode:string;
    documentNumber:string;
    isLatNumberDisable: boolean =false;
    fillAllFields=Constants.fillAllFields;
    btndisabled:boolean=false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private AccountReceivableOptionService:AccountReceivableOptionService,
         
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
    }

     ngOnInit() {
       this.getAddScreenDetail();
       this.getAccountRecievableSetup();
    }
    
    getAddScreenDetail()
    {
         this.screenCode = "S-1182";
         this.defaultAddFormValues = [
                    { 'fieldName': 'ADD_ACC_REC_OPTION_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_OPTION_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_OPTION_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_OPTION_NEXT_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_OPTION_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_REC_OPTION_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
         ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
         this.getScreenDetail(this.screenCode,'Add');
    }
    // docType:string;
    // documentTypeDescription:string;
    // documentTypeDescriptionArabic:string;
    // documentCode:string;
    // :string;
    validateArrValue(ActionType)
    {
        if(ActionType == "documentNumber")
        {
            if(this.docType != "" && this.documentTypeDescription != ""  && this.documentCode != "" && this.documentNumber != "") 
            {
                this.rmDocumentTypeList.push({"documentTypeId": "","docType": this.docType,"documentTypeDescription": this.documentTypeDescription,"documentTypeDescriptionArabic": this.documentTypeDescriptionArabic,"documentCode": this.documentCode,"documentNumber": this.documentNumber});
                this.isLatNumberDisable=false;
                this.docType="";
                this.documentTypeDescription = "";
                this.documentTypeDescriptionArabic = "";
                this.documentCode = "";
                this.documentNumber = "";
            }
             else{
                 
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.fillAllFields;
                 window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
            }
        }
        else if(ActionType == "documentCode")
        {
            var getSameCodeList=this.rmDocumentTypeList.filter( obj => obj.documentCode.toString().toLowerCase() == this.documentCode.toString().toLowerCase().trim())
                                            .map( obj => obj.documentNumber);
            if(getSameCodeList.length > 0)
            {
                this.isLatNumberDisable=true;
                var getSameCodeListDesc= getSameCodeList.reverse();
                var maxNextNumber= getSameCodeListDesc[0];
                this.documentNumber =(parseInt(maxNextNumber) + 1).toString();
            }
            else{
                 this.isLatNumberDisable=false;
                 this.documentNumber = '1';
            }
        }
    }
    RemoveArrValue(RowIndex)
    {
         this.rmDocumentTypeList.splice(RowIndex, 1);

    }
    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    //setting pagination
   
   getAccountRecievableSetup()
   {
     this.AccountReceivableOptionService.getAccountRecievableOptionSetup().then(data => {
         if(data.code != 404)
         {
            this.model.rmDocumentTypeList = data.result.records;
            this.rmDocumentTypeList = data.result.records;
         }
      });
   }
    
    CreateAccountReceivable() {
        this.btndisabled=true;
        if(this.rmDocumentTypeList.length>0){
            this.model.rmDocumentTypeList=this.rmDocumentTypeList;
       this.AccountReceivableOptionService.saveAccountRecievableSetup(this.model).then(data => {
           window.scrollTo(0,0);
           var datacode = data.code;
           this.btndisabled=false;
           if (datacode == 200 || datacode == 201) {
               this.isModify=false;
               this.model.bankId=null;
                   this.isSuccessMsg = true;
                   this.isfailureMsg = false;
                   this.hasMsg = true;
                   this.showMsg = true;
                   this.messageText = data.btiMessage.message;
                   this.getAccountRecievableSetup();
                   window.setTimeout(() => {
                   this.showMsg = false;
                   this.hasMsg = false;
               }, 4000);
           }
         
       }).catch(error => {
           window.setTimeout(() => {
               this.isSuccessMsg = false;
               this.isfailureMsg = true;
               this.showMsg = true;
               this.hasMsg = true;
               this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
           }, 100)
       });
        }
        else{
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = this.fillAllFields;
                 window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
        }
        this.btndisabled=false;
    }
    
    Clear(){
        this.docType='';
        this.documentTypeDescription='';
        this.documentCode='';
        this.documentNumber='';
      this.rmDocumentTypeList=[]
    }
    
    clearStartDate(){
		this.exchangeDate ='';

	}
	clearStartDate1(){
		this.exchangeExpireDate ='';
	}
	formatstartTime(selectedTime){
		this.exchangeTime = moment(selectedTime).format('HH:mm a');
	}
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
}
            else{
                return false;
            }
        }
        
    }
    