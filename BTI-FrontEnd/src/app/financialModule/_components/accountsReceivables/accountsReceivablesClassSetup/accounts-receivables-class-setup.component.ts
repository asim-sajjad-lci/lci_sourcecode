import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {AccountRecievables} from '../../../../financialModule/_models/accounts-receivables-setup/accounts-receivables-class-setup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {AccountReceivableClassSetupService} from '../../../../financialModule/_services/accountReceivables/accountReceivablesClassSetup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
declare var $: any;

@Component({
   selector: 'account-receivables-class-set-up',
   templateUrl: './accounts-receivables-class-setup.component.html', 
    providers: [AccountReceivableClassSetupService,CommonService],
})

export class AccountsReceivablesClassComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	myHtml;
	btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
	page = new Page();
	rows = new Array<AccountRecievables>();
    temp = new Array<AccountRecievables>();
	Searchselected = [];
	isScreenLock;
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	tableViewtext=Constants.tableViewtext;
    currentClassId:string;
	// Serachable Select Box Step1
	ArrSalesmanId = [];
    salesmanId : any = [];
    ddlSalesmanIdSetting = {};

	ArrPaymentTermId = [];
    paymentTermId : any = [];
    ddlPaymentTermIdSetting = {};
	
	ArrSalesTerritoryId = [];
    salesTerritoryId : any = [];
    ddlSalesTerritoryIdSetting = {};

	ArrShipmentMethodId = [];
    shipmentMethodId : any = [];
    ddlShipmentMethodIdSetting = {};

	
	ArrCheckbookId = [];
    checkbookId : any = [];
    ddlCheckbookIdSetting = {};

	ArrVatId = [];
    vatId : any = [];
    ddlVatIdSetting = {};
    disableButton;
	ArrCurrencyId = [];
    currencyId : any = [];
    ddlCurrencyIdSetting = {};
	EmptyMessage = Constants.EmptyMessage;
	search=Constants.search;
	isPercent1:boolean = false
	isPercent2:boolean = false
	isPercent3:boolean = false
    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	payments;
	shippingMethod;
	vat;
	currency;
	salesTerritory;
	checkbook;
	disableId;
	ftextDisabled;
	mtextDisabled;
	ctextDisabled;
	salesman;
	valueToChange;
	changedValue;
	selectedCurrency;
	unFrmtfinanceChargeAmount;
	unFrmtminimumChargeAmount;
	unFrmtCreditLimitAmount;
	unFrmtTradeDiscountPercent;
	inputValue;
	editfinanceChargeAmount;
	editMinimumChargeAmount;
	editCreditLimitAmount ;
	editTradeDiscountPercent;
	financeChargeAmountValue;
	minimumChargeAmountValue;
	creditLimitAmountValue;
	TradeDiscountPercentValue
	btndisabled:boolean=false;
	accountReceivableClass=
	{
		'customerClassId' : '',
		'customerClassDescription' : '',
		'customeClassDescriptionArabic' : '',
		'balanceType' : '',
		'financeCharge' : '',
		'financeChargeAmount' : '',
		'minimumCharge' : '',
		'minimumChargeAmount' : '',
		'creditLimit' : 0,
		'creditLimitAmount' : '',
		'tradeDiscountPercent' : '',
		'paymentTermId' : '',
		'shipmentMethodId' : '',
		'vatId' : '',
		'currencyId' : '',
		'priceLevelId' : '',
		'salesmanId' : '',
		'salesTerritoryId' : '',
		'checkbookId' : '',
		'customerPriority' : '',
		'userDefine1' : '',
		'userDefine2' : '',
		'userDefine3' : '',
		'openMaintenanceHistoryCalendarYear' : false,
		'openMaintenanceHistoryTransaction' : false,
		'openMaintenanceHistoryFiscalYear' : false,
		'openMaintenanceHistoryDistribution' : false
	}
	
	
	constructor(private router: Router,private route: ActivatedRoute,private accountReceivableClassSetupService: AccountReceivableClassSetupService,private getScreenDetailService: GetScreenDetailService,private commonService:CommonService) {
		this.disableButton = true;
		this.add = true;
		this.disableId = false;
		this.ftextDisabled = false;
		this.mtextDisabled = false;
		this.ctextDisabled = false;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.payments=[];
		this.shippingMethod=[];
		this.vat=[];
		this.currency=[];
		this.salesTerritory=[];
		this.checkbook=[];
		this.salesman=[];
		
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_ACC_REC_CLASS_CLASS_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_REC_CLASS_DESC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_REC_CLASS_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_ACC_REC_CLASS_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_ACC_REC_CLASS_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}, 
            { 'fieldName': 'MANAGE_ACC_REC_CLASS_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
		];
		
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1158").then(data => {
			
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_BALANCE_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_OPEN_ITEM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_BALANCE_FORWARD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_FINANCE_CHARGE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_FINANCE_CHARGE_NONE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_FINANCE_CHARGE_PERCENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_FINANCE_CHARGE_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_MIN_PAYMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_MIN_PAYMENT_NO_MIN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_MIN_PAYMENT_PERCENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_MIN_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CREDIT_LIMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CREDIT_LIMIT_NO_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CREDIT_LIMIT_UNLIMITED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CREDIT_LIMIT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_TRADE_DIS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_PAYMENT_TERM_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_SHIP_METHOD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_PRICE_LEVEL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_SALESMAN_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_TERRITORY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_ACC_REC_CLASS_PRIORITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_ACC_REC_CLASS_MAINTAIN_HISTORY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CALENDAR_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_ACC_REC_CLASS_FISCAL_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_ACC_REC_CLASS_TRANSACTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_ACC_REC_CLASS_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_ACC_REC_CLASS_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_ACC_REC_CLASS_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_ACC_REC_CLASS_USER_DEFINE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_ACC_REC_CLASS_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },		
				{ 'fieldName': 'ADD_ACC_REC_CLASS_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },		
				{ 'fieldName': 'ADD_ACC_REC_CLASS_ACCOUNTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }				
				];

				this.getScreenDetailService.ValidateScreen("S-1157").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1157").then(data => {
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
	}

	ngOnInit() {

		
		
		// Serachable Select Box Step2
		this.ddlSalesmanIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.ddlPaymentTermIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.ddlSalesTerritoryIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.ddlShipmentMethodIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.ddlCheckbookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.ddlVatIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.ddlCurrencyIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.getPayment();
		this.getShippingMethod();
		this.getVatDetail();
		this.getCurrency();
		this.getSalesMan();
		this.getSalesTerritory();
		this.getCheckbook();
	}	

	

	getPayment()
	{
		this.accountReceivableClassSetupService.searchPayment().subscribe(pagedData => {
			this.payments = pagedData.records;
			
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrPaymentTermId.push({ "id": pagedData.records[i].paymentTermId, "itemName": pagedData.records[i].description})
              }
			    // Searchable Select Box Step3 End
        });
	}

	getShippingMethod()
	{
		//fetch shipping method records 
		this.accountReceivableClassSetupService.searchShippingMethod().subscribe(pagedData => {
            this.shippingMethod = pagedData.records;
			                // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrShipmentMethodId.push({ "id": pagedData.records[i].shipmentMethodId, "itemName": pagedData.records[i].shipmentDescription})
              }
			    // Searchable Select Box Step3 End
        });
	}
	getVatDetail()
	{
			//fetch vat method records 
		this.accountReceivableClassSetupService.searchVat().subscribe(pagedData => {
            this.vat = pagedData.records;
			                // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrVatId.push({ "id": pagedData.records[i].vatScheduleId, "itemName": pagedData.records[i].vatDescription})
              }
			    // Searchable Select Box Step3 End
        });
	}

	getCurrency()
	{
			//fetch currency records 
		this.accountReceivableClassSetupService.searchCurrency().subscribe(pagedData => {
            this.currency = pagedData.records;
			                // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrCurrencyId.push({ "id": pagedData.records[i].currencyId, "itemName": pagedData.records[i].currencyDescription})
              }
			    // Searchable Select Box Step3 End
        });
	}
	getSalesMan()
	{
		//fetch Salesman records 
		this.accountReceivableClassSetupService.searchSalesman().subscribe(pagedData => {
            this.salesman = pagedData.records;
			                // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrSalesmanId.push({ "id": pagedData.records[i].salesmanId, "itemName": pagedData.records[i].salesmanFirstName})
              }
			    // Searchable Select Box Step3 End
        });
	}
	getSalesTerritory()
	{
			//fetch sales terrotory records 
		this.accountReceivableClassSetupService.searchsalesTerritory().subscribe(pagedData => {
            this.salesTerritory = pagedData.records;
			                // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrSalesTerritoryId.push({ "id": pagedData.records[i].salesTerritoryId, "itemName": pagedData.records[i].descriptionPrimary})
              }
			    // Searchable Select Box Step3 End
        });
	}

	getCheckbook()
	{
		//fetch checkbook records 
		this.accountReceivableClassSetupService.searchCheckbook().subscribe(pagedData => {
            this.checkbook = pagedData.records;
			                // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrCheckbookId.push({ "id": pagedData.records[i].checkBookId, "itemName": pagedData.records[i].checkBookId})
              }
			    // Searchable Select Box Step3 End
        });
	}

	updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.accountReceivableClassSetupService.searchAccountReceivablesClass(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

	//Is id already exist
    IsIdAlreadyExist(){
		if(this.accountReceivableClass.customerClassId != '' && this.accountReceivableClass.customerClassId != undefined && this.accountReceivableClass.customerClassId != 'undefined')
        {
			this.accountReceivableClassSetupService.getAccountReceivableClass(this.accountReceivableClass.customerClassId).then(data => {
				
              var datacode = data.code;
                if (data.status == "NOT_FOUND") {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("ddl_insClassId");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.accountReceivableClass.customerClassId='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
}

     // Serachable Select Box Step4
    getSalesmanByID(item:any){
		this.accountReceivableClass.salesmanId=item.id;
		this.commonService.closeMultiselectWithId("salesmanId")

    }
    OnSalesmanIdDeSelect(item:any){
		this.accountReceivableClass.salesmanId='';
		this.commonService.closeMultiselectWithId("salesmanId")
    }

	     // Serachable Select Box Step4
    getPaymentTermByID(item:any){
		this.accountReceivableClass.paymentTermId=item.id;
		this.commonService.closeMultiselectWithId("paymentTermId")
    }
    OnPaymentTermIdDeSelect(item:any){
		this.accountReceivableClass.paymentTermId='';
		this.commonService.closeMultiselectWithId("paymentTermId")
    }

	     // Serachable Select Box Step4
    getSalesTerritoryByID(item:any){
		this.accountReceivableClass.salesTerritoryId=item.id;
		this.commonService.closeMultiselectWithId("salesTerritoryId")
    }
    OnSalesTerritoryIdDeSelect(item:any){
		this.accountReceivableClass.salesTerritoryId='';
		this.commonService.closeMultiselectWithId("salesTerritoryId")
    }

	     // Serachable Select Box Step4
    getShipmentMethodByID(item:any){
		this.accountReceivableClass.shipmentMethodId=item.id;
		this.commonService.closeMultiselectWithId("shipmentMethodId")
    }
    OnShipmentMethodIdDeSelect(item:any){
		this.accountReceivableClass.shipmentMethodId='';
		this.commonService.closeMultiselectWithId("shipmentMethodId")
    }

	     // Serachable Select Box Step4
    getCheckbookByID(item:any){
		this.accountReceivableClass.checkbookId=item.id;
		this.commonService.closeMultiselectWithId("checkbookId")
    }
    OnCheckbookIdDeSelect(item:any){
		this.accountReceivableClass.checkbookId='';
		this.commonService.closeMultiselectWithId("checkbookId")
    }

	     // Serachable Select Box Step4
    getVatByID(item:any){
		this.accountReceivableClass.vatId=item.id;
		this.commonService.closeMultiselectWithId("vatId")
    }
    OnVatIdDeSelect(item:any){
		this.accountReceivableClass.vatId='';
		this.commonService.closeMultiselectWithId("vatId")
    }

	     // Serachable Select Box Step4
   	getCurrencyByID(CurrencyId:string){
		this.selectedCurrency = this.accountReceivableClass.currencyId=CurrencyId
		this.commonService.closeMultiselectWithId("currencyId")
		if(this.accountReceivableClass.financeChargeAmount.length > 0){
			this.formatfinanceChargeAmount()
		}
		if(this.accountReceivableClass.minimumChargeAmount.length > 0){
			this.formatminimumChargeAmount()
		}
		if(this.accountReceivableClass.creditLimitAmount.length > 0){
			this.formatCreditLimitAmount()
		}
		if(this.accountReceivableClass.tradeDiscountPercent.length > 0){
			this.formatTradeDiscountPercent()
		}
		
	}
	formatfinanceChargeAmount(){
		this.commonService.changeInputText("financeChargeAmount"
	)
		var inputValue = 0;
		
		if(!this.add){
			if(this.editfinanceChargeAmount == 'undefined' && this.unFrmtfinanceChargeAmount){
				inputValue = this.unFrmtfinanceChargeAmount
			}
			else if(this.editfinanceChargeAmount == 'undefined'){
				inputValue = this.records.financeChargeAmount
			}else{
                inputValue = this.editfinanceChargeAmount
            }
		}else{
			 inputValue = this.unFrmtfinanceChargeAmount
		}
		
			this.commonService.changeInputText("financeChargeAmount")
			var inputValue = 0;
			if(!this.add){
				if(this.editfinanceChargeAmount == 'undefined'&& this.unFrmtfinanceChargeAmount){
					inputValue = this.unFrmtfinanceChargeAmount
				}
				else if(this.editfinanceChargeAmount == 'undefined'){
					inputValue = this.records.financeChargeAmount
				}else{
					inputValue = this.editfinanceChargeAmount
				}
				this.financeChargeAmountValue = inputValue
			}else{
				inputValue = this.unFrmtfinanceChargeAmount
			}
			if(this.isPercent1 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.accountReceivableClass.financeChargeAmount = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatfinanceChargeAmountChanged()
			}
		}
	}
	formatfinanceChargeAmountChanged(){
		
			this.commonService.changeInputNumber("financeChargeAmount")
			if(!this.add){
				if(this.editMinimumChargeAmount == 'undefined'){
					this.accountReceivableClass.financeChargeAmount = this.records.financeChargeAmount
				}else{
					this.accountReceivableClass.financeChargeAmount = this.editfinanceChargeAmount
				}
			}else{
				this.accountReceivableClass.financeChargeAmount = this.unFrmtfinanceChargeAmount
			}
		}
	
	getfinanceChargeAmountValue(event){
		this.editfinanceChargeAmount = 'undefined'
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		
		this.unFrmtfinanceChargeAmount = event
	}

	//minimumChargeAmount
	//add Symbols
	formatminimumChargeAmount(){
		
		this.commonService.changeInputText("minimumChargeAmount")
		var inputValue = 0;
		if(!this.add){
			if (this.editMinimumChargeAmount == 'undefined' && this.unFrmtminimumChargeAmount){
				inputValue = this.unFrmtminimumChargeAmount
			}
			else if(this.editMinimumChargeAmount == 'undefined'){
				inputValue = this.records.minimumChargeAmount
			}else{
				inputValue = this.editMinimumChargeAmount
			}
			this.minimumChargeAmountValue =  inputValue
		}else{
			inputValue = this.unFrmtminimumChargeAmount
		}
		if(this.isPercent2 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.accountReceivableClass.minimumChargeAmount = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatminimumChargeAmountChanged()
			}
		}
	}
	//Change To Original Value of minimumChargeAmount
	formatminimumChargeAmountChanged(){
		this.commonService.changeInputNumber("minimumChargeAmount")
			if(!this.add){
				if(this.editMinimumChargeAmount == 'undefined'){
					this.accountReceivableClass.minimumChargeAmount = this.records.minimumChargeAmount
				}else{
					this.accountReceivableClass.minimumChargeAmount = this.editMinimumChargeAmount
				}
			}else{
				
			this.accountReceivableClass.minimumChargeAmount = this.unFrmtminimumChargeAmount
			}
		}
	
	// get Original Minimum Charge Amount
	getminimumChargeAmountValue(event){
		this.editMinimumChargeAmount = 'undefined'
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		
		this.unFrmtminimumChargeAmount = event
	}
	//End MinimumChargeAmount

	//Credit Limit Amount
	//add Symbols
	formatCreditLimitAmount(){
		this.commonService.changeInputText("creditLimitAmount")
		var inputValue = 0;
		if(!this.add){
			if (this.editCreditLimitAmount == 'undefined' && this.unFrmtCreditLimitAmount ){
				inputValue = this.unFrmtCreditLimitAmount
			}
			else if(this.editCreditLimitAmount == 'undefined'){
                inputValue = this.records.creditLimitAmount
            }else{
                inputValue = this.editCreditLimitAmount
            }
			this.creditLimitAmountValue = inputValue
		}else{
			inputValue = this.unFrmtCreditLimitAmount
		}
		if(this.isPercent3 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.accountReceivableClass.creditLimitAmount = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatCreditLimitAmountChanged()
			}
		}
	}
	//Change To Original Value of Credit Limit Amount
	formatCreditLimitAmountChanged(){
		this.commonService.changeInputNumber("creditLimitAmount")
		if(this.records){
			if(this.editCreditLimitAmount == 'undefined'){
                this.accountReceivableClass.creditLimitAmount = this.records.creditLimitAmount
            }else{
                this.accountReceivableClass.creditLimitAmount = this.editCreditLimitAmount
            }
		}else{
			this.accountReceivableClass.creditLimitAmount = this.unFrmtCreditLimitAmount
		}
		
		
	}
	// get Original Credit Limit Amount
	getCreditLimitAmountValue(event){
		this.editCreditLimitAmount = 'undefined'
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		this.unFrmtCreditLimitAmount = event
	}
	//End CreditLimitAmount

    getTradeDiscountPercent(event){
		this.editTradeDiscountPercent = 'undefined'
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		this.unFrmtTradeDiscountPercent = event

	}
	formatTradeDiscountPercent(){
		this.commonService.changeInputText("tradeDiscountPercent")
		var inputValue = 0;
		if(!this.add){
			if (this.editTradeDiscountPercent == 'undefined' && this.unFrmtTradeDiscountPercent ){
				inputValue = this.unFrmtTradeDiscountPercent
			}
			else if(this.editTradeDiscountPercent == 'undefined'){
                inputValue = this.records.tradeDiscountPercent
            }else{
                inputValue = this.editTradeDiscountPercent
            }
			this.TradeDiscountPercentValue = inputValue
		}else{
			inputValue = this.unFrmtTradeDiscountPercent
		}
		if(this.isPercent3 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.accountReceivableClass.tradeDiscountPercent = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatTradeDiscountPercentChanged()
			}
		}

	}
	formatTradeDiscountPercentChanged(){
		this.commonService.changeInputNumber("tradeDiscountPercent")
		if(this.records){
			if(this.editTradeDiscountPercent == 'undefined'){
                this.accountReceivableClass.tradeDiscountPercent = this.records.tradeDiscountPercent
            }else{
                this.accountReceivableClass.tradeDiscountPercent = this.editTradeDiscountPercent
            }
		}else{
			this.accountReceivableClass.tradeDiscountPercent = this.unFrmtTradeDiscountPercent
		}
	}
	
    OnCurrencyIdDeSelect(item:any){
		this.formatfinanceChargeAmountChanged();
		this.formatCreditLimitAmountChanged();
		this.formatminimumChargeAmountChanged();
		this.formatTradeDiscountPercentChanged(); 
		this.accountReceivableClass.currencyId='';
		this.commonService.closeMultiselectWithId("currencyId")
    }
    
	redirect(currentClassId){
	  this.router.navigate(['accountreceivablesetup/accountreceivableclassglsetup',this.currentClassId]);
	}
	

	save(accountReceivableClassSetup: NgForm){
		this.btndisabled=true;
		if(this.add){
			if(this.accountReceivableClass.openMaintenanceHistoryCalendarYear){
			var calenderYear = '1';
		}else{
			var calenderYear = '0';
		}
		
		if(this.accountReceivableClass.openMaintenanceHistoryTransaction){
			var transaction = '1';
		}else{
			var transaction = '0';
		}
		
		if(this.accountReceivableClass.openMaintenanceHistoryFiscalYear){
			var fisYear = '1';
		}else{
			var fisYear = '0';
		}
		
		if(this.accountReceivableClass.openMaintenanceHistoryDistribution){
			var distribution = '1';
		}else{
			var distribution = '0';
		}
		var balanceType="1";
		if(this.accountReceivableClass.balanceType)
		{
 			balanceType=this.accountReceivableClass.balanceType;
		}
		let submittedData =
		{
			'customerClassId' : this.accountReceivableClass.customerClassId,
			'customerClassDescription' : this.accountReceivableClass.customerClassDescription,
			'customeClassDescriptionArabic' : this.accountReceivableClass.customeClassDescriptionArabic,
			'balanceType' : balanceType,
			'financeCharge' : this.accountReceivableClass.financeCharge,
			'financeChargeAmount' : this.unFrmtfinanceChargeAmount,
			'minimumCharge' : this.accountReceivableClass.minimumCharge,
			'minimumChargeAmount' : this.unFrmtminimumChargeAmount,
			'creditLimit' : this.accountReceivableClass.creditLimit,
			'creditLimitAmount' : this.unFrmtCreditLimitAmount,
			'tradeDiscountPercent' : this.unFrmtTradeDiscountPercent,
			'paymentTermId' : this.accountReceivableClass.paymentTermId,
			'shipmentMethodId' : this.accountReceivableClass.shipmentMethodId,
			'vatId' : this.accountReceivableClass.vatId,
			'currencyId' : this.currencyId[0].id,
			'priceLevelId' : this.accountReceivableClass.priceLevelId,
			'salesmanId' : this.accountReceivableClass.salesmanId,
			'salesTerritoryId' : this.accountReceivableClass.salesTerritoryId,
			'checkbookId' : this.accountReceivableClass.checkbookId,
			'customerPriority' : this.accountReceivableClass.customerPriority,
			'userDefine1' : this.accountReceivableClass.userDefine1,
			'userDefine2' : this.accountReceivableClass.userDefine2,
			'userDefine3' : this.accountReceivableClass.userDefine3,
			'openMaintenanceHistoryCalendarYear' : calenderYear,
			'openMaintenanceHistoryTransaction' : transaction,
			'openMaintenanceHistoryFiscalYear' : fisYear,
			'openMaintenanceHistoryDistribution' : distribution
		};
	
		this.accountReceivableClassSetupService.createAccountReceivablesAccountSetup(submittedData).then(data => {
		this.showBtns=false;	
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				window.scrollTo(0,0);
				this.setPage({ offset: 0 });
				this.messageText = data.btiMessage.message;
			    accountReceivableClassSetup.resetForm();
				this.paymentTermId=[];
				this.shipmentMethodId=[];
				this.vatId=[];
				this.currencyId=[];
				this.salesmanId=[];
				this.salesTerritoryId=[];
				this.checkbookId=[];
				this.setPage({ offset: 0 });
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		}else{
			if(this.accountReceivableClass.openMaintenanceHistoryCalendarYear){
			var calenderYear = '1';
		}else{
			var calenderYear = '0';
		}
		
		if(this.accountReceivableClass.openMaintenanceHistoryTransaction){
			var transaction = '1';
		}else{
			var transaction = '0';
		}
		
		if(this.accountReceivableClass.openMaintenanceHistoryFiscalYear){
			var fisYear = '1';
		}else{
			var fisYear = '0';
		}
		
		if(this.accountReceivableClass.openMaintenanceHistoryDistribution){
			var distribution = '1';
		}else{
			var distribution = '0';
		}
		var balanceType="1";
		if(this.accountReceivableClass.balanceType)
		{
 			balanceType=this.accountReceivableClass.balanceType;
		}
		let submittedData =
		{
			'customerClassId' : this.accountReceivableClass.customerClassId,
			'customerClassDescription' : this.accountReceivableClass.customerClassDescription,
			'customeClassDescriptionArabic' : this.accountReceivableClass.customeClassDescriptionArabic,
			'balanceType' : balanceType,
			'financeCharge' : this.accountReceivableClass.financeCharge,
			'financeChargeAmount' : this.financeChargeAmountValue,
			'minimumCharge' : this.accountReceivableClass.minimumCharge,
			'minimumChargeAmount' : this.minimumChargeAmountValue,
			'creditLimit' : this.accountReceivableClass.creditLimit,
			'creditLimitAmount' : this.creditLimitAmountValue,
			'tradeDiscountPercent' : this.TradeDiscountPercentValue,
			'paymentTermId' : this.accountReceivableClass.paymentTermId,
			'shipmentMethodId' : this.accountReceivableClass.shipmentMethodId,
			'vatId' : this.accountReceivableClass.vatId,
			'currencyId' : this.currencyId[0].id,
			'priceLevelId' : this.accountReceivableClass.priceLevelId,
			'salesmanId' : this.accountReceivableClass.salesmanId,
			'salesTerritoryId' : this.accountReceivableClass.salesTerritoryId,
			'checkbookId' : this.accountReceivableClass.checkbookId,
			'customerPriority' : this.accountReceivableClass.customerPriority,
			'userDefine1' : this.accountReceivableClass.userDefine1,
			'userDefine2' : this.accountReceivableClass.userDefine2,
			'userDefine3' : this.accountReceivableClass.userDefine3,
			'openMaintenanceHistoryCalendarYear' : calenderYear,
			'openMaintenanceHistoryTransaction' : transaction,
			'openMaintenanceHistoryFiscalYear' : fisYear,
			'openMaintenanceHistoryDistribution' : distribution
		};
	
		this.accountReceivableClassSetupService.updateAccountReceivablesAccountSetup(submittedData).then(data => {
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 200) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				window.scrollTo(0,0);
				this.setPage({ offset: 0 });
				this.messageText = data.btiMessage.message;
				this.showBtns=false;
			    accountReceivableClassSetup.resetForm();
				this.disableButton = true;
				this.paymentTermId=[];
				this.shipmentMethodId=[];
				this.vatId=[];
				this.currencyId=[];
				this.salesmanId=[];
				this.salesTerritoryId=[];
				this.checkbookId=[];
				this.add =true;
				this.disableId = false;
				this.accountReceivableClass=
				{
					'customerClassId' : '',
					'customerClassDescription' : '',
					'customeClassDescriptionArabic' : '',
					'balanceType' : '',
					'financeCharge' : '',
					'financeChargeAmount' : '',
					'minimumCharge' : '',
					'minimumChargeAmount' : '',
					'creditLimit' : 0,
					'creditLimitAmount' : '',
					'tradeDiscountPercent' : '',
					'paymentTermId' : '',
					'shipmentMethodId' : '',
					'vatId' : '',
					'currencyId' : '',
					'priceLevelId' : '',
					'salesmanId' : '',
					'salesTerritoryId' : '',
					'checkbookId' : '',
					'customerPriority' : '',
					'userDefine1' : '',
					'userDefine2' : '',
					'userDefine3' : '',
					'openMaintenanceHistoryCalendarYear' : false,
					'openMaintenanceHistoryTransaction' : false,
					'openMaintenanceHistoryFiscalYear' : false,
					'openMaintenanceHistoryDistribution' : false
				}
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		}
		this.disableButton=false;
	}
	
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.accountReceivableClassSetupService.searchAccountReceivablesClass(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	
	edit(row : any){
		this.paymentTermId=[];
		this.shipmentMethodId=[];
		this.vatId=[];
		this.disableButton = false;
		this.currencyId=[];
		this.salesmanId=[];
		this.salesTerritoryId=[];
		this.checkbookId=[];
		this.add = false;
		this.disableId = true;
		this.showBtns=true;
		this.accountReceivableClassSetupService.getAccountReceivableClass(row.customerClassId).then(data => {
			this.records = data.result;
			this.currentClassId = row.customerClassId;
			this.records = data.result;
			
			
			var selectedPaymentTerm = this.ArrPaymentTermId.find(x => x.id ==   data.result.paymentTermId);
			this.paymentTermId.push({'id':selectedPaymentTerm.id,'itemName':selectedPaymentTerm.itemName}); 

			var	selectedVat = this.ArrVatId.find(x => x.id ==  data.result.vatId);
			this.vatId.push({'id':selectedVat.id,'itemName':selectedVat.itemName});
			var	selectedCurrency = this.ArrCurrencyId.find(x => x.id ==  data.result.currencyId);
			if(selectedCurrency)
			{
				this.currencyId.push({'id':selectedCurrency.id,'itemName':selectedCurrency.itemName});
			}
			var	selectedSalesman = this.ArrSalesmanId.find(x => x.id ==  data.result.salesmanId);
	        this.salesmanId.push({'id':selectedSalesman.id,'itemName':selectedSalesman.itemName});
			
			var	selectedSalesTerritory = this.ArrSalesTerritoryId.find(x => x.id ==  data.result.salesTerritoryId);
            this.salesTerritoryId.push({'id':selectedSalesTerritory.id,'itemName':selectedSalesTerritory.itemName});

			var selectedCheckBook = this.ArrCheckbookId.find(x => x.id ==  data.result.checkbookId);
	        this.checkbookId.push({'id':selectedCheckBook.id,'itemName':selectedCheckBook.itemName});

			var selectedShipmentMethod = this.ArrShipmentMethodId.find(x => x.id ==   data.result.shipmentMethodId);
			this.shipmentMethodId.push({'id':selectedShipmentMethod.id,'itemName':selectedShipmentMethod.itemName}); 
			this.editfinanceChargeAmount = this.records.financeChargeAmount
			this.editMinimumChargeAmount = this.records.minimumChargeAmount
			this.editCreditLimitAmount = this.records.creditLimitAmount
			this.editTradeDiscountPercent = this.records.tradeDiscountPercent
			if(this.records.financeCharge == '1'){
				this.isPercent1 = true
			}
			if(this.records.minimumCharge == '1'){
				this.isPercent2 = true	
			}
			if(this.records.creditLimit == '1'){
				this.isPercent3 = true
			}
			this.selectedCurrency = data.result.currencyId;
			this.formatfinanceChargeAmount();
			this.formatminimumChargeAmount();
			this.formatCreditLimitAmount();
			this.formatTradeDiscountPercent();
			// this.shipmentMethodId.push({'id':data.result.shipmentMethodId,'itemName':data.result.shipmentDescription}); 
			// this.vatId.push({'id':data.result.vatScheduleId,'itemName':data.result.vatDescription}); 
			// this.currencyId.push({'id':data.result.currencyId,'itemName':data.result.currencyDescription}); 
			// this.salesmanId.push({'id':data.result.salesmanId,'itemName':data.result.salesmanFirstName}); 
			// this.salesTerritoryId.push({'id':data.result.salesTerritoryId,'itemName':data.result.descriptionPrimary}); 
			// this.checkbookId.push({'id':data.result.checkBookId,'itemName':data.result.checkbookDescription}); 
		
			if(this.records.openMaintenanceHistoryCalendarYear==1){
				var histCal = true;
			}else{
				var histCal = false;
			}
			if(this.records.openMaintenanceHistoryTransaction==1){
				var histTrans = true;
			}else{
				var histTrans = false;
			}
			if(this.records.openMaintenanceHistoryFiscalYear==1){
				var histFisc = true;
			}else{
				var histFisc = false;
			}
			if(this.records.openMaintenanceHistoryDistribution==1){
				var histDis = true;
			}else{
				var histDis = false;
			}

			this.accountReceivableClass=
			{
				'customerClassId' : this.records.customerClassId,
				'customerClassDescription' : this.records.customerClassDescription,
				'customeClassDescriptionArabic' : this.records.customeClassDescriptionArabic,
				'balanceType' : this.records.balanceType.toString(),
				'financeCharge' : this.records.financeCharge.toString(),
				'financeChargeAmount' : this.records.financeChargeAmount,
				'minimumCharge' : this.records.minimumCharge.toString(),
				'minimumChargeAmount' : this.records.minimumChargeAmount,
				'creditLimit' : this.records.creditLimit.toString(),
				'creditLimitAmount' : this.records.creditLimitAmount,
				'tradeDiscountPercent' : this.records.tradeDiscountPercent,
				'paymentTermId' : this.records.paymentTermId,
				'shipmentMethodId' : this.records.shipmentMethodId,
				'vatId' : this.records.vatId,
				'currencyId' : this.records.currencyId,
				'priceLevelId' : this.records.priceLevelId,
				'salesmanId' : this.records.salesmanId,
				'salesTerritoryId' : this.records.salesTerritoryId,
				'checkbookId' : this.records.checkbookId,
				'customerPriority' : this.records.customerPriority,
				'userDefine1' : this.records.userDefine1,
				'userDefine2' : this.records.userDefine2,
				'userDefine3' : this.records.userDefine3,
				'openMaintenanceHistoryCalendarYear' : histCal,
				'openMaintenanceHistoryTransaction' : histTrans,
				'openMaintenanceHistoryFiscalYear' : histFisc,
				'openMaintenanceHistoryDistribution' : histDis
			}
		});
	}
	
	set(value){
		if(value=="none"){
			this.ftextDisabled = true;
			this.accountReceivableClass.financeChargeAmount='';
			
		}else if(value=="percent"){
			this.isPercent1 = true
			this.ftextDisabled = false;
		}else{
			this.isPercent1 = false
			this.ftextDisabled = false;
		}
	}
	set1(value){
	    if(value=="none"){
			this.mtextDisabled = true;
			this.accountReceivableClass.minimumChargeAmount='';
			
		}else if(value=="percent"){
			this.isPercent2 = true
			this.mtextDisabled = false;
		}else{
			this.isPercent2 = false
			this.mtextDisabled = false;
		}
	}
	set2(value){
		if(value=="none"){
			this.ctextDisabled = true;
			this.accountReceivableClass.creditLimitAmount='';
			
		}else if(value=="unlimited"){
			this.isPercent3 = true
			this.ctextDisabled = true;
			this.accountReceivableClass.creditLimitAmount='';
		}else{
			this.isPercent3 = false
			this.ctextDisabled = false;
		}
	}
	clearForm(accountReceivableClassSetup:NgForm){
	      accountReceivableClassSetup.resetForm();
		  this.paymentTermId=[];
		  this.shipmentMethodId=[];
		  this.vatId=[];
		  this.currencyId=[];
		  this.salesmanId=[];
		  this.salesTerritoryId=[];
		  this.checkbookId=[];
		  this.unFrmtCreditLimitAmount = ''
		  this.unFrmtfinanceChargeAmount = ''
		  this.unFrmtminimumChargeAmount = ''
		  this.unFrmtTradeDiscountPercent = '' 
		  this.editfinanceChargeAmount = ''
		  this.editMinimumChargeAmount = ''
		  this.editCreditLimitAmount = ''
		  this.editTradeDiscountPercent = ''
		  this.selectedCurrency = undefined
		  this.isPercent1   = false
		  this.isPercent2 = false
		  this.isPercent3 = false
		  if(this.records){
		  var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		  selectedValue.value=this.records.customerClassId;
		  this.accountReceivableClass.customerClassId = this.records.customerClassId;
				
			 if(!this.add){
				this.myHtml = "border-vanish";
			 }else{
				this.myHtml = "";
			 }
		}
	}

    Cancel(accountReceivableClassSetup)
		{
		this.add=true;
		this.disableButton = true;
		this.clearForm(accountReceivableClassSetup);
		 this.paymentTermId=[];
		  this.shipmentMethodId=[];
		  this.vatId=[];
		  this.currencyId=[];
		  this.salesmanId=[];
		  this.salesTerritoryId=[];
		  this.checkbookId=[];
		this.accountReceivableClass.customerClassId= '';
		this.disableId=false;
		 
		}

	onSelect({ Searchselected }) {
        this.Searchselected.splice(0, this.Searchselected.length);
        this.Searchselected.push(...Searchselected);
	}

	
	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
	
	/** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
	   LockScreen(writeAccess)
	   {
		   if(!writeAccess)
		   {
			   return true
		   }
		   else if(this.btndisabled)
		   {
			   return true
		   }
		   else if(this.isScreenLock)
		   {
			   return true;
		   }
		   else{
			   return false;
		   }
	   }
	
}