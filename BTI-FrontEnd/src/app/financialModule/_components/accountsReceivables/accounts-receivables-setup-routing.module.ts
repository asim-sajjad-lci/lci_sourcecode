import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountReceivableSetupComponent } from './accountsReceivablesSetup/accounts-receivables-setup.component';
import { AccountsReceivablesClassComponent } from './accountsReceivablesClassSetup/accounts-receivables-class-setup.component';
import { AccountReceivableOptionSetupComponent } from './accountReceivableOptionSetup/account-receivable-option-setup.component';
import { AccountReceivableClassGLSetupComponent } from './accountReceivablesClassGLSetup/account-receivables-classGL-setup.component';
const routes: Routes = [
   { path: '', component: AccountReceivableSetupComponent },
   {path:'accountreceivableclasssetup', component:AccountsReceivablesClassComponent},
   {path:'accountreceivableoptionsetup', component:AccountReceivableOptionSetupComponent},
   {path:'accountreceivableclassglsetup',component:AccountReceivableClassGLSetupComponent},
   {path:'accountreceivableclassglsetup/:customerClassId',component:AccountReceivableClassGLSetupComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountsReceivableRoutingModule { }
