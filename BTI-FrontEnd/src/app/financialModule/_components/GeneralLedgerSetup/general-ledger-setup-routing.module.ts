import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { PostingAccountSetupComponent } from './posting-accounts-setup/posting-accounts-setup.component';
import { ExchangeTableSetupComponent } from '././exchangetablesetup/exchange-table-setup.component';
import { CurrencySetupComponent } from './currency/currency-setup.component';
import { ExchangeTableRateDetailsComponent } from './exchangeTableRateDetails/exchange-table-rate-detail.component';
import { GeneralLedgerSetupComponent } from './generalLedgerSetup/general-ledger-setup.component';
import { FiscalFinancePeriodSetupComponent } from './fiscalFinancePeriodSetup/fiscal-finance-period-setup.component';
import { MassCloseFiscalFinancePeriodSetupComponent } from '../GeneralLedgerSetup/massCloseFiscalFinancePeriodSetup/mass-close-fiscal-finance-period-setup.component';
import { AccountStructureSetupComponent } from './account-structure-setup/account-structure-setup.component';
import { AccountCategoryComponent } from './GeneralLedgerConfigurationSetup/accounts-category/accounts-category.component';
import { AccountTypeComponent } from './GeneralLedgerConfigurationSetup/accounts-type/accounts-type.component';
import { AuditTrialComponent } from './GeneralLedgerConfigurationSetup/audit-trial/audit-trial.component';

const routes: Routes = [
   { path: '', component: GeneralLedgerSetupComponent },
  // { path: 'postingaccountsetup', component: PostingAccountSetupComponent },
  { path: 'currencysetup', component: CurrencySetupComponent },
  { path: 'exchangetablesetup', component: ExchangeTableSetupComponent },
  { path: 'exchangeratedetail', component:  ExchangeTableRateDetailsComponent },
  { path:'generalledgersetup', component:GeneralLedgerSetupComponent},
  {path:'fiscalfinanceperiodsetup', component:FiscalFinancePeriodSetupComponent},
  {path:'massclosefiscalfinanceperiod', component:MassCloseFiscalFinancePeriodSetupComponent},
  {path:'accountstructure', component:AccountStructureSetupComponent},
  { path: 'accountcategory', component: AccountCategoryComponent },
  { path: 'accounttype', component: AccountTypeComponent },
  {path:'audittrial', component:AuditTrialComponent},
  { path: 'exchangeratedetail/:exchangeId', component: ExchangeTableRateDetailsComponent },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralLedgerSetupRoutingModule { }
