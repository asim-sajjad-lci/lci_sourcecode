import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountCategory } from '../../../../../financialModule/_models/general-ledger-configuration-setup/account-category';
import { AccountCategoryService } from '../../../../../financialModule/_services/general-ledger-configuration-setup/account-category.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: './accounts-category.component.html',
    providers:[AccountCategoryService]
})

//export to make it available for other classes
export class AccountCategoryComponent {
    page = new Page();
    rows = new Array<AccountCategory>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isScreenLock;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    ArrDiscountTypePeriod=[];
    ArrDiscountType=[];
    ArrDueType=[];
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private AccountCategoryService:AccountCategoryService
        
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
    }
    
    ngOnInit() {
        
        this.getAddScreenDetail();
        this.getViewScreenDetail();
        this.setPage({ offset: 0 });
    }
    
    getAddScreenDetail()
    {
        this.screenCode = "S-1058";
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_ACCOUNT_CAT_ACC_CAT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_ACCOUNT_CAT_ACC_CAT_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_ACCOUNT_CAT_SAVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_ACCOUNT_CAT_CLEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetail(this.screenCode,'Add');
        }
        
        getViewScreenDetail()
        {
            this.screenCode = "S-1059";
            this.defaultManageFormValues = [
                { 'fieldName': 'MANAGE_ACCOUNT_CAT_ACC_CAT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_ACCOUNT_CAT_ACC_CAT_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_ACCOUNT_CAT_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_ACCOUNT_CAT_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'CATEGORY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
            ];
            this.getScreenDetail(this.screenCode,'Manage');
        }
        
        getScreenDetail(screenCode,ArrayType)
        {
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                this.screenName=data.result.dtoScreenDetail.screenName;
                this.moduleName=data.result.moduleName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                if(ArrayType == 'Add')
                {
                    
                    
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
                else
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
            });
        }
        
        // search rolegroup details by group name 
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.AccountCategoryService.searchGroup(this.page, this.searchKeyword).subscribe(pagedData => {
                
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            }, error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        resetMe(f)
        {
            f.resetForm();
        }
        
        
        Cancel(f)
        {
            this.isModify=false;
            this.resetMe(f);
        } 
        
        //setting pagination
        setPage(pageInfo) {
            this.selected = []; // remove any selected checkbox on paging
            this.page.pageNumber = pageInfo.offset;
            this.AccountCategoryService.searchAccountCategory(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        changePageSize(event) {
            this.page.size = event.target.value;
            this.setPage({ offset: 0 });
        }
        
        getaccountCategoryById(row: any)
        {
            this.showBtns=true;
            this.isModify=true;
            this.AccountCategoryService.getAccountCategoryById(row.accountCategoryId).then(data => {
                this.model = data.result; 
                
            });
        }
        
        //check Account Category already exist
        IsAccountCategoryAlreadyExist(){
            
            if(this.model.accountCategoryDescription != '' && this.model.accountCategoryDescription != undefined && this.model.accountCategoryDescription != 'undefined')
            {
                this.AccountCategoryService.GetAccountCategoryByNameUrl(this.model.accountCategoryDescription).then(data => {
                    var datacode = data.code;
                    if (data.status == "NOT_FOUND") {
                        return true;
                    }
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        window.scrollTo(0,0);
                        this.model.accountCategoryDescription='';
                        return false;
                    }
                    
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
        }
        
        
        saveaccountCategory(f: NgForm)
        {
            this.btndisabled=true;
            if(this.isModify)
            {
                this.AccountCategoryService.updateAccountCategory(this.model).then(data => {
                    var datacode = data.code;
                    this.btndisabled=false;
                    if (datacode == 200) {
                        this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            else
            {
                this.AccountCategoryService.createAccountCategory(this.model).then(data => {
                    var datacode = data.code;
                    this.btndisabled=false;
                    this.showBtns=false;
                    if (datacode == 200) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            
        }
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
    }