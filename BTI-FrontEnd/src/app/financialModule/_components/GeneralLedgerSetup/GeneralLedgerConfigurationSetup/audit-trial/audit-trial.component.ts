import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuditTrial } from '../../../../../financialModule/_models/general-ledger-configuration-setup/audit-trial';
import { AuditTrialService } from '../../../../../financialModule/_services/general-ledger-configuration-setup/audit-trial.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: './audit-trial.component.html',
    providers:[AuditTrialService]
})

//export to make it available for other classes
export class AuditTrialComponent {
    page = new Page();
    rows = new Array<AuditTrial>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    ArrDiscountTypePeriod=[];
    ArrDiscountType=[];
    ArrDueType=[];
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    seriesOptions=[];
    condition;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false; 
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private AuditTrialService:AuditTrialService
        
    ){
        this.condition = false;
        this.page.pageNumber = 0;
        this.page.size = 5;
    }
    
    ngOnInit() {
        
        this.getSeriesType();
        this.getAddScreenDetail();
        this.getViewScreenDetail();
        this.setPage({ offset: 0 });
        
        // this.seriesOptions.push({"seriesId" : "1","name":"GL"});
        // this.seriesOptions.push({"seriesId" : "2","name":"GL2"});
        // this.seriesOptions.push({"seriesId" : "3","name":"GL3"});
        
    }
    
    getAddScreenDetail()
    {
        this.screenCode = "S-1205";
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_AUDIT_TRIAL_SERIES_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_AUDIT_TRIAL_SOURCE_DOC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_AUDIT_TRIAL_SOURCE_CODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_AUDIT_TRIAL_SAVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_AUDIT_TRIAL_CLEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_AUDIT_TRIAL_SERIES_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetail(this.screenCode,'Add');
        }
        
        getViewScreenDetail()
        {
            this.screenCode = "S-1206";
            this.defaultManageFormValues = [
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_SERIES_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_SERIES_SEQ', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_SOURCE_DOC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_SOURCE_CODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_AUDIT_TRIAL_SERIES_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
            ];
            this.getScreenDetail(this.screenCode,'Manage');
        }
        
        getSeriesType()
        {
            // this.AuditTrialService.getAuditTrialSeries().then(data =>{
            //     this.seriesOptions=data.result;
            this.AuditTrialService.getAuditTrialSeries().then(data => {
                this.seriesOptions = data.result;
                this.seriesOptions.splice(0, 0,{"typeId": "", "name": this.select });
                this.model.seriesId='';
                
            });
        }
        
        getScreenDetail(screenCode,ArrayType)
        {
            
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                this.screenName=data.result.dtoScreenDetail.screenName;
                this.moduleName=data.result.moduleName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                if(ArrayType == 'Add')
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
                else
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
            });
        }
        
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.AuditTrialService.searchAuditTrial(this.page, this.searchKeyword).subscribe(pagedData => {
                
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            }, error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        resetMe(f){
            this.model.sourceCode='';
            this.model.sourceDocument='';
            this.model.seriesNumber='';
        }
        
        Cancel(f)
        {
            this.condition = false;
            this.isModify=false;
            this.model.seriesId='';
            this.resetMe(f);
        }
        
        //setting pagination
        setPage(pageInfo) {
            this.selected = []; // remove any selected checkbox on paging
            this.page.pageNumber = pageInfo.offset;
            this.AuditTrialService.searchAuditTrial(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        
        getAuditTrialById(row: any)
        { 
            this.showBtns=true; 
            this.condition = true;
            this.isModify=true;
            this.AuditTrialService.getAuditTrialById(row.seriesIndex).then(data => {
                this.model = data.result; 
            });
        }
        
        
        changePageSize(event) {
            this.page.size = event.target.value;
            this.setPage({ offset: 0 });
        }
        
        saveAuditTrial(f: NgForm)
        {
            this.btndisabled=true;
            if(this.isModify)
            {
                this.AuditTrialService.updateAuditTrial(this.model).then(data => {
                    window.scrollTo(0,0);
                    this.btndisabled=false;
                    var datacode = data.code;
                    if (datacode == 200) {
                        // this.setPage({ offset: 0 });
                        this.condition = false;
                        this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                    
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            else
            {
                this.AuditTrialService.createAuditTrial(this.model).then(data => {
                    window.scrollTo(0,0);
                    this.btndisabled=false;
                    this.showBtns=false;
                    var datacode = data.code;
                    if (datacode == 200) {
                        // this.setPage({ offset: 0 });
                        this.condition = false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        // this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
        }
        
        onlyDecimalNumberKey(event) {
            return this.getScreenDetailService.onlyDecimalNumberKey(event);
        }
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
    }