import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FiscalFinancePeriod } from '../../../../financialModule/_models/general-ledger-setup/fiscalFinancePeriodSetup';
import { FiscalFinancePeriodSetupService } from '../../../../financialModule/_services/general-ledger-setup/fiscalFinancePeriodSetup';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
declare var $: any;
import {ModalModule, Modal} from "ng2-modal";

@Component({
    templateUrl: './fiscal-finance-period-setup.component.html',
    providers:[FiscalFinancePeriodSetupService]
})

//export to make it available for other classes
export class FiscalFinancePeriodSetupComponent {
    page = new Page();
    rows = new Array<FiscalFinancePeriod>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    isScreenLock;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    ArrDiscountTypePeriod=[];
    ArrDiscountType=[];
    ArrDueType=[];
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean= false;
    historicalYear:boolean= false;
    periodSeriesFinancial:boolean= false;
    periodSeriesSales:boolean= false;
    periodSeriesPurchase:boolean= false;
    periodSeriesInventory:boolean= false;
    periodSeriesProject:boolean= false;
    periodSeriesPayroll:boolean= false;
    showCalander:boolean= false;
    finalMonth :string;
    finalDate :string;
    fullDate :string;
    btndisabled:boolean=false;
    arrYears :any;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private FiscalFinancePeriodSetupService:FiscalFinancePeriodSetupService
        
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
    }
    
    ngOnInit() {
        this.getAddScreenDetail();
        //   this.DateBalanceOptions={
        //     dateFormat: 'dd/mm/yyyy',
        //     minYear:2018,
        //     maxYear: 2018,
        //     disableUntil: {year:0, month: 0, day: 0 }
        //    // disableUntil: {year: 2016, month: 8, day: 10}
        
        // } 
        this.getYears();
    }
    getFiscalFinancePeriodSetup(f:NgForm)
    {
        this.model.numberOfPeriods='';
        this.DateBalanceOptions={
            dateFormat: 'dd/mm/yyyy',
            minYear: this.model.year,
            maxYear: this.model.year,
            disableUntil: {year:this.model.year, month: 0, day: 0 }
            //disableUntil: {year: 2016, month: 8, day: 10}
            
        } 
        
        this.FiscalFinancePeriodSetupService.getFiscalFinancePeriodByYear(this.model.year).then(data => {
            this.model.dtoFiscalFinancialPeriodSetupDetail=[];
            
            if(data.btiMessage.messageShort != 'RECORD_NOT_FOUND')
            {
                this.model = data.result;
                //    for(var i=0;i< this.model.dtoFiscalFinancialPeriodSetupDetail.length;i++)  
                //    {
                //        var StartDate = this.model.dtoFiscalFinancialPeriodSetupDetail[i]["startPeriodDate"];
                //        var ArrStartDay = StartDate.split('/');
                //        StartDate = {"date":{"year":parseInt(ArrStartDay[2]),"month":parseInt(ArrStartDay[1]),"day":parseInt(ArrStartDay[0])}};
                //        this.model.dtoFiscalFinancialPeriodSetupDetail[i]["startPeriodDate"] =  StartDate;
                //    }
                
                if(this.model.firstDay.formatted == undefined)
                {
                    var firstDay = this.model.firstDay;
                    if(firstDay.date != undefined)
                    {
                        this.model.firstDay = firstDay.date.day +'/'+ firstDay.date.month +'/'+ firstDay.date.year;
                    }
                }
                else
                {
                    this.model.firstDay = this.model.firstDay.formatted;
                }
                if(this.model.lastDay.formatted == undefined)
                {
                    var lastDay = this.model.lastDay;
                    if(lastDay.date != undefined)
                    {
                        this.model.lastDay = lastDay.date.day +'/'+ lastDay.date.month +'/'+ lastDay.date.year;
                    }
                }
                else
                {
                    this.model.lastDay = this.model.lastDay.formatted;
                }
                var firstDay = this.model.firstDay;
                var ArrfirstDay = firstDay.split('/');
                this.model.firstDay = {"date":{"year":parseInt(ArrfirstDay[2]),"month":parseInt(ArrfirstDay[1]),"day":parseInt(ArrfirstDay[0])}};
                
                var lastDay = this.model.lastDay;
                var ArrlastDay = lastDay.split('/');
                this.model.lastDay = {"date":{"year":parseInt(ArrlastDay[2]),"month":parseInt(ArrlastDay[1]),"day":parseInt(ArrlastDay[0])}};
            }
            else{
                f.resetForm();
                this.model.firstDay = {"date":{"year":this.model.year,"month":1,"day":1}};
                this.model.lastDay = {"date":{"year":this.model.year,"month":12,"day":31}};
            }
        });
    }
    
    getAddScreenDetail()
    {
        this.screenCode = "S-1117";
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_YEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_FIRST_DAY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_LAST_DAY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_HISTORICAL_YEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_NO_PERIOD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_OPEN_ALL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_CLOSE_ALL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_SERIES_MODULE_CLOSED', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_PERIOD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_PERIOD_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_FINANCIAL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_SALES', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_PURCHASE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_INVENTORY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_PROJECT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_PAYROLL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_OK', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_CALCULATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_REDISPLAY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_FIS_FIN_PRD_SETUP_MASSCLOSE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetail(this.screenCode,'Add');
        }
        
        getScreenDetail(screenCode,ArrayType)
        {
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                this.screenName=data.result.dtoScreenDetail.screenName;
                this.moduleName=data.result.moduleName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                if(ArrayType == 'Add')
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
                else
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
            });
        }
        
        getYears()
        {
            this.arrYears=[];
            var min = new Date().getFullYear() - 5,
            max = min + 20;
            
            for (var i = min; i<=max; i++){
                this.arrYears.push({'year':i});
            }
        }
        DateBalanceOptions: INgxMyDpOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            minYear: this.model.year,
            maxYear: this.model.year,
            disableUntil: {year:0, month: 0, day: 0 }
        };
        onDateChanged(event: IMyDateModel,ActionType): void {
            this.model.firstDay=event.date;
            this.validateDate('1');
        }
        
        onlastDateChanged(event: IMyDateModel,ActionType): void {
            this.model.lastDay=event.date;
            this.validateDate('2');
        }
        refreshCalendar()
        {
            this.DateBalanceOptions={
                dateFormat: 'dd/mm/yyyy',
                minYear: this.model.year,
                maxYear: this.model.year,
                disableUntil: {year:0, month: 0, day: 0 }
            }    
        }
        
        
        mydiff(date1,date2,interval) {
            var start = Math.floor(date1.getTime() / (3600 * 24 * 1000)); //days as integer from..
            var end = Math.floor(date2.getTime() / (3600 * 24 * 1000)); //days as integer from..
            var daysDiff = end - start; // exact dates
            
            var second=1000, minute=second*60, hour=minute*60, day=hour*24, week=day*7;
            // date1 = new Date(date1);
            // date2 = new Date(date2);
            var timediff = date2 - date1;
            if (isNaN(timediff)) return NaN;
            switch (interval) 
            {
                case "years": return date2.getFullYear() - date1.getFullYear();
                case "months": return (
                    ( date2.getFullYear() * 12 + date2.getMonth() )
                    -
                    ( date1.getFullYear() * 12 + date1.getMonth() )
                );
                case "weeks"  : return Math.floor(timediff / week);
                case "days"   : return Math.floor(timediff / day); 
                case "hours"  : return Math.floor(timediff / hour); 
                case "minutes": return Math.floor(timediff / minute);
                case "seconds": return Math.floor(timediff / second);
                default: return undefined;
            }
        }
        
        parseDate(str) {
            var mdy = str.split('/');
            return new Date(mdy[2],mdy[0] , mdy[1]);
        }
        
        daydiff(first, second) {
            return Math.round((second-first)/(1000*60*60*24));
        }
        
        validateDate(action)
        {
            if(this.model.firstDay.formatted == undefined)
            {
                var firstDay = this.model.firstDay;
                if(firstDay.date != undefined)
                {
                    this.model.firstDay = firstDay.date.day +'/'+ firstDay.date.month +'/'+ firstDay.date.year;
                }
                else if(firstDay.day != undefined)
                {
                    this.model.firstDay = firstDay.day +'/'+ firstDay.month +'/'+ firstDay.year;
                }
            }
            else
            {
                this.model.firstDay = this.model.firstDay.formatted;
            }
            if(this.model.lastDay.formatted == undefined)
            {
                var lastDay = this.model.lastDay;
                if(lastDay.date != undefined)
                {
                    this.model.lastDay = lastDay.date.day +'/'+ lastDay.date.month +'/'+ lastDay.date.year;
                }
                else if(lastDay.day != undefined)
                {
                    this.model.lastDay = lastDay.day +'/'+ lastDay.month +'/'+ lastDay.year;
                }
            }
            else
            {
                this.model.lastDay = this.model.lastDay.formatted;
            }
            var firstDay = this.model.firstDay.split('/');
            var lastDay =  this.model.lastDay.split('/');
            
            var firstDate=new Date();
            firstDate.setFullYear(firstDay[2],(firstDay[1] - 1 ),firstDay[0]);
            
            var secondDate=new Date();
            secondDate.setFullYear(lastDay[2],(lastDay[1] - 1 ),lastDay[0]);     
            
            if (firstDate > secondDate)
            {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.hasMsg = true;
                this.showMsg = true;
                if(action == '1')
                {
                    this.messageText = Constants.firstdateGreaterMsg;
                }
                else
                {
                    this.messageText = Constants.lastdateGreaterMsg;
                }
                
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                    if(action == '1')
                    {
                        this.model.firstDay = '';
                    }
                    else
                    {
                        this.model.lastDay = '';
                    }
                    
                }, 4000);
            }
        }
        
        dateDiffInDays = function (a, b) {
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            // Discard the time and time-zone information.
            var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
            var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
            return Math.floor((utc2 - utc1) / _MS_PER_DAY);
        }
        
        getSeriesDetail(f:NgForm)
        {
            if(f.valid)
            {
                this.model.dtoFiscalFinancialPeriodSetupDetail = [];
                var CurrentYear = this.model.year;
                this.model.year=CurrentYear;
                var date = new Date();
                
                if(this.model.firstDay.formatted == undefined)
                {
                    var firstDay = this.model.firstDay;
                    if(firstDay.date != undefined)
                    {
                        this.model.firstDay = firstDay.date.day +'/'+ firstDay.date.month +'/'+ firstDay.date.year;
                    }
                }
                else
                {
                    this.model.firstDay = this.model.firstDay.formatted;
                }
                if(this.model.lastDay.formatted == undefined)
                {
                    var lastDay = this.model.lastDay;
                    if(lastDay.date != undefined)
                    {
                        this.model.lastDay = lastDay.date.day +'/'+ lastDay.date.month +'/'+ lastDay.date.year;
                    }
                }
                else
                {
                    this.model.lastDay = this.model.lastDay.formatted;
                }
                var firstDay = this.model.firstDay;
                var lastDay =  this.model.lastDay;
                var endPeriodDate = this.model.firstDay;
                
                var startdateData = firstDay.split('/');
                firstDay= {"date":{"year":parseInt(startdateData[2]),"month":parseInt(startdateData[1]),"day":parseInt(startdateData[0])}};
                
                var enddateData = lastDay.split('/');
                lastDay = {"date":{"year":parseInt(enddateData[2]),"month":parseInt(enddateData[1]),"day":parseInt(enddateData[0])}};
                
                firstDay = firstDay.date.month +'/'+ firstDay.date.day +'/'+ firstDay.date.year;
                endPeriodDate = firstDay;
                lastDay = lastDay.date.month +'/'+ lastDay.date.day +'/'+ lastDay.date.year;
                var daydiff=this.daydiff(this.parseDate(firstDay),this.parseDate(lastDay)) + parseInt("1");
                
                var AddDays =Math.round(parseInt(daydiff.toString()) / parseInt(this.model.numberOfPeriods));
                //  firstDay = this.model.firstDay;
                // lastDay =  this.model.lastDay;
                
                for(var i=0;i< this.model.numberOfPeriods;i++)  
                {
                    if(i > 0)
                    {
                        firstDay = this.parseDate(firstDay);
                        var d = new Date(firstDay);
                        d.setDate(d.getDate() + parseInt(AddDays.toString()));
                        if(i==this.model.numberOfPeriods-1)
                        {
                            firstDay =  this.model.lastDay;
                            endPeriodDate = firstDay;
                        }
                        else{
                            
                            if(d.getMonth() == 0)
                            {
                                
                                firstDay =  12  + "/" + d.getDate() + "/" + this.model.year;
                            }
                            else{
                                firstDay =  d.getMonth()  + "/" + d.getDate() + "/" + d.getFullYear()
                            }
                        }
                    }
                    var startdateData = firstDay.split('/');
                    firstDay= {"date":{"year":parseInt(startdateData[2]),"month":parseInt(startdateData[0]),"day":parseInt(startdateData[1])}};
                    
                    var enddateData = lastDay.split('/');
                    lastDay = {"date":{"year":parseInt(enddateData[2]),"month":parseInt(enddateData[0]),"day":parseInt(enddateData[1])}};
                    if(i==this.model.numberOfPeriods-1)
                    {
                        
                        endPeriodDate = enddateData[0] + '/' + enddateData[1] + '/' + enddateData[2] 
                    }
                    else{
                        endPeriodDate = firstDay.date.month +'/'+ firstDay.date.day +'/'+ firstDay.date.year;
                    }
                    
                    firstDay = firstDay.date.day +'/'+ firstDay.date.month +'/'+ firstDay.date.year;
                    lastDay = lastDay.date.day +'/'+ lastDay.date.month +'/'+ lastDay.date.year;
                    
                    if(i>0)
                    {
                        // endPeriodDate = this.parseDate(endPeriodDate);
                        var d = new Date(endPeriodDate);
                        var newdate = new Date(d);
                        newdate.setDate(d.getDate() - 1)
                        //  d.setDate(d.getDate() - 1);
                        if(d.getMonth() == 0)
                        {
                            // endPeriodDate =  d.getDate()  + "/" + 12 + "/" + d.getFullYear()
                            endPeriodDate= {"date":{"year":this.model.year,"month":12,"day":newdate.getDate()}};
                        }
                        else{
                            // endPeriodDate =  d.getDate()  + "/" + d.getMonth() + "/" + d.getFullYear()
                            endPeriodDate= {"date":{"year":this.model.year,"month":newdate.getMonth() + 1,"day":newdate.getDate()}};
                        }
                        
                        var tempendPeriodDate= endPeriodDate.date.day +'/'+endPeriodDate.date.month+'/'+endPeriodDate.date.year;
                        this.model.dtoFiscalFinancialPeriodSetupDetail[i-1]["endPeriodDate"] = tempendPeriodDate;
                        
                    }
                    var tempStartDate= '';
                    if(i==this.model.numberOfPeriods-1)
                    {
                        
                        endPeriodDate =  this.model.lastDay;
                        tempStartDate= startdateData[0] +'/'+startdateData[1]+'/'+startdateData[2];
                    }
                    else{
                        tempStartDate= startdateData[1] +'/'+startdateData[0]+'/'+startdateData[2];
                    }
                    
                    var PrimaryId = i + 1;
                    
                    //var tempStartDate = {"date":{"year":parseInt(startdateData[2]),"month":parseInt(startdateData[0]),"day":parseInt(startdateData[1])}};
                    
                    this.model.dtoFiscalFinancialPeriodSetupDetail.push({"transactionDocumentDescriptionPrimary":"transaction in eng1",
                    "transactionDocumentDescriptionSecondary":"transaction in ar1","periodId":i + 1 ,
                    "startPeriodDate":tempStartDate,"endPeriodDate":endPeriodDate,"periodNamePrimary":"Period " + PrimaryId ,
                    "periodNameSecondary":"", "periodSeriesFinancial":true,"periodSeriesSales":true,
                    "periodSeriesPurchase":true,"periodSeriesInventory":true,"periodSeriesProject":true,
                    "periodSeriesPayroll":true
                });
                
                var startdateData = firstDay.split('/');
                firstDay= {"date":{"year":parseInt(startdateData[2]),"month":parseInt(startdateData[1]),"day":parseInt(startdateData[0])}};
                
                var enddateData = lastDay.split('/');
                lastDay = {"date":{"year":parseInt(enddateData[2]),"month":parseInt(enddateData[1]),"day":parseInt(enddateData[0])}};
                
                
                firstDay = firstDay.date.month +'/'+ firstDay.date.day +'/'+ firstDay.date.year;
                lastDay = lastDay.date.month +'/'+ lastDay.date.day +'/'+ lastDay.date.year;
            }
            this.showCalander=true;
        }
    }
    updateFinancialSetUpArray(Index,evt,actionType)
    {
        if(actionType == "periodSeriesFinancial")
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[Index].periodSeriesFinancial=evt.target.checked;
        }
        else if(actionType == "periodSeriesSales")
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[Index].periodSeriesSales=evt.target.checked;
        }
        else if(actionType == "periodSeriesPurchase")
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[Index].periodSeriesPurchase=evt.target.checked;
        }
        else if(actionType == "periodSeriesInventory")
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[Index].periodSeriesInventory=evt.target.checked;
        }
        else if(actionType == "periodSeriesProject")
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[Index].periodSeriesProject=evt.target.checked;
        }
        else if(actionType == "periodSeriesPayroll")
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[Index].periodSeriesPayroll=evt.target.checked;
        }
        
    }
    
    // UpdatePeriodDate(peridStartdate,idx:number)
    // {
    //   
    //     peridStartdate = peridStartdate.formatted.trim();
    //     var IsdataValid =  this.ValidateDateFormat(peridStartdate);
    //   
    //     if(IsdataValid != false)
    //     {
    //         var sDate = peridStartdate.split("/");
    //         this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]=sDate[0]+'/'+sDate[1]+'/'+this.model.year;
    //     }
    //     else{
    //         this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]='';
    //     }
    // }
    
    redirect1(){
        this.router.navigate(['generalledgersetup/massclosefiscalfinanceperiod']);
    }
    
    AddSubstractDay(Mydate,numberOfDay, Type) {
        //date = new Date();
        Mydate.setDate(Mydate.getDate() + numberOfDay);
        if (Type == '+') {
            Mydate.setDate(Mydate.getDate() + numberOfDay);
        }
        else {
            Mydate.setDate(Mydate.getDate() - numberOfDay);
        }
        
        if (Mydate.getMonth() < 10) {
            this.finalMonth = '0' + (Mydate.getMonth());
        }
        else {
            this.finalMonth = (Mydate.getMonth()).toString();
        }
        if (Mydate.getDate() < 10) {
            this.finalDate = '0' + (Mydate.getDate());
        }
        else {
            this.finalDate = (Mydate.getDate()).toString();
        }
        // this.fullDate = date.getFullYear() + '-' + this.finalMonth + '-' + this.finalDate;
        this.fullDate = this.finalDate + '/' + this.finalMonth + '/' + Mydate.getFullYear();
        return this.fullDate;
    }
    checkedAll()
    {
        for(var i=0;i<  this.model.dtoFiscalFinancialPeriodSetupDetail.length;i++)  
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesFinancial=true;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesSales=true;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesPurchase=true;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesInventory=true;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesProject=true;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesPayroll=true;
        }
    }
    
    unCheckedAll()
    {
        for(var i=0;i<  this.model.dtoFiscalFinancialPeriodSetupDetail.length;i++)  
        {
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesFinancial=false;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesSales=false;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesPurchase=false;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesInventory=false;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesProject=false;
            this.model.dtoFiscalFinancialPeriodSetupDetail[i].periodSeriesPayroll=false;
        }
    }
    
    ReDisplay()
    {
        this.model.historicalYear=false;
        this.model.year='';
        this.model.firstDay='';
        this.model.lastDay='';
        this.model.numberOfPeriods='';
        this.model.dtoFiscalFinancialPeriodSetupDetail=[];
    }
    
    
    SavefiscalfinanceperiodSetup(f: NgForm) {
        
        this.btndisabled=true;
        if(f.valid)
        {
            
            this.model.historicalYear= this.model.historicalYear;
            this.model.periodSeriesFinancial=this.periodSeriesFinancial;
            this.model.periodSeriesSales=this.periodSeriesSales;
            this.model.periodSeriesPurchase=this.periodSeriesPurchase;
            this.model.periodSeriesInventory=this.periodSeriesInventory;
            this.model.periodSeriesProject=this.periodSeriesProject;
            this.model.periodSeriesPayroll=this.periodSeriesPayroll;
            
            // for(var i=0;i< this.model.dtoFiscalFinancialPeriodSetupDetail.length;i++)  
            // {
            //     var StartDate = this.model.dtoFiscalFinancialPeriodSetupDetail[i]["startPeriodDate"];
            //  //   var startperiod= StartDate.date.day +'/'+StartDate.date.month +'/' + StartDate.date.year;
            //     if(StartDate.formatted)
            //     {
            //         this.model.dtoFiscalFinancialPeriodSetupDetail[i]["startPeriodDate"] =  StartDate.formatted;
            //     }
            //     else{
            //         var startperiod= StartDate.date.day +'/'+StartDate.date.month +'/' + StartDate.date.year; 
            //         this.model.dtoFiscalFinancialPeriodSetupDetail[i]["startPeriodDate"] =  startperiod;
            //     }
            // }
            
            if(this.model.firstDay.formatted == undefined)
            {
                var firstDay = this.model.firstDay;
                if(firstDay.date != undefined)
                {
                    this.model.firstDay = firstDay.date.day +'/'+ firstDay.date.month +'/'+ firstDay.date.year;
                }
            }
            else
            {
                this.model.firstDay = this.model.firstDay.formatted;
            }
            
            if(this.model.lastDay.formatted == undefined)
            {
                var lastDay = this.model.lastDay;
                if(lastDay.date != undefined)
                {
                    this.model.lastDay = lastDay.date.day  +'/'+ lastDay.date.month +'/'+ lastDay.date.year;
                }
            }
            else
            {
                this.model.lastDay = this.model.lastDay.formatted;
            }
            
            this.FiscalFinancePeriodSetupService.saveFiscalFinancePeriodSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                if (datacode == 200 || datacode == 201) {
                    this.isModify=false;
                    this.model.bankId=null;
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.hasMsg = true;
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                    // this.getAccountStructureSetup();
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            })
            // .catch(error => {
            //     window.setTimeout(() => {
            //         this.isSuccessMsg = false;
            //         this.isfailureMsg = true;
            //         this.showMsg = true;
            //         this.hasMsg = true;
            //         this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            //     }, 100)
            // });
        }
        this.btndisabled=false;
    }
    
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
    
    ValidateNumberOfDay()
    {
        if(this.model.numberOfPeriods > 365)
        {
            this.model.numberOfPeriods = 365;
        }
        else if(this.model.numberOfPeriods < 1)
        {
            this.model.numberOfPeriods = 1;
        }
    }
    
    ValidateTwoDate(evt:IMyDateModel,idx)
    {
        
        var currentDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"];
        var oldcurrentDate= currentDate;
        
        currentDate =  evt.date.year +','+evt.date.month +','+evt.date.day;
        
        var objCurrentDate=new Date(currentDate);
        // objCurrentDate.setFullYear(currentDate[2],(currentDate[1] - 1 ),currentDate[0]);
        
        
        if(idx==0)
        {
            var nextDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx + 1]["startPeriodDate"];
            nextDate =   nextDate.date.year +','+nextDate.date.month +','+nextDate.date.day;
            
            var objNextDate=new Date(nextDate);
            // objNextDate.setFullYear(nextDate[2],(nextDate[1] - 1 ),nextDate[0]); 
            if (objCurrentDate > objNextDate)
            {
                // this.isSuccessMsg = false;
                // this.isfailureMsg = true;
                // this.hasMsg = true;
                // this.showMsg = true;
                oldcurrentDate = {"date":{"year":parseInt(oldcurrentDate.date.year),"month":parseInt(oldcurrentDate.date.month),"day":parseInt(oldcurrentDate.date.day)}}
                this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]=evt.date;
                $('#txtstartPeriodDate' + idx).val(oldcurrentDate.date.day + '/' + oldcurrentDate.date.month + '/'+oldcurrentDate.date.year);
                alert("Current Period Can't Greater than Next Priod");
            }
        }
        else if(idx == this.model.numberOfPeriods - 1){
            
            
            var PrevDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx-1]["startPeriodDate"];
            
            PrevDate =   PrevDate.date.year +','+PrevDate.date.month +','+PrevDate.date.day;
            
            var objPrevDate=new Date(PrevDate);
            // objPrevDate.setFullYear(PrevDate[2],(PrevDate[1] - 1 ),PrevDate[0]);     
            
            if (objCurrentDate < objPrevDate)
            {
                // this.isSuccessMsg = false;
                // this.isfailureMsg = true;
                // this.hasMsg = true;
                // this.showMsg = true;
                oldcurrentDate = {"date":{"year":parseInt(oldcurrentDate.date.year),"month":parseInt(oldcurrentDate.date.month),"day":parseInt(oldcurrentDate.date.day)}}
                this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]= evt.date;
                $('#txtstartPeriodDate' + idx).val(oldcurrentDate.date.day + '/' + oldcurrentDate.date.month + '/'+oldcurrentDate.date.year);
                alert("Current Period Can't less than prev Priod");
            }
        }
        else{
            
            var nextDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx + 1]["startPeriodDate"];
            nextDate =   nextDate.date.year +','+nextDate.date.month +','+nextDate.date.day;
            
            var objNextDate=new Date(nextDate);
            // objNextDate.setFullYear(nextDate[2],(nextDate[1] - 1 ),nextDate[0]); 
            
            var PrevDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx-1]["startPeriodDate"];
            
            PrevDate =   PrevDate.date.year +','+PrevDate.date.month +','+PrevDate.date.day;
            
            var objPrevDate=new Date(PrevDate);
            // objPrevDate.setFullYear(PrevDate[2],(PrevDate[1] - 1 ),PrevDate[0]);     
            
            if (objCurrentDate > objNextDate)
            {
                // this.isSuccessMsg = false;
                // this.isfailureMsg = true;
                // this.hasMsg = true;
                // this.showMsg = true;
                oldcurrentDate = {"date":{"year":parseInt(oldcurrentDate.date.year),"month":parseInt(oldcurrentDate.date.month),"day":parseInt(oldcurrentDate.date.day)}}
                this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]= evt.date;
                $('#txtstartPeriodDate' + idx).val(oldcurrentDate.date.day + '/' + oldcurrentDate.date.month + '/'+oldcurrentDate.date.year);
                alert("Current Period Can't Greater than Next Priod");
            }else if (objCurrentDate < objPrevDate)
            {
                // this.isSuccessMsg = false;
                // this.isfailureMsg = true;
                // this.hasMsg = true;
                // this.showMsg = true;
                oldcurrentDate = {"date":{"year":parseInt(oldcurrentDate.date.year),"month":parseInt(oldcurrentDate.date.month),"day":parseInt(oldcurrentDate.date.day)}}
                this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]= evt.date;
                $('#txtstartPeriodDate' + idx).val(oldcurrentDate.date.day + '/' + oldcurrentDate.date.month + '/'+oldcurrentDate.date.year);
                alert("Current Period Can't less than prev Priod");
            }
            
        }
    }
    
    /** If Screen is Lock then prevent user to perform any action.
    *  This function also cover Role Management Write acceess functionality */
    LockScreen(writeAccess)
    {
        if(!writeAccess)
        {
            return true
        }
        else if(this.btndisabled)
        {
            return true
        }
        else if(this.isScreenLock)
        {
            return true;
        }
        else{
            return false;
        }
    }
    
    ValidateDateFormat(inputdate,idx)
    {
        var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
        var isValid = dateformat.test(inputdate);
        if(inputdate == '')
        {
            alert(Constants.fillDate);
        }
        else if(!isValid)
        {
            alert(Constants.invalidPeriodDate);
            this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"] = '';
        }
        else{
            var currentDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"];
            var oldcurrentDate= '';
            var arrCurrentdate = currentDate.split('/');
            
            currentDate = arrCurrentdate[2] +','+arrCurrentdate[1] +','+arrCurrentdate[0];
            
            var objCurrentDate=new Date(currentDate);
            
            if(idx==0)
            {
                var nextDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx + 1]["startPeriodDate"];
                var arrnextDate = nextDate.split('/');
                nextDate = arrnextDate[2] +','+arrnextDate[1] +','+arrnextDate[0];
                
                var objNextDate=new Date(nextDate);
                if (objCurrentDate >= objNextDate)
                {
                    this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]=oldcurrentDate;
                    alert(Constants.currentPeriodgreaterthanPrv);
                }
            }
            else if(idx == this.model.numberOfPeriods - 1){
                var PrevDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx-1]["startPeriodDate"];
                
                var arrPrevDate = PrevDate.split('/');
                PrevDate = arrPrevDate[2] +','+arrPrevDate[1] +','+arrPrevDate[0];
                
                var objPrevDate=new Date(PrevDate);
                
                if (objCurrentDate < objPrevDate)
                {
                    this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"] = oldcurrentDate;
                    alert(Constants.currentPeriodLessthanPrv);
                }
                else
                {
                    var endPeriodDate = this.parseDate( arrCurrentdate[1] +'/'+arrCurrentdate[0] +'/'+arrCurrentdate[2]);
                    var finalEndPeriodDate = '';
                    var d = new Date(endPeriodDate);
                    d.setDate(d.getDate() - 1);
                    if(d.getMonth() == 0)
                    {
                        finalEndPeriodDate =  d.getDate()  + "/" + 12 + "/" + d.getFullYear()
                    }
                    else{
                        finalEndPeriodDate =  d.getDate()  + "/" + d.getMonth() + "/" + this.model.year;
                    }
                    this.model.dtoFiscalFinancialPeriodSetupDetail[idx-1]["endPeriodDate"]= finalEndPeriodDate;
                }
            }
            else{
                
                var nextDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx + 1]["startPeriodDate"];
                var arrnextDate = nextDate.split('/');
                nextDate = arrnextDate[2] +','+arrnextDate[1] +','+arrnextDate[0];
                
                var objNextDate=new Date(nextDate);
                
                var PrevDate = this.model.dtoFiscalFinancialPeriodSetupDetail[idx-1]["startPeriodDate"];
                var arrPrevDate = PrevDate.split('/');
                PrevDate = arrPrevDate[2] +','+arrPrevDate[1] +','+arrPrevDate[0];
                
                var objPrevDate=new Date(PrevDate);
                
                if (objCurrentDate >= objNextDate)
                {
                    this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]= oldcurrentDate;
                    alert(Constants.currentPeriodgreaterthanPrv);
                }else if (objCurrentDate <= objPrevDate)
                {
                    this.model.dtoFiscalFinancialPeriodSetupDetail[idx]["startPeriodDate"]= oldcurrentDate;
                    alert(Constants.currentPeriodLessthanPrv);
                }
                else
                {
                    var endPeriodDate = this.parseDate( arrCurrentdate[1] +'/'+arrCurrentdate[0] +'/'+arrCurrentdate[2]);
                    var finalEndPeriodDate = '';
                    var d = new Date(endPeriodDate);
                    d.setDate(d.getDate() - 1);
                    if(d.getMonth() == 0)
                    {
                        finalEndPeriodDate =  d.getDate()  + "/" + 12 + "/" + d.getFullYear()
                    }
                    else{
                        finalEndPeriodDate =  d.getDate()  + "/" + d.getMonth() + "/" + this.model.year;
                    }
                    this.model.dtoFiscalFinancialPeriodSetupDetail[idx-1]["endPeriodDate"]= finalEndPeriodDate;
                }
                
            }
        }
    }
    
}