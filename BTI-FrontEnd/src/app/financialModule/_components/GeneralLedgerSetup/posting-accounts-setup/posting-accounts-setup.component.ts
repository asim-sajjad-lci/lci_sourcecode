import { Component, ViewChild,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { PostingAccountSetup } from '../../../../financialModule/_models/general-ledger-setup/posting-account-setup';
import { PostingAccountService } from '../../../../financialModule/_services/general-ledger-setup/posting-account-setup.service ';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 

@Component({
    templateUrl: './posting-accounts-setup.component.html',
    providers:[PostingAccountService,AccountStructureService,CommonService],
    
})

//export to make it available for other classes
export class PostingAccountSetupComponent {
    page = new Page();
    rows = new Array<PostingAccountSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    descriptionSecondary;
    descriptionPrimary;
    ArrDiscountTypePeriod=[];
    ArrDiscountType=[];
    postingAccountSeriesList=[];
    ArrDueType=[];
    ArrAccountNumberList=[];
    isModalOpen:boolean=false;
    // Serachable Select Box Step1
	ArrAccNo = [];
    accountTableRowIndex : any = [];
    ddlAccNoSetting = {};
	// Serachable Select Box Step1 End
    // Serachable Select Box Step1
	ArrPostingId = [];
    seriesNumber : any = [];
    ddlPostingIdSetting = {};
	// Serachable Select Box Step1 End
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean= false;
    singleSelectComponent;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    arrSegment=[];
    accountDescription=[];
	accountNumberIndex=[];
	accountNumberTitleLabel:string;
	createAccountNumberLabel:string;
	btnCancelLabel:string;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    isScreenLock;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private postingAccountService:PostingAccountService,
        private commonService:CommonService,
        private accountStructureService:AccountStructureService,public toastr: ToastsManager,	vcr: ViewContainerRef
        
    ){
        this.toastr.setRootViewContainerRef(vcr);
        this.accountNumberTitleLabel=Constants.accountNumberTitle;
        this.createAccountNumberLabel=Constants.createAccountNumber;
        this.btnCancelLabel=Constants.btnCancelText;
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.postingAccountService.getSeriesList().then(data => {
            
            // Searchable Select Box Step3
            if(data.btiMessage.messageShort !='RECORD_NOT_FOUND')
            {
                for(var i=0;i< data.result.records.length;i++)
                {
                    this.ArrPostingId.push({ "id": data.result.records[i].seriesId, "itemName": data.result.records[i].seriesNamePrimary})
                }
            }
            // Searchable Select Box Step3 End            
        });
    }
    
    
    ngOnInit() {
        this.getSegmentCount();
        this.getAccountNumber();
        this.getAddScreenDetail();
        this.getViewScreenDetail();
        this.setPage({ offset: 0 });
        // Serachable Select Box Step2
        this.ddlAccNoSetting = { 
            singleSelection: true, 
            text:this.select,
            enableSearchFilter: true,
            classes:"myclass custom-class",
            searchPlaceholderText:this.search,
        }; 
        // Serachable Select Box Step2
        // Serachable Select Box Step2
        this.ddlPostingIdSetting = { 
            singleSelection: true, 
            text:this.select,
            enableSearchFilter: true,
            classes:"myclass custom-class",
            searchPlaceholderText:this.search,
        }; 
        // Serachable Select Box Step2
    }
    
    getAddScreenDetail()
    {
        this.screenCode = "S-1031";
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_POSTING_ACCOUNT_SERIES', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_POSTING_ACCOUNT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_POSTING_ACCOUNT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_POSTING_ACCOUNT_ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_POSTING_ACCOUNT_SAVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
            { 'fieldName': 'ADD_POSTING_ACCOUNT_CLEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
        ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetail(this.screenCode,'Add');
        }
        
        getViewScreenDetail()
        {
            this.screenCode = "S-1078";
            this.defaultManageFormValues = [
                { 'fieldName': 'MANAGE_POSTING_ACCOUNT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_POSTING_ACCOUNT_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_POSTING_ACCOUNT_ACCOUNT_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_POSTING_ACCOUNT_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_POSTING_ACCOUNT_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
            ];
            this.getScreenDetail(this.screenCode,'Manage');
        }
        
        getScreenDetail(screenCode,ArrayType)
        {
            
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                this.screenName=data.result.dtoScreenDetail.screenName;
                this.moduleName=data.result.moduleName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                if(ArrayType == 'Add')
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
                else
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
            });
        }
        
        clearForm()
        {
            // f.resetForm();
            this.model.descriptionSecondary='';
            this.model.descriptionPrimary='';
            this.accountTableRowIndex=[];      
            this.seriesNumber=[];
        }
        
        Cancel(f)
        {
            this.isModify=false;
            this.clearForm();
        }
        
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.postingAccountService.searchPostingAccountSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            }, error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        //setting pagination
        setPage(pageInfo) {
            this.selected = []; // remove any selected checkbox on paging
            this.page.pageNumber = pageInfo.offset;
            this.postingAccountService.searchPostingAccountSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        
        changePageSize(event) {
            this.page.size = event.target.value;
            this.setPage({ offset: 0 });
        }
        
        getpaymentTermById(row: any)
        {
            this.seriesNumber=[];
            this.accountTableRowIndex=[];
            this.showBtns=true;
            this.isModify=true;
            this.postingAccountService.getPostingAccountSetupById(row.postingId).then(data => {
                this.model = data.result; 
                this.model.seriesId =  data.result.seriesNumber;
                var	selectedPostingSeries = this.ArrPostingId.find(x => x.id ==  data.result.seriesNumber);
                this.seriesNumber.push({'id':selectedPostingSeries.id,'itemName':selectedPostingSeries.itemName}); 
                var	selectedAccountNumber = this.ArrAccNo.find(x => x.id ==  data.result.accountTableRowIndex);
                this.accountTableRowIndex.push({'id':selectedAccountNumber.id,'itemName':selectedAccountNumber.itemName}); 
            });
            
        }
        //get id already exist
        IsIdAlreadyExist(){
            if(this.model.postingId != '' && this.model.postingId != undefined && this.model.postingId != 'undefined')
            {
                this.postingAccountService.getPostingAccountSetupById(this.model.postingId).then(data => {
                    var datacode = data.code;
                    if (data.status == "NOT_FOUND" || data.btiMessage.messageShort =='POSTING_ACCOUNT_FETCHED_FAIL') {
                        return true;
                    }
                    else{
                        var txtId = <HTMLInputElement> document.getElementById("postingIdText");
                        txtId.focus();
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        window.scrollTo(0,0);
                        this.model.postingId='';
                        return false;
                    }
                    
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
        }
        getAccountNumber(){
            this.postingAccountService.getGlAccountNumberList().then(data => {
                
                // DropDown Search Step 3
                // this.ArrAccountNumberList = data.result;
                // this.model.accountNumber='';
                // DropDown Search Step 3 End
                // Serachable Select Box Step3
                this.ArrAccNo = [];
                for(var i=0;i< data.result.length;i++)
                {
                    this.ArrAccNo.push({ "id": data.result[i].accountTableRowIndex, "itemName": data.result[i].accountNumber})
                }
                // Serachable Select Box Step3 End
                
            });
        }
        // Serachable Select Box Step4
        
        getAccNoByID(item:any){
            this.model.accountTableRowIndex=item.id;
            this.commonService.closeMultiselectWithId("accountTableRowIndex")
        }
        OnAccNoDeSelect(item:any){
            this.model.accountTableRowIndex='';
            this.commonService.closeMultiselectWithId("accountTableRowIndex")
            
        }
        
        // Serachable Select Box Step4
        getPostingIdByID(item:any){
            this.model.seriesId=item.id;
            this.commonService.closeMultiselectWithId("accountTableRowIndex")
            
        }
        OnPostingIdDeSelect(item:any){
            this.model.seriesId='';
            this.commonService.closeMultiselectWithId("accountTableRowIndex")
            
        }
        // Serachable Select Box Step4 ends
        
        
        getpostingAccountSeries(postingId){
            this.postingAccountService.getpostingAccountSeriesList(postingId).then(data => {
                this.postingAccountSeriesList = data.result;
                this.postingAccountSeriesList.splice(0, 0, {"seriesId": this.select });
                this.model.postingId='';
            });
        }
        
        savePaymentTerm(f: NgForm)
        {
            this.btndisabled=true;
            if(this.seriesNumber.length!=0){
                this.model.seriesNumber=this.model.seriesId;
                if(this.isModify)
                {
                    this.model.pASeries = this.seriesNumber[0].id;
                    
                    this.postingAccountService.updatePostingAccountSetup(this.model).then(data => {
                        this.seriesNumber=[];
                        this.accountTableRowIndex=[];
                        window.scrollTo(0,0);
                        this.btndisabled=false;
                        var datacode = data.code;
                        
                        if (data.btiMessage.messageShort=='POSTING_ACCOUNT_UPDATED_SUCCESSFULLY') {
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.isModify=false;
                                this.showBtns=false;
                                this.accountTableRowIndex=[];
                                this.seriesNumber=[];
                                this.setPage({ offset: 0 });
                            }, 1000);
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        f.resetForm();
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
                else
                {
                    this.postingAccountService.createPostingAccountSetup(this.model).then(data => {
                        this.showBtns=false;
                        this.btndisabled=false;
                        this.seriesNumber=[];
                        this.accountTableRowIndex=[];
                        //    this.model.pASeries = this.seriesNumber[0].id;
                        var datacode = data.code;
                        if (data.btiMessage.messageShort=='POSTING_ACCOUNT_SAVED_SUCCESSFULLY') {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.accountTableRowIndex=[];
                            this.seriesNumber=[];
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        f.resetForm();
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
            }
        }
        getSegmentCount()
        {
            this.accountStructureService.getAccountStructureSetup().then(data => {
                this.arrSegment=[];
                for(var i=0;i<data.result.segmentNumber;i++)	
                {
                    this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
                }
            });
        }
        
		fetchFirstAccountIndexInfo(segmentIndex){
            
            var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ segmentIndex);
            var segmentNumber=segment.value;
            
            if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
            {
                return false;
            }
            
            if(segmentNumber)
            {
                this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {
                    
                    let status = pagedData.status;
                    let result = pagedData.result;
                    if(status == "FOUND"){	
                        if(this.accountNumberIndex[segmentIndex])
                        {
                            this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
                            this.accountDescription[segmentIndex] = result.mainAccountDescription;
                        }
                        else{
                            this.accountDescription.push(result.mainAccountDescription);
                            this.accountNumberIndex.push(result.actIndx.toString());
                        }
                        
                        for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
                        {
                            var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
                            this.accountNumberIndex[m]='0';
                            this.accountDescription[m]='0';
                            if(currentSegment != m)
                            {
                                segment.disabled = false;
                            }
                            if(m == segmentIndex + 1)
                            {
                                segment.focus();
                            }
                        }
                    }else{
                        this.accountDescription = [];
                        this.accountNumberIndex = [];
                        var currentSegment=segmentIndex;
                        for(var m = currentSegment;m < this.arrSegment.length;m++)
                        {
                            var ctrlId='';
                            if(m >  0)
                            {
                                ctrlId = 'txtSegment_'+m;
                            }
                            else{
                                ctrlId='txtfirstSegment_'+m;
                            }
                            var segment = <HTMLInputElement>document.getElementById(ctrlId);
                            segment.value = '';
                        }
						this.toastr.warning(pagedData.btiMessage.message);				
					}
                });
            }
            else{
                this.accountDescription = [];
                this.accountNumberIndex = [];
                
                var currentSegment=segmentIndex;
                for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
                {
                    var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
                    segment.value = '';
                    segment.disabled = true;
                }
            }
        }
        fetchOtherAccountIndexInfo(segmentIndex){
            
            var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
            var segmentNumber=segment.value;
            
            if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
            {
                return false;
            }
            var acctIndex = this.accountNumberIndex.indexOf(segment.value);
            
			if(segmentNumber)
			{
				this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
					let status = pagedData.status;
                    let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberIndex[segmentIndex])
						{
							this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountDescription.push(result.dimensionDescription);
							this.accountNumberIndex.push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
						segment.value = '';
						this.accountNumberIndex[segmentIndex]='0';
						this.accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
                });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberIndex[currentSegment]='0';
				this.accountDescription[currentSegment]='0';
			}
        }
        
        createNewGlAccountNumber()
        {
            
            var tempAccountNumberList=[];
            var accountDesc = this.accountDescription.join();
            accountDesc=accountDesc.replace(/,/g , " ");
            accountDesc=accountDesc.replace("0"," ");
            tempAccountNumberList.push({'accountNumberIndex':this.accountNumberIndex,'accountDescription':accountDesc})
            this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {
                
                if(data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED')
                {
                    this.toastr.success(data.btiMessage.message);
                    this.getAccountNumber();
                    this.closeModal();
                }
                else{
                    this.toastr.warning(data.btiMessage.message);
                }
            });
            
        }
        
        openAccountWindow()
        {
            this.accountNumberIndex=[];
            this.accountDescription=[];
            this.isModalOpen = true;
        }
        closeModal()
        {
            this.isModalOpen = false;
        }
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
    }