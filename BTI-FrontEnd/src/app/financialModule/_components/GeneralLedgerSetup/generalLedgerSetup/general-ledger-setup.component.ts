import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import {fixedAssetCompanySetup} from '../../../../financialModule/_models/fixedAssets/fixedAssetCompanySetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import {GeneralLedgerSetupService} from '../../../../financialModule/_services/general-ledger-setup/generalLedgerSetup.service';
import { MainAccountSetupService } from '../../../../financialModule/_services/MasterData/mainAccountSetup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 
import * as moment from 'moment';
import { AuditTrialService } from '../../../_services/general-ledger-configuration-setup/audit-trial.service';
@Component({
   selector: 'general-ledger-set-up',
   templateUrl: './general-ledger-setup.component.html', 
    providers: [GeneralLedgerSetupService,MainAccountSetupService,CommonService,AccountStructureService, AuditTrialService],
})

export class GeneralLedgerSetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
	availableFormValues: [object];
	isScreenLock;
	gridAvailableFormValues: [object];
	accountNumberTitleLabel:string;
	createAccountNumberLabel:string;
	select=Constants.select; 
	moduleName;
    screenName;
	serialSequence = 1;
	page = new Page();
	rows = new Array<fixedAssetCompanySetup>();
    temp = new Array<fixedAssetCompanySetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
	deleteConfirmationText = Constants.deleteConfirmationText;
	selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
	btnCancelText=Constants.btnCancelText;
	showBtns:boolean=false;
	selectAccount = Constants.selectAccount;
	search=Constants.search;
	btndisabled:boolean=false;

	ddlAccountSetting = {};
    arrMainAccount=[];
	MainAccountOption=[];
	mainAccount1:any= [];
	mainAccount2:any= [];
	SelectedAccount : any = [];
	ddl_MultiSelectAccount = [];
	arrSegment= [];

	accountNumberIndex=[];
	accountDescription=[];
	btnCancelLabel:string;
	isModalOpen:boolean=false;

	emptyArray = [{id:'0',text: '--Select--'}];

	// transaction type serial
	arrCashPayment = this.emptyArray;
	activeCashPayment =this.emptyArray;

	arrCashReceipt = this.emptyArray;
	activeCashReceipt = this.emptyArray;

	arrCheckPayment = this.emptyArray;
	activeCheckPayment =this.emptyArray;

	arrCheckReceipt = this.emptyArray;
	activeCheckReceipt = this.emptyArray;

	arrTransferPayment = this.emptyArray;
	activeTransferPayment =this.emptyArray;

	arrTransferReceipt = this.emptyArray;
	activeTransferReceipt = this.emptyArray;

	arrCreditPayment = this.emptyArray;
	activeCreditPayment =this.emptyArray;

	arrCreditReceipt = this.emptyArray;
	activeCreditReceipt = this.emptyArray;

	auditPage = new Page();

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	tempVar;
	fetchedData;
	mode;
	 SearchableDDLSetting = {};
	generalSetup = {
		'sequenceType': '',
		'includeDate': false,
		'nextJournalEntry': '',
		'nextBudgetJournalEntry': '',
		'nextReconciliation': '',
		'displayNCorPB': '',
		'retainedEraningsCheckBox': false,
		'accountSegments': '',
		'mainAccount1': '',
		'mainAccount2': '',
		'maintainHistoryAccounts': false,
		'maintainHistoryTransactions': false,
		'maintainHistoryBudgetTransactions': false,
		'allowPostingToHistory': false,
		'allowDeletionOfSavedTrans': false,
		'allowvoidingCorrectingSubTrans': false,
		'userDefine1': '',
		'userDefine2': '',
		'userDefine3': '',
		'userDefine4': '',
		'userDefine5': '',

		'cashPayment': '',
		'cashReceipt': '',
		'checkPayment': '',
		'checkReceipt': '',
		'transferPayment': '',
		'transferReceipt': '',
		'creditPayment': '',
		'creditReceipt': ''
	}
	
	constructor(private accountStructureService:AccountStructureService,private router: Router,private route: ActivatedRoute,private generalLedgerSetupService: GeneralLedgerSetupService,
		private getScreenDetailService: GetScreenDetailService,private mainAccountSetupService:MainAccountSetupService,
		private commonService: CommonService,public toastr: ToastsManager,	vcr: ViewContainerRef,private auditTrialService: AuditTrialService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.tempVar ='';
		this.fetchedData =[];
		this.mode ="add";
		this.toastr.setRootViewContainerRef(vcr);
        this.accountNumberTitleLabel=Constants.accountNumberTitle;
	    this.createAccountNumberLabel=Constants.createAccountNumber;
	    this.btnCancelLabel=Constants.btnCancelText;
		

		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_GL_SETUP_NEXT_JOURNAL_ENTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_NEXT_BUDGET_JOURNAL_ENTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_NEXT_RECONCILIATION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_DISPLAY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_NET_CHANGE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_PERIOD_BALANCES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_RETAINED_EARNINGS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_CLOSE_TO_DIVISIONAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_MAIN_ACCOUNT1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_MAIN_ACCOUNT2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_MAINTAIN_HISTORY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_ACCOUNTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_TRANSACTIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_BUDGET_TRANSACTIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_ALLOW', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_POSTING_HISTORY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_DEL_SAVED_TRANS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_VOIDING_CORRECTING', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_USER_DEFINE_FIELDS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_USER_DEFINE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_USER_DEFINE4', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_USER_DEFINE5', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_OK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_GL_SETUP_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'SEQUENCE_OPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'SEQUENCE_SERIAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'SOURCE_DOCUMENT_SERIAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'INCLUDE_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'TRANSACTION_TYPE_SEQUENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'TRANSACTION_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'PAYMENT_VOUCHER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'RECEIPT_VOUCHER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'CASH', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'CHECK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'BANK_TRANSFER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'CREDIT_CARD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
				];
				vm.getScreenDetailService.ValidateScreen("S-1173").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1173").then(data => {
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
	}

	ngOnInit() {
		this.SearchableDDLSetting = { 
		 singleSelection: true, 
		 text:this.select,
		 enableSearchFilter: true,
		 classes:"myclass custom-class",
		 searchPlaceholderText:this.search,
	   }; 
	   
	   this.ddlAccountSetting = {
		  singleSelection: true, 
		  text:this.selectAccount,
		  selectAllText: this.selectAll,
		  unSelectAllText: this.unselectAll,
		  enableSearchFilter: true,
		  disabled:true,
		  classes:"myclass custom-class",
		  searchPlaceholderText:this.search,
		}; 
		this.getAccountNumber();
		this.getSegmentCount();
		window.setTimeout(() => {
			this.BindGLSetup();
		}, 1000);
	
	  this.mainAccountSetupService.getActiveMainAccountSetupList().subscribe(data => {
		   this.MainAccountOption=data.records;
			  for(var i=0;i< data.records.length;i++)
			{
			  this.arrMainAccount.push({ "id": data.records[i].actIndx, "itemName": data.records[i].mainAccountNumber})
			}
	  });

	 this.getAuditTrialsOptions();
	}	
	
	getAuditTrialsOptions() {
		this.auditPage.pageNumber = 0;
		this.auditPage.size = 100;
		this.auditTrialService.searchAuditTrial(this.auditPage, '').subscribe(pagedData => {
				console.log(pagedData);
				let data =pagedData.data;
				this.arrCashPayment = [{ 'id': '0', 'text': this.select }];
				this.arrCashReceipt = [{ 'id': '0', 'text': this.select }];

				this.arrCheckPayment = [{ 'id': '0', 'text': this.select }];
				this.arrCheckReceipt = [{ 'id': '0', 'text': this.select }];

				this.arrTransferPayment = [{ 'id': '0', 'text': this.select }];
				this.arrTransferReceipt = [{ 'id': '0', 'text': this.select }];

				this.arrCreditPayment = [{ 'id': '0', 'text': this.select }];
				this.arrCreditReceipt = [{ 'id': '0', 'text': this.select }];
				for(let i  = 0; i<data.length; i++){
					this.arrCashPayment.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});
					this.arrCashReceipt.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});

					this.arrCheckPayment.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});
					this.arrCheckReceipt.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});

					this.arrTransferPayment.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});
					this.arrTransferReceipt.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});

					this.arrCreditPayment.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});
					this.arrCreditReceipt.push({id :data[i].seriesIndex+'', text:data[i].sourceCode+':'+data[i].sourceDocument});


				}
		}, error => {
				this.hasMsg = true;
				window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
				}, 100)
		});
	}

  BindGLSetup()
   {
	this.generalLedgerSetupService.getData().then(data => {
		
		this.fetchedData = data.result;
		if(this.fetchedData){
			this.mainAccount1=[];
			var	selectedAccount = this.arrMainAccount.find(x => x.id ==  this.fetchedData.mainAccount1);
			if(selectedAccount){
				this.mainAccount1.push({'id':selectedAccount.id,'itemName':selectedAccount.itemName});
			} 
			 

			this.mainAccount2=[];
			 var	selectedAccount2 = this.arrMainAccount.find(x => x.id ==  this.fetchedData.mainAccount2);
			 if(selectedAccount2){
				this.mainAccount2.push({'id':selectedAccount2.id,'itemName':selectedAccount2.itemName});
			 }
			  
			 
			console.log(this.fetchedData);
			 // Transaction type 
			 if(this.fetchedData.cashPayment){
				let selectedCashPayment = this.arrCashPayment.find(x => x.id == this.fetchedData.cashPayment);
				this.activeCashPayment= [{'id':selectedCashPayment.id,'text':selectedCashPayment.text}];
			 }
			 if(this.fetchedData.cashReceipt){
				let selectedCashRecceipt = this.arrCashReceipt.find(x => x.id == this.fetchedData.cashReceipt);
				this.activeCashReceipt= [{'id':selectedCashRecceipt.id,'text':selectedCashRecceipt.text}];
			 }
			 if(this.fetchedData.checkPayment){
				let selectedCheckPayment = this.arrCheckPayment.find(x => x.id == this.fetchedData.checkPayment);
				this.activeCheckPayment= [{'id':selectedCheckPayment.id,'text':selectedCheckPayment.text}];
			 }
			 if(this.fetchedData.checkReceipt){
				let selectedCheckReceipt = this.arrCheckReceipt.find(x => x.id == this.fetchedData.checkReceipt);
				this.activeCheckReceipt= [{'id':selectedCheckReceipt.id,'text':selectedCheckReceipt.text}];
			 }
			 if(this.fetchedData.transferPayment){
				let selectedTransferPayment = this.arrTransferPayment.find(x => x.id == this.fetchedData.transferPayment);
				this.activeTransferPayment = [{'id':selectedTransferPayment.id,'text':selectedTransferPayment.text}];
			 }
			 if(this.fetchedData.transferReceipt){
				let selectedTransferReceipt = this.arrTransferReceipt.find(x => x.id == this.fetchedData.transferReceipt);
				this.activeTransferReceipt= [{'id':selectedTransferReceipt.id,'text':selectedTransferReceipt.text}];
			 }
			 if(this.fetchedData.creditPayment){
				let selectedcreditPayment = this.arrCreditPayment.find(x => x.id == this.fetchedData.creditPayment);
				this.activeCreditPayment= [{'id':selectedcreditPayment.id,'text':selectedcreditPayment.text}];
			 }
			 if(this.fetchedData.creditReceipt){
				let selectedCreditReceipt = this.arrCreditReceipt.find(x=> x.id == this.fetchedData.creditReceipt);
				this.activeCreditReceipt= [{'id':selectedCreditReceipt.id,'text':selectedCreditReceipt.text}];
			 }

			 // Bug Need To Fix
			 if(this.fetchedData.accountSegments)
			 {
				var selectedAccount3 = this.ddl_MultiSelectAccount.find(x => x.id ==  this.fetchedData.accountSegments);
				console.log(this.ddl_MultiSelectAccount);
				console.log(this.fetchedData.accountSegments,selectedAccount3);
				if(selectedAccount3){
					this.SelectedAccount.push({'id':selectedAccount3.id,'itemName':selectedAccount3.itemName});
				}
			 }

			 
			 if(this.fetchedData.retainedEraningsCheckBox)
			 {
				this.ddlAccountSetting = {
					singleSelection: true, 
					text:this.selectAccount,
					selectAllText: this.selectAll,
					unSelectAllText: this.unselectAll,
					enableSearchFilter: true,
					disabled:false,
					classes:"myclass custom-class",
					searchPlaceholderText:this.search,
				  }; 
			 }

			this.generalSetup = {
				'sequenceType': this.fetchedData.sequenceType+'',
				'includeDate':this.fetchedData.includeDate,
				'nextJournalEntry': this.fetchedData.nextJournalEntry,
				'nextBudgetJournalEntry': this.fetchedData.nextBudgetJournalEntry,
				'nextReconciliation': this.fetchedData.nextReconciliation,
				'displayNCorPB': this.fetchedData.displayNCorPB.toString(),
				'retainedEraningsCheckBox': this.fetchedData.retainedEraningsCheckBox,
				'accountSegments': this.fetchedData.accountSegments,
				'mainAccount1': this.fetchedData.mainAccount1,
				'mainAccount2': this.fetchedData.mainAccount2,
				'maintainHistoryAccounts': this.fetchedData.maintainHistoryAccounts,
				'maintainHistoryTransactions': this.fetchedData.maintainHistoryTransactions,
				'maintainHistoryBudgetTransactions': this.fetchedData.maintainHistoryBudgetTransactions,
				'allowPostingToHistory': this.fetchedData.allowPostingToHistory,
				'allowDeletionOfSavedTrans': this.fetchedData.allowDeletionOfSavedTrans,
				'allowvoidingCorrectingSubTrans': this.fetchedData.allowvoidingCorrectingSubTrans,
				'userDefine1': this.fetchedData.userDefine1,
				'userDefine2': this.fetchedData.userDefine2,
				'userDefine3': this.fetchedData.userDefine3,
				'userDefine4': this.fetchedData.userDefine4,
				'userDefine5': this.fetchedData.userDefine5,
				'cashPayment': this.fetchedData.CashPayment,
				'cashReceipt': this.fetchedData.cashReceipt,
				'checkPayment': this.fetchedData.checkPayment,
				'checkReceipt': this.fetchedData.checkReceipt,
				'transferPayment': this.fetchedData.transferPayment,
				'transferReceipt': this.fetchedData.transferReceipt,
				'creditPayment': this.fetchedData.creditPayment,
				'creditReceipt': this.fetchedData.creditReceipt
			}
		}
	});
   }
    
	Clear(generalLedgerSetup){
		this.mainAccount1=[];
		this.mainAccount2=[];
		this.SelectedAccount=[];
      generalLedgerSetup.resetForm();

	}

	set(event){
		
		if(!event.target.checked){
			this.ddlAccountSetting = {
				singleSelection: true, 
				text:this.selectAccount,
				selectAllText: this.selectAll,
				unSelectAllText: this.unselectAll,
				enableSearchFilter: true,
				disabled:true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			  }; 
			this.tempVar =this.generalSetup.accountSegments;
			this.generalSetup.accountSegments ='0';
		}else{
			this.ddlAccountSetting = {
				singleSelection: true, 
				text:this.selectAccount,
				selectAllText: this.selectAll,
				unSelectAllText: this.unselectAll,
				enableSearchFilter: true,
				disabled:false,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			  }; 
			this.generalSetup.accountSegments =this.tempVar;
		}
	}
	  // set MainAccount1  selected Value
    onSelectMainAccount1(item:any){
        this.mainAccount1=[];
		this.mainAccount1.push({ "id": item.id, "itemName": item.itemName})
		this.commonService.closeMultiselectWithId('mainAccount1')
    }

    //  MainAccount1 DeSelected 
    OnDeSelectMainAccount1(item:any){
		this.mainAccount1=[];
		this.commonService.closeMultiselectWithId('mainAccount1')
    }

	  // set MainAccount2  selected Value
    onSelectMainAccount2(item:any){
        this.mainAccount2=[];
		this.mainAccount2.push({ "id": item.id, "itemName": item.itemName})
		this.commonService.closeMultiselectWithId('mainAccount2')
    }

	getAccountNumber(){
        this.ddl_MultiSelectAccount=[];
       this.commonService.getGlAccountNumberList().then(data => {
		   
              for(var i=0;i< data.result.length;i++)
              {
                this.ddl_MultiSelectAccount.push({ "id": data.result[i].accountTableRowIndex, "itemName": data.result[i].accountNumber })
              }
        });
	}

	getSegmentCount()
	{
		 this.commonService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}
	
    //  MainAccount2 DeSelected 
    OnDeSelectMainAccount2(item:any){
		
		this.mainAccount2=[];
		this.commonService.closeMultiselectWithId('mainAccount2')
	}
	
	onItemSelect(item:any){
		this.commonService.closeMultiselectWithId("SelectedAccount")
    }
    OnItemDeSelect(item:any){
		this.commonService.closeMultiselectWithId("SelectedAccount")
    }
    onSelectAll(items: any){
    }
    onDeSelectAll(items: any){
        
	}

	onSelectcashPayment(event){
		this.activeCashPayment = [event];
	}

	onSelectcashReceipt(event){
		this.activeCashReceipt = [event];
	}

	onSelectcheckPayment(event){
		this.activeCheckPayment = [event];
	}

	onSelectcheckReceipt(event){
		this.activeCheckReceipt = [event];
	}

	onSelecttransferPayment(event){
		this.activeTransferPayment = [event];
	}

	onSelecttransferReceipt(event){
		this.activeTransferReceipt = [event];
	}

	onSelectcreditPayment(event){
		this.activeCreditPayment = [event];
	}

	onSelectcreditReceipt(event){
		this.activeCreditReceipt = [event];
	}

	

	// openAccountWindow()
	// {
	// 	this.accountNumberIndex=[];
	// 	this.accountDescription=[];
	// 	this.isModalOpen = true;
	// }
	// closeModal()
    // {
    //      this.isModalOpen = false;
	// }
	
	save(location: NgForm){
		//this.btndisabled=true;
		if(location.valid && this.mainAccount1.length > 0 && this.mainAccount2.length > 0)
		{
			var SelectedAccount=0
			if(this.SelectedAccount.length > 0)
			{
				SelectedAccount = this.SelectedAccount[0].id;
			}
			let submittedData = {
				'nextJournalEntry': this.generalSetup.nextJournalEntry,
				'sequenceType':this.generalSetup.sequenceType,
				'includeDate': this.generalSetup.includeDate,
				'nextBudgetJournalEntry': this.generalSetup.nextBudgetJournalEntry,
				'nextReconciliation': this.generalSetup.nextReconciliation,
				'displayNCorPB': this.generalSetup.displayNCorPB,
				'retainedEraningsCheckBox': this.generalSetup.retainedEraningsCheckBox,
				'accountSegments':SelectedAccount,
				'mainAccount1': this.mainAccount1[0].id,
				'mainAccount2': this.mainAccount2[0].id,
				'maintainHistoryAccounts': this.generalSetup.maintainHistoryAccounts,
				'maintainHistoryTransactions': this.generalSetup.maintainHistoryTransactions,
				'maintainHistoryBudgetTransactions': this.generalSetup.maintainHistoryBudgetTransactions,
				'allowPostingToHistory': this.generalSetup.allowPostingToHistory,
				'allowDeletionOfSavedTrans': this.generalSetup.allowDeletionOfSavedTrans,
				'allowvoidingCorrectingSubTrans': this.generalSetup.allowvoidingCorrectingSubTrans,
				'userDefine1': this.generalSetup.userDefine1,
				'userDefine2': this.generalSetup.userDefine2,
				'userDefine3': this.generalSetup.userDefine3,
				'userDefine4': this.generalSetup.userDefine4,
				'userDefine5': this.generalSetup.userDefine5,

				'cashPayment': this.activeCashPayment[0].id,
				'cashReceipt': this.activeCashReceipt[0].id,
				'checkPayment': this.activeCheckPayment[0].id,
				'checkReceipt': this.activeCheckReceipt[0].id,
				'transferPayment': this.activeTransferPayment[0].id,
				'transferReceipt': this.activeTransferReceipt[0].id,
				'creditPayment': this.activeCreditPayment[0].id,
				'creditReceipt': this.activeCreditReceipt[0].id
				}
					
			this.generalLedgerSetupService.createGeneralLedgerSetup(submittedData).then(data => {
				var datacode = data.code;
				this.btndisabled=false;
				if (datacode == 200) {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					this.messageText = data.btiMessage.message
					//this.setPage({ offset: 0 });
					 window.scrollTo(0,0);
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					
					
			 }else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;
					 this.showMsg = false;  
				   }, 4000);
			}
		   });
		}
		
	}

	createNewGlAccountNumber()
	{
		var tempAccountNumberList=[];
        var accountDesc = this.accountDescription.join();
        accountDesc=accountDesc.replace(/,/g , " ");
        accountDesc=accountDesc.replace("0"," ");
       
		tempAccountNumberList.push({'accountNumberIndex':this.accountNumberIndex,'accountDescription':accountDesc})
		this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {
			
			if(data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED')
			{
				this.toastr.success(data.btiMessage.message);
				this.getAccountNumber();
				//this.closeModal();
			}
			else{
				this.toastr.warning(data.btiMessage.message);
			}
		 });
	}

	fetchFirstAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		
		if(segmentNumber)
		{
			this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {
                
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberIndex[segmentIndex])
					{
						this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountDescription.push(result.mainAccountDescription);
						this.accountNumberIndex.push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
                    	var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
						this.accountNumberIndex[m]='0';
						this.accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
						}
						if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountDescription = [];
					this.accountNumberIndex = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
                    {
						var ctrlId='';
                        if(m >  0)
                        {
                            ctrlId = 'txtSegment_'+m;
                        }
                        else{
                            ctrlId='txtfirstSegment_'+m;
                        }
                        var segment = <HTMLInputElement>document.getElementById(ctrlId);
                        segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
			this.accountDescription = [];
			this.accountNumberIndex = [];

			var currentSegment=segmentIndex;
			for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
			{
				var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
				segment.value = '';
				segment.disabled = true;
			}
		}
	}
	fetchOtherAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberIndex.indexOf(segment.value);
		if(segmentNumber)
			{
				this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
                    
					let status = pagedData.status;
    				let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberIndex[segmentIndex])
						{
							this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountDescription.push(result.dimensionDescription);
							this.accountNumberIndex.push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
						segment.value = '';
						this.accountNumberIndex[segmentIndex]='0';
						this.accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberIndex[currentSegment]='0';
				this.accountDescription[currentSegment]='0';
			}
	}
	print(){
		window.print();
	}
	
	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
	}
	
	 /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
	   LockScreen(writeAccess)
	   {
		   if(!writeAccess)
		   {
			   return true
		   }
		   else if(this.btndisabled)
		   {
			   return true
		   }
		   else if(this.isScreenLock)
		   {
			   return true;
		   }
		   else{
			   return false;
		   }
	   }
	
	
}