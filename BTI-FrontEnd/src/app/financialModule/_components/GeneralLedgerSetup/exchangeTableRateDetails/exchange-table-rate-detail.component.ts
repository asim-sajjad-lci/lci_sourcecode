import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { ExchangeTableRateDetail } from '../../../../financialModule/_models/general-ledger-setup/exchangeTableRateDetail';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import {ExchangeTableRateDetailService} from '../../../../financialModule/_services/general-ledger-setup/exchangeTableRateDetail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'exchange-table-rate-details',
   templateUrl: './exchange-table-rate-detail.component.html', 
    providers: [ExchangeTableRateDetailService,CommonService],
})

export class ExchangeTableRateDetailsComponent implements OnInit {
	customerId;
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	myHtml;
	exchangeId;
	description:string;
	arabicDescription:string;
	currencyId:string;
	page = new Page();
	rows = new Array<ExchangeTableRateDetail>();
    temp = new Array<ExchangeTableRateDetail>();
	Searchselected = [];
	 // Serachable Select Box Step1
	ArrExchangeID = [];
    exchangeID : any = [];
    ddlExchangeIDSetting = {};
	// Serachable Select Box Step1 End
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	tableViewtext=Constants.tableViewtext;
	btnCancelText=Constants.btnCancelText;
	search=Constants.search;
    showBtns:boolean=false;
	EmptyMessage = Constants.EmptyMessage;
	btndisabled:boolean=false;
	isScreenLock

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	currency;
	add;
	disableId;
	exchangeDate;
	exchangeExpireDate;
	exchangeTime='';
	exchangeTableRateDetail = {
    'exchangeID': '',
    'description': '',
    'arabicDescription': '',
    'currencyId': '',
    'exchangeRate': '',
	'exchangeDate':'',
	'time':'',
	'expireDate':'',
	'exchangeExpirationDate':''
  };
	private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
		disableUntil: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() -1 }
    };
	private myOptions1: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
		disableUntil: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() -1 }
    };
	constructor(private router: Router,private route: ActivatedRoute,private exchangeTableRateDetailService: ExchangeTableRateDetailService,private getScreenDetailService: GetScreenDetailService,private commonService:CommonService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.disableId = false;
		this.currency = [];
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		//fetch currency records 
		this.exchangeTableRateDetailService.searchCurrency().subscribe(pagedData => {
			this.currency = pagedData.records;
			 });

		this.exchangeTableRateDetailService.exchangeIdList().subscribe(pagedData => {
            // this.currency = pagedData.records;
			// Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                 this.ArrExchangeID.push({ "id": pagedData.records[i].exchangeId, "itemName": pagedData.records[i].exchangeId})
              }
			    // Searchable Select Box Step3 End
        });
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_EXC_RATE_DETAIL_DATE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_EXC_RATE_DETAIL_TIME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_EXC_RATE_DETAIL_EXCHANGE_RATE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_EXC_RATE_DETAIL_EXPIRATION_DATE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_EXC_RATE_DETAIL_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_EXC_RATE_DETAIL_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
		];


		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1086").then(data => {
		
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_EXCHANGE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_TIME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_EXCHANGE_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_EXPIRATION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_EXC_RATE_DETAIL_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				];

				this.getScreenDetailService.ValidateScreen("S-1085").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1085").then(data => {
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */
	}
	

	
	ngOnInit() {

		// Serachable Select Box Step2
		    this.ddlExchangeIDSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
		 
		 this.route.params.subscribe((params: Params) => {
			 
			this.exchangeId = params['exchangeId'];
			if(this.exchangeId)
			{
				//this.GetAccountRecievableClassSetupByClassIdById(this.exchangeId);
				this.getExchangeIdDetailsByID(this.exchangeId);
				
			}
	 });
	}
	
	getExchangeIdDetailsByID(exchangeId){
		// this.exchangeTableRateDetailService.exchangeIdListDescription(exchangeId).then(data => {
		// 	this.exchangeID.push({'id':data.result.exchangeId,'itemName':data.result.exchangeId});
		// 	this.exchangeTableRateDetail.description = data.result.description;
		// 	this.exchangeTableRateDetail.arabicDescription = data.result.descriptionArabic;
		// 	this.exchangeTableRateDetail.currencyId = data.result.currencyId;
		// });
		this.exchangeID=[];
		this.exchangeTableRateDetailService.getExchangeTableRate(exchangeId).then(data => {
			this.records = data.result;
			this.exchangeID.push({'id':data.result.exchangeId,'itemName':data.result.exchangeId});
			this.exchangeTableRateDetail = {
				'exchangeID': this.records.exchangeId,
				'description': this.records.description,
				'arabicDescription': this.records.descriptionArabic,
				'currencyId': this.records.currencyId,
				'exchangeRate': this.records.exchangeRate,
				'exchangeDate': this.records.exchangeDate,
				'time': this.records.time,
				'expireDate': this.records.expireDate,
				'exchangeExpirationDate': this.records.exchangeExpirationDate
			  };
			 this.exchangeTime = this.records.exchangeTime;
			 var splitExchangeDate = this.records.exchangeDate.split('/');
			 var splitExchangeExpireDate = this.records.exchangeExpirationDate.split('/');
			 this.exchangeDate =
			 {
			  "date": {
				"year": splitExchangeDate[2],
				"month": splitExchangeDate[1].toString().replace(/(^0)/g,""),
				"day": splitExchangeDate[0].toString().replace(/(^0)/g,""),
			  },
			};
			
			this.exchangeExpireDate =
			 {
			  "date": {
				"year": splitExchangeExpireDate[2],
				"month": splitExchangeExpireDate[1].toString().replace(/(^0)/g,""),
				"day": splitExchangeExpireDate[0].toString().replace(/(^0)/g,""),
			  },
			};
			
		});
	}

	 // Serachable Select Box Step4
	getExchangeIDByID(item:any){
		
			this.exchangeID=[];
			this.exchangeID.push({'id':item.id,'itemName':item.itemName});
			this.commonService.closeMultiselectWithId("exchangeID")
			this.exchangeTableRateDetailService.exchangeIdListDescription(item.id).then(data => {
				
				this.exchangeTableRateDetail.description = data.result.description;
				this.exchangeTableRateDetail.arabicDescription = data.result.descriptionArabic;
				this.exchangeTableRateDetail.currencyId = data.result.currencyId;
			});
			//this.currencyId=item.id;
		}
		OnExchangeIDDeSelect(item:any){
			this.exchangeID=[];
		}
	 //get id already exist
	 IsIdAlreadyExist(){
        if(this.exchangeTableRateDetail.exchangeID != '' && this.exchangeTableRateDetail.exchangeID != undefined && this.exchangeTableRateDetail.exchangeID != 'undefined')
        {
            this.exchangeTableRateDetailService.getExchangeTableRate(this.exchangeTableRateDetail.exchangeID).then(data => {
                var datacode = data.code;
                if (data.status == "NOT_FOUND") {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("txtExchangeId");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.exchangeTableRateDetail.exchangeID='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    }
	save(exchangeTableDetails:NgForm){
		this.btndisabled=true;
		if(this.add){
			
			let exDate = this.exchangeDate.date.day +'/'+ this.exchangeDate.date.month +'/'+ this.exchangeDate.date.year;
			let exExpDate = this.exchangeExpireDate.date.day +'/'+ this.exchangeExpireDate.date.month +'/'+ this.exchangeExpireDate.date.year;
			let submittedData = {
				"exchangeId":this.exchangeID[0].id,
				"description":this.exchangeTableRateDetail.description,
				"descriptionArabic":this.exchangeTableRateDetail.arabicDescription,
				"exchangeRate":this.exchangeTableRateDetail.exchangeRate,
				"exchangeDate":exDate,
				"exchangeTime":this.exchangeTime,
				"exchangeExpirationDate":exExpDate,
				"currencyId":this.exchangeTableRateDetail.currencyId,
				"rateCalcMethod":"multiply"
			};
			this.exchangeTableRateDetailService.createExchangeTableRate(submittedData).then(data => {
                this.showBtns=false;
				var datacode = data.code;
				this.btndisabled=false;
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					//exchangeTableDetails.resetForm();
					this.clearForm(exchangeTableDetails);
					exchangeTableDetails.resetForm();
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					this.exchangeID=[];
				}, 100);
				this.setPage({ offset: 0 });
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.setPage({ offset: 0 });
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;  
			   }, 4000);
		}
       });
		}else{
			
			let exDate = this.exchangeDate.date.day +'/'+ this.exchangeDate.date.month +'/'+ this.exchangeDate.date.year;
			let exExpDate = this.exchangeExpireDate.date.day +'/'+ this.exchangeExpireDate.date.month +'/'+ this.exchangeExpireDate.date.year;
			let submittedData = {
				"exchangeId":this.exchangeID[0].id,
				"description":this.exchangeTableRateDetail.description,
				"descriptionArabic":this.exchangeTableRateDetail.arabicDescription,
				"exchangeRate":this.exchangeTableRateDetail.exchangeRate,
				"exchangeDate":exDate,
				"exchangeTime":this.exchangeTime,
				"exchangeExpirationDate":exExpDate,
				"currencyId":this.exchangeTableRateDetail.currencyId,
				"rateCalcMethod":"multiply"
			};
			this.exchangeTableRateDetailService.editExchangeTableRate(submittedData).then(data => {
			    this.btndisabled=false;
				var datacode = data.code;
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					
					this.disableId = false;
					this.add = true;
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					this.showBtns=false;
					
				}, 100);
				this.setPage({ offset: 0 });
				// exchangeTableDetails.resetForm();
				this.clearForm(exchangeTableDetails);
				exchangeTableDetails.resetForm();
				this.exchangeID=[];
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.setPage({ offset: 0 });
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;  
			   }, 4000);
		}
       });
			
		}
		
	}
	
	edit(row: any) {
		this.showBtns=true;
		this.add = false;
		this.disableId = true;
		this.setPage({ offset: 0 });
		this.exchangeID=[];
		this.exchangeTableRateDetailService.getExchangeTableRate(row.exchangeId).then(data => {
			this.records = data.result;
			this.exchangeID.push({'id':data.result.exchangeId,'itemName':data.result.exchangeId});
			this.exchangeTableRateDetail = {
				'exchangeID': this.records.exchangeId,
				'description': this.records.description,
				'arabicDescription': this.records.descriptionArabic,
				'currencyId': this.records.currencyId,
				'exchangeRate': this.records.exchangeRate,
				'exchangeDate': this.records.exchangeDate,
				'time': this.records.time,
				'expireDate': this.records.expireDate,
				'exchangeExpirationDate': this.records.exchangeExpirationDate
			  };
			 this.exchangeTime = this.records.exchangeTime;
			 var splitExchangeDate = this.records.exchangeDate.split('/');
			 var splitExchangeExpireDate = this.records.exchangeExpirationDate.split('/');
			 this.exchangeDate =
			 {
			  "date": {
				"year": splitExchangeDate[2],
				"month": splitExchangeDate[1].toString().replace(/(^0)/g,""),
				"day": splitExchangeDate[0].toString().replace(/(^0)/g,""),
			  },
			};
			
			this.exchangeExpireDate =
			 {
			  "date": {
				"year": splitExchangeExpireDate[2],
				"month": splitExchangeExpireDate[1].toString().replace(/(^0)/g,""),
				"day": splitExchangeExpireDate[0].toString().replace(/(^0)/g,""),
			  },
			};
			
		});
    }
	
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.exchangeTableRateDetailService.searchTableRateDetail(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.exchangeTableRateDetailService.searchTableRateDetail(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
	clearForm(exchangeTableDetails:NgForm){
		
		this.exchangeTableRateDetail.exchangeRate='';
		this.exchangeDate='';
		this.exchangeTime='';
		this.exchangeExpireDate ='';
		//exchangeTableDetails.resetForm();
        
		// var selectedValue = <HTMLInputElement>document.getElementById("txtExchangeId")
        // selectedValue.value=this.records.exchangeId;
		//this.exchangeTableRateDetail.exchangeID=this.records.exchangeId;
		if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
		 }
	}

	Cancel(exchangeTableDetails)
    {
    this.add=true;
    this.clearForm(exchangeTableDetails);
	exchangeTableDetails.resetForm();
	this.exchangeID= [];
	this.disableId=false;
    }
	
	onSelect({ Searchselected }) {
        this.Searchselected.splice(0, this.Searchselected.length);
        this.Searchselected.push(...Searchselected);
    }
	
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	clearStartDate(){
		this.exchangeDate ='';
	}
	clearStartDate1(){
		this.exchangeExpireDate ='';
	}
	formatstartTime(selectedTime){
		this.exchangeTime = moment(selectedTime).format('HH:mm a');
	}

	
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
	}
	
	/** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
       LockScreen(writeAccess)
       {
           if(!writeAccess)
           {
               return true
           }
           else if(this.btndisabled)
           {
               return true
           }
           else if(this.isScreenLock)
           {
               return true;
           }
           else{
               return false;
           }
       }
}