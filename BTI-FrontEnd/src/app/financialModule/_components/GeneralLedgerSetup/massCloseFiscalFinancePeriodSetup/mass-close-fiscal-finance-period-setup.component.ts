import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { MassCloseFiscalFinancePeriodSetup } from '../../../../financialModule/_models/general-ledger-setup/mass-close-fiscal-finance-period-setup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { MassCloseFiscalFinancePeriodSetupService } from '../../../../financialModule/_services/general-ledger-setup/massCloseFiscalFinancePeriodSetup.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
   selector: 'mass-close-fiscal-finance-period-setup',
   templateUrl: './mass-close-fiscal-finance-period-setup.component.html',
    providers:[MassCloseFiscalFinancePeriodSetupService]
})

export class MassCloseFiscalFinancePeriodSetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	
	page = new Page();
	rows = new Array<MassCloseFiscalFinancePeriodSetup>();
    temp = new Array<MassCloseFiscalFinancePeriodSetup>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	oneRadio= Constants.oneRadio;
	yearAndSeries = Constants.yearAndSeries;
	
	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	tempVar;
	fetchedData;
	getClass;
	years;
	mode;
	accountNumberList;
	description;
	arabicDescription;
	accountTableRowIndex;
	accountNumberListObj;
	total;
	series;
	periods;
	finalGridObj;
	year;
	seriesId;
	originId;
	display;
	all;
	finalGrid;
	firstDrop;
	secondDrop;
	firstText;
	secondText;
	getMax;
	arrOrigin:any;
	seriesName;
	selectedSeries:any;
	greaterorEqualValue = Constants.greaterorEqualValue
	constructor(private router: Router,private route: ActivatedRoute,private massCloseFiscalFinancePeriodSetupService:MassCloseFiscalFinancePeriodSetupService,private getScreenDetailService: GetScreenDetailService,public toastr: ToastsManager,vcr: ViewContainerRef) {
		this.toastr.setRootViewContainerRef(vcr);
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.tempVar ='';
		this.fetchedData =[];
		this.getClass =[];
		this.years =[];
		this.year ='';
		this.seriesId ='';
		this.originId='';
		this.series =[];
		this.accountNumberList =[];
		this.accountNumberListObj =[];
		this.finalGridObj =[];
		this.finalGrid =[];
		this.getMax =[];
		this.accountTableRowIndex ='';
		this.mode ="add";	
		this.total=0;
		this.display='';
		this.all=false;
		
		
		this.massCloseFiscalFinancePeriodSetupService.getYears().then(pagedData => {
            this.years = pagedData.result;
		
		});
	
		this.massCloseFiscalFinancePeriodSetupService.getSeries().then(pagedData => {
            this.series = pagedData.result;
            this.series.splice(0, 0,{"typeId": "", "name": this.select });
            this.seriesId='';
        });
		
		/* for add screen */
				vm.defaultFormValues = [
				   { 'fieldName': 'ADD_MASS_CLOSE_YEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_SERIES', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_ORIGIN', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_PERIODS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_ALL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_FROM', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_TO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_OPEN_ALL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_CLOSE_ALL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_SERIES_GRID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_ORIGIN_GRID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_PERIOD_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_CLOSED', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_REDISPLAY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_SAVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
				   { 'fieldName': 'ADD_MASS_CLOSE_CALCULATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
			 ];
			 	vm.getScreenDetailService.ValidateScreen("S-1218")
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1218").then(data => {
			
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */		
	}

	ChangeYear()
	{
		this.finalGridObj=[];
		this.arrOrigin=[];
		 this.seriesId='';
	}
	getOrigin()
	{
	   this.finalGridObj=[];
	//    this.arrOrigin=[{'originId':'','originName':''}];
	   this.arrOrigin=[];
	   this.selectedSeries = this.series.find(x => x.typeId == this.seriesId);
	  
	   this.massCloseFiscalFinancePeriodSetupService.getOrigin(this.selectedSeries.typeId).then(pagedData => {
            this.arrOrigin = pagedData.result;
        });
	}
	
	save(massCloseFiscal : NgForm){
		this.btndisabled=true;
		let submittedData = {
			'year': this.year,
			'seriesId': this.seriesId,
			'originId':this.originId,
		};
		submittedData["records"] = [];
		for(let i=0;i<this.finalGridObj.length;i++){
			submittedData["records"].push({
				"periodName":this.finalGridObj[i]["periodName"],
				"originId":this.finalGridObj[i]["originId"],
				"isClose":this.finalGridObj[i]["isClose"],
				"periodId": this.finalGridObj[i]["periodId"]
			});
		}
	
		this.massCloseFiscalFinancePeriodSetupService.save(submittedData).then(data => {
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 200) {
			 window.setTimeout(() => {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				this.messageText = data.btiMessage.message;
			}, 100);
			}else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  //this.setPage({ offset: 0 });
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;  
			   }, 4000);
			}
		});
	}
	getFirstPeriodName(){
		for(let i=0;i<this.getMax.length;i++){
			if(this.firstDrop==this.getMax[i]["periodNumber"]){
				this.firstText =this.getMax[i]["periodName"];
			}
		}
	}
	getSecondPeriodName(){
		for(let i=0;i<this.getMax.length;i++){
			if(this.secondDrop==this.getMax[i]["periodNumber"]){
				this.secondText =this.getMax[i]["periodName"];
			}
		}
	}
	getAllPeriods(event){

		if(event.target.value==2){
			this.finalGridObj=[];
			this.massCloseFiscalFinancePeriodSetupService.getMaxPeriod(this.year).subscribe(pagedData => {
				this.getMax=pagedData.result;
			
				for(let i=0;i<this.getMax.length;i++){
					if(i==0){
						this.firstDrop =this.getMax[i]["periodNumber"];
						this.firstText =this.getMax[i]["periodName"];
					}else if(i==this.getMax.length-1){
						this.secondDrop =this.getMax[i]["periodNumber"];
						this.secondText =this.getMax[i]["periodName"];
					}
				}
			});
		}else{
			this.finalGridObj=[];
			this.getMax=[];
			this.firstText='';
			this.secondText='';
		}
	}
	calculate(){
		if(!this.year && !this.seriesId){
			this.toastr.error(this.yearAndSeries);
		}else{
			
			if(!this.display){
				this.toastr.error(this.oneRadio);
			}else{
			
				if(this.display==1){
					this.all=false;
					
					var Request={
						'year': this.year,
						'seriesId': this.seriesId, 
						'allRecord': this.all,
						"allOrigin": false,
						'originId':this.originId,
					}

					if(this.originId == '')
					{
						this.all=true;
						Request={
							'year': this.year,
							'seriesId': this.seriesId, 
							'allRecord': this.all,
							"allOrigin": true,
							'originId': 0,
						}
					}
					
					this.massCloseFiscalFinancePeriodSetupService.getAll(Request).subscribe(pagedData => {
						this.finalGrid=pagedData.result;
							if(pagedData.status == "NOT_FOUND")
							{
								this.isSuccessMsg = false;
								this.isfailureMsg = true;
								this.showMsg = true;
								this.hasMsg = true;
								//this.setPage({ offset: 0 });
								this.messageText = pagedData.btiMessage.message;
								 window.setTimeout(() => {
									 this.hasMsg = false;  
								 }, 4000);
							}
							else{
								this.finalGridObj=[];
								for(let i=0;i<this.finalGrid.length;i++){
									let temp={
										"seriesName" : this.finalGrid[i]["seriesName"],
										"originId" : this.finalGrid[i]["originId"],
										"origin" : this.finalGrid[i]["origin"],
										"periodName" : this.finalGrid[i]["periodName"],
										"isClose" : this.finalGrid[i]["isClose"],
										"periodId": this.finalGrid[i]["periodId"]
									};
									this.finalGridObj.push(temp);
								}
							}
					});
				}else if(this.display==2){
					if(this.secondDrop < this.firstDrop){
						this.toastr.warning(this.greaterorEqualValue);
					}else{
						this.all=false;
						var RequestData=
						{
							"year" : this.year,
							'seriesId': this.seriesId, 
							'allRecord': this.all,
							"startPeriodId":this.firstDrop,
							"endPeriodId":this.secondDrop,
						    "allOrigin": false,
							'originId':this.originId,
						}
						this.massCloseFiscalFinancePeriodSetupService.getFrom(RequestData).subscribe(pagedData => {
							if(pagedData.status == "NOT_FOUND")
							{
								this.isSuccessMsg = false;
								this.isfailureMsg = true;
								this.showMsg = true;
								this.hasMsg = true;
								//this.setPage({ offset: 0 });
								this.messageText = pagedData.btiMessage.message;
								 window.setTimeout(() => {
									 this.hasMsg = false;  
								 }, 4000);
							}
							else{
								this.finalGrid=pagedData.result;
								this.finalGridObj=[];
								
								for(let i=0;i<this.finalGrid.length;i++){
								let temp={
									"seriesName" : this.finalGrid[i]["seriesName"],
									"originId" : this.finalGrid[i]["originId"],
									"origin" : this.finalGrid[i]["origin"],
									"periodName" : this.finalGrid[i]["periodName"],
									"isClose" : this.finalGrid[i]["isClose"],
									"periodId": this.finalGrid[i]["periodId"]
								};
								this.finalGridObj.push(temp);
							}
							}
							
						});
					}
				}
			}
		}
	}
	openClose(event,i){
		if(event){
			this.finalGridObj[i]["isClose"]=event;
		}else{
			this.finalGridObj[i]["isClose"]=event;
		}
	}
	closeAll(){
		for(let i=0;i<this.finalGridObj.length;i++){
			this.finalGridObj[i]["isClose"]=true;
		}
	}
	openAll(){
		for(let i=0;i<this.finalGridObj.length;i++){
			this.finalGridObj[i]["isClose"]=false;
		}
	}
	clearForm(){
		this.year='';
		this.seriesId='';
		this.originId='';
		this.finalGridObj=[];
		this.getMax=[];
		this.display='';
		this.firstText='';
		this.secondText='';
	}
	ngOnInit() {
	}	
	
	
	
}