import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CurrencySetup } from '../../../../financialModule/_models/general-ledger-setup/currencysetup';
import { CurrencySetupService } from '../../../../financialModule/_services/general-ledger-setup/currencysetup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<currency-setup></currency-setup>',
    templateUrl: './currency-setup.component.html',
    styles: ["user.component.css"],
    providers:[CurrencySetupService]
})

//export to make it available for other classes
export class CurrencySetupComponent {
    page = new Page();
    rows = new Array<CurrencySetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    isScreenLock
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    isModify:boolean=false;
    isIncludeSpace:boolean=false;
    ArrNegativeSymbol=[];
    ArrDecimalType=[];
    ArrThousandType=[];
    ArrNegativeSumbol=[];
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    disableId;
    myHtml;
    add;
	abc;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private currencySetupService: CurrencySetupService,
        private getScreenDetailService: GetScreenDetailService){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.disableId = false;
        this.add = true;
    }

    ngOnInit() {
      var rdbeforeamount = <HTMLInputElement> document.getElementById("rdbeforeamount");
      rdbeforeamount.checked=false;
      
      var rdafteramount = <HTMLInputElement> document.getElementById("rdafteramount");
      rdafteramount.checked=false ;
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });
               
      this.currencySetupService.getNegativeSymbolTypes().then(data => {
                    this.ArrNegativeSymbol = data.result;
                    this.ArrNegativeSymbol.splice(0, 0, { "typeId": "", "typeValue": this.select });
                     this.model.negativeSymbol='';
                 });
                    this.currencySetupService.getSeperatorDecimalTypesList().then(data => {
                    this.ArrDecimalType = data.result;
                    this.ArrDecimalType.splice(0, 0, { "typeId": "", "typeValue": this.select });
                    this.model.separatorsDecimal='';
                 });
                   this.currencySetupService.getSeperatorThousandsTypesList().then(data => {
                    this.ArrThousandType = data.result;
                    this.ArrThousandType.splice(0, 0, { "typeId": "", "typeValue": this.select });
                     this.model.separatorsThousands='';
                 });

                  this.currencySetupService.getNegativeSymbolSignTypes().then(data => {
                    this.ArrNegativeSumbol = data.result;
                  
                  });
 }

      
 onDisplaySymbolChange() {
    var rdbeforeamount = <HTMLInputElement> document.getElementById("rdbeforeamount");
    var rdafteramount = <HTMLInputElement> document.getElementById("rdafteramount");
    if (this.isIncludeSpace) {
      setTimeout(()=>{
        this.model.includeSpaceAfterCurrencySymbol = true;
        // rdbeforeamount.checked=false;
        // rdafteramount.checked=false ;
      })
    }
    else
    {
        this.isIncludeSpace=false;
        this.model.includeSpaceAfterCurrencySymbol = false;
        // rdbeforeamount.checked=false;
        // rdafteramount.checked=false ;
    }
  }
    getAddScreenDetail()
    {
         this.screenCode = "S-1046";
           // default form parameter for adding CURRENCY
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_CURRENCY_SETUP_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CURRENCY_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_CURRENCY_SYMBOL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CURRENCY_SETUP_NEGATIVE_SIGN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CURRENCY_SETUP_DISPLAY_SYMBOL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_BEFORE_AMOUNT_SYMBOL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CURRENCY_SETUP_AFTER_AMOUNT_SYMBOL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_INCLUDE_SPACE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_SEPRATORS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_DECIMAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_THOUSANDS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CURRENCY_SETUP_BEFORE_AMOUNT_SIGN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
            { 'fieldName': 'ADD_CURRENCY_SETUP_DISPLAY_SIGN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
            { 'fieldName': 'ADD_CURRENCY_SETUP_AFTER_AMOUNT_SIGN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_CURRENCY_UNIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_CURRENCY_UNIT_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_SUBUNIT_CONNECTOR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CURRENCY_SETUP_SUBUNIT_CONNECTOR_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
            { 'fieldName': 'ADD_CURRENCY_SETUP_CURRENCY_SUBUNIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
            { 'fieldName': 'ADD_CURRENCY_SETUP_CURRENCY_SUBUNIT_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CURRENCY_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
           
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1047";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_CURRENCY_SYMBOL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.currencySetupService.searchCurrencySetup(this.page, this.searchKeyword).subscribe(pagedData => {
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.currencySetupService.searchCurrencySetup(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

    // getpaymentTermById(row: any)
    getCurrencySetUpDetailsById(row: any)
    {
        this.showBtns=true;
        this.add = false;
         this.disableId = true;
        this.isModify=true;
         
         this.myHtml = "border-vanish";
         this.currencySetupService.getCurrencySetUpDetailsById(row.currencyId).then(data => {
            
             this.model = data.result; 
             this.model.displayNegativeSymbolSign= data.result.displayNegativeSymbolSign.toString();
             this.model.displayCurrencySymbol= data.result.displayCurrencySymbol.toString();
             var rdbeforeamount = <HTMLInputElement> document.getElementById("rdbeforeamount");
             var rdafteramount = <HTMLInputElement> document.getElementById("rdafteramount");
             if(this.model.displayCurrencySymbol == 1)
             {
                rdbeforeamount.checked=true;
                rdafteramount.checked=false;
             }
             else{
                rdbeforeamount.checked=false;
                rdafteramount.checked=true ;
             }
             var chkIncludeSpaceAfterCurrencySymbol = <HTMLInputElement> document.getElementById("chkIncludeSpaceAfterCurrencySymbol");
             if(this.model.includeSpaceAfterCurrencySymbol == false)
             {
                    chkIncludeSpaceAfterCurrencySymbol.checked=false;
                    this.isIncludeSpace = false;
             }
             else
             {
                this.isIncludeSpace=true;
                chkIncludeSpaceAfterCurrencySymbol.checked=true;
             }
         });
    }

    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    clearForm()
    {
            this.model.currencyDescription= '';
            this.model.currencyDescriptionArabic='';
            this.model.currencySymbol='';
            this.model.currencyUnit= '';
            this.model.unitSubunitConnector='';
            this.model.currencySubunit='';
            this.model.currencyUnitArabic='';
            this.model.unitSubunitConnectorArabic='';
            this.model.currencySubunitArabic='';
            this.model.separatorsDecimal='';
            this.model.includeSpaceAfterCurrencySymbol='';
            this.model.negativeSymbol='';
            this.model.displayNegativeSymbolSign='';
            this.model.separatorsThousands=''; 
            this.isIncludeSpace=false;
            this.abc='';
            
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.currencyId='';
		 }
    }

     Cancel(f)
        {
        this.add=true;
        this.model.currencyId= '';
        this.clearForm();
        this.disableId=false;
        }
    //get id already exist
    IsIdAlreadyExist(){
        if(this.model.currencyId != '' && this.model.currencyId != undefined && this.model.currencyId != 'undefined')
        {
            this.currencySetupService.getCurrencySetUpDetailsById(this.model.currencyId).then(data => {
                var datacode = data.code;
                if (data.status == "NOT_FOUND") {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("txtId");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.model.currencyId='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    }
    saveCurrencySetup(f: NgForm)
    {
        this.btndisabled=true;
        var rdbeforeamount = <HTMLInputElement> document.getElementById("rdbeforeamount");
        var rdafteramount = <HTMLInputElement> document.getElementById("rdafteramount");
        if(this.isIncludeSpace)
        {
            this.model.includeSpaceAfterCurrencySymbol=true;
        }
        else{
            this.model.includeSpaceAfterCurrencySymbol= false;
        }

        if(rdbeforeamount.checked)
        {
            this.model.displayCurrencySymbol=1;
        }
        else
        {
            this.model.displayCurrencySymbol=2;
        }

        if(!this.add)
        {
            this.disableId = false;
            
             this.currencySetupService.updateCurrencySetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 200) {
                      this.isModify=false;
                      this.model.currencyId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                    else{
                     this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }

                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
             this.currencySetupService.createCurrencySetup(this.model).then(data => {
                this.showBtns=false;
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                    else{
                     this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }

                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }

    onlyAlphabets(event)
    {
        var charCode = event.keyCode;
        if (charCode == 8 || charCode == 46 ||(charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))

            return true;

        else

            return false;
    }

     /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
       LockScreen(writeAccess)
       {
           if(!writeAccess)
           {
               return true
           }
           else if(this.btndisabled)
           {
               return true
           }
           else if(this.isScreenLock)
           {
               return true;
           }
           else{
               return false;
           }
       }
}