import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { ExchangeTableSetupComponent } from '././exchangetablesetup/exchange-table-setup.component';
import { CurrencySetupComponent } from './currency/currency-setup.component';
import { ExchangeTableRateDetailsComponent } from './exchangeTableRateDetails/exchange-table-rate-detail.component';
import { GeneralLedgerSetupComponent } from './generalLedgerSetup/general-ledger-setup.component';
import { FiscalFinancePeriodSetupComponent } from './fiscalFinancePeriodSetup/fiscal-finance-period-setup.component';
import { MassCloseFiscalFinancePeriodSetupComponent } from '../GeneralLedgerSetup/massCloseFiscalFinancePeriodSetup/mass-close-fiscal-finance-period-setup.component';
import { AccountStructureSetupComponent } from './account-structure-setup/account-structure-setup.component';
import { AccountCategoryComponent } from './GeneralLedgerConfigurationSetup/accounts-category/accounts-category.component';
import { AccountTypeComponent } from './GeneralLedgerConfigurationSetup/accounts-type/accounts-type.component';
import { AuditTrialComponent } from './GeneralLedgerConfigurationSetup/audit-trial/audit-trial.component';
import { GeneralLedgerSetupRoutingModule } from './general-ledger-setup-routing.module';
import {ModalModule} from "ng2-modal";
import {SelectModule} from 'ng2-select';
// import { TrimWhitespace } from '../../../_sharedresource/TrimWhitespace.directive';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}
@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    GeneralLedgerSetupRoutingModule,
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule,
    ModalModule,
    SelectModule
    
  ],
  declarations: [ExchangeTableSetupComponent,CurrencySetupComponent, ExchangeTableRateDetailsComponent
    ,GeneralLedgerSetupComponent,FiscalFinancePeriodSetupComponent,MassCloseFiscalFinancePeriodSetupComponent,
    AccountStructureSetupComponent,AutoFocusDirective,
    AccountCategoryComponent,AccountTypeComponent,AuditTrialComponent]
})
export class GeneralLedgerSetupModule { }
