import { Component,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AccountStructureSetup } from '../../../../financialModule/_models/general-ledger-setup/accounts-structure-setup';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../_sharedresource/Constants';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 
import * as moment from 'moment';

@Component({
    selector: '<accounts-receivables></accounts-receivables>',
    templateUrl: './account-structure-setup.component.html',
    styles: ["user.component.css"],
    providers:[AccountStructureService]
})

//export to make it available for other classes
export class AccountStructureSetupComponent {
    page = new Page();
    rows = new Array<AccountStructureService>();
    ArrBankId = [];
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    AccountStructureSetup = {};
    id: string;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    isModify:boolean=true;
    ArrApplyByOption=[];
    exchangeDate;
	exchangeExpireDate;
	exchangeTime='';
    checkbookoptions;
    ageingByOptions;
    minRange:string;
    maxRange:string;
    currentFrom:string;
    currentTo:string;
    OldMaxLimit:number;
    dimensionList = [];
    lastDateBalanceForwardAge: boolean =false;
    compoundFinanceCharge: boolean =false;
    arTrackingDiscountAvailable: boolean =false;
    deleteUnpostedPrintedDocuments: boolean =false;
    printHistoricalAgedTrialBalance: boolean =false;
    arPayCommissionsInvoicePay: boolean =false;
    documentType:string;
    documentTypeDescription:string;
    documentTypeDescriptionArabic:string;
    documentNumberLastNumber:string;
    documentCode:string;
    arrDimensionsList = [];
    arrNewdimensionList=[];
    arrMainAccountList=[];
    tempArrdimensionList=[];
    selectedDimention = [];
    btndisabled:boolean=false;
    isScreenLock;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private AccountStructureService:AccountStructureService,
        public toastr: ToastsManager,	vcr: ViewContainerRef
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.toastr.setRootViewContainerRef(vcr);
    }

     ngOnInit() {
       this.getAddScreenDetail();
       this.getAccountStructureSetup();
    }
  
    getAddScreenDetail()
    {
         this.screenCode = "S-1209";
         this.defaultAddFormValues = [
                    { 'fieldName': 'ADD_ACC_STR_SEGMENTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_STR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_STR_SEGMENT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_STR_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_STR_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_STR_MAIN_ACC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_STR_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'ADD_ACC_STR_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
         ];
         this.getScreenDetail(this.screenCode);
    }
    
 
    getScreenDetail(screenCode)
    {
         this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
             for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
           
           
        });
    }

    //setting pagination
   
   
   getAccountStructureSetup()
   {
     this.AccountStructureService.getAccountStructureSetup().then(data => {
         if(data.code != 404)
         {
            this.isModify=true;
            this.model = data.result;
            
           if(this.model.isMainAccount)
           {
              this.AccountStructureService.getMainAccountList().then(data => {
                 this.arrMainAccountList = data.result;
                 this.arrMainAccountList.splice(0, 0, { "actIndx": "", "mainAccountNumber": this.select });
              });
           }
           else
           {
             this.arrMainAccountList = [];
           }
            this.getDimensionArray();
            this.getDimensionArrayOnEdit();
         }
      });
   }
 
  createSegments()
   {
       
        var txtSegment = <HTMLInputElement>document.getElementById('txtsegmentNumber'); 
        if(this.isModify)
        {
            if(txtSegment.value != '' && (txtSegment.value != this.model.segmentNumber))
            {
                this.arrDimensionsList = [];
                this.arrNewdimensionList = [];
                this.AccountStructureService.getFinancialDimensionsList().then(data => {
                     this.arrDimensionsList = data.result;
                     this.arrDimensionsList.splice(0, 0, { "dimInxd": "", "dimensionName": this.select });
                     for(var i = 1 ; i< this.model.segmentNumber;i++)
                     {
                            this.arrNewdimensionList.push({"Id" : i+1,"SegmentName" : this.arrDimensionsList,"From" : "","To" : "","coaFinancialDimensionsIndexId":"","coaFinancialDimensionsFromIndexValue":"","coaFinancialDimensionsToIndexValue":""});
                     }
                });

                var ddlDimentionList = <HTMLSelectElement>document.getElementById("DimentionList_0");
                ddlDimentionList.disabled=false;
            }
        }
        else{
            if(txtSegment.value != '')
            {
                this.arrDimensionsList = [];
                this.arrNewdimensionList = [];
                this.AccountStructureService.getFinancialDimensionsList().then(data => {
                     this.arrDimensionsList = data.result;
                     this.arrDimensionsList.splice(0, 0, { "dimInxd": "", "dimensionName": this.select });
                     for(var i = 1 ; i< this.model.segmentNumber;i++)
                     {
                            this.arrNewdimensionList.push({"Id" : i+1,"SegmentName" : this.arrDimensionsList,"From" : "","To" : "","coaFinancialDimensionsIndexId":"","coaFinancialDimensionsFromIndexValue":"","coaFinancialDimensionsToIndexValue":""});
                     }
                });
            }
            
        }
   }
   
    getDimensionArray()
    {
        
         this.arrDimensionsList = [];
         this.arrNewdimensionList = [];
        this.AccountStructureService.getFinancialDimensionsList().then(data => {
             
             this.arrDimensionsList = data.result;
             this.arrDimensionsList.splice(0, 0, { "dimInxd": "", "dimensionName": this.select });
             if(!this.isModify)
             {
                for(var i = 1 ; i< this.model.segmentNumber;i++)
                {
                    this.arrNewdimensionList.push({"Id" : i+1,"SegmentName" : this.arrDimensionsList,"From" : "","To" : "","coaFinancialDimensionsIndexId":"","coaFinancialDimensionsFromIndexValue":"","coaFinancialDimensionsToIndexValue":""});
                }
             }
        });
       
    }

    getDimensionArrayOnEdit()
    {
        this.arrNewdimensionList = [];
        for(var i = 1 ; i< this.model.segmentNumber;i++)
        {
            this.arrNewdimensionList.push({"Id" : i+1,"SegmentName" : this.arrDimensionsList,"From" : "","To" : "","coaFinancialDimensionsIndexId":"","coaFinancialDimensionsFromIndexValue":"","coaFinancialDimensionsToIndexValue":""});
        }
        
           var FromValue =[];
            var ToValue =[];
            var coaFinancialDimensionsFromIndexValue = '';
            var coaFinancialDimensionsToIndexValue = '';
           
        for(var i=0;i< this.model.dimensionList.length;i++)
        {
              var coaFinancialDimensionsIndexId = this.model.dimensionList[i].coaFinancialDimensionsIndexId;
               coaFinancialDimensionsFromIndexValue = this.model.dimensionList[i].coaFinancialDimensionsFromIndexValue;
               coaFinancialDimensionsToIndexValue = this.model.dimensionList[i].coaFinancialDimensionsToIndexValue;
                this.arrNewdimensionList[i].coaFinancialDimensionsFromIndexValue = coaFinancialDimensionsFromIndexValue;
                this.arrNewdimensionList[i].coaFinancialDimensionsToIndexValue = coaFinancialDimensionsToIndexValue;
                this.arrNewdimensionList[i].coaFinancialDimensionsIndexId = coaFinancialDimensionsIndexId;
                this.binddata(i,coaFinancialDimensionsIndexId);
        }
    }

    binddata(index,Id)
    {
         this.AccountStructureService.getFinancialDimensionsValuesByFinancialDimensionId(Id).then(data => {
                  this.arrNewdimensionList[index].From  = data.result;
                  this.arrNewdimensionList[index].To = data.result;
                  this.arrNewdimensionList[index].From.splice(0, 0, { "dimInxValue": "", "dimensionValue": this.select });

         });
    }

    

   getMainAccountList(evt)
   {
       
       if(evt.target.checked)
       {
            this.AccountStructureService.getMainAccountList().then(data => {
               this.arrMainAccountList = data.result;
               this.arrMainAccountList.splice(0, 0, { "actIndx": "", "mainAccountNumber": this.select });
            });
           
       }
       else
       {
         this.arrMainAccountList = [];
       }
   }

   getFinancialDimensionsValuesByFinancialDimensionId(event,RowIndex)
   {
        
        var currentIndex = RowIndex-2;
     //   var ddlDimentionList = document.getElementById("DimentionList_"+ currentIndex);

        // for(var i=currentIndex + 1;i<this.arrNewdimensionList.length;i++)
        // {
        //     var ddlDimentionList = <HTMLSelectElement>document.getElementById("DimentionList_"+ i);
        //     ddlDimentionList.remove(i);
        // }

        var ddlDimentionList = <HTMLSelectElement>document.getElementById("DimentionList_"+ (RowIndex -1) );
        if(ddlDimentionList)
        {
            ddlDimentionList.disabled=false;
        }


        var Id=event.target.value;
        if(this.selectedDimention.indexOf(Id) > -1)
        {
            var ddlDimentionList = <HTMLSelectElement>document.getElementById("DimentionList_"+ currentIndex);
       
            this.selectedDimention[currentIndex] = '0';
            ddlDimentionList.selectedIndex=0;
        }
        else{
            
            if(this.selectedDimention[currentIndex] && event.target.value !='')
            {
                this.selectedDimention[currentIndex] = Id;
            }
            else if(event.target.value =='')
            {
                this.selectedDimention[currentIndex] = '0';
            }
            else{
                this.selectedDimention.push(event.target.value);
            }
            // this.selectedDimention.push({'RowIndex':currentIndex,'Value':event.target.value});
           
            this.AccountStructureService.getFinancialDimensionsValuesByFinancialDimensionId(Id).then(data => {
                   this.arrNewdimensionList[RowIndex - 2].From = data.result;
                   this.arrNewdimensionList[RowIndex - 2].To = data.result;
                   this.arrNewdimensionList[RowIndex - 2].coaFinancialDimensionsIndexId = Id;
                   this.arrNewdimensionList[RowIndex - 2].From.splice(0, 0, { "dimInxValue": "", "dimensionValue": this.select });
            });
        }
   }
    
    UpdateArrayValue(event,RowIndex,ActionType)
    {
        var Id=event.target.value;
        var fromValue=<HTMLSelectElement>document.getElementById('ddlDimensionsFrom_'+RowIndex);
        var toValue=<HTMLSelectElement>document.getElementById('ddlDimensionsTo_'+RowIndex);
         
         if(fromValue && toValue && parseInt(fromValue.value) > parseInt(toValue.value))
         {
             this.toastr.warning(Constants.ValidationToFromValue);
             if(ActionType == "FromValue")
             {
                fromValue.value='';
             }
             else if(ActionType == "ToValue")
             {
                toValue.value='';
             }
             return false;
         }
         else{
            if(ActionType == "FromValue")
            {
               this.arrNewdimensionList[RowIndex - 2].coaFinancialDimensionsFromIndexValue = Id;
            }
            else if(ActionType == "ToValue")
            {
               this.arrNewdimensionList[RowIndex - 2].coaFinancialDimensionsToIndexValue = Id;
            }
         }
    }
    resetMe()
    {
        
      if(this.isModify)
      {
         this.getAccountStructureSetup();
      }
      else
      {
         this.model.segmentNumber='';
         this.arrDimensionsList = [];
         this.arrNewdimensionList = [];
      }
      
    }
    CreateStructureSetup() {
            this.btndisabled=true;
             this.tempArrdimensionList=[];
             for(var i = 0 ; i< this.arrNewdimensionList.length;i++)
             {
                this.tempArrdimensionList.push({"coaFinancialDimensionsIndexId":this.arrNewdimensionList[i].coaFinancialDimensionsIndexId,
                "coaFinancialDimensionsFromIndexValue":this.arrNewdimensionList[i].coaFinancialDimensionsFromIndexValue,
                "coaFinancialDimensionsToIndexValue":this.arrNewdimensionList[i].coaFinancialDimensionsToIndexValue})
             } 
            
            if(!this.model.segmentNumber)
            {
                 this.isSuccessMsg = false;
                 this.isfailureMsg = true;
                 this.showMsg = true;
                 this.hasMsg = true;
                 this.messageText = "Please Enter Segment ! !";
                 window.setTimeout(() => {
                          this.showMsg = false;
                          this.hasMsg = false;
                    }, 4000);
                return false;
                
            }
             this.model.dimensionList = this.tempArrdimensionList;
             if(this.model.isMainAccount)
             {
                 this.model.isMainAccount = 1;
             }
             else
             {
                 this.model.isMainAccount = 0;
             }
        
           this.AccountStructureService.saveAccountStructureSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 200 || datacode == 201) {
                    this.isModify=false;
                    this.model.bankId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.getAccountStructureSetup();
                        this.isModify=false;
                       window.setTimeout(() => {
                          this.showMsg = false;
                          this.hasMsg = false;
                    }, 4000);
                }
              
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
            this.btndisabled=false;
    }
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

      /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
       LockScreen(writeAccess)
       {
           if(!writeAccess)
           {
               return true
           }
           else if(this.btndisabled)
           {
               return true
           }
           else if(this.isScreenLock)
           {
               return true;
           }
           else{
               return false;
           }
       }
}
