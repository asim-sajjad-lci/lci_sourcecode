import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ExchangeTableSetup } from '../../../../financialModule/_models/general-ledger-setup/exchangetablesetup';
import { ExchangeTableSetupService } from '../../../../financialModule/_services/general-ledger-setup/exchangetablesetup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<exchangetable-setup></exchangetable-setup>',
    templateUrl: './exchange-table-setup.component.html',
    styles: ["user.component.css"],
    providers:[ExchangeTableSetupService,CommonService]
})

//export to make it available for other classes
export class ExchangeTableSetupComponent {
    page = new Page();
    rows = new Array<ExchangeTableSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    isScreenLock
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    currencyoptions;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    exchangeId;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean=false;
    ArrrateFrequencyType=[];
    disableButton;
    // Serachable Select Box Step1
	ArrCurrencyId = [];
    currencyId : any = [];
    ddlCurrencyIdSetting = {};
	// Serachable Select Box Step1 End
    disableId;
    myHtml;
    add;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private exchangeTableSetupService: ExchangeTableSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService){
            this.page.pageNumber = 0;
            this.page.size = 5;
            this.disableId = false;
            this.add = true;
            this.disableButton = true;
        }
        
        ngOnInit() {
            this.getAddScreenDetail();
            this.getViewScreenDetail();
            this.getCurrencyList();
            this.setPage({ offset: 0 });
            // Serachable Select Box Step2
            this.ddlCurrencyIdSetting = { 
                singleSelection: true, 
                text:this.select,
                enableSearchFilter: true,
                classes:"myclass custom-class",
                searchPlaceholderText:this.search,
            }; 
            // Serachable Select Box Step2
            
            this.exchangeTableSetupService.getRateFrequencyTypes().then(data => {
                this.ArrrateFrequencyType = data.result;
                this.ArrrateFrequencyType.splice(0, 0, { "typeId": "", "typeValue": this.select });
                this.model.rateFrequency='';
            });
        }
        
        
        getAddScreenDetail()
        {
            this.screenCode = "S-1070";
            // default form parameter for adding exchange currency set up
            this.defaultAddFormValues = [
                { 'fieldName': 'ADD_EXCH_TB_SETUP_EXCHANGE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_EXCHANGE_SOURCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_EXCHANGE_SOURCE_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_RATE_FREQ', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_RATE_VARIENCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_RATE_CALC_METHOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_MULTIPLY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_DIVIDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
                { 'fieldName': 'ADD_EXCH_TB_SETUP_RATES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
                this.getScreenDetail(this.screenCode,'Add');
            }
            
            getCurrencyList()
            {
                this.exchangeTableSetupService.getListForCurrency().then(data => {
                    // this.currencyoptions = data.result.records;
                    // Searchable Select Box Step3
                    for(var i=0;i< data.result.records.length;i++)
                    {
                        this.ArrCurrencyId.push({ "id": data.result.records[i].currencyId, "itemName": data.result.records[i].currencyId})
                    }
                    // Searchable Select Box Step3 End
                    //this.currencyoptions.splice(0, 0, { "currencyId": this.select });
                    // this.model.currencyId='';
                });
            }
            
            // Serachable Select Box Step4
            getCurrencyByID(item:any){
                this.model.currencyId=item.id;
                this.commonService.closeMultiselect()
            }
            OnCurrencyIdDeSelect(item:any){
                this.model.currencyId='';
                this.commonService.closeMultiselect()
            }
            // Serachable Select Box Step4 ends
            
            
            getViewScreenDetail()
            {
                this.screenCode = "S-1071";
                this.defaultManageFormValues = [
                    { 'fieldName': 'MANAGE_EXCH_TB_SETUP_EXCHANGE_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_EXCH_TB_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_EXCH_TB_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_EXCH_TB_SETUP_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_EXCH_TB_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_EXCH_TB_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
                ];
                this.getScreenDetail(this.screenCode,'Manage');
            }
            
            getScreenDetail(screenCode,ArrayType)
            {
                
                this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                    this.screenName=data.result.dtoScreenDetail.screenName;
                    this.moduleName=data.result.moduleName;
                    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                    if(ArrayType == 'Add')
                    {
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                    else
                    {
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                });
            }
            
            //setting pagination
            setPage(pageInfo) {
                this.selected = []; // remove any selected checkbox on paging
                this.page.pageNumber = pageInfo.offset;
                this.exchangeTableSetupService.searchExchangeTableSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                });
            }
            
            updateFilter(event) {
                this.searchKeyword = event.target.value.toLowerCase();
                this.page.pageNumber = 0;
                this.page.size = this.ddPageSize;
                this.exchangeTableSetupService.searchExchangeTableSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                    
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.table.offset = 0;
                }, error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            
            // getpaymentTermById(row: any)
            getExchangeTableSetupById(row: any)
            {
                this.disableButton = false;
                this.showBtns=true;
                this.currencyId=[];
                this.add = false;
                this.isModify=true;
                this.disableId = true;
                this.myHtml = "border-vanish";
                this.exchangeTableSetupService.getExchangeTableSetupById(row.exchangeId).then(data => {
                    this.model = data.result;
                    this.exchangeId =  row.exchangeId;
                    this.currencyId.push({'id':data.result.currencyId,'itemName':data.result.currencyId});
                });
            }
            
            redirect1(exchangeId){
                
                this.router.navigate(['generalledgersetup/exchangeratedetail',this.exchangeId]);
            }
            
            changePageSize(event) {
                this.page.size = event.target.value;
                this.setPage({ offset: 0 });
            }
            
            clearForm()
            {
                
                this.model.description='';
                this.model.descriptionArabic='';
                this.model.exchangeRateSource ='';
                this.model.exchangeRateSourceArabic ='';
                this.model.rateFrequency='';
                this.model.rateVariance='';
                this.currencyId=[];
                this.model.rateCalcMethod='';
                
                if(!this.add){
                    this.myHtml = "border-vanish";
                }else{
                    this.myHtml = "";
                    this.model.exchangeId='';
                }
            }
            
            Cancel(f)
            {
                this.add=true;
                this.model.exchangeId= '';
                this.clearForm();
                this.disableId=false;
            }
            
            //get id already exist
            IsIdAlreadyExist(){
                if(this.model.exchangeId != '' && this.model.exchangeId != undefined && this.model.exchangeId != 'undefined')
                {
                    this.exchangeTableSetupService.getExchangeTableSetupById(this.model.exchangeId).then(data => {
                        var datacode = data.code;
                        if (data.status == "NOT_FOUND") {
                            return true;
                        }
                        else{
                            var txtId = <HTMLInputElement> document.getElementById("exchangeIdText");
                            txtId.focus();
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            window.scrollTo(0,0);
                            this.model.exchangeId='';
                            return false;
                        }
                        
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
            }
            saveExchangeTableSetUp(f: NgForm)
            {
                this.btndisabled=true;
                if(!this.add)
                {
                    this.disableId = false;
                    this.disableButton = false;
                    this.exchangeTableSetupService.updateExchangeTableSetup(this.model).then(data => {
                        this.currencyId=[];
                        window.scrollTo(0,0);
                        this.btndisabled=false;
                        var datacode = data.code;
                        if (datacode == 201) {
                            this.isModify=false;
                            this.model.exchangeId=null;
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.disableButton = true;
                            this.showBtns=false;
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        
                        f.resetForm();
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
                else
                {
                    this.exchangeTableSetupService.createExchangeTableSetup(this.model).then(data => {
                        this.showBtns=false;
                        window.scrollTo(0,0);
                        this.currencyId=[];
                        this.btndisabled=false;
                        this.disableButton = false;
                        var datacode = data.code;
                        if (datacode == 201) {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.disableButton = true;
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.setPage({ offset: 0 });
                            
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        
                        f.resetForm();
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
                
            }
            
            onlyDecimalNumberKey(event) {
                return this.getScreenDetailService.onlyDecimalNumberKey(event);
            }
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
        }