import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import {AccountRecievables} from '../../../../../financialModule/_models/accounts-receivables-setup/accounts-receivables-class-setup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import {CustomerOptionsMaintenanceService} from '../../../../../financialModule/_services/accounts-payables-setup/customer-maintenance-options.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import {CommonService} from '../../../../../_sharedresource/_services/common-services.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';

@Component({
   selector: 'customer-maintenance-options',
   templateUrl: './customer-maintenance-options.component.html', 
    providers: [CustomerOptionsMaintenanceService,CommonService],
})

export class CustomerMaintenanceOptionsComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	btnCancelText=Constants.btnCancelText;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	currentVendorId:string;
	page = new Page();
	rows = new Array<AccountRecievables>();
    temp = new Array<AccountRecievables>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
	totalText=Constants.totalText;
	search=Constants.search;
    deleteConfirmationText = Constants.deleteConfirmationText;
    
	// Serachable Select Box Step1
    ArrCustomerNumberId = [];
    customerNumberId : any = [];
    ddlCustomerNumberIdSetting = {};

		ArrPaymentTermId = [];
    paymentTermId : any = [];
    ddlPaymentTermIdSetting = {};


	// Serachable Select Box Step1
	ArrShipmentMethodId = [];
    shipmentMethodId : any = [];
    ddlShipmentMethodSetting = {};

		ArrVatScheduleId = [];
    vatId : any = [];
    ddlVatScheduleSetting = {};

	
	ArrCurrencyId = [];
    currencyId : any = [];
    ddlCurrencyIdSetting = {};

		ArrCheckBookId = [];
    checkbookId : any = [];
    ddlCheckBookIdSetting = {};

	ArrSalesTerritoryId = [];
    salesTerritoryId : any = [];
    ddlSalesTerritoryIdSetting = {};

		ArrSalesmanId = [];
    salesmanId : any = [];
    ddlSalesmanIdSetting = {};

	    
	ArrPriceLevel = [];
    priceLevel : any = [];
    ddlPriceLevelSetting = {};

    isModify:boolean=false;

	selectedCurrency;
	unFrmtfinanceChargeAmount;
	unFrmtminimumChargeAmount;
	unFrmtCreditLimitAmount;
	unFrmtTradeDiscountPercent;
	inputValue;
	editfinanceChargeAmount;
	editMinimumChargeAmount;
	editCreditLimitAmount ;
	editTradeDiscountPercent;
	financeChargeAmountValue;
	minimumChargeAmountValue;
	creditLimitAmountValue;
	TradeDiscountPercentValue;
	isPercent1:boolean = false
	isPercent2:boolean = false
	isPercent3:boolean = false
	// Serachable Select Box Step1 ends

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	customers;
	simpleName;
	arabicName;
	ftextDisabled;
	mtextDisabled;
	ctextDisabled;
	shippingMethod;
	vat;
	currency;
	salesman;
	checkbook;
	payments;
	editData;
	condition;
	salesTerritory;
	myHtml;
	
	CustomerMaintenance=
	{
		'customerNumberId' : '',
		'balanceType' : '1',
		'financeCharge' : '0',
		'financeChargeAmount' : '',
		'minimumCharge' : '0',
		'minimumChargeAmount' : '',
		'creditLimit' : '0',
		'creditLimitAmount' : '',
		'tradeDiscountPercent' : '',
		'paymentTermId' : '',
		'shipmentMethodId' : '',
		'vatId' : '',
		'currencyId' : '',
		'priceLevel' : '',
		'salesmanId' : '',
		'salesTerritoryId' : '',
		'checkbookId' : '',
		'customerPriority' : '',
		'openMaintenanceHistoryCalendarYear' : false,
		'openMaintenanceHistoryTransaction' : false,
		'openMaintenanceHistoryFiscalYear' : false,
		'openMaintenanceHistoryDistribution' : false,
	}
	
	constructor(private router: Router,private route: ActivatedRoute,private customerOptionsMaintenanceService: CustomerOptionsMaintenanceService,private getScreenDetailService: GetScreenDetailService,private commonService:CommonService) {
		this.add = true;
		this.disableId = false;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.customers=[];
		this.shippingMethod=[];
		this.vat=[];
		this.currency=[];
		this.salesman=[];
		this.checkbook=[];
		this.payments=[];
		this.salesTerritory=[];
		this.condition = false;
		this.ftextDisabled = false;
		this.mtextDisabled = false;
		this.ctextDisabled = false;
		
		
	
		
		
		
	
		/* for edit data ends here */
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_CUS_OPT_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_BALANCE_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_OPEN_ITEM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_BALANCE_FORWARD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_FINANCE_CHARGE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_FINANCE_CHARGE_NONE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_FINANCE_CHARGE_PERCENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_FINANCE_CHARGE_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_MIN_PAYMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_MIN_PAYMENT_NO_MIN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_MIN_PAYMENT_PERCENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_MIN_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_CREDIT_LIMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_CREDIT_LIMIT_NO_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_CREDIT_LIMIT_UNLIMITED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_CREDIT_LIMIT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_TRADE_DIS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_PAYMENT_TERM_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_SHIP_METHOD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_PRICE_LEVEL_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_SALESMAN_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPT_TERRITORY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUS_OPTCHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_CUS_OPT_PRIORITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_CUS_OPT_MAINTAIN_HISTORY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_CUS_OPT_CALENDAR_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_CUS_OPT_FISCAL_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_CUS_OPT_TRANSACTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },		
				{ 'fieldName': 'ADD_CUS_OPT_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_CUS_OPT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_CUS_OPT_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }			
				];

				this.getScreenDetailService.ValidateScreen("S-1183");
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1183").then(data => {
		
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
	
	}

	save(customerMaintenanceOptions:NgForm){
		if(customerMaintenanceOptions.valid)
		{
			if(this.CustomerMaintenance.openMaintenanceHistoryCalendarYear){
				var calenderYear = '1';
			}else{
				var calenderYear = '0';
			}
			
			if(this.CustomerMaintenance.openMaintenanceHistoryTransaction){
				var transaction = '1';
			}else{
				var transaction = '0';
			}
			
			if(this.CustomerMaintenance.openMaintenanceHistoryFiscalYear){
				var fisYear = '1';
			}else{
				var fisYear = '0';
			}
			
			if(this.CustomerMaintenance.openMaintenanceHistoryDistribution){
				var distribution = '1';
			}else{
				var distribution = '0';
			}
			var priceLevelId=0
			if(this.priceLevel.length > 0)
			{
				priceLevelId = this.priceLevel[0].id;
			}
			let submittedData = 
			{
				'customerNumberId' : this.currentVendorId,
				'balanceType' : this.CustomerMaintenance.balanceType,
				'financeCharge' : this.CustomerMaintenance.financeCharge,
				'financeChargeAmount' : this.unFrmtfinanceChargeAmount,
				'minimumCharge' : this.CustomerMaintenance.minimumCharge,
				'minimumChargeAmount' : this.unFrmtminimumChargeAmount,
				'creditLimit' : this.CustomerMaintenance.creditLimit,
				'creditLimitAmount' : this.unFrmtCreditLimitAmount,
				'tradeDiscountPercent' : this.unFrmtTradeDiscountPercent,
				'paymentTermId' : this.paymentTermId[0].id,
				'shipmentMethodId' : this.shipmentMethodId[0].id,
				'vatId' : this.vatId[0].id,
				'currencyId' : this.currencyId[0].id,
				'priceLevel' : priceLevelId,
				'salesmanId' : this.salesmanId[0].id,
				'salesTerritoryId' : this.salesTerritoryId[0].id,
				'checkbookId' : this.checkbookId[0].id,
				'customerPriority' : this.CustomerMaintenance.customerPriority,
				'openMaintenanceHistoryCalendarYear' : calenderYear,
				'openMaintenanceHistoryTransaction' : transaction,
				'openMaintenanceHistoryFiscalYear' : fisYear,
				'openMaintenanceHistoryDistribution' : distribution,
			};
			this.customerOptionsMaintenanceService.create(submittedData).then(data => {
				var datacode = data.code;
				if (datacode == 201) {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					this.condition=true;
					this.myHtml = "border-vanish";
					window.scrollTo(0,0);
					this.messageText = data.btiMessage.message;
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					
			 }else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;
					 this.showMsg = false;  
				   }, 4000);
			}
		   });
		}
	}
	getNames(){
		this.customerOptionsMaintenanceService.getCustomerByID(this.CustomerMaintenance.customerNumberId).then(data => {
			this.records = data.result;
			this.simpleName = data.result.name;
			this.arabicName = data.result.arabicName;
			
		});
	}
	set(value){
	
		if(value=="none"){
			this.ftextDisabled = true;
			this.CustomerMaintenance.financeChargeAmount='';
		}else if(value=="percent"){
			this.isPercent1 = true
			this.ftextDisabled = false;
		}else{
			this.isPercent1 = false
			this.ftextDisabled = false;
		}
	}
	set1(value){
	
		if(value=="none"){
			this.mtextDisabled = true;
			this.CustomerMaintenance.minimumChargeAmount='';
		}else if(value=="percent"){
			this.isPercent2 = true
			this.mtextDisabled = false;
		}else{
			this.isPercent2 = false
			this.mtextDisabled = false;
		}
	}
	set2(value){
	
		if(value=="none"){
			this.ctextDisabled = true;
			this.CustomerMaintenance.creditLimitAmount='';
		}else if(value=="unlimited"){
			this.ctextDisabled = true;
			this.isPercent3 = true
			this.CustomerMaintenance.creditLimitAmount='';
		}else{
			this.isPercent3 = false
			this.ctextDisabled = false;
		}
	}
    // Serachable Select Box Step4
	getPaymentTermByID(item:any){
		this.paymentTermId=[];
		this.paymentTermId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("paymentTermId")
		
	}
	OnPaymentTermIdDeSelect(item:any){
		this.paymentTermId=[];
		this.commonService.closeMultiselectWithId("paymentTermId")
	}
	
	getPriceLevelByID(item:any){
		this.priceLevel=[];
		this.priceLevel.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("priceLevel")
		
	}
	OnPriceLevelIdDeSelect(item:any){
		this.priceLevel=[];
		this.commonService.closeMultiselectWithId("priceLevel")
	}

	getShipmentMethodByID(item:any){
		this.shipmentMethodId=[];
		this.shipmentMethodId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("shipmentMethodId")
		
	}
	OnShipmentMethodIdDeSelect(item:any){
		this.shipmentMethodId=[];
		this.commonService.closeMultiselectWithId("shipmentMethodId")
	}

	getVatScheduleByID(item:any){
		this.vatId=[];
		this.vatId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("vatId")
		
	}
	OnVatScheduleIdDeSelect(item:any){
		this.vatId=[];
		this.commonService.closeMultiselectWithId("vatId")
	}

	getCurrencyByID(item:any){
		this.currencyId=[];
		this.currencyId.push({'id':item.id,'itemName':item.itemName});
		this.selectedCurrency = item.id
		this.commonService.closeMultiselectWithId("currencyId")
		if(this.CustomerMaintenance.financeChargeAmount.length > 0){
			this.formatfinanceChargeAmount()
		}
		if(this.CustomerMaintenance.minimumChargeAmount.length > 0){
			this.formatminimumChargeAmount()
		}
		if(this.CustomerMaintenance.creditLimitAmount.length > 0){
			this.formatCreditLimitAmount()
		}
		if(this.CustomerMaintenance.tradeDiscountPercent.length > 0){
			this.formatTradeDiscountPercent()
		}
		
	}
	CurrencyIdDeSelect(item:any){
		this.currencyId=[];
		this.formatfinanceChargeAmountChanged();
		this.formatCreditLimitAmountChanged();
		this.formatminimumChargeAmountChanged();
		this.formatTradeDiscountPercentChanged(); 
		this.commonService.closeMultiselectWithId("currencyId")
	}
	getCheckBookByID(item:any){
		this.checkbookId=[];
		this.checkbookId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("checkbookId")
	}
	OnCheckBookIdDeSelect(item:any){
		this.checkbookId=[];
		this.commonService.closeMultiselectWithId("checkbookId")
	}

	getSalesTerritoryByID(item:any){
		this.salesTerritoryId=[];
		this.salesTerritoryId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("salesTerritoryId")
		
	}
	OnSalesTerritoryIdDeSelect(item:any){
		this.salesTerritoryId=[];
		this.commonService.closeMultiselectWithId("salesTerritoryId")
	}

	getSalesmanByID(item:any){
		this.salesmanId=[];
		this.salesmanId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("salesmanId")
		
	}
	OnSalesmanIdDeSelect(item:any){
		this.salesmanId=[];
		this.commonService.closeMultiselectWithId("salesmanId")
	}
	formatfinanceChargeAmount(){
		this.commonService.changeInputText("financeChargeAmount")
		var inputValue = 0;
		if(this.editData){
			if(this.editfinanceChargeAmount == 'undefined' && this.unFrmtfinanceChargeAmount){
				inputValue = this.unFrmtfinanceChargeAmount
			}
			else if(this.editfinanceChargeAmount == 'undefined'){
				inputValue = this.records.financeChargeAmount
			}else{
                inputValue = this.editfinanceChargeAmount
            }
		}else{
			 inputValue = this.unFrmtfinanceChargeAmount
		}
		
		if(this.isPercent1 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.CustomerMaintenance.financeChargeAmount = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatfinanceChargeAmountChanged()
			}
		}
	}
	formatfinanceChargeAmountChanged(){
		this.commonService.changeInputNumber("financeChargeAmount")
		if(this.editData){
			if(this.editMinimumChargeAmount == undefined){
				this.CustomerMaintenance.financeChargeAmount = this.editData.financeChargeAmount
			}else{
				this.CustomerMaintenance.financeChargeAmount = this.editfinanceChargeAmount
			}
		}else{
			this.CustomerMaintenance.financeChargeAmount = this.unFrmtfinanceChargeAmount
		}
	}
	
	getfinanceChargeAmountValue(event){
		this.editfinanceChargeAmount = undefined
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		
		this.unFrmtfinanceChargeAmount = event
	}

	//minimumChargeAmount
	//add Symbols
	formatminimumChargeAmount(){
		this.commonService.changeInputText("minimumChargeAmount")
		var inputValue = 0;
		if(this.editData){
			if (this.editMinimumChargeAmount == undefined && this.unFrmtminimumChargeAmount){
				inputValue = this.unFrmtminimumChargeAmount
			}
			else if(this.editMinimumChargeAmount == undefined){
				inputValue = this.records.minimumChargeAmount
			}else{
				inputValue = this.editMinimumChargeAmount
			}
			this.minimumChargeAmountValue =  inputValue
		}else{
			inputValue = this.unFrmtminimumChargeAmount
		}
		if(this.isPercent2 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.CustomerMaintenance.minimumChargeAmount = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatminimumChargeAmountChanged()
			}
		}
	}
	//Change To Original Value of minimumChargeAmount
	formatminimumChargeAmountChanged(){
		this.commonService.changeInputNumber("minimumChargeAmount")
			if(this.editData){
				if(this.editMinimumChargeAmount == undefined){
					this.CustomerMaintenance.minimumChargeAmount = this.editData.minimumChargeAmount
				}else{
					this.CustomerMaintenance.minimumChargeAmount = this.editMinimumChargeAmount
				}
			}else{
				
			this.CustomerMaintenance.minimumChargeAmount = this.unFrmtminimumChargeAmount
			}
		}
	
	// get Original Minimum Charge Amount
	getminimumChargeAmountValue(event){
		this.editMinimumChargeAmount = undefined
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		
		this.unFrmtminimumChargeAmount = event
	}
	//End MinimumChargeAmount

	//Credit Limit Amount
	//add Symbols
	formatCreditLimitAmount(){
		this.commonService.changeInputText("creditLimitAmount")
		var inputValue = 0;
		
		if(this.editData){
			if (this.editCreditLimitAmount == undefined && this.unFrmtCreditLimitAmount ){
				inputValue = this.unFrmtCreditLimitAmount
			}
			else if(this.editCreditLimitAmount == undefined){
                inputValue = this.editData.creditLimitAmount
            }else{
                inputValue = this.editCreditLimitAmount
            }
			this.creditLimitAmountValue = inputValue
		}else{
			inputValue = this.unFrmtCreditLimitAmount
		}
		if(this.isPercent3 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.CustomerMaintenance.creditLimitAmount = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatCreditLimitAmountChanged()
			}
		}
	}
	//Change To Original Value of Credit Limit Amount
	formatCreditLimitAmountChanged(){
		this.commonService.changeInputNumber("creditLimitAmount")
		if(this.editData){
			if(this.editCreditLimitAmount == undefined){
                this.CustomerMaintenance.creditLimitAmount = this.editData.creditLimitAmount
            }else{
                this.CustomerMaintenance.creditLimitAmount = this.editCreditLimitAmount
            }
		}else{
			this.CustomerMaintenance.creditLimitAmount = this.unFrmtCreditLimitAmount
		}
		
		
	}
	// get Original Credit Limit Amount
	getCreditLimitAmountValue(event){
		this.editCreditLimitAmount = undefined
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		this.unFrmtCreditLimitAmount = event
	}
	//End CreditLimitAmount

    getTradeDiscountPercent(event){
		this.editTradeDiscountPercent = undefined
		if(event){
			var amt = event.split(".")
			if(amt.length == 1 && amt[0].length >= 6){
				event = amt[0].slice(0, 6)
			}
			else{
				if(amt.length > 1 && amt[1].length >= 3){
					event = amt[0]+"."+amt[1].slice(0, 3)
				}
				if(amt.length > 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)+"."+amt[1]
				}
			}
		}
		this.unFrmtTradeDiscountPercent = event

	}
	formatTradeDiscountPercent(){
		this.commonService.changeInputText("tradeDiscountPercent")
		var inputValue = 0;
		if(this.editData){
			if (this.editTradeDiscountPercent == undefined && this.unFrmtTradeDiscountPercent ){
				inputValue = this.unFrmtTradeDiscountPercent
			}
			else if(this.editTradeDiscountPercent == undefined){
                inputValue = this.editData.tradeDiscountPercent
            }else{
                inputValue = this.editTradeDiscountPercent
            }
			this.TradeDiscountPercentValue = inputValue
		}else{
			inputValue = this.unFrmtTradeDiscountPercent
		}
		if(this.isPercent3 == false){
			if (this.selectedCurrency !=undefined ){
				let submitInfo = {
					'currencyId':this.selectedCurrency
				};
				this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
					this.CustomerMaintenance.tradeDiscountPercent = this.commonService.formatAmount(data, inputValue)
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
					}, 100)
				});
				
				
			}else{
				this.formatTradeDiscountPercentChanged()
			}
		}

	}
	formatTradeDiscountPercentChanged(){
		this.commonService.changeInputNumber("tradeDiscountPercent")
		if(this.editData){
			if(this.editTradeDiscountPercent == 'undefined'){
                this.CustomerMaintenance.tradeDiscountPercent = this.editData.tradeDiscountPercent
            }else{
                this.CustomerMaintenance.tradeDiscountPercent = this.editTradeDiscountPercent
            }
		}else{
			this.CustomerMaintenance.tradeDiscountPercent = this.unFrmtTradeDiscountPercent
		}
	}
	
	// 	getCustomerNumberByID(item:any){
	//    this.customerNumberId=[];
    //    this.customerNumberId.push({'id':item.id,'itemName':item.itemName});
	//    this.customerOptionsMaintenanceService.getCustomerByID(item.id).then(data => {
	// 			this.simpleName = data.result.name;
	// 			this.arabicName = data.result.arabicName;
	// 	});
    // }
	// OnCustomerNumberIdDeSelect(item:any){
       
	//    this.simpleName ='';
	//    this.arabicName ='';
    // }
	// Serachable Select Box Step4 ends
    
	redirect1(){
		this.router.navigate(['masterdata/customermaintenance']);
	}

    // getCustomerDetail(currentVendorId){
	// 	
	//    this.customerOptionsMaintenanceService.getCustomerData(this.currentVendorId).then(pagedData => {
	// 	   this.currentVendorId = pagedData.result.customerNumberId;
	// 	   this.CustomerMaintenance.balanceType= pagedData.result.balanceType;
	// 	   this.CustomerMaintenance.financeCharge= pagedData.result.financeCharge;
	// 	   this.CustomerMaintenance.minimumCharge= pagedData.result.minimumCharge;
	// 	   this.CustomerMaintenance.creditLimit= pagedData.result.creditLimit;
	// 	   this.CustomerMaintenance.tradeDiscountPercent=pagedData.result.tradeDiscountPercent;
	// 	   this.paymentTermId=pagedData.result.paymentTermId;
	// 	   this.shipmentMethodId=pagedData.result.shipmentMethodId;
	// 	   this.vatId=pagedData.result.vatId;
	// 	   this.currencyId=pagedData.result.currencyId;
	// 	   this.priceLevel=pagedData.result.priceLevel;
	// 	   this.salesmanId=pagedData.result.salesmanId;
	// 	   this.salesTerritoryId=pagedData.result.salesTerritoryId;
	// 	   this.checkbookId=pagedData.result.checkbookId;
	// 	   this.CustomerMaintenance.customerPriority=pagedData.result.customerPriority;
    //        this.CustomerMaintenance.openMaintenanceHistoryCalendarYear=pagedData.result.openMaintenanceHistoryCalendarYear;
	// 	   this.CustomerMaintenance.openMaintenanceHistoryTransaction=pagedData.result.openMaintenanceHistoryTransaction;
	// 	   this.CustomerMaintenance.openMaintenanceHistoryFiscalYear=pagedData.result.openMaintenanceHistoryFiscalYear;
	// 	   this.CustomerMaintenance.openMaintenanceHistoryDistribution=pagedData.result.openMaintenanceHistoryDistribution;
		  
	//    });
	//  }


	ClearForm(customerMaintenanceOptions:NgForm){
		//customerMaintenanceOptions.resetForm();
		this.unFrmtCreditLimitAmount = ''
		this.unFrmtfinanceChargeAmount = ''
		this.unFrmtminimumChargeAmount = ''
		this.unFrmtTradeDiscountPercent = '' 
		this.editfinanceChargeAmount = ''
		this.editMinimumChargeAmount = ''
		this.editCreditLimitAmount = ''
		this.editTradeDiscountPercent = ''
		this.selectedCurrency = undefined
		this.isPercent1   = false
		this.isPercent2 = false
		this.isPercent3 = false
		this.ArrCustomerNumberId=[];
		// this.customerNumberId=[];
		// this.arabicName='';
		// this.simpleName='';
		this.customerNumberId=[];
		this.paymentTermId=[];
		this.shipmentMethodId=[];
		this.vatId=[];
		this.currencyId=[];
		this.priceLevel=[];
		this.salesmanId=[];
		this.salesTerritoryId=[];
		this.checkbookId=[];
	      /*customerMaintenanceOptions.resetForm();
		  var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		  selectedValue.value=this.editData.customerNumberId;
		  this.CustomerMaintenance.customerNumberId = this.editData.customerNumberId;*/
		  this.CustomerMaintenance=
			{
				'customerNumberId' : this.editData.customerNumberId,
				'balanceType' : '1',
				'financeCharge' : '0',
				'financeChargeAmount' : '',
				'minimumCharge' : '0',
				'minimumChargeAmount' : '',
				'creditLimit' : '0',
				'creditLimitAmount' : '',
				'tradeDiscountPercent' : '',
				'paymentTermId' : '',
				'shipmentMethodId' : '',
				'vatId' : '',
				'currencyId' : '',
				'priceLevel' : '',
				'salesmanId' : '',
				'salesTerritoryId' : '',
				'checkbookId' : '',
				'customerPriority' : '',
				'openMaintenanceHistoryCalendarYear' : false,
				'openMaintenanceHistoryTransaction' : false,
				'openMaintenanceHistoryFiscalYear' : false,
				'openMaintenanceHistoryDistribution' : false,
			}
		    this.customerOptionsMaintenanceService.getCustomerByID(this.editData.customerNumberId).then(data => {
				this.records = data.result;
				this.simpleName = data.result.name;
				this.arabicName = data.result.arabicName;
			
			});
				
			 if(this.condition){
				this.myHtml = "border-vanish";
			 }else{
				this.myHtml = "";
			 }
	}
	
	ngOnInit() {
             
			this.route.params.subscribe((params: Params) => {
                this.currentVendorId = params['customerId'];
				});
				
			this.customerOptionsMaintenanceService.getCustomerByID(this.currentVendorId ).then(data => {
					
					this.simpleName = data.result.name;
					this.arabicName = data.result.arabicName;
			});
            //  this.getCustomerDetail(this.currentVendorId);
			  
		  	this.ddlCustomerNumberIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		  this.ddlPaymentTermIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };
		 // Serachable Select Box Step2 ends

		 	// Serachable Select Box Step2
		   this.ddlShipmentMethodSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		 	this.ddlVatScheduleSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		 
		this.ddlCurrencyIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };

		 	this.ddlCheckBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };

		this.ddlSalesTerritoryIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };
		 
		  	this.ddlSalesmanIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };
         
		 this.ddlPriceLevelSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		this.getPriceLevel();
		this.getPayment();
		this.getShippingMethod();
    	this.getVatDetail();
		this.getCurrency();
		this.getSalesman();
		this.getSalesTerritory();
    	this.getCheckBook();
	
		
		window.setTimeout(() => {
			this.getCustomerData();
		}, 500);
	}	

	getPriceLevel()
	{
		this.customerOptionsMaintenanceService.getPriceLevelList().then(data=> {
			this.ArrPriceLevel=[];
			
			 for(var i=0;i< data.result.length;i++)
              {
                this.ArrPriceLevel.push({ "id": data.result[i].priceLevelIndex, "itemName": data.result[i].description})
              }
        });
	}
	getPayment()
	{
		this.customerOptionsMaintenanceService.searchPayment().subscribe(pagedData => {
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrPaymentTermId.push({ "id": pagedData.records[i].paymentTermId, "itemName": pagedData.records[i].description})
              }
        });
	}
	getShippingMethod()
	{
		this.customerOptionsMaintenanceService.searchShippingMethod().subscribe(pagedData => {
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrShipmentMethodId.push({ "id": pagedData.records[i].shipmentMethodId, "itemName": pagedData.records[i].shipmentDescription})
              }
        });
	}
    getVatDetail()
	{
		//fetch vat method records 
		this.customerOptionsMaintenanceService.searchVat().subscribe(pagedData => {
            // this.vat = pagedData.records;
			// Searchable Select Box Step3
					
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrVatScheduleId.push({ "id": pagedData.records[i].vatScheduleId, "itemName": pagedData.records[i].vatDescription})
              }
			    // Searchable Select Box Step3 End
        });
	}
	getCurrency()
	{
		this.customerOptionsMaintenanceService.searchCurrency().subscribe(pagedData => {
            // this.currency = pagedData.records;
		
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrCurrencyId.push({ "id": pagedData.records[i].currencyId, "itemName": pagedData.records[i].currencyDescription})
              }
        });
	}

	getSalesman()
	{
		//fetch Salesman records 
		this.customerOptionsMaintenanceService.searchSalesman().subscribe(pagedData => {
			 this.ArrSalesmanId=[];
            // this.salesman = pagedData.records;
	
			for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrSalesmanId.push({ "id": pagedData.records[i].salesmanId, "itemName": pagedData.records[i].salesmanFirstName})
              }
        });
	}
  	getSalesTerritory()
 	{
		this.customerOptionsMaintenanceService.searchsalesTerritory().subscribe(pagedData => {
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrSalesTerritoryId.push({ "id": pagedData.records[i].salesTerritoryId, "itemName": pagedData.records[i].descriptionPrimary})
              }
        });
    }


	getCheckBook()
	{
			//fetch checkbook records 
		this.customerOptionsMaintenanceService.searchCheckbook().subscribe(pagedData => {
            // this.checkbook = pagedData.records;
			this.ArrCheckBookId = [];
			
							// Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrCheckBookId.push({ "id": pagedData.records[i].checkBookId, "itemName": pagedData.records[i].checkBookId})
              }
			    // Searchable Select Box Step3 End
        });
	}
	
	getCustomerData()
	{
		
		this.customerOptionsMaintenanceService.getCustomerData(this.currentVendorId ).then(data => {
			
			this.isModify=true;
            this.editData = data.result;
           	this.checkbookId = [];
			if(this.ArrCustomerNumberId.length == 0){
				this.customerOptionsMaintenanceService.searchCustomers().subscribe(pagedData => {
					for(var i=0;i< pagedData.records.length;i++)
					{
						this.ArrCustomerNumberId.push({ "id": pagedData.records[i].customerId, "itemName": pagedData.records[i].name})
					}
					var	selectedCustomerNumberId= this.ArrCustomerNumberId.find(x => x.id ==  data.result.customerNumberId);
					if(selectedCustomerNumberId){
						this.customerNumberId.push({'id':selectedCustomerNumberId.id,'itemName':selectedCustomerNumberId.itemName});
					}
					
				});
			
			}
			else
			{
				var	selectedCustomerNumberId= this.ArrCustomerNumberId.find(x => x.id ==  data.result.customerNumberId);
				if(selectedCustomerNumberId){
					this.customerNumberId.push({'id':selectedCustomerNumberId.id,'itemName':selectedCustomerNumberId.itemName});
				}
			}
			var	selectedPaymentTermId= this.ArrPaymentTermId.find(x => x.id ==  data.result.paymentTermId);
			if(selectedPaymentTermId){
				this.paymentTermId.push({'id':selectedPaymentTermId.id,'itemName':selectedPaymentTermId.itemName});
			}
			var	selectedAsset = this.ArrShipmentMethodId.find(x => x.id ==  data.result.shipmentMethodId);
			if(selectedAsset){
				this.shipmentMethodId.push({'id':selectedAsset.id,'itemName':selectedAsset.itemName});
			}
			var	selectedBook = this.ArrVatScheduleId.find(x => x.id ==  data.result.vatId);
			if(selectedBook){
				this.vatId.push({'id':selectedBook.id,'itemName':selectedBook.itemName});
			}
			
			var	selectedCurrency = this.ArrCurrencyId.find(x => x.id ==  data.result.currencyId);
			if(selectedCurrency){
				this.selectedCurrency = selectedCurrency.id;
				this.currencyId.push({'id':selectedCurrency.id,'itemName':selectedCurrency.itemName});
			}
			this.CustomerMaintenance.minimumChargeAmount = this.editData.minimumChargeAmount;
			this.CustomerMaintenance.financeChargeAmount = this.editData.financeChargeAmount;
			this.CustomerMaintenance.creditLimitAmount = this.editData.creditLimitAmount;
			this.CustomerMaintenance.tradeDiscountPercent = this.editData.tradeDiscountPercent
			if(this.commonService.checkIsNumeric(this.editData.financeChargeAmount)){
				this.editfinanceChargeAmount = data.result.financeChargeAmount
				this.formatfinanceChargeAmount();
			}
			if(this.commonService.checkIsNumeric(this.editData.minimumChargeAmount)){
				this.editMinimumChargeAmount = data.result.minimumChargeAmount
				this.formatminimumChargeAmount();
			}
			if(this.commonService.checkIsNumeric(this.editData.creditLimitAmount)){
				this.editCreditLimitAmount = data.result.creditLimitAmount
				this.formatCreditLimitAmount();
			}
			if(this.commonService.checkIsNumeric(this.editData.tradeDiscountPercent)){
				this.editTradeDiscountPercent = this.editData.tradeDiscountPercent
				this.formatTradeDiscountPercent();
			}
			
			if(this.editData.financeCharge == '1'){
				this.isPercent1 = true
			}
			if(this.editData.minimumCharge == '1'){
				this.isPercent2 = true	
			}
			if(this.editData.creditLimit == '1'){
				this.isPercent3 = true
			}
			if(data.result.priceLevel)
			{
				var	selectedPriceLevel = this.ArrPriceLevel.find(x => x.id ==  data.result.priceLevel);
				if(selectedPriceLevel)
				{
					this.priceLevel.push({'id':selectedPriceLevel.id,'itemName':selectedPriceLevel.itemName});
				}
			}
		 	this.checkbookId = [];
			  if(this.ArrCheckBookId.length == 0)
			  {
				 this.ArrCheckBookId = [];
				 this.customerOptionsMaintenanceService.searchCheckbook().subscribe(pagedData => {
				 for(var i=0;i< pagedData.records.length;i++)
           		 {
                	this.ArrCheckBookId.push({ "id": pagedData.records[i].checkBookId, "itemName": pagedData.records[i].checkbookDescription})
              	 }
			    // Searchable Select Box Step3 End
				 var selectedCheckBook = this.ArrCheckBookId.find(x => x.id ==  data.result.checkbookId);
	         	  this.checkbookId.push({'id':selectedCheckBook.id,'itemName':selectedCheckBook.itemName});
        		 });

			
			  }
			  else
			  {
				   var selectedCheckBook = this.ArrCheckBookId.find(x => x.id ==  data.result.checkbookId);
	         	   this.checkbookId.push({'id':selectedCheckBook.id,'itemName':selectedCheckBook.itemName});
			  }
			
		  
			var	selectedSalesTerritory = this.ArrSalesTerritoryId.find(x => x.id ==  data.result.salesTerritoryId);
	         this.salesTerritoryId.push({'id':selectedSalesTerritory.id,'itemName':selectedSalesTerritory.itemName});

			 
			var	selectedSalesman = this.ArrSalesmanId.find(x => x.id ==  data.result.salesmanId);
	         this.salesmanId.push({'id':selectedSalesman.id,'itemName':selectedSalesman.itemName});
				
			if(this.editData){
				this.condition=true;
				this.customerOptionsMaintenanceService.getCustomerByID(this.editData.customerNumberId).then(data => {
				this.records = data.result;
				this.simpleName = data.result.name;
				this.arabicName = data.result.arabicName;
			
			});
				if(this.editData.openMaintenanceHistoryCalendarYear==1){
					var histCal = true;
				}else{
					var histCal = false;
				}
				if(this.editData.openMaintenanceHistoryTransaction==1){
					var histTrans = true;
				}else{
					var histTrans = false;
				}
				if(this.editData.openMaintenanceHistoryFiscalYear==1){
					var histFisc = true;
				}else{
					var histFisc = false;
				}
				if(this.editData.openMaintenanceHistoryDistribution==1){
					var histDis = true;
				}else{
					var histDis = false;
				}
			this.CustomerMaintenance=
				{
					'customerNumberId' : this.editData.customerNumberId,
					'balanceType' : this.editData.balanceType.toString(),
					'financeCharge' : this.editData.financeCharge.toString(),
					'financeChargeAmount' : this.editData.financeChargeAmount,
					'minimumCharge' : this.editData.minimumCharge.toString(),
					'minimumChargeAmount' : this.editData.minimumChargeAmount,
					'creditLimit' : this.editData.creditLimit.toString(),
					'creditLimitAmount' : this.editData.creditLimitAmount,
					'tradeDiscountPercent' : this.editData.tradeDiscountPercent,
					'paymentTermId' : this.editData.paymentTermId,
					'shipmentMethodId' : this.editData.shipmentMethodId,
					'vatId' : this.editData.vatId,
					'currencyId' : this.editData.currencyId,
					'priceLevel' : this.editData.priceLevel,
					'salesmanId' : this.editData.salesmanId,
					'salesTerritoryId' : this.editData.salesTerritoryId,
					'checkbookId' : this.editData.checkbookId,
					'customerPriority' : this.editData.customerPriority,
					'openMaintenanceHistoryCalendarYear' : histCal,
					'openMaintenanceHistoryTransaction' : histTrans,
					'openMaintenanceHistoryFiscalYear' : histFisc,
					'openMaintenanceHistoryDistribution' : histDis,
				};
			}		
			if(this.editData.financeCharge==0){
				this.ftextDisabled = true;
				this.CustomerMaintenance.financeChargeAmount='';
			}
			if(this.editData.minimumCharge==0){
				this.mtextDisabled = true;
				this.CustomerMaintenance.minimumChargeAmount='';
			}
			if(this.editData.creditLimit==0 || this.editData.creditLimit==1){
				this.ctextDisabled = true;
				this.CustomerMaintenance.creditLimitAmount='';
			}
        });
	}

	
	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
	
	
}