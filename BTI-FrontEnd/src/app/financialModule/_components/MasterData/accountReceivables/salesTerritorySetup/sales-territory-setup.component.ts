import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SalesTerritorySetup } from '../../../../../financialModule/_models/master-data/accountReceivables/sales-territory-setup';
import { SalesTerritorySetupService } from '../../../../../financialModule/_services/MasterData/accountReceivable/sales-territory-setup.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../../_sharedresource/page';

@Component({
    selector: '<sales-territory-setup></sales-territory-setup>',
    templateUrl: './sales-territory-setup.component.html',
    styles: ["user.component.css"],
    providers:[SalesTerritorySetupService,CommonService]
})



//export to make it available for other classes
export class SalesTerritorySetupComponent {
    page = new Page();
    rows = new Array<SalesTerritorySetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    cardTypeStatus =[];
    disableId;
    myHtml;
    add;
    requiredValid=Constants.requiredValid;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    
    btndisabled:boolean=false;
    isScreenLock;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private salesTerritorySetupService: SalesTerritorySetupService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService){
            this.page.pageNumber = 0;
            this.page.size = 5;
            this.disableId = false;
            this.add = true;
        }
        
        ngOnInit() {
            this.getAddScreenDetail();
            this.getViewScreenDetail();
            this.setPage({ offset: 0 });
            
        }
        
        getAddScreenDetail()
        {
            this.screenCode = "S-1137";
            // default form parameter for adding exchange creditCardSetup set up
            this.defaultAddFormValues = [
                { 'fieldName': 'ADD_SALESTERRITORY_TERRITORY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_PHONE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_PHONE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_MID_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_FIRST_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_MID_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_LAST_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_TOTAL_COMMISSIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_COST_OF_SALES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_COMMISSIONS_SALES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_YEAR_TO_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_LAST_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESTERRITORY_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESTERRITORY_MANAGER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
                this.getScreenDetail(this.screenCode,'Add');
            }
            
            getViewScreenDetail()
            {
                this.screenCode = "S-1138";
                this.defaultManageFormValues = [
                    { 'fieldName': 'MANAGE_SALESTERRITORY_TERRITORY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESTERRITORY_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESTERRITORY_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESTERRITORY_MANAGER_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESTERRITORY_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESTERRITORY_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESTERRITORY_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
                    
                ];
                this.getScreenDetail(this.screenCode,'Manage');
            }
            
            getScreenDetail(screenCode,ArrayType)
            {
                
                this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                    this.screenName=data.result.dtoScreenDetail.screenName;
                    this.moduleName=data.result.moduleName;
                    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                    if(ArrayType == 'Add')
                    {
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                    else
                    {
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                });
            }
            
            //setting pagination
            setPage(pageInfo) {
                this.selected = []; // remove any selected checkbox on paging
                this.page.pageNumber = pageInfo.offset;
                this.salesTerritorySetupService.searchSalesTerritory(this.page, this.searchKeyword).subscribe(pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                });
            }
            
            changePageSize(event) {
                this.page.size = event.target.value;
                this.setPage({ offset: 0 });
            }
            
            
            // getSalesTerritoryById(row: any)
            getSalesTerritoryById(row: any)
            {
                this.add = false;
                this.disableId = true;
                this.myHtml = "border-vanish";
                this.showBtns=true;
                this.salesTerritorySetupService.getSalesTerritoryById(row.salesTerritoryId).then(data => {
                    this.model = data.result; 
                });
            }
            
            updateFilter(event) {
                this.searchKeyword = event.target.value.toLowerCase();
                this.page.pageNumber = 0;
                this.page.size = this.ddPageSize;
                this.salesTerritorySetupService.searchSalesTerritory(this.page, this.searchKeyword).subscribe(pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.table.offset = 0;
                }, error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            
            //for reseting data
            clearForm(){
                this.model.descriptionPrimary= '';
                this.model.descriptionSecondary= '';
                this.model.phone1='';
                this.model.phone2='';
                this.model.managerFirstNamePrimary='';
                this.model.managerMidNamePrimary= '';
                this.model.managerLastNamePrimary='';
                this.model.managerFirstNameSecondary='';
                this.model.managerMidNameSecondary='';
                this.model.managerLastNameSecondary='';
                this.model.totalCommissionsYTD='';
                this.model.totalCommissionsLY='';
                this.model.commissionsSalesYTD='';
                this.model.commissionsSalesLY='';
                this.model.costOfSalesYTD='';
                this.model.costOfSalesLY='';
                if(!this.add){
                    this.myHtml = "border-vanish";
                }else{
                    this.myHtml = "";
                    this.model.salesTerritoryId='';
                }
            }
            
            Cancel(f)
            {
                this.add = true;
                this.model.salesTerritoryId= '';
                this.clearForm();
                this.disableId=false;
            }
            
            //get id already exist
            IsIdAlreadyExist(){
                if(this.model.salesTerritoryId != '' && this.model.salesTerritoryId != undefined && this.model.salesTerritoryId != 'undefined')
                {
                    this.salesTerritorySetupService.getSalesTerritoryById(this.model.salesTerritoryId).then(data => {
                        var datacode = data.code;
                        if (data.status == "NOT_FOUND") {
                            return true;
                        }
                        else{
                            var txtId = <HTMLInputElement> document.getElementById("salesTerritoryIdText");
                            txtId.focus();
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            window.scrollTo(0,0);
                            this.model.salesTerritoryId='';
                            return false;
                        }
                        
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
            }
            
            onlyDecimalNumberKey(event) {
                return this.getScreenDetailService.onlyDecimalNumberKey(event);
            }
            
            saveSalesTerritorySetup(f: NgForm)
            {
                this.btndisabled=true;
                if(!this.add)
                {
                    this.salesTerritorySetupService.updateSalesTerritorySetup(this.model).then(data => {
                        window.scrollTo(0,0);
                        this.btndisabled=false;
                        var datacode = data.code;
                        if (datacode == 201) {
                            this.isModify=false;
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.disableId = false;
                            this.showBtns=false;
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        f.resetForm();
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
                else
                {
                    this.salesTerritorySetupService.createSalesTerritorySetup(this.model).then(data => {
                        window.scrollTo(0,0);
                        this.showBtns=false;
                        this.disableId = false;
                        this.btndisabled=false;
                        var datacode = data.code;
                        if (datacode == 201) {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.setPage({ offset: 0 });
                            
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                        }
                        f.resetForm();
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
                this.btndisabled=false; 
            }
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
            
        }