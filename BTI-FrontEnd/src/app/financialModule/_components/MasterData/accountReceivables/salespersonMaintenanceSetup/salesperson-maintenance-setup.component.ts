import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { SalespersonMaintenanceSetup } from '../../../../../financialModule/_models/master-data/accountReceivables/salesperson-maintenance-setup';
import { SalespersonMaintenanceSetupService } from '../../../../../financialModule/_services/MasterData/accountReceivable/salespersonMaintenanceSetup.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {Constants} from '../../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../../_sharedresource/page';

@Component({
    selector: '<salesperson-maintenance-setup></salesperson-maintenance-setup>',
    templateUrl: './salesperson-maintenance-setup.component.html',
    styles: ["user.component.css"],
    providers:[SalespersonMaintenanceSetupService,CommonService]
})

//export to make it available for other classes
export class SalesPersonMaintenanceSetupComponent {
    page = new Page();
    rows = new Array<SalespersonMaintenanceSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    // Serachable Select Box Step1
	ArrsalesTerritoryId = [];
    salesTerritoryId : any = [];
    ddlsalesTerritorySetting = {};
	// Serachable Select Box Step1 End
    isModify:boolean=false;
    cardTypeStatus =[];
    salesTerritoryOpt;
    disableId;
    myHtml;
    add;
    requiredValid=Constants.requiredValid;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    
    btndisabled:boolean=false;
    isScreenLock;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private salespersonMaintenanceSetupService: SalespersonMaintenanceSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService){
            this.page.pageNumber = 0;
            this.page.size = 5;
            this.disableId = false;
            this.add = true;
        }
        
        ngOnInit() {
            // Serachable Select Box Step2
            this.ddlsalesTerritorySetting = { 
                singleSelection: true, 
                text:this.select,
                enableSearchFilter: true,
                classes:"myclass custom-class",
                searchPlaceholderText:this.search,
            }; 
            // Serachable Select Box Step2
            this.getAddScreenDetail();
            this.getViewScreenDetail();
            this.setPage({ offset: 0 });
            this.getSalesTerritoryList();
        }
        
        getAddScreenDetail()
        {
            this.screenCode = "S-1133";
            // default form parameter for adding exchange creditCardSetup set up
            this.defaultAddFormValues = [
                { 'fieldName': 'ADD_SALESPERSON_SALESPERSONS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_MID_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_FIRST_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_MID_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_LAST_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_TERRITORY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_ADDRESS1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_ADDRESS2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_PHONE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_PHONE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_TOTAL_COMMISSIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_COMMISSIONS_SALES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_COST_OF_SALES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_YEAR_TO_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_SALESPERSON_LAST_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_SALESPERSON_ACTIVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
                this.getScreenDetail(this.screenCode,'Add');
            }
            
            getViewScreenDetail()
            {
                this.screenCode = "S-1134";
                this.defaultManageFormValues = [
                    { 'fieldName': 'MANAGE_SALESPERSON_SALESPERSONS_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESPERSON_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESPERSON_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESPERSON_TERRITORY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESPERSON_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESPERSON_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESPERSON_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                    { 'fieldName': 'MANAGE_SALESPERSON_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
                    
                ];
                this.getScreenDetail(this.screenCode,'Manage');
            }
            
            getScreenDetail(screenCode,ArrayType)
            {
                this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                    this.screenName=data.result.dtoScreenDetail.screenName;
                    this.moduleName=data.result.moduleName;
                    this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                    if(ArrayType == 'Add')
                    {
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                    else
                    {
                        for (var j = 0; j < this.availableFormValues.length; j++) {
                            var fieldKey = this.availableFormValues[j]['fieldName'];
                            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                            var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                            objDefault['fieldValue'] = objAvailable['fieldValue'];
                            objDefault['helpMessage'] = objAvailable['helpMessage'];
                            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                            objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                            objDefault['readAccess'] = objAvailable['readAccess'];
                            objDefault['writeAccess'] = objAvailable['writeAccess'];
                        }
                    }
                });
            }
            
            //setting pagination
            setPage(pageInfo) {
                this.selected = []; // remove any selected checkbox on paging
                this.page.pageNumber = pageInfo.offset;
                this.salespersonMaintenanceSetupService.searchSalespersonSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                });
            }
            
            updateFilter(event) {
                this.searchKeyword = event.target.value.toLowerCase();
                this.page.pageNumber = 0;
                this.page.size = this.ddPageSize;
                this.salespersonMaintenanceSetupService.searchSalespersonSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                    
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.table.offset = 0;
                }, error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            
            changePageSize(event) {
                this.page.size = event.target.value;
                this.setPage({ offset: 0 });
            }
            
            getSalesTerritoryList()
            {
                this.salespersonMaintenanceSetupService.getSalesTerritoryList().then(data => {
                    // this.salesTerritoryOpt = data.result.records;
                    // Serachable Select Box Step3
                    for(var i=0;i< data.result.records.length;i++)
                    {
                        this.ArrsalesTerritoryId.push({ "id": data.result.records[i].salesTerritoryId, "itemName": data.result.records[i].salesTerritoryId})
                    }
                    // Serachable Select Box Step3 End
                });
            }
            
            
            // Serachable Select Box Step4
            getsalesTerritoryByID(item:any){
                this.model.salesTerritoryId=item.id;
                this.commonService.closeMultiselectWithId("salesTerritoryId")
            }
            OnsalesTerritoryDeSelect(item:any){
                this.model.salesTerritoryId='';
                this.commonService.closeMultiselectWithId("salesTerritoryId")
            }
            
            // getSalesmanMaintenanceById(row: any)
            getSalesmanMaintenanceById(row: any)
            {
                this.salesTerritoryId=[];
                this.add = false;
                this.disableId = true;
                this.myHtml = "border-vanish";
                this.showBtns=true;
                this.salespersonMaintenanceSetupService.salesmanMaintenanceGetById(row.salesmanId).then(data => {
                    this.model = data.result; 
                    this.model.inactive = !this.model.inactive;
                    this.salesTerritoryId.push({'id':data.result.salesTerritoryId,'itemName':data.result.salesTerritoryId});
                });
            }
            //for reseting data
            clearForm(){
                this.model.address1= '';
                this.model.address2= '';
                this.model.applyPercentage='';
                this.model.costOfSalesYTD='';
                this.model.costOfSalesLastYear='';   
                this.model.employeeId='';
                this.model.salesmanFirstName='';
                this.model.salesmanFirstNameArabic='';
                this.model.inactive='';
                this.model.salesmanLastName='';
                this.model.salesmanLastNameArabic= '';
                this.model.salesmanMidName='';
                this.model.salesmanMidNameArabic='';
                this.model.percentageAmount='';
                this.model.phone1='';
                this.model.phone2='';
                this.salesTerritoryId=[];
                this.model.salesCommissionsYTD='';
                this.model.salesCommissionsLastYear='';
                this.model.totalCommissionsYTD='';
                this.model.totalCommissionsLastYear='';
                if(!this.add){
                    this.myHtml = "border-vanish";
                }else{
                    this.myHtml = "";
                    this.model.salesmanId='';
                }
            }
            
            Cancel(f)
            {
                this.add=true;
                this.model.salesmanId='';
                this.clearForm();
                this.disableId=false;
            }
            //get id already exist
            IsIdAlreadyExist(){
                if(this.model.salesmanId != '' && this.model.salesmanId != undefined && this.model.salesmanId != 'undefined')
                {
                    this.salespersonMaintenanceSetupService.salesmanMaintenanceGetById(this.model.salesmanId).then(data => {
                        var datacode = data.code;
                        if (data.status == "NOT_FOUND") {
                            return true;
                        }
                        else{
                            var txtId = <HTMLInputElement> document.getElementById("salesmanIdText");
                            txtId.focus();
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.setPage({ offset: 0 });
                            window.setTimeout(() => {
                                this.showMsg = false;
                                this.hasMsg = false;
                            }, 4000);
                            window.scrollTo(0,0);
                            this.model.salesmanId='';
                            return false;
                        }
                        
                    }).catch(error => {
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                        }, 100)
                    });
                }
            }
            
            onlyDecimalNumberKey(event) {
                return this.getScreenDetailService.onlyDecimalNumberKey(event);
            }
            
            saveSalespersonMaintenance(f: NgForm)
            {
                this.btndisabled=true;
                if(f.valid && this.model.salesTerritoryId && this.model.salesTerritoryId != '')
                {
                    this.model.inactive = !this.model.inactive;
                    if(this.model.inactive){
                        var active = "1";
                    }else{
                        var active = "0";
                    }
                    this.model.inactive=active;
                    if(!this.add)
                    {
                        
                        this.salespersonMaintenanceSetupService.updateSalesmanMaintenance(this.model).then(data => {
                            this.salesTerritoryId=[];
                            window.scrollTo(0,0);
                            this.btndisabled=false;
                            var datacode = data.code;
                            if (datacode == 200) {
                                this.isModify=false;
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.model.salesmanId=null;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.disableId = false;
                                this.showBtns=false;
                                this.setPage({ offset: 0 });
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            f.resetForm();
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                    }
                    else
                    {
                        this.salespersonMaintenanceSetupService.createSalesPersonMaintenaceSetup(this.model).then(data => {
                            this.btndisabled=false;
                            this.salesTerritoryId=[];
                            window.scrollTo(0,0);
                            this.showBtns=false;
                            this.disableId = false;
                            var datacode = data.code;
                            if (datacode == 200) {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.setPage({ offset: 0 });
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            else{
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = data.btiMessage.message;
                                this.setPage({ offset: 0 });
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                            }
                            f.resetForm();
                        }).catch(error => {
                            window.setTimeout(() => {
                                this.isSuccessMsg = false;
                                this.isfailureMsg = true;
                                this.showMsg = true;
                                this.hasMsg = true;
                                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                            }, 100)
                        });
                    }
                    
                }
                
                
                
            }
            
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
        }