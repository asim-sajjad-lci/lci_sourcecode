import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NationalAccountMaintenance } from '../../../../../financialModule/_models/master-data/accountReceivables/national-account-maintenance';
import { NationalAccountMaintenanceService } from '../../../../../financialModule/_services/MasterData/accountReceivable/national-account-maintenance.service';
import { masterAccountReceivableService } from '../../../../../financialModule/_services/MasterData/accountReceivable/masterAccountReceivables.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../../_sharedresource/page'; 

@Component({
    selector: '<national-account-setup></national-account-setup>',
    templateUrl: './national-account-maintenance.component.html',
    styles: ["user.component.css"],
    providers:[NationalAccountMaintenanceService,CommonService]
})

//export to make it available for other classes
export class NationalAccountMaintenanceComponent {
    page = new Page();
    rows = new Array<NationalAccountMaintenance>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    isScreenLock;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    getCustomerId;
    model: any = {};
    custNameSecondary;
    custNamePrimary;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean=false;
    ArrDepreciationPeriodTypes =[];
    calenderoption=[];
    calenderString=[];
    disableId;
    myHtml;
    add;
    childCustId;
    childCustNamePrimary;
    chilsCustNameSecondary;
    custChildBalance;
    accountMaintenanceDetail =[];
    errortest;
    // Serachable Select Box Step1
	ArrCustNumber = [];
    custNumber : any = [];
    ddlCustNumberSetting = {};
	// Serachable Select Box Step1 End
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    fillAllFields= Constants.fillAllFields;

    btndisabled:boolean=false;

     @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private nationalAccountMaintenanceService: NationalAccountMaintenanceService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService){
        this.getCustomerId =[];    ;
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.disableId = false;
        this.add = true;
        
        this.nationalAccountMaintenanceService.getCustomerId().then(data => {
            this.getCustomerId = data.result.records;
				 // Serachable Select Box Step3
               //  alert(data.result.records.length)
                //  alert(data.result.records[i].custNumber)
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrCustNumber.push({ "id": data.result.records[i].customerId, "itemName": data.result.records[i].customerId })
              }
			    // Serachable Select Box Step3 End
        });
    }

    ngOnInit() {
        // this.custNameSecondary='';
        // this.custNamePrimary='';
        this.getAddScreenDetail();
        this.getViewScreenDetail();
        this.setPage({ offset: 0 });
          // Serachable Select Box Step2
		   this.ddlCustNumberSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
    }

 
    getAddScreenDetail()
    {
         this.screenCode = "S-1165";
           // default form parameter for adding exchange structureSetup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_NAT_ACC_PARENT_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_NAT_ACC_PARENT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_PARENT_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_NAT_ACC_OPTIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_ALLOW_RECIPT_ENTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_BASE_CREDIT_CHECK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_APPLY_HOLD_INACTIVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_BASE_FINANCE_CHARGE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            {'fieldName': 'ADD_NAT_ACC_CHILD_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_CHILD_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_CHILD_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_CHILD_BALANCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            {'fieldName': 'ADD_NAT_ACC_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NAT_ACC_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
           
            ];
         this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
			{
				this.isScreenLock = res;
			});
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1166";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_NAT_ACC_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_NAT_ACC_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_NAT_ACC_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_NAT_ACC_NO_ACCOUNT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_NAT_ACC_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_NAT_ACC_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_NAT_ACC_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
              
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }
 
    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        
        this.nationalAccountMaintenanceService.searchNationalAccountMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
            
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

     updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.nationalAccountMaintenanceService.searchNationalAccountMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
    
    RemoveArrValue(RowIndex)
    {
         
        this.model.accountMaintenanceDetail.splice(RowIndex, 1);
         
        this.accountMaintenanceDetail = this.model.accountMaintenanceDetail ;

    }

    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    
    // getNationalAccountMaintenanceById(row: any)
    getNationalAccountMaintenanceById(row: any)
    {
        this.add = false;
        this.isModify=true;
         this.disableId = true;
         this.myHtml = "border-vanish";
         this.custNumber=[];
         this.showBtns=true;
         this.nationalAccountMaintenanceService.getNationalAccountMaintenanceById(row.custNumber).then(data => {
                    this.model = data.result; 
                      var selectedCustNumber = this.ArrCustNumber.find(x => x.id ==  data.result.custNumber);
                     this.custNumber.push({ "id": selectedCustNumber.id, "itemName": selectedCustNumber.itemName});
                
         });
    }
    //for clear data
    clearForm()
    {
        this.custChildBalance='';
        this.getCustomerId=[];
        this.custNumber=[];  
        this.model.custNamePrimary= '';
        this.model.custNameSecondary= '';
        this.model.allowReceiptEntry='';
        this.model.baseCreditCheck='';
        this.model.applyStatus='';   
        this.model.baseFinanceCharge='';
        this.model.accountMaintenanceDetail=[];
       
        
      if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
		 }
         if(this.add){
			this.model.custNumber='';
		 }
    }

    
    Cancel(f)
    {
    this.isModify=false;
    this.clearForm();
    }

    	 // Serachable Select Box Step4
		getCustNumberByID(item:any){
			
		 this.model.custNumber=item.id;
          this.custNumber.push({'id':item.id});
	      this.nationalAccountMaintenanceService.getCustomerDetails(item.id).then(data => {
			 this.model.custNamePrimary = data.result.name;
	         this.model.custNameSecondary = data.result.arabicName;
        });
        this.commonService.closeMultiselectWithId("custNumber")
    }

    getdetail($event){
        //
       // alert($event.target.value);
      //  alert("this.model.childCustId"+this.model.childCustId)
      //  alert("this.model.childCustNamePrimary"+this.model.childCustNamePrimary)
        
          this.childCustId=$event.target.value;
          //this.childCustId.push({'id':$event.target.value});
          this.nationalAccountMaintenanceService.getCustomerDetails($event.target.value).then(data => {
              
			 this.childCustNamePrimary = data.result.name;
	         this.chilsCustNameSecondary = data.result.arabicName;
             });
       
    }
		OnCustNumberDeSelect(item:any){
			this.custNamePrimary='';
            this.custNameSecondary='';
            this.commonService.closeMultiselectWithId("custNumber")
		}
		// Serachable Select Box Step4 ends
  
    clearArray(){
    
     this.model.accountMaintenanceDetail=[];

    }
    generateAccountArray()
    {
        
       if(this.childCustId == '' || this.childCustNamePrimary == '' || this.chilsCustNameSecondary == '' || this.custChildBalance == '')
        {
            window.scrollTo(0,0);
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            
            this.messageText = this.fillAllFields;
            window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
            }, 4000);
        }
        else
        {

            this.accountMaintenanceDetail.push({"childCustId":this.childCustId,
	        "childCustNamePrimary":this.childCustNamePrimary,
	        "chilsCustNameSecondary":this.chilsCustNameSecondary,
	        "custChildBalance":this.custChildBalance 
             });
             this.childCustId = '';
             this.childCustNamePrimary = '';
             this.chilsCustNameSecondary = '';
             this.custChildBalance = '';
              this.model.accountMaintenanceDetail = this.accountMaintenanceDetail;
        }
    }
      //get id already exist
	 IsIdAlreadyExist(){
        if(this.model.custNumber != '' && this.model.custNumber != undefined && this.model.custNumber != 'undefined')
        {
            this.nationalAccountMaintenanceService.getNationalAccountMaintenanceById(this.model.custNumber).then(data => {
                var datacode = data.code;
                if (data.status == "NOT_FOUND") {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("custNumberText");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.model.custNumber='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
    
    //for save and update
    saveNationalAccountMaintenance(f: NgForm)
    {
        this.btndisabled=true;
    if(this.model.allowReceiptEntry){
				var receiptEntry = "1";
			}else{
				var receiptEntry = "0";
			}
			if(this.model.baseCreditCheck){
				var creditCheck = "1";
			}else{
				var creditCheck = "0";
			}
			
			if(this.model.applyStatus){
				var appStatus = "1";
			}else{
				var appStatus = "0";
			}
			
			if(this.model.baseFinanceCharge){
				var financeCharge = "1";
			}else{
				var financeCharge = "0";
			}
		
       
        this.model.allowReceiptEntry=receiptEntry;
        this.model.applyStatus=appStatus;
        this.model.baseCreditCheck=creditCheck;
        this.model.baseFinanceCharge=financeCharge;
        //this.model.faCalendarSetupIndexId = this.model.faCalendarSetupIndexId.calendarIndex;
        if(!this.add)
        {
            this.disableId = false;
             this.nationalAccountMaintenanceService.updateNationalAccountMaintenance(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.model.custNumber=null;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                 this.model.childCustId='';
                this.model.childCustNamePrimary='';
                this.model.chilsCustNameSecondary='';
                this.model.custChildBalance='';
                this.model.accountMaintenanceDetail=[];
                this.accountMaintenanceDetail=[];
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
             this.nationalAccountMaintenanceService.createNationalAccountSetup(this.model).then(data => {
                this.showBtns=false;
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                  else{
                     this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }

                f.resetForm();
                this.model.childCustId='';
                this.model.childCustNamePrimary='';
                this.model.chilsCustNameSecondary='';
                this.model.custChildBalance='';
                this.model.accountMaintenanceDetail=[];
                this.accountMaintenanceDetail=[];

             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }

    
	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
    }