import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { CustomerMaintenance } from '../../../../financialModule/_models/master-data/accountReceivables/customerMaintenance';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { CommonFunctions } from '../../../../_sharedresource/CommonFunctions';
import {masterAccountReceivableService} from '../../../../financialModule/_services/MasterData/accountReceivable/masterAccountReceivables.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
@Component({
   selector: 'customer-maintenance',
   templateUrl: './customer-maintenance.component.html', 
    providers: [masterAccountReceivableService,CommonService],
})

export class CustomerMaintenanceComponent implements OnInit {
	//public countries;
	countries;
	states;
	cities;
	customerId;
	records;
	editCountries;
	editStates;
	editCities;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	tableViewtext=Constants.tableViewtext;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
	screenName;
	isScreenLock;
	formCountries;
	formStates;
	formCities;
	myHtml;
	currentCustomerId:string;
	    	// Serachable Select Box Step1
	ArrClassId = [];
    classId : any = [];
    ddlClassIdSetting = {};
    	// Serachable Select Box Step1 ends
	page = new Page();
	rows = new Array<CustomerMaintenance>();
    temp = new Array<CustomerMaintenance>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	btnCancelText=Constants.btnCancelText;
	showBtns:boolean=false;
	EmptyMessage = Constants.EmptyMessage;
	search=Constants.search;

	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	disableButton;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	customerClass;
	customerMaintenance = {
    'active': false,
    'hold': false,
    'customerID': '',
    'firstName': '',
    'ArabicName': '',
    'ShortName': '',
    'classId': '',
    'StatementName': '',
    'adress1': '',
    'adress2': '',
    'adress3': '',
    'phone1': '',
    'phone2': '',
    'phone3': '',
    'fax': '',
    'userDefine1': '',
    'userDefine2': ''
  };
	constructor(private router: Router,private route: ActivatedRoute, private commonService:CommonService,
		private masterAccountReceivableService: masterAccountReceivableService,private getScreenDetailService: GetScreenDetailService) {
		this.disableButton = true;
		this.add = true;
		let vm = this;
		this.countries =[];
		this.states =[];
		this.cities =[];
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.formCountries='';
		this.formStates='';
		this.formCities='';
		this.disableId = false;
		this.masterAccountReceivableService.getCountryList()
			.then(data => {
				this.countries = data.result;
				this.countries.splice(0,1);
		});
		this.masterAccountReceivableService.getCustomerClassList().subscribe(pagedData => {
            this.customerClass = pagedData.records;
	
			                // Searchable Select Box Step3
                for(var i=0;i< pagedData.records.length;i++)
                {
                    this.ArrClassId.push({ "id": pagedData.records[i].customerClassId, "itemName":pagedData.records[i].customerClassId})
                }
			    // Searchable Select Box Step3 End
        });
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_CUSTOMER_MAINTENANCE_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_CUSTOMER_MAINTENANCE_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_CUSTOMER_MAINTENANCE_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_CUSTOMER_MAINTENANCE_CLASS_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_CUSTOMER_MAINTENANCE_PRIORITY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}, 
            { 'fieldName': 'MANAGE_CUSTOMER_MAINTENANCE_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_CUSTOMER_MAINTENANCE_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
		];

		this.getScreenDetailService.ValidateScreen("S-1051").then(res=>
			{
				this.isScreenLock = res;
			});
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1051").then(data => {
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		
		vm.route.params.subscribe((params: Params) => 
		{
			vm.customerId = params['customerId'];
			if (vm.customerId != '' && vm.customerId != undefined) {
			}
			else {
				vm.screenCode = "S-1050";
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_CUSTOMER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_ACTIVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_HOLD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_SHORT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_STATEMENT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_ADDRESS1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_PHONE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_PHONE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_PHONE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_ADDRESS2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_ADDRESS3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_OPTIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_ACCOUNTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_CUSTOMER_MAINTENANCE_PRINT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				];
				
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,vm.screenCode).then(data => {
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
			}
		});
	}


	ngOnInit() {
		            // Serachable Select Box Step2
		   this.ddlClassIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
	}	
	redirect1(currentCustomerId){
		this.router.navigate(['masterdata/customeroptionmaintenance',this.currentCustomerId]);
	}
	redirect2(currentCustomerId){
		this.router.navigate(['masterdata/customeraccountmaintenance',this.currentCustomerId]);
	}
	
	selected(event){
		let countryID = this.formCountries;
		this.masterAccountReceivableService.getStateList(countryID).then(data => {
			this.states=[];
			this.cities=[];
			this.formStates='';
			this.formCities='';
			this.states = data.result;
        });
	}
	selectedState(event){
		let stateID = this.formStates;
		this.masterAccountReceivableService.getCityList(stateID).then(data => {
			this.cities=[];
			this.cities = data.result;
        });
	}
	
	onSelect({ Searchselected }) {
        this.Searchselected.splice(0, this.Searchselected.length);
        this.Searchselected.push(...Searchselected);
    }
	
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.masterAccountReceivableService.searchMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
			
        });
    }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.masterAccountReceivableService.searchMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
	
	  //get id already exist
	  IsIdAlreadyExist(){
        if(this.customerMaintenance.customerID != '' && this.customerMaintenance.customerID != undefined && this.customerMaintenance.customerID != 'undefined')
        {
			this.masterAccountReceivableService.getMasterCustomerMaintenance(this.customerMaintenance.customerID).then(data => {
                var datacode = data.code;
                if (data.status == "NOT_FOUND") {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("txtCustomerId");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.customerMaintenance.customerID='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    }
	save(custmain: NgForm){
		if(this.classId.length == 0)
		{
			return false;
		}
		this.btndisabled=true;
		if(this.add){
			if(this.customerMaintenance.hold){
			var hold = 1;
		}else{
			var hold = 0;
		}
		
		if(this.customerMaintenance.active){
			var active = 1;
		}else{
			var active = 0;
		}
		
		for(let i=0; i<this.countries.length; i++){
				if(this.formCountries == this.countries[i].countryId){
					var countryName = this.countries[i]["countryName"];
				}
			}
						  
			for(let i=0; i<this.states.length; i++){
				if(this.formStates == this.states[i].stateId){
					var stateName = this.states[i]["stateName"];
				}
			}
			
			for(let i=0; i<this.cities.length; i++){
				if(this.formCities == this.cities[i].cityId){
					var cityName = this.cities[i]["cityName"];
				}
			}
		let submittedData =
		{
			"customerId":this.customerMaintenance.customerID,
			"name" : this.customerMaintenance.firstName,
			"arabicName" : this.customerMaintenance.ArabicName,
			"shortName" : this.customerMaintenance.ShortName,
			"statementName" : this.customerMaintenance.StatementName,
			"phone1" :this.customerMaintenance.phone1,
			"phone2" : this.customerMaintenance.phone2,
			"phone3" : this.customerMaintenance.phone3,
			"address1" : this.customerMaintenance.adress1,
			"address2" : this.customerMaintenance.adress2,
			"address3" : this.customerMaintenance.adress3,
			"cityId" : this.formCities,
			"stateId" : this.formStates,
			"countryId" : this.formCountries,
			"fax" : this.customerMaintenance.fax,
			"countryName" : countryName,
			"stateName" : stateName,
			"cityName" : cityName,
			"userDefine1" : this.customerMaintenance.userDefine1,
			"userDefine2" : this.customerMaintenance.userDefine2,
			"customerHold": hold,
			"activeCustomer": active,
			"classId": this.classId[0].id
		}
		
		this.masterAccountReceivableService.createMasterCustomerMaintenance(submittedData).then(data => {
			this.showBtns=false;
			this.classId=[];
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 200) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.setPage({ offset: 0 });
				this.messageText = data.btiMessage.message;
			    custmain.resetForm();
				this.setPage({ offset: 0 });
				this.formCountries='';
				this.formCities ='';
				this.formStates='';
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			  this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		}
		else{
			if(this.customerMaintenance.hold){
			var hold = 1;
		}else{
			var hold = 0;
		}
		
		if(this.customerMaintenance.active){
			var active = 1;
		}else{
			var active = 0;
		}
		
		for(let i=0; i<this.countries.length; i++){
				if(this.formCountries == this.countries[i].countryId){
					var countryName = this.countries[i]["countryName"];
				}
			}
						  
			for(let i=0; i<this.states.length; i++){
				if(this.formStates == this.states[i].stateId){
					var stateName = this.states[i]["stateName"];
				}
			}
			
			for(let i=0; i<this.cities.length; i++){
				if(this.formCities == this.cities[i].cityId){
					var cityName = this.cities[i]["cityName"];
				}
			}
		
		let submittedData =
		{
			"customerId":this.customerMaintenance.customerID,
			"name" : this.customerMaintenance.firstName,
			"arabicName" : this.customerMaintenance.ArabicName,
			"shortName" : this.customerMaintenance.ShortName,
			"statementName" : this.customerMaintenance.StatementName,
			"phone1" :this.customerMaintenance.phone1,
			"phone2" : this.customerMaintenance.phone2,
			"phone3" : this.customerMaintenance.phone3,
			"address1" : this.customerMaintenance.adress1,
			"address2" : this.customerMaintenance.adress2,
			"address3" : this.customerMaintenance.adress3,
			"cityId" : this.formCities,
			"stateId" : this.formStates,
			"countryId" : this.formCountries,
			"fax" : this.customerMaintenance.fax,
			"countryName" : countryName,
			"stateName" : stateName,
			"cityName" : cityName,
			"userDefine1" : this.customerMaintenance.userDefine1,
			"userDefine2" : this.customerMaintenance.userDefine2,
			"customerHold": hold,
			"activeCustomer": active,
			"classId": this.classId[0].id
		}
		this.masterAccountReceivableService.editMasterCustomerMaintenance(submittedData).then(data => {
			this.classId=[];
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 200) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				this.showBtns=false;
				this.setPage({ offset: 0 });
				custmain.resetForm();
				this.disableButton = true;
				this.disableId = false;
				this.add = true;
				this.formCountries='';
				this.formCities ='';
				this.formStates='';
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			  this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;  
				 this.showMsg = false;
			   }, 4000);
		}
        });
		}
	}
	
	delete(row){
		this.masterAccountReceivableService.deleteCustomerMaintenance(row.customerId).then(data => {
			var datacode = data.code;
			if (datacode == 200) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
			    this.setPage({ offset: 0 });
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			  this.setPage({ offset: 0 });
			   window.setTimeout(() => {
				 this.hasMsg = false;  
				 this.showMsg = false;
			   }, 4000);
		}
		});
	}
	// Serachable Select Box Step4
    getClassByID(item:any){
	 this.classId=[];
	 this.classId.push({'id':item.id,'itemName':item.itemName});
	 this.commonService.closeMultiselectWithId("classId")
    }
    OnClassIdDeSelect(item:any){
		this.classId=[];
		this.commonService.closeMultiselectWithId("classId")
    }
	edit(row: any) {
		this.add = false;
		this.disableId = true;
		this.classId=[];
		this.disableButton = false;
		this.showBtns=true;
		// this.setPage({ offset: 0 });
		this.masterAccountReceivableService.getMasterCustomerMaintenance(row.customerId).then(data => {
			// this.records = data.result;
		
			this.records = data.result;
			this.currentCustomerId = row.customerId;
			var selectedClass=this.ArrClassId.find(x => x.id ==  data.result.classId);
			this.classId.push({'id':selectedClass.id,'itemName':selectedClass.itemName});
			let countryID = this.records.countryId;
			this.masterAccountReceivableService.getStateList(countryID).then(data => {
				this.states = data.result;
			});
			
			let stateID = this.records.stateId;
			this.masterAccountReceivableService.getCityList(stateID).then(data => {
				this.cities = data.result;
			});
			if(this.records.activeCustomer==1){
				var act = true;
			}else{
				var act = false;
			}
			if(this.records.customerHold==1){
				var hol = true;
			}else{
				var hol = false;
			}
			this.customerMaintenance = {
				'active': act,
				'hold': hol,
				'customerID': this.records.customerId,
				'firstName': this.records.name,
				'ArabicName': this.records.arabicName,
				'ShortName': this.records.shortName,
				'classId': this.records.classId,
				'StatementName': this.records.statementName,
				'adress1': this.records.address1,
				'adress2': this.records.address2,
				'adress3': this.records.address3,
				'phone1': this.records.phone1,
				'phone2': this.records.phone2,
				'phone3': this.records.phone3,
				'fax': this.records.fax,
				'userDefine1': this.records.userDefine1,
				'userDefine2': this.records.userDefine2
			  };
			
			this.masterAccountReceivableService.getCountryList()
			.then(data => {
				this.editCountries = data.result;
				this.editCountries.splice(0,1);
				for(let i=0; i<this.editCountries.length; i++){
					if(this.records.countryId == this.editCountries[i].countryId){
						//this.formCountries =this.editCountries[i];
						this.formCountries = this.editCountries[i]["countryId"];
					}
			}
			});
			  
			this.masterAccountReceivableService.getStateList(this.records.countryId).then(data => {
				this.editStates = data.result;
				//this.formStates = {"stateId": '',"stateName": ""}
				for(let i=0; i<this.editStates.length; i++){
					if(this.records.stateId == this.editStates[i].stateId){
						this.formStates = this.editStates[i]["stateId"];
					}
				}
			});
			this.masterAccountReceivableService.getCityList(this.records.stateId).then(data => {
				this.editCities = data.result;
				//this.formCities = {"cityId": '',"cityName": ""}
				for(let i=0; i<this.editCities.length; i++){
					if(this.records.cityId == this.editCities[i].cityId){
						this.formCities = this.editCities[i]["cityId"];
					}
				}
			})
			
		});
    }
	clearForm(custmain:NgForm){
		custmain.resetForm();
		this.classId=[];
		var selectedValue = <HTMLInputElement>document.getElementById("txtCustomerId")
        selectedValue.value=this.records.customerId;
		this.customerMaintenance.customerID=this.records.customerId;
		if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
		 }
	}

	print(){

		if (!this.customerMaintenance.customerID){
			alert("No record selected for print");
			return;
		}

		var id = this.customerMaintenance.customerID;
        var parentURL = '&ParentFolderUri=' + '%2Freports';;
        var reportUnit = '&reportUnit=%2Freports%2FCurrentCustomerDetails_EQ';
        var query = '&PROMPT1=' + id;

		CommonFunctions.print(parentURL, reportUnit, query);
	}

	Cancel(custmain)
    {
    this.add=true;
    this.clearForm(custmain);
    this.customerMaintenance.customerID= '';
	this.disableButton = true;
	this.disableId=false;
	}
	
	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}

	nameInputFormatter = (result: { countryName: string }) => result.countryName
	formatter = (result: { countryName: string }) => result.countryName
	Search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.countries.filter(v => v.countryName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
		
	nameInputFormatterState = (resultState: { stateName: string }) => resultState.stateName
	formatterState = (resultState: { stateName: string }) => resultState.stateName
	searchState = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.states.filter(v => v.stateName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
		
	nameInputFormatterCity = (resultCity: { cityName: string }) => resultCity.cityName
	formatterCity = (resultCity: { cityName: string }) => resultCity.cityName
	searchCity = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.cities.filter(v => v.cityName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
}