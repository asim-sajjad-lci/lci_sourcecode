import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import {fixedAssetCompanySetup} from '../../../../../financialModule/_models/fixedAssets/fixedAssetCompanySetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import {FixedAllocationAccountService} from '../../../../../financialModule/_services/MasterData/general-ledger/fixed-allocation-account-setup.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
	selector: 'fixed-allocation-account-setup',
	templateUrl: './fixed-allocation-account-setup.component.html',
	providers: [FixedAllocationAccountService],
})

export class FixedAllocationAccountSetupComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
	availableFormValues: [object];
	gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
	screenName;
	isScreenLock;
	page = new Page();
	rows = new Array<fixedAssetCompanySetup>();
	temp = new Array<fixedAssetCompanySetup>();
	Searchselected = [];
	searchKeyword = '';
	ddPageSize = 5;
	atATimeText=Constants.atATimeText;
	selectedText=Constants.selectedText;
	totalText=Constants.totalText;
	deleteConfirmationText = Constants.deleteConfirmationText;
	search=Constants.search;
	// Serachable Select Box Step1
	ArrAccountTableIndex = [];
	accountTableIndex : any = [];
	ddlAccountTableIndexSetting = {};
	// Serachable Select Box Step1 End
	
	@ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
	message = { 'type': '', 'text': '' };
	hasMessage;
	hasMsg = false;
	showMsg = false;
	isSuccessMsg = false;
	isfailureMsg = false;
	add;
	tempVar;
	fetchedData;
	getClass;
	getAccountList;
	mode;
	accountNumberList;
	description;
	arabicDescription;
	aliasName;
	aliasNameArabic;
	accountTableRowIndex;
	mainAccountID;
	accountNumberListObj;
	total;
	btndisabled:boolean=false;
	
	constructor(private router: Router,private route: ActivatedRoute,private fixedAllocationAccountService: FixedAllocationAccountService,private getScreenDetailService: GetScreenDetailService,public toastr: ToastsManager,vcr: ViewContainerRef) {
		this.toastr.setRootViewContainerRef(vcr);
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
		this.page.size = 5;	
		this.tempVar ='';
		this.fetchedData =[];
		this.getClass =[];
		this.getAccountList =[];
		this.accountNumberList =[];
		this.accountNumberListObj =[];
		this.accountTableRowIndex ='';
		this.mode ="add";	
		this.total=0;
		
		this.accountNumberListObj.push({
			"accountTableRowIndex":"",	
			"percentage":""
		});
		this.fixedAllocationAccountService.getAccountList().then(pagedData => {
			this.getAccountList = pagedData.result.records;
			
			// Searchable Select Box Step3
			for(var i=0;i< pagedData.result.records.length;i++)
			{
				this.ArrAccountTableIndex.push({ "id": pagedData.result.records[i].actIndx , "itemName": pagedData.result.records[i].mainAccountNumber})
			}
			// Searchable Select Box Step3 End
		});
		
		/* for add screen */
		vm.defaultFormValues = [
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_ALIAS_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_ALIAS_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_DIS_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_PERCENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_TOTAL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FIXED_ALLO_ACC_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
		];
		
		this.getScreenDetailService.ValidateScreen("S-1217").then(res=>
			{
				this.isScreenLock = res;
			});
			vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1217").then(data => {
				
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
			/* for add screen ends */		
			
		}
		save(fixedAllocationSetup:NgForm){
			this.btndisabled=true;
			if(this.accountTableIndex.length > 0){
				if(this.total>100){
					this.toastr.error("Percentage should not be greater than 100");
				}else{
					
					let submittedData = {
						'mainAccountID': this.accountTableIndex[0].id
						// 'accountTableRowIndex': this.accountTableIndex[0].id
					}
					submittedData["accountNumberList"] = [];
					for(let i=0;i<this.accountNumberListObj.length;i++){
						if(this.accountNumberListObj[i]["accountTableRowIndex"])
						{
							submittedData["accountNumberList"].push({
								"accountTableRowIndex":this.accountNumberListObj[i]["accountTableRowIndex"],
								"percentage":this.accountNumberListObj[i]["percentage"]
							});			
						}
					}
					
					this.fixedAllocationAccountService.save(submittedData).then(data => {
						this.btndisabled=false;
						var datacode = data.code;
						if (datacode == 201) {
							window.setTimeout(() => {
								this.hasMsg = true;
								this.isSuccessMsg = true;
								this.isfailureMsg = false;
								this.showMsg = true;
								window.setTimeout(() => {
									this.showMsg = false;
									this.hasMsg = false;
								}, 4000);
								this.messageText = data.btiMessage.message;
							}, 100);
						}else{
							this.isSuccessMsg = false;
							this.isfailureMsg = true;
							this.showMsg = true;
							this.hasMsg = true;
							this.btndisabled=false;
							this.messageText = data.btiMessage.message;
							window.setTimeout(() => {
								this.hasMsg = false;  
							}, 4000);
						}
					});
				}
			} 
			
			this.btndisabled=false;
		}
		getDesc(){
			
			this.fixedAllocationAccountService.getAccountDescription(this.accountTableIndex[0].id).then(pagedData => {
				//this.description = pagedData.result.mainAccountDescription;
				//this.arabicDescription = pagedData.result.classDescriptionArabic;
			});
			this.fixedAllocationAccountService.getAccountDetails(this.accountTableIndex[0].id).subscribe(pagedData => {
				
				if(pagedData.result){
					this.accountNumberListObj=[];
					for(let i=0;i<pagedData.result.accountNumberList.length;i++){
						this.accountNumberListObj.push({
							"accountNumber":pagedData.result.accountNumberList[i]["accountNumber"],
							"accountTableRowIndex": pagedData.result.accountNumberList[i]["accountTableRowIndex"],	
							"percentage": pagedData.result.accountNumberList[i]["percentage"]
						});
					}
					a =0;
					for(let i=0; i<this.accountNumberListObj.length;i++){
						var a = parseFloat(a) + parseFloat(this.accountNumberListObj[i]["percentage"]);
					}
					this.total = parseFloat(a);
				}else{
					this.accountNumberListObj=[];
					this.accountNumberListObj.push({
						"accountTableRowIndex":"",	
						"percentage":""
					});
					this.total=0;
				}
			});
		}
		addMore(i){
			if(i<this.accountNumberListObj.length-1){
				this.toastr.error("Cannot add here");
			}else{
				if(this.accountNumberListObj[i]["accountTableRowIndex"] && this.accountNumberListObj[i]["percentage"] ){
					if(this.total>100){
						this.toastr.error("Percentage should not be greater than 100");
					}else{
						let a ={
							"accountTableRowIndex":"",	
							"percentage":""
						};
						this.accountNumberListObj.push(a);
					}
				}else{
					this.toastr.error("Please select account and percentage!!");
				}
			}
		}
		deleteMore(i){
			this.accountNumberListObj.splice(i,1);
			a =0;
			for(let i=0; i<this.accountNumberListObj.length;i++){
				if(this.accountNumberListObj[i]["percentage"]==""){
					var per =0;
				}else{
					var per =parseFloat(this.accountNumberListObj[i]["percentage"]);
				}
				var a = parseFloat(a) + per;
			}
			this.total = parseFloat(a);
		}
		setTotal(i){
			a =0;
			for(let i=0; i<this.accountNumberListObj.length;i++){
				if(this.accountNumberListObj[i]["percentage"]==""){
					var per =0;
				}else{
					var per =parseFloat(this.accountNumberListObj[i]["percentage"]);
				}
				var a = parseFloat(a) + per;
			}
			this.total = parseFloat(a);
		}
		clearForm(){
			this.total='';
			this.accountNumberListObj=[];
			this.accountTableIndex=[];
			this.description ='';
			this.arabicDescription ='';
			this.aliasName ='';
			this.aliasNameArabic ='';
			// this.accountNumberListObj=[];
			// this.accountNumberListObj.push({
			// 	"accountTableRowIndex":"",	
			// 	 "percentage":""
			// });
			// this.total=0;
		}
		ngOnInit() {
			// Serachable Select Box Step2
			this.ddlAccountTableIndexSetting = { 
				singleSelection: true, 
				text:this.select,
				enableSearchFilter: true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			}; 
			// Serachable Select Box Step2
		}	
		
		// Serachable Select Box Step4
		getAccountTableIndexByID(item:any){
			this.accountTableIndex=[];
			this.accountTableIndex.push({'id':item.id,'itemName':item.itemName});
			this.fixedAllocationAccountService.getAccountDescription(item.id).then(pagedData => {
				this.description = pagedData.result.mainAccountDescription;
				this.arabicDescription = pagedData.result.mainAccountDescriptionArabic;
				this.aliasName = pagedData.result.aliasAccount;
				this.aliasNameArabic = pagedData.result.aliasAccountArabic;
			});
			this.fixedAllocationAccountService.getAccountDetails(this.accountTableIndex[0].id).subscribe(pagedData => {
				
				if(pagedData.result){
					this.accountNumberListObj=[];
					for(let i=0;i<pagedData.result.accountNumberList.length;i++){
						this.accountNumberListObj.push({
							"accountNumber":pagedData.result.accountNumberList[i]["accountNumber"],
							"accountTableRowIndex": pagedData.result.accountNumberList[i]["accountTableRowIndex"],	
							"percentage": pagedData.result.accountNumberList[i]["percentage"]
						});
					}
					a =0;
					for(let i=0; i<this.accountNumberListObj.length;i++){
						var a = parseFloat(a) + parseFloat(this.accountNumberListObj[i]["percentage"]);
					}
					this.total = parseFloat(a);
				}else{
					this.accountNumberListObj=[];
					this.accountNumberListObj.push({
						"accountNumber":"",
						"accountTableRowIndex":"",	
						"percentage":""
					});
					this.total=0;
				}
			});
		}
		OnAccountTableIndexDeSelect(item:any){
			this.description ='';
			this.arabicDescription ='';
			this.aliasName ='';
			this.aliasNameArabic ='';
		}
		// Serachable Select Box Step4 End 
		onlyDecimalNumberKey(event) {
			return this.getScreenDetailService.onlyDecimalNumberKey(event);
		}
		
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
		
		
	}