import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { FinancialDimentionsValue } from '../../../../../financialModule/_models/master-data/general-ledger/financial-dimensions-value-setup';
import { FinancialDimentionsValueService } from '../../../../../financialModule/_services/MasterData/general-ledger/financial-dimensions-value-setup.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';

@Component({
    templateUrl: './financial-dimensions-value-setup.component.html',
    providers:[FinancialDimentionsValueService,CommonService]
})

//export to make it available for other classes
export class FinancialDimentionsValueComponent {
    page = new Page();
    rows = new Array<FinancialDimentionsValue>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    ArrFinancialDimenionsLst=[];
    	// Serachable Select Box Step1
	ArrClassId = [];
    dimInxd : any = [];
    ddlClassIdSetting = {};
    	// Serachable Select Box Step1 ends
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    isModify:boolean=false;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    btnCancelText=Constants.btnCancelText;
    search=Constants.search;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    
    btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private FinancialDimentionsValueService:FinancialDimentionsValueService,
        private commonService:CommonService
        
    ){
         this.page.pageNumber = 0;
        this.page.size = 5;
    }

    ngOnInit() {
        
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                   this.FinancialDimentionsValueService.getFinancialDimentionsList().then(data => {
                    this.ArrFinancialDimenionsLst = data.result.records;
                    // this.ArrFinancialDimenionsLst.splice(0, 0, { "dimInxd": "", "dimensionName": this.select });
                    //  this.model.dimInxd='';
                // Searchable Select Box Step3
                for(var i=0;i< data.result.records.length;i++)
                {
                    this.ArrClassId.push({ "id": data.result.records[i].dimInxd, "itemName":data.result.records[i].dimensionName})
                }
			    // Searchable Select Box Step3 End
                 });
                this.setPage({ offset: 0 });

            // Serachable Select Box Step2
		   this.ddlClassIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1081";
         this.defaultAddFormValues = [
               { 'fieldName': 'ADD_FIN_DIM_VALUE_DIM_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_FIN_DIM_VALUE_DIM_VALUE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_FIN_DIM_VALUE_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_FIN_DIM_VALUE_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_FIN_DIM_VALUE_SAVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_FIN_DIM_VALUE_CLEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
         this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1082";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_FIN_DIM_VALUE_DIM_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_FIN_DIM_VALUE_DIM_VALUE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_FIN_DIM_VALUE_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_FIN_DIM_VALUE_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_FIN_DIM_VALUE_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_FIN_DIM_VALUE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
       
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.FinancialDimentionsValueService.searchFinancialDimentionsValue(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.FinancialDimentionsValueService.searchFinancialDimentionsValue(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
    
    resetMe(f){
     f.resetForm();
    this.dimInxd=[];
    }

       Cancel(f)
    {
    this.isModify=false;
    this.resetMe(f);
    }

    

    getFinancialDimentionsValueById(row: any)
    {
        this.showBtns=true;
        this.isModify=true;
        this.dimInxd=[];
        this.FinancialDimentionsValueService.getFinancialDimentionsValueById(row.dimInxValue).then(data => {
        this.model = data.result; 
        this.dimInxd.push({'id':data.result.dimInxd,'itemName':data.result.dimensionName});
        });
    }
     changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    // Serachable Select Box Step4
    getClassByID(item:any){
        this.model.dimInxd=item.id;
        this.commonService.closeMultiselectWithId("dimInxd")
    }
    OnClassIdDeSelect(item:any){
        this.model.dimInxd='';
        this.commonService.closeMultiselectWithId("dimInxd")
    }

    saveFinancialDimentionsValue(f: NgForm)
    {
        this.btndisabled=true;
        if(this.isModify)
        {
            this.FinancialDimentionsValueService.updateFinancialDimentionsValue(this.model).then(data => {
                this.dimInxd=[];
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                     this.isModify=false;
                      this.model.exchangeId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
            this.FinancialDimentionsValueService.createFinancialDimentionsValue(this.model).then(data => {
                this.showBtns=false;
               window.scrollTo(0,0);
                this.dimInxd=[];
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }

    	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
		
}