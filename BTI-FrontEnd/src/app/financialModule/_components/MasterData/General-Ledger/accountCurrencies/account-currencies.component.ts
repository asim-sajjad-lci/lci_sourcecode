import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { CurrencySetup } from '../../../../../financialModule/_models/general-ledger-setup/currencysetup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import {AccountCurrecniesService} from '../../../../../financialModule/_services/MasterData/general-ledger/accountCurrencies.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'account-currencies',
   templateUrl: './account-currencies.component.html', 
    providers: [AccountCurrecniesService,CommonService],
})

export class AccountCurrenciesComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
	screenName;
	isScreenLock;
    mainAccount;
	myHtml;
	currencyId;
	
	page = new Page();
	rows = new Array<CurrencySetup>();
    temp = new Array<CurrencySetup>();
	selected = [];
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
	deleteConfirmationText = Constants.deleteConfirmationText;
	search=Constants.search;
    arrCurrencylist=[];
    
	// Serachable Select Box Step1
	ArrAccountIndex = [];
    accountindex : any = [];
    ddlAccountIndexSetting = {};
	// Serachable Select Box Step1 End

	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	accountIndex;
	description;
	descriptionArabic;
	mode;
	fetchedData;
	isAllChecked:boolean=false;
	constructor(private router: Router,private route: ActivatedRoute,private commonService:CommonService,private accountCurrecniesService: AccountCurrecniesService,private getScreenDetailService: GetScreenDetailService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.disableId = false;	
		this.accountIndex ='';
		this.currencyId =[];
		this.mode ="add";
		this.fetchedData =[];
			
		
		/* to fetch the main accounts*/
		this.accountCurrecniesService.getMainAccounts().subscribe(pagedData => {
            this.mainAccount = pagedData.records;
	
				// Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrAccountIndex.push({ "id": pagedData.records[i].actIndx, "itemName": pagedData.records[i].mainAccountNumber})
              }
			  
			    // Searchable Select Box Step3 End

			/* for edit */
		this.accountCurrecniesService.getData().then(data => {
			
			this.fetchedData = data.result;
			if(data.btiMessage.messageShort != 'RECORD_NOT_FOUND')
			{
				var	selectedAccount = this.ArrAccountIndex.find(x => x.id ==  this.fetchedData.accountIndex);
				this.accountindex.push({'id':selectedAccount.id,'itemName':selectedAccount.itemName});
			 if(this.fetchedData.accountIndex){
				 this.accountIndex = this.fetchedData.accountIndex;
					 
				 for(let i=0 ; i<this.mainAccount.length; i++){
					 if(this.accountIndex == this.mainAccount[i]["actIndx"]){
						 var accountIndex = this.mainAccount[i]["actIndx"]
					 }
				 }
				 this.accountCurrecniesService.getAccountById(accountIndex).then(pagedData => {
					
					this.description = pagedData.result.accountDescription;
					this.descriptionArabic = pagedData.result.accountDescriptionArabic;
				 });		
				 
				 this.currencyId=[];
				 for(var i=0; i< this.arrCurrencylist.length;i++)
				 {
					 for(var j=0; j< data.result.currencyId.length;j++)
					 {
						 if(data.result.currencyId[j] == this.arrCurrencylist[i].currencyId)
						 {
							 this.arrCurrencylist[i].isCheked=true;
						 }
					 }
				 }	
			 }
			}
			
        });
		/* for edit ends */
			
        });
				
		/* for grid binding */
		
	
		 this.gridDefaultFormValues = [
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_CURRENCY_SYMBOL', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CURRENCY_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
		 ];
		 

		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1047").then(data => {
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_ACC_CUR_MAIN_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_SELECT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_CURRENCY_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_CURRENCY_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_ACC_CUR_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
				];

				this.getScreenDetailService.ValidateScreen("S-1210").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1210").then(data => {
	
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */
	}
	ngOnInit() {
		this.getCurrencylist();
			// Serachable Select Box Step2
		   this.ddlAccountIndexSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
	}	
	getDesc(){
		for(let i=0 ; i<this.mainAccount.length; i++){
			if(this.accountIndex == this.mainAccount[i]["actIndx"]){
				var accountIndex = this.mainAccount[i]["actIndx"]
			}
		}
		this.accountCurrecniesService.getAccountById(accountIndex).then(pagedData => {
			
			this.description = pagedData.result.accountDescription;
			this.descriptionArabic = pagedData.result.accountDescriptionArabic;
        });
	}
	getCurrencylist()
	{
		this.accountCurrecniesService.getCurrencylist().subscribe(data =>{
			//this.arrCurrencylist=data.records;
			this.arrCurrencylist=[];
			for(var i=0; i< data.records.length;i++)
			{
				this.arrCurrencylist.push({'currencyId':data.records[i].currencyId,'currencyDescription':data.records[i].currencyDescription,'currencyDescriptionArabic':data.records[i].currencyDescriptionArabic,'isCheked':false});
			}
			
		
		})
     
	}

    Clear(accountCurrencies){
		this.accountindex=[];
		accountCurrencies.resetForm();
		this.checkUncheckAll()
		
	}
	

	checkUncheckAll()
	{
		if(this.isAllChecked)
		{
			for(var i=0; i< this.arrCurrencylist.length;i++)
			{
				this.arrCurrencylist[i].isCheked=true;
			}
		}
		else{
			for(var i=0; i< this.arrCurrencylist.length;i++)
			{
				this.arrCurrencylist[i].isCheked=false;
			}
		}
			
	}

	// Serachable Select Box Step4
	getAccountIndexByID(item:any){
	   this.accountindex=[];
       this.accountindex.push({'id':item.id,'itemName':item.itemName});
	   this.accountCurrecniesService.getAccountById(item.id).then(pagedData => {
			     this.isSuccessMsg = false;
				  this.isfailureMsg = false;
				  this.showMsg = false;
				  this.hasMsg = false;
				  for(var i=0; i< this.arrCurrencylist.length;i++)
				  {
					this.arrCurrencylist[i].isCheked=false;
				  }
				  this.description = pagedData.result.accountDescription;
				  this.descriptionArabic = pagedData.result.accountDescriptionArabic;
				if(pagedData.btiMessage.messageShort != 'RECORD_NOT_FOUND')
				{
					this.currencyId=[];
					for(var i=0; i< this.arrCurrencylist.length;i++)
					{
						for(var j=0; j< pagedData.result.currencyId.length;j++)
						{
							if(pagedData.result.currencyId[j] == this.arrCurrencylist[i].currencyId)
							{
								this.arrCurrencylist[i].isCheked=true;
							}
						}
					}
				}
				else{
					this.currencyId=[];
				}
			
		});
		this.commonService.closeMultiselectWithId("accountindex")
    }

	OnAccountIndexIdDeSelect(item:any){
       
	   this.description ='';
	   this.descriptionArabic ='';
	   this.commonService.closeMultiselectWithId("accountindex")
	   
    }
	// Serachable Select Box Step4 End 
	
	onSelect(event){
		if(event.selected.length>0){
			this.currencyId=[];
			for(let i=0; i<event.selected.length;i++){
				this.currencyId.push(event.selected[i]["currencyId"]);
			}
		}else if(event.selected.length==0){
			this.currencyId=[];
		}
	}
	save(accountCurrencies:NgForm){
		
	    this.btndisabled=true;
		this.currencyId=[];
		if(this.accountindex.length > 0 && this.accountindex[0].id != '0')
		{
			for(var i=0; i< this.arrCurrencylist.length;i++)
			{
				if(this.arrCurrencylist[i].isCheked)
				{
					this.currencyId.push(this.arrCurrencylist[i].currencyId);
				}
				
			}
			
			let submittedData = {
				"accountIndex":this.accountindex[0].id,
				"currencyId":this.currencyId
			}
			this.accountCurrecniesService.save(submittedData).then(data => {
					var datacode = data.code;
					this.btndisabled=false;
					if (datacode == 201) {
					 window.setTimeout(() => {
						this.hasMsg = true;
						this.isSuccessMsg = true;
						this.isfailureMsg = false;
						this.showMsg = true;
						this.btndisabled=false;	
						window.setTimeout(() => {
							this.showMsg = false;
							this.hasMsg = false;
						 }, 4000);
						this.messageText = data.btiMessage.message;
					}, 100);
						this.Clear(accountCurrencies);
					}else{
					  this.isSuccessMsg = false;
					  this.isfailureMsg = true;
					  this.showMsg = true;
					  this.hasMsg = true;
					  this.btndisabled=false;
					  this.messageText = data.btiMessage.message;
					   window.setTimeout(() => {
						 this.hasMsg = false;  
					   }, 4000);
					}
				});
				
		}
		this.btndisabled=false;	
	}

	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
	
}