import { Component, ViewChild,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { CheckbookMaintenance } from '../../../../../financialModule/_models/master-data/general-ledger/checkbook-maintenance';
import { CheckbookMaintenanceService } from '../../../../../financialModule/_services/MasterData/general-ledger/checkbook-maintenance.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { AccountStructureService } from '../../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 

@Component({
    templateUrl: './checkbook-maintenance.component.html',
    providers:[CheckbookMaintenanceService,AccountStructureService,CommonService]
})

//export to make it available for other classes
export class CheckbookMaintenanceComponent {
    page = new Page();
    rows = new Array<CheckbookMaintenance>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    ArrAccountNumberList=[];
    bankIdOptions;
    currencyIdOptions;
    AccountIdOptions;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean=false;
    isModalOpen:boolean=false;
    // inactive:false;
    myHtml;
    add;
    // Serachable Select Box Step1
	ArrCurrencyId = [];
    currencyId : any = [];
    ddlCurrencyIdSetting = {};

    ArrBankId = [];
    bankId : any = [];
    ddlBankIdSetting = {};
    ArrglAccountId = [];
    glAccountId : any = [];
    ddlglAccountIdSetting = {};
	// Serachable Select Box Step1 End
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    arrSegment=[];
    accountDescription=[];
	accountNumberIndex=[];
	accountNumberTitleLabel:string;
	createAccountNumberLabel:string;
	btnCancelLabel:string;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    unFrmtLastReconciledBalance;
    unFrmtCurrentCheckbookBalance
    unFrmtCashAccountBalance;
    unFrmtExceedMaxCheckAmount;
    isScreenLock;
    btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private checkbookMaintenanceService:CheckbookMaintenanceService,
        private commonService: CommonService,
        private accountStructureService:AccountStructureService,public toastr: ToastsManager,	vcr: ViewContainerRef
        
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.toastr.setRootViewContainerRef(vcr);
        this.accountNumberTitleLabel=Constants.accountNumberTitle;
	    this.createAccountNumberLabel=Constants.createAccountNumber;
	    this.btnCancelLabel=Constants.btnCancelText;
    }

    ngOnInit() {
                this.getSegmentCount();
                this.getAccountNumber();
                this.getCurrencyIdList();
                this.currencyIdOptions="";
                this.getbankIdList();
                this.bankIdOptions="";
                this.getAccountIdList();
                this.AccountIdOptions="";
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });
           // Serachable Select Box Step2
		   this.ddlCurrencyIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 

         	this.ddlBankIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         };

         	this.ddlglAccountIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1129";
         this.defaultAddFormValues = [
               { 'fieldName': 'ADD_CHKBOOK_MAIN_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_ACCOUNT_NO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_NEXT_CHECK_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_NEXT_DEPOSIT_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_LAST_RECONCILED_BALANCE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_LAST_RECONCILED_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_BANK_ACC_NO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_BANK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_CURRENT_CHECKBOOK_BALANCE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_CASH_ACC_BALANCE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_CASH_PAYABLE_OPTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_MAX_CHECK_AMOUNT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_PASSWORD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_DUPLICATE_CHECK_NO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_OVERRIDE_CHECK_NO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_SAVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_CLEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_CHKBOOK_MAIN_ACTIVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'VIEW_CHKBOOK_MAIN_ACTIVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
         this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1130";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_BANK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CHKBOOK_MAIN_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.checkbookMaintenanceService.searchCheckbookMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

    resetMe(f)
    {
        this.model.inactive='';
        this.model.checkbookDescription= '';
        this.model.checkbookDescriptionArabic= '';
        this.currencyId=[];
        this.glAccountId=[];
        this.model.nextCheckNumber='';
        this.model.lastReconciledBalance='';
        this.model.nextDepositNumber='';
        this.model.lastReconciledDate='';
        this.model.officialBankAccountNumber='';
        this.bankId=[];
        this.model.userDefine1='';
        this.model.userDefine2='';
        this.model.currentCheckbookBalance='';
        this.model.cashAccountBalance='';
        this.model.exceedMaxCheckAmount='';
        this.model.passwordOfMaxCheckAmount='';
        this.model.duplicateCheckNumber='';
        this.model.overrideCheckNumber='';

    }

    Cancel(f)
    {
    this.isModify=false;
    this.model.checkBookId= '';
    this.resetMe(f);
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.checkbookMaintenanceService.searchCheckbookMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    getAccountNumber() {
        this.ArrAccountNumberList = [];
       this.checkbookMaintenanceService.getGlAccountNumberList().then(data => {
                        this.ArrAccountNumberList = data.result;
                        // this.ArrAccountNumberList.splice(0, 0, { "accountTableRowIndex": "", "accountNumber": this.select });
                        // this.model.glAccountId='';

                    // Searchable Select Box Step3
                    this.ArrglAccountId = [];
                    for(var i=0;i< data.result.length;i++)
                    {
                        this.ArrglAccountId.push({ "id": data.result[i].accountTableRowIndex, "itemName": data.result[i].accountNumber})
                    }
                        // Searchable Select Box Step3 End
                  });
    }

    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    DateBalanceOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        disableUntil: {year:0, month: 0, day: 0 }
   };
   

    onlastDateChanged(event: IMyDateModel,ActionType): void {
         this.model.lastReconciledDate=event.date;
    }

    getbankIdList()
   {
               
              this.checkbookMaintenanceService.getBankIDSetupList().then(data => {
              this.bankIdOptions = data.result.records;
            //   this.bankIdOptions.splice(0,0,{ "bankId": "", "bankDescription": this.select });
            //   this.model.bankId = "";
                              // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrBankId.push({ "id": data.result.records[i].bankId, "itemName": data.result.records[i].bankDescription})
              }
			    // Searchable Select Box Step3 End
       });
   }
   getCurrencyIdList()
   {
               
              this.checkbookMaintenanceService.getCurrencyIDSetupList().then(data => {
              this.currencyIdOptions = data.result.records;
            //   this.currencyIdOptions.splice(0,0,{ "currencyId": "", "currencyDescription": this.select });
            //   this.model.currencyId = "";
                // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrCurrencyId.push({ "id": data.result.records[i].currencyId, "itemName": data.result.records[i].currencyDescription})
              }
			    // Searchable Select Box Step3 End
       });
   }

    // Serachable Select Box Step4
 getCurrencyByID(item:any){
     this.model.currencyId=item.id;
     this.formatCashAccountBalanceAmount();
     this.formatCheckBookBalanceAmount();
     this.formatExceedMaxCheckAmount();
     this.formatLastReconciledBalanceAmount();
     this.commonService.closeMultiselectWithId("currencyId")
 }
 OnCurrencyIdDeSelect(item:any){
     this.model.currencyId='';
     this.formatCashAccountBalanceAmount();
     this.formatCheckBookBalanceAmount();
     this.formatExceedMaxCheckAmount();
     this.formatLastReconciledBalanceAmount();
     this.commonService.closeMultiselectWithId("currencyId")
 }
  // Serachable Select Box Step4 ends

  // Serachable Select Box Step4
 getBankByID(item:any){
     this.model.bankId=item.id;
     this.commonService.closeMultiselectWithId("bankId")
 }
 OnBankIdDeSelect(item:any){
     this.model.bankId='';
     this.commonService.closeMultiselectWithId("bankId")
 }
  // Serachable Select Box Step4 ends

// Serachable Select Box Step4
 getglAccountByID(item:any){
     this.model.glAccountId=item.id;
     this.commonService.closeMultiselectWithId("glAccountId")
 }
 OnglAccountIdDeSelect(item:any){
     this.model.glAccountId='';
     this.commonService.closeMultiselectWithId("glAccountId")
 }
  // Serachable Select Box Step4 ends

   getAccountIdList()
   {
               
              this.checkbookMaintenanceService.getAccountIDSetupList().then(data => {
              this.AccountIdOptions = data.result.records;
               this.AccountIdOptions.splice(0,{ "accountTypeId": "", "mainAccountNumber": this.select });

       });
   }
   

    getFinancialDimentionsById(row: any)
    {
        this.currencyId=[];
        this.glAccountId=[];
        this.bankId=[];
        this.isModify=true;
        this.showBtns=true;
         this.checkbookMaintenanceService.getCheckbookMaintenanceById(row.checkBookId).then(data => {
              this.model = data.result; 
              this.model.inactive = !this.model.inactive;

              this.unFrmtCashAccountBalance = this.model.cashAccountBalance;
              this.unFrmtCurrentCheckbookBalance = this.model.currentCheckbookBalance;
              this.unFrmtExceedMaxCheckAmount=this.model.exceedMaxCheckAmount;
              this.unFrmtLastReconciledBalance=this.model.lastReconciledBalance;

              var selectedCurrency = this.ArrCurrencyId.find(x => x.id ==  data.result.currencyId);
              this.currencyId.push({'id':selectedCurrency.id,'itemName':selectedCurrency.itemName});
              if(selectedCurrency)
              {
                this.formatCashAccountBalanceAmount();
                this.formatCheckBookBalanceAmount();
                this.formatExceedMaxCheckAmount();
                this.formatLastReconciledBalanceAmount();
              }
              var selectedBankId = this.ArrBankId.find(x => x.id ==  data.result.bankId);
              this.bankId.push({'id':selectedBankId.id,'itemName':selectedBankId.itemName});
              
              var selectedAccount = this.ArrglAccountId.find(x => x.id ==  data.result.glAccountId);
              this.glAccountId.push({'id':selectedAccount.id,'itemName':selectedAccount.itemName});
              var lastReconciledDate = this.model.lastReconciledDate;
              var lastReconciledDateData = lastReconciledDate.split('/');
              this.model.lastReconciledDate = {"date":{"year":parseInt(lastReconciledDateData[2]),"month":parseInt(lastReconciledDateData[1]),"day":parseInt(lastReconciledDateData[0])}};
         }); 
    }

      //get id already exist
	 IsIdAlreadyExist(){
        if(this.model.checkBookId != '' && this.model.checkBookId != undefined && this.model.checkBookId != 'undefined')
        {
            this.checkbookMaintenanceService.getCheckbookMaintenanceById(this.model.checkBookId).then(data => {
                var datacode = data.code;
                if (data.status == "NOT_FOUND") {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("checkBookIdText");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.model.checkBookId='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
    }

    saveCheckbook(f: NgForm)
    {
        this.btndisabled=true;
        this.model.inactive = !this.model.inactive;
        this.model.cashAccountBalance=this.unFrmtCashAccountBalance;
        this.model.currentCheckbookBalance=this.unFrmtCurrentCheckbookBalance;
        this.model.exceedMaxCheckAmount=this.unFrmtExceedMaxCheckAmount;
        this.model.lastReconciledBalance=this.unFrmtLastReconciledBalance;

        if(f.valid && this.isModify && this.model.glAccountId)
        {
            if(this.model.lastReconciledDate.formatted == undefined)
            {
               var lastReconciledDate = this.model.lastReconciledDate;
               if(lastReconciledDate.date != undefined)
               {
                  this.model.lastReconciledDate = lastReconciledDate.date.day +'/'+ lastReconciledDate.date.month +'/'+ lastReconciledDate.date.year;
               }
            }
            else
            {
                this.model.lastReconciledDate = this.model.lastReconciledDate.formatted;
            }
            this.checkbookMaintenanceService.updateCheckbookMaintenance(this.model).then(data => {
                this.bankId=[];
                this.currencyId=[];
                this.glAccountId=[];
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 302) {
                      this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });

                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else if(f.valid && this.model.glAccountId)
        {
             this.model.lastReconciledDate = this.model.lastReconciledDate.formatted;
            this.checkbookMaintenanceService.createCheckbookMaintenance(this.model).then(data => {
                // this.model.exceedMaxCheckAmount='';
                this.showBtns=false;
                this.bankId=[];
                this.currencyId=[];
                this.glAccountId=[];
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
         
    }

    getSegmentCount()
	{
		 this.accountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}
    
	fetchFirstAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		
		if(segmentNumber)
		{
			this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {
                
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberIndex[segmentIndex])
					{
						this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountDescription.push(result.mainAccountDescription);
						this.accountNumberIndex.push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
                    	var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
						this.accountNumberIndex[m]='0';
						this.accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
                        }
                        if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountDescription = [];
					this.accountNumberIndex = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
                    {
						var ctrlId='';
                        if(m >  0)
                        {
                            ctrlId = 'txtSegment_'+m;
                        }
                        else{
                            ctrlId='txtfirstSegment_'+m;
                        }
                        var segment = <HTMLInputElement>document.getElementById(ctrlId);
                        segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
			this.accountDescription = [];
			this.accountNumberIndex = [];

			var currentSegment=segmentIndex;
			for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
			{
				var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
				segment.value = '';
				segment.disabled = true;
			}
		}
	}
	fetchOtherAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberIndex.indexOf(segment.value);
		
			if(segmentNumber)
			{
				this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
					let status = pagedData.status;
    				let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberIndex[segmentIndex])
						{
							this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountDescription.push(result.dimensionDescription);
							this.accountNumberIndex.push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
						segment.value = '';
						this.accountNumberIndex[segmentIndex]='0';
						this.accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberIndex[currentSegment]='0';
				this.accountDescription[currentSegment]='0';
			}
	}

	createNewGlAccountNumber()
	{
		var tempAccountNumberList=[];
        var accountDesc = this.accountDescription.join();
        accountDesc=accountDesc.replace(/,/g , " ");
        accountDesc=accountDesc.replace("0"," ");
		tempAccountNumberList.push({'accountNumberIndex':this.accountNumberIndex,'accountDescription':accountDesc})
		this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {
			
			if(data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED')
			{
				this.toastr.success(data.btiMessage.message);
				this.getAccountNumber();
				this.closeModal();
			}
			else{
				this.toastr.warning(data.btiMessage.message);
			}
		 });
		
	}

    openAccountWindow()
	{
		this.accountNumberIndex=[];
		this.accountDescription=[];
		this.isModalOpen = true;
	}
	closeModal()
    {
         this.isModalOpen = false;
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

    //Change To Original Value of receiptAmount
 formatAmountChanged(Type:string){
    
    if(Type == 'ExceedMaxCheckAmount')
    {
        this.commonService.changeInputNumber("exceedMaxCheckAmount")
        this.model.exceedMaxCheckAmount = this.unFrmtExceedMaxCheckAmount
    }
    else if(Type == 'LastReconciledBalance')
    {
        this.commonService.changeInputNumber("lastReconciledBalance")
        this.model.lastReconciledBalance = this.unFrmtLastReconciledBalance
    }
    else if(Type == 'CurrentCheckbookBalance')
    {
        this.commonService.changeInputNumber("currentCheckbookBalance")
        this.model.currentCheckbookBalance = this.unFrmtCurrentCheckbookBalance
    }
    else if(Type == 'CashAccountBalance')
    {
        this.commonService.changeInputNumber("cashAccountBalance")
        this.model.cashAccountBalance = this.unFrmtCashAccountBalance
    }
    
  // this.commonService.changeInputNumber("arTransactionCost") ;
  
}
// get Original receiptAmount
getAmountValue(Type:string,event){
    if(event)
    {
        var amt = event.split(".")
        if(amt.length == 1 && amt[0].length >= 6){
            event = amt[0].slice(0, 6)
        }
        else{
            if(amt.length > 1 && amt[1].length >= 3){
                event = amt[0]+"."+amt[1].slice(0, 3)
            }
            if(amt.length > 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)+"."+amt[1]
            }
        }
    }
  
   if(Type == 'ExceedMaxCheckAmount')
   {
       this.unFrmtExceedMaxCheckAmount = event
   }
   else if(Type == 'LastReconciledBalance')
   {
       this.unFrmtLastReconciledBalance = event
   }
   else if(Type == 'CurrentCheckbookBalance')
   {
       this.unFrmtCurrentCheckbookBalance = event
   }
   else if(Type == 'CashAccountBalance')
   {
       this.unFrmtCashAccountBalance = event
   }
}
    
formatCheckBookBalanceAmount(){
    
    if(this.unFrmtCurrentCheckbookBalance)
    {
        this.commonService.changeInputText("currentCheckbookBalance")
        var inputValue = this.unFrmtCurrentCheckbookBalance;
        
        this.model.currentCheckbookBalance = this.unFrmtCurrentCheckbookBalance;
        if (this.currencyId[0].id !='0' ){
            let submitInfo = {
                'currencyId':this.currencyId[0].id
            };
            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                this.model.currentCheckbookBalance = this.commonService.formatAmount(data, inputValue)
                
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                }, 100)
            });
        }else{
            this.formatAmountChanged('CurrentCheckbookBalance')
        }
    }
}

formatCashAccountBalanceAmount(){
    
    if(this.unFrmtCashAccountBalance)
    {
        this.commonService.changeInputText("cashAccountBalance")
        var inputValue = this.unFrmtCashAccountBalance;
        
        this.model.cashAccountBalance = this.unFrmtCashAccountBalance;
        if (this.currencyId[0].id !='0' ){
            let submitInfo = {
                'currencyId':this.currencyId[0].id
            };
            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                this.model.cashAccountBalance = this.commonService.formatAmount(data, inputValue)
                
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                }, 100)
            });
        }else{
            this.formatAmountChanged('CashAccountBalance')
        }
    }
}

formatExceedMaxCheckAmount(){
    
    if(this.unFrmtExceedMaxCheckAmount)
    {
        this.commonService.changeInputText("exceedMaxCheckAmount")
        var inputValue = this.unFrmtExceedMaxCheckAmount;
        
        this.model.exceedMaxCheckAmount = this.unFrmtExceedMaxCheckAmount;
        if (this.currencyId[0].id !='0' ){
            let submitInfo = {
                'currencyId':this.currencyId[0].id
            };
            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                this.model.exceedMaxCheckAmount = this.commonService.formatAmount(data, inputValue)
                
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                }, 100)
            });
        }else{
            this.formatAmountChanged('exceedMaxCheckAmount')
        }
    }
}


formatLastReconciledBalanceAmount(){
    
    if(this.unFrmtLastReconciledBalance)
    {
        this.commonService.changeInputText("lastReconciledBalance")
        var inputValue = this.unFrmtLastReconciledBalance;
        
        this.model.lastReconciledBalance = this.unFrmtLastReconciledBalance;
        if (this.currencyId[0].id !='0' ){
            let submitInfo = {
                'currencyId':this.currencyId[0].id
            };
            this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
                this.model.lastReconciledBalance = this.commonService.formatAmount(data, inputValue)
                
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                }, 100)
            });
        }else{
            this.formatAmountChanged('lastReconciledBalance')
        }
    }
}

	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
		

}