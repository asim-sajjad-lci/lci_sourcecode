import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import {MainAccountSetup} from '../../../../financialModule/_models/master-data/mainAccountSetup';
import { MainAccountSetupService } from '../../../../financialModule/_services/MasterData/mainAccountSetup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<main-account-setup></main-account-setup>',
    templateUrl: './main-account-setup.component.html',
    styles: ["user.component.css"],
    providers:[MainAccountSetupService,CommonService]
})

//export to make it available for other classes
export class MainAccountSetupComponent {
    page = new Page();
    rows = new Array<MainAccountSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    // Serachable Select Box Step1
	ArraccountTypeId = [];
    accountTypeId : any = [];
    ddlaccountTypeIdSetting = {};
	// Serachable Select Box Step1 End
    
    // Serachable Select Box Step1
	ArraccountCategoryId = [];
    accountCategoryId : any = [];
    ddlaccountCategoryIdSetting = {};
    
	// Serachable Select Box Step1 End
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    isModify:boolean=false;
    accountoption;
    categoryoption;
    ArrBalanceTypeStatus=[];
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    add;
    oldmainAccountNumber;
    myHtml;
    disableId;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    search=Constants.search;
    btndisabled:boolean=false;
    
    @ViewChild(DatatableComponent) table: DatatableComponent;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private mainAccountSetupService: MainAccountSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private commonService:CommonService){
            this.page.pageNumber = 0;
            this.page.size = 5;
            this.disableId = false;
            this.add = true;
            
        }
        
        ngOnInit() {
            this.getAddScreenDetail();
            this.getViewScreenDetail();
            this.setPage({ offset: 0 });
            this.getaccountType();
            this.getaccountCategory();
            
            this.mainAccountSetupService.getTypicalBalanceTypeStatus().then(data => {
                this.ArrBalanceTypeStatus = data.result;
                this.ArrBalanceTypeStatus.splice(0, 0, { "typeId": "", "name": this.select });
                this.model.tpclblnc='';
            });
            
            // Serachable Select Box Step2
            this.ddlaccountTypeIdSetting = { 
                singleSelection: true, 
                text:this.select,
                enableSearchFilter: true,
                classes:"myclass custom-class",
                searchPlaceholderText:this.search,
            }; 
            
            this.ddlaccountCategoryIdSetting = { 
                singleSelection: true, 
                text:this.select,
                enableSearchFilter: true,
                classes:"myclass custom-class",
                searchPlaceholderText:this.search,
            }; 
            // Serachable Select Box Step2
            
        }
        
        getAddScreenDetail()
        {
            this.screenCode = "S-1093";
            // default form parameter for adding CURRENCY
            this.defaultAddFormValues = [
                { 'fieldName': 'ADD_MAIN_ACC_MAIN_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_MAIN_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_MAIN_ACC_ACC_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_ACC_CATEGORY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_TYPICAL_BALANCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_MAIN_ACC_AILAS_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_AILAS_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_MAIN_ACC_ALLOW_ACC_ENTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_EXCH_TB_SETUP_MULTIPLY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_MAIN_ACC_ACTIVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_MAIN_ACC_USER_DEF1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_USER_DEF2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_USER_DEF3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADD_MAIN_ACC_USER_DEF4', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_USER_DEF5', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_MAIN_ACC_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
            this.getScreenDetail(this.screenCode,'Add');
        }
        
        getViewScreenDetail()
        {
            this.screenCode = "S-1094";
            this.defaultManageFormValues = [
                { 'fieldName': 'MANAGE_MAIN_ACC_MAIN_ACC_NO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_MAIN_ACC_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_MAIN_ACC_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_MAIN_ACC_ACC_TYPE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_MAIN_ACC_ACC_CATEGORY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_MAIN_ACC_ACC_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'MANAGE_MAIN_ACC_ACC_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                
            ];
            this.getScreenDetail(this.screenCode,'Manage');
        }
        
        getScreenDetail(screenCode,ArrayType)
        {
            
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
                this.screenName=data.result.dtoScreenDetail.screenName;
                this.moduleName=data.result.moduleName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                if(ArrayType == 'Add')
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
                else
                {
                    for (var j = 0; j < this.availableFormValues.length; j++) {
                        var fieldKey = this.availableFormValues[j]['fieldName'];
                        var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                        var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                        objDefault['fieldValue'] = objAvailable['fieldValue'];
                        objDefault['helpMessage'] = objAvailable['helpMessage'];
                        objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                        objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                        objDefault['readAccess'] = objAvailable['readAccess'];
                        objDefault['writeAccess'] = objAvailable['writeAccess'];
                    }
                }
            });
        }
        
        //setting pagination
        setPage(pageInfo) {
            this.selected = []; // remove any selected checkbox on paging
            this.page.pageNumber = pageInfo.offset;
            this.mainAccountSetupService.searchMainAccountSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.mainAccountSetupService.searchMainAccountSetup(this.page, this.searchKeyword).subscribe(pagedData => {
                
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            }, error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        // getMainAccountSetupById
        getMainAccountSetupById(row: any)
        {
            this.showBtns=true;
            this.accountTypeId=[];
            this.accountCategoryId=[];
            this.isModify=true;
            this.add = false;
            this.disableId = true;
            this.myHtml = "border-vanish";
            this.mainAccountSetupService.getMainAccountSetupById(row.actIndx).then(data => {
                this.model = data.result; 
                this.accountTypeId.push({'id':data.result.accountTypeId,'itemName':data.result.accountTypeName});
                this.accountCategoryId.push({'id':data.result.accountCategoryId,'itemName':data.result.accountCategoryDescription});
            });
        }
        getaccountType()
        {
            this.mainAccountSetupService.getListForAccountType().then(data => {
                this.accountoption = data.result.records;
                // this.accountoption.splice(0, 0, { "accountTypeId": "", "accountTypeName": this.select });
                // this.model.accountCategoryId='';
                // Searchable Select Box Step3
                for(var i=0;i< data.result.records.length;i++)
                {
                    this.ArraccountTypeId.push({ "id": data.result.records[i].accountTypeId, "itemName": data.result.records[i].accountTypeName})
                }
                // Searchable Select Box Step3 End
            });
        }
        
        // Serachable Select Box Step4
        getaccountTypeByID(item:any){
            this.model.accountTypeId=item.id;
            this.commonService.closeMultiselectWithId("accountTypeId")
        }
        OnaccountTypeIdDeSelect(item:any){
            this.model.accountTypeId='';
            this.commonService.closeMultiselectWithId("accountTypeId")
        }
        // Serachable Select Box Step4 ends
        
        // Serachable Select Box Step4
        getaccountCategoryByID(item:any){
            this.model.accountCategoryId=item.id;
            this.commonService.closeMultiselectWithId("accountCategoryId")
        }
        OnaccountCategoryIdDeSelect(item:any){
            this.model.accountCategoryId='';
            this.commonService.closeMultiselectWithId("accountCategoryId")
        }
        // Serachable Select Box Step4 ends
        
        getaccountCategory()
        {
            this.mainAccountSetupService.getListForAccountCategory().then(data => {
                this.categoryoption = data.result.records;
                //  this.categoryoption.splice(0, 0, { "accountCategoryId": "", "accountCategoryDescription": this.select });
                // this.model.accountCategoryId='';
                // Searchable Select Box Step3
                for(var i=0;i< data.result.records.length;i++)
                {
                    this.ArraccountCategoryId.push({ "id": data.result.records[i].accountCategoryId, "itemName": data.result.records[i].accountCategoryDescription})
                }
                // Searchable Select Box Step3 End
            });
        }
        
        changePageSize(event) {
            this.page.size = event.target.value;
            this.setPage({ offset: 0 });
        }
        
        //for reseting data
        clearForm(f){
            
            this.oldmainAccountNumber=this.model.mainAccountNumber;
            
            if(!this.add){
                this.myHtml = "border-vanish";
                f.resetForm();
                this.accountTypeId=[];
                this.accountCategoryId=[];
                this.model.mainAccountNumber= this.oldmainAccountNumber;
                var controlCheckbox = <HTMLInputElement>document.getElementById("txtmainAccountNumber")
                controlCheckbox.value=this.model.mainAccountNumber;
            }else{
                this.myHtml = " ";
                f.resetForm();
            }
        }
        
        Cancel(f)
        {
            
            this.add = true;
            this.model.mainAccountNumber='';
            this.accountTypeId=[];
            this.accountCategoryId=[];
            this.clearForm(f);
            this.disableId=false;
        }
        //get id already exist
        IsIdAlreadyExist(){
            if(this.model.mainAccountNumber != '' && this.model.mainAccountNumber != undefined && this.model.mainAccountNumber != 'undefined')
            {
                this.mainAccountSetupService.getMainAccountSetupById(this.model.mainAccountNumber).then(data => {
                    var datacode = data.code;
                    if (data.status == "NOT_FOUND" || data.btiMessage.messageShort =='RECORD_NOT_FOUND') {
                        return true;
                    }
                    else{
                        var txtId = <HTMLInputElement> document.getElementById("txtmainAccountNumber");
                        txtId.focus();
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        window.scrollTo(0,0);
                        this.model.mainAccountNumber='';
                        return false;
                    }
                    
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
        }
        saveMainAccountSetup(f: NgForm)
        {   
            this.btndisabled=true;
            this.myHtml = "";
            if(this.model.active){
                var active = 1;
            }else{
                var active = 0;
            }
            if(this.model.allowAccountTransactionEntry){
                var allowAccountTransactionEntry = 1;
            }else{
                var allowAccountTransactionEntry = 0;
            }
            if(!this.add)
            {this.disableId = false;
                this.mainAccountSetupService.updateMainAccountSetUp(this.model).then(data => {
                    this.accountTypeId=[];
                    this.accountCategoryId=[];
                    window.scrollTo(0,0);
                    this.btndisabled=false;
                    var datacode = data.code;
                    if (datacode == 201) {
                        this.isModify=false;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });
                        this.add = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                        f.resetForm();
                    }
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            else
            {
                this.mainAccountSetupService.createAccountSetup(this.model).then(data => {
                    this.showBtns=false;
                    this.accountTypeId=[];
                    this.accountCategoryId=[]; 	
                    window.scrollTo(0,0);
                    this.btndisabled=false;
                    var datacode = data.code;
                    if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        // this.myHtml = "border-vanish";
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    
                    else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            
        }
        
        onlyDecimalNumberKey(event) {
            return this.getScreenDetailService.onlyDecimalNumberKey(event);
        }

        	
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
    }