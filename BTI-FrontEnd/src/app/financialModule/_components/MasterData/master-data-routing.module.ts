import { DashboardComponent } from './../../../userModule/_components/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerMaintenanceComponent } from './accountReceivables/customer-maintenance.component';
import { VendorMaintenanceComponent } from './accountPayables/vendor-maintenance.component';
import { FinancialDimentionsComponent } from './General-Ledger/financial-dimensions-setup/financial-dimensions-setup.component';
import { FinancialDimentionsValueComponent } from './General-Ledger/financial-dimensions-value-setup/financial-dimensions-value-setup.component';
import { MainAccountSetupComponent } from './mainaccountsetup/main-account-setup.component';
import { CheckbookMaintenanceComponent } from './General-Ledger/checkbook-maintenance/checkbook-maintenance.component';
import { SalesTerritorySetupComponent } from './accountReceivables/salesTerritorySetup/sales-territory-setup.component';
import { SalesPersonMaintenanceSetupComponent } from './accountReceivables/salespersonMaintenanceSetup/salesperson-maintenance-setup.component';
import { NationalAccountMaintenanceComponent } from './accountReceivables/national-account-maintenance/national-account-maintenance.component'; 
import { CustomerMaintenanceOptionsComponent } from './accountReceivables/customer-maintenance-options/customer-maintenance-options.component';
import { AccountCurrenciesComponent } from './General-Ledger/accountCurrencies/account-currencies.component';
import { CustomerAccountMaintenanceComponent } from './accountReceivables/customer-account-maintenance/customer-account-maintenance.component';
import { VendorAccountMaintenanceComponent } from './accountPayables/vendor-accounts-maintenance/vendor-account-maintenance.component';
import { VendotMaintenanceOptionsComponent } from './accountPayables/vendorMaintenaceOptions/vendor-maintenance-options.component';
import {FixedAllocationAccountSetupComponent } from './General-Ledger/fixed-allocation-accounts/fixed-allocation-account-setup.component';
import {FaAccountMaintenanceComponent } from '../MasterData/fixedAssets/faAccountMaintenance/faAccountMaintenance';
import {FaBookMaintenanceComponent } from '../MasterData/fixedAssets/fabookMaintenance/book-maintenance.component';
import {FaInformationMaintenance } from '../MasterData/fixedAssets/faInformationMaintenance/information-maintenance.component';
import {FaLeaseMaintenanceComponent } from '../MasterData/fixedAssets/faLeaseMaintenance/lease-maintenance.component';
import {FaInsuranceMaintenanceComponent } from '../MasterData/fixedAssets/faInsuranceMaintenance/insurance-maintenance.component';
import { PostingAccountSetupComponent } from '../GeneralLedgerSetup/posting-accounts-setup/posting-accounts-setup.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'vendormaintenance', component:  VendorMaintenanceComponent },
  { path: 'customermaintenance', component: CustomerMaintenanceComponent },
  { path: 'financialdimensionsetup', component: FinancialDimentionsComponent },
  { path: 'financialdimensionvalue', component: FinancialDimentionsValueComponent },
  { path: 'mainaccountsetup', component:  MainAccountSetupComponent },
  { path:'checkbookmaintenance', component:CheckbookMaintenanceComponent},
  {path:'salesterritorysetup', component:SalesTerritorySetupComponent},
  {path:'salespersonmaintenance', component:SalesPersonMaintenanceSetupComponent},
  {path:'nationalaccount',component:NationalAccountMaintenanceComponent},
  {path:'customeroptionmaintenance', component:CustomerMaintenanceOptionsComponent},
  {path:'customeroptionmaintenance/:customerId', component:CustomerMaintenanceOptionsComponent},
  {path:'accountCurrencies', component:AccountCurrenciesComponent},
  {path:'customeraccountmaintenance', component:CustomerAccountMaintenanceComponent},
  {path:'customeraccountmaintenance/:customerId', component:CustomerAccountMaintenanceComponent},
  {path:'vendoraccountmaintenance',component:VendorAccountMaintenanceComponent},
  {path:'vendoraccountmaintenance/:vendorId',component:VendorAccountMaintenanceComponent},
  {path:'vendoroptionmaintenance/:vendorId', component:VendotMaintenanceOptionsComponent},
  {path:'fixedallocationaccount',component:FixedAllocationAccountSetupComponent},
  {path:'faaccountmaintenance',component:FaAccountMaintenanceComponent},
  {path:'fabookmaintenance',component:FaBookMaintenanceComponent},
  {path:'fainformationmaintenance',component:FaInformationMaintenance},
  {path:'faleasemaintenance',component:FaLeaseMaintenanceComponent},
  {path:'fainsurancemaintenance',component:FaInsuranceMaintenanceComponent},
  { path: 'postingaccountsetup', component: PostingAccountSetupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterDataRoutingModule { }
