import { Component,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { faAccountMaintenance} from '../../../../_models/master-data/fixedAssets/faAccountMaintenance';
import { faAccountMaintenanceService} from '../../../../_services/MasterData/fixedAssets/faAccountMaintenanceService';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { AccountStructureService } from '../../../../_services/general-ledger-setup/accounts-structure-setup.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import { Constants} from '../../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../../_sharedresource/page';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';  

@Component({
	selector: 'fa - AccountMaintenance',
	templateUrl: './faAccountMaintenance.html',
	providers: [faAccountMaintenanceService,AccountStructureService,CommonService],
})

export class FaAccountMaintenanceComponent {
	page = new Page();
	rows = new Array<faAccountMaintenance>();
	selected = [];
	moduleCode = Constants.financialModuleCode;
	sameAccountNumberMsg=Constants.sameAccountNumberMsg;
	screenCode;
	screenName;
	moduleName;
	isScreenLock;
	defaultAddFormValues: Array<object>;
	availableFormValues: [object];
	messageText;
	hasMsg = false;
	showMsg = false;
	isSuccessMsg;
	isfailureMsg;
	model: any = {};
	select=Constants.select;
	atATimeText=Constants.atATimeText;
	accountNumberTitle=Constants.accountNumberTitle;
	search=Constants.search;
	isModify:boolean=false;
	customerClassDescription:string;
	customeClassDescriptionArabic:string;
	classOptions;
	accountGroup;
	accountNumberList =[];
	AccountTypeList=[];
	fetchedData=[];
	// Serachable Select Box Step1
	ArrAssetId = [];
	assetId : any = [];
	ddlAssetIdSetting = {};
	// Serachable Select Box Step1 End
	// Serachable Select Box Step1
	ArrAccountGroupIndex = [];
	accountGroupIndex : any = [];
	ddlAccountGroupIndexSetting = {};
	// Serachable Select Box Step1 End
	arrSegment= [];
	btndisabled:boolean=false;
	
	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private faAccountMaintenanceService: faAccountMaintenanceService,
		private AccountStructureService:AccountStructureService,
		private getScreenDetailService: GetScreenDetailService,
		private commonService:CommonService,
		public toastr: ToastsManager,
		vcr: ViewContainerRef,
	){
		this.toastr.setRootViewContainerRef(vcr);
	}
	
	ngOnInit() {
		this.accountNumberList=[];
		this. getFAGeneralMaintenanceList();
		this.searchAccountGroupSetup();
		this.getAccountType();
		this.getAddScreenDetail();
		this.getSegmentCount();
		window.setTimeout(() => {
			this.getAccountMaintainanceData();
		}, 500);
		
		// Serachable Select Box Step2
		this.ddlAssetIdSetting = { 
			singleSelection: true, 
			text:this.select,
			enableSearchFilter: true,
			classes:"myclass custom-class",
			searchPlaceholderText:this.search,
		}; 
		// Serachable Select Box Step2
		
		// Serachable Select Box Step2
		this.ddlAccountGroupIndexSetting = { 
			singleSelection: true, 
			text:this.select,
			enableSearchFilter: true,
			classes:"myclass custom-class",
			searchPlaceholderText:this.search,
		}; 
		// Serachable Select Box Step2
	}
	
	getSegmentCount()
	{
		this.AccountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		});
	}
	
	getAddScreenDetail()
	{
		this.screenCode = "S-1216";
		this.defaultAddFormValues = [
			{ 'fieldName': 'ADD_FA_ACC_MAIN_ASSET_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_ACC_MAIN_ACC_GROUP_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
			{ 'fieldName': 'ADD_FA_ACC_MAIN_ACC_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_ACC_MAIN_ACC_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
			{ 'fieldName': 'ADD_FA_ACC_MAIN_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
			{ 'fieldName': 'ADD_FA_ACC_MAIN_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
			{ 'fieldName': 'ADD_FA_ACC_MAIN_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
		];
		this.getScreenDetail(this.screenCode);
	}
	
	
	
	getScreenDetail(screenCode)
	{
		
		this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
			{
				this.isScreenLock = res;
			});
			this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
				this.screenName=data.result.dtoScreenDetail.screenName;
				this.moduleName=data.result.moduleName;
				this.availableFormValues = data.result.dtoScreenDetail.fieldList;
				
				for (var j = 0; j < this.availableFormValues.length; j++) {
					var fieldKey = this.availableFormValues[j]['fieldName'];
					var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
				}
				
			});
		}
		
		getAccountType()
		{
			this.faAccountMaintenanceService.getAccountType().then(data => {
				this.AccountTypeList = data.result;
				for(let i=0; i<this.AccountTypeList.length; i++){
					let temp={
						"accountType": this.AccountTypeList[i]["typeId"],
						"typeValue": this.AccountTypeList[i]["name"],
						"accountDescription":[],
						"accountNumberIndex":[]
					}
					this.accountNumberList.push(temp);
				}
			});
		}
		getFAGeneralMaintenanceList()
		{
			this.faAccountMaintenanceService.getFAGeneralMaintenance().then(data => {
				
				if(data.btiMessage.messageShort !="RECORD_NOT_FOUND")
				{
					this.classOptions = data.result.records;
					for(var i=0;i< data.result.records.length;i++)
					{
						this.ArrAssetId.push({ "id": data.result.records[i].assetId, "itemName": data.result.records[i].faDescription})
					}
				}
			});
		}
		
		searchAccountGroupSetup(){
			this.faAccountMaintenanceService.searchAccountGroupSetup().then(data => {
				this.accountGroup = data.result.records;
				for(var i=0;i< data.result.records.length;i++)
				{
					this.ArrAccountGroupIndex.push({ "id": data.result.records[i].accountGroupIndex, "itemName": data.result.records[i].accountGroupId})
				}
			});
		}
		
		// Serachable Select Box Step4
		getAssetByID(item:any){
			//this.model.assetId=item.id;
			this.assetId = [];
			this.assetId.push({ "id": item.id, "itemName": item.itemName})
			this.commonService.closeMultiselectWithId("assetId")
		}
		OnAssetIdDeSelect(item:any){
			//this.model.assetId='';
			this.assetId = [];
			this.commonService.closeMultiselectWithId("assetId")
		}
		// Serachable Select Box Step4 ends
		
		// Serachable Select Box Step4
		getAccountGroupIndexByID(item:any){
			//this.model.accountGroupIndex=item.id;
			this.accountGroupIndex = [];
			this.accountGroupIndex.push({ "id": item.id, "itemName": item.itemName})
			this.commonService.closeMultiselectWithId("accountGroupIndex")
		}
		OnAccountGroupIndexDeSelect(item:any){
			//this.model.accountGroupIndex='';
			this.accountGroupIndex = [];
			this.commonService.closeMultiselectWithId("accountGroupIndex")
		}
		// Serachable Select Box Step4 ends
		
		
		getAccountMaintainanceData()
		{
			this.faAccountMaintenanceService.getfaAccountMaintenanceList().then(data => {
				if(data.btiMessage.messageShort !='RECORD_NOT_FOUND')
				{
					this.model=data.result;
					
					var selectedAsset = this.ArrAssetId.find(x => x.id ==   data.result.assetId);
					this.assetId.push({ "id": selectedAsset.id, "itemName": selectedAsset.itemName})
					
					var selectedAccountGroupIndex = this.ArrAccountGroupIndex.find(x => x.id ==   data.result.accountGroupIndex);
					this.accountGroupIndex.push({ "id": selectedAccountGroupIndex.id, "itemName": selectedAccountGroupIndex.itemName})
					for(var i=0;i<this.accountNumberList.length;i++)
					{
						var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
						if(currentRowSegment)
						{
							for(var k=0;k< currentRowSegment.length;k++)
							{
								var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
								if(segment)
								{
									if(k > 0)
									{
										segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
										segment.disabled= true;
									}
									else{
										segment.disabled=false;
									}
									segment.value = '';
								}
							}
							this.accountNumberList[i]["accountDescription"] =[];
							this.accountNumberList[i]["accountNumberIndex"] =[];
							this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = [];
						}
						
						
						for(var j =0;j<data.result.accountNumberList.length;j++)
						{
							if(this.accountNumberList[i]["accountType"] == data.result.accountNumberList[j]["accountType"])
							{
								this.accountNumberList[i]["accountDescription"] = data.result.accountNumberList[j]["accountDescription"].split(',');
								this.accountNumberList[i]["accountNumberIndex"] = data.result.accountNumberList[j]["accountNumberIndex"];
								this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = data.result.accountNumberList[j]["accountNumber"].split('-');
								var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
								
								for(var k=0;k<currentRowSegment.length;k++)
								{
									var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
									if(k > 0)
									{
										segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
									}
									segment.disabled=false;
									if(currentRowSegment[k] == 0)
									{
										segment.value = '';
									}	
									else{
										segment.value = currentRowSegment[k];
									}
								}
							}
						}
					}
					
				}
				
				// for(let i=0;i<this.model.accountNumberList.length;i++){
				// 	let j = this.model.accountNumberList[i]["accountType"]-1;
				// 	var splitAccountNumber = this.model.accountNumberList[i]["accountNumber"].split("-");
				
				// 	var splitDesription = this.model.accountNumberList[i]["accountDescription"].split(" ");
				
				// 	
				// 	// binding here
				// 	this.accountNumberList[j]["accountNumber"] = this.model.accountNumberList[i]["accountNumber"];
				// 	this.accountNumberList[j]["accountDescription"] = this.model.accountNumberList[i]["accountDescription"];
				// 	this.accountNumberList[j]["accountNumberIndex"] = [];
				
				// 	for(let p=0;p < this.model.accountNumberList[i]["accNumberIndexValue"].length;p++){
				// 		if(p==0){
				// 			this.accountNumberList[j]["accountDescription1"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["description"];
				// 			this.accountNumberList[j]["accountNumberIndex1"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"];
				// 			this.accountNumberList[j]["accountNumberIndex"].push(this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"]);
				// 		}else if(p==1){
				// 			this.accountNumberList[j]["accountDescription2"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["description"];
				// 			this.accountNumberList[j]["dimIndex2"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"];
				// 			this.accountNumberList[j]["accountNumberIndex"].push(this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"]);
				// 		}else if(p==2){
				// 			this.accountNumberList[j]["accountDescription3"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["description"];
				// 			this.accountNumberList[j]["dimIndex3"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"];
				// 			this.accountNumberList[j]["accountNumberIndex"].push(this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"]);
				// 		}else if(p==3){
				// 			this.accountNumberList[j]["accountDescription4"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["description"];
				// 			this.accountNumberList[j]["dimIndex4"] = this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"];
				// 			this.accountNumberList[j]["accountNumberIndex"].push(this.model.accountNumberList[i]["accNumberIndexValue"][p]["indexId"]);
				// 		}
				// 	}
				// 	if(splitAccountNumber[0]){
				// 		this.accountNumberList[j]["accountNumber1"] = splitAccountNumber[0];
				// 	}
				// 	if(splitAccountNumber[1]){
				// 		this.accountNumberList[j]["accountNumber2"] = splitAccountNumber[1];
				// 		this.accountNumberList[j]["disabled2"] = false;
				// 	}
				// 	if(splitAccountNumber[2]){
				// 		this.accountNumberList[j]["accountNumber3"] = splitAccountNumber[2];
				// 		this.accountNumberList[j]["disabled3"] = false;
				// 	}
				// 	if(splitAccountNumber[3]){
				// 		this.accountNumberList[j]["accountNumber4"] = splitAccountNumber[3];
				// 		this.accountNumberList[j]["disabled4"] = false;
				// 	}						
				// }    						
				
			});
		}
		savefaAccountMaintenance(f: NgForm)
		{
			this.btndisabled=true;
			var accountNumberIndexArr=[];
			var tempAccountNumberList=[];
			for(let i=0;i<this.accountNumberList.length;i++){
				if(this.accountNumberList[i]["accountNumberIndex"])
				{
					var accountDesc = this.accountNumberList[i]["accountDescription"].join();
					if(this.accountNumberList[i]["accountNumberIndex"].length > 0)
					{
						tempAccountNumberList.push({
							"accountType":this.accountNumberList[i]["accountType"],
							"accountDescription": accountDesc,
							"accountNumberIndex":this.accountNumberList[i]["accountNumberIndex"]
						});	
					}
				}
			}
			if(tempAccountNumberList.length == 0)
			{
				this.isSuccessMsg = false;
				this.isfailureMsg = true;
				this.showMsg = true;
				this.hasMsg = true;
				this.btndisabled=false;
				this.messageText = this.accountNumberTitle;
				return false ;
			}
			this.model.accountNumberList = tempAccountNumberList;
			this.model.accountGroupIndex =  this.accountGroupIndex[0].id;
			this.faAccountMaintenanceService.savefaAccountMaintenance(this.model).then(data => {
				window.scrollTo(0,0);
				var datacode = data.code;
				this.btndisabled=false;
				if (datacode == 200 || datacode == 201) {
					this.isModify=false;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.hasMsg = true;
					this.showMsg = true;
					this.messageText = data.btiMessage.message;
					// this.getAccountMaintainanceData();
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					}, 4000);
				}
				
			}).catch(error => {
				window.setTimeout(() => {
					this.isSuccessMsg = false;
					this.isfailureMsg = true;
					this.showMsg = true;
					this.hasMsg = true;
					this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
				}, 100)
			}); 
			this.btndisabled=false;
		}  
		
		
		fetchFirstAccountIndexInfo(i,segmentIndex){
			var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+segmentIndex);
			var segmentNumber=segment.value;
			
			var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));
			
			if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
			{
				return false;
			}
			
			if(segmentNumber)
			{
				this.faAccountMaintenanceService.firstTextApi(segmentNumber).subscribe(pagedData => {
					let status = pagedData.status;
					let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberList[i].accountNumberIndex[segmentIndex])
						{
							this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.actIndx.toString();
							this.accountNumberList[i].accountDescription[segmentIndex] = result.mainAccountDescription;
						}
						else{
							this.accountNumberList[i]["accountDescription"].push(result.mainAccountDescription);
							this.accountNumberList[i]["accountNumberIndex"].push(result.actIndx.toString());
						}
						
						for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
						{
							var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
							this.accountNumberList[i].accountNumberIndex[m]='0';
							this.accountNumberList[i].accountDescription[m]='0';
							if(currentSegment != m)
							{
								segment.disabled = false;
							}
							if(m == segmentIndex + 1)
							{
								segment.focus();
							}
						}
					}else{
						this.accountNumberList[i]["accountDescription"] = [];
						this.accountNumberList[i]["accountNumberIndex"] = [];
						var currentSegment=segmentIndex;
						for(var m = currentSegment;m < this.arrSegment.length;m++)
						{
							var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
							if(m == 0)
							{
								segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+m);
							}
							segment.value = '';
						}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
				});
			}
			else{
				this.accountNumberList[i]["accountDescription"] = [];
				this.accountNumberList[i]["accountNumberIndex"] = [];
				
				var currentSegment=segmentIndex;
				for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
				{
					var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
					segment.value = '';
					segment.disabled = true;
				}
			}
		}
		fetchOtherAccountIndexInfo(i,segmentIndex){
			var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
			var segmentNumber=segment.value;
			
			var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));
			if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
			{
				return false;
			}
			var acctIndex = this.accountNumberList[i].accountNumberIndex.indexOf(segment.value);
			if(acctIndex > -1)
			{
				this.toastr.warning(this.sameAccountNumberMsg);
				segment.value = '';
			}
			else{
				if(segmentNumber)
				{
					this.faAccountMaintenanceService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
						let status = pagedData.status;
						let result = pagedData.result;
						if(status == "FOUND"){	
							if(this.accountNumberList[i].accountNumberIndex[segmentIndex] || this.accountNumberList[i].accountNumberIndex[segmentIndex] == '0')
							{
								this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
								this.accountNumberList[i].accountDescription[segmentIndex] = result.dimensionDescription;
							}
							else{
								this.accountNumberList[i]["accountDescription"].push(result.dimensionDescription);
								this.accountNumberList[i]["accountNumberIndex"].push(result.dimInxValue.toString());
							}
						}else{
							var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
							segment.value = '';
							this.accountNumberList[i].accountNumberIndex[segmentIndex]='0';
							this.accountNumberList[i].accountDescription[segmentIndex]='0';
							this.toastr.warning(pagedData.btiMessage.message);				
						}
					});
				}
				else
				{
					var currentSegment=segmentIndex;
					this.accountNumberList[i].accountNumberIndex[currentSegment]='0';
					this.accountNumberList[i].accountDescription[currentSegment]='0';
				}
			}
		}
		
		
		
		resetMe()
		{
			this.assetId=[];
			this.accountGroupIndex=[];
			this.model.accountGroupIndex='';
			this.AccountTypeList=[];
			this. getAccountType();
			this.accountNumberList = [];
		}
		
		
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
		
		
	}