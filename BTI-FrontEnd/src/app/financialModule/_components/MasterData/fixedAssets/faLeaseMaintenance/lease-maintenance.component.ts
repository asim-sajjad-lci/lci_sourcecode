import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { leaseMaintenance } from '../../../../_models/master-data/fixedAssets/faLeaseMaintenance';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import {LeaseMaintenanceService} from '../../../../_services/MasterData/fixedAssets/faLeaseMaintenance.service';
// import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'lease-maintenance',
   templateUrl: './lease-maintenance.component.html', 
    providers: [LeaseMaintenanceService,CommonService],
})

export class FaLeaseMaintenanceComponent implements OnInit {
	customerId;
	records;
	screenCode;
	isScreenLock;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues:any= [];
	gridDefaultFormValues:any= [];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	// Serachable Select Box Step1
	ArrAssetId = [];
	ArrCompanyId = [];
    assetId : any = [];
	leaseCompanyIndex : any = [];
    ddlAssetIdSetting = {};
	ddlCompanyIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<leaseMaintenance>();
    temp = new Array<leaseMaintenance>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	tableViewtext=Constants.tableViewtext;
	EmptyMessage = Constants.EmptyMessage;
	search=Constants.search;
	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	btnCancelText=Constants.btnCancelText;
	showBtns:boolean=false;
	add;
	disableId;
	assets;
	leaseComp;
	leaseTy;
	leaseEndDate;
	leaseUpdateindexValue;
	lease = {
    'assetId': '',
    'leaseCompanyIndex': '',
    'leaseTypeIndex': '',
    'leaseContactNumber': '',
    'leasePayment': '',
    'interestRatePercent': '',
  };
	private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
	constructor(private router: Router,private route: ActivatedRoute,private commonService:CommonService,private leaseMaintenanceService: LeaseMaintenanceService,private getScreenDetailService: GetScreenDetailService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.disableId = false;
		this.assets=[];
		this.leaseComp=[];
		this.leaseTy=[];
		this.leaseUpdateindexValue='';
	
		this.leaseMaintenanceService.getAssets().subscribe(pagedData => {
			if(pagedData)
			{
			 this.assets = pagedData.records;
			    // Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrAssetId.push({ "id": pagedData.records[i].assetId, "itemName": pagedData.records[i].faDescription})
              }
			}
            
			    // Searchable Select Box Step3 End
        });
		this.leaseMaintenanceService.getLeaseCompanyList().subscribe(pagedData => {	
			if(pagedData)
			{
				this.ArrCompanyId = [];
				for(var i=0;i< pagedData.records.length;i++)
				{
				  this.ArrCompanyId.push({ "id": pagedData.records[i].companyIndex, "itemName": pagedData.records[i].companyId})
				}
			}
			//this.leaseComp=data.result;
        });
		this.leaseMaintenanceService.getLeaseType().then(data => {	
            this.leaseTy = data.result;
        });
		
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
				{ 'fieldName': 'MANAGE_FA_LEASE_ASSET_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_LEASE_LEASE_COMPANY_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_LEASE_TYPE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_LEASE_CONTRACT_NO', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_LEASE_END_DATE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_LEASE_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_LEASE_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_LEASE_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
			];

			
			this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1194").then(data => {
				this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
					var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
					var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
				}
			});
		/* for grid binding ends */
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_FA_LEASE_ASSET_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_LEASE_COMPANY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_LEASE_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_LEASE_CONTRACT_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_PAYMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_INTEREST_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_LEASE_END_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_LEASE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
				];

				this.getScreenDetailService.ValidateScreen("S-1193").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1193").then(data => {

				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */
	}
	
	 // Serachable Select Box Step4
		getAssetByID(item:any){
			this.assetId=[];
			this.assetId.push({'id':item.id,'itemName':item.itemName});
			this.commonService.closeMultiselectWithId("assetId")
		}
		OnAssetIdDeSelect(item:any){
			this.assetId=[];
			this.commonService.closeMultiselectWithId("assetId")
		}
		// Serachable Select Box Step4 ends

		// Serachable Select Box Step4
		getCompanyIdByID(item:any){
			this.leaseCompanyIndex=[];
			this.leaseCompanyIndex.push({'id':item.id,'itemName':item.itemName});
			this.commonService.closeMultiselectWithId("leaseCompanyIndex")
		}
		OnCompanyIdDeSelect(item:any){
			this.leaseCompanyIndex=[];
			this.commonService.closeMultiselectWithId("leaseCompanyIndex")
		}
		// Serachable Select Box Step4 ends
        
		
	save(leaseMaintenance:NgForm){
		this.btndisabled=true;
		
			if(this.add){
				let leaseEndDate = this.leaseEndDate.date.day +'/'+ this.leaseEndDate.date.month +'/'+ this.leaseEndDate.date.year;
				let submittedData = {
				'assetId': this.assetId[0].id,
				'leaseCompanyIndex': this.leaseCompanyIndex[0].id,
				'leaseTypeIndex': this.lease.leaseTypeIndex,
				'leaseContactNumber': this.lease.leaseContactNumber,
				'leasePayment': this.lease.leasePayment,
				'interestRatePercent': this.lease.interestRatePercent,
				'leaseEndDate': leaseEndDate,
				'assetSerialId':"0"
				};				
				this.leaseMaintenanceService.save(submittedData).then(data => {
				this.btndisabled=false;
				this.showBtns=false;
				var datacode = data.code;
				if (datacode == 200) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					leaseMaintenance.resetForm();
					this.leaseCompanyIndex = [];
					this.assetId=[];
					this.setPage({ offset: 0 });
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					}, 100);
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.setPage({ offset: 0 });
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}else{			
			let leaseEndDate = this.leaseEndDate.date.day +'/'+ this.leaseEndDate.date.month +'/'+ this.leaseEndDate.date.year;
				let submittedData = {
				'leaseMaintenanceIndex' : this.leaseUpdateindexValue,
				'assetId': this.lease.assetId,
				'leaseCompanyIndex': this.lease.leaseCompanyIndex,
				'leaseTypeIndex': this.lease.leaseTypeIndex,
				'leaseContactNumber': this.lease.leaseContactNumber,
				'leasePayment': this.lease.leasePayment,
				'interestRatePercent': this.lease.interestRatePercent,
				'leaseEndDate': leaseEndDate,
				'assetSerialId':"0"
				};				
				this.leaseMaintenanceService.edit(submittedData).then(data => {
				this.assetId=[];
				var datacode = data.code;
				this.btndisabled=false;
				if (datacode == 200) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					leaseMaintenance.resetForm();
					this.leaseCompanyIndex = [];
				  this.add=true;
				  this.disableId=false;
					this.setPage({ offset: 0 });
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					this.showBtns=false;
					}, 100);
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.setPage({ offset: 0 });
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}
	}
    
	ClearForm(leaseMaintenance){
     leaseMaintenance.resetForm();
	this.assetId=[];
	this.leaseCompanyIndex=[];
	}


    Cancel(leaseMaintenance)
    {
    this.add=true;
    this.ClearForm(leaseMaintenance);
	this.disableId=false;
	}
	

	updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.leaseMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
	
	edit(row : any){
		
		this.assetId=[];
		this.add = false;
		this.disableId = true;
		this.showBtns=true;
		this.leaseUpdateindexValue=row.leaseMaintenanceIndex;
		this.leaseMaintenanceService.getData(row.leaseMaintenanceIndex).then(data => {
			
			this.records = data.result;
			var selectedAsset = this.ArrAssetId.find(x => x.id ==  data.result.assetId);
			if(selectedAsset){
				this.assetId.push({'id':selectedAsset.id,'itemName':selectedAsset.itemName});
			}
			var selectedCompany = this.ArrCompanyId.find(x => x.id ==  data.result.leaseCompanyIndex);
			this.leaseCompanyIndex.push({'id':selectedCompany.id,'itemName':selectedCompany.itemName});
		
			this.lease = {
				'assetId': this.records.assetId,
				'leaseCompanyIndex': this.records.leaseCompanyIndex,
				'leaseTypeIndex': this.records.leaseTypeIndex,
				'leaseContactNumber': this.records.leaseContactNumber,
				'leasePayment': this.records.leasePayment,
				'interestRatePercent': this.records.interestRatePercent,
			  };	
			var splitEndDate = this.records.leaseEndDate.split('/');
			this.leaseEndDate =
			 {
			  "date": {
				"year": splitEndDate[2],
				"month": splitEndDate[1].toString().replace(/(^0)/g,""),
				"day": splitEndDate[0].toString().replace(/(^0)/g,""),
			  },
			};
		});
	}
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.leaseMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	clearEndDate(){
		this.leaseEndDate='';
	}
	ngOnInit() {

		   // Serachable Select Box Step2
		   this.ddlAssetIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2

		 // Serachable Select Box Step2
		   this.ddlCompanyIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
	}

	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
	onFocus(elemId){
        this.commonService.changeInputNumber(elemId)
    }
    onlyNumber(event){
		if(event)
		{
			this.lease.leasePayment = this.commonService.maxlenghtOnNumber(event)
		}
	}
	
	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}

}