import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import { InfoMaintenance } from '../../../../_models/master-data/fixedAssets/faInformationMaintenance';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import {InformationMaintenanceService} from '../../../../_services/MasterData/fixedAssets/faInformationMaintenance.service';
// import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
	selector: 'information-maintenance',
	templateUrl: './information-maintenance.component.html', 
	providers: [InformationMaintenanceService,CommonService],
})

export class FaInformationMaintenance implements OnInit {
	customerId;
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
	availableFormValues: [object];
	gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
	screenName;
	//Step1
	ArrCurrencyId = [];
	currencyId : any = [];
	ddlCurrencyIdSetting = {};
	
	ArrfaPhysicalLocationId = [];
	faPhysicalLocationId : any = [];
	ddlfaPhysicalLocationIdSetting = {};
	
	ArrfaStructureId = [];
	faStructureId : any = [];
	ddlfaStructureIdSetting = {};
	
	ArrfaClassSetupId = [];
	faClassSetupId : any = [];
	ddlfaClassSetupIdSetting = {};
	//Step1 ends
	page = new Page();
	rows = new Array<InfoMaintenance>();
	temp = new Array<InfoMaintenance>();
	Searchselected = [];
	searchKeyword = '';
	ddPageSize = 5;
	atATimeText=Constants.atATimeText;
	selectedText=Constants.selectedText;
	tableViewtext=Constants.tableViewtext;
	totalText=Constants.totalText;
	deleteConfirmationText = Constants.deleteConfirmationText;
	EmptyMessage = Constants.EmptyMessage;
	unFrmtCostAmount;
	btndisabled:boolean=false;
	isScreenLock;
	@ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
	message = { 'type': '', 'text': '' };
	hasMessage;
	hasMsg = false;
	showMsg = false;
	isSuccessMsg = false;
	isfailureMsg = false;
	btnCancelText=Constants.btnCancelText;
	search=Constants.search;
	showBtns:boolean=false;
	add;
	status;
	assetstType;
	currency;
	location;
	physicalLocation;
	structure;
	getClass;
	faAcquisitionDate;
	lastMaintenance;
	faAddedDate;
	disableId;
	myHtml;
	accountGroup;
	property;
	arrAssetIdSerial;
	infoMaintenance = {
		
		'assetId': '',
		'assetIdSerial':1,
		'faDescription': '',
		'faDescriptionArabic': '',
		'assetExtendedDescription': '',
		'assetStatus': '',
		'faShortName': '',
		'faClassSetupId': '',
		'faMasterAssetId': '',
		'faAccountGroupsSetupId': '',
		'faAccountGroupIndex':'',
		'assetType': '',
		'faPropertyTypes': '',
		'faAcquisitionCost': '',
		'currencyId': '',
		'assetLabelId': '',
		'faLocationId': '',
		'assetQuantity': '',
		'faPhysicalLocationId': '',
		'faStructureId': '',
		'employeeId': '',
		'manufacturerName': '',
	};
	private myOptions: INgxMyDpOptions = {
		// other options...
		dateFormat: 'dd/mm/yyyy',
	};
	private myOptions1: INgxMyDpOptions = {
		// other options...
		dateFormat: 'dd/mm/yyyy',
		disableSince: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() +1 }
	};
	private myOptions2: INgxMyDpOptions = {
		// other options...
		dateFormat: 'dd/mm/yyyy',
		disableSince: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() +1 }
	};
	constructor(private router: Router,private route: ActivatedRoute,
		private informationMaintenanceService: InformationMaintenanceService,
		private getScreenDetailService: GetScreenDetailService,
		private commonService: CommonService,) {
			this.add = true;
			let vm = this;
			this.page.pageNumber = 0;
			this.page.size = 5;
			this.disableId = false;
			this.status=[];
			this.assetstType=[];
			this.currency=[];
			this.location=[];
			this.physicalLocation=[];
			this.structure=[];
			this.getClass=[];
			this.accountGroup=[];
			this.property=[];
			this.GetAssetIdSerial();
			this.informationMaintenanceService.getStatus().then(data => {
				this.status = data.result;
			});
			this.informationMaintenanceService.getPropertyType().then(data => {
				
				this.property = data.result;
			});
			this.informationMaintenanceService.getAssetType().then(data => {
				this.assetstType = data.result;
				
			});
			this.informationMaintenanceService.getCurrency().then(pagedData => {
				// Searchable Select Box Step3
				
				this.currency = pagedData.records;
				
				if(pagedData.records.length > 0)
				{
					for(var i=0;i< pagedData.records.length;i++)
					{
						this.ArrCurrencyId.push({ "id": pagedData.records[i].currencyId, "itemName": pagedData.records[i].currencyDescription})
					}
				}
				
				// Searchable Select Box Step3 End
			});
			this.informationMaintenanceService.getClass().then(pagedData => {
				this.getClass = pagedData.records;
				// Searchable Select Box Step3
				for(var i=0;i< pagedData.records.length;i++)
				{
					this.ArrfaClassSetupId.push({ "id": pagedData.records[i].classId, "itemName": pagedData.records[i].description})
				}
				// Searchable Select Box Step3 End
			});
			this.informationMaintenanceService.getLocation().then(pagedData => {
				
				this.location = pagedData.records;
			});
			this.informationMaintenanceService.getPhysicalLocation().then(pagedData => {
				
				
				this.physicalLocation = pagedData.records;
				// Searchable Select Box Step3
				
				for(var i=0;i< pagedData.records.length;i++)
				{
					this.ArrfaPhysicalLocationId.push({ "id": pagedData.records[i].physicalLocationId, "itemName": pagedData.records[i].descriptionPrimary})
				}
				
				// Searchable Select Box Step3 End
			});
			this.informationMaintenanceService.getStructure().then(pagedData => {
				
				if(pagedData.btiMessage.messageShort != "RECORD_NOT_FOUND")
				{
					this.structure = pagedData.result.records;
					
					for(var i=0;i< pagedData.result.records.length;i++)
					{
						this.ArrfaStructureId.push({ "id": pagedData.result.records[i].structureId, "itemName": pagedData.result.records[i].structureDescription})
					}
				}
				// Searchable Select Box Step3 End
			});
			// this.informationMaintenanceService.getAccountGroup().then(pagedData => {
			// 	this.accountGroup.push(pagedData.result);
			// });
			
			this.informationMaintenanceService.getAccountGroup().then(pagedData => {
				
				this.accountGroup  = pagedData.result.records;
			});
			/* for grid binding */
			this.setPage({ offset: 0 });
			this.gridDefaultFormValues = [
				{ 'fieldName': 'MANAGE_FA_INFO_ASSET_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_INFO_DESC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_INFO_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_INFO_CLASS_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_INFO_DATE_ADDED', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_INFO_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_INFO_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
				{ 'fieldName': 'MANAGE_FA_INFO_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
			];
			
			
			this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1170").then(data => {
				this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
					var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
					var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
				}
			});
			/* for grid binding ends */
			
			
			/* for add screen */
			vm.defaultFormValues = [
				{ 'fieldName': 'ADD_FA_INFO_ASSET_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_EXTENDED_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_STATUS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_SHORT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_ASSET_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_MASTER_ASSET_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_ACC_GROUP_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_ASSET_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_PROPERTY_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_ACQUISITION_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_ACQUISITION_COST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_ASSET_LABEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_LOCATION_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_PHY_LOCATION_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_STRUCTURE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_MENUFACTURER_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_QUANTITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_LAST_MAINTENANCE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_ASSET_DATE_ADDED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_INFO_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
			];
			
			this.getScreenDetailService.ValidateScreen("S-1169").then(res=>
				{
					this.isScreenLock = res;
				});
			vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1169").then(data => {
				
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
			/* for add screen ends */
		}
		
		getCurrencyByID(item:any){
			this.currencyId=[];
			this.currencyId.push({'id':item.id,'itemName':item.itemName});
			this.formatAmount();
			this.commonService.closeMultiselectWithId("currencyId")
		}
		OnCurrencyIdDeSelect(item:any){
			this.currencyId=[];
			this.formatAmount();
		}
		
		GetAssetIdSerial()
		{
			this.arrAssetIdSerial=[];
			for(var i=1;i<100;i++)
			{
				this.arrAssetIdSerial.push(i);
			}
			this.commonService.closeMultiselectWithId("currencyId")
		}
		//Is id already exist
		IsIdAlreadyExist(){
			if(this.infoMaintenance.assetId != '' && this.infoMaintenance.assetId != undefined && this.infoMaintenance.assetId != 'undefined')
			{
				this.informationMaintenanceService.get(this.infoMaintenance.assetId).then(data => {
					var datacode = data.code;
					if (data.status == "NOT_FOUND") {
						return true;
					}
					else{
						var txtId = <HTMLInputElement> document.getElementById("ddl_insClassId");
						txtId.focus();
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = data.btiMessage.message;
						this.setPage({ offset: 0 });
						window.setTimeout(() => {
							this.showMsg = false;
							this.hasMsg = false;
						}, 4000);
						window.scrollTo(0,0);
						this.infoMaintenance.assetId='';
						return false;
					}
					
				}).catch(error => {
					window.setTimeout(() => {
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
					}, 100)
				});
			}
		}
		
		// Serachable Select Box Step4
		getfaPhysicalLocationyByID(item:any){
			this.faPhysicalLocationId=[];
			this.faPhysicalLocationId.push({'id':item.id,'itemName':item.itemName});
			this.commonService.closeMultiselectWithId("faPhysicalLocationId")
			
		}
		OnfaPhysicalLocationIdDeSelect(item:any){
			this.faPhysicalLocationId=[];
			this.commonService.closeMultiselectWithId("faPhysicalLocationId")
		}
		
		// Serachable Select Box Step4
		getfaStructureByID(item:any){
			this.faStructureId=[];
			this.faStructureId.push({'id':item.id,'itemName':item.itemName});
			this.commonService.closeMultiselectWithId("faStructureId")
		}
		OnfaStructureIdDeSelect(item:any){
			this.faStructureId=[];
			this.commonService.closeMultiselectWithId("faStructureId")
		}
		
		// Serachable Select Box Step4
		getfaClassSetupByID(item:any){
			this.faClassSetupId=[];
			this.faClassSetupId.push({'id':item.id,'itemName':item.itemName});
			this.commonService.closeMultiselectWithId("faClassSetupId")
		}
		OnfaClassSetupIdDeSelect(item:any){
			this.faClassSetupId=[];
			this.commonService.closeMultiselectWithId("faClassSetupId")
		}
		
		
		save(informationMaintenance:NgForm){
			
			this.btndisabled=true;
			if(this.add){
				let faDate = this.faAcquisitionDate.date.day +'/'+ this.faAcquisitionDate.date.month +'/'+ this.faAcquisitionDate.date.year;
				
				let LmDate="";
				if(this.lastMaintenance){
					LmDate = this.lastMaintenance.date.day +'/'+ this.lastMaintenance.date.month +'/'+ this.lastMaintenance.date.year;
				}
				
				let FaAddedDate = this.faAddedDate.date.day +'/'+ this.faAddedDate.date.month +'/'+ this.faAddedDate.date.year;
				
				let submittedData=
				{
					'assetId': this.infoMaintenance.assetId,
					'assetIdSerial': this.infoMaintenance.assetIdSerial,
					'faDescription': this.infoMaintenance.faDescription,
					'faDescriptionArabic': this.infoMaintenance.faDescriptionArabic,
					'assetExtendedDescription': this.infoMaintenance.assetExtendedDescription,
					'assetStatus': this.infoMaintenance.assetStatus,
					'faShortName': this.infoMaintenance.faShortName,
					'faClassSetupId': this.faClassSetupId[0].id,
					'faMasterAssetId': this.infoMaintenance.faMasterAssetId,
					'faAccountGroupsSetupId': this.infoMaintenance.faAccountGroupsSetupId,
					'faAccountGroupIndex':this.infoMaintenance.faAccountGroupIndex,
					'assetType': this.infoMaintenance.assetType,
					'faPropertyTypes': this.infoMaintenance.faPropertyTypes,
					'faAcquisitionCost': this.unFrmtCostAmount,
					'currencyId': this.currencyId[0].id, 
					'assetLabelId': this.infoMaintenance.assetLabelId,
					'faLocationId': this.infoMaintenance.faLocationId,
					'assetQuantity': this.infoMaintenance.assetQuantity,
					'faPhysicalLocationId': this.faPhysicalLocationId[0].id,
					'faStructureId': this.faStructureId[0].id, 
					'employeeId': this.infoMaintenance.employeeId,
					'manufacturerName': this.infoMaintenance.manufacturerName,
					'faAcquisitionDate': faDate,
					'lastMaintenance': LmDate,
					'faAddedDate': FaAddedDate,
				};
				
				//   this.currencyId=[];
				//  this.faPhysicalLocationId=[];
				//  this.faStructureId=[];
				this.informationMaintenanceService.save(submittedData).then(data => {
					
					this.showBtns=false;
					this.btndisabled=false;
					var datacode = data.code;
					if (datacode == 201) {
						window.setTimeout(() => {
							this.hasMsg = true;
							this.isSuccessMsg = true;
							this.isfailureMsg = false;
							this.showMsg = true;
							informationMaintenance.resetForm();
							this.currencyId=[];
							this.faPhysicalLocationId=[];
							this.faStructureId=[];
							window.setTimeout(() => {
								this.showMsg = false;
								this.hasMsg = false;
							}, 4000);
							this.messageText = data.btiMessage.message;
						}, 100);
						this.setPage({ offset: 0 });
					}else{
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						//this.setPage({ offset: 0 });
						this.messageText = data.btiMessage.message;
						window.setTimeout(() => {
							this.hasMsg = false;  
						}, 4000);
					}
				});
			}else{
				let faDate = this.faAcquisitionDate.date.day +'/'+ this.faAcquisitionDate.date.month +'/'+ this.faAcquisitionDate.date.year;
				let LmDate ="";
				
				if(this.lastMaintenance)
				{
					LmDate = this.lastMaintenance.date.day +'/'+ this.lastMaintenance.date.month +'/'+ this.lastMaintenance.date.year;
				}
				
				let FaAddedDate = this.faAddedDate.date.day +'/'+ this.faAddedDate.date.month +'/'+ this.faAddedDate.date.year;
				let submittedData=
				{
					'assetId': this.infoMaintenance.assetId,
					'faDescription': this.infoMaintenance.faDescription,
					'faDescriptionArabic': this.infoMaintenance.faDescriptionArabic,
					'assetExtendedDescription': this.infoMaintenance.assetExtendedDescription,
					'assetStatus': this.infoMaintenance.assetStatus,
					'faShortName': this.infoMaintenance.faShortName,
					'faClassSetupId': this.faClassSetupId[0].id,
					'faMasterAssetId': this.infoMaintenance.faMasterAssetId,
					'faAccountGroupsSetupId': this.infoMaintenance.faAccountGroupsSetupId,
					'faAccountGroupIndex': this.infoMaintenance.faAccountGroupIndex,
					'assetType': this.infoMaintenance.assetType,
					'faPropertyTypes': this.infoMaintenance.faPropertyTypes,
					'faAcquisitionCost': this.unFrmtCostAmount,
					'currencyId': this.currencyId[0].id, 
					'assetLabelId': this.infoMaintenance.assetLabelId,
					'faLocationId': this.infoMaintenance.faLocationId,
					'assetQuantity': this.infoMaintenance.assetQuantity,
					'faPhysicalLocationId': this.faPhysicalLocationId[0].id,
					'faStructureId': this.faStructureId[0].id, 
					'employeeId': this.infoMaintenance.employeeId,
					'manufacturerName': this.infoMaintenance.manufacturerName,
					'faAcquisitionDate': faDate,
					'lastMaintenance': LmDate,
					'faAddedDate': FaAddedDate,
				};
				
				//   this.currencyId=[];
				//   this.faPhysicalLocationId=[];
				//  this.faStructureId=[];
				
				this.informationMaintenanceService.edit(submittedData).then(data => {
					var datacode = data.code;
					this.btndisabled=false;
					if (datacode == 200) {
						window.setTimeout(() => {
							this.hasMsg = true;
							this.isSuccessMsg = true;
							this.isfailureMsg = false;
							this.showMsg = true;
							informationMaintenance.resetForm();
							this.currencyId=[];
							this.faPhysicalLocationId=[];
							this.faStructureId=[];
							this.add=true;
							this.disableId=false;
							window.setTimeout(() => {
								this.showMsg = false;
								this.hasMsg = false;
							}, 4000);
							this.messageText = data.btiMessage.message;
							this.showBtns=false;
						}, 100);
						this.setPage({ offset: 0 });
					}else{
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						this.setPage({ offset: 0 });
						this.messageText = data.btiMessage.message;
						window.setTimeout(() => {
							this.hasMsg = false;  
						}, 4000);
					}
				});
			}
		}
		
		updateFilter(event) {
			this.searchKeyword = event.target.value.toLowerCase();
			this.page.pageNumber = 0;
			this.page.size = this.ddPageSize;
			this.informationMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
				this.page = pagedData.page;
				this.rows = pagedData.data;
				this.table.offset = 0;
			}, error => {
				this.hasMsg = true;
				window.setTimeout(() => {
					this.isSuccessMsg = false;
					this.isfailureMsg = true;
					this.showMsg = true;
					this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
				}, 100)
			});
		}
		
		
		edit(row : any){
			this.add = false;
			this.disableId = true;
			this.currencyId=[];
			this.faPhysicalLocationId=[];
			this.faStructureId=[];
			this.faClassSetupId=[];
			this.showBtns=true;
			this.informationMaintenanceService.get(row.assetId).then(data => {
				this.records = data.result;
				var selectedCurrency = this.ArrCurrencyId.find(x => x.id ==  data.result.currencyId);
				if(selectedCurrency){
					this.currencyId.push({'id':selectedCurrency.id,'itemName':selectedCurrency.itemName});
				}
				var selectedPhysicalLocation = this.ArrfaPhysicalLocationId.find(x => x.id ==  data.result.faPhysicalLocationId);
				this.faPhysicalLocationId.push({'id':selectedPhysicalLocation.id,'itemName':selectedPhysicalLocation.itemName});
				
				var selectedStructure = this.ArrfaStructureId.find(x => x.id ==  data.result.faStructureId);
				this.faStructureId.push({'id':selectedStructure.id,'itemName':selectedStructure.itemName});
				
				var selectedClass = this.ArrfaClassSetupId.find(x => x.id ==  data.result.faClassSetupId);
				this.faClassSetupId.push({'id':selectedClass.id,'itemName':selectedClass.itemName});
				
				this.infoMaintenance = {
					'assetId': this.records.assetId,
					'assetIdSerial': this.infoMaintenance.assetIdSerial,
					'faDescription': this.records.faDescription,
					'faDescriptionArabic': this.records.assetId,
					'assetExtendedDescription': this.records.faDescriptionArabic,
					'assetStatus': this.records.assetStatus,
					'faShortName': this.records.faShortName,
					'faClassSetupId': this.records.faClassSetupId,
					'faMasterAssetId': this.records.faMasterAssetId,
					'faAccountGroupsSetupId': this.records.faAccountGroupsSetupId,
					'faAccountGroupIndex':this.records.faAccountGroupIndex,
					'assetType': this.records.assetType,
					'faPropertyTypes': this.records.faPropertyTypes,
					'faAcquisitionCost': this.records.faAcquisitionCost,
					'currencyId': this.records.currencyId,
					'assetLabelId': this.records.assetLabelId,
					'faLocationId': this.records.faLocationId,
					'assetQuantity': this.records.assetQuantity,
					'faPhysicalLocationId': this.records.faPhysicalLocationId,
					'faStructureId': this.records.faStructureId,
					'employeeId': this.records.employeeId,
					'manufacturerName': this.records.manufacturerName,
				};
				this.unFrmtCostAmount=this.infoMaintenance.faAcquisitionCost;
				this.formatAmount();
				var splitFADate = this.records.faAcquisitionDate.split('/');
				var splitLastMainDate = '';
				if(this.records.lastMaintenance)
				{
					splitLastMainDate = this.records.lastMaintenance.split('/');
				}
				var splitFaAddedDate = this.records.faAddedDate.split('/');
				this.faAcquisitionDate =
				{
					"date": {
						"year": splitFADate[2],
						"month": splitFADate[1].toString().replace(/(^0)/g,""),
						"day": splitFADate[0].toString().replace(/(^0)/g,""),
					},
				};
				if(this.records.lastMaintenance)
				{
					this.lastMaintenance =
					{
						"date": {
							"year": splitLastMainDate[2],
							"month": splitLastMainDate[1].toString().replace(/(^0)/g,""),
							"day": splitLastMainDate[0].toString().replace(/(^0)/g,""),
						},
					};
				}
				
				
				
				this.faAddedDate =
				{
					"date": {
						"year": splitFaAddedDate[2],
						"month": splitFaAddedDate[1].toString().replace(/(^0)/g,""),
						"day": splitFaAddedDate[0].toString().replace(/(^0)/g,""),
					},
				};
				
			});
		}
		
		ClearForm(informationMaintenance:NgForm){
			informationMaintenance.resetForm();
			this.faPhysicalLocationId=[];
			this.faStructureId=[];
			this.faClassSetupId=[];
			this.currencyId=[];
			if(!this.add){
				var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
				selectedValue.value=this.records.assetId;
				this.infoMaintenance.assetId = this.records.assetId;
				
				if(!this.add){
					this.myHtml = "border-vanish";
				}else{
					this.myHtml = "";
				}
			}
			
		}
		
		Cancel(informationMaintenance)
		{
			this.add=false;
			this.ClearForm(informationMaintenance);
			this.infoMaintenance.assetId= '';
			this.disableId=false;
		}
		
		
		setPage(pageInfo) {
			this.Searchselected = [];
			this.page.pageNumber = pageInfo.offset;
			this.informationMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
				this.page = pagedData.page;
				this.rows = pagedData.data;
			});
		}
		
		clearStartDate(){
			this.faAcquisitionDate='';
		}
		clearMaintenanceDate(){
			this.lastMaintenance='';
		}
		clearAddedDate(){
			this.faAddedDate='';
		}
		changePageSize(event) {
			this.page.size = event.target.value;
			this.setPage({ offset: 0 });
		}
		
		ngOnInit() {
			
			this.ddlCurrencyIdSetting = { 
				singleSelection: true, 
				text:this.select,
				enableSearchFilter: true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			}; 
			
			this.ddlfaPhysicalLocationIdSetting = { 
				singleSelection: true, 
				text:this.select,
				enableSearchFilter: true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			}; 
			
			this.ddlfaStructureIdSetting = { 
				singleSelection: true, 
				text:this.select,
				enableSearchFilter: true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			}; 
			
			this.ddlfaClassSetupIdSetting = { 
				singleSelection: true, 
				text:this.select,
				enableSearchFilter: true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			}; 
		}
		
		onlyDecimalNumberKey(event) {
			return this.getScreenDetailService.onlyDecimalNumberKey(event);
		}
		
		//Change To Original Value of receiptAmount
		formatAmountChanged(){
			this.commonService.changeInputNumber("faAcquisitionCost");
			this.infoMaintenance.faAcquisitionCost = this.unFrmtCostAmount
		}
		// get Original receiptAmount
		getreceiptAmountValue(event){
			if(event)
			{
				var amt = event.split(".")
				if(amt.length == 1 && amt[0].length >= 6){
					event = amt[0].slice(0, 6)
				}
				else{
					if(amt.length > 1 && amt[1].length >= 3){
						event = amt[0]+"."+amt[1].slice(0, 3)
					}
					if(amt.length > 1 && amt[0].length >= 6){
						event = amt[0].slice(0, 6)+"."+amt[1]
					}
				}
				this.unFrmtCostAmount = event
			}
		}
		
		formatAmount(){
			
			if(this.unFrmtCostAmount)
			{
				this.commonService.changeInputText("faAcquisitionCost")
				var inputValue = this.unFrmtCostAmount;
				
				this.infoMaintenance.faAcquisitionCost = this.unFrmtCostAmount;
				if (this.currencyId[0].id !='0' ){
					let submitInfo = {
						'currencyId':this.currencyId[0].id
					};
					this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
						this.infoMaintenance.faAcquisitionCost = this.commonService.formatAmount(data, inputValue)
						
					}).catch(error => {
						window.setTimeout(() => {
							this.isSuccessMsg = false;
							this.isfailureMsg = true;
							this.showMsg = true;
							this.hasMsg = true;
							this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
						}, 100)
					});
					
					
				}else{
					this.formatAmountChanged()
				}
			}
		}
		
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
	}