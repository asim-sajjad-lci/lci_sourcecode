import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { BookMaintenance } from '../../../../_models/master-data/fixedAssets/faBookMaintenance';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import {BookMaintenanceService} from '../../../../_services/MasterData/fixedAssets/faBookMaintenance.service';
import {InformationMaintenanceService} from '../../../../_services/MasterData/fixedAssets/faInformationMaintenance.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
// import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
// import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'book-maintenance',
   templateUrl: './book-maintenance.component.html', 
    providers: [BookMaintenanceService,CommonService,InformationMaintenanceService],
})

export class FaBookMaintenanceComponent implements OnInit {
	customerId;
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	myHtml;
	// Serachable Select Box Step1
	ArrAssetId = [];
    assetId : any = [];
    ddlAssetIdSetting = {};

	ArrBookId = [];
    bookId : any = [];
    ddlBookIdSetting = {};
	// Serachable Select Box Step1 End
	page = new Page();
	rows = new Array<BookMaintenance>();
    temp = new Array<BookMaintenance>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	tableViewtext=Constants.tableViewtext;
	btnCancelText=Constants.btnCancelText;
	search=Constants.search;
	showBtns:boolean=false;
	EmptyMessage = Constants.EmptyMessage;
	btndisabled:boolean=false;
	isScreenLock;
    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	assets;
	books;
	disableId;
	placedInServiceDate;
	depreciatedDate;
	convention;
	sw;
	am;
	dep;
	arrStatus=[];
	bookMaintenance = {
    'assetId': '',
    'bookId': '',
    'beginYearCost': '',
    'costBasis': '',
    'salvageAmount': '',
    'yearlyDepreciatedRate': '',
    'currentDepreciation': '',
    'yearToDateDepreciation': '',
    'lifeToDateDepreciation': '',
    'netFAValue': '',
    'averagingConvention': '',
    'switchOver': '',
    'amortizationCode': '',
    'amortizationAmount': '',
    'status': '',
    'originalLifeDay': '',
    'originalLifeYear': '',
    'remainingLifeDay': '',
    'remainingLifeYear': '',
    'depreciationMethodId': '',
  };
	private myOptions: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };
	private myOptions1: INgxMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
    };

	constructor(private router: Router,private route: ActivatedRoute,
		private bookMaintenanceService: BookMaintenanceService,
		private getScreenDetailService: GetScreenDetailService,
		private informationMaintenanceService: InformationMaintenanceService,
		private commonService: CommonService
		) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.disableId = false;
		this.assets=[];
		this.books=[];
		this.convention=[];
		this.sw=[];
		this.am=[];
		this.dep=[];
		this.commonService.GetActiveFAGeneralMaintenance().then(pagedData => {
			 if(pagedData.btiMessage.messageShort =='RECORD_GET_SUCCESSFULLY')
			 {
				
				for(var i=0;i< pagedData.result.records.length;i++)
				{
				  this.ArrAssetId.push({ "id": pagedData.result.records[i].assetId, "itemName": pagedData.result.records[i].faDescription})
				}
			 }
			
        });
		this.bookMaintenanceService.getBooks().subscribe(pagedData => {
            // this.books = pagedData.records;
			 // Searchable Select Box Step3
			 if(pagedData)
			 {
				for(var i=0;i< pagedData.records.length;i++)
				{
				  this.ArrBookId.push({ "id": pagedData.records[i].bookId, "itemName": pagedData.records[i].bookId})
				}
			 }
			 
			    // Searchable Select Box Step3 End
		
        });
		this.bookMaintenanceService.getConventionList().then(data => {
            this.convention = data.result;
			
        });
		this.bookMaintenanceService.getSwitchOverList().then(data => {
            this.sw = data.result;
			
        });
		this.bookMaintenanceService.getamortizationCodeList().then(data => {
            this.am = data.result;
		
        });
		this.bookMaintenanceService.getDepList().then(data => {
            this.dep = data.result;
		
		});
		
		this.informationMaintenanceService.getStatus().then(data => {
			this.arrStatus=[];
            this.arrStatus = data.result;
		});
		
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_FA_BOOK_ASSET_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_BOOK_DEPC_METHOD', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_BOOK_AVG_CONVENTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_BOOK_AMORTIZATION_CODE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_BOOK_STATUS', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_BOOK_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_BOOK_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_FA_BOOK_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
		];

	
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1178").then(data => {
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_FA_BOOK_ASSET_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_BOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_PLACE_SERVICE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_DEPRECIATED_DATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_BEGIN_YEAR_COST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_COST_BASIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_SALVAGE_VALUE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_YEARLY_DEPC_RATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_CURRENT_DEPC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_YTD_DEPC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_LTD_DEPC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_NET_VALUE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_DEPC_METHOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_AVG_CONVENTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_SWITCHOVER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_STATUS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_ORIGIN_LIFE_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_REMAINING_LIFE_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_AMORTIZATION_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_AMORTIZATION_ACC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_FA_BOOK_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
				];

				this.getScreenDetailService.ValidateScreen("S-1177").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1177").then(data => {
				
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
		/* for add screen ends */
	}
    
	updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.bookMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

	 // Serachable Select Box Step4
	getAssetByID(item:any){
		this.assetId=[];
		this.assetId.push({'id':item.id,'itemName':item.itemName});
		this.informationMaintenanceService.get(this.assetId[0].id).then(data => {	
			var selectedStatus = this.arrStatus.find(x => x.typeId == data.result.assetStatus);
			this.bookMaintenance.status = selectedStatus.typeValue;
		});
		this.commonService.closeMultiselectWithId("assetId")
		
	}
	OnAssetIdDeSelect(item:any){
		this.assetId=[];
		this.commonService.closeMultiselectWithId("assetId")
	}
	// Serachable Select Box Step4 ends

	// Serachable Select Box Step4
	getBookByID(item:any){
		this.bookId=[];
		this.bookId.push({'id':item.id,'itemName':item.itemName});
		this.bookMaintenanceService.getLifeDayByFABook(item.id).subscribe(data => {	
			
			if(data.btiMessage.messageShort == 'RECORD_GET_SUCCESSFULLY')
			{
				this.bookMaintenance.originalLifeYear =data.result.originalLifeYear;
				this.bookMaintenance.remainingLifeDay =data.result.remainingLifeDay;
				this.bookMaintenance.originalLifeDay =data.result.originalLifeDay;
				this.bookMaintenance.remainingLifeYear =data.result.remainingLifeYear;
			}
			
		});
		
		this.commonService.closeMultiselectWithId("bookId")
	}
	OnBookIdDeSelect(item:any){
		this.bookId=[];
		this.commonService.closeMultiselectWithId("bookId")
	}
	getLifeDayDynamicByFABook()
	{
		
		if(this.bookId[0].id != '0')
		{
			let RequestedData=
			{
			 'bookId': this.bookId[0].id,
			'originalLifeDay': this.bookMaintenance.originalLifeDay,
			'originalLifeYear': this.bookMaintenance.originalLifeYear,
			}
			this.bookMaintenanceService.getLifeDayDynamicByFABookId(RequestedData).then(data => {
				
				if(data.btiMessage.messageShort == 'RECORD_GET_SUCCESSFULLY')
				{
					// this.bookMaintenance.originalLifeDay = data.result.originalLifeDay;
					// this.bookMaintenance.originalLifeYear = data.result.originalLifeYear;
					this.bookMaintenance.remainingLifeDay = data.result.remainingLifeDay;
					this.bookMaintenance.remainingLifeYear = data.result.remainingLifeYear;
				}
			})
		}
		
	}
	// Serachable Select Box Step4 ends
	
	save(BookMaintenance:NgForm){
		this.btndisabled=true;
		if(this.add){
			let placedInDate = this.placedInServiceDate.date.day +'/'+ this.placedInServiceDate.date.month +'/'+ this.placedInServiceDate.date.year;
		let depDate = this.depreciatedDate.date.day +'/'+ this.depreciatedDate.date.month +'/'+ this.depreciatedDate.date.year;
		let submittedData ={
			'assetId': this.assetId[0].id,
			'bookId': this.bookId[0].id,
			'beginYearCost': this.bookMaintenance.beginYearCost,
			'costBasis': this.bookMaintenance.costBasis,
			'salvageAmount': this.bookMaintenance.salvageAmount,
			'yearlyDepreciatedRate': this.bookMaintenance.yearlyDepreciatedRate,
			'currentDepreciation': this.bookMaintenance.currentDepreciation,
			'yearToDateDepreciation': this.bookMaintenance.yearToDateDepreciation,
			'lifeToDateDepreciation': this.bookMaintenance.lifeToDateDepreciation,
			'netFAValue': this.bookMaintenance.netFAValue,
			'averagingConvention': this.bookMaintenance.averagingConvention,
			'switchOver': this.bookMaintenance.switchOver,
			'amortizationCode': this.bookMaintenance.amortizationCode,
			'amortizationAmount': this.bookMaintenance.amortizationAmount,
			'status': this.bookMaintenance.status,
			'originalLifeDay': this.bookMaintenance.originalLifeDay,
			'originalLifeYear': this.bookMaintenance.originalLifeYear,
			'remainingLifeDay': this.bookMaintenance.remainingLifeDay,
			'remainingLifeYear': this.bookMaintenance.remainingLifeYear,
			'depreciationMethodId': this.bookMaintenance.depreciationMethodId,
			'placedInServiceDate': placedInDate,
			'depreciatedDate': depDate
		}
		
		 this.bookMaintenanceService.save(submittedData).then(data => {
			 this.showBtns=false;
			 		this.assetId=[];
					this.bookId=[];
					this.btndisabled=false;
				var datacode = data.code;
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					BookMaintenance.resetForm();
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
				}, 100);
					this.setPage({ offset: 0 });
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.setPage({ offset: 0 });
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}else{
			let placedInDate = this.placedInServiceDate.date.day +'/'+ this.placedInServiceDate.date.month +'/'+ this.placedInServiceDate.date.year;
			let depDate = this.depreciatedDate.date.day +'/'+ this.depreciatedDate.date.month +'/'+ this.depreciatedDate.date.year;
			let submittedData ={
				'assetId': this.assetId[0].id,
				'bookId': this.bookId[0].id,
				'beginYearCost': this.bookMaintenance.beginYearCost,
				'costBasis': this.bookMaintenance.costBasis,
				'salvageAmount': this.bookMaintenance.salvageAmount,
				'yearlyDepreciatedRate': this.bookMaintenance.yearlyDepreciatedRate,
				'currentDepreciation': this.bookMaintenance.currentDepreciation,
				'yearToDateDepreciation': this.bookMaintenance.yearToDateDepreciation,
				'lifeToDateDepreciation': this.bookMaintenance.lifeToDateDepreciation,
				'netFAValue': this.bookMaintenance.netFAValue,
				'averagingConvention': this.bookMaintenance.averagingConvention,
				'switchOver': this.bookMaintenance.switchOver,
				'amortizationCode': this.bookMaintenance.amortizationCode,
				'amortizationAmount': this.bookMaintenance.amortizationAmount,
				'status': this.bookMaintenance.status,
				'originalLifeDay': this.bookMaintenance.originalLifeDay,
				'originalLifeYear': this.bookMaintenance.originalLifeYear,
				'remainingLifeDay': this.bookMaintenance.remainingLifeDay,
				'remainingLifeYear': this.bookMaintenance.remainingLifeYear,
				'depreciationMethodId': this.bookMaintenance.depreciationMethodId,
				'placedInServiceDate': placedInDate,
				'depreciatedDate': depDate
			}

		 this.bookMaintenanceService.edit(submittedData).then(data => {
			 		this.assetId=[];
					this.bookId=[];
					this.btndisabled=false;
				var datacode = data.code;
				if (datacode == 201) {
				 window.setTimeout(() => {
					this.hasMsg = true;
					this.isSuccessMsg = true;
					this.isfailureMsg = false;
					this.showMsg = true;
					BookMaintenance.resetForm();
					this.add=true;
					this.disableId=false;
					window.setTimeout(() => {
						this.showMsg = false;
						this.hasMsg = false;
					 }, 4000);
					this.messageText = data.btiMessage.message;
					this.showBtns=false;
				}, 100);
					this.setPage({ offset: 0 });
				}else{
				  this.isSuccessMsg = false;
				  this.isfailureMsg = true;
				  this.showMsg = true;
				  this.hasMsg = true;
				  this.setPage({ offset: 0 });
				  this.messageText = data.btiMessage.message;
				   window.setTimeout(() => {
					 this.hasMsg = false;  
				   }, 4000);
				}
			});
		}
		
	}
	clearPlacedServiceDate(){
		this.placedInServiceDate='';
	}
	cleardepDate(){
		this.depreciatedDate='';
	}
	edit(row : any){
		this.add = false;
		this.disableId = true;
		this.assetId=[];
		this.bookId=[];
		this.showBtns=true;
		this.bookMaintenanceService.getData(row.assetId).then(data => {
			this.records = data.result;
			var	selectedAsset = this.ArrAssetId.find(x => x.id ==  data.result.assetId);
			this.assetId.push({'id':selectedAsset.id,'itemName':selectedAsset.itemName});

			var	selectedBook = this.ArrBookId.find(x => x.id ==  data.result.bookId);
			this.bookId.push({'id':selectedBook.id,'itemName':selectedBook.itemName});
			this.bookMaintenance = {
				'assetId': this.records.assetId,
				'bookId': this.records.bookId,
				'beginYearCost': this.records.beginYearCost,
				'costBasis': this.records.costBasis,
				'salvageAmount': this.records.salvageAmount,
				'yearlyDepreciatedRate': this.records.yearlyDepreciatedRate,
				'currentDepreciation': this.records.currentDepreciation,
				'yearToDateDepreciation': this.records.yearToDateDepreciation,
				'lifeToDateDepreciation': this.records.lifeToDateDepreciation,
				'netFAValue': this.records.netFAValue,
				'averagingConvention': this.records.averagingConvention,
				'switchOver': this.records.switchOver,
				'amortizationCode': this.records.amortizationCode,
				'amortizationAmount': this.records.amortizationAmount,
				'status': this.records.status,
				'originalLifeDay': this.records.originalLifeDay,
				'originalLifeYear': this.records.originalLifeYear,
				'remainingLifeDay': this.records.remainingLifeDay,
				'remainingLifeYear': this.records.remainingLifeYear,
				'depreciationMethodId': this.records.depreciationMethodId
			  };
			
			 var splitplacedInServiceDate = this.records.placedInServiceDate.split('/');
			 var splitdepreciatedDate = this.records.depreciatedDate.split('/');

			 this.placedInServiceDate =
			 {
			  "date": {
				"year": splitplacedInServiceDate[2],
				"month": splitplacedInServiceDate[1].toString().replace(/(^0)/g,""),
				"day": splitplacedInServiceDate[0].toString().replace(/(^0)/g,""),
			  },
			};
			
			this.depreciatedDate =
			 {
			  "date": {
				"year": splitdepreciatedDate[2],
				"month": splitdepreciatedDate[1].toString().replace(/(^0)/g,""),
				"day": splitdepreciatedDate[0].toString().replace(/(^0)/g,""),
			  },
			};
						
		});
	}
	ClearForm(BookMaintenance:NgForm){
	      BookMaintenance.resetForm();
		  this.bookId=[];
		  this.assetId=[];
		  var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		  selectedValue.value=this.records.assetId;
		  this.bookMaintenance.assetId = this.records.assetId;
				
			 if(!this.add){
				this.myHtml = "border-vanish";
			 }else{
				this.myHtml = "";
			 }
	}

    
    Cancel(BookMaintenance)
    {
	this.ClearForm(BookMaintenance);
	this.assetId= '';
    this.add=true;
	this.disableId=false;
	}

	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.bookMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	ngOnInit() {
		  // Serachable Select Box Step2
		   this.ddlAssetIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		   this.ddlBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2
	}

	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
	}
	
	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
}