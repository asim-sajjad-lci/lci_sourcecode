import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { insuranMaintenance } from '../../../../_models/master-data/fixedAssets/faInsuranceMaintenance';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import {InsuranceMaintenanceService} from '../../../../_services/MasterData/fixedAssets/faInsuranceMaintenance.service';
import {Constants} from '../../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
	selector: 'insurance-maintenance',
	templateUrl: './insurance-maintenance.component.html', 
	providers: [InsuranceMaintenanceService,CommonService],
})

export class FaInsuranceMaintenanceComponent implements OnInit {
	customerId;
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
	availableFormValues: [object];
	gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
	screenName;
	// Serachable Select Box Step1
	ArrAssetId = [];
	assetId : any = [];
	ddlAssetIdSetting = {};
	
	ArrInsClassId = [];
	insClassId : any = [];
	ddlInsClassIdSetting = {};
	// Serachable Select Box Step1 ends
	btnCancelText=Constants.btnCancelText;
	showBtns:boolean=false;
	page = new Page();
	rows = new Array<insuranMaintenance>();
	temp = new Array<insuranMaintenance>();
	Searchselected = [];
	searchKeyword = '';
	ddPageSize = 5;
	atATimeText=Constants.atATimeText;
	selectedText=Constants.selectedText;
	totalText=Constants.totalText;
	deleteConfirmationText = Constants.deleteConfirmationText;
	search=Constants.search;
	insuranceClass;
	myHtml;
	EmptyMessage = Constants.EmptyMessage;
	btndisabled:boolean=false;
	isScreenLock;
	@ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
	message = { 'type': '', 'text': '' };
	hasMessage;
	hasMsg = false;
	showMsg = false;
	isSuccessMsg = false;
	isfailureMsg = false;
	add;
	disableId;
	assets;
	// insClassId ='';
	insurance = {
		'assetId': '',
		'insClassId': '',
		'insuranceYear': '',
		'insuranceValue': '',
		'replacementCost': '',
		'reproductionCost': '',
	};
	private myOptions: INgxMyDpOptions = {
		// other options...
		dateFormat: 'dd/mm/yyyy',
	};
	private myOptions1: INgxMyDpOptions = {
		// other options...
		dateFormat: 'dd/mm/yyyy',
	};
	
	constructor(private router: Router,private route: ActivatedRoute,private insuranceMaintenanceService: InsuranceMaintenanceService,private getScreenDetailService: GetScreenDetailService, private commonService:CommonService) {
		this.add = true;
		let vm = this;
		this.page.pageNumber = 0;
		this.page.size = 5;
		this.disableId = false;
		this.assets=[];
		this.insuranceMaintenanceService.getAssets().subscribe(pagedData => {
			if(pagedData)
			{
				this.assets = pagedData.records;
				// Searchable Select Box Step3
				for(var i=0;i< pagedData.records.length;i++)
				{
					this.ArrAssetId.push({ "id": pagedData.records[i].assetId, "itemName":pagedData.records[i].faDescription})
				}
			}
		});
		
		this.insuranceMaintenanceService.searchInsuranceClass().subscribe(pagedData => {
			//this.insuranceClass = pagedData.records;
			for(var i=0;i< pagedData.records.length;i++)
			{
				this.ArrInsClassId.push({ "id": pagedData.records[i].insuranceClassId, "itemName":pagedData.records[i].insuranceClassId})
			}
			// Searchable Select Box Step3 End
			
		});
		
		/* for grid binding */
		this.setPage({ offset: 0 });
		this.gridDefaultFormValues = [
			{ 'fieldName': 'MANAGE_FA_INS_ASSET_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
			{ 'fieldName': 'MANAGE_FA_INS_INS_CLASS_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
			{ 'fieldName': 'MANAGE_FA_INS_INS_YEAR', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
			{ 'fieldName': 'MANAGE_FA_INS_INS_VALUE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
			{ 'fieldName': 'MANAGE_FA_INS_STATUS', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
			{ 'fieldName': 'MANAGE_FA_INS_ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
			{ 'fieldName': 'MANAGE_FA_INS_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
			{ 'fieldName': 'MANAGE_FA_INS_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
		];
		
		
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1190").then(data => {
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
			for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
				var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
				var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
				var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
				objDefault['fieldValue'] = objAvailable['fieldValue'];
				objDefault['helpMessage'] = objAvailable['helpMessage'];
				objDefault['deleteAccess'] = objAvailable['deleteAccess'];
				objDefault['readAccess'] = objAvailable['readAccess'];
				objDefault['writeAccess'] = objAvailable['writeAccess'];
			}
		});
		/* for grid binding ends */
		/* for add screen */
		vm.defaultFormValues = [
			{ 'fieldName': 'ADD_FA_INS_ASSET_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_INS_INS_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_INS_INS_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_INS_INS_VALUE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_INS_REPLACEMENT_COST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_INS_REPRODUCTION_COST', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_INS_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
			{ 'fieldName': 'ADD_FA_INS_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }
		];
		
		this.getScreenDetailService.ValidateScreen("S-1189").then(res=>
			{
				this.isScreenLock = res;
			});
			vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1189").then(data => {
				
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
			/* for add screen ends */
		}
		
		save(insuranceMaintenance:NgForm){
			this.btndisabled=true;
			if(this.add){
				let submittedData = {
					'assetId': this.assetId[0].id,
					'insClassId': this.insClassId[0].id,
					'insuranceYear': this.insurance.insuranceYear,
					'insuranceValue': this.insurance.insuranceValue,
					'replacementCost': this.insurance.replacementCost,
					'reproductionCost': this.insurance.reproductionCost,
				}
				
				this.insuranceMaintenanceService.save(submittedData).then(data => {
					this.showBtns=false;
					var datacode = data.code;
					this.btndisabled=false;
					if (datacode == 200) {
						window.setTimeout(() => {
							this.hasMsg = true;
							this.isSuccessMsg = true;
							this.isfailureMsg = false;
							this.showMsg = true;
							this.assetId=[];
							this.insClassId=[];
							insuranceMaintenance.resetForm();
							window.setTimeout(() => {
								this.showMsg = false;
								this.hasMsg = false;
							}, 4000);
							this.messageText = data.btiMessage.message;
						}, 100);
						this.setPage({ offset: 0 });
					}else{
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						//this.setPage({ offset: 0 });
						this.messageText = data.btiMessage.message;
						window.setTimeout(() => {
							this.hasMsg = false;  
						}, 4000);
					}
				});
			}else{
				let submittedData = {
					'assetId': this.assetId[0].id,
					'insClassId': this.insClassId[0].id,
					'insuranceYear': this.insurance.insuranceYear,
					'insuranceValue': this.insurance.insuranceValue,
					'replacementCost': this.insurance.replacementCost,
					'reproductionCost': this.insurance.reproductionCost,
				}
				
				this.insuranceMaintenanceService.edit(submittedData).then(data => {
					this.btndisabled=false;
					var datacode = data.code;
					if (datacode == 200) {
						window.setTimeout(() => {
							this.hasMsg = true;
							this.isSuccessMsg = true;
							this.isfailureMsg = false;
							this.showMsg = true;
							this.assetId=[];
							this.insClassId=[];
							insuranceMaintenance.resetForm();
							this.add=true;
							this.disableId=false;
							window.setTimeout(() => {
								this.showMsg = false;
								this.hasMsg = false;
							}, 4000);
							this.messageText = data.btiMessage.message;
							this.showBtns=false;
						}, 100);
						this.setPage({ offset: 0 });
					}else{
						this.isSuccessMsg = false;
						this.isfailureMsg = true;
						this.showMsg = true;
						this.hasMsg = true;
						//this.setPage({ offset: 0 });
						this.messageText = data.btiMessage.message;
						window.setTimeout(() => {
							this.hasMsg = false;  
						}, 4000);
					}
				});
			}
			this.btndisabled=false;
		}
		
		updateFilter(event) {
			this.searchKeyword = event.target.value.toLowerCase();
			this.page.pageNumber = 0;
			this.page.size = this.ddPageSize;
			this.insuranceMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
				this.page = pagedData.page;
				this.rows = pagedData.data;
				this.table.offset = 0;
			}, error => {
				this.hasMsg = true;
				window.setTimeout(() => {
					this.isSuccessMsg = false;
					this.isfailureMsg = true;
					this.showMsg = true;
					this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
				}, 100)
			});
		}
		
		// Serachable Select Box Step4
		getAssetByID(item:any){
			this.assetId=[];
			this.assetId.push({ "id": item.id, "itemName":item.itemName})
			this.commonService.closeMultiselectWithId("assetId")
		}
		OnAssetIdDeSelect(item:any){
			this.assetId=[];
			this.commonService.closeMultiselectWithId("assetId")
		}
		
		getInsClassIdByID(item:any){
			// this.assetId=[];
			this.insClassId.push({ "id": item.id})
			this.commonService.closeMultiselectWithId("insClassId")
		}
		OnInsClassIdDeSelect(item:any){
			this.insClassId=[];
			this.commonService.closeMultiselectWithId("insClassId")
		}
		// Serachable Select Box Step4 ends
		
		edit(row : any){
			this.assetId=[];
			this.insClassId=[];
			this.add = false;
			this.disableId = true;
			this.showBtns=true;
			this.insuranceMaintenanceService.getData(row.insClassId).then(data => {
				this.records = data.result;
				var selectedAsset=this.ArrAssetId.find(x => x.id ==  data.result.assetId);
				this.assetId.push({ "id": selectedAsset.id, "itemName":selectedAsset.itemName})
				var selectedInsuranceClassID=this.ArrInsClassId.find(x => x.id ==  data.result.insClassId);
				this.insClassId.push({ "id": selectedInsuranceClassID.id, "itemName":selectedInsuranceClassID.itemName})
				this.insurance = {
					'assetId': this.records.assetId,
					'insClassId': this.records.insClassId,
					'insuranceYear': this.records.insuranceYear,
					'insuranceValue': this.records.insuranceValue,
					'replacementCost': this.records.replacementCost,
					'reproductionCost': this.records.reproductionCost,
				};					
			});
		}
		setPage(pageInfo) {
			this.Searchselected = [];
			this.page.pageNumber = pageInfo.offset;
			this.insuranceMaintenanceService.search(this.page, this.searchKeyword).subscribe(pagedData => {
				this.page = pagedData.page;
				this.rows = pagedData.data;
			});
		}
		changePageSize(event) {
			this.page.size = event.target.value;
			this.setPage({ offset: 0 });
		}
		clearForm(insuranceMaintenance:NgForm){
			this.assetId=[],
			this.insClassId=[],
			this.insurance.insuranceYear="",
			this.insurance.insuranceValue="",
			this.insurance.replacementCost="",
			this.insurance.reproductionCost=""
		}
		
		
		Cancel(insuranceMaintenance)
		{
			this.add=true;
			this.clearForm(insuranceMaintenance);
			this.assetId= [];
			this.disableId=false;
		}
		
		ngOnInit() {
			// Serachable Select Box Step2
			this.ddlAssetIdSetting = { 
				singleSelection: true, 
				text:this.select,
				enableSearchFilter: true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			}; 
			
			this.ddlInsClassIdSetting = { 
				singleSelection: true, 
				text:this.select,
				enableSearchFilter: true,
				classes:"myclass custom-class",
				searchPlaceholderText:this.search,
			}; 
			// Serachable Select Box Step2 ends 
		}
		
		onlyDecimalNumberKey(event) {
			return this.getScreenDetailService.onlyDecimalNumberKey(event);
		}
		
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
	}