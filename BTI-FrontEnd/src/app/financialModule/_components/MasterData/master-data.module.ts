import { DashboardModule } from './../../../userModule/_components/dashboard/dashboard.module';
import { DashboardComponent } from './../../../userModule/_components/dashboard/dashboard.component';
import { NgModule,Directive,ElementRef, HostListener, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { CustomerMaintenanceComponent } from './accountReceivables/customer-maintenance.component';
import { VendorMaintenanceComponent } from './accountPayables/vendor-maintenance.component';
import { FinancialDimentionsComponent } from './General-Ledger/financial-dimensions-setup/financial-dimensions-setup.component';
import { FinancialDimentionsValueComponent } from './General-Ledger/financial-dimensions-value-setup/financial-dimensions-value-setup.component';
import { MainAccountSetupComponent } from './mainaccountsetup/main-account-setup.component';
import { CheckbookMaintenanceComponent } from './General-Ledger/checkbook-maintenance/checkbook-maintenance.component';
import { SalesTerritorySetupComponent } from './accountReceivables/salesTerritorySetup/sales-territory-setup.component';
import { SalesPersonMaintenanceSetupComponent } from './accountReceivables/salespersonMaintenanceSetup/salesperson-maintenance-setup.component';
import { NationalAccountMaintenanceComponent } from './accountReceivables/national-account-maintenance/national-account-maintenance.component'; 
import { CustomerMaintenanceOptionsComponent } from './accountReceivables/customer-maintenance-options/customer-maintenance-options.component';
import { AccountCurrenciesComponent } from './General-Ledger/accountCurrencies/account-currencies.component';
import { CustomerAccountMaintenanceComponent } from './accountReceivables/customer-account-maintenance/customer-account-maintenance.component';
import { VendorAccountMaintenanceComponent } from './accountPayables/vendor-accounts-maintenance/vendor-account-maintenance.component';
import { VendotMaintenanceOptionsComponent } from './accountPayables/vendorMaintenaceOptions/vendor-maintenance-options.component';
import {FixedAllocationAccountSetupComponent } from './General-Ledger/fixed-allocation-accounts/fixed-allocation-account-setup.component';
import {FaAccountMaintenanceComponent } from '../MasterData/fixedAssets/faAccountMaintenance/faAccountMaintenance';
import {FaBookMaintenanceComponent } from '../MasterData/fixedAssets/fabookMaintenance/book-maintenance.component';
import {FaInformationMaintenance } from '../MasterData/fixedAssets/faInformationMaintenance/information-maintenance.component';
import {FaLeaseMaintenanceComponent } from '../MasterData/fixedAssets/faLeaseMaintenance/lease-maintenance.component';
import {FaInsuranceMaintenanceComponent } from '../MasterData/fixedAssets/faInsuranceMaintenance/insurance-maintenance.component';
import { PostingAccountSetupComponent } from '../GeneralLedgerSetup/posting-accounts-setup/posting-accounts-setup.component';
import { MasterDataRoutingModule } from './master-data-routing.module';
//import { OnlyAlphaNumeric } from './../../../_sharedresource/OnlyAlphanumeric.directive';

@Directive({
  selector: '[myAutofocus]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@Directive({
  selector: '[OnlyAlphaNumeric]'
})
export class OnlyAlphaNumeric {
  constructor(private el: ElementRef) { }

  @Input() OnlyAlphaNumeric: boolean;

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    let e = <KeyboardEvent> event;
    if (this.OnlyAlphaNumeric) {
        if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39 || e.keyCode >= 65 && e.keyCode <= 90) 
      ) {
          // let it happen, don't do anything
          return;
         
        }
        if(e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) {
          e.preventDefault();
        }
    }
}
}

@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    MasterDataRoutingModule,    
    FormsModule,
    NgxDatatableModule,
    NgxMyDatePickerModule,
    DateTimePickerModule,
    DashboardModule
  ],
  declarations: [
    CustomerMaintenanceComponent,VendorMaintenanceComponent,FinancialDimentionsComponent,
    FinancialDimentionsValueComponent, MainAccountSetupComponent,CheckbookMaintenanceComponent,
    SalesTerritorySetupComponent,SalesPersonMaintenanceSetupComponent
    ,NationalAccountMaintenanceComponent,CustomerMaintenanceOptionsComponent,
    AccountCurrenciesComponent, CustomerAccountMaintenanceComponent,
    VendorAccountMaintenanceComponent,VendotMaintenanceOptionsComponent,FixedAllocationAccountSetupComponent,FaAccountMaintenanceComponent,
    FaBookMaintenanceComponent,FaInformationMaintenance,FaLeaseMaintenanceComponent,
    FaInsuranceMaintenanceComponent,PostingAccountSetupComponent,AutoFocusDirective,OnlyAlphaNumeric
    ]
})
export class MasterDataModule { }
