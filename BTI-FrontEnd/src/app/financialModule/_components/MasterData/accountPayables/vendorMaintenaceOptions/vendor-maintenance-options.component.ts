import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../../_sharedresource/page';
import { CommonService } from '../../../../../_sharedresource/_services/common-services.service';
import {AccountRecievables} from '../../../../../financialModule/_models/accounts-receivables-setup/accounts-receivables-class-setup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import {VendorOptionsMaintenanceService} from '../../../../_services/MasterData/accountPayables/vendor-maintenance-options.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import * as moment from 'moment';
@Component({
   selector: 'vendor-maintenance-options',
   templateUrl: './vendor-maintenance-options.component.html', 
    providers: [VendorOptionsMaintenanceService,CommonService],
})

export class VendotMaintenanceOptionsComponent implements OnInit {
	records;
	screenCode;
	moduleCode = Constants.financialModuleCode;
	btnCancelText=Constants.btnCancelText;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
	screenName;
	isScreenLock;
	currentVendorId:string;
	page = new Page();
	rows = new Array<AccountRecievables>();
    temp = new Array<AccountRecievables>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
	totalText=Constants.totalText;
	search=Constants.search;
    deleteConfirmationText = Constants.deleteConfirmationText;

	// Serachable Select Box Step1
	ArrShipmentMethodId = [];
    shipmentMethodId : any = [];
    ddlShipmentMethodSetting = {};

	ArrVatScheduleId = [];
    vatScheduleId : any = [];
    ddlVatScheduleSetting = {};

	ArrCurrencyId = [];
    currencyId : any = [];
    ddlCurrencyIdSetting = {};

	ArrCheckBookId = [];
    checkBookId : any = [];
    ddlCheckBookIdSetting = {};

	ArrPaymentTermId = [];
    paymentTermId : any = [];
    ddlPaymentTermIdSetting = {};
	// Serachable Select Box Step1 ends

		// Serachable Select Box Step1
	// ArrVendorId = [];
    // vendorId : any = [];
    // ddlVendorIdSetting = {};
	// Serachable Select Box Step1 End

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	add;
	disableId;
	vendors;
	simpleName;
	arabicName;
	mtextDisabled;
	mintextDisabled;
	credittextDisabled;
	shippingMethod;
	vat;
	currency;
	checkbook;
	payments;
	editData;
	condition;
	myHtml;
	vendorId;
	selectedCurrency;
	editMaximumInvoiceAmountValue;
	unFrmtMaximumInvoiceAmountValue;
	editMinimumChargeAmount;
	unFrmtMinimumChargeAmount;
	editCardLimitAmount;
	unFrmtCardLimitAmount;
	editTradeDiscountPercent; 
	unFrmtTradeDiscountPercent;
	vendorMaintenance=
	{
		'vendorId' : '',
		'maximumInvoiceAmount' : '1',
		'maximumInvoiceAmountValue' : '',
		'minimumCharge' : '0',
		'minimumChargeAmount' : '',
		'creditLimit' : '0',
		'cardLimitAmount' : '',
		'tradeDiscountPercent' : '',
		'minimumOrderAmount' : '',
		'shipmentMethodId' : '',
		'vatScheduleId' : '',
		'currencyId' : '',
		'checkBookId' : '',
		'paymentTermId' : '',
		'userDefine1' : '',
		'userDefine2' : '',
		'userDefine3' : '',
		'openMaintenanceHistoryCalendarYear' : false,
		'openMaintenanceHistoryFiscalYear' : false,
		'openMaintenanceHistoryTransaction' : false,
		'openMaintenanceHistoryDistribution' : false
	}
	
	constructor(private router: Router,private route: ActivatedRoute,private vendorOptionsMaintenanceService: VendorOptionsMaintenanceService,private getScreenDetailService: GetScreenDetailService,private commonService:CommonService) {
		this.add = true;
		this.disableId = false;
		let vm = this;
		this.page.pageNumber = 0;
        this.page.size = 5;	
		this.vendors=[];
		this.shippingMethod=[];
		this.vat=[];
		this.currency=[];
		this.checkbook=[];
		this.payments=[];
		this.mtextDisabled = false;
		this.mintextDisabled = false;
		this.credittextDisabled = false;
		this.condition = false;
		 
		/* for edit data */
		this.shipmentMethodId=[];
		//fetch currency records 
	
		this.vendorOptionsMaintenanceService.searchVat().subscribe(pagedData => {
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrVatScheduleId.push({ "id": pagedData.records[i].vatScheduleId, "itemName": pagedData.records[i].vatDescription})
              }
        });

		this.vendorOptionsMaintenanceService.searchCurrency().subscribe(pagedData => {
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrCurrencyId.push({ "id": pagedData.records[i].currencyId, "itemName": pagedData.records[i].currencyDescription})
              }
     	   });

				//fetch checkbook records 
		this.vendorOptionsMaintenanceService.searchCheckbook().subscribe(pagedData => {
            // this.checkbook = pagedData.records;
				// Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrCheckBookId.push({ "id": pagedData.records[i].checkBookId, "itemName": pagedData.records[i].checkBookId})
              }
			    // Searchable Select Box Step3 End
        });
				//fetch checkbook records 
		this.vendorOptionsMaintenanceService.searchShippingMethod().subscribe(pagedData => {
            // this.checkbook = pagedData.records;
				// Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrShipmentMethodId.push({ "id": pagedData.records[i].shipmentMethodId, "itemName": pagedData.records[i].shipmentDescription})
              }
			    // Searchable Select Box Step3 End
        });
		this.vendorOptionsMaintenanceService.searchPayment().subscribe(pagedData => {
            // this.payments = pagedData.records;
							// Searchable Select Box Step3
			 for(var i=0;i< pagedData.records.length;i++)
              {
                this.ArrPaymentTermId.push({ "id": pagedData.records[i].paymentTermId, "itemName": pagedData.records[i].description})
              }
			    // Searchable Select Box Step3 End
        });
	     this.route.params.subscribe((params: Params) => {
                this.currentVendorId = params['vendorId'];
				});
				
				this.vendorOptionsMaintenanceService.getVendorByID(this.currentVendorId ).then(data => {
					
					this.simpleName = data.result.venderNamePrimary;
					this.arabicName = data.result.venderNameSecondary;
				});
				
			window.setTimeout(() => {
				this.bindData()
			 }, 500);
	
	
		/* for edit data ends here */
		
		// // fetch vendor records
		// this.vendorOptionsMaintenanceService.searchVendors().subscribe(pagedData => {
        //     // this.vendors = pagedData.records;
		// 				// Searchable Select Box Step3
		// 	 for(var i=0;i< pagedData.records.length;i++)
        //       {
        //         this.ArrVendorId.push({ "id": pagedData.records[i].venderId, "itemName": pagedData.records[i].venderNamePrimary})
        //       }
		// 	    // Searchable Select Box Step3 End
        // });
	
		
		
		
		// //fetch checkbook records 
		// this.vendorOptionsMaintenanceService.searchCheckbook().subscribe(pagedData => {
        //     // this.checkbook = pagedData.records;
		// 		// Searchable Select Box Step3
		// 	 for(var i=0;i< pagedData.records.length;i++)
        //       {
        //         this.ArrCheckBookId.push({ "id": pagedData.records[i].checkBookId, "itemName": pagedData.records[i].checkbookDescription})
        //       }
		// 	    // Searchable Select Box Step3 End
        // });
		// fetch payments records
		
		
		/* for add screen */
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_VAN_OPT_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MAX_INV_AMT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MAX_INV_AMT_NO_MAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MAX_INV_AMT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MIN_PAYMENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MIN_PAYMENT_NO_MIN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MIN_PAYMENT_PERCENT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MIN_PAYMENT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_CREDIT_LIMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_CREDIT_LIMIT_NO_CREDIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_CREDIT_LIMIT_UNLIMITED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_CREDIT_LIMIT_AMOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_TRADE_DIS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MIN_ORDER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_SHIP_METHOD_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_VAT_SCH_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_CURRENCY_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_PAYMENT_TERM_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_MAINTAIN_HISTORY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_CALENDAR_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_FISCAL_YEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_TRANSACTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_DISTRIBUTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VAN_OPT_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_VAN_OPT_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_VAN_OPT_USER_DEFINE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_VAN_OPT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },			
				{ 'fieldName': 'ADD_VAN_OPT_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },				
				{ 'fieldName': 'ADD_VAN_OPT_ACCOUNTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  }			
				];

				this.getScreenDetailService.ValidateScreen("S-1184").then(res=>
					{
						this.isScreenLock = res;
					});
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,"S-1184").then(data => {
					
					vm.moduleName = data.result.moduleName;
					vm.screenName = data.result.dtoScreenDetail.screenName;
					vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
					for (var j = 0; j < vm.availableFormValues.length; j++) {
						var fieldKey = vm.availableFormValues[j]['fieldName'];
						var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
						var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
						objDefault['fieldValue'] = objAvailable['fieldValue'];
						objDefault['helpMessage'] = objAvailable['helpMessage'];
						objDefault['deleteAccess'] = objAvailable['deleteAccess'];
						objDefault['readAccess'] = objAvailable['readAccess'];
						objDefault['writeAccess'] = objAvailable['writeAccess'];
						objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
					}
			});
		/* for add screen ends */
	}
	
	bindData()
	{
			this.vendorOptionsMaintenanceService.getData(this.currentVendorId).then(data => {
            this.editData = data.result;
			
			if(data.result)
			{

			var	selectedAsset = this.ArrShipmentMethodId.find(x => x.id ==  data.result.shipmentMethodId);
			this.shipmentMethodId.push({'id':selectedAsset.id,'itemName':selectedAsset.itemName});

			//fetch vat method records 
            
		   if(data.result.vatScheduleId)
		   {
			var	selectedBook = this.ArrVatScheduleId.find(x => x.id ==  data.result.vatScheduleId);
			this.vatScheduleId.push({'id':selectedBook.id,'itemName':selectedBook.itemName});
		   }
		    
		
		   if(data.result.currencyId)
		   	{
				var	selectedCurrency = this.ArrCurrencyId.find(x => x.id ==  data.result.currencyId);
				if(selectedCurrency)
				{
					this.currencyId.push({'id':selectedCurrency.id,'itemName':selectedCurrency.itemName});
					this.selectedCurrency = selectedCurrency.id
				}
            }
		    if(data.result.checkBookId)
            {
			 var selectedCheckBook = this.ArrCheckBookId.find(x => x.id ==  data.result.checkBookId);
	         this.checkBookId.push({'id':selectedCheckBook.id,'itemName':selectedCheckBook.itemName});
			}
			
			if(this.editData.maximumInvoiceAmount==1){
				this.mtextDisabled = true;
				this.vendorMaintenance.maximumInvoiceAmountValue='';
			}
			if(this.commonService.checkIsNumeric(this.editData.maximumInvoiceAmountValue)){
				this.editMaximumInvoiceAmountValue = this.editData.maximumInvoiceAmountValue
				this.formatMaximumInvoiceAmountValue();
			}
			if(this.commonService.checkIsNumeric(this.editData.minimumChargeAmount) ){
				this.editMinimumChargeAmount = this.editData.minimumChargeAmount
				this.formatminimumChargeAmount();
			}
			if(this.commonService.checkIsNumeric(this.editData.cardLimitAmount)){
				this.editCardLimitAmount = this.editData.cardLimitAmount
				this.formatcardLimitAmount();
			}
			if(this.commonService.checkIsNumeric(this.editData.tradeDiscountPercent)){
				this.editTradeDiscountPercent = this.editData.tradeDiscountPercent
				this.formatTradeDiscountPercent();
			}
			if(this.editData.minimumCharge==0){
				this.mintextDisabled = true;
				this.vendorMaintenance.minimumChargeAmount='';
			}
			if(this.editData.creditLimit==0 || this.editData.creditLimit==1){
				this.credittextDisabled = true;
				this.vendorMaintenance.cardLimitAmount='';
			}
			
			// fetch vendor records
		// this.vendorOptionsMaintenanceService.searchVendors().subscribe(pagedData => {
		// });
            // this.vendors = pagedData.records;
					// Searchable Select Box Step3
		// 	 for(var i=0;i< pagedData.records.length;i++)
        //       {
        //         this.ArrVendorId.push({ "id": pagedData.records[i].venderId, "itemName": pagedData.records[i].venderNamePrimary})
        //       }
		// 	    // Searchable Select Box Step3 End
			
		// 	 var selectedvendor = this.ArrVendorId.find(x => x.id ==  data.result.vendorId);
	    //      this.vendorId.push({'id':selectedvendor.id,'itemName':selectedvendor.itemName});
        // });

		// 	if(data.result.paymentTermId)
		//    {
			var	selectedPaymentTermId= this.ArrPaymentTermId.find(x => x.id ==  data.result.paymentTermId);
			this.paymentTermId.push({'id':selectedPaymentTermId.id,'itemName':selectedPaymentTermId.itemName});
		//    }

			if(this.editData){
				this.condition=true;
				this.myHtml = "border-vanish";
				// this.vendorOptionsMaintenanceService.getVendorByID(this.editData.vendorId).then(data => {
				// 	this.records = data.result;
				// 	this.simpleName = data.result.venderNamePrimary;
				// 	this.arabicName = data.result.venderNameSecondary;			
				// });
				if(this.editData.openMaintenanceHistoryCalendarYear==1){
				var histCal = true;
			}else{
				var histCal = false;
			}
			if(this.editData.openMaintenanceHistoryTransaction==1){
				var histTrans = true;
			}else{
				var histTrans = false;
			}
			if(this.editData.openMaintenanceHistoryFiscalYear==1){
				var histFisc = true;
			}else{
				var histFisc = false;
			}
			if(this.editData.openMaintenanceHistoryDistribution==1){
				var histDis = true;
			}else{
				var histDis = false;
			}
			this.vendorMaintenance=
				{
					'vendorId' : this.editData.vendorId,
					'maximumInvoiceAmount' :this.editData.maximumInvoiceAmount.toString(),
					'maximumInvoiceAmountValue' : this.editData.maximumInvoiceAmountValue,
					'minimumCharge' : this.editData.minimumCharge.toString(),
					'minimumChargeAmount' : this.editData.minimumChargeAmount,
					'creditLimit' : this.editData.creditLimit.toString(),
					'cardLimitAmount' : this.editData.cardLimitAmount,
					'tradeDiscountPercent' : this.editData.tradeDiscountPercent,
					'minimumOrderAmount' : this.editData.minimumOrderAmount,
					'shipmentMethodId' : this.editData.shipmentMethodId,
					'vatScheduleId' : this.editData.vatScheduleId,
					'currencyId' : this.editData.currencyId,
					'checkBookId' : this.editData.checkBookId,
					'paymentTermId' : this.editData.paymentTermId,
					'userDefine1' : this.editData.userDefine1,
					'userDefine2' : this.editData.userDefine2,
					'userDefine3' : this.editData.userDefine3,
					'openMaintenanceHistoryCalendarYear' : histCal,
					'openMaintenanceHistoryFiscalYear' : histFisc,
					'openMaintenanceHistoryTransaction' : histTrans,
					'openMaintenanceHistoryDistribution' : histDis
				};
			
		}	
			}		
        });
	}
	 // Serachable Select Box Step4
	getShipmentMethodByID(item:any){
		this.shipmentMethodId=[];
		this.shipmentMethodId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("shipmentMethodId")
	}
	OnShipmentMethodIdDeSelect(item:any){
		this.shipmentMethodId=[];
		this.commonService.closeMultiselectWithId("shipmentMethodId")
	}

		getVatScheduleByID(item:any){
		this.vatScheduleId=[];
		this.vatScheduleId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("vatScheduleId")
	}
	OnVatScheduleIdDeSelect(item:any){
		this.vatScheduleId=[];
		this.commonService.closeMultiselectWithId("vatScheduleId")
	}

	getCurrencyByID(item:any){
		
		this.currencyId=[];
		this.currencyId.push({'id':item.id,'itemName':item.itemName});
		this.selectedCurrency = item.id
		this.commonService.closeMultiselectWithId("currencyId")
		if(this.vendorMaintenance.maximumInvoiceAmountValue){
			this.formatMaximumInvoiceAmountValue()
		}
		if(this.vendorMaintenance.minimumChargeAmount){
			this.formatminimumChargeAmount()
		}
		if(this.vendorMaintenance.cardLimitAmount){
			this.formatcardLimitAmount()
		}
		if(this.vendorMaintenance.tradeDiscountPercent){
			this.formatTradeDiscountPercent()
		}
		// if(this.vendorMaintenance.minimumChargeAmount){
		// 	this.formatminimumChargeAmount()
		// }
		// if(this.vendorMaintenance.creditLimitAmount){
		// 	this.formatCreditLimitAmount()
        // }
        // if(this.vendorMaintenance.tradeDiscountPercent){
		// 	this.formatTradeDiscountPercent()
        // }
	}
	OnCurrencyIdDeSelect(item:any){
		this.currencyId=[];
		this.commonService.closeMultiselectWithId("currencyId")
		this.formatMaximumInvoiceAmountValueChanged();
		this.formatminimumChargeAmountChanged();
		this.formatTradeDiscountPercentChanged();
		this.formatcardLimitAmountChanged();
	}

	getCheckBookByID(item:any){
		this.checkBookId=[];
		this.checkBookId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("checkBookId")
	}
	OnCheckBookIdDeSelect(item:any){
		this.checkBookId=[];
		this.commonService.closeMultiselectWithId("checkBookId")
	}

	getPaymentTermByID(item:any){
		this.paymentTermId=[];
		this.paymentTermId.push({'id':item.id,'itemName':item.itemName});
		this.commonService.closeMultiselectWithId("paymentTermId")
	}
	OnPaymentTermIdDeSelect(item:any){
		this.paymentTermId=[];
		this.commonService.closeMultiselectWithId("paymentTermId")
	}
	// Serachable Select Box Step4 ends

			   // Serachable Select Box Step4
	getVendorByID(item:any){
		
	//    this.vendorId=[];
    //    this.vendorId.push({'id':item.id,'itemName':item.id});
	   this.vendorOptionsMaintenanceService.getVendorByID(item.id).then(pagedData => {
			this.simpleName = pagedData.result.venderNamePrimary;
			this.arabicName = pagedData.result.venderNameSecondary;
		});
    }

	getVendorData(currentVendorId){
		
	   this.vendorOptionsMaintenanceService.getData(this.currentVendorId).then(pagedData => {
		   this.vendorMaintenance.maximumInvoiceAmountValue = pagedData.result.maximumInvoiceAmountValue;
		   this.vendorMaintenance.minimumChargeAmount = pagedData.result.minimumChargeAmount;
		   this.vendorMaintenance.cardLimitAmount = pagedData.result.cardLimitAmount;
			this.vendorMaintenance.tradeDiscountPercent = pagedData.result.tradeDiscountPercent;
			this.vendorMaintenance.minimumOrderAmount = pagedData.result.minimumOrderAmount;
			this.vendorMaintenance.shipmentMethodId = pagedData.result.shipmentMethodId[0].id;
			this.vendorMaintenance.vatScheduleId = pagedData.result.vatScheduleId[0].id;
		    this.vendorMaintenance.userDefine1 = pagedData.result.userDefine1;
			 this.vendorMaintenance.userDefine2= pagedData.result.userDefine2;
			 this.vendorMaintenance.userDefine3= pagedData.result.userDefine3;
             this.vendorMaintenance.currencyId= pagedData.result.currencyId[0].id;
			 this.vendorMaintenance.checkBookId= pagedData.result.checkBookId[0].id;
			 this.vendorMaintenance.paymentTermId= pagedData.result.paymentTermId[0].id;
			 this.vendorMaintenance.openMaintenanceHistoryCalendarYear = pagedData.result.openMaintenanceHistoryCalendarYear;
			 this.vendorMaintenance.openMaintenanceHistoryFiscalYear= pagedData.result.openMaintenanceHistoryFiscalYear;
			 this.vendorMaintenance.openMaintenanceHistoryTransaction= pagedData.result.openMaintenanceHistoryTransaction;
			 this.vendorMaintenance.openMaintenanceHistoryDistribution= pagedData.result.openMaintenanceHistoryDistribution;
		});
    }
	formatMaximumInvoiceAmountValue(){
		this.commonService.changeInputText("maximumInvoiceAmountValue")
        var inputValue 
        if(this.editData){
            if(this.editMaximumInvoiceAmountValue == 'undefined' && this.unFrmtMaximumInvoiceAmountValue){
                inputValue = this.unFrmtMaximumInvoiceAmountValue
            }
           else if(this.editMaximumInvoiceAmountValue == 'undefined'){
                inputValue = this.editData.maximumInvoiceAmountValue
            }else{
                inputValue = this.editMaximumInvoiceAmountValue
            }
			this.unFrmtMaximumInvoiceAmountValue = inputValue
		}else{
			 inputValue = this.unFrmtMaximumInvoiceAmountValue
		}
        
        
		if (this.selectedCurrency !=undefined ){
            let submitInfo = {
				'currencyId':this.selectedCurrency
			};
			this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
				this.vendorMaintenance.maximumInvoiceAmountValue = this.commonService.formatAmount(data, inputValue)
			});
			
			
		}else{
			this.formatMaximumInvoiceAmountValueChanged()
		}
		
	
	}
	formatMaximumInvoiceAmountValueChanged(){
		this.commonService.changeInputNumber("maximumInvoiceAmountValue")
		this.vendorMaintenance.maximumInvoiceAmountValue = this.unFrmtMaximumInvoiceAmountValue
	}
	getMaximumInvoiceAmountValue(event){
		this.editMaximumInvoiceAmountValue = 'undefined'
        if(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
			}
			this.unFrmtMaximumInvoiceAmountValue = event
        }else{
			this.unFrmtMaximumInvoiceAmountValue = this.editData.maximumInvoiceAmountValue
		}
        
		
	}
	formatminimumChargeAmount(){
		this.commonService.changeInputText("minimumChargeAmount")
        var inputValue 
        if(this.editData){
            if(this.editMinimumChargeAmount == 'undefined' && this.unFrmtMinimumChargeAmount){
                inputValue = this.unFrmtMinimumChargeAmount
            }
           else if(this.editMinimumChargeAmount == 'undefined'){
                inputValue = this.editData.minimumChargeAmount
            }else{
                inputValue = this.editMinimumChargeAmount
            }
			this.unFrmtMinimumChargeAmount = inputValue
		}else{
			 inputValue = this.unFrmtMinimumChargeAmount
		}
        
        
		if (this.selectedCurrency !=undefined ){
            let submitInfo = {
				'currencyId':this.selectedCurrency
			};
			this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
				this.vendorMaintenance.minimumChargeAmount = this.commonService.formatAmount(data, inputValue)
			});
			
			
		}else{
			this.formatminimumChargeAmountChanged()
		}
		
	
	}
	formatminimumChargeAmountChanged(){
		this.commonService.changeInputNumber("minimumChargeAmount")
		this.vendorMaintenance.minimumChargeAmount = this.unFrmtMinimumChargeAmount
	}
	getminimumChargeAmountValue(event){
		this.editMinimumChargeAmount = 'undefined'
        if(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
			}
			this.unFrmtMinimumChargeAmount = event
        }else{
			this.unFrmtMinimumChargeAmount = this.editData.minimumChargeAmount
		}
        
		
	}
	formatcardLimitAmount(){
		
		this.commonService.changeInputText("minimumChargeAmount")
        var inputValue 
        if(this.editData){
            if(this.editCardLimitAmount == 'undefined' && this.unFrmtCardLimitAmount){
                inputValue = this.unFrmtCardLimitAmount
            }
           else if(this.editCardLimitAmount == 'undefined'){
                inputValue = this.editData.cardLimitAmount
            }else{
                inputValue = this.editCardLimitAmount
            }
			this.unFrmtCardLimitAmount = inputValue
		}else{
			 inputValue = this.unFrmtCardLimitAmount
		}
        if (this.selectedCurrency !=undefined ){
            let submitInfo = {
				'currencyId':this.selectedCurrency
			};
			this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
				this.vendorMaintenance.cardLimitAmount = this.commonService.formatAmount(data, inputValue)
			});
			
			
		}else{
			this.formatcardLimitAmountChanged()
		}
		
	
	}
	formatcardLimitAmountChanged(){
		this.commonService.changeInputNumber("minimumChargeAmount")
		this.vendorMaintenance.cardLimitAmount = this.unFrmtCardLimitAmount
	}
	getcardLimitAmountValue(event){
		this.editCardLimitAmount = 'undefined'
        if(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
			}
			this.unFrmtCardLimitAmount = event
        }else{
			this.unFrmtCardLimitAmount = this.editData.cardLimitAmount
		}
        
		
	}
	formatTradeDiscountPercent(){
		this.commonService.changeInputText("tradeDiscountPercent")
        var inputValue 
        if(this.editData){
            if(this.editTradeDiscountPercent == 'undefined' && this.unFrmtTradeDiscountPercent){
                inputValue = this.unFrmtTradeDiscountPercent
            }
           else if(this.editTradeDiscountPercent == 'undefined'){
                inputValue = this.vendorMaintenance.tradeDiscountPercent
            }else{
                inputValue = this.editTradeDiscountPercent
            }
			this.unFrmtTradeDiscountPercent = inputValue
		}else{
			 inputValue = this.unFrmtTradeDiscountPercent
		}
        
        
		if (this.selectedCurrency !=undefined ){
            let submitInfo = {
				'currencyId':this.selectedCurrency
			};
			this.commonService.getCurrencySetupDetail(submitInfo).then(data => {
				this.vendorMaintenance.tradeDiscountPercent = this.commonService.formatAmount(data, inputValue)
			});
			
			
		}else{
			this.formatTradeDiscountPercentChanged()
		}
		
	
	}
	formatTradeDiscountPercentChanged(){
		this.commonService.changeInputNumber("tradeDiscountPercent")
		this.vendorMaintenance.tradeDiscountPercent = this.unFrmtTradeDiscountPercent
	}
	getTradeDiscountPercent(event){
		this.editTradeDiscountPercent = 'undefined'
        if(event){
            var amt = event.split(".")
            if(amt.length == 1 && amt[0].length >= 6){
                event = amt[0].slice(0, 6)
            }
            else{
                if(amt.length > 1 && amt[1].length >= 3){
                    event = amt[0]+"."+amt[1].slice(0, 3)
                }
                if(amt.length > 1 && amt[0].length >= 6){
                    event = amt[0].slice(0, 6)+"."+amt[1]
                }
			}
			this.unFrmtTradeDiscountPercent = event
        }else{
			this.unFrmtTradeDiscountPercent = this.editData.tradeDiscountPercent
		}
        
		
	}
	// OnVendorIdDeSelect(item:any){
       
	//    this.simpleName ='';
	//    this.arabicName ='';
    // }
	// Serachable Select Box Step4 End 
	 redirect(currentVendorId){
		this.router.navigate(['masterdata/vendoraccountmaintenance',this.currentVendorId]);
	}
    
	redirect1(){
		this.router.navigate(['masterdata/vendormaintenance']);
	}
	save(vendorMaintenanceOptions: NgForm){
		if(this.vendorMaintenance.openMaintenanceHistoryCalendarYear){
			var calenderYear = '1';
		}else{
			var calenderYear = '0';
		}
		
		if(this.vendorMaintenance.openMaintenanceHistoryTransaction){
			var transaction = '1';
		}else{
			var transaction = '0';
		}
		
		if(this.vendorMaintenance.openMaintenanceHistoryFiscalYear){
			var fisYear = '1';
		}else{
			var fisYear = '0';
		}
		
		if(this.vendorMaintenance.openMaintenanceHistoryDistribution){
			var distribution = '1';
		}else{
			var distribution = '0';
		}
		let submittedData=
		{
			'vendorId' : this.currentVendorId,
			'maximumInvoiceAmount' : this.vendorMaintenance.maximumInvoiceAmount,
			'maximumInvoiceAmountValue' : parseInt(this.unFrmtMaximumInvoiceAmountValue),
			'minimumCharge' : this.vendorMaintenance.minimumCharge,
			'minimumChargeAmount' : this.unFrmtMinimumChargeAmount,
			'creditLimit' : this.vendorMaintenance.creditLimit,
			'cardLimitAmount' : this.unFrmtCardLimitAmount,
			'tradeDiscountPercent' : this.vendorMaintenance.tradeDiscountPercent,
			'minimumOrderAmount' : this.vendorMaintenance.minimumOrderAmount,
			'shipmentMethodId' : this.shipmentMethodId[0].id,
			'vatScheduleId' : this.vatScheduleId[0].id,
			'currencyId' : this.currencyId[0].id,
			'checkBookId' : this.checkBookId[0].id,
			'paymentTermId' : this.paymentTermId[0].id,
			'userDefine1' : this.vendorMaintenance.userDefine1,
			'userDefine2' : this.vendorMaintenance.userDefine2,
			'userDefine3' : this.vendorMaintenance.userDefine3,
			'openMaintenanceHistoryCalendarYear' : calenderYear,
			'openMaintenanceHistoryFiscalYear' : fisYear,
			'openMaintenanceHistoryTransaction' : transaction,
			'openMaintenanceHistoryDistribution' : distribution
		};
		this.vendorOptionsMaintenanceService.create(submittedData).then(data => {
			// this.shipmentMethodId=[];
			var datacode = data.code;
			if (datacode == 200) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				this.condition=true;
				this.myHtml = "border-vanish";
			    //vendorMaintenanceOptions.resetForm();
				//this.setPage({ offset: 0 });
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
				
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
	   });
		
		
	}
	getNames(){
		this.vendorOptionsMaintenanceService.getVendorByID(this.currentVendorId).then(data => {
			this.records = data.result;
			this.simpleName = data.result.venderNamePrimary;
			this.arabicName = data.result.venderNameSecondary;
			
		});
	}
	set(value){
		if(value=="none"){
			this.mtextDisabled = true;
			this.vendorMaintenance.maximumInvoiceAmountValue='';
		}else if(value=="amount"){
			this.mtextDisabled = false;
		}
	}
	set1(value){
		if(value=="none"){
			this.mintextDisabled = true;
			this.vendorMaintenance.minimumChargeAmount='';
		}else if(value=="percent"){
			this.mintextDisabled = false;
		}else if(value=="amount"){
			this.mintextDisabled = false;
		}
	}
	set2(value){
		if(value=="none"){
			this.credittextDisabled = true;
			this.vendorMaintenance.cardLimitAmount='';
		}else if(value=="unlimited"){
			this.credittextDisabled = true;
			this.vendorMaintenance.cardLimitAmount='';
		}else if(value=="amount"){
			this.credittextDisabled = false;
		}
	}
	ClearForm(vendorMaintenanceOptions:NgForm){
	      //vendorMaintenanceOptions.resetForm();
		  
		  this.vendorMaintenance.maximumInvoiceAmount='';
		  this.vendorMaintenance.minimumCharge='';
		  this.vendorMaintenance.creditLimit='';
		  this.vendorMaintenance.minimumCharge='';
		  this.vendorMaintenance.creditLimit='';
		  this.vendorMaintenance.maximumInvoiceAmount='';
		  this.vendorMaintenance.minimumCharge='';
		  this.vendorMaintenance.creditLimit='';
		  this.vendorMaintenance.maximumInvoiceAmountValue ='';
		  this.shipmentMethodId=[];
		  this.vatScheduleId=[];
		  this.currencyId=[];
		  this.checkBookId=[];
		  this.paymentTermId=[];
		  this.vendorMaintenance.minimumChargeAmount='';
		  this.vendorMaintenance.cardLimitAmount='';
		  this.vendorMaintenance.tradeDiscountPercent='';
		  this.vendorMaintenance.minimumOrderAmount='';
		  this.vendorMaintenance.userDefine1='';
		  this.vendorMaintenance.userDefine2='';
		  this.vendorMaintenance.userDefine3='';
		  this.vendorMaintenance.openMaintenanceHistoryCalendarYear=false;
		  this.vendorMaintenance.openMaintenanceHistoryFiscalYear=false;
		  this.vendorMaintenance.openMaintenanceHistoryTransaction=false;
		  this.vendorMaintenance.openMaintenanceHistoryDistribution=false;
		  this.editMaximumInvoiceAmountValue = ''
		  this.editMinimumChargeAmount
		  this.editCardLimitAmount = ''
		  this.editTradeDiscountPercent = ''
		  this.selectedCurrency = undefined
		  this.unFrmtCardLimitAmount =''
		  this.unFrmtMaximumInvoiceAmountValue=''
		  this.unFrmtMinimumChargeAmount = ''
		  this.unFrmtTradeDiscountPercent = ''
		  var selectedValue = <HTMLInputElement>document.getElementById("ddl_insClassId")
		  selectedValue.value=this.editData.vendorId;
		  this.vendorMaintenance.vendorId = this.editData.vendorId;
		  this.vendorOptionsMaintenanceService.getVendorByID(this.editData.vendorId).then(data => {
			this.records = data.result;
			this.simpleName = data.result.venderNamePrimary;
			this.arabicName = data.result.venderNameSecondary;			
		});
				
			 if(this.condition){
				this.myHtml = "border-vanish";
			 }else{
				this.myHtml = "";
			 }
	}
	ngOnInit() {

		
			//	this.getVendorData(this.currentVendorId);

			// Serachable Select Box Step2
		   this.ddlShipmentMethodSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		   this.ddlVatScheduleSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         }; 

		 	this.ddlCurrencyIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };

		   this.ddlCheckBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };

		 	this.ddlPaymentTermIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
         };
		 // Serachable Select Box Step2 ends

		 	 // Serachable Select Box Step2
		//    this.ddlVendorIdSetting = { 
        //    singleSelection: true, 
        //    text:this.select,
        //    enableSearchFilter: true,
        //    classes:"myclass custom-class"
        //  }; 
		 // Serachable Select Box Step2
	}	

	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
	}
	
		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}
	
	
}