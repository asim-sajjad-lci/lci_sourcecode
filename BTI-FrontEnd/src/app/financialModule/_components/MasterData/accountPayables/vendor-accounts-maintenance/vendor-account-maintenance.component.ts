import { Component,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import {VendorAccountMaintenance} from '../../../../../financialModule/_models/master-data/accountPayables/vendor-account-maintenance';
import {VendorAccountMaintenanceService} from '../../../../../financialModule/_services/MasterData/accountPayables/vendor-account-maintenance.service';
import { AccountStructureService } from '../../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { GetScreenDetailService } from '../../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../../_sharedresource/page';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 

@Component({
   selector: 'account-group-setup',
   templateUrl: './vendor-account-maintenance.component.html',
    providers: [VendorAccountMaintenanceService,AccountStructureService],
})

export class VendorAccountMaintenanceComponent {
    page = new Page();
    rows = new Array<VendorAccountMaintenance>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
	moduleName;
	isScreenLock;
    defaultAddFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
	currentVendorId:string;
	vendorId:string;
    select=Constants.select;
    atATimeText=Constants.atATimeText;
	btnCancelText=Constants.btnCancelText;
	sameAccountNumberMsg=Constants.sameAccountNumberMsg;
    isModify:boolean=false;
    venderNamePrimary:string;
    venderNameSecondary:string;
    VendorOptions;
    accountNumberList =[];
    AccountTypeList=[];
    fetchedData=[];
	arrSegment= [];
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private VendorAccountMaintenanceService: VendorAccountMaintenanceService,
		private AccountStructureService:AccountStructureService,
        private getScreenDetailService: GetScreenDetailService,
		public toastr: ToastsManager,
		vcr: ViewContainerRef,
        ){
			this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
         this.route.params.subscribe((params: Params) => {
			 
                this.currentVendorId = params['vendorId'];
				});
        this.getVendorByID(this.currentVendorId);
		this.vendorId='';
        this.accountNumberList=[];
		this.getSegmentCount();
        this.getAccountType();
        // this. getVendordata();
        this.getAddScreenDetail();
        this.getAccountMaintainanceData();
    }

 
    getAddScreenDetail()
    {
         this.screenCode = "S-1212";
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_VEN_ACC_MAIN_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_VEN_ACC_MAIN_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_VEN_ACC_MAIN_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_VEN_ACC_MAIN_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_VEN_ACC_MAIN_ACC_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_VEN_ACC_MAIN_ACC_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_VEN_ACC_MAIN_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_VEN_ACC_MAIN_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            ];
         this.getScreenDetail(this.screenCode);
    }

  
  
    getScreenDetail(screenCode)
    {
		this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
			{
				this.isScreenLock = res;
			});
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
         
        });
    }
	getSegmentCount()
	{
		 this.AccountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}
    getAccountType()
    {
		 this.accountNumberList=[];
        this.VendorAccountMaintenanceService.getAccountType().then(data => {
            this.AccountTypeList = data.result;
			for(let i=0; i<this.AccountTypeList.length; i++){
					let temp={
					 "accountType": this.AccountTypeList[i]["typeId"],
					 "typeValue": this.AccountTypeList[i]["typeValue"],
					 "accountDescription":[],
					 "accountNumberIndex":[]
					}
    			this.accountNumberList.push(temp);
			}
        });
    }
    // getVendordata()
    // {
    //     this.VendorAccountMaintenanceService.getVendorIdList().then(data => {
            
    //         this.VendorOptions = data.result.records;
    //         this.VendorOptions.splice(0, 0, { "venderId": "", "venderNamePrimary": this.select });
    //         this.model.venderId='';
    //     });
    // }
    getAccountMaintainanceData()
    {
        this.VendorAccountMaintenanceService.getVendorAccountMaintenanceList(this.currentVendorId).then(data => {
            this.model=data.result;
			  if(data.status != 'NOT_FOUND')
			  {
				for(var i=0;i<this.accountNumberList.length;i++)
			    {
				  var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
				  if(currentRowSegment)
				  {
				  	for(var k=0;k< currentRowSegment.length;k++)
				  	{
				  	  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
					  if(segment)
					  {
					  	if(k > 0)
					  	{
					  		   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
					  		   segment.disabled= true;
					  	}
					  	else{
					  		  segment.disabled=false;
					  	}
    				   	segment.value = '';
					   }
					}
					this.accountNumberList[i]["accountDescription"] =[];
					this.accountNumberList[i]["accountNumberIndex"] =[];
				    this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = [];
				  }
			  
			  
				   for(var j =0;j<data.result.accountNumberList.length;j++)
				   {
					   if(this.accountNumberList[i]["accountType"] == data.result.accountNumberList[j]["accountType"])
					   {
						 this.accountNumberList[i]["accountDescription"] = data.result.accountNumberList[j]["accountDescription"].split(',');
						 this.accountNumberList[i]["accountNumberIndex"] = data.result.accountNumberList[j]["accountNumberIndex"];
						 this.accountNumberList[i]["accountNumberIndex"]["arrSegment"] = data.result.accountNumberList[j]["accountNumber"].split('-');
						 var currentRowSegment = this.accountNumberList[i]["accountNumberIndex"]["arrSegment"];
					 
						 for(var k=0;k<currentRowSegment.length;k++)
						 {
							  var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+k);
							  if(k > 0)
							  {
								   segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+k);
							  }
							  segment.disabled=false;
							  if(currentRowSegment[k] == 0)
							  {
								  segment.value = '';
							  }	
							  else{
								segment.value = currentRowSegment[k];
							  }
						 }
					   }
				   }
			   }		
			  }
			
			});
    }
    saveAccountMaintenance(f: NgForm)
    {
		
		    var vendorId='';
            var accountNumberIndexArr=[];
              var tempAccountNumberList=[];
           	
		for(let i=0;i<this.accountNumberList.length;i++){
				if(this.accountNumberList[i]["accountNumberIndex"])
				{
					var accountDesc = this.accountNumberList[i]["accountDescription"].join();
					accountDesc=accountDesc.replace(/,/g , " ");
					accountDesc=accountDesc.replace("0"," ");
					accountDesc=accountDesc.replace(0," ");
					if(this.accountNumberList[i]["accountNumberIndex"].length > 0)
					{
						tempAccountNumberList.push({
						"accountType":this.accountNumberList[i]["accountType"],
						"accountDescription": accountDesc,
						"accountNumberIndex":this.accountNumberList[i]["accountNumberIndex"]
					});	
					}
				}
		     }
			 let temp = {
					 "vendorId":this.currentVendorId,
					 "accountNumberList": tempAccountNumberList,
				}
          
             this.VendorAccountMaintenanceService.saveVendorAccountMaintenanace(temp).then(data => {
				 
                window.scrollTo(0,0);
                var datacode = data.code;
                if (datacode == 201) {
                     
                    this.isModify=false;

                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        // f.resetForm();
                        window.setTimeout(() => {
                         this.showMsg = false;
                         this.hasMsg = false;
                    }, 4000);
                }
              
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            }); 
    }  

    // getVendorByID(evt)
    // {
    //     this.VendorAccountMaintenanceService.getVendorByID(evt.target.value).then(data => {
	// 		this.venderNamePrimary = data.result.venderNamePrimary;
	// 		this.venderNameSecondary = data.result.venderNameSecondary;
	// 	});
    // }

	 getVendorByID(currentVendorId)
    {
        this.VendorAccountMaintenanceService.getVendorByID(this.currentVendorId).then(data => {
			//this.model = data.result;
			this.vendorId= this.currentVendorId;
			// this.vendorId=data.result.vendorId;
			this.venderNamePrimary = data.result.venderNamePrimary;
			this.venderNameSecondary = data.result.venderNameSecondary;
		});
    }

	    fetchFirstAccountIndexInfo(i,segmentIndex){
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+segmentIndex);
		var segmentNumber=segment.value;

		var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));

		if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		
		if(segmentNumber)
		{
			this.VendorAccountMaintenanceService.firstTextApi(segmentNumber).subscribe(pagedData => {
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberList[i].accountNumberIndex[segmentIndex])
					{
						this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountNumberList[i].accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountNumberList[i]["accountDescription"].push(result.mainAccountDescription);
						this.accountNumberList[i]["accountNumberIndex"].push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
						this.accountNumberList[i].accountNumberIndex[m]='0';
						this.accountNumberList[i].accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
						}
						if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountNumberList[i]["accountDescription"] = [];
					this.accountNumberList[i]["accountNumberIndex"] = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
					{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
						if(m == 0)
						{
							segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ i+m);
						}
						segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
			this.accountNumberList[i]["accountDescription"] = [];
			this.accountNumberList[i]["accountNumberIndex"] = [];

			var currentSegment=segmentIndex;
			for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
			{
				var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+m);
				segment.value = '';
				segment.disabled = true;
			}
		}
	}
	fetchOtherAccountIndexInfo(i,segmentIndex){
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
		var segmentNumber=segment.value;

		var nextSegment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+ (parseInt(segmentIndex) + 1));
		if(this.accountNumberList[i].accountNumberIndex[segmentIndex] && this.accountNumberList[i].accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberList[i].accountNumberIndex.indexOf(segment.value);
		if(acctIndex > -1)
		{
			this.toastr.warning(this.sameAccountNumberMsg);
			segment.value = '';
		}
		else{
			if(segmentNumber)
			{
				this.VendorAccountMaintenanceService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
					let status = pagedData.status;
    				let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberList[i].accountNumberIndex[segmentIndex] || this.accountNumberList[i].accountNumberIndex[segmentIndex] == '0')
						{
							this.accountNumberList[i].accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountNumberList[i].accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountNumberList[i]["accountDescription"].push(result.dimensionDescription);
							this.accountNumberList[i]["accountNumberIndex"].push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+ i+segmentIndex);
						segment.value = '';
						this.accountNumberList[i].accountNumberIndex[segmentIndex]='0';
						this.accountNumberList[i].accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberList[i].accountNumberIndex[currentSegment]='0';
				this.accountNumberList[i].accountDescription[currentSegment]='0';
			}
		}
	}


  
	redirect1(){
		this.router.navigate(['masterdata/vendormaintenance']);
	}

	resetMe()
	{
		window.scrollTo(0,0);
		this. getAccountType();
	}

		/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}

    }