import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { VendorMaintenance } from '../../../../financialModule/_models/master-data/accountPayables/vendorMaintenance';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import {masterAccountPayableService} from '../../../../financialModule/_services/MasterData/accountPayables/masterAccountPayables.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
@Component({
   selector: 'vendor-maintenance',
   templateUrl: './vendor-maintenance.component.html', 
    providers: [masterAccountPayableService,CommonService],
})

export class VendorMaintenanceComponent implements OnInit {
	countries;
	states;
	cities;
	customerId;
	records;
	editCountries;
	editStates;
	editCities;
	screenCode;
	isScreenLock;
	vendorId = {};
	currentVendorId:string;
	moduleCode = Constants.financialModuleCode;
	defaultFormValues: [object];
	gridDefaultFormValues: [object];
    availableFormValues: [object];
    gridAvailableFormValues: [object];
	select=Constants.select; 
	moduleName;
    screenName;
	formCountries;
	formStates;
	formCities;
	vendorClass;
	myHtml;
	// Serachable Select Box Step1
	ArrClassId = [];
    classId : any = [];
    ddlClassIdSetting = {};
    // Serachable Select Box Step1 ends
	page = new Page();
	rows = new Array<VendorMaintenance>();
    temp = new Array<VendorMaintenance>();
	Searchselected = [];
	searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    selectedText=Constants.selectedText;
    totalText=Constants.totalText;
    deleteConfirmationText = Constants.deleteConfirmationText;
	tableViewtext=Constants.tableViewtext;
    selectedClass:any;
	btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
	add:boolean=true;
	EmptyMessage = Constants.EmptyMessage;
	search=Constants.search;
    
	btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;
	messageText;
    message = { 'type': '', 'text': '' };
    hasMessage;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg = false;
    isfailureMsg = false;
	//add;
	disableId;
	disableButton;
	vendorMaintenance = {
    'hold': false,
    'vendorID': '',
    'status': '',
    'firstName': '',
    'ArabicName': '',
    'ShortName': '',
    'classID': '',
    'StatementName': '',
    'adress1': '',
    'adress2': '',
    'adress3': '',
    'phone1': '',
    'phone2': '',
    'phone3': '',
    'fax': '',
    'userDefine1': '',
    'userDefine2': ''
  };
	constructor(private router: Router,private route: ActivatedRoute,private commonService:CommonService,private masterAccountPayableService: masterAccountPayableService,private getScreenDetailService: GetScreenDetailService) {
		//this.add = true;
		this.disableButton = true;
		let vm = this;
		this.countries =[];
		this.states =[];
		this.cities =[];
		this.page.pageNumber = 0;
        this.page.size = 5;
		this.formCountries='';
		this.formStates='';
		this.formCities='';
		this.disableId = false;
		this.selectedClass=[{'id':'','itemName':''}];
		this.masterAccountPayableService.getCountryList()
			.then(data => {
				this.countries = data.result;
				this.countries.splice(0,1);
		});
		this.masterAccountPayableService.getVendorClass().subscribe(pagedData => {
            this.vendorClass = pagedData.records;
						                // Searchable Select Box Step3
                for(var i=0;i< pagedData.records.length;i++)
                {
                    this.ArrClassId.push({ "id": pagedData.records[i].vendorClassId, "itemName":pagedData.records[i].classDescription})
                }
			    // Searchable Select Box Step3 End
        });
		
		/* for grid binding */
		this.setPage({ offset: 0 });
			this.gridDefaultFormValues = [
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_VENDOR_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_HOLD', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_STATUS', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}, 
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_SHORT_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_CLASS_ID', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_STATEMENT_NAME', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_ADDRESS', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_PHONE1', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_PHONE2', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_PHONE3', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}, 
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_FAX', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_COUNTRY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_STATE', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_CITY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_EDIT', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_VIEW', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''},  
            { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''} ,
			{ 'fieldName': 'MANAGE_VENDOR_MAINTENANCE_PRIORITY', 'fieldValue': '', 'helpMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':''}
		];

		this.getScreenDetailService.ValidateScreen("S-1067").then(res=>
			{
				this.isScreenLock = res;
			});
		this.getScreenDetailService.getScreenDetailUser(this.moduleCode, "S-1067").then(data => {
		
			this.gridAvailableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.gridAvailableFormValues.length; j++) {
                var fieldKey = this.gridAvailableFormValues[j]['fieldName'];
                var objAvailable = this.gridAvailableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.gridDefaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
            }
        });
		/* for grid binding ends */
		
		vm.route.params.subscribe((params: Params) => 
		{
			vm.customerId = params['customerId'];
			if (vm.customerId != '' && vm.customerId != undefined) {
				
			}
			else {
				vm.screenCode = "S-1066";
				vm.defaultFormValues = [
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_VENDOR_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_HOLD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_STATUS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_ARABIC_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_SHORT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_STATEMENT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_ADDRESS1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_PHONE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_PHONE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_PHONE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_USER_DEFINE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_USER_DEFINE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_ADDRESS2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_ADDRESS3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_OPTIONS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				{ 'fieldName': 'ADD_VENDOR_MAINTENANCE_ACCOUNTS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
				];
				
				vm.getScreenDetailService.getScreenDetailUser(vm.moduleCode,vm.screenCode).then(data => {
				vm.moduleName = data.result.moduleName;
				vm.screenName = data.result.dtoScreenDetail.screenName;
				vm.availableFormValues = data.result.dtoScreenDetail.fieldList;
				for (var j = 0; j < vm.availableFormValues.length; j++) {
					var fieldKey = vm.availableFormValues[j]['fieldName'];
					var objAvailable = vm.availableFormValues.find(x => x['fieldName'] === fieldKey);
					var objDefault = vm.defaultFormValues.find(x => x['fieldName'] === fieldKey);
					objDefault['fieldValue'] = objAvailable['fieldValue'];
					objDefault['helpMessage'] = objAvailable['helpMessage'];
					objDefault['deleteAccess'] = objAvailable['deleteAccess'];
					objDefault['readAccess'] = objAvailable['readAccess'];
					objDefault['writeAccess'] = objAvailable['writeAccess'];
					objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
				}
			});
			}
		});
	}
	 //get id already exist
	 IsIdAlreadyExist(){
        if(this.vendorMaintenance.vendorID != '' && this.vendorMaintenance.vendorID != undefined && this.vendorMaintenance.vendorID != 'undefined')
        { 
			this.masterAccountPayableService.getMasterVendorMaintenance(this.vendorMaintenance.vendorID).then(data => {
		
                var datacode = data.code;
                if (data.status == "NOT_FOUND" || data.btiMessage.messageShort =='RECORD_NOT_FOUND') {
                    return true;
                }
                else{
                    var txtId = <HTMLInputElement> document.getElementById("txtvendorId");
                    txtId.focus();
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                window.scrollTo(0,0);
                this.vendorMaintenance.vendorID='';
                return false;
                }

                }).catch(error => {
                window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
        });
    }
}

	// Serachable Select Box Step4
    getClassByID(item:any){
		this.classId = [];
		this.classId.push({ "id": item.id, "itemName":item.itemName});
		this.commonService.closeMultiselectWithId('classId')
    }
    OnClassIdDeSelect(item:any){
		this.classId=[];
		this.commonService.closeMultiselectWithId('classId')
    }
	
	save(vendor: NgForm){
	
		if(this.classId.length == 0)
		{
			return false;
		}
		this.btndisabled=true;
		if(this.add){
			if(this.vendorMaintenance.hold){
				var hold = 1;
			}else{
				var hold = 2;
			}
			
			for(let i=0; i<this.countries.length; i++){
				if(this.formCountries == this.countries[i].countryId){
					var countryName = this.countries[i]["countryName"];
				}
			}
						  
			for(let i=0; i<this.states.length; i++){
				if(this.formStates == this.states[i].stateId){
					var stateName = this.states[i]["stateName"];
				}
			}
			
			for(let i=0; i<this.cities.length; i++){
				if(this.formCities == this.cities[i].cityId){
					var cityName = this.cities[i]["cityName"];
				}
			}

		let submittedData =
		{
			"venderId":this.vendorMaintenance.vendorID,
			"venderNamePrimary" : this.vendorMaintenance.firstName,
			"venderNameSecondary" : this.vendorMaintenance.ArabicName,
			"venderStatus" : this.vendorMaintenance.status,
			"shortName" : this.vendorMaintenance.ShortName,
			"classId": this.classId[0].id,
			"statementName" : this.vendorMaintenance.StatementName,
			"address1" : this.vendorMaintenance.adress1,
			"address2" : this.vendorMaintenance.adress2,
			"address3" : this.vendorMaintenance.adress3,
			"country" : this.formCountries,
			"state" : this.formStates,
			"city" : this.formCities,
			"phone1" :this.vendorMaintenance.phone1,
			"phone2" : this.vendorMaintenance.phone2,
			"phone3" : this.vendorMaintenance.phone3,
			"fax" : this.vendorMaintenance.fax,
			"userDefine1" : this.vendorMaintenance.userDefine1,
			"userDefine2" : this.vendorMaintenance.userDefine2,
			"vendorHold": hold 
		}
		
		this.masterAccountPayableService.createMasterVendorMaintenance(submittedData).then(data => {
			this.showBtns=false;
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				vendor.resetForm();
				this.messageText = data.btiMessage.message;
				this.setPage({ offset: 0 });
				this.formCountries='';
				this.formCities ='';
				this.formStates='';
				this.vendorMaintenance.status='';
				this.vendorMaintenance.classID='';
				this.classId=[];
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
			
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.setPage({ offset: 0 });
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
       });
		}else{
			if(this.vendorMaintenance.hold){
				var hold = 1;
			}else{
				var hold = 2;
			}
			let submittedData =
			{
			"venderId":this.vendorMaintenance.vendorID,
			"venderNamePrimary" : this.vendorMaintenance.firstName,
			"venderNameSecondary" : this.vendorMaintenance.ArabicName,
			"venderStatus" : this.vendorMaintenance.status,
			"shortName" : this.vendorMaintenance.ShortName,
			"classId": this.classId[0].id,
			"statementName" : this.vendorMaintenance.StatementName,
			"address1" : this.vendorMaintenance.adress1,
			"address2" : this.vendorMaintenance.adress2,
			"address3" : this.vendorMaintenance.adress3,
			"country" : this.formCountries,
			"state" : this.formStates,
			"city" : this.formCities,
			"phone1" :this.vendorMaintenance.phone1,
			"phone2" : this.vendorMaintenance.phone2,
			"phone3" : this.vendorMaintenance.phone3,
			"fax" : this.vendorMaintenance.fax,
			"userDefine1" : this.vendorMaintenance.userDefine1,
			"userDefine2" : this.vendorMaintenance.userDefine2,
			"vendorHold": hold 
		}
		this.masterAccountPayableService.editMasterVendorMaintenance(submittedData).then(data => {
			var datacode = data.code;
			this.btndisabled=false;
			if (datacode == 201) {
				this.hasMsg = true;
				this.isSuccessMsg = true;
				this.isfailureMsg = false;
				this.showMsg = true;
				this.messageText = data.btiMessage.message;
				this.showBtns=false;
				vendor.resetForm();
				this.disableId = false;
				this.disableButton = true;
				//this.add = true;
				this.setPage({ offset: 0 });
				this.formCountries='';
				this.formCities ='';
				this.formStates='';
				this.vendorMaintenance.status='';
				this.vendorMaintenance.classID='';
				this.classId=[];
				window.setTimeout(() => {
					this.showMsg = false;
					this.hasMsg = false;
				 }, 4000);
		 }else{
			  this.isSuccessMsg = false;
			  this.isfailureMsg = true;
			  this.showMsg = true;
			  this.hasMsg = true;
			  this.setPage({ offset: 0 });
			  this.messageText = data.btiMessage.message;
			   window.setTimeout(() => {
				 this.hasMsg = false;
				 this.showMsg = false;  
			   }, 4000);
		}
       });
		}
	}
	redirect(){
		this.router.navigate(['masterdata/vendoroptionmaintenance',this.currentVendorId]);
	}
	redirect1(){
		this.router.navigate(['masterdata/vendoraccountmaintenance',this.currentVendorId]);
	}
	
	ngOnInit() {
		// Serachable Select Box Step2
		   this.ddlClassIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
		   classes:"myclass custom-class",
		   searchPlaceholderText:this.search,
		   // Serachable Select Box Step2 ends
         }; 
	}	
	
	edit(row: any) {
		this.classId=[];
		
		//this.add=false;
		this.disableButton = false;
		this.disableId = true;
		this.showBtns=true;
		this.masterAccountPayableService.getMasterVendorMaintenance(row.venderId).then(data => {
			this.records = data.result;
	        this.currentVendorId = row.venderId;
			
			this.selectedClass = this.ArrClassId.find(x => x.id ==  data.result.classId);
			this.classId.push({ "id": data.result.classId, "itemName": this.selectedClass.itemName});
			this.setPage({ offset: 0 });
			let countryID = this.records.country;
			this.masterAccountPayableService.getStateList(countryID).then(data => {
				this.states = data.result;
			});
			
			let stateID = this.records.state;
			this.masterAccountPayableService.getCityList(stateID).then(data => {
				this.cities = data.result;
			});
			if(this.records.vendorHold=="1"){
				var hold = true;
			}else{
				var hold = false;
			}
			this.vendorMaintenance = {
				'hold': hold,
				'vendorID': this.records.venderId,
				'status': this.records.venderStatus,
				'firstName': this.records.venderNamePrimary,
				'ArabicName': this.records.venderNameSecondary,
				'ShortName': this.records.shortName,
				'classID': this.classId[0].id,
				'StatementName': this.records.statementName,
				'adress1': this.records.address1,
				'adress2': this.records.address2,
				'adress3': this.records.address3,
				'phone1': this.records.phone1,
				'phone2': this.records.phone2,
				'phone3': this.records.phone3,
				'fax': this.records.fax,
				'userDefine1': this.records.userDefine1,
				'userDefine2': this.records.userDefine2
			  };
			
			this.masterAccountPayableService.getCountryList()
			.then(data => {
				this.editCountries = data.result;
				this.editCountries.splice(0,1);
				//this.formCountries = {"countryId": "","countryName": "", "countryCode": "","shortName": ""}
				for(let i=0; i<this.editCountries.length; i++){
				if(this.records.country == this.editCountries[i].countryId){
					this.formCountries = this.editCountries[i]["countryId"];
				}
			}
			});
			  
			this.masterAccountPayableService.getStateList(this.records.country).then(data => {
				this.editStates = data.result;
				//this.formStates = {"stateId": '',"stateName": ""}
				for(let i=0; i<this.editStates.length; i++){
					if(this.records.state == this.editStates[i].stateId){
						this.formStates = this.editStates[i]["stateId"];
					}
				}
			});
			this.masterAccountPayableService.getCityList(this.records.state).then(data => {
				this.editCities = data.result;
				//this.formCities = {"cityId": '',"cityName": ""}
				for(let i=0; i<this.editCities.length; i++){
					if(this.records.city == this.editCities[i].cityId){
						this.formCities = this.editCities[i]["cityId"];
					}
				}
			})
			
		});
    }
	selected(event){
		let countryID = this.formCountries;
		this.masterAccountPayableService.getStateList(countryID).then(data => {
			this.states=[];
			this.cities=[];
			this.formStates='';
			this.formCities='';
			this.states = data.result;
        });
	}
	selectedState(event){
		let stateID = this.formStates;
		this.masterAccountPayableService.getCityList(stateID).then(data => {
			this.cities=[];
			this.cities = data.result;
        });
	}
	
	setPage(pageInfo) {
        this.Searchselected = [];
        this.page.pageNumber = pageInfo.offset;
        this.masterAccountPayableService.searchMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
	changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
	updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.masterAccountPayableService.searchMaintenance(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
	clear(vendor:NgForm){
		vendor.resetForm();
		this.classId=[];
		var selectedValue = <HTMLInputElement>document.getElementById("txtvendorId")
        selectedValue.value=this.records.venderId;
		this.vendorMaintenance.vendorID=this.records.venderId;
		if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
		 }
	}

	Cancel(vendor)
    {
    this.add=true;
    this.clear(vendor);
    this.vendorMaintenance.vendorID='';
	this.disableId=false;
	this.disableButton=true;
	}
	
	/** If Screen is Lock then prevent user to perform any action.
		*  This function also cover Role Management Write acceess functionality */
		LockScreen(writeAccess)
		{
			if(!writeAccess)
			{
				return true
			}
			else if(this.btndisabled)
			{
				return true
			}
			else if(this.isScreenLock)
			{
				return true;
			}
			else{
				return false;
			}
		}

	
	onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

	nameInputFormatter = (result: { countryName: string }) => result.countryName
	formatter = (result: { countryName: string }) => result.countryName
	Search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.countries.filter(v => v.countryName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
		
	nameInputFormatterState = (resultState: { stateName: string }) => resultState.stateName
	formatterState = (resultState: { stateName: string }) => resultState.stateName
	searchState = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.states.filter(v => v.stateName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
		
	nameInputFormatterCity = (resultCity: { cityName: string }) => resultCity.cityName
	formatterCity = (resultCity: { cityName: string }) => resultCity.cityName
	searchCity = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.cities.filter(v => v.cityName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
}