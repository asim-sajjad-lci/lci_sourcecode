import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { PaymentTermSetup } from '../../../../financialModule/_models/company-setup/payment-terms-setup';
import { PaymentTermSetupService } from '../../../../financialModule/_services/company-setup/payment-terms-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: '<user></user>',
    templateUrl: './payment-terms-Setup.component.html',
    styles: ["user.component.css"],
    providers:[PaymentTermSetupService]
})

//export to make it available for other classes
export class PaymentTermSetupComponent {
    page = new Page();
    rows = new Array<PaymentTermSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    ArrDiscountTypePeriod=[];
    ArrDiscountType=[];
    ArrDueType=[];
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    myHtml;
    add;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    isScreenLock;
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private paymentTermService:PaymentTermSetupService
        
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
    }

    ngOnInit() {
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.model.discountPeriodDays='0';
                this.model.dueDays='0';
                this.paymentTermService.getDiscountTypePeriod().then(data => {
                    this.ArrDiscountTypePeriod = data.result;
                    this.ArrDiscountTypePeriod.splice(0, 0, { "typeId": "", "typeValue": this.select });
                     this.model.discountPeriod='';
                 });

                this.paymentTermService.getDiscountType().then(data => {
                    this.ArrDiscountType = data.result;
                    this.ArrDiscountType.splice(0, 0, { "typeId": "", "typeValue": this.select });
                     this.model.discountType='';
                 });

                  this.paymentTermService.getDueType().then(data => {
                    this.ArrDueType = data.result;
                    this.ArrDueType.splice(0, 0, { "typeId": "", "typeValue": this.select });
                    this.model.dueType='';
                   
                 });
                this.setPage({ offset: 0 });
               
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1036";
         this.defaultAddFormValues = [
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_PAYMENT_TERM_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_DUE_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_CALC_DAYS_FROM', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_ADD_DAYS_DUE_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_DISCOUNT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_DISCOUNT_PERIOD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_DISCOUNT_TYPE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_ADD_DAYS_DISCOUNT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '' ,'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':''},
               { 'fieldName': 'ADD_PAYMENT_TERM_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1037";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_PAYMENT_TERM_SETUP_PAYMENT_TERM_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_PAYMENT_TERM_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_PAYMENT_TERM_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_PAYMENT_TERM_SETUP_CALCULATION_TYPE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_PAYMENT_TERM_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_PAYMENT_TERM_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_PAYMENT_TERM_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.paymentTermService.searchPaymentTermSetup(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

    resetMe(f)
    {
        this.model.description= '';
        this.model.arabicDescription= '';
        this.model.dueType='';
        this.model.dueDays='0';
        this.model.discountPeriod='';
        this.model.discountPeriodDays='0';
        this.model.discountType='';
        this.model.discountTypeValue='';
        
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.paymentTermId='';
		 }
    }

    Cancel(f)
      {
            this.isModify=false;
            this.model.paymentTermId= '';
            this.resetMe(f);
       }
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.paymentTermService.searchPaymentTermSetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
     
     changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
    getpaymentTermById(row: any)
    {
         this.isModify=true;
         this.showBtns=true;
         this.paymentTermService.getPaymentTermSetupById(row.paymentTermId).then(data => {
         this.model = data.result; 
         });
    }
//Is id already exist
IsIdAlreadyExist(){
if(this.model.paymentTermId != '' && this.model.paymentTermId != undefined && this.model.paymentTermId != 'undefined')
    {
        this.paymentTermService.getPaymentTermSetupById(this.model.paymentTermId).then(data => {
         var datacode = data.code;
          if (data.status == "NOT_FOUND") {
                return true;
            }
            else{
                var txtId = <HTMLInputElement> document.getElementById("paymentTextId");
                txtId.focus();
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                this.setPage({ offset: 0 });
                window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
            window.scrollTo(0,0);
            this.model.paymentTermId='';
            return false;
            }

            }).catch(error => {
            window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
    });
}
}
    savePaymentTerm(f: NgForm)
    {
        
        this.btndisabled=true;
        if(this.isModify)
        {
            this.paymentTermService.updatePaymentTermSetup(this.model).then(data => {
                window.scrollTo(0,0);
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 200) {
                      this.isModify=false;
                      this.model.paymentTermId=null;
                            this.hasMsg = true;
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.showBtns=false;
                         this.setPage({ offset: 0 });
                      
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                this.model.discountPeriodDays='0';
                this.model.dueDays='0';
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
            this.paymentTermService.createPaymentTermSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.showBtns=false;
                var datacode = data.code;
                this.btndisabled=false;
                  if (datacode == 201) {
                        this.hasMsg = true;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                        
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                this.model.discountPeriodDays='0';
                this.model.dueDays='0';
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        this.btndisabled=false;  
    }
    checkValue(value){
        if(value == 0){
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = "Please enter Discount Type greater than zero";
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
            this.model.discountTypeValue = '';
        }, 2000);
        }
    }

    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

    
/** If Screen is Lock then prevent user to perform any action.
*  This function also cover Role Management Write acceess functionality */
LockScreen(writeAccess)
{
    if(!writeAccess)
    {
        return true
    }
    else if(this.btndisabled)
    {
        return true
    }
    else if(this.isScreenLock)
    {
        return true;
    }
    else{
        return false;
    }
}
}