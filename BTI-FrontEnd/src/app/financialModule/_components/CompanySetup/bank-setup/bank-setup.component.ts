import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { BankSetup } from '../../../../financialModule/_models/bank/bank-setup';
import { BankSetupService } from '../../../../financialModule/_services/bank/bank-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { Constants } from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: '<user></user>',
    templateUrl: './bank-setup.component.html',
    styles: ["user.component.css"],
    providers:[BankSetupService]
})

//export to make it available for other classes
export class BankSetupComponent {
    page = new Page();
    rows = new Array<BankSetup>();
    ArrBankId = [];
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    countryOptions;
    stateOptions;
    cityOptions;
    BankSetup = {};
    bankId: string;
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    btnCancelText=Constants.btnCancelText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    myHtml;
    add;
    showCancel;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    
    ADD_BANK_SETUP_BANK_ID : any = { 'fieldName': 'ADD_BANK_SETUP_BANK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_DESC : any = { 'fieldName': 'ADD_BANK_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'', 'isMandatory':'' };
    ADD_BANK_SETUP_DESC_ARABIC : any = { 'fieldName': 'ADD_BANK_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_ADDRESS1 : any = { 'fieldName': 'ADD_BANK_SETUP_ADDRESS1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_PHONE1 : any = { 'fieldName': 'ADD_BANK_SETUP_PHONE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' , 'isMandatory':''};
    ADD_BANK_SETUP_PHONE2 : any = { 'fieldName': 'ADD_BANK_SETUP_PHONE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_PHONE3 : any = { 'fieldName': 'ADD_BANK_SETUP_PHONE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_FAX : any = { 'fieldName': 'ADD_BANK_SETUP_FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_COUNTRY : any = { 'fieldName': 'ADD_BANK_SETUP_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_STATE : any = { 'fieldName': 'ADD_BANK_SETUP_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_CITY : any = { 'fieldName': 'ADD_BANK_SETUP_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_SAVE : any = { 'fieldName': 'ADD_BANK_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_CLEAR : any = { 'fieldName': 'ADD_BANK_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_ADDRESS2 : any = { 'fieldName': 'ADD_BANK_SETUP_ADDRESS2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    ADD_BANK_SETUP_ADDRESS3 : any = { 'fieldName': 'ADD_BANK_SETUP_ADDRESS3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''};
    
    MANAGE_BANK_SETUP_BANK_ID : any =  { 'fieldName': 'MANAGE_BANK_SETUP_BANK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'', 'isMandatory':'' };
    MANAGE_BANK_SETUP_DESC : any =  { 'fieldName': 'MANAGE_BANK_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''};
    MANAGE_BANK_SETUP_DESC_ARABIC : any =  { 'fieldName': 'MANAGE_BANK_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'', 'isMandatory':'' };
    MANAGE_BANK_SETUP_SEARCH : any =  { 'fieldName': 'MANAGE_BANK_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'', 'isMandatory':'' };
    MANAGE_BANK_SETUP_EDIT : any =  { 'fieldName': 'MANAGE_BANK_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'', 'isMandatory':'' };
    MANAGE_BANK_SETUP_VIEW : any =  { 'fieldName': 'MANAGE_BANK_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'', 'isMandatory':'' };
    ACTION : any =  { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''};

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private bankSetupService:BankSetupService,
         
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
    }

     ngOnInit() {
         this.setPage({ offset: 0 });
                this.getAddScreenDetail();
                this.getViewScreenDetail();
        this.bankSetupService.getCountryList().then(data => {
         this.countryOptions = data.result;
         this.countryOptions.splice(0, 0, { "countryId": "", "countryName": this.select });
         this.model.countryId = "";
        });
                
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.bankSetupService.searchBankSetup(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
  

    //event returning state data by state id
     getStateData(event) {
        this.model.cityId = "";
        var stateId = event.target.value ? event.target.value : -1;
        this.bankSetupService.getCityList(stateId).then(data => {
            this.cityOptions = data.result;
            this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
        });
    }
    

    // event returning country data by country id
    getCountryData(event) {
        this.model.stateId = "";
        var countryId = event.target.value ? event.target.value : -1;
        this.bankSetupService.getStateList(countryId).then(data => {
            this.stateOptions = data.result;
            this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
        });
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1034";
         this.defaultAddFormValues = [
                    { 'fieldName': 'ADD_BANK_SETUP_BANK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'', 'isMandatory':'' },
                    { 'fieldName': 'ADD_BANK_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_ADDRESS1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_PHONE1', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'', 'writeAccess':'' , 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_PHONE2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_PHONE3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_ADDRESS2', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
                    { 'fieldName': 'ADD_BANK_SETUP_ADDRESS3', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'' , 'writeAccess':'', 'isMandatory':''},
         ];
         this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
         this.getScreenDetail(this.screenCode,'Add');

            this.ADD_BANK_SETUP_BANK_ID = this.defaultAddFormValues[0];
            this.ADD_BANK_SETUP_DESC = this.defaultAddFormValues[1];
            this.ADD_BANK_SETUP_DESC_ARABIC = this.defaultAddFormValues[2];
            this.ADD_BANK_SETUP_ADDRESS1 = this.defaultAddFormValues[3];
            this.ADD_BANK_SETUP_PHONE1 = this.defaultAddFormValues[4];
            this.ADD_BANK_SETUP_PHONE2 = this.defaultAddFormValues[5];
            this.ADD_BANK_SETUP_PHONE3 = this.defaultAddFormValues[6];
            this.ADD_BANK_SETUP_FAX = this.defaultAddFormValues[7];
            this.ADD_BANK_SETUP_COUNTRY = this.defaultAddFormValues[8];
            this.ADD_BANK_SETUP_STATE = this.defaultAddFormValues[9];
            this.ADD_BANK_SETUP_CITY = this.defaultAddFormValues[10];
            this.ADD_BANK_SETUP_SAVE = this.defaultAddFormValues[11];
            this.ADD_BANK_SETUP_CLEAR = this.defaultAddFormValues[12];
            this.ADD_BANK_SETUP_ADDRESS2 = this.defaultAddFormValues[13];
            this.ADD_BANK_SETUP_ADDRESS3 = this.defaultAddFormValues[14];

    }
    

    getViewScreenDetail()
    {
         this.screenCode = "S-1038";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_BANK_SETUP_BANK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
               { 'fieldName': 'MANAGE_BANK_SETUP_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
               { 'fieldName': 'MANAGE_BANK_SETUP_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
               { 'fieldName': 'MANAGE_BANK_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
               { 'fieldName': 'MANAGE_BANK_SETUP_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
               { 'fieldName': 'MANAGE_BANK_SETUP_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
         ];
        this.getScreenDetail(this.screenCode,'Manage');
               
            this.MANAGE_BANK_SETUP_BANK_ID = this.defaultManageFormValues[0];
            this.MANAGE_BANK_SETUP_DESC = this.defaultManageFormValues[1];
            this.MANAGE_BANK_SETUP_DESC_ARABIC = this.defaultManageFormValues[2];
            this.MANAGE_BANK_SETUP_SEARCH = this.defaultManageFormValues[3];
            this.MANAGE_BANK_SETUP_EDIT = this.defaultManageFormValues[4];
            this.MANAGE_BANK_SETUP_VIEW = this.defaultManageFormValues[5];
            this.ACTION = this.defaultManageFormValues[6];
              
    }
  
    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                   objDefault['isMandatory'] = objAvailable['isMandatory'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                   objDefault['isMandatory'] = objAvailable['isMandatory'];
                }
            }
        });
    }

    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.bankSetupService.searchBankSetup(this.page, this.searchKeyword).subscribe(pagedData => {
             this.page = pagedData.page;
             this.rows = pagedData.data;
        });
    }
    

    getBankDetailsById(row: any)
    {
        this.showBtns=true;
        this.isModify=true;
        this.bankSetupService.getByIdBankSetup(row.bankId).then(data => {
        this.model = data.result; 
         });
    }

    Cancel(f)
    {
    this.isModify=false;
    this.model.bankId= '';
    this.resetMe(f);
}

    resetMe(f)
    {
        this.model.bankDescription= '';
        this.model.bankArabicDescription= '';
        this.model.address1='';
        this.model.phone1='';
        this.model.address2='';
        this.model.phone2='';
        this.model.address3='';
        this.model.phone3='';
        this.model.cityId='';
        this.model.fax='';
        this.model.stateId='';
        this.model.countryId='';
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.bookId='';
		 }
    }
//Is id already exist
IsIdAlreadyExist(){
    if(this.model.bankId != '' && this.model.bankId != undefined && this.model.bankId != 'undefined')
    {
        this.bankSetupService.getByIdBankSetup(this.model.bankId).then(data => {
         var datacode = data.code;
            if (data.status == "NOT_FOUND") {
                return true;
            }
            else{
                var txtId = <HTMLInputElement> document.getElementById("bankTextId");
                txtId.focus();
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                this.messageText = data.btiMessage.message;
                this.setPage({ offset: 0 });
                window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
            window.scrollTo(0,0);
            this.model.bankId='';
            return false;
            }

            }).catch(error => {
            window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.hasMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
    });
}
}
     CreateBankSetup(f: NgForm) {
         
        this.btndisabled=true;
       if (this.isModify) {
            this.bankSetupService.updateBankSetup(this.model).then(data => {
                this.btndisabled=false;
                window.scrollTo(0,0);
                var datacode = data.code;
                if (datacode == 200) {
                    this.isModify=false;
                    this.model.bankId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.hasMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                        this.setPage({ offset: 0 });

                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        else {
            
            this.bankSetupService.addBankSetup(this.model).then(data => {
                this.showBtns=false;
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                if (datacode == 200) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });

                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
            }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
    }
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }

        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
       LockScreen(writeAccess)
       {
           if(!writeAccess)
           {
               return true
           }
           else if(this.btndisabled)
           {
               return true
           }
           else if(this.isScreenLock)
           {
               return true;
           }
           else{
               return false;
           }
       }
}
