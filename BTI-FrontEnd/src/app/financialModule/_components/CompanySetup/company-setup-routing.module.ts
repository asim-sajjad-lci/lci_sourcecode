import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BankSetupComponent } from './bank-setup/bank-setup.component';
import { PaymentTermSetupComponent } from './payment-terms-setup/payment-terms-Setup.component';
import { ConfigurationComponent } from './configuration-setup/configuration-setup.component';
import { CreditCardSetupComponent } from './credit-card-setup/credit-card-setup.component';
import { ShippingMethodSetupComponent } from './shipping-method-setup/shipping-method-setup.component';
import { VatDetailSetupComponent } from './vat-detail-setup/vat-detail-setup.component';

const routes: Routes = [
   { path: '', component: BankSetupComponent },
  { path: 'banksetup', component: BankSetupComponent },
  { path: 'paymentTermSetup', component: PaymentTermSetupComponent },
  { path: 'configurationsetup', component: ConfigurationComponent },
  { path: 'creditcardsetup', component: CreditCardSetupComponent },
  { path: 'shippingmethod', component: ShippingMethodSetupComponent },
  { path: 'vatdetailsetup', component: VatDetailSetupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompanySetupRoutingModule { }
