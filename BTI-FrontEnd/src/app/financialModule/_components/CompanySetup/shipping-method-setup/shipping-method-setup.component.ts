import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ShippingMethodSetup } from '../../../../financialModule/_models/company-setup/shipping-method-setup';
import { ShippingMethodSetupService } from '../../../../financialModule/_services/company-setup/shipping-method-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {Constants} from '../../../../_sharedresource/Constants';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';


@Component({
    selector: '<shipping-method-setup></shipping-method-setup>',
    templateUrl: './shipping-method-setup.component.html',
    styles: ["user.component.css"],
    providers:[ShippingMethodSetupService]
})

//export to make it available for other classes
export class ShippingMethodSetupComponent {
    page = new Page();
    rows = new Array<ShippingMethodSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    isModify:boolean=false;
    ArrShippmentStatus =[];
    add;
    myHtml;
    disableId;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;
    isScreenLock
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private shippingMethodSetupService: ShippingMethodSetupService,
        private getScreenDetailService: GetScreenDetailService){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.add = true;
        this.disableId = false;
    }

    ngOnInit() {
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });
                this.getShipmentMethodTypeStatus();
 }

    getAddScreenDetail()
    {
        this.screenCode = "S-1089";
           // default form parameter for adding shipping method setup
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_SHIP_METHOD_SHIP_METHOD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_SHIP_METHOD_DESC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_SHIP_METHOD_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_SHIP_METHOD_SHIP_TYPE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_SHIP_METHOD_PICKUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_SHIP_METHOD_DELIVERY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_SHIP_METHOD_CARRIER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_SHIP_METHOD_CARRIER_ACCOUNT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_SHIP_METHOD_CONTACT_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_SHIP_METHOD_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_SHIP_METHOD_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_SHIP_METHOD_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
          
         ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1090";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_SHIP_METHOD_SHIP_METHOD', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_SHIP_METHOD_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_SHIP_METHOD_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_SHIP_METHOD_CARRIER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_SHIP_METHOD_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_SHIP_METHOD_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

   //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
         this.shippingMethodSetupService.searchShipmentMethodSetup(this.page, this.searchKeyword).subscribe(pagedData => {
          this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.shippingMethodSetupService.searchShipmentMethodSetup(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

    // getpaymentTermById(row: any)
    getShipmentMethodSetupById(row: any)
    {
         this.add = false;
         this.disableId = true;
         this.showBtns=true;
         this.myHtml = "border-vanish";
         this.shippingMethodSetupService.getShipmentMethodSetupById(row.shipmentMethodId).then(data => {
        this.model = data.result; 
         });
    }

getShipmentMethodTypeStatus(){
      this.shippingMethodSetupService.getShipmentMethodTypeStatus().then(data => {
      this.ArrShippmentStatus = data.result;
                 });
}
//for reseting data
    clearForm(f){
        this.model.shipmentDescription= '';
        this.model.shipmentDescriptionArabic= '';
        this.model.shipmentCarrierAccount='';
        this.model.shipmentCarrier='';
        this.model.shipmentCarrierArabic='';
        this.model.shipmentCarrierContactName= '';
        this.model.shipmentPhoneNumber='';
        this.model.shipmentMethodType='';
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.shipmentMethodId='';
		 }
    }
    Cancel(f)
    {
    this.add=true;
    this.model.shipmentMethodId='';
    this.clearForm(f);
    this.disableId=false;
    }

  //Is id already exist
IsIdAlreadyExist(){
    if(this.model.shipmentMethodId != '' && this.model.shipmentMethodId != undefined && this.model.shipmentMethodId != 'undefined')
      {
          this.shippingMethodSetupService.getShipmentMethodSetupById(this.model.shipmentMethodId).then(data => {
           var datacode = data.code;
           if (data.status == "NOT_FOUND" || data.btiMessage.messageShort =='RECORD_NOT_FOUND') {
                  return true;
              }
              else{
                  var txtId = <HTMLInputElement> document.getElementById("txtshipmentMethodId");
                  txtId.focus();
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.hasMsg = true;
                  this.messageText = data.btiMessage.message;
                  this.setPage({ offset: 0 });
                  window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
              window.scrollTo(0,0);
              this.model.shipmentMethodId='';
              return false;
              }
  
              }).catch(error => {
              window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
          }, 100)
      });
  }
  }
    saveShippingMethodSetup(f: NgForm)
    {
        // if(this.model.shipmentMethodType=='undefined' && this.model.shipmentMethodType==undefined){
       
         if( this.model.shipmentMethodType =='undefined' && this.model.shipmentMethodType==undefined){
            this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = "Server error. Please contact admin !";
        }
        
        if(!this.add)
        {
            this.btndisabled=true;
            this.disableId = false;
             this.shippingMethodSetupService.updateShipmentMethodSetup(this.model).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                    //   this.isModify=false;
                    //   this.model.currencyId=null;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.showBtns=false;
                           this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
               f.resetForm();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        else
        {
            this.btndisabled=true;
             this.shippingMethodSetupService.createShippingMethodSetup(this.model).then(data => {
                this.showBtns=false;
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                         this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                var selectedValue = <HTMLInputElement>document.getElementById("txtshipmentMethodId")
                selectedValue=this.model.shipmentMethodType;
             }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        this.btndisabled=false;
    }
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
            
            
            /** If Screen is Lock then prevent user to perform any action.
            *  This function also cover Role Management Write acceess functionality */
            LockScreen(writeAccess)
            {
                if(!writeAccess)
                {
                    return true
                }
                else if(this.btndisabled)
                {
                    return true
                }
                else if(this.isScreenLock)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
}