import { NgModule,Directive,ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BankSetupComponent } from './bank-setup/bank-setup.component';
import { PaymentTermSetupComponent } from './payment-terms-setup/payment-terms-Setup.component';
import { ConfigurationComponent } from './configuration-setup/configuration-setup.component';
import { CreditCardSetupComponent } from './credit-card-setup/credit-card-setup.component';
import { ShippingMethodSetupComponent } from './shipping-method-setup/shipping-method-setup.component';
import { VatDetailSetupComponent } from './vat-detail-setup/vat-detail-setup.component';
import { CompanySetupRoutingModule } from './company-setup-routing.module';
import { OnlyNumber } from './../../../_sharedresource/onlynumber.directive';
import { OnlyAlphaNumeric } from './../../../_sharedresource/OnlyAlphanumeric.directive';
//import { TrimWhitespace } from '../../../_sharedresource/TrimWhitespace.directive';

@Directive({
  selector: '[myAutofocus,OnlyNumber]'
})
export class AutoFocusDirective {
  constructor(private elementRef: ElementRef) { };
  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
  }
}

@NgModule({
  imports: [
    CommonModule,
    AngularMultiSelectModule,
    CompanySetupRoutingModule,
    FormsModule,
    NgxDatatableModule,
    // onlyAlphabet
  ],
  declarations: [BankSetupComponent,PaymentTermSetupComponent,ConfigurationComponent, 
    CreditCardSetupComponent,ShippingMethodSetupComponent,VatDetailSetupComponent,AutoFocusDirective,
    OnlyAlphaNumeric]
})
export class CompanySetupModule { }
