import { Component, ViewChild,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { CreditCardSetup } from '../../../../financialModule/_models/Company-setup/credit-card-setup';
import { CreditCardSetupService } from '../../../../financialModule/_services/Company-setup/credit-card-setup.service';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {Constants} from '../../../../_sharedresource/Constants';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { Page } from '../../../../_sharedresource/page';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 

declare var $:any
@Component({
    selector: '<credit-card-setup></credit-card-setup>',
    templateUrl: './credit-card-setup.component.html',
    styles: ["user.component.css"],
    providers:[CreditCardSetupService,AccountStructureService,CommonService]
})

//export to make it available for other classes
export class CreditCardSetupComponent {
    page = new Page();
    rows = new Array<CreditCardSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    isScreenLock;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    checkBookIdOptions;
    model: any = {};
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean=false;
    cardTypeStatus =[];
    ArrAccountNumberList=[];
    ddl_MultiSelectAccount = [];
    SelectedAccount : any = [];
    ddlAccountSetting = {};
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    selectAccount = Constants.selectAccount;
    // Serachable Select Box Step1
	ArrCheckBookId = [];
    checkBookId : any = [];
    ddlCheckBookIdSetting = {};
    isModalOpen:boolean=false;
    arrSegment= [];
	accountNumberList =[];
	accountDescription=[];
	accountNumberIndex=[];
	accountNumberTitleLabel:string;
	createAccountNumberLabel:string;
	btnCancelLabel:string;
	// Serachable Select Box Step1 End
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
    private router: Router,
    private route: ActivatedRoute,
    private creditCardSetupService: CreditCardSetupService,
    private getScreenDetailService: GetScreenDetailService,
    private commonService:CommonService,
    private accountStructureService:AccountStructureService,public toastr: ToastsManager,	vcr: ViewContainerRef){
        this.toastr.setRootViewContainerRef(vcr);
        this.accountNumberTitleLabel=Constants.accountNumberTitle;
	    this.createAccountNumberLabel=Constants.createAccountNumber;
	    this.btnCancelLabel=Constants.btnCancelText;
        this.page.pageNumber = 0;
        this.page.size = 5;
    }

    ngOnInit() {
        this.checkBookIdOptions="";
        this.getcheckBookIdList();
        
        this.getSegmentCount();
			
        this.ddlAccountSetting = {
           singleSelection: true, 
           text:this.selectAccount,
           selectAllText: this.selectAll,
           unSelectAllText: this.unselectAll,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 

          // Serachable Select Box Step2
		   this.ddlCheckBookIdSetting = { 
           singleSelection: true, 
           text:this.select,
           enableSearchFilter: true,
           classes:"myclass custom-class",
           searchPlaceholderText:this.search,
         }; 
		 // Serachable Select Box Step2

         this.model={
             'checkBookId':'',
             'creditCardNameArabic':'',
             'creditCardName':'',
             'cardType':'',
             'cardId':'',
              'accountTableRowIndex':'',
             'accountNumber':'',
         }
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });
                this.getAccountNumber();
       //getCreditCardTypeStatus
          this.creditCardSetupService.getCreditCardTypeStatus().then(data => {
              this.cardTypeStatus = data.result;
          });
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1074";
           // default form parameter for adding exchange creditCardSetup set up
        this.defaultAddFormValues = [
            { 'fieldName': 'ADD_CREDIT_CARD_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CREDIT_CARD_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CREDIT_CARD_BANK_CARD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CREDIT_CARD_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CREDIT_CARD_CHARGE_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CREDIT_CARD_ACCOUNT_NO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            { 'fieldName': 'ADD_CREDIT_CARD_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_CREDIT_CARD_CLEAR', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
          
            ];
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
                {
                    this.isScreenLock = res;
                });
           this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1075";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_CREDIT_CARD_NAME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CREDIT_CARD_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CREDIT_CARD_CHECKBOOK_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CREDIT_CARD_ACCOUNT_NO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CREDIT_CARD_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_CREDIT_CARD_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
             
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            this.screenName=data.result.dtoScreenDetail.screenName;
            this.moduleName=data.result.moduleName;
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

        updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.creditCardSetupService.searchCreditCardSetup(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }
    
    resetMe(f){
      this.checkBookId=[];
      this.SelectedAccount=[];
      this.model.creditCardNameArabic='';
      this.model.cardType='';
    }
    
    Cancel(f){
    this.model.creditCardName='';
    this.isModify=false;
    this.resetMe(f);
    }
   

    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.creditCardSetupService.searchCreditCardSetup(this.page, this.searchKeyword).subscribe(pagedData => {
           this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }

    getcheckBookIdList()
   {
               
              this.creditCardSetupService.getcheckBookIdList().then(data => {
              this.checkBookIdOptions = data.result.records;
            //   this.checkBookIdOptions.splice(0,0,{"checkBookId": this.select });
            //   this.model.checkBookId = "";
              // Searchable Select Box Step3
			 for(var i=0;i< data.result.records.length;i++)
              {
                this.ArrCheckBookId.push({ "id": data.result.records[i].checkBookId, "itemName": data.result.records[i].checkBookId})
              }
			    // Searchable Select Box Step3 End
       });
   }
 
  // Serachable Select Box Step4
 getCheckBookByID(item:any){
     this.model.checkBookId=item.id;
     this.commonService.closeMultiselect();
 }
 OnCheckBookIdDeSelect(item:any){
     this.model.checkBookId='';
     this.commonService.closeMultiselect();
 }
  // Serachable Select Box Step4 ends

    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }

    
    // getCreditCardDrtailsSetupById(row: any)
    getCreditCardDrtailsSetupById(row: any)
    {
        this.SelectedAccount=[];
        this.checkBookId=[];
        this.isModify=true;
        this.showBtns=true;
         this.creditCardSetupService.getCreditCardDrtailsSetupById(row.cardId).then(data => {
                    this.model = data.result; 
                    this.SelectedAccount.push({"id":data.result.accountTableRowIndex,"itemName":data.result.accountNumber});
                    this.checkBookId.push({'id':data.result.checkBookId,'itemName':data.result.checkBookId});
         });
    }
    getAccountNumber(){
        this.ddl_MultiSelectAccount=[];
       this.creditCardSetupService.getGlAccountNumberList().then(data => {
              for(var i=0;i< data.result.length;i++)
              {
                this.ddl_MultiSelectAccount.push({ "id": data.result[i].accountTableRowIndex, "itemName": data.result[i].accountNumber })
              }
        });
    }

    getSegmentCount()
	{
		 this.accountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}
    
	fetchFirstAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		
		if(segmentNumber)
		{
			this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {
                
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberIndex[segmentIndex])
					{
						this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountDescription.push(result.mainAccountDescription);
						this.accountNumberIndex.push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
                    	var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
						this.accountNumberIndex[m]='0';
						this.accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
                        }
                        if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountDescription = [];
					this.accountNumberIndex = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
                    {
						var ctrlId='';
                        if(m >  0)
                        {
                            ctrlId = 'txtSegment_'+m;
                        }
                        else{
                            ctrlId='txtfirstSegment_'+m;
                        }
                        var segment = <HTMLInputElement>document.getElementById(ctrlId);
                        segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
			this.accountDescription = [];
			this.accountNumberIndex = [];

			var currentSegment=segmentIndex;
			for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
			{
				var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
				segment.value = '';
				segment.disabled = true;
			}
		}
	}
	fetchOtherAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberIndex.indexOf(segment.value);
		if(segmentNumber)
			{
				this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
                    
					let status = pagedData.status;
    				let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberIndex[segmentIndex])
						{
							this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountDescription.push(result.dimensionDescription);
							this.accountNumberIndex.push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
						segment.value = '';
						this.accountNumberIndex[segmentIndex]='0';
						this.accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberIndex[currentSegment]='0';
				this.accountDescription[currentSegment]='0';
			}
	}

	createNewGlAccountNumber()
	{
		var tempAccountNumberList=[];
        var accountDesc = this.accountDescription.join();
        accountDesc=accountDesc.replace(/,/g , " ");
        accountDesc=accountDesc.replace("0"," ");
		tempAccountNumberList.push({'accountNumberIndex':this.accountNumberIndex,'accountDescription':accountDesc})
		this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {
			if(data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED')
			{
				this.toastr.success(data.btiMessage.message);
				this.getAccountNumber();
				this.closeModal();
			}
			else{
				this.toastr.warning(data.btiMessage.message);
			}
		 });
		
	}

    openAccountWindow()
	{
		this.accountNumberIndex=[];
		this.accountDescription=[];
		this.isModalOpen = true;
	}
	closeModal()
    {
         this.isModalOpen = false;
    }


    onItemSelect(item:any){
        this.commonService.closeMultiselectWithId('SelectedAccount');
    }
    OnItemDeSelect(item:any){
        this.commonService.closeMultiselectWithId('SelectedAccount');
    }
    onSelectAll(items: any){
    }
    onDeSelectAll(items: any){
        
    }
    uppdateCardType(cardType)
    {
        
        this.model.cardType=cardType;
    }

    saveCreditCardSetUp(f: NgForm)
    {
        this.btndisabled=true;
      
        if(this.SelectedAccount.length > 0 && this.model.cardType>0)
        {
            this.model.accountTableRowIndex=this.SelectedAccount[0].id;
            if(this.isModify)
            {
                 this.creditCardSetupService.updateCreditCardSetup(this.model).then(data => {
                    this.SelectedAccount=[];
                    this.checkBookId=[];
                    window.scrollTo(0,0);
                    this.btndisabled=false;
                    var datacode = data.code;
                      if (datacode == 200) {
                          this.isModify=false;
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.showBtns=false;
                            this.setPage({ offset: 0 });
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                    this.SelectedAccount=[];
                 }).catch(error => {
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                    this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');;
                    }, 100)
                 });
            }
            else
            {
                 this.creditCardSetupService.createCreditCardSetup(this.model).then(data => {
                     this.SelectedAccount=[];
                     this.checkBookId=[];
                     this.showBtns=false;
                    window.scrollTo(0,0);
                    var datacode = data.code;
                    this.btndisabled=false;
                      if (datacode == 200) {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.hasMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.setPage({ offset: 0 });

                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                        }, 4000);
                    }
                    f.resetForm();
                    this.SelectedAccount=[];
                 }).catch(error => {
                     
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.hasMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                 });
            }
         
        }
        this.btndisabled=false;
        // else
        // {
        //     this.isSuccessMsg = false;
        //     this.isfailureMsg = true;
        //     this.showMsg = true;
        //     this.hasMsg = true;
        //     this.messageText = this.defaultAddFormValues[5]['listDtoFieldValidationMessage'][0]['validationMessage']};
        //     this.setPage({ offset: 0 });
        //     window.setTimeout(() => {
        //         this.showMsg = false;
        //         this.hasMsg = false;
        //     }, 4000);  
        // }
    }
    
     
/** If Screen is Lock then prevent user to perform any action.
*  This function also cover Role Management Write acceess functionality */
LockScreen(writeAccess)
{
    if(!writeAccess)
    {
        return true
    }
    else if(this.btndisabled)
    {
        return true
    }
    else if(this.isScreenLock)
    {
        return true;
    }
    else{
        return false;
    }
}
    
}

