import { Component,ViewChild,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { VatDetailSetup } from '../../../../financialModule/_models/company-setup/vat-detail-setup';
import { VatDetailSetupService } from '../../../../financialModule/_services/company-setup/vat-detail-setup.service';
import { AccountStructureService } from '../../../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { GetScreenDetailService } from '../../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../../_sharedresource/_services/common-services.service';
import { Autofocus } from '../../../../_sharedresource/Autofocus';
import {Constants} from '../../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../../_sharedresource/page';
import { AgmCoreModule } from '@agm/core';
import {Observable} from 'rxjs/Observable';
import { ToastsManager } from 'ng2-toastr/ng2-toastr'; 
declare var $:any
@Component({
    selector: '<VatDetail></VatDetail>',
    templateUrl: './vat-detail-setup.component.html',
    styles: ["user.component.css"],
    providers:[VatDetailSetupService,AccountStructureService,CommonService]
})

//export to make it available for other classes
export class VatDetailSetupComponent {
    page = new Page();
    rows = new Array<VatDetailSetup>();
    selected = [];
    moduleCode = Constants.financialModuleCode;
    screenCode;
    screenName;
    moduleName;
    isScreenLock;
    defaultAddFormValues: Array<object>;
    defaultManageFormValues: Array<object>;
    availableFormValues: [object];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    ArrDiscountTypePeriod=[];
    ArrDiscountType=[];
    ArrDueType=[];
    ArraySeriesType=[];
    ArrayBasedOnList=[];
    ArrAccountNumberList=[];
    // Serachable Select Box Step1
	ArraccountRowId = [];
    accountRowId : any = [];
    ddlaccountRowSetting = {};
	// Serachable Select Box Step1 End
    select=Constants.select;
    searchKeyword = '';
    ddPageSize = 5;
    atATimeText=Constants.atATimeText;
    tableViewtext=Constants.tableViewtext;
    search=Constants.search;
    isModify:boolean=false;
    myHtml;
    add;
    btnCancelText=Constants.btnCancelText;
    showBtns:boolean=false;
    isModalOpen:boolean=false;
    arrSegment= [];
	accountNumberList =[];
	accountDescription=[];
	accountNumberIndex=[];
	accountNumberTitleLabel:string;
	createAccountNumberLabel:string;
	btnCancelLabel:string;
    totalText = Constants.totalText;
    EmptyMessage = Constants.EmptyMessage;
    btndisabled:boolean=false;

    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private getScreenDetailService: GetScreenDetailService,
        private vatDetailSetupService:VatDetailSetupService,
        private commonService:CommonService,
        private accountStructureService:AccountStructureService,public toastr: ToastsManager,	vcr: ViewContainerRef
        
    ){
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.toastr.setRootViewContainerRef(vcr);
        this.accountNumberTitleLabel=Constants.accountNumberTitle;
	    this.createAccountNumberLabel=Constants.createAccountNumber;
	    this.btnCancelLabel=Constants.btnCancelText;
    }

    ngOnInit() {

                this.GetInitialValues();
                this.getSegmentCount();
                // Serachable Select Box Step2
                this.ddlaccountRowSetting = { 
                singleSelection: true, 
                text:this.select,
                enableSearchFilter: true,
                classes:"myclass custom-class",
                searchPlaceholderText:this.search,
                }; 
                // Serachable Select Box Step2

                this.getAccountNumber();
                this.getAddScreenDetail();
                this.getViewScreenDetail();
                this.setPage({ offset: 0 });

                    this.vatDetailSetupService.getSeriesType().then(data => {
                    this.ArraySeriesType = data.result;
                    this.ArraySeriesType.splice(0, 0, { "typeId": "", "name": this.select });
                    this.model.vatSeriesType='';
                 });

                    this.vatDetailSetupService.getBasedOnList().then(data => {
                    this.ArrayBasedOnList = data.result;
                    this.ArrayBasedOnList.splice(0, 0, { "typeId": "", "name": this.select });
                    this.model.vatBaseOn='';
                 });
               
    }

    getAddScreenDetail()
    {
         this.screenCode = "S-1097";
         this.defaultAddFormValues = [
               { 'fieldName': 'ADD_VAT_DETAIL_VAT_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_TYPE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_VAT_ID_NUMBER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_ACCOUNT_NO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_BASED_ON', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_PERCENTAGE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_VAT_AMOUNT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_MIN', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_MAX', 'fieldValue': '', 'helpMessage': '' ,'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':''},
               { 'fieldName': 'ADD_VAT_DETAIL_SAVE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_CLEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_YEAR_TO_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_LAST_YEAR', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_TOTAL_SALE_PURCHASE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_TAXABLE_SALE_PURCHASE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ADD_VAT_DETAIL_SALE_PURCHASE_TAXES', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
         ];
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
         this.getScreenDetail(this.screenCode,'Add');
    }

    getViewScreenDetail()
    {
         this.screenCode = "S-1098";
         this.defaultManageFormValues = [
               { 'fieldName': 'MANAGE_VAT_DETAIL_VAT_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_VAT_DETAIL_DESC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_VAT_DETAIL_DESC_ARABIC', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_VAT_DETAIL_TYPE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_VAT_DETAIL_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'MANAGE_VAT_DETAIL_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
               { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
         ];
        this.getScreenDetail(this.screenCode,'Manage');
    }

    getScreenDetail(screenCode,ArrayType)
    {
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenCode).then(data => {
            
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            if(ArrayType == 'Add')
            {
                this.screenName=data.result.dtoScreenDetail.screenName;
                this.moduleName=data.result.moduleName;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultAddFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
            else
            {
                for (var j = 0; j < this.availableFormValues.length; j++) {
                   var fieldKey = this.availableFormValues[j]['fieldName'];
                   var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                   var objDefault = this.defaultManageFormValues.find(x => x['fieldName'] === fieldKey);
                   objDefault['fieldValue'] = objAvailable['fieldValue'];
                   objDefault['helpMessage'] = objAvailable['helpMessage'];
                   objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                   objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                   objDefault['readAccess'] = objAvailable['readAccess'];
                   objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            }
        });
    }

    // Serachable Select Box Step4
    getaccountRowByID(item:any){
      
        this.model.accountRowId=item.id;
        this.vatDetailSetupService.getSalePurchaseAmountByAccountNumber(item.id).then(pagedData => {
          
          this.model.ytdTotalSalesPurchase = pagedData.result.ytdTotalSalesPurchase; 
          this.model.ytdTotalTaxableSalesPurchase = pagedData.result.totalApplyAmountYtd; 
          this.model.ytdTotalSalesPurchaseTaxes = pagedData.result.ytdTotalSalesPurchaseTaxes;

          this.model.lastYearTaxableSalesPurchase = pagedData.result.lastYearTotalSalesPurchase;
          this.model.lastYearTotalSalesPurchase = pagedData.result.totalApplyAmount;
          this.model.lastYearSalesPurchaseTaxes = pagedData.result.lastYearSalesPurchaseTaxes;
   });
        this.commonService.closeMultiselect()
    }

      //Get SalePurchase Amount By AccountNumber
      GetSalePurchaseAmountByAccountNumber(acctRowId){
        this.model.accountRowId=acctRowId;
        this.vatDetailSetupService.getSalePurchaseAmountByAccountNumber(acctRowId).then(pagedData => {
          
          this.model.ytdTotalSalesPurchase = pagedData.result.ytdTotalSalesPurchase; 
          this.model.ytdTotalTaxableSalesPurchase = pagedData.result.totalApplyAmountYtd; 
          this.model.ytdTotalSalesPurchaseTaxes = pagedData.result.ytdTotalSalesPurchaseTaxes;

          this.model.lastYearTaxableSalesPurchase = pagedData.result.lastYearTotalSalesPurchase;
          this.model.lastYearTotalSalesPurchase = pagedData.result.totalApplyAmount;
          this.model.lastYearSalesPurchaseTaxes = pagedData.result.lastYearSalesPurchaseTaxes;
        });
    }
    OnaccountRowDeSelect(item:any){
        this.model.accountRowId='';
        this.commonService.closeMultiselect()
    }
    // Serachable Select Box Step4 ends
    
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.page.size = this.ddPageSize;
        this.vatDetailSetupService.searchVatDetailSetup(this.page, this.searchKeyword).subscribe(pagedData => {
         
            this.page = pagedData.page;
            this.rows = pagedData.data;
            this.table.offset = 0;
       }, error => {
            this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
        });
    }

    resetMe(f)
    {
        this.model.vatDescription= '';
        this.model.vatDescriptionArabic= '';
        this.model.vatSeriesType='';
        this.model.vatIdNumber='';
        this.accountRowId=[];
        this.model.vatBaseOn='';
        this.model.basperct='';
        this.model.minimumVATAmount='';
        this.model.maximumVATAmount='';
        this.model.ytdTotalSalesPurchase='';
        this.model.lastYearTotalSalesPurchase='';
        this.model.ytdTotalTaxableSalesPurchase='';
        this.model.lastYearTaxableSalesPurchase='';
        this.model.ytdTotalSalesPurchaseTaxes='';
        this.model.lastYearSalesPurchaseTaxes='';
       if(!this.add){
			this.myHtml = "border-vanish";
		 }else{
			this.myHtml = "";
            this.model.vatScheduleId='';
		 }
        this.GetInitialValues();
    }

    Cancel(f)
    {
    this.isModify=false;
    this.model.vatScheduleId= '';
    this.resetMe(f);
    }
    //setting pagination
    setPage(pageInfo) {
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        this.vatDetailSetupService.searchVatDetailSetup(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
        });
    }
     
    getAccountNumber(){
       this.vatDetailSetupService.getGlAccountNumberList().then(data => {
            // Serachable Select Box Step3
             this.ArraccountRowId = [];
			 for(var i=0;i< data.result.length;i++)
              {
                this.ArraccountRowId.push({ "id": data.result[i].accountTableRowIndex, "itemName": data.result[i].accountNumber})
              }
			    // Serachable Select Box Step3 End
                        // this.ArrAccountNumberList = data.result;
                        // this.ArrAccountNumberList.splice(0, 0, { "accountTableRowIndex": "", "accountNumber": this.select });
                        // this.model.accountRowId='';
                  });
    }

     changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0 });
    }
    getvatDetailById(row: any)
    {
         this.showBtns=true;
         this.isModify=true;
         this.vatDetailSetupService.getVatDetailSetupById(row.vatScheduleId).then(data => {
         this.model = data.result;
         this.GetSalePurchaseAmountByAccountNumber(data.result.accountRowId);
         this.accountRowId=[];
         var selectedAccount = this.ArraccountRowId.find(x => x.id ==   data.result.accountRowId);
         if(selectedAccount)
         {
            this.accountRowId.push({'id':selectedAccount.id,'itemName':selectedAccount.itemName}); 
         }
         
         });
    }
  //Is id already exist
  IsIdAlreadyExist(){
    if(this.model.vatScheduleId != '' && this.model.vatScheduleId != undefined && this.model.vatScheduleId != 'undefined')
      {
          this.vatDetailSetupService.getVatDetailSetupById(this.model.vatScheduleId).then(data => {
                        // Serachable Select Box Step5
						// Serachable Select Box Step5 End
           var datacode = data.code;
           if (data.status == "NOT_FOUND" || data.btiMessage.messageShort =='RECORD_NOT_FOUND') {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = false;
                  this.showMsg = false;
                  this.hasMsg = false;
                  return true;
              }
              else{
              //    this.accountRowId.push({'id':data.result.accountTableRowIndex,'itemName':data.result.accountNumber});
                  var txtId = <HTMLInputElement> document.getElementById("vatScheduleIdText");
                  txtId.focus();
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.hasMsg = true;
                  this.messageText = data.btiMessage.message;
                  this.setPage({ offset: 0 });
                  window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
              window.scrollTo(0,0);
              this.model.vatScheduleId='';
              return false;
              }
  
            }).catch(error => {
              window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
          }, 100)
      });
  }
  }
    saveVATDetail(f: NgForm)
    {
      
       
        if(this.isModify)
        {
            this.btndisabled=true;
            this.vatDetailSetupService.updateVatDetailSetup(this.model).then(data => {
                this.accountRowId=[];
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                      this.isModify=false;
                      this.model.vatScheduleId=null;
                            this.hasMsg = true;
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.messageText = data.btiMessage.message;
                            this.showBtns=false;
                         this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                this.GetInitialValues();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
             this.btndisabled=false;
        }
        else
        {
            this.btndisabled=true;
            this.vatDetailSetupService.createVatDetailSetup(this.model).then(data => {
                this.showBtns=false;
                this.accountRowId=[];
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
                  if (datacode == 201) {
                        this.hasMsg = true;
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                else{
                     this.hasMsg = true;
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.setPage({ offset: 0 });
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                f.resetForm();
                this.GetInitialValues();
             }).catch(error => {
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.hasMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
             });
        }
        this.btndisabled=false;
         
    }

    GetInitialValues(){
       this.model.ytdTotalSalesPurchase='0';
       this.model.lastYearTotalSalesPurchase='0';
       this.model.ytdTotalTaxableSalesPurchase='0';
       this.model.lastYearTaxableSalesPurchase='0';
       this.model.ytdTotalSalesPurchaseTaxes='0';
       this.model.lastYearSalesPurchaseTaxes='0';
    }

    getSegmentCount()
	{
		 this.accountStructureService.getAccountStructureSetup().then(data => {
			this.arrSegment=[];
			for(var i=0;i<data.result.segmentNumber;i++)	
			{
				this.arrSegment.push({'segmentNumber':i,'isReadOnly':"true"});
			}
		 });
	}
    
	fetchFirstAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtfirstSegment_'+ segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		
		if(segmentNumber)
		{
			this.accountStructureService.firstTextApi(segmentNumber).subscribe(pagedData => {
                
        		let status = pagedData.status;
        		let result = pagedData.result;
				if(status == "FOUND"){	
					if(this.accountNumberIndex[segmentIndex])
					{
						this.accountNumberIndex[segmentIndex] = result.actIndx.toString();
						this.accountDescription[segmentIndex] = result.mainAccountDescription;
					}
					else{
						this.accountDescription.push(result.mainAccountDescription);
						this.accountNumberIndex.push(result.actIndx.toString());
					}
				
					for(var m = segmentIndex + 1;m < this.arrSegment.length;m++)
					{
                    	var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
						this.accountNumberIndex[m]='0';
						this.accountDescription[m]='0';
						if(currentSegment != m)
						{
							segment.disabled = false;
                        }
                        if(m == segmentIndex + 1)
						{
							segment.focus();
						}
					}
				}else{
					this.accountDescription = [];
					this.accountNumberIndex = [];
					var currentSegment=segmentIndex;
					for(var m = currentSegment;m < this.arrSegment.length;m++)
                    {
						var ctrlId='';
                        if(m >  0)
                        {
                            ctrlId = 'txtSegment_'+m;
                        }
                        else{
                            ctrlId='txtfirstSegment_'+m;
                        }
                        var segment = <HTMLInputElement>document.getElementById(ctrlId);
                        segment.value = '';
					}
						this.toastr.warning(pagedData.btiMessage.message);				
					}
        	});
		}
		else{
			this.accountDescription = [];
			this.accountNumberIndex = [];

			var currentSegment=segmentIndex;
			for(var m = currentSegment + 1;m < this.arrSegment.length;m++)
			{
				var segment = <HTMLInputElement>document.getElementById('txtSegment_'+m);
				segment.value = '';
				segment.disabled = true;
			}
		}
	}
	fetchOtherAccountIndexInfo(segmentIndex){
		
        var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
		var segmentNumber=segment.value;

		if(this.accountNumberIndex[segmentIndex] && this.accountNumberIndex[segmentIndex] == segmentNumber)
		{
			return false;
		}
		var acctIndex = this.accountNumberIndex.indexOf(segment.value);
	
			if(segmentNumber)
			{
				this.accountStructureService.restTextApi(segmentNumber, segmentIndex).subscribe(pagedData => {
					let status = pagedData.status;
    				let result = pagedData.result;
					if(status == "FOUND"){	
						if(this.accountNumberIndex[segmentIndex])
						{
							this.accountNumberIndex[segmentIndex] = result.dimInxValue.toString();
							this.accountDescription[segmentIndex] = result.dimensionDescription;
						}
						else{
							this.accountDescription.push(result.dimensionDescription);
							this.accountNumberIndex.push(result.dimInxValue.toString());
						}
					}else{
						var segment = <HTMLInputElement>document.getElementById('txtSegment_'+segmentIndex);
						segment.value = '';
						this.accountNumberIndex[segmentIndex]='0';
						this.accountDescription[segmentIndex]='0';
						this.toastr.warning(pagedData.btiMessage.message);				
					}
    		    });
			}
			else
			{
				var currentSegment=segmentIndex;
				this.accountNumberIndex[currentSegment]='0';
				this.accountDescription[currentSegment]='0';
			}
	}

	createNewGlAccountNumber()
	{		
		var tempAccountNumberList=[];
        var accountDesc = this.accountDescription.join();
        accountDesc=accountDesc.replace(/,/g , " ");
        accountDesc=accountDesc.replace("0"," ");
		tempAccountNumberList.push({'accountNumberIndex':this.accountNumberIndex,'accountDescription':accountDesc})
		this.accountStructureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {
			
			if(data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED')
			{
				this.toastr.success(data.btiMessage.message);
				this.getAccountNumber();
				this.closeModal();
			}
			else{
				this.toastr.warning(data.btiMessage.message);
			}
		 });
		
	}

    openAccountWindow()
	{
		this.accountNumberIndex=[];
		this.accountDescription=[];
		this.isModalOpen = true;
	}
	closeModal()
    {
         this.isModalOpen = false;
    }
    
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }
    onFocus(elemId){
        this.commonService.changeInputNumber(elemId)
    }
    onlyNumber(Type:string,event){
        if(Type == 'minimumVATAmount')
        {
            this.model.minimumVATAmount = this.commonService.maxlenghtOnNumber(event)
        }
        if(Type == 'maximumVATAmount')
        {
            this.model.maximumVATAmount = this.commonService.maxlenghtOnNumber(event)
        }
        
    }
        
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
}