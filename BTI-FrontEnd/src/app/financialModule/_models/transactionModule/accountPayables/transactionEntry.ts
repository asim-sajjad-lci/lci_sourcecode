/**
 * A model for AP Transaction Entry
 */
export class TransactionEntrySetup {
  apTransactionNumber: string;
  apTransactionType: string;
  batchID: string;
  apTransactionDescription: string;
  apTransactionDate: string;
  vendorID: string;
  vendorName: string;
  vendorNameArabic: string;
  currencyID: string;
  paymentTermsID: string;
  purchasesAmount: string;
  apTransactionTradeDiscount: string;
  apTransactionFreightAmount: string;
  apTransactionMiscellaneous: string;
  apTransactionVATAmount: string;
  apTransactionFinanceChargeAmount: string;
  apTransactionCreditMemoAmount:number;
  apTransactionTotalAmount:number;
  apTransactionCashAmount:number;
  apTransactionCheckAmount:number;
  apTransactionCreditCardAmount:number;
  apTransactionDebitMemoAmount:number;
  apServiceRepairAmount:number;
  apTransactionWarrantyAmount:string;
  apReturnAmount:number;
  checkbookID:number;
  checkNumber:number;
  creditCardID:number;
  creditCardNumber:number;
  creditCardExpireYear:number;
  creditCardExpireMonth:number;
  apTransactionStatus:number;
  exchangeTableIndex:number;
  exchangeTableRate:number;
  transactionVoid:number;
  shippingMethodId:number;
  vatScheduleID:number;
  paymentTypeId:number;
  isPayment:boolean;
  paymentAmount:string;
  
  //initializing AP Transaction Entry Setup parameters
    constructor (apTransactionNumber: string,apTransactionType: string,batchID: string,apTransactionDescription: string,apTransactionDate: string,vendorID: string,
      vendorName: string,vendorNameArabic: string,currencyID: string,paymentTermsID: string,purchasesAmount: string,apTransactionTradeDiscount: string,apTransactionFreightAmount: string,
      apTransactionMiscellaneous: string,apTransactionVATAmount: string,apTransactionFinanceChargeAmount: string,apTransactionCreditMemoAmount:number,apTransactionTotalAmount:number,
      apTransactionCashAmount:number,apTransactionCheckAmount:number,apTransactionCreditCardAmount:number,apTransactionDebitMemoAmount:number,apServiceRepairAmount:number,
      apTransactionWarrantyAmount:string,apReturnAmount:number,checkbookID:number,checkNumber:number,creditCardID:number,creditCardNumber:number,creditCardExpireYear:number,
      creditCardExpireMonth:number,apTransactionStatus:number,exchangeTableIndex:number,exchangeTableRate:number,transactionVoid:number,shippingMethodId:number,vatScheduleID:number,
      paymentTypeId:number,isPayment:boolean,paymentAmount:string )
        {
                    this.apTransactionNumber = apTransactionNumber;
                    this.apTransactionType = apTransactionType;
                    this.batchID = batchID;
                    this.apTransactionDescription = apTransactionDescription;
                    this.apTransactionDate = apTransactionDate;
                    this.vendorID = vendorID;
                    this.vendorName = vendorName;
                    this.vendorNameArabic = vendorNameArabic;
                    this.currencyID = currencyID;
                    this.paymentTermsID = paymentTermsID;
                    this.purchasesAmount = purchasesAmount;
                    this.apTransactionTradeDiscount = apTransactionTradeDiscount;
                    this.apTransactionFreightAmount = apTransactionFreightAmount;
                    this.apTransactionMiscellaneous = apTransactionMiscellaneous;
                    this.apTransactionVATAmount = apTransactionVATAmount;
                    this.apTransactionFinanceChargeAmount = apTransactionFinanceChargeAmount;
                    this.apTransactionCreditMemoAmount = apTransactionCreditMemoAmount;
                    this.apTransactionTotalAmount = apTransactionTotalAmount;
                    this.apTransactionCashAmount = apTransactionCashAmount;
                    this.apTransactionCheckAmount = apTransactionCheckAmount;
                    this.apTransactionCreditCardAmount = apTransactionCreditCardAmount;
                    this.apTransactionDebitMemoAmount = apTransactionDebitMemoAmount;
                    this.apServiceRepairAmount = apServiceRepairAmount;
                    this.apTransactionWarrantyAmount = apTransactionWarrantyAmount;
                    this.apReturnAmount = apReturnAmount;
                    this.checkbookID = checkbookID;
                    this.checkNumber = checkNumber;
                    this.creditCardID = creditCardID;
                    this.creditCardNumber = creditCardNumber;
                    this.creditCardExpireYear = creditCardExpireYear;
                    this.creditCardExpireMonth = creditCardExpireMonth;
                    this.apTransactionStatus = apTransactionStatus;
                    this.exchangeTableIndex = exchangeTableIndex;
                    this.exchangeTableRate = exchangeTableRate;
                    this.transactionVoid = transactionVoid;
                    this.shippingMethodId = shippingMethodId;
                    this.vatScheduleID = vatScheduleID;
                    this.paymentTypeId = paymentTypeId;
                    this.isPayment = isPayment;
                    this.paymentAmount = paymentAmount;
        }            
}  

