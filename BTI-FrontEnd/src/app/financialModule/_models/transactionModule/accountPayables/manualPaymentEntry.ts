/**
 * A model for Cash Receipt Entry
 */
export class ManualPaymentEntry{
    manualPaymentNumber: string;
    manualPaymentDescription: string;
    manualPaymentCreateDate: string;
    batchID: string;
    vendorID: number;
    currencyID: string;
    manualPaymentType: string;
    manualPaymentAmount: string;
    checkBookId: string;
    checkNumber: string;
    creditCardID: string;
    creditCardNumber: string;
    creditCardExpireMonth: string;
    creditCardExpireYear: string;
    exchangeTableIndex: string;
    exchangeTableRate: number;
    transactionVoid:number
  
    //initializing Manual Payment Entry Setup parameters
      constructor (manualPaymentNumber: string,manualPaymentDescription: string,manualPaymentCreateDate: string,batchID: string,vendorID: number,
          currencyID: string,manualPaymentType: string,manualPaymentAmount: string,
          checkBookId: string,checkNumber: string,
          creditCardID: string,creditCardNumber: string,creditCardExpireMonth: string,
          creditCardExpireYear: string, exchangeTableIndex: string,exchangeTableRate: number,transactionVoid:number)
          {
                      this.manualPaymentNumber= manualPaymentNumber;
                      this.manualPaymentDescription= manualPaymentDescription;
                      this.manualPaymentCreateDate= manualPaymentCreateDate;
                      this.batchID= batchID;
                      this.vendorID= vendorID;
                      this.currencyID= currencyID;
                      this.exchangeTableIndex= exchangeTableIndex;
                      this.exchangeTableRate= exchangeTableRate;
                      this.manualPaymentType= manualPaymentType;
                      this.manualPaymentAmount= manualPaymentAmount;
                      this.checkBookId= checkBookId;
                      this.checkNumber= checkNumber;
                      this.creditCardID= creditCardID;
                      this.creditCardNumber= creditCardNumber;
                      this.creditCardExpireMonth= creditCardExpireMonth;
                      this.creditCardExpireYear= creditCardExpireYear;
                      this.transactionVoid= transactionVoid;
          }
  }