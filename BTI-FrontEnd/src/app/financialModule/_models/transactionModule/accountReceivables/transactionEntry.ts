/**
 * A model for Transaction Entry
 */
export class TransactionEntrySetup {
  arTransactionNumber: string;
  arTransactionType: string;
  batchID: string;
  arTransactionDescription: string;
  arTransactionDate: string;
  customerID: string;
  customerName: string;
  customerNameArabic: string;
  currencyID: string;
  paymentTermsID: string;
  arTransactionCost: string;
  arTransactionSalesAmount: string;
  arTransactionTradeDiscount: string;
  arTransactionFreightAmount: string;
  arTransactionMiscellaneous: string;
  arTransactionVATAmount: string;
  arTransactionDebitMemoAmount: string;
  arTransactionFinanceChargeAmount: string;
  arTransactionWarrantyAmount: string;
  arTransactionCreditMemoAmount: number;
  arTransactionTotalAmount: number;
  arTransactionCashAmount: number;
  arTransactionCheckAmount: number;
  arTransactionCreditCardAmount: number;
  salesmanID: number;
  checkbookID: number;
  checkNumber: number;
  cashReceiptNumber: string;
  creditCardID: number;
  creditCardNumber: number;
  creditCardExpireYear: number;
  creditCardExpireMonth: number;
  arTransactionStatus: number;
  exchangeTableIndex: number;
  exchangeTableRate: number;
  transactionVoid: number;
  shippingMethodID: number;
  vatScheduleID: number;
  paymentTypeId: number;
  territoryId :number;
  isPayment:boolean;
  paymentAmount:number;
  arServiceRepairAmount:number;
  arReturnAmount:number;
  paymentTransactionTypeId:number;
  
  //initializing Transaction Entry Setup parameters
    constructor (arTransactionNumber: string,arTransactionType: string,batchID: string,arTransactionDescription: string,arTransactionDate: string,customerID: string,customerName: string,customerNameArabic: string,
                 currencyID: string,paymentTermsID: string,arTransactionCost: string,arTransactionSalesAmount: string,arTransactionTradeDiscount: string,arTransactionFreightAmount: string,arTransactionMiscellaneous: string,  
                 arTransactionVATAmount: string,arTransactionDebitMemoAmount: string,arTransactionFinanceChargeAmount: string,arTransactionWarrantyAmount: string,arTransactionCreditMemoAmount: number,arTransactionTotalAmount: number,
                 arTransactionCashAmount: number,arTransactionCheckAmount: number,arTransactionCreditCardAmount: number,salesmanID: number,checkbookID: number,checkNumber: number,cashReceiptNumber: string,creditCardID: number,creditCardNumber: number,creditCardExpireYear: number,
                 creditCardExpireMonth: number,arTransactionStatus: number,exchangeTableIndex: number,exchangeTableRate: number,transactionVoid: number,shippingMethodID: number,vatScheduleID: number,paymentTypeId: number,territoryId:number,
                 isPayment:boolean,paymentAmount:number,arServiceRepairAmount:number,arReturnAmount:number,paymentTransactionTypeId:number
     )
        {
                    this.arTransactionNumber= arTransactionNumber;
                    this.arTransactionType= arTransactionType;
                    this.batchID= batchID;
                    this.arTransactionDescription= arTransactionDescription;
                    this.arTransactionDate= arTransactionDate;
                    this.customerID= customerID;
                    this.customerName= customerName;
                    this.customerNameArabic= customerNameArabic;
                    this.currencyID= currencyID;
                    this.paymentTermsID= paymentTermsID;
                    this.arTransactionCost= arTransactionCost;
                    this.arTransactionSalesAmount= arTransactionSalesAmount;
                    this.arTransactionTradeDiscount= arTransactionTradeDiscount;
                    this.arTransactionFreightAmount= arTransactionFreightAmount;
                    this.arTransactionMiscellaneous= arTransactionMiscellaneous;
                    this.arTransactionVATAmount= arTransactionVATAmount;
                    this.arTransactionDebitMemoAmount= arTransactionDebitMemoAmount;
                    this.arTransactionFinanceChargeAmount= arTransactionFinanceChargeAmount;
                    this.arTransactionWarrantyAmount= arTransactionWarrantyAmount;
                    this.arTransactionCreditMemoAmount= arTransactionCreditMemoAmount;
                    this.arTransactionTotalAmount= arTransactionTotalAmount;
                    this.arTransactionCashAmount= arTransactionCashAmount;
                    this.arTransactionCheckAmount= arTransactionCheckAmount;
                    this.arTransactionCreditCardAmount= arTransactionCreditCardAmount;
                    this.salesmanID= salesmanID;
                    this.checkbookID= checkbookID;
                    this.checkNumber= checkNumber;
                    this.cashReceiptNumber= cashReceiptNumber;
                    this.creditCardID= creditCardID;
                    this.creditCardNumber= creditCardNumber;
                    this.creditCardExpireYear= creditCardExpireYear;
                    this.creditCardExpireMonth= creditCardExpireMonth;
                    this.arTransactionStatus= arTransactionStatus;
                    this.exchangeTableIndex= exchangeTableIndex;
                    this.exchangeTableRate= exchangeTableRate;
                    this.transactionVoid= transactionVoid;
                    this.shippingMethodID= shippingMethodID;
                    this.vatScheduleID= vatScheduleID;
                    this.paymentTypeId= paymentTypeId;
                    this.territoryId= territoryId;
                    this.isPayment=isPayment;
                    this.paymentAmount=paymentAmount;
                    this.arServiceRepairAmount=arServiceRepairAmount;
                    this.arReturnAmount=arReturnAmount;
                    this.paymentTransactionTypeId=paymentTransactionTypeId;
        }
}                