/**
 * A model for AR Batch Setup
 */
export class BatchSetup {
  batchId: string;
  description: string;
  transactionTypeId: number;
  transactionType: string;
  totalTransactions: number;
  quantityTotal: string;
//   postingDate: string; 

  //initializing AR Batch Setup parameters
    constructor (batchId: string,description: string,transactionTypeId: number,transactionType: string,totalTransactions: number,quantityTotal: string
//     ,postingDate: string
    )
        {
                this.batchId= batchId;
                this.description= description;
                this.transactionTypeId= transactionTypeId;
                this.transactionType= transactionType;
                this.totalTransactions= totalTransactions;
                this.quantityTotal= quantityTotal;
                // this.postingDate= postingDate;
        }
}