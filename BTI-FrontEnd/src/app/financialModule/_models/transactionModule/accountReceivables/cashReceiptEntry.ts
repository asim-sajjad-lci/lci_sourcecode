/**
 * A model for Cash Receipt Entry
 */
export class ARCashReceiptSetup {
  cashReceiptNumber: string;
  cashReceiptDescription: string;
  cashReceiptCreateDate: string;
  transactionVoid: number;
  batchID: string;
  customerNumber: string;
  currencyID: string;
  exchangeTableIndex: string;
  exchangeTableRate: string;
  cashReceiptType: number;
  cashReceiptAmount: string;
  checkBookId: string;
  checkNumber: string;
  creditCardID: string;
  creditCardNumber: string;
  creditCardExpireMonth: number;
  creditCardExpireYear: number;

  //initializing Cash Receipt Entry Setup parameters
    constructor (cashReceiptNumber: string,cashReceiptDescription: string,cashReceiptCreateDate: string,transactionVoid: number,batchID: string,customerNumber: string,
        currencyID: string,exchangeTableIndex: string,exchangeTableRate: string,cashReceiptType: number,cashReceiptAmount: string,checkBookId: string,checkNumber: string,
        creditCardID: string,creditCardNumber: string,creditCardExpireMonth: number,creditCardExpireYear: number)
        {
                    this.cashReceiptNumber= cashReceiptNumber;
                    this.cashReceiptDescription= cashReceiptDescription;
                    this.cashReceiptCreateDate= cashReceiptCreateDate;
                    this.transactionVoid= transactionVoid;
                    this.batchID= batchID;
                    this.customerNumber= customerNumber;
                    this.currencyID= currencyID;
                    this.exchangeTableIndex= exchangeTableIndex;
                    this.exchangeTableRate= exchangeTableRate;
                    this.cashReceiptType= cashReceiptType;
                    this.cashReceiptAmount= cashReceiptAmount;
                    this.checkBookId= checkBookId;
                    this.checkNumber= checkNumber;
                    this.creditCardID= creditCardID;
                    this.creditCardNumber= creditCardNumber;
                    this.creditCardExpireMonth= creditCardExpireMonth;
                    this.creditCardExpireYear= creditCardExpireYear;
        }
}