/**
 * A model for GL Cash Receipt Setup
 */
export class CashReceiptSetup {
  receiptNumber: string;
  checkBookId: string;
  currencyID: string;
  customerId: string;
  vendorId:string;
  receiptDate: string;
  receiptType: string;
  paymentMethod: number;
  accountTableRowIndex: string;
  receiptDescription: string;
  receiptAmount: number;
  checkNumber: string;
  bankName: string;
  creditCardID: string;
  creditCardNumber: string;
  creditCardExpireYear: number;
  creditCardExpireMonth: number;
  vatScheduleID: string;
  vatAmount: number;
  depositorName: string;
  batchID:string;
  exchangeTableIndex: string;
  exchangeRate: number;

   //initializing Setup parameters
    constructor (receiptNumber: string,checkBookId: string,currencyID: string,customerId:string,vendorId:string,receiptDate:string,receiptType:string,paymentMethod:number,
        accountTableRowIndex:string,receiptDescription:string,receiptAmount:number,checkNumber:string,bankName:string,creditCardID:string,creditCardNumber:string,creditCardExpireYear:number,
        creditCardExpireMonth:number,vatScheduleID:string,vatAmount:number,depositorName:string,batchID:string,exchangeTableIndex:string,exchangeRate:number)
            {
                this.receiptNumber= receiptNumber;
                this.checkBookId= checkBookId;
                this.currencyID= currencyID;
                this.customerId= customerId;
                this.receiptDate= receiptDate;
                this.receiptType= receiptType;
                this.paymentMethod= paymentMethod;
                this.accountTableRowIndex= accountTableRowIndex;
                this.receiptDescription= receiptDescription;
                this.receiptAmount= receiptAmount;
                this.checkNumber= checkNumber;
                this.bankName= bankName;
                this.creditCardID= creditCardID;
                this.creditCardNumber= creditCardNumber;
                this.creditCardExpireYear= creditCardExpireYear;
                this.creditCardExpireMonth= creditCardExpireMonth;
                this.vatScheduleID= vatScheduleID;
                this.vatAmount= vatAmount;
                this.depositorName= depositorName;
                this.batchID=batchID;
                this.exchangeTableIndex= exchangeTableIndex;
                this.exchangeRate= exchangeRate;
            }
}    