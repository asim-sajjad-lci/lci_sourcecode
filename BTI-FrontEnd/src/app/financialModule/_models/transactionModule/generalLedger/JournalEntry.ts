/**
 * A model for Journal Entry Setup
 */
export class JournalEntrySetup {
  id:number;
  journalID: string;
  journalNumber: string;
  glBatchId: string;
  transactionType: number;
  transactionDate: string;
  transactionReversingDate: string;
  sourceDocumentId: string;
  journalDescription: string;
  journalDescriptionArabic: string;
  currencyID: string;
  totalJournalEntryDebit: number;
  totalJournalEntryCredit: number;
  exchangeTableIndex: string;
  interCompany: boolean =true;
  journalEntryDetailsList=[];
  originalJournalID: string;
  correction: boolean =false;
  exchangeRate: number;
  exchangeDate: string;
  sourceDocument: string;

  //initializing Journal Entry Setup parameters
    constructor (id:number,journalID: string,journalNumber: string,glBatchId: string,transactionType: number,transactionDate: string,
        transactionReversingDate: string,sourceDocumentId: string,journalDescription: string,
        journalDescriptionArabic: string,currencyID: string,totalJournalEntryDebit: number,
        totalJournalEntryCredit: number,exchangeTableIndex: string,interCompany: boolean,
        journalEntryDetailsList: any[],exchangeRate: number,exchangeDate: string, sourceDocument:string)
        {
                this.id=id;
                this.journalID= journalID;
                this.journalNumber = journalNumber;
                this.glBatchId= glBatchId;
                this.transactionType= transactionType;
                this.transactionDate= transactionDate;
                this.transactionReversingDate= transactionReversingDate;
                this.sourceDocumentId= sourceDocumentId;
                this.journalDescription= journalDescription;
                this.journalDescriptionArabic= journalDescriptionArabic;
                this.currencyID= currencyID;
                this.totalJournalEntryDebit= totalJournalEntryDebit;
                this.totalJournalEntryCredit= totalJournalEntryCredit;
                this.exchangeTableIndex= exchangeTableIndex;
                this.interCompany= interCompany;
                this.journalEntryDetailsList= journalEntryDetailsList;
                this.exchangeRate= exchangeRate;
                this.exchangeDate= exchangeDate;
                this.sourceDocument = sourceDocument;
        }

     
}