/**
 * A model for Bank Transfer Entry Setup
 */
export class BankTransferEntrySetup {
  bankTransferNumber: string;
  transferDate: string;
  batchID: string;
  currencyID: string;
  exchangeTableID: string;
  companyTransferFrom: string;
  checkbookIDFrom: string;
  commentFrom: string;
  transferAmountFrom: string;
  rateCalculateMethodFrom: string;
  exchangeRateFrom: string;
  companyTransferTo: string;
  checkbookIDTo: string;
  commentTo: string;
  transferAmountTo: string;
  rateCalculateMethodTo: string;
  exchangeRateTo: string;

  //initializing Bank Transfer Entry Setup parameters
    constructor (bankTransferNumber: string,transferDate: string,batchID: string,currencyID: string,exchangeTableID: string,companyTransferFrom: string,checkbookIDFrom: string,commentFrom: string,transferAmountFrom: string,rateCalculateMethodFrom: string,exchangeRateFrom: string,companyTransferTo: string,checkbookIDTo: string,commentTo: string,transferAmountTo: string,rateCalculateMethodTo: string,exchangeRateTo: string)
        {
                this.bankTransferNumber= bankTransferNumber;
                this.transferDate= transferDate;
                this.batchID= batchID;
                this.currencyID= currencyID;
                this.exchangeTableID= exchangeTableID;
                this.companyTransferFrom= companyTransferFrom;
                this.checkbookIDFrom= checkbookIDFrom;
                this.commentFrom= commentFrom;
                this.transferAmountFrom= transferAmountFrom;
                this.rateCalculateMethodFrom= rateCalculateMethodFrom;
                this.exchangeRateFrom= exchangeRateFrom;
                this.companyTransferTo= companyTransferTo;
                this.checkbookIDTo= checkbookIDTo;
                this.commentTo= commentTo;
                this.transferAmountTo= transferAmountTo;
                this.rateCalculateMethodTo= rateCalculateMethodTo;
                this.exchangeRateTo= exchangeRateTo;
        }
}        