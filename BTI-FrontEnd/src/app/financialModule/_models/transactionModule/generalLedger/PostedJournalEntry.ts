/**
 * A model for Posted Journal Entry Setup
 */
export class PostedJournalEntry {

    rowIndex:number;
    journalEntryID: string;
    journalDescription: string;
    journalNumber: string;
    journalDescriptionArabic: string;
    transactionDate: string;
    transactionPostingDate: string;
    transaxtionSource: string;
    accountTableRowIndex: string;
    debitAmount: number;
    creadiAmount: number;
    totalDebitAmmount: number;
    totalCreditAmmount: number;
    currency:string;
    sourceDocument:string;
    sourceDocumentId: number;
    originalTransactionNumber:string;
  
    //initializing Journal Entry Setup parameters
      constructor (rowIndex:number,journalNumber:string,journalEntryID: string,journalDescription: string,
            journalDescriptionArabic: string,transactionDate: string,transactionPostingDate: string,
            transaxtionSource: string,accountTableRowIndex: string,
            debitAmount: number,creadiAmount: number,totalDebitAmmount:number,totalCreditAmmount:number,sourceDocument:string,sourceDocumentId:number,originalTransactionNumber:string)
          {
              this.rowIndex = rowIndex;
              this.journalNumber = journalNumber;
              this.journalEntryID = journalEntryID;
              this.journalDescription = journalDescription;
              this.journalDescriptionArabic = journalDescriptionArabic;
              this.transactionDate = transactionDate;
              this.transactionPostingDate = transactionPostingDate;
              this.transaxtionSource = transaxtionSource;
              this.accountTableRowIndex = accountTableRowIndex;
              this.debitAmount = debitAmount;
              this.creadiAmount = creadiAmount;  
              this.totalDebitAmmount = totalDebitAmmount;
              this.totalCreditAmmount = totalCreditAmmount;  
              this.sourceDocument = sourceDocument;
              this.sourceDocumentId = sourceDocumentId;
              this.originalTransactionNumber = originalTransactionNumber;
          }
  
       
  }