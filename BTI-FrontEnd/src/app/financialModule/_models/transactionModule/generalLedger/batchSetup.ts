/**
 * A model for GL Batch Setup
 */
export class BatchSetup {
  batchId: string;
  description: string;
  transactionTypeId: number;
  transactionType: string;
  totalTransactions: number;
  quantityTotal: string;
  sourceDocument: number;
//   postingDate: string; 

  //initializing GL Batch Setup parameters
    constructor (batchId: string,description: string,transactionTypeId: number,transactionType: string,totalTransactions: number,quantityTotal: string, sourceDocument:number
//     ,postingDate: string
    )
        {
                this.batchId= batchId;
                this.description= description;
                this.transactionTypeId= transactionTypeId;
                this.transactionType= transactionType;
                this.totalTransactions= totalTransactions;
                this.quantityTotal= quantityTotal;
                this.sourceDocument = sourceDocument;
                // this.postingDate= postingDate;
        }
}