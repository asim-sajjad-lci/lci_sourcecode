/**
 * A model for Clearing Entry Setup
 */
export class ClearingEntry {
  journalID: string;
  balanceType: number;
  transactionDate: string;
  transactionDescription: string;
  glBatchId: string;
  sourceDocumentId: string;
  journalEntryDetailsList=[];

  //initializing parameters
    constructor (journalID: string,balanceType: number,transactionDate: string,transactionDescription: string,glBatchId: string,sourceDocumentId: string,journalEntryDetailsList: any[])
        {
                this.journalID= journalID;
                this.balanceType= balanceType;
                this.transactionDate= transactionDate;
                this.transactionDescription= transactionDescription;
                this.glBatchId= glBatchId;
                this.sourceDocumentId= sourceDocumentId;
                this.journalEntryDetailsList= journalEntryDetailsList;
        }
    }