/**
 * Model for Payment Voucher Header
 */

export class PaymentVoucher {
    id: number;
    paymentVoucherId: number;
    paymentTypeID: string;
    auditTrial: number;
    auditTrialStr: string;
    glBatcheId: string;
    glBatcheStr: string;
    creditCardExpireDate= [];
    creditCardId: number;
    creditCardStr: number;
    creditCardNumber: number;
    voucherDescription: string;
    voucherDescriptionArabic: string;
    transactionDate: string;
    currencyID: string;
    exchangeTableIndex: number;
    exchangeRate: number;
    totalPaymentVoucherCredit: number;
    originalTotalPaymentVoucherDebit: number;

    accountTableRowIndex: string;
    accountNumber: string;
    sourceDcoument: number;
    
    totalPaymentVoucher: number;
    totalPaymentVoucherDebit: number;

    originalTotalPaymentVoucher: number;
    
    originalTotalPaymentVoucherCredit: number;
    
    voucherDetailList: any[];
    checkbookID: string;
    checkNumber: number;




}