export class AccountReceivableSetup {
  ageingBy: number;
  applyByDefault: number;
  checkbookId: string;
  priceLevel: string;
  compoundFinanceCharge: boolean;
  arTrackingDiscountAvailable: boolean;
  deleteUnpostedPrintedDocuments: boolean;
  printHistoricalAgedTrialBalance: boolean;
  arPayCommissionsInvoicePay: boolean;
  creditLimitPassword: number;
  customerHoldPassword: number;
  waivedFinanceChargePassword: number;
  writeoffPassword: number;
  lastDateBalanceForwardAge: string;
  deleteUnpostedPrintedDocumentDate: string;
  lastDateStatementPrinted: string;
  lastFinanceChargeDate: string;
  miscVatScheduleId: number;
  salesVatScheduleId: number;
  freightVatScheduleId: number;
  userDefine1: string;
  userDefine2: string;
  userDefine3: string;
  dtoRMPeriodSetupsList:string[];

  constructor (ageingBy: number,applyByDefault: number,checkbookId: string, priceLevel: string, compoundFinanceCharge: boolean,
        arTrackingDiscountAvailable: boolean,deleteUnpostedPrintedDocuments: boolean,printHistoricalAgedTrialBalance: boolean,
        arPayCommissionsInvoicePay: boolean, creditLimitPassword: number,customerHoldPassword: number,waivedFinanceChargePassword: number,
        writeoffPassword: number,lastDateBalanceForwardAge: string,deleteUnpostedPrintedDocumentDate: string,
        lastDateStatementPrinted: string,lastFinanceChargeDate: string, miscVatScheduleId: number, salesVatScheduleId: number,
        freightVatScheduleId: number,userDefine1: string,userDefine2: string,userDefine3: string,dtoRMPeriodSetupsList:string[])
   {
                    this.ageingBy=ageingBy;
                    this.applyByDefault= applyByDefault;
                    this.checkbookId= checkbookId;
                    this.priceLevel= priceLevel;
                    this.compoundFinanceCharge= compoundFinanceCharge;
                    this.arTrackingDiscountAvailable= arTrackingDiscountAvailable;
                    this.deleteUnpostedPrintedDocuments= deleteUnpostedPrintedDocuments;
                    this.printHistoricalAgedTrialBalance= printHistoricalAgedTrialBalance;
                    this.arPayCommissionsInvoicePay= arPayCommissionsInvoicePay;
                    this.creditLimitPassword= creditLimitPassword;
                    this.customerHoldPassword= customerHoldPassword;
                    this.waivedFinanceChargePassword= waivedFinanceChargePassword;
                    this.writeoffPassword= writeoffPassword;
                    this.lastDateBalanceForwardAge= lastDateBalanceForwardAge;
                    this.deleteUnpostedPrintedDocumentDate= deleteUnpostedPrintedDocumentDate;
                    this.lastDateStatementPrinted= lastDateStatementPrinted;
                    this.lastFinanceChargeDate= lastFinanceChargeDate;
                    this.miscVatScheduleId= miscVatScheduleId;
                    this.salesVatScheduleId= salesVatScheduleId;
                    this.freightVatScheduleId= freightVatScheduleId;
                    this.userDefine1= userDefine1;
                    this.userDefine2= userDefine2;
                    this.userDefine3= userDefine3;
                    this.dtoRMPeriodSetupsList=dtoRMPeriodSetupsList;
   }
}