

export interface IAccountStatementReport {
    printDate: string;
    printTime: string;
    startDate: string;
    endDate: string;
    accountStatmentMasterRecordList: IMaster[];
}

export interface IMaster {
    accountNumber: string;
    description: string;
    accountCategory: string;
    beginningBalance: number;
    totalDebitAmount: number;
    totalCreditAmount: number;
    totalBalance: number;
    accountStatmentDetailRecordList: IDetail[];
}

export interface IDetail {
    journalNo: string;
    journalDate: string;
    distributionReference: string;
    debitAmount: number;
    creditAmount: number;
    balance: number;
}
