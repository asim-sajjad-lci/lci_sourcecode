/**
 * A model for Account Type Setup
 */
export class AuditTrial {
    sourceCode:string;
    seriesId: string;
    sourceDocument:string;
    sequenceNumber:string;
    seriesIndex:number;
    seriesNumber:number;
    seriesName: string;
    //initializing Account Type parameters
    constructor (sourceCode?:string ,seriesId? : string,sourceDocument?: string,sequenceNumber?:string,seriesIndex?:number,seriesNumber?:number,seriesName?:string)
    {
        this.seriesName = seriesName;
        this.sourceCode = sourceCode;
        this.seriesId= seriesId;
        this.sourceDocument= sourceDocument;
        this.sequenceNumber=sequenceNumber;
        this.seriesIndex=seriesIndex;
        this.seriesNumber=seriesNumber;
    }
}