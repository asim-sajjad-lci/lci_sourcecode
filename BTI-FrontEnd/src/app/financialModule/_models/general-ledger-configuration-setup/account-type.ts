/**
 * A model for Account Type Setup
 */
export class AccountType {
    accountTypeId:string;
    accountTypeName: string;
    accountTypeNameArabic:string;
    //initializing Account Type parameters
    constructor (accountTypeId:string ,accountTypeName : string,accountTypeNameArabic: string)
    {
        this.accountTypeId = accountTypeId;
        this.accountTypeName= accountTypeName;
        this.accountTypeNameArabic= accountTypeNameArabic;
    }
}