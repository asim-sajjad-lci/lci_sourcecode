/**
 * A model for Account Category
 */
export class AccountCategory {
    accountCategoryId:string;
    accountCategoryDescription: string;
    accountCategoryDescriptionArabic:string;
    preDefined:number;
    //initializing Account Category parameters
    constructor (accountCategoryId:string ,accountCategoryDescription : string,accountCategoryDescriptionArabic: string,preDefined:number)
    {
        this.accountCategoryId = accountCategoryId;
        this.accountCategoryDescription= accountCategoryDescription;
        this.accountCategoryDescriptionArabic= accountCategoryDescriptionArabic;
        this.preDefined=preDefined;
    }
}