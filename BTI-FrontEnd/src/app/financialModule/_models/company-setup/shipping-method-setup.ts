/**
 * A model for ShippingMethodSetup
 */
export class ShippingMethodSetup {
    shipmentMethodId: string;
    shipmentDescription:string;
    shipmentDescriptionArabic:string;
    shipmentCarrierAccount:string;
    shipmentCarrier : string;
    shipmentCarrierArabic :string ;
    shipmentCarrierContactName:string;
    shipmentPhoneNumber:number;
    shipmentMethodType:number;
    //initializing shippingMethodSetup parameters
    constructor (shipmentMethodId: string, shipmentDescription:string, shipmentDescriptionArabic:string,shipmentCarrierAccount:string, shipmentCarrier : string,
                shipmentCarrierArabic :string,shipmentCarrierContactName:string,shipmentPhoneNumber:number,shipmentMethodType:number)
    {
        this.shipmentMethodId= shipmentMethodId;
        this.shipmentDescription= shipmentDescription;
        this.shipmentDescriptionArabic= shipmentDescriptionArabic;
        this.shipmentCarrierAccount=shipmentCarrierAccount;
        this.shipmentCarrier=shipmentCarrier;
        this.shipmentCarrierArabic=shipmentCarrierArabic;
        this.shipmentCarrierContactName= shipmentCarrierContactName;
        this.shipmentPhoneNumber=shipmentPhoneNumber;
        this.shipmentMethodType=shipmentMethodType;
 
    }
}