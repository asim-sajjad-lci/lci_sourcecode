/**
 * A model for Var Detail Setup
 */
export class VatDetailSetup {
    vatScheduleId: string;
    vatDescription:string;
    vatDescriptionArabic:string;
    vatSeriesType:number;
    vatIdNumber : number;
    vatBaseOn :number ;
    basperct:number;
    maximumVATAmount:number;
    minimumVATAmount:number;
    accountRowId : number;
    lastYearTotalSalesPurchase :number ;
    lastYearSalesPurchaseTaxes:number;
    lastYearTaxableSalesPurchase:number;
    ytdTotalSalesPurchase:number;
    ytdTotalSalesPurchaseTaxes :number ;
    ytdTotalTaxableSalesPurchase:number;
  
    //initializing Var Detail Setup parameters
    constructor (vatScheduleId: string, vatDescription:string, vatDescriptionArabic:string,vatSeriesType:number,
                vatIdNumber : number, vatBaseOn:number,
                basperct :number,maximumVATAmount:number,minimumVATAmount:number,accountRowId:number,
                lastYearTotalSalesPurchase:number,lastYearSalesPurchaseTaxes:number,lastYearTaxableSalesPurchase:number,
                ytdTotalSalesPurchase:number,ytdTotalSalesPurchaseTaxes:number,ytdTotalTaxableSalesPurchase:number)
    {
        this.vatScheduleId= vatScheduleId;
        this.vatDescription= vatDescription;
        this.vatDescriptionArabic= vatDescriptionArabic;
        this.vatSeriesType=vatSeriesType;
        this.vatIdNumber=vatIdNumber;
        this.vatBaseOn=vatBaseOn;
        this.basperct= basperct;
        this.maximumVATAmount=maximumVATAmount;
        this.minimumVATAmount=minimumVATAmount;
        this.accountRowId=accountRowId;
        this.lastYearTotalSalesPurchase= lastYearTotalSalesPurchase;
        this.lastYearSalesPurchaseTaxes=lastYearSalesPurchaseTaxes;
        this.lastYearTaxableSalesPurchase=lastYearTaxableSalesPurchase;
        this.ytdTotalSalesPurchase=ytdTotalSalesPurchase;
        this.ytdTotalSalesPurchaseTaxes=ytdTotalSalesPurchaseTaxes;
        this.lastYearTotalSalesPurchase= lastYearTotalSalesPurchase;
        this.ytdTotalTaxableSalesPurchase=ytdTotalTaxableSalesPurchase;
    }
}