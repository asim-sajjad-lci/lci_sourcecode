/**
 * A model for payment Term Setup
 */
export class PaymentTermSetup {
    paymentTermId: string;
    description:string;
    arabicDescription:string;
    discountPeriod:number;
    discountType : number;
    discountTypeValue :number ;
    discountPeriodDays:number;
    dueDays:number;
    dueType:number;
    dueTypeValue:string;
    //initializing payment Term Setup parameters
    constructor (paymentTermId: string, description:string, arabicDescription:string,discountPeriod:number, discountType : number,
                discountTypeValue :number,discountPeriodDays:number,dueDays:number,dueType:number,dueTypeValue:string)
    {
        this.paymentTermId= paymentTermId;
        this.description= description;
        this.arabicDescription= arabicDescription;
        this.discountPeriod=discountPeriod;
        this.discountType=discountType;
        this.discountTypeValue= discountTypeValue;
        this.discountPeriodDays=discountPeriodDays;
        this.dueDays=dueDays;
        this.dueType=dueType;
        this.dueTypeValue=dueTypeValue;
    }
}