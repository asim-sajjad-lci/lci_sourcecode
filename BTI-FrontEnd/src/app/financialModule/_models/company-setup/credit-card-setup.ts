/**
 * A model for credit card Setup
 */
export class CreditCardSetup {
    checkBookId: string;
    creditCardNameArabic:string;
    creditCardName:string;
    cardType : string;
    cardId:string;
    accountTableRowIndex:number;
    accountNumber:string;
       
  //initializing credit card Setup parameters
    constructor (checkBookId: string, creditCardNameArabic:string,creditCardName:string, cardType:string,cardId:string,accountTableRowIndex:number,accountNumber:string)
    { this.checkBookId= checkBookId;
        this.creditCardNameArabic= creditCardNameArabic;
        this.creditCardName= creditCardName;
        this.cardType=cardType;
        this.cardId=cardId;
        this.accountTableRowIndex=accountTableRowIndex;
        this.accountNumber=accountNumber;
       
    }
}