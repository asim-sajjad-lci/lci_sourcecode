/**
 * A model for Configuration Setup
 */
export class Configuration {
    seriesId: string;
    seriesNamePrimary:string;
    seriesNameSecondary:string;
    originNamePrimary:string;
    originNameSecondary:string;
    //initializing Configuration Setup parameters
    constructor (seriesId : string,seriesNamePrimary: string, seriesNameSecondary:string, originNamePrimary:string,originNameSecondary:string)
    {
        this.seriesId= seriesId;
        this.seriesNamePrimary= seriesNamePrimary;
        this.seriesNameSecondary= seriesNameSecondary;
        this.originNamePrimary=originNamePrimary;
        this.originNameSecondary=originNamePrimary;
    }
}