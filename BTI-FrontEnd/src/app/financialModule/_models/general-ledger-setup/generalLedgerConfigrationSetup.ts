/**
 * A model for General Ledger Configuration Setup
 */
export class GeneralLedgerConfigurationSetup {
    accountTypeId:string;
    accountTypeName:string;
    accountTypeNameArabic:string;
    accountCategoryDescription:string;
    accountCategoryDescriptionArabic:string;
    sourceCode:string;
    seriesId:number;
    seriesIndex:number;
    sourceDocument:string;

    //initializing General Ledger Configuration  Setup parameters
    constructor (accountTypeId:string,accountTypeName:string ,accountTypeNameArabic : string,accountCategoryDescription: string,accountCategoryDescriptionArabic: string,sourceCode: string,seriesId: number,seriesIndex:number,sourceDocument: string)
    {
        this.accountTypeId = accountTypeId;  
        this.accountTypeName = accountTypeName;
        this.accountTypeNameArabic = accountTypeNameArabic;
        this.accountCategoryDescription = accountCategoryDescription;
        this.accountCategoryDescriptionArabic = accountCategoryDescriptionArabic;
        this.sourceCode = sourceCode;
        this.seriesId = seriesId;
        this.seriesIndex = seriesIndex;
        this.sourceDocument = sourceDocument;
    }
}

