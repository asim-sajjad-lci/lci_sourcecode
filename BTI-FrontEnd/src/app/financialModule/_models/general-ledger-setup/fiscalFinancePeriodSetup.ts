/**
 * A model for fiscalFinancePeriodSetup
 */
export class FiscalFinancePeriod {
    year:number;
    firstDay:string;
    lastDay:string;
    historicalYear:boolean;
    numberOfPeriods:number;
    dtoFiscalFinancialPeriodSetupDetail:string[]
   
    //initializing Configuration Setup parameters
    constructor (year:number,firstDay:string,historicalYear:boolean,numberOfPeriods:number,transactionDocumentDescriptionPrimary:string,transactionDocumentDescriptionSecondary:string,periodId:number,startPeriodDate:string,endPeriodDate:string,periodNamePrimary:string,periodNameSecondary:string,periodSeriesFinancial:boolean,periodSeriesSales:boolean,periodSeriesPurchase:boolean,periodSeriesInventory:boolean,periodSeriesProject:boolean,periodSeriesPayroll:boolean,dtoFiscalFinancialPeriodSetupDetail:string[])
    {
        this.year= year;
        this.firstDay= firstDay;
        this.historicalYear=historicalYear;
        this.numberOfPeriods=numberOfPeriods;
        this.dtoFiscalFinancialPeriodSetupDetail=dtoFiscalFinancialPeriodSetupDetail;
    }
}
