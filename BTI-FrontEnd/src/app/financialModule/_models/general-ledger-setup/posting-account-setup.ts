/**
 * A model for Configuration Setup
 */
export class PostingAccountSetup {
    postingId:string;
    seriesNumber: string;
    descriptionPrimary: string;
    descriptionSecondary:string;
    accountNumber:string;
    accountTableRowIndex:string;
    //initializing Configuration Setup parameters
    constructor (postingId:string,seriesNumber:string ,descriptionPrimary : string,descriptionSecondary: string,accountNumber:string,accountTableRowIndex:string)
    {
        this.postingId= postingId;
        this.seriesNumber = seriesNumber;
        this.descriptionPrimary= descriptionPrimary;
        this.descriptionSecondary= descriptionSecondary;
        this.accountNumber=accountNumber;
        this.accountTableRowIndex=accountTableRowIndex;
    }
}