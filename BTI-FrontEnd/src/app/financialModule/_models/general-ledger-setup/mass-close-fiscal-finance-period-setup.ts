/**
 * A model for Mass Close Fiscal Finance Period Setup
 */
export class MassCloseFiscalFinancePeriodSetup {
    year:number;
    seriesId: number;
    records:string[];
    originId:number;
    //initializing Mass Close Fiscal Finance Period Setup parameters
    constructor (year:number,seriesId: number,records:string[],originId:number)
    {
        this.year = year;
        this.seriesId= seriesId;
        this.originId= originId;
        this.records= records;
    }
}