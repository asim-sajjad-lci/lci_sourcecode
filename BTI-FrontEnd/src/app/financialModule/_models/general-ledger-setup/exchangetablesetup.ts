/**
 * A model for excahnge table set up
 */
export class ExchangeTableSetup {
    exchangeId: string;
    description:string;
    descriptionArabic:string;
    exchangeRateSource : string;
    exchangeRateSourceArabic : string;
    rateFrequency:string;
    rateVariance:string;
    currencyId:string;
    rateCalcMethod:string;
   
  //initializing Authorization parameters
    constructor (exchangeId: string, description:string,descriptionArabic:string,
        exchangeRateSource:string,exchangeRateSourceArabic:string,rateFrequency:string,rateVariance:string,
    currencyId:string,rateCalcMethod:string)
        {
    this.exchangeId=exchangeId;
    this.description=description;
    this.descriptionArabic=descriptionArabic;
    this.exchangeRateSource =exchangeRateSource;
    this.exchangeRateSourceArabic =exchangeRateSourceArabic;
    this.rateFrequency=rateFrequency;
    this.rateVariance=rateVariance;
    this.currencyId=currencyId;
    this.rateCalcMethod=rateCalcMethod;
            
        }
}