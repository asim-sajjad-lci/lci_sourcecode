/**
 * A model for currency set up
 */
export class CurrencySetup {
    currencyId: string;
    currencyDescription:string;
    currencyDescriptionArabic:string;
    currencySymbol : string;
    currencyUnit : string;
    unitSubunitConnector:string;
    currencySubunit:string;
    currencyUnitArabic:string;
    unitSubunitConnectorArabic:string;
    currencySubunitArabic:string;
    separatorsDecimal:string[];
    includeSpaceAfterCurrencySymbol:string;
    negativeSymbol:string[];
    displayNegativeSymbolSign:string;
    separatorsThousands:string[];
    displayCurrencySymbol:number
  

    //initializing Authorization parameters
    constructor (currencyId: string, currencyDescription:string,currencyDescriptionArabic:string,currencySymbol:string,
        currencyUnit:string,unitSubunitConnector:string,currencySubunit:string,currencyUnitArabic:string,unitSubunitConnectorArabic:string,currencySubunitArabic:string, separatorsDecimal:string[],
    includeSpaceAfterCurrencySymbol:string,negativeSymbol:string[],displayNegativeSymbolSign:string,separatorsThousands:string[],displayCurrencySymbol:number)
        {
          this.currencyId= currencyId;
           this.currencyDescription= currencyDescription;
            this.currencyDescriptionArabic=currencyDescriptionArabic;
            this.currencySymbol=currencySymbol;
            this.currencyUnit= currencyUnit;
            this.unitSubunitConnector=unitSubunitConnector;
            this.currencySubunit=currencySubunit;
            this.currencyUnitArabic=currencyUnitArabic;
            this.unitSubunitConnectorArabic=unitSubunitConnectorArabic;
            this.currencySubunitArabic=currencySubunitArabic;
            this.separatorsDecimal=separatorsDecimal;
            this.includeSpaceAfterCurrencySymbol=includeSpaceAfterCurrencySymbol;
            this.negativeSymbol=negativeSymbol;
            this.displayNegativeSymbolSign=displayNegativeSymbolSign;
            this.separatorsThousands=separatorsThousands; 
            this.displayCurrencySymbol=displayCurrencySymbol;
            
        }
}