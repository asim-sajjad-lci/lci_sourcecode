export class AccountStructureSetup {
  segmentNumber:number;
  isMainAccount:number;
  coaMainAccountsFromActIndexId:number;
  coaMainAccountsToActIndexId:number;
  dimensionList:string[] = [];

  constructor (segmentNumber:number,isMainAccount:number,coaMainAccountsFromActIndexId:number,coaMainAccountsToActIndexId:number,dimensionList:string[])
   {
        this.segmentNumber=segmentNumber;
        this.isMainAccount=isMainAccount;
        this.coaMainAccountsFromActIndexId=coaMainAccountsFromActIndexId;
        this.coaMainAccountsToActIndexId=coaMainAccountsToActIndexId;
        this.dimensionList=dimensionList;
   }
}