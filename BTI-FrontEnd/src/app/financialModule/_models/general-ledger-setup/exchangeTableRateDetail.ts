/**
 * A model for excahnge table set up
 */
export class ExchangeTableRateDetail {
	currencyId: string;
    description:string;
    descriptionArabic:string;
    exchangeDate : string;
    exchangeExpirationDate : string;
    exchangeId:string;
    exchangeRate:string;
    exchangeTime:string;
   
  //initializing Authorization parameters
    constructor (
			currencyId: string,
			description:string,
			descriptionArabic:string,
			exchangeDate:string,
			exchangeExpirationDate:string,
			exchangeId:string,
			exchangeRate:string,
			exchangeTime:string
			)
        {
			this.currencyId=currencyId;
			this.description=description;
			this.descriptionArabic=descriptionArabic;
			this.exchangeDate =exchangeDate;
			this.exchangeExpirationDate =exchangeExpirationDate;
			this.exchangeId=exchangeId;
			this.exchangeRate=exchangeRate;
			this.exchangeTime=exchangeTime;    
        }
}