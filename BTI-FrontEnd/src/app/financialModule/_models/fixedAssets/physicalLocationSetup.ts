/**
 * A model for excahnge table set up
 */
export class physicalLocationSetup {
	descriptionPrimary: string;
    descriptionSecondary:string;
    locationIndex:string;
    physicalLocationId : string;  
  //initializing Authorization parameters
    constructor (
			descriptionPrimary: string,
			descriptionSecondary:string,
			locationIndex:string,
			physicalLocationId:string
			)
        {
			this.descriptionPrimary=descriptionPrimary;
			this.descriptionSecondary=descriptionSecondary;
			this.locationIndex=locationIndex;
			this.physicalLocationId =physicalLocationId; 
        }
}