/**
 * A model for excahnge table set up
 */
export class locationSetup {
	city: string;
    cityId:string;
    country:string;
    countryId : string;
    locationId : string;
    state:string;
    stateId:string;   
  //initializing Authorization parameters
    constructor (
			city: string,
			cityId:string,
			country:string,
			countryId:string,
			locationId:string,
			state:string,
			stateId:string
			)
        {
			this.city=city;
			this.cityId=cityId;
			this.country=country;
			this.countryId =countryId;
			this.locationId =locationId;
			this.state=state;
			this.stateId=stateId;   
        }
}