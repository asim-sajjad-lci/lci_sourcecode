/**
 * A model for excahnge table set up
 */
export class fixedAssetCompanySetup {
	autoAddBookInformation: string;
    bookDescription:string;
    bookDescriptionArabic:string;
    bookIndexId : string;
    defaultAssetLabel : string;
    postDetails:string;
    postPayableManagement:string;
    postPurchaseOrderProcessing:string;
    requireAssetAccount:string;
   
  //initializing Authorization parameters
    constructor (
			autoAddBookInformation: string,
			bookDescription:string,
			bookDescriptionArabic:string,
			bookIndexId:string,
			defaultAssetLabel:string,
			postDetails:string,
			postPayableManagement:string,
			postPurchaseOrderProcessing:string,
			requireAssetAccount:string
			)
        {
			this.autoAddBookInformation=autoAddBookInformation;
			this.bookDescription=bookDescription;
			this.bookDescriptionArabic=bookDescriptionArabic;
			this.bookIndexId =bookIndexId;
			this.defaultAssetLabel =defaultAssetLabel;
			this.postDetails=postDetails;
			this.postPayableManagement=postPayableManagement;
			this.postPurchaseOrderProcessing=postPurchaseOrderProcessing;    
			this.requireAssetAccount=requireAssetAccount;    
        }
}