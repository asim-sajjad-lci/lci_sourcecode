/**
 * A model for book setup
 */
export class BookSetup {
    bookId: string;
    bookDescription:string;
    bookDescriptionArabic:string;
    currentYear:number;
    depreciationPeriodId:number;
    faCalendarSetupIndexId:string;
    bookIndexId:number;
    calendarIndex:number;
    calendarId:string;
    depreciationPeriodValue:string;
 
  //initializing structure setup parameters
    constructor (bookId: string, bookDescription:string,bookDescriptionArabic:string,currentYear:number,depreciationPeriodId:number,faCalendarSetupIndexId:string ,bookIndexId:number,calendarIndex:number,calendarId:string,depreciationPeriodValue:string)
    { 
        this.bookId= bookId;
        this.bookDescription= bookDescription;
        this.bookDescriptionArabic= bookDescriptionArabic;
        this.currentYear=currentYear;
        this.depreciationPeriodId=depreciationPeriodId;
        this.faCalendarSetupIndexId=faCalendarSetupIndexId;
        this.bookIndexId=bookIndexId;
        this.calendarIndex=calendarIndex;
        this.calendarId=calendarId;
        this.depreciationPeriodValue=depreciationPeriodValue;
    }
}