/**
 * A model for Insurance Class setup
 */
export class InsuranceClassSetup {
    insuranceClassId: string;
    insuranceDescriptionPrimary: string;
    insuranceDescriptionSecondary : string;
    inflationPercent: number;
    depriciationRate: number;
    
 
  //initializing Insurance Class parameters
    constructor (insuranceClassId: string, insuranceDescriptionPrimary: string,insuranceDescriptionSecondary : string, inflationPercent: number,depriciationRate: number)
    { 
        this.insuranceClassId = insuranceClassId;
        this.insuranceDescriptionPrimary = insuranceDescriptionPrimary;
        this.insuranceDescriptionSecondary = insuranceDescriptionSecondary;
        this.inflationPercent = inflationPercent;
        this.depriciationRate = depriciationRate;
        
    }
}