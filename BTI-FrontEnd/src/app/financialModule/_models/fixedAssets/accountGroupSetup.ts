export class AccountGroupSetup {
    accountGroupId: string;
    description: string;
    descriptionArabic: string;
    accountNumberList:string[];
    constructor (accountGroupId:string,description:string,descriptionArabic:string, accountNumberList:string[]){
       this.accountGroupId=accountGroupId;
       this.description=description;
       this.descriptionArabic=descriptionArabic;
       this.accountNumberList=accountNumberList;
    }
}