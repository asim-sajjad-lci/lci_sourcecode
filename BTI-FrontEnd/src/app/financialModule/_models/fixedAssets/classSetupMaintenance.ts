/**
 * A model for excahnge table set up
 */
export class classSetupMaintenance {
	classId: string;
    classIndex:string;
    description:string;
    descriptionArabic : string;  
    faAccountGroupId : string;  
    faInsuranceClassIndexId : string;  
  //initializing Authorization parameters
    constructor (
			classId: string,
			classIndex:string,
			description:string,
			descriptionArabic:string,
			faAccountGroupId:string,
			faInsuranceClassIndexId:string
			)
        {
			this.classId=classId;
			this.classIndex=classIndex;
			this.description=description;
			this.descriptionArabic =descriptionArabic; 
			this.faAccountGroupId =faAccountGroupId; 
			this.faInsuranceClassIndexId =faInsuranceClassIndexId; 
        }
}