/**
 * A model for structure setup
 */
export class StructureSetup {
    structureId: number;
    structureDescription:string;
    structureDescriptionArabic:string;
    structureIndex:number;
 
  //initializing structure setup parameters
    constructor (structureId: number, structureDescription:string,structureDescriptionArabic:string,structureIndex:number)
    { 
        this.structureId= structureId;
        this.structureDescription= structureDescription;
        this.structureDescriptionArabic= structureDescriptionArabic;
        this.structureIndex=structureIndex;
     
    }
}