/**
 * A model for Purchasing Posting Setup
 */
export class PurchasingPostingSetup {
classId:string;
accountTableRowIndex:string;

//initializing Purchasing Posting Setup parameters
    constructor (classId:string,accountTableRowIndex:string)
    { 
        this.classId=classId;
        this.accountTableRowIndex=accountTableRowIndex;
    }
}