export class leaseCompanySetup {
	companyId: string;
    vendorId: string;
    companyName: string;
    companyIndex: string;
    vendorName: string;
      //initializing Authorization parameters
    constructor (
			companyId: string,vendorId: string,companyName: string,companyIndex: string,vendorName: string
            	)
        {
			this.companyId=companyId;
            this.vendorId =vendorId;
            this.companyName=companyName;
            this.companyIndex=companyIndex; 
            this.vendorName=vendorName;
        }
}