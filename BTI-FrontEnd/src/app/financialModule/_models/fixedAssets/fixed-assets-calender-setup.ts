/**
 * A model for fixed assets calender setup
 */
export class FixedAssetsCalenderSetup {
    calendarId: string;
    calendarDescription:string;
    calendarDescriptionArabic:string;
    year1:number;
    monthlyCalendar:string[];
    calendarIndex:number
 
  //initializing structure setup parameters
    constructor (calendarId: string, calendarDescription:string,calendarDescriptionArabic:string,year1:number, monthlyCalendar:string[],calendarIndex:number)
    { 
        this.calendarId= calendarId;
        this.calendarDescription= calendarDescription;
        this.calendarDescriptionArabic= calendarDescriptionArabic;
        this.year1=year1;
        this.monthlyCalendar=monthlyCalendar;
        this.calendarIndex=calendarIndex;
     
    }
}