/**
 * A model for calender quarter setup
 */
export class CalenderQuarterSetup {
    calendarId: string;
    calendarDescription:string;
    calendarDescriptionArabic:string;
    year1:number;
    quarterCalendar:string[];
    quarterIndex:number;
    calendarIndex:number;
 
  //initializing structure setup parameters
    constructor (calendarId: string, calendarDescription:string,calendarDescriptionArabic:string,year1:number, quarterCalendar:string[],quarterIndex:number,calendarIndex:number)
    { 
        this.calendarId= calendarId;
        this.calendarDescription= calendarDescription;
        this.calendarDescriptionArabic= calendarDescriptionArabic;
        this.year1=year1;
        this.quarterCalendar=quarterCalendar;
        this.quarterIndex=quarterIndex;
        this.calendarIndex=calendarIndex;
     
    }
}