/**
 * A model for book Class setup
 */
export class BookClassSetup {
bookIndex:number;  
classId:string; 
bookId:string;
amortizationCode:number;
initialAllowancePercentage:number;
amortizationPercentage:number;
amortizationAmountAllowance:string;
amortizationAmount:number;
averagingConvention:number;
origLifeDays:number;
origLifeYears:number;
salvageEstimatecheck:number;
salvageEstimate:number;
salvagePercentage:number;
specialDepreciationAllowance:number;
specialDepreciationAllowancePercent:number;
swtch:number;
depreciationMethodId:number;
depreciationMethod:string;
averagingConventionType:string;

//initializing book Class setup parameters
    constructor (bookIndex:number,classId:string,bookId: string, amortizationCode:number,initialAllowancePercentage:number,amortizationAmountAllowance:string,amortizationAmount:number,amortizationPercentage:number,averagingConvention:number,origLifeDays:number,origLifeYears:number,salvageEstimate:number,salvageEstimatecheck:number,salvagePercentage:number,specialDepreciationAllowance:number,specialDepreciationAllowancePercent:number,swtch:number,depreciationMethodId:number,depreciationMethod:string,averagingConventionType:string)
    { 
        this.bookIndex=bookIndex;
        this.classId=classId;
        this.bookId= bookId;
        this.amortizationCode=amortizationCode;
        this.amortizationCode=amortizationCode;
        this.amortizationPercentage=amortizationPercentage;
        this.amortizationAmountAllowance = amortizationAmountAllowance;
        this.amortizationAmount=amortizationAmount;
        this.amortizationPercentage = amortizationPercentage;
        this.averagingConvention=averagingConvention;
        this.origLifeDays=origLifeDays;
        this.origLifeYears=origLifeYears;
        this.salvageEstimate=salvageEstimate;
        this.salvageEstimatecheck = salvageEstimatecheck;
        this.salvagePercentage=salvagePercentage;
        this.specialDepreciationAllowance=specialDepreciationAllowance;
        this.specialDepreciationAllowancePercent=specialDepreciationAllowancePercent;
        this.swtch=swtch;
        this.depreciationMethodId=depreciationMethodId;
        this.depreciationMethod=depreciationMethod;
        this.averagingConventionType=averagingConventionType;
    }
}