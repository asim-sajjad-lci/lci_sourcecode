/**
 * A model for Retirement setup
 */
export class RetirementSetup {
    faRetirementId: string;
    descriptionPrimary:string;
    descriptionSecondary:string;
 
  //initializing Retirement setup parameters
    constructor (faRetirementId: string, descriptionPrimary:string,descriptionSecondary:string)
    { 
        this.faRetirementId= faRetirementId;
        this.descriptionPrimary= descriptionPrimary;
        this.descriptionSecondary= descriptionSecondary;
    }
}