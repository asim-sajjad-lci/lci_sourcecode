/**
 * A model for AccountPayablesSetup
 */
export class AccountPayablesSetup {
  allowDuplicateInvoicePerVendor:number;
  ageingBy: number;
  ageUnappliedCreditAmounts: boolean;
  applyByDefault: number;
  apTrackingDiscountAvailable: boolean;
  checkBookBankId: string;
  checkFormat: number;
  removeVendorHoldPassword: string;
  deleteUnpostedPrintedDocuments:boolean;
  exceedMaximumInvoiceAmount:string;
  exceedMaximumWriteoffAmount:string;
  freightVatScheduleId: string;
  miscVatScheduleId: string;
  nextPaymentNumber: number;
  nextSchedulePaymentNumber: number;
  nextVoucherNumber: number;
  overrideVoucherNumberTransactionEntry:boolean;
  purchaseVatScheduleId: string;
  printHistoricalAgedTrialBalance:boolean;
  userDefine1: string;
  userDefine2: string;
  userDefine3: string;
  pmPeriodSetupsList:string[];
  constructor (allowDuplicateInvoicePerVendor:number , ageingBy: number,ageUnappliedCreditAmounts: boolean,applyByDefault: number, apTrackingDiscountAvailable: boolean, checkBookBankId: string,
        checkFormat: number,removeVendorHoldPassword: string,deleteUnpostedPrintedDocuments: boolean,
        exceedMaximumInvoiceAmount: string, exceedMaximumWriteoffAmount: string,freightVatScheduleId: string,miscVatScheduleId: string,
      nextPaymentNumber:number,nextSchedulePaymentNumber:number,nextVoucherNumber:number,overrideVoucherNumberTransactionEntry:boolean,purchaseVatScheduleId:string,
      printHistoricalAgedTrialBalance:boolean,userDefine1:string,userDefine2:string,userDefine3:string,pmPeriodSetupsList:string[] )
   {
                    this.allowDuplicateInvoicePerVendor=allowDuplicateInvoicePerVendor;
                    this.ageingBy=ageingBy;
                    this.ageUnappliedCreditAmounts= ageUnappliedCreditAmounts;
                    this.applyByDefault= applyByDefault;
                    this.apTrackingDiscountAvailable= apTrackingDiscountAvailable;
                    this.checkBookBankId= checkBookBankId;
                    this.checkFormat= checkFormat;
                    this.removeVendorHoldPassword=removeVendorHoldPassword;
                    this.deleteUnpostedPrintedDocuments=deleteUnpostedPrintedDocuments;
                    this.exceedMaximumInvoiceAmount=exceedMaximumInvoiceAmount;
                    this.exceedMaximumWriteoffAmount=exceedMaximumWriteoffAmount;
                    this.freightVatScheduleId=freightVatScheduleId;
                    this.miscVatScheduleId=miscVatScheduleId;
                    this.nextPaymentNumber=nextPaymentNumber;
                    this.nextSchedulePaymentNumber=nextSchedulePaymentNumber;
                    this.nextVoucherNumber=nextVoucherNumber;
                    this.overrideVoucherNumberTransactionEntry=overrideVoucherNumberTransactionEntry;
                    this.purchaseVatScheduleId=purchaseVatScheduleId;
                    this.printHistoricalAgedTrialBalance=printHistoricalAgedTrialBalance;
                    this.userDefine1=userDefine1;
                    this.userDefine2=userDefine2;
                    this.userDefine3=userDefine3;
                    this.pmPeriodSetupsList=pmPeriodSetupsList;
   }
}