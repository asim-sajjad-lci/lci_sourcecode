/**
 * A model for AccountPayablesClassSetup
 */
export class AccountPayablesClassSetup {
  vendorClassId: number;
  classDescription: string;
  classDescriptionArabic: string;
  creditLimit: number;
  creditLimitAmount: number;
  currencyId: string;
  maximumInvoiceAmount:number
  maximumInvoiceAmountValue : number;
  minimumCharge: number;
  minimumChargeAmount: number;
  minimumOrderAmount: number;
  tradeDiscountPercent: number;
  openMaintenanceHistoryCalendarYear: number;
  openMaintenanceHistoryDistribution: number;
  openMaintenanceHistoryFiscalYear: number;
  openMaintenanceHistoryTransaction: number;
  userDefine1: string;
  userDefine2: string;
  userDefine3: string;
  vatScheduleId:string;
  shipmentMethodId:string;
  checkBookId:string;
  paymentTermId:number;
  accTableId:number;

  constructor (vendorClassId: number,classDescription: string,classDescriptionArabic: string, creditLimit: number, creditLimitAmount: number,
        currencyId: string,maximumInvoiceAmount: number,minimumCharge: number,
        minimumChargeAmount: number, minimumOrderAmount: number,tradeDiscountPercent: number,openMaintenanceHistoryCalendarYear: number,
      openMaintenanceHistoryDistribution:number,openMaintenanceHistoryFiscalYear:number,openMaintenanceHistoryTransaction:number,
      userDefine1:string,userDefine2:string,userDefine3:string,vatScheduleId:string,shipmentMethodId:string,checkBookId:string,paymentTermId:number
      ,accTableId:number )
   {
                    this.vendorClassId=vendorClassId;
                    this.classDescription=classDescription;
                    this.classDescriptionArabic=classDescriptionArabic;
                    this.creditLimit=creditLimit;
                    this.creditLimitAmount=creditLimitAmount;
                    this.currencyId=currencyId;
                    this.maximumInvoiceAmount=maximumInvoiceAmount;
                    this.minimumCharge=minimumCharge;
                    this.minimumChargeAmount=minimumChargeAmount;
                    this.minimumOrderAmount=minimumOrderAmount;
                    this.tradeDiscountPercent=tradeDiscountPercent;
                    this.openMaintenanceHistoryCalendarYear=openMaintenanceHistoryCalendarYear;
                    this.openMaintenanceHistoryDistribution=openMaintenanceHistoryDistribution;
                    this.openMaintenanceHistoryFiscalYear=openMaintenanceHistoryFiscalYear;
                    this.openMaintenanceHistoryTransaction=openMaintenanceHistoryTransaction;
                    this.userDefine1=userDefine1;
                    this.userDefine2=userDefine2;
                    this.userDefine3=userDefine3;
                    this.vatScheduleId=vatScheduleId;
                    this.shipmentMethodId=shipmentMethodId;
                    this.checkBookId=checkBookId;
                    this.paymentTermId=paymentTermId;
                    this.accTableId=accTableId;
                  
   }
}