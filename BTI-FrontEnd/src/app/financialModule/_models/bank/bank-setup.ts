
export class BankSetup {
  bankId: string;
  bankDescription: string;
  bankArabicDescription: string;
  address1: string;
  address2: string;
  address3: string;
  phone1: string;
  phone2: string;
  phone3: string;
  cityId: number;
  stateId: number;
  countryId: number;
  fax: string;

    
    //initializing Bank Term Setup parameters
    constructor (bankId: string,bankDescription: string,bankArabicDescription: string,address1: string,address2: string,address3: string,phone1: string,phone2: string,phone3: string,cityId: number,stateId: number,countryId: number,fax: string)
        {
                this.bankId= bankId;
                this.bankDescription= bankDescription;
                this.bankArabicDescription= bankArabicDescription;
                this.address1= address1;
                this.address2= address2;
                this.address3= address3;
                this.phone1= phone1;
                this.phone2= phone2;
                this.phone3= phone3;
                this.cityId= cityId;
                this.stateId= stateId;
                this.countryId= countryId;
                this.fax= fax;
        }
}