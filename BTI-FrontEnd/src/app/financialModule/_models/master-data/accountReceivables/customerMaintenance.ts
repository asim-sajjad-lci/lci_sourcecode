/**
 * A model for user group
 */
export class CustomerMaintenance{
    activeCustomer:string;
    customerHold : string;
	customerId : number;
    name : string;
    arabicName:string;
    shortName:string;
    classId:string;
    statementName:string;
    address1:string;
    address2:string;
    address3:string;
    phone1:string;
    phone2:string;
    phone3:string;
    fax:string;
    countryName:string;
    stateName:string;
    cityName:string;
    userDefine1:string;
    userDefine2:string;
    priority:string;

    //initializing user group parameters
    constructor(activeCustomer:string,
    customerHold : string,
	customerId : number,
    name : string,
    arabicName:string,
    shortName:string,
    classId:string,
    statementName:string,
    address1:string,
    address2:string,
    address3:string,
    phone1:string,
    phone2:string,
    phone3:string,
    fax:string,
    countryName:string,
    stateName:string,
    cityName:string,
    userDefine1:string,
    userDefine2:string,
	priority:string
	)
      {
            this.activeCustomer=activeCustomer;
            this.customerHold=customerHold;
            this.customerId=customerId;
            this.name=name;
            this.arabicName=arabicName;
            this.shortName=shortName;
            this.classId=classId;
            this.statementName=statementName;
            this.address1=address1;
            this.address2=address2;
            this.address3=address3;
            this.phone1=phone1;
            this.phone2=phone2;
            this.phone3=phone3;
            this.fax=fax;
            this.countryName=countryName;
            this.stateName=stateName;
            this.cityName=cityName;
            this.userDefine1=userDefine1;
            this.userDefine2=userDefine2;
            this.priority=priority;
      }
}