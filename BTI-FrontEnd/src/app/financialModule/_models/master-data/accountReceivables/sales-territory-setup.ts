/**
 * A model for SalesTerritorySetup
 */
export class SalesTerritorySetup {
    salesTerritoryId: string;
    descriptionPrimary:string;
    descriptionSecondary:string;
    phone1:number;
    phone2 : number;
    managerFirstNamePrimary :string ;
    managerMidNamePrimary:string;
    managerLastNamePrimary:string;
    managerFirstNameSecondary:string;
	managerMidNameSecondary:string;
	managerLastNameSecondary:string;
	totalCommissionsYTD:number;
	totalCommissionsLY:number;
	commissionsSalesYTD:number;
	commissionsSalesLY:number;
	costOfSalesYTD:number;
	costOfSalesLY:number;
    //initializing salesTerritorySetup parameters
    constructor (salesTerritoryId: string, descriptionPrimary:string, descriptionSecondary:string,phone1:number, phone2 : number,
                managerFirstNamePrimary :string,managerMidNamePrimary:string,managerLastNamePrimary:string,managerFirstNameSecondary:string,
             managerMidNameSecondary:string,managerLastNameSecondary:string,totalCommissionsYTD:number,totalCommissionsLY:number,commissionsSalesYTD:number,commissionsSalesLY:number ,costOfSalesYTD:number,costOfSalesLY:number)
    {
        this.salesTerritoryId= salesTerritoryId;
        this.descriptionPrimary= descriptionPrimary;
        this.descriptionSecondary= descriptionSecondary;
        this.phone1=phone1;
        this.phone2=phone2;
        this.managerFirstNamePrimary=managerFirstNamePrimary;
        this.managerMidNamePrimary= managerMidNamePrimary;
        this.managerLastNamePrimary=managerLastNamePrimary;
        this.managerFirstNameSecondary=managerFirstNameSecondary;
		this.managerMidNameSecondary=managerMidNameSecondary;
		this.managerLastNameSecondary=managerLastNameSecondary;
		this.totalCommissionsYTD=totalCommissionsYTD;
		this.totalCommissionsLY=totalCommissionsLY;
		this.commissionsSalesYTD=commissionsSalesYTD;
		this.commissionsSalesLY=commissionsSalesLY;
		this.costOfSalesYTD=costOfSalesYTD;
		this.costOfSalesLY=costOfSalesLY;
		
 
    }
}