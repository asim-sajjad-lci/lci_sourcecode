/**
 * A model for NationalAccountMaintenance
 */
export class NationalAccountMaintenance {
    custNumber: string;
    custNamePrimary:string;
    custNameSecondary:string;
    allowReceiptEntry:number;
    baseCreditCheck : number;
    applyStatus :number ;
    baseFinanceCharge:number;
    accountMaintenanceDetail:string[];
   
    //initializing NationalAccountMaintenance parameters
    constructor (custNumber: string,  custNamePrimary:string,custNameSecondary:string, allowReceiptEntry : number,
                baseCreditCheck :number,applyStatus:number,baseFinanceCharge:number,accountMaintenanceDetail:string[])
    {
        this.custNumber= custNumber;
        this.custNamePrimary= custNamePrimary;
        this.custNameSecondary= custNameSecondary;
        this.allowReceiptEntry=allowReceiptEntry;
        this.baseCreditCheck=baseCreditCheck;
        this.applyStatus=applyStatus;   
        this.baseFinanceCharge=baseFinanceCharge;
        this.accountMaintenanceDetail=accountMaintenanceDetail;
       
 
    }
}