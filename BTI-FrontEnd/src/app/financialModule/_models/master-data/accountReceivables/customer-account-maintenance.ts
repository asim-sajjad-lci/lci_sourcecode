/**
 * A model for Customer Account Maintenance
 */
export class customerAccountMaintenance{
    customerId:string;
    accountNumberList : string[];
    constructor(customerId:string, accountNumberList : string[])
      {
            this.customerId=customerId;
            this.accountNumberList=accountNumberList;
      }
}