/**
 * A model for CustomerMaintenanceOptions
 */
export class CustomerMaintenanceOptions {
    customerNumberId: string;
    currencyId:string;
    customerPriority:number;
    balanceType : number;
    financeCharge : number;
    minimumCharge:number;
    creditLimit:number;
    openMaintenanceHistoryCalendarYear:number;
    openMaintenanceHistoryDistribution:number;
    openMaintenanceHistoryFiscalYear:number;
    openMaintenanceHistoryTransaction:number;
    priceLevel:number;
    vatId:number;
    shipmentMethodId:string;
    paymentTermId:string;
    salesmanId:string;
    salesTerritoryId:string;
    checkbookId:string;
    financeChargeAmount:number;
    minimumChargeAmount:number;
    tradeDiscountPercent:number;
    creditLimitAmount:number;
  

    //initializing Authorization parameters
    constructor (customerNumberId: string, currencyId:string,customerPriority:number,balanceType:number,
        financeCharge:number,minimumCharge:number,creditLimit:number,openMaintenanceHistoryCalendarYear:number,openMaintenanceHistoryDistribution:number,openMaintenanceHistoryFiscalYear:number, openMaintenanceHistoryTransaction:number,
    priceLevel:number,vatId:number,shipmentMethodId:string,paymentTermId:string,salesmanId:string, salesTerritoryId:string, checkbookId:string,financeChargeAmount:number,minimumChargeAmount:number,tradeDiscountPercent:number,creditLimitAmount:number)
        {
          this.customerNumberId= customerNumberId;
           this.currencyId= currencyId;
            this.customerPriority=customerPriority;
            this.balanceType=balanceType;
            this.financeCharge= financeCharge;
            this.minimumCharge=minimumCharge;
            this.creditLimit=creditLimit;
            this.openMaintenanceHistoryCalendarYear=openMaintenanceHistoryCalendarYear;
            this.openMaintenanceHistoryDistribution=openMaintenanceHistoryDistribution;
            this.openMaintenanceHistoryFiscalYear=openMaintenanceHistoryFiscalYear;
            this.openMaintenanceHistoryTransaction=openMaintenanceHistoryTransaction;
            this.priceLevel=priceLevel;
            this.vatId=vatId;
            this.shipmentMethodId=shipmentMethodId;
            this.paymentTermId=paymentTermId; 
            this.salesmanId=salesmanId;
            this.salesTerritoryId=salesTerritoryId;
            this.checkbookId=checkbookId;
            this.financeChargeAmount=financeChargeAmount;
            this.minimumChargeAmount=minimumChargeAmount;
            this.tradeDiscountPercent=tradeDiscountPercent;
            this.creditLimitAmount=creditLimitAmount;
            
        }
}