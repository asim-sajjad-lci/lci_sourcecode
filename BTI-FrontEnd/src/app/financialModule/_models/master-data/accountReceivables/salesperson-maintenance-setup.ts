/**
 * A model for SalespersonMaintenanceSetup
 */
export class SalespersonMaintenanceSetup {
    salesmanId: string;
    address1:string;
    address2:string;
    applyPercentage:number;
    costOfSalesYTD : number;
    costOfSalesLastYear :number ;
    employeeId:string;
    salesmanFirstName:string;
    salesmanFirstNameArabic:string;
	inactive:number;
	salesmanLastName:string;
	salesmanLastNameArabic:string;
	salesmanMidName:string;
	salesmanMidNameArabic:string;
	percentageAmount:number;
	phone1:string;
	phone2:string;
    salesTerritoryId:string;
    salesCommissionsYTD:number;
    salesCommissionsLastYear:number;
    totalCommissionsYTD:number;
    totalCommissionsLastYear:number;

    //initializing salesTerritorySetup parameters
    constructor (salesmanId: string, address1:string, address2:string,applyPercentage:number, costOfSalesYTD : number,
                costOfSalesLastYear :number,employeeId:string,salesmanFirstName:string,salesmanFirstNameArabic:string,
             inactive:number,salesmanLastName:string,salesmanLastNameArabic:string,salesmanMidName:string,salesmanMidNameArabic:string,percentageAmount:number ,phone1:string,phone2:string,
             salesTerritoryId:string,salesCommissionsYTD:number,salesCommissionsLastYear:number,totalCommissionsYTD:number,totalCommissionsLastYear:number)
    {
        this.salesmanId= salesmanId;
        this.address1= address1;
        this.address2= address2;
        this.applyPercentage=applyPercentage;
        this.costOfSalesYTD=costOfSalesYTD;
        this.costOfSalesLastYear=costOfSalesLastYear;   
        this.employeeId=employeeId;
        this.salesmanFirstName=salesmanFirstName;
        this.salesmanFirstNameArabic=salesmanFirstNameArabic;
        this.inactive=inactive;
        this.salesmanLastName=salesmanLastName;
        this.salesmanLastNameArabic= salesmanLastNameArabic;
        this.salesmanMidName=salesmanMidName;
        this.salesmanMidNameArabic=salesmanMidNameArabic;
		this.percentageAmount=percentageAmount;
		this.phone1=phone1;
		this.phone2=phone2;
		this.salesTerritoryId=salesTerritoryId;
		this.salesCommissionsYTD=salesCommissionsYTD;
		this.salesCommissionsLastYear=salesCommissionsLastYear;
		this.totalCommissionsYTD=totalCommissionsYTD;
		this.totalCommissionsLastYear=totalCommissionsLastYear;
 
    }
}