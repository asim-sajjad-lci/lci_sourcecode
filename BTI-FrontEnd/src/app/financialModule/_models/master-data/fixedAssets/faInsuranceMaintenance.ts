/**
 * A model for excahnge table set up
 */
export class insuranMaintenance {
	assetId: string;
    insClassId:string;
    insuranceValue:string;
    insuranceYear : string;  
    replacementCost : string;  
    reproductionCost : string;  
  //initializing Authorization parameters
    constructor (
			assetId: string,
			insClassId:string,
			insuranceValue:string,
			insuranceYear:string,
			replacementCost:string,
			reproductionCost:string
			)
        {
			this.assetId=assetId;
			this.insClassId=insClassId;
			this.insuranceValue=insuranceValue;
			this.insuranceYear =insuranceYear; 
			this.replacementCost =replacementCost; 
			this.reproductionCost =reproductionCost; 
        }
}