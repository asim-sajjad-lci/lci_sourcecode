/**
 * A model for excahnge table set up
 */
export class leaseMaintenance {
	assetId: string;
    leaseCompanyId:string;
    leaseTypeIndex:string;
    leaseContactNumber : string;  
    leaseEndDate : string;
    leaseMaintenanceIndex : string;
    assetSerialId : string;
    interestRatePercent : string;
    leaseCompanyIndex : string;
    leasePayment : string;
  //initializing Authorization parameters
    constructor (
			assetId: string,
			leaseCompanyId:string,
			leaseTypeIndex:string,
			leaseContactNumber:string,
			leaseEndDate:string,
			leaseMaintenanceIndex:string,
			assetSerialId:string,
			interestRatePercent:string,
			leaseCompanyIndex:string,
			leasePayment:string
			)
        {
			this.assetId=assetId;
			this.leaseCompanyId=leaseCompanyId;
			this.leaseTypeIndex=leaseTypeIndex;
			this.leaseContactNumber =leaseContactNumber; 
			this.leaseEndDate =leaseEndDate;
			this.leaseMaintenanceIndex =leaseMaintenanceIndex;
			this.assetSerialId =assetSerialId;
			this.interestRatePercent =interestRatePercent;
			this.leaseCompanyIndex =leaseCompanyIndex;
			this.leasePayment =leasePayment;
        }
}