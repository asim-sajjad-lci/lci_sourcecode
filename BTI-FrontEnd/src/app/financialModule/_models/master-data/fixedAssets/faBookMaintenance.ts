/**
 * A model for excahnge table set up
 */
export class BookMaintenance {
	assetId: string;
    depreciationMethodId:string;
    averagingConvention:string;
    amortizationCode : string; 
	depreciatedDate: string;
	placedInServiceDate: string; 
  //initializing Authorization parameters
    constructor (
			assetId: string,
			depreciationMethodId:string,
			averagingConvention:string,
			amortizationCode:string,
			depreciatedDate:string,
			placedInServiceDate: string
			)
        {
			this.assetId=assetId;
			this.depreciationMethodId=depreciationMethodId;
			this.averagingConvention=averagingConvention;
			this.amortizationCode =amortizationCode; 
			this.depreciatedDate = depreciatedDate;
			this.placedInServiceDate = placedInServiceDate;
        }
}