/**
 * A model for Customer Account Maintenance
 */
export class faAccountMaintenance{
    assetId:string;
    accountGroupIndex:String;
    accountNumberList : string[];
    constructor(assetId:string,accountGroupIndex:String, accountNumberList : string[])
      {
            this.assetId=assetId;
            this.accountGroupIndex=accountGroupIndex;
            this.accountNumberList=accountNumberList;
      }
}