/**
 * A model for excahnge table set up
 */
export class InfoMaintenance {
	assetId: string;
    faDescription:string;
    faDescriptionArabic:string;
    faShortName : string;  
    faClassSetupId : string;  
  //initializing Authorization parameters
    constructor (
			assetId: string,
			faDescription:string,
			faDescriptionArabic:string,
			faShortName:string,
			faClassSetupId:string
			)
        {
			this.assetId=assetId;
			this.faDescription=faDescription;
			this.faDescriptionArabic=faDescriptionArabic;
			this.faShortName =faShortName; 
			this.faClassSetupId =faClassSetupId; 
        }
}