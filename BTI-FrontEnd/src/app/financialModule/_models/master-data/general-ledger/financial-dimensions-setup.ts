/**
 * A model for Account Category
 */
export class FinancialDimentions {
    dimInxd:string;
    dimensionName: string;
    dimensionMask:string;
    //initializing Account Category parameters
    constructor (dimInxd:string ,dimensionName : string,dimensionMask: string)
    {
        this.dimInxd = dimInxd;
        this.dimensionName= dimensionName;
        this.dimensionMask= dimensionMask;
    }
}