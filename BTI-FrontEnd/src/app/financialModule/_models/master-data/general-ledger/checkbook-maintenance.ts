/**
 * A model for Checkbook Maintenance
 */
export class CheckbookMaintenance {
    checkBookId:string;
    inactive:boolean;
    checkbookDescription:string;
    checkbookDescriptionArabic:string;
    currencyId:string;
    glAccountId:string;
    nextCheckNumber:number;
    nextDepositNumber:number;
    lastReconciledDate:boolean;
    lastReconciledBalance:number;
    officialBankAccountNumber:string;
    bankId:string;
    userDefine1:string;
    userDefine2:string;
    currentCheckbookBalance:number;
    cashAccountBalance:number;
    exceedMaxCheckAmount:number;
    passwordOfMaxCheckAmount:string;
    duplicateCheckNumber:boolean;
    overrideCheckNumber:boolean;
    
    //initializing Checkbook Maintenance parameters
    constructor (checkBookId:string, inactive:boolean, checkbookDescription:string, checkbookDescriptionArabic:string,
        currencyId:string, glAccountId:string, nextCheckNumber:number, nextDepositNumber:number, lastReconciledDate:boolean,
        lastReconciledBalance:number, officialBankAccountNumber:string, bankId:string, userDefine1:string,
        userDefine2:string, currentCheckbookBalance:number, cashAccountBalance:number, exceedMaxCheckAmount:number,
        passwordOfMaxCheckAmount:string, duplicateCheckNumber:boolean, overrideCheckNumber:boolean)
    {
        this.checkBookId=checkBookId;
        this.inactive=inactive;
        this.checkbookDescription=checkbookDescription;
        this.checkbookDescriptionArabic=checkbookDescriptionArabic;
        this.currencyId=currencyId;
        this.glAccountId=glAccountId;
        this.nextCheckNumber=nextCheckNumber;
        this.nextDepositNumber=nextDepositNumber;
        this.lastReconciledDate=lastReconciledDate;
        this.lastReconciledBalance=lastReconciledBalance;
        this.officialBankAccountNumber=officialBankAccountNumber;
        this.bankId=bankId;
        this.userDefine1=userDefine1;
        this.userDefine2=userDefine2;
        this.currentCheckbookBalance=currentCheckbookBalance;
        this.cashAccountBalance=cashAccountBalance;
        this.exceedMaxCheckAmount=exceedMaxCheckAmount;
        this.passwordOfMaxCheckAmount=passwordOfMaxCheckAmount;
        this.duplicateCheckNumber=duplicateCheckNumber;
        this.overrideCheckNumber=overrideCheckNumber;
    
    }
}