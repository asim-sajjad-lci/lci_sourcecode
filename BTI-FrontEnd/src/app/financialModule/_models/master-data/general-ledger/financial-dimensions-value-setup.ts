/**
 * A model for Account Category
 */
export class FinancialDimentionsValue {
    dimInxValue:number;
    dimInxd:string;
    dimensionValue: string;
    dimensionDescription: string;
    dimensionDescriptionArabic:string;
    dimensionName:string;
    //initializing Account Category parameters
    constructor (dimInxValue:number,dimInxd:string ,dimensionValue : string,dimensionDescription: string,dimensionDescriptionArabic: string,dimensionName:string)
    {
        this.dimInxValue=dimInxValue;
        this.dimInxd = dimInxd;
        this.dimensionValue= dimensionValue;
        this.dimensionDescription= dimensionDescription;
        this.dimensionDescriptionArabic= dimensionDescriptionArabic;
        this.dimensionName= dimensionName;
        
    }
}