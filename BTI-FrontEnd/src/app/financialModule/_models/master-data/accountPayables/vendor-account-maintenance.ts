/**
 * A model for VendorAccountMaintenance
 */
export class VendorAccountMaintenance {
    vendorId: string;
    accountNumberList:string[];
  
   
    //initializing NationalAccountMaintenance parameters
    constructor (vendorId: string,  accountNumberList:string[])
    {
        this.vendorId= vendorId;
        this.accountNumberList= accountNumberList;
    }
}