/**
 * A model for user group
 */
export class VendorMaintenance{
	venderId : string;
    venderNamePrimary : string;
    venderNameSecondary:string;
    venderStatus:string;
    shortName:string;
    classId:string;
    statementName:string;
    address1:string;
    address2:string;
    address3:string;
    country:string;
    state:string;
    city:string;
    phone1:string;
    phone2:string;
    phone3:string;
    fax:string;
    userDefine1:string;
    userDefine2:string;
    priority:string;

    //initializing user group parameters
    constructor(
		venderId : string,
		venderNamePrimary : string,
		venderNameSecondary:string,
		venderStatus:string,
		shortName:string,
		classId:string,
		statementName:string,
		address1:string,
		address2:string,
		address3:string,
		country:string,
		state:string,
		city:string,
		phone1:string,
		phone2:string,
		phone3:string,
		fax:string,
		userDefine1:string,
		userDefine2:string,
		priority:string
		)
      {
            this.venderId=venderId;
            this.venderNamePrimary=venderNamePrimary;
            this.venderNameSecondary=venderNameSecondary;
            this.venderStatus=venderStatus;
            this.shortName=shortName;
            this.classId=classId;
            this.statementName=statementName;
            this.address1=address1;
            this.address2=address2;
            this.address3=address3;
            this.country=country;
            this.state=state;
            this.city=city;
            this.phone1=phone1;
            this.phone2=phone2;
            this.phone3=phone3;
            this.fax=fax;
            this.userDefine1=userDefine1;
            this.userDefine2=userDefine2;
            this.priority=priority;
      }
}