/**
 * A model for user group
 */
export class MainAccountSetup{
    accountCategoryId:number;
    accountTypeId : number;
	mainAccountNumber : number;
    mainAccountDescription : string;
    mainAccountDescriptionArabic:string;
    tpclblnc:number;
    aliasAccount:string;
    aliasAccountArabic:string;
    allowAccountTransactionEntry:string;
    active:string;
    userDef1:string;
    userDef2:string;
    userDef3:string;
    userDef4:string;
    userDef5:string;
    accountTypeName:string;
    accountCategoryDescription:string;
    actIndx:string;

    //initializing user group parameters
    constructor(accountCategoryId:number,
    accountTypeId : number,
	mainAccountNumber : number,
    mainAccountDescription : string,
    mainAccountDescriptionArabic:string,
    tpclblnc:number,
    aliasAccount:string,
   aliasAccountArabic:string,
    allowAccountTransactionEntry:string,
    active:string,
    userDef1:string,
    userDef2:string,
    userDef3:string,
    userDef4:string,
    userDef5:string,
    accountTypeName:string,
    accountCategoryDescription:string,
    actIndx:string
    )
      {
            this.accountCategoryId=accountCategoryId;
            this.accountTypeId=accountTypeId;
            this.mainAccountNumber=mainAccountNumber;
            this.mainAccountDescription=mainAccountDescription;
            this.mainAccountDescriptionArabic=mainAccountDescriptionArabic;
            this.tpclblnc=tpclblnc;
            this.aliasAccount=aliasAccount;
            this.aliasAccountArabic=aliasAccountArabic;
            this.allowAccountTransactionEntry=allowAccountTransactionEntry;
            this.active=active;
            this.userDef1=userDef1;
            this.userDef2=userDef2;
            this.userDef3=userDef3;
            this.userDef4=userDef4;
            this.userDef5=userDef5;
            this.accountTypeName=accountTypeName;
            this.accountCategoryDescription=accountCategoryDescription;
            this.actIndx=actIndx;
      }
}