import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { leaseCompanySetup } from '../../../financialModule/_models/fixedAssets/leaseCompanySetup';

@Injectable()
export class LeaseCompanySetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private vendorUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/find/Activevender';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/leaseCompanySetup';
	private getListUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/leaseCompanySetup/getAll';
    private getVendorNameUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/getOne/vender';
    private getUpdateUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/leaseCompanySetup/update';
    private getEditUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/leaseCompanySetup/getById';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
    
    getEdit(companyIndex : any) {
        return this.http.post(this.getEditUrl, { companyIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    update(submittedData) {
        return this.http.post(this.getUpdateUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // searchVendors(page: Page,searchKeyword): Observable<PagedData<leaseCompanySetup>> {
    //    return this.http
    //         .post(this.vendorUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size  }, { headers: this.headers })
    //         .map(data => this.getPagedData(page, data.json().result));
    // } 
    
  searchVendors() {
        return this.http.post(this.vendorUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

	create(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	// getList() {
    //     return this.http.get(this.getListUrl, { headers: this.headers })
    //    .toPromise()
    //    .then(res => res.json())
    //    .catch(this.handleError);

    // }

    getList(page: Page): Observable<PagedData<leaseCompanySetup>> {
       return this.http
            .post(this.getListUrl, {'pageNumber': page.pageNumber, 'pageSize': page.size  }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    } 

	getVendorNameByID(venderId : any) {
        return this.http.post(this.getVendorNameUrl, { venderId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
private getPagedData(page: Page, data: any): PagedData<leaseCompanySetup> {
        let pagedData = new PagedData<leaseCompanySetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
				//console.log(jsonObj);
                let group = new leaseCompanySetup(jsonObj.companyId,jsonObj.vendorId,jsonObj.companyName,jsonObj.companyIndex,jsonObj.vendorName);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}