/**
 * A service class for Retirement setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { RetirementSetup } from '../../../../financialModule/_models/fixedAssets/retirement/retirement-setup';


@Injectable()
export class RetirementSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchRetirementSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup/search';
    private getRetirementSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup/getById';
    private updateRetirementSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup/update';
    private saveRetirementSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup';
     
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



    //Search Retirement setup detials
    searchRetirementSetup(page:Page, searchKeyword):Observable<PagedData<RetirementSetup>>{
        return this.http.post(this.searchRetirementSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }



     //getting details of  Retirement setup details By ID
    getRetirementSetupById(faRetirementId: string) {
        
        return this.http.post(this.getRetirementSetupByIdUrl,{faRetirementId: faRetirementId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update Retirement details for edit 
    updateRetirementSetup(RetirementSetup:RetirementSetup){
        return this.http.post(this.updateRetirementSetupUrl,JSON.stringify(RetirementSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

      //save Retirement setup
    createRetirementSetup(RetirementSetup:RetirementSetup)
    {
      return this.http.post(this.saveRetirementSetupUrl,JSON.stringify(RetirementSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<RetirementSetup> {
        let pagedData = new PagedData<RetirementSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new RetirementSetup(jsonObj.faRetirementId,jsonObj.descriptionPrimary, jsonObj.descriptionSecondary);
                pagedData.data.push(group);
            }
            
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}