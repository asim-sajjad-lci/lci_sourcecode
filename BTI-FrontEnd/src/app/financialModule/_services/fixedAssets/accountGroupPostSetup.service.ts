/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { AccountGroupSetup} from '../../../financialModule/_models/fixedAssets/accountGroupSetup';

@Injectable()
export class AccountGroupSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
	private getAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'getFAAccountTypes';
	private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
	private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/accountGroupSetup';
	private getEditRecordUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getAccountGroupSetupByAccountGroupId';
    private searchUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getAccountGroupSetupBySearch';
    private getAccountGroupByIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getAccountGroupSetupByAccountGroupId';
	/*private getAccountListUrl = Constants.financialModuleApiBaseUrl + 'getGlAccountNumberList';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/purchasePostingAccountSetup';
	private getDataUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getPurchasePostingAccountSetup';*/
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
	getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }

    search(page: Page,searchKeyword): Observable<PagedData<AccountGroupSetup>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }

    getAccountGroupById(accountGroupId : any) {
        
        return this.http.post(this.getAccountGroupByIdUrl, { accountGroupId:accountGroupId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getEditRecord() {
        return this.http.get(this.getEditRecordUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	/*getClass() {
        return this.http
            .post(this.getClassUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	/*getCurrency() {
        return this.http
            .post(this.searchCurrencyUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	getLocation() {
        return this.http
            .post(this.searchLocationUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	getPhysicalLocation() {
        return this.http
            .post(this.searchPhysicalLocationUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	getStructure() {
        return this.http
            .post(this.searchStructureUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	get(assetId : any) {
        return this.http.post(this.getUrl, { assetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   
	search(page: Page,searchKeyword): Observable<PagedData<InfoMaintenance>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	
	edit(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    private getPagedData(page: Page, data: any): PagedData<InfoMaintenance> {
        let pagedData = new PagedData<InfoMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
				console.log(jsonObj);
                let group = new InfoMaintenance(jsonObj.assetId,jsonObj.faDescription,jsonObj.faDescriptionArabic,jsonObj.faShortName,jsonObj.faClassSetupId);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }*/

    private getPagedData(page: Page, data: any): PagedData<AccountGroupSetup> {
        let pagedData = new PagedData<AccountGroupSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new AccountGroupSetup(jsonObj.accountGroupId,jsonObj.description,jsonObj.descriptionArabic,jsonObj.accountNumberList);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              