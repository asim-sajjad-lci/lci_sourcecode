/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import {fixedAssetCompanySetup} from '../../../financialModule/_models/fixedAssets/fixedAssetCompanySetup';


@Injectable()
export class CompanySetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
	private searchBooksUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchBookSetup';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/saveFixedAssetCompanySetup';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFixedAssetCompanySetup';
	private getFixedAssetCompanyUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetCompanySetupByIndexId';
	private editFACompanySetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateFixedAssetCompanySetup';
	private getListUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetCompanySetup';
    private getFixedAssetCompanySetupByBookIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetCompanySetupByBookId';
    private getBookSetupByBookIndexIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetBookSetupByBookIndexId';
    currentLanguage = "";
    //initializing parameter for constructor
	constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	// searchBooks1(){
	// 	 return this.http
	// 		.post(this.searchBooksUrl, {'searchKeyword':''}, { headers: this.headers })
	// 		.map(data => data.json().result);
    // }
    
    searchBooks() {
        return this.http
            .post(this.searchBooksUrl, {'searchKeyword':''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   createFACompanySetup(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
	searchFACompanySetup(page: Page,searchKeyword): Observable<PagedData<fixedAssetCompanySetup>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getFixedAssetCompany(bookIndexId : any) {
        return this.http.post(this.getFixedAssetCompanyUrl, { bookIndexId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	editFACompanySetup(submittedData) {
        return this.http.post(this.editFACompanySetupUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	   //getting details of  book setup details By bookIndexId
    getBookSetupByBookIndexId(bookIndexId: number) {
        return this.http.post(this.getBookSetupByBookIndexIdUrl, { bookIndexId: bookIndexId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    private getPagedData(page: Page, data: any): PagedData<fixedAssetCompanySetup> {
        let pagedData = new PagedData<fixedAssetCompanySetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new fixedAssetCompanySetup(jsonObj.autoAddBookInformation,jsonObj.bookDescription,jsonObj.bookDescriptionArabic,jsonObj.bookIndexId,jsonObj.defaultAssetLabel,jsonObj.postDetails,jsonObj.postPayableManagement,jsonObj.postPurchaseOrderProcessing,jsonObj.requireAssetAccount);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
	getList() {
        return this.http.get(this.getListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

       //getting details of  book setup details By bookIndexId
    getFixedAssetCompanySetupByBookId(bookIndexId: number) {
        return this.http.post(this.getFixedAssetCompanySetupByBookIdUrl, { bookIndexId: bookIndexId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              