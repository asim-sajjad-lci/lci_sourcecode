/**
 * A service class for FixedAssetsCalenderSetup 
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { CalenderQuarterSetup } from '../../../financialModule/_models/fixedAssets/calender-quarter-setup';



@Injectable()
export class CalenderQuarterSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchCalendarMonthlySetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchCalendarQuarterSetup';
    private getCalendarQuarterSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getCalendarQuarterSetupById';
    private updateCalendarQuarterSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateCalendarQuarterSetup';
    private calendarQuarterSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/calendarQuarterSetup';
   private getCalendarQuarterSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getCalendarQuarterSetup';
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



    //Search calender monthly  detials
    searchFaCalendarSetup(page:Page, searchKeyword):Observable<PagedData<CalenderQuarterSetup>>{
        return this.http.post(this.searchCalendarMonthlySetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


//getCalendarQuarterSetup
getCalendarQuarterSetup() {
       return this.http.get(this.getCalendarQuarterSetupUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

     //getting details of calender quarter setup  details By ID
    getCalendarQuarterSetupById(calendarIndex: number) {
        return this.http.post(this.getCalendarQuarterSetupByIdUrl, { calendarIndex: calendarIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update calender quarter  details for edit 
    updateCalendarQuarterSetup(CalenderQuarterSetup:CalenderQuarterSetup){
        return this.http.post(this.updateCalendarQuarterSetupUrl,JSON.stringify(CalenderQuarterSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //save calender quarter setup
    createCalendarQuarterSetup(CalenderQuarterSetup:CalenderQuarterSetup)
    {
      return this.http.post(this.calendarQuarterSetupUrl,JSON.stringify(CalenderQuarterSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<CalenderQuarterSetup> {
        let pagedData = new PagedData<CalenderQuarterSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new CalenderQuarterSetup(jsonObj.calendarId,jsonObj.calendarDescription, jsonObj.calendarDescriptionArabic,jsonObj.year1, jsonObj.quarterCalendar, jsonObj.quarterIndex, jsonObj.calendarIndex);
                pagedData.data.push(group);
            }
            
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}