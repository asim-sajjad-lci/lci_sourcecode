/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import {locationSetup} from '../../../financialModule/_models/fixedAssets/locationSetup';
import {physicalLocationSetup} from '../../../financialModule/_models/fixedAssets/physicalLocationSetup';

@Injectable()
export class physicalLocationService {
    private headers = new Headers({ 'content-type': 'application/json' });
	private searchLocationUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/locationSetup/search';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/physicalLocationSetup';
	private searchPhysicalLocationUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/physicalLocationSetup/search';
	private getUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/physicalLocationSetup/getById';
	private editUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/physicalLocationSetup/update';
    
    currentLanguage = "";
    //initializing parameter for constructor
	constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
	
	searchLocation() {
        return this.http
            .post(this.searchLocationUrl, { 'searchKeyword':'','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	
   createPhysicalLocation(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
	searchPhysicalLocation(page: Page,searchKeyword): Observable<PagedData<physicalLocationSetup>> {
        return this.http
            .post(this.searchPhysicalLocationUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getPhysicalLocationById(physicalLocationId : any) {
        return this.http.post(this.getUrl, { physicalLocationId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	editPhysicalLocation(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    private getPagedData(page: Page, data: any): PagedData<physicalLocationSetup> {
        let pagedData = new PagedData<physicalLocationSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new physicalLocationSetup(jsonObj.descriptionPrimary,jsonObj.descriptionSecondary,jsonObj.locationIndex,jsonObj.physicalLocationId);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              