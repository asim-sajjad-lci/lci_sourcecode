/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { classSetupMaintenance } from '../../../financialModule/_models/fixedAssets/classSetupMaintenance';


@Injectable()
export class ClassSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getinsuranceClassUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/insuranceClassSetup/search';
    private getAccountGroupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getAccountGroupSetupBySearch';
    /*private getLeaseCompanyUrl = Constants.financialModuleApiBaseUrl + '/fixed/assets/leaseCompanySetup/get';
    private getLeaseTypeUrl = Constants.financialModuleApiBaseUrl + '/masterdata/fixed/assets/leaseType';*/
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/saveFixedAssetClassSetup';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAClassSetup';
	private getUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetClassSetupByClassId';
	private editUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateFixedAssetClassSetup';

    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	getinsuranceClass() {
       return this.http
            .post(this.getinsuranceClassUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }

    getAccountGroup()
    {
        
         return this.http.post(this.getAccountGroupUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	/*getLeaseType() {
        return this.http.get(this.getLeaseTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }*/
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	search(page: Page,searchKeyword): Observable<PagedData<classSetupMaintenance>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getData(classId : any) {
        return this.http.post(this.getUrl, { classId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	edit(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    private getPagedData(page: Page, data: any): PagedData<classSetupMaintenance> {
        let pagedData = new PagedData<classSetupMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
				//console.log(jsonObj);
                let group = new classSetupMaintenance(jsonObj.classId,jsonObj.classIndex,jsonObj.description,jsonObj.descriptionArabic,jsonObj.faAccountGroupId,jsonObj.faInsuranceClassIndexId);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                              