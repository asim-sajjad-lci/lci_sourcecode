/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import {locationSetup} from '../../../financialModule/_models/fixedAssets/locationSetup';


@Injectable()
export class LocationService {
    private headers = new Headers({ 'content-type': 'application/json' });
	private getCountryListUrl = Constants.userModuleApiBaseUrl + 'getCountryList';
	private getStateListUrl = Constants.userModuleApiBaseUrl + 'getStateListByCountryId';
	private getCityListUrl = Constants.userModuleApiBaseUrl + 'getCityListByStateId';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/locationSetup';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/locationSetup/search';
	private getUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/locationSetup/getById';
	private editUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/locationSetup/update';
    
    currentLanguage = "";
    //initializing parameter for constructor
	constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
	
	getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getStateList(countryId: any) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getCityList(stateId: any) {
        return this.http.post(this.getCityListUrl, { 'stateId': stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   createLocation(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
	searchLocation(page: Page,searchKeyword): Observable<PagedData<locationSetup>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getLocationById(locationId : any) {
        return this.http.post(this.getUrl, { locationId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	editLocation(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    private getPagedData(page: Page, data: any): PagedData<locationSetup> {
        let pagedData = new PagedData<locationSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new locationSetup(jsonObj.city,jsonObj.cityId,jsonObj.country,jsonObj.countryId,jsonObj.locationId,jsonObj.state,jsonObj.stateId);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              