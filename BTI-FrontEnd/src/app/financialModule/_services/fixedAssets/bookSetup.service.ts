/**
 * A service class for book setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { BookSetup } from '../../../financialModule/_models/fixedAssets/book-setup';


@Injectable()
export class BookSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchBookSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchBookSetup';
    private getBookSetupByBookIndexIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetBookSetupByBookIndexId';
    private updateBookSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateFixedAssetBookSetupByBookIndexId';
    private saveBookSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/saveFixedAssetBookSetup';
    private getDepreciationPeriodTypesListUrl = Constants.financialModuleApiBaseUrl + 'getDepreciationPeriodTypesList';
    private getCalenderTypeIndexUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getCalendarMonthlySetup';
    private getYearsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/massCloseFiscalPeriodSetup/getYears';
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    getYears()
    {
         return this.http.get(this.getYearsUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Search book setup detials
    searchBookSetup(page:Page, searchKeyword):Observable<PagedData<BookSetup>>{
        return this.http.post(this.searchBookSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }
//getCalenderTypeIndex
 
  getCalenderTypeIndex()
    {
         return this.http.get(this.getCalenderTypeIndexUrl, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //getting details of  book setup details By bookIndexId
    getBookSetupByBookIndexId(bookIndexId: number) {
        return this.http.post(this.getBookSetupByBookIndexIdUrl, { bookIndexId: bookIndexId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getDepreciationPeriodTypesList
    getDepreciationPeriodTypesList()
    {
         return this.http.get(this.getDepreciationPeriodTypesListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update book setup details for edit 
    updateBookSetup(BookSetup:BookSetup){
        return this.http.post(this.updateBookSetupUrl,JSON.stringify(BookSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //save book setup
    createBookSetup(BookSetup:BookSetup)
    {
      return this.http.post(this.saveBookSetupUrl,JSON.stringify(BookSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<BookSetup> {
        let pagedData = new PagedData<BookSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new BookSetup(jsonObj.bookId,jsonObj.bookDescription, jsonObj.bookDescriptionArabic,jsonObj.currentYear, jsonObj.depreciationPeriodId, jsonObj.faCalendarSetupIndexId, jsonObj.bookIndexId, jsonObj.calendarIndex, jsonObj.calendarId, jsonObj.depreciationPeriodValue);
                pagedData.data.push(group);
            }
           
            
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}