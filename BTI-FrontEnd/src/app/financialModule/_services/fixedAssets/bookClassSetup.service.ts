
/**
 * A service class for book class setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { BookClassSetup } from '../../../financialModule/_models/fixedAssets/bookClassSetup';


@Injectable()
export class BookClassSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchBookClassSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFABookClassSetup';
    private getBookClassSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFABookClassSetupById';
    private updateBookClassSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateFABookClassSetup';
    private saveBookClassSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/saveFABookClassSetup';
    private searchBookSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchBookSetup';
    private getBookSetupByBookIndexIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetBookSetupByBookIndexId';
    private searchUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAClassSetup';
    private getUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFixedAssetClassSetupByClassId';
    private getconventionListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/averagingConventionType';
    private getSwitchOverListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/switchOverType';	
    private getamortizationCodeListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/amortizationCodeType';	
    private getdepListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/depreciationMethod';	
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



    //Search book class setup detials
    searchBookClassSetup(page:Page, searchKeyword):Observable<PagedData<BookClassSetup>>{
        return this.http.post(this.searchBookClassSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


    //Search book setup detials
    getBookIDSetup(){
        return this.http.post(this.searchBookSetupUrl, { 'searchKeyword': '' }, {headers:this.headers})
                 .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
    }

    getClassIDSetup(){
         return this.http.post(this.searchUrl, { 'searchKeyword': '' }, {headers:this.headers})
                 .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
    }

       //getting details of  book setup details By bookIndexId
    getBookSetupByBookIndexId(bookIndexId: number) {
        return this.http.post(this.getBookSetupByBookIndexIdUrl, { bookIndexId: bookIndexId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getData(classId : any) {
        return this.http.post(this.getUrl, { classId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //getting details of  book class setup details By bookIndexId
    getBookClassSetupById(bookIndex: number) {
        return this.http.post(this.getBookClassSetupByIdUrl, { bookIndex: bookIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //update book class setup details for edit 
    updateBookClassSetup(BookClassSetup:BookClassSetup){
        return this.http.post(this.updateBookClassSetupUrl,JSON.stringify(BookClassSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //save book class setup
    saveBookClassSetup(BookClassSetup:BookClassSetup)
    {
      return this.http.post(this.saveBookClassSetupUrl,JSON.stringify(BookClassSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    	getConventionList() {
        return this.http.get(this.getconventionListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getSwitchOverList() {
        return this.http.get(this.getSwitchOverListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getamortizationCodeList() {
        return this.http.get(this.getamortizationCodeListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getDepList() {
        return this.http.get(this.getdepListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }

  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<BookClassSetup> {
        let pagedData = new PagedData<BookClassSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new BookClassSetup(jsonObj.bookIndex,jsonObj.classId,jsonObj.bookId, jsonObj.amortizationCode,jsonObj.initialAllowancePercentage, jsonObj.amortizationPercentage,jsonObj.amortizationAmountAllowance,jsonObj.amortizationAmount, jsonObj.averagingConvention, jsonObj.origLifeDays, jsonObj.origLifeYears,jsonObj.salvageEstimate,jsonObj.salvageEstimatecheck,jsonObj.salvagePercentage,jsonObj.specialDepreciationAllowance,jsonObj.specialDepreciationAllowancePercent,jsonObj.swtch,jsonObj.depreciationMethodId,jsonObj.depreciationMethod,jsonObj.averagingConventionType);
                pagedData.data.push(group);
            }
            
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}