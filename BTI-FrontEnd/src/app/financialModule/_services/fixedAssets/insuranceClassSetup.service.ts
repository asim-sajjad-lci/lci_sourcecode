/**
 * A service class for Insurance class setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { InsuranceClassSetup } from '../../../financialModule/_models/fixedAssets/insuranceClassSetup';


@Injectable()
export class InsuranceClassSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchInsuranceClassSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/insuranceClassSetup/search';
    private updateInsuranceClassSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/insuranceClassSetup/update';
    private saveInsuranceClassSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/insuranceClassSetup';
    private dropdownInsuranceClassIdUrl  = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/insuranceMaintenance/search';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

     private getPagedData(page: Page, data: any): PagedData<InsuranceClassSetup> {
        let pagedData = new PagedData<InsuranceClassSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new InsuranceClassSetup(jsonObj.insuranceClassId,jsonObj.insuranceDescriptionPrimary,jsonObj.insuranceDescriptionSecondary,jsonObj.inflationPercent,jsonObj.depriciationRate);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }

    //Search Insurance class setup detials
    searchInsuranceClassSetup(page:Page, searchKeyword):Observable<PagedData<InsuranceClassSetup>>{
        return this.http.post(this.searchInsuranceClassSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


    //update Insurance class setup for edit 
    updateInsuranceClassSetup(InsuranceClassSetup:InsuranceClassSetup){
        return this.http.post(this.updateInsuranceClassSetupUrl,JSON.stringify(InsuranceClassSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

      //save Insurance class setup
    createInsuranceClassSetup(InsuranceClassSetup:InsuranceClassSetup)
    {
      return this.http.post(this.saveInsuranceClassSetupUrl,JSON.stringify(InsuranceClassSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

     
     getdropdownInsuranceClassId() {
        return this.http.post(this.dropdownInsuranceClassIdUrl, { 'searchKeyword': '' }, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
  
    //  getBookIDSetup(){
    //     return this.http.post(this.searchBookSetupUrl, { 'searchKeyword': '' }, {headers:this.headers})
    //              .toPromise()
    //             .then(res => res.json())
    //             .catch(this.handleError);
    // }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}