/**
 * A service class for FixedAssetsCalenderSetup 
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { FixedAssetsCalenderSetup } from '../../../financialModule/_models/fixedAssets/fixed-assets-calender-setup';



@Injectable()
export class FaCalenderSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchCalendarMonthlySetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchCalendarMonthlySetup';
    private getCalendarMonthlySetupByIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getCalendarMonthlySetupById';
    private updateCalendarMonthlySetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateCalendarMonthlySetup';
    private saveCalenderMonthlySetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/calendarMonthlySetup';
    private getCalendarMonthlySetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getCalendarMonthlySetup';
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



    //Search calender monthly  detials
    searchFaCalendarSetup(page:Page, searchKeyword):Observable<PagedData<FixedAssetsCalenderSetup>>{
        return this.http.post(this.searchCalendarMonthlySetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }
//getCalendarMonthlySetup
getCalendarMonthlySetup() {
       return this.http.get(this.getCalendarMonthlySetupUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

     //getting details of calender monthly setup  details By ID
    getCalendarMonthlySetupById(calendarIndex: number) {
        return this.http.post(this.getCalendarMonthlySetupByIdUrl, { calendarIndex: calendarIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update calender monthly  details for edit 
    updateCalendarMonthlySetup(FixedAssetsCalenderSetup:FixedAssetsCalenderSetup){
        return this.http.post(this.updateCalendarMonthlySetupUrl,JSON.stringify(FixedAssetsCalenderSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //save calender monthly setup
    createCalendarMonthlySetup(FixedAssetsCalenderSetup:FixedAssetsCalenderSetup)
    {
      return this.http.post(this.saveCalenderMonthlySetupUrl,JSON.stringify(FixedAssetsCalenderSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<FixedAssetsCalenderSetup> {
        let pagedData = new PagedData<FixedAssetsCalenderSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new FixedAssetsCalenderSetup(jsonObj.calendarId,jsonObj.calendarDescription, jsonObj.calendarDescriptionArabic,jsonObj.year1, jsonObj.monthlyCalendar, jsonObj.calendarIndex);
                pagedData.data.push(group);
            }
            
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}