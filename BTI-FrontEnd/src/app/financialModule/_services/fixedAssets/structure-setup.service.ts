/**
 * A service class for structure setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { StructureSetup } from '../../../financialModule/_models/fixedAssets/structure-setup';


@Injectable()
export class StructureSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchStructureSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchStructureSetup';
    private getStuctureSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getStructureSetupById';
    private updateStructureSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateStructureSetup';
    private saveStructureSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/structureSetup';
     
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



    //Search structure setup detials
    searchStructureSetup(page:Page, searchKeyword):Observable<PagedData<StructureSetup>>{
        return this.http.post(this.searchStructureSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }



     //getting details of  structure setup details By ID
    getStructureSetupById(structureIndex: number) {
         return this.http.post(this.getStuctureSetupByIdUrl, { structureIndex: structureIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update structure details for edit 
    updateStructureSetup(StructureSetup:StructureSetup){
        return this.http.post(this.updateStructureSetupUrl,JSON.stringify(StructureSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //save structure setup
    createStructureSetup(StructureSetup:StructureSetup)
    {
      return this.http.post(this.saveStructureSetupUrl,JSON.stringify(StructureSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<StructureSetup> {
        let pagedData = new PagedData<StructureSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new StructureSetup(jsonObj.structureId,jsonObj.structureDescription, jsonObj.structureDescriptionArabic,jsonObj.structureIndex);
                pagedData.data.push(group);
            }
            
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}