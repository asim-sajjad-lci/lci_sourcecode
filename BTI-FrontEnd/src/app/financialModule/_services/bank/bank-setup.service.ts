
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { BankSetup } from '../../../financialModule/_models/bank/bank-setup';


@Injectable()
export class BankSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private addBankSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/bankSetup';
    private getByIdBankSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/bankSetupGetById';
    private updateBankSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/bankSetupUpdate';
    private searchBankSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchBankSetup';
    private getCountryListUrl = Constants.financialModuleApiBaseUrl + 'getCountryList';
    private getStateListUrl = Constants.financialModuleApiBaseUrl + 'getStateListByCountryId';
    private getCityListUrl = Constants.financialModuleApiBaseUrl + 'getCityListByStateId';
    private getCountryCodeUrl = Constants.userModuleApiBaseUrl + 'getCountryCodeByCountryId';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
   //getting detail of bank
    getByIdBankSetup(id: string) {
        return this.http.post(this.getByIdBankSetupUrl, {bankId: id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   //update bank for edit
    updateBankSetup(bankSetup:BankSetup) {
        return this.http.post(this.updateBankSetupUrl, JSON.stringify(bankSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //add new bank
    addBankSetup(bankSetup:BankSetup) {
        return this.http.post(this.addBankSetupUrl,JSON.stringify(bankSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //search data from bankSetup
    searchBankSetup(page: Page,searchKeyword): Observable<PagedData<BankSetup>> {
        return this.http
            .post(this.searchBankSetupUrl, {'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
    
     //getting country list
    getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getting state list
    getStateList(countryId: any) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //getting city list
    getCityList(stateId: any) {
        return this.http.post(this.getCityListUrl, { 'stateId': stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // //getting country code list
    // getCountryCode(countryId: any) {
    //     return this.http.post(this.getCountryCodeUrl, { 'countryId': countryId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    getBankId(){
       var bankId=[];
    
        if(this.currentLanguage == "1")
        {
          bankId = [
           {id: 1, name: "-- Select --"}
          ];
        }
        else
        {
           bankId = [
             {id: 1, name: "-- تحديد --"}
          ];
        }
       return bankId;
    }

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<BankSetup> {
        let pagedData = new PagedData<BankSetup>();
        if(data)
        {
            var gridRecords = data.records;
             page.totalElements = data.totalCount;
             page.totalPages = page.totalElements / page.size;
             let start = page.pageNumber * page.size;
             let end = Math.min((start + page.size), page.totalElements);
             for (let i = 0; i < gridRecords.length; i++) {
                 let jsonObj = gridRecords[i];
                 let employee = new BankSetup(jsonObj.bankId,jsonObj.bankDescription,jsonObj.bankArabicDescription,jsonObj.address1,jsonObj.address2,jsonObj.address3,jsonObj.phone1,jsonObj.phone2,jsonObj.phone3,jsonObj.cityId,jsonObj.stateId,jsonObj.countryId,jsonObj.fax);
                 pagedData.data.push(employee);
             }
             pagedData.page = page;     
        }
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}