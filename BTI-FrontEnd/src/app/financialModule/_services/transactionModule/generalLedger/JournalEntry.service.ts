/**
 * A service class for Journal Entry Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { JournalEntrySetup } from "../../../../financialModule/_models/transactionModule/generalLedger/JournalEntry";
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { PostedJournalEntry } from '../../../_models/transactionModule/generalLedger/PostedJournalEntry';

@Injectable()
export class JournalEntryService {
    private headers = new Headers({ 'content-type': 'application/json' });
    //private postJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'post/postingWithNextNumber?flage=1';
    private saveJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'post/postingWithNextNumber?flage=1';
    private updateJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/updateJournalEntry';
    private getJournalIdListUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getJournalIdList';
    private getAllUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getAllJournalHeader'
    private CopyJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/copyJournalEntry';
    private getYearsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/massCloseFiscalPeriodSetup/getYears';
    private getJournalEntryByYearUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getJournalEntryByYear';
    private actionListUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/correctJournalEntry/action';
    private saveCorrectJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/correctJournalEntry';
    private getCorrectJournalEntryIdByYearUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getCorrectJournalEntryIdByYear';
    private getExchangeTableSetupByCurrencyIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getExchangeTableSetupByCurrencyId';
    private getJournalEntryByJournalIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getJournalEntryByJournalId';
    private getJournalEntryByJournalIdAndAuditUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getJournalEntryByJournalIdAndAudit';
    private postJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/postJournalEntry';
    private getAllPostJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getAllPostedJournalHeader';
    private getCurrencyExchangeDetailUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencyExchangeDetail';
    private deleteJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/deleteJournalEntry';
    private deleteJournalEntriesUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/deleteJournalEntries';
    private getReportUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/reports/getTransactionReport';
    private getPostedJournalEntry = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getPostedJournalHeaderById';
    private getPostedJournalEntryByIdAndSourceDocument = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getPostedJournalHeaderByIdAndAudit'

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }


    //add new Journal Entry
    SaveUpdateJournalSetup(journalEntrySetup: JournalEntrySetup, isUpdate: boolean) {
        if (isUpdate) {
            return this.http.post(this.updateJournalEntryUrl, JSON.stringify(journalEntrySetup), { headers: this.headers })
                .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
        }
        else {
            return this.http.post(this.saveJournalEntryUrl, JSON.stringify(journalEntrySetup), { headers: this.headers })
                .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
        }
    }

    //Delete Journal Entry
    DeleteJournalEntry(journalID: string) {
        return this.http.post(this.deleteJournalEntryUrl, { 'journalID': journalID }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Journal Entry
    DeleteJournalEntries(ids: any) {
        return this.http.post(this.deleteJournalEntriesUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Journal Entry
    getJournalIdList() {
        return this.http.get(this.getJournalIdListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Years List
    getYearsList() {
        return this.http.get(this.getYearsUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Journal Entry By Year
    getJournalEntryByYear(year: string) {

        return this.http.post(this.getJournalEntryByYearUrl, { year: year }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //save details of Correct Journal Entry
    savecorrectJournalEntry(journalID: string, year: string, actionType: string) {

        return this.http.post(this.saveCorrectJournalEntryUrl, { 'journalID': journalID, 'year': year, 'actionType': actionType }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //get Action List
    actionList() {
        return this.http.get(this.actionListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Journal Entry By Year
    getCorrectJournalEntryIdByYear(year: string) {

        return this.http.post(this.getCorrectJournalEntryIdByYearUrl, { year: year }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //save details of Copy Journal Entry
    CopyJournalEntry(journalID: string, year: string) {

        return this.http.post(this.CopyJournalEntryUrl, { journalID: journalID, year: year }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Exchange Table SetupByCurrencyId
    GetCurrencyExchangeDetail(exchangeId: string) {

        return this.http
            .post(this.getCurrencyExchangeDetailUrl, { 'exchangeId': exchangeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting ExchangeTableSetup By CurrencyId
    getExchangeTableSetupByCurrencyId(currencyId: string) {
        return this.http
            .post(this.getExchangeTableSetupByCurrencyIdUrl, { currencyId: currencyId, 'pageNumber': 0, 'pageSize': 1000 }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }



    //getting detail of JournalEntry By JournalId
    getJournalEntryByJournalId(journalID: string) {
        return this.http.post(this.getJournalEntryByJournalIdUrl, { journalID: journalID }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get JournalEntry By JournalId
    GetJournalEntryByJournalId(journalID: string) {

        return this.http.post(this.getJournalEntryByJournalIdUrl, { journalID: journalID }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get JournalEntry By JournalId And Audit 
    GetJournalEntryByJournalIdAndAudit(journalEntryId:string , sourceDocumentId: number) {

        return this.http.post(this.getJournalEntryByJournalIdAndAuditUrl, { journalID: journalEntryId,sourceDocumentId:sourceDocumentId  }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Post Journal Entry
    PostJournalSetup(journalEntrySetup: JournalEntrySetup) {
        return this.http.post(this.postJournalEntryUrl, JSON.stringify(journalEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Report
    GetReport(journalID: string) {

        return this.http.post(this.getReportUrl, { journalID: journalID }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    // get all Unposted jpurnal entry for grid
    getAllUnpostedJournalEntry(page: Page, searchKeyword): Observable<PagedData<JournalEntrySetup>> {
        return this.http
            .post(this.getAllUrl, { 'searchKeyword': searchKeyword, 'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedDataUnpostedJournalEntry(page, data.json().result))
            .catch(this.handleError);
    }

    //bind json data for view
    private getPagedDataUnpostedJournalEntry(page: Page, data: any): PagedData<JournalEntrySetup> {
        let pagedData = new PagedData<JournalEntrySetup>();
        if (data) {
            var gridRecords = data.records;
            console.log(data);
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];

                let journalEntry = new JournalEntrySetup(
                    jsonObj.id,
                    jsonObj.journalID, jsonObj.journalID, jsonObj.glBatchId, jsonObj.transactionType,
                    jsonObj.transactionDate, jsonObj.transactionReversingDate,
                    jsonObj.sourceDocumentId,
                    jsonObj.journalDescription, jsonObj.journalDescriptionArabic,
                    jsonObj.currencyID,
                    jsonObj.totalJournalEntryDebit, jsonObj.totalJournalEntryCredit,
                    jsonObj.exchangeTableIndex, jsonObj.interCompany,
                    jsonObj.journalEntryDetailsList, jsonObj.exchangeRate,
                    jsonObj.exchangeDate,
                    jsonObj.sourceDocument);

                pagedData.data.push(journalEntry);
            }
            pagedData.page = page;
        }
        return pagedData;
    }

    // get all posted journal entry for grid
    getAllPostedJournalEntry(page: Page, searchKeyword): Observable<PagedData<PostedJournalEntry>> {
        return this.http
            .post(this.getAllPostJournalEntryUrl, { 'searchKeyword': searchKeyword, 'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedDataPostedJournalEntry(page, data.json().result))
            .catch(this.handleError);
    }

    // get Posted Journal Entry for grid
    getPostedJournalById(JournalID: string) {
        return this.http.post(this.getPostedJournalEntry, { 'journalID': JournalID }, { headers: this.headers })
            .map(data => data.json())
            .catch(this.handleError);
    }

    // get Posted Journal Entry for grid
    getPostedJournalByIdAndAudit(journalEntry) {
        return this.http.post(this.getPostedJournalEntryByIdAndSourceDocument, { 'journalID': journalEntry.journalEntryID, 'sourceDocumentId': journalEntry.sourceDocumentId }, { headers: this.headers })
            .map(data => data.json())
            .catch(this.handleError);
    }
    //bind json data for view
    private getPagedDataPostedJournalEntry(page: Page, data: any): PagedData<PostedJournalEntry> {
        let pagedData = new PagedData<PostedJournalEntry>();
        if (data) {
            console.log(data);
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];

                let journalEntry = new PostedJournalEntry(
                    jsonObj.rowIndex,
                    jsonObj.journalEntryID,
                    jsonObj.journalEntryID,
                    jsonObj.journalDescription,
                    jsonObj.journalDescriptionArabic,
                    jsonObj.transactionDate,
                    jsonObj.transactionPostingDate,
                    jsonObj.transaxtionSource,
                    jsonObj.accountTableRowIndex,
                    jsonObj.debitAmount,
                    jsonObj.creadiAmount,
                    jsonObj.totalDebitAmmount,
                    jsonObj.totalCreditAmmount,
                    jsonObj.sourceDocument,
                    jsonObj.sourceDocumentId,
                    jsonObj.originalTransactionNumber);

                pagedData.data.push(journalEntry);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
}