import { Injectable } from "@angular/core";
import { Headers,Http } from "@angular/http";
import { Constants } from "../../../../_sharedresource/Constants";

@Injectable()
export class paymentVoucher {
    
    private headers = new Headers({ 'content-type': 'application/json' });
    private getNextPaymentVoucherIDURL = Constants.financialModuleApiBaseUrl+'transaction/paymentVoucher/getNextPaymentVoucherID';
    private getPaymentVoucerIDsURL = Constants.financialModuleApiBaseUrl+'transaction/paymentVoucher/getAllVoucherIDs';
    private savePaymentVoucherURL = Constants.financialModuleApiBaseUrl + 'transaction/paymentVoucher/savePaymentVoucher';
    private getPaymentVoucherByIDURL = Constants.financialModuleApiBaseUrl+'transaction/paymentVoucher/getPaymentVoucherById';
    private deletePaymentVoucherByIdURL = Constants.financialModuleApiBaseUrl+'transaction/paymentVoucher/deletePaymentVoucherById';
    private updatePaymentVoucherByIdURL = Constants.financialModuleApiBaseUrl+'transaction/paymentVoucher/updatePaymentVoucher';
    private postPaymentVoucherByIdURL = Constants.financialModuleApiBaseUrl+'transaction/paymentVoucher/postPaymentVoucher';
    private getAccountNumberByCheckbookIDURL = Constants.financialModuleApiBaseUrl + 'transaction/paymentVoucher/getCheckbookIDAccount';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    getNextPaymentVoucherID(){
        return this.http.get(this.getNextPaymentVoucherIDURL,{headers:this.headers})
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }


    getPaymentVoucerIDs(){
        return this.http.get(this.getPaymentVoucerIDsURL,{headers:this.headers})
            .toPromise().then(res => res.json())
            .catch(this.handleError);
    }

    savePaymentVoucher(model:any){
        return this.http.post(this.savePaymentVoucherURL,JSON.stringify(model),{headers:this.headers})
        .toPromise().then(res => res.json())
        .catch(this.handleError);
    }

    getPaymentVoucherById(paymentVoucherId){
        return this.http.post(this.getPaymentVoucherByIDURL,{"paymentVoucherID":paymentVoucherId},{headers:this.headers})
            .toPromise().then(res => res.json())
            .catch(this.handleError);
    }

    deletePaymentVoucherById(paymentVoucherID){
        return this.http.post(this.deletePaymentVoucherByIdURL,{"paymentVoucherID":paymentVoucherID},{headers:this.headers});
    }

    updatePaymentVoucherById(model:any){
        return this.http.post(this.updatePaymentVoucherByIdURL,JSON.stringify(model),{headers:this.headers});
    }

    postPaymentVoucher(model:any){
        return this.http.post(this.postPaymentVoucherByIdURL,JSON.stringify(model),{headers:this.headers});
    }

    getCheckbookIDAccount(checkbookID){
        return this.http.post(this.getAccountNumberByCheckbookIDURL,{"checkBookId":checkbookID},{headers:this.headers});
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }



}