/**
 * A service class for Bank Transfer Entry Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { BankTransferEntrySetup } from '../../../../financialModule/_models/transactionModule/generalLedger/bankTransferEntry';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class BankTransferEntryService {
    private headers = new Headers({ 'content-type': 'application/json' }); 
    private SaveBankTransferEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/saveBankTransferEntry';
    private UpdateBankTransferEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/updateBankTransferEntry';
    private GetBankTransferEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBankTransferEntry';
    private DeleteBankTransferEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/deleteBankTransferEntry';
    private PostBankTransferEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/postBankTransferEntry';
    private GetBankTranferNumbersListUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBankTranferNumbersList';
    private GetBankTransferDistrubutionByBankTransferNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBankTransferDistrubutionByBankTransferNumber';
    private SaveBankTransferDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/saveBankTransferDistribution';
    private DeleteBankTransferEntryChargesUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/deleteBankTransferEntryCharges';
    private GetBankTransferChargesByBankTransferNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBankTransferChargesByBankTransferNumber';
    private SaveBankTransferChargesUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/saveBankTransferCharges';
    private GetBankTransferChargesByChargeNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBankTransferChargesByChargeNumber';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //Save Update Bank Transfer Entry
    SaveUpdateBankTransferEntry(bankTransferEntrySetup:BankTransferEntrySetup,isUpdate:boolean) {
        
        if(isUpdate)
        {
            return this.http.post(this.UpdateBankTransferEntryUrl, JSON.stringify(bankTransferEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.SaveBankTransferEntryUrl,JSON.stringify(bankTransferEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }


    }

    //update Bank Transfer Entry for edit
    UpdateBankTransferEntry(bankTransferEntrySetup:BankTransferEntrySetup) {
        return this.http.post(this.UpdateBankTransferEntryUrl, JSON.stringify(bankTransferEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Bank Transfer Entry
    GetBankTransferEntry(bankTransferNumber: string) {
        return this.http.post(this.GetBankTransferEntryUrl, {bankTransferNumber: bankTransferNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Bank Transfer Entry
    DeleteBankTransferEntry(bankTransferNumber: string) {
        return this.http.post(this.DeleteBankTransferEntryUrl, {bankTransferNumber: bankTransferNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //Post Bank Transfer Entry
    PostBankTransferEntry(bankTransferEntrySetup:BankTransferEntrySetup) {
        
        return this.http.post(this.PostBankTransferEntryUrl,JSON.stringify(bankTransferEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //Get BankTranferNumbers List
    GetBankTranferNumbersList() {
        return this.http.get(this.GetBankTranferNumbersListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //Get BankTransfer Distrubution By Bank Transfer Number
    GetBankTransferDistrubutionByBankTransferNumber(bankTransferNumber: string) {
        return this.http.post(this.GetBankTransferDistrubutionByBankTransferNumberUrl, {bankTransferNumber: bankTransferNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    
    //Save Bank Transfer Entry
    SaveBankTransferDistribution(bankTransferNumber: string,accountTableRowIndexFrom: string,accountTableRowIndexTo: string) {
        return this.http.post(this.SaveBankTransferDistributionUrl, {bankTransferNumber: bankTransferNumber,accountTableRowIndexFrom: accountTableRowIndexFrom,accountTableRowIndexTo:accountTableRowIndexTo}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //Save Bank Transfer Charges
     SaveBankTransferCharges(submitChargesEntry) {
        return this.http.post(this.SaveBankTransferChargesUrl,submitChargesEntry, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //Delete Bank Transfer Entry
    DeleteBankTransferEntryCharges(chargeNumber: string) {
        return this.http.post(this.DeleteBankTransferEntryChargesUrl, {chargeNumber: chargeNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Bank Transfer Charges By BankTransferNumber
    GetBankTransferChargesByBankTransferNumber(bankTransferNumber: string) {
        return this.http.post(this.GetBankTransferChargesByBankTransferNumberUrl, {bankTransferNumber: bankTransferNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get BankTransferCharges By ChargeNumber
    GetBankTransferChargesByChargeNumber(chargeNumber: number) {
        return this.http.post(this.GetBankTransferChargesByChargeNumberUrl, {chargeNumber: chargeNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

}
