/**
 * A service class for GL Batch Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { BatchSetup } from '../../../../financialModule/_models/transactionModule/generalLedger/batchSetup';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class BatchSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private saveBatchesUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/saveBatches';
    private getAllBatchesTransactionUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getAllBatchesTransaction';
    private getAllBatchesUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getAllBatches';
    private updateBatchesUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/updateBatches';
    private getBatchesByBatchIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBatchesByBatchId';
    private getBatchTotalTrasactionsByTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getBatchTotalTrasactionsByTransactionType';
    private postBatchTransactionUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/postBatchTransaction';
    private deleteBatchesByBatchIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/deleteBatchesByBatchId';
   
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new Batch
    saveBatches(batchSetup:BatchSetup) {
        
        return this.http.post(this.saveBatchesUrl,JSON.stringify(batchSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //Delete Batches
     DeleteBatches(batchId:string,transactionTypeId:string)
     {
         return this.http.post(this.deleteBatchesByBatchIdUrl,{'batchId':batchId,'transactionTypeId':transactionTypeId}, { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
     }

    //getting Batches Transaction
    getAllBatches(page: Page,searchKeyword): Observable<PagedData<BatchSetup>> {
        return this.http
            .post(this.getAllBatchesUrl, {'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }

      //getting Batches Transaction
    getAllBatchesList() {
               return this.http.post(this.getAllBatchesUrl,{'searchKeyword':''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

    //getting Batches Transaction
    getAllBatchesTransaction() {
        return this.http.get(this.getAllBatchesTransactionUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //update Batches for edit
    updateBatches(batchSetup:BatchSetup) {
        
        return this.http.post(this.updateBatchesUrl, JSON.stringify(batchSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Batches
    getBatchesByBatchId(id: string,transactionTypeId: string) {
        return this.http.post(this.getBatchesByBatchIdUrl, {batchId: id,transactionTypeId: transactionTypeId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Batch TotalTrasactions By TransactionType
    getBatchTotalTrasactionsByTransactionType(batchId: string, transactionTypeId:string) {
        return this.http.post(this.getBatchTotalTrasactionsByTransactionTypeUrl, {batchId:batchId,transactionTypeId:transactionTypeId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getting detail of Batch Trasaction
    PostBatchTransaction(batchId: string, transactionTypeId:string) {
        return this.http.post(this.postBatchTransactionUrl, {batchId:batchId,transactionTypeId:transactionTypeId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }   
    

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<BatchSetup> {
        let pagedData = new PagedData<BatchSetup>();
        if(data)
        {
            var gridRecords = data.records;
             page.totalElements = data.totalCount;
             page.totalPages = page.totalElements / page.size;
             let start = page.pageNumber * page.size;
             let end = Math.min((start + page.size), page.totalElements);
             for (let i = 0; i < gridRecords.length; i++) {
                 let jsonObj = gridRecords[i];
                 let employee = new BatchSetup(jsonObj.batchId,jsonObj.description,jsonObj.transactionTypeId,jsonObj.transactionType,jsonObj.totalTransactions,jsonObj.quantityTotal,jsonObj.sourceDocument);
                 pagedData.data.push(employee);
             }
             pagedData.page = page;     
        }
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}