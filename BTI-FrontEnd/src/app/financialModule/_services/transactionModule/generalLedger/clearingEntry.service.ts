/**
 * A service class for Clearing Entry Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { ClearingEntry } from '../../../../financialModule/_models/transactionModule/generalLedger/clearingEntry';
import { Constants } from '../../../../_sharedresource/Constants';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class ClearingEntryService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private saveClearingJournalEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/saveClearingJournalEntry';
    private updateClearingJournalEntryUrl = Constants.financialModuleApiBaseUrl + '/transaction/generalLedger/updateClearingJournalEntry';
    private postClearingJournalEntryUrl = Constants.financialModuleApiBaseUrl + '/transaction/generalLedger/postClearingJournalEntry';
    private getClearingJournalEntryByJournalIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getClearingJournalEntryByJournalId';
    private getJournalIdListForUpdateClearingJVEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getJournalIdListForUpdateClearingJVEntry';
    private getAccountNumberForClearingJVEntryUrl = Constants.financialModuleApiBaseUrl +'transaction/generalLedger/getAccountNumberForClearingJVEntry'
    private deleteClearingJournalEntryUrl = Constants.financialModuleApiBaseUrl +'transaction/generalLedger/deleteClearingJournalEntry'

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new Clearing Journal Entry Seetup
    SaveUpdateClearingJournalEntry(clearingEntry:ClearingEntry, isUpdate:boolean) {
        
        if(isUpdate)
        {
            return this.http.post(this.updateClearingJournalEntryUrl,JSON.stringify(clearingEntry), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveClearingJournalEntryUrl,JSON.stringify(clearingEntry), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        
    }

      //Delete Clearing Journal Entry
      DeleteClearingJournalEntry(journalID:string)
      {
          
          return this.http.post(this.deleteClearingJournalEntryUrl,{'journalID':journalID}, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
      }

    PostClearingJournalEntry(clearingEntry:ClearingEntry)
    {
        return this.http.post(this.postClearingJournalEntryUrl,JSON.stringify(clearingEntry), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
    
    //getting detail of Clearing Journal Entry
    GetClearingJournalEntryByJournalId(id: string) {
        return this.http.post(this.getClearingJournalEntryByJournalIdUrl, {journalID: id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


     //get JournalId List For Update Clearing JV EntryUrl
     getJournalIdListForUpdateClearingJVEntry()
     {
         return this.http.get(this.getJournalIdListForUpdateClearingJVEntryUrl,{ headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
     }

      //get Account Number List For Clearing JV Entry
      getAccountNumberForClearingJVEntry()
      {
          return this.http.get(this.getAccountNumberForClearingJVEntryUrl,{ headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
      }
     

    // private getPagedData(page: Page, data: any): PagedData<ClearingEntry> {
    //     let pagedData = new PagedData<ClearingEntry>();
    //     if(data)
    //     {
    //         var gridRecords = data.records;
    //          page.totalElements = data.totalCount;
    //          page.totalPages = page.totalElements / page.size;
    //          let start = page.pageNumber * page.size;
    //          let end = Math.min((start + page.size), page.totalElements);
    //          for (let i = 0; i < gridRecords.length; i++) {
    //              let jsonObj = gridRecords[i];
    //              let employee = new ClearingEntry(jsonObj.journalID,jsonObj.balanceType,jsonObj.transactionDate,jsonObj.transactionDescription,jsonObj.glBatchId,jsonObj.sourceDocumentId,jsonObj.journalEntryDetailsList);
    //              pagedData.data.push(employee);
    //          }
    //          pagedData.page = page;     
    //     }
    //     return pagedData;
    // }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}