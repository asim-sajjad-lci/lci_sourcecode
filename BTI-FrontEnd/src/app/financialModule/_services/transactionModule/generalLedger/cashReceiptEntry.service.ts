/**
 * A service class for Cash Receipt Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { CashReceiptSetup } from '../../../../financialModule/_models/transactionModule/generalLedger/cashReceiptEntry';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class CashReceiptService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private saveCashReceiptEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/saveCashReceiptEntry';    
    private getAllCashReceiptNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getAllCashReceiptNumber';
    private updateCashReceiptEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/updateCashReceiptEntry';
    private getCashReceiptEntryByReceiptNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getCashReceiptEntryByReceiptNumber';
    private postCashReceiptEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/postCashReceiptEntry';
    private getPaymentMethodTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getPaymentMethodType';
    private getReceiptTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getReceiptType';
    private getCheckBookMaintenanceByCheckbookIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCheckBookMaintenanceByCheckbookId';
    private maintenanceGetByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/maintenanceGetById';
    private getVatDetailSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getVatDetailSetupById';
    private getCashReceiptDistributionByReceiptNumberUrl = Constants.financialModuleApiBaseUrl + '/transaction/generalLedger/getCashReceiptDistributionByReceiptNumber';
    private saveCashReceiptDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/saveCashReceiptDistribution';
    private deleteCashReceiptUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/deleteCashReceiptEntry';
    private getDistributionAccountTypesListUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getDistributionAccountTypesList';

    currentLanguage = "";
    
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //save Update Cash Receipt
    saveUpdateCashReceiptEntry(cashReceiptSetup:CashReceiptSetup,isUpdate:boolean) {
        if(isUpdate)
        {
            return this.http.post(this.updateCashReceiptEntryUrl,JSON.stringify(cashReceiptSetup),{headers:this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveCashReceiptEntryUrl,JSON.stringify(cashReceiptSetup),{headers:this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
    }
   
    //Delete Cash Reciept
    DeleteCashReciept(receiptNumber:string)
    {
        return this.http.post(this.deleteCashReceiptUrl,{'receiptNumber':receiptNumber}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //update Cash Receipt for edit
    updateCashReceiptEntry(cashReceiptSetup:CashReceiptSetup) {
        
        return this.http.post(this.updateCashReceiptEntryUrl, JSON.stringify(cashReceiptSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Reciept Number
    GetRecieptNumber() {
            return this.http.get(this.getAllCashReceiptNumberUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Distribution Account Types List
    getDistributionAccountTypesList() {
        return this.http.get(this.getDistributionAccountTypesListUrl, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
    

    //Getting detail of Cash Receipt By ReceiptNumber
    getCashReceiptEntryByReceiptNumber(receiptNumber: string) {
        return this.http.post(this.getCashReceiptEntryByReceiptNumberUrl, {receiptNumber: receiptNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Post Cash ReceiptEntry
    PostCashReceiptEntry(cashReceiptSetup:CashReceiptSetup) {
        
        return this.http.post(this.postCashReceiptEntryUrl,JSON.stringify(cashReceiptSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Get Payment Method
    GetPaymentMethod() {
        return this.http.get(this.getPaymentMethodTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Getting Receipt Type
    GetReceiptType() {
        return this.http.get(this.getReceiptTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Checkbook List
    GetCheckBookMaintenanceByCheckbookId(checkBookId: string) {
        return this.http.post(this.getCheckBookMaintenanceByCheckbookIdUrl, {checkBookId: checkBookId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Customer List
    maintenanceGetById(customerId: string) {
        return this.http.post(this.maintenanceGetByIdUrl, {customerId: customerId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get VatDetail Setup By VatId
    GetVatDetailSetupById(vatScheduleId: string) {
        return this.http.post(this.getVatDetailSetupByIdUrl, {vatScheduleId: vatScheduleId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    
       //getting detail of Cash Receipt Distribution By ReceiptNumber
    GetCashReceiptDistributionByReceiptNumber(receiptNumber: string) {
        return this.http.post(this.getCashReceiptDistributionByReceiptNumberUrl, {receiptNumber: receiptNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //saving detail of Cash Receipt Distribution By ReceiptNumber
    SaveCashReceiptDistribution(receiptNumber: string) {
        return this.http.post(this.saveCashReceiptDistributionUrl, {receiptNumber: receiptNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}