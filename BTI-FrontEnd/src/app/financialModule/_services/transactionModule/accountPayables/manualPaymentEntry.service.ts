/**
 * A service class for AR Cash Receipt Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { ManualPaymentEntry } from '../../../../financialModule/_models/transactionModule/accountPayables/manualPaymentEntry';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class ManualPaymentEntryService {
    private headers = new Headers({ 'content-type': 'application/json' });
   
   
    private getManualPaymentTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getManualPaymentType';
    private getAllManualPaymentNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getAllManualPaymentNumber';
    private saveManualPaymentEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/saveManualPaymentEntry';
    private updateManualPaymentEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/updateManualPaymentEntry';
    private postManualPaymentEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/postManualPaymentEntry';
    private getManualPaymentEntryByManualPaymentNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getManualPaymentEntryByManualPaymentNumber';
    private deleteManualPaymentEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/deleteManualPaymentEntry';
    private getDistributionAccountTypesListUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getDistributionAccountTypesList';
    private getManualPaymentDistributionByManualPaymentNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getManualPaymentDistributionByManualPaymentNumber';
    private saveManualPaymentDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/saveManualPaymentDistribution';
    private deleteManualPaymentDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/deleteManualPaymentDistribution';
    private getAPDistributionAccountTypesList = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getDistributionAccountTypesList';
    private getManualPaymentDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getManualPaymentDistribution';

    currentLanguage = "";
    
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //save Manual Payment  Entry
    SaveUpdateManualPaymentEntry(manualPaymentNumber:ManualPaymentEntry,isUpdate:boolean) {
        
        if(isUpdate)
        {
            return this.http.post(this.updateManualPaymentEntryUrl,JSON.stringify(manualPaymentNumber), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveManualPaymentEntryUrl,JSON.stringify(manualPaymentNumber), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
    }

    //Get Distribution Account Type List
    GetDistributionAccountTypesList() {
        return this.http.get(this.getAPDistributionAccountTypesList, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

      //Get Manual Payment Entry By Manual Payment Number
    GetManualPaymentEntryByManualPaymentNumber(manualPaymentNumber: string) {
        return this.http.post(this.getManualPaymentEntryByManualPaymentNumberUrl, {manualPaymentNumber: manualPaymentNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Post Manual Payment  Entry
    PostManualPaymentEntry(manualPaymentNumber:ManualPaymentEntry) {
        
        return this.http.post(this.postManualPaymentEntryUrl,JSON.stringify(manualPaymentNumber), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Manual Payment Distribution By Manual Payment Number
    GetManualPaymentDistributionByManualPaymentNumber(manualPaymentNumber: string) {
        return this.http.post(this.getManualPaymentDistributionByManualPaymentNumberUrl, {manualPaymentNumber: manualPaymentNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Manual Payment Entry
    DeleteManualPaymentEntry(manualPaymentNumber:string)
    {
        
        return this.http.post(this.deleteManualPaymentEntryUrl,{manualPaymentNumber:manualPaymentNumber}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //Get Manual Payment Type
    GetManualPaymentType() {
            return this.http.get(this.getManualPaymentTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get All Manual Payment Number
    GetAllManualPaymentNumber() {
            return this.http.get(this.getAllManualPaymentNumberUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   

    //Save Manual Payment Distribution
    SaveManualPaymentDistribution(manualPaymentNumber:ManualPaymentEntry) {
        
        return this.http.post(this.saveManualPaymentDistributionUrl,manualPaymentNumber, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Manual Payment Distribution
    DeleteManualPaymentDistribution(manualPaymentNumber:string)
    {
        
        return this.http.post(this.deleteManualPaymentDistributionUrl,{manualPaymentNumber:manualPaymentNumber}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    /**
     * Get Manual Payment Distribution
     * @param ManualPaymentEntry 
     */
    GetManualPaymentDistribution(ManualPaymentEntry:ManualPaymentEntry)
    {
        return this.http.post(this.getManualPaymentDistributionByManualPaymentNumberUrl,JSON.stringify(ManualPaymentEntry), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}