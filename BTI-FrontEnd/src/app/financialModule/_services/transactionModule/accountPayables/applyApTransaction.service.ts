/**
 * A service class for AR Batch Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
//import { BatchSetup } from '../../../../financialModule/_models/transactionModule/accountReceivables/batchSetup';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class ApplyAPTransactionService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private getDocumentTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getDocumentType';
    private getDocumentNumberByVendorAndDocTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getDocumentNumberByVendorAndDocType';
    private getOpenTransactionsByVendorIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getOpenTransactionsByVendorId';
    private getOpenTransactionsByPostingDateUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getOpenTransactionsByPostingDate';
    private getAmountByPaymentNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getAmountByPaymentNumber';
    private applyTransactionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/applyTransaction';
    private unApplyTransactionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/unApplyTransaction';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //getting Document Type
    GetDocumentType() {
        return this.http.get(this.getDocumentTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting Document Number By Vendor And DocType
    GetDocumentNumberByVendorAndDocType(vendorID: string, documentType:string) {
        return this.http.post(this.getDocumentNumberByVendorAndDocTypeUrl, {vendorID:vendorID,documentType:documentType},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getting Open Transactions By VendorId
    GetOpenTransactionsByVendorId(vendorID: string) {
        return this.http.post(this.getOpenTransactionsByVendorIdUrl, {vendorID:vendorID},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    
    //getting Open Transactions By PostingDate
    GetOpenTransactionsByPostingDate(vendorID: string,postingDate:string,paymentNumber:string) {
        return this.http.post(this.getOpenTransactionsByPostingDateUrl, {vendorID:vendorID,postingDate:postingDate,paymentNumber:paymentNumber},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting Amount By Payment Number
    GetAmountByPaymentNumber(paymentNumber:string) {
        return this.http.post(this.getAmountByPaymentNumberUrl, {paymentNumber:paymentNumber},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //Apply Transaction 
    ApplyTransaction(applyTransaction:any) {
        return this.http.post(this.applyTransactionUrl, JSON.stringify(applyTransaction),{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //UnApply Transaction
    UnApplyTransaction(applyTransaction:any) {
        return this.http.post(this.unApplyTransactionUrl,JSON.stringify(applyTransaction),{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}