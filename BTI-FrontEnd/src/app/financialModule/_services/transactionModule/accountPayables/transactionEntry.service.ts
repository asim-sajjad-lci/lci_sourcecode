/**
 * A service class for AP Transaction Entry Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { TransactionEntrySetup } from '../../../../financialModule/_models/transactionModule/accountPayables/transactionEntry';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class TransactionEntryService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private saveTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/saveTransactionEntry';
    private updateTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/updateTransactionEntry';
    private getTransactionEntryByTransactionNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getTransactionEntryByTransactionNumber';
    private getTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getTransactionType';
    private getTransactionNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getTransactionNumber';
    private getVendorAccountDetailByVendorIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getVendorAccountDetailByVendorId';
    private getTransactionNumberByTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getTransactionNumberByTransactionType';
    private getDistributionDetailByTransactionNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getDistributionDetailByTransactionNumber';
    private saveDistributionDetailUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/saveDistributionDetail';
    private deleteTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/deleteTransactionEntry';
    private deleteTransactionEntryDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/deleteTransactionEntryDistribution';
    private postAPTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/postAPTransactionEntry';
    private getAPDistributionAccountTypesList = Constants.financialModuleApiBaseUrl + 'transaction/accountPayable/getDistributionAccountTypesList';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //Save or Update AP Transaction Entry
    SaveUpdateTransactionEntry(transactionEntrySetup:any,isUpdate:boolean) {
        
        if(isUpdate)
        {
            return this.http.post(this.updateTransactionEntryUrl, JSON.stringify(transactionEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveTransactionEntryUrl,JSON.stringify(transactionEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
    }

    //getting detail of Transaction Entry by Transaction Number
    GetTransactionEntryByTransactionNumber(apTransactionNumber: string) {
        return this.http.post(this.getTransactionEntryByTransactionNumberUrl, {apTransactionNumber: apTransactionNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Transaction Type List
    GetTransactionType() {
        return this.http.get(this.getTransactionTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Transaction Number By Transaction Type
    GetTransactionNumber(apTransactionType: string) {
        return this.http.post(this.getTransactionNumberUrl, {apTransactionType: apTransactionType}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Vendor AccountDetail By VendorId 
    GetVendorAccountDetailByVendorId(venderId: string) {
        return this.http.post(this.getVendorAccountDetailByVendorIdUrl, {venderId: venderId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getting detail of Transaction Number List by Transaction Type
    GetTransactionNumberByTransactionType(apTransactionType: string) {
        return this.http.post(this.getTransactionNumberByTransactionTypeUrl, {apTransactionType: apTransactionType}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //Get Distribution Account Type List
    GetDistributionAccountTypesList() {
        return this.http.get(this.getAPDistributionAccountTypesList, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //getting detail of Transaction Entry Distribution by Transaction Number
    GetDistributionDetailByTransactionNumber(apTransactionNumber: string) {
        return this.http.post(this.getDistributionDetailByTransactionNumberUrl, {apTransactionNumber: apTransactionNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Transaction Entry by Transaction detail
    GetDistributionDetailByTransaction(objTransactionNumber: any) {
        return this.http.post(this.getDistributionDetailByTransactionNumberUrl, JSON.stringify(objTransactionNumber), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    SaveDistributionDetail(APDistributionDetail:any) {
        return this.http.post(this.saveDistributionDetailUrl, JSON.stringify(APDistributionDetail), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
    
     //Delete Transaction Entry
     DeleteTransactionEntry(apTransactionNumber:string)
     {
         
         return this.http.post(this.deleteTransactionEntryUrl,{apTransactionNumber:apTransactionNumber}, { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
     }
     
     //Delete Transaction Number Distribution
     DeleteTransactionEntryDistribution(apTransactionNumber:string)
     {
         
         return this.http.post(this.deleteTransactionEntryDistributionUrl,{apTransactionNumber:apTransactionNumber}, { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
     }

     //getting detail of Transaction Entry by Transaction Number
    PostAPTransactionEntry(transactionEntrySetup:any) {
        return this.http.post(this.postAPTransactionEntryUrl,JSON.stringify(transactionEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
     
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}

