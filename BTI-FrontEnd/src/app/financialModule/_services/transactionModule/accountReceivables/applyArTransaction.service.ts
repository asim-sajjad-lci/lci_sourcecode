/**
 * A service class for AR Batch Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
//import { BatchSetup } from '../../../../financialModule/_models/transactionModule/accountReceivables/batchSetup';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class ApplyARTransactionService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private getDocumentTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getDocumentType';
    private getDocumentNumberByCustomerAndDocTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getDocumentNumberByCustomerAndDocType';
    private getOpenTransactionsByCustomerIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getOpenTransactionsByCustomerId';
    private getOpenTransactionsByPostingDateUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getOpenTransactionsByPostingDate';
    private getAmountByPaymentNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getAmountByPaymentNumber';
    private applyTransactionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/applyTransaction';
    private unApplyTransactionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/unApplyTransaction';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //getting Document Type
    GetDocumentType() {
        return this.http.get(this.getDocumentTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting Document Number By Customer And DocType
    GetDocumentNumberByCustomerAndDocType(customerID: string, documentType:string) {
        return this.http.post(this.getDocumentNumberByCustomerAndDocTypeUrl, {customerID:customerID,documentType:documentType},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getting Open Transactions By CustomerId
    GetOpenTransactionsByCustomerId(customerID: string) {
        return this.http.post(this.getOpenTransactionsByCustomerIdUrl, {customerID:customerID},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    
    //getting Open Transactions By PostingDate
    GetOpenTransactionsByPostingDate(customerID: string,postingDate:string,paymentNumber:string) {
        return this.http.post(this.getOpenTransactionsByPostingDateUrl, {customerID:customerID,postingDate:postingDate,paymentNumber:paymentNumber},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting Amount By Payment Number
    GetAmountByPaymentNumber(paymentNumber:string) {
        return this.http.post(this.getAmountByPaymentNumberUrl, {paymentNumber:paymentNumber},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //Apply Transaction 
    ApplyTransaction(applyTransaction:any) {
        return this.http.post(this.applyTransactionUrl, JSON.stringify(applyTransaction),{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //UnApply Transaction
    UnApplyTransaction(applyTransaction:any) {
        return this.http.post(this.unApplyTransactionUrl,JSON.stringify(applyTransaction),{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}