/**
 * A service class for Transaction Entry Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { TransactionEntrySetup } from '../../../../financialModule/_models/transactionModule/accountReceivables/transactionEntry';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class TransactionEntryService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private saveTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/saveTransactionEntry';
    private updateTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/updateTransactionEntry';
    private saveDistributionDetailUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/saveDistributionDetail';
    private getTransactionEntryByTransactionNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getTransactionEntryByTransactionNumber';
    private getDistributionDetailByTransactionNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getDistributionDetailByTransactionNumber';
    private getCustomerAccountDetailByCustomerIdUrl = Constants.financialModuleApiBaseUrl +  '/transaction/accountReceivable/getCustomerAccountDetailByCustomerId'
    private getTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getTransactionType';
    private getTransactionNumberListByTransactionTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getTransactionNumberByTransactionType';
    private getTransactionNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getTransactionNumber';
    private deleteTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/deleteTransactionEntry';
    private deleteTransactionEntryDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/deleteTransactionEntryDistribution';
    private postARTransactionEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/postARTransactionEntry';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //Save or Update Transaction Entry
    SaveUpdateTransactionEntry(transactionEntrySetup:any,isUpdate:boolean) {
        if(isUpdate)
        {
            return this.http.post(this.updateTransactionEntryUrl, JSON.stringify(transactionEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveTransactionEntryUrl,JSON.stringify(transactionEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
    }

    SaveDistributionDetail(ARDistributionDetail:any) {
        
        return this.http.post(this.saveDistributionDetailUrl, JSON.stringify(ARDistributionDetail), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

     //Delete Transaction Entry
     DeleteTransactionEntry(arTransactionNumber:string)
     {
         
         return this.http.post(this.deleteTransactionEntryUrl,{arTransactionNumber:arTransactionNumber}, { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
     }

    

    //get Customer AccountDetail By CustomerId 
    GetCustomerAccountDetailByCustomerId(customerId: string) {
        return this.http.post(this.getCustomerAccountDetailByCustomerIdUrl, {customerId: customerId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //getting detail of Transaction Entry by Transaction Number
    GetTransactionEntryByTransactionNumber(arTransactionNumber: string) {
        return this.http.post(this.getTransactionEntryByTransactionNumberUrl, {arTransactionNumber: arTransactionNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Transaction Type List
    GetTransactionType() {
        return this.http.get(this.getTransactionTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Transaction Number By Transaction Type
    GetTransactionNumber(arTransactionType: string) {
        return this.http.post(this.getTransactionNumberUrl, {arTransactionType: arTransactionType}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Transaction Number List  by Transaction Type
    GetTransactionNumberListByTransactionType(arTransactionType: string) {
        return this.http.post(this.getTransactionNumberListByTransactionTypeUrl, {arTransactionType: arTransactionType}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
 //getting detail of Transaction Entry by Transaction Number
    GetDistributionDetailByTransactionNumber(arTransactionNumber: string) {
        return this.http.post(this.getDistributionDetailByTransactionNumberUrl, {arTransactionNumber: arTransactionNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Transaction Entry by Transaction detail
    GetDistributionDetailByTransaction(objTransactionNumber: any) {
        return this.http.post(this.getDistributionDetailByTransactionNumberUrl, JSON.stringify(objTransactionNumber), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Transaction Entry Distribution
    DeleteTransactionEntryDistribution(arTransactionNumber:string)
    {
        
        return this.http.post(this.deleteTransactionEntryDistributionUrl,{arTransactionNumber:arTransactionNumber}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
    
    //Post AR Transaction Entry
    PostARTransactionEntry(transactionEntrySetup:any) {
        
        return this.http.post(this.postARTransactionEntryUrl,JSON.stringify(transactionEntrySetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}