/**
 * A service class for AR Batch Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { BatchSetup } from '../../../../financialModule/_models/transactionModule/accountReceivables/batchSetup';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class BatchSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    
    private saveBatchesUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/saveBatches';
    private updateBatchesUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/updateBatches';
    private getAllBatchesTransactionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getAllBatchesTransaction';
    private getBatchesByBatchIdUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getBatchesByBatchId';
    private getAllBatchesUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getAllBatches';
    private getBatchByTransactionTypeId = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getBatchByTransactionTypeId';
    private getBatchTotalTrasactionsByTransactionType = Constants.financialModuleApiBaseUrl +  'transaction/accountReceivable/getBatchTotalTrasactionsByTransactionType';
    private postBatchTransaction =  Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/postBatchTransaction';
    private deleteBatchTransaction =  Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/deleteBatchesByBatchId';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //save or Update AR Batches
    SaveUpdateBatches(batchSetup:BatchSetup,isUpdate:boolean) {
        
        if(isUpdate)
        {
            return this.http.post(this.updateBatchesUrl,JSON.stringify(batchSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveBatchesUrl,JSON.stringify(batchSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
    }

    //getting Batches Transaction
    GetAllBatchesTransaction() {
        return this.http.get(this.getAllBatchesTransactionUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    
    
    //getting detail of Batch Setup By Batch Id
    GetBatchesByBatchId(batchId: string,transactionTypeId:string) {
        
        return this.http.post(this.getBatchesByBatchIdUrl, {batchId:batchId,transactionTypeId:transactionTypeId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    } 

    //getting Batches Transaction
    GetAllBatches(page: Page,searchKeyword): Observable<PagedData<BatchSetup>> {
        return this.http
            .post(this.getAllBatchesUrl, {'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }

    //getting detail of Batch TotalTrasactions By TransactionType
    GetBatchTotalTrasactionsByTransactionType(batchId: string, transactionTypeId:string) {
        return this.http.post(this.getBatchTotalTrasactionsByTransactionType, {batchId:batchId,transactionTypeId:transactionTypeId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Batch Trasaction
    PostBatchTransaction(batchId: string, transactionTypeId:string) {
        return this.http.post(this.postBatchTransaction, {batchId:batchId,transactionTypeId:transactionTypeId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Batches
     DeleteBatches(batchId:string, transactionTypeId:string)
     {
         return this.http.post(this.deleteBatchTransaction,{'batchId':batchId,'transactionTypeId':transactionTypeId}, { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
     }


    //for binding Grid on Page
    private getPagedData(page: Page, data: any): PagedData<BatchSetup> {
        let pagedData = new PagedData<BatchSetup>();
        if(data)
        {
            var gridRecords = data.records;
             page.totalElements = data.totalCount;
             page.totalPages = page.totalElements / page.size;
             let start = page.pageNumber * page.size;
             let end = Math.min((start + page.size), page.totalElements);
             for (let i = 0; i < gridRecords.length; i++) {
                 let jsonObj = gridRecords[i];
                 let employee = new BatchSetup(jsonObj.batchId,jsonObj.description,jsonObj.transactionTypeId,jsonObj.transactionType,jsonObj.totalTransactions,jsonObj.quantityTotal);
                 pagedData.data.push(employee);
             }
             pagedData.page = page;     
        }
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

}
   