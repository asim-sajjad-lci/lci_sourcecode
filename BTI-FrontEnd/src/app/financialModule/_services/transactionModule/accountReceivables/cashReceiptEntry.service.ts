/**
 * A service class for AR Cash Receipt Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { ARCashReceiptSetup } from '../../../../financialModule/_models/transactionModule/accountReceivables/cashReceiptEntry';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()
export class ARCashReceiptService {
    private headers = new Headers({ 'content-type': 'application/json' });
   
    private saveCashReceiptEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/saveCashReceiptEntry';
    private updateCashReceiptEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/updateCashReceiptEntry';
    private postCashReceiptEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/postCashReceiptEntry';
    private getCashReceiptEntryByReceiptNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getCashReceiptEntryByReceiptNumber';
    private deleteCashReceiptEntryUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/deleteCashReceiptEntry';
    private getCashReceiptTypeUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getCashReceiptType';
    private getAllCashReceiptNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getAllCashReceiptNumber';
    private getCashReceiptDistributionByReceiptNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/getCashReceiptDistributionByReceiptNumber';   
    private saveCashReceiptDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/saveCashReceiptDistribution';
    private getUniqueReceiptNumberUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/getUniqueReceiptNumber';
    private deleteCashReceiptEntryDistributionUrl = Constants.financialModuleApiBaseUrl + 'transaction/accountReceivable/deleteCashReceiptEntryDistribution';

    currentLanguage = "";
    
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //save Update Cash Receipt
    SaveUpdateCashReceiptEntry(arCashReceiptSetup:ARCashReceiptSetup,isUpdate:boolean) {
        if(isUpdate)
        {
            return this.http.post(this.updateCashReceiptEntryUrl,JSON.stringify(arCashReceiptSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveCashReceiptEntryUrl,JSON.stringify(arCashReceiptSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
    }

    //Post Cash Receipt Entry
    PostCashReceiptEntry(arCashReceiptSetup:ARCashReceiptSetup) {
        
        return this.http.post(this.postCashReceiptEntryUrl,JSON.stringify(arCashReceiptSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting detail of Cash Receipt Entry By Cash Receipt Number
    GetCashReceiptEntryByReceiptNumber(cashReceiptNumber: string) {
        
        return this.http.post(this.getCashReceiptEntryByReceiptNumberUrl, {cashReceiptNumber: cashReceiptNumber}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Cash Reciept Entry
    DeleteCashReceiptEntry(cashReceiptNumber:string)
    {
        
        return this.http.post(this.deleteCashReceiptEntryUrl,{cashReceiptNumber:cashReceiptNumber}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //Get Cash Receipt Type
    GetCashReceiptType() {
            return this.http.get(this.getCashReceiptTypeUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Cash Receipt Number
    GetAllCashReceiptNumber() {
            return this.http.get(this.getAllCashReceiptNumberUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Cash Receipt Distribution By Receipt Number
    GetCashReceiptDistributionByReceiptNumber(cashReceiptNumber:string)
    {
        return this.http.post(this.getCashReceiptDistributionByReceiptNumberUrl,{cashReceiptNumber:cashReceiptNumber}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

     //Get Cash Reciept Distribution
     GetCashRecieptDistribution(arCashReceiptSetup:ARCashReceiptSetup)
     {
         return this.http.post(this.getCashReceiptDistributionByReceiptNumberUrl,JSON.stringify(arCashReceiptSetup), { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
     }

    //saving detail of Cash Receipt Distribution 
    SaveCashReceiptDistribution(arCashReceiptSetup:ARCashReceiptSetup) {
        return this.http.post(this.saveCashReceiptDistributionUrl,arCashReceiptSetup, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Unique receipt Number
    GetUniqueReceiptNumber() {
            return this.http.get(this.getUniqueReceiptNumberUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Delete Cash Reciept Distribution Entry
    DeleteCashReceiptEntryDistribution(cashReceiptNumber:string)
    {
        
        return this.http.post(this.deleteCashReceiptEntryDistributionUrl,{cashReceiptNumber:cashReceiptNumber}, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}