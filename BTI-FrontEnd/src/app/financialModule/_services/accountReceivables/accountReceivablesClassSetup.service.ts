/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import {AccountRecievables} from '../../../financialModule/_models/accounts-receivables-setup/accounts-receivables-class-setup';

@Injectable()
export class AccountReceivableClassSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
	private searchPaymentUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchPaymentTermSetup';
	private searchShippingMethodUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchShipmentMethodSetup';
	private searchVatUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchVatDetailSetup';
	private searchCurrencyUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
	private searchSalesmanUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveSalesmanMaintenance';
	private searchsearchsalesTerritoryUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/search';
	private searchCheckbookUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/saveAccountRecievableClassSetup';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchAccountRecievableClassSetup';
	private getUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getAccountRecievableClassSetupByClassId';
	private updateUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/updateAccountRecievableClassSetup';
    
    currentLanguage = "";
    //initializing parameter for constructor
	constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
	searchPayment() {
        return this.http
            .post(this.searchPaymentUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchShippingMethod() {
        return this.http
            .post(this.searchShippingMethodUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchVat() {
        return this.http
            .post(this.searchVatUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchCurrency() {
        return this.http
            .post(this.searchCurrencyUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchSalesman() {
        return this.http
            .post(this.searchSalesmanUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchsalesTerritory() {
        return this.http
            .post(this.searchsearchsalesTerritoryUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchCheckbook() {
        return this.http
            .post(this.searchCheckbookUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	
   createAccountReceivablesAccountSetup(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
	searchAccountReceivablesClass(page: Page,searchKeyword): Observable<PagedData<AccountRecievables>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	
	getAccountReceivableClass(customerClassId : any) {
        return this.http.post(this.getUrl, { customerClassId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	updateAccountReceivablesAccountSetup(submittedData) {
        return this.http.post(this.updateUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    private getPagedData(page: Page, data: any): PagedData<AccountRecievables> {
        let pagedData = new PagedData<AccountRecievables>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new AccountRecievables(jsonObj.customerClassId,jsonObj.customerClassDescription,jsonObj.customeClassDescriptionArabic);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              