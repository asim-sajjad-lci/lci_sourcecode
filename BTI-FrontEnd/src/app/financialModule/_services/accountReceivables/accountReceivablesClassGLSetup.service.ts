import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';

import { AccountReceivableClassGLSetup } from '../../../financialModule/_models/accounts-receivables-setup/account-receivables-classGL-setup';
 

@Injectable()
export class AccountReceivableClassGLSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'getArAccountTypes';
    private saveAccountReceivableClassGLSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/accountRecievableClassGlSetup';
    private getAccountReceivableClassGLSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getAccountRecievableClassGlSetup';
    private getAccountRecievableClassSetupByClassIdByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getAccountRecievableClassSetupByClassId';
    private getAccountRecievableClassGlSetupByCustomerClassIdURL = Constants.financialModuleApiBaseUrl +'/setup/accounts/getAccountRecievableClassGlSetupByCustomerClassId'
    private getAccountRecievableClassListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchAccountRecievableClassSetup';
    private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
    private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
    currentLanguage = "";
    private saveAccountReceivableClassGLSetupUrlIms = Constants.imsModuleApiBaseUrl + 'transactionEntry/saveAccountPayableClassSetup';
    private getAccountRecievableClassListUrlIms = Constants.imsModuleApiBaseUrl + 'transactionEntry/searchAccountRecievableClassSetup';
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

   getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    //getCustomerIdList
    getAccountRecievableClassList()
    {
         return this.http.post(this.getAccountRecievableClassListUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAccountRecievableClassListIms(id : any)
    {
         return this.http.post(this.getAccountRecievableClassListUrlIms,{'searchKeyword': id}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


     getAccountReceivableClassGLSetup() {
       return this.http.get(this.getAccountReceivableClassGLSetupUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
    getAccountRecievableClassSetupByClassIdById(customerClassId : any) {
        return this.http.post(this.getAccountRecievableClassSetupByClassIdByIdUrl,{customerClassId},{headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getAccountRecievableClassGlSetupByCustomerClassId(customerClassId : any) {
        return this.http.post(this.getAccountRecievableClassGlSetupByCustomerClassIdURL,{customerClassId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Save CustomerMaintenanceOption
    saveCustomerAccountMaintenanace(AccountReceivableClassGLSetup:AccountReceivableClassGLSetup) {
        return this.http.post(this.saveAccountReceivableClassGLSetupUrl,JSON.stringify(AccountReceivableClassGLSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    saveCustomerAccountMaintenanaceIms(AccountReceivableClassGLSetup:Object) {
        return this.http.post(this.saveAccountReceivableClassGLSetupUrlIms,JSON.stringify(AccountReceivableClassGLSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }

    restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}