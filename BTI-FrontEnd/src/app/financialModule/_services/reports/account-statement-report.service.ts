
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class AccountStatementReportService {
    private headers = new Headers({ 'content-type': 'application/json' });

    private getAccountStatementReportUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/reports/accountStatementReportNew';
    private getAccountNumbersForStatementReportUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/reports/getAccountNumberForStatementReport';
     
    currentLanguage:string;
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
    
    //Getting Account Statement Report
    GetAccountStatementReport(startDate: string,endDate: string) {
        return this.http.post(this.getAccountStatementReportUrl,{startDate: startDate,endDate:endDate }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Getting Account Numbers for Statement Report
    GetAccountNumbersForStatementReport(startDate: string,endDate: string) {
        return this.http.post(this.getAccountNumbersForStatementReportUrl,{startDate: startDate,endDate:endDate }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

}