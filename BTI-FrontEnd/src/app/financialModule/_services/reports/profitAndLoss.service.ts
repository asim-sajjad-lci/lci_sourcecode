import { Injectable } from "@angular/core";
import { Constants } from "../../../_sharedresource/Constants";
import { Headers,Http } from "@angular/http";

@Injectable()
export class ProfitAndLossService{
    
    
    private headers = new Headers({ 'content-type': 'application/json' });

    private getProfitAndLossReportUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/reports/profitAndLossReport';


    currentLanguage:string;

    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //Getting Profit and Loss Report
    GetProfitAndLossReport(Date: number,period:number) {
        return this.http.post(this.getProfitAndLossReportUrl,{year: Date , period:period}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

        //error handler
        private handleError(error: any): Promise<any> {
            return Promise.reject(error.message || error);
        }
    
}