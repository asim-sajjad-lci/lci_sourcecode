
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Constants } from '../../../_sharedresource/Constants';


@Injectable()
export class TrialBalanceReportService {
    private headers = new Headers({ 'content-type': 'application/json' });

    private getTrailBalanceReportUrl = Constants.financialModuleApiBaseUrl + 'transaction/generalLedger/reports/trailBalanceReport';
     
    currentLanguage:string;
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
    
    //Getting Trial Balance Report
    GetTrailBalanceReport(startDate: string,endDate: string) {
        return this.http.post(this.getTrailBalanceReportUrl,{startDate: startDate,endDate:endDate }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

}