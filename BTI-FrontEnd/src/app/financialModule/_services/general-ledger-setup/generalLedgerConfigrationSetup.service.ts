/**
 * A service class for General Ledger Configration Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { GeneralLedgerConfigurationSetup } from '../../../financialModule/_models/general-ledger-setup/generalLedgerConfigrationSetup';


@Injectable()
export class GeneralLedgerConfigrationSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/accountTypeList';
    private getAccountTypeByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountTypeById';
    private updateAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateAccountType';
    private saveAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveAccountType';
    private searchAccountCategoryUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/accountCategoryList';
    private getAccountCategoryByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountCategoryById';
    private updateAccountCategoryUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateAccountCategory';
    private saveAccountCategoryUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveAccountCategory';
    private searchAuditTrialCodeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchAuditTrialCode';
    private getAuditTrialCodeByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAuditTrialCodeById';
    private updateAuditTrialCodeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateAccountCategory';
    private saveAuditTrialCodeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveAuditTrialCode';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

    //Search AccountType
    searchAccountType(page:Page, searchKeyword):Observable<PagedData<GeneralLedgerConfigurationSetup>>{
        return this.http.post(this.searchAccountTypeUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


     //getting details of  AccountType By ID
    getAccountTypeById(accountTypeId: string) {
        return this.http.post(this.getAccountTypeByIdUrl, { accountTypeId: accountTypeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //updateAccountType for edit 
    updateAccountType(GeneralLedgerConfigurationSetup:GeneralLedgerConfigurationSetup){
        return this.http.post(this.updateAccountTypeUrl,JSON.stringify(GeneralLedgerConfigurationSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create AccountType
    createAccountType(GeneralLedgerConfigurationSetup:GeneralLedgerConfigurationSetup)
    {
      return this.http.post(this.saveAccountTypeUrl,JSON.stringify(GeneralLedgerConfigurationSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    //Search AccountCategory
    searchAccountCategory(page:Page, searchKeyword):Observable<PagedData<GeneralLedgerConfigurationSetup>>{
        return this.http.post(this.searchAccountCategoryUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


     //getting details of  AccountCategory By ID
    getAccountCategoryById(accountCategoryId: string) {
        return this.http.post(this.getAccountCategoryByIdUrl, { accountCategoryId: accountCategoryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update AccountCategorye for edit 
    updateAccountCategory(GeneralLedgerConfigurationSetup:GeneralLedgerConfigurationSetup){
        return this.http.post(this.updateAccountCategoryUrl,JSON.stringify(GeneralLedgerConfigurationSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create AccountCategory
    createAccountCategory(GeneralLedgerConfigurationSetup:GeneralLedgerConfigurationSetup)
    {
      return this.http.post(this.saveAccountCategoryUrl,JSON.stringify(GeneralLedgerConfigurationSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    //Search Audit Trial Code
    searchAuditTrialCode(page:Page, searchKeyword):Observable<PagedData<GeneralLedgerConfigurationSetup>>{
        return this.http.post(this.searchAuditTrialCodeUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


     //getting details of  Audit Trial Code By ID
    getAuditTrialCodeById(seriesIndex: string) {
        return this.http.post(this.getAuditTrialCodeByIdUrl, { seriesIndex: seriesIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update Audit Trial Code for edit 
    updateAuditTrialCode(GeneralLedgerConfigurationSetup:GeneralLedgerConfigurationSetup){
        return this.http.post(this.updateAuditTrialCodeUrl,JSON.stringify(GeneralLedgerConfigurationSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create Audit Trial Code
    createAuditTrialCode(GeneralLedgerConfigurationSetup:GeneralLedgerConfigurationSetup)
    {
      return this.http.post(this.saveAuditTrialCodeUrl,JSON.stringify(GeneralLedgerConfigurationSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<GeneralLedgerConfigurationSetup> {
        let pagedData = new PagedData<GeneralLedgerConfigurationSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new GeneralLedgerConfigurationSetup(jsonObj.accountTypeId,jsonObj.accountTypeName,jsonObj.accountTypeNameArabic,jsonObj.accountCategoryDescription,jsonObj.accountCategoryDescriptionArabic,jsonObj.sourceCode,jsonObj.seriesId,jsonObj.seriesIndex,jsonObj.sourceDocument);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}