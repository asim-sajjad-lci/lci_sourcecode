/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { PostingAccountSetup } from '../../../financialModule/_models/general-ledger-setup/posting-account-setup';


@Injectable()
export class PostingAccountService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchPostingAccountSetupUrl = Constants.financialModuleApiBaseUrl + 'general/ledger/posting/account/search';
    private getPostingAccountSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'general/ledger/posting/account/getById';
    private updatePostingAccountSetupUrl = Constants.financialModuleApiBaseUrl + 'general/ledger/posting/account/update';
    private savePostingAccountSetupUrl = Constants.financialModuleApiBaseUrl + 'general/ledger/posting/account';
    private getGlAccountNumberListUrl=Constants.financialModuleApiBaseUrl + 'getGlAccountNumberList';
    private getpostingAccountSeriesListUrl=Constants.financialModuleApiBaseUrl + '/setup/company/getOne/configure/module';
    private getSeriesListUrl=Constants.financialModuleApiBaseUrl + '/setup/company/get/configure/module';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

    //Search payment Terms
    searchPostingAccountSetup(page:Page, searchKeyword):Observable<PagedData<PostingAccountSetup>>{
        return this.http.post(this.searchPostingAccountSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

    getSeriesList() {
        return this.http.post(this.getSeriesListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


     //getting details of  payment Terms By ID
    getPostingAccountSetupById(postingId: string) {
        
        return this.http.post(this.getPostingAccountSetupByIdUrl, { postingId:postingId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getGlAccountNumberList
      getGlAccountNumberList()
    {
         return this.http.get(this.getGlAccountNumberListUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getpostingAccountSeriesList(postingId: string)
    {
         return this.http.post(this.getpostingAccountSeriesListUrl,{postingId:postingId},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //update rolegroup for edit 
    updatePostingAccountSetup(PostingAccountSetup:PostingAccountSetup){
        return this.http.post(this.updatePostingAccountSetupUrl,JSON.stringify(PostingAccountSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create comapny setup
    createPostingAccountSetup(PostingAccountSetup:PostingAccountSetup)
    {
      return this.http.post(this.savePostingAccountSetupUrl,JSON.stringify(PostingAccountSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<PostingAccountSetup> {
        let pagedData = new PagedData<PostingAccountSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new PostingAccountSetup(jsonObj.postingId,jsonObj.seriesNumber,jsonObj.descriptionPrimary,jsonObj.descriptionSecondary,jsonObj.accountNumber,jsonObj.accountTableRowIndex);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}