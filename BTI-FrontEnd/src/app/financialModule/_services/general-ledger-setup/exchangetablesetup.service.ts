/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { ExchangeTableSetup } from '../../../financialModule/_models/general-ledger-setup/exchangetablesetup';


@Injectable()
export class ExchangeTableSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
    private updateExchangeTableSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateCurrencyExchangeHeaderSetup';
    private exchangeTableSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveCurrencyExchangeHeaderSetup';
    private  searchExchangeTableSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencyExchangeHeaderSetup';
      private  searchCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencySetups';
    private  getExchangeTableSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencyExchangeHeaderSetup';
    private  getRateFrequencyTypesUrl = Constants.financialModuleApiBaseUrl + 'getRateFrequencyTypes';
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
    //update rolegroup for edit 
    updateExchangeTableSetup(ExchangeTableSetup:ExchangeTableSetup){
        return this.http.post(this.updateExchangeTableSetupUrl,JSON.stringify(ExchangeTableSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create exchange table setup
    createExchangeTableSetup(ExchangeTableSetup:ExchangeTableSetup)
    {
      return this.http.post(this.exchangeTableSetupUrl,JSON.stringify(ExchangeTableSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)

    }
    //getListForCurrency
    
     getListForCurrency() {
        return this.http.post(this.searchCurrencySetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   //Search exchange table 
    searchExchangeTableSetup(page:Page, searchKeyword):Observable<PagedData<ExchangeTableSetup>>{
        return this.http.post(this.searchExchangeTableSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }
    //getRateFrequencyTypes
     getRateFrequencyTypes()
    {
         return this.http.get(this.getRateFrequencyTypesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //pagination of data
    private getPagedData(page: Page, data: any): PagedData<ExchangeTableSetup> {
        let pagedData = new PagedData<ExchangeTableSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new ExchangeTableSetup(jsonObj.exchangeId,jsonObj.description,jsonObj.descriptionArabic,jsonObj.exchangeRateSource, 
                jsonObj.exchangeRateSourceArabic,jsonObj.rateFrequency, jsonObj.rateVariance, jsonObj.currencyId, jsonObj.rateCalcMethod);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
      //getting details of  exchange table By ID
    getExchangeTableSetupById(exchangeId: string) {
        return this.http.post(this.getExchangeTableSetupByIdUrl, { exchangeId: exchangeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              