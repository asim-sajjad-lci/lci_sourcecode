/**
 * A service class for currency set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { CurrencySetup } from '../../../financialModule/_models/general-ledger-setup/currencysetup';


@Injectable()
export class CurrencySetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    //private getCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencySetupDetails';
    private updateCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateCurrencySetup';
    private currencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/currencySetup';
    private  searchCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencySetups';
    private  getCurrencySetUpDetailsByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencySetupDetails';
    private  getNegativeSymbolTypesUrl = Constants.financialModuleApiBaseUrl + 'getNegativeSymbolTypes';
     private getSeperatorDecimalTypesListUrl = Constants.financialModuleApiBaseUrl + 'getSeperatorDecimalTypesList';
    
    private getSeperatorThousandsTypesListUrl = Constants.financialModuleApiBaseUrl + 'getSeperatorThousandsTypesList';
    private getNegativeSymbolSignTypesUrl = Constants.financialModuleApiBaseUrl + 'getNegativeSymbolSignTypes';
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

     //getting details of currency
    // getCurrencySetup(currencyId: string) {
    //     return this.http.post(this.getCurrencySetupUrl, { currencyId: currencyId }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    //update rolegroup for edit 
    updateCurrencySetup(CurrencySetup:CurrencySetup){
        return this.http.post(this.updateCurrencySetupUrl,JSON.stringify(CurrencySetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create currency setup
    createCurrencySetup(CurrencySetup:CurrencySetup)
    {
      return this.http.post(this.currencySetupUrl,JSON.stringify(CurrencySetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
   //Search currency Terms
    searchCurrencySetup(page:Page, searchKeyword):Observable<PagedData<CurrencySetup>>{
        return this.http.post(this.searchCurrencySetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

    //getNegativeSymbolTypes

     
    getNegativeSymbolTypes()
    {
         return this.http.get(this.getNegativeSymbolTypesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getSeperatorDecimalTypesList
 getSeperatorDecimalTypesList()
    {
         return this.http.get(this.getSeperatorDecimalTypesListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //getSeperatorThousandsTypesList

     getSeperatorThousandsTypesList()
    {
         return this.http.get(this.getSeperatorThousandsTypesListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //getNegativeSymbolSignTypes
     getNegativeSymbolSignTypes()
    {
         return this.http.get(this.getNegativeSymbolSignTypesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

      //pagination of data
    private getPagedData(page: Page, data: any): PagedData<CurrencySetup> {
        let pagedData = new PagedData<CurrencySetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new CurrencySetup(jsonObj.currencyId,jsonObj.currencyDescription,jsonObj.currencyDescriptionArabic,jsonObj.currencySymbol, jsonObj.currencyUnit, 
                jsonObj.unitSubunitConnector,jsonObj.currencySubunit, jsonObj.currencyUnitArabic, jsonObj.unitSubunitConnectorArabic, jsonObj.currencySubunitArabic,jsonObj.separatorsDecimal,jsonObj.includeSpaceAfterCurrencySymbol,
				jsonObj.negativeSymbol,jsonObj.displayNegativeSymbolSign,jsonObj.separatorsThousands,jsonObj.displayCurrencySymbol);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
      //getting details of  currency setup By ID
    getCurrencySetUpDetailsById(currencyId: string) {
        return this.http.post(this.getCurrencySetUpDetailsByIdUrl, {currencyId: currencyId}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              