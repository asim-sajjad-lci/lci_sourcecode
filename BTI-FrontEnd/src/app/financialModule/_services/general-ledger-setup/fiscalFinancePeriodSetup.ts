/**
 * A service class for FiscalFinancePeriodSetup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { FiscalFinancePeriod } from '../../../financialModule/_models/general-ledger-setup/fiscalFinancePeriodSetup';

@Injectable()
export class FiscalFinancePeriodSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private saveFiscalFinancePeriodSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/fiscalFinancePeriod/setup';
    private getFiscalFinancePeriodSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/fiscalFinancePeriod/get';
    private getFiscalFinancePeriodByYearSetupUrl = Constants.financialModuleApiBaseUrl + '/setup/generalLedger/fiscalFinancePeriod/getByYear';
    currentLanguage = "";

//initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

     //getting details of  FiscalFinancePeriodSetup
    getFiscalFinancePeriodSetup() {
        return this.http.get(this.getFiscalFinancePeriodSetupUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   
      //create FiscalFinancePeriodSetup
    saveFiscalFinancePeriodSetup(FiscalFinancePeriod:FiscalFinancePeriod)
    {
        
      return this.http.post(this.saveFiscalFinancePeriodSetupUrl,JSON.stringify(FiscalFinancePeriod),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    
    //get Fiscal Finance Period By Year
    getFiscalFinancePeriodByYear(year: string) {
    return this.http.post(this.getFiscalFinancePeriodByYearSetupUrl, { 'year': year }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
 
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}

