/**
 * A service class for Mass Close Fiscal Finance Period Setup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { MassCloseFiscalFinancePeriodSetup } from '../../../financialModule/_models/general-ledger-setup/mass-close-fiscal-finance-period-setup';


@Injectable()
export class MassCloseFiscalFinancePeriodSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private saveUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/massCloseFiscalPeriodSetup';
    private getMassCloseFiscalFinancePeriodSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getMassCloseFiscalPeriodSetup';
    private getMaxPeriodUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/massCloseFiscalPeriodSetup/getMaxPeriod';
    private getSeriesUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/massCloseFiscalPeriodSetup/getSeries';
    private getYearsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/massCloseFiscalPeriodSetup/getYears';
    private getOriginUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/massCloseFiscalPeriodSetup/getOrigin';
    private getAllUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getMassCloseFiscalPeriodSetup';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	
	getMaxPeriod(year : any) {
	 return this.http
		.post(this.getMaxPeriodUrl, { 'year': year }, { headers: this.headers })
		.map(data => data.json());
    }
	getAll(Request : any) {
	 return this.http
        .post(this.getAllUrl, Request, { headers: this.headers })
		.map(data => data.json());
    }
	// getFrom(year : any,seriesId:any,allRecord:boolean,startPeriodId:any,endPeriodId:any) {
	//  return this.http
	// 	.post(this.getAllUrl, { 'year': year,'seriesId': seriesId, 'allRecord': allRecord, 'startPeriodId': startPeriodId , 'endPeriodId': endPeriodId }, { headers: this.headers })
	// 	.map(data => data.json());
    // }
    getFrom(Request : any) {
        return this.http
           .post(this.getAllUrl,Request, { headers: this.headers })
           .map(data => data.json());
       }
    
    getSeries()
   {
        return this.http.get(this.getSeriesUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getYears()
    {
         return this.http.get(this.getYearsUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getOrigin(seriesTypeId:string)
    {
         return this.http.post(this.getOriginUrl,{'serieTypeId':seriesTypeId},{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    
    getMassCloseFiscalFinancePeriodSetup(MassCloseFiscalFinancePeriodSetup:MassCloseFiscalFinancePeriodSetup){
        return this.http.post(this.getMassCloseFiscalFinancePeriodSetupUrl,JSON.stringify(MassCloseFiscalFinancePeriodSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

      //create MassCloseFiscalFinancePeriodSetup
   save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<MassCloseFiscalFinancePeriodSetup> {
        let pagedData = new PagedData<MassCloseFiscalFinancePeriodSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new MassCloseFiscalFinancePeriodSetup(jsonObj.year,jsonObj.seriesId,jsonObj.records,jsonObj.originId);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}