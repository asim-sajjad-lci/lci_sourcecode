/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { ExchangeTableSetup } from '../../../financialModule/_models/general-ledger-setup/exchangetablesetup';


@Injectable()
export class GeneralLedgerSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
    private searchUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/generalLedgerSetup/get';
    private saveUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/generalLedgerSetup';
    /*private exchangeTableSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveCurrencyExchangeHeaderSetup';
    private  searchExchangeTableSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencyExchangeHeaderSetup';
      private  searchCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencySetups';
    private  getExchangeTableSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencyExchangeHeaderSetup';
    private  getRateFrequencyTypesUrl = Constants.financialModuleApiBaseUrl + 'getRateFrequencyTypes';*/
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	/*getData() {
        return this.http
            .get(this.searchUrl, { headers: this.headers })
            .map(data => data.json().result);
    }*/
	/*getData(){
        return this.http.get(this.searchUrl, { headers: this.headers })
        .map(data => data.json());
    }*/
	getData() {
        return this.http.get(this.searchUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	createGeneralLedgerSetup(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update rolegroup for edit 
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              