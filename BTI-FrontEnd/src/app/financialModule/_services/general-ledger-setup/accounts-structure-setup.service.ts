import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data'
import { Constants } from '../../../_sharedresource/Constants';

import { AccountStructureSetup } from '../../_models/general-ledger-setup/accounts-structure-setup';


@Injectable()
export class AccountStructureService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAccountStructureSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountStructureSetup';
    private getAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'getAccountTypes';
    private getMainAccountListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getMainAccountList';
    private getFinancialDimensionsListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getFinancialDimensionsList';
    private getFinancialDimensionsValuesByFinancialDimensionIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getFinancialDimensionsValuesByFinancialDimensionId';
    private saveAccountStructureSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveAccountStructureSetup';
    private searchAccountStructureSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getAccountGroupSetupBySearch';
    private checkGlAccountNumberUrl = Constants.financialModuleApiBaseUrl + 'checkGlAccountNumber';
    private createNewGlAccountNumberUrl = Constants.financialModuleApiBaseUrl + 'account/payables/createNewGlAccountNumber';
    
    private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
    private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    searchPhysicalLocation(page: Page,searchKeyword): Observable<PagedData<AccountStructureSetup>> {
        return this.http
            .post(this.searchAccountStructureSetupUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }

    getMainAccountList() {
       return this.http.get(this.getMainAccountListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    getAccountStructureSetup() {
       return this.http.get(this.getAccountStructureSetupUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    getFinancialDimensionsList() {
       return this.http.get(this.getFinancialDimensionsListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    getFinancialDimensionsValuesByFinancialDimensionId(dimInxd: string) {
        return this.http.post(this.getFinancialDimensionsValuesByFinancialDimensionIdUrl, { dimInxd: dimInxd }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
  
  
    //Save AccountStructure
    saveAccountStructureSetup(AccountStructureSetup:AccountStructureSetup) {
        return this.http.post(this.saveAccountStructureSetupUrl,JSON.stringify(AccountStructureSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //getting details of  GlAccountNumber 
    checkGlAccountNumber(accountNumber: string) {
        return this.http.post(this.checkGlAccountNumberUrl, { accountNumber: accountNumber }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    } 

    //Save AccountStructure
    createNewGlAccountNumber(accountNumberList:any) {
       
        return this.checkGlAccountNumber(accountNumberList[0].accountNumberIndex.join('')).then(data => {
         if(data.btiMessage.messageShort == 'RECORD_ALREADY_EXIST')
         {
           
            return data ;
         }
         else{
           
            return this.http.post(this.createNewGlAccountNumberUrl,{accountNumberList:accountNumberList}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
         }

        }).catch(this.handleError);
        
    }

    

    firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }

    restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
    
    private getPagedData(page: Page, data: any): PagedData<AccountStructureSetup> {
        let pagedData = new PagedData<AccountStructureSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new AccountStructureSetup(jsonObj.segmentNumber,jsonObj.isMainAccount,jsonObj.coaMainAccountsFromActIndexId,jsonObj.coaMainAccountsToActIndexId,jsonObj.dimensionList);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}