/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { ExchangeTableRateDetail } from '../../../financialModule/_models/general-ledger-setup/exchangeTableRateDetail';


@Injectable()
export class ExchangeTableRateDetailService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
    private createExchangeTableRateUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveCurrencyExchangeDetail';
    private searchExchangeTableRateUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencyExchangeDetail';
    private getExchangeTableRateUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencyExchangeDetail';
    private editExchangeTableRateUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateCurrencyExchangeDetail';
	private searchCurrencyUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencySetups';
    private exchangeIdListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencyExchangeHeaderSetup';
    private exchangeIdListDescriptionUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencyExchangeHeaderSetup';

    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   searchCurrency() {
        return this.http
            .post(this.searchCurrencyUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }

    exchangeIdList() {
        return this.http
            .post(this.exchangeIdListUrl, { 'searchKeyword': ''}, { headers: this.headers })
            .map(data => data.json().result);
    }

    exchangeIdListDescription(exchangeId : any) {
        return this.http.post(this.exchangeIdListDescriptionUrl, { exchangeId:exchangeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

	
   createExchangeTableRate(submittedData) {
        return this.http.post(this.createExchangeTableRateUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
	searchTableRateDetail(page: Page,searchKeyword): Observable<PagedData<ExchangeTableRateDetail>> {
        return this.http
            .post(this.searchExchangeTableRateUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getExchangeTableRate(exchangeId : any) {
        return this.http.post(this.getExchangeTableRateUrl, { exchangeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	editExchangeTableRate(submittedData) {
        return this.http.post(this.editExchangeTableRateUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    private getPagedData(page: Page, data: any): PagedData<ExchangeTableRateDetail> {
        let pagedData = new PagedData<ExchangeTableRateDetail>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new ExchangeTableRateDetail(jsonObj.currencyId,jsonObj.description,jsonObj.descriptionArabic,jsonObj.exchangeDate,jsonObj.exchangeExpirationDate,jsonObj.exchangeId,jsonObj.exchangeRate,jsonObj.exchangeTime);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              