/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { CurrencySetup } from '../../../financialModule/_models/currency/currencysetup';


@Injectable()
export class CurrencySetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllCompanyListUrl = Constants.userModuleApiBaseUrl + 'company/companyListCountOfUsers';
    private getCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencySetupDetails';
    private updateCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateCurrencySetup';
    private currencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/currencySetup';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }


     //getting details of authsetting
    getCurrencySetup(currencyId: string) {
        return this.http.post(this.getCurrencySetupUrl, { currencyId: currencyId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update rolegroup for edit 
    updateCurrencySetup(CurrencySetup:CurrencySetup){
        return this.http.post(this.updateCurrencySetupUrl,JSON.stringify(CurrencySetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create comapny setup
    createCurrencySetup(CurrencySetup:CurrencySetup)
    {
      return this.http.post(this.currencySetupUrl,JSON.stringify(CurrencySetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}