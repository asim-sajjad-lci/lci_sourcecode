import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';

import { AccountReceivableSetup } from '../../../financialModule/_models/accounts-receivables-setup/accounts-receivables-setup';


@Injectable()
export class AccountReceivableService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private saveAccountRecievableSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/saveAccountRecievableSetup';
    private getAccountRecievableSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getAccountRecievableSetup';
    private defaultListAccountReceivableUrl = Constants.financialModuleApiBaseUrl + 'getRmApplyByDefaultList';
    private masterAgingListAccountReceivableUrl = Constants.financialModuleApiBaseUrl + 'getRmAgeingList';
    private checkbookUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    getListForCheckbook() {
        return this.http.post(this.checkbookUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

  
   //getting detail of AccountReceivable
    getAccountRecievableSetup() {
        return this.http.get(this.getAccountRecievableSetupUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    //master AgingList AccountReceivable
    getmasterAgingListAccountReceivable() {
        return this.http.get(this.masterAgingListAccountReceivableUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    //Save AccountReceivable
    saveAccountRecievableSetup(AccountReceivableSetup:AccountReceivableSetup) {
        return this.http.post(this.saveAccountRecievableSetupUrl,JSON.stringify(AccountReceivableSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    

    getdefaultListAccountReceivable()
    {
        return this.http.get(this.defaultListAccountReceivableUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    
    
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}