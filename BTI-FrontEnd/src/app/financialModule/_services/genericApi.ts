/**
 * A service class for salesterritorysetup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Page } from '../../_sharedresource/page';
import { PagedData } from '../../_sharedresource/paged-data';
import { Constants } from '../../_sharedresource/Constants';



@Injectable()
export class genericService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
   private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
   private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';

   
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
         this.headers.append("tenantid", localStorage.getItem('tenantid'))
    }

  
    firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }

    restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }


    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              