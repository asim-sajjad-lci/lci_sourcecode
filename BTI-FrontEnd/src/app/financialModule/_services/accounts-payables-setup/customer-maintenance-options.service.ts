import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';

@Injectable()
export class CustomerOptionsMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private customerUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveCustomerMaintenance';
    private getCustomerByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/maintenanceGetById';
	private searchPaymentUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchPaymentTermSetup';
	private searchShippingMethodUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchShipmentMethodSetup';
	private searchVatUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchVatDetailSetup';
	private searchCurrencyUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
	private searchSalesmanUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveSalesmanMaintenance';
	private searchsearchsalesTerritoryUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/search';
	private searchCheckbookUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';
	private priceLevelListUrl = Constants.financialModuleApiBaseUrl + 'getPriceLevelList';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/saveCustomerMaintenanceOptions';
	private getListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getCustomerMaintenanceOption';
    private getCustomerDataUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getCustomerMaintenanceOptionByCustomerId';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    searchCustomers() {
       return this.http
            .post(this.customerUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    } 
	getCustomerByID(customerId : any) {
        return this.http.post(this.getCustomerByIdUrl, { customerId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getCustomerData(customerNumberId : any) {
        
        return this.http.post(this.getCustomerDataUrl, { customerNumberId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	searchPayment() {
        return this.http
            .post(this.searchPaymentUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchShippingMethod() {
        return this.http
            .post(this.searchShippingMethodUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchVat() {
        return this.http
            .post(this.searchVatUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchCurrency() {
        return this.http
            .post(this.searchCurrencyUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchSalesman() {
        return this.http
            .post(this.searchSalesmanUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchsalesTerritory() {
        return this.http
            .post(this.searchsearchsalesTerritoryUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchCheckbook() {
        return this.http
            .post(this.searchCheckbookUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
  
    create(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
//    getList() {
//        
//         return this.http.get(this.getListUrl, { headers: this.headers })
//        .toPromise()
//        .then(res => res.json())
//        .catch(this.handleError);

//     }

       getPriceLevelList() {
        return this.http.get(this.priceLevelListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}