import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { AccountPayablesSetup } from '../../../financialModule/_models/accounts-payables/accounts-payables-setup';


@Injectable()
export class AccountPayablesService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private addAccountPayablesUrl = Constants.financialModuleApiBaseUrl + 'account/payables/accountPayableSetup';
    private getByIdAccountPayablesUrl = Constants.financialModuleApiBaseUrl + 'account/payables/accountPayableSetupGetById';
    private updateAccountPayablesUrl = Constants.financialModuleApiBaseUrl + 'account/payables/updateAccountPayableSetup';
    private searchAccountPayablesUrl = Constants.financialModuleApiBaseUrl + 'account/payables/searchAccountPayableSetup';
    private getCheckFormatTypeUrl = Constants.financialModuleApiBaseUrl + 'account/payables/getCheckFormatTypeStatus';
    private masterAgingListAccountReceivableUrl = Constants.financialModuleApiBaseUrl + 'getRmAgeingList';
    private defaultListAccountReceivableUrl = Constants.financialModuleApiBaseUrl + 'getRmApplyByDefaultList';
    private checkbookUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';
    private getListUrl = Constants.financialModuleApiBaseUrl + 'account/payables/getAccountPayableSetup';
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  // Fetch all records
  getList() {
        return this.http.get(this.getListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
  
   //getting detail of AccountPayables
    getByIdAccountPayablesSetup(id: string) {
        return this.http.post(this.getByIdAccountPayablesUrl, {id: id}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   //update AccountPayables for edit
    updateAccountPayablesSetup(AccountPayablesSetup:AccountPayablesSetup) {
        return this.http.post(this.updateAccountPayablesUrl, JSON.stringify(AccountPayablesSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //add new AccountPayables
    addAccountPayablesSetup(AccountPayablesSetup:AccountPayablesSetup) {
        return this.http.post(this.addAccountPayablesUrl,JSON.stringify(AccountPayablesSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
  
    getAccountPayableSetup()
    {
         return this.http.post(this.searchAccountPayablesUrl, {'searchKeyword':""},{ headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }


    getCheckFormatType()
    {
         return this.http.get(this.getCheckFormatTypeUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    getListForCheckbook() {
        return this.http.post(this.checkbookUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     getmasterAgingListAccountReceivable() {
        return this.http.get(this.masterAgingListAccountReceivableUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    getdefaultListAccountReceivable()
    {
        return this.http.get(this.defaultListAccountReceivableUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}