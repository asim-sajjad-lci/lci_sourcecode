/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';

@Injectable()
export class AccountPayableClassAccountSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
	private getClassUrl = Constants.financialModuleApiBaseUrl + 'account/payables/searchAccountPayableClassSetup';
	private getClassDescUrl = Constants.financialModuleApiBaseUrl + 'account/payables/accountPayableClassSetupGetById';
    private getAccountPayableClassSetupUrl = Constants.financialModuleApiBaseUrl + 'account/payables/getAccountPayableClassSetup';
	private getAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'getAccountTypes';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'account/payables/saveAccountPayableClassSetup';	
	private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
	private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
	private getEditRecordUrl = Constants.financialModuleApiBaseUrl + 'account/payables/getAccountPayableClassSetup';
	/*private getAccountListUrl = Constants.financialModuleApiBaseUrl + 'getGlAccountNumberList';
	
	private getDataUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getPurchasePostingAccountSetup';*/
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
	getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	getClass() {
        return this.http
            .post(this.getClassUrl, { 'searchKeyword': '','pageNumber': '0', 'pageSize': '1000' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	getClassDescription(vendorClassId : any) {
        return this.http.post(this.getClassDescUrl, { vendorClassId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getEditRecord() {
        return this.http.get(this.getEditRecordUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

     getAccountPayableClassSetup(classId : any) {
        return this.http.post(this.getAccountPayableClassSetupUrl,{classId},{headers: this.headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	/*getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	*/
	
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              