/**
 * A service class for accountPayablesclassSetupService
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { AccountPayablesClassSetup } from '../../../../financialModule/_models/accounts-payables/account-payables-class-setup/account-payables-class-setup';


@Injectable()
export class AccountPayablesClassSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAccountPayableClassSetupGetByIdUrl = Constants.financialModuleApiBaseUrl + 'account/payables/accountPayableClassSetupGetById';
    private updateAccountPayableClassSetupUrl = Constants.financialModuleApiBaseUrl + 'account/payables/updateAccountPayableClassSetup';
    private accountPayableClassSetupUrl = Constants.financialModuleApiBaseUrl + 'account/payables/accountPayableClassSetup';
    private searchAccountPayableClassSetupUrl = Constants.financialModuleApiBaseUrl + 'account/payables/searchAccountPayableClassSetup';
    private  searchCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
    private  searchCheckbookUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';
    private  searchShipmentMethodSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchShipmentMethodSetup';
    private  searchPaymentTermSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchPaymentTermSetup';
   private  searchVatDetailSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchVatDetailSetup';
   
    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
    //getListForCurrency
    
     getListForCurrency() {
        return this.http.post(this.searchCurrencySetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
      //getListForCheckbook
    
     getListForCheckbook() {
        return this.http.post(this.searchCheckbookUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //getListForShippingMethod
     getListForShippingMethod() {
        return this.http.post(this.searchShipmentMethodSetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //getPaymentTermList
     getPaymentTermList() {
        return this.http.post(this.searchPaymentTermSetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
//getListForVatId
 getListForVatId() {
        return this.http.post(this.searchVatDetailSetupUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

 //get all details of account payables class set up
    searchAccountPayableClassSetup(page:Page, searchKeyword):Observable<PagedData<AccountPayablesClassSetup>>{
        return this.http.post(this.searchAccountPayableClassSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of account payble class by vendorClassId
    getAcountPayableClassSetupGetById(vendorClassId: string) {
        return this.http.post(this.getAccountPayableClassSetupGetByIdUrl, { vendorClassId: vendorClassId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update AccountPayableClassSetup for edit 
    updateAccountPayableClassSetup(AccountPayablesClassSetup:AccountPayablesClassSetup){
        
        return this.http.post(this.updateAccountPayableClassSetupUrl,JSON.stringify(AccountPayablesClassSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create AccountPayables Class Setup
    createAccountPayablesClassSetup(AccountPayablesClassSetup:AccountPayablesClassSetup)
    {
      return this.http.post(this.accountPayableClassSetupUrl,JSON.stringify(AccountPayablesClassSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
    //pagination of data
    private getPagedData(page: Page, data: any): PagedData<AccountPayablesClassSetup> {
        let pagedData = new PagedData<AccountPayablesClassSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new AccountPayablesClassSetup(jsonObj.vendorClassId,jsonObj.classDescription,jsonObj.classDescriptionArabic, jsonObj.creditLimit, jsonObj.creditLimitAmount,
        jsonObj.currencyId,jsonObj.maximumInvoiceAmount,jsonObj.minimumCharge,
        jsonObj.minimumChargeAmount, jsonObj.minimumOrderAmount,jsonObj.radeDiscountPercent,jsonObj.openMaintenanceHistoryCalendarYear,
        jsonObj.openMaintenanceHistoryDistribution,jsonObj.openMaintenanceHistoryFiscalYear,jsonObj.openMaintenanceHistoryTransaction,
        jsonObj.userDefine1,jsonObj.userDefine2,jsonObj.userDefine3,jsonObj.vatScheduleId,jsonObj.shipmentMethodId,jsonObj.checkBookId,
        jsonObj.paymentTermId,jsonObj.accTableId);
        
        pagedData.data.push(group);
            }
        pagedData.page = page;
            }
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}