import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';

import { AccountPayablesOptionSetup } from '../../../financialModule/_models/accounts-payables/accounts-payables-option-setup';


@Injectable()
export class AccountPayableOptionService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private saveAccountPayablesSetupOptionsUrl = Constants.financialModuleApiBaseUrl + 'account/payables/accountPayableOptionSetup';
    private getAccountPayablesOptionSetupUrl = Constants.financialModuleApiBaseUrl + 'account/payables/getAccountPayableOptionSetup';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    getAccountPayablesOptionSetup() {
       return this.http.get(this.getAccountPayablesOptionSetupUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
  
    //Save AccountPayableOption
    saveAccountPayablesSetup(AccountPayablesOptionSetup:AccountPayablesOptionSetup) {
        return this.http.post(this.saveAccountPayablesSetupOptionsUrl,JSON.stringify(AccountPayablesOptionSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}