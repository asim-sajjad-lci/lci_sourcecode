/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { BookMaintenance} from '../../../_models/master-data/fixedAssets/faBookMaintenance';

@Injectable()
export class BookMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAssetsUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAGeneralMaintenance';
    private getLifeDayByFABookIdUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/getLifeDayByFABookId';
    private getBooksUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchBookSetup';
    private getconventionListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/averagingConventionType';
    private getSwitchOverListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/switchOverType';	
    private getamortizationCodeListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/amortizationCodeType';	
    private getdepListUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/depreciationMethod';	
	private saveUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/add';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/search';
	private getUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/getById';
	private editUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/bookMaintenance/update';
    private getLifeDayDynamicByFABookIdUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/getLifeDayDynamicByFABookId';
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	getAssets() {
       return this.http
            .post(this.getAssetsUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	getBooks() {
       return this.http
            .post(this.getBooksUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }

    getLifeDayByFABook(bookId : string)
    {
        return this.http
        .post(this.getLifeDayByFABookIdUrl, { 'bookId': bookId }, { headers: this.headers })
        .map(data => data.json());
    }
	getConventionList() {
        return this.http.get(this.getconventionListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getSwitchOverList() {
        return this.http.get(this.getSwitchOverListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getamortizationCodeList() {
        return this.http.get(this.getamortizationCodeListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getDepList() {
        return this.http.get(this.getdepListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }

    getLifeDayDynamicByFABookId(Requesteddata)
    {
        return this.http.post(this.getLifeDayDynamicByFABookIdUrl, Requesteddata, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	search(page: Page,searchKeyword): Observable<PagedData<BookMaintenance>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getData(assetId : any) {
        return this.http.post(this.getUrl, { assetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	edit(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    private getPagedData(page: Page, data: any): PagedData<BookMaintenance> {
        let pagedData = new PagedData<BookMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
				//console.log(jsonObj);
                let group = new BookMaintenance(jsonObj.assetId,jsonObj.depreciationMethodId,jsonObj.averagingConvention,jsonObj.amortizationCode,jsonObj.depreciatedDate,jsonObj.placedInServiceDate);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              