/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { leaseMaintenance} from '../../../_models/master-data/fixedAssets/faLeaseMaintenance';

@Injectable()
export class LeaseMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAssetsUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAGeneralMaintenance';
    private getLeaseCompanyUrl = Constants.financialModuleApiBaseUrl + '/fixed/assets/leaseCompanySetup/getAll';
    private getLeaseTypeUrl = Constants.financialModuleApiBaseUrl + '/masterdata/fixed/assets/leaseType';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance/search';
	private getUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance/getById';
	private editUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance/update';
    private getleaseCompanyListUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/leaseCompanySetup/getAll';
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	getAssets() {
       return this.http
            .post(this.getAssetsUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }

	getLeaseCompanyList() {
        
      return this.http
            .post(this.getleaseCompanyListUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }

    getLease(){
        return this.http
            .post(this.searchUrl, { 'searchKeyword':'' }, { headers: this.headers })
            .map(data => data.json());
    }
    
	getLeaseType() {
        return this.http.get(this.getLeaseTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	search(page: Page,searchKeyword): Observable<PagedData<leaseMaintenance>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getData(leaseMaintenanceIndex : any) {
        
        return this.http.post(this.getUrl, { leaseMaintenanceIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	edit(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    private getPagedData(page: Page, data: any): PagedData<leaseMaintenance> {
        let pagedData = new PagedData<leaseMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new leaseMaintenance(jsonObj.assetId,jsonObj.leaseCompanyId,jsonObj.leaseTypeIndex,jsonObj.leaseContactNumber,jsonObj.leaseEndDate,jsonObj.leaseMaintenanceIndex,jsonObj.assetSerialId,jsonObj.interestRatePercent,jsonObj.leaseCompanyIndex,jsonObj.leasePayment);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              