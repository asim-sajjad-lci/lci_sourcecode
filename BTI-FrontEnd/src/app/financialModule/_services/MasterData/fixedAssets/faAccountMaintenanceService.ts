import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { faAccountMaintenance} from '../../../_models/master-data/fixedAssets/faAccountMaintenance';

@Injectable()
export class faAccountMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/accountTableSetup/accountType';
    private savefaAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/accountTableSetup';
    private getfaAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/getAccountTableSetup';
    private getFAGeneralMaintenanceByAssetIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFAGeneralMaintenanceByAssetId';
    private getFAGeneralMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAGeneralMaintenance';
    private searchAccountGroupSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getAccountGroupSetupBySearch';
    private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
    private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   
    getAccountType() {
        
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }

    getFAGeneralMaintenance()
    {
         return this.http.post(this.getFAGeneralMaintenanceUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    searchAccountGroupSetup()
    {
         return this.http.post(this.searchAccountGroupSetupUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    getfaAccountMaintenanceList() {
       return this.http.get(this.getfaAccountMaintenanceUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
   

    //Save CustomerMaintenanceOption
    savefaAccountMaintenance(faAccountMaintenance:faAccountMaintenance) {
        return this.http.post(this.savefaAccountMaintenanceUrl,JSON.stringify(faAccountMaintenance), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }

    restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}