/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { insuranMaintenance} from '../../../_models/master-data/fixedAssets/faInsuranceMaintenance';

@Injectable()
export class InsuranceMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAssetsUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAGeneralMaintenance';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/insuranceMaintenance';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/insuranceMaintenance/search';
	private getUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/insuranceMaintenance/getById';
	private editUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/insuranceMaintenance/update';
	private searchInsuranceUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/insuranceClassSetup/search';

    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	searchInsuranceClass() {
       return this.http
            .post(this.searchInsuranceUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .map(data => data.json().result);
    } 
	getAssets() {
       return this.http
            .post(this.getAssetsUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

	search(page: Page,searchKeyword): Observable<PagedData<insuranMaintenance>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getData(insClassId : any) {
        return this.http.post(this.getUrl, { insClassId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	edit(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    private getPagedData(page: Page, data: any): PagedData<insuranMaintenance> {
        let pagedData = new PagedData<insuranMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
               let group = new insuranMaintenance(jsonObj.assetId,jsonObj.insClassId,jsonObj.insuranceValue,jsonObj.insuranceYear,jsonObj.replacementCost,jsonObj.reproductionCost);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              