/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { InfoMaintenance} from '../../../_models/master-data/fixedAssets/faInformationMaintenance';

@Injectable()
export class InformationMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getStatusUrl = Constants.financialModuleApiBaseUrl + 'getFAGeneralMaintenanceAssetStatusList';
    private getClassUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAClassSetup';
    private getAssetTypeUrl = Constants.financialModuleApiBaseUrl + 'getFAGeneralMaintenanceAssetTypesList';
	private searchCurrencyUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
	private searchLocationUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/locationSetup/search';
	private searchPhysicalLocationUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/physicalLocationSetup/search';
	private searchStructureUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchStructureSetup';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/saveFAGeneralMaintenance';
	private getUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getFAGeneralMaintenanceByAssetId';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/searchFAGeneralMaintenance';
	private editUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/updateFAGeneralMaintenance';
	private getAccountGroupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/getAccountGroupSetupBySearch';
    private getPropertyTypeUrl = Constants.financialModuleApiBaseUrl + 'getFAPropertyTypes';
    
    
    /*private searchExchangeTableRateUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCurrencyExchangeDetail';
    private getExchangeTableRateUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCurrencyExchangeDetail';
    private editExchangeTableRateUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateCurrencyExchangeDetail';*/
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
	getPropertyType() {
        return this.http.get(this.getPropertyTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	
    getAccountGroup() {
        return this.http
            .post(this.getAccountGroupUrl, { 'searchKeyword': ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
	getStatus() {
        return this.http.get(this.getStatusUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getAssetType() {
        return this.http.get(this.getAssetTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }

    getClass() {
        return this.http
            .post(this.getClassUrl, { 'searchKeyword': ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json().result)
            .catch(this.handleError);
    }

    getCurrency() {
        return this.http
            .post(this.searchCurrencyUrl, { 'searchKeyword': ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json().result)
            .catch(this.handleError);
    }

    getLocation() {
        return this.http
            .post(this.searchLocationUrl, { 'searchKeyword': ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json().result)
            .catch(this.handleError);
    }
    getPhysicalLocation() {
        return this.http
            .post(this.searchPhysicalLocationUrl, { 'searchKeyword': ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json().result)
            .catch(this.handleError);
    }

    getStructure() {
        return this.http
            .post(this.searchStructureUrl, {'searchKeyword': ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    
	get(assetId : any) {
        return this.http.post(this.getUrl, { assetId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   
	search(page: Page,searchKeyword): Observable<PagedData<InfoMaintenance>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	
	edit(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	
    private getPagedData(page: Page, data: any): PagedData<InfoMaintenance> {
        let pagedData = new PagedData<InfoMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new InfoMaintenance(jsonObj.assetId,jsonObj.faDescription,jsonObj.faDescriptionArabic,jsonObj.faShortName,jsonObj.faClassSetupId);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              