/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { MainAccountSetup } from '../../../financialModule/_models/master-data/mainAccountSetup';


@Injectable()
export class MainAccountSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
    private updateMainAccountSetUpUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/updateMainAccountSetUp';
    private mainAccountSetUpUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/mainAccountSetUp';
    private  searchMainAccountSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchMainAccountSetup';
    private  searchActiveMainAccountSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchActiveMainAccountSetup';
    private  searchCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
    private  getMainAccountSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/getMainAccountSetupById';
    private  accountTypeListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/accountTypeList';
    private  accountCategoryListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/accountCategoryList';
    private  getTypicalBalanceTypeStatusUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/getTypicalBalanceTypeStatus';
   
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
    //update main account for edit 
    updateMainAccountSetUp(MainAccountSetup:MainAccountSetup){
        return this.http.post(this.updateMainAccountSetUpUrl,JSON.stringify(MainAccountSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create main account setup
    createAccountSetup(MainAccountSetup:MainAccountSetup)
    {
      return this.http.post(this.mainAccountSetUpUrl,JSON.stringify(MainAccountSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)

    }
    //Search main account setup 
    searchMainAccountSetup(page:Page, searchKeyword):Observable<PagedData<MainAccountSetup>>{
        return this.http.post(this.searchMainAccountSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

    getMainAccountSetupList()
    {
         return this.http.post(this.searchMainAccountSetupUrl,{ 'searchKeyword': ''}, {headers:this.headers})
             .map(data => data.json().result);
    }

    //get Active Main AccountSetup List
    getActiveMainAccountSetupList()
    {
         return this.http.post(this.searchActiveMainAccountSetupUrl,{ 'searchKeyword': ''}, {headers:this.headers})
             .map(data => data.json().result);
    }
//getListForAccountType
  getListForAccountType() {
        return this.http.post(this.accountTypeListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getListForAccountCategory
    getListForAccountCategory()
    {
         return this.http.post(this.accountCategoryListUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    //getTypicalBalanceTypeStatus

getTypicalBalanceTypeStatus()
    {
         return this.http.post(this.getTypicalBalanceTypeStatusUrl,{}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    
      //pagination of data
    private getPagedData(page: Page, data: any): PagedData<MainAccountSetup> {
        let pagedData = new PagedData<MainAccountSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new MainAccountSetup(jsonObj.accountCategoryId,jsonObj.accountTypeId,jsonObj.mainAccountNumber,jsonObj.mainAccountDescription, 
                jsonObj.mainAccountDescriptionArabic,jsonObj.tpclblnc, jsonObj.aliasAccount, jsonObj.aliasAccountArabic, jsonObj.allowAccountTransactionEntry
                ,jsonObj.active,jsonObj.userDef1 ,jsonObj.userDef2, jsonObj.userDef3, jsonObj.userDef4,jsonObj.userDef5,jsonObj.accountTypeName,jsonObj.accountCategoryDescription,jsonObj.actIndx);
                pagedData.data.push(group);
            }

            pagedData.page = page;
         }
        return pagedData;
    }
      //getting details of  mainaccount setup details By ID
    getMainAccountSetupById(actIndx: any) {
        
        return this.http.post(this.getMainAccountSetupByIdUrl, {'actIndx': actIndx }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              