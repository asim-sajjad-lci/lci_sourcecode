import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { VendorAccountMaintenance } from '../../../../financialModule/_models/master-data/accountPayables/vendor-account-maintenance';
 

@Injectable()
export class VendorAccountMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'getAccountTypes';
    private saveCustomerAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'account/payables/saveVendorAccountMaintenance';
    private getVendorAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'account/payables/getVendorAccountMaintenanceByVendorId';
    private getVendorByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/getOne/vender';
    private getVendorMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/find/Activevender';
    private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
    private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        
    }

   

   getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }


    //getCustomerIdList

    getVendorIdList()
    {
         return this.http.post(this.getVendorMaintenanceListUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


     getVendorAccountMaintenanceList(vendorId :string) {
        return this.http.post(this.getVendorAccountMaintenanceUrl, { vendorId:vendorId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   getVendorByID(venderId :string) {
        return this.http.post(this.getVendorByIdUrl, { venderId:venderId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Save CustomerMaintenanceOption
     saveVendorAccountMaintenanace(VendorAccountMaintenance:VendorAccountMaintenance) {
        return this.http.post(this.saveCustomerAccountMaintenanceUrl,JSON.stringify(VendorAccountMaintenance), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }

    restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}