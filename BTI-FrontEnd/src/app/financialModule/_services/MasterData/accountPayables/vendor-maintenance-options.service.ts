import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';

@Injectable()
export class VendorOptionsMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private vendorUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/find/Activevender';
    private getVendorByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/getOne/vender';
	private searchShippingMethodUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchShipmentMethodSetup';
	private searchVatUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchVatDetailSetup';
	private searchCurrencyUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
	private searchCheckbookUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';
	private searchPaymentUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchPaymentTermSetup';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/vendorMaintenanceOptions';
	private getListUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/getVendorMaintenanceOptions';
    private getDataUrl = Constants.financialModuleApiBaseUrl + '/setup/accountPayableMaster/getVendorMaintenanceOptionsByVendorId';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

     getData(vendorId : any) {
        return this.http.post(this.getDataUrl, { vendorId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    searchVendors() {
       return this.http
            .post(this.vendorUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    } 
	getVendorByID(venderId : any) {
        return this.http.post(this.getVendorByIdUrl, { venderId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	searchShippingMethod() {
        return this.http
            .post(this.searchShippingMethodUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchVat() {
        return this.http
            .post(this.searchVatUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchCurrency() {
        return this.http
            .post(this.searchCurrencyUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchCheckbook() {
        return this.http
            .post(this.searchCheckbookUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	searchPayment() {
        return this.http
            .post(this.searchPaymentUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
  
    create(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   getList() {
        return this.http.get(this.getListUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}