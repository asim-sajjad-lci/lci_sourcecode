/**
 * A service class for authsetting
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { CustomerMaintenance } from '../../../../financialModule/_models/master-data/accountReceivables/customerMaintenance';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()

export class masterAccountReceivableService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getCountryListUrl = Constants.userModuleApiBaseUrl + 'getCountryList';
	private getStateListUrl = Constants.userModuleApiBaseUrl + 'getStateListByCountryId';
	private getCityListUrl = Constants.userModuleApiBaseUrl + 'getCityListByStateId';
	private createMasterCustomerMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/maintenance';
	private editMasterCustomerMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/updateMaintenance';
	private getMasterCustomerMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/maintenanceGetById';
	private searchCustomerMaintenanceGroups = Constants.financialModuleApiBaseUrl +'setup/accounts/searchMaintenance';
   
    //initializing parameter for constructor  
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
         localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }  
	 getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getStateList(countryId: any) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getCityList(stateId: any) {
        return this.http.post(this.getCityListUrl, { 'stateId': stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	createMasterCustomerMaintenance(submittedData) {
        return this.http.post(this.createMasterCustomerMaintenanceUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	editMasterCustomerMaintenance(submittedData) {
        return this.http.post(this.editMasterCustomerMaintenanceUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getMasterCustomerMaintenance(customerId : any) {
        return this.http.post(this.getMasterCustomerMaintenanceUrl, { customerId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	searchMaintenance(page: Page,searchKeyword): Observable<PagedData<CustomerMaintenance>> {
        return this.http
            .post(this.searchCustomerMaintenanceGroups, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	
	 //Pagination of data 
    private getPagedData(page: Page, data: any): PagedData<CustomerMaintenance> {
        let pagedData = new PagedData<CustomerMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new CustomerMaintenance(jsonObj.activeCustomer,jsonObj.customerHold,jsonObj.customerId, jsonObj.name, jsonObj.arabicName,jsonObj.shortName,jsonObj.classId,jsonObj.statementName,jsonObj.address1,jsonObj.address2, jsonObj.address3, jsonObj.phone1,jsonObj.phone2,jsonObj.phone3,jsonObj.fax, jsonObj.countryName, jsonObj.stateName,jsonObj.cityName,jsonObj.userDefine1,jsonObj.userDefine2,jsonObj.priority);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
	//error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}