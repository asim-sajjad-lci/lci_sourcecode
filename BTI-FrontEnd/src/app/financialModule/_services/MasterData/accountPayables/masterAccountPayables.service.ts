/**
 * A service class for authsetting
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { VendorMaintenance } from '../../../../financialModule/_models/master-data/accountPayables/vendorMaintenance';
import { Constants } from '../../../../_sharedresource/Constants'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Injectable()

export class masterAccountPayableService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getCountryListUrl = Constants.userModuleApiBaseUrl + 'getCountryList';
	private getStateListUrl = Constants.userModuleApiBaseUrl + 'getStateListByCountryId';
	private getCityListUrl = Constants.userModuleApiBaseUrl + 'getCityListByStateId';
	private createVendorCustomerMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/add/vender';
	private editMasterVendorMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/add/vender';
	private getMasterVendorMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accountPayableMaster/getOne/vender';
	private searchVendorMaintenanceGroups = Constants.financialModuleApiBaseUrl +'setup/accountPayableMaster/find/vender';
	private vendorClassUrl = Constants.financialModuleApiBaseUrl +'account/payables/searchAccountPayableClassSetup';
   
    //initializing parameter for constructor  
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
         localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }  
	getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getStateList(countryId: any) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getCityList(stateId: any) {
        return this.http.post(this.getCityListUrl, { 'stateId': stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	createMasterVendorMaintenance(submittedData) {
        return this.http.post(this.createVendorCustomerMaintenanceUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	editMasterVendorMaintenance(submittedData) {
        return this.http.post(this.editMasterVendorMaintenanceUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getMasterVendorMaintenance(venderId : any) {
        return this.http.post(this.getMasterVendorMaintenanceUrl, { venderId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	searchMaintenance(page: Page,searchKeyword): Observable<PagedData<VendorMaintenance>> {
        return this.http
            .post(this.searchVendorMaintenanceGroups, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	 getVendorClass() {
       return this.http
            .post(this.vendorClassUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	 //Pagination of data 
    private getPagedData(page: Page, data: any): PagedData<VendorMaintenance> {
        let pagedData = new PagedData<VendorMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new VendorMaintenance(jsonObj.venderId, jsonObj.venderNamePrimary, jsonObj.venderNameSecondary,jsonObj.venderStatus,jsonObj.shortName,jsonObj.classId,jsonObj.statementName,jsonObj.address1,jsonObj.address2, jsonObj.address3,jsonObj.country, jsonObj.state,jsonObj.city, jsonObj.phone1,jsonObj.phone2,jsonObj.phone3,jsonObj.fax, jsonObj.userDefine1,jsonObj.userDefine2,jsonObj.priority);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }
	//error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}