/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';

@Injectable()
export class FixedAllocationAccountService {
    private headers = new Headers({ 'content-type': 'application/json' });
	private getAccountListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchActiveMainAccountSetup';
	private getAccountDescUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/getMainAccountSetupById';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/saveFixedAllocationDetail';	
	private getAccountDetailsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/fixedAllocationDetail/getByAccountNumber';		
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
	// getAccountList() {
    //     return this.http.get(this.getAccountListUrl, { headers: this.headers })
    //    .toPromise()
    //    .then(res => res.json())
    //    .catch(this.handleError);
    // }

    getAccountList() {
        return this.http.post(this.getAccountListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

	getAccountDescription(actIndx : any) {
        return this.http.post(this.getAccountDescUrl, { actIndx }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	getAccountDetails(mainAccountID : any) {
         return this.http
            .post(this.getAccountDetailsUrl, { mainAccountID }, { headers: this.headers })
            .map(data => data.json());
    }
	/*getClass() {
        return this.http
            .post(this.getClassUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	getClassDescription(vendorClassId : any) {
        return this.http.post(this.getClassDescUrl, { vendorClassId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	
	getEditRecord() {
        return this.http.get(this.getEditRecordUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	/*getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	*/
	
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              