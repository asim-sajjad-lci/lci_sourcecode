/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { FinancialDimentions } from '../../../../financialModule/_models/master-data/general-ledger/financial-dimensions-setup';


@Injectable()
export class FinancialDimentionsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchFinancialDimentionsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchFinancialDimensionSetup';
    private getFinancialDimentionsByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/financialDimensionSetupGetById';
    private updateFinancialDimentionsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/financialDimensionSetupUpdate';
    private saveFinancialDimentionsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/financialDimensionSetup';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

    //Search FinancialDimentions
    searchFinancialDimentions(page:Page, searchKeyword):Observable<PagedData<FinancialDimentions>>{
        return this.http.post(this.searchFinancialDimentionsUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of FinancialDimentions By ID
    getFinancialDimentionsById(dimInxd: string) {
        return this.http.post(this.getFinancialDimentionsByIdUrl, { dimInxd: dimInxd }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update FinancialDimentions for edit 
    updateFinancialDimentions(FinancialDimentions:FinancialDimentions){
        return this.http.post(this.updateFinancialDimentionsUrl,JSON.stringify(FinancialDimentions),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create FinancialDimentions
    createFinancialDimentions(FinancialDimentions:FinancialDimentions)
    {
      return this.http.post(this.saveFinancialDimentionsUrl,JSON.stringify(FinancialDimentions),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<FinancialDimentions> {
        let pagedData = new PagedData<FinancialDimentions>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new FinancialDimentions(jsonObj.dimInxd,jsonObj.dimensionName,jsonObj.dimensionMask);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}