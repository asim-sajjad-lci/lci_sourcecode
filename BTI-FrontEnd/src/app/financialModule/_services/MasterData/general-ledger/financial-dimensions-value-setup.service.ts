/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { FinancialDimentionsValue } from '../../../../financialModule/_models/master-data/general-ledger/financial-dimensions-value-setup';

@Injectable()
export class FinancialDimentionsValueService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getFinancialDimentionsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchFinancialDimensionSetup';
    private searchFinancialDimentionsValueUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchFinancialDimensionValue';
    private getFinancialDimentionsValueByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/financialDimensionValueGetById';
    private updateFinancialDimentionsValueUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/financialDimensionValueUpdate';
    private saveFinancialDimentionsValueUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/financialDimensionValue';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

     //get FinancialDimentions List
    getFinancialDimentionsList() {
        return this.http.post(this.getFinancialDimentionsUrl, {"searchKeyword": "",
	"pageNumber" : 0,
	"pageSize" : 1000
  }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }



    //Search FinancialDimentionsValue
    searchFinancialDimentionsValue(page:Page, searchKeyword):Observable<PagedData<FinancialDimentionsValue>>{
        return this.http.post(this.searchFinancialDimentionsValueUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of FinancialDimentionsValue By ID
    getFinancialDimentionsValueById(dimInxValue: string) {
        return this.http.post(this.getFinancialDimentionsValueByIdUrl, { dimInxValue: dimInxValue }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update FinancialDimentionsValue for edit 
    updateFinancialDimentionsValue(FinancialDimentionsValue:FinancialDimentionsValue){
        return this.http.post(this.updateFinancialDimentionsValueUrl,JSON.stringify(FinancialDimentionsValue),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create FinancialDimentionsValue
    createFinancialDimentionsValue(FinancialDimentionsValue:FinancialDimentionsValue)
    {
      return this.http.post(this.saveFinancialDimentionsValueUrl,JSON.stringify(FinancialDimentionsValue),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<FinancialDimentionsValue> {
        let pagedData = new PagedData<FinancialDimentionsValue>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new FinancialDimentionsValue(jsonObj.dimInxValue,jsonObj.dimInxd,jsonObj.dimensionValue,jsonObj.dimensionDescription,jsonObj.dimensionDescriptionArabic,jsonObj.dimensionName);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}