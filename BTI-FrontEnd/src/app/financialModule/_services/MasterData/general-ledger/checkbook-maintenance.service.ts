/**
 * A service class for checkbook Maintenance
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { CheckbookMaintenance } from '../../../../financialModule/_models/master-data/general-ledger/checkbook-maintenance';


@Injectable()
export class CheckbookMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchCheckbookMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchCheckBookMaintenance';
    private getCheckbookMaintenanceByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getCheckBookMaintenanceByCheckbookId';
    private updateCheckbookMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateCheckBookMaintenanceByCheckbookId';
    private saveCheckbookMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveCheckBookMaintenance';
    private getBankIdListUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchBankSetup';
    private getCurrencyIdListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
    private getAccountIdListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchActiveMainAccountSetup';
    private getGlAccountNumberListUrl=Constants.financialModuleApiBaseUrl + 'getGlAccountNumberList';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

    //Search checkbook Maintenance
    searchCheckbookMaintenance(page:Page, searchKeyword):Observable<PagedData<CheckbookMaintenance>>{
        return this.http.post(this.searchCheckbookMaintenanceUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of checkbook Maintenance By ID
    getCheckbookMaintenanceById(checkBookId: string) {
        return this.http.post(this.getCheckbookMaintenanceByIdUrl, { checkBookId: checkBookId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update checkbook Maintenance for edit 
    updateCheckbookMaintenance(CheckbookMaintenance:CheckbookMaintenance){
        return this.http.post(this.updateCheckbookMaintenanceUrl,JSON.stringify(CheckbookMaintenance),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create checkbook Maintenance
    createCheckbookMaintenance(CheckbookMaintenance:CheckbookMaintenance)
    {
      return this.http.post(this.saveCheckbookMaintenanceUrl,JSON.stringify(CheckbookMaintenance),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    
    getBankIDSetupList(){
         return this.http.post(this.getBankIdListUrl, { 'searchKeyword': '' }, {headers:this.headers})
                 .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
    }

    getGlAccountNumberList()
    {
         return this.http.get(this.getGlAccountNumberListUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCurrencyIDSetupList(){
         return this.http.post(this.getCurrencyIdListUrl, { 'searchKeyword': '' }, {headers:this.headers})
                 .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
    }
    getAccountIDSetupList(){
        return this.http.post(this.getAccountIdListUrl, { 'searchKeyword': '' }, {headers:this.headers})
                 .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
    }

  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<CheckbookMaintenance> {
        let pagedData = new PagedData<CheckbookMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new CheckbookMaintenance(jsonObj.checkBookId,jsonObj.inactive,jsonObj.checkbookDescription,jsonObj.checkbookDescriptionArabic,jsonObj.currencyId,jsonObj.glAccountId,jsonObj.nextCheckNumber,jsonObj.nextDepositNumber,jsonObj.lastReconciledDate,jsonObj.lastReconciledBalance,jsonObj.officialBankAccountNumber,jsonObj.bankId,jsonObj.userDefine1,jsonObj.userDefine2,jsonObj.currentCheckbookBalance,jsonObj.cashAccountBalance,jsonObj.exceedMaxCheckAmount,jsonObj.passwordOfMaxCheckAmount,jsonObj.duplicateCheckNumber,jsonObj.overrideCheckNumber);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}