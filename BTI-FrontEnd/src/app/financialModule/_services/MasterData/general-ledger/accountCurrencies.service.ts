/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { CurrencySetup } from '../../../../financialModule/_models/general-ledger-setup/currencysetup';


@Injectable()
export class AccountCurrecniesService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getMainAccountsUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/searchActiveMainAccountSetup';
    private getAccountByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/accountCurrencyAccess/getById';
	 private searchCurrencySetupUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getActiveCurrencySetup';
	 private saveUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/accountCurrencyAccess/add';
	 private searchUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedgerMaster/getAccountCurrencyAccess';
    /*private getLeaseCompanyUrl = Constants.financialModuleApiBaseUrl + '/fixed/assets/leaseCompanySetup/get';
    private getLeaseTypeUrl = Constants.financialModuleApiBaseUrl + '/masterdata/fixed/assets/leaseType';
	private saveUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance';
	private searchUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance/search';
	private getUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance/getById';
	private editUrl = Constants.financialModuleApiBaseUrl + 'masterdata/fixed/assets/leaseMaintenance/update';*/

    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

	getData() {
        return this.http.get(this.searchUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);

    }
	getMainAccounts() {
       return this.http
            .post(this.getMainAccountsUrl, { 'searchKeyword': '','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
            .map(data => data.json().result);
    }
	getAccountById(accountIndex : any) {
        return this.http.post(this.getAccountByIdUrl, {accountIndex}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	// searchCurrencySetup(page:Page, searchKeyword):Observable<PagedData<CurrencySetup>>{
    //     return this.http.post(this.searchCurrencySetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
    //          .map(data => this.getPagedData(page,data.json().result));
    // }
    getCurrencylist(){
        return this.http.post(this.searchCurrencySetupUrl,{ 'searchKeyword': '','pageNumber': '0','pageSize':'100' }, {headers:this.headers})
             .map(data => data.json().result);
    }
	private getPagedData(page: Page, data: any): PagedData<CurrencySetup> {
        let pagedData = new PagedData<CurrencySetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new CurrencySetup(jsonObj.currencyId,jsonObj.currencyDescription,jsonObj.currencyDescriptionArabic,jsonObj.currencySymbol, jsonObj.currencyUnit, 
                jsonObj.unitSubunitConnector,jsonObj.currencySubunit, jsonObj.currencyUnitArabic, jsonObj.unitSubunitConnectorArabic, jsonObj.currencySubunitArabic,jsonObj.separatorsDecimal,jsonObj.includeSpaceAfterCurrencySymbol,
				jsonObj.negativeSymbol,jsonObj.displayNegativeSymbolSign,jsonObj.separatorsThousands,jsonObj.displayCurrencySymbol);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	/*getLease() {
        return this.http.get(this.getLeaseCompanyUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	getLeaseType() {
        return this.http.get(this.getLeaseTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
	save(submittedData) {
        return this.http.post(this.saveUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	search(page: Page,searchKeyword): Observable<PagedData<leaseMaintenance>> {
        return this.http
            .post(this.searchUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }
	getData(leaseMaintenanceIndex : any) {
        return this.http.post(this.getUrl, { leaseMaintenanceIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
	edit(submittedData) {
        return this.http.post(this.editUrl, submittedData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    private getPagedData(page: Page, data: any): PagedData<leaseMaintenance> {
        let pagedData = new PagedData<leaseMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
				console.log(jsonObj);
                let group = new leaseMaintenance(jsonObj.assetId,jsonObj.leaseCompanyId,jsonObj.leaseTypeIndex,jsonObj.leaseContactNumber,jsonObj.leaseEndDate,jsonObj.leaseMaintenanceIndex,jsonObj.assetSerialId,jsonObj.interestRatePercent,jsonObj.leaseCompanyIndex,jsonObj.leasePayment);
                pagedData.data.push(group);
            }
            pagedData.page = page;
        }
        return pagedData;
    }*/
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              