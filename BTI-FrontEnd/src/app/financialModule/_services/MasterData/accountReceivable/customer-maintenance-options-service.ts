import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';

import { CustomerMaintenanceOptions } from '../../../../financialModule/_models/master-data/accountReceivables/customer-maintenance-option';
 

@Injectable()
export class CustomerMaintenanceOptionsService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private saveCustomerMaintenanceOptionsUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/saveCustomerMaintenanceOptions';
    private getCustomerMaintenanceOptionUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getCustomerMaintenanceOption';
     private getCustomerMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveCustomerMaintenance';
    private getSalesmanMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveSalesmanMaintenance';
    private getCustomerDataUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getCustomerMaintenanceOptionByCustomerId';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    getCustomerMaintenanceOption() {
       return this.http.get(this.getCustomerMaintenanceOptionUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }
    //getCustomerIdList

     getCustomerData(customerNumberId : any) {
        return this.http.post(this.getCustomerDataUrl, { customerNumberId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getCustomerIdList()
    {
         return this.http.post(this.getCustomerMaintenanceListUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
       //getSalesmanMaintenanceList
     getSalesmanMaintenanceList() {
        return this.http.post(this.getSalesmanMaintenanceListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
  
    //Save CustomerMaintenanceOption
    saveCustomerMaintenanaceOption(CustomerMaintenanceOptions:CustomerMaintenanceOptions) {
        return this.http.post(this.saveCustomerMaintenanceOptionsUrl,JSON.stringify(CustomerMaintenanceOptions), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}