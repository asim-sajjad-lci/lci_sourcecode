/**
 * A service class for exchange table set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { NationalAccountMaintenance } from '../../../../financialModule/_models/master-data/accountReceivables/national-account-maintenance';


@Injectable()
export class NationalAccountMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
    private updateNationalAccountUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/nationalAccountMaintenance/update';
    private createNationalAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/nationalAccountMaintenance';
    private  searchNationalAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/nationalAccountMaintenance/search';
    private  getNationalAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/nationalAccountMaintenance/getById';
    private  getCustomerListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveCustomerMaintenance';
    private  getCustomerDetailsUrl  = Constants.financialModuleApiBaseUrl + 'setup/accounts/maintenanceGetById';
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
    //update NationalAccountMaintenance for edit 
    updateNationalAccountMaintenance(NationalAccountMaintenance:NationalAccountMaintenance){
        return this.http.post(this.updateNationalAccountUrl,JSON.stringify(NationalAccountMaintenance),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create NationalAccountMaintenance
    createNationalAccountSetup(NationalAccountMaintenance:NationalAccountMaintenance)
    {
      return this.http.post(this.createNationalAccountMaintenanceUrl,JSON.stringify(NationalAccountMaintenance),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)

    }
    //Search national Account Maintenance
    searchNationalAccountMaintenance(page:Page, searchKeyword):Observable<PagedData<NationalAccountMaintenance>>{
        return this.http.post(this.searchNationalAccountMaintenanceUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

    getCustomerId() {
        return this.http.post(this.getCustomerListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    } 

  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<NationalAccountMaintenance> {
        let pagedData = new PagedData<NationalAccountMaintenance>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new NationalAccountMaintenance(jsonObj.custNumber,jsonObj.custNamePrimary,jsonObj.custNameSecondary,jsonObj.allowReceiptEntry, 
                jsonObj.baseCreditCheck,jsonObj.applyStatus, jsonObj.baseFinanceCharge, jsonObj.accountMaintenanceDetail);
                pagedData.data.push(group);
            }

        pagedData.page = page;
         }
        return pagedData;
    }
      //getting details of  National Account Maintenance details By ID
    getNationalAccountMaintenanceById(custNumber: string) {
        return this.http.post(this.getNationalAccountMaintenanceUrl, { custNumber: custNumber }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     getCustomerDetails(customerId: string) {
        return this.http.post(this.getCustomerDetailsUrl, { customerId:customerId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              