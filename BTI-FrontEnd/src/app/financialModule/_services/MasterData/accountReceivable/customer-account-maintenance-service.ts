import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';

import { customerAccountMaintenance } from '../../../../financialModule/_models/master-data/accountReceivables/customer-account-maintenance';
 

@Injectable()
export class CustomerAccountMaintenanceService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'getArAccountTypes';
    private saveCustomerAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/saveCustomerAccountMaintenance';
    private getCustomerAccountMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getCustomerAccountMaintenanceByCustomerId';
    private getCustomerByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/maintenanceGetById';
    private getCustomerMaintenanceListUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchActiveCustomerMaintenance';
    private firstTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkMainAccountNumber';
    private restTextApiUrl = Constants.financialModuleApiBaseUrl + 'checkDimensionValues';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

   getAccountType() {
        return this.http.get(this.getAccountTypeUrl, { headers: this.headers })
       .toPromise()
       .then(res => res.json())
       .catch(this.handleError);
    }


    //getCustomerIdList

    getCustomerIdList()
    {
         return this.http.post(this.getCustomerMaintenanceListUrl,{'searchKeyword': ''}, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    getCustomerAccountMaintenanceList(customerId : any) {
         return this.http.post(this.getCustomerAccountMaintenanceUrl, { customerId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getCustomerByID(customerId : any) {
        return this.http.post(this.getCustomerByIdUrl, { customerId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Save CustomerMaintenanceOption
    saveCustomerAccountMaintenanace(customerAccountMaintenance:customerAccountMaintenance) {
        return this.http.post(this.saveCustomerAccountMaintenanceUrl,JSON.stringify(customerAccountMaintenance), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    firstTextApi(mainAccountNumber : any) {
         return this.http
            .post(this.firstTextApiUrl, { mainAccountNumber }, { headers: this.headers })
            .map(data => data.json());
    }

    restTextApi(dimensionValue : any,segmentNumber:number) {
         return this.http
            .post(this.restTextApiUrl, { 'dimensionValue':dimensionValue,'segmentNumber':segmentNumber }, { headers: this.headers })
            .map(data => data.json());
    }
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}