/**
 * A service class for salesterritorysetup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { SalesTerritorySetup } from '../../../../financialModule/_models/master-data/accountReceivables/sales-territory-setup';


@Injectable()
export class SalesTerritorySetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
    private updateSalesTerritorySetupUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/update';
    private  searchSalesTerritoryUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/search';
    private  getSalesTerritoryByIdUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/getById';
    private  accountCategoryListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/accountCategoryList';
    private  saveSalesTerritorySetupUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup';
   
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
    //update sales territory for edit 
    updateSalesTerritorySetup(SalesTerritorySetup:SalesTerritorySetup){
        return this.http.post(this.updateSalesTerritorySetupUrl,JSON.stringify(SalesTerritorySetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create sales territory setup
    createSalesTerritorySetup(SalesTerritorySetup:SalesTerritorySetup)
    {
      return this.http.post(this.saveSalesTerritorySetupUrl,JSON.stringify(SalesTerritorySetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)

    }
    //Search main account setup 
    searchSalesTerritory(page:Page, searchKeyword):Observable<PagedData<SalesTerritorySetup>>{
        return this.http.post(this.searchSalesTerritoryUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


  

  
    
      //pagination of data
    private getPagedData(page: Page, data: any): PagedData<SalesTerritorySetup> {
        let pagedData = new PagedData<SalesTerritorySetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new SalesTerritorySetup(jsonObj.salesTerritoryId,jsonObj.descriptionPrimary,jsonObj.descriptionSecondary,jsonObj.phone1, 
                jsonObj.phone2,jsonObj.managerFirstNamePrimary, jsonObj.managerMidNamePrimary, jsonObj.managerLastNamePrimary, jsonObj.managerFirstNameSecondary
                ,jsonObj.managerMidNameSecondary,jsonObj.managerLastNameSecondary ,jsonObj.totalCommissionsYTD, jsonObj.totalCommissionsLY, jsonObj.commissionsSalesYTD,jsonObj.costOfSalesYTD,jsonObj.accountTypeName,jsonObj.costOfSalesLY);
               
                pagedData.data.push(group);
            }
                                            
                pagedData.page = page;
         }
        return pagedData;
    }
      //getting details of  sales territory setup details By salesTerritoryId
    getSalesTerritoryById(salesTerritoryId: number) {
        return this.http.post(this.getSalesTerritoryByIdUrl, { salesTerritoryId: salesTerritoryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              