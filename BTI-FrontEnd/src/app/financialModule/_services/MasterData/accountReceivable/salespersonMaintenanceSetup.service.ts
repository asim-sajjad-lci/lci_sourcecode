/**
 * A service class for SalespersonMaintenanceSetup
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../../_sharedresource/paged-data';
import { Page } from '../../../../_sharedresource/page';
import { Constants } from '../../../../_sharedresource/Constants';
import { SalespersonMaintenanceSetup } from '../../../../financialModule/_models/master-data/accountReceivables/salesperson-maintenance-setup';


@Injectable()
export class SalespersonMaintenanceSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
 
    private  updateSalesmanMaintenanceUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/updateSalesmanMaintenance';
    private  searchSalespersonSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/searchSalesmanMaintenance';
    private  getSalesmanMaintenanceByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/salesmanMaintenanceGetById';
    private  getSalesTerritoryListUrl = Constants.financialModuleApiBaseUrl + 'account/receivable/salesTerritorySetup/search';
    private  saveSalesPersonMaintenaceSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/salesmanMaintenance';
    
    currentLanguage = "";
    //initializing parameter for constructor
 constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  //getSalesTerritoryList
 getSalesTerritoryList() {
        return this.http.post(this.getSalesTerritoryListUrl, { 'searchKeyword': '' }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
 

    //update salespeson maintenance for edit 
    updateSalesmanMaintenance(SalespersonMaintenanceSetup:SalespersonMaintenanceSetup){
        return this.http.post(this.updateSalesmanMaintenanceUrl,JSON.stringify(SalespersonMaintenanceSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create salespeson maintenance setup
    createSalesPersonMaintenaceSetup(SalespersonMaintenanceSetup:SalespersonMaintenanceSetup)
    {
      return this.http.post(this.saveSalesPersonMaintenaceSetupUrl,JSON.stringify(SalespersonMaintenanceSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)

    }
    //Search salesperson M. setup
    searchSalespersonSetup(page:Page, searchKeyword):Observable<PagedData<SalespersonMaintenanceSetup>>{
        return this.http.post(this.searchSalespersonSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<SalespersonMaintenanceSetup> {
        let pagedData = new PagedData<SalespersonMaintenanceSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new SalespersonMaintenanceSetup(jsonObj.salesmanId, jsonObj.address1, jsonObj.address2,jsonObj.applyPercentage, jsonObj.costOfSalesYTD,
                jsonObj.costOfSalesLastYear ,jsonObj.employeeId,jsonObj.salesmanFirstName,jsonObj.salesmanFirstNameArabic,
                jsonObj.inactive,jsonObj.salesmanLastName,jsonObj.salesmanLastNameArabic,jsonObj.salesmanMidName,jsonObj.salesmanMidNameArabic,jsonObj.percentageAmount ,jsonObj.phone1,jsonObj.phone2,
                jsonObj.salesTerritoryId,jsonObj.salesCommissionsYTD,jsonObj.salesCommissionsLastYear,jsonObj.totalCommissionsYTD,jsonObj.totalCommissionsLastYear)
                pagedData.data.push(group);
            }
                                            
                pagedData.page = page;
         }
        return pagedData;
    }
      //getting details of  salesperson maintenance setup details By salesmanId
    salesmanMaintenanceGetById(salesmanId: string) {
        return this.http.post(this.getSalesmanMaintenanceByIdUrl, { salesmanId: salesmanId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
                                                                                                                                                                                                                
                                              