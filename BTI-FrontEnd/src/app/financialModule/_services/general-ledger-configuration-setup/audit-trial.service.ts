/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { AuditTrial } from '../../../financialModule/_models/general-ledger-configuration-setup/audit-trial';


@Injectable()
export class AuditTrialService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchAuditTrialUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchAuditTrialCode';
    private getAuditTrialByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAuditTrialCodeById';
    private updateAuditTrialUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateAuditTrialCode';
    private saveAuditTrialUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveAuditTrialCode';
    
    private getAuditTrialseriesUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAuditTrialSeriesTypeStatus';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    // getAuditTrialSeries()
    // {
    //     return this.http.get(this.getAuditTrialseriesUrl, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }
    
// getAuditTrialSeries() {
//         return this.http
//             .post(this.getAuditTrialseriesUrl, { 'searchKeyword':'','pageNumber': '', 'pageSize': '' }, { headers: this.headers })
//             .map(data => data.json().result);
//     }

getAuditTrialSeries()
    {
         return this.http.get(this.getAuditTrialseriesUrl, {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
  searchAuditTrial(page:Page, searchKeyword):Observable<PagedData<AuditTrial>>{
        return this.http.post(this.searchAuditTrialUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }
     //getting details of  payment Terms By ID
    getAuditTrialById(seriesIndex: number) {
        return this.http.post(this.getAuditTrialByIdUrl, { seriesIndex: seriesIndex }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update rolegroup for edit 
    updateAuditTrial(AuditTrial:AuditTrial){
        return this.http.post(this.updateAuditTrialUrl,JSON.stringify(AuditTrial),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create comapny setup
    createAuditTrial(AuditTrial:AuditTrial)
    {
      return this.http.post(this.saveAuditTrialUrl,JSON.stringify(AuditTrial),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<AuditTrial> {
        let pagedData = new PagedData<AuditTrial>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new AuditTrial(jsonObj.sourceCode,jsonObj.seriesId,jsonObj.sourceDocument,jsonObj.sequenceNumber,jsonObj.seriesIndex,jsonObj.seriesNumber,jsonObj.seriesName);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}