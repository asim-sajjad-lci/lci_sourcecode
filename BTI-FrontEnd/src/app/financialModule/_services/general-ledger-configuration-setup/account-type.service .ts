/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { AccountType } from '../../../financialModule/_models/general-ledger-configuration-setup/account-type';


@Injectable()
export class AccountTypeService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/accountTypeList';
    private getAccountTypeByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountTypeById';
    private updateAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateAccountType';
    private saveAccountTypeUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveAccountType';
    private getAccountTypeByNameUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountTypeByName';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

   

    //Search payment Terms
    searchAccountType(page:Page, searchKeyword):Observable<PagedData<AccountType>>{
        return this.http.post(this.searchAccountTypeUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of  payment Terms By ID
    getAccountTypeById(accountTypeId: string) {
        return this.http.post(this.getAccountTypeByIdUrl, { accountTypeId: accountTypeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update rolegroup for edit 
    updateAccountType(AccountType:AccountType){
       return this.http.post(this.updateAccountTypeUrl,JSON.stringify(AccountType),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create comapny setup
    createAccountType(AccountType:AccountType)
    {
      return this.http.post(this.saveAccountTypeUrl,JSON.stringify(AccountType),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

      //Get AccountType By Name
    GetAccountTypeByName(accountTypeName:string)
    {
        
      return this.http.post(this.getAccountTypeByNameUrl,{accountTypeName:accountTypeName},{headers:this.headers})
        .toPromise().then(res => res.json())
        .catch(this.handleError)
    }

  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<AccountType> {
        let pagedData = new PagedData<AccountType>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new AccountType(jsonObj.accountTypeId,jsonObj.accountTypeName,jsonObj.accountTypeNameArabic);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}