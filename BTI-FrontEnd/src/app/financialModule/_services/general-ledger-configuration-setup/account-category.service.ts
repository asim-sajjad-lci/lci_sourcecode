/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { AccountCategory } from '../../../financialModule/_models/general-ledger-configuration-setup/account-category';


@Injectable()
export class AccountCategoryService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchAccountCategoryUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/accountCategoryList';
    private getAccountCategoryByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountCategoryById';
    private updateAccountCategoryUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/updateAccountCategory';
    private saveAccountCategoryUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/saveAccountCategory';
    private getAccountCategoryByNameUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/getAccountCategoryByName';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

     //search userGroup
    searchGroup(page: Page,searchKeyword): Observable<PagedData<AccountCategory>> {
        return this.http
            .post(this.searchAccountCategoryUrl, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result));
    }

    //Search AccountCategory
    searchAccountCategory(page:Page, searchKeyword):Observable<PagedData<AccountCategory>>{
        return this.http.post(this.searchAccountCategoryUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of AccountCategory By ID
    getAccountCategoryById(accountCategoryId: string) {
        return this.http.post(this.getAccountCategoryByIdUrl, { accountCategoryId: accountCategoryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update AccountCategory for edit 
    updateAccountCategory(AccountCategory:AccountCategory){
        return this.http.post(this.updateAccountCategoryUrl,JSON.stringify(AccountCategory),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create Account Category
    createAccountCategory(AccountCategory:AccountCategory)
    {
      return this.http.post(this.saveAccountCategoryUrl,JSON.stringify(AccountCategory),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    //Get Account Category By Name
    GetAccountCategoryByNameUrl(categoryName:string)
    {
        
      return this.http.post(this.getAccountCategoryByNameUrl,{categoryName:categoryName},{headers:this.headers})
        .toPromise().then(res => res.json())
        .catch(this.handleError)
    }

  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<AccountCategory> {
        let pagedData = new PagedData<AccountCategory>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new AccountCategory(jsonObj.accountCategoryId,jsonObj.accountCategoryDescription,jsonObj.accountCategoryDescriptionArabic,jsonObj.preDefined);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}