/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { PaymentTermSetup } from '../../../financialModule/_models/company-setup/payment-terms-setup';


@Injectable()
export class PaymentTermSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchPaymentTermSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchPaymentTermSetup';
    private getPaymentTermSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/company/paymentTermSetupGetById';
    private updatePaymentTermSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/updatePaymentTermSetup';
    private savePaymentTermSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/paymentTermSetup';
    private getDiscountTypePeriodUrl = Constants.financialModuleApiBaseUrl + 'getDiscountPeriodTypes';
    private getDiscountTypesUrl = Constants.financialModuleApiBaseUrl + 'getDiscountTypes';
    private getDueTypesUrl = Constants.financialModuleApiBaseUrl + 'getDueTypes';
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
    getDiscountTypePeriod()
    {
         return this.http.get(this.getDiscountTypePeriodUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     getDiscountType()
    {
         return this.http.get(this.getDiscountTypesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     getDueType()
    {
         return this.http.get(this.getDueTypesUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Search payment Terms
    searchPaymentTermSetup(page:Page, searchKeyword):Observable<PagedData<PaymentTermSetup>>{
        return this.http.post(this.searchPaymentTermSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of  payment Terms By ID
    getPaymentTermSetupById(paymentTermId: string) {
        return this.http.post(this.getPaymentTermSetupByIdUrl, { paymentTermId: paymentTermId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update rolegroup for edit 
    updatePaymentTermSetup(PaymentTermSetup:PaymentTermSetup){
        return this.http.post(this.updatePaymentTermSetupUrl,JSON.stringify(PaymentTermSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create comapny setup
    createPaymentTermSetup(PaymentTermSetup:PaymentTermSetup)
    {
      return this.http.post(this.savePaymentTermSetupUrl,JSON.stringify(PaymentTermSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<PaymentTermSetup> {
        let pagedData = new PagedData<PaymentTermSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new PaymentTermSetup(jsonObj.paymentTermId,jsonObj.description,jsonObj.arabicDescription,jsonObj.discountPeriod, jsonObj.discountType, jsonObj.discountTypeValue, jsonObj.discountPeriodDays, jsonObj.dueDays, jsonObj.dueType, jsonObj.dueTypeValue);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}