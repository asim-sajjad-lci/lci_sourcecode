/**
 * A service class for shipping method set up
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { ShippingMethodSetup } from '../../../financialModule/_models/company-setup/shipping-method-setup';


@Injectable()
export class ShippingMethodSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchShipmentMethodSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchShipmentMethodSetup';
    private getShipmentMethodSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getShipmentMethodSetupById';
    private updateShipmentMethodSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/updateShipmentMethodSetup';
    private saveshipmentMethodSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/shipmentMethodSetup';
    private getShipmentMethodTypeStatusUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getShipmentMethodTypeStatus';
    
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
    getShipmentMethodTypeStatus()
    {
         return this.http.post(this.getShipmentMethodTypeStatusUrl,{},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

   //Search payment Terms
    searchShipmentMethodSetup(page:Page, searchKeyword):Observable<PagedData<ShippingMethodSetup>>{
        return this.http.post(this.searchShipmentMethodSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of  shipping method detail By shipmentMethodId
    getShipmentMethodSetupById(shipmentMethodId: string) {
        return this.http.post(this.getShipmentMethodSetupByIdUrl, { shipmentMethodId: shipmentMethodId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update shippingMethodSetup for edit 
    updateShipmentMethodSetup(ShippingMethodSetup:ShippingMethodSetup){
        return this.http.post(this.updateShipmentMethodSetupUrl,JSON.stringify(ShippingMethodSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //addede new deatils of shippingmethodsetup
    createShippingMethodSetup(ShippingMethodSetup:ShippingMethodSetup)
    {
      return this.http.post(this.saveshipmentMethodSetupUrl,JSON.stringify(ShippingMethodSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<ShippingMethodSetup> {
        let pagedData = new PagedData<ShippingMethodSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new ShippingMethodSetup(jsonObj.shipmentMethodId,jsonObj.shipmentDescription,jsonObj.shipmentDescriptionArabic,jsonObj.shipmentCarrierAccount, jsonObj.shipmentCarrier,jsonObj.shipmentCarrierArabic,
                 jsonObj.shipmentCarrierContactName, jsonObj.shipmentPhoneNumber, jsonObj.shipmentMethodType);
                pagedData.data.push(group);
            }

            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}