/**
 * A service class for VAT
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { VatDetailSetup } from '../../../financialModule/_models/company-setup/vat-detail-setup';


@Injectable()
export class VatDetailSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchVatDetailSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchVatDetailSetup';
    private getVatDetailSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getVatDetailSetupById';
    private updateVatDetailSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/updateVatDetailSetup';
    private saveVatDetailSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/vatDetailSetup';
    private getSeriesTypeUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getVatSeriesTypeStatus';
    private getSalePurchaseAmountByAccountNumberUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getSalePurchaseAmountByAccountNumber';
    // private getDiscountTypesUrl = Constants.financialModuleApiBaseUrl + 'getDiscountTypes';
    // private getDueTypesUrl = Constants.financialModuleApiBaseUrl + 'getDueTypes';
    private getBasedOnListUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getVatBasedOnTypeStatus';
    private getGlAccountNumberListUrl=Constants.financialModuleApiBaseUrl + 'getGlAccountNumberList';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

  
    getSeriesType()
    {
         return this.http.post(this.getSeriesTypeUrl,{},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getBasedOnList()
    {
         return this.http.post(this.getBasedOnListUrl,{},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //  getDiscountType()
    // {
    //      return this.http.get(this.getDiscountTypesUrl, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    //  getDueType()
    // {
    //      return this.http.get(this.getDueTypesUrl, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    //Search VAT
    searchVatDetailSetup(page:Page, searchKeyword):Observable<PagedData<VatDetailSetup>>{
        return this.http.post(this.searchVatDetailSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }


     //Get All VAT List
    getVatDetailSetup(){
        return this.http.post(this.searchVatDetailSetupUrl,{ 'searchKeyword': '' }, {headers:this.headers})
             .map(data => data.json().result);
    }

    //getGlAccountNumberList
      getGlAccountNumberList()
    {
         return this.http.get(this.getGlAccountNumberListUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //getting details of VAT By ID
    getVatDetailSetupById(vatScheduleId: string) {
        return this.http.post(this.getVatDetailSetupByIdUrl, { vatScheduleId: vatScheduleId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update VAT for edit 
    updateVatDetailSetup(VatDetailSetup:VatDetailSetup){
        return this.http.post(this.updateVatDetailSetupUrl,JSON.stringify(VatDetailSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create VAT
    createVatDetailSetup(VatDetailSetup:VatDetailSetup)
    {
      return this.http.post(this.saveVatDetailSetupUrl,JSON.stringify(VatDetailSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }

    //get Sale/Purchase Amount By AccountNumber Url
    getSalePurchaseAmountByAccountNumber(accountTableRowIndex: string) {
        return this.http.post(this.getSalePurchaseAmountByAccountNumberUrl,{accountTableRowIndex: accountTableRowIndex},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<VatDetailSetup> {
        let pagedData = new PagedData<VatDetailSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new VatDetailSetup(jsonObj.vatScheduleId,jsonObj.vatDescription,jsonObj.vatDescriptionArabic,jsonObj.vatSeriesType, jsonObj.vatIdNumber, jsonObj.vatBaseOn,
                 jsonObj.basperct, jsonObj.maximumVATAmount, jsonObj.minimumVATAmount,jsonObj.accountRowId, jsonObj.lastYearTotalSalesPurchase
                 ,jsonObj.lastYearSalesPurchaseTaxes,jsonObj.lastYearTaxableSalesPurchase, jsonObj.ytdTotalSalesPurchase, jsonObj.ytdTotalSalesPurchaseTaxes, jsonObj.ytdTotalTaxableSalesPurchase);

       pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}