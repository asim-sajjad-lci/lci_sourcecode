/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { Configuration } from '../../../financialModule/_models/company-setup/configuration';


@Injectable()
export class ConfigurationService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchConfigurationUrl = Constants.financialModuleApiBaseUrl + 'setup/company/get/configure/module';
    private getConfigurationByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getOne/configure/module';
    private updateConfigurationUrl = Constants.financialModuleApiBaseUrl + 'setup/company/configure/module';
    private saveConfigurationUrl = Constants.financialModuleApiBaseUrl + 'setup/company/configure/module';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        
    }

    //Search payment Terms
    searchConfiguration(page:Page, searchKeyword):Observable<PagedData<Configuration>>{
        return this.http.post(this.searchConfigurationUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, 
        {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

     //getting details of  payment Terms By ID
    getConfigurationById(seriesId: string) {
        return this.http.post(this.getConfigurationByIdUrl, { seriesId: seriesId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //update rolegroup for edit 
    updateConfiguration(Configuration:Configuration){
        return this.http.post(this.updateConfigurationUrl,JSON.stringify(Configuration),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //create comapny setup
    createConfiguration(Configuration:Configuration)
    {
      return this.http.post(this.saveConfigurationUrl,JSON.stringify(Configuration),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<Configuration> {
        let pagedData = new PagedData<Configuration>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new Configuration(jsonObj.seriesId,jsonObj.seriesNamePrimary,jsonObj.seriesNameSecondary,jsonObj.originNamePrimary,jsonObj.originNamePrimary);
                pagedData.data.push(group);
            }
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}