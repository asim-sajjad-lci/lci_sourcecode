/**
 * A service class for dashboard
 */
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { CreditCardSetup } from '../../../financialModule/_models/company-setup/credit-card-setup';


@Injectable()
export class CreditCardSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private searchCreditCardSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/searchCreditCardSetup';
    private getCreditCardDrtailsSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getCreditCardSetupById';
    private updateCreditCardSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/updateCreditCardSetup';
    private saveCreditCardSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/company/saveCreditCardSetup';
    private getCreditCardTypeStatusUrl = Constants.financialModuleApiBaseUrl + 'setup/company/getCreditCardTypeStatus';
    private getGlAccountNumberListUrl=Constants.financialModuleApiBaseUrl + 'getGlAccountNumberList';
    private getcheckBookIdListUrl = Constants.financialModuleApiBaseUrl + 'setup/generalLedger/searchActiveCheckBookMaintenance';
    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }



    //Search credit card detials
    searchCreditCardSetup(page:Page, searchKeyword):Observable<PagedData<CreditCardSetup>>{
        return this.http.post(this.searchCreditCardSetupUrl,{ 'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
             .map(data => this.getPagedData(page,data.json().result));
    }

    getcheckBookIdList(){
         return this.http.post(this.getcheckBookIdListUrl, { 'searchKeyword': '' }, {headers:this.headers})
                 .toPromise()
                .then(res => res.json())
                .catch(this.handleError);
    }

     getCreditCardTypeStatus()
    {
         return this.http.post(this.getCreditCardTypeStatusUrl,{},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

     //getting details of  credit card details By ID
    getCreditCardDrtailsSetupById(cardId: string) {
        return this.http.post(this.getCreditCardDrtailsSetupByIdUrl, { cardId: cardId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //getGlAccountNumberList
    getGlAccountNumberList()
    {
         return this.http.get(this.getGlAccountNumberListUrl,{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update credit card details for edit 
    updateCreditCardSetup(CreditCardSetup:CreditCardSetup){
        return this.http.post(this.updateCreditCardSetupUrl,JSON.stringify(CreditCardSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
      //save credit card setup
    createCreditCardSetup(CreditCardSetup:CreditCardSetup)
    {
      return this.http.post(this.saveCreditCardSetupUrl,JSON.stringify(CreditCardSetup),{headers:this.headers})
        .toPromise().then(data => data.json())
        .catch(this.handleError)
    }
  //pagination of data
    private getPagedData(page: Page, data: any): PagedData<CreditCardSetup> {
        let pagedData = new PagedData<CreditCardSetup>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let group = new CreditCardSetup(jsonObj.checkBookId,jsonObj.creditCardNameArabic,jsonObj.creditCardName,jsonObj.cardType, jsonObj.cardId, jsonObj.accountTableRowIndex,jsonObj.accountNumber);
                pagedData.data.push(group);
            }
            
            pagedData.page = page;
         }
        return pagedData;
    }
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}