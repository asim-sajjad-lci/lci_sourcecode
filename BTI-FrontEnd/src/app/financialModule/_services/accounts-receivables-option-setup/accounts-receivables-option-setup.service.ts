import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';

import { AccountReceivableOptionSetup } from '../../../financialModule/_models/accounts-receivables-setup/accounts-receivable-option-setup';


@Injectable()
export class AccountReceivableOptionService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private saveAccountRecievableSetupOptionsUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/saveAccountRecievableSetupOptions';
    private updateAccountRecievableSetupOptionsUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/updateAccountRecievableSetupOptions';
    private getAccountRecievableOptionSetupUrl = Constants.financialModuleApiBaseUrl + 'setup/accounts/getRecievableSetupOptions';

    currentLanguage = "";

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    getAccountRecievableOptionSetup() {
        return this.http.post(this.getAccountRecievableOptionSetupUrl, { }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
  
    //Save AccountReceivableOption
    saveAccountRecievableSetup(AccountReceivableOptionSetup:AccountReceivableOptionSetup) {
        return this.http.post(this.saveAccountRecievableSetupOptionsUrl,JSON.stringify(AccountReceivableOptionSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
     //Update AccountReceivableOption
    updateAccountRecievableSetupOptions(AccountReceivableOptionSetup:AccountReceivableOptionSetup) {
        return this.http.post(this.updateAccountRecievableSetupOptionsUrl,JSON.stringify(AccountReceivableOptionSetup), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   
    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
    
}