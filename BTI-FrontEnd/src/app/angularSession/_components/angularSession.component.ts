import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
import { RetirementSetup } from '../../angularSession/_models/RetirementSetup';
import { AngularSessionService } from '../../angularSession/_services/angularSession.service';

@Component({
    templateUrl: './angularSession.component.html',
    providers:[AngularSessionService]
})

export class AngularSessionComponent {
    moduleName:string;
    isModify:boolean = false;
    hasMsg:boolean = false;
    messageText:string;
    isSuccessMsg:boolean = false;
    isfailureMsg:boolean = false;
    model: any = {};
    lstRetirementSetup: any = [];
    showCancel:boolean=false;
    test:string;
    arrCountry:any;
    constructor(private router: Router,private route: ActivatedRoute,
    private angularSessionService: AngularSessionService)
    {
    
    }

    ngOnInit() {
       this.moduleName='Angular Session';
       this.test="Testing";
       this.GetRetirementSetupList();
    }

     // Get All RetirementSetup List
     GetRetirementSetupList()
     {
        this.lstRetirementSetup =[];
        this.angularSessionService.GetRetirementSetup().then(data => {
            this.lstRetirementSetup = data.result.records; 
        });
     }

     getCountry()
     {
        this.angularSessionService.getCountryList().then(data => {
            this.arrCountry = data.result;
        });
     }

    // Save Update Retirement Setup
    SaveUpdateRetirementSetup(f: NgForm)
    {
        this.angularSessionService.SaveUpdateRetirementSetup(this.model,this.isModify).then(data => {
            window.scrollTo(0,0);
            this.hasMsg = true;
            this.messageText = data.btiMessage.message;
            if (data.btiMessage.messageShort == 'FA_RETIREMENT_SETUP_ALREADY_EXISTS') {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            }
            else{
                this.isModify=false;
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.GetRetirementSetupList();
                f.resetForm();
            }
            window.setTimeout(() => {
               
                this.hasMsg = false;
                this.showCancel=false;
            }, 4000);
           
         }).catch(error => {
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.hasMsg = true;
                this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
         });
    }


     // get RetirementSetup By Id
     GetRetirementSetupById(faRetirementId: string)
     {
         this.isModify=true;
         this.showCancel=true;
         this.angularSessionService.GetRetirementSetupById(faRetirementId).then(data => {
             this.model = data.result; 
          });
     }

    // Reset Form
     Reset()
     {
        if(!this.isModify)
        {
            this.model.faRetirementId='';
        }
        this.model.descriptionPrimary= '';
        this.model.descriptionSecondary= '';
     }

    // Cancel Form 
     Cancel(f: NgForm)
     {
        this.isModify=false;
        this.showCancel=false;
        f.resetForm();
     }
}