
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Constants } from '../../_sharedresource/Constants';
import { RetirementSetup } from '../../angularSession/_models/RetirementSetup';

@Injectable()
export class AngularSessionService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getCountryListUrl = Constants.userModuleApiBaseUrl + 'getCountryList';
    private getRetirementSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup/search';
    private getRetirementSetupByIdUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup/getById';
    private updateRetirementSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup/update';
    private saveRetirementSetupUrl = Constants.financialModuleApiBaseUrl + 'fixed/assets/retirementSetup';
     
    currentLanguage:string;
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        this.currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", this.currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //Get Retirement setup detials
    GetRetirementSetup(){
        
        return this.http.post(this.getRetirementSetupUrl,{'searchKeyword': '' }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
    }

    //Getting Retirement setup details By ID
    GetRetirementSetupById(faRetirementId: string) {
    
        return this.http.post(this.getRetirementSetupByIdUrl,{faRetirementId: faRetirementId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // Save Update Retirement Setup
    SaveUpdateRetirementSetup(RetirementSetup:RetirementSetup,isModify:boolean)
    {
        
        if(isModify)
        {
            return this.http.post(this.updateRetirementSetupUrl,JSON.stringify(RetirementSetup),{headers : this.headers})
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
        }
        else{
            return this.http.post(this.saveRetirementSetupUrl,JSON.stringify(RetirementSetup),{headers:this.headers})
            .toPromise().then(data => data.json())
            .catch(this.handleError)
        }

    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

}