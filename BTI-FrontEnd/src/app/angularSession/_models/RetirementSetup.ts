/**
 * A model for Retirement setup
 */
export class RetirementSetup {
    faRetirementId: string;
    descriptionPrimary:string;
    descriptionSecondary:string;
    temparr:any={};
 
  //initializing Retirement setup parameters
    constructor (faRetirementId: string, descriptionPrimary:string,descriptionSecondary:string,temparr:{})
    { 
        this.faRetirementId= faRetirementId;
        this.descriptionPrimary= descriptionPrimary;
        this.descriptionSecondary= descriptionSecondary;
        this.temparr=temparr;
    }
}