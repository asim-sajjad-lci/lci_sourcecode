import { Component } from '@angular/core';
import {Observable} from "rxjs";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../_sharedresource/Constants';
import {HCMSidebarComponent} from '../hcm/_components/hcm-sidebar.component';

@Component({
  selector: 'main-sidebar',
  templateUrl: './main-sidebar.component.html'
})
export class MainSidebarComponent {
  isActive: number;
  moduleCode =Constants.financialModuleCode;
  screenCode = "S-1004";
  screenName;
  moduleId = "";
  defaultFormValues: Array<Object>;
  availableFormValues: [object];
  sidebarValues: Array<Object>;
  val:Array<Object>;
  oldLinkId:string='';
  currentLanguage:string;
  currentUrl:string;
  loggedInUserRole:string;
  oldPath:string;
  screenAccess=true;
  // location: Location;


  constructor(
     private router: Router,
     private getScreenDetailService:GetScreenDetailService,
    ) { 
     
     var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";

    this.currentLanguage=currentLanguage;

    var userData = JSON.parse(localStorage.getItem('currentUser'));
    if(userData != null){
      this.loggedInUserRole=userData.role;
    }

    this.getScreenDetailService
          .getCurrentModule()
          .subscribe(sidebarValues=>{ 
            // alert('sidebarValues');
            
            if(typeof sidebarValues == 'string'){
              this.sidebarValues = [];
            } else {
              this.sidebarValues = sidebarValues;
            }
            // alert("this.sidebarValues: " + this.sidebarValues['moduleId']);
            if (this.sidebarValues['moduleId'] == '2') {
              // location.href = '/hcm/hcmMenu';
              // alert(location.href);
              // this.router.navigate(["/hcm/hcmMenu"]);
            }

           
    });
    
    this.defaultFormValues=[
      {'fieldName':'MANAGE_COMPANY', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'MANAGE_USER', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'MANAGE_USER_GROUPS', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'MANAGE_ROLES', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'AUTH_SETTINGS', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'SALES_NOTES', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'SETTINGS', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'ITEM', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'MANAGE_ROLE_GROUP', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'WHITE_LIST_IP', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'LEFT_MENU_DASHBOARD', 'fieldValue':'', 'helpMessage':''},
    ];
  }

  

  ngOnInit() {
     
   
         this.currentUrl = this.router.url.replace('/',''); 
        
    // getting screen
    this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
      if(data.status !='BAD_REQUEST')
      {
         this.availableFormValues = data.result.dtoScreenDetail.fieldList;
        for(var j=0; j<this.availableFormValues.length; j++)
        {
            var fieldKey = this.availableFormValues[j]['fieldName'];
            var objAvailable = this.availableFormValues.find(x=>x['fieldName']===fieldKey);
            var objDefault = this.defaultFormValues.find(x=>x['fieldName']===fieldKey);
                objDefault['fieldValue']=objAvailable['fieldValue'];
                objDefault['helpMessage']=objAvailable['helpMessage'];
        }
      }
      else{
       // alert(data.btiMessage.message);
       // return false;
        this.screenAccess=false;
      }
    });

     //getting initial sidebar menu based on first header
       this.getScreenDetailService.getHeaderDetail().subscribe(data =>
        {
          
          if(data.result)
          {
            this.moduleId = data.result[0].moduleId;
           
           
            //Bind side bar with  selected module module
            var selectedModule=localStorage.getItem('currentModule');
            if(selectedModule!='')
            {
              this.getScreenDetailService.setCurrentModule(selectedModule);
            }
            else{
           this.getScreenDetailService.setCurrentModule(this.moduleId);
          }
            
            
          }
        });
    
    // setting active language
    var currentLanguage = localStorage.getItem('currentLanguage');
    if (currentLanguage == "1") {
      this.isActive = 1;
    }
    else if (currentLanguage == "2") {
      this.isActive = 2;
    }
    else {
      this.isActive = 1;
    }
  }

 redirectTo(RefUrl)
 { 
   var oldlinkId =  <HTMLAnchorElement>document.getElementById(this.oldLinkId);
   var currentUrl =  <HTMLAnchorElement>document.getElementById(this.currentUrl);

   if(oldlinkId)
   {
     oldlinkId.removeAttribute("class");
   }
   
   if(currentUrl)
   {
     currentUrl.removeAttribute("class");
   }
  
   var linkId =  <HTMLAnchorElement>document.getElementById(RefUrl);
   linkId.className="active";
   this.oldLinkId=RefUrl;
   this.router.navigate([RefUrl]);
 }
  redirectMe(screenID,RefUrl)
  {
    if(RefUrl == 0)
    {
      return false;
    }
    else
    {
      
      this.getScreenDetailService.getScreenDetailUser(this.moduleCode, screenID).then(data => {
   
         if(data.status !='BAD_REQUEST')
         {
          var oldlinkId =  <HTMLAnchorElement>document.getElementById( this.oldLinkId);
          if(oldlinkId)
          {
            oldlinkId.removeAttribute("class");
          }
          var linkId =  <HTMLAnchorElement>document.getElementById(screenID);
          linkId.className="active";
          this.oldLinkId=screenID;
          this.router.navigate([RefUrl]);
         }
         else{
          alert(data.btiMessage.message);
          return false;
        }
          
      });
  
    }
  }


    setPath(path){
      this.oldPath = path
    }
    getClass(path) {

    if(this.oldPath === path) {
      return "active";
    } else {
      return "";
    }
  }

}