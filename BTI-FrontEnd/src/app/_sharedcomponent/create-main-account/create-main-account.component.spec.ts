import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMainAccountComponent } from './create-main-account.component';

describe('CreateMainAccountComponent', () => {
  let component: CreateMainAccountComponent;
  let fixture: ComponentFixture<CreateMainAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMainAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMainAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
