import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AccountStructureService } from '../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'create-main-account',
  templateUrl: './create-main-account.component.html',
  styleUrls: ['./create-main-account.component.css'],
  providers:[AccountStructureService]
})
export class CreateMainAccountComponent implements OnInit {

  @ViewChild('myInput') segmentNumber_1: ElementRef;

  accountNumberIndeies:number[];
  accountDescription:string[];
  accountDescriptionArabic:string[];

  segmentNumbers;

  DimensionsFieldsStatus:boolean=true;

  constructor(private accountStructureService:AccountStructureService,public toastr: ToastsManager ) { }

  ngOnInit() {
    //this.getSegmentCount();
  }

  /* getSegmentCount()
	{
		 this.accountStructureService.getAccountStructureSetup().then(data => {
      if(data.code == 302){
        this.segmentNumbers = [];
        for(let i = 0 ; i<data.result.segmentNumber;i++ ){
          this.segmentNumbers.push("");
        }
      }
		 });
  }
  
  fetchIndexInformation(i,segmentNumber){
    console.log(this.segmentNumbers);
    //Main Account Case
    if(i == 0){
      //this.accountStructureService.firstTextApi()
      this.validateMainAccount(segmentNumber,i);
    }else{ // Dimension Case

    }
  }
  resetFields(){
    for(let i = 0 ; i < this.segmentNumbers.length;i++){
      this.segmentNumbers[i] = "";
    }
    this.DimensionsFieldsStatus = true;
  }

  // to track change for primitive value type in array of segment Number
  trackByFn(index: any, item: any) {
    return index;
 }

 validateMainAccount(mainAccountNumber,fieldIndex){
  this.accountStructureService.firstTextApi(mainAccountNumber).subscribe(data => {
    if(data.status == "FOUND"){
      this.toastr.success(data.btiMessage.message);
      this.DimensionsFieldsStatus = false;
      let nextFieldIndex = fieldIndex+1;
      console.log('segmentNumber_'+nextFieldIndex);
      console.log(document.getElementById('segmentNumber_'+nextFieldIndex));
      document.getElementById('segmentNumber_'+1).focus();
      this.segmentNumber_1.nativeElement.focus();
    }
  })
 } */

}
