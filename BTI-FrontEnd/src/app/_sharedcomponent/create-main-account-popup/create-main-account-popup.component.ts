import { Component, OnInit, Output } from '@angular/core';
import { MainAccountSetup } from '../../financialModule/_models/master-data/mainAccountSetup';
import { AccountStructureService } from '../../financialModule/_services/general-ledger-setup/accounts-structure-setup.service';
import { stringify } from 'querystring';
import { ToastsManager } from 'ng2-toastr';
import { FormControl } from '@angular/forms';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-create-main-account-popup',
  templateUrl: './create-main-account-popup.component.html',
  styleUrls: ['./create-main-account-popup.component.css']
})
export class CreateMainAccountPopupComponent implements OnInit {

  @Output() refreshAccountNumber = new EventEmitter();

  mainAccountList: Array<any> = [{ 'id': '0', 'text': '--Select--' }];
  activeMainAccountNumber: Array<any> = [{ 'id': '0', 'text': '--Select--' }];

  flag = true;
  isLTR: boolean ;
  dimentionsSetup: Array<String> = [];

  dimentionsListsValues = [];

  // variable for creating new account number
  accountNumber = [];
  accountNumberIndex = [];
  arrAccountDescription: string[] = [];
  arrAccountDescriptionArabic: string[] = [];

  accountAliase: string = '';
  accountAliaseArabic: string = '';


  constructor(
    private accountStractureService: AccountStructureService,
    public toastr: ToastsManager
  ) { 
    const languageOrientation = localStorage.getItem('languageOrientation');
    if(languageOrientation === 'LTR'){
      this.isLTR = true;
    }else{
      this.isLTR = false;
    }
  }

  ngOnInit() {
    this.getMainAccountList();
    this.getFinancialDimentionsList();
  }

  // Get Lists Information
  // 1- main account
  getMainAccountList() {
    this.mainAccountList = [{ 'id': '0', 'text': '--Select--' }];
    this.accountStractureService.getMainAccountList().then(data => {
      this.mainAccountList = [{ 'id': '0', 'text': '--Select--' }];
      this.activeMainAccountNumber = [{ 'id': '0', 'text': '--Select--' }];
      this.accountNumber[0] = '';
      this.accountNumberIndex[0] = 0;
      this.arrAccountDescription[0] = '';
      this.arrAccountDescriptionArabic[0] = '';
      for (let i = 0; i < data.result.length; i++) {
        this.mainAccountList.push({ 'id': data.result[i].mainAccountNumber + '', 'text': data.result[i].mainAccountNumber + ' : ' + data.result[i].mainAccountDescription });
      }
    });
  }

  // 2- Dimentions Setup (Labels)
  getFinancialDimentionsList() {
    this.accountStractureService.getAccountStructureSetup().then(setup => {
      if (setup) {

        const dimenionList = setup.result.dimensionList

        for (let i = 0; i < dimenionList.length; i++) {
          let dimensionLabel;
          this.accountStractureService.getFinancialDimensionsList().then(data => {
            dimensionLabel = data.result.find(x => x.dimInxd === dimenionList[i].coaFinancialDimensionsIndexId);
            this.dimentionsSetup.push(dimensionLabel.dimensionName);
            this.accountNumber[i+1] = '';
            this.accountNumberIndex[i+1] = 0;
            this.arrAccountDescription[i+1] = '';
            this.arrAccountDescriptionArabic[i+1] = '';
            this.getDimentionsValue(dimensionLabel.dimInxd, i);
          });
        }
      }
    });
  }

  // 3- Dimentions Values (Dropdown List)
  getDimentionsValue(dimentionIndex: number, index: number) {
    this.accountStractureService.getFinancialDimensionsValuesByFinancialDimensionId('' + dimentionIndex).then(data => {
      console.log(data);
      let dimentionValue = new Array();
      for (let i = 0; i < data.result.length; i++) {
        dimentionValue.push({ 'id': data.result[i].dimensionValue + '', 'text': data.result[i].dimensionValue + ' : ' + data.result[i].dimensionDescription });

      }
      this.dimentionsListsValues[index] = (dimentionValue);
    });
    console.log(this.dimentionsListsValues);
  }

  // select data event 
  // 1- check main account Number
  checkMainAccountNumber(mainAccountSelection) {
    console.log(mainAccountSelection);
    const mainAccountNumber = mainAccountSelection.id;
    this.accountStractureService.firstTextApi(mainAccountNumber).subscribe(pagedData => {
      let status = pagedData.status;
      let result = pagedData.result;
      if (status == "FOUND") {
        this.accountNumber[0] = result.mainAccountNumber;
        this.accountNumberIndex[0] = result.actIndx.toString();
        this.arrAccountDescription[0] = result.mainAccountDescription;
        this.arrAccountDescriptionArabic[0] = result.mainAccountDescriptionArabic;
        this.activeMainAccountNumber = [mainAccountSelection];
      } else {
        this.activeMainAccountNumber = [{ 'id': '0', 'text': '--Select--' }];
        this.accountNumber[0] = '';
        this.accountNumberIndex[0] = '';
        this.arrAccountDescription[0] = '';
        this.arrAccountDescriptionArabic[0] = '';
        this.activeMainAccountNumber = [mainAccountSelection];
        this.toastr.warning(pagedData.btiMessage.message);
      }
    });
  }

  checkDimentionsValue(index, value) {
    console.log(value.id);
    console.log(index);
    this.accountStractureService.restTextApi(value.id, index + 1).subscribe(pagedData => {
      let status = pagedData.status;
      let result = pagedData.result;
      if (status == "FOUND") {
        this.accountNumber[index + 1] = result.dimensionValue;
        this.accountNumberIndex[index + 1] = result.dimInxValue.toString();
        this.arrAccountDescription[index + 1] = result.dimensionDescription;
        this.arrAccountDescriptionArabic[index + 1] = result.dimensionDescriptionArabic;
      } else {
        this.accountNumber[index + 1] = '';
        this.accountNumberIndex[index + 1] = '';
        this.arrAccountDescription[index + 1] = '';
        this.arrAccountDescriptionArabic[index + 1] = '';
        this.toastr.warning(pagedData.btiMessage.message);
      }
    });
    console.log(this.accountNumber);
    console.log(this.accountNumberIndex);
    console.log(this.arrAccountDescription);
    console.log(this.arrAccountDescriptionArabic);
  }

  // create Main Account 
  createNewGlAccountNumber(field1: FormControl, field2: FormControl) {
    if (this.activeMainAccountNumber && this.activeMainAccountNumber[0].id != '0' && this.accountAliase != '' && this.accountAliaseArabic != '') {
      var tempAccountNumberList = [];
      var accountDesc = this.arrAccountDescription.join("-");
      var accounDescArabic = this.arrAccountDescriptionArabic.join("-");
      var accounNumber = this.accountNumber.join(" - ");
      console.log(this.accountNumber);
      tempAccountNumberList.push({ 'accountNumber': accounNumber, 'accountNumberIndex': this.accountNumberIndex, 'accountDescription': accountDesc, 'accountDescriptionArabic': accounDescArabic })
      this.accountStractureService.createNewGlAccountNumber(tempAccountNumberList).then(data => {

        if (data.btiMessage.messageShort == 'GL_ACCOUNT_NUMBER_CREATED') {
          this.flag = false;
          setTimeout(() => { this.flag = true; })
          this.toastr.success(data.btiMessage.message);
          this.activeMainAccountNumber = [{ 'id': '0', 'text': '--Select--' }];
          for(let i = 0; i<= this.dimentionsSetup.length; i++){
            this.accountNumber[i] = '';
            this.accountNumberIndex[i] = 0 ;
            this.arrAccountDescription[i] = '';
            this.arrAccountDescriptionArabic[i] = '';
          }
          this.accountAliase = '';
          this.accountAliaseArabic = '';
          field1.reset();
          field2.reset();
          this.refreshAccountNumber.emit('refresh');
        }
        else {
          this.toastr.warning(data.btiMessage.message);
        }
      });
    }else if(this.activeMainAccountNumber[0].id == '0'){
      this.toastr.warning('Please! Select Main Account ');
    }else if(this.accountAliase == '' || this.accountAliaseArabic == ''){
      this.toastr.warning('Please! Define An Aliase For Account Number ');
    }


  }

}
