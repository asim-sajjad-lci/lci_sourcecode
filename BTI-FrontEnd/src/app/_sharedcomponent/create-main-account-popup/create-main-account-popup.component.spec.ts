import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMainAccountPopupComponent } from './create-main-account-popup.component';

describe('CreateMainAccountPopupComponent', () => {
  let component: CreateMainAccountPopupComponent;
  let fixture: ComponentFixture<CreateMainAccountPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMainAccountPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMainAccountPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
