import {Pipe, PipeTransform} from '@angular/core';
import { CurrencyPipe } from '@angular/common';

const _NUMBER_FORMAT_REGEXP = /^(\d+)?\.((\d+)(-(\d+))?)?$/;

@Pipe({
    name: 'looseCurrency'
})

@Pipe({name: 'looseCurrency'})
export class LooseCurrencyPipe implements PipeTransform {
  // constructor(private _currencyPipe: CurrencyPipe) {}

  // _currencyPipe: CurrencyPipe;

  transform(value: any, currencyCode: string, symbolDisplay: boolean, digits: string): string {
    // this._currencyPipe = new CurrencyPipe();

    // let type = typeof value;
    // alert ('type 1: ' + type);

    if (typeof value === 'string'){
      value = +value;
    }
    // let type2 = typeof value;
    // alert ('type 2: ' + type2);

    if (typeof value === 'number'|| _NUMBER_FORMAT_REGEXP.test(value)){
      return this.format2(value, 'SAR'); //'SAR ' + value;
    } else {
      return value;
    }

    // if (typeof value === 'number' || _NUMBER_FORMAT_REGEXP.test(value)) {
      // return this._currencyPipe.transform(value, currencyCode, symbolDisplay, digits);
      // return (new CurrencyPipe('en_US')).transform(value, currencyCode, symbolDisplay, digits);
    // } 
  }

 format2(n, currency) {
    return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }  
}