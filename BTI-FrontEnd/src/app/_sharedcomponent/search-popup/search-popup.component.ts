import { Component, OnInit, Output, ViewChild, ElementRef } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { CommonService } from '../../_sharedresource/_services/common-services.service';
import { Page } from '../../_sharedresource/page';

@Component({
  selector: 'app-search-popup',
  templateUrl: './search-popup.component.html',
  styleUrls: ['./search-popup.component.css']
})
export class SearchPopupComponent implements OnInit {

  @Output() messageEvent = new EventEmitter();

  @ViewChild('closeBtn') closeBtn: ElementRef;


  page = new Page();
  rows = new Array<any>();
  accountSearchWord= '';


  selectedAccountNumber: string;
  data = new Array();

  columns = [
    { name: 'Account Number' },
    { name: 'Account Description' },
    { name: 'Account Description Arabic' }
  ];
  constructor(private commonServices: CommonService) { 

   }

  ngOnInit() {
    this.getAccountNumbersData();
    this.resetSearchPopup();
  }

  getAccountNumbersData(){
    this.commonServices.getGlAccountNumberListNew().then(data => {
      console.log(data);
      if (data.code === 200) {
        this.data = data.result;
        this.page.totalElements = data.result.length;
      }

    });
   }

  resetSearchPopup(){
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.pageNumber = 0;
    this.accountSearchWord = '';
    
  }

  //unposted JournalEntry
  setPageUnPosted(pageInfo) {
    
    this.page.pageNumber = pageInfo.offset;
    console.log(this.page);
    this.commonServices.searchGlAccountNumberListNew(this.page, this.accountSearchWord).then(pagedData => {
      console.log(pagedData);
      this.page.pageNumber = pagedData.result.pageNumber;
      this.page.totalElements = pagedData.result.totalCount;
      this.page.size = pagedData.result.pageSize;
      this.data = pagedData.result.records;
      console.log(this.page);
    });
  }

  pagination(event){
    console.log(event);
    this.page.pageNumber = event.offset;
    console.log(this.page)
  }

  saerchAccountNumber(event) {
     
    this.commonServices.searchGlAccountNumberListNew(this.page,event.target.value.toLowerCase()).then(pagedData => {
      console.log(pagedData);
      this.page.pageNumber = pagedData.result.pageNumber;
      this.page.totalElements = pagedData.result.totalCount;
      this.page.size = pagedData.result.pageSize;
      this.data = pagedData.result.records;
      console.log(this.data)
      console.log(this.page);
    });
  }

  func1(event) {
    this.selectedAccountNumber = event.row.accountTableRowIndex;
  }

  func2(event) {
    let selectedItem = this.data.find(x => (x.accountTableRowIndex == this.selectedAccountNumber));
    if(selectedItem){
    this.messageEvent.emit(selectedItem);
    this.closeBtn.nativeElement.click();
    this.selectedAccountNumber = '';
      this.resetSearchPopup();
      this.getAccountNumbersData();
    }
  }



  rowClass = (row) => {
    return {
      'some-class': (this.selectedAccountNumber == row.accountTableRowIndex) 
    };
  }

  ok(){
    this.func2(event);
  }
  
  

}
