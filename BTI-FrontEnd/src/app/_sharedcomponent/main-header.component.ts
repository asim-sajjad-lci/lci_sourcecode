import { Component, Inject  } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { SettingsService } from '../userModule/_services/settings/settings.service';
import { LanguageSetupService } from '../userModule/_services/languageSetup/language-setup.service';
import { GetScreenDetailService } from '../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../_sharedresource/_services/common-services.service';
import {Constants} from '../_sharedresource/Constants';
import { CompanyService } from '../userModule/_services/companymanagement/company.service';


@Component({
  selector: 'main-header',
  templateUrl: './main-header.component.html',
  providers: [SettingsService,LanguageSetupService,CommonService]
})
export class MainHeaderComponent {
  isActive: number;
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1003";
  screenName;
  defaultFormValues: [object];
  availableFormValues: [object];
  headerValues : [object];
  currentLanguage : string;
  loggedInUserRole:string;
  arrLanguage:[object];
  forbiddenMsg=Constants.ForbiddenMsg;
  Close=Constants.close;
  constructor(
    private router: Router,
    private getScreenDetailService:GetScreenDetailService,
    private settingsService: SettingsService,
    private languageSetupService:LanguageSetupService,
    private companyService:CompanyService,
    private commonService: CommonService,
    @Inject(DOCUMENT) private document: any
  ) { 
    this.defaultFormValues=[
      {'fieldName':'HOME', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'DASHBOARD', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'ACCOUNTS', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'GENERAL_LEDGER', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'SALES', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'SALES_NOTES', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'RULES', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'LOGOUT', 'fieldValue':'', 'helpMessage':''},
      {'fieldName':'HCM', 'fieldValue':'', 'helpMessage':''},
    ];
  }

  ngOnInit() {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    if(userData != null){
      this.loggedInUserRole=userData.role;
    }
    //getting header menu list
    this.getHeaderList();
     this.getScreenDetailService.getLanguageListForDropDown().subscribe(currentdata =>
     {
        this.arrLanguage = currentdata;
     });
     
   
       this.currentLanguage = localStorage.getItem('currentLanguage');
    // getting screen
    this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
    
      this.availableFormValues = data.result.dtoScreenDetail.fieldList;
      for(var j=0; j<this.availableFormValues.length; j++)
        {
            var fieldKey = this.availableFormValues[j]['fieldName'];
            var objAvailable = this.availableFormValues.find(x=>x['fieldName']===fieldKey);
            var objDefault = this.defaultFormValues.find(x=>x['fieldName']===fieldKey);
                objDefault['fieldValue']=objAvailable['fieldValue'];
                objDefault['helpMessage']=objAvailable['helpMessage'];
        }
    });
    this.updateSession();
    setInterval( this.updateSession.bind(this), 60000);
        
   
    
//  setInterval(function() {
//           alert("testing")
    
//   }, 10000);
    // setting active language
  

    // if (currentLanguage == "1") {
    //   this.isActive = 1;
    // }
    // else if (currentLanguage == "2") {
    //   this.isActive = 2;
    // }
    // else {
    //   this.isActive = 1;
    // }
  }

  UserLogOut(myModal)
  {
   
    myModal.open();
  }
  updateSession()
    {
       if(localStorage.getItem('currentUser'))
       {
        this.getScreenDetailService.UpdateActiveSession().then(currentdata =>
        {
          if(currentdata.btiMessage.messageShort != 'USER_UPDATED_SUCCESS')
          {
            localStorage.setItem('currentUser', '');
            this.router.navigate(['login']);
          }
        })
       }
    }

    settings() {
      this.router.navigate(['settings']);  
  }

  dashboard(){
    this.router.navigate(['dashboard']);
  }

  Logout() {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
      this.settingsService.logOut(userData.userId).then(data => {
        localStorage.setItem('currentUser', '');
        this.router.navigate(['login']);
    });
    this.companyService.changeCompanyName("");
  }

  ChangeLanguage() {
  
     localStorage.setItem('currentLanguage',  this.currentLanguage);
     this.languageSetupService.getLanguageById(this.currentLanguage).then(data => {
           localStorage.setItem('languageOrientation',  data.result.languageOrientation);
            window.location.reload();
      });

    
  }

  getHeaderList() {
    this.getScreenDetailService.getHeaderDetail().subscribe(data =>
      {
        if(data.btiMessage.messageShort != 'FORBIDDEN')
        {
         
          this.headerValues = data.result
        }
        else{
          localStorage.setItem('currentUser', '');
          this.router.navigate(['login']);
        }
      });
  }
  setCurrentHeader(event,moduleId) {
    event.preventDefault();
    this.getScreenDetailService.setCurrentModule(moduleId)
    localStorage.setItem('currentModule', moduleId);
    if (moduleId == 2 || moduleId == 21) {
      this.router.navigate(["hcm"]);
    } else if (moduleId == 99 || moduleId == 100) {
      this.router.navigate(["ims"]);
    } else if (moduleId == 12 || moduleId == 14){
      this.router.navigate(["masterdata"]);
    } else if (moduleId == 101 || moduleId == 102){
      this.router.navigate(["crmdashboard"]);
    } else {
      this.router.navigate(["dashboard"]);
    }
  }

  workflow() {
    
    this.commonService.openWorkflow();


  }
}