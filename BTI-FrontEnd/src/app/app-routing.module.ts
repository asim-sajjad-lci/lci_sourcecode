import { DashboardComponent } from './userModule/_components/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginHeaderComponent } from './_sharedcomponent/login-header.component';
import { LoginSidebarComponent } from './_sharedcomponent/login-sidebar.component';
import { MainHeaderComponent } from './_sharedcomponent/main-header.component';
import { MainSidebarComponent } from './_sharedcomponent/main-sidebar.component';
import { LoginComponent } from './loginModule/_components/login.component';
import { VerifyOtpComponent } from './loginModule/_components/verify-otp.component';
import { ForgotPasswordComponent } from './loginModule/_components/forgot-password.component';
import { AuthGuardService} from './_sharedresource/_services/auth-guard.service';
import { ResetPasswordComponent } from './loginModule/_components/reset-password.component';
import { SelectCompanyComponent } from './loginModule/_components/selectCompany.component';
// import { DemoComponent } from './demo/_components/demo.component';
import { AngularSessionComponent } from './angularSession/_components/angularSession.component';
import { UserSessionComponent } from './userModule/_components/userSession/userSession.component';
import { SettingsComponent } from './userModule/_components/settings/settings.component';

import { FieldAccessComponent } from './userModule/_components/fields/fieldAccess.component';
import { ReportManagementComponent } from './userModule/_components/report-management/report-management.component';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    {
        path: '', children: [
            { path: '', component: LoginHeaderComponent, outlet: 'header' },
            { path: '', component: LoginSidebarComponent, outlet: 'sidebar' },
            { path: 'login', component: LoginComponent },
            { path: 'verifyotp/:userId/:otp/:isResetPassword', component: VerifyOtpComponent },
            { path: 'forgotpassword', component: ForgotPasswordComponent },
            { path: 'resetpassword/:userId', component: ResetPasswordComponent },
            { path: 'selectcompany', component: SelectCompanyComponent  },
            
        ]
    },

    {
        path: '', children: [
            { path: '', component: MainHeaderComponent, outlet: 'header' },
            { path: '', component: MainSidebarComponent, outlet: 'sidebar' },
            // {path:'dashboard',component:DashboardComponent},
// Start: User Management
            { path: 'settings', component: SettingsComponent,canActivate: [AuthGuardService] },
            { path: 'dashboard', loadChildren: './userModule/_components/dashboard/dashboard.module#DashboardModule' },
        //    { path: 'dashboard/:userId', loadChildren: './userModule/_components/dashboard/dashboard.module#DashboardModule' },
            { path: 'authSetting', loadChildren: './userModule/_components/authsetting/auth-setting.module#AuthSettingModule' },
            { path: 'company', loadChildren: './userModule/_components/company/company.module#CompanyModule' },
            { path: 'manageLanguage', loadChildren: './userModule/_components/LanguageSetup/language-setup.module#LanguageSetupModule' },
            { path: 'restrictIp', loadChildren: './userModule/_components/restrictIP/restrictIP.module#RestrictIPModule' },
            { path: 'usergroup', loadChildren: './userModule/_components/usergroup/usergroup.module#UserGroupModule' },
            { path: 'rolegroup', loadChildren: './userModule/_components/rolegroup/rolegroup.module#RoleGroupModule' },
            { path: 'role', loadChildren: './userModule/_components/role/role.module#RoleModule' },
            { path: 'user', loadChildren: './userModule/_components/user/user.module#UserModule' },
            { path: 'whiteListIP', loadChildren: './userModule/_components/whiteListIP/white-list-IP.module#WhiteListIPModule' },
// End: User Management
// Start: Financial Module
            { path: 'companysetup', loadChildren: './financialModule/_components/CompanySetup/company-setup.module#CompanySetupModule' },
            { path: 'generalledgersetup', loadChildren: './financialModule/_components/GeneralLedgerSetup/general-ledger-setup.module#GeneralLedgerSetupModule' },
            { path: 'accountreceivablesetup', loadChildren: './financialModule/_components/AccountsReceivables/accounts-receivables-setup.module#AccountsReceivableModule' },
            { path: 'accountpayablesetup', loadChildren: './financialModule/_components/AccountsPayables/accounts-payables-setup.module#AccountsPayablesModule' },
            { path: 'fixedasset', loadChildren: './financialModule/_components/FixedAssets/fa-setup.module#AccountsReceivableModule' },
            { path: 'masterdata', loadChildren: './financialModule/_components/MasterData/master-data.module#MasterDataModule' },
            //{ path:'masterdata',component:DashboardComponent},
            { path: 'transaction', loadChildren: './financialModule/_components/TransactionModule/transactionModule.module#TransactionModule' },
            { path: 'report', loadChildren: './financialModule/_components/reports/report.module#ReportModule' },
// End: Financial Module
            // { path: 'hcm', loadChildren: './hcm/_components/hcm.module#HcmModule'},
// Start: IMS
            // { path: 'ims',loadChildren: './ims/_components/ims.module#ImsModule'},
// End: IMS
// Start: HCM
            { path: 'hcm', loadChildren: './hcm/_components/hcm.module#HcmModule'},
            { path: 'companyReportManagement', loadChildren: './userModule/_components/company-report-management/company-report-management.module#CompanyReportManagementModule' },
            { path: 'userReportManagement', loadChildren: './userModule/_components/user-report-management/user-report-management.module#UserReportManagementModule' },
            { path: 'reportManagement', component: ReportManagementComponent, canActivate: [AuthGuardService] },

// End: HCM
            // { path: 'crm', loadChildren: './crm/_components/crm.module#CrmModule'},
            { path: 'killSession',component:UserSessionComponent},
            // { path: 'demo',component:AngularSessionComponent},
            { path: 'fieldAccess/:companyId', component: FieldAccessComponent, canActivate: [AuthGuardService] },           
        ]
    }
/*    
    ,
    {
        path: 'ims', children: [
            { path: '', component: MainHeaderComponent, outlet: 'header' },
            { path: '', component: IMSSidebarComponent, outlet: 'sidebar' },
            { path: '', redirectTo: 'materialsSetupBill', pathMatch: 'full' },
            { path: 'materialsSetupBill', component: BillMaterialsSetupComponent },
            { path: 'priceLevelSetup', component: PriceLevelSetupComponent },
            { path: 'priceGroupSetup', component: PricegroupSetupComponent },
            { path: 'itemLotCategorySetup', component: ItemLotCategorySetupComponent },
            { path: 'siteSetup', component: SiteSetupComponent },
            { path: 'itemCategory', component: ItemCategoryComponent },
            { path: 'siteBinsSetup', component: SiteBinsSetupComponent },
            { path: 'stockCalenderSetup', component: StockCalendarSetupComponent },
            { path: 'inventoryContSetup', component: InventoryControlSetupComponent },
            { path: 'itemClassSetup', component: ItemClassSetupComponent },
            { path: 'classCategorySetup', component: ClassCurrencySetupComponent },
            { path: 'unitOfMeasureSetup', component: UnitOfMeasureComponent },
            { path: 'salesDocumentsSetup', component: SalesDocumentsSetupComponent },
            { path: 'salesInventorySetup', component: SalesInventorySetupComponent },
            { path: 'itemMaintenance', component: ItemMaintenanceComponent },
            { path: 'itemMaintenanceOption', component: ItemMaintenanceOptionComponent },
            { path: 'itemSerialNumberDefination', component: ItemSerialNumberDefinationComponent },
            { path: 'itemClassAccountSetup', component: itemClassAccuntSetupComponent },
            { path: 'itemSiteAssignments', component: ItemSiteAssignmentsComponent },
            { path: 'itemVendorMaster', component: ItemVendorMasterComponent },
            { path: 'itemQuantityMaster', component: ItemQuantityMasterComponent },
            { path: 'itemKistMaintenance', component: ItemKistMaintenanceComponent }

       ], 
    }
*/
// HCM start
/*
,
{
        path: 'hcm', children: [
            { path: '', component: MainHeaderComponent, outlet: 'header' },
            { path: '', component: HCMSidebarComponent, outlet: 'sidebar' },
            { path: '', component: DepartmentComponent },
            { path: 'department', component: DepartmentComponent },
            { path: 'division', component: DivisionComponent },
            { path: 'positionClass', component: PositionClassComponent },
            { path: 'positionSetup', component: PositionSetupComponent },
            { path: 'positionPlanSetup/:positionId/:id', component: PositionPlanSetupComponent },
            { path: 'positionTraining/:positionId/:positionDescription/:positionArbicDescription/:id', component: PositionTrainingComponent },
            { path: 'positionAttachment', component: PositionAttachmentComponent },
            { path: 'positionBudget', component: PositionBudgetComponent },
            { path: 'location', component: LocationComponent },
            { path: 'supervisor', component: SupervisorComponent },
            { path: 'positionPlanPayCode', component: PositionPayCodeComponent },
            { path: 'dateConverter', component: DateConverterComponent },
            { path: 'attendanceSetup', component: AttendanceSetupComponent },
            { path: 'attendanceSetupOptions', component: AttendanceSetupOptionsComponent },
            { path: 'deductionCodeSetup', component: DeductionCodeSetupComponent },
            { path: 'accrualSetupMain', component: AccrualMainCodeSetupComponent },
            { path: 'accrualSetupList', component: AccrualSetupComponent },
            { path: 'accrualScheduleSetup', component: AccrualScheduleSetupComponent },
            { path: 'healthCoverageTypeSetup', component: HealthCoverageTypeSetupComponent },
            { path: 'skillsSetup', component: SkillsSetupComponent },
            { path: 'skillSetSetup', component: SkillSetSetupComponent },
            { path: 'benefitCodeSetup', component: BenefitCodeSetupComponent },
            { path: 'healthInsuranceSetup', component: HealthInsuranceSetupComponent },
            { path: 'salaryMatrixSetup', component: SalaryMatrixSetupComponent },
            { path: 'healthInsuranceSetup', component: HealthInsuranceSetupComponent },
            { path: 'timeCode', component: TimeCodeComponent },
            { path: 'miscellaneousBenefits', component: MiscellaneousBenefitsComponent },
            { path: 'terminationSetup', component: TerminationSetupComponent },
            { path: 'lifeInsuranceSetup', component: LifeInsuranceSetupComponent },
            { path: 'retirementPlanSetup', component: RetirementPlanSetupComponent },
            { path: 'healthInsSetup', component: HealthInsSetupComponent },
            { path: 'ur5', component: UR5MandatoryComponent },
            { path: 'shiftCodeSetup', component: ShiftCodeSetupComponent },
			{ path: 'predefinechecklist', component:  PredefineChecklistComponent },
            { path: 'payCodeSetup', component:  PaycodeSetupComponent },
            { path: 'exitInterview', component:  ExitInterviewComponent },
            { path: 'requisitionsSetup', component: RequisitionsSetupComponent },
            { path: 'benefitPreferences', component: BenefitPreferencesComponent },
            { path: 'trainingCourse', component: TrainingCourseComponent },
            { path: 'trainingBatchSignup', component: TrainingBatchSignupComponent },
            { path: 'orientationSetup',component: OrientationSetupComponent},
            { path: 'interviewTypeSetup', component: InterviewTypeSetupComponent },
            { path: 'payScheduleSetup', component: PayScheduleSetupComponent },
            { path: 'class-skill',component: ClassSkillComponent},
            { path: 'clsenrollment',component: ClassEnrollmentComponent},
            { path: 'employeeMaster',component: EmployeeMasterComponent},
            { path: 'employeeDeductnMaster', component: EmployeeDeductionMaintenanceComponent },
            { path: 'employeeBenefitMaster', component: EmployeeBenefitMaintenanceComponent },
            { path: 'employeePayCodeMaster', component: EmployeePayCodeMaintenanceComponent },
            { path: 'healthInsuranceEnrollment', component: HealthInsuranceEnrollmentComponent },
            { path: 'miscellaneousBenefitEnroll', component: MiscellaneousBenefitsEnrollmentComponent },
            { path: 'employeePostDatedPayRate', component: EmployeePostDatedPayrateComponent },
            { path: 'employeeQuickAssignment', component: EmployeeQuickAssignmentComponent },
			{ path: 'employeeNationality',component: EmployeeNationalityComponent},
			{ path: 'employeeEducation',component: EmployeeEducationComponent},
			{ path: 'employeeDependents',component: EmployeeDependentsComponent},
            { path: 'employeeEducation',component: EmployeeEducationComponent},
            { path: 'employeeSkills', component: EmployeeSkillsComponent },
            { path: 'employeeDirectDeposit', component: EmployeeDirectDepositComponent },
            { path: 'employeeContacts', component: EmployeeContactsComponent },
			{ path: 'report', component: ReportComponent },
			{ path: 'employeeAddressMaster', component: EmployeeAddressMasterComponent },
            { path: 'employeePositionHistory', component: EmployeePositionHistoryComponent },
            { path: 'batches', component: BatchesComponent },
            // { path: 'accountStatementReport', component: AccountStatementReportComponent },
            { path: 'transactionEntry', component: TransactionEntryComponent },
            { path: 'buildChecks', component: BuildChecksComponent },
            { path: 'buildPayrollCheckPayCodes', component: BuildPayrollCheckComponentPayCode },
            { path: 'buildPayrollCheckDeductions', component: BuildPayrollCheckDeductionsComponent },
            { path: 'buildPayrollCheckBenefits', component: BuildPayrollCheckBenefitsComponent },
            { path: 'buildPayrollCheckBatches', component: BuildPayrollCheckBatchesComponent },
			// { path: 'accountReport', component: AccountReportComponent },
            // { path: 'accountReport', component: AccountReportComponent },
            { path:'activateEmployeePostDatePayRate',component:ActiveEmployeePostDatedPayrateComponent},
            { path:'calculateChecks',component:CalculateChecksComponent},
            { path:'buildcheckReport',component:BuildCheckReportComponent},
            { path:'buildPayrollCheckDefault',component:BuildPayrollCheckDefaultComponent},
        ]
}
*/
// HCM ends
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { } 