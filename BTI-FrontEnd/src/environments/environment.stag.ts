// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  userModuleApiBaseUrl: 'https://staging.algoras.com/user_staging/',
  financialModuleApiBaseUrl: 'https://staging.algoras.com/financial_staging/',
  hcmModuleApiBaseUrl: 'https://staging.algoras.com/hcm_staging/',
  hcmModuleApiDirectBaseUrl: 'https://staging-cfdirect.algoras.com/hcm_staging/',
  imsModuleApiBaseUrl: 'https://staging.algoras.com/ims_staging/',
  BaseUrl: 'https://staging.algoras.com/user_staging/'

};
