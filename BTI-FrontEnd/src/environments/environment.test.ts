// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  userModuleApiBaseUrl: 'https://test.algoras.com/user_test/',
  financialModuleApiBaseUrl: 'https://test.algoras.com/financial_test/',
  hcmModuleApiBaseUrl: 'https://test.algoras.com/hcm_test/',
  hcmModuleApiDirectBaseUrl: 'https://test-cfdirect.algoras.com/hcm_test/',
  imsModuleApiBaseUrl: 'https://test.algoras.com/ims_test/',
  BaseUrl: 'https://test.algoras.com/user_test/'

};
 