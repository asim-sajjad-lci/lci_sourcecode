// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  userModuleApiBaseUrl: 'http://localhost:8082/',
  financialModuleApiBaseUrl: 'http://localhost:8085/',
  hcmModuleApiBaseUrl: 'http://localhost:8087/',
  hcmModuleApiDirectBaseUrl: 'http://localhost:8087/',
  imsModuleApiBaseUrl: 'http://localhost:8089/',
  BaseUrl: 'http://localhost:8082/'
};
