export const environment = {
  production: true,

  userModuleApiBaseUrl: 'https://bti.algoras.com/user/',
  financialModuleApiBaseUrl: 'https://bti.algoras.com/financial/',
  hcmModuleApiBaseUrl: 'https://bti.algoras.com/hcm/',
  hcmModuleApiDirectBaseUrl: 'https://bti-cfdirect.algoras.com/hcm/',
  imsModuleApiBaseUrl: 'https://bti.algoras.com/ims/',
  BaseUrl: 'https://bti.algoras.com/user/'

};
