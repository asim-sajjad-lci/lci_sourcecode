/**
 * A service class for login
 */
import { Injectable } from '@angular/core'
import { Headers, Http } from '@angular/http';
import {Constants} from '../../_sharedresource/Constants'
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs';
 
@Injectable()
export class LoginService {
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private checkMyIPUrl = Constants.userModuleApiBaseUrl+'myIP';
    private checkIpUrl = Constants.userModuleApiBaseUrl+'login/checkIpBeforeLogin';
    private loginUrl = Constants.userModuleApiBaseUrl+'login/loginUserForOtp';
    private verifyOtpUrl = Constants.userModuleApiBaseUrl+'login/verifyOtpAuthentication';
    private getScreenDetailUrl = Constants.userModuleApiBaseUrl+'screenDetail';
    private forgotPasswordUrl = Constants.userModuleApiBaseUrl+'forgotPassword';
    private resetPasswordUrl = Constants.userModuleApiBaseUrl+'resetPassword';

    //initializing parameter for constructor 
    constructor(private http: Http) {
        var currentUser=localStorage.getItem('currentUser');
        if (currentUser) {
            var userData = JSON.parse(currentUser);
            if (userData) {
                this.headers.append("session", userData.session);
                alert(userData.session);
                this.headers.append("userid", userData.userId);
            }
        }
        var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", currentLanguage);
    }

    //check ip of system
    checkMyIp() {
        return this.http
            .get(this.checkMyIPUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //check loggedin user Ip
    checkUserIp() {
        return this.http
            .get(this.checkIpUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //loginUserForOTP
    loginUserForOTP(username: string, password: string) {
        return this.http
            .post(this.loginUrl, JSON.stringify({ userName: username, password: password }), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //forgotPassword
    forgotPassword(email: string) {
      return this.http
            .post(this.forgotPasswordUrl, JSON.stringify({ email: email}), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    

    //verify otp for authentication
    verifyOtpAuthentication(userId: string, otp: string) {
        
        return this.http
            .post(this.verifyOtpUrl, JSON.stringify({ userId: userId, otp: otp }), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //reset password
    resetPassword(userId: string, password: string) {
        return this.http
            .post(this.resetPasswordUrl, JSON.stringify({ userId: userId, password: password }), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error);
    }
}