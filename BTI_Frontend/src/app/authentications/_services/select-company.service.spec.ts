import { TestBed, inject } from '@angular/core/testing';

import { SelectCompanyService } from './select-company.service';

describe('SelectCompanyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectCompanyService]
    });
  });

  it('should be created', inject([SelectCompanyService], (service: SelectCompanyService) => {
    expect(service).toBeTruthy();
  }));
});
