import { Constants } from '../../_sharedresource/Constants';
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';


@Injectable()
export class SelectCompanyService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getCompanyListUrl = Constants.userModuleApiBaseUrl + 'companyListByUserId';
    private checkCompanyAccessUrl = Constants.userModuleApiBaseUrl + 'login/checkCompanyAccess';
    private checkMyIPUrl = Constants.userModuleApiBaseUrl+'myIP';
    private userId = '';
  
  constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", currentLanguage);
        this.userId=userData.userId;
    }

    //getting company list
    getCompanyList() {
        return this.http.post(this.getCompanyListUrl, {'userId':this.userId},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    checkCompanyAccess(companyId) {
        console.log("user info",localStorage.getItem('currentUser'))
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        console.log("user info",userData)
        return this.http.post(this.checkCompanyAccessUrl, {'companyId':companyId,'session':userData.session,'userId':userData.userId},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check ip of system
    checkMyIp(tenantid) {
        this.headers.append("tenantid", tenantid);
        return this.http
            .get(this.checkMyIPUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
   

     //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }}