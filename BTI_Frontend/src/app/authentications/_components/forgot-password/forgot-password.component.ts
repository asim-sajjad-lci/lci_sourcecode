import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from 'app/_sharedresource/Constants';
import { LoginService } from 'app/authentications/_services/login.service';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [LoginService,AlertService,GetScreenDetailService]
})
export class ForgotPasswordComponent implements OnInit {
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1001";
  screenName;
  moduleName;
  defaultFormValues: any = [];
  availableFormValues: any = [];
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  model: any = {};
  username;
  email;
  password;
  isActive: number;
  currentLanguage: string;
  currentLanguageId:string;
  currentLanguageName:string;
  arrLanguage:[object];

  constructor(
    private router: Router,
    private loginService: LoginService,
    private getScreenDetailService: GetScreenDetailService,
    private alertService: AlertService
) {
    //defaultFormValues for forgotpassword screen
    this.defaultFormValues = [
        { 'fieldName': 'FORGOT_PASS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
        { 'fieldName': 'EMAIL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
        { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
        { 'fieldName': 'SUBMIT', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
    ];
}
// Screen initialization 
ngOnInit() {

  this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
   {
      this.arrLanguage = data
   });  
   this.currentLanguageName=localStorage.getItem('currentLanguageName');
   //  this.currentLanguage = localStorage.getItem('currentLanguage');
  // getting forgot password screen
   this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
          this.moduleName=data.result.moduleName;
          this.screenName=data.result.dtoScreenDetail.screenName;
          this.availableFormValues = data.result.dtoScreenDetail.fieldList;
          for (var j = 0; j < this.availableFormValues.length; j++) {
              var fieldKey = this.availableFormValues[j]['fieldName'];
              var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
              var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
              if (objDefault) {
                  objDefault['fieldValue'] = objAvailable['fieldValue'];
                  objDefault['helpMessage'] = objAvailable['helpMessage'];
                  objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
              }
          }
      });

      //setting active language
      var currentLanguage = localStorage.getItem('currentLanguage');
      if (currentLanguage == "1") {
          this.isActive = 1;
      }
      else if (currentLanguage == "2") {
          this.isActive = 2;
      }
      else {
          this.isActive = 1;
      }
  }

  //function call for sending email with password
  getPassword() {
    debugger;
      this.loginService.forgotPassword(this.model.email).then(data => {
        debugger;
          var code = data.code;
          if (code == "200") {
              window.setTimeout(() => {
                      this.isSuccessMsg = true;
                      this.isfailureMsg = false;
                      this.showMsg = true;
                      this.messageText = data.btiMessage.message;
              }, 100);
                      this.hasMsg = true;
              window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                      this.router.navigate(['login']);
              }, 4000);
              }
          else if (code == "404") {
              window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      this.messageText = data.btiMessage.message;
              }, 100);
                      this.hasMsg = true;
              window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
              }, 4000);
          }
      return false; 
      }).catch(error => {
          this.hasMsg = true;
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
          }, 100)
      });
  }

  ChangeLanguage(lng) {
    //  alert(this.currentLanguage)
      // this.currentLanguage=lngId;
     
       console.log("language...",lng);
       this.currentLanguageId=lng.languageId;
       localStorage.setItem('currentLanguage',  lng.languageId);
       this.currentLanguageName=lng.languageName;
       localStorage.setItem('currentLanguageName', this.currentLanguageName)
       

      this.getScreenDetailService.getLanguageById(this.currentLanguageId).then(data => {
            localStorage.setItem('languageOrientation',  data.result.languageOrientation);
             window.location.reload();
       });



//         isActive: true
// languageId: 2
// languageName: "Arabic"
// languageOrientation: "RTL"
// languageStatus: "Active"
   }
  gotoLogin(){
    debugger;
    this.router.navigate(["login"]);

  }
}
