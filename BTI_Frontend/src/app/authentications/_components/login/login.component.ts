import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, Inject } from '@angular/core';
import { Router } from '@angular/router';
import {state, style, transition, animate, trigger, AUTO_STYLE} from '@angular/animations';
import 'rxjs/add/operator/filter';
import { MenuItems } from 'app/shared/menu-items/menu-items';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from '../../_services/login.service';
import { Constants } from 'app/_sharedresource/Constants';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { DOCUMENT } from '../../../../../node_modules/@angular/platform-browser';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService,GetScreenDetailService,CookieService,AlertService],
 
  animations: [ trigger('slideInOut', [
    state('in', style({
      transform: 'translate3d(0, 0, 0)'
    })),
    state('out', style({
      transform: 'translate3d(100%, 0, 0)'
    })),
    transition('in => out', animate('400ms ease-in-out')),
    transition('out => in', animate('400ms ease-in-out'))
  ]),
  trigger('slideOnOff', [
    state('on', style({
      transform: 'translate3d(0, 0, 0)'
    })),
    state('off', style({
      transform: 'translate3d(100%, 0, 0)'
    })),
    transition('on => off', animate('400ms ease-in-out')),
    transition('off => on', animate('400ms ease-in-out'))
  ]),
  trigger('mobileMenuTop', [
      state('no-block, void',
          style({
              overflow: 'hidden',
              height: '0px',
          })
      ),
      state('yes-block',
          style({
              height: AUTO_STYLE,
          })
      ),
      transition('no-block <=> yes-block', [
          animate('400ms ease-in-out')
      ])
  ])
]
  
 // D:\mustahsin_project\BTI_ERP\Algora_ERP_31_03_19_2\Algora_ERP\src\assets\pages\css\login-5.min.css

})
export class LoginComponent implements OnInit {

  moduleCode = Constants.userModuleCode;
  screenCode = "S-1000";
  screenName;
  defaultFormValues: any = [];
  availableFormValues: any = [];
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  model: any = {};
  MyIP;
  userId: number;
  otp: string;
  isResetPassword:string;
  isActive: number;
  currentLanguage: string;
  currentLanguageId:string;
  currentLanguageName:string;
  arrLanguage:[object];
  atATimeText=Constants.atATimeText;


  isForgotPassword=false;
  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  chatToggle = 'out';
  chatInnerToggle = 'off';
  innerHeight: string;
  isScrolled = false;
  isCollapsedMobile = 'no-block';
  toggleOn = true;
  windowWidth: number;



  @ViewChild('searchFriends') search_friends: ElementRef;
  @ViewChild('toggleButton') toggle_button: ElementRef;
  @ViewChild('sideMenu') side_menu: ElementRef;
  constructor(
    
    private router: Router,
    public menuItems: MenuItems,
   
    private loginService: LoginService,
    private getScreenDetailService: GetScreenDetailService,
    private cookieService: CookieService,
    private alertService: AlertService,
    @Inject(DOCUMENT) private document: any
  ) 
  {

  
    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;

    //defaultFormValues for login screen
    this.defaultFormValues = [
      { 'fieldName': 'LOGIN', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'REMEMBER_ME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'FORGOT_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
      { 'fieldName': 'LOGIN_BUTTON', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' }
    ];
   
   }

  ngOnInit() {
    debugger
    // this.currentLanguage="1";
    // localStorage.setItem('currentLanguage',  this.currentLanguage);
      //getting data from cookie
      const cookieUsernameExists: boolean = this.cookieService.check('username');
      const cookiePasswordExists: boolean = this.cookieService.check('password');
      
       if(cookieUsernameExists && cookiePasswordExists)
       {
         this.model.remember=true;
         this.model.username = this.cookieService.get('username');
         this.model.password = this.cookieService.get('password');
       }
       this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
       {
                this.arrLanguage = data
       });
    
     
       this.currentLanguageName=localStorage.getItem('currentLanguageName');
      //  this.currentLanguage = localStorage.getItem('currentLanguage');
      //getting screen for login
      this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
  
            
      this.screenName=data.result.dtoScreenDetail.screenName
      this.availableFormValues = data.result.dtoScreenDetail.fieldList;
        for (var j = 0; j < this.availableFormValues.length; j++) {
          var fieldKey = this.availableFormValues[j]['fieldName'];
          var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
          var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
          objDefault['fieldValue'] = objAvailable['fieldValue'];
          objDefault['helpMessage'] = objAvailable['helpMessage'];
          objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
        }
      });
  
    
  
  
    //setting active language
    var currentLanguage = localStorage.getItem('currentLanguage');
    this.currentLanguage=currentLanguage;
    
  
    if (currentLanguage == "1") {
        this.isActive = 1;
      }
    else if (currentLanguage == "2") {
        this.isActive = 2;
      }
    else {
        this.isActive = 1;
      }

    
      
  }

  //check Ip for system
 checkMyIp()
 {
   this.loginService.checkMyIp().then(data => {
         this.MyIP=data.result;
    });
 }

  //function call for loggedin user
  Login() {
  debugger
    this.loginService.loginUserForOTP(this.model.username, this.model.password).then(data => {
        if(data.result != undefined && data.result != 'undefined')
        {
         
          this.userId = data.result.userId;
          this.otp = data.result.otp;
          this.isResetPassword=data.result.isResetPassword;
         if (this.model.remember) {
          // storing data in cookie
           this.cookieService.set( 'username', this.model.username );
           this.cookieService.set( 'password', this.model.password );
          }
          else{
            const cookieUsernameExists: boolean = this.cookieService.check('username');
            const cookiePasswordExists: boolean = this.cookieService.check('password');
             if(cookieUsernameExists && cookiePasswordExists)
             {
               this.cookieService.delete('username');
               this.cookieService.delete('password');
             }
          }
          // storing credential
          var userCredential={'username':this.model.username,'password':this.model.password};
          localStorage.setItem('userCredential',JSON.stringify(userCredential));
          this.router.navigate(['verifyotp', this.userId, this.otp,this.isResetPassword]);  

         // this.router.navigate(["verifyotp"]);
        //  if(data.result.isResetPassword== 'Y')
        //  {
        //   this.router.navigate(['resetpassword', this.userId]);
        //   this.router.navigate(['verifyotp', this.userId, this.otp,this.isResetPassword]); 
        //  }
         }
        else
        {
       
          this.hasMsg = true;
          window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = data.btiMessage.message;
        return false;
      }, 100)
        }
    }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
        }, 100)
     });
    }

     ChangeLanguage(lng) {
      //  alert(this.currentLanguage)
        // this.currentLanguage=lngId;
       
         console.log("language...",lng);
         this.currentLanguageId=lng.languageId;
         localStorage.setItem('currentLanguage',  lng.languageId);
         this.currentLanguageName=lng.languageName;
         localStorage.setItem('currentLanguageName', this.currentLanguageName)
         

        this.getScreenDetailService.getLanguageById(this.currentLanguageId).then(data => {
              localStorage.setItem('languageOrientation',  data.result.languageOrientation);
               window.location.reload();
         });



//         isActive: true
// languageId: 2
// languageName: "Arabic"
// languageOrientation: "RTL"
// languageStatus: "Active"
     }
 
     gotoForgotPassword(){
      this.router.navigate(["forgotPassword"]);
     }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }
}
