import { Routes } from '@angular/router';

import { SelectCompanyComponent } from './select-company.component';



export const VerifyotpRoutes: Routes = [{
  path: 'selectCompany',
  component: SelectCompanyComponent,
  data: {
    //breadcrumb: "Dashboard"
  }
}];