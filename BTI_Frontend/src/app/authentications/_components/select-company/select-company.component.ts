import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectCompanyService } from 'app/authentications/_services/select-company.service';
import { SettingsService } from 'app/userModule/_services/settings/settings.service';
import { Constants } from 'app/_sharedresource/Constants';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { debounce } from '../../../../../node_modules/rxjs/operator/debounce';
declare var $:any
@Component({
  selector: 'app-select-company',
  templateUrl: './select-company.component.html',
  styleUrls: ['./select-company.component.css'],
  providers: [SelectCompanyService,SettingsService,GetScreenDetailService]
})
export class SelectCompanyComponent implements OnInit {
  screenCode = "S-1227";
  screenName;
  ddl_MultiSelectCompany = [];
  ddlCompanySetting = {};
  // defaultFormValues: [object];
  //availableFormValues: [object];
  defaultFormValues: any = [];
  availableFormValues: any = [];
  moduleCode = Constants.userModuleCode;
  selectCompany = Constants.selectCompany;
  selectAll = Constants.selectAll; 
  unselectAll = Constants.unselectAll;
  search=Constants.search;
  
  arrLanguage:[object];
  currentLanguage: string;
  currentLanguageId:string;
  currentLanguageName:string;
  companyId:string;
   SelectedCompany = [];
  SelectedCompanyModule ="";
  SelectedCompanyId;
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  isResetPassword:string;
  private MyIP;
  isActive: number;
  @Output() change = new EventEmitter<any[]>();
  //selectedCompany:any[]=[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private selectCompanyService: SelectCompanyService,
    private settingsService: SettingsService,
    private getScreenDetailService: GetScreenDetailService,
 ) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.isResetPassword =localStorage.getItem('isResetPassword');
         this.defaultFormValues = [
            { 'fieldName': 'SELECT_COM_PROCEED', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
            { 'fieldName': 'SELECT_COM_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' }
            ];
 }

// public countryList=[{name:"+Add new",value:"-1"},{name:"",value:""},{name:"America",value:"1"},{name:"Australia",value:"2"},{name:"India",value:"3"},{name:"Pakistan",value:"4"},{name:"Saudi Arabia",value:"5"},{name:"Sudan",value:"6"}]
ngOnInit() {
    this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
    {
       this.arrLanguage = data
    });

    this.currentLanguageName=localStorage.getItem('currentLanguageName');
      //  this.currentLanguage = localStorage.getItem('currentLanguage');
      //getting screen for login

        this.getScreenDetailService.getScreenDetail(this.moduleCode, this.screenCode).then(data => {
        this.screenName=data.result.dtoScreenDetail.screenName
        this.availableFormValues = data.result.dtoScreenDetail.fieldList;
        for (var j = 0; j < this.availableFormValues.length; j++) {
            var fieldKey = this.availableFormValues[j]['fieldName'];
            var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
            var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
            objDefault['fieldValue'] = objAvailable['fieldValue'];
            objDefault['helpMessage'] = objAvailable['helpMessage'];
            objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
        }
        });



        
         this.ddlCompanySetting = { 
          singleSelection: true, 
          text:this.selectCompany,
          selectAllText: this.selectAll,
          unSelectAllText: this.unselectAll,
          enableSearchFilter: true,
          classes:"myclass custom-class",              
            searchPlaceholderText:this.search,
        };  

          this.selectCompanyService.getCompanyList().then(data => {
              if(data.result)
              {
                    for(var i=0;i<data.result.length;i++)
                    {
                        this.ddl_MultiSelectCompany.push({ "id": data.result[i].companyId, "itemName": data.result[i].name,"checked":false })
                    }

                    console.log("ddl data",this.ddl_MultiSelectCompany)
              }
           
       });


        //setting active language
    var currentLanguage = localStorage.getItem('currentLanguage');
  
     
  
    if (currentLanguage == "1") {
        this.isActive = 1;
      }
    else if (currentLanguage == "2") {
        this.isActive = 2;
      }
    else {
        this.isActive = 1;
      }
  
    }

//     check(option)
// {
  
//   console.log(option);
//   option.checked=!option.checked;
  
//   this.SelectedCompany=this.ddl_MultiSelectCompany.filter((x:any)=>x.checked).map(x=>
//     {
//    //   this.selectedData.push(x.label);
//       return {id:x.id,itemName:x.itemName}
     
//     })

//     console.log(this.SelectedCompany);
  
//   this.change.emit(this.SelectedCompany);
// }

// remove(itmem){
 
//   console.log("sfdgfdsgsdffg");
  
//   for(let i=0;i<this.SelectedCompany.length;i++)
//   {
//     if(this.SelectedCompany[i].id == itmem.id) 
//     {
//       itmem.checked=!itmem.checked;
//       this.SelectedCompany.splice(i,1);

//     }
//   }

//   for(let i=0;i<this.ddl_MultiSelectCompany.length;i++)
//   {
//     if(this.ddl_MultiSelectCompany[i].id == itmem.id) 
//     {
//      // itmem.checked=!itmem.checked;
//       this.ddl_MultiSelectCompany[i].checked = false;

//     }
//   }
//  // this.change.emit(this.data);
//   console.log(itmem);
  
  
//   this.change.emit(this.SelectedCompany);
//  }

 onAutocompleteFocus(labelId){
  debugger;
  let elem: HTMLElement = document.getElementById(labelId);
  elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
}

onAutocompleteFocusOut(labelId){
  // let elem: HTMLElement = document.getElementById(labelId);
  // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
  
}

onCompanySelect(event){

 console.log("companyevent",event)
 this.SelectedCompany.push(event);
  if(event.value==-1){
    // this.SelectedCompany="";
    // this.openMyModal('effect-15')
   
  }
}

Proceed()
{
  debugger
  
    if(this.SelectedCompany.length > 0)
    {
        this.selectCompanyService.checkCompanyAccess(this.SelectedCompany[0].id).then(data => {
          debugger
          var datacode = data.code;
            if (datacode == 200) {
              localStorage.setItem('tenantid', data.result.companyTenantId);
              if(this.isResetPassword == 'Y')
              {
               this.router.navigate(['resetpassword', data.result.userId]);  
              }
              else
              {
                   
                //this.router.navigate(['']);  
                //location.href='dashboard';
                localStorage.setItem("currentCompany",this.SelectedCompany[0].itemName)
                this.router.navigate(['dashboard']);  
              }
        }
        else{
               this.hasMsg = true;
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = data.btiMessage.message+' !!';
              window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
              }, 3000);
        }
    }).catch(error => {
        this.hasMsg = true;
               window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
            }, 100)
    });
}
else{
        this.hasMsg = true;
        this.isSuccessMsg = true;
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = "Please Select Company !!"
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
        }, 3000);
     }  
}
    
cancel() {
var userData = JSON.parse(localStorage.getItem('currentUser'));
  this.settingsService.logOutBeforeCompanySelection(userData.userId).then(data => {
    localStorage.setItem('currentUser', '');
    this.router.navigate(['login']);
});
}

// onItemSelect(item:any){
//  this.SelectedCompany=[];
//  this.SelectedCompany.push({"id":item.id,"itemName":item.itemName});
//  $( ".c-btn" ).trigger( "click" );
// }
// OnItemDeSelect(item:any){
//  this.SelectedCompany=[];
// }
onSelectAll(items: any){
}
onDeSelectAll(items: any){
    
}


ChangeLanguage(lng) {
  //  alert(this.currentLanguage)
    // this.currentLanguage=lngId;
   
     console.log("language...",lng);
     this.currentLanguageId=lng.languageId;
     localStorage.setItem('currentLanguage',  lng.languageId);
     this.currentLanguageName=lng.languageName;
     localStorage.setItem('currentLanguageName', this.currentLanguageName)
     

    this.getScreenDetailService.getLanguageById(this.currentLanguageId).then(data => {
          localStorage.setItem('languageOrientation',  data.result.languageOrientation);
           window.location.reload();
     });



//         isActive: true
// languageId: 2
// languageName: "Arabic"
// languageOrientation: "RTL"
// languageStatus: "Active"
 }
}
