import { Routes } from '@angular/router';

import { VerifyOtpComponent } from './verify-otp.component';



export const VerifyotpRoutes: Routes = [{
  path: 'verifyotp',
  component: VerifyOtpComponent,
  data: {
    //breadcrumb: "Dashboard"
  }
}];