import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import {ScrollModule} from './scroll/scroll.module';
import {LocationStrategy, PathLocationStrategy, HashLocationStrategy} from '@angular/common';
import { LoginComponent } from './authentications/_components/login/login.component';
import { VerifyOtpComponent } from './authentications/_components/verify-otp/verify-otp.component';
import { SelectCompanyComponent } from './authentications/_components/select-company/select-company.component';
import { Ng2AutoCompleteModule } from '../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { ForgotPasswordComponent } from 'app/authentications/_components/forgot-password/forgot-password.component';
import { ExDialog, DialogService } from './generic-component/ngx-dialog/dialog.module';
import { ComponentLoaderFactory, PositioningService } from '../../node_modules/ngx-bootstrap';










@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    BreadcrumbsComponent,
    TitleComponent,
    LoginComponent,
    VerifyOtpComponent,
    SelectCompanyComponent,
    ForgotPasswordComponent,
  
  
   

  
   
    
  
 
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpModule,
    ScrollModule,
    Ng2AutoCompleteModule,
    NoopAnimationsModule,
    
  ],
  exports: [ScrollModule],
  providers:
  [ExDialog,DialogService,ComponentLoaderFactory,PositioningService,
     { provide: LocationStrategy, useClass: HashLocationStrategy }
 ],
  bootstrap: [AppComponent]
})
export class AppModule {}
