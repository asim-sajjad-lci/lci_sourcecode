import { Injectable } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';
import { Observable } from 'rxjs';
import { PagedData } from 'app/_sharedresource/paged-data';
import { Page } from 'app/_sharedresource/page';
import { Headers, Http } from '@angular/http';
import { TicketClassSetupModel } from 'app/HcmModule/_models/ticket-class-setup/ticket-class-setup';


@Injectable()
export class TicketClassSetupService {

  private headers = new Headers({ 'content-type': 'application/json' });
    private getAllTicketClassUrl = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/getAll';
    private createTicketClassUrl = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/create';
    private getTicketClassByIdUrl = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/getById';
    private updateTicketClassUrl = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/update';
    private deleteTicketClassUrl = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/delete';
    private checkTicketClassIdx = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/idCheck';
	private getTicketClassSetup = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/getAllDropDown';

  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
}

//add new TicketClass
createTicketClass(data: TicketClassSetupModel) {
    return this.http.post(this.createTicketClassUrl, JSON.stringify(data), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//update for edit TicketClass
updateTicketClass(data: TicketClassSetupModel) {
    return this.http.post(this.updateTicketClassUrl, JSON.stringify(data), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//delete TicketClass
deleteTicketClass(ids: any) {
    return this.http.put(this.deleteTicketClassUrl, { 'delIds': ids }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getTicketClasses(){
 return this.http.get(this.getTicketClassSetup, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);

}

//check for duplicate ID TicketClass
checkDuplicateClassId(codeId: any) {
    return this.http.post(this.checkTicketClassIdx, { 'codeId': codeId }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//get TicketClass detail by Id
getTicketClassById(id: string) {
    return this.http.post(this.getTicketClassByIdUrl, { id: id }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//get list
getlist(page: Page, searchKeyword): Observable<PagedData<TicketClassSetupModel>> {
    return this.http.post(this.getAllTicketClassUrl, {
        'searchKeyword': searchKeyword,
        'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
}


//pagination for data
private getPagedData(page: Page, data: any): PagedData<TicketClassSetupModel> {
  debugger
    let pagedData = new PagedData<TicketClassSetupModel>();
    if (data) {
        var gridRecords = data.records;
        page.totalElements = data.totalCount;
        if (gridRecords && gridRecords.length > 0) {
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let department = new TicketClassSetupModel(
                    jsonObj.id,
                    jsonObj.codeId,
                    jsonObj.descr,
                    jsonObj.arabicDescr
                );
                pagedData.data.push(department);
            }
        }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
}

//error handler
private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
}

}
