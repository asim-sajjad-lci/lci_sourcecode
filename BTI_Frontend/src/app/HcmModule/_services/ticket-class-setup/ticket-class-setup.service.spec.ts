import { TestBed, inject } from '@angular/core/testing';

import { TicketClassSetupService } from './ticket-class-setup.service';

describe('TicketClassSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketClassSetupService]
    });
  });

  it('should be created', inject([TicketClassSetupService], (service: TicketClassSetupService) => {
    expect(service).toBeTruthy();
  }));
});
