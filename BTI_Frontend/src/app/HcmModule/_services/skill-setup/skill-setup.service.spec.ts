import { TestBed, inject } from '@angular/core/testing';

import { SkillSetupService } from './skill-setup.service';

describe('SkillSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkillSetupService]
    });
  });

  it('should be created', inject([SkillSetupService], (service: SkillSetupService) => {
    expect(service).toBeTruthy();
  }));
});
