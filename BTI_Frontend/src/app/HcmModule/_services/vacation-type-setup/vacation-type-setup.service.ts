import { Injectable } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { VacationTypeSetupModel } from 'app/HcmModule/_models/vacation-type-setup/vacation-type-setup';
import { PagedData } from 'app/_sharedresource/paged-data';
import { Page } from 'app/_sharedresource/page';
import { Observable } from 'rxjs';

@Injectable()
export class VacationTypeSetupService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllVacationTypeUrl = Constants.hcmModuleApiBaseUrl + 'vacationTypeSetup/getAll';
  private createVacationTypeUrl = Constants.hcmModuleApiBaseUrl + 'vacationTypeSetup/create';
  private getVacationTypeByIdUrl = Constants.hcmModuleApiBaseUrl + 'vacationTypeSetup/getById';
  private updateVacationTypeUrl = Constants.hcmModuleApiBaseUrl + 'vacationTypeSetup/update';
  private deleteVacationTypeUrl = Constants.hcmModuleApiBaseUrl + 'vacationTypeSetup/delete';
  private checkVacationTypeIdx = Constants.hcmModuleApiBaseUrl + 'vacationTypeSetup/idCheck';
private getVacationTypeSetup = Constants.hcmModuleApiBaseUrl + 'vacationTypeSetup/getAllDropDown';

constructor(private http: Http) {
  var userData = JSON.parse(localStorage.getItem('currentUser'));
  this.headers.append('session', userData.session);
  this.headers.append('userid', userData.userId);
  var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
  this.headers.append("langid", currentLanguage);
  this.headers.append("tenantid", localStorage.getItem('tenantid'));
}

//add new VacationType
createVacationType(data: VacationTypeSetupModel) {
  return this.http.post(this.createVacationTypeUrl, JSON.stringify(data), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//update for edit VacationType
updateVacationType(data: VacationTypeSetupModel) {
  return this.http.post(this.updateVacationTypeUrl, JSON.stringify(data), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//delete VacationType
deleteVacationType(ids: any) {
  return this.http.put(this.deleteVacationTypeUrl, { 'delIds': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

getVacationTypes(){
return this.http.get(this.getVacationTypeSetup, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);

}

//check for duplicate ID VacationType
checkDuplicateTypeId(codeId: any) {
  return this.http.post(this.checkVacationTypeIdx, { 'codeId': codeId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//get VacationType detail by Id
getVacationTypeById(id: string) {
  return this.http.post(this.getVacationTypeByIdUrl, { id: id }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//get list
getlist(page: Page, searchKeyword): Observable<PagedData<VacationTypeSetupModel>> {
  return this.http.post(this.getAllVacationTypeUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
  }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
}


//pagination for data
private getPagedData(page: Page, data: any): PagedData<VacationTypeSetupModel> {
debugger
  let pagedData = new PagedData<VacationTypeSetupModel>();
  if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
          for (let i = 0; i < gridRecords.length; i++) {
              let jsonObj = gridRecords[i];
              let department = new VacationTypeSetupModel(
                  jsonObj.id,
                  jsonObj.codeId,
                  jsonObj.descr,
                  jsonObj.arabicDescr,
                  jsonObj.days
              );
              pagedData.data.push(department);
          }
      }
  }
  page.totalPages = page.totalElements / page.size;
  let start = page.pageNumber * page.size;
  let end = Math.min((start + page.size), page.totalElements);
  pagedData.page = page;
  return pagedData;
}

//error handler
private handleError(error: any): Promise<any> {
  return Promise.reject(error.message || error);
}
}

