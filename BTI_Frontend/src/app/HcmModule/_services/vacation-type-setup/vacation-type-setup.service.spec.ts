import { TestBed, inject } from '@angular/core/testing';

import { VacationTypeSetupService } from './vacation-type-setup.service';

describe('VacationTypeSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VacationTypeSetupService]
    });
  });

  it('should be created', inject([VacationTypeSetupService], (service: VacationTypeSetupService) => {
    expect(service).toBeTruthy();
  }));
});
