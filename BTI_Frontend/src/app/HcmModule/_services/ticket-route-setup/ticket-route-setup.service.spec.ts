import { TestBed, inject } from '@angular/core/testing';

import { TicketRouteSetupService } from './ticket-route-setup.service';

describe('TicketRouteSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketRouteSetupService]
    });
  });

  it('should be created', inject([TicketRouteSetupService], (service: TicketRouteSetupService) => {
    expect(service).toBeTruthy();
  }));
});
