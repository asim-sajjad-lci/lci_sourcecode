import { Injectable } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';
import { TicketRouteSetupModel } from 'app/HcmModule/_models/ticket-route-setup/ticket-route-setup';
import { Observable } from 'rxjs';
import { PagedData } from 'app/_sharedresource/paged-data';
import { Page } from 'app/_sharedresource/page';
import { Headers, Http } from '@angular/http';

@Injectable()
export class TicketRouteSetupService {

  private headers = new Headers({ 'content-type': 'application/json' });
    private getAllTicketRouteUrl = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/getAll';
    private createTicketRouteUrl = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/create';
    private getTicketRouteByIdUrl = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/getById';
    private updateTicketRouteUrl = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/update';
    private deleteTicketRouteUrl = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/delete';
    private checkTicketRouteIdx = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/idCheck';
	private getTicketRouteSetup = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/getAllDropDown';

  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
}

//add new TicketRoute
createTicketRoute(data: TicketRouteSetupModel) {
    return this.http.post(this.createTicketRouteUrl, JSON.stringify(data), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//update for edit TicketRoute
updateTicketRoute(data: TicketRouteSetupModel) {
    return this.http.post(this.updateTicketRouteUrl, JSON.stringify(data), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//delete TicketRoute
deleteTicketRoute(ids: any) {
    return this.http.put(this.deleteTicketRouteUrl, { 'delIds': ids }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

getTicketRoutes(){
 return this.http.get(this.getTicketRouteSetup, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);

}

//check for duplicate ID TicketRoute
checkDuplicateRouteId(codeId: any) {
    return this.http.post(this.checkTicketRouteIdx, { 'codeId': codeId }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//get TicketRoute detail by Id
getTicketRouteById(id: string) {
    return this.http.post(this.getTicketRouteByIdUrl, { id: id }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

//get list
getlist(page: Page, searchKeyword): Observable<PagedData<TicketRouteSetupModel>> {
    return this.http.post(this.getAllTicketRouteUrl, {
        'searchKeyword': searchKeyword,
        'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
}


//pagination for data
private getPagedData(page: Page, data: any): PagedData<TicketRouteSetupModel> {
  debugger
    let pagedData = new PagedData<TicketRouteSetupModel>();
    if (data) {
        var gridRecords = data.records;
        page.totalElements = data.totalCount;
        if (gridRecords && gridRecords.length > 0) {
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let department = new TicketRouteSetupModel(
                    jsonObj.id,
                    jsonObj.codeId,
                    jsonObj.descr,
                    jsonObj.arabicDescr
                );
                pagedData.data.push(department);
            }
        }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
}

//error handler
private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
}
}
