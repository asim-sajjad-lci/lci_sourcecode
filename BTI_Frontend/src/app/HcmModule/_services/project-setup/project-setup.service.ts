import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Http,Headers } from '@angular/http';
import { ProjectSetup } from '../../_models/project-setup/project-setup';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs';
import { Page } from '../../../_sharedresource/page';

@Injectable()
export class ProjectSetupService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private createProjectUrl = Constants.hcmModuleApiBaseUrl + 'projectSetup/create';
  private checkProjectIdx = Constants.hcmModuleApiBaseUrl + 'projectSetup/projectIdcheck';
  private getAllProjectUrl = Constants.hcmModuleApiBaseUrl + 'projectSetup/getAll';
  private deleteProjectUrl = Constants.hcmModuleApiBaseUrl + 'projectSetup/delete';
  private updateProjectUrl = Constants.hcmModuleApiBaseUrl + 'projectSetup/update';
  private getProjectByProjectIdUrl = Constants.hcmModuleApiBaseUrl + 'projectSetup/getProjectById';
  private getProjectId = Constants.hcmModuleApiBaseUrl + 'projectSetup/getAllProjectDropDown';


  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
  }

  //add new Project
  createProject(project: ProjectSetup) {
    return this.http.post(this.createProjectUrl, JSON.stringify(project), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //check for duplicate ID Project
  checkDuplicateProjectId(codeId: any) {
    return this.http.post(this.checkProjectIdx, { 'projectId': codeId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get list
  getlist(page: Page, searchKeyword): Observable<PagedData<ProjectSetup>> {
    return this.http.post(this.getAllProjectUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<ProjectSetup> {
    debugger;
    let pagedData = new PagedData<ProjectSetup>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let project = new ProjectSetup(
            jsonObj.id,
            jsonObj.projectId,
            jsonObj.projectName,
            jsonObj.projectArabicName,
            jsonObj.projectDescription,
            jsonObj.projectArabicDescription,
            jsonObj.startDate,
            jsonObj.endDate
          );
          pagedData.data.push(project);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }

  //delete Project
  deleteProject(ids: any) {
    return this.http.post(this.deleteProjectUrl, { 'ids': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //update for edit Project
  updateProject(project: ProjectSetup) {
    return this.http.post(this.updateProjectUrl, JSON.stringify(project), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get Project detail by Id
  getProject(projectId: string) {
    return this.http.post(this.getProjectByProjectIdUrl, { id: projectId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  getAllProject() {
    debugger;
    return this.http.get(this.getProjectId, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);

  }

  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
