import { TestBed, inject } from '@angular/core/testing';

import { DeductionCodeSetupService } from './deduction-code-setup.service';

describe('DeductionCodeSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeductionCodeSetupService]
    });
  });

  it('should be created', inject([DeductionCodeSetupService], (service: DeductionCodeSetupService) => {
    expect(service).toBeTruthy();
  }));
});
