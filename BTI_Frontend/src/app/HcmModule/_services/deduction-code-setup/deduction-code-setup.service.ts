import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { DeductionCodeSetup } from '../../_models/deduction-code-setup/deduction-code-setup.module';
                                    
@Injectable()
export class DeductionCodeSetupService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/getAll';
  private searchDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/search';
  private createDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/create';
  private getDeductionCodeByIdUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/getDeductionCodeById';
  private updateDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/update';
  private deleteDeductionCodeUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/delete';
  private checkDeductionCodeIdx = Constants.hcmModuleApiBaseUrl + 'deductionCode/deductionIdcheck';
  private deductioncodeIdUrl = Constants.hcmModuleApiBaseUrl + 'deductionCode/searchdiductionId';
  private getAllBasedOnPayCodeDropDownUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAllPaycodeforDeduction';
  private getAllAccountListUrl=Constants.hcmModuleApiBaseUrl + 'coaMainAccount/getAllData';

  private typeIdDetails = Constants.hcmModuleApiBaseUrl + 'typeFieldForCodes/getAllIds';
  //initializing parameter for constructor
  constructor(private http: Http) {
      var userData = JSON.parse(localStorage.getItem('currentUser'));
      this.headers.append('session', userData.session);
      this.headers.append('userid', userData.userId);
      var currentLanguage = localStorage.getItem('currentLanguage') ?
          localStorage.getItem('currentLanguage') : "1";
      this.headers.append("langid", currentLanguage);
      this.headers.append("tenantid", localStorage.getItem('tenantid'));
      console.log('Header: ', this.headers)
  }


  //add new Deduction Code
  createDeductionCode(deductionCodeSetup: DeductionCodeSetup) {
      // console.log(JSON.stringify(deductionCodeSetup));
      return this.http.post(this.createDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  //update for edit department
  updateDeductionCode(deductionCodeSetup: DeductionCodeSetup) {
      return this.http.post(this.updateDeductionCodeUrl, JSON.stringify(deductionCodeSetup), { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  //delete department
  deleteDeductionCode(ids: any) {
      return this.http.put(this.deleteDeductionCodeUrl, { 'ids': ids }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  //get department detail by Id
  getDeductionCode(deductionCodeId: any) {
      return this.http.post(this.getDeductionCodeByIdUrl, { id: deductionCodeId }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  //get department detail by Id
  getAllBasedOnPayCodeDropDown(payload: any) {
      return this.http.post(this.getAllBasedOnPayCodeDropDownUrl, payload, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }


  //get list
  getlist(page: Page, searchKeyword): Observable<PagedData<DeductionCodeSetup>> {
      return this.http.post(this.getAllDeductionCodeUrl, {
          'searchKeyword': searchKeyword,
          'pageNumber': page.pageNumber,
          'pageSize': page.size,
          'sortOn': page.sortOn,
          'sortBy': page.sortBy
      }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //get list by search keyword
  searchDeductionCodelist(page: Page, searchKeyword): Observable<PagedData<DeductionCodeSetup>> {
      return this.http.post(this.searchDeductionCodeUrl, {
          'searchKeyword': searchKeyword,
          'pageNumber': page.pageNumber,
          'pageSize': page.size
      }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  checkDuplicateDeductionCodeId(deductionCodeId: any) {
      return this.http.post(this.checkDeductionCodeIdx, { 'diductionId': deductionCodeId }, { headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);
  }

  //get MainAccountList
  getMainAccountList(){
    return this.http.post(this.getAllAccountListUrl, {}, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError); 
  }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<DeductionCodeSetup> {
      let pagedData = new PagedData<DeductionCodeSetup>();
      if (data) {
          var gridRecords = data.records;
          page.totalElements = data.totalCount;
          if (gridRecords && gridRecords.length > 0) {
              for (let i = 0; i < gridRecords.length; i++) {
                  let jsonObj = gridRecords[i];
                  let deductionCode = new DeductionCodeSetup(
                      jsonObj.id,
                      jsonObj.diductionId,
                      jsonObj.discription,
                      jsonObj.arbicDiscription,
                      jsonObj.startDate,
                      jsonObj.endDate,
                      jsonObj.frequency,
                      jsonObj.method,
                      jsonObj.amount,
                      jsonObj.percent,
                      jsonObj.transction,
                      jsonObj.inActive,                        
                      jsonObj.perPeriod,
                      jsonObj.perYear,
                      jsonObj.lifeTime,
                      jsonObj.dtoPayCode,
                      jsonObj.payFactor,
                      jsonObj.customDate,
                      jsonObj.noOfDays,
                      jsonObj.endDateDays,
                      jsonObj.deductionTypeId,
                      jsonObj.roundOf
                  );
                  pagedData.data.push(deductionCode);
              }
          }
      }
      page.totalPages = page.totalElements / page.size;
      let start = page.pageNumber * page.size;
      let end = Math.min((start + page.size), page.totalElements);
      pagedData.page = page;
      return pagedData;
  }

  //error handler
  private handleError(error: any): Promise<any> {
      return Promise.reject(error.message || error);
  }

  getDeductioncodeId(){
      return this.http.post(this.deductioncodeIdUrl,{'searchKeyword' : ''} ,{ headers: this.headers })
          .toPromise()
          .then(res => res.json())
          .catch(this.handleError);

  }

       //get type field detail by Id
       getTypeFieldDetails(typeid) {
          return this.http.post(this.typeIdDetails, { id: typeid }, { headers: this.headers })
              .toPromise()
              .then(res => res.json())
              .catch(this.handleError);
      }
      
}

