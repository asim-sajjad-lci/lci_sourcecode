import { TestBed, inject } from '@angular/core/testing';

import { BenefitCodeSetupService } from './benefit-code-setup.service';

describe('BenefitCodeSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BenefitCodeSetupService]
    });
  });

  it('should be created', inject([BenefitCodeSetupService], (service: BenefitCodeSetupService) => {
    expect(service).toBeTruthy();
  }));
});
