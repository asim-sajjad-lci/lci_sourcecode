import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { MiscellaneousSetup } from '../../_models/miscellaneous-setup/miscellaneous-setup';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs';
import { EmployeeMiscellaneousMaster } from 'app/HcmModule/_models/employee-master/employee-master';


@Injectable()
export class MiscellaneousSetupService {
  private headers = new Headers({ 'content-type': 'application/json' });
  private createMiscllaneousUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/create';
  private checkMisclaneousIdx = Constants.hcmModuleApiBaseUrl + 'miscellaneous/miscellaneousIdCheck';
  private getAllMiscellaneousUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/getAll';
  private deleteMiscellaneousUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/delete';
  private updateMiscllaneousUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/update';
  private getMiscellaneousByMiscellaneousIdUrl = Constants.hcmModuleApiBaseUrl + 'miscellaneous/getById';
  private getMiscellaneousId = Constants.hcmModuleApiBaseUrl + 'miscellaneous/getAllMiscellaneousDropDown';
  private getAllMiscellaneousForEmployee = Constants.hcmModuleApiBaseUrl + '/miscellaneous/getAllMiscellaneousForEmployee';
  private createMiscellaneousForEmployee = Constants.hcmModuleApiBaseUrl + '/miscellaneousMaintenance/create';
  private getAllMiscellaneousForEmployeeWithId = Constants.hcmModuleApiBaseUrl + '/miscellaneousMaintenance/getAllMiscellaneousForEmployeeWithId';



  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
      localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
  }

  //add new Miscellaneous
  createMiscellaneous(miscellaneous: MiscellaneousSetup) {
    return this.http.post(this.createMiscllaneousUrl, JSON.stringify(miscellaneous), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //check for duplicate ID Miscellaneous
  checkDuplicateMiscId(codeId: any) {
    return this.http.post(this.checkMisclaneousIdx, { 'codeId': codeId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get list
  getlist(page: Page, searchKeyword): Observable<PagedData<MiscellaneousSetup>> {
    return this.http.post(this.getAllMiscellaneousUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
  }

  //pagination for data
  private getPagedData(page: Page, data: any): PagedData<MiscellaneousSetup> {
    debugger;
    let pagedData = new PagedData<MiscellaneousSetup>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let miscellaneous = new MiscellaneousSetup(
            jsonObj.id,
            jsonObj.codeId,
            jsonObj.codeName,
            jsonObj.codeArabicName,
            jsonObj.dimension
          );
          pagedData.data.push(miscellaneous);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }

  //delete Miscellaneous
  deleteMiscellaneous(ids: any) {
    return this.http.post(this.deleteMiscellaneousUrl, { 'ids': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //update for edit Miscellaneous
  updateMiscellaneous(department: MiscellaneousSetup) {
    return this.http.post(this.updateMiscllaneousUrl, JSON.stringify(department), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  //get miscellaneous detail by Id
  getMiscellaneous(miscellaneousId: string) {
    return this.http.post(this.getMiscellaneousByMiscellaneousIdUrl, { id: miscellaneousId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  getAllMiscellaneous() {
    debugger;
    return this.http.get(this.getMiscellaneousId, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);

  }

  //get all miscellaneous and their values for employee master

  getAllMiscellaneousForEmploye(page: Page, empId): Observable<PagedData<EmployeeMiscellaneousMaster>> {
    return this.http.post(this.getAllMiscellaneousForEmployeeWithId, {
      'employeeId': empId,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': "",
      'sortBy': ""
    }, { headers: this.headers }).map(data => this.getPagedData2(page, data.json().result));
  }

   //pagination for data
   private getPagedData2(page: Page, data: any): PagedData<EmployeeMiscellaneousMaster> {
    debugger;
    let pagedData = new PagedData<EmployeeMiscellaneousMaster>();
    if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
        for (let i = 0; i < gridRecords.length; i++) {
          let jsonObj = gridRecords[i];
          let miscellaneous ={
            "id" :jsonObj.id,
            "include" : (jsonObj.masterId!=0),
            "masterId" :jsonObj.masterId,
            "codeId" :jsonObj.codeId,
            "codeName" :jsonObj.codeName,
            "valueIndexId" :jsonObj.valueIndexId,
            "dtoValuesList" :jsonObj.dtoValuesList,
            
          };
          pagedData.data.push(miscellaneous);
        }
      }
    }
    page.totalPages = page.totalElements / page.size;
    let start = page.pageNumber * page.size;
    let end = Math.min((start + page.size), page.totalElements);
    pagedData.page = page;
    return pagedData;
  }


    //add new Miscellaneous
    createMiscellaneousForEmployees(saveData) {
      return this.http.post(this.createMiscellaneousForEmployee, JSON.stringify(saveData), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

  //error handler
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
