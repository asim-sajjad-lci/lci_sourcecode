import { TestBed, inject } from '@angular/core/testing';

import { PositionPayCodeService } from './position-pay-code.service';

describe('PositionPayCodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PositionPayCodeService]
    });
  });

  it('should be created', inject([PositionPayCodeService], (service: PositionPayCodeService) => {
    expect(service).toBeTruthy();
  }));
});
