// import { Injectable } from '@angular/core';

// @Injectable()
// export class PositionClassService {

//   constructor() { }

// }
import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import {Constants} from '../../../_sharedresource/Constants';
import {PositionClassModule} from '../../_models/position-class/position-class.module';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Page} from '../../../_sharedresource/page';


@Injectable()
export class PositionClassService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllPositionClassUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAll';
    private searchPositionClassUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/searchPositionClass';
    private createPositionClassUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/create';
    private getPositionClassByPositionClassIdUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getPositionClassById';
    private updatePositionClassUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/update';
    private deletePositionClassUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/delete';
    private checkPositionClassIdx = Constants.hcmModuleApiBaseUrl + 'positionClass/positionClassIdcheck';
    private positionClassIdUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/searchAllPositionClassIds';
    private getPositionClassListUrl = Constants.hcmModuleApiBaseUrl + 'positionClass/getAllPostionClassId';
    

    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // get position class detail by Id
    getPositionClass(positionClassId: string) {
        return this.http.post(this.getPositionClassByPositionClassIdUrl, { id: positionClassId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // check for duplicate ID position class
    checkDuplicatePositionClassId(positionClassId: any) {
        return this.http.post(this.checkPositionClassIdx, { 'positionClassId': positionClassId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // add new position class
    createPositionClass(positionClass: PositionClassModule) {
        return this.http.post(this.createPositionClassUrl, JSON.stringify(positionClass), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit position class
    updatePositionClass(positionClass: PositionClassModule) {
        return this.http.post(this.updatePositionClassUrl, JSON.stringify(positionClass), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete position class
    deletePositionClass(ids: any) {
        return this.http.put(this.deletePositionClassUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    searchPositionClasslist(page: Page, searchKeyword): Observable<PagedData<PositionClassModule>> {
        return this.http.post(this.searchPositionClassUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PositionClassModule>> {
        return this.http.post(this.getAllPositionClassUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<PositionClassModule> {
        let pagedData = new PagedData<PositionClassModule>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let positionClass = new PositionClassModule(
                        jsonObj.id,
                        jsonObj.positionClassId,
                        jsonObj.positionClassDescription,
                        jsonObj.arabicPositionClassDescription
                    );
                    pagedData.data.push(positionClass);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    getPositionClassId(){
        return this.http.post(this.positionClassIdUrl, {'searchKeyword' : ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }


      // getting Position Class list
      getPositionClassList() {
        return this.http.get(this.getPositionClassListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

}
