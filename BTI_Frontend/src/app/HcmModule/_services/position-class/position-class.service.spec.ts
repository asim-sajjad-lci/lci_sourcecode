import { TestBed, inject } from '@angular/core/testing';

import { PositionClassService } from './position-class.service';

describe('PositionClassService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PositionClassService]
    });
  });

  it('should be created', inject([PositionClassService], (service: PositionClassService) => {
    expect(service).toBeTruthy();
  }));
});




