import { TestBed, inject } from '@angular/core/testing';

import { PositionSetupService } from './position-setup.service';

describe('PositionSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PositionSetupService]
    });
  });

  it('should be created', inject([PositionSetupService], (service: PositionSetupService) => {
    expect(service).toBeTruthy();
  }));
});
