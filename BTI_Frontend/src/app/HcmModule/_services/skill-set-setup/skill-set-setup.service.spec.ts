import { TestBed, inject } from '@angular/core/testing';

import { SkillSetSetupService } from './skill-set-setup.service';

describe('SkillSetSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SkillSetSetupService]
    });
  });

  it('should be created', inject([SkillSetSetupService], (service: SkillSetSetupService) => {
    expect(service).toBeTruthy();
  }));
});
