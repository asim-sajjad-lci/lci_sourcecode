import { TestBed, inject } from '@angular/core/testing';

import { DepartmentSetupService } from './department-setup.service';

describe('DepartmentSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DepartmentSetupService]
    });
  });

  it('should be created', inject([DepartmentSetupService], (service: DepartmentSetupService) => {
    expect(service).toBeTruthy();
  }));
});
