import { TestBed, inject } from '@angular/core/testing';

import { PaycodeSetupService } from './paycode-setup.service';

describe('PaycodeSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PaycodeSetupService]
    });
  });

  it('should be created', inject([PaycodeSetupService], (service: PaycodeSetupService) => {
    expect(service).toBeTruthy();
  }));
});
