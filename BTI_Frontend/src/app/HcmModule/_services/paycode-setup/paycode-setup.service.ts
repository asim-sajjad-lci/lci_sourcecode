// import { Injectable } from '@angular/core';

// @Injectable()
// export class PaycodeSetupService {

//   constructor() { }

// }
import { Injectable } from '@angular/core';
import {Constants} from '../../../_sharedresource/Constants';
import {Headers, Http} from '@angular/http';
import {Page} from '../../../_sharedresource/page';
import {PagedData} from '../../../_sharedresource/paged-data';
import {Observable} from 'rxjs/Rx';
import {PaycodeSetupModule} from '../../_models/paycode-setup/paycode-setup.module';

@Injectable()
export class PaycodeSetupService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllPaycodeUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getAll';
    private searchPaycodeUrl = Constants.hcmModuleApiBaseUrl + 'payCode/search';
    private createPaycodeUrl = Constants.hcmModuleApiBaseUrl + 'payCode/create';
    private getSupervisorBySupervisorIdUrl = Constants.hcmModuleApiBaseUrl + 'location/getLocationDetailByLocationId';
    private updatePaycodeUrl = Constants.hcmModuleApiBaseUrl + 'payCode/update';
    private deletePaycodeUrl = Constants.hcmModuleApiBaseUrl + 'payCode/delete';
    private checkPaycodeIdx = Constants.hcmModuleApiBaseUrl + 'payCode/payCodeIdcheck';
    private getgetPayTypeDropdown = Constants.hcmModuleApiBaseUrl + 'payCodeType/searchIds';
    private getgetBaseOnPayCodeDropdown = Constants.hcmModuleApiBaseUrl + 'payCode/searchPayCodeId';
    private getbaseonpaycode=Constants.hcmModuleApiBaseUrl + 'payCode/searchAllPayCodeId';
    //private getgetBaseOnPayCodeDescDropdown = Constants.hcmModuleApiBaseUrl + 'payCode/getById';
    private paycodeIdUrl = Constants.hcmModuleApiBaseUrl + 'payCode/getById';
    private paycodeDetails = Constants.hcmModuleApiBaseUrl + 'payCode/searchAllPayCodeId';
    private typeIdDetails = Constants.hcmModuleApiBaseUrl + 'typeFieldForCodes/getAllIds';
    private getAllAccountListUrl=Constants.hcmModuleApiBaseUrl + 'coaMainAccount/getAllData';

    //initializing parameter for constructor
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
        console.log('Header: ', this.headers)
    }


    //add new location
    createPaycode(paycode: PaycodeSetupModule) {
        console.log('paycode in service', paycode)
        return this.http.post(this.createPaycodeUrl, JSON.stringify(paycode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //update for edit department
    updatePaycode(paycode: PaycodeSetupModule) {
        return this.http.post(this.updatePaycodeUrl, JSON.stringify(paycode), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //delete department
    deletePaycode(ids: any) {
        return this.http.put(this.deletePaycodeUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //check for duplicate ID department
    checkDuplicatePayCode(payCodeId: any) {
        return this.http.post(this.checkPaycodeIdx, { 'payCodeId': payCodeId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get department detail by Id
    getSupervisor(superVisionCode: string) {
        return this.http.post(this.getSupervisorBySupervisorIdUrl, { superVisionCode: superVisionCode }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PaycodeSetupModule>> {
        return this.http.post(this.getAllPaycodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    //get list by search keyword
    searchPaycodelist(page: Page, searchKeyword): Observable<PagedData<PaycodeSetupModule>> {
        return this.http.post(this.searchPaycodeUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }
	
	getPayTypeDropdown() {
		return this.http.post(this.getgetPayTypeDropdown, { "searchKeyword": "" }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
       
    }
	getBaseOnPayCodeDropdown(ignorid = 0) {
		return this.http.post(this.getgetBaseOnPayCodeDropdown, { 
			"id":ignorid,	
			"searchKeyword": "",
			"pageNumber": 0,
			"pageSize": 5
			}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
       
    }
    getbaseonPaycode(){
        return this.http.post(this.getbaseonpaycode,{"searchKeyword": ""
    },{headers:this.headers})
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }
	getPayCodeById(id:any){
		return this.http.post(this.paycodeIdUrl, { "id": id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
	}

    //pagination for data
    private getPagedData(page: Page, data: any): PagedData<PaycodeSetupModule> {
        let pagedData = new PagedData<PaycodeSetupModule>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let supervisor = new PaycodeSetupModule(
                        jsonObj.id,
                        jsonObj.payCodeId,
                        jsonObj.description,
                        jsonObj.arbicDescription,
                        jsonObj.payCodeTypeId,
                        jsonObj.baseOnPayCode,
                        jsonObj.baseOnPayCode1,
                        jsonObj.baseOnPayCodeAmount,
                        jsonObj.payFactor,
                        jsonObj.payRate,
                        jsonObj.unitofPay,
                        jsonObj.payperiod,
                        jsonObj.inActive,
                        jsonObj.payCodeTypeDesc,
                        jsonObj.baseOnPayCodeId,
                        );
                    pagedData.data.push(supervisor);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    // getPaycodeId(){
    //     return this.http.post(this.paycodeIdUrl, {'searchKeyword' : ''}, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);

    // }
    
    getPaycodeDetails(){
        return this.http.post(this.paycodeDetails, {'searchKeyword' : ''}, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

      //get type field detail by Id
      getTypeFieldDetails(typeid) {
        return this.http.post(this.typeIdDetails, { id: typeid }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


     //get MainAccountList
  getMainAccountList(){
    return this.http.post(this.getAllAccountListUrl, {}, { headers: this.headers })
    .toPromise()
    .then(res => res.json())
    .catch(this.handleError); 
  }

}