import { Injectable } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';
import { Division } from "../../_models/division/division";
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { PagedData } from 'app/_sharedresource/paged-data';
import { Page } from 'app/_sharedresource/page';

@Injectable()
export class DivionService {
 

  private headers = new Headers({ 'content-type': 'application/json' });
  private getAllCitiesByStateIdUrl = Constants.hcmModuleApiBaseUrl + 'hrCity/getCityByState';
  private getAllCountriesUrl = Constants.hcmModuleApiBaseUrl + 'hrCountry/getCountryList';
  private getAllStatesByCountryIdUrl = Constants.hcmModuleApiBaseUrl + 'hrState/getStateByCountry';
  private getDivisionByDivisionIdUrl = Constants.hcmModuleApiBaseUrl + 'division/getDivisionById';
  private getAllDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/getAll';
  private checkDivisionIdx = Constants.hcmModuleApiBaseUrl + 'division/divisionIdcheck';
  private createDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/create';
  private updateDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/update';
  //private getCountryListUrl = Constants.hcmModuleApiBaseUrl + 'division/getCountryList';
  private deleteDivisionUrl = Constants.hcmModuleApiBaseUrl + 'division/delete';
  private getAllDivisionDropDownURL = Constants.hcmModuleApiBaseUrl + 'division/getAllDivisionDropDown';

 //initializing parameter for constructor
 constructor (private http: Http) {
  console.log("new Company Service");
  if(localStorage.getItem('currentUser')){
      console.log("test");
  var userData = JSON.parse(localStorage.getItem('currentUser'));
  this.headers.append('session', userData.session);
  this.headers.append('userid', userData.userId);
  var currentLanguage = localStorage.getItem('currentLanguage') ?
  localStorage.getItem('currentLanguage') : "1";
  this.headers.append("langid", currentLanguage);
  this.headers.append("tenantid", localStorage.getItem('tenantid'));
}
}
  
  getlist(page: Page, searchKeyword): Observable<PagedData<Division>> {
    debugger;
    return this.http.post(this.getAllDivisionUrl, {
        'searchKeyword': searchKeyword,
        'pageNumber': page.pageNumber,
        'pageSize': page.size,
        'sortOn': page.sortOn,
        'sortBy': page.sortBy
    }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
}
  // getAllDivisionUrl(getAllDivisionUrl: any, arg1: { 'searchKeyword': any; 'pageNumber': any; 'pageSize': any; 'sortOn': any; 'sortBy': any; }, arg2: { headers: any; }) {
  //   throw new Error("Method not implemented.");
  // }
getCountry() {
  return this.http.get(this.getAllCountriesUrl, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}
getCitiesByStateId(stateId: number) {
  return this.http.post(this.getAllCitiesByStateIdUrl, { stateId: stateId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}
getStatesByCountryId(countryId: number) {
  return this.http.post(this.getAllStatesByCountryIdUrl, { countryId: countryId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}
  getDivision(divisionId: any) {
    return this.http.post(this.getDivisionByDivisionIdUrl, { id: divisionId }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
}

// getting Division Class list
getDivisionClassList() {
  debugger;
  return this.http.get(this.getAllDivisionDropDownURL, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}
//delete division
deleteDivision(ids: any) {
  return this.http.put(this.deleteDivisionUrl, { 'ids': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//add new division
createDivision(division: Division) {
  return this.http.post(this.createDivisionUrl, JSON.stringify(division), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}
checkDivisionId(divisionId: any) {
  return this.http.post(this.checkDivisionIdx, { 'divisionId': divisionId }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}
//update for edit division
updateDivision(division: Division) {
  return this.http.post(this.updateDivisionUrl, JSON.stringify(division), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}
private handleError(error: any): Promise<any> {
  return Promise.reject(error.message || error);
}
//pagination for data
private getPagedData(page: Page, data: any): PagedData<Division> {
  let pagedData = new PagedData<Division>();
  if (data) {
      var gridRecords = data.records;

      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
          for (let i = 0; i < gridRecords.length; i++) {
              let jsonObj = gridRecords[i];
              let division = new Division(
                  jsonObj.id,
                  jsonObj.divisionId,
                  jsonObj.divisionDescription,
                  jsonObj.arabicDivisionDescription,
                  jsonObj.divisionAddress,
                  jsonObj.phoneNumber,
                  jsonObj.fax,
                  jsonObj.email,
                  jsonObj.cityName,
                  jsonObj.countryName,
                  jsonObj.stateName,
                  jsonObj.cityId,
                  jsonObj.countryId,
                  jsonObj.stateId,
              );
              pagedData.data.push(division);
          }
      }
  }
  page.totalPages = page.totalElements / page.size;
  let start = page.pageNumber * page.size;
  let end = Math.min((start + page.size), page.totalElements);
  pagedData.page = page;
  return pagedData;
}

  

}
