import { TestBed, inject } from '@angular/core/testing';

import { DivionService } from './division.service';

describe('DivionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DivionService]
    });
  });

  it('should be created', inject([DivionService], (service: DivionService) => {
    expect(service).toBeTruthy();
  }));
});
