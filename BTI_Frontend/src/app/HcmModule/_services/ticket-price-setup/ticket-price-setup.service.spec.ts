import { TestBed, inject } from '@angular/core/testing';

import { TicketPriceSetupService } from './ticket-price-setup.service';

describe('TicketPriceSetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketPriceSetupService]
    });
  });

  it('should be created', inject([TicketPriceSetupService], (service: TicketPriceSetupService) => {
    expect(service).toBeTruthy();
  }));
});
