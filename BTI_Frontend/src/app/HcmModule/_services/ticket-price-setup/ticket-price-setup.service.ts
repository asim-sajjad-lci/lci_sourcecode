import { Injectable } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';
import { Headers, Http } from '@angular/http';
import { TicketPriceSetupModel } from 'app/HcmModule/_models/ticket-price-setup/ticket-price-setup';
import { Page } from 'app/_sharedresource/page';
import { Observable } from 'rxjs';
import { PagedData } from 'app/_sharedresource/paged-data';

@Injectable()
export class TicketPriceSetupService {

  private headers = new Headers({ 'content-type': 'application/json' });
  private getTicketClassSetup = Constants.hcmModuleApiBaseUrl + 'ticketClassSetup/getAllDropDown';
  private getTicketRouteSetup = Constants.hcmModuleApiBaseUrl + 'ticketRouteSetup/getAllDropDown';
  private createTicketPriceUrl = Constants.hcmModuleApiBaseUrl + 'ticketPricesSetup/create';
  private updateTicketPriceUrl = Constants.hcmModuleApiBaseUrl + 'ticketPricesSetup/update';
  private getAllTicketPriceUrl = Constants.hcmModuleApiBaseUrl + 'ticketPricesSetup/getAll';
  private deleteTicketPriceUrl = Constants.hcmModuleApiBaseUrl + 'ticketPricesSetup/delete';
  private getTicketPriceByIdUrl = Constants.hcmModuleApiBaseUrl + 'ticketPricesSetup/getById';
  
  constructor(private http: Http) {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
    this.headers.append("langid", currentLanguage);
    this.headers.append("tenantid", localStorage.getItem('tenantid'));
}

//get all Ticket Class
getTicketClasses(){
  return this.http.get(this.getTicketClassSetup, { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
 
 }

 //get all Ticket Routes
 getTicketRoutes(){
  return this.http.get(this.getTicketRouteSetup, { headers: this.headers })
         .toPromise()
         .then(res => res.json())
         .catch(this.handleError);
 
 }

 //add new TicketPrice
createTicketPrice(data: TicketPriceSetupModel) {
  console.log(data);
  console.log(JSON.stringify(data));
  return this.http.post(this.createTicketPriceUrl, JSON.stringify(data), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//update for edit TicketPrice
updateTicketPrice(data: TicketPriceSetupModel) {
  return this.http.post(this.updateTicketPriceUrl, JSON.stringify(data), { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//delete TicketPrice
deleteTicketPrice(ids: any) {
  return this.http.put(this.deleteTicketPriceUrl, { 'delIds': ids }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}

//get list
getlist(page: Page, searchKeyword): Observable<PagedData<TicketPriceSetupModel>> {
  return this.http.post(this.getAllTicketPriceUrl, {
      'searchKeyword': searchKeyword,
      'pageNumber': page.pageNumber,
      'pageSize': page.size,
      'sortOn': page.sortOn,
      'sortBy': page.sortBy
  }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
}

//get TicketPrice detail by Id
getTicketPriceById(id: string) {
  return this.http.post(this.getTicketPriceByIdUrl, { id: id }, { headers: this.headers })
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
}


//pagination for data
private getPagedData(page: Page, data: any): PagedData<TicketPriceSetupModel> {
debugger
  let pagedData = new PagedData<TicketPriceSetupModel>();
  if (data) {
      var gridRecords = data.records;
      page.totalElements = data.totalCount;
      if (gridRecords && gridRecords.length > 0) {
          for (let i = 0; i < gridRecords.length; i++) {
              let jsonObj = gridRecords[i];
              let department = new TicketPriceSetupModel(
                  jsonObj.id,
                  jsonObj.adultTckRate,
                  jsonObj.childTckRate,
                  jsonObj.infacntTckRate,
                  jsonObj.ticketRouteId,
                  jsonObj.ticketClassId,
                  jsonObj.ticketRouteCodeId,
                  jsonObj.ticketClassCodeId
              );
              pagedData.data.push(department);
          }
      }
  }
  page.totalPages = page.totalElements / page.size;
  let start = page.pageNumber * page.size;
  let end = Math.min((start + page.size), page.totalElements);
  pagedData.page = page;
  return pagedData;
}

 //error handler
private handleError(error: any): Promise<any> {
  return Promise.reject(error.message || error);
}

}
