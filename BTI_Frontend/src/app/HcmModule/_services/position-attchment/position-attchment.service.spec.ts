import { TestBed, inject } from '@angular/core/testing';

import { PositionAttchmentService } from './position-attchment.service';

describe('PositionAttchmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PositionAttchmentService]
    });
  });

  it('should be created', inject([PositionAttchmentService], (service: PositionAttchmentService) => {
    expect(service).toBeTruthy();
  }));
});
