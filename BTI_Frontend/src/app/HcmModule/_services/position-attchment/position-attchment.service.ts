import { Injectable } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Headers, Http, RequestOptions, ResponseContentType } from '@angular/http';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Observable } from 'rxjs/Rx';
import { PositionAttachmentModule } from 'app/HcmModule/_models/position-attachment/position-attachment';


@Injectable()
export class PositionAttchmentService {

  private headers = new Headers({ 'content-type': 'application/json' });
    private positionURL = 'positionAttachmentSetup';
    private getAllPositionAttachUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAll';
    private getAttachmentByIdUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/attachment';
    private searchPositionAttachUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/searchPositionSetup';
    private createPositionAttachUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/createPosition';
    private getAllAttachmentByPositionIdIdUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/getAllAttachmentByPositionId';
    private updatePositionAttachUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/update';
    private deletePositionAttachUrl = Constants.hcmModuleApiBaseUrl + this.positionURL + '/delete';
    //private checkPositionAttachIdx = Constants.hcmModuleApiBaseUrl + this.positionURL + '/positionPlanIdcheck';


    constructor(private http: Http) {
        let userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        let currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : '1';
        this.headers.append('langid', currentLanguage);
        this.headers.append('tenantid', localStorage.getItem('tenantid'));
    }

    // getAttachmentById(id: number) {
    //     return this.http.put(this.getAttachmentByIdUrl, { id: id }, { headers: this.headers })
    //         .toPromise()
    //         .then(res => res.json())
    //         .catch(this.handleError);
    // }

    getAttachmentById(id: number) {
        return this.http.get(this.getAttachmentByIdUrl + "/" + id, { headers: this.headers, responseType: ResponseContentType.Blob })
            .map((res) => {               
                //const contentDisposition = res.headers.get('content-disposition') || '';              
                // const matches = /filename=([^;]+)/ig.exec(contentDisposition);
                // console.log(matches);
                // const fileName = (matches[1] || 'untitled').trim();
                // console.log(fileName);                
                //return new Blob([res.blob()], { type: 'application/octet-stream' })
                return res.url;
            })
            .catch(this.handleError)
    }

    download(id:number, name:string){
        this.getAttachmentById(id).subscribe(data => {
            this.http.get(data, { headers: this.headers, responseType: ResponseContentType.Blob }).subscribe((response:any) => {
                let blob = new Blob([response._body], { type: 'text/csv' });
                var url= window.URL.createObjectURL(blob);

                var anchor = document.createElement("a");
                anchor.download = name;
                anchor.href = url;
                anchor.click();
                //window.open(url);
            });
            
        });
    }

    getFile(id: number,ftype: string):Observable<Blob>{   
        /* var b:Blob;
        return this.getAttachmentById(id).subscribe(data => {          
            return this.http.get(data, { headers: this.headers, responseType: ResponseContentType.Blob }).map((response:any) => {
                return b = new Blob([response.blob()]);
            });
        });  */ 
        return this.http.get(this.getAttachmentByIdUrl + "/" + id, { headers: this.headers, responseType: ResponseContentType.Blob })
            .map((res) => {               
                //const contentDisposition = res.headers.get('content-disposition') || '';              
                // const matches = /filename=([^;]+)/ig.exec(contentDisposition);
                // console.log(matches);
                // const fileName = (matches[1] || 'untitled').trim();
                // console.log(fileName);  
                if(ftype == "png" || ftype == "jpg" || ftype == "jpeg")
                    ftype= "image/"+ftype;          
                else
                    ftype= "application/"+ftype; 
                return new Blob([res.blob()], { type: ftype});
            })
            .catch(this.handleError)     
    }

    // get position class detail by Id
    // getPositionClass(positionIds: number): Observable<any> {
    //     return this.http.put(this.getAllAttachmentByPositionIdIdUrl,  { positionIds: positionIds },{ headers: this.headers})
    //         .map((data) => {               
    //             console.log("Position dfgsdfgsdfg sdf",data.json().result.records);
    //            return data.json().result.records;
    //         })

    //     /**/
     
    // }

    // getPositionClass(page, positionIds: number){
    //     return this.http.put(this.getAllAttachmentByPositionIdIdUrl, { 
    //         positionIds: positionIds ,
    //         pageNumber: page.pageNumber,  
    //         pageSize:page.pageSize}, { headers: this.headers })
    //     .toPromise()
    //     .then(res => res.json())
    //     .catch(this.handleError);
    // }

    getFileAttachedPositionClass(page:Page, positionIds: any, searchKeyword ){
       
        return this.http.put(this.getAllAttachmentByPositionIdIdUrl, {
            'id':positionIds,
            'searchKeyword': searchKeyword,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy,
            'pageNumber': page.pageNumber,
            'pageSize':  page.size
            
        }, { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }


    createPositionAttachment(positionPlanSetup: PositionAttachmentModule, file: File) {

        console.log("file definition..",positionPlanSetup)
        let formData: FormData = new FormData();
        formData.append('file', file, file.name);
        formData.append('DtoPositionSetup', JSON.stringify(positionPlanSetup));
        this.headers.delete('Content-Type');

        return this.http.post(this.createPositionAttachUrl, formData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // update for edit position class
    updatePositionAttachment(positionPlanSetup: PositionAttachmentModule, file: File) {
        var prop = "$$index";
        delete positionPlanSetup[prop];
        console.log(positionPlanSetup);
        let formData: FormData = new FormData();
        formData.append('file', file, file.name);
        formData.append('DtoPositionSetup', JSON.stringify(positionPlanSetup));
        this.headers.delete('Content-Type');

        return this.http.post(this.updatePositionAttachUrl, formData, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // delete position class
    deletePositionAttachment(ids: any) {
        return this.http.put(this.deletePositionAttachUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // get list by search keyword
    // searchPositionAttachmentlist(page: Page, searchKeyword): Observable<PagedData<PositionAttachmentModule>> {
    //     return this.http.post(this.searchPositionAttachUrl, {
    //         'searchKeyword': searchKeyword,
    //         'pageNumber': page.pageNumber,
    //         'pageSize': page.size,
    //         'sortOn': page.sortOn,
    //         'sortBy': page.sortBy
    //     }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    // }

    // get list
    getlist(page: Page, searchKeyword): Observable<PagedData<PositionAttachmentModule>> {
        return this.http.post(this.getAllPositionAttachUrl, {
            'searchKeyword': searchKeyword,
            'pageNumber': page.pageNumber,
            'pageSize': page.size,
            'sortOn': page.sortOn,
            'sortBy': page.sortBy
        }, { headers: this.headers }).map(data => this.getPagedData(page, data.json().result));
    }

    // pagination for data
    private getPagedData(page: Page, data: any): PagedData<PositionAttachmentModule> {
        let pagedData = new PagedData<PositionAttachmentModule>();
        if (data) {
            let gridRecords = data.records;
            page.totalElements = data.totalCount;
            if (gridRecords && gridRecords.length > 0) {
                for (let i = 0; i < gridRecords.length; i++) {
                    let jsonObj = gridRecords[i];
                    let positionAttachment = new PositionAttachmentModule(
                        jsonObj.id,
                        jsonObj.positionIds,
                        jsonObj.documentAttachmenDesc,
                        jsonObj.attachmentType
                    );
                    pagedData.data.push(positionAttachment);
                }
            }
        }
        page.totalPages = page.totalElements / page.size;
        let start = page.pageNumber * page.size;
        let end = Math.min((start + page.size), page.totalElements);
        pagedData.page = page;
        return pagedData;
    }

    // error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

}
