
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute, Params } from '@angular/router';


import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {TypeaheadMatch} from 'ngx-bootstrap/typeahead';

import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { SkillsSetupService } from 'app/HcmModule/_services/skill-setup/skill-setup.service';
import { SkillsSetup } from 'app/HcmModule/_models/skill-setup/skill-setup';
import { DropDownModule } from 'app/_sharedresource/_modules/drop-down.module';

@Component({
  selector: 'app-create-skills-setup',
  templateUrl: './create-skills-setup.component.html',
  styleUrls: ['./create-skills-setup.component.css'],
  providers: [SkillsSetupService,CommonService]
})
export class CreateSkillsSetupComponent implements OnInit {

  page = new Page();
    rows = new Array<SkillsSetup>();
    temp = new Array<SkillsSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1411';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    skillId = {};
    searchKeyword = '';
    ddPageSize: number = 5;
  //model: Division;
    model: any = {};
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    
    hasMsg = false;
    showMsg = false;
	tempp:string[]=[];
	islifeTimeValid: boolean = true;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    skillIdvalue: number;
    frequencyArray = ["Hourly","Weekly","Biweekly","Semimonthly","Monthly","Quarterly","Semianually","Anually"];
    // label variable
    SKILLS_SETUP_SEARCH:any;
    SKILLS_SETUP_ID:any;
    SKILLS_SETUP_DESCRIPTION:any;
    SKILLS_SETUP_ARABIC_DESCRIPTION:any;
    SKILLS_SETUP_CONTACT_NAME:any;
    SKILLS_SETUP_FREQUENCY:any;
    SKILLS_SETUP_COMPENSATION:any;
    SKILLS_SETUP_ACTION:any;
    SKILLS_SETUP_CREATE_LABEL:any;
    SKILLS_SETUP_SAVE_LABEL:any;
    SKILLS_SETUP_CLEAR_LABEL:any;
    SKILLS_SETUP_CANCEL_LABEL:any;
    SKILLS_SETUP_UPDATE_LABEL:any;
    SKILLS_SETUP_DELETE_LABEL:any;
    SKILLS_SETUP_CREATE_FORM_LABEL:any;
    SKILLS_SETUP_UPDATE_FORM_LABEL:any;
    SKILLS_SETUP_TABLEVIEW:any;



    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    frequencies: DropDownModule[] = [
        { id: 1, name: 'Hourly' },
        { id: 2, name: 'Weekly' },
        { id: 3, name: 'Biweekly' },
        { id: 4, name: 'Semimonthly' },
        { id: 5, name: 'Monthly' },
        { id: 6, name: 'Quarterly' },
        { id: 7, name: 'Semiannually' },
        { id: 8, name: 'Annually' },
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    skillIdList: Observable<any>;
    getSkillId:any[]=[];
    typeaheadLoading: boolean;

    isSkillIdOpen=false;
    isFrequencyOpen=false;
    selectedSkill;
    selectedFrequency;

    constructor(private router: Router,
        private SkillsSetupService: SkillsSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private route: ActivatedRoute,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'SKILLS_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_CONTACT_NAME', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_FREQUENCY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_COMPENSATION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
            { 'fieldName': 'SKILLS_SETUP_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': ''  },
        ];

        this.SKILLS_SETUP_SEARCH = this.defaultFormValues[0];
        this.SKILLS_SETUP_ID = this.defaultFormValues[1];
        this.SKILLS_SETUP_DESCRIPTION = this.defaultFormValues[2];
        this.SKILLS_SETUP_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.SKILLS_SETUP_CONTACT_NAME = this.defaultFormValues[4];
        this.SKILLS_SETUP_FREQUENCY = this.defaultFormValues[5];
        this.SKILLS_SETUP_COMPENSATION = this.defaultFormValues[6];
        this.SKILLS_SETUP_ACTION = this.defaultFormValues[7];
        this.SKILLS_SETUP_CREATE_LABEL = this.defaultFormValues[8];
        this.SKILLS_SETUP_SAVE_LABEL = this.defaultFormValues[9];
        this.SKILLS_SETUP_CLEAR_LABEL = this.defaultFormValues[10];
        this.SKILLS_SETUP_CANCEL_LABEL = this.defaultFormValues[11];
        this.SKILLS_SETUP_UPDATE_LABEL = this.defaultFormValues[12];
        this.SKILLS_SETUP_DELETE_LABEL = this.defaultFormValues[13];
        this.SKILLS_SETUP_CREATE_FORM_LABEL = this.defaultFormValues[14];
        this.SKILLS_SETUP_UPDATE_FORM_LABEL = this.defaultFormValues[15];
        this.SKILLS_SETUP_TABLEVIEW = this.defaultFormValues[16];
        this.skillIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.skillId);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

    }

    ngOnInit() {


        this.route.params.subscribe((params: Params) => {
            debugger;
            this.skillId = params['skillId']; 
    
            if (this.skillId != '' && this.skillId != undefined) {
    
                this.getSkillById(this.skillId);
                this.isUnderUpdate=true;
                this.setAutocompleteLabels();
            }
    
        });




        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Following shifted from SetPage for better performance
        this.SkillsSetupService.getSkillId().then(data => {
            this.getSkillId = data.result.records;
           //console.log("Data class options : "+this.getSkillId);
        });
    }


    setAutocompleteLabels(){
        this.onAutocompleteFocus('lblSkillId');
        this.onAutocompleteFocus('lblFrequency');
    }
    getSuperviserIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getSkillId.filter((id: any) => {
                return query.test(id.skillId);
            })
        );
    }

    getRowValue (row, gridFieldName) {
        if (gridFieldName === 'frequency') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                        row[gridFieldName] === 7 ? 'Annually' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
       console.log('Selected value: ', e);
       this.SkillsSetupService.getSkillsSetup(e.item.id).then(pagedData => {
        console.log('pagedData: ', pagedData);
        this.model = pagedData.result;
        this.model.id = 0;

    });

    }

    setPage(pageInfo) {
        debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.page.size=0;
        this.SkillsSetupService.getSkillList().then(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.result.records;
            console.log("divisionids.........",this.rows);
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            skillId: null,
            skillDesc: '',
            arabicDesc: '',
            compensation: null,
            frequency: 0,
			frequencyName: '',
			
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        // f.resetForm();
		// f.form.controls.frequency.setValue(0);
        // console.log(f);		
        this.model=[];
        this.selectedSkill="";
        this.selectedFrequency="";
        
    }


    //function call for creating new location
    CreateSkillsSetup(f: NgForm, event: Event) {
        debugger;
        event.preventDefault();

        this.model.skillId=this.selectedSkill;
        this.model.frequency=this.selectedFrequency.id;
        var locIdx = this.model.skillId;

        //Check if the id is available in the model.
        //If id avalable then update the Deduction Code, else Add new Deduction Code.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Deduction Code Id according to it create new Deduction Code
            this.SkillsSetupService.checkDuplicateSkillsSetupId(locIdx).then(response => {
                if (response && response.code == 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                       
                    }, 100);
                } else {
                    //Call service api for Creating new Deduction Code
                    this.SkillsSetupService.createSkillSteup(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            this.goBack();
                            //Refresh the Grid data after adding new Deduction Code
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(event) {
   
   
        if(event.cellIndex!=0 && event.cellIndex!=1){
            this.skillId = event.row.id;
          
            var myurl = `${'createdivsionsetup'}/${this.skillId}`;
            this.router.navigateByUrl(myurl);
        }
    
       
    }

    updateStatus() {
        this.closeModal();
        debugger;
        this.model.skillId = this.selectedSkill;
        this.model.frequency=this.selectedFrequency.id;
        //Call service api for updating selected department
        this.SkillsSetupService.updateSkillsSetup(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedSkillsSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSkillsSetups.push(this.selected[i].id);
        }
        this.SkillsSetupService.deleteSkillsSetup(selectedSkillsSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.SkillsSetupService.searchSkillsSetuplist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    CheckNumber(event) {
        if (isNaN(event.target.value) == true) {
            //this.model.skillId = 0;
            this.model.compensation = 1;
            return false;
        }
    }
    CheckNumberNonZero(event) {
		if (event.target.value <= 0) {
            this.model.compensation = 1;
            return false;
        }
    }
    checkFrequency(event) {
		if (event.target.value == 0) {
            return false;
        }
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
    
    checkdecimal(digit)
    {
        console.log(digit);
        if(digit==null || digit==''){
            this.islifeTimeValid = true;
            return;
        }
        digit = digit.toString();
        this.tempp=digit.split(".");
        if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValid = false;
            console.log(this.islifeTimeValid);
        }
        else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
            console.log("in the condition");
            this.islifeTimeValid = false;
            console.log(this.islifeTimeValid);
        }

        else{
            this.islifeTimeValid = true;
            console.log(this.islifeTimeValid);
        }

    }



    onAutocompleteFocus(labelId){

        let elem: HTMLElement = document.getElementById(labelId);
        elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
      }
      
      onAutocompleteFocusOut(labelId,ngmodel){
          // let elem: HTMLElement = document.getElementById(labelId);
          // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
          
      }
     
      skillIdFocusout(){
             this.isSkillIdOpen=false;
     
    }
    
    skillIdFocusIn(){
        this.isSkillIdOpen=true;
    }
    
    frequencyFocusout(){
      debugger;
      this.isFrequencyOpen=false;
  
    }
    
    frequencyFocusIn(){
      this.isFrequencyOpen=true;
    }
    
   
    
    
    
      goBack(){
        this.router.navigate(['hcm/skillsSetup']); 
      }


      getSkillData(event){
   
        var skillId = event.id ? event.id : -1;
      
        this.model.skillId=skillId;
   
        if(event.id>0 && event.id!=undefined)
        {
            this.getSkillById(event.id);
        }
   
       this.setAutocompleteLabels();
      
       //  this.divisionService.getDivision(divId).then(data => {
       //     this.model.countryCode = data.result.countryCode;
       // });
       //   this.companyService.getStateList(countryId).then(data => {
       //       this.stateOptions = data.result;
       //       this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
       //   });
   }
    

   getSkillById(skillId){
    this.SkillsSetupService.getSkillsSetup(skillId).then(allData => {
debugger;

this.isUnderUpdate=true;
this.setAutocompleteLabels();
        console.log("selecteddivision.........",allData)
        this.model.id=allData.result.id;
        
    
        this.selectedSkill=allData.result.skillId;
        // setTimeout(() => {
            var Frequency=this.frequencies.filter(p => p.id == allData.result.frequency);
            this.selectedFrequency=Frequency[0].name;
            

        //   }, 1000);
     
        this.model = allData.result;


      
    });

  }

  EscapeModal(event){
        
    var key = event.key;
    if(key=="Escape"){
        this.closeModal();
    }
  }
}
