import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSkillsSetupComponent } from './create-skills-setup.component';

describe('CreateSkillsSetupComponent', () => {
  let component: CreateSkillsSetupComponent;
  let fixture: ComponentFixture<CreateSkillsSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSkillsSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSkillsSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
