import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { CreateSkillSetupRoutes } from './create-skill-setup.routing';
import { CreateSkillsSetupComponent } from './create-skills-setup.component';






@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateSkillSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
  ],

  providers:[AlertService],
  declarations: [CreateSkillsSetupComponent]
})
export class CreateSkillSetupModule { }
