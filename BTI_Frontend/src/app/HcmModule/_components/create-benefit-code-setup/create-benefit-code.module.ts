import { CreateBenefitCodeSetupRoutes } from './create-benefit-code-setup.routing';
import { CreateBenefitCodeSetupComponent } from './create-benefit-code-setup.component';
import { AlertService } from './../../../_sharedresource/_services/alert.service';
import { RouterModule } from '@angular/router';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import { SharedModule } from 'app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { TypeaheadModule } from 'ngx-bootstrap';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';



@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    AngularMultiSelectModule,
    RouterModule.forChild(CreateBenefitCodeSetupRoutes),
    NgxMyDatePickerModule.forRoot(),
  ],
  providers:[AlertService],
  declarations: [CreateBenefitCodeSetupComponent]
})
export class CreateBenefitCodeModule { }
