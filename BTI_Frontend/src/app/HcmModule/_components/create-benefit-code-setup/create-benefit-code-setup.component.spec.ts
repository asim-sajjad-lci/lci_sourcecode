import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBenefitCodeSetupComponent } from './create-benefit-code-setup.component';

describe('CreateBenefitCodeSetupComponent', () => {
  let component: CreateBenefitCodeSetupComponent;
  let fixture: ComponentFixture<CreateBenefitCodeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBenefitCodeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBenefitCodeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
