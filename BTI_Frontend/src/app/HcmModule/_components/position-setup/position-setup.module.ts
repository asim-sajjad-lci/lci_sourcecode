
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { PositionSetupRoutes } from './position-setup.routing';
import { PositionSetupComponent } from './position-setup.component';





@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PositionSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
  ],

  providers:[AlertService],
  declarations: [PositionSetupComponent]
})
export class PositionSetupModule { }
