
import { Routes } from '@angular/router';
import { PositionSetupComponent } from './position-setup.component';




export const PositionSetupRoutes: Routes = [{
  path: '',
  component: PositionSetupComponent,
  data: {
    breadcrumb: ""
  }
}];