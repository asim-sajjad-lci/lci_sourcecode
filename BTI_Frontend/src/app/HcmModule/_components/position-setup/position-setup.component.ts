
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Page } from '../../../_sharedresource/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Router } from '@angular/router';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';

import { PositionSetupService } from '../../_services/position-setup/position-setup.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

import { RoutesRecognized } from '@angular/router';
import * as cloneDeep from 'lodash/cloneDeep';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { PositionSetupModule } from 'app/HcmModule/_models/position-setup/position-setup';
import { PositionAttchmentService } from 'app/HcmModule/_services/position-attchment/position-attchment.service';
import { UploadFileService } from 'app/_sharedresource/_services/upload-file.service';
@Component({
  selector: 'app-position-setup',
  templateUrl: './position-setup.component.html',
  styleUrls: ['./position-setup.component.css'],
  providers: [PositionSetupService, PositionAttchmentService,CommonService,UploadFileService]
})
export class PositionSetupComponent implements OnInit {

  page = new Page();
  rows = new Array<PositionSetupModule>();
  temp = new Array<PositionSetupModule>();
  selected = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1313';
 
  moduleName;
  screenName;
  defaultFormValues: any = [];

  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  positionId = {};
  positionClassOptions = [];
  skillSetIdOptions = [];
  searchKeyword = '';
  ddPageSize: number = 5;
  model: PositionSetupModule;
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;

  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  positionIdvalue: string;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;
  dataSource: Observable<any>;
  asyncSelected: string;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;
  isAttached: boolean = false;
  uploadfile: any;
  getPositionID: any[] = [];
  positionIdList: Observable<any>;
  previousUrl: boolean = false;
  isReporttopositionValid:boolean=true;

          POSITION_SETUP_SEARCH: any;
          POSITION_SETUP_ID:any;
          POSITION_SETUP_DESCRIPTION:any;
          POSITION_SETUP_ARABIC_DESCRIPTION:any;
          POSITION_SETUP_REPORT_TO_POSITION:any;
          POSITION_CLASS:any;
          POSITION_SETUP_LONG_DESCRIPTION:any;
          POSITION_SETUP_DEFAULT_SKILL_ID:any;
          POSITION_SETUP_ACTION:any;
          POSITION_SETUP_CREATE_LABEL:any;
          POSITION_SETUP_SAVE_LABEL:any;
          POSITION_SETUP_CLEAR_LABEL:any;
          POSITION_SETUP_CANCEL_LABEL:any;
          POSITION_SETUP_UPDATE_LABEL:any;
          POSITION_SETUP_DELETE_LABEL:any;
          POSITION_SETUP_CREATE_FORM_LABEL:any;
          POSITION_SETUP_UPDATE_FORM_LABEL:any;
          POSITION_SETUP_TRAINING_FORM_LABEL:any;
          POSITION_SETUP_PLAN_FORM_LABEL:any;
          MANAGE_USER_TABLE_VIEW:any;
          MANAGE_USER_TABLE_ACCESS:any;
          MANAGE_USER_TABLE_DELETE:any;
          MANAGE_USER_TEXT_ADD_NEW_USER:any;
          MANAGE_USER_TABLE_STATE:any;
          MANAGE_USER_TABLE_CITY:any;
          MANAGE_USER_TABLE_POSTALCODE:any;
          MANAGE_USER_TABLE_DOB:any;
          POSITION_SETUP_REPORTS_TO_POSITION:any;


        

  constructor(
      private router: Router,
      private positionSetupService: PositionSetupService,
      private getScreenDetailService: GetScreenDetailService,
      private alertService: AlertService,
      private uploadFileService: UploadFileService,
      private commonService: CommonService,
      private positionAttchmentService: PositionAttchmentService
  ) {
      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortOn = 'id';
      this.page.sortBy = 'DESC';

      this.router.events.filter(e => e instanceof RoutesRecognized).pairwise().subscribe((e: any[]) => {
          console.log("E: ", e);

          if (e[0].urlAfterRedirects == "/hcm/positionAttachment" && e[1].urlAfterRedirects == "/hcm/positionSetup")
              this.uploadFileService.modelClear = false;
          else if (e[1].urlAfterRedirects == "/hcm/positionSetup")
              this.uploadFileService.modelClear = true;
      });
      
      // default form parameter for position  screen
      this.defaultFormValues = [
          { 'fieldName': 'POSITION_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_REPORT_TO_POSITION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_CLASS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_LONG_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_DEFAULT_SKILL_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_TRAINING_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_PLAN_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_SETUP_REPORTS_TO_POSITION', 'fieldValue': '', 'helpMessage': '' },
      ];

          this.POSITION_SETUP_SEARCH=this.defaultFormValues[0];
          this.POSITION_SETUP_ID=this.defaultFormValues[1];
          this. POSITION_SETUP_DESCRIPTION=this.defaultFormValues[2];
          this.POSITION_SETUP_ARABIC_DESCRIPTION=this.defaultFormValues[3];
          this.POSITION_SETUP_REPORT_TO_POSITION=this.defaultFormValues[4];
          this.POSITION_CLASS=this.defaultFormValues[5];
          this.POSITION_SETUP_LONG_DESCRIPTION=this.defaultFormValues[6];
          this.POSITION_SETUP_DEFAULT_SKILL_ID=this.defaultFormValues[7];
          this.POSITION_SETUP_ACTION=this.defaultFormValues[8];
          this.POSITION_SETUP_CREATE_LABEL=this.defaultFormValues[9];
          this.POSITION_SETUP_SAVE_LABEL=this.defaultFormValues[10];
          this.POSITION_SETUP_CLEAR_LABEL=this.defaultFormValues[11];
          this.POSITION_SETUP_CANCEL_LABEL=this.defaultFormValues[12];
          this.POSITION_SETUP_UPDATE_LABEL=this.defaultFormValues[13];
          this.POSITION_SETUP_DELETE_LABEL=this.defaultFormValues[14];
          this.POSITION_SETUP_CREATE_FORM_LABEL=this.defaultFormValues[15];
          this.POSITION_SETUP_UPDATE_FORM_LABEL=this.defaultFormValues[16];
          this.POSITION_SETUP_TRAINING_FORM_LABEL=this.defaultFormValues[17];
          this.POSITION_SETUP_PLAN_FORM_LABEL=this.defaultFormValues[18];
          this.MANAGE_USER_TABLE_VIEW=this.defaultFormValues[19];
          this.MANAGE_USER_TABLE_ACCESS=this.defaultFormValues[20];
          this.MANAGE_USER_TABLE_DELETE=this.defaultFormValues[21];
          this.MANAGE_USER_TEXT_ADD_NEW_USER=this.defaultFormValues[22];
          this.MANAGE_USER_TABLE_STATE=this.defaultFormValues[23];
          this.MANAGE_USER_TABLE_CITY=this.defaultFormValues[24];
          this.MANAGE_USER_TABLE_POSTALCODE=this.defaultFormValues[25];
          this.MANAGE_USER_TABLE_DOB=this.defaultFormValues[26];
          this.POSITION_SETUP_REPORTS_TO_POSITION=this.defaultFormValues[27];


      
     


      this.dataSource = Observable.create((observer: any) => {
          // Runs on every search
          observer.next(this.model.reportToPostion);
      }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));

      this.positionIdList = Observable.create((observer: any) => {
          // Runs on every search
          observer.next(this.model.positionId);
      }).mergeMap((token: string) => this.getReportPositionIdAsObservable(token));
  }

  ngOnInit() {
      console.log("PD After Attached:", this.uploadFileService.positionAttchModelData);
      console.log("PD:", this.uploadFileService);
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      if (this.uploadFileService.positionModelData && !this.uploadFileService.modelClear) {
          this.showCreateForm = true;
          this.isUnderUpdate = true;
          this.positionIdvalue = this.uploadFileService.positionModelData.positionId;
          this.model = this.uploadFileService.positionModelData;
      } else {
          this.showCreateForm = false
          this.isUnderUpdate = false;
          
      }
      this.currentLanguage = localStorage.getItem('currentLanguage');

      // getting screen labels, help messages and validation messages
      this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

      
      
      //Following shifted from SetPage for better performance
      this.positionSetupService.getPositionClassList().then(data => {
          this.positionClassOptions = data.result.records;
          console.log("Data class options : " + data.result.records);
      });
      this.positionSetupService.getSkillSetList().then(data => {
          this.skillSetIdOptions = data.result.records;
      });
      this.positionSetupService.getPositionIdList().then(data => {
          this.getPositionID = data.result.records;
      });
     

  }

  getReportPositionsAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'i');
      return Observable.of(
          this.getPositionID.filter((id: any) => {
              return query.test(id.positionId);
          })
      );
  }
  getReportPositionIdAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'i');
      return Observable.of(
          this.getPositionID.filter((id: any) => {
              return query.test(id.positionId);
          })
      );
  }

  changeTypeaheadLoading(e: boolean): void {
      this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
      console.log('Selected value: ', e);
      this.positionSetupService.getPositionSetup(e.item.id).then(pagedData => {
          console.log('PagedData', pagedData)
          this.model = pagedData.result;
          
          this.model.id = 0;
          console.log('this.model', this.model)
      });
  }
  typeaheadOnSelectReportToPosition(e: TypeaheadMatch): void {
      console.log('Selected value: ', e);
      this.positionSetupService.getPositionSetup(e.item.id).then(pagedData => {
          console.log('PagedData', pagedData)
          this.model = pagedData.result;

          //this.model.id = 0;
          console.log('this.model', this.model)
      });
  }
  // setting pagination
  setPage(pageInfo) {
      // debugger;
      this.selected = []; // remove any selected checkbox on paging
      this.page.pageNumber = pageInfo.offset;
      if (pageInfo.sortOn == undefined) {
          this.page.sortOn = this.page.sortOn;
      } else {
          this.page.sortOn = pageInfo.sortOn;
      }
      if (pageInfo.sortBy == undefined) {
          this.page.sortBy = this.page.sortBy;
      } else {
          this.page.sortBy = pageInfo.sortBy;
      }
      this.page.searchKeyword = '';
      this.positionSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
      });
  }

  // Open form for create position setup
  Create() {
      this.isAttached = false;
      this.showCreateForm = false;
      this.isUnderUpdate = false;
      this.isReporttopositionValid = true;
      setTimeout(() => {
          this.showCreateForm = true;
          setTimeout(() => {
              window.scrollTo(0, 2000);
          }, 10);
      }, 10);
      this.model = {
          id: 0,
          positionId: '',
          description: '',
          arabicDescription: '',
          reportToPostion: '',
          positionClassId: null,
          skillSetId: null,
          postionLongDesc: '',
          positionClassPrimaryId: null,
          skillsetPrimaryId: null,
      };
  }

  // edit position class by row
  // edit(row: PositionSetupModule) {
  //     this.isReporttopositionValid = true;
  //     this.isAttached = false;
  //     this.showCreateForm = true;
  //     this.model = Object.assign({}, row);
  //     this.uploadFileService.positionData(this.model);
  //     this.positionId = row.positionId;
  //     this.isUnderUpdate = true;
  //     this.positionIdvalue = this.model.positionId;
  //     this.model.positionClassId = row.positionClassPrimaryId;
  //     this.model.skillSetId = row.skillsetPrimaryId;
  //     this.model.postionLongDesc = row.postionLongDesc;
  //     setTimeout(() => {
  //         window.scrollTo(0, 2000);
  //     }, 10);
  // }


  edit(event) {
   
   
    if(event.cellIndex!=0 && event.cellIndex!=1){
        this.positionId = event.row.id;
      
        var myurl = `${'createpositionSetup'}/${this.positionId}`;
        this.router.navigateByUrl(myurl);
    }

   
}

  updateStatus() {
      this.closeModal();


      // Call service api for updating selected position class
      this.model.positionId = this.uploadFileService.positionModelData.positionId;
      this.positionSetupService.createPositionSetup(this.model).then(data => {
          let datacode = data.code;


          if (this.uploadFileService.positionAttchModelData !== undefined)
              if (this.uploadFileService.positionAttchModelData.isAttachedFile === true) {
                  this.uploadFileService.positionAttchModelData.positionAttach.positionIds = data.result.id
                  let uploadAttachModel = this.uploadFileService.positionAttchModelData.positionAttach;
                  let uploadAttachfile = this.uploadFileService.positionAttchModelData.attachmentFile;
                  this.positionAttchmentService.createPositionAttachment(uploadAttachModel, uploadAttachfile);
                  this.uploadFileService.positionAttchModelData.isAttachedFile = false;
              }

          if (datacode === 201) {
              // Refresh the Grid data after editing position class
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
             
              // Scroll to top after editing position class
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                  }, 4000);
                  this.messageText = data.btiMessage.message;

                  this.showCreateForm = false;
              }, 100);
              this.hasMessage = false;
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
          }
      }).catch(error => {
          this.hasMsg = true;
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = 'Server error. Please contact admin.';
          }, 100);
      });
  }

  varifyDelete() {
      debugger;
      if (this.selected.length > 0) {
          this.showCreateForm = false;
          this.isDeleteAction = true;
          this.isConfirmationModalOpen = true;
          this.confirmationModalBody=this.deleteConfirmationText;
      } else {
          this.isSuccessMsg = false;
          this.hasMessage = true;
          this.message.type = 'error';
          this.isfailureMsg = true;
          this.showMsg = true;
          this.message.text = 'Please select at least one record to delete.';
          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.hasMessage = false;

          }, 4000);
      }
  }

  navToTraining() {
      console.log("function is called");
      if (this.selected.length === 1) {

          this.router.navigate(['hcm/positionTraining', this.selected[0].positionId,
              this.selected[0].description, this.selected[0].arabicDescription, this.selected[0].id]);
      } else if (this.selected.length > 1) {
          //console.log(this.selected.length);
          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = true;
                  this.hasMsg = false;
              }, 4000);
              this.messageText = "Please select only one record.";
          }, 100);
      } else {
          //console.log("down:-->"+this.selected.length);
          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = true;
                  this.hasMsg = false;
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
              }, 4000);
              window.setTimeout(() => {
                  this.messageText = "Please select at least one record.";
              }, 100);

          }, 100);

      }

  }

  navToPlanSetup() {

      if (this.selected.length === 1) {

          this.router.navigate(['hcm/positionPlanSetup', this.selected[0].positionId,
              this.selected[0].id]);
      } else if (this.selected.length > 1) {
          //console.log(this.selected.length);
          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = true;
                  this.hasMsg = false;
              }, 4000);
              this.messageText = "Please select only one record";
          }, 100);
      } else {
          //console.log("down:-->"+this.selected.length);
          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = true;
                  this.hasMsg = false;
              }, 4000);
              this.messageText = "Please select at least one record.";
          }, 100);
      }
  }

  navToAttachment(f: NgForm) {
      console.log("PD:", this.uploadFileService);
      this.isAttached = true;
      this.uploadFileService.positionModelData = this.model;
      if (this.model.id === 0 && this.model.positionId !== null) {
          // Check for duplicate positionClass Id according to it create new position class
          this.positionSetupService.checkDuplicatePositionSetupId(this.model.positionId).then(response => {
              if (response && response.code === 302 && response.result && response.result.isRepeat) {
                  this.duplicateWarning = true;
                  this.message.type = 'success';

                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      window.setTimeout(() => {
                          this.showMsg = false;
                          this.duplicateWarning = false;
                      }, 4000);
                      this.message.text = response.btiMessage.message;
                  }, 100);
              } else if (this.model.positionId !== "" && this.model.description !== "") {
                  this.router.navigate(['hcm/positionAttachment']);
              }

          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = 'Server error. Please contact admin.';
              }, 100);
          });
      } else if (this.model.positionId !== ("" || null) && this.model.description !== ("" || null)) {
          this.router.navigate(['hcm/positionAttachment']);
      } else {
          null;
      }
  }

  attachMsg() {
      window.scrollTo(0, 0);
      this.isSuccessMsg = false;
      this.isfailureMsg = true;
      this.showMsg = true;
      window.setTimeout(() => {
          this.showMsg = true;
          this.hasMsg = true;
      }, 100);
      this.messageText = "Please save the Position Setup first.";
  }

  // delete position class by passing whole object of perticular position Setup
  delete() {
      let selectedPositionSetup = [];
      for (let i = 0; i < this.selected.length; i++) {
          selectedPositionSetup.push(this.selected[i].id);
      }
      this.positionSetupService.deletePositionSetup(selectedPositionSetup).then(data => {
          let datacode = data.code;
          if (datacode === 200) {
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
          }
          this.hasMessage = true;
          console.log("Data",datacode);
          if(datacode == "302"){
              this.message.type = "error";
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
          } else {
              this.isSuccessMsg = true;
              this.message.type = "success";
              this.isfailureMsg = false;
          }
          // this.message.text = data.btiMessage.message;

          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.showMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMessage = false;
              }, 4000);

              this.message.text = data.btiMessage.message;
          }, 100);

          // Refresh the Grid data after deletion of position Setup
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

      }).catch(error => {
          this.hasMessage = true;
          this.message.type = 'error';
          let errorCode = error.status;
          this.message.text = 'Server issue. Please contact admin.';
      });
      this.closeModal();
  }

  // default list on page
  onSelect({ selected }) {
      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
  }

  // search position class by keyword
  updateFilter(event) {
      this.searchKeyword = event.target.value.toLowerCase();
      this.page.pageNumber = 0;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.table.offset = 0;
  }


  // Set default page size
  changePageSize(event) {
      this.page.size = event.target.value;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }


  // Clear form to reset to default blank
  Clear(f: NgForm) {
      f.resetForm({ positionClassId: null, skillSetId: null });
      this.isAttached = false;
      this.isReporttopositionValid = true;
  }


  closeModal() {
      this.isDeleteAction = false;
      this.isConfirmationModalOpen = false;
  }

  // function call for creating new positionsetup
  CreatePositionSetup(f: NgForm, event: Event) {
      this.isAttached = true;

      if (!f.form.valid) {
          return;
      }
      event.preventDefault();

      let posIdx = this.model.positionId;

      // Check if the id is available in the model.
      // If id avalable then update the position class, else Add new position class.
      if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
          this.isConfirmationModalOpen = true;
          this.isDeleteAction = false;
      } else {
          // Check for duplicate positionClass Id according to it create new position class
          this.positionSetupService.checkDuplicatePositionSetupId(posIdx).then(response => {
              if (response && response.code === 302 && response.result && response.result.isRepeat) {
                  this.duplicateWarning = true;
                  this.message.type = 'success';

                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      window.setTimeout(() => {
                          this.showMsg = false;
                          this.duplicateWarning = false;
                      }, 4000);
                      this.message.text = response.btiMessage.message;
                  }, 100);
              } else {
                  // Call service api for Creating new position class
                  this.positionSetupService.createPositionSetup(this.model).then(data => {
                      let datacode = data.code;
                      if (this.uploadFileService.positionAttchModelData !== undefined)
                          if (this.uploadFileService.positionAttchModelData.isAttachedFile === true) {
                              this.uploadFileService.positionAttchModelData.positionAttach.positionIds = data.result.id
                              let uploadAttachModel = this.uploadFileService.positionAttchModelData.positionAttach;
                              let uploadAttachfile = this.uploadFileService.positionAttchModelData.attachmentFile;
                              this.positionAttchmentService.createPositionAttachment(uploadAttachModel, uploadAttachfile);
                              this.uploadFileService.positionAttchModelData.isAttachedFile = false;
                          }
                      // this.uploadFileService.positionAttchModelData.positionAttach.positionIds = data.result.id
                      // let uploadAttachModel = this.uploadFileService.positionAttchModelData.positionAttach;
                      // let uploadAttachfile = this.uploadFileService.positionAttchModelData.attachmentFile;

                      // this.positionAttchmentService.createPositionAttachment(uploadAttachModel, uploadAttachfile)
                      if (datacode === 201) {
                          window.scrollTo(0, 0);
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.hasMessage = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.showCreateForm = false;
                              this.messageText = data.btiMessage.message;
                          }, 100);

                          this.hasMsg = true;
                          f.resetForm();
                          // Refresh the Grid data after adding new position class
                          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                      }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = 'Server error. Please contact admin.';
                      }, 100);
                  });
              }

          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = 'Server error. Please contact admin.';
              }, 100);
          });

      }
  }

  sortColumn(val) {
      if (this.page.sortOn == val) {
          if (this.page.sortBy == 'DESC') {
              this.page.sortBy = 'ASC';
          } else {
              this.page.sortBy = 'DESC';
          }
      }
      this.page.sortOn = val;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  CheckValidID(eve: any) {
      console.log("test",eve.target.value);
      if (eve.target.value == "") {
          this.isReporttopositionValid = true;
      } else {
          for (let i = 0; i < this.getPositionID.length; i++) {
              //if (this.getItemList[i].itemNumber.includes(eve.target.value)) {
              if (this.getPositionID[i].positionId.toLowerCase().match(eve.target.value.toLowerCase()))
              {
                  this.isReporttopositionValid = true;
                  break;
              } else {
                  this.isReporttopositionValid = false;
              }
          }
      }
  }


  goToCreate(){
    this.router.navigate(['createpositionSetup']);   
  }
  EscapeModal(event){
        
    var key = event.key;
    if(key=="Escape"){
        this.closeModal();
    }
  }
}
