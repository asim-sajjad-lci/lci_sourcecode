import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLocationSetupComponent } from './create-location-setup.component';

describe('CreateLocationSetupComponent', () => {
  let component: CreateLocationSetupComponent;
  let fixture: ComponentFixture<CreateLocationSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLocationSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLocationSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
