

// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// @NgModule({
//   imports: [
//     CommonModule
//   ],
//   declarations: []
// })
// export class CreateLocationModule { }



import { AlertService } from './../../../_sharedresource/_services/alert.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CreateLocationSetupComponent } from './create-location-setup.component';
import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { CreatelocationSetupRoutes } from './create-location.routing';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreatelocationSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,

  ],
  providers:[AlertService],
  
  declarations: [CreateLocationSetupComponent]
})
export class CreateLocationModule { }
