import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaycodeSetupComponent } from './paycode-setup.component';

describe('PaycodeSetupComponent', () => {
  let component: PaycodeSetupComponent;
  let fixture: ComponentFixture<PaycodeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaycodeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaycodeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
