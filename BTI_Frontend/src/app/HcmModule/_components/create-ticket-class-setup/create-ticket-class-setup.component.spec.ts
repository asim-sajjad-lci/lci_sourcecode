import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTicketClassSetupComponent } from './create-ticket-class-setup.component';

describe('CreateTicketClassSetupComponent', () => {
  let component: CreateTicketClassSetupComponent;
  let fixture: ComponentFixture<CreateTicketClassSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTicketClassSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTicketClassSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
