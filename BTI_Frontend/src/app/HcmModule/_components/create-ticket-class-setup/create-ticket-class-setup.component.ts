import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { Observable } from 'rxjs';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { TicketClassSetupService } from 'app/HcmModule/_services/ticket-class-setup/ticket-class-setup.service';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { Constants } from 'app/_sharedresource/Constants';
import { TicketClassSetupModel } from 'app/HcmModule/_models/ticket-class-setup/ticket-class-setup';

@Component({
  selector: 'app-create-ticket-class-setup',
  templateUrl: './create-ticket-class-setup.component.html',
  styleUrls: ['./create-ticket-class-setup.component.css'],
  providers: [CommonService, TicketClassSetupService]
})
export class CreateTicketClassSetupComponent implements OnInit {

 
  model: TicketClassSetupModel;

  isCodeIdValid = true;

  moduleCode = "M-1011";
  screenCode = "S-1791";
  moduleName;
  screenName;
  currentLanguage: any;
  defaultFormValues: object[];

  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  departmentId = {};
  searchKeyword = '';
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  isDeleteAction: boolean = false;

  selectedTicketClassSetup;
  ClassId;

  ticketClassIdList: Observable<any>;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;

  getDepartment: any[] = [];
  rows = new Array<TicketClassSetupModel>();

  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;


  TICKETCLASS_CodeId: any;
  TICKETCLASS_DESCRIPTION: any;
  TICKETCLASS_SEARCH: any;
  TICKETCLASS_ARABIC_DESCRIPTION: any;
  TICKETCLASS_CREATE_LABEL: any;
  TICKETCLASS_SAVE_LABEL: any;
  TICKETCLASS_CLEAR_LABEL: any;
  TICKETCLASS_CANCEL_LABEL: any;
  TICKETCLASS_UPDATE_LABEL: any;
  TICKETCLASS_DELETE_LABEL: any;
  TICKETCLASS_CREATE_FORM_LABEL: any;
  TICKETCLASS_UPDATE_FORM_LABEL: any;
  TICKETCLASS_TICKETCLASS_LIST: any;


  constructor(private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private service: TicketClassSetupService, ) {


      this.defaultFormValues = [
        { 'fieldName': 'TICKETCLASS_CodeId', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETCLASS_TICKETCLASS_LIST', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
        { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
    ];
    this.TICKETCLASS_CodeId = this.defaultFormValues[0];
    this.TICKETCLASS_DESCRIPTION = this.defaultFormValues[1];
    this.TICKETCLASS_SEARCH = this.defaultFormValues[2];
    this.TICKETCLASS_ARABIC_DESCRIPTION = this.defaultFormValues[3];
    this.TICKETCLASS_CREATE_LABEL = this.defaultFormValues[4];
    this.TICKETCLASS_SAVE_LABEL = this.defaultFormValues[5];
    this.TICKETCLASS_CLEAR_LABEL = this.defaultFormValues[6];
    this.TICKETCLASS_CANCEL_LABEL = this.defaultFormValues[7];
    this.TICKETCLASS_UPDATE_LABEL = this.defaultFormValues[8];
    this.TICKETCLASS_DELETE_LABEL = this.defaultFormValues[9];
    this.TICKETCLASS_CREATE_FORM_LABEL = this.defaultFormValues[10];
    this.TICKETCLASS_UPDATE_FORM_LABEL = this.defaultFormValues[11];
    this.TICKETCLASS_TICKETCLASS_LIST = this.defaultFormValues[12];


    this.ticketClassIdList = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.model.codeId);
    }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));

  }

  ngOnInit() {

    this.model = {
      id: 0,
      codeId: '',
      descr: '',
      arabicDescr: '',
    };

    this.route.params.subscribe((params: Params) => {
      debugger
      this.ClassId = params['id'];

      if (this.ClassId != '' && this.ClassId != undefined) {

        this.getTicketClassById(this.ClassId);
        this.isUnderUpdate = true;
        this.onAutocompleteFocus('lblTicketClassId');
      }

    });

    this.setPage();

    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


    //   this.model = {
    //     id: 0,
    //     codeId: '',
    //     descr: '',
    //     arabicDescr: '',
    // };
  }

  getReportPositionsAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getDepartment.filter((id: any) => {
        return query.test(id.departmentId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    // console.log('Selected value: ', e);
    this.service.getTicketClassById(e.item.id).then(allData => {
      // console.log('allData', allData);
      debugger;
      this.model = allData.result;
      this.model.id = 0;
    });

  }


  goBack() {
    this.router.navigate(['hcm/ticketclasssetup']);
  }

  //function call for creating new TicketClass
  CreateTicketClass(f: NgForm, event: Event) {
    debugger;

    event.preventDefault();
    var classIdx = this.model.codeId;

    //Check if the id is available in the model.
    //If id avalable then update the TicketClass, else Add new TicketClass.
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
      this.isConfirmationModalOpen = true;
      this.isDeleteAction = false;
    }
    else {
      //Check for duplicate TicketClass Id according to it create new TicketClass
      this.service.checkDuplicateClassId(classIdx).then(response => {
        if (response && response.code == 302 && response.result && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = "success";

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;

          }, 100);
        } else {
          //Call service api for Creating new TicketClass
          this.service.createTicketClass(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {

              this.isSuccessMsg = true;
              this.isfailureMsg = false;

              this.showMsg = true;
              this.messageText = data.btiMessage.message;
              this.hasMsg = true;
              window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
                this.goBack();


              }, 4000);

            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = "Server error. Please contact admin.";
            }, 100)
          });
        }

      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = "Server error. Please contact admin.";
        }, 100)
      });

    }
  }

  // handleNameInput(e) {

  //   let keycode = e.keyCode;

  //   if ((keycode < 65 || keycode > 90) && keycode != 189 && keycode != 16 && keycode != 20 && keycode != 8) {
  //     this.isCodeIdValid = false;
  //     var str = this.model.codeId;
  //     str = str.slice(0, -1);
  //     this.model.codeId = str;
  //     window.setTimeout(() => {
  //       this.isCodeIdValid = true;
  //     }, 2000);
  //   }


  // }

  getTicketClassById(Classid) {
    this.model.id = Classid;
    debugger;
    this.isUnderUpdate = true;
    this.onAutocompleteFocus('lblTicketClassId');
    this.service.getTicketClassById(Classid).then(allData => {

      console.log("selectedDepartmentSetup.........", allData)
      this.model.id = allData.result.id;
      this.selectedTicketClassSetup = allData.result.id;

      this.model = allData.result;
      // this.model.id = 0;
      // console.log('this.model', this.model)
    });

  }


  getClassSetupData(event) {

    var Id = event.id ? event.id : -1;

    this.model.id = Id;

    if (event.id > 0 && event.id != undefined) {
      this.getTicketClassById(event.id);
    }

  }

  onAutocompleteFocus(labelId) {

    let elem: HTMLElement = document.getElementById(labelId);
    elem.setAttribute("style", "top:-10px;font-size:14px;color:#5264AE;font-weight: bold;");
  }

  setPage() {
    debugger;
    // this.selected = []; // remove any selected checkbox on paging
    // this.page.pageNumber = pageInfo.offset;
    // if (pageInfo.sortOn == undefined) {
    //     this.page.sortOn = this.page.sortOn;
    // } else {
    //     this.page.sortOn = pageInfo.sortOn;
    // }
    // if (pageInfo.sortBy == undefined) {
    //     this.page.sortBy = this.page.sortBy;
    // } else {
    //     this.page.sortBy = pageInfo.sortBy;
    // }

    // this.page.searchKeyword = '';
    this.service.getTicketClasses().then(pagedData => {
      debugger;
      // this.page = pagedData.page;
      this.rows = pagedData.result;
    });
  }


  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }


  updateStatus() {
    this.closeModal();
    //Call service api for updating selected TicketClass
    debugger;
    this.model.id = this.selectedTicketClassSetup;
    this.service.updateTicketClass(this.model).then(data => {
      var datacode = data.code;
      if (datacode == 201) {

        this.isSuccessMsg = true;
        this.isfailureMsg = false;

        this.showMsg = true;
        this.messageText = data.btiMessage.message;
        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
          // this.goBack();


        }, 4000);

      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = "Server error. Please contact admin.";
      }, 100)
    });
  }

  Clear(f: NgForm) {
    f.resetForm();
    this.ClassId=[];
      this.model = {
        id: 0,
        codeId: '',
        descr: '',
        arabicDescr: '',
    };
}

}
