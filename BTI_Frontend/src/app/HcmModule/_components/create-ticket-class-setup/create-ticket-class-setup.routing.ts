import { Routes } from '@angular/router';
import { CreateTicketClassSetupComponent } from './create-ticket-class-setup.component';


export const CreateTicketClassSetupRoutes: Routes = [{
  path: '',
  component: CreateTicketClassSetupComponent,
  data: {
    breadcrumb: ""
  }
}];