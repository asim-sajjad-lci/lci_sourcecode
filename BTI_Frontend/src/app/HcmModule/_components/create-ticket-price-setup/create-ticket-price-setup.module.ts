import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { CreateTicketPriceSetupRoutes } from './create-ticket-price-setup.routing';
import { CreateTicketPriceSetupComponent } from './create-ticket-price-setup.component';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/dist/multiselect.component';
import { MultiselectDropdownModule } from 'angular-4-dropdown-multiselect';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateTicketPriceSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    AngularMultiSelectModule,
    MultiselectDropdownModule,
  ],

  providers:[AlertService],
  declarations: [CreateTicketPriceSetupComponent]
})
export class CreateTicketPriceSetupModule { }