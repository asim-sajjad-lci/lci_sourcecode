import { Routes } from '@angular/router';
import { CreateTicketPriceSetupComponent } from './create-ticket-price-setup.component';


export const CreateTicketPriceSetupRoutes: Routes = [{
  path: '',
  component: CreateTicketPriceSetupComponent,
  data: {
    breadcrumb: ""
  }
}];