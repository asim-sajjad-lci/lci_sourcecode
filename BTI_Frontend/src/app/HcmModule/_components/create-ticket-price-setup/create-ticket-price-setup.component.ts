import { Component, OnInit } from '@angular/core';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { TicketClassSetupService } from 'app/HcmModule/_services/ticket-class-setup/ticket-class-setup.service';
import { Constants } from 'app/_sharedresource/Constants';
import { Observable } from 'rxjs';
import { TicketClassSetupModel } from 'app/HcmModule/_models/ticket-class-setup/ticket-class-setup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TicketPriceSetupModel } from 'app/HcmModule/_models/ticket-price-setup/ticket-price-setup';
import { TicketPriceSetupService } from 'app/HcmModule/_services/ticket-price-setup/ticket-price-setup.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-ticket-price-setup',
  templateUrl: './create-ticket-price-setup.component.html',
  styleUrls: ['./create-ticket-price-setup.component.css'],
  providers: [CommonService, TicketPriceSetupService]
})
export class CreateTicketPriceSetupComponent implements OnInit {

  model: TicketPriceSetupModel;

  isCodeIdValid = true;

  moduleCode = "M-1011";
  screenCode = "S-1792";
  moduleName;
  screenName;
  currentLanguage: any;
  defaultFormValues: object[];

  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  departmentId = {};
  searchKeyword = '';
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  isDeleteAction: boolean = false;

  selectedTicketPriceSetup;
  PriceId;

  ticketClassIdList: Observable<any>;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;

  getDepartment: any[] = [];
  rows = new Array<TicketPriceSetupModel>();

  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;


  TICKETPRICE_ROUTEID: any;
  TICKETPRICE_CLASSID: any;
  TICKETPRICE_SEARCH: any;
  TICKETPRICE_ADULT: any;
  TICKETPRICE_CHILD: any;
  TICKETPRICE_INFANT: any;
  TICKETPRICE_CREATE_LABEL: any;
  TICKETPRICE_SAVE_LABEL: any;
  TICKETPRICE_CLEAR_LABEL: any;
  TICKETPRICE_CANCEL_LABEL: any;
  TICKETPRICE_UPDATE_LABEL: any;
  TICKETPRICE_DELETE_LABEL: any;
  TICKETPRICE_CREATE_FORM_LABEL: any;
  TICKETPRICE_UPDATE_FORM_LABEL: any;
  TICKETPRICE_LIST: any;
  TICKETPRICE_LABEL: any;

  RouteData = [];
  ClassData = [];
  modelRouteId = [];
  modelClassId = [];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private service: TicketPriceSetupService, ) {

      this.defaultFormValues = [
        { 'fieldName': 'TICKETPRICE_ROUTEID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_CLASSID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_ADULT', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_CHILD', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_INFANT', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_LIST', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'TICKETPRICE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
        { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
    ];
    this.TICKETPRICE_ROUTEID = this.defaultFormValues[0];
    this.TICKETPRICE_CLASSID = this.defaultFormValues[1];
    this.TICKETPRICE_SEARCH = this.defaultFormValues[2];
    this.TICKETPRICE_ADULT = this.defaultFormValues[3];
    this.TICKETPRICE_CHILD = this.defaultFormValues[4];
    this.TICKETPRICE_INFANT = this.defaultFormValues[5];
    this.TICKETPRICE_CREATE_LABEL = this.defaultFormValues[6];
    this.TICKETPRICE_SAVE_LABEL = this.defaultFormValues[7];
    this.TICKETPRICE_CLEAR_LABEL = this.defaultFormValues[8];
    this.TICKETPRICE_CANCEL_LABEL = this.defaultFormValues[9];
    this.TICKETPRICE_UPDATE_LABEL = this.defaultFormValues[10];
    this.TICKETPRICE_DELETE_LABEL = this.defaultFormValues[11];
    this.TICKETPRICE_CREATE_FORM_LABEL = this.defaultFormValues[12];
    this.TICKETPRICE_UPDATE_FORM_LABEL = this.defaultFormValues[13];
    this.TICKETPRICE_LIST = this.defaultFormValues[14];
    this.TICKETPRICE_LABEL = this.defaultFormValues[15];
  
   }

  ngOnInit() {

    this.model = {
      id: 0,
      adultTckRate:null,
      childTckRate:null,
      infacntTckRate:null,
      ticketRouteId :'',
      ticketClassId :'',
      ticketRouteCodeId :'',
      ticketClassCodeId :''

    };

    this.route.params.subscribe((params: Params) => {
      debugger
      this.PriceId = params['id'];


      if (this.PriceId != '' && this.PriceId != undefined) {

        this.getTicketPriceById(this.PriceId);
        this.isUnderUpdate = true;
        this.onAutocompleteFocus('RouteIdLabel');
        this.onAutocompleteFocus('ClassIdLabel');
      }

    });

    this.setPage();

    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
  }

  onAutocompleteFocus(labelId){

    let elem: HTMLElement = document.getElementById(labelId);
    elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
  }

  setPage() {
    debugger;
    // this.selected = []; // remove any selected checkbox on paging
    // this.page.pageNumber = pageInfo.offset;
    // if (pageInfo.sortOn == undefined) {
    //     this.page.sortOn = this.page.sortOn;
    // } else {
    //     this.page.sortOn = pageInfo.sortOn;
    // }
    // if (pageInfo.sortBy == undefined) {
    //     this.page.sortBy = this.page.sortBy;
    // } else {
    //     this.page.sortBy = pageInfo.sortBy;
    // }

    // this.page.searchKeyword = '';
    this.service.getTicketClasses().then(pagedData => {
      debugger;
      // this.page = pagedData.page;
      for(let i=0;i<pagedData.result.length;i++)
      {
        this.ClassData.push({"id":pagedData.result[i].id,"itemName":pagedData.result[i].codeId});
      }
     
    });

    this.service.getTicketRoutes().then(pagedData => {
      debugger;
      // this.page = pagedData.page;
      for(let i=0;i<pagedData.result.length;i++)
      {
        this.RouteData.push({"id":pagedData.result[i].id,"itemName":pagedData.result[i].codeId});
      }
     
    });
  }

  // CreateTicketPrice(f){
    
  //   console.log(this.modelRouteId)
  //   console.log(this.modelClassId)
  //   for(let i =0;i<this.modelRouteId.length;i++)
  //   {
  //     this.model.ticketRouteId = this.modelRouteId[i].id
  //   }
  //   for(let i =0;i<this.modelClassId.length;i++)
  //   {
  //     this.model.ticketClassId = this.modelClassId[i].id
  //   }
  //   console.log(this.model)
   
  // }

  goBack() {
    this.router.navigate(['hcm/ticketpricesetup']);
  }

  Clear(f: NgForm) {
    f.resetForm();
    this.PriceId=[];
    this.modelRouteId=[];
    this.modelClassId=[];
    this.model = {
      id: 0,
      adultTckRate:null,
      childTckRate:null,
      infacntTckRate:null,
      ticketRouteId :'',
      ticketClassId :'',
      ticketRouteCodeId :'',
      ticketClassCodeId :''
    };
}

closeModal() {
  this.isDeleteAction = false;
  this.isConfirmationModalOpen = false;
}

 //function call for creating new TicketRoute
 CreateTicketPrice(f: NgForm, event: Event) {
  debugger;
     for(let i =0;i<this.modelRouteId.length;i++)
    {
      this.model.ticketRouteId = String(this.modelRouteId[i].id);
    }
    for(let i =0;i<this.modelClassId.length;i++)
    {
      this.model.ticketClassId = String(this.modelClassId[i].id);
    }

  event.preventDefault();

  //Check if the id is available in the model.
  //If id avalable then update the ticketPrice, else Add new ticketPrice.
  if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
    this.isConfirmationModalOpen = true;
    this.isDeleteAction = false;
  }
  else {
    console.log(this.model)
        //Call service api for Creating new ticketPrice
        this.service.createTicketPrice(this.model).then(data => {
          var datacode = data.code;
          if (datacode == 201) {

            this.isSuccessMsg = true;
            this.isfailureMsg = false;

            this.showMsg = true;
            this.messageText = data.btiMessage.message;
            this.hasMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.hasMsg = false;
              this.goBack();


            }, 4000);

          }
        }).catch(error => {
          this.hasMsg = true;
          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = "Server error. Please contact admin.";
          }, 100)
        });

  }
}

getTicketPriceById(PriceId) {
  this.model.id = PriceId;
  debugger;
  this.service.getTicketPriceById(PriceId).then(allData => {

    console.log("selectedDepartmentSetup.........", allData)
    this.model.id = allData.result.id;
    this.selectedTicketPriceSetup = allData.result.id;
    this.modelRouteId.push({id:allData.result.ticketRouteId,itemName:allData.result.ticketRouteCodeId});
    this.modelClassId.push({id:allData.result.ticketClassId,itemName:allData.result.ticketClassCodeId});
    this.model = allData.result;
    // this.model.id = 0;
    // console.log('this.model', this.model)
  });

}


updateStatus() {
  this.closeModal();
  //Call service api for updating selected TicketClass
  debugger;
  this.model.id = this.selectedTicketPriceSetup;
  this.service.updateTicketPrice(this.model).then(data => {
    var datacode = data.code;
    if (datacode == 201) {

      this.isSuccessMsg = true;
      this.isfailureMsg = false;

      this.showMsg = true;
      this.messageText = data.btiMessage.message;
      this.hasMsg = true;
      window.setTimeout(() => {
        this.showMsg = false;
        this.hasMsg = false;
        // this.goBack();


      }, 4000);

    }
  }).catch(error => {
    this.hasMsg = true;
    window.setTimeout(() => {
      this.isSuccessMsg = false;
      this.isfailureMsg = true;
      this.showMsg = true;
      this.messageText = "Server error. Please contact admin.";
    }, 100)
  });
}



EscapeModal(event){
      
  var key = event.key;
  if(key=="Escape"){
      this.closeModal();
  }
}
}
