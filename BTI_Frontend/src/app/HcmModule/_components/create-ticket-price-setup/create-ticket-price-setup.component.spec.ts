import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTicketPriceSetupComponent } from './create-ticket-price-setup.component';

describe('CreateTicketPriceSetupComponent', () => {
  let component: CreateTicketPriceSetupComponent;
  let fixture: ComponentFixture<CreateTicketPriceSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTicketPriceSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTicketPriceSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
