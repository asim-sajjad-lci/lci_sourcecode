import { Routes } from '@angular/router';
import { VacationTypeSetupListComponent } from './vacation-type-setup-list.component';




export const VacationTypeSetupListRoutes: Routes = [{
  path: '',
  component: VacationTypeSetupListComponent,
  data: {
    breadcrumb: ""
  }
}];