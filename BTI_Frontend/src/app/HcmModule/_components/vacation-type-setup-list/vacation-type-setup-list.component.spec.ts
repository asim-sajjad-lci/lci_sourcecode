import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacationTypeSetupListComponent } from './vacation-type-setup-list.component';

describe('VacationTypeSetupListComponent', () => {
  let component: VacationTypeSetupListComponent;
  let fixture: ComponentFixture<VacationTypeSetupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VacationTypeSetupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacationTypeSetupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
