import { Component, OnInit, ViewChild } from '@angular/core';
import { Page } from 'app/_sharedresource/page';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { Constants } from 'app/_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { VacationTypeSetupService } from 'app/HcmModule/_services/vacation-type-setup/vacation-type-setup.service';

@Component({
  selector: 'app-vacation-type-setup-list',
  templateUrl: './vacation-type-setup-list.component.html',
  styleUrls: ['./vacation-type-setup-list.component.css'],
  providers:[CommonService,VacationTypeSetupService]
})
export class VacationTypeSetupListComponent implements OnInit {

  page = new Page();
  rows = {};
  selected = [];

  moduleCode = "M-1011";
  screenCode = "S-1793";

  moduleName;
  screenName;
  currentLanguage: any;
  defaultFormValues: object[];

  VACATIONTYPE_CodeId: any;
  VACATIONTYPE_DESCRIPTION: any;
  VACATIONTYPE_SEARCH: any;
  VACATIONTYPE_ARABIC_DESCRIPTION: any;
  VACATIONTYPE_DAYS: any;
  VACATIONTYPE_CREATE_LABEL: any;
  VACATIONTYPE_SAVE_LABEL: any;
  VACATIONTYPE_CLEAR_LABEL: any;
  VACATIONTYPE_CANCEL_LABEL: any;
  VACATIONTYPE_UPDATE_LABEL: any;
  VACATIONTYPE_DELETE_LABEL: any;
  VACATIONTYPE_CREATE_FORM_LABEL: any;
  VACATIONTYPE_UPDATE_FORM_LABEL: any;
  VACATIONTYPE_VACATIONTYPE_LIST: any;
  VACATIONTYPE_TABLEVIEW: any;
  
  createbtnText=Constants.createButtonText;
  searchKeyword = '';
  vacationTypeId;
  ddPageSize: number = 5;

  isDeleteAction: boolean = false;
  isConfirmationModalOpen: boolean = false;

  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;

  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };

  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
    private commonService: CommonService,
    private service: VacationTypeSetupService
  ) { 
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'id';
    this.page.sortBy = 'DESC';
    
  this.defaultFormValues = [
    { 'fieldName': 'VACATIONTYPE_CodeId', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_DAYS', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_VACATIONTYPE_LIST', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    { 'fieldName': 'VACATIONTYPE_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
    { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
    { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
    { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
    { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
    { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
    { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
    { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
];
this.VACATIONTYPE_CodeId = this.defaultFormValues[0];
this.VACATIONTYPE_DESCRIPTION = this.defaultFormValues[1];
this.VACATIONTYPE_SEARCH = this.defaultFormValues[2];
this.VACATIONTYPE_ARABIC_DESCRIPTION = this.defaultFormValues[3];
this.VACATIONTYPE_DAYS = this.defaultFormValues[4];
this.VACATIONTYPE_CREATE_LABEL = this.defaultFormValues[5];
this.VACATIONTYPE_SAVE_LABEL = this.defaultFormValues[6];
this.VACATIONTYPE_CLEAR_LABEL = this.defaultFormValues[7];
this.VACATIONTYPE_CANCEL_LABEL = this.defaultFormValues[8];
this.VACATIONTYPE_UPDATE_LABEL = this.defaultFormValues[9];
this.VACATIONTYPE_DELETE_LABEL = this.defaultFormValues[10];
this.VACATIONTYPE_CREATE_FORM_LABEL = this.defaultFormValues[11];
this.VACATIONTYPE_UPDATE_FORM_LABEL = this.defaultFormValues[12];
this.VACATIONTYPE_VACATIONTYPE_LIST = this.defaultFormValues[13];
this.VACATIONTYPE_TABLEVIEW = this.defaultFormValues[14];



 }

  ngOnInit() {

    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
  }

  goToCreate(){
    this.router.navigate(['createVacationTypeSetup']);   
  }

  //setting pagination
  setPage(pageInfo) {
    debugger;
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
        this.page.sortOn = this.page.sortOn;
    } else {
        this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
        this.page.sortBy = this.page.sortBy;
    } else {
        this.page.sortBy = pageInfo.sortBy;
    }

    this.page.searchKeyword = '';
    this.service.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        debugger;
        this.page = pagedData.page;
        this.rows = pagedData.data;
    });
}

  // search department by keyword 
  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
    // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
    //     this.page = pagedData.page;
    //     this.rows = pagedData.data;
    //     this.table.offset = 0;
    // });
}

edit(event) {
   
   
  if(event.cellIndex!=0 ){
      this.vacationTypeId = event.row.id;
    
      var myurl = `${'createVacationTypeSetup'}/${this.vacationTypeId}`;
      this.router.navigateByUrl(myurl);
  }
}

changePageSize(event) {
  this.page.size = event.target.value;
  this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

varifyDelete() {
  debugger;
  if (this.selected.length > 0) {
    //  this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
      this.confirmationModalBody = this.deleteConfirmationText;

  } else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;
      this.message.text = 'Please select at least one record to delete.';
      window.scrollTo(0, 0);
  }
}



//delete vacation by passing whole object of perticular vacation
delete() {
  debugger;
  var selectedVacation = [];
  for (var i = 0; i < this.selected.length; i++) {
    selectedVacation.push(this.selected[i].id);
  }
  this.service.deleteVacationType(selectedVacation).then(data => {
      var datacode = data.code;
      if (datacode == 200) {
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if(datacode == "302"){
          this.message.type = "error";
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
      } else {
          this.isSuccessMsg = true;
          this.message.type = "success";
          this.isfailureMsg = false;
      }
      //this.message.text = data.btiMessage.message;

      window.scrollTo(0, 0);
      window.setTimeout(() => {
          this.showMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMessage = false;
          }, 4000);
          this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of department
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

  }).catch(error => {
      this.hasMessage = true;
      this.message.type = "error";
      var errorCode = error.status;
      this.message.text = "Server issue. Please contact admin.";
  });
  this.closeModal();
}

closeModal() {
  this.isDeleteAction = false;
  this.isConfirmationModalOpen = false;
}

EscapeModal(event){
      
  var key = event.key;
  if(key=="Escape"){
      this.closeModal();
  }
}

}
