import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { VacationTypeSetupListComponent } from './vacation-type-setup-list.component';
import { VacationTypeSetupListRoutes } from './vacation-type-setup-list.routing';






@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(VacationTypeSetupListRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
  ],

  providers:[AlertService],
  declarations: [VacationTypeSetupListComponent]
})
export class VacationTypeSetupListModule { }
