import { Division } from './../../_models/division/division';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from 'app/_sharedresource/page';
import { DivionService } from 'app/HcmModule/_services/division/division.service';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { Constants } from 'app/_sharedresource/Constants';
import { Observable } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { NgForm } from '@angular/forms';
import { DropDownModule } from 'app/_sharedresource/_modules/drop-down.module';
import { TypeaheadMatch } from 'ngx-bootstrap';


@Component({
  selector: 'app-division-setup-list',
  templateUrl: './division-setup-list.component.html',
  styleUrls: ['./division-setup-list.component.css'],
  providers: [DivionService,CommonService]

})


export class DivisionSetupListComponent implements OnInit {
// export to make it available for other classes

  page = new Page();
  rows = new Array<Division>();
  temp = new Array<Division>();
  selected = [];
  moduleCode = "M-1011";
  screenCode = "S-1221";
  moduleName;
  screenName;
  defaultFormValues: object[];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  divisionId = {};
  searchKeyword = '';
  ddPageSize = 5;
  model: Division;
  cityOptions = [];
  showCreateForm: boolean = false;
  isSuccessMsg;
  isfailureMsg;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText;
  emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}";
  phonePattern = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
  isModalPopUpActive = false;
  currentLanguage: any;
  isConfirmationModalOpen: boolean = false;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  divisionIdvalue: string;
  countries = [];
  states = [];
  cities = [];
  dataSource: Observable<any>;
  divisionIdSearch = [];
  typeaheadLoading: boolean;
  // Level Veriable
  DIVISION_SEARCH: any
  DIVISION_ID: any
  DIVISION_DESCRIPTION: any
  DIVISION_ARABIC_DESCRIPTION: any
  DIVISION_CITY: any
  DIVISION_ADDRESS: any
  DIVISION_PHONE: any
  DIVISION_FAX: any
  DIVISION_EMAIL: any
  DIVISION_ACTION: any
  DIVISION_CREATE_LABEL: any
  DIVISION_UPDATE_LABEL: any
  DIVISION_CANCEL_LABEL: any
  DIVISION_CLEAR_LABEL: any
  DIVISION_SAVE_LABEL: any
  DIVISION_DELETE_LABEL: any
  DIVISION_CREATE_FORM_LABEL: any
  DIVISION_UPDATE_FORM_LABEL: any
  DIVISION_COUNTRY: any
  DIVISION_STATE: any
  DIVISION_TABLEVIEW: any
  tableViews:DropDownModule[] = [
      {id:5, name:'5 at a time'},
      {id:15, name:'15 at a time'},
      {id:50, name:'50 at a time'},
      {id:100, name:'100 at a time'}
  ];

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;

  constructor(
      private router: Router,
      private divisionService: DivionService,
      private getScreenDetailService: GetScreenDetailService,
      private alertService: AlertService,
      private commonService: CommonService) {
      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortOn = 'id';
      this.page.sortBy = 'DESC';
      this.defaultFormValues = [
          { 'fieldName': 'DIVISION_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_CITY', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_PHONE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_FAX', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_EMAIL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_ACTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_STATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'DIVISION_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      ];
      this.DIVISION_SEARCH = this.defaultFormValues[0];
      this.DIVISION_ID = this.defaultFormValues[1];
      this.DIVISION_DESCRIPTION = this.defaultFormValues[2];
      this.DIVISION_ARABIC_DESCRIPTION = this.defaultFormValues[3];
      this.DIVISION_CITY = this.defaultFormValues[4];
      this.DIVISION_ADDRESS = this.defaultFormValues[5];
      this.DIVISION_PHONE = this.defaultFormValues[6];
      this.DIVISION_FAX = this.defaultFormValues[7];
      this.DIVISION_EMAIL = this.defaultFormValues[8];
      this.DIVISION_CREATE_LABEL = this.defaultFormValues[9];
      this.DIVISION_CLEAR_LABEL = this.defaultFormValues[10];
      this.DIVISION_SAVE_LABEL = this.defaultFormValues[11];
      this.DIVISION_CANCEL_LABEL = this.defaultFormValues[12];
      this.DIVISION_UPDATE_LABEL = this.defaultFormValues[13];
      this.DIVISION_ACTION = this.defaultFormValues[14];
      this.DIVISION_DELETE_LABEL = this.defaultFormValues[15];
      this.DIVISION_CREATE_FORM_LABEL = this.defaultFormValues[16];
      this.DIVISION_UPDATE_FORM_LABEL = this.defaultFormValues[17];
      this.DIVISION_STATE = this.defaultFormValues[18];
      this.DIVISION_COUNTRY = this.defaultFormValues[19];
      this.DIVISION_TABLEVIEW = this.defaultFormValues[20];
      this.dataSource = Observable.create((observer: any) => {
          // Runs on every search
          observer.next(this.model.divisionId);
      }).mergeMap((token: string) => this.getReportDivisionAsObservable(token));
  }

  // Screen initialization
  ngOnInit() {
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.currentLanguage = localStorage.getItem('currentLanguage');
      this.setPage({ offset: 0 });
      this.divisionService.getCountry().then(data => {
          this.countries = data.result;
          //console.log(data.result);       
      });

      debugger;
      //getting screen
      this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

      //Following shifted from SetPage for better performance
      this.divisionService.getDivisionClassList().then(data => {
          this.divisionIdSearch = data.result;

         
          //console.log("Data class options : "+data.result);
      });
  }
  // Exiting Id Search on every click
  getReportDivisionAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'i');
      return Observable.of(
          this.divisionIdSearch.filter((id: any) => {
              return query.test(id.divisionId);
          })
      );
  }

  changeTypeaheadLoading(e: boolean): void {
      this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
      // console.log('Selected value: ', e);
      this.divisionService.getDivision(e.item.id).then(allData => {
          // console.log('Data', allData)
          this.divisionService.getStatesByCountryId(allData.result.countryId).then(data => {
              this.states = data.result;
              // console.log(data.result);
              this.divisionService.getCitiesByStateId(allData.result.stateId).then(data => {
                  this.cities = data.result;
                  // console.log(data.result);
                  this.model = allData.result;
                  this.model.id = 0;
                  // console.log('this.model', this.model)
              });
          });

      });
  }

  //setting pagination
  setPage(pageInfo) {
      debugger;
      this.selected = []; // remove any selected checkbox on paging
      this.page.pageNumber = pageInfo.offset;
      if (pageInfo.sortOn == undefined) {
          this.page.sortOn = this.page.sortOn;
      } else {
          this.page.sortOn = pageInfo.sortOn;
      }
      if (pageInfo.sortBy == undefined) {
          this.page.sortBy = this.page.sortBy;
      } else {
          this.page.sortBy = pageInfo.sortBy;
      }
      this.page.searchKeyword = '';
      this.divisionService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.data;
          /*this.divisionService.getCityList().then(data => {
              this.cityOptions = data.result;
          });*/
      });
  }

  create() {
      this.showCreateForm = false;
      this.isUnderUpdate = false;
      setTimeout(() => {
          this.showCreateForm = true;
          setTimeout(() => {
              window.scrollTo(0, 600);
          }, 10);
      }, 10);
      this.model = {
          id: 0,
          arabicDivisionDescription: '',
          divisionAddress: '',
          divisionDescription: '',
          divisionId: '',
          email: '',
          fax: null,
          cityName: '',
          countryName: '',
          stateName: '',
          cityId: null,
          countryId: null,
          stateId: null,
          phoneNumber: null
      };
      this.divisionService.getCountry().then(data => {
          this.countries = data.result;
          //console.log(data.result);       
      });

      this.countries.forEach(ele => {
          if (ele.defaultCountry == 1) {
              this.model.countryId = ele.countryId;
          }
      });

      this.divisionService.getStatesByCountryId(this.model.countryId).then(data => {
          this.states = data.result;
          //console.log(data.result);
          this.model.stateId = this.states[0].stateId;
          this.divisionService.getCitiesByStateId(this.model.stateId).then(data => {
              this.cities = data.result;
              //console.log(data.result);
              this.model.cityId = this.cities[0].cityId;
          });
      });

  }

  // Clear form to reset to default blank
  Clear(f: NgForm) {
      f.resetForm({ country: 0, state: 0, city: 0 });
  }

  //function call for creating new division
  CreateDivision(f: NgForm, event: Event) {
      event.preventDefault();
      var divIdx = this.model.divisionId;
      //Check if the id is available in the model.
      //If id avalable then update the division, else Add new division.
      if (this.model.id > 0 && this.model.id != undefined) {
          this.isConfirmationModalOpen = true;
          this.isDeleteAction = false;
      } else {
          //Check for duplicate Division Id according to it create new division
          this.divisionService.checkDivisionId(divIdx).then(res => {
              if (res && res.result && res.result.isRepeat) {
                  this.duplicateWarning = true;
                  this.message.type = "success";

                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.hasMessage = false;
                      this.showMsg = true;
                      window.setTimeout(() => {
                          this.showMsg = false;
                          this.duplicateWarning = false;
                      }, 4000);
                      this.message.text = res.btiMessage.message;
                  }, 100);
              } else {
                  //Call service api for Creating new division
                  this.divisionService.createDivision(this.model).then(data => {

                      var datacode = data.code;
                      if (datacode == 201) {
                          window.scrollTo(0, 0);
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.showCreateForm = false;
                              this.messageText = data.btiMessage.message;;
                          }, 100);

                          this.hasMsg = true;

                          f.resetForm();
                          this.states = [];
                          this.cities = [];
                          //Refresh the Grid data after adding new division
                          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                      }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = "Server error. Please contact admin.";
                      }, 100)
                  });
              }

          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = "Server error. Please contact admin.";
              }, 100);
          });
      }
  }

  //edit division by row id
  editrow(row: Division) {
      //console.log('row ', row)
      this.showCreateForm = true;
      this.model = Object.assign({}, row);
      this.isUnderUpdate = true;
      this.divisionId = row.divisionId;
      this.divisionIdvalue = this.model.divisionId;
      this.divisionService.getStatesByCountryId(row.countryId).then(data => {
          this.states = data.result;
          //console.log(data.result);       
      });
      this.divisionService.getCitiesByStateId(row.stateId).then(data => {
          this.cities = data.result;
          //console.log(data.result);       
      });
      setTimeout(() => {
          window.scrollTo(0, 500);
      }, 10);
  }

  updateStatus() {
      this.closeModal();
      this.model.divisionId = this.divisionIdvalue;
      //Call service api for updating selected division
      this.divisionService.updateDivision(this.model).then(data => {
          var datacode = data.code;
          if (datacode == 201) {
              //Refresh the Grid data after editing department
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
              this.states = [];
              this.cities = [];
              //Scroll to top after editing department
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                  }, 4000);
                  this.messageText = data.btiMessage.message;
                  this.showCreateForm = false;
              }, 100);

              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
          }
      }).catch(error => {
          this.hasMsg = true;
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = "Server error. Please contact admin.";
          }, 100)
      });
  }

  varifyDelete() {
      if (this.selected.length > 0) {
          this.showCreateForm = false;
          this.isDeleteAction = true;
          this.isConfirmationModalOpen = true;
          this.confirmationModalBody = this.deleteConfirmationText;
           
            
          
      } else {
          this.isSuccessMsg = false;
          this.hasMessage = true;
          this.message.type = 'error';
          this.isfailureMsg = true;
          this.showMsg = true;
          this.message.text = 'Please select at least one record to delete.';
          window.scrollTo(0, 0);
      }
  }

  //delete one or multiple divisions
  delete() {
      var selectedDivisions = [];
      for (var i = 0; i < this.selected.length; i++) {
          selectedDivisions.push(this.selected[i].id);
      }
      this.divisionService.deleteDivision(selectedDivisions).then(data => {
          var datacode = data.code;
          if (datacode == 200) {
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
          }
          this.hasMessage = true;
          if(datacode == "302"){
              this.message.type = "error";
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
          } else {
              this.isSuccessMsg = true;
              this.message.type = "success";
              this.isfailureMsg = false;
          }
          
          //this.message.text = data.btiMessage.message;

          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.showMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMessage = false;
              }, 4000);
              this.message.text = data.btiMessage.message;
          }, 100);

          //Refresh the Grid data after deletion of division
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

      }).catch(error => {
          this.hasMessage = true;
          this.message.type = "error";
          var errorCode = error.status;
          this.message.text = "Server issue. Please contact admin.";
      });
      this.closeModal();
  }

  // default list on page
  onSelect({ selected }) {
      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
  }

  onCountrySelect(event) {
      this.model.stateId = 0;
      this.model.cityId = 0;
      this.divisionService.getStatesByCountryId(event.target.value).then(data => {
          this.states = data.result;
          //console.log(data.result);       
      });
      /* this.states = this._dataService.getStates()
                    .filter((item)=> item.countryid == countryid);*/
      this.cities = [];
  }

  onStateSelect(event) {
      this.model.cityId = 0;
      this.divisionService.getCitiesByStateId(event.target.value).then(data => {
          this.cities = data.result;
          //console.log(data.result);       
      });
      /* this.states = this._dataService.getStates()
                    .filter((item)=> item.countryid == countryid);*/
  }
  // search rolegroup details by group name 
  updateFilter(event) {
      this.searchKeyword = event.target.value.toLowerCase();
      this.page.pageNumber = 0;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.table.offset = 0;
      // this.divisionService.searchDivisionlist(this.page, this.searchKeyword).subscribe(pagedData => {
      //     this.page = pagedData.page;
      //     this.rows = pagedData.data;
      //     this.table.offset = 0;
      // });
  }

  // Set default page size
  changePageSize(event) {
      this.page.size = event.target.value;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  confirm(): void {
      this.messageText = 'Confirmed!';
      this.delete();
  }

  closeModal() {
      this.isDeleteAction = false;
      this.isConfirmationModalOpen = false;
  }

  keyPress(event: any) {
      const pattern = /^[0-9\-()]+$/;

      let inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode != 8 && !pattern.test(inputChar)) {
          event.preventDefault();
      }
  }

  sortColumn(val) {
      if (this.page.sortOn == val) {
          if (this.page.sortBy == 'DESC') {
              this.page.sortBy = 'ASC';
          } else {
              this.page.sortBy = 'DESC';
          }
      }
      this.page.sortOn = val;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  

}

goToCreate(){
    this.router.navigate(['createdivsionsetup']);   
  }


  edit(event) {
   
   
    if(event.cellIndex!=0 && event.cellIndex!=1){
        this.divisionId = event.row.id;
      
        var myurl = `${'createdivsionsetup'}/${this.divisionId}`;
        this.router.navigateByUrl(myurl);
    }
}

EscapeModal(event){
        
    var key = event.key;
    if(key=="Escape"){
        this.closeModal();
    }
  }

}

