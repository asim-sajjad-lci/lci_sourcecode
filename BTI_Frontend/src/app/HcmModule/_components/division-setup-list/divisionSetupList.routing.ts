import { DivisionSetupListComponent } from './division-setup-list.component';
import { Routes } from '@angular/router';




export const DivisionSetupListRoutes: Routes = [{
  path: '',
  component: DivisionSetupListComponent,
  data: {
    breadcrumb: ""
  }
}];
