import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionSetupListComponent } from './division-setup-list.component';

describe('DivisionSetupListComponent', () => {
  let component: DivisionSetupListComponent;
  let fixture: ComponentFixture<DivisionSetupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivisionSetupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionSetupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
