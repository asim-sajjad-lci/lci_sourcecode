
import { Routes } from '@angular/router';
import { SkillSetSetupComponent } from './skill-set-setup.component';




export const SkillSetSetupRoutes: Routes = [{
  path: '',
  component: SkillSetSetupComponent,
  data: {
    breadcrumb: ""
  }
}];
