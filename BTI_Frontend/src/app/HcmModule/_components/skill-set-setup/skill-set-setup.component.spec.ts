import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillSetSetupComponent } from './skill-set-setup.component';

describe('SkillSetSetupComponent', () => {
  let component: SkillSetSetupComponent;
  let fixture: ComponentFixture<SkillSetSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillSetSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillSetSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
