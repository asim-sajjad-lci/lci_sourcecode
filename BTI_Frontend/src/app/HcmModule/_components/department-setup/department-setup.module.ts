import { DepartmentSetupComponent } from './department-setup.component';
import { DepartmentSetupRoutes } from './department-setup.routing';
// import { PositionClassRoutes } from './position-class.routing';
// import { PositionClassComponent } from './position-class.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DepartmentSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
  ],

  providers:[AlertService],
  declarations: [DepartmentSetupComponent]
})
export class DepartmentSetupModule { }
