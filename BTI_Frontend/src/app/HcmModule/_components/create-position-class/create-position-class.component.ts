// import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PositionClassModule } from '../../_models/position-class/position-class.module';
import { Page } from '../../../_sharedresource/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Constants } from '../../../_sharedresource/Constants';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
// import { Router } from '@angular/router';

import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { PositionClassService } from '../../_services/position-class/position-class.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
// import { DropDownModule } from '../../../_sharedresource/_module/drop-down.module';
import { DropDownModule } from 'app/_sharedresource/_modules/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';

@Component({
  selector: 'app-create-position-class',
  templateUrl: './create-position-class.component.html',
  styleUrls: ['./create-position-class.component.css'],
  providers: [PositionClassService,CommonService]
})
export class CreatePositionClassComponent implements OnInit {
    @ViewChild('filterInput1') filterInput1: ElementRef;
  page = new Page();
  rows = new Array<PositionClassModule>();
  temp = new Array<PositionClassModule>();
  selected = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1311';
  moduleName;
  screenName;
  defaultFormValues: object[];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  positionClassId = {};
  searchKeyword = '';
  ddPageSize: number = 5;
  // model: PositionClassModule;
  model: any = {};
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  positionClassIdvalue: string;
  positionclassIdList: Observable<any>;
  getpositionClassId: any[] = [];
  typeaheadLoading: boolean;
  isPositionClassOpen:boolean=false;

  // leval Veriable
  POSITION_CLASS_DESCRIPTION: any;
  POSITION_CLASS_ID: any;
  POSITION_CLASS_SEARCH: any;
  POSITION_CLASS_ARABIC_DESCRIPTION: any;
  POSITION_CLASS_ACTION: any;
  POSITION_CLASS_CREATE_LABEL: any;
  POSITION_CLASS_SAVE_LABEL: any;
  POSITION_CLASS_CLEAR_LABEL: any;
  POSITION_CLASS_CANCEL_LABEL: any;
  POSITION_CLASS_UPDATE_LABEL: any;
  POSITION_CLASS_DELETE_LABEL: any;
  POSITION_CLASS_CREATE_FORM_LABEL: any;
  POSITION_CLASS_UPDATE_FORM_LABEL: any;
  POSITION_CLASS_TABLEVIEW: any;
 
  selectedPositionClass;



  tableViews: DropDownModule[] = [
      { id: 5, name: '5 at a time' },
      { id: 15, name: '15 at a time' },
      { id: 50, name: '50 at a time' },
      { id: 100, name: '100 at a time' }
  ];

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('target') private myScrollContainer: ElementRef;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private positionClassService: PositionClassService,
      private getScreenDetailService: GetScreenDetailService,
      private alertService: AlertService,
      private commonService: CommonService) {
      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortOn = 'id';
      this.page.sortBy = 'DESC';
      // default form parameter for position  screen
      this.defaultFormValues = [
          { 'fieldName': 'POSITION_CLASS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
          { 'fieldName': 'POSITION_CLASS_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      ];
      this.POSITION_CLASS_DESCRIPTION = this.defaultFormValues[0];
      this.POSITION_CLASS_ID = this.defaultFormValues[1];
      this.POSITION_CLASS_SEARCH = this.defaultFormValues[2];
      this.POSITION_CLASS_ARABIC_DESCRIPTION = this.defaultFormValues[3];
      this.POSITION_CLASS_ACTION = this.defaultFormValues[4];
      this.POSITION_CLASS_CREATE_LABEL = this.defaultFormValues[5];
      this.POSITION_CLASS_SAVE_LABEL = this.defaultFormValues[6];
      this.POSITION_CLASS_CLEAR_LABEL = this.defaultFormValues[7];
      this.POSITION_CLASS_CANCEL_LABEL = this.defaultFormValues[8];
      this.POSITION_CLASS_UPDATE_LABEL = this.defaultFormValues[9];
      this.POSITION_CLASS_DELETE_LABEL = this.defaultFormValues[10];
      this.POSITION_CLASS_CREATE_FORM_LABEL = this.defaultFormValues[11];
      this.POSITION_CLASS_UPDATE_FORM_LABEL = this.defaultFormValues[12];
      this.POSITION_CLASS_TABLEVIEW = this.defaultFormValues[13];

      this.positionclassIdList = Observable.create((observer: any) => {
          // Runs on every search
          observer.next(this.model.positionClassId);
      }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));
  }

  ngOnInit() {

      
    this.route.params.subscribe((params: Params) => {
        this.positionClassId = params['positionClassId']; 

        if (this.positionClassId != '' && this.positionClassId != undefined) {

            this.getpositionClassById(this.positionClassId);
            this.isUnderUpdate=true;
            this.onAutocompleteFocus('lblPositionClassId');
        }

    });




      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.currentLanguage = localStorage.getItem('currentLanguage');

      // getting screen labels, help messages and validation messages
      this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

      //Following shifted from SetPage for better performance
      this.positionClassService.getPositionClassId().then(data => {
          this.getpositionClassId = data.result.records;
          //console.log("Data class options : " + this.getpositionClassId);
      });
  }

//   setAutocompleteLabel(){
//     this.onAutocompleteFocus('lblDivisionId');
//     this.onAutocompleteFocus('lblCountry');
//     this.onAutocompleteFocus('lblState');
//     this.onAutocompleteFocus('lblCity');
// }
  getSuperviserIdAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'i');
      return Observable.of(
          this.getpositionClassId.filter((id: any) => {
              return query.test(id.positionClassId);
          })
      );
  }

  changeTypeaheadLoading(e: boolean): void {
      this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
      console.log('Selected value: ', e);
      this.positionClassService.getPositionClass(e.item.id).then(pagedData => {
          this.model = pagedData.result;
          this.model.id = 0;
      });
  }


  // setting pagination
  setPage(pageInfo) {
       debugger;
      this.selected = []; // remove any selected checkbox on paging
      this.page.pageNumber = pageInfo.offset;
      if (pageInfo.sortOn == undefined) {
          this.page.sortOn = this.page.sortOn;
      } else {
          this.page.sortOn = pageInfo.sortOn;
      }
      if (pageInfo.sortBy == undefined) {
          this.page.sortBy = this.page.sortBy;
      } else {
          this.page.sortBy = pageInfo.sortBy;
      }


      this.page.searchKeyword = '';
      this.positionClassService.getPositionClassList().then(pagedData => {
          this.page = pagedData.page;
          this.rows = pagedData.result.records;
      });
  }

  // Open form for create position class
  Create() {
      this.showCreateForm = false;
      this.isUnderUpdate = false;
      setTimeout(() => {
          this.showCreateForm = true;
          setTimeout(() => {
              window.scrollTo(0, 2000);
          }, 10);
      }, 10);
      this.model = {
          id: 0,
          arabicPositionClassDescription: '',
          positionClassDescription: '',
          positionClassId: ''
      };
  }

  // Clear form to reset to default blank
  Clear(f: NgForm) {
      this.model=[];
      this.onAutocompleteFocusOut("lblPositionClassId");
      f.resetForm();
  }

  // function call for creating new positionClass
  CreatePositionClass(f: NgForm, event: Event) {
      event.preventDefault();
      let posIdx = this.model.positionClassId;

      // Check if the id is available in the model.
      // If id avalable then update the position class, else Add new position class.
      if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
          this.isConfirmationModalOpen = true;
          this.isDeleteAction = false;
      } else {
          // Check for duplicate positionClass Id according to it create new position class
          this.positionClassService.checkDuplicatePositionClassId(posIdx).then(response => {
              if (response && response.code === 302 && response.result && response.result.isRepeat) {
                  this.duplicateWarning = true;
                  this.message.type = 'success';

                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      window.setTimeout(() => {
                          this.showMsg = false;
                          this.duplicateWarning = false;
                      }, 4000);
                      this.message.text = response.btiMessage.message;
                     
                  }, 100);
              } else {
                  // Call service api for Creating new position class
                  this.positionClassService.createPositionClass(this.model).then(data => {
                      let datacode = data.code;
                      if (datacode == 201) {
                   
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                    
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                        this.goBack();
                          
                        
                    }, 4000);
                    
                }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = 'Server error. Please contact admin.';
                      }, 100);
                  });
              }

          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = 'Server error. Please contact admin.';
              }, 100);
          });

      }
  }

  // edit position class by row
  edit(row: PositionClassModule) {
      this.showCreateForm = true;
      this.model = Object.assign({}, row);
      this.isUnderUpdate = true;
      this.positionClassId = row.positionClassId;
      this.positionClassIdvalue = this.model.positionClassId;
      setTimeout(() => {
          window.scrollTo(0, 2000);
      }, 10);
  }

  updateStatus() {
      this.closeModal();
      // Call service api for updating selected position class      selectedDivision
      this.model.positionClassId = this.selectedPositionClass;
      this.positionClassService.updatePositionClass(this.model).then(data => {
          let datacode = data.code;
          if (datacode == 201) {
                   
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
        
        this.showMsg = true;
        this.messageText = data.btiMessage.message;
        this.hasMsg = true;
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
            // this.goBack();
              
            
        }, 4000);
        
    }
      }).catch(error => {
          this.hasMsg = true;
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = 'Server error. Please contact admin.';
          }, 100);
      });
  }

  varifyDelete() {
      if (this.selected.length > 0) {
          this.showCreateForm = false;
          this.isDeleteAction = true;
          this.isConfirmationModalOpen = true;
      } else {
          this.isSuccessMsg = false;
          this.hasMessage = true;
          this.message.type = 'error';
          this.isfailureMsg = true;
          this.showMsg = true;
          this.message.text = 'Please select at least one record to delete.';
          window.scrollTo(0, 0);
      }
  }
  // delete position class by passing whole object of perticular position class
  delete() {
      let selectedPositionClass = [];
      for (let i = 0; i < this.selected.length; i++) {
          selectedPositionClass.push(this.selected[i].id);
      }
      this.positionClassService.deletePositionClass(selectedPositionClass).then(data => {
          let datacode = data.code;
          if (datacode === 200) {
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
          }
          this.hasMessage = true;
          if(datacode == "302"){
              this.message.type = "error";
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
          } else {
              this.isSuccessMsg = true;
              this.message.type = "success";
              this.isfailureMsg = false;
          }
          // this.message.text = data.btiMessage.message;

          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.showMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMessage = false;
              }, 4000);
              this.message.text = data.btiMessage.message;
          }, 100);

          // Refresh the Grid data after deletion of position class
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

      }).catch(error => {
          this.hasMessage = true;
          this.message.type = 'error';
          let errorCode = error.status;
          this.message.text = 'Server issue. Please contact admin.';
      });
      this.closeModal();
  }

  // default list on page
  onSelect({ selected }) {
      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
  }

  // search position class by keyword
  updateFilter(event) {
      this.searchKeyword = event.target.value.toLowerCase();
      this.page.pageNumber = 0;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      this.table.offset = 0;
      // this.positionClassService.searchPositionClasslist(this.page, this.searchKeyword).subscribe(pagedData => {
      //     this.page = pagedData.page;
      //     this.rows = pagedData.data;
      //     this.table.offset = 0;
      // });
  }


  // Set default page size
  changePageSize(event) {
      this.page.size = event.target.value;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  confirm(): void {
      this.messageText = 'Confirmed!';
      // this.modalRef.hide();
      this.delete();
  }

  closeModal() {
      this.isDeleteAction = false;
      this.isConfirmationModalOpen = false;
  }

  sortColumn(val) {
      if (this.page.sortOn == val) {
          if (this.page.sortBy == 'DESC') {
              this.page.sortBy = 'ASC';
          } else {
              this.page.sortBy = 'DESC';
          }
      }
      this.page.sortOn = val;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }


  onAutocompleteFocus(labelId){

    let elem: HTMLElement = document.getElementById(labelId);
    elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
  }
  
  onAutocompleteFocusOut(labelId){
      let elem: HTMLElement = document.getElementById(labelId);
      elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
      
  }




getPositionClassData(event){
   
    var posId = event.id ? event.id : -1;
  
    this.model.positionClassId=posId;

    if(event.id>0 && event.id!=undefined)
    {
        this.getpositionClassById(event.id);
    }

  
  
   //  this.divisionService.getDivision(divId).then(data => {
   //     this.model.countryCode = data.result.countryCode;
   // });
   //   this.companyService.getStateList(countryId).then(data => {
   //       this.stateOptions = data.result;
   //       this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
   //   });
}

positionClassFocusIn(){
    this.isPositionClassOpen=true;

  }
  positionClassFocusOut(){

    this.isPositionClassOpen=false;

  }

toggle1() {
     
      
    this.isPositionClassOpen=!this.isPositionClassOpen;
    // this.selectedDepartmentSetup ='';
    
      this.filterInput1.nativeElement.focus();
     
  }
  goBack(){
    this.router.navigate(['hcm/positionClass']); 
  }

  
  getpositionClassById(posId){
    this.positionClassService.getPositionClass(posId).then(allData => {
        this.isUnderUpdate=true;
        this.onAutocompleteFocus('lblPositionClassId');
        console.log("selectedPositionClass.........",allData)
        this.model.id=allData.result.id;
        this.selectedPositionClass=allData.result.positionClassId;
        // this.selectedCountry=allData.result.countryName;
        // this.selectedState=allData.result.stateName;
        // this.selectedCity=allData.result.cityName;


        // console.log('Data', allData)
        // this.positionClassService.getStatesByCountryId(allData.result.countryId).then(data => {
        //     // this.states = data.result;
        //     // console.log(data.result);
        //     this.positionClassService.getCitiesByStateId(allData.result.stateId).then(data => {
        //         this.cities = data.result;
                // console.log(data.result);
                this.model = allData.result;
                // this.model.id = 0;
                // console.log('this.model', this.model)
            });
    //     });

    // });

  }

  EscapeModal(event){
        
    var key = event.key;
    if(key=="Escape"){
        this.closeModal();
    }
  }

}





