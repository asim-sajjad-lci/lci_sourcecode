import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePositionClassComponent } from './create-position-class.component';

describe('CreatePositionClassComponent', () => {
  let component: CreatePositionClassComponent;
  let fixture: ComponentFixture<CreatePositionClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePositionClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePositionClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
