import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionPayCodeComponent } from './position-pay-code.component';

describe('PositionPayCodeComponent', () => {
  let component: PositionPayCodeComponent;
  let fixture: ComponentFixture<PositionPayCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PositionPayCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionPayCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
