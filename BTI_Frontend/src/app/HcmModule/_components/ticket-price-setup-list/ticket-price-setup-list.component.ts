import { Component, OnInit, ViewChild } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { TicketClassSetupService } from 'app/HcmModule/_services/ticket-class-setup/ticket-class-setup.service';
import { Page } from 'app/_sharedresource/page';
import { TicketPriceSetupService } from 'app/HcmModule/_services/ticket-price-setup/ticket-price-setup.service';

@Component({
  selector: 'app-ticket-price-setup-list',
  templateUrl: './ticket-price-setup-list.component.html',
  styleUrls: ['./ticket-price-setup-list.component.css'],
  providers:[CommonService,TicketPriceSetupService]
})
export class TicketPriceSetupListComponent implements OnInit {

  page = new Page();
  rows = {};
  selected = [];

  moduleCode = "M-1011";
  screenCode = "S-1792";
  moduleName;
  screenName;
  currentLanguage: any;
  defaultFormValues: object[];
  searchKeyword = '';
  ticketRouteId;
  ddPageSize: number = 5;

  isDeleteAction: boolean = false;
  isConfirmationModalOpen: boolean = false;

  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;

  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };

  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;

  TICKETPRICE_ROUTEID: any;
  TICKETPRICE_CLASSID: any;
  TICKETPRICE_SEARCH: any;
  TICKETPRICE_ADULT: any;
  TICKETPRICE_CHILD: any;
  TICKETPRICE_INFANT: any;
  TICKETPRICE_CREATE_LABEL: any;
  TICKETPRICE_SAVE_LABEL: any;
  TICKETPRICE_CLEAR_LABEL: any;
  TICKETPRICE_CANCEL_LABEL: any;
  TICKETPRICE_UPDATE_LABEL: any;
  TICKETPRICE_DELETE_LABEL: any;
  TICKETPRICE_CREATE_FORM_LABEL: any;
  TICKETPRICE_UPDATE_FORM_LABEL: any;
  TICKETPRICE_LIST: any;
  TICKETPRICE_LABEL: any;
  
  createbtnText=Constants.createButtonText;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  
  constructor(private router: Router,
    private commonService: CommonService,
    private service: TicketPriceSetupService) {

      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortOn = 'id';
      this.page.sortBy = 'DESC';
      
    this.defaultFormValues = [
      { 'fieldName': 'TICKETPRICE_ROUTEID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_CLASSID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_ADULT', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_CHILD', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_INFANT', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_LIST', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETPRICE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
      { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
  ];
  this.TICKETPRICE_ROUTEID = this.defaultFormValues[0];
  this.TICKETPRICE_CLASSID = this.defaultFormValues[1];
  this.TICKETPRICE_SEARCH = this.defaultFormValues[2];
  this.TICKETPRICE_ADULT = this.defaultFormValues[3];
  this.TICKETPRICE_CHILD = this.defaultFormValues[4];
  this.TICKETPRICE_INFANT = this.defaultFormValues[5];
  this.TICKETPRICE_CREATE_LABEL = this.defaultFormValues[6];
  this.TICKETPRICE_SAVE_LABEL = this.defaultFormValues[7];
  this.TICKETPRICE_CLEAR_LABEL = this.defaultFormValues[8];
  this.TICKETPRICE_CANCEL_LABEL = this.defaultFormValues[9];
  this.TICKETPRICE_UPDATE_LABEL = this.defaultFormValues[10];
  this.TICKETPRICE_DELETE_LABEL = this.defaultFormValues[11];
  this.TICKETPRICE_CREATE_FORM_LABEL = this.defaultFormValues[12];
  this.TICKETPRICE_UPDATE_FORM_LABEL = this.defaultFormValues[13];
  this.TICKETPRICE_LIST = this.defaultFormValues[14];
  this.TICKETPRICE_LABEL = this.defaultFormValues[15];



   }

  ngOnInit() {

    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


  }

  goToCreate(){
    this.router.navigate(['createTicketPriceSetup']);   
  }

  //setting pagination
  setPage(pageInfo) {
    debugger;
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
        this.page.sortOn = this.page.sortOn;
    } else {
        this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
        this.page.sortBy = this.page.sortBy;
    } else {
        this.page.sortBy = pageInfo.sortBy;
    }

    this.page.searchKeyword = '';
    this.service.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        debugger;
        this.page = pagedData.page;
        this.rows = pagedData.data;
    });
}

  // search ticket class by keyword 
  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
    // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
    //     this.page = pagedData.page;
    //     this.rows = pagedData.data;
    //     this.table.offset = 0;
    // });
}

edit(event) {
   
   debugger;
  if(event.cellIndex!=0 ){
      this.ticketRouteId = event.row.id;
    
      var myurl = `${'createTicketPriceSetup'}/${this.ticketRouteId}`;
      this.router.navigateByUrl(myurl);
  }
}

changePageSize(event) {
  this.page.size = event.target.value;
  this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

varifyDelete() {
  debugger;
  if (this.selected.length > 0) {
    //  this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
      this.confirmationModalBody = this.deleteConfirmationText;

  } else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;
      this.message.text = 'Please select at least one record to delete.';
      window.scrollTo(0, 0);
  }
}



//delete ticket price by passing whole object of perticular ticket price
delete() {
  debugger;
  var selectedPrice = [];
  for (var i = 0; i < this.selected.length; i++) {
    selectedPrice.push(this.selected[i].id);
  }
  this.service.deleteTicketPrice(selectedPrice).then(data => {
      var datacode = data.code;
      if (datacode == 200) {
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if(datacode == "302"){
          this.message.type = "error";
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
      } else {
          this.isSuccessMsg = true;
          this.message.type = "success";
          this.isfailureMsg = false;
      }
      //this.message.text = data.btiMessage.message;

      window.scrollTo(0, 0);
      window.setTimeout(() => {
          this.showMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMessage = false;
          }, 4000);
          this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of ticket class
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

  }).catch(error => {
      this.hasMessage = true;
      this.message.type = "error";
      var errorCode = error.status;
      this.message.text = "Server issue. Please contact admin.";
  });
  this.closeModal();
}

closeModal() {
  this.isDeleteAction = false;
  this.isConfirmationModalOpen = false;
}

EscapeModal(event){
      
  var key = event.key;
  if(key=="Escape"){
      this.closeModal();
  }
}

}