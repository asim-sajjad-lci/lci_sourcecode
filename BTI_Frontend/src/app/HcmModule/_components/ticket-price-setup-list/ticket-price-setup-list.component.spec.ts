import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketPriceSetupListComponent } from './ticket-price-setup-list.component';

describe('TicketPriceSetupListComponent', () => {
  let component: TicketPriceSetupListComponent;
  let fixture: ComponentFixture<TicketPriceSetupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketPriceSetupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketPriceSetupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
