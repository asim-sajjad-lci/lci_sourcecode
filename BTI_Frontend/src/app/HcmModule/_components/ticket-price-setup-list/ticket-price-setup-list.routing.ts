import { Routes } from '@angular/router';
import { TicketPriceSetupListComponent } from './ticket-price-setup-list.component';


export const TicketPriceSetupListRoutes: Routes = [{
  path: '',
  component: TicketPriceSetupListComponent,
  data: {
    breadcrumb: ""
  }
}];