import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDepartmentSetupComponent } from './create-department-setup.component';

describe('CreateDepartmentSetupComponent', () => {
  let component: CreateDepartmentSetupComponent;
  let fixture: ComponentFixture<CreateDepartmentSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDepartmentSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDepartmentSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
