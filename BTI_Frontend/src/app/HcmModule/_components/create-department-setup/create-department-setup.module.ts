import { CreateDepartmentSetupRoutes } from './create-department-setup.routing';
import { CreateDepartmentSetupComponent } from './create-department-setup.component';

import { CommonModule } from '@angular/common';
import { AlertService } from './../../../_sharedresource/_services/alert.service';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
// import { CreatePositionClassRoutes } from './create-position-class.routing';



@NgModule({
  imports: [
   
    CommonModule,
    RouterModule.forChild(CreateDepartmentSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,

  ],

  
  providers:[AlertService],
  declarations: [CreateDepartmentSetupComponent]
})
export class CreateDepartmentSetupModule { }
