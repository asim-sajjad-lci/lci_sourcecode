import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePositionSetupComponent } from './create-position-setup.component';

describe('CreatePositionSetupComponent', () => {
  let component: CreatePositionSetupComponent;
  let fixture: ComponentFixture<CreatePositionSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePositionSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePositionSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
