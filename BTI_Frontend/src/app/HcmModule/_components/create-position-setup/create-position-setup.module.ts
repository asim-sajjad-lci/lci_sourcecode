import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';

import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import { CreatePositionSetupRoutes } from './create-position-setup.routing';
import { CreatePositionClassComponent } from '../create-position-class/create-position-class.component';
import { CreatePositionSetupComponent } from './create-position-setup.component';
import { SecurePipe } from '../../../_sharedresource/secure.pipe';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';









@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreatePositionSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule,
    FormsModule,ReactiveFormsModule
  ],

  providers:[AlertService],
  declarations: [CreatePositionSetupComponent,SecurePipe]
})
export class CreatePositionSetupModule { }
