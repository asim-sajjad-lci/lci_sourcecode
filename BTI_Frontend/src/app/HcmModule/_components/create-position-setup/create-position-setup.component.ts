
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Page } from '../../../_sharedresource/page';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm, FormControl, FormGroup, Validators } from '@angular/forms';

import { PositionSetupService } from '../../_services/position-setup/position-setup.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

import { RoutesRecognized } from '@angular/router';
import * as cloneDeep from 'lodash/cloneDeep';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { PositionSetupModule } from 'app/HcmModule/_models/position-setup/position-setup';
import { PositionAttchmentService } from 'app/HcmModule/_services/position-attchment/position-attchment.service';
import { UploadFileService } from 'app/_sharedresource/_services/upload-file.service';
import { PositionClassService } from 'app/HcmModule/_services/position-class/position-class.service';
import { SkillSetSetupService } from 'app/HcmModule/_services/skill-set-setup/skill-set-setup.service';
import { PositionAttachmentModule } from 'app/HcmModule/_models/position-attachment/position-attachment';
import { DomSanitizer } from '@angular/platform-browser';
import {  ViewChildren } from '@angular/core';
import {  Input, forwardRef, HostListener, QueryList } from '@angular/core';

@Component({
  selector: 'app-create-position-setup',
  templateUrl: './create-position-setup.component.html',
  styleUrls: ['./create-position-setup.component.css'],
  providers: [PositionSetupService, PositionAttchmentService,CommonService,UploadFileService,PositionClassService,SkillSetSetupService]
})
export class CreatePositionSetupComponent implements OnInit {
   
    @ViewChild('filterInput') filterInput: ElementRef;
    @ViewChild('filterInput1') filterInput1: ElementRef;
    @ViewChild('filterInput2') filterInput2: ElementRef;
    @ViewChild('filterInput3') filterInput3: ElementRef;
    @ViewChild('filterInput4') filterInput4: ElementRef;
    @ViewChildren('listItems') listItems: QueryList<ElementRef>;

    
    isListHide = true;
  page = new Page();
    rows = new Array<PositionSetupModule>();
    temp = new Array<PositionSetupModule>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1313';
    positionClassscreenCode = 'S-1311';
    skillsetscreenCode='S-1421';
    moduleName;
    screenName;
    defaultFormValues: any = [];
    availableFormValues: [object];
    defaultFormValuesPositionclass:any=[];
    defaultFormValuesSkillset:any=[];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    positionId;
    positionClassId;
    positionClassOptions;
    skillSetIdOptions = [];
    searchKeyword = '';
    ddPageSize: number = 5;
    // model: PositionSetupModule;
    model:any={};
    posclsmodel:any={};
    skillSetmodel:any={};
    
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    searchValue:boolean;
    searchValue1:boolean;
    positionClass:boolean;
    hasMsg = false;
    showMsg = false;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    isAddClassModalOpen:boolean=false;
    isAddSkillsetModalOpen:boolean=false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    positionIdvalue: string;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    dataSource: Observable<any>;
    asyncSelected: string;
    typeaheadLoading: boolean;
    typeaheadNoResults: boolean;
    isAttached: boolean = false;
    uploadfile: any;
    getPositionID: any[] = [];
    positionIdList: Observable<any>;
    previousUrl: boolean = false;
    isReporttopositionValid:boolean=true;
    isattConfirmationModalOpen:boolean=false;

    isPositionOpen:boolean=false;
    isPositionClassOpen:boolean=false;
    isSkillOpen:boolean=false;
    isReportOpen:boolean=false;
    // isPositionClassOpen=true;

            POSITION_SETUP_SEARCH: any;
            POSITION_SETUP_ID:any;
            POSITION_SETUP_DESCRIPTION:any;
            POSITION_SETUP_ARABIC_DESCRIPTION:any;
            POSITION_SETUP_REPORT_TO_POSITION:any;
            POSITION_CLASS:any;
            POSITION_SETUP_LONG_DESCRIPTION:any;
            POSITION_SETUP_DEFAULT_SKILL_ID:any;
            POSITION_SETUP_ACTION:any;
            POSITION_SETUP_CREATE_LABEL:any;
            POSITION_SETUP_SAVE_LABEL:any;
            POSITION_SETUP_CLEAR_LABEL:any;
            POSITION_SETUP_CANCEL_LABEL:any;
            POSITION_SETUP_UPDATE_LABEL:any;
            POSITION_SETUP_DELETE_LABEL:any;
            POSITION_SETUP_CREATE_FORM_LABEL:any;
            POSITION_SETUP_UPDATE_FORM_LABEL:any;
            POSITION_SETUP_TRAINING_FORM_LABEL:any;
            POSITION_SETUP_PLAN_FORM_LABEL:any;
            MANAGE_USER_TABLE_VIEW:any;
            MANAGE_USER_TABLE_ACCESS:any;
            MANAGE_USER_TABLE_DELETE:any;
            MANAGE_USER_TEXT_ADD_NEW_USER:any;
            MANAGE_USER_TABLE_STATE:any;
            MANAGE_USER_TABLE_CITY:any;
            MANAGE_USER_TABLE_POSTALCODE:any;
            MANAGE_USER_TABLE_DOB:any;
            POSITION_SETUP_REPORTS_TO_POSITION:any;

              //Position class label variables
    POSITION_CLASS_DESCRIPTION: any;
    POSITION_CLASS_ID: any;
    POSITION_CLASS_ARABIC_DESCRIPTION: any;
    POSITION_CLASS_CREATE_LABEL: any;
    POSITION_CLASS_SAVE_LABEL: any;
    POSITION_CLASS_CLEAR_LABEL: any;
    POSITION_CLASS_CREATE_FORM_LABEL: any;
    POSITION_CLASS_CANCEL_LABEL: any;


    //for position class create
    isclsSuccessMsg: boolean;
    isclsfailureMsg: boolean;
    hasclsMessage = false;
    showclsMsg = false;
    hasclsMsg=false;
    clsmessageText;


    //Skillset label variables
     // Label Veriable
     SKILL_SET_SETUP_SET_ID:any;
     SKILL_SET_SETUP_DESCRIPTION:any;
     SKILL_SET_SETUP_ARABIC_DESCRIPTION:any;
     SKILL_SET_SETUP_SKILL_ID:any;
     SKILL_SET_SETUP_SKILL_REQUIRED:any;
     SKILL_SET_SETUP_COMMENT:any;
     SKILL_SET_SETUP_SEQUENCE:any;
     SKILL_SET_SETUP_DELETE_LABEL:any;
     SKILL_SET_SETUP_SAVE_LABEL:any;
     SKILL_SET_SETUP_CLEAR_LABEL:any;
     SKILL_SET_SETUP_CREATE_LABEL:any;
     SKILL_SET_SETUP_UPDATE_LABEL:any;
     SKILL_SET_SETUP_ACTION:any;
     SKILL_SET_SETUP_CANCEL_LABEL:any;
     SKILL_SET_SETUP_SEARCH_LABEL:any;
     SKILL_SET_SETUP_CREATE_FORM_LABEL:any;
     SKILL_SET_SETUP_UPDATE_FORM_LABEL:any;
     SKILL_SET_SETUP_TABLEVIEW:any;

      //for akillset create
    isskillsetSuccessMsg: boolean;
    isskillsetfailureMsg: boolean;
    hasskillsetMessage = false;
    showskillsetMsg = false;
    hasskillsetMsg=false;
    skillsetmessageText;
    ddl_skillSetIdOptions=[];
    skillSetDescId: any;
    skillsetsetupdesc:any[]=[];
    skillsetdesparr:any[]=[];
    skillSetsetId:any=[]; 
    SelectedSkill:any=[];
    skillId={};
	skillSetIndexId : any = [];
    skillSetId = {};
    skillsetupindexdesc;
    ddlCompanySetting = {};

    islifeTimeValid: boolean = true;
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selectCompany = "";
    selectedPositionClass;
    selectedSkillset;
    selectedreportToPostion;

    //Upload file
    POSITION_ATTACHMENT_SEARCH:any;
    POSITION_ID:any;
    POSITION_DESCRIPTION:any;
    POSITION_ATTACHMENT_DESCRIPTION:any;
    POSITION_ATTACHMENT_TYPE:any;
    POSITION_ATTACHMENT_DATE:any;
    POSITION_ATTACHMENT_TIME:any;
    POSITION_ATTACHMENT_USER_ID:any;
    POSITION_ATTACHMENT_ACTION:any;
    POSITION_ATTACHMENT_CREATE_LABEL:any;
    POSITION_ATTACHMENT_CREATE_FORM_LABEL:any;
    POSITION_ATTACHMENT_UPDATE_FORM_LABEL:any;
    POSITION_TRAINING_SAVE_LABEL:any;
    POSITION_TRAINING_CLEAR_LABEL:any;
    POSITION_TRAINING_CANCEL_LABEL:any;
    POSITION_TRAINING_UPDATE_LABEL:any;
    POSITION_TRAINING_DELETE_LABEL:any;
    POSITION_TRAINING_CREATE_FORM_LABEL:any;
    POSITION_TRAINING_UPDATE_FORM_LABEL:any;
    POSITION_TRAINING_PREVIEW_FORM_LABEL:any;
    POSITION_TRAINING_ATTACH_FORM_LABEL:any;
  



    @ViewChild('file') file;

    attpage = new Page();
    tempatt = new Array<PositionAttachmentModule>();
    selectedatt = [];
   
    attscreenCode = 'S-1319';
    attmoduleCode = 'M-1011';
   attmoduleName;
    attscreenName;
    // filemessage = '';
    attdefaultFormValues: Array<any> = [];
    attavailableFormValues:any= [];
    atthasMessage;
    attduplicateWarning;
    attmessage = { 'type': '', 'text': '' };
    positionPlanId = {};
    positionIds = '';
    positionDesc = '';
    documentAttachmentName = '';
    startDate;
    endDate;
    attsearchKeyword = '';
    attddPageSize: number = 5;
    // attmodel: PositionAttachmentModule;
    attmodel: any={};
    attshowCreateForm: boolean = false;
    attisSuccessMsg: boolean;
    attisfailureMsg: boolean;
    atthasMsg = false;
    attshowMsg = false;
    attmessageText: string;
    attisConfirmationModalOpen: boolean = false;
    attcurrentLanguage: any;
    attconfirmationModalTitle = Constants.confirmationModalTitle;
    attconfirmationModalBody = Constants.confirmationModalBody;
    attdeleteConfirmationText = Constants.deleteConfirmationText;
    attOkText = Constants.OkText;
    attCancelText = Constants.CancelText;
    attisDeleteAction: boolean = false;
    private sub: any;
    positionPid: number;
    positionData: any;
    isAttachedFile: boolean;
    isattachFiles: boolean = false;
    url = '';
    fileName: any;
    isSelected: boolean = false;
    fileSelect: File;
    FileNotFetched: boolean = false;
    attachmentrows: any = [];
    isFileAttachModalOpen:boolean=false;
  
  

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private positionSetupService: PositionSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private uploadFileService: UploadFileService,
        private commonService: CommonService,
        private positionAttchmentService: PositionAttchmentService,
        private positionClassService: PositionClassService,
        private SkillSetSetupService: SkillSetSetupService,
        private _DomSanitizationService: DomSanitizer
    ) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';

        //File_upload
        this.attpage.pageNumber = 0;
        this.attpage.size = 5;
        this.attpage.sortOn = 'id';
        this.attpage.sortBy = 'DESC';

        this.router.events.filter(e => e instanceof RoutesRecognized).pairwise().subscribe((e: any[]) => {
            console.log("E: ", e);

            if (e[0].urlAfterRedirects == "/hcm/positionAttachment" && e[1].urlAfterRedirects == "/hcm/positionSetup")
                this.uploadFileService.modelClear = false;
            else if (e[1].urlAfterRedirects == "/hcm/positionSetup")
                this.uploadFileService.modelClear = true;
        });
        
        // default form parameter for position  screen
        this.defaultFormValues = [
            { 'fieldName': 'POSITION_SETUP_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_REPORT_TO_POSITION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_CLASS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_LONG_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_DEFAULT_SKILL_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_TRAINING_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_PLAN_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_SETUP_REPORTS_TO_POSITION', 'fieldValue': '', 'helpMessage': '' },
        ];

            this.POSITION_SETUP_SEARCH=this.defaultFormValues[0];
            this.POSITION_SETUP_ID=this.defaultFormValues[1];
            this. POSITION_SETUP_DESCRIPTION=this.defaultFormValues[2];
            this.POSITION_SETUP_ARABIC_DESCRIPTION=this.defaultFormValues[3];
            this.POSITION_SETUP_REPORT_TO_POSITION=this.defaultFormValues[4];
            this.POSITION_CLASS=this.defaultFormValues[5];
            this.POSITION_SETUP_LONG_DESCRIPTION=this.defaultFormValues[6];
            this.POSITION_SETUP_DEFAULT_SKILL_ID=this.defaultFormValues[7];
            this.POSITION_SETUP_ACTION=this.defaultFormValues[8];
            this.POSITION_SETUP_CREATE_LABEL=this.defaultFormValues[9];
            this.POSITION_SETUP_SAVE_LABEL=this.defaultFormValues[10];
            this.POSITION_SETUP_CLEAR_LABEL=this.defaultFormValues[11];
            this.POSITION_SETUP_CANCEL_LABEL=this.defaultFormValues[12];
            this.POSITION_SETUP_UPDATE_LABEL=this.defaultFormValues[13];
            this.POSITION_SETUP_DELETE_LABEL=this.defaultFormValues[14];
            this.POSITION_SETUP_CREATE_FORM_LABEL=this.defaultFormValues[15];
            this.POSITION_SETUP_UPDATE_FORM_LABEL=this.defaultFormValues[16];
            this.POSITION_SETUP_TRAINING_FORM_LABEL=this.defaultFormValues[17];
            this.POSITION_SETUP_PLAN_FORM_LABEL=this.defaultFormValues[18];
            this.MANAGE_USER_TABLE_VIEW=this.defaultFormValues[19];
            this.MANAGE_USER_TABLE_ACCESS=this.defaultFormValues[20];
            this.MANAGE_USER_TABLE_DELETE=this.defaultFormValues[21];
            this.MANAGE_USER_TEXT_ADD_NEW_USER=this.defaultFormValues[22];
            this.MANAGE_USER_TABLE_STATE=this.defaultFormValues[23];
            this.MANAGE_USER_TABLE_CITY=this.defaultFormValues[24];
            this.MANAGE_USER_TABLE_POSTALCODE=this.defaultFormValues[25];
            this.MANAGE_USER_TABLE_DOB=this.defaultFormValues[26];
            this.POSITION_SETUP_REPORTS_TO_POSITION=this.defaultFormValues[27];

             //   Position class control labels

          this.defaultFormValuesPositionclass = [
            { 'fieldName': 'POSITION_CLASS_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'POSITION_CLASS_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];
        this.POSITION_CLASS_DESCRIPTION = this.defaultFormValuesPositionclass[0];
        this.POSITION_CLASS_ID = this.defaultFormValuesPositionclass[1];
      
        this.POSITION_CLASS_ARABIC_DESCRIPTION = this.defaultFormValuesPositionclass[3];
      
        this.POSITION_CLASS_CREATE_LABEL = this.defaultFormValuesPositionclass[5];
        this.POSITION_CLASS_SAVE_LABEL = this.defaultFormValuesPositionclass[6];
        this.POSITION_CLASS_CLEAR_LABEL = this.defaultFormValuesPositionclass[7];
        this.POSITION_CLASS_CREATE_FORM_LABEL = this.defaultFormValuesPositionclass[11];
        this.POSITION_CLASS_CANCEL_LABEL=this.defaultFormValuesPositionclass[8];

        // Skillset control labels

          
          this.defaultFormValuesSkillset = [
            { 'fieldName': 'SKILL_SET_SETUP_SET_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SKILL_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SKILL_REQUIRED', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_COMMENT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SEQUENCE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SEARCH_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];

        this.SKILL_SET_SETUP_SET_ID = this.defaultFormValuesSkillset[0];
        this.SKILL_SET_SETUP_DESCRIPTION = this.defaultFormValuesSkillset[1];
        this.SKILL_SET_SETUP_ARABIC_DESCRIPTION = this.defaultFormValuesSkillset[2];
        this.SKILL_SET_SETUP_SKILL_ID = this.defaultFormValuesSkillset[3];
        this.SKILL_SET_SETUP_SKILL_REQUIRED = this.defaultFormValuesSkillset[4];
        this.SKILL_SET_SETUP_COMMENT = this.defaultFormValuesSkillset[5];
        this.SKILL_SET_SETUP_SEQUENCE = this.defaultFormValuesSkillset[6];
        this.SKILL_SET_SETUP_DELETE_LABEL = this.defaultFormValuesSkillset[7];
        this.SKILL_SET_SETUP_SAVE_LABEL = this.defaultFormValuesSkillset[8];
        this.SKILL_SET_SETUP_CLEAR_LABEL = this.defaultFormValuesSkillset[9];
        this.SKILL_SET_SETUP_CREATE_LABEL = this.defaultFormValuesSkillset[10];
        this.SKILL_SET_SETUP_UPDATE_LABEL = this.defaultFormValuesSkillset[11];
        this.SKILL_SET_SETUP_ACTION = this.defaultFormValuesSkillset[12];
        this.SKILL_SET_SETUP_CANCEL_LABEL = this.defaultFormValuesSkillset[13];
        this.SKILL_SET_SETUP_SEARCH_LABEL = this.defaultFormValuesSkillset[14];
        this.SKILL_SET_SETUP_CREATE_FORM_LABEL = this.defaultFormValuesSkillset[15];
        this.SKILL_SET_SETUP_UPDATE_FORM_LABEL = this.defaultFormValuesSkillset[16];
        this.SKILL_SET_SETUP_TABLEVIEW = this.defaultFormValuesSkillset[17];


        //File_Upload_screen
         // default form parameter for position  screen
         this.attdefaultFormValues = [
            { 'fieldName': 'POSITION_ATTACHMENT_SEARCH', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_TYPE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_DATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_TIME', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_USER_ID', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_ACTION', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_ATTACHMENT_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_PREVIEW_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'POSITION_TRAINING_ATTACH_FORM_LABEL', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '' },
            { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '' }
        ];


        this.POSITION_ATTACHMENT_SEARCH= this.attdefaultFormValues[0];
        this.POSITION_ID= this.attdefaultFormValues[1];
        this.POSITION_DESCRIPTION =this.attdefaultFormValues[2];
        this.POSITION_ATTACHMENT_DESCRIPTION =this.attdefaultFormValues[3];
        this.POSITION_ATTACHMENT_TYPE =this.attdefaultFormValues[4];
        this.POSITION_ATTACHMENT_DATE =this.attdefaultFormValues[5];
        this.POSITION_ATTACHMENT_TIME =this.attdefaultFormValues[6];
        this.POSITION_ATTACHMENT_USER_ID =this.attdefaultFormValues[7];
        this.POSITION_ATTACHMENT_ACTION =this.attdefaultFormValues[8];
        this.POSITION_ATTACHMENT_CREATE_LABEL =this.attdefaultFormValues[9];
        this.POSITION_ATTACHMENT_CREATE_FORM_LABEL =this.attdefaultFormValues[10];
        this.POSITION_ATTACHMENT_UPDATE_FORM_LABEL =this.attdefaultFormValues[11];
        this.POSITION_TRAINING_SAVE_LABEL =this.attdefaultFormValues[12];
        this.POSITION_TRAINING_CLEAR_LABEL =this.attdefaultFormValues[13];
        this.POSITION_TRAINING_CANCEL_LABEL =this.attdefaultFormValues[14];
        this.POSITION_TRAINING_UPDATE_LABEL =this.attdefaultFormValues[15];
        this.POSITION_TRAINING_DELETE_LABEL =this.attdefaultFormValues[16];
        this.POSITION_TRAINING_CREATE_FORM_LABEL =this.attdefaultFormValues[17];
        this.POSITION_TRAINING_UPDATE_FORM_LABEL =this.attdefaultFormValues[18];
        this.POSITION_TRAINING_PREVIEW_FORM_LABEL =this.attdefaultFormValues[19];
        this.POSITION_TRAINING_ATTACH_FORM_LABEL =this.attdefaultFormValues[20];

        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.reportToPostion);
        }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));

        this.positionIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.positionId);
        }).mergeMap((token: string) => this.getReportPositionIdAsObservable(token));
    }

    ngOnInit() {


   
    this.ddlCompanySetting = { 
        singleSelection: false, 
        text:this.selectCompany,
        selectAllText: this.selectAll,
        unSelectAllText: this.unselectAll,
        enableSearchFilter: true,
        classes:"myclass custom-class"
      }; 

        console.log("PD After Attached:", this.uploadFileService.positionAttchModelData);
        console.log("PD:", this.uploadFileService);
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        if (this.uploadFileService.positionModelData && !this.uploadFileService.modelClear) {
            this.showCreateForm = true;
            this.isUnderUpdate = true;
            this.positionIdvalue = this.uploadFileService.positionModelData.positionId;
            this.model = this.uploadFileService.positionModelData;
        } else {
            this.showCreateForm = false
            this.isUnderUpdate = false;
        }
        this.currentLanguage = localStorage.getItem('currentLanguage');

        // getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        //Get screen labels for Add position class Pop-Up
      this.commonService.getScreenDetails(this.moduleCode, this.positionClassscreenCode, this.defaultFormValuesPositionclass);

       //Get screen labels for Add skill set Pop-Up
        this.commonService.getScreenDetails(this.moduleCode, this.skillsetscreenCode, this.defaultFormValuesSkillset);

        //Following shifted from SetPage for better performance
        debugger
        
        this.positionSetupService.getSkillList().then(data => {
         
            var skillSOptions = data.result.records;
             console.log('test',skillSOptions );
            
            for(var i=0;i<skillSOptions.length;i++)
            {
             
                this.ddl_skillSetIdOptions.push({ "id":skillSOptions[i].id, "itemName": skillSOptions[i].skillId,"itemDescription":skillSOptions[i].skillDesc })
            }
          
        });
        
        this.positionSetupService.getPositionIdList().then(data => {
            this.getPositionID = data.result.records;
        });
       
       //bind position class and skill set dropdown
       this.getDropDownvalues();
      

        this.route.params.subscribe((params: Params) => {
            this.positionId = params['positionId']; 
    
            if (this.positionId != '' && this.positionId != undefined) {
    
                this.getpositionById(this.positionId);
                this.isUnderUpdate=true;
               this.setAutocompleteLabels();
               
                
            }
    
        });

         // getting screen labels, help messages and validation messages of Upload file pop-up
      this.getScreenDetailService.getScreenDetail(this.attmoduleCode, this.attscreenCode).then(data => {
       
              this.attmoduleName = data.result.moduleName;
              this.attscreenName = data.result.dtoScreenDetail.screenName;
              this.attavailableFormValues = data.result.dtoScreenDetail.fieldList;
              for (let j = 0; j < this.attavailableFormValues.length; j++) {
                  let fieldKey = this.attavailableFormValues[j]['fieldName'];
                  let objAvailable = this.attavailableFormValues.find(x => x['fieldName'] === fieldKey);
                  let objDefault = this.attdefaultFormValues.find(x => x['fieldName'] === fieldKey);
                  objDefault['fieldValue'] = objAvailable['fieldValue'];
                  objDefault['helpMessage'] = objAvailable['helpMessage'];
                  if (objAvailable['listDtoFieldValidationMessage']) {
                      objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                  }
              }
          });
    
        // ///File_Upload
        //   //console.log("PD:", this.uploadFileService);
        //   this.positionData = this.uploadFileService.positionModelData;
        //   if (!this.positionData) {
        //     //   this.router.navigate(['hcm/positionSetup']);
        //     //   return;
        //   }
          //console.log("PositionData:", this.positionData);
        //   alert(this.positionId);
          if (this.positionId != undefined) {
              this.Create();
          }
    

    }


    setAutocompleteLabels(){
        this.onAutocompleteFocus('lblPositionId');
        this.onAutocompleteFocus('lblPositionId');
        this.onAutocompleteFocus('lblPositionClassId');
        this.onAutocompleteFocus('lblPositionClassId');
        this.onAutocompleteFocus('lblSkillSetId');
        this.onAutocompleteFocus('lblreporttoPositionId');
        this.onAutocompleteFocus('lblreporttoPositionId');
    }

    getReportPositionsAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getPositionID.filter((id: any) => {
                return query.test(id.positionId);
            })
        );
    }
    getReportPositionIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getPositionID.filter((id: any) => {
                return query.test(id.positionId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
    }

    typeaheadOnSelect(e: TypeaheadMatch): void {
        console.log('Selected value: ', e);
        this.positionSetupService.getPositionSetup(e.item.id).then(pagedData => {
            console.log('PagedData', pagedData)
            this.model = pagedData.result;
            
            this.model.id = 0;
            console.log('this.model', this.model)
        });
    }
    typeaheadOnSelectReportToPosition(e: TypeaheadMatch): void {
        console.log('Selected value: ', e);
        this.positionSetupService.getPositionSetup(e.item.id).then(pagedData => {
            console.log('PagedData', pagedData)
            this.model = pagedData.result;

            //this.model.id = 0;
            console.log('this.model', this.model)
        });
    }
    // setting pagination
    setPage(pageInfo) {
        //
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }
        this.page.searchKeyword = '';
        this.positionSetupService.getAllPostionDropDownList().then(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.result;
        });
    }

    // Open form for create position setup
    Create() {
        this.isAttached = false;
        // this.showCreateForm = false;
        // this.isUnderUpdate = false;
        this.isReporttopositionValid = true;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.attmodel = {
            id: 0,
            positionId: '',
            description: '',
            arabicDescription: '',
            reportToPostion: '',
            positionClassId: null,
            skillSetId: null,
            postionLongDesc: '',
            positionClassPrimaryId: null,
            skillsetPrimaryId: null,
        };
    }

    // edit position class by row
    // edit(row: PositionSetupModule) {
    //     this.isReporttopositionValid = true;
    //     this.isAttached = false;
    //     this.showCreateForm = true;
    //     this.model = Object.assign({}, row);
    //     this.uploadFileService.positionData(this.model);
    //     this.positionId = row.positionId;
    //     this.isUnderUpdate = true;
    //     this.positionIdvalue = this.model.positionId;
    //     this.model.positionClassId = row.positionClassPrimaryId;
    //     this.model.skillSetId = row.skillsetPrimaryId;
    //     this.model.postionLongDesc = row.postionLongDesc;
    //     setTimeout(() => {
    //         window.scrollTo(0, 2000);
    //     }, 10);
    // }


    // download(row: PositionAttachmentModule) {
    //     this.positionAttchmentService.download(row.id, row["documentAttachmentName"]);
     
    // }


    download(event) {

        if(event.cellIndex!=0 && event.cellIndex!=1){
            this.positionAttchmentService.download(event.row.id, event.row["documentAttachmentName"]);
        }
     
     
    }

    edit(event) {
   
   
        if(event.cellIndex!=0 && event.cellIndex!=1){
            this.positionId = event.row.id;
          
            var myurl = `${'createpositionSetup'}/${this.positionId}`;
            this.router.navigateByUrl(myurl);
        }
    
       
    }

    updateStatus() {
        this.closeModal();

debugger;
        // Call service api for updating selected position class
        //this.model.positionId = this.uploadFileService.positionModelData.positionId;
        this.model.positionClassPrimaryId=this.selectedPositionClass.id;
        this.model.skillsetPrimaryId=this.selectedSkillset.id;
        if(this.selectedreportToPostion!=undefined){
            this.model.reportToPostion=this.selectedreportToPostion.id;
        }


        this.positionSetupService.createPositionSetup(this.model).then(data => {
            let datacode = data.code;

            if (this.uploadFileService.positionAttchModelData !== undefined)
                if (this.uploadFileService.positionAttchModelData.isAttachedFile === true) {
                    this.uploadFileService.positionAttchModelData.positionAttach.positionIds = data.result.id
                    let uploadAttachModel = this.uploadFileService.positionAttchModelData.positionAttach;

                   
                    let updateUploadAttachModel:any={};
                    updateUploadAttachModel.attachmentType= uploadAttachModel.attachmentType;
                    updateUploadAttachModel.documentAttachmenDesc= uploadAttachModel.documentAttachmenDesc;
                    updateUploadAttachModel.documentAttachmentName= uploadAttachModel.documentAttachmentName;
                    updateUploadAttachModel.positionIds= uploadAttachModel.positionIds;
                    // : "asdf"
                    // : "C:\fakepath\cc7600202b332c98c360c3012861ef4c.jpg"
                    // : 1369

                    let uploadAttachfile = this.uploadFileService.positionAttchModelData.attachmentFile;
                    this.positionAttchmentService.createPositionAttachment(updateUploadAttachModel, uploadAttachfile);
                    this.uploadFileService.positionAttchModelData.isAttachedFile = false;
                    
                  
                }

                if (datacode == 201) {
                   
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                
                this.showMsg = true;
                this.messageText = data.btiMessage.message;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                    this.attsetPage({ attoffset: 0, attsortOn: this.attpage.sortOn, attsortBy: this.attpage.sortBy });
                    this.attmodel.attachmentType="";
                    this.attmodel.documentAttachmenDesc="";
                    this.fileName="";
                    
                    this.ngOnInit();
                   

                    // this.goBack();
                      
                    
                }, 4000);
                
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.hasMessage = false;

            }, 4000);
        }
    }

   

    navToTraining() {
        console.log("function is called");
        if (this.selected.length === 1) {

            this.router.navigate(['hcm/positionTraining', this.selected[0].positionId,
                this.selected[0].description, this.selected[0].arabicDescription, this.selected[0].id]);
        } else if (this.selected.length > 1) {
            //console.log(this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = "Please select only one record.";
            }, 100);
        } else {
            //console.log("down:-->"+this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = false;
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                }, 4000);
                window.setTimeout(() => {
                    this.messageText = "Please select at least one record.";
                }, 100);

            }, 100);

        }

    }

    navToPlanSetup() {

        if (this.selected.length === 1) {

            this.router.navigate(['hcm/positionPlanSetup', this.selected[0].positionId,
                this.selected[0].id]);
        } else if (this.selected.length > 1) {
            //console.log(this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = "Please select only one record";
            }, 100);
        } else {
            //console.log("down:-->"+this.selected.length);
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = true;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = "Please select at least one record.";
            }, 100);
        }
    }

    navToAttachment(f: NgForm) {
        console.log("PD:", this.uploadFileService);
        this.isAttached = true;
        this.uploadFileService.positionModelData = this.model;
        if (this.model.id === 0 && this.model.positionId !== null) {
            // Check for duplicate positionClass Id according to it create new position class
            this.positionSetupService.checkDuplicatePositionSetupId(this.model.positionId).then(response => {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                    }, 100);
                } else if (this.model.positionId !== "" && this.model.description !== "") {
                    this.router.navigate(['hcm/positionAttachment']);
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });
        } else if (this.model.positionId !== ("" || null) && this.model.description !== ("" || null)) {
            this.router.navigate(['hcm/positionAttachment']);
        } else {
            null;
        }
    }

    attachMsg() {
        window.scrollTo(0, 0);
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        window.setTimeout(() => {
            this.showMsg = true;
            this.hasMsg = true;
        }, 100);
        this.messageText = "Please save the Position Setup first.";
    }

    // delete position class by passing whole object of perticular position Setup
    delete() {
        let selectedPositionSetup = [];
        for (let i = 0; i < this.selected.length; i++) {
            selectedPositionSetup.push(this.selected[i].id);
        }
        this.positionSetupService.deletePositionSetup(selectedPositionSetup).then(data => {
            let datacode = data.code;
            if (datacode === 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            console.log("Data",datacode);
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            // this.message.text = data.btiMessage.message;

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);

                this.message.text = data.btiMessage.message;
            }, 100);

            // Refresh the Grid data after deletion of position Setup
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            let errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search position class by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }

    positionFocusIn(positionId){
        this.isPositionOpen=true;
      }

    positionFocusOut(selectedModel){
    
        this.isPositionOpen=false;
 
      }
    
      
    
    positionClassFocusIn(){
        this.isPositionClassOpen=true;

      }
      positionClassFocusOut(){
    
        this.isPositionClassOpen=false;

      }

      skillFocusIn(skillSetId){
        this.isSkillOpen=true;
      }
      skillFocusOut(selectedModel){
        
        this.isSkillOpen=false;
        // this.isPositionClassOpen=false;
        // if(typeof selectedModel.model === "string"){
        //     this.selectedPositionClass="";
            
        // }
      
      }

      reportFocusIn(){
        this.isReportOpen=true;
      }
      reportFocusOut(){
        this.isReportOpen=false;
      }
    
    toggle() {
     
        // this.isListHide = !this.isListHide;
        this.isPositionOpen=!this.isPositionOpen;
       
        
          this.filterInput.nativeElement.focus();
         
      }
      toggle1() {
     
      
        this.isPositionClassOpen=!this.isPositionClassOpen;
        // this.selectedDepartmentSetup ='';
        
          this.filterInput1.nativeElement.focus();
         
      }
      toggle2() {
     
      
        this.isSkillOpen=!this.isSkillOpen;
        // this.selectedDepartmentSetup ='';
        
          this.filterInput2.nativeElement.focus();
         
      }
      toggle3() {
     
      
        this.isReportOpen=!this.isReportOpen;
        // this.selectedDepartmentSetup ='';
        
          this.filterInput3.nativeElement.focus();
         
      }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }


    // Clear form to reset to default blank
    // Clear(f: NgForm) {
        // f.resetForm({ positionClassId: null, skillSetId: null });
        // this.isAttached = false;
        // this.isReporttopositionValid = true;
    // }
    Clear(f: NgForm){
          
        // this.po=null;
        f.resetForm({ positionClassId: null, skillSetId: null });
        this.isAttached = false;
        this.isReporttopositionValid = true;
        this.selectedSkillset="";
        this.model="";
        this.selectedreportToPostion="";
        this.selectedSkillset="";
        this.selectedPositionClass="";
        // this.positionId=[];

        this.onAutocompleteFocusOut('lblPositionId');
        this.onAutocompleteFocusOut('lblPositionId');
        this.onAutocompleteFocusOut('lblPositionClassId');
        this.onAutocompleteFocusOut('lblPositionClassId');
        this.onAutocompleteFocusOut('lblSkillSetId');
        this.onAutocompleteFocusOut('lblreporttoPositionId');
        this.onAutocompleteFocusOut('lblreporttoPositionId');
    }


    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }

    // function call for creating new positionsetup
    CreatePositionSetup(f: NgForm, event: Event) {
       
        this.isAttached = true;

        if (!f.form.valid) {
            return;
        }
        event.preventDefault();

        let posIdx = this.model.positionId;
        this.model.positionClassPrimaryId=this.selectedPositionClass.id;
        this.model.skillsetPrimaryId=this.selectedSkillset.id;
        if(this.selectedreportToPostion!=undefined){
            this.model.reportToPostion=this.selectedreportToPostion.positionId;
        }

        // Check if the id is available in the model.
        // If id avalable then update the position class, else Add new position class.
        if (this.model.id > 0 && this.model.id !== 0 && this.model.id !== undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        } else {
            // Check for duplicate positionClass Id according to it create new position class
            this.positionSetupService.checkDuplicatePositionSetupId(posIdx).then(response => {
                if (response && response.code === 302 && response.result && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message;
                       
                    }, 100);
                } else {
                    // Call service api for Creating new position class
                    this.positionSetupService.createPositionSetup(this.model).then(data => {
                        let datacode = data.code;
                        if (this.uploadFileService.positionAttchModelData !== undefined)
                            if (this.uploadFileService.positionAttchModelData.isAttachedFile === true) {
                                this.uploadFileService.positionAttchModelData.positionAttach.positionIds = data.result.id
                                let uploadAttachModel = this.uploadFileService.positionAttchModelData.positionAttach;
                                let uploadAttachfile = this.uploadFileService.positionAttchModelData.attachmentFile;
                                this.positionAttchmentService.createPositionAttachment(uploadAttachModel, uploadAttachfile);
                                this.uploadFileService.positionAttchModelData.isAttachedFile = false;
                            }
                        // this.uploadFileService.positionAttchModelData.positionAttach.positionIds = data.result.id
                        // let uploadAttachModel = this.uploadFileService.positionAttchModelData.positionAttach;
                        // let uploadAttachfile = this.uploadFileService.positionAttchModelData.attachmentFile;

                        // this.positionAttchmentService.createPositionAttachment(uploadAttachModel, uploadAttachfile)
                        if (datacode === 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.hasMessage = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            this.goBack();
                            // Refresh the Grid data after adding new position class
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin.';
                        }, 100);
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin.';
                }, 100);
            });

        }
    }

    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    CheckValidID(eve: any) {
        console.log("test",eve.target.value);
        if (eve.target.value == "") {
            this.isReporttopositionValid = true;
        } else {
            for (let i = 0; i < this.getPositionID.length; i++) {
                //if (this.getItemList[i].itemNumber.includes(eve.target.value)) {
                if (this.getPositionID[i].positionId.toLowerCase().match(eve.target.value.toLowerCase()))
                {
                    this.isReporttopositionValid = true;
                    break;
                } else {
                    this.isReporttopositionValid = false;
                }
            }
        }
    }

    getpositionById(posId){
        this.positionSetupService.getPositionSetup(posId).then(pagedData => {
            this.setAutocompleteLabels();
            console.log('PagedData', pagedData)
            this.model = pagedData.result;
            this.selectedPositionClass={};

             setTimeout(() => {

            if(this.positionClassOptions!=undefined){
                var currentPosclass=this.positionClassOptions.filter(x=> x.id==this.model.positionClassPrimaryId);
                this.selectedPositionClass=currentPosclass[0];
            }
               
            if(this.skillSetIdOptions!=undefined){
            var currentSkillset=this.skillSetIdOptions.filter(x=> x.id==this.model.skillsetPrimaryId);
            this.selectedSkillset=currentSkillset[0];
            }

         

            if(this.rows!=undefined && this.rows.length!=0){
              
                var currentPostion=this.rows.filter(x=> x.id==this.model.reportToPostion);
                this.selectedreportToPostion=currentPostion[0];
                }
                // setTimeout(() => {
                //     window.scrollTo(0, 2000);
                // }, 10);
            }, 10);

          
            
            this.model.id = pagedData.result.id;

            

            this.attsetPage({ attoffset: 0, attsortOn: this.attpage.sortOn, attsortBy: this.attpage.sortBy });
            // //Get position attachment data
            // this.currentLanguage = localStorage.getItem('currentLanguage');
            // this.positionPid = this.model.id;
            // this.positionIds = this.positionData.positionId; // (+) converts string 'id' to a number
            // this.positionDesc = this.positionData.description;
            // this.attsetPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            // console.log('this.model', this.model)
        });
    }

    onAutocompleteFocus(labelId){

      let elem: HTMLElement = document.getElementById(labelId);
      elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
    }
    
    onAutocompleteFocusOut(labelId){
        let elem: HTMLElement = document.getElementById(labelId);
        elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
        
    }
  
  
  
  
  getPositionsData(event){
     
      var posId = event.id ? event.id : -1;
    
      this.model.positionId=posId;
  
      if(event.id>0 && event.id!=undefined)
      {
          this.getpositionById(event.id);
          this.isUnderUpdate=true;
      }
  
    
    
     //  this.divisionService.getDivision(divId).then(data => {
     //     this.model.countryCode = data.result.countryCode;
     // });
     //   this.companyService.getStateList(countryId).then(data => {
     //       this.stateOptions = data.result;
     //       this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
     //   });
  }
  
  
    goBack(){
      this.router.navigate(['hcm/positionSetup']); 
    }
  
    
 addClassPopup(){
   
   this.isAddClassModalOpen=true;
 }

 closeClassModal() {
  
    this.isAddClassModalOpen = false;
}


addSkillsetPopup(){
   
   this.isAddSkillsetModalOpen=true;
 }

 closeSkillsetPopup(){
     this.isAddSkillsetModalOpen=false;
 }


//Add new position class
// function call for creating new positionClass
CreatePositionClass(f1: NgForm, event: Event) {
   
    event.preventDefault();
    let posIdx = this.posclsmodel.positionClassId;

    // Check if the id is available in the posclsmodel.
    // If id avalable then update the position class, else Add new position class.
    if (this.posclsmodel.id > 0 && this.posclsmodel.id !== 0 && this.posclsmodel.id !== undefined) {
        this.isConfirmationModalOpen = true;
        this.isDeleteAction = false;
    } else {
        // Check for duplicate positionClass Id according to it create new position class
        this.positionClassService.checkDuplicatePositionClassId(posIdx).then(response => {
            if (response && response.code === 302 && response.result && response.result.isRepeat) {
                this.duplicateWarning = true;
                this.message.type = 'success';

                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = response.btiMessage.message;
                }, 100);
            } else {
                // Call service api for Creating new position class
                this.positionClassService.createPositionClass(this.posclsmodel).then(data => {
                    let datacode = data.code;
                    if (datacode === 201) {
                        window.scrollTo(0, 0);
                        window.setTimeout(() => {

                            this.isclsSuccessMsg=true;
                            this.isclsfailureMsg=false;
                            this.hasclsMessage = false;
                            this.showclsMsg = true;
                            window.setTimeout(() => {
                                this.showclsMsg = false;
                                this.hasclsMsg = false;
                                this.getDropDownvalues();
                            }, 4000);
                           
                            this.clsmessageText = data.btiMessage.message;
                            this.ngOnInit();
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                            this.isAddClassModalOpen=false;
                            this.selectedPositionClass="";
                        }, 100);

                        this.hasMsg = true;
                        this.closeClassModal();
                        f1.resetForm();
                        // Refresh the Grid data after adding new position class
                       
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = 'Server error. Please contact admin.';
                    }, 100);
                });
            }

        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100);
        });

    }
}


onItemSelect(item:any){
    console.log("selected_skills",item)

    this.skillsetdesparr=[];
    this.SelectedSkill.forEach(element => {
        this.skillsetdesparr.push(element.itemName+':'+'\t'+element.itemDescription);
    this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
    });

   
}
OnItemDeSelect(item:any){
   
    this.skillsetdesparr=[];
    
    if(this.SelectedSkill.length>0){
        this.SelectedSkill.forEach(element => {
            this.skillsetdesparr.push(element.itemName+':'+'\t'+element.itemDescription);
        this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
        });
    }
    else{
        this.skillsetdesparr.pop();
        this.skillsetupindexdesc=""
    }
   
    
}
onSelectAll(items: any){
   
    this.skillsetdesparr=[];
    this.SelectedSkill.forEach(element => {
        this.skillsetdesparr.push(element.itemName+':'+'\t'+element.itemDescription);
    this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
    });
}
onDeSelectAll(items: any){
   
    
    this.skillsetdesparr.pop();
    this.skillsetdesparr=[];
    this.skillsetupindexdesc=""
}


//get postion by id
getPositionData(e){
debugger;
    this.getpositionById(e.id);
    this.positionId=e.id;
    this.isUnderUpdate=true;
}

// getDepartmentSetupData(event){
   
//     var depId = event.id ? event.id : -1;
  
//     this.model.departmentId=depId;

//     if(event.id>0 && event.id!=undefined)
//     {
//         this.getdepartmentIdById(event.id);
//     }

// }
//add new skill set
 //function call for creating new location
 CreateSkillsSetup(f2: NgForm, event: Event) {
   
      event.preventDefault();

      this.skillSetmodel.listSkillSteup=[];
      this.skillSetmodel.skillsetId=this.skillSetmodel.skillsetId;

      this.skillSetmodel.arabicDiscription= "";
      this.skillSetmodel.id= 0;
      this.skillSetmodel.skillId= "";
      this.skillSetmodel.skillIdParam= "";
      this.skillSetmodel.skillSetDescId= 0;
      this.skillSetmodel.skillSetDiscription="";
      this.skillSetmodel.skillSetIndexId= "";
      this.skillSetmodel.skillsIds= "";
      this.skillSetmodel.skillSetId=this.skillSetmodel.skillsetId;

      this.SelectedSkill.forEach(element => {
          this.skillSetsetId.push(element.id);
      });

      var locIdx = this.skillSetmodel.skillsetId;

     

      //Check if the id is available in the model.
      //If id avalable then update the Deduction Code, else Add new Deduction Code.
      if (this.skillSetmodel.id > 0 && this.skillSetmodel.id != 0 && this.skillSetmodel.id != undefined) {
          this.isConfirmationModalOpen = true;
          this.isDeleteAction = false;
      }
      else {
          //Check for duplicate Deduction Code Id according to it create new Deduction Code
         
          this.skillSetsetId.forEach(element => {
              this.skillSetmodel.listSkillSteup.push({id:element});
          });
          this.SkillSetSetupService.checkDuplicateSkillsSetupId(locIdx).then(response => {
             
              
              if (response && response.code == 302 && response.result && response.result.isRepeat) {
                  this.duplicateWarning = true;
                  this.message.type = 'success';

                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      window.setTimeout(() => {
                          this.showMsg = false;
                          this.duplicateWarning = false;
                      }, 4000);
                      this.message.text = response.btiMessage.message;
                  }, 100);
              } else {
                  //Call service api for Creating new Deduction Code
                 

                 
                  
                  this.SkillSetSetupService.createSkillSteup(this.skillSetmodel).then(data => {
                     
                      var datacode = data.code;
                      if (datacode == 201) {
                   
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                    
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.hasMsg = true;
                    this.getDropDownvalues();
                    this.closeSkillsetPopup();
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                        // this.goBack();
                       
                       
                        
                    }, 4000);
                    
                }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true; 
                          this.showMsg = true;
                          this.messageText = 'Server error. Please contact admin.';
                      }, 100)
                  });
              }

          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = 'Server error. Please contact admin.';
              }, 100)
          });

      }
  }


//   File_Upload
 // setting pagination
 attsetPage(pageInfo) {
 
    this.selected = []; // remove any selected checkbox on paging
    this.attpage.pageNumber = pageInfo.attoffset;
    // this.pageAtt.pageNumber = pageInfo.offset;
    //console.log("Page details : ",this.page);
    if (pageInfo.attsortOn == undefined) {
        this.attpage.sortOn = this.attpage.sortOn;
    } else {
        this.attpage.sortOn = pageInfo.attsortOn;
    }
    if (pageInfo.attsortBy == undefined) {
        this.attpage.sortBy = this.attpage.sortBy;
    } else {
        this.attpage.sortBy = pageInfo.attsortBy;
    }
    //console.log("SearchWord:", this.searchKeyword);
    this.attpage.searchKeyword = this.attsearchKeyword;
    //console.log("Position page size",this.page.pageNumber);

   
    if (this.positionId !== 0) {
        this.positionAttchmentService.getFileAttachedPositionClass(this.attpage, this.positionId, this.attpage.searchKeyword).then(pagedData => {
         
            //console.log("Pagedatasssssssss:", pagedData);
            if (pagedData.code !== 404) {
                // this.pageAtt = pagedData.result;   
                             
                this.attpage = pagedData.result;
                this.attpage.size = pagedData.result.pageSize;
                this.attpage.totalElements = pagedData.result.totalCount;
               
                if(pagedData.result.totalCount!=0){
                    this.attachmentrows = pagedData.result.records;

                    this.attachmentrows=[...this.attachmentrows];
                   
                    console.log("attachmetn List...",pagedData)
                }else{
                    this.attachmentrows=[];
                }
               
            } else {
                this.rows = [];
                // this.pageAtt = this.pageAtt;
                this.page = this.page;

            }

        });
    }

}

// Open form for create position class
attCreate() {
    this.showCreateForm = false;
    setTimeout(() => {
        this.showCreateForm = true;
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }, 10);
    this.model = {
        id: 0,
        positionIds: '',
        documentAttachmenDesc: '',
        attachmentType: null
    };
}

// Clear form to reset to default blank
attClear(f: NgForm) {
    f.resetForm();
    this.fileName = null;
    this.fileSelect = undefined;
    this.isSelected = false;
    this.url = "";
}

Cancel(f: NgForm) {
    this.fileName = null;
    this.fileSelect = undefined;
    this.isSelected = false;
    this.url = "";
    this.showCreateForm = false;

}

// function call for creating new positionClass
CreatePositionAttachment(f: NgForm, event: Event) {
   
    event.preventDefault();

    //console.log("PositionAttachData:", this.uploadFileService);
    // this.model.positionIds = this.positionIds;

    //console.log('file:', this.file.nativeElement.files[0]);

    if (this.attmodel.attachmentType !== null && this.attmodel.documentAttachmenDesc !== "" && this.file.nativeElement.files[0] !== undefined) {
        this.isAttachedFile = true;
        this.uploadFileService.positionAttachData(this.attmodel, this.file.nativeElement.files[0], this.isAttachedFile);
        //console.log("Attached Data:",this.uploadFileService);
        //console.log('file:', this.file.nativeElement.files[0]);
        //console.log("Model:",this.model);
        this.fileName = this.file.nativeElement.files[0].name;
      
        this.isattachFiles = false;
        this.isFileAttachModalOpen=false;
        // this.router.navigate(['hcm/positionSetup']);
    } else {
        this.isattachFiles = true;
        this.isSuccessMsg = false;
        this.hasMessage = true;
        this.message.type = 'error';
        this.isfailureMsg = true;
        this.showMsg = true;
        this.message.text = 'Please upload the file.';
        window.scrollTo(0, 0);
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMessage = false;
        }, 5000);
    }
}





// edit position class by row
attedit(row: any) {
    //console.log("Row:", row);
    this.fileName = row.documentAttachmentName;
    this.FileNotFetched=true;
    this.showCreateForm = true;
    this.isSelected = false;        
    this.model = row;
    this.positionPlanId = row.positionIds;
    this.positionAttchmentService.getAttachmentById(row.id).subscribe(data => {
        this.url = data;
    });
    var x = this.positionAttchmentService.getFile(row.id,row.documentAttachmentName.split('.')[1]);
    x.subscribe((blob) => {
        this.fileSelect = new File([blob], row.documentAttachmentName, {type:blob.type});
        console.log("FILE FETCHED: ", this.fileSelect);
        if (this.fileSelect) {
            this.fileName = this.fileSelect.name;
            var reader = new FileReader();
            if (this.model.attachmentType != null) {
                if (Number(this.model.attachmentType) == 1 &&
                    (this.fileSelect.type == "application/pdf" ||
                        this.fileSelect.type == "application/doc" ||
                        this.fileSelect.type == "text/plain" ||
                        this.fileSelect.type == "" ||
                        this.fileSelect.type == "application/msword" ||
                        this.fileSelect.type == "application/xlsx" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.template" ||
                        this.fileSelect.type == "application/vnd.ms-word.document.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-word.template.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.template" ||
                        this.fileSelect.type == "application//vnd.ms-excel.sheet.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel.template.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel.addin.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-excel.sheet.binary.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.template" ||
                        this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.slideshow" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.addin.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.presentation.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.template.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-powerpoint.slideshow.macroEnabled.12" ||
                        this.fileSelect.type == "application/vnd.ms-access")
                )

                    this.isSelected = false;

                else if (Number(this.model.attachmentType) == 2 &&
                    (this.fileSelect.type == "image/png" ||
                        this.fileSelect.type == "image/jpg" ||
                        this.fileSelect.type == "image/jpeg")
                )
                    this.isSelected = false;
                else
                    this.isSelected = true;
            }
            this.FileNotFetched=false;
            //reader.readAsDataURL(this.fileSelect);
        }
    });
    //this.file.nativeElement.files[0]=row.attachment;
    //console.log("URL: ", this.file.nativeElement.url);
    setTimeout(() => {
        window.scrollTo(0, 2000);
    }, 10);
}

onSelectFile(event) {
    //console.log("aaaa:", event.target.files[0]);
    this.fileName = null;

    if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.fileSelect = event.target.files[0];
        var reader = new FileReader();
        if (this.model.attachmentType != null) {
            if (this.model.attachmentType == 1 &&
                (this.fileSelect.type == "application/pdf" ||
                    this.fileSelect.type == "application/doc" ||
                    this.fileSelect.type == "text/plain" ||
                    this.fileSelect.type == "" ||
                    this.fileSelect.type == "application/msword" ||
                    this.fileSelect.type == "application/xlsx" ||
                    this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
                    this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.template" ||
                    this.fileSelect.type == "application/vnd.ms-word.document.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-word.template.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-excel" ||
                    this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
                    this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.template" ||
                    this.fileSelect.type == "application//vnd.ms-excel.sheet.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-excel.template.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-excel.addin.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-excel.sheet.binary.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-powerpoint" ||
                    this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
                    this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.template" ||
                    this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.slideshow" ||
                    this.fileSelect.type == "application/vnd.ms-powerpoint.addin.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-powerpoint.presentation.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-powerpoint.template.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-powerpoint.slideshow.macroEnabled.12" ||
                    this.fileSelect.type == "application/vnd.ms-access")
            )

                this.isSelected = false;

            else if (this.model.attachmentType == 2 &&
                (this.fileSelect.type == "image/png" ||
                    this.fileSelect.type == "image/jpg" ||
                    this.fileSelect.type == "image/jpeg")
            )
                this.isSelected = false;
            else
                this.isSelected = true;
        }
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed 
            this.url = event.target.result;
        }
    }
}

checkAttachedFile(v) {
    if (this.fileSelect === undefined) {
        this.isSelected = false;
        return;
    }
    if (v == 1 &&
        (this.fileSelect.type == "application/pdf" ||
            this.fileSelect.type == "application/doc" ||
            this.fileSelect.type == "text/plain" ||
            this.fileSelect.type == "" ||
            this.fileSelect.type == "application/msword" ||
            this.fileSelect.type == "application/xlsx" ||
            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.template" ||
            this.fileSelect.type == "application/vnd.ms-word.document.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-word.template.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-excel" ||
            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.template" ||
            this.fileSelect.type == "application//vnd.ms-excel.sheet.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-excel.template.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-excel.addin.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-excel.sheet.binary.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-powerpoint" ||
            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.presentation" ||
            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.template" ||
            this.fileSelect.type == "application/vnd.openxmlformats-officedocument.presentationml.slideshow" ||
            this.fileSelect.type == "application/vnd.ms-powerpoint.addin.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-powerpoint.presentation.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-powerpoint.template.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-powerpoint.slideshow.macroEnabled.12" ||
            this.fileSelect.type == "application/vnd.ms-access")
    )

        this.isSelected = false;

    else if (v == 2 &&
        (this.fileSelect.type == "image/png" ||
            this.fileSelect.type == "image/jpg" ||
            this.fileSelect.type == "image/jpeg")
    )

        this.isSelected = false;
    else
        this.isSelected = true;
}

// else {
//     this.isSelected = true; 
//     if(this.model.attachmentType)
//         this.filemessage = "Allowed files : png,jpg and jpeg";
//     else
//         this.filemessage  = "";
// }


navigateToPositionSetup() {
    //console.log("Go to Position setup!!");
    this.isAttachedFile = false;
    this.uploadFileService.positionAttchModelData = this.model;
    /* this.router.events.forEach((event) => {
       //console.log(event);
    }); */
    this.router.navigate(['hcm/positionSetup']);
}

attupdateStatus(f: NgForm) {
    if (this.isSelected || f.form.invalid) {
        return;
    }

 

    this.closeModal();
    this.uploadFileService.positionAttchModelData = this.model;
    //console.log("Attached File:", this.uploadFileService.positionAttchModelData);
    // Call service api for updating selected position class
    this.positionAttchmentService.updatePositionAttachment(this.model, this.fileSelect).then(data => {
        let datacode = data.code;
        if (datacode === 201) {
          
            // Refresh the Grid data after editing position class
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            this.attmodel="";
            this.uploadFileService.positionAttachData(null,null,false);
            // Scroll to top after editing position class
            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.isSuccessMsg = true;
                this.isfailureMsg = false;
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
                this.messageText = data.btiMessage.message;

                this.showCreateForm = false;
            }, 100);

            this.hasMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
            }, 4000);
        }
    }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = 'Server error. Please contact admin.';
        }, 100);
    });
}

attvarifyDelete() {
    if (this.selected.length > 0) {
           
        this.isattConfirmationModalOpen = true;
    } else {
        this.isSuccessMsg = false;
        this.hasMessage = true;
        this.message.type = 'error';
        this.isfailureMsg = true;
        this.showMsg = true;
        this.message.text = 'Please select at least one record to delete.';
        window.scrollTo(0, 0);
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMessage = false;
        }, 5000);
    }
}
// delete position class by passing whole object of perticular position class
attdelete() {
   
    let selectedPositionAttachment = [];
    for (let i = 0; i < this.selected.length; i++) {
        selectedPositionAttachment.push(this.selected[i].id);
    }
    this.positionAttchmentService.deletePositionAttachment(selectedPositionAttachment).then(data => {
     
        let datacode = data.code;
        this.attsetPage({ attoffset: 0, attsortOn: this.attpage.sortOn, attsortBy: this.attpage.sortBy });
        // if (datacode === 200) {
        //     this.attsetPage({ attoffset: 0, attsortOn: this.attpage.sortOn, attsortBy: this.attpage.sortBy });
        // }

        this.attcloseModal();
     
        this.ngOnInit();
        this.hasMessage = true;
        this.message.type = 'success';
        // this.message.text = data.btiMessage.message;

        window.scrollTo(0, 0);
        window.setTimeout(() => {
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = data.btiMessage.message;
        }, 100);
      
        // Refresh the Grid data after deletion of position class
      

    }).catch(error => {
        this.hasMessage = true;
        this.message.type = 'error';
        let errorCode = error.status;
        this.message.text = 'Server issue. Please contact admin.';
    });
   
}

// default list on page
attonSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
}

// search position class by keyword
attupdateFilter(event) {

    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    //console.log("aaaaa:Search", this.searchKeyword);
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
}


// Set default page size
attchangePageSize(event) {
    //console.log('CHHHHHHHHH : ',event);
    this.page.size = event.target.value;
    this.setPage({ offset: 0 });
}

confirm(): void {
    this.messageText = 'Confirmed';
    // this.modalRef.hide();
    this.delete();
}

attcloseModal() {
    this.attisDeleteAction = false;
    this.isattConfirmationModalOpen = false;
    this.attmodel="";
}

CancelUpload(){
    this.isFileAttachModalOpen=false;
    this.attmodel.attachmentType="";
    this.attmodel.documentAttachmenDesc="";
    this.fileName="";
}


attsortColumn(val) {
    if (this.page.sortOn == val) {
        if (this.page.sortBy == 'DESC') {
            this.page.sortBy = 'ASC';
        } else {
            this.page.sortBy = 'DESC';
        }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

goToAttachment(){

    this.isFileAttachModalOpen=true;
    this.positionIds=this.model.positionId;
    this.positionDesc=this.model.description;     
}


EscapeModal(event){
     
    var key = event.key;
    if(key=="Escape"){
        this.closeClassModal();
        this.closeModal();
        this.closeClassModal();
        this.closeSkillsetPopup();
        this.CancelUpload();
        this.attcloseModal();
       
    }
  }

  getDropDownvalues(){
    this.positionSetupService.getSkillSetList().then(data => {
       
        data.result.records.unshift({id: -1,"skillSetId":"Add new skillset","skillSetDiscription":""})
        this.skillSetIdOptions = data.result.records;
        console.log("skill_options..",this.skillSetIdOptions)
     
      
    });

    this.positionSetupService.getPositionClassList().then(data => {
        debugger;
       
        data.result.records.unshift({id: -1,"positionClassId":"Add new Position class"})
        this.positionClassOptions = data.result.records;
        // this.positionClassOptions.push({"id":-1,"positionClassId":"Add new"})
        console.log("Data class options : ",data);
    });
  
  }

  changeSkillset(event){
  if(event.id==-1){
      this.isAddSkillsetModalOpen=true;
      this.selectedSkillset="";
  }
  }


  changePositionClass(event){
    if(event.id==-1){
        this.isAddClassModalOpen=true;
        this.selectedPositionClass="";
    }
    }

  

}
