import { Routes } from '@angular/router';
import { CreatePositionClassComponent } from '../create-position-class/create-position-class.component';
import { CreatePositionSetupComponent } from './create-position-setup.component';





export const CreatePositionSetupRoutes: Routes = [{
  path: '',
  component: CreatePositionSetupComponent,
  data: {
    breadcrumb: ""
  }
}];