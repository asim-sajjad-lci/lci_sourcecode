import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateVacationTypeSetupComponent } from './create-vacation-type-setup.component';

describe('CreateVacationTypeSetupComponent', () => {
  let component: CreateVacationTypeSetupComponent;
  let fixture: ComponentFixture<CreateVacationTypeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateVacationTypeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateVacationTypeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
