import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { Constants } from 'app/_sharedresource/Constants';
import { VacationTypeSetupService } from 'app/HcmModule/_services/vacation-type-setup/vacation-type-setup.service';
import { VacationTypeSetupModel } from 'app/HcmModule/_models/vacation-type-setup/vacation-type-setup';

@Component({
  selector: 'app-create-vacation-type-setup',
  templateUrl: './create-vacation-type-setup.component.html',
  styleUrls: ['./create-vacation-type-setup.component.css'],
  providers: [CommonService, VacationTypeSetupService]
})
export class CreateVacationTypeSetupComponent implements OnInit {

  model: VacationTypeSetupModel;

  isCodeIdValid = true;

  moduleCode = "M-1011";
  screenCode = "S-1793";
  moduleName;
  screenName;
  currentLanguage: any;
  defaultFormValues: object[];

  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  departmentId = {};
  searchKeyword = '';
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  isDeleteAction: boolean = false;

  selectedTicketRouteSetup;
  vacationTypeId;

  ticketRouteIdList: Observable<any>;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;

  getDepartment: any[] = [];
  rows = new Array<VacationTypeSetupModel>();

  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;


  VACATIONTYPE_CodeId: any;
  VACATIONTYPE_DESCRIPTION: any;
  VACATIONTYPE_SEARCH: any;
  VACATIONTYPE_ARABIC_DESCRIPTION: any;
  VACATIONTYPE_DAYS: any;
  VACATIONTYPE_CREATE_LABEL: any;
  VACATIONTYPE_SAVE_LABEL: any;
  VACATIONTYPE_CLEAR_LABEL: any;
  VACATIONTYPE_CANCEL_LABEL: any;
  VACATIONTYPE_UPDATE_LABEL: any;
  VACATIONTYPE_DELETE_LABEL: any;
  VACATIONTYPE_CREATE_FORM_LABEL: any;
  VACATIONTYPE_UPDATE_FORM_LABEL: any;
  VACATIONTYPE_VACATIONTYPE_LIST: any;
  VACATIONTYPE_TABLEVIEW: any;


  constructor(private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private service: VacationTypeSetupService ) {


      this.defaultFormValues = [
        { 'fieldName': 'VACATIONTYPE_CodeId', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_DAYS', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_VACATIONTYPE_LIST', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'VACATIONTYPE_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
        { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
        { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
    ];

    this.VACATIONTYPE_CodeId = this.defaultFormValues[0];
    this.VACATIONTYPE_DESCRIPTION = this.defaultFormValues[1];
    this.VACATIONTYPE_SEARCH = this.defaultFormValues[2];
    this.VACATIONTYPE_ARABIC_DESCRIPTION = this.defaultFormValues[3];
    this.VACATIONTYPE_DAYS = this.defaultFormValues[4];
    this.VACATIONTYPE_CREATE_LABEL = this.defaultFormValues[5];
    this.VACATIONTYPE_SAVE_LABEL = this.defaultFormValues[6];
    this.VACATIONTYPE_CLEAR_LABEL = this.defaultFormValues[7];
    this.VACATIONTYPE_CANCEL_LABEL = this.defaultFormValues[8];
    this.VACATIONTYPE_UPDATE_LABEL = this.defaultFormValues[9];
    this.VACATIONTYPE_DELETE_LABEL = this.defaultFormValues[10];
    this.VACATIONTYPE_CREATE_FORM_LABEL = this.defaultFormValues[11];
    this.VACATIONTYPE_UPDATE_FORM_LABEL = this.defaultFormValues[12];
    this.VACATIONTYPE_VACATIONTYPE_LIST = this.defaultFormValues[13];
    this.VACATIONTYPE_TABLEVIEW = this.defaultFormValues[14];
    

    this.ticketRouteIdList = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.model.codeId);
    }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));

  }

  ngOnInit() {

    this.model = {
      id: 0,
      codeId: '',
      descr: '',
      arabicDescr: '',
      days:''
    };

    this.route.params.subscribe((params: Params) => {
      debugger
      this.vacationTypeId = params['id'];

      if (this.vacationTypeId != '' && this.vacationTypeId != undefined) {

        this.getVacationTypeById(this.vacationTypeId);
        this.isUnderUpdate = true;
        this.onAutocompleteFocus('lblVacationTypeId');
      }

    });

    this.setPage();

    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


    //   this.model = {
    //     id: 0,
    //     codeId: '',
    //     descr: '',
    //     arabicDescr: '',
    // };
  }

  getReportPositionsAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getDepartment.filter((id: any) => {
        return query.test(id.departmentId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    // console.log('Selected value: ', e);
    this.service.getVacationTypeById(e.item.id).then(allData => {
      // console.log('allData', allData);
      debugger;
      this.model = allData.result;
      this.model.id = 0;
    });

  }


  goBack() {
    this.router.navigate(['hcm/vacationtypesetup']);
  }

  //function call for creating new TicketRoute
  CreateVacationType(f: NgForm, event: Event) {
    debugger;

    event.preventDefault();
    var routeIdx = this.model.codeId;

    //Check if the id is available in the model.
    //If id avalable then update the department, else Add new department.
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
      this.isConfirmationModalOpen = true;
      this.isDeleteAction = false;
    }
    else {
      //Check for duplicate Division Id according to it create new division
      this.service.checkDuplicateTypeId(routeIdx).then(response => {
        if (response && response.code == 302 && response.result && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = "success";

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;

          }, 100);
        } else {
          //Call service api for Creating new department
          this.service.createVacationType(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {

              this.isSuccessMsg = true;
              this.isfailureMsg = false;

              this.showMsg = true;
              this.messageText = data.btiMessage.message;
              this.hasMsg = true;
              window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
                this.goBack();


              }, 4000);

            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = "Server error. Please contact admin.";
            }, 100)
          });
        }

      }).catch(error => {
        console.log(error)
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = "Server error. Please contact admin.";
        }, 100)
      });

    }
  }


  getVacationTypeById(typeId) {
    debugger
    this.model.id = typeId;
    debugger;
    this.isUnderUpdate = true;
    this.onAutocompleteFocus('lblVacationTypeId');
    this.service.getVacationTypeById(typeId).then(allData => {

      console.log("selectedDepartmentSetup.........", allData)
      this.model.id = allData.result.id;
      this.selectedTicketRouteSetup = allData.result.id;

      this.model = allData.result;
      // this.model.id = 0;
      // console.log('this.model', this.model)
    });

  }


  getVacationSetupData(event) {
    debugger
    var Id = event.id ? event.id : -1;

    this.model.id = Id;

    if (event.id > 0 && event.id != undefined) {
      this.getVacationTypeById(event.id);
    }

  }

  onAutocompleteFocus(labelId) {

    let elem: HTMLElement = document.getElementById(labelId);
    elem.setAttribute("style", "top:-10px;font-size:14px;color:#5264AE;font-weight: bold;");
  }

  setPage() {
    debugger;
    // this.selected = []; // remove any selected checkbox on paging
    // this.page.pageNumber = pageInfo.offset;
    // if (pageInfo.sortOn == undefined) {
    //     this.page.sortOn = this.page.sortOn;
    // } else {
    //     this.page.sortOn = pageInfo.sortOn;
    // }
    // if (pageInfo.sortBy == undefined) {
    //     this.page.sortBy = this.page.sortBy;
    // } else {
    //     this.page.sortBy = pageInfo.sortBy;
    // }

    // this.page.searchKeyword = '';
    this.service.getVacationTypes().then(pagedData => {
      debugger;
      // this.page = pagedData.page;
      this.rows = pagedData.result;
    });
  }


  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }


  updateStatus() {
    this.closeModal();
    //Call service api for updating selected department
    debugger;
    this.model.id = this.selectedTicketRouteSetup;
    this.service.updateVacationType(this.model).then(data => {
      var datacode = data.code;
      if (datacode == 201) {

        this.isSuccessMsg = true;
        this.isfailureMsg = false;

        this.showMsg = true;
        this.messageText = data.btiMessage.message;
        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
          // this.goBack();


        }, 4000);

      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = "Server error. Please contact admin.";
      }, 100)
    });
  }

  Clear(f: NgForm) {
    f.resetForm();
    this.vacationTypeId=[];
      this.model = {
        id: 0,
        codeId: '',
        descr: '',
        arabicDescr: '',
        days:''
    };
}

EscapeModal(event){
      
  var key = event.key;
  if(key=="Escape"){
      this.closeModal();
  }
}

}
 