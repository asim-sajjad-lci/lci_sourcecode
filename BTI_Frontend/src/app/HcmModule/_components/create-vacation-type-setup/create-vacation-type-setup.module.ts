import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { CreateVacationTypeSetupRoutes } from './create-vacation-type-setup.routing';
import { CreateVacationTypeSetupComponent } from './create-vacation-type-setup.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateVacationTypeSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
  ],

  providers:[AlertService],
  declarations: [CreateVacationTypeSetupComponent]
})
export class CreateVacationTypeSetupModule { }