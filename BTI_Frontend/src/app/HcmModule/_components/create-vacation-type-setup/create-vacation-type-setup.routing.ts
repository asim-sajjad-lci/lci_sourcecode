import { Routes } from '@angular/router';
import { CreateVacationTypeSetupComponent } from './create-vacation-type-setup.component';


export const CreateVacationTypeSetupRoutes: Routes = [{
  path: '',
  component: CreateVacationTypeSetupComponent,
  data: {
    breadcrumb: ""
  }
}];