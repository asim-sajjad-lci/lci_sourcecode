import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePaycodeSetupComponent } from './create-paycode-setup.component';

describe('CreatePaycodeSetupComponent', () => {
  let component: CreatePaycodeSetupComponent;
  let fixture: ComponentFixture<CreatePaycodeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePaycodeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePaycodeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
