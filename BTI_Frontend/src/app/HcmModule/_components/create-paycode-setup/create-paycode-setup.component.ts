
// import { Component, OnInit } from '@angular/core';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Page } from '../../../_sharedresource/page';

import { PaycodeSetupModule } from '../../_models/paycode-setup/paycode-setup.module';
import { PaycodeSetupService } from '../../_services/paycode-setup/paycode-setup.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { DropDownModule } from 'app/_sharedresource/_modules/drop-down.module';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import {  Input, forwardRef, HostListener, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {  ViewChildren } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';


@Component({
  selector: 'app-create-paycode-setup',
  templateUrl: './create-paycode-setup.component.html',
  styleUrls: ['./create-paycode-setup.component.css'],
  providers: [PaycodeSetupService,CommonService]
})
export class CreatePaycodeSetupComponent  {

    
   @ViewChild('filterInput') filterInput: ElementRef;
   @ViewChild('filterInput1') filterInput1: ElementRef;
   @ViewChild('filterInput2') filterInput2: ElementRef;
   @ViewChild('filterInput3') filterInput3: ElementRef;
   @ViewChild('filterInput4') filterInput4: ElementRef;
   @ViewChild('filterInput5') filterInput5: ElementRef;
   @ViewChild('filterInput6') filterInput6: ElementRef;
   @ViewChild('filterInput7') filterInput7: ElementRef;


   @ViewChildren('listItems') listItems: QueryList<ElementRef>;
    page = new Page();
    rows = new Array<PaycodeSetupModule>();
    temp = new Array<PaycodeSetupModule>();
    tempp: any;
    decimalValue: boolean = true;
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1414';
    moduleName;
    screenName;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    payCodeId = {};
    searchKeyword = '';
    baseOnPayCodedesc = 0;
    payFactor: any;
    ddPageSize: number = 5;
    // model: PaycodeSetupModule;
    model:any={};
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean=false;
    hasMsg = false;
    showMsg = false;
    PayTypeDropdownList: any;
    baseOnPayCodeDropdownList: any;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    UpdateText=Constants.updateField;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    payCodeIdvalue: string;
    isPaycodeFormdisabled: boolean = false;
    unitofPayArray = ["Weekly", "Biweekly", "Semimonthly", "Monthly", "Quarterly", "Semianually", "Anually", "Daily/Miscellaneous"];
    pattern = /^\d*\.?\d{0,3}$/;
    selectedPaycode;

    SelectedpayCodeTypeId;
    selectedRoundOf;
    SelectedunitofPay;
    SelectedbaseOnPayCode;
    selectedPayType;
    Selectedpayperiod;
    selectedMainAccount;
    mainAccList;

isPayCodeOpen=false;
isPaycodeTypeOpen=false;
isbaseOnPayCodeOpen=false;
isunitofPayOpen=false;
isPayPeriodOpen=false;
isTypeOpen=false;
isRoundOpen=false;

    // LABEL VARIABLE
    PAYCODE_SEARCH: any;
    PAY_CODE: any;
    PAYCODE_DESCRIPTION: any;
    PAYCODE_ARABIC_DESCRIPTION: any;
    PAYCODE_PAY_TYPE: any;
    PAYCODE_PAY_PERIOD: any;
    PAYCODE_UNIT_OF_PAY: any;
    PAYCODE_BASE_ON_PAYCODE: any;
    PAYCODE_PAY_FACTOR: any;
    PAYCODE_PAY_RATE: any;
    PAYCODE_SAVE_LABEL: any;
    PAYCODE_CLEAR_LABEL: any;
    PAYCODE_CANCEL_LABEL: any;
    PAYCODE_UPDATE_LABEL: any;
    PAYCODE_DELETE_LABEL: any;
    PAYCODE_CREATE_FORM_LABEL: any;
    PAYCODE_UPDATE_FORM_LABEL: any;
    PAYCODE_INACTIVE: any;
    PAYCODE_ACTION: any;
    PAYCODE_CREATE_LABEL: any;
    PAYCODE_TABLEVIEW: any;
    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    payperiods: DropDownModule[] = [
        { id: 1, name: 'Weekly' },
        { id: 2, name: 'Biweekly' },
        { id: 3, name: 'Semimonthly' },
        { id: 4, name: 'Monthly' },
        { id: 5, name: 'Quarterly' },
        { id: 6, name: 'Semiannually' },
        { id: 7, name: 'Annually' },
        { id: 8, name: 'Daily/Miscellaneous' },
    ];
    unitofPays: DropDownModule[] = [
        { id: 1, name: 'Weekly' },
        { id: 2, name: 'Biweekly' },
        { id: 3, name: 'Semimonthly' },
        { id: 4, name: 'Monthly' },
        { id: 5, name: 'Quarterly' },
        { id: 6, name: 'Semiannually' },
        { id: 7, name: 'Annually' },
        { id: 8, name: 'Daily/Miscellaneous' },
    ];

    roundings:DropDownModule[] = [
        { id: 0, name: '0' },
        { id: 1, name: '0.0' },
        { id: 2, name: '0.00' },
        { id: 3, name: '0.000' },
        { id: 4, name: '0.0000' },
       
];

    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;
    @ViewChild('fieldName1')
    fieldName1: any;
    paycodeIdList: Observable<any>;
    getPayCode: any[] = [];
    getPayDetails: any[] = [];
    typeaheadLoading: boolean;
    dataSource: Observable<any>;
    setBasePayCode: boolean = false;
    loaderImg: boolean = true;
    typeId = 1;
    typeData= [];

    constructor(private router: Router,
        private route: ActivatedRoute,
        private paycodeSetupService: PaycodeSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
       
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'PAYCODE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAY_CODE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_PAY_TYPE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_PAY_PERIOD', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_UNIT_OF_PAY', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_BASE_ON_PAYCODE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_PAY_FACTOR', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_PAY_RATE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_INACTIVE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'PAYCODE_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },

        ];
        this.PAYCODE_SEARCH = this.defaultFormValues[0];
        this.PAY_CODE = this.defaultFormValues[1];
        this.PAYCODE_DESCRIPTION = this.defaultFormValues[2];
        this.PAYCODE_ARABIC_DESCRIPTION = this.defaultFormValues[3];
        this.PAYCODE_PAY_TYPE = this.defaultFormValues[4];
        this.PAYCODE_PAY_PERIOD = this.defaultFormValues[5];
        this.PAYCODE_UNIT_OF_PAY = this.defaultFormValues[6];
        this.PAYCODE_BASE_ON_PAYCODE = this.defaultFormValues[7];
        this.PAYCODE_PAY_FACTOR = this.defaultFormValues[8];
        this.PAYCODE_PAY_RATE = this.defaultFormValues[9];
        this.PAYCODE_SAVE_LABEL = this.defaultFormValues[10];
        this.PAYCODE_CLEAR_LABEL = this.defaultFormValues[11];
        this.PAYCODE_CANCEL_LABEL = this.defaultFormValues[12];
        this.PAYCODE_UPDATE_LABEL = this.defaultFormValues[13];
        this.PAYCODE_DELETE_LABEL = this.defaultFormValues[14];
        this.PAYCODE_CREATE_FORM_LABEL = this.defaultFormValues[15];
        this.PAYCODE_UPDATE_FORM_LABEL = this.defaultFormValues[16];
        this.PAYCODE_INACTIVE = this.defaultFormValues[17];
        this.PAYCODE_ACTION = this.defaultFormValues[18];
        this.PAYCODE_CREATE_LABEL = this.defaultFormValues[19];
        this.PAYCODE_TABLEVIEW = this.defaultFormValues[20];
        this.paycodeIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.payCodeId);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

        this.dataSource = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.baseOnPayCode);
        }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));


    }

    ngOnInit() {
        debugger;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        this.getTypeData();

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

        this.paycodeSetupService.getPayTypeDropdown().then(data => {
            debugger;
            this.loaderImg = false;
            this.PayTypeDropdownList = data.result.records;
            console.log('dropdown---', data.result.records);

        });

        this.paycodeSetupService.getBaseOnPayCodeDropdown().then(data => {
            this.baseOnPayCodeDropdownList = data.result.records;
            //console.log('dropdown',data.result.records);
        });

        //Following shifted from SetPage for better performance
        this.paycodeSetupService.getPaycodeDetails().then(data => {
            this.getPayDetails = data.result.records;
            this.getPayCode = data.result.records;
            //console.log("Data class options111111 : " + this.getPayDetails);
        });


        this.paycodeSetupService.getMainAccountList().then(data => {
            debugger;
            if(data.code == 201)
            {
                this.mainAccList = [];
                let val = data.result;
                for(let i=0;i<val.length;i++)
                {
                    let desc=val[i].accountCategoryIndex+"-"+val[i].mainAccDescription;
                    this.mainAccList.push({id:val[i].id,value:desc})
                }
            }
            else{
               this.mainAccList = [];
            }
            // this.mainAccList = data.result;
        });

        this.route.params.subscribe((params: Params) => {
            this.payCodeId = params['payCodeId']; 
    
            if (this.payCodeId != '' && this.payCodeId != undefined) {
               
                this.getPaycodeById(this.payCodeId);
              
                this.isUnderUpdate=true;
               this.setAutoCompleteLabel();

                
            }
    
        });

    }

    setAutoCompleteLabel(){
        this.onAutocompleteFocus('lblpayCodeId');
        this.onAutocompleteFocus('lblPaycodetype');
        this.onAutocompleteFocus('lblbaseOnPayCode');
        this.onAutocompleteFocus('lblUnitoofPay');
        this.onAutocompleteFocus('lblPayPeriod');
        this.onAutocompleteFocus('lblType');
        this.onAutocompleteFocus('lblRound');
        this.onAutocompleteFocus('lblmainAccount');
    }

    getTypeData(){
        this.paycodeSetupService.getTypeFieldDetails(this.typeId).then(data => {
         if(data.code == 201)
         {
             let val = data.result.records;
             for(let i=0;i<val.length;i++)
             {
                 this.typeData.push({id:val[i].id,value:val[i].desc})
             }
         }
         else{
            this.typeData = [];
         }
        });
    }

    getRowValue(row, gridFieldName) {
        if (gridFieldName === 'payperiod') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                        row[gridFieldName] === 7 ? 'Annually' :
                                            row[gridFieldName] === 8 ? 'Daily/Miscellaneous' : ''
            );
        } else if (gridFieldName === 'unitofPay') {
            return (
                row[gridFieldName] === 1 ? 'Weekly' :
                    row[gridFieldName] === 2 ? 'Biweekly' :
                        row[gridFieldName] === 3 ? 'Semimonthly' :
                            row[gridFieldName] === 4 ? 'Monthly' :
                                row[gridFieldName] === 5 ? 'Quarterly' :
                                    row[gridFieldName] === 6 ? 'Semiannually' :
                                        row[gridFieldName] === 7 ? 'Annually' :
                                            row[gridFieldName] === 8 ? 'Daily/Miscellaneous' : ''
            );
        } else if (gridFieldName === 'payCodeTypeId') {
            return (
                row[gridFieldName] === 1 ? 'Hourly' :
                    row[gridFieldName] === 2 ? 'Salary' :
                        row[gridFieldName] === 3 ? 'Piece work' :
                            row[gridFieldName] === 4 ? 'Overtime' :
                                row[gridFieldName] === 5 ? 'Vacation' :
                                    row[gridFieldName] === 6 ? 'Sick Time' :
                                        row[gridFieldName] === 7 ? 'Holiday' : ''
            );
        } else {
            return row[gridFieldName];
        }
    }

    getSuperviserIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
            this.getPayCode.filter((id: any) => {
                return query.test(id.payCodeId);
            })
        );
    }

    getPaycodeDetailsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'ig');
        return Observable.of(
            this.getPayDetails.filter((id: any) => {
                return query.test(id.payCodeId);
            })
        );
    }

    changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
        if (!e) {
            this.setBasePayCode = false;
        }
    }

    // typeaheadOnSelect(e: TypeaheadMatch): void {
    //     console.log("E", e);
    //     this.setBasePayCode = true;
    //     this.model.baseOnPayCodeId = e.item.id;
    //     if (e.value == '') {
    //         this.setBasePayCode = false;
    //     }
    //     if (e.item.baseOnPayCodeAmount != null) {
    //         this.model.baseOnPayCodeAmount = e.item.baseOnPayCodeAmount;
    //         this.model.payFactor = e.item.payFactor;
    //     } else {
    //         this.model.baseOnPayCodeAmount = e.item.payRate;
    //     }
    //     this.model.payRate = this.model.baseOnPayCodeAmount * this.model.payFactor;
    // }

    changeBasedOnPaycode(e) {
        debugger;
        this.setBasePayCode = true;
        this.model.baseOnPayCodeId = e.id;
        if (e.value == '') {
            this.setBasePayCode = false;
        }
        if (e.baseOnPayCodeAmount != null) {
            this.model.baseOnPayCodeAmount = e.baseOnPayCodeAmount;
            this.model.payFactor = e.payFactor;
        } else {
            this.model.baseOnPayCodeAmount = e.payRate;
        }
        this.model.payRate = this.model.baseOnPayCodeAmount * this.model.payFactor;
    }
    
    typeaheadOnSelect1(e: TypeaheadMatch): void {
        console.log("E", e);
        this.paycodeSetupService.getPayCodeById(e.item.id).then(data => {
            debugger;
            console.log("paycode get by id", data)
            this.model = data.result;
            this.model.id = 0;
        });
    }

    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if (pageInfo.sortOn == undefined) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if (pageInfo.sortBy == undefined) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;
        }

        // this.paycodeSetupService.getPaycodeId().then(data => {
        //     this.getPayCode = data.result.records;
        //     console.log("Data class options : " + this.getPayCode);
        // });



        this.page.searchKeyword = '';
        this.paycodeSetupService.getPaycodeDetails().then(pagedData => {
            debugger;

            // this.page = pagedData.result.records;
            this.rows =  pagedData.result.records;
        });
    }

    // Open form for create location
    Create() {
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        this.isPaycodeFormdisabled = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        /*this.paycodeSetupService.getBaseOnPayCodeDropdown(0).then(data => {
            this.baseOnPayCodeDropdownList = data.result.records;
            //console.log('dropdown',data.result.records);
        });*/
        this.paycodeSetupService.getbaseonPaycode().then(data => {
            this.baseOnPayCodeDropdownList = data.result.records;
            //console.log('dropdown',data.result.records);
        });

        this.paycodeSetupService.getPaycodeDetails().then(data => {
            this.getPayDetails = data.result.records;
            this.getPayCode = data.result.records;
            //console.log("Data class options111111 : " + this.getPayDetails);
        });

        this.model = {
            id: 0,
            payCodeId: '',
            description: '',
            arbicDescription: '',
            payCodeTypeId: '',
            baseOnPayCode: '',
            baseOnPayCode1: '',
            baseOnPayCodeAmount: '',
            payFactor: '',
            payRate: '',
            unitofPay: '',
            payperiod: '',
            inActive: false,
            payCodeTypeDesc: '',
            baseOnPayCodeId: null,
            payTypeId: 0,
            roundOf:0
            
        };
    }

    // Clear form to reset to default blank
    Clear(f: NgForm) {
        debugger;
        f.resetForm({
            payCodeTypeId: '',
            baseOnPayCode: '',
            baseOnPayCodeAmount: 0,
            unitofPay: '',
            payperiod: ''
        });

        this.selectedPaycode="";
        this.SelectedpayCodeTypeId="";
        this.selectedRoundOf="";
        this.SelectedunitofPay="";
        this.SelectedbaseOnPayCode="";
        this.Selectedpayperiod="";
        

        this.isPaycodeFormdisabled = false;
       // this.fieldName1.nativeElement.focus()

        this.model.payRate="";
        this.model=[];

        this.onAutocompleteFocusOut('lblpayCodeId');
        this.onAutocompleteFocusOut('lblPaycodetype');
        this.onAutocompleteFocusOut('lblbaseOnPayCode');
        this.onAutocompleteFocusOut('lblUnitoofPay');
        this.onAutocompleteFocusOut('lblPayPeriod');
        this.onAutocompleteFocusOut('lblType');
        this.onAutocompleteFocusOut('lblRound');
        this.onAutocompleteFocusOut('lblmainAccount');
    }

    //function call for creating new location
    CreatePayCode(f: NgForm, event: Event) {
        debugger;
        event.preventDefault();
        this.model.payCodeId=this.selectedPaycode;
        this.model.payperiod= this.Selectedpayperiod.id;
        this.model.roundOf=this.selectedRoundOf.id;
        this.model.unitofPay=this.SelectedunitofPay.id;
        this.model.baseOnPayCodeId=this.SelectedbaseOnPayCode.id;
        this.model.payCodeTypeDesc=this.SelectedpayCodeTypeId.desc;
        this.model.baseOnPayCode=this.SelectedbaseOnPayCode.payCodeId;
        this.model.baseOnPayCode1= "";

        if(this.selectedMainAccount!=undefined){
            this.model.actIndx=this.selectedMainAccount.id;
          }
        if(this.model.inActive!=undefined){
            this.model.inActive=this.model.inActive;
        }else{
            this.model.inActive=false;
        }
       
        this.model.payCodeTypeId=this.SelectedpayCodeTypeId.id;
        this.model.payTypeId=this.selectedPayType.id;

      
        var supIdx = this.model.payCodeId;

        if (!this.decimalValue) {
            return;
        }
        //Check if the id is available in the model.
        //If id avalable then update the location, else Add new location.
        if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
            this.isConfirmationModalOpen = true;
            this.isDeleteAction = false;
        }
        else {
            //Check for duplicate Location Id according to it create new location
            this.paycodeSetupService.checkDuplicatePayCode(supIdx).then(response => {
                if (response && response.code == 201 && response.result.isRepeat) {
                    this.duplicateWarning = true;
                    this.message.type = 'success';

                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.duplicateWarning = false;
                        }, 4000);
                        this.message.text = response.btiMessage.message + ' !';
                    }, 100);
                } else {
                    //Call service api for Creating new location
                   
                    this.paycodeSetupService.createPaycode(this.model).then(data => {
                        var datacode = data.code;
                        if (datacode == 201) {
                            window.scrollTo(0, 0);
                            window.setTimeout(() => {
                                this.isSuccessMsg = true;
                                this.isfailureMsg = false;
                                this.showMsg = true;
                                window.setTimeout(() => {
                                    this.showMsg = false;
                                    this.hasMsg = false;
                                    this.goBack();
                                }, 4000);
                                this.showCreateForm = false;
                                this.messageText = data.btiMessage.message;
                                ;
                            }, 100);

                            this.hasMsg = true;
                            f.resetForm();
                            //Refresh the Grid data after adding new location
                            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                        }
                    }).catch(error => {
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                            this.showMsg = true;
                            this.messageText = 'Server error. Please contact admin !';
                        }, 100)
                    });
                }

            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = 'Server error. Please contact admin !';
                }, 100)
            });

        }
    }

    //edit department by row
    edit(row: PaycodeSetupModule) {
        debugger;
        console.log('ROW', row)
        this.showCreateForm = true;
        this.model = Object.assign({}, row);
        console.log('this.model', this.model);
        // if (this.model.baseOnPayCodeAmount == null) {
        //     this.model.baseOnPayCodeAmount = row.payRate;
        // }
        this.payCodeId = row.payCodeId;
        this.isUnderUpdate = true;
        this.payCodeIdvalue = this.model.payCodeId;
        if (this.model.inActive == true) {
            this.isPaycodeFormdisabled = true;
        } else {
            this.isPaycodeFormdisabled = false;
        }

        /* */
        if(row.baseOnPayCode != ''){
            this.paycodeSetupService.getPayCodeById(row.baseOnPayCodeId).then(data => {
                console.log("paycode get by id", data)
                this.model.baseOnPayCodeAmount = data.result.payRate;
                this.calculatePayRate();
            });
        }

        this.paycodeSetupService.getBaseOnPayCodeDropdown(row.id).then(data => {
            this.baseOnPayCodeDropdownList = data.result.records;
            //console.log('dropdown',data.result.records);
            if (this.baseOnPayCodeDropdownList.baseOnPayCodeAmount == null) {
                this.baseOnPayCodeDropdownList.baseOnPayCodeAmount = this.baseOnPayCodeDropdownList.payRate;
            }
        });


        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
    }



    updateStatus() {
        this.closeModal();
        debugger;
        //Call service api for updating selected department

        // var tempModel=this.model;
        // this.model=[];
         
        
        // this.payCodeIdvalue=tempModel.payCodeId;
        // this.model.payCodeId = this.payCodeIdvalue;

        // this.model.arbicDescription=tempModel.arbicDescription
        // this.model.baseOnPayCode=tempModel.baseOnPayCode
        // this.model.baseOnPayCode1=tempModel.baseOnPayCode1
        // this.model.baseOnPayCodeAmount=tempModel.baseOnPayCodeAmount
        // this.model.baseOnPayCodeId=tempModel.baseOnPayCodeId
        // this.model.description=tempModel.description
        // this.model.id=tempModel.id
        // this.model.inActive=tempModel.inActive
     
        // this.model.payCodeTypeDesc=tempModel.payCodeTypeDesc
        // this.model.payCodeTypeId=tempModel.payCodeTypeId
        // this.model.payFactor=tempModel.payFactor
        // this.model.payRate=tempModel.payRate
        // this.model.payTypeId=tempModel.payTypeId
        // this.model.payperiod=tempModel.payperiod
        // this.model.roundOf=tempModel.roundOf
        // this.model.unitofPay=tempModel.unitofPay

        if(this.selectedMainAccount!=undefined){
            this.model.actIndx=this.selectedMainAccount.id;
          }

       
        this.paycodeSetupService.updatePaycode(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin !';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }

    checkdecimalpayRate(digit) {
        let isFirstDecimal = digit.indexOf('.') > -1 ? digit.split('.')[0].length == 0 : false

        if (digit == null || digit == '' || isFirstDecimal) {
            this.decimalValue = true;
            return false;
        } else {
            let count = (digit.indexOf('.') > -1) ? 11 : 10;
            this.tempp = digit.split(".");
            if (count > 10) {
                if (this.tempp[0].length > 7) {
                    this.decimalValue = false;
                    return true;
                } else if (this.tempp[1].length > 3) {
                    this.decimalValue = false;
                    return true;
                } else {
                    this.decimalValue = true;
                    return false;
                }

            } else {
                if (this.tempp[0].length > 10) {
                    this.decimalValue = false;
                    return true;
                } else {
                    this.decimalValue = true;
                    return false;
                }
            }
        }
    }

    //delete department by passing whole object of perticular Department
    delete() {
        var selectedSupervisors = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSupervisors.push(this.selected[i].id);
        }
        this.paycodeSetupService.deletePaycode(selectedSupervisors).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message + ' !';
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin !';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.supervisorService.searchSupervisorlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }

    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    /*confirm(): void {
        this.messageText = 'Confirmed!';
        //this.modalRef.hide();
        this.delete();
    }*/

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }



    sortColumn(val) {
        if (this.page.sortOn == val) {
            if (this.page.sortBy == 'DESC') {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    checkPaycodeForm(event) {
        if (event.target.checked == true) {
            this.isPaycodeFormdisabled = true;
        } else {
            this.isPaycodeFormdisabled = false;
        }

    }
    checkFrequency(event) {

        if (event.target.value == '') {
            this.baseOnPayCodedesc = 0;
            return false;
        }
        // this.paycodeSetupService.getPayCodeById(this.model.baseOnPayCode).then(pagedData => {
        //     this.baseOnPayCodedesc = pagedData.result.description;
        // });
    }
    calculatePayRate() {
        //console.log(event.target.value+'===='+this.model.baseOnPayCodeAmount);
        this.model.payRate = parseFloat(Number(this.model.baseOnPayCodeAmount) * Number(this.model.payFactor) + "").toFixed(3);
    }
    clearPrefixedData() {
        if (this.model.baseOnPayCode == '') {
            this.model.payRate = '';
            this.model.baseOnPayCodeAmount = '';
            this.model.payFactor = '';
        }
    }

    goBack(){
        this.router.navigate(['hcm/payCodeSetup']); 
      }
    goToCreate(){
        this.router.navigate(['createpaycodesetup']);   
      }
      edit1(event) {
     
     
        if(event.cellIndex!=0 && event.cellIndex!=1){
            this.payCodeId = event.row.id;
          
            var myurl = `${'createpaycodesetup'}/${this.payCodeId}`;
            this.router.navigateByUrl(myurl);
        }
    }
      EscapeModal(event){
          
        var key = event.key;
        if(key=="Escape"){
            this.closeModal();
        }
      }

      onAutocompleteFocus(labelId){

        let elem: HTMLElement = document.getElementById(labelId);
         elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
      }
      
      onAutocompleteFocusOut(labelId){
          let elem: HTMLElement = document.getElementById(labelId);
          elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
          
      }
      paycodeFocusIn(){
        this.isPayCodeOpen=true;
      }
      paycodeFocusOut(){
        
        this.isPayCodeOpen=false;
      
      }
    

      toggle1() {
        debugger;
        // this.isListHide = !this.isListHide;
        this.isPayCodeOpen=!this.isPayCodeOpen;
     
        
          this.filterInput1.nativeElement.focus();
         
      }

      toggle2() {
        debugger;
        // this.isListHide = !this.isListHide;
        this.isPaycodeTypeOpen=!this.isPaycodeTypeOpen;
     
        
          this.filterInput2.nativeElement.focus();
         
      }

      toggle3() {
        debugger;
        // this.isListHide = !this.isListHide;
        this.isunitofPayOpen=!this.isunitofPayOpen;
     
        
          this.filterInput3.nativeElement.focus();
         
      }

      
      toggle4(){
        this.isbaseOnPayCodeOpen=!this.isbaseOnPayCodeOpen;
     
        
          this.filterInput4.nativeElement.focus();
      }

      toggle5(){
        this.isPayPeriodOpen=!this.isPayPeriodOpen;
     
        
          this.filterInput5.nativeElement.focus();
      }

      toggle6(){
        this.isTypeOpen=!this.isTypeOpen;
     
        
          this.filterInput6.nativeElement.focus();
      }

      toggle7(){
        this.isRoundOpen=!this.isRoundOpen;
     
        
          this.filterInput7.nativeElement.focus();
      }


      getPaycodeById(id){
        this.paycodeSetupService.getPayCodeById(id).then(data => {
            debugger;
            console.log("paycode get by id", data)
            this.isUnderUpdate=true;
            this.setAutoCompleteLabel();
           
            this.model = data.result;
            this.selectedPaycode=this.model.payCodeId;

            if(this.model.baseOnPayCode){
                this.setBasePayCode=true;
            }
            else{
                this.setBasePayCode=false;
            }

            setTimeout(() => {

                if(this.PayTypeDropdownList!=undefined){
                    var currentPaytype=this.PayTypeDropdownList.filter(x=> x.id==this.model.payCodeTypeId);
                    this.SelectedpayCodeTypeId=currentPaytype[0];
                }
                   
                if(this.rows!=undefined){
                var currentBasedonpaycode=this.rows.filter(x=> x.id==this.model.baseOnPayCodeId);
                this.SelectedbaseOnPayCode=currentBasedonpaycode[0];
                }
    
                if(this.unitofPays!=undefined){
                  
                   var currentUnitofpay=this.unitofPays.filter(x=> x.id==this.model.unitofPay);
                   this.SelectedunitofPay=currentUnitofpay[0];
                    }

                    if(this.payperiods!=undefined){
                  
                        var currentPaypeiod=this.payperiods.filter(x=> x.id==this.model.payperiod);
                        this.Selectedpayperiod=currentPaypeiod[0];
                         }

                         if(this.payperiods!=undefined){
                  
                            var currentRoundof=this.roundings.filter(x=> x.id==this.model.roundOf);
                            this.selectedRoundOf=currentRoundof[0];
                             }

                             if(this.typeData!=undefined){
                  
                                var currentType=this.typeData.filter(x=> x.id==this.model.payTypeId);
                                this.selectedPayType=currentType[0];
                                 }


                                 if (this.mainAccList != undefined) {

                                    var currentMainAccount = this.mainAccList.filter(x => x.id == this.model.actIndx);
                                    this.selectedMainAccount = currentMainAccount[0];
                                }
                         
                    setTimeout(() => {
                        window.scrollTo(0, 2000);
                    }, 10);
                }, 10);
            this.SelectedpayCodeTypeId=this.model.pay;





        });
      }


      getPayCodeData(event){
          debugger;
        if(event.id!=undefined){
            var divId = event.id ? event.id : -1;
            this.model.payCodeId=divId;
           //  if(event.divisionId!=undefined){
            if(event.id>0 && event.id!=undefined)
            {
                this.getPaycodeById(event.id);
                this.setAutoCompleteLabel();
                this.isUnderUpdate=true;
            }
           } 
      }
}