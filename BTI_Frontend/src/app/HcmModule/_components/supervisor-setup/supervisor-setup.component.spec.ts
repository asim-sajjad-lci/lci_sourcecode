import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorSetupComponent } from './supervisor-setup.component';

describe('SupervisorSetupComponent', () => {
  let component: SupervisorSetupComponent;
  let fixture: ComponentFixture<SupervisorSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
