
import { Routes } from '@angular/router';

import { SupervisorSetupComponent } from 'app/HcmModule/_components/supervisor-setup/supervisor-setup.component';




export const SupervisorSetupRoutes: Routes = [{
  path: '',
  component: SupervisorSetupComponent,
  data: {
    breadcrumb: ""
  }
}];