import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { CreateSkillSetSetupRoutes } from './create-skill-set-setup.routing';
import { CreateSkillSetSetupComponent } from './create-skill-set-setup.component';
import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';







@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateSkillSetSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule
  ],

  providers:[AlertService],
  declarations: [CreateSkillSetSetupComponent]
})
export class CreateSkillSetSetupModule { }
