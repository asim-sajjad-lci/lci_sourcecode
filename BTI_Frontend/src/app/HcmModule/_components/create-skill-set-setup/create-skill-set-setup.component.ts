
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../../_sharedresource/page';
import { Constants } from '../../../_sharedresource/Constants';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { SkillSetSetupService } from '../../_services/skill-set-setup/skill-set-setup.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';

import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { skillSetSetup } from 'app/HcmModule/_models/skill-set-setup/skill-set-setup';
import { DropDownModule } from 'app/_sharedresource/_modules/drop-down.module';
import { element } from '../../../../../node_modules/protractor';
@Component({
  selector: 'app-create-skill-set-setup',
  templateUrl: './create-skill-set-setup.component.html',
  styleUrls: ['./create-skill-set-setup.component.css'],
  providers: [SkillSetSetupService,CommonService]
})
export class CreateSkillSetSetupComponent implements OnInit {

  page = new Page();
    rows = new Array<skillSetSetup>();
    temp = new Array<skillSetSetup>();
    selected = [];
    moduleCode = 'M-1011';
    screenCode = 'S-1421';
    moduleName;
    screenName;
	skillsetupindexdesc;
    defaultFormValues: object[];
    availableFormValues: [object];
    hasMessage;
    duplicateWarning;
    message = { 'type': '', 'text': '' };
    skillId={};
	skillSetIndexId : any = [];
    skillSetId = {};
    skillSetIdOptions=[];
    ddl_skillSetIdOptions=[];
    SelectedSkill;
    searchKeyword = '';
    ddPageSize: number = 5;
    // model: skillSetSetup;
    model: any = {};
    showCreateForm: boolean = false;
    isSuccessMsg: boolean;
    isfailureMsg: boolean;
    isUnderUpdate: boolean;
    hasMsg = false;
    showMsg = false;
	tempp:string[]=[];
	islifeTimeValid: boolean = true;
    messageText: string;
    isConfirmationModalOpen: boolean = false;
    currentLanguage: any;
    confirmationModalTitle = Constants.confirmationModalTitle;
    confirmationModalBody = Constants.confirmationModalBody;
    deleteConfirmationText = Constants.deleteConfirmationText;
    OkText = Constants.OkText;
    CancelText = Constants.CancelText;
    isDeleteAction: boolean = false;
    skillIdvalue: string;
    getSetId:any[]=[];
    setIdList: Observable<any>;
    typeaheadLoading: boolean;
	typeaheadNoResults: boolean;
    skillSetDescId: any;
    skillsetsetupdesc:any[]=[];
    skillsetdesparr:any[]=[];
    skillSetsetId:any=[]; 

    isSkillsetIdOpen:boolean=false;
    selectedSkillSet;
    frequencyArray = ["Hourly","Weekly","Biweekly","Semimonthly","Monthly","Quarterly","Semianually","Anually"];
    skillrequiredpara = {"true" : "Yes","false" : "No"};
    // Label Veriable
    SKILL_SET_SETUP_SET_ID:any;
    SKILL_SET_SETUP_DESCRIPTION:any;
    SKILL_SET_SETUP_ARABIC_DESCRIPTION:any;
    SKILL_SET_SETUP_SKILL_ID:any;
    SKILL_SET_SETUP_SKILL_REQUIRED:any;
    SKILL_SET_SETUP_COMMENT:any;
    SKILL_SET_SETUP_SEQUENCE:any;
    SKILL_SET_SETUP_DELETE_LABEL:any;
    SKILL_SET_SETUP_SAVE_LABEL:any;
    SKILL_SET_SETUP_CLEAR_LABEL:any;
    SKILL_SET_SETUP_CREATE_LABEL:any;
    SKILL_SET_SETUP_UPDATE_LABEL:any;
    SKILL_SET_SETUP_ACTION:any;
    SKILL_SET_SETUP_CANCEL_LABEL:any;
    SKILL_SET_SETUP_SEARCH_LABEL:any;
    SKILL_SET_SETUP_CREATE_FORM_LABEL:any;
    SKILL_SET_SETUP_UPDATE_FORM_LABEL:any;
    SKILL_SET_SETUP_TABLEVIEW:any;

    ddlCompanySetting = {};
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selectCompany = "";

    tableViews: DropDownModule[] = [
        { id: 5, name: '5 at a time' },
        { id: 15, name: '15 at a time' },
        { id: 50, name: '50 at a time' },
        { id: 100, name: '100 at a time' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('target') private myScrollContainer: ElementRef;

    constructor(private router: Router,
        private SkillSetSetupService: SkillSetSetupService,
        private getScreenDetailService: GetScreenDetailService,
        private alertService: AlertService,
        private route: ActivatedRoute,
        private commonService: CommonService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        this.page.sortOn = 'id';
        this.page.sortBy = 'DESC';
        // default form parameter for department  screen
        this.defaultFormValues = [
            { 'fieldName': 'SKILL_SET_SETUP_SET_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SKILL_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SKILL_REQUIRED', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_COMMENT', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SEQUENCE', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_ACTION', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_SEARCH_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
            { 'fieldName': 'SKILL_SET_SETUP_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        ];

        this.SKILL_SET_SETUP_SET_ID = this.defaultFormValues[0];
        this.SKILL_SET_SETUP_DESCRIPTION = this.defaultFormValues[1];
        this.SKILL_SET_SETUP_ARABIC_DESCRIPTION = this.defaultFormValues[2];
        this.SKILL_SET_SETUP_SKILL_ID = this.defaultFormValues[3];
        this.SKILL_SET_SETUP_SKILL_REQUIRED = this.defaultFormValues[4];
        this.SKILL_SET_SETUP_COMMENT = this.defaultFormValues[5];
        this.SKILL_SET_SETUP_SEQUENCE = this.defaultFormValues[6];
        this.SKILL_SET_SETUP_DELETE_LABEL = this.defaultFormValues[7];
        this.SKILL_SET_SETUP_SAVE_LABEL = this.defaultFormValues[8];
        this.SKILL_SET_SETUP_CLEAR_LABEL = this.defaultFormValues[9];
        this.SKILL_SET_SETUP_CREATE_LABEL = this.defaultFormValues[10];
        this.SKILL_SET_SETUP_UPDATE_LABEL = this.defaultFormValues[11];
        this.SKILL_SET_SETUP_ACTION = this.defaultFormValues[12];
        this.SKILL_SET_SETUP_CANCEL_LABEL = this.defaultFormValues[13];
        this.SKILL_SET_SETUP_SEARCH_LABEL = this.defaultFormValues[14];
        this.SKILL_SET_SETUP_CREATE_FORM_LABEL = this.defaultFormValues[15];
        this.SKILL_SET_SETUP_UPDATE_FORM_LABEL = this.defaultFormValues[16];
        this.SKILL_SET_SETUP_TABLEVIEW = this.defaultFormValues[17];
        this.setIdList = Observable.create((observer: any) => {
            // Runs on every search
            observer.next(this.model.skillSetId);
          }).mergeMap((token: string) => this.getSetIdAsObservable(token));
    }

    ngOnInit() {


        this.ddlCompanySetting = { 
            singleSelection: false, 
            text:this.selectCompany,
            selectAllText: this.selectAll,
            unSelectAllText: this.unselectAll,
            enableSearchFilter: true,
            classes:"myclass custom-class"
          };            

      this.route.params.subscribe((params: Params) => {
        debugger;
        this.skillSetId = params['skillSetId']; 

        if (this.skillSetId != '' && this.skillSetId != undefined) {

            this.getSkillSetById(this.skillSetId);
            this.isUnderUpdate=true;
            this.onAutocompleteFocus('lblskills');
            this.onAutocompleteFocus('lblSkillSetId');
        }

    });



        this.skillSetsetId.forEach(element => {
            this.model.listSkillSteup.push({id:element});
        });
        
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.currentLanguage = localStorage.getItem('currentLanguage');

        //getting screen labels, help messages and validation messages
        this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);
        
        //Following shifted from SetPage for better performance
        this.SkillSetSetupService.getSetId().then(data => {
            this.getSetId = data.result.records;                
           //console.log("Data class options : "+this.getSetId);
        });

    }
    getSetIdAsObservable(token: string): Observable<any> {
        token = token.replace(/\\/g, "\\\\");
        let query = new RegExp(token, 'i');
        return Observable.of(
          this.getSetId.filter((id: any) => {
            return query.test(id.skillSetId);
          })
        );
      }
     
      changeTypeaheadLoading(e: boolean): void {
        this.typeaheadLoading = e;
      }
     
      typeaheadOnSelect(e: TypeaheadMatch): void {
          console.log('Selected value: ', e);
            this.SkillSetSetupService.getSkillsSetup(e.item.id).then(data => {
                console.log('DATA',data );
                this.model = data.result;
                console.log('this.model',this.model );
                this.model.id = 0;
                this.skillsetsetupdesc = [];    
                this.model.listSkillSteup = [];
                this.skillsetupindexdesc = '';
                this.model['dtoSkillSteupList'] != undefined && this.model['dtoSkillSteupList'].length > 0?
                    this.model['dtoSkillSteupList'].forEach(element=>{
                        this.model.listSkillSteup.push(element['id']);
                        this.skillsetsetupdesc.push(element['skillId']);
                    }):null;
                this.skillsetupindexdesc = this.skillsetsetupdesc.join();
            });
      } 
    setPage(pageInfo) {
        //debugger;
        this.selected = []; // remove any selected checkbox on paging
        this.page.pageNumber = pageInfo.offset;
        if( pageInfo.sortOn == undefined ) {
            this.page.sortOn = this.page.sortOn;
        } else {
            this.page.sortOn = pageInfo.sortOn;
        }
        if( pageInfo.sortBy == undefined ) {
            this.page.sortBy = this.page.sortBy;
        } else {
            this.page.sortBy = pageInfo.sortBy;   
        }
        this.page.searchKeyword = '';
        this.SkillSetSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
            this.page = pagedData.page;
            this.rows = pagedData.data;
            console.log(this.rows);
            this.SkillSetSetupService.getSkillSetList().then(data => {
                this.skillSetIdOptions = data.result.records;

                for(var i=0;i<this.skillSetIdOptions.length;i++)
            {
              debugger;
                this.ddl_skillSetIdOptions.push({ "id": this.skillSetIdOptions[i].id, "itemName": this.skillSetIdOptions[i].skillId,"itemDescription":this.skillSetIdOptions[i].skillDesc })
            }
               //console.log('test',this.skillSetIdOptions );
            });
			console.log('all data',pagedData);
        });
       
    }

    // Open form for create location
    Create() {
        this.skillSetsetId=[];
        this.showCreateForm = false;
        this.isUnderUpdate = false;
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                window.scrollTo(0, 2000);
            }, 10);
        }, 10);
        this.model = {
            id: 0,
            skillId:'',
            skillSetId:'',
            skillSetDiscription: '',
            arabicDiscription: '',
            skillSetSeqn: '',
            skillRequired:true,
            comment:'',
            skillSetDescId:0,
            skillSetIndexId:'',
            skillIdParam:'',
            listSkillSteup:[],
            skillsIds:''
        };
		this.skillsetupindexdesc='';
    }

    
     // Clear form to reset to default blank
     Clear(f: NgForm) {
        f.resetForm({frequency:0,skillId:'', listSkillSteup:{} });
        this.skillsetupindexdesc='';
        this.skillSetsetId = "";
        this.selectedSkillSet=null;
        this.SelectedSkill=null;
      

        this.onAutocompleteFocusOut('lblskills');
        this.onAutocompleteFocusOut('lblSkillSetId');
    }


    //function call for creating new location
    CreateSkillsSetup(f: NgForm, event: Event) {
        debugger;
          event.preventDefault();
  
      
          var locIdx = this.selectedSkillSet;
  
         
  
          //Check if the id is available in the model.
          //If id avalable then update the Deduction Code, else Add new Deduction Code.
          if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
              this.isConfirmationModalOpen = true;
              this.isDeleteAction = false;
          }
          else {
              //Check for duplicate Deduction Code Id according to it create new Deduction Code
              debugger;
              this.model.listSkillSteup=[];
              this.model.skillsetId=this.selectedSkillSet;
      
              this.model.arabicDiscription= this.model.skillSetArbicDesc;
              // this.model.id= 0;
              this.model.skillId= "";
              this.model.skillIdParam= "";
              this.model.skillSetDescId= 0;
              this.model.skillSetDiscription=this.model.skillSetDesc;
              this.model.skillSetIndexId= "";
              this.model.skillsIds= "";
              this.model.skillSetId=this.model.skillsetId;
      
              this.SelectedSkill.forEach(element => {
                  this.skillSetsetId.push(element.id);
              });
      
  
              this.skillSetsetId.forEach(element => {
                  this.model.listSkillSteup.push({id:element});
              });
              this.SkillSetSetupService.checkDuplicateSkillsSetupId(locIdx).then(response => {
                 
                  
                  if (response && response.code == 302 && response.result && response.result.isRepeat) {
                      this.duplicateWarning = true;
                      this.message.type = 'success';
  
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.duplicateWarning = false;
                          }, 4000);
                          this.message.text = response.btiMessage.message;
                         
                      }, 100);
                  } else {
                      //Call service api for Creating new Deduction Code
                      debugger;
  
                     
                      
                      this.SkillSetSetupService.createSkillSteup(this.model).then(data => {
                         
                          var datacode = data.code;
                          if (datacode == 201) {
                              window.scrollTo(0, 0);
                              window.setTimeout(() => {
                                  this.isSuccessMsg = true;
                                  this.isfailureMsg = false;
                                  this.showMsg = true;
                                  window.setTimeout(() => {
                                      this.showMsg = false;
                                      this.hasMsg = false;
                                  }, 4000);
                                  this.showCreateForm = false;
                                  this.messageText = data.btiMessage.message;
                                  ;
                              }, 100);
  
                              this.hasMsg = true;
                              f.resetForm();
                              this.goBack();
                              this.skillsetupindexdesc='';
                              //Refresh the Grid data after adding new Deduction Code
                              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                          }
                      }).catch(error => {
                          this.hasMsg = true;
                          window.setTimeout(() => {
                              this.isSuccessMsg = false;
                              this.isfailureMsg = true;
                              this.showMsg = true;
                              this.messageText = 'Server error. Please contact admin.';
                          }, 100)
                      });
                  }
  
              }).catch(error => {
                  this.hasMsg = true;
                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      this.messageText = 'Server error. Please contact admin.';
                  }, 100)
              });
  
          }
      }
    //edit department by row
    editcheckbox(row: skillSetSetup,event) {        
        this.model = Object.assign({},row);
		this.model.skillRequired =  event.target.checked;
		this.skillSetId = row.skillSetId;
        this.skillSetDescId = this.model.skillSetDescId;
		this.skillIdvalue = this.model.skillSetId;
		this.isConfirmationModalOpen = true;
        this.isDeleteAction = false;
		//console.log('model == ',this.model);
    }
	//edit department by row
    edit(row: skillSetSetup) {
        this.showCreateForm = true;
        this.model = Object.assign({},row);
        this.skillSetId = row.skillSetId;
        this.isUnderUpdate = true;
        this.skillIdvalue = this.model.skillSetId;
        this.skillSetDescId = this.model.skillSetDescId;
        this.skillsetsetupdesc=[];
        this.skillSetsetId =[];
        this.model.listSkillSteup.forEach(element=>{
            this.skillSetsetId.push(element['id']);
            this.skillsetsetupdesc.push(element['skillDesc']);
        });
        this.skillsetupindexdesc = this.skillsetsetupdesc.join();
       //console.log('skills',this.skillsetupindexdesc)
        this.model.listSkillSteup=[];
        setTimeout(() => {
            window.scrollTo(0, 2000);
        }, 10);
		this.SkillSetSetupService.getSkillSetupById(row.listSkillSteup['id']).then(pagedData => {
            this.skillsetupindexdesc = pagedData.result.skillDesc;
        });
    }

    updateStatus() {
        this.closeModal();
   
        // this.model.skillSetId = this.skillIdvalue;

    var tempModel=this.model;

        this.model.skillSetDescId = this.model.skillsetId;

        this.model={};


       
        this.model.arabicDiscription=tempModel.skillSetArbicDesc;
        this.model.comment= tempModel.comment;
        this.model.id=tempModel.id;
     
        this.model.skillId=null;
        this.model.skillIdParam=null;
        this.model.skillRequired=tempModel.skillRequired;
        this.model.skillSetArbicDesc= tempModel.skillSetArbicDesc;
        this.model.skillSetDesc= tempModel.skillSetDesc;
        this.model.skillSetDescId= tempModel.id;
        this.model.skillSetDiscription= tempModel.skillSetDesc;
        this.model.skillSetId= tempModel.skillSetId;
        this.model.skillSetIndexId=0;
        this.model.skillSetSeqn= tempModel.skillSetSeqn;
        // this.model.skillsIds="DEF,";
        this.model.listSkillSteup=[];

        // this.model.arabicDiscription= this.model.skillSetArbicDesc;
       
        // this.model.skillId= "";
        // this.model.skillIdParam= "";
        // this.model.skillSetDescId= 0;
        // this.model.skillSetDiscription=this.model.skillSetDesc;
        // this.model.skillSetIndexId= "";
        // this.model.skillsIds= "";
        // this.model.skillSetId=this.model.skillsetId;


        this.SelectedSkill.forEach(element => {
            this.skillSetsetId.push(element.id);
        });


        this.skillSetsetId.forEach(element => {
            this.model.listSkillSteup.push({id:element});
        });
        
        //Call service api for updating selected department
        this.SkillSetSetupService.updateSkillsSetup(this.model).then(data => {

            var datacode = data.code;
           
            if (datacode == 201) {
                //Refresh the Grid data after editing department
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
				this.skillsetupindexdesc='';
                //Scroll to top after editing department
                window.scrollTo(0, 0);
                window.setTimeout(() => {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    this.messageText = data.btiMessage.message;
                    ;
                    this.showCreateForm = false;
                }, 100);

                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMsg = false;
                }, 4000);
            }
        }).catch(error => {
            console.log("error...",error)
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = 'Server error. Please contact admin.';
            }, 100)
        });
    }

    varifyDelete() {
        if (this.selected.length > 0) {
            this.showCreateForm = false;
            this.isDeleteAction = true;
            this.isConfirmationModalOpen = true;
        } else {
            this.isSuccessMsg = false;
            this.hasMessage = true;
            this.message.type = 'error';
            this.isfailureMsg = true;
            this.showMsg = true;
            this.message.text = 'Please select at least one record to delete.';
            window.scrollTo(0, 0);
        }
    }


    //delete department by passing whole object of perticular Department
    delete() {
        var selectedSkillsSetups = [];
        for (var i = 0; i < this.selected.length; i++) {
            selectedSkillsSetups.push(this.selected[i].id);
        }
        this.SkillSetSetupService.deleteSkillsSetup(selectedSkillsSetups).then(data => {
            var datacode = data.code;
            if (datacode == 200) {
                this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
            }
            this.hasMessage = true;
            if(datacode == "302"){
                this.message.type = "error";
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
            } else {
                this.isSuccessMsg = true;
                this.message.type = "success";
                this.isfailureMsg = false;
            }
            //this.message.text = data.btiMessage.message + " !";

            window.scrollTo(0, 0);
            window.setTimeout(() => {
                this.showMsg = true;
                window.setTimeout(() => {
                    this.showMsg = false;
                    this.hasMessage = false;
                }, 4000);
                this.message.text = data.btiMessage.message;
            }, 100);

            //Refresh the Grid data after deletion of department
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

        }).catch(error => {
            this.hasMessage = true;
            this.message.type = 'error';
            var errorCode = error.status;
            this.message.text = 'Server issue. Please contact admin.';
        });
        this.closeModal();
    }

    // default list on page
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    // search department by keyword
    updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
    }


    // Set default page size
    changePageSize(event) {
        this.page.size = event.target.value;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }

    confirm(): void {
        this.messageText = 'Confirmed!';
        this.delete();
    }

    closeModal() {
        this.isDeleteAction = false;
        this.isConfirmationModalOpen = false;
    }
    checkFrequency(event) {
        this.skillSetsetId.forEach(element=>{
        this.skillsetdesparr = [];
       //console.log('id',element);
		this.SkillSetSetupService.getSkillSetupById(element).then(pagedData => {
            //this.skillsetupindexdesc = pagedData.result.skillDesc;
            this.skillsetdesparr.push(pagedData.result.skillDesc);
            this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
        });
		if (event.target.value == 0) {
            return false;
        }
    });
    }

    sortColumn(val){
        if( this.page.sortOn == val ) {
            if( this.page.sortBy == 'DESC' ) {
                this.page.sortBy = 'ASC';
            } else {
                this.page.sortBy = 'DESC';
            }
        }
        this.page.sortOn = val;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    }
	
	checkdecimal(digit)
	{
		console.log(digit);
		this.tempp=digit.split(".");
		if(this.tempp != null && this.tempp[1]!=null && ((this.tempp[0].length > 7) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValid = false;
			console.log(this.islifeTimeValid);
		}
		else if(this.tempp != null &&  ((this.tempp[1]==null && this.tempp[0].length > 10) || (this.tempp[1] != null && this.tempp[1].length > 3))){
			console.log("in the condition");
			this.islifeTimeValid = false;
			console.log(this.islifeTimeValid);
		}
		
		else{
			this.islifeTimeValid = true;
			console.log(this.islifeTimeValid);
		}
	
	}
	updateSkillCheckbox(val1,val2){
  }
  

  onAutocompleteFocus(labelId){

    let elem: HTMLElement = document.getElementById(labelId);
    elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
  }
  
  onAutocompleteFocusOut(labelId){
      let elem: HTMLElement = document.getElementById(labelId);
      elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
      
  }
 
  skillsetIdFocusout(){
         this.isSkillsetIdOpen=false;
 
}

skillsetIdFocusIn(){
    this.isSkillsetIdOpen=true;
}







  goBack(){
    this.router.navigate(['hcm/skillSetSetup']); 
  }


  getSkillSetById(skillsetId){
    debugger;  
   this.SelectedSkill=[];
//    alert(skillsetId)
          this.SkillSetSetupService.getSkillsSetup(skillsetId).then(data => {
            debugger;

            this.isUnderUpdate=true;
            this.onAutocompleteFocus('lblskills');
            this.onAutocompleteFocus('lblSkillSetId');


            console.log('DATA',data );
            this.model = data.result;
      
            this.selectedSkillSet=data.result.skillSetId;
            console.log('this.model',this.model );
            this.model.id = data.result.skillsSetPrimaryId;
            this.skillsetsetupdesc = [];    
            this.model.listSkillSteup = [];
            this.skillsetupindexdesc = '';
            this.model['dtoSkillSteupList'] != undefined && this.model['dtoSkillSteupList'].length > 0?
                this.model['dtoSkillSteupList'].forEach(element=>{
                    debugger;
                    this.model.listSkillSteup.push(element['id']);
                    this.skillsetsetupdesc.push(element['skillId']);

                
                           this.SelectedSkill.push({ "id": element['id'], "itemName": element['skillId'],"itemDescription":element['skillDesc']})
                      

                }):null;
             
                this.SelectedSkill.forEach(element => {
                    this.skillsetdesparr.push(element.itemName+':'+'\t'+element.itemDescription);
                this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
                });
               
          //  this.skillsetupindexdesc = this.skillsetsetupdesc.join();
        });

       
  }

  getSkillsetData(event){
    var skillsetId = event.id ? event.id : -1;
      
    // this.model.skillsetId=skillsetId;

    if(event.id>0 && event.id!=undefined)
    {
        this.getSkillSetById(event.id);
    }

  }


  onItemSelect(item:any){
    console.log("selected_skills",item)

    this.skillsetdesparr=[];
    this.SelectedSkill.forEach(element => {
        this.skillsetdesparr.push(element.itemName+':'+'\t'+element.itemDescription);
    this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
    });

   
}
OnItemDeSelect(item:any){
    debugger;
    this.skillsetdesparr=[];
    
    if(this.SelectedSkill.length>0){
        this.SelectedSkill.forEach(element => {
            this.skillsetdesparr.push(element.itemName+':'+'\t'+element.itemDescription);
        this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
        });
    }
    else{
        this.skillsetdesparr.pop();
        this.skillsetupindexdesc=""
    }
   
    
}
onSelectAll(items: any){
    debugger;
    this.skillsetdesparr=[];
    this.SelectedSkill.forEach(element => {
        this.skillsetdesparr.push(element.itemName+':'+'\t'+element.itemDescription);
    this.skillsetupindexdesc = this.skillsetdesparr.join('\n');
    });
}
onDeSelectAll(items: any){
    debugger;
    
    this.skillsetdesparr.pop();
    this.skillsetdesparr=[];
    this.skillsetupindexdesc=""
}

EscapeModal(event){
        
    var key = event.key;
    if(key=="Escape"){
        this.closeModal();
    }
  }

}
