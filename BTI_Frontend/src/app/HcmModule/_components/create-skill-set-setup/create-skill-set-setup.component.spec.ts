import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSkillSetSetupComponent } from './create-skill-set-setup.component';

describe('CreateSkillSetSetupComponent', () => {
  let component: CreateSkillSetSetupComponent;
  let fixture: ComponentFixture<CreateSkillSetSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSkillSetSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSkillSetSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
