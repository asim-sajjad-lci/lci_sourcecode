import { Component, OnInit } from '@angular/core';
import { TicketRouteSetupModel } from 'app/HcmModule/_models/ticket-route-setup/ticket-route-setup';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { NgForm } from '@angular/forms';
import { TicketRouteSetupService } from 'app/HcmModule/_services/ticket-route-setup/ticket-route-setup.service';
import { Observable } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { Constants } from 'app/_sharedresource/Constants';

@Component({
  selector: 'app-create-ticket-route-setup',
  templateUrl: './create-ticket-route-setup.component.html',
  styleUrls: ['./create-ticket-route-setup.component.css'],
  providers: [CommonService, TicketRouteSetupService]
})
export class CreateTicketRouteSetupComponent implements OnInit {

  model: TicketRouteSetupModel;

  isCodeIdValid = true;

  moduleCode = "M-1011";
  screenCode = "S-1790";
  moduleName;
  screenName;
  currentLanguage: any;
  defaultFormValues: object[];

  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  departmentId = {};
  searchKeyword = '';
  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;
  isDeleteAction: boolean = false;

  selectedTicketRouteSetup;
  RoueteId;

  ticketRouteIdList: Observable<any>;
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;

  getDepartment: any[] = [];
  rows = new Array<TicketRouteSetupModel>();

  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;


  TICKETROUTE_CodeId: any;
  TICKETROUTE_DESCRIPTION: any;
  TICKETROUTE_SEARCH: any;
  TICKETROUTE_ARABIC_DESCRIPTION: any;
  TICKETROUTE_CREATE_LABEL: any;
  TICKETROUTE_SAVE_LABEL: any;
  TICKETROUTE_CLEAR_LABEL: any;
  TICKETROUTE_CANCEL_LABEL: any;
  TICKETROUTE_UPDATE_LABEL: any;
  TICKETROUTE_DELETE_LABEL: any;
  TICKETROUTE_CREATE_FORM_LABEL: any;
  TICKETROUTE_UPDATE_FORM_LABEL: any;
  TICKETROUTE_TICKETROUTE_LIST: any;


  constructor(private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private service: TicketRouteSetupService, ) {


    this.defaultFormValues = [
      { 'fieldName': 'TICKETROUTE_CodeId', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_TICKETROUTE_LIST', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
      { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
    ];
    this.TICKETROUTE_CodeId = this.defaultFormValues[0];
    this.TICKETROUTE_DESCRIPTION = this.defaultFormValues[1];
    this.TICKETROUTE_SEARCH = this.defaultFormValues[2];
    this.TICKETROUTE_ARABIC_DESCRIPTION = this.defaultFormValues[3];
    this.TICKETROUTE_CREATE_LABEL = this.defaultFormValues[4];
    this.TICKETROUTE_SAVE_LABEL = this.defaultFormValues[5];
    this.TICKETROUTE_CLEAR_LABEL = this.defaultFormValues[6];
    this.TICKETROUTE_CANCEL_LABEL = this.defaultFormValues[7];
    this.TICKETROUTE_UPDATE_LABEL = this.defaultFormValues[8];
    this.TICKETROUTE_DELETE_LABEL = this.defaultFormValues[9];
    this.TICKETROUTE_CREATE_FORM_LABEL = this.defaultFormValues[10];
    this.TICKETROUTE_UPDATE_FORM_LABEL = this.defaultFormValues[11];
    this.TICKETROUTE_TICKETROUTE_LIST = this.defaultFormValues[12];


    this.ticketRouteIdList = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.model.codeId);
    }).mergeMap((token: string) => this.getReportPositionsAsObservable(token));

  }

  ngOnInit() {

    this.model = {
      id: 0,
      codeId: '',
      descr: '',
      arabicDescr: '',
    };

    this.route.params.subscribe((params: Params) => {
      debugger
      this.RoueteId = params['id'];

      if (this.RoueteId != '' && this.RoueteId != undefined) {

        this.getTicketRouteById(this.RoueteId);
        this.isUnderUpdate = true;
        this.onAutocompleteFocus('lblTicketRouteId');
      }

    });

    this.setPage();

    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


    //   this.model = {
    //     id: 0,
    //     codeId: '',
    //     descr: '',
    //     arabicDescr: '',
    // };
  }

  getReportPositionsAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
      this.getDepartment.filter((id: any) => {
        return query.test(id.departmentId);
      })
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    // console.log('Selected value: ', e);
    this.service.getTicketRouteById(e.item.id).then(allData => {
      // console.log('allData', allData);
      debugger;
      this.model = allData.result;
      this.model.id = 0;
    });

  }


  goBack() {
    this.router.navigate(['hcm/ticketroutesetup']);
  }

  //function call for creating new TicketRoute
  CreateTicketRoute(f: NgForm, event: Event) {
    debugger;

    event.preventDefault();
    var routeIdx = this.model.codeId;

    //Check if the id is available in the model.
    //If id avalable then update the department, else Add new department.
    if (this.model.id > 0 && this.model.id != 0 && this.model.id != undefined) {
      this.isConfirmationModalOpen = true;
      this.isDeleteAction = false;
    }
    else {
      //Check for duplicate Division Id according to it create new division
      this.service.checkDuplicateRouteId(routeIdx).then(response => {
        if (response && response.code == 302 && response.result && response.result.isRepeat) {
          this.duplicateWarning = true;
          this.message.type = "success";

          window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            window.setTimeout(() => {
              this.showMsg = false;
              this.duplicateWarning = false;
            }, 4000);
            this.message.text = response.btiMessage.message;

          }, 100);
        } else {
          //Call service api for Creating new department
          this.service.createTicketRoute(this.model).then(data => {
            var datacode = data.code;
            if (datacode == 201) {

              this.isSuccessMsg = true;
              this.isfailureMsg = false;

              this.showMsg = true;
              this.messageText = data.btiMessage.message;
              this.hasMsg = true;
              window.setTimeout(() => {
                this.showMsg = false;
                this.hasMsg = false;
                this.goBack();


              }, 4000);

            }
          }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = "Server error. Please contact admin.";
            }, 100)
          });
        }

      }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
          this.showMsg = true;
          this.messageText = "Server error. Please contact admin.";
        }, 100)
      });

    }
  }

  handleNameInput(e) {

    let keycode = e.keyCode;

    if ((keycode < 65 || keycode > 90) && keycode != 189 && keycode != 16 && keycode != 20 && keycode != 8) {
      this.isCodeIdValid = false;
      var str = this.model.codeId;
      str = str.slice(0, -1);
      this.model.codeId = str;
      window.setTimeout(() => {
        this.isCodeIdValid = true;
      }, 2000);
    }


  }

  getTicketRouteById(Routeid) {
    this.model.id = Routeid;
    debugger;
    this.isUnderUpdate = true;
    this.onAutocompleteFocus('lblTicketRouteId');
    this.service.getTicketRouteById(Routeid).then(allData => {

      console.log("selectedDepartmentSetup.........", allData)
      this.model.id = allData.result.id;
      this.selectedTicketRouteSetup = allData.result.id;

      this.model = allData.result;
      // this.model.id = 0;
      // console.log('this.model', this.model)
    });

  }


  getRouteSetupData(event) {

    var Id = event.id ? event.id : -1;

    this.model.id = Id;

    if (event.id > 0 && event.id != undefined) {
      this.getTicketRouteById(event.id);
    }

  }

  onAutocompleteFocus(labelId) {

    let elem: HTMLElement = document.getElementById(labelId);
    elem.setAttribute("style", "top:-10px;font-size:14px;color:#5264AE;font-weight: bold;");
  }

  setPage() {
    debugger;
    // this.selected = []; // remove any selected checkbox on paging
    // this.page.pageNumber = pageInfo.offset;
    // if (pageInfo.sortOn == undefined) {
    //     this.page.sortOn = this.page.sortOn;
    // } else {
    //     this.page.sortOn = pageInfo.sortOn;
    // }
    // if (pageInfo.sortBy == undefined) {
    //     this.page.sortBy = this.page.sortBy;
    // } else {
    //     this.page.sortBy = pageInfo.sortBy;
    // }

    // this.page.searchKeyword = '';
    this.service.getTicketRoutes().then(pagedData => {
      debugger;
      // this.page = pagedData.page;
      this.rows = pagedData.result;
    });
  }


  closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
  }


  updateStatus() {
    this.closeModal();
    //Call service api for updating selected department
    debugger;
    this.model.id = this.selectedTicketRouteSetup;
    this.service.updateTicketRoute(this.model).then(data => {
      var datacode = data.code;
      if (datacode == 201) {

        this.isSuccessMsg = true;
        this.isfailureMsg = false;

        this.showMsg = true;
        this.messageText = data.btiMessage.message;
        this.hasMsg = true;
        window.setTimeout(() => {
          this.showMsg = false;
          this.hasMsg = false;
          // this.goBack();


        }, 4000);

      }
    }).catch(error => {
      this.hasMsg = true;
      window.setTimeout(() => {
        this.isSuccessMsg = false;
        this.isfailureMsg = true;
        this.showMsg = true;
        this.messageText = "Server error. Please contact admin.";
      }, 100)
    });
  }

  Clear(f: NgForm) {
    f.resetForm();
    this.RoueteId=[];
      this.model = {
        id: 0,
        codeId: '',
        descr: '',
        arabicDescr: '',
    };
}

}
