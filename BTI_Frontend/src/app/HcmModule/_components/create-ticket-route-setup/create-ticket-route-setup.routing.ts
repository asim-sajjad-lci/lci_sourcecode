import { Routes } from '@angular/router';
import { CreateTicketRouteSetupComponent } from './create-ticket-route-setup.component';


export const CreateTicketRouteSetupRoutes: Routes = [{
  path: '',
  component: CreateTicketRouteSetupComponent,
  data: {
    breadcrumb: ""
  }
}];