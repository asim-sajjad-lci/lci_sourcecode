import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { CreateTicketRouteSetupComponent } from './create-ticket-route-setup.component';
import { CreateTicketRouteSetupRoutes } from './create-ticket-route-setup.routing';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateTicketRouteSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
  ],

  providers:[AlertService],
  declarations: [CreateTicketRouteSetupComponent]
})
export class CreateTicketRouteSetupModule { }