import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTicketRouteSetupComponent } from './create-ticket-route-setup.component';

describe('CreateTicketRouteSetupComponent', () => {
  let component: CreateTicketRouteSetupComponent;
  let fixture: ComponentFixture<CreateTicketRouteSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTicketRouteSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTicketRouteSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
