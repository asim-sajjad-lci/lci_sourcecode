import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeIdentificationInfoComponent } from './employee-identification-info.component';

describe('EmployeeIdentificationInfoComponent', () => {
  let component: EmployeeIdentificationInfoComponent;
  let fixture: ComponentFixture<EmployeeIdentificationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeIdentificationInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeIdentificationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
