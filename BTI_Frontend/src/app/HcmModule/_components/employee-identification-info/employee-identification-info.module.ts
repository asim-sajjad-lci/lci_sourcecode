import { ReactiveFormsModule } from '@angular/forms';

import { AlertService } from './../../../_sharedresource/_services/alert.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';


import { DateTimePickerModule } from '../../../../../node_modules/ng-pick-datetime';
import { NgxMyDatePickerModule } from '../../../../../node_modules/ngx-mydatepicker/dist/ngx-my-date-picker.module';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect/dropdown/dropdown.module';
import { EmployeeIdentificationInfoComponent } from './employee-identification-info.component';





@NgModule({
  imports: [
    CommonModule,
  
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    ReactiveFormsModule,
    NgxMyDatePickerModule.forRoot(),
    DateTimePickerModule,
    
    AngularMultiSelectModule,
    MultiselectDropdownModule,

  ],
  exports: [EmployeeIdentificationInfoComponent],
  providers:[AlertService],
  
  declarations: [EmployeeIdentificationInfoComponent]
})
export class EmployeeIdentificationInfoModule { }
