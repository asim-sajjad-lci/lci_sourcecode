import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// import { SharedModule } from '../../../shared/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { TypeaheadModule } from '../../../../../node_modules/ngx-bootstrap';
// import { Ng2AutoComplete } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { BenefitCodeSetuptRoutes } from './benefit-code-setup.routing';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { BenefitCodeSetupComponent } from './benefit-code-setup.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BenefitCodeSetuptRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
  ],
  providers:[AlertService],
  declarations: [BenefitCodeSetupComponent]
})
export class BenefitCodeSetupModule { }
