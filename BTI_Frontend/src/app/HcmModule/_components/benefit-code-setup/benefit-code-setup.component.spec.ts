import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitCodeSetupComponent } from './benefit-code-setup.component';

describe('BenefitCodeSetupComponent', () => {
  let component: BenefitCodeSetupComponent;
  let fixture: ComponentFixture<BenefitCodeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitCodeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitCodeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
