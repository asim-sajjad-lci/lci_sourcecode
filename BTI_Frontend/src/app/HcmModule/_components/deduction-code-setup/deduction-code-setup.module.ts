import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { DeductionCodeSetupRoutes } from './deduction-code-setup.routing';
import { DeductionCodeSetupComponent } from './deduction-code-setup.component';







@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DeductionCodeSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
  ],

  providers:[AlertService],
  declarations: [DeductionCodeSetupComponent]
})
export class DeductionCodeSetupModule { }
