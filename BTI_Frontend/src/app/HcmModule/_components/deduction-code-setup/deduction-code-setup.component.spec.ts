import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeductionCodeSetupComponent } from './deduction-code-setup.component';

describe('DeductionCodeSetupComponent', () => {
  let component: DeductionCodeSetupComponent;
  let fixture: ComponentFixture<DeductionCodeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeductionCodeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeductionCodeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
