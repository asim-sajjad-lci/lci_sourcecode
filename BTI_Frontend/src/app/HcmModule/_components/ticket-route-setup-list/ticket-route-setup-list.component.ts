import { Component, OnInit, ViewChild } from '@angular/core';
import { Page } from 'app/_sharedresource/page';
import { Router } from '@angular/router';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { TicketRouteSetupService } from 'app/HcmModule/_services/ticket-route-setup/ticket-route-setup.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { constants } from 'ngx-chips/dist/modules/core';
import { Constants } from 'app/_sharedresource/Constants';

@Component({
  selector: 'app-ticket-route-setup-list',
  templateUrl: './ticket-route-setup-list.component.html',
  styleUrls: ['./ticket-route-setup-list.component.css'],
  providers:[CommonService,TicketRouteSetupService]
})
export class TicketRouteSetupListComponent implements OnInit {

  page = new Page();
  rows = {};
  selected = [];

  moduleCode = "M-1011";
  screenCode = "S-1790";
  moduleName;
  screenName;
  currentLanguage: any;
  defaultFormValues: object[];
  searchKeyword = '';
  ticketRouteId;
  ddPageSize: number = 5;

  isDeleteAction: boolean = false;
  isConfirmationModalOpen: boolean = false;

  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;

  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };

  showCreateForm: boolean = false;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;

  TICKETROUTE_CodeId: any;
  TICKETROUTE_DESCRIPTION: any;
  TICKETROUTE_SEARCH: any;
  TICKETROUTE_ARABIC_DESCRIPTION: any;
  TICKETROUTE_CREATE_LABEL: any;
  TICKETROUTE_SAVE_LABEL: any;
  TICKETROUTE_CLEAR_LABEL: any;
  TICKETROUTE_CANCEL_LABEL: any;
  TICKETROUTE_UPDATE_LABEL: any;
  TICKETROUTE_DELETE_LABEL: any;
  TICKETROUTE_CREATE_FORM_LABEL: any;
  TICKETROUTE_UPDATE_FORM_LABEL: any;
  TICKETROUTE_TICKETROUTE_LIST: any;
  createbtnText=Constants.createButtonText;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  
  constructor(private router: Router,
    private commonService: CommonService,
    private service: TicketRouteSetupService) {

      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortOn = 'id';
      this.page.sortBy = 'DESC';
      
    this.defaultFormValues = [
      { 'fieldName': 'TICKETROUTE_CodeId', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      { 'fieldName': 'TICKETROUTE_TICKETROUTE_LIST', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
      /*{ 'fieldName': 'MANAGE_USER_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':'' },
      { 'fieldName': 'MANAGE_USER_TABLE_ACCESS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TEXT_ADD_NEW_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_STATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_CITY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_POSTALCODE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},
      { 'fieldName': 'MANAGE_USER_TABLE_DOB', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' , 'isMandatory':''},*/
  ];
  this.TICKETROUTE_CodeId = this.defaultFormValues[0];
  this.TICKETROUTE_DESCRIPTION = this.defaultFormValues[1];
  this.TICKETROUTE_SEARCH = this.defaultFormValues[2];
  this.TICKETROUTE_ARABIC_DESCRIPTION = this.defaultFormValues[3];
  this.TICKETROUTE_CREATE_LABEL = this.defaultFormValues[4];
  this.TICKETROUTE_SAVE_LABEL = this.defaultFormValues[5];
  this.TICKETROUTE_CLEAR_LABEL = this.defaultFormValues[6];
  this.TICKETROUTE_CANCEL_LABEL = this.defaultFormValues[7];
  this.TICKETROUTE_UPDATE_LABEL = this.defaultFormValues[8];
  this.TICKETROUTE_DELETE_LABEL = this.defaultFormValues[9];
  this.TICKETROUTE_CREATE_FORM_LABEL = this.defaultFormValues[10];
  this.TICKETROUTE_UPDATE_FORM_LABEL = this.defaultFormValues[11];
  this.TICKETROUTE_TICKETROUTE_LIST = this.defaultFormValues[12];



   }

  ngOnInit() {

    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);


  }

  goToCreate(){
    this.router.navigate(['createTicketRouteSetup']);   
  }

  //setting pagination
  setPage(pageInfo) {
    debugger;
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
        this.page.sortOn = this.page.sortOn;
    } else {
        this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
        this.page.sortBy = this.page.sortBy;
    } else {
        this.page.sortBy = pageInfo.sortBy;
    }

    this.page.searchKeyword = '';
    this.service.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        debugger;
        this.page = pagedData.page;
        this.rows = pagedData.data;
    });
}

  // search department by keyword 
  updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
    // this.departmentService.searchDepartmentlist(this.page, this.searchKeyword).subscribe(pagedData => {
    //     this.page = pagedData.page;
    //     this.rows = pagedData.data;
    //     this.table.offset = 0;
    // });
}

edit(event) {
   
   
  if(event.cellIndex!=0 && event.cellIndex!=1){
      this.ticketRouteId = event.row.id;
    
      var myurl = `${'createTicketRouteSetup'}/${this.ticketRouteId}`;
      this.router.navigateByUrl(myurl);
  }
}

changePageSize(event) {
  this.page.size = event.target.value;
  this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

varifyDelete() {
  debugger;
  if (this.selected.length > 0) {
    //  this.showCreateForm = false;
      this.isDeleteAction = true;
      this.isConfirmationModalOpen = true;
      this.confirmationModalBody = this.deleteConfirmationText;

  } else {
      this.isSuccessMsg = false;
      this.hasMessage = true;
      this.message.type = 'error';
      this.isfailureMsg = true;
      this.showMsg = true;
      this.message.text = 'Please select at least one record to delete.';
      window.scrollTo(0, 0);
  }
}



//delete department by passing whole object of perticular Department
delete() {
  debugger;
  var selectedDepartments = [];
  for (var i = 0; i < this.selected.length; i++) {
      selectedDepartments.push(this.selected[i].id);
  }
  this.service.deleteTicketRoute(selectedDepartments).then(data => {
      var datacode = data.code;
      if (datacode == 200) {
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
      }
      this.hasMessage = true;
      if(datacode == "302"){
          this.message.type = "error";
          this.isSuccessMsg = false;
          this.isfailureMsg = true;
      } else {
          this.isSuccessMsg = true;
          this.message.type = "success";
          this.isfailureMsg = false;
      }
      //this.message.text = data.btiMessage.message;

      window.scrollTo(0, 0);
      window.setTimeout(() => {
          this.showMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMessage = false;
          }, 4000);
          this.message.text = data.btiMessage.message;
      }, 100);

      //Refresh the Grid data after deletion of department
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

  }).catch(error => {
      this.hasMessage = true;
      this.message.type = "error";
      var errorCode = error.status;
      this.message.text = "Server issue. Please contact admin.";
  });
  this.closeModal();
}

closeModal() {
  this.isDeleteAction = false;
  this.isConfirmationModalOpen = false;
}

EscapeModal(event){
      
  var key = event.key;
  if(key=="Escape"){
      this.closeModal();
  }
}

}



