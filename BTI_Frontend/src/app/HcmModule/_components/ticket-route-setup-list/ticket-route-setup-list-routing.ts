import { Routes } from '@angular/router';

import { TicketRouteSetupListComponent } from './ticket-route-setup-list.component';




export const TicketRouteSetupListRoutes: Routes = [{
  path: '',
  component: TicketRouteSetupListComponent,
  data: {
    breadcrumb: ""
  }
}];