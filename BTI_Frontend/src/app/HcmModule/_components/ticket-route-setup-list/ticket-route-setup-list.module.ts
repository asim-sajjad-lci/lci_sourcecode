import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { TicketRouteSetupListRoutes } from './ticket-route-setup-list-routing';
import { TicketRouteSetupListComponent } from './ticket-route-setup-list.component';






@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TicketRouteSetupListRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
  ],

  providers:[AlertService],
  declarations: [TicketRouteSetupListComponent]
})
export class TicketRouteSetupListModule { }
