import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketRouteSetupListComponent } from './ticket-route-setup-list.component';

describe('TicketRouteSetupListComponent', () => {
  let component: TicketRouteSetupListComponent;
  let fixture: ComponentFixture<TicketRouteSetupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketRouteSetupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketRouteSetupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
