import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { EmployeeMasterRoutes } from './employee-master.routing';
import { EmployeeMasterComponent } from './employee-master.component';







@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EmployeeMasterRoutes),
    SharedModule,
    NgxDatatableModule,
    Ng2AutoCompleteModule,

    
  ],

  
  declarations: [EmployeeMasterComponent]
})
export class EmployeeMasterModule { }
