import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Constants } from '../../../_sharedresource/Constants';
import { Router } from '@angular/router';

import { EmployeeMasterService } from '../../_services/employee-master/employee-master.service';
import { AlertService } from '../../../_sharedresource/_services/alert.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';

import { SupervisorService } from '../../_services/supervisor/supervisor.service';


import { PositionSetupService } from '../../_services/position-setup/position-setup.service';
import { LocationService } from '../../_services/location/location.service';
import { UploadFileService } from '../../../_sharedresource/_services/upload-file.service';
import { Page } from '../../../_sharedresource/page';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { DatePipe } from '@angular/common';

import * as $ from 'jquery';
import { EmployeeMiscellaneousMaster, EmployeeMaster, EmployeeMiscellaneousMasterSave } from 'app/HcmModule/_models/employee-master/employee-master';
import { DateValidator } from 'app/_sharedresource/directive/date-validator.directive';
import { MiscellaneousSetupService } from 'app/HcmModule/_services/miscellaneous-setup/miscellaneous-setup.service';
import { DivionService } from 'app/HcmModule/_services/division/division.service';
import { DepartmentSetupService } from 'app/HcmModule/_services/department-setup/department-setup.service';
import { ProjectSetupService } from 'app/HcmModule/_services/project-setup/project-setup.service';
import { toHijri, toGregorian } from "hijri-converter";
@Component({
  selector: 'app-create-employee-master-setup',
  templateUrl: './create-employee-master-setup.component.html',
  styleUrls: ['./create-employee-master-setup.component.css'],
  
  providers: [
    EmployeeMasterService,
    SupervisorService,
    DivionService,
    DepartmentSetupService,
    ProjectSetupService,
    PositionSetupService,
    LocationService,
    CommonService,
    DatePipe,
    DateValidator,
    MiscellaneousSetupService,
    AlertService,
    UploadFileService
]
})
export class CreateEmployeeMasterSetupComponent implements OnInit {

  page = new Page();
  page2 = new Page();
  valRows = new Array<EmployeeMiscellaneousMaster>();
  rows = new Array<EmployeeMaster>();
  selectedData = new Array<EmployeeMiscellaneousMasterSave>();
  temp = new Array<EmployeeMaster>();
  supervisorDescription: string;
  locationDescription: string;
  positionDescription: string;
  departmentDescription: string;
  projectDescription: string;
  divisionDescription: string;
  selected = [];
  moduleCode = 'M-1011';
  screenCode = 'S-1445';
  moduleName;
  screenName;
  defaultFormValues: object[];
  availableFormValues: [object];
  hasMessage;
  duplicateWarning;
  message = { 'type': '', 'text': '' };
  supervisorId = {};
  // model: EmployeeMaster;
  model: any={};
  searchKeyword = '';

  showCreateForm: boolean = true;
  isSuccessMsg: boolean;
  isfailureMsg: boolean;
  isUnderUpdate: boolean;
  hasMsg = false;
  showMsg = false;
  messageText: string;
  isConfirmationModalOpen: boolean = false;


  currentLanguage: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.confirmationModalBody;
  deleteConfirmationText = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction: boolean = false;
  supervisorIdvalue: string;
  getDivision: any[] = [];
  getDepartment: any[] = [];
  getProject: any[] = [];
  getPosition: any[] = [];
  getLocation: any[] = [];
  getEmployeeUserIdInSystem: any[] = [];
  getEmployeeNationalities: any[] = [];
  getEmployeeAddressMaster: any[] = [];

  divisionIdList: Observable<any>;
  departmentIdList: Observable<any>;
  projectIdList: Observable<any>;
  positionIdList: Observable<any>;
  locationIdList: Observable<any>;
  employeeUserIdInSystemList: Observable<any>;
  employeeNationalitiesIdList: Observable<any>;
  employeeAddressIndexIdList: Observable<any>;
  superviserIdList: Observable<any>;
  employeeIdList: Observable<any>;
  getSuperviser: any[] = [];
  getEmployee: any[] = [];
  typeaheadLoading: boolean;
  typeaheadNoResults: boolean;

  empList=[];

//   Gender List
GenderList=[{id:0,itemName:"N/A"},{id:1,itemName:"Male"},{id:2,itemName:"Female"}]

  //Date
  modelEmployeeHireDate;
  frmEmployeeHireDate;
  modelEmployeeAdjustHireDate;
  frmEmployeeAdjustHireDate;
  modelEmployeeLastWorkDate;
  frmEmployeeLastWorkDate;
  modelEmployeeInactiveDate;
  frmEmployeeInactiveDate;
  modelEmployeeBirthDate;
  frmEmployeeBirthDate;
  modelexpire;
  frmEmployeeexpire;
  modelexpires;
  frmEmployeeexpires;
  url = '';
  fileName: string;
  isSelected: boolean = false;
  fileSelect: any;
  uploadAttachfile;
  AddressDetails;
  ddPageSize = 5;
  ddPageSize2 = 5;

  DivisionId = null;
  DepartmentId = null;
  ProjectId = null;
  PositionId = null;
  LocationId = null;
  SuperVisionCode = null;
  SystemUserId = null;
  EmployeeNationalityId = null;
  addressId = null;
  employeeId = null;
  employeeIndexId = null;
  employeeIdValue = null;
  employeeIndexIdValue = null;

  gridLists;
  getScreenDetailArr;
  isConfigure = false;
  isShowHideGrid = false;
  erroremployeeId: any = { isError: false, errorMessage: '' };
  erroremployeeHireDate: any = { isError: false, errorMessage: '' };
  erroremployeeAdjustHireDate: any = { isError: false, errorMessage: '' };
  erroremployeeLastWorkDate: any = { isError: false, errorMessage: '' };
  erroremployeeInactiveDate: any = { isError: false, errorMessage: '' };
  erroremployeeMaritalStatus: any = { isError: false, errorMessage: '' };
  errorUpload: any = { isError: false, errorMessage: '' };

  //for print functionalities
  printDetails: boolean = true;


  NationalityDescription;
  employeeHireDate;
  employeeAdjustHireDate;
  employeeLastDayDate;
  constAddressDetails = {
      "address1": "",
      "address2": "",
      "addressArabic1": "",
      "addressArabic2": "",
      "addressId": "",
      "associateMessage": "",
      "businessEmail": "",
      "businessPhone": "",
      "employeeAddressIndexId": "",
      "locationLinkGoogleMap": "",
      "message": "",
      "messageType": "",
      "otherPhone": "",
      "pOBox": "",
      "pageNumber": "",
      "pageSize": "",
      "personalEmail": "",
      "personalPhone": "",
      "postCode": "",
      "city": '',
      "country": ''
  };
  arrMaritalStatus: any[] = ['N/A','Single','Married','Divorcee','Widow'];
  arrGender: any[] = ['N/A','Male','Female'];
  arremployeeType: any[] = ['N/A','Full Time Regular','Full Time Temp','Part Time Regular','Part Time Temp','Inter','Other'];

empId;
isValueDisable = true;
  constructor(private router: Router,
      private employeeMasterService: EmployeeMasterService,
      private getScreenDetailService: GetScreenDetailService,
      private alertService: AlertService,
      private supervisorService: SupervisorService,
      private divisionService: DivionService,
      private departmentService: DepartmentSetupService,
      private projectService: ProjectSetupService,
      private positionSetupService: PositionSetupService,
      private locationService: LocationService,
      private uploadFileService: UploadFileService,
      private commonService: CommonService,
      private MiscellaneousService : MiscellaneousSetupService,
      private datePipe: DatePipe) {

      this.page.pageNumber = 0;
      this.page.size = 5;
      this.page.sortOn = 'id';
      this.page.sortBy = 'DESC';

      this.page2.pageNumber = 0;
      this.page2.size = 5;
      this.page2.sortOn = 'id';
      this.page2.sortBy = 'DESC';
      // default form parameter for department  screen
      this.defaultFormValues = [
          //0
          { 'fieldName': 'EMPLOYEE_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'TITLE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'TITLE_ARABIC', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'LAST_NAME', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'FIRST_NAME', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'LAST_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'FIRST_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'MIDDLE_NAME_ARABIC', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'HIRE_DATE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'ADJUSTED_HIRE_DATE', 'fieldValue': '', 'helpMessage': '' },
          //11
          { 'fieldName': 'LAST_DAY_WORKED', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DATE_INACTIVE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'REASON', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'ADDRESS_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'ADDRESS1', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'ADDRESS2', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PERSONAL_EMAIL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'BUSINESS_EMAIL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'COUNTRY', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'CITY', 'fieldValue': '', 'helpMessage': '' },
          //21
          { 'fieldName': 'PERSONAL_PHONE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'BUSINESS_PHONE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'EXT', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DIVISION_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'DEPARTMENT_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'POSITION_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'LOCATION_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'SUPERVISOR_ID', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'EMPLOYMENT_TYPE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'GENDER', 'fieldValue': '', 'helpMessage': '' },
          //31
          { 'fieldName': 'MARITAL_STATUS', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'BIRTH_DATE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'SYSTEM_USER', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'WORK_HOURS_YEARLY', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'EMPLOYEE_CITIZEN', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'EMPLOYEE_SMOKER', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'INACTIVE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'EMPLOYEE_IMMIGRATION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'IMAGE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'NATIONALITY', 'fieldValue': '', 'helpMessage': '' },
          //41
          { 'fieldName': 'DELETE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'PRINT', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'CLEAR', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'UPLOAD', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'ACTION', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'CREATE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'UPDATE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'SEARCH', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'STATUSEMP', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'CANCEL', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'HIJRIDATE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'EXPIREHIJRIDATE', 'fieldValue': '', 'helpMessage': '' },
          { 'fieldName': 'EXPIRESHIJRIDATE', 'fieldValue': '', 'helpMessage': '' }
      ];

      this.employeeIdList = Observable.create((observer: any) => {
          observer.next(this.model.employeeId);
      }).mergeMap((token: string) => this.getEmployeeIdAsObservable(token));

      this.superviserIdList = Observable.create((observer: any) => {
          observer.next(this.model.supervisorId);
      }).mergeMap((token: string) => this.getSuperviserIdAsObservable(token));

      this.divisionIdList = Observable.create((observer: any) => {
          observer.next(this.model.divisionId);
      }).mergeMap((token: string) => this.getDivisionAsObservable(token));

      this.departmentIdList = Observable.create((observer: any) => {
          observer.next(this.model.departmentId);
      }).mergeMap((token: string) => this.getDepartmentIdAsObservable(token));

      this.projectIdList = Observable.create((observer: any) => {
          observer.next(this.model.projectId);
      }).mergeMap((token: string) => this.getProjectIdAsObservable(token));

      this.positionIdList = Observable.create((observer: any) => {
          observer.next(this.model.positionId);
      }).mergeMap((token: string) => this.getPositionAsObservable(token));

      this.locationIdList = Observable.create((observer: any) => {
          observer.next(this.model.locationId);
      }).mergeMap((token: string) => this.getLocationAsObservable(token));

      this.employeeUserIdInSystemList = Observable.create((observer: any) => {
          observer.next(this.model.employeeUserIdInSystem);
      }).mergeMap((token: string) => this.getEmployeeUserIdInSystemAsObservable(token));

      this.employeeNationalitiesIdList = Observable.create((observer: any) => {
          observer.next(this.model.employeeNationalitiesId);
      }).mergeMap((token: string) => this.getEmployeeNationalitiesAsObservable(token));

      this.employeeAddressIndexIdList = Observable.create((observer: any) => {
          observer.next(this.model.employeeAddressIndexId);
      }).mergeMap((token: string) => this.getEmployeeAddressMasterAsObservable(token));
  }

  ngOnInit() {
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
     
      this.currentLanguage = localStorage.getItem('currentLanguage');
      this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

      this.AddressDetails = {
          "address1": "",
          "address2": "",
          "addressArabic1": "",
          "addressArabic2": "",
          "addressId": "",
          "associateMessage": "",
          "businessEmail": "",
          "businessPhone": "",
          "employeeAddressIndexId": "",
          "locationLinkGoogleMap": "",
          "message": "",
          "messageType": "",
          "otherPhone": "",
          "pOBox": "",
          "pageNumber": "",
          "pageSize": "",
          "personalEmail": "",
          "personalPhone": "",
          "postCode": "",
          "city": '',
          "country": ''
      }

      this.supervisorService.getEmployee().then(data => {
          this.getEmployee = data.result;
      });

      this.employeeMasterService.getAllEmployeeAddressMasterDropDown().then(data => {
          this.getEmployeeAddressMaster = data.result;
      });

      this.employeeMasterService.getAllEmployeeNationalitiesDropDown().then(data => {
          this.getEmployeeNationalities = data.result;
      });

      this.employeeMasterService.getAllUserDetailDropDown().then(data => {
          this.getEmployeeUserIdInSystem = data.result;
      });

      //Following shifted from SetPage for better performance
      this.departmentService.getDepartments().then(data => {
          this.getDepartment = data.result;
      });

      this.projectService.getAllProject().then(data => {
         // debugger;
          this.getProject = data.result;
      });


  }

  // Open form for create department
  Create() {
      this.employeeMasterService.getEmployee().then(data => {
          this.getEmployee = data.result;
      });
      window.scrollTo(0, 0);
      this.modelEmployeeBirthDate = null;
      this.modelexpire = null;
      this.modelexpires = null;
      this.modelEmployeeHireDate = null;
      this.modelEmployeeAdjustHireDate = null;
      this.modelEmployeeLastWorkDate = null;
      this.modelEmployeeInactiveDate = null;
      this.url = null;
      this.showCreateForm = false;
      this.isUnderUpdate = false;
      this.erroremployeeId = { isError: false, errorMessage: '' };
      this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
      this.erroremployeeHireDate = { isError: false, errorMessage: '' };
      this.employeeHireDate = undefined;
      this.employeeAdjustHireDate = undefined;
      this.employeeLastDayDate = undefined;
      this.AddressDetails = this.constAddressDetails;
      this.empId = 0;
      setTimeout(() => {
          this.showCreateForm = true;
          setTimeout(() => {
              window.scrollTo(0, 500);
          }, 10);
      }, 10);
      this.model = {
          "employeeIndexId": 0,
          "employeeId": "",
          "employeeTitle": "",
          "employeeLastName": "",
          "employeeFirstName": "",
          "employeeMiddleName": "",
          "employeeLastNameArabic": "",
          "employeeFirstNameArabic": "",
          "employeeMiddleNameArabic": "",
          "employeeHireDate": null,
          "employeeAdjustHireDate": null,
          "employeeLastWorkDate": null,
          "employeeInactiveDate": null,
          "employeeInactiveReason": "",
          "employeeType": 0,
          "employeeGender": '',
          "employeeMaritalStatus": '',
          "employeeBirthDate": null,
          "employeeUserIdInSystem": null,
          "employeeWorkHourYearly": null,
          "employeeCitizen": null,
          "employeeInactive": null,
          "employeeImmigration": null,
          "divisionId": null,
          "departmentId": null,
          "projectId": null,
          "locationId": null,
          "supervisorId": null,
          "positionId": null,
          "employeeNationalitiesId": null,
          "employeeAddressIndexId": null,
          "employeeTitleArabic": "",
          "employeeSmoker": null,
          "idNumber": null,
          "expire": null,
          "passportNumber": null,
          "expires": null,
          "employeeBirthDateH": null,
          "expireHijri": null,
          "expiresHijri": null

      };

      this.DivisionId = null;
      this.DepartmentId = null;
      this.PositionId = null;
      this.LocationId = null;
      this.SuperVisionCode = null;
      this.EmployeeNationalityId = null;
      this.addressId = null;
  }

  //setting pagination
  setPage(pageInfo) {
      this.selected = []; // remove any selected checkbox on paging
      // this.page.pageNumber = pageInfo.offset;
      // if (pageInfo.sortOn == undefined) {
      //     this.page.sortOn = this.page.sortOn;
      // } else {
      //     this.page.sortOn = pageInfo.sortOn;
      // }
      // if (pageInfo.sortBy == undefined) {
      //     this.page.sortBy = this.page.sortBy;
      // } else {
      //     this.page.sortBy = pageInfo.sortBy;
      // }
      // this.page.searchKeyword = '';
      this.employeeMasterService.getEmployee().then(pagedData => {
          debugger
         // this.page = pagedData.page;
          this.rows = pagedData.result;
          this.empList=[];

          for(let i=0;i<this.rows.length;i++)
          {
            this.empList.push({"id":this.rows[i].employeeIndexId,"itemName":this.rows[i].employeeId});
          }
      });
  }

  setPage2(pageInfo) {

      debugger;
      this.selectedData =[];
      this.selected = []; // remove any selected checkbox on paging
      this.page2.pageNumber = pageInfo.offset;
      if (pageInfo.sortOn == undefined) {
          this.page2.sortOn = this.page2.sortOn;
      } else {
          this.page2.sortOn = pageInfo.sortOn;
      }
      if (pageInfo.sortBy == undefined) {
          this.page2.sortBy = this.page2.sortBy;
      } else {
          this.page2.sortBy = pageInfo.sortBy;
      }
      this.page2.searchKeyword = '';
      this.MiscellaneousService.getAllMiscellaneousForEmploye(this.page2, this.empId).subscribe(pagedData => {
          debugger
          this.page2 = pagedData.page;
          this.valRows = pagedData.data;
          this.valRows = [...this.valRows];

          for(let i=0;i<this.valRows.length;i++)
          {
              if(this.valRows[i].masterId != 0)
              {
                  let val = new EmployeeMiscellaneousMasterSave (this.valRows[i].id,+this.valRows[i].valueIndexId,this.empId,this.valRows[i].masterId,false);
                  this.selectedData.push(val);
              }
          }

         
          // this.valRows = pagedData.result.records;
          // this.page2.pageNumber = pagedData.pageNumber;
          // this.page2.size = pagedData.pageSize;
          setTimeout(() => {
              for (let i = 0; i < this.valRows.length; i++) {
                  if(this.valRows[i].masterId == 0)
                  {
                      const element = document.getElementsByClassName('custom-' + i);
                      for (let j = 0; j < element.length; j++) {
      
                          (<HTMLInputElement>element[j]).setAttribute("disabled", "");
                          (<HTMLInputElement>element[j]).value = this.valRows[i].valueIndexId;
                      }
                  }
                  else{
                      const element = document.getElementsByClassName('custom-' + i);
                      for (let j = 0; j < element.length; j++) {
      
                          // (<HTMLInputElement>element[j]).setAttribute("disabled", "");
                          (<HTMLInputElement>element[j]).value = this.valRows[i].valueIndexId;
                      }
                  }

              }
          }, 250);
      });
  }


  // default list on page
  onSelect({ selected }) {
      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
  }

  onSelect2(event,row) {
      debugger;
      console.log("Selected",this.selectedData)
      if(event.target.checked == false)
      {
          let j = row.$$index;

          const element = document.getElementsByClassName('custom-' + j);
          for (let i = 0; i < element.length; i++) {
              (<HTMLInputElement>element[i]).setAttribute("disabled", "");
              //(<HTMLInputElement>element[i]).removeAttribute("disabled");
              // (<HTMLInputElement>element[i]).focus();// note the type assertion on the element
          }

          let del;
          if(row.masterId){
          for(let i=0;i<this.selectedData.length;i++)
          {
              if(this.selectedData[i].id == row.masterId)
              {
                  del = i;
              }
          }
          // this.selectedData.splice(del,1);
          this.selectedData[del].isDeleted = true;
      }
      }

      else{
          console.log("Selected",this.selectedData)
          // let len = event.selected.length;
          // let i = event.selected[len - 1].$$index;
          let i = row.$$index;
  
          const element = document.getElementsByClassName('custom-' + i);
          for (let i = 0; i < element.length; i++) {
  
              (<HTMLInputElement>element[i]).removeAttribute("disabled");
              // (<HTMLInputElement>element[i]).focus();// note the type assertion on the element
          }
      }
      

  }

  changeValue(row,value){
      debugger;
      console.log("Row",row);
      let val = new EmployeeMiscellaneousMasterSave (row.id,value,this.empId,0,false);
      this.selectedData.push(val);
  }

  Misc(){
      this.valRows=[...this.valRows]
      this.setPage2({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  // search department by keyword 
  updateFilter(event) {
      this.searchKeyword = event.target.value.toLowerCase();
      this.page.pageNumber = 0;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

  }

  // Set default page size
  changePageSize(event) {
      this.page.size = event.target.value;
      this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
  }

  changeEmployeeMaritalStatus(e) {
      //console.log(e.target.value);
      if (e.target.value == 4) {
          this.erroremployeeMaritalStatus = { isError: true, errorMessage: 'Invalid Marital Status.' };
      } else {
          this.erroremployeeMaritalStatus = { isError: false, errorMessage: '' };
      }

  }

  getEmployeeIdAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'i');
      return Observable.of(
          this.getEmployee.filter((id: any) => {
              return query.test(id.employeeId);
          })
      );
  }
  getSuperviserIdAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      console.log(Observable.of(
          this.getSuperviser.filter((id: any) => {
              return query.test(id.superVisionCode);
          })
      ));
      return Observable.of(
          this.getSuperviser.filter((id: any) => {
              return query.test(id.superVisionCode);
          })
      );
  }
  getDivisionAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      console.log(query);
      return Observable.of(
          this.getDivision.filter((id: any) => {
              return query.test(id.divisionId);
          })
      );
  }

  getDepartmentIdAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      //console.log(query);
      return Observable.of(
          this.getDepartment.filter((id: any) => {
              return query.test(id.departmentId);
          })
      );
  }

  getProjectIdAsObservable(token: string): Observable<any> {
     // debugger
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      //console.log(query);
      return Observable.of(
          this.getProject.filter((id: any) => {
              return query.test(id.projectId);
          })
      );
  }

  getPositionAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      //console.log(query);
      return Observable.of(
          this.getPosition.filter((id: any) => {
              return query.test(id.positionId);
          })
      );
  }

  getLocationAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      //console.log(query);
      return Observable.of(
          this.getLocation.filter((id: any) => {
              return query.test(id.locationId);
          })
      );
  }

  getEmployeeUserIdInSystemAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      //console.log(query);
      return Observable.of(
          this.getEmployeeUserIdInSystem.filter((id: any) => {
              return query.test(id.udId);
          })
      );
  }

  getEmployeeNationalitiesAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      //console.log(query);
      return Observable.of(
          this.getEmployeeNationalities.filter((id: any) => {
              return query.test(id.employeeNationalityId);
          })
      );
  }

  getEmployeeAddressMasterAsObservable(token: string): Observable<any> {
      token = token.replace(/\\/g, "\\\\");
      let query = new RegExp(token, 'ig');
      //console.log(this.getEmployeeAddressMaster);
      console.log(Observable.of(
          this.getEmployeeAddressMaster.filter((id: any) => {
              return query.test(id.addressId);
          })));
      return Observable.of(
          this.getEmployeeAddressMaster.filter((id: any) => {
              return query.test(id.addressId);
          })
      );
  }



  changeTypeaheadLoading(e: boolean): void {
      this.typeaheadLoading = e;
      //this.model.employeeFirstName = '';
      this.employeeId = 0;
      //console.log(e);
  }

  validateEmployee(){

      this.divisionService.getDivisionClassList().then(data => {
          this.getDivision = data.result;
      });

      this.departmentService.getDepartments().then(data => {
          this.getDepartment = data.result;
      });

      this.positionSetupService.getAllPostionDropDownList().then(data => {
          this.getPosition = data.result;
      });

      this.locationService.getAllLocationDropDownId().then(data => {
          this.getLocation = data.result;
          //console.log(this.getLocation);
      });

      this.employeeMasterService.getSuperviserId().then(data => {
          this.getSuperviser = data.result;
          //console.log(this.getSuperviser);
      });
      // this.employeeMasterService.validateEmployee(this.model.employeeId).then(data => {
      //     if (data.result.isRepeat) {
      //         this.erroremployeeId = { isError: true, errorMessage: 'Employee already exists!' };
      //     }else{
      //         this.erroremployeeId = { isError: false, errorMessage: '' };
      //     }
      // });
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
      console.log('Selected value: ', e.item);
      // this.employeeMasterService.validateEmployee(this.model.employeeId).then(data => {
      //     if (data.result.isRepeat) {
      //         this.erroremployeeId = { isError: true, errorMessage: 'Employee already exists!' };
      //     }else{
      //         this.erroremployeeId = { isError: false, errorMessage: '' };
      //     }
      // });
      if (typeof (e.item.employeeFirstName) != 'undefined')
          this.employeeId = e.item.id;

      this.employeeMasterService.getEmployeeDetailsByEmployeeId(e.item.employeeIndexId).then(data => {
          console.log("DATA",data);
          if(data.result != null){
              this.model.employeeFirstName = data.result.employeeFirstName;
              this.model.employeeLastName = data.result.employeeLastName;
              this.model.employeeFirstNameArabic = data.result.employeeFirstNameArabic;
              this.model.employeeLastNameArabic = data.result.employeeLastNameArabic;
              this.model.employeeMiddleName = data.result.employeeMiddleName;
              this.model.employeeMiddleNameArabic = data.result.employeeMiddleNameArabic;
              this.model.employeeTitle = data.result.employeeTitle;
              this.model.employeeTitleArabic = data.result.employeeTitleArabic;
              this.model.divisionId = data.result.division_Id;
              this.model.departmentId = data.result.department_Id;
              this.model.projectId = data.result.project_Id;
              this.model.positionId = data.result.position_Id;
              this.model.locationId = data.result.dtoLocation.locationId;
              this.model.supervisorId = data.result.dtoSupervisor.superVisionCode;
              this.modelEmployeeHireDate = this.formatDateFordatePicker(data.result.employeeHireDate != null ? new Date(data.result.employeeHireDate) : null);
              this.modelEmployeeAdjustHireDate = this.formatDateFordatePicker(data.result.employeeAdjustHireDate != null ? new Date(data.result.employeeAdjustHireDate) : null);
              this.modelEmployeeLastWorkDate = this.formatDateFordatePicker(data.result.employeeLastWorkDate != null ? new Date(data.result.employeeLastWorkDate) : null);
              this.model.employeeAddressIndexId = data.result.dtoEmployeeAddressMaster.addressId;
              this.AddressDetails = data.result.dtoEmployeeAddressMaster;
              this.supervisorDescription = data.result.dtoSupervisor.description;
              this.locationDescription = data.result.dtoLocation.description;
              this.positionDescription = data.result.dtoPosition.description;
              this.departmentDescription = data.result.dtoDepartment.departmentDescription;
              this.divisionDescription = data.result.dtoDivision.divisionDescription;
              /*this.model.divisionId = data.result.divisionId;
              this.model.departmentId = data.result.departmentId;
              this.model.positionId = data.result.positionId;
              this.model.locationId = data.result.locationId;
              this.model.supervisorId = data.result.supervisorId;*/
              this.DivisionId = data.result.divisionId;
              this.DepartmentId = data.result.departmentId;
              this.ProjectId = data.result.projectId;
              this.PositionId = data.result.positionId;
              this.LocationId = data.result.locationId;
              this.SuperVisionCode = data.result.supervisorId;
              this.SystemUserId = data.result.employeeUserIdInSystem;
              this.EmployeeNationalityId = data.result.employeeNationalitiesId;
              this.addressId = data.result.dtoEmployeeAddressMaster.employeeAddressIndexId;
              this.model.employeeNationalitiesId = data.result.dtoEmployeeNationalities.employeeNationalityId;
              this.model.employeeAddressIndexId = data.result.dtoEmployeeAddressMaster.addressId;
              this.modelEmployeeBirthDate = this.formatDateFordatePicker(data.result.employeeBirthDate != null ? new Date(data.result.employeeBirthDate) : null);
              this.modelexpire = this.formatDateFordatePicker(data.result.expire != null ? new Date(data.result.expire) : null);
              this.modelexpires = this.formatDateFordatePicker(data.result.expires != null ? new Date(data.result.expires) : null);
              this.model.employeeUserIdInSystem = data.result.employeeUserIdInSystem;
              this.model.employeeType = data.result.employeeType;
              this.model.employeeImmigration = data.result.employeeImmigration;
              this.model.passportNumber = data.result.passportNumber;
              this.model.employeeMaritalStatus = data.result.employeeMaritalStatus;
              this.frmEmployeeHireDate = data.result.employeeHireDate;
              this.frmEmployeeAdjustHireDate = data.result.employeeAdjustHireDate;
              this.frmEmployeeLastWorkDate = data.result.employeeLastWorkDate;
              this.frmEmployeeBirthDate = data.result.employeeBirthDate;
              this.frmEmployeeexpire = data.result.expire;
              this.frmEmployeeexpires = data.result.expires;
              this.NationalityDescription = data.result.dtoEmployeeNationalities.employeeNationalityDescription;
              if (data.result && !data.result.employeeInactive) {
                  this.modelEmployeeInactiveDate = null;
                  this.model.employeeInactiveReason = '';
              } else {
                  this.modelEmployeeInactiveDate = this.formatDateFordatePicker(new Date(data.result.employeeInactiveDate));
                  this.model.employeeInactiveReason = data.result.employeeInactiveReason;
                  //this.frmEmployeeInactiveDate = data.result.employeeInactiveDate;
              }
              this.supervisorService.getUserDetailByUserId(data.result.employeeUserIdInSystem).then(sdata => {
                  //console.log(data);
                  if(sdata.result != "undefined")
                  this.model.employeeUserIdInSystem = sdata.result.firstName;
              });
              this.model.employeeGender = data.result.employeeGender;
              this.model.employeeCitizen = data.result.employeeCitizen;
              this.model.employeeSmoker = data.result.employeeSmoker;
              this.model.employeeWorkHourYearly = data.result.employeeWorkHourYearly;
              this.model.idNumber = data.result.idNumber;
          }
      });
  }

  typeaheadOnSelectDivisionId(e: TypeaheadMatch): void {
      //console.log('typeaheadOnSelectDivisionId: ', e.item);
      if (typeof (e.item.id) != 'undefined')
          this.DivisionId = e.item.id;
      this.divisionDescription = e.item.divisionDescription;
  }
  typeaheadOnSelectDepartmentId(e: TypeaheadMatch): void {
      debugger;
      //console.log('typeaheadOnSelectDepartmentId: ', e.item);
      if (typeof (e.item.id) != 'undefined')
          this.DepartmentId = e.item.id;
      this.departmentDescription = e.item.departmentDescription;
  }

  typeaheadOnSelectProjectId(e: TypeaheadMatch): void {
      debugger;
      //console.log('typeaheadOnSelectDepartmentId: ', e.item);
      if (typeof (e.item.id) != 'undefined')
          this.ProjectId = e.item.id;
        
      this.projectDescription = e.item.projectDescription;
  }

  typeaheadOnSelectPositionId(e: TypeaheadMatch): void {
      //console.log('typeaheadOnSelectPositionId: ', e.item);
      if (typeof (e.item.id) != 'undefined')
          this.PositionId = e.item.id;
      this.positionDescription = e.item.description;

  }

  typeaheadOnSelectLocationId(e: TypeaheadMatch): void {
      //console.log('typeaheadOnSelectLocationId: ', e.item);
      if (typeof (e.item.id) != 'undefined')
          this.LocationId = e.item.id;
      this.locationDescription = e.item.description;
  }

  typeaheadOnSelectSuperVisionCode(e: TypeaheadMatch): void {
      //console.log('typeaheadOnSelectSuperVisionCode: ', e.item);
      if (typeof (e.item.id) != 'undefined')
          this.SuperVisionCode = e.item.id;
      this.supervisorDescription = e.item.description;
      //console.log('supervisor', this.SuperVisionCode);
  }

  typeaheadOnSelectSystemUserId(e: TypeaheadMatch): void {
      //console.log('typeaheadOnSelectSystemUserId: ', e.item);
      if (typeof (e.item.firstName) != 'undefined')
          this.SystemUserId = e.item.udId;
  }

  typeaheadOnSelectEmployeeNationalityId(e: TypeaheadMatch): void {
      //console.log('typeaheadOnSelectEmployeeNationalityId: ', e.item);
      if (typeof (e.item.employeeNationalityIndexId) != 'undefined')
          this.EmployeeNationalityId = e.item.employeeNationalityIndexId;
      this.NationalityDescription = e.item.employeeNationalityDescription;
  }

  typeaheadOnSelectEmployeeaddressId(e: TypeaheadMatch): void {
      //console.log('typeaheadOnSelectEmployeeaddressId: ', e.item);
      if (typeof (e.item.employeeAddressIndexId) != 'undefined') {
          this.addressId = e.item.employeeAddressIndexId;
          this.AddressDetails = Object.assign({}, e.item);
          //console.log(this.addressId);
      }
  }

  // Clear form to reset to default blank
  Clear(f: NgForm) {
      f.resetForm({ 'employeeGender': '', 'employeeType': 0, 'employeeMaritalStatus': '' });
      this.erroremployeeId = { isError: false, errorMessage: '' };
      this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
      this.erroremployeeHireDate = { isError: false, errorMessage: '' };
      this.employeeHireDate = undefined;
      this.employeeAdjustHireDate = undefined;
      this.employeeLastDayDate = undefined;
      this.AddressDetails = this.constAddressDetails;
  }



  //function call for creating new location
  CreateEmployeeMaster(f: NgForm, event: Event) {
      debugger;
      //console.log(this.model);
      //console.log(this.DivisionId);
      //console.log(this.DepartmentId);
      //console.log(this.PositionId);
      //console.log(this.LocationId);
      //console.log(this.SuperVisionCode);
      //console.log(this.EmployeeNationalityId);
      //console.log(this.addressId);
      if (!this.DivisionId ||
          !this.DepartmentId ||
          !this.PositionId ||
          !this.ProjectId ||
          !this.LocationId ||
          !this.SuperVisionCode ||
          !this.EmployeeNationalityId ||
          !this.addressId
      ) {
          //console.log('INvalide selection')
          return false;
      }
      var supIdx = this.model.employeeId;
      this.model.employeeInactive = this.model.employeeInactive ? 1 : 0;
      this.model.employeeCitizen = this.model.employeeCitizen ? 1 : 0;
      this.model.employeeSmoker = this.model.employeeSmoker ? 1 : 0;
      this.model.employeeImmigration = this.model.employeeImmigration ? 1 : 0;
      this.model.divisionId = this.DivisionId ? this.DivisionId : null;
      this.model.departmentId = this.DepartmentId ? this.DepartmentId : null;
      this.model.projectId = this.ProjectId ? this.ProjectId : null;
      this.model.positionId = this.PositionId ? this.PositionId : null;
      this.model.locationId = this.LocationId ? this.LocationId : null;
      this.model.supervisorId = this.SuperVisionCode ? this.SuperVisionCode : null;
      this.model.employeeUserIdInSystem = this.SystemUserId ? this.SystemUserId : null;
      this.model.employeeNationalitiesId = this.EmployeeNationalityId ? this.EmployeeNationalityId : null;
      this.model.employeeAddressIndexId = this.addressId ? this.addressId : null;
      this.model.employeeId = this.employeeId ? this.employeeId : this.model.employeeId;
      this.model.employeeHireDate = this.frmEmployeeHireDate;
      this.model.employeeAdjustHireDate = this.frmEmployeeAdjustHireDate;
      this.model.employeeLastWorkDate = this.frmEmployeeLastWorkDate;
      this.model.employeeBirthDate = this.frmEmployeeBirthDate;
      this.model.expire = this.frmEmployeeexpire;
      this.model.expires = this.frmEmployeeexpires;
      this.model.employeeInactiveDate = this.frmEmployeeInactiveDate;
      event.preventDefault();

      if (this.model.employeeIndexId > 0 && this.model.employeeIndexId != 0 && this.model.employeeIndexId != undefined) {
          //Check if the id is available in the model.
          //If id avalable then update the location, else Add new location.

          this.isConfirmationModalOpen = true;
          this.isDeleteAction = false;
      }
      else {
          //Check for duplicate Location Id according to it create new location
          this.employeeMasterService.validateEmployee(supIdx).then(response => {
              if (response && response.result.isRepeat) {
                  this.duplicateWarning = true;
                  this.message.type = 'success';
                  window.scroll(0,500) 
                  window.setTimeout(() => {
                      this.isSuccessMsg = false;
                      this.isfailureMsg = true;
                      this.showMsg = true;
                      window.setTimeout(() => {
                          this.showMsg = false;
                          this.duplicateWarning = false;
                      }, 4000);
                      this.message.text = response.btiMessage.message;
                  }, 100);
              } else {
                  //Call service api for Creating new location


                  this.employeeMasterService.createEmployeeMasterAttachment(this.model, this.uploadAttachfile).then(data => {
                      debugger;
                      var datacode = data.code;
                      this.empId = data.result.employeeIndexId;
                      this.isValueDisable = false;

                      if (datacode == 201) {
                          window.scrollTo(0, 0);
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                             // this.showCreateForm = false;
                              this.messageText = data.btiMessage.message;
                              ;
                          }, 100);


                          //Refresh the Grid data after deletion of division
                          //this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
                          this.hasMsg = true;
                          f.resetForm();

                      }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = 'Server error. Please contact admin !';
                      }, 100)
                  });
              }
          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = 'Server error. Please contact admin !';
              }, 100)
          });
      }
  }

  getEmployeeDetail(event) {
      if (event.target.value != "") {
          if (isNaN(event.target.value) == true) {
              this.model.employeeId = '';
              return false;
          } else {
              this.employeeMasterService.getEmployeeDetail(event.target.value).then(response => {
                  var datacode = response.code;
                  if (datacode == 404) {
                      this.hasMessage = true;
                      this.message.type = 'success';
                      window.scrollTo(0, 0);
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMessage = false;
                          }, 4000);
                          this.message.text = response.btiMessage.message + ' !';
                      }, 100);
                      this.model.employeeFirstName = '';
                  } else {
                      this.model.employeeFirstName = response.result.records.firstName + " " + response.result.records.lastName;
                  }
              }).catch(error => {
                  this.hasMessage = true;
                  this.message.type = 'error';
                  var errorCode = error.status;
                  this.message.text = 'Server issue. Please contact admin !';
              });
          }
      } else {
          this.model.employeeFirstName = '';
      }
  }

  myOptions: INgxMyDpOptions = {
      // other options...
      dateFormat: 'dd-mm-yyyy',
  };

  formatDateFordatePicker(strDate: any): any {
      if (strDate != null) {
          var setDate = new Date(strDate);
          return { date: { year: setDate.getFullYear(), month: setDate.getMonth() + 1, day: setDate.getDate() } };
      } else {
          return null;
      }
  }

  onEmployeeHireDateChanged(event: IMyDateModel): void {
      this.frmEmployeeHireDate = event.jsdate;

      this.employeeHireDate = event.epoc;

      /*if((this.model.employeeAdjustHireDate < this.model.employeeHireDate) && this.model.employeeAdjustHireDate!=undefined){
          this.erroremployeeHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
      }else{
          this.erroremployeeHireDate={isError:false,errorMessage:''};
      }
      if((this.model.employeeLastWorkDate < this.model.employeeHireDate) && this.model.employeeLastWorkDate!=undefined){
          this.erroremployeeHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
      }else{
          this.erroremployeeHireDate={isError:false,errorMessage:''};
      }*/

      if ((this.employeeHireDate > this.employeeLastDayDate) && this.employeeLastDayDate != undefined) {
          this.erroremployeeHireDate = { isError: true, errorMessage: 'Invalid Hire Date.' };
      }
      if (this.employeeHireDate <= this.employeeLastDayDate) {
          this.erroremployeeHireDate = { isError: false, errorMessage: '' };
      }
      if (this.employeeLastDayDate == undefined && this.employeeHireDate == undefined) {
          this.erroremployeeHireDate = { isError: false, errorMessage: '' };
      }
  }

  onEmployeeAdjustHireDateChanged(event: IMyDateModel): void {
      this.frmEmployeeAdjustHireDate = event.jsdate;
      this.employeeAdjustHireDate = event.epoc;

      /*if((this.model.employeeHireDate > this.model.employeeAdjustHireDate) && this.model.employeeAdjustHireDate!=undefined){
          this.erroremployeeAdjustHireDate={isError:true,errorMessage:'Invalid Adjust Hire Date.'};
      }else if(this.model.employeeHireDate <= this.model.employeeAdjustHireDate){
          this.erroremployeeAdjustHireDate={isError:false,errorMessage:''};
      }else if(this.model.employeeAdjustHireDate==undefined && this.model.employeeHireDate==undefined){
     this.erroremployeeAdjustHireDate={isError:false,errorMessage:''};
      }
      if((this.model.employeeAdjustHireDate < this.model.employeeLastWorkDate) && this.model.employeeLastWorkDate!=undefined){
          this.erroremployeeAdjustHireDate={isError:true,errorMessage:'Invalid Adjust Hire Date.'};
      }else{
          this.erroremployeeAdjustHireDate={isError:false,errorMessage:''};
      }
      if((this.model.employeeAdjustHireDate < this.model.employeeHireDate) && this.model.employeeAdjustHireDate!=undefined){
          this.erroremployeeHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
      }else{
          this.erroremployeeHireDate={isError:false,errorMessage:''};
      }
      if((this.model.employeeLastWorkDate < this.model.employeeHireDate) && this.model.employeeLastWorkDate!=undefined){
          this.erroremployeeHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
      }else{
          this.erroremployeeHireDate={isError:false,errorMessage:''};
      }*/
      if ((this.employeeAdjustHireDate > this.employeeLastDayDate) && this.employeeLastDayDate != undefined) {
          this.erroremployeeAdjustHireDate = { isError: true, errorMessage: 'Invalid Adjust Hire Date.' };
      }
      if (this.employeeAdjustHireDate <= this.employeeLastDayDate) {
          this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
      }
      if (this.employeeAdjustHireDate == undefined && this.employeeLastDayDate == undefined) {
          this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
      }
  }

  onEmployeeLastWorkDateChanged(event: IMyDateModel): void {
      this.frmEmployeeLastWorkDate = event.jsdate;
      this.employeeLastDayDate = event.epoc;
      /*if((this.model.employeeAdjustHireDate > this.model.employeeLastWorkDate) && this.model.employeeLastWorkDate!=undefined){
          this.erroremployeeLastWorkDate={isError:true,errorMessage:'Invalid Last Work Date.'};
      }else if((this.model.employeeHireDate > this.model.employeeLastWorkDate) && this.model.employeeLastWorkDate!=undefined){
          this.erroremployeeLastWorkDate={isError:true,errorMessage:'Invalid Last Work Date.'};
      }else if(this.model.employeeAdjustHireDate <= this.model.employeeLastWorkDate){
          this.erroremployeeLastWorkDate={isError:false,errorMessage:''};
      }else if(this.model.employeeLastWorkDate==undefined && this.model.employeeAdjustHireDate==undefined){
     this.erroremployeeLastWorkDate={isError:false,errorMessage:''};
      }

      if((this.model.employeeAdjustHireDate < this.model.employeeHireDate) && this.model.employeeAdjustHireDate!=undefined){
          this.erroremployeeHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
      }else{
          this.erroremployeeHireDate={isError:false,errorMessage:''};
      }
      if((this.model.employeeLastWorkDate < this.model.employeeHireDate) && this.model.employeeLastWorkDate!=undefined){
          this.erroremployeeHireDate={isError:true,errorMessage:'Invalid Hire Date.'};
      }else{
          this.erroremployeeHireDate={isError:false,errorMessage:''};
      }*/
      if (this.employeeLastDayDate == 0) {
          this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
          this.erroremployeeHireDate = { isError: false, errorMessage: '' };
          this.employeeLastDayDate = undefined;
          return;
      }

      if ((this.employeeAdjustHireDate > this.employeeLastDayDate) && this.employeeAdjustHireDate != undefined) {
          this.erroremployeeAdjustHireDate = { isError: true, errorMessage: 'Invalid Adjust Hire Date.' };
      }
      if (this.employeeAdjustHireDate <= this.employeeLastDayDate) {
          this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
      }
      if (this.employeeAdjustHireDate == undefined && this.employeeLastDayDate == undefined) {
          this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
      }

      if ((this.employeeHireDate > this.employeeLastDayDate) && this.employeeHireDate != undefined) {
          this.erroremployeeHireDate = { isError: true, errorMessage: 'Invalid Hire Date.' };
      }
      if (this.employeeHireDate <= this.employeeLastDayDate) {
          this.erroremployeeHireDate = { isError: false, errorMessage: '' };
      }
      if (this.employeeLastDayDate == undefined && this.employeeHireDate == undefined) {
          this.erroremployeeHireDate = { isError: false, errorMessage: '' };
      }
  }
  onEmployeeInactiveDateChangedd(event: IMyDateModel): void {
      this.frmEmployeeInactiveDate = event.jsdate;
      this.model.employeeInactiveDate = event.epoc;

      if ((this.modelEmployeeLastWorkDate.epoc > this.model.employeeInactiveDate) && this.model.employeeInactiveDate != undefined) {
          this.erroremployeeInactiveDate = { isError: true, errorMessage: 'Invalid End Date.' };
      }
      if (this.modelEmployeeLastWorkDate.epoc <= this.model.employeeInactiveDate) {
          this.erroremployeeInactiveDate = { isError: false, errorMessage: '' };
      }
      if (this.model.employeeInactiveDate == undefined && this.modelEmployeeLastWorkDate.epoc == undefined) {
          this.erroremployeeInactiveDate = { isError: false, errorMessage: '' };
      }
  }

  onEmployeeBirthDateChangedd(event: IMyDateModel): void {
      this.frmEmployeeBirthDate = event.jsdate;
      this.model.employeeBirthDate = event.epoc;
      console.log(event);
      if (event.date.year != 0 || event.date.month != 0 ||event.date.day != 0  ) {
          let hijriDate = toHijri(
              event.date.year,
              event.date.month,
              event.date.day);
          let hijriDateHM = this.zeroPrefix(hijriDate.hm);
          let hijriDateHD= this.zeroPrefix(hijriDate.hd) ;
          this.model.employeeBirthDateH = hijriDateHD + '/' + hijriDateHM + '/' + hijriDate.hy;
      }else{
          this.model.employeeBirthDateH= '';
      }
  }

  zeroPrefix(val: number): string{
      if(val < 10){return "0"+val }
      return ''+val;
  }

  onEmployeeBirthDateHijriChangedd() {
      let date = this.model.employeeBirthDateH.split('/');
      console.log(date);
      let gregorian = toGregorian(+date[2], +date[1], +date[0]);
      console.log(gregorian);
      console.log(new Date(gregorian.gy, (gregorian.gm - 1), gregorian.gd));
      let fullDate = new Date(gregorian.gy, (gregorian.gm - 1), gregorian.gd);
      console.log(fullDate.valueOf());

      this.frmEmployeeBirthDate =  fullDate;
      this.modelEmployeeBirthDate = { 
          "date": { "year": gregorian.gy, "month": gregorian.gm, "day": gregorian.gd },
          "jsdate": fullDate,
          "formatted": gregorian.gd.toString()+'-'+gregorian.gm.toString()+'-'+gregorian.gy.toString(), "epoc": fullDate.valueOf() 
      }
      this.model.employeeBirthDate = fullDate.valueOf();
  }

  onexpireChangedd(event: IMyDateModel): void {
      this.frmEmployeeexpire = event.jsdate;
      this.model.expire = event.epoc;
      console.log(event);
      if (event.date.year != 0 || event.date.month != 0 ||event.date.day != 0  ) {
          let hijriDate = toHijri(
              event.date.year,
              event.date.month,
              event.date.day);
          
          let hijriDateHM = this.zeroPrefix(hijriDate.hm);
          let hijriDateHD= this.zeroPrefix(hijriDate.hd) ;
          console.log(hijriDate);
          this.model.expireHijri = hijriDateHD + '/' + hijriDateHM + '/' + hijriDate.hy;
      }else{
          this.model.expireHijri= '';
      }
  }

  onexpireHijriChangedd() {
      let date = this.model.expireHijri.split('/');
      console.log(date);
      let gregorian = toGregorian(+date[2], +date[1], +date[0]);
      console.log(gregorian);
      console.log(new Date(gregorian.gy, (gregorian.gm - 1), gregorian.gd));
      let fullDate = new Date(gregorian.gy, (gregorian.gm - 1), gregorian.gd);
      console.log(fullDate.valueOf());

      this.frmEmployeeexpire =  fullDate;
      this.modelexpire = { 
          "date": { "year": gregorian.gy, "month": gregorian.gm, "day": gregorian.gd },
          "jsdate": fullDate,
          "formatted": gregorian.gd.toString()+'-'+gregorian.gm.toString()+'-'+gregorian.gy.toString(), "epoc": fullDate.valueOf() 
      }
      this.model.expire = fullDate.valueOf();
  }

  onexpiresHijriChangedd(){
      let date = this.model.expiresHijri.split('/');
      console.log(date);
      let gregorian = toGregorian(+date[2], +date[1], +date[0]);
      console.log(gregorian);
      console.log(new Date(gregorian.gy, (gregorian.gm - 1), gregorian.gd));
      let fullDate = new Date(gregorian.gy, (gregorian.gm - 1), gregorian.gd);
      console.log(fullDate.valueOf());

      this.frmEmployeeexpires =  fullDate;
      this.modelexpires = { 
          "date": { "year": gregorian.gy, "month": gregorian.gm, "day": gregorian.gd },
          "jsdate": fullDate,
          "formatted": gregorian.gd.toString()+'-'+gregorian.gm.toString()+'-'+gregorian.gy.toString(), "epoc": fullDate.valueOf() 
      }
      this.model.expires = fullDate.valueOf();
  }

  onexpiresChangedd(event: IMyDateModel): void {
      this.frmEmployeeexpires = event.jsdate;
      this.model.expire = event.epoc;
      console.log(event);
      if (event.date.year != 0 || event.date.month != 0 ||event.date.day != 0  ) {
          let hijriDate = toHijri(
              event.date.year,
              event.date.month,
              event.date.day);
          
          let hijriDateHM = this.zeroPrefix(hijriDate.hm);
          let hijriDateHD= this.zeroPrefix(hijriDate.hd) ;
          console.log(hijriDate);
          this.model.expiresHijri = hijriDateHD + '/' + hijriDateHM + '/' + hijriDate.hy;
      }else{
          this.model.expiresHijri= '';
      }
  }

  //edit department by row
  edit(row: EmployeeMaster) {

      debugger
      console.log(row);
      /*for(var i=0;i<this.getEmployeeAddressMaster.length;i++){
          if(this.getEmployeeAddressMaster[i].)
      }*/
      this.isValueDisable = false
      this.empId = row.employeeIndexId;
      this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
      this.erroremployeeHireDate = { isError: false, errorMessage: '' };
      this.DivisionId = row.divisionId;
      this.DepartmentId = row.departmentId;
      this.ProjectId = row.projectId;
      this.PositionId = row.positionId;
      this.LocationId = row.locationId;
      this.SuperVisionCode = row.supervisorId;
      this.SystemUserId = row.employeeUserIdInSystem;
      this.EmployeeNationalityId = row.employeeNationalitiesId;
      this.addressId = row['dtoEmployeeAddressMaster'].employeeAddressIndexId;
      this.employeeId = row.employeeId;
      this.AddressDetails = row['dtoEmployeeAddressMaster'];
      this.modelEmployeeBirthDate = this.formatDateFordatePicker(row.employeeBirthDate != null ? new Date(row.employeeBirthDate) : null);
      this.modelexpire = this.formatDateFordatePicker(row.expire != null ? new Date(row.expire) : null);
      this.modelexpires = this.formatDateFordatePicker(row.expires != null ? new Date(row.expires) : null);
      this.modelEmployeeHireDate = this.formatDateFordatePicker(row.employeeHireDate != null ? new Date(row.employeeHireDate) : null);
      this.modelEmployeeAdjustHireDate = this.formatDateFordatePicker(row.employeeAdjustHireDate != null ? new Date(row.employeeAdjustHireDate) : null);
      this.modelEmployeeLastWorkDate = this.formatDateFordatePicker(row.employeeLastWorkDate != null ? new Date(row.employeeLastWorkDate) : null);
      this.frmEmployeeHireDate = row.employeeHireDate;
      this.frmEmployeeAdjustHireDate = row.employeeAdjustHireDate;
      this.frmEmployeeLastWorkDate = row.employeeLastWorkDate;
      this.frmEmployeeBirthDate = row.employeeBirthDate;
      this.frmEmployeeexpire = row.expire;
      this.frmEmployeeexpires = row.expires;
      this.NationalityDescription = row['dtoEmployeeNationalities'].employeeNationalityDescription;
      if (row['employeeImage'] != null) {
          this.url = "data:image/png;base64," + row['employeeImage'];
      }
      else {
          this.url = '/assets/img/no-profile-pic-tiny.png';
      }
      this.showCreateForm = true;
      this.model = Object.assign({}, row);

      this.model.divisionId = row['division_Id'];
      this.model.departmentId = row['department_Id'];
      this.model.projectId = row['project_Id'];
      this.model.positionId = row['position_Id'];
      this.model.locationId = row['dtoLocation']['locationId'];
      this.model.supervisorId = row['dtoSupervisor']['superVisionCode'];
      this.supervisorDescription = row['dtoSupervisor']['description'];
      this.locationDescription = row['dtoLocation']['description'];
      this.positionDescription = row['dtoPosition']['description'];
      this.departmentDescription = row['dtoDepartment']['departmentDescription'];
      this.divisionDescription = row['dtoDivision']['divisionDescription'];
      this.model.employeeNationalitiesId = row['dtoEmployeeNationalities']['employeeNationalityId'];
      this.model.employeeAddressIndexId = row['dtoEmployeeAddressMaster']['addressId'];
      this.model.employeeGender = row['employeeGender'].toString();

      if (row && !row['employeeInactive']) {
          this.modelEmployeeInactiveDate = null;
          this.model.employeeInactiveReason = '';
      } else {
          this.modelEmployeeInactiveDate = this.formatDateFordatePicker(new Date(row['employeeInactiveDate']));
          this.model.employeeInactiveReason = row['employeeInactiveReason'];
          this.frmEmployeeInactiveDate = row['employeeInactiveDate'];
      }
      this.supervisorService.getUserDetailByUserId(row['employeeUserIdInSystem']).then(data => {
          //console.log(data);
          this.model.employeeUserIdInSystem = data.result.firstName;
      });
      this.isUnderUpdate = true;
      this.employeeIndexId = row.employeeIndexId;
      this.employeeIndexIdValue = this.model.employeeIndexId;
      if(row['dtoProjectSetup'] != null)
      this.projectDescription = row['dtoProjectSetup']['projectDescription'];
      setTimeout(() => {
          window.scrollTo(0, 2000);
      }, 10);
  }

  dataURLtoFile(dataurl, filename) {
      console.log(dataurl);
      if (dataurl != "/assets/img/no-profile-pic-tiny.png") {
          var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
              bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
          while (n--) {
              u8arr[n] = bstr.charCodeAt(n);
          }
          return new File([u8arr], filename, { type: mime });
      }
      return new File([], filename, { type: mime });

  }

  updateStatus() {
      debugger;
      this.closeModal();
      //Call service api for updating selected department
      this.model.employeeIndexId = this.employeeIndexId;
      let UpdatePostData = {
          "employeeIndexId": this.model.employeeIndexId,
          "employeeId": this.model.employeeId,
          "employeeTitle": this.model.employeeTitle,
          "employeeTitleArabic": this.model.employeeTitleArabic,
          "employeeLastName": this.model.employeeLastName,
          "employeeFirstName": this.model.employeeFirstName,
          "employeeMiddleName": this.model.employeeMiddleName,
          "employeeLastNameArabic": this.model.employeeLastNameArabic,
          "employeeFirstNameArabic": this.model.employeeFirstNameArabic,
          "employeeMiddleNameArabic": this.model.employeeMiddleNameArabic,
          "employeeHireDate": this.model.employeeHireDate,
          "employeeAdjustHireDate": this.model.employeeAdjustHireDate,
          "employeeLastWorkDate": this.model.employeeLastWorkDate,
          "employeeInactiveDate": this.model.employeeInactiveDate,
          "employeeInactiveReason": this.model.employeeInactiveReason,
          "employeeType": this.model.employeeType,
          "employeeGender": this.model.employeeGender,
          "employeeMaritalStatus": this.model.employeeMaritalStatus,
          "employeeBirthDate": this.model.employeeBirthDate,
          "employeeUserIdInSystem": this.model.employeeUserIdInSystem,
          "employeeWorkHourYearly": this.model.employeeWorkHourYearly,
          "employeeCitizen": this.model.employeeCitizen,
          "employeeInactive": this.model.employeeInactive,
          "employeeImmigration": this.model.employeeImmigration,
          "divisionId": this.model.divisionId,
          "departmentId": this.model.departmentId,
          "projectId":this.model.projectId,
          "locationId": this.model.locationId,
          "supervisorId": this.model.supervisorId,
          "positionId": this.model.positionId,
          "employeeNationalitiesId": this.model.employeeNationalitiesId,
          "employeeAddressIndexId": this.model.employeeAddressIndexId,
          "employeeSmoker": this.model.employeeSmoker,
          "expire": this.model.expire,
          "expires": this.model.expires,
          "passportNumber": this.model.passportNumber,
          "idNumber": this.model.idNumber,
          "employeeBirthDateH": this.model.employeeBirthDateH,
          "expireHijri": this.model.expireHijri,
          "expiresHijri": this.model.expiresHijri
      }

      if ((!this.uploadAttachfile || !this.uploadAttachfile.name) && this.url) {
          this.uploadAttachfile = this.dataURLtoFile(this.url, 'a.png');
      }

      // console.log(this.uploadAttachfile);

      this.employeeMasterService.updateEmployeeMaster(UpdatePostData, this.uploadAttachfile).then(data => {
          var datacode = data.code;
          if (datacode == 201) {
              //Refresh the Grid data after editing department
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

              //Scroll to top after editing department
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                  }, 4000);
                  this.messageText = data.btiMessage.message;
                  this.showCreateForm = false;
                  this.modelEmployeeBirthDate = null;
                  this.modelexpire = null;
                  this.modelexpires = null;
                  this.modelEmployeeHireDate = null;
                  this.modelEmployeeAdjustHireDate = null;
                  this.modelEmployeeLastWorkDate = null;
                  this.modelEmployeeInactiveDate = null;
                  this.url = null;

                  this.isUnderUpdate = false;
                  this.erroremployeeId = { isError: false, errorMessage: '' };
                  this.erroremployeeAdjustHireDate = { isError: false, errorMessage: '' };
                  this.erroremployeeHireDate = { isError: false, errorMessage: '' };
                  this.employeeHireDate = undefined;
                  this.employeeAdjustHireDate = undefined;
                  this.employeeLastDayDate = undefined;
                  this.AddressDetails = this.constAddressDetails;

                  this.model = {
                      "employeeIndexId": 0,
                      "employeeId": "",
                      "employeeTitle": "",
                      "employeeLastName": "",
                      "employeeFirstName": "",
                      "employeeMiddleName": "",
                      "employeeLastNameArabic": "",
                      "employeeFirstNameArabic": "",
                      "employeeMiddleNameArabic": "",
                      "employeeHireDate": null,
                      "employeeAdjustHireDate": null,
                      "employeeLastWorkDate": null,
                      "employeeInactiveDate": null,
                      "employeeInactiveReason": "",
                      "employeeType": 0,
                      "employeeGender": '',
                      "employeeMaritalStatus": '',
                      "employeeBirthDate": null,
                      "employeeUserIdInSystem": null,
                      "employeeWorkHourYearly": null,
                      "employeeCitizen": null,
                      "employeeInactive": null,
                      "employeeImmigration": null,
                      "divisionId": null,
                      "departmentId": null,
                      "projectId": null,
                      "locationId": null,
                      "supervisorId": null,
                      "positionId": null,
                      "employeeNationalitiesId": null,
                      "employeeAddressIndexId": null,
                      "employeeTitleArabic": "",
                      "employeeSmoker": null,
                      "idNumber": null,
                      "expire": null,
                      "passportNumber": null,
                      "expires": null,
                      "employeeBirthDateH": null,
                      "expireHijri": null,
                      "expiresHijri": null

                  };

                  this.DivisionId = null;
                  this.DepartmentId = null;
                  this.ProjectId = null;
                  this.PositionId = null;
                  this.LocationId = null;
                  this.SuperVisionCode = null;
                  this.EmployeeNationalityId = null;
                  this.addressId = null;
                  this.url = null;
              }, 100);
              this.hasMessage = false;
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
          }
      }).catch(error => {
          this.hasMsg = true;
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = "Server error. Please contact admin.";
          }, 100)
      });
  }

  print() {

      let printContents, popupWin;
      printContents = document.getElementById('print-section').innerHTML;
      printContents = printContents.replace(/\r?\n|\r/g, "");
      printContents = printContents.replace(" ", "");
      printContents = printContents.replace(/<!--.*?-->/g, "");

      // var postedOnes = this.getElementsByIdStartsWith("print-section", "canvas", "canvasdiv-", printContents);
      // var postedOnes1 = this.getElementsByIdStartsWith("print-section", "canvas", "categorychart-", postedOnes);
      // this.getElementsByIdStartsWith("print-section", "canvas", "innerchart-", postedOnes1);

      printContents = document.getElementById('print-section').innerHTML;

      popupWin = window.open('', '_blank', 'top=0,left=0,height=400px,width=800px');
      popupWin.document.open();
      popupWin.document.write(`
<html>
  <head>
  <link rel="stylesheet" type="text/css" media="screen,print" href="assets/css/bootstrap.min.css">
    <title>Employee Master</title>
    <style>
      .print-tabel{disaplay:block}
    </style>
  </head>
<body onload="">${printContents}</body>
<script type="text/javascript">
window.print();
if(navigator.userAgent.match(/iPad/i)){
window.onfocus=function(){window.close(); }
}else{        
window.close();
}
</script>
</html>`
      );
      popupWin.document.close();
  }


  printEmployeeDetails() {
      this.printDetails = false;

      setTimeout(() => { this.print(); }, 100);
      setTimeout(() => { this.resetAfterPrint(); }, 100);

  }

  resetAfterPrint() {
      this.printDetails = true;
  }

  DeleteImage() {
      this.uploadAttachfile = '';
      this.url = '';
  }

  CheckIsInactive() {
      if (!this.model.employeeInactive) {
          this.modelEmployeeInactiveDate = null;
          this.model.employeeInactiveReason = '';
      }
  }

  onUploadBtn(event) {
      //console.log("aaaa:", event.target.files[0]);
      this.fileName = ""

      if (event.target.files && event.target.files[0]) {
          this.fileName = event.target.files[0].name;
          this.fileSelect = event.target.files[0];
          var reader = new FileReader();

          if (
              (this.fileSelect.type == "image/png" ||
                  this.fileSelect.type == "image/jpg" ||
                  this.fileSelect.type == "image/jpeg")
          ) {
              this.isSelected = false;
          } else {
              this.isSelected = false;
              this.errorUpload = { isError: true, errorMessage: 'Invalid File.' };
              return false;
          }

          if (event.target.files[0].size / 1048576 > 2) {
              this.errorUpload = { isError: true, errorMessage: 'File size exceeds 2 MB.' };
              return false;
          }

          this.uploadAttachfile = event.target.files[0];
          reader.readAsDataURL(event.target.files[0]); // read file as data url
          reader.onload = (event: any) => { // called once readAsDataURL is completed 
              this.url = event.target.result;
              //console.log(this.url);
          }
      }
  }

  varifyDelete() {
      if (this.selected.length > 0) {
          this.showCreateForm = false;
          this.isDeleteAction = true;
          this.isConfirmationModalOpen = true;
      } else {
          this.isSuccessMsg = false;
          this.hasMessage = true;
          this.message.type = 'error';
          this.isfailureMsg = true;
          this.showMsg = true;
          window.setTimeout(() => {
              this.showMsg = false;
              this.hasMessage = false;
          }, 4000);
          this.message.text = 'Please select at least one record to delete.';
          window.scrollTo(0, 0);
      }
  }

  //delete traningCourse by passing whole object of perticular TrainingCourse
  delete() {
      var selectedEmp = [];
      //console.log(this.selected);
      for (var i = 0; i < this.selected.length; i++) {
          selectedEmp.push(this.selected[i].employeeIndexId);
      }
      //console.log(selectedEmp);

      this.employeeMasterService.deleteEmployeeMaster(selectedEmp).then(data => {
          var datacode = data.code;
          if (datacode == 200) {

          }
          //Refresh the Grid data after deletion of division
          this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
          this.hasMessage = true;
          if(datacode == "302"){
              this.message.type = "error";
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
          } else {
              this.isSuccessMsg = true;
              this.message.type = "success";
              this.isfailureMsg = false;
          }
          //this.message.text = data.btiMessage.message;

          window.scrollTo(0, 0);
          window.setTimeout(() => {
              this.showMsg = true;
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMessage = false;
              }, 4000);
              this.message.text = data.btiMessage.message;
          }, 100);
      }).catch(error => {
          this.hasMessage = true;
          this.message.type = "error";
          var errorCode = error.status;
          this.message.text = "Server issue. Please contact admin.";
      });
      this.closeModal();
  }
  confirm(): void {
      this.messageText = 'Confirmed!';
      //this.modalRef.hide();
      this.delete();
  }

  closeModal() {
      this.isDeleteAction = false;
      this.isConfirmationModalOpen = false;
  }

  _keyPress(event: any) {
      const pattern = /[0-9\+\-\ ]/;
      let inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
          // invalid character, prevent input
          event.preventDefault();
      }
  }

  saveEmployeeMiscellaneousValues(){
      debugger;
      this.MiscellaneousService.createMiscellaneousForEmployees(this.selectedData).then(data => {
          var datacode = data.code;

          if (datacode == 201) {
              window.scrollTo(0, 0);
              window.setTimeout(() => {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                  }, 4000);
                  this.showCreateForm = false;
                  this.messageText = data.btiMessage.message;
                  ;
              }, 100);
              this.isValueDisable = true;
              this.selectedData = [];
              //Refresh the Grid data after deletion of division
              this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
              this.hasMsg = true;
             // f.resetForm();

          }
      }).catch(error => {
          this.hasMsg = true;
          window.setTimeout(() => {
              this.isSuccessMsg = false;
              this.isfailureMsg = true;
              this.showMsg = true;
              this.messageText = 'Server error. Please contact admin !';
          }, 100)
      });
  }

   // Set default page size
 changePageSize2(event) {
  this.page2.size = event.target.value;
  this.setPage2({ offset: 0, sortOn: this.page2.sortOn, sortBy: this.page2.sortBy });
}

}
