import { CreateEmployeeMasterSetupComponent } from './create-employee-master-setup.component';

import { Routes } from '@angular/router';




export const CreateEmployeeMasterSetupRoutes: Routes = [{
  path: '',
  component: CreateEmployeeMasterSetupComponent,
  data: {
    breadcrumb: ""
  }
}];


