import { ReactiveFormsModule } from '@angular/forms';

import { AlertService } from './../../../_sharedresource/_services/alert.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { CreateEmployeeMasterSetupComponent } from './create-employee-master-setup.component';
import { CreateEmployeeMasterSetupRoutes } from './create-employee-master-setup.routing';

import { DateTimePickerModule } from '../../../../../node_modules/ng-pick-datetime';
import { NgxMyDatePickerModule } from '../../../../../node_modules/ngx-mydatepicker/dist/ngx-my-date-picker.module';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect/dropdown/dropdown.module';
import { EmployeePersonalInfoModule } from '../employee-personal-info/employee-personal-info.module';
import { EmployeeIdentificationInfoModule } from '../employee-identification-info/employee-identification-info.module';
import { EmployeeBankInfoModule } from '../employee-bank-info/employee-bank-info.module';
import { EmployeeOrganizationInfoModule } from '../employee-organization-info/employee-organization-info.module';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateEmployeeMasterSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    ReactiveFormsModule,
    NgxMyDatePickerModule.forRoot(),
    DateTimePickerModule,
    EmployeePersonalInfoModule,
    EmployeeIdentificationInfoModule,
    EmployeeBankInfoModule,
    EmployeeOrganizationInfoModule,
    AngularMultiSelectModule,
    MultiselectDropdownModule,


  ],
  providers:[AlertService],
  
  declarations: [CreateEmployeeMasterSetupComponent]
})
export class CreateEmployeeMasterSetupModule { }
