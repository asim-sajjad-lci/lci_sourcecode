import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEmployeeMasterSetupComponent } from './create-employee-master-setup.component';

describe('CreateEmployeeMasterSetupComponent', () => {
  let component: CreateEmployeeMasterSetupComponent;
  let fixture: ComponentFixture<CreateEmployeeMasterSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEmployeeMasterSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEmployeeMasterSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
