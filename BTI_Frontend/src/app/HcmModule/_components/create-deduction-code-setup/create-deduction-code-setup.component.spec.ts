import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDeductionCodeSetupComponent } from './create-deduction-code-setup.component';

describe('CreateDeductionCodeSetupComponent', () => {
  let component: CreateDeductionCodeSetupComponent;
  let fixture: ComponentFixture<CreateDeductionCodeSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDeductionCodeSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDeductionCodeSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
