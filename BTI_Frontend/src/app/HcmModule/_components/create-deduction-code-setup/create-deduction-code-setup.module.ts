import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
// import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';\
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import { CreateDeductionCodeSetupComponent } from './create-deduction-code-setup.component';
import { CreateDeductionCodeSetupRoutes } from './create-deduction-code-setup.routing';
import { SharedModule } from 'app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { TypeaheadModule } from 'ngx-bootstrap';
// import { Ng2AutoCompleteModule } from 'ng2-auto-complete/dist/ng2-auto-complete.module';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { RouterModule } from '@angular/router';
import { AlertService } from './../../../_sharedresource/_services/alert.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    AngularMultiSelectModule,
    RouterModule.forChild(CreateDeductionCodeSetupRoutes),
    NgxMyDatePickerModule.forRoot(),
  ],
  providers:[AlertService],
  declarations: [CreateDeductionCodeSetupComponent]
})
export class CreateDeductionCodeSetupModule { }


