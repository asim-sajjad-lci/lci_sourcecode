import { CreateDeductionCodeSetupComponent } from './create-deduction-code-setup.component';


import { Routes } from '@angular/router';




export const CreateDeductionCodeSetupRoutes: Routes = [{
  path: '',
  component: CreateDeductionCodeSetupComponent,
  data: {
    breadcrumb: ""
  }
}];
