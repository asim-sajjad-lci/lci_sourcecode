import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeBankInfoComponent } from './employee-bank-info.component';

describe('EmployeeBankInfoComponent', () => {
  let component: EmployeeBankInfoComponent;
  let fixture: ComponentFixture<EmployeeBankInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeBankInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeBankInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
