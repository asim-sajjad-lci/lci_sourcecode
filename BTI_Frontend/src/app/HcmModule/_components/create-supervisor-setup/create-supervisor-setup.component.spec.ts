import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSupervisorSetupComponent } from './create-supervisor-setup.component';

describe('CreateSupervisorSetupComponent', () => {
  let component: CreateSupervisorSetupComponent;
  let fixture: ComponentFixture<CreateSupervisorSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSupervisorSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSupervisorSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
