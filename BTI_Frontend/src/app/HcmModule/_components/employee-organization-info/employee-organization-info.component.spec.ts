import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeOrganizationInfoComponent } from './employee-organization-info.component';

describe('EmployeeOrganizationInfoComponent', () => {
  let component: EmployeeOrganizationInfoComponent;
  let fixture: ComponentFixture<EmployeeOrganizationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeOrganizationInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeOrganizationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
