import { Routes } from '@angular/router';
import { TicketClassSetupListComponent } from './ticket-class-setup-list.component';


export const TicketClassSetupListRoutes: Routes = [{
  path: '',
  component: TicketClassSetupListComponent,
  data: {
    breadcrumb: ""
  }
}];