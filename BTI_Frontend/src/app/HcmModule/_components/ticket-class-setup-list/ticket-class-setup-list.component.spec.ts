import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketClassSetupListComponent } from './ticket-class-setup-list.component';

describe('TicketClassSetupListComponent', () => {
  let component: TicketClassSetupListComponent;
  let fixture: ComponentFixture<TicketClassSetupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketClassSetupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketClassSetupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
