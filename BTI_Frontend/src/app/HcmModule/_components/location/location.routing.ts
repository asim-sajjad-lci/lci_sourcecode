import { LocationComponent } from './location.component';

import { Routes } from '@angular/router';




export const LocationRoutes: Routes = [{
  path: '',
  component: LocationComponent,
  data: {
    breadcrumb: ""
  }
}];
