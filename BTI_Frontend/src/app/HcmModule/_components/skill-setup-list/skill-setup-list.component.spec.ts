import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillSetupListComponent } from './skill-setup-list.component';

describe('SkillSetupListComponent', () => {
  let component: SkillSetupListComponent;
  let fixture: ComponentFixture<SkillSetupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillSetupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillSetupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
