
import { Routes } from '@angular/router';
import { SkillSetupListComponent } from './skill-setup-list.component';




export const SkillSetupListRoutes: Routes = [{
  path: '',
  component: SkillSetupListComponent,
  data: {
    breadcrumb: ""
  }
}];