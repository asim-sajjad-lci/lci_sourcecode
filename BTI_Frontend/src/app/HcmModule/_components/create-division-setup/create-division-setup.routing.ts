import { CreateDivisionSetupComponent } from './create-division-setup.component';

import { Routes } from '@angular/router';




export const CreateDivisionSetupRoutes: Routes = [{
  path: '',
  component: CreateDivisionSetupComponent,
  data: {
    breadcrumb: ""
  }
}];
