import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDivisionSetupComponent } from './create-division-setup.component';

describe('CreateDivisionSetupComponent', () => {
  let component: CreateDivisionSetupComponent;
  let fixture: ComponentFixture<CreateDivisionSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDivisionSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDivisionSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
