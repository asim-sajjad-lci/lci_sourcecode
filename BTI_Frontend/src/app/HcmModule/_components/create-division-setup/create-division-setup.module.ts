import { ReactiveFormsModule } from '@angular/forms';
import { CreateDivisionSetupComponent } from './create-division-setup.component';
import { AlertService } from './../../../_sharedresource/_services/alert.service';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { CreateDivisionSetupRoutes } from './create-division-setup.routing';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateDivisionSetupRoutes),
    SharedModule,
    NgxDatatableModule,
    TypeaheadModule,
    Ng2AutoCompleteModule,
    ReactiveFormsModule,
    

  ],
  providers:[AlertService],
  
  declarations: [CreateDivisionSetupComponent]
})
export class CreateDivisionSetupModule { }
