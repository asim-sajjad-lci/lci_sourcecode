
import { Division } from './../../_models/division/division';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Page } from 'app/_sharedresource/page';
import { DivionService } from 'app/HcmModule/_services/division/division.service';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import { Constants } from 'app/_sharedresource/Constants';
import { Observable } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { AlertService } from 'app/_sharedresource/_services/alert.service';
import { NgForm,FormBuilder, FormGroup,FormControl, Validators  } from '@angular/forms';
import { DropDownModule } from 'app/_sharedresource/_modules/drop-down.module';
import { TypeaheadMatch } from 'ngx-bootstrap';
import {  Input, forwardRef, HostListener, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {  ViewChildren } from '@angular/core';

@Component({
  selector: 'app-create-division-setup',
  templateUrl: './create-division-setup.component.html',
  styleUrls: ['./create-division-setup.component.css'],
  providers: [DivionService,CommonService]
})
export class CreateDivisionSetupComponent implements OnInit {
// export to make it available for other classes

@ViewChild('filterInput') filterInput: ElementRef;
@ViewChild('filterInput1') filterInput1: ElementRef;
@ViewChild('filterInput2') filterInput2: ElementRef;
@ViewChild('filterInput3') filterInput3: ElementRef;
@ViewChildren('listItems') listItems: QueryList<ElementRef>;
// registerForm: FormGroup;
// submitted = false;

get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
  }
  _value: string;
isListHide = true;
isListHide1 = true;
page = new Page();
rows = new Array<Division>();
temp = new Array<Division>();
selected = [];
moduleCode = "M-1011";
screenCode = "S-1221";
moduleName;
screenName;
defaultFormValues: object[];
availableFormValues: [object];
hasMessage;
duplicateWarning;
message = { 'type': '', 'text': '' };
divisionId = {};
searchKeyword = '';
ddPageSize = 5;
 //model: Division;
model: any = {};
cityOptions = [];
showCreateForm: boolean = false;
isSuccessMsg;
isfailureMsg;
isUnderUpdate: boolean;
hasMsg = false;
showMsg = false;
messageText;
emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{2,}[.]{1}[a-zA-Z]{2,}";
// phonePattern="[0-9]" ;
 phonePattern = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
myForm: FormGroup;
//phonePattern="/+91[0-9]{10}";
isModalPopUpActive = false;
currentLanguage: any;
isConfirmationModalOpen: boolean = false;
confirmationModalTitle = Constants.confirmationModalTitle;
confirmationModalBody = Constants.confirmationModalBody;
deleteConfirmationText = Constants.deleteConfirmationText;
OkText = Constants.OkText;
CancelText = Constants.CancelText;
isDeleteAction: boolean = false;
divisionIdvalue: string;
countries = [];
states = [];
cities = [];
dataSource: Observable<any>;
divisionIdSearch = [];
typeaheadLoading: boolean;
// Level Veriable
DIVISION_SEARCH: any
DIVISION_ID: any
DIVISION_DESCRIPTION: any
DIVISION_ARABIC_DESCRIPTION: any
DIVISION_CITY: any
DIVISION_ADDRESS: any
DIVISION_PHONE: any
DIVISION_FAX: any
DIVISION_EMAIL: any
DIVISION_ACTION: any
DIVISION_CREATE_LABEL: any
DIVISION_UPDATE_LABEL: any
DIVISION_CANCEL_LABEL: any
DIVISION_CLEAR_LABEL: any
DIVISION_SAVE_LABEL: any
DIVISION_DELETE_LABEL: any
DIVISION_CREATE_FORM_LABEL: any
DIVISION_UPDATE_FORM_LABEL: any
DIVISION_COUNTRY: any
DIVISION_STATE: any
DIVISION_TABLEVIEW: any

isDivisionOpen=false;
isCountryOpen=false;
isStateOpen=false;
isCityOpen=false;



selectedCountry;
 selectedState;
 selectedCity;
 selectedDivision;
tableViews:DropDownModule[] = [
    {id:5, name:'5 at a time'},
    {id:15, name:'15 at a time'},
    {id:50, name:'50 at a time'},
    {id:100, name:'100 at a time'}
];

@ViewChild(DatatableComponent) table: DatatableComponent;
@ViewChild('target') private myScrollContainer: ElementRef;

constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private divisionService: DivionService,
    private getScreenDetailService: GetScreenDetailService,
    private alertService: AlertService,
    private commonService: CommonService) {

        // let phoneNumber = new FormControl('', [Validators.required,Validators.max(999999999999999)]);
    this.page.pageNumber = 0;
    this.page.size = 5;
    this.page.sortOn = 'id';
    this.page.sortBy = 'DESC';
    // this.myForm = new FormGroup({
       
    //     phoneNumber: phoneNumber,
        
    //   });
    this.defaultFormValues = [
        { 'fieldName': 'DIVISION_SEARCH', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_ID', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_ARABIC_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_CITY', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_PHONE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_FAX', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_EMAIL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_CREATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_CLEAR_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_SAVE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_CANCEL_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_UPDATE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_ACTION', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_DELETE_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_CREATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_UPDATE_FORM_LABEL', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_STATE', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
        { 'fieldName': 'DIVISION_TABLEVIEW', 'fieldValue': '', 'helpMessage': '', 'deleteAccess': '', 'readAccess': '', 'writeAccess': '', 'isMandatory': '' },
    ];
    this.DIVISION_SEARCH = this.defaultFormValues[0];
    this.DIVISION_ID = this.defaultFormValues[1];
    this.DIVISION_DESCRIPTION = this.defaultFormValues[2];
    this.DIVISION_ARABIC_DESCRIPTION = this.defaultFormValues[3];
    this.DIVISION_CITY = this.defaultFormValues[4];
    this.DIVISION_ADDRESS = this.defaultFormValues[5];
    this.DIVISION_PHONE = this.defaultFormValues[6];
    this.DIVISION_FAX = this.defaultFormValues[7];
    this.DIVISION_EMAIL = this.defaultFormValues[8];
    this.DIVISION_CREATE_LABEL = this.defaultFormValues[9];
    this.DIVISION_CLEAR_LABEL = this.defaultFormValues[10];
    this.DIVISION_SAVE_LABEL = this.defaultFormValues[11];
    this.DIVISION_CANCEL_LABEL = this.defaultFormValues[12];
    this.DIVISION_UPDATE_LABEL = this.defaultFormValues[13];
    this.DIVISION_ACTION = this.defaultFormValues[14];
    this.DIVISION_DELETE_LABEL = this.defaultFormValues[15];
    this.DIVISION_CREATE_FORM_LABEL = this.defaultFormValues[16];
    this.DIVISION_UPDATE_FORM_LABEL = this.defaultFormValues[17];
    this.DIVISION_STATE = this.defaultFormValues[18];
    this.DIVISION_COUNTRY = this.defaultFormValues[19];
    this.DIVISION_TABLEVIEW = this.defaultFormValues[20];
    this.dataSource = Observable.create((observer: any) => {
        // Runs on every search
        observer.next(this.model.divisionId);
    }).mergeMap((token: string) => this.getReportDivisionAsObservable(token));
}

// Screen initialization
ngOnInit() {
  
    // this.registerForm = this.formBuilder.group({
    //     // firstName: ['', Validators.required],
    //     // lastName: ['', Validators.required],
    //     // email: ['', [Validators.required, Validators.email]],
    //     phoneNumber: ['', [Validators.required, Validators.minLength(6)]]
    // });


    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.currentLanguage = localStorage.getItem('currentLanguage');
    this.setPage({ offset: 0 });
    this.divisionService.getCountry().then(data => {
        debugger;
        this.countries = data.result;
        console.log("country",data);
        //console.log(data.result);       
    });

   
    //getting screen
    this.commonService.getScreenDetails(this.moduleCode, this.screenCode, this.defaultFormValues);

   

    //Following shifted from SetPage for better performance
    this.divisionService.getDivisionClassList().then(data => {
        this.divisionIdSearch = data.result;

       
        //console.log("Data class options : "+data.result);
    });

    this.route.params.subscribe((params: Params) => {
        this.divisionId = params['divisionId']; 

        if (this.divisionId != '' && this.divisionId != undefined) {

            this.getDivisionById(this.divisionId);
            this.isUnderUpdate=true;

            this.setAutocompleteLabel();
          
        }

    });
}

setAutocompleteLabel(){
    this.onAutocompleteFocus('lblDivisionId');
    this.onAutocompleteFocus('lblCountry');
    this.onAutocompleteFocus('lblState');
    this.onAutocompleteFocus('lblCity');
}
// get f() { return this.registerForm.controls; }


// onSubmit() {
//     this.submitted = true;

//     // stop here if form is invalid
//     if (this.registerForm.invalid) {
//         return;
//     }

//     alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
// }
// Exiting Id Search on every click
getReportDivisionAsObservable(token: string): Observable<any> {
    token = token.replace(/\\/g, "\\\\");
    let query = new RegExp(token, 'i');
    return Observable.of(
        this.divisionIdSearch.filter((id: any) => {
            return query.test(id.divisionId);
        })
    );
}

changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
}

typeaheadOnSelect(e: TypeaheadMatch): void {
   
    // console.log('Selected value: ', e);
    this.divisionService.getDivision(e.item.id).then(allData => {
        // console.log('Data', allData)
        this.divisionService.getStatesByCountryId(allData.result.countryId).then(data => {
            this.states = data.result;
            // console.log(data.result);
            this.divisionService.getCitiesByStateId(allData.result.stateId).then(data => {
                this.cities = data.result;
                // console.log(data.result);
                this.model = allData.result;
                this.model.id = 0;
                // console.log('this.model', this.model)
            });
        });

    });
}

//setting pagination
setPage(pageInfo) {
   
    this.selected = []; // remove any selected checkbox on paging
    this.page.pageNumber = pageInfo.offset;
    if (pageInfo.sortOn == undefined) {
        this.page.sortOn = this.page.sortOn;
    } else {
        this.page.sortOn = pageInfo.sortOn;
    }
    if (pageInfo.sortBy == undefined) {
        this.page.sortBy = this.page.sortBy;
    } else {
        this.page.sortBy = pageInfo.sortBy;
    }
    this.page.searchKeyword = '';
    this.divisionService.getDivisionClassList().then(pagedData => {
        this.page = pagedData.page;
        this.rows = pagedData.result;

        console.log("divisionids.........",this.rows)
        /*this.divisionService.getCityList().then(data => {
            this.cityOptions = data.result;
        });*/
    });
}

create() {
    this.showCreateForm = false;
    this.isUnderUpdate = false;
    setTimeout(() => {
        this.showCreateForm = true;
        setTimeout(() => {
            window.scrollTo(0, 600);
        }, 10);
    }, 10);
    this.model = {
        id: 0,
        arabicDivisionDescription: '',
        divisionAddress: '',
        divisionDescription: '',
        divisionId: '',
        email: '',
        fax: null,
        cityName: '',
        countryName: '',
        stateName: '',
        cityId: null,
        countryId: null,
        stateId: null,
        phoneNumber: null
    };
    this.divisionService.getCountry().then(data => {
        this.countries = data.result;
        //console.log(data.result);       
    });

    this.countries.forEach(ele => {
        if (ele.defaultCountry == 1) {
            this.model.countryId = ele.countryId;
        }
    });

    this.divisionService.getStatesByCountryId(this.model.countryId).then(data => {
        this.states = data.result;
        //console.log(data.result);
        this.model.stateId = this.states[0].stateId;
        this.divisionService.getCitiesByStateId(this.model.stateId).then(data => {
            this.cities = data.result;
            //console.log(data.result);
            this.model.cityId = this.cities[0].cityId;
        });
    });

}

// Clear form to reset to default blank
Clear(f: NgForm) {
    // f.resetForm({ country: 0, state: 0, city: 0 });
    this.selectedDivision="";
    this.model=[];
    this.selectedCountry="";
    this.selectedState="";
    this.selectedCity="";

            this.onAutocompleteFocusOut('lblDivisionId');
            this.onAutocompleteFocusOut('lblCountry');
            this.onAutocompleteFocusOut('lblState');
            this.onAutocompleteFocusOut('lblCity');
}

//function call for creating new division
CreateDivision(f: NgForm, event: Event) {
    debugger;
    // event.preventDefault();

    this.model.divisionId=this.selectedDivision;
   
    var divIdx = this.model.divisionId;
    
    //Check if the id is available in the model.
    //If id avalable then update the division, else Add new division.
    if (this.model.id > 0 && this.model.id != undefined) {
        this.isConfirmationModalOpen = true;
        this.isDeleteAction = false;
    } else {
        //Check for duplicate Division Id according to it create new division
        this.divisionService.checkDivisionId(divIdx).then(res => {
            if (res && res.result && res.result.isRepeat) {
                this.duplicateWarning = true;
                this.message.type = "success";

                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.hasMessage = false;
                    this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.duplicateWarning = false;
                    }, 4000);
                    this.message.text = res.btiMessage.message;
                  
                }, 100);
            } else {

                this.model.countryId=this.selectedCountry.countryId;
                this.model.stateId=this.selectedState.stateId;
                this.model.cityId=this.selectedCity.cityId;
               
                //Call service api for Creating new division
                this.divisionService.createDivision(this.model).then(data => {

                   
                    var datacode = data.code;
                 if (datacode == 201) {
                   
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                    
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                        this.goBack();
                          
                        
                    }, 4000);
                    
                }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = "Server error. Please contact admin.";
                    }, 100)
                });
            }

        }).catch(error => {
            this.hasMsg = true;
            window.setTimeout(() => {
                this.isSuccessMsg = false;
                this.isfailureMsg = true;
                this.showMsg = true;
                this.messageText = "Server error. Please contact admin.";
            }, 100);
        });
    }
}

//edit division by row id
edit(row: Division) {
    //console.log('row ', row)
    this.showCreateForm = true;
    this.model = Object.assign({}, row);
    this.isUnderUpdate = true;
    this.divisionId = row.divisionId;
    this.divisionIdvalue = this.model.divisionId;
    this.divisionService.getStatesByCountryId(row.countryId).then(data => {
        this.states = data.result;
        //console.log(data.result);       
    });
    this.divisionService.getCitiesByStateId(row.stateId).then(data => {
        this.cities = data.result;
        //console.log(data.result);       
    });
    setTimeout(() => {
        window.scrollTo(0, 500);
    }, 10);
}

updateStatus() {
    this.closeModal();
    debugger;
    // this.model.divisionId = this.divisionIdvalue;

    this.model.divisionId = this.selectedDivision;
    //Call service api for updating selected division
    this.divisionService.updateDivision(this.model).then(data => {
        var datacode = data.code;
        if (datacode == 201) {
                   
            this.isSuccessMsg = true;
            this.isfailureMsg = false;
        
        this.showMsg = true;
        this.messageText = data.btiMessage.message;
        this.hasMsg = true;
        window.setTimeout(() => {
            this.showMsg = false;
            this.hasMsg = false;
            // this.goBack();
              
            
        }, 4000);
        
    }
    }).catch(error => {
        this.hasMsg = true;
        window.setTimeout(() => {
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
            this.showMsg = true;
            this.messageText = "Server error. Please contact admin.";
        }, 100)
    });
}

varifyDelete() {
    if (this.selected.length > 0) {
        this.showCreateForm = false;
        this.isDeleteAction = true;
        this.isConfirmationModalOpen = true;
    } else {
        this.isSuccessMsg = false;
        this.hasMessage = true;
        this.message.type = 'error';
        this.isfailureMsg = true;
        this.showMsg = true;
        this.message.text = 'Please select at least one record to delete.';
        window.scrollTo(0, 0);
    }
}

//delete one or multiple divisions
delete() {
    var selectedDivisions = [];
    for (var i = 0; i < this.selected.length; i++) {
        selectedDivisions.push(this.selected[i].id);
    }
    this.divisionService.deleteDivision(selectedDivisions).then(data => {
        var datacode = data.code;
        if (datacode == 200) {
            this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        }
        this.hasMessage = true;
        if(datacode == "302"){
            this.message.type = "error";
            this.isSuccessMsg = false;
            this.isfailureMsg = true;
        } else {
            this.isSuccessMsg = true;
            this.message.type = "success";
            this.isfailureMsg = false;
        }
        
        //this.message.text = data.btiMessage.message;

        window.scrollTo(0, 0);
        window.setTimeout(() => {
            this.showMsg = true;
            window.setTimeout(() => {
                this.showMsg = false;
                this.hasMessage = false;
            }, 4000);
            this.message.text = data.btiMessage.message;
        }, 100);

        //Refresh the Grid data after deletion of division
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });

    }).catch(error => {
        this.hasMessage = true;
        this.message.type = "error";
        var errorCode = error.status;
        this.message.text = "Server issue. Please contact admin.";
    });
    this.closeModal();
}

// default list on page
onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
}

onCountrySelect(event) {
    this.model.stateId = 0;
    this.model.cityId = 0;
    this.divisionService.getStatesByCountryId(event.target.value).then(data => {
        this.states = data.result;
        //console.log(data.result);       
    });
    /* this.states = this._dataService.getStates()
                  .filter((item)=> item.countryid == countryid);*/
    this.cities = [];
}

onStateSelect(event) {
    this.model.cityId = 0;
    this.divisionService.getCitiesByStateId(event.target.value).then(data => {
        this.cities = data.result;
        //console.log(data.result);       
    });
    /* this.states = this._dataService.getStates()
                  .filter((item)=> item.countryid == countryid);*/
}
// search rolegroup details by group name 
updateFilter(event) {
    this.searchKeyword = event.target.value.toLowerCase();
    this.page.pageNumber = 0;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
    this.table.offset = 0;
    // this.divisionService.searchDivisionlist(this.page, this.searchKeyword).subscribe(pagedData => {
    //     this.page = pagedData.page;
    //     this.rows = pagedData.data;
    //     this.table.offset = 0;
    // });
}

// Set default page size
changePageSize(event) {
    this.page.size = event.target.value;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
}

confirm(): void {
    this.messageText = 'Confirmed!';
    this.delete();
}

closeModal() {
    this.isDeleteAction = false;
    this.isConfirmationModalOpen = false;
}

keyPress(event: any) {
    const pattern = /^[0-9\-()]+$/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
        event.preventDefault();
    }
}

sortColumn(val) {
    if (this.page.sortOn == val) {
        if (this.page.sortBy == 'DESC') {
            this.page.sortBy = 'ASC';
        } else {
            this.page.sortBy = 'DESC';
        }
    }
    this.page.sortOn = val;
    this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });


}


getDivisionData(event){
    if(event.divisionId!=undefined){
     var divId = event.id ? event.id : -1;
     this.model.divisionId=divId;
    //  if(event.divisionId!=undefined){
     if(event.id>0 && event.id!=undefined)
     {
         this.getDivisionById(event.id);
         this.setAutocompleteLabel();
     }
    } 
}



getCountryData(event){ 
    var countryId = event.countryId ? event.countryId : -1;       
    this.model.countryId=countryId;
    if(event.countryId!=undefined){
        setTimeout(() => {
            this.showCreateForm = true;
            setTimeout(() => {
                this.divisionService.getStatesByCountryId(countryId).then(data => {
                   console.log(data);
                    this.states = data.result;
                    //console.log(data.result);       
                });
                /* this.states = this._dataService.getStates()
                              .filter((item)=> item.countryid == countryid);*/
                this.cities = [];
            }, 10);
        }, 10);
    }
    setTimeout(() => {
        this.showCreateForm = true;
        setTimeout(() => {
            this.divisionService.getStatesByCountryId(countryId).then(data => {
               console.log(data);
                this.states = data.result;
                //console.log(data.result);       
            });
        }, 10);
    }, 10);
}


getStateData(event){
    if(event.stateId!=undefined){
        var stateId = event.stateId ? event.stateId : -1;
        this.model.stateId=stateId;
         this.divisionService.getCitiesByStateId(stateId).then(data => {
             console.log('city data',data.result);
             this.cityOptions = data.result;
            //  this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
         });
        }
}



getCityData(event){
    if(event.cityId!=undefined){
       
    var cityId = event.cityId ? event.cityId : -1;
   
    this.model.cityId=cityId;
    }
}



onAutocompleteFocus(labelId){

    let elem: HTMLElement = document.getElementById(labelId);
    elem.setAttribute("style", "top:-10px;font-size:14px;color:#5264AE;font-weight: bold;");
  }
  
  onAutocompleteFocusOut(labelId){
      let elem: HTMLElement = document.getElementById(labelId);
      elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
      
  }

  countryFocusout(selectedModel){
    debugger;
    this.isCountryOpen=false;
  if(typeof selectedModel.model === "string"){
      this.selectedCountry="";
      
  }
}

countryFocusIn(countryId){
    this.isCountryOpen=true;
}

// divisionFocusout(selectedModel){
//     debugger;
//     this.isDivisionOpen=false;
//   if(typeof selectedModel.model === "string"){
//       this.selectedDivision="";
      
//   }
// }

// divisionFocusIn(divisionId){
//     this.isDivisionOpen=true;
// }

divisionFocusIn(){
    this.isDivisionOpen=true;
  }
  divisionFocusOut(){
    
    this.isDivisionOpen=false;
  
  }

stateFocusout(selectedModel){
  debugger;
  this.isStateOpen=false;
if(typeof selectedModel.model === "string"){
    this.selectedState="";
    
}
}

stateFocusIn(countryId){
  this.isStateOpen=true;
}

cityFocusout(selectedModel){
  debugger;
  this.isCityOpen=false;
if(typeof selectedModel.model === "string"){
    this.selectedCity="";
    
}
}

cityFocusIn(countryId){
  this.isCityOpen=true;
}




  goBack(){
    this.router.navigate(['hcm/division']); 
  }

  toggle() {
    debugger;
    this.isListHide = !this.isListHide;
    // this.searchText = '';
    // this.selectedCountry='';
    // this.selectedState='';
    // if (!this.isListHide) {
      this.filterInput.nativeElement.focus();
     
  }

  toggle1() {
   
   
    this.selectedState='';
    
    // if (!this.isListHide) {
      setTimeout(() => this.filterInput1.nativeElement.focus(), 0);
      this.listItems.forEach((item) => {
        if (JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value) {
          this.addHightLightClass(item.nativeElement);
          this.scrollToView(item.nativeElement);
        } else {
          this.removeHightLightClass(item.nativeElement);
        }
      })
    // }
  }
  addHightLightClass(elem: HTMLElement) {
    // elem.classList.add(CSS_CLASS_NAMES.highLight)
  }
  scrollToView(elem?: HTMLElement) {
    if (elem) {
      setTimeout(() => elem.scrollIntoView(), 0)
    } else {
      const selectedItem = this.listItems.find((item) => JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value);
      if (selectedItem) {
        setTimeout(() => selectedItem.nativeElement.scrollIntoView(), 0);
      }
    }
  }
  removeHightLightClass(elem: HTMLElement) {
    // elem.classList.remove(CSS_CLASS_NAMES.highLight);
  }
  toggle2() {
    //  this.isListHide = !this.isListHide;
    
      this.selectedCity='';
  
  
    if (!this.isListHide) {
      setTimeout(() => this.filterInput2.nativeElement.focus(), 0);
      this.listItems.forEach((item) => {
        if (JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value) {
          this.addHightLightClass(item.nativeElement);
          this.scrollToView(item.nativeElement);
        } else {
          this.removeHightLightClass(item.nativeElement);
        }
      })
    }
  }
  toggle3() {
    debugger;
    this.selectedDivision='';
    this.isListHide1 = !this.isListHide1;
   
      this.filterInput3.nativeElement.focus();
     
  }


  getDivisionById(divId){
    this.divisionService.getDivision(divId).then(allData => {
        this.setAutocompleteLabel();
        console.log("selecteddivision.........",allData)
        this.model.id=allData.result.id;
        this.selectedDivision=allData.result.divisionId;
        this.selectedCountry=allData.result.countryName;
        this.selectedState=allData.result.stateName;
        this.selectedCity=allData.result.cityName;

        this.divisionService.getStatesByCountryId(allData.result.countryId).then(data => {
            this.states = data.result;
            // console.log(data.result);
            this.divisionService.getCitiesByStateId(allData.result.stateId).then(data => {
                this.cities = data.result;
                // console.log(data.result);
                this.model = allData.result;
                // this.model.id = 0;
                // console.log('this.model', this.model)
            });
        });

    });

  }

  onlyDecimalNumberKey(event) {
    return this.getScreenDetailService.onlyDecimalNumberKey(event);
}

  
  EscapeModal(event){
        
    var key = event.key;
    if(key=="Escape"){
        this.closeModal();
    }
  }

}
