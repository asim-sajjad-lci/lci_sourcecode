// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// @NgModule({
//   imports: [
//     CommonModule
//   ],
//   declarations: []
// })
// export class DeductionCodeSetupModule { }
export class DeductionCodeSetup {
  id: number;
  diductionId: string;
  discription: string;
  arbicDiscription: string;
  startDate: Date;
  endDate: Date;
  frequency: number;
  method: number;
  amount: any;
  percent: string;
  transction: boolean;
  inActive: boolean;
  perPeriod: string;
  perYear: string;
  lifeTime: string;
  dtoPayCode: any;
  payFactor: any;
  customDate: boolean;
  noOfDays: any;
  endDateDays: any;
  deductionTypeId:number;
  roundOf: number;

  constructor(id: number, deductionCodeId: string, description: string, arabicDescription: string,
          startDate: Date, endDate: Date, frequency: number, method: number,
          amount: any, percent: string, transction: boolean, inActive: boolean, perPeriod: string,
          perYear: string, lifeTime: string, dtoPayCode: any, payFactor: any, customDate: boolean,
          noOfDays: any,
          endDateDays: any,deductionTypeId:number,roundOf: number) {
          this.id = id;
          this.diductionId = deductionCodeId;
          this.discription = description;
          this.arbicDiscription = arabicDescription;
          this.startDate = startDate;
          this.endDate = endDate;
          this.frequency = frequency;
          this.method = method;
          this.percent = percent
          this.amount = amount;
          this.transction = transction;
          this.inActive = inActive;
          this.perPeriod = perPeriod;
          this.perYear = perYear;
          this.lifeTime = lifeTime;
          this.dtoPayCode = dtoPayCode;
          this.payFactor = payFactor;
          this.customDate = customDate;
          this.noOfDays = noOfDays;
          this.endDateDays = endDateDays;
          this.deductionTypeId = deductionTypeId;
          this.roundOf = roundOf;
  }

}

