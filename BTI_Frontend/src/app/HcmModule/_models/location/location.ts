// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// @NgModule({
//   imports: [
//     CommonModule
//   ],
//   declarations: []
// })
// export class LocationModule { }

export class Location {
  id: number;
  locationId: string;
  description: string;
  arabicDescription: string;
  contactName: string;
  locationAddress: string;
  cityName: string;
  countryName: string;
  stateName: string;    
  cityId: number;
  countryId: number;
  stateId: number;  
  phone: string;
  fax: string;


  constructor(id: number, locationId: string, description: string,
              arabicDescription: string, contactName: string, locationAddress: string,
              cityName: string, countryName: string,stateName:string,cityId:number,countryId:number,stateId:number, 
              phone: string, fax: string) {
      this.id = id;
      this.locationId = locationId;
      this.description = description;
      this.arabicDescription = arabicDescription;
      this.contactName = contactName;
      this.locationAddress = locationAddress;
      this.cityName =  cityName;
      this.countryName =  countryName;
      this.stateName = stateName;    
      this.cityId = cityId;
      this.countryId =  countryId;
      this.stateId = stateId;
      this.phone = phone;
      this.fax = fax;

  }


}


