/**
 * A model for division 
 */
export class Division {
    id: number;
    divisionId: string;
    divisionDescription: string;
    arabicDivisionDescription: string;
    divisionAddress: string;
    cityName: string;
    countryName: string;
    stateName: string;    
    phoneNumber: number;
    fax: number;
    email: string;    
    cityId: number;
    countryId: number;
    stateId: number;  

    //initializing division parameters
    constructor(id: number, divisionId: string, divisionDescription: string, arabicDivisionDescription: string,
        divisionAddress: string, phoneNumber: number, fax: number, email: string,cityName: string, countryName: string,stateName:string,cityId: number, countryId: number, stateId: number  ) {
        this.id = id;
        this.divisionId = divisionId;
        this.divisionDescription = divisionDescription;
        this.arabicDivisionDescription = arabicDivisionDescription;
        this.divisionAddress = divisionAddress;
        //this.city = city;
        this.phoneNumber = phoneNumber;
        this.fax = fax;
        this.email = email;
        this.cityName =  cityName;
        this.countryName =  countryName;
        this.stateName = stateName;   
        this.cityId = cityId;
        this.countryId =  countryId;
        this.stateId = stateId;
    }
}