export class PositionSetupModule {
    id: number;
    positionId: string;
    description: string;
    arabicDescription: string;
    reportToPostion: string;
    positionClassId: number;
    skillSetId: number;
    postionLongDesc: string;
    positionClassPrimaryId: number;
    skillsetPrimaryId: number;

    // initializing Position Class parameters
    constructor(id: number, positionId: string, description: string,
                arabicDescription: string, reportToPostion: string,
                positionClassId: number, skillSetId: number, postionLongDesc: string,
                 positionClassPrimaryId: number, skillsetPrimaryId: number) {
        this.id = id;
        this.positionId = positionId;
        this.description = description;
        this.arabicDescription = arabicDescription;
        this.reportToPostion = reportToPostion;
        this.positionClassId = positionClassId;
        this.skillSetId = skillSetId;
        this.postionLongDesc = postionLongDesc;
        this.positionClassPrimaryId = positionClassPrimaryId;
        this.skillsetPrimaryId = skillsetPrimaryId;
    }
}
