// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// @NgModule({
//   imports: [
//     CommonModule
//   ],
//   declarations: []
// })
// export class PositionClassModule { }

export class PositionClassModule {
  id: number;
  positionClassId: string;
  positionClassDescription: string;
  arabicPositionClassDescription: string;

  // initializing Position Class parameters
  constructor(id: number, positionClassId: string, positionClassDescription: string,
              arabicPositionClassDescription: string) {
      this.id = id;
      this.positionClassId = positionClassId;
      this.arabicPositionClassDescription = arabicPositionClassDescription;
      this.positionClassDescription = positionClassDescription;
  }
}

