// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';

// @NgModule({
//   imports: [
//     CommonModule
//   ],
//   declarations: []
// })
// export class PositionPayCodeModule { }


export class PositionPayCode {
  id: number;
  positionPlanId: string;
  codeType: string;
  payCodeId: string;
  payRate: string;
  positionPlanDesc: string;
  positiondescription: string;
  positionId: string;
  positionPlan: string;



  constructor(id: number, positionPlanId: string, codeType: string,
              payCodeId: string, payRate: string,
              positionPlanDesc: string, positiondescription: string, positionId: string, positionPlan: string) {
      this.id = id;
      this.positionPlanId = positionPlanId;
      this.codeType = codeType;
      this.payCodeId = payCodeId;
      this.payRate = payRate;
      this.positionPlanDesc = positionPlanDesc;
      this.positiondescription = positiondescription;
      this.positionId = positionId;
      this.positionPlan = positionPlan;
  }


}



