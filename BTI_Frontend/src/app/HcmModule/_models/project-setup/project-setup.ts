/**
 * A model for Project Setup
 */
export class ProjectSetup {
    id: number;
    projectId: string;
    projectName: string;
    projectArabicName: string;
    projectDescription: string;
    projectArabicDescription: string;
    startDate: string;
    endDate: string;
 
    //initializing Project Setup parameters
    constructor(id: number, projectId: string, projectName: string,
       projectArabicName: string, projectDescription: string, projectArabicDescription: string, startDate: string, endDate: string) {
       this.id = id;
       this.projectId = projectId;
       this.projectName = projectName;
       this.projectArabicName = projectArabicName;
       this.projectDescription = projectDescription;
       this.projectArabicDescription = projectArabicDescription;
       this.startDate = startDate;
       this.endDate = endDate;
    }
 }