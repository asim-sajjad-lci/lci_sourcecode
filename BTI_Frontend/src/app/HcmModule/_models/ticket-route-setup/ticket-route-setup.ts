export class TicketRouteSetupModel {
    id: number;
    codeId: string;
    descr: string;
    arabicDescr: string;

    constructor(id: number, codeId: string, descr: string,
        arabicDescr: string) {
        this.id = id;
        this.codeId = codeId;
        this.descr = descr;
        this.arabicDescr = arabicDescr;


    }


}