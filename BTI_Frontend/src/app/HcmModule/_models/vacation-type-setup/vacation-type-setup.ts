export class VacationTypeSetupModel {
    id: number;
    codeId: string;
    descr: string;
    arabicDescr: string;
    days:string;

    constructor(id: number, codeId: string, descr: string,
        arabicDescr: string, days:string) {
        this.id = id;
        this.codeId = codeId;
        this.descr = descr;
        this.arabicDescr = arabicDescr;
        this.days = days;



    }


}