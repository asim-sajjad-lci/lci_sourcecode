export class TicketPriceSetupModel {
    id: number;
    adultTckRate: number;
    childTckRate: number;
    infacntTckRate: number;
    ticketRouteId: string;
    ticketClassId: string;
    ticketRouteCodeId:string;
    ticketClassCodeId:string;

    constructor(id: number,adultTckRate: number,childTckRate: number,infacntTckRate: number,ticketRouteId: string,
        ticketClassId: string , ticketRouteCodeId:string, ticketClassCodeId:string) {
        this.id = id;
        this.adultTckRate = adultTckRate;
        this.childTckRate = childTckRate;
        this.infacntTckRate = infacntTckRate;
        this.ticketRouteId = ticketRouteId;
        this.ticketClassId = ticketClassId;
        this.ticketRouteCodeId = ticketRouteCodeId;
        this.ticketClassCodeId = ticketClassCodeId;


    }


}