    
import { Constants } from './Constants';

export class CommonFunctions {
    static print(parentURL, reportUnit, query)
    {
        var reportURL = Constants.JASPER_SERVER + Constants.JASPER_REPORT_URL + Constants.JASPER_REPORT_STATIC_PARAMS + parentURL + reportUnit + query;
        window.open(reportURL);
    }
}