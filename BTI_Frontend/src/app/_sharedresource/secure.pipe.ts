import { Pipe, PipeTransform } from '@angular/core';
import { Headers, Http, RequestOptions, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Pipe({
  name: 'secure'
})
export class SecurePipe implements PipeTransform {

  private headers = new Headers({ 'content-type': 'application/json' });

  constructor(private http: Http) { 
    let userData = JSON.parse(localStorage.getItem('currentUser'));
    this.headers.append('session', userData.session);
    this.headers.append('userid', userData.userId);
    let currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : '1';
    this.headers.append('langid', currentLanguage);
    this.headers.append('tenantid', localStorage.getItem('tenantid'));
  }

  transform(url: string) {

    return new Observable<string>((observer) => {
      // This is a tiny blank image
      observer.next('data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==');

      // The next and error callbacks from the observer
      const {next, error} = observer;

      this.http.get(url, { headers: this.headers, responseType: ResponseContentType.Blob }).subscribe((response:any) => {
        const reader = new FileReader();
        reader.readAsDataURL(response._body);
        reader.onloadend = function() {
          observer.next(reader.result);
        };
      });

      return {unsubscribe() {  }};
    });
  }
}