export class Constants {

    // static userModuleApiBaseUrl='http://localhost:8082/';
    // static financialModuleApiBaseUrl='http://localhost:8085/';
    // static hcmModuleApiBaseUrl='http://localhost:8087/';
    // static BaseUrl='http://localhost:8082/';


    static userModuleApiBaseUrl='http://localhost:8081/';
    static financialModuleApiBaseUrl='http://localhost:8085/';
    static hcmModuleApiBaseUrl='http://localhost:8087/';
    static imsModuleApiBaseUrl='http://localhost:8089/';
    static crmModuleApiBaseUrl='http://localhost:8082/';
    static BaseUrl='http://localhost:8081/';

    static userModuleCode='M-1000';
    static financialModuleCode='M-1001';
    static hcmModuleCode='M-1011';
    static crmModuleCode='M-2000';
    static currentModule="1";
    static atATimeText:string;
    static selectedText:string;
    static totalText:string;
    static deleteConfirmationText:string;
    static serverErrorText:string;
    static deleteCompanyAssociatedMessage:string;
    static deleteSuccessMessage:string;
    static butText:string;
    static ConfirmPasswordText:string;
    static alertmessage:string;
    static validationMessage:string;
    static selectAll:string;
    static unselectAll:string;
    static selectUser:string;
    static selectDays:string;
    static selectRoles:string;
    static selectCompany:string;
    static selectUserGroup:string;
    static select:string;
    static activeUsers:string;
    static inactiveUsers:string;
    static userGroups:string;
    static roleGroups:string;
    static roles:string;
    static passwordPolicy:string;
    static invalidPassword:string;
    static requiredValid:string;
    static tableViewtext:string;
    static msgText:string;
    static firstdateGreaterMsg : string;
    static lastdateGreaterMsg : string;
    static quarter : string;
    static firstQuarter : string;
    static secondQuarter : string;
    static thirdQuarter : string;
    static forthQuarter : string;
    static selectAccount:string;
    static accountNumberMissing : string;
    static confirmationModalTitle : string;
    static confirmationModalBody : string;
    static deleteConfirmationModalBody : string;
    static OkText : string;
    static CancelText : string;
    static btnCancelText : string;
    static accountDescription:string;
    static sameAccountNumberMsg : string;
    static accountNumberTitle : string;
    static createAccountNumber : string;
    static isDataFatch:boolean=false;
    static close: string;
    static ValidationToFromValue: string;
    static EmptyMessage: string;
    static fillAllFields: string;
    static minimumRange: string;
    static maximumRange: string;
    static fromPeriod: string;
    static toPeriod: string;
    static toValue: string;
    static toGreaterThan: string;
    static lessThan: string;
    static onePeriod: string;
    static twoPeriod: string;
    static greaterThan: string;
    static toLessThan: string;
    static maxrangeGreater: string;
    static minrange: string;
    static agingPeriod: string;
    static sevenAgingPeriod: string;
    static missingAccount: string;
    static greaterorEqualValue: string;
    static yearAndSeries: string;
    static oneRadio: string;
    static InvalidDate: string;
    static InvalidMonth: string;
    static InvalidYear: string;
    static InvalidDay: string;
    static mustBe: string;
    static footer: string;
    static footervalue: string;
    static ForbiddenMsg:string;
    static search: string;
    static currentPeriodLessthanPrv: string;
    static currentPeriodgreaterthanPrv: string;
    static invalidPeriodDate: string;
    static fillDate: string;
    static AmountText: string;
    static PercentageText: string;
    static YesText: string;
    static NoText: string;
    static JASPER_SERVER : string = 'http://localhost:8080/';
    static JASPER_REPORT_URL: string = 'jasperserver/flow.html?';
    static JASPER_REPORT_STATIC_PARAMS: string = '_flowId=viewReportFlow&_flowId=viewReportFlow&standAlone=true&decorate=no';
    static logoutConfirmation:string;
    static clearText:string;
    static updateField:string;
    static createButtonText:string;
    
    
    

    static batchTransacitonType:any={'JOURNAL_ENTRY':1,'CLEARING_ENTRY':2,'QUICK_JOURNAL_ENTRY':3,
        'BUDGET_ENTRY':4,'CASH_RECEIPT_ENTRY':5,'BANK_TRANSFER':6,'BANK_TRANSACTION':7,
        'AR_TransferEntry':8,'AR_Cash_Receipt_Entry':9,'AP_ENTER_VOID_TRANSACTION':10,'AP_ENTER_VOID_RECEIPT':11};

    static massCloseOriginType:any={'GL_JOURNAL_ENTRY':1,'GL_Cash_Receipt_Entry':2,'GL_Bank_Transfer_Entry':3,
        'AR_Transaction_Entry':8,'AR_Cash_Receipt_Entry':9,'AP_Transaction_Entry':11,'AP_Manual_Payment_Entry':12};
}