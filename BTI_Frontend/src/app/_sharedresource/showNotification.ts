// import { LeadDetailsService } from "../crmModule/_services/lead-details/lead-details.service";

// import { Injectable } from "@angular/core";
// import { DatePipe } from "@angular/common";
// import * as moment from 'moment';
// import { AlertService } from "./_services/alert.service";
// import { newactivity, Meetactivity, Taskactivity } from "../crmModule/_models/opportunity-details/opportunity-detailsModel";


// @Injectable()
// export class ShowNotification {

//     public plannedStatusId = 7;
//     plannedCalls: Array<newactivity>;
//     plannedMeetings: Array<Meetactivity>;
//     plannedTasks: Array<Taskactivity>;

//     public currentUserName;
//     public currentUserId;

//     constructor
//         (
//         private service: LeadDetailsService,
//         //  public changeDetectorRef: ChangeDetectorRef,
//         public datepipe: DatePipe,
//         public alertservice: AlertService
//         ) {

//             this.getCurrentUser();
//     }

//     getCurrentUser() {
//         let loggedInUser = JSON.parse(localStorage.getItem('currentUser'));
//         this.currentUserName = loggedInUser.secondaryFullName;
//         this.currentUserId = loggedInUser.userId;
//         console.log("user");
//         console.log(loggedInUser.secondaryFullName + " " + loggedInUser.userId)

//     }

//     getActivityCallRemainder() {
//         //alert("Hiiii");
//         //  this.getAllActivityCalls(id);
//         let today = new Date();
//         let filterarray = this.plannedCalls
//         let dates = this.datepipe.transform(today, 'yyyy-MM-dd');
//         dates = dates + " 00:00:00.0";
//         let filteredArray = filterarray.filter(
//             call => call.callDate == dates);

//         //   this.data = [];
//         console.log(filteredArray);

//         for (let i = 0; i < filteredArray.length; i++) {
//             let act = filteredArray[i];
//             if (act.reminderBeforeTime != undefined) {
//                 //  let date = act.callDate.toString().split(" ");
//                 let time = new Date(act.callTime);
//                 let newtime = time.toLocaleTimeString();

//                 let remtime = new Date(act.reminderBeforeTime);
//                 let remnewtime = remtime.toLocaleTimeString();
//                 let arrremtime = remnewtime.split(":");
//                 const res = moment(newtime, "hh:mm a").subtract(arrremtime[1], 'minutes');
//                 let dateToBeCompare = new Date(res.toDate());


//                 let dateToBeCompareMilli = dateToBeCompare.getTime();
//                 let today = new Date();
//                 let todayMilli = today.getTime();

//                 let lattime = today.toLocaleTimeString();
//                 let latcomptime = dateToBeCompare.toLocaleTimeString();

//                 if (lattime == latcomptime) {
//                     this.alertservice.success("Call Remainder  at " + dateToBeCompare);
//                 }

//                 // if (dateToBeCompareMilli == todayMilli) {
//                 //     alert("hiiiiiii")
//                 //     this.alertservice.success("Call Remainder");
//                 // }
//             }

//         }


//     }

//     getActivityMeetingRemainder() {

//         let today = new Date();
//         let filterarray = this.plannedMeetings
//         let dates = this.datepipe.transform(today, 'yyyy-MM-dd');
//         dates = dates + " 00:00:00.0";
//         let filteredArray = filterarray.filter(
//             meet => meet.meetingDate == dates);

//         //   this.data = [];
//         console.log(filteredArray);

//         for (let i = 0; i < filteredArray.length; i++) {
//             let act = filteredArray[i];
//             if (act.reminderBeforeTime != undefined) {
//                 //  let date = act.callDate.toString().split(" ");
//                 let time = new Date(act.meetingTime);
//                 let newtime = time.toLocaleTimeString();

//                 let remtime = new Date(act.reminderBeforeTime);
//                 let remnewtime = remtime.toLocaleTimeString();
//                 let arrremtime = remnewtime.split(":");
//                 const res = moment(newtime, "hh:mm a").subtract(arrremtime[1], 'minutes');
//                 let dateToBeCompare = new Date(res.toDate());


//                 let dateToBeCompareMilli = dateToBeCompare.getTime();
//                 let today = new Date();
//                 let todayMilli = today.getTime();

//                 let lattime = today.toLocaleTimeString();
//                 let latcomptime = dateToBeCompare.toLocaleTimeString();

//                 if (lattime == latcomptime) {
//                     this.alertservice.success("Meeting Remainder  at " + dateToBeCompare);
//                 }

//                 // if (dateToBeCompareMilli == todayMilli) {
//                 //     alert("hiiiiiii")
//                 //     this.alertservice.success("Call Remainder");
//                 // }
//             }

//         }


//     }

//     getActivityTaskRemainder() {

//         let today = new Date();
//         let filterarray = this.plannedTasks
//         let dates = this.datepipe.transform(today, 'yyyy-MM-dd');
//         dates = dates + " 00:00:00.0";
//         let filteredArray = filterarray.filter(
//             task => task.taskDate == dates);

//         //   this.data = [];
//         console.log(filteredArray);

//         for (let i = 0; i < filteredArray.length; i++) {
//             let act = filteredArray[i];
//             if (act.reminderBeforeTime != undefined) {
//                 //  let date = act.callDate.toString().split(" ");
//                 let time = new Date(act.taskTime);
//                 let newtime = time.toLocaleTimeString();

//                 let remtime = new Date(act.reminderBeforeTime);
//                 let remnewtime = remtime.toLocaleTimeString();
//                 let arrremtime = remnewtime.split(":");
//                 const res = moment(newtime, "hh:mm a").subtract(arrremtime[1], 'minutes');
//                 let dateToBeCompare = new Date(res.toDate());


//                 let dateToBeCompareMilli = dateToBeCompare.getTime();
//                 let today = new Date();
//                 let todayMilli = today.getTime();

//                 let lattime = today.toLocaleTimeString();
//                 let latcomptime = dateToBeCompare.toLocaleTimeString();

//                 if (lattime == latcomptime) {
//                     this.alertservice.success("Task Remainder  at " + dateToBeCompare);
//                 }

//                 // if (dateToBeCompareMilli == todayMilli) {
//                 //     alert("hiiiiiii")
//                 //     this.alertservice.success("Call Remainder");
//                 // }
//             }

//         }


//     }

//     getAllActivityCalls() {
       

//         return this.service.getAllActivityCalls().subscribe(allCalls => {

//             //Filter all planned calls
//             let filteredPlanned = allCalls.result.records.filter(p => p.userId == this.currentUserId)


//             let newfilteredPlanned = (this.plannedStatusId) ?
//                 filteredPlanned.filter(p => p.status == this.plannedStatusId) :
//                 filteredPlanned = [];
//             this.plannedCalls = newfilteredPlanned;

//              this.getActivityCallRemainder();

//         });

//         // return this.service.getActyCallId(leadId).subscribe(allCalls => {

//         //     //Filter all planned calls
//         //     let filteredPlanned = (this.plannedStatusId) ?
//         //         allCalls.result.records.filter(p => p.status == this.plannedStatusId) :
//         //         allCalls.result.records = [];
//         //     this.plannedCalls = filteredPlanned;

//         //   console.log("shownotification");
//         //     console.log(this.plannedCalls);
//         //     this.getActivityCallRemainder();

//         // });

//     }

//     getAllActivityMeetings() {


//         return this.service.getAllActivityCalls().subscribe(allMeetings => {

//             let filteredPlanned = allMeetings.result.records.filter(p => p.userId == this.currentUserId)

//             //Filter all planned meeting
//             let newfilteredPlanned = (this.plannedStatusId) ?
//                 filteredPlanned.filter(p => p.status == this.plannedStatusId) :
//                 filteredPlanned = [];
//             this.plannedMeetings = newfilteredPlanned;

//             this.getActivityMeetingRemainder();

//         });
//     }
//     // return this.service.getActyMeetId(leadId).subscribe(allMeetings => {

//     //     //Filter all planned meeting
//     //     let filteredPlanned = (this.plannedStatusId) ?
//     //         allMeetings.result.records.filter(p => p.status == this.plannedStatusId) :
//     //         allMeetings.result.records = [];
//     //     this.plannedMeetings = filteredPlanned;

//     //     this.getActivityMeetingRemainder();

//     // });
//     // }

//     getAllActivityTask() {

//         return this.service.getAllActivityTask().subscribe(allTask => {
//             // this.plannedTasks = allTask.result.records;
//             let filteredPlanned = allTask.result.records.filter(p => p.userId == this.currentUserId)
//             //Filter all planned meeting
//             let newfilteredPlanned = (this.plannedStatusId) ?
//                 filteredPlanned.filter(p => p.status == this.plannedStatusId) :
//                 filteredPlanned = [];
//             this.plannedTasks = newfilteredPlanned;

//             this.getActivityTaskRemainder();
//         });

//         //     return this.service.getActyTaskId(leadId).subscribe(allTask => {
//         //        // this.plannedTasks = allTask.result.records;

//         //         //Filter all planned meeting
//         //         let filteredPlanned = (this.plannedStatusId) ?
//         //             allTask.result.records.filter(p => p.status == this.plannedStatusId ) :
//         //             allTask.result.records = [];
//         //         this.plannedTasks = filteredPlanned;

//         //         this.getActivityTaskRemainder();
//         //     });
//     }

// }

