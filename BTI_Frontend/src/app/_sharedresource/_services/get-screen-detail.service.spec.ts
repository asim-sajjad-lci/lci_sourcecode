import { TestBed, inject } from '@angular/core/testing';

import { GetScreenDetailService } from './get-screen-detail.service';

describe('GetScreenDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetScreenDetailService]
    });
  });

  it('should be created', inject([GetScreenDetailService], (service: GetScreenDetailService) => {
    expect(service).toBeTruthy();
  }));
});
