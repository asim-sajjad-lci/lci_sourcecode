import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PositionSetupModule } from 'app/HcmModule/_models/position-setup/position-setup';
import { PositionAttachmentModule } from 'app/HcmModule/_models/position-attachment/position-attachment';


@Injectable()
export class UploadFileService {
    positionModelData:PositionSetupModule;
    positionAttchModelData:any;
    modelClear:boolean;

    positionData(positionData:any){
        this.positionModelData = positionData;
    }

    positionAttachData(positionAttach:PositionAttachmentModule, attachmentFile:any, isAttachedFile:boolean){

        this.positionAttchModelData = { 
            positionAttach: positionAttach, 
            attachmentFile: attachmentFile,
            isAttachedFile: isAttachedFile,
        };
    }
}
