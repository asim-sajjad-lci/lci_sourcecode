 
import { Injectable } from '@angular/core'
import { Router } from '@angular/router';
import { Headers, Http } from '@angular/http';
import {Constants} from '../Constants';
import {BehaviorSubject} from "rxjs/BehaviorSubject"
import { ExDialog } from 'app/generic-component/ngx-dialog/ex-dialog.service';

declare var $:any
@Injectable()
export class GetScreenDetailService {
    constant:Constants;
    private state$ = new BehaviorSubject<any>('[object]');
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private getScreenDetailUrl = Constants.userModuleApiBaseUrl+'screenDetail';
    private getscreenGridDetailUrl = Constants.userModuleApiBaseUrl+'screenGridDetail';
    private getScreenDetailUserUrl = Constants.userModuleApiBaseUrl+'getScreenDetailOfUser';
    private screenLockAndCheckEditAccessUrl = Constants.userModuleApiBaseUrl+'screenLockAndCheckEditAccess';
    private getScreenDetailUserCommanUrl = Constants.userModuleApiBaseUrl+'getScreenDetailOfUser';
    private getHeaderDetailUrl = Constants.userModuleApiBaseUrl+'user/sideBarMenuDetail';
    private getSideMenuUrl = Constants.userModuleApiBaseUrl+'user/sideMenuByHeaderId';
    private getLanguageListForDropDownURL =  Constants.userModuleApiBaseUrl +'getLanguageListForDropDown';
    private getLanguageByIdUrl = Constants.userModuleApiBaseUrl + 'language/getById';
    private getCommonConstantListUrl = Constants.userModuleApiBaseUrl+'getCommononConstantList';
    private updateActiveSessionUrl = Constants.userModuleApiBaseUrl + 'updateActiveSession';
    private moduleId;
    private sidebarValues: Array<object>;
    private Url;
    userData:any;
    private constantsList={};
    accessRoleId = 1;
    private getchangeColumnVisibleStatusUrl = Constants.userModuleApiBaseUrl+'grids/changeColumnVisibleStatus';
    private gethideAllColumnsUrl = Constants.userModuleApiBaseUrl+'grids/hideAllColumns';
    private geShowAllColumnsUrl = Constants.userModuleApiBaseUrl+'grids/showAllColumns';
    private geResetGridUrl = Constants.userModuleApiBaseUrl+'grids/resetGrid';

   
    constructor(private http: Http,  private router: Router,  private exDialog: ExDialog) {

        debugger;
        
        var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";
        this.headers.append("langid", currentLanguage);
       
        
       // alert($('#btnLogout').text());
        if(localStorage.getItem('currentUser'))
        {
            
            this.userData = JSON.parse(localStorage.getItem('currentUser'));
           
            if(this.userData  != null){
                this.headers.append('session', this.userData.session);
                this.headers.append('userid', this.userData.userId);
                this.headers.append("tenantid", localStorage.getItem('tenantid'))
            }
        }
         this.ValidateUser();
        //  if(Constants.isDataFatch == false)
        //  {
           
        //  }
         this.getConstants(currentLanguage);
         this.getAllConstantList();
    
        

        // this.constantsList =  this.http
        //     .get(this.getCommonConstantListUrl, { headers: this.headers })
        //     .toPromise()
        //     .then(res => res.json())
        //     .catch(this.handleError);
        
           
      
         
     }

     ValidateUser()
     {
        if(localStorage.getItem('currentUser'))
        {
         this.UpdateActiveSession().then(currentdata =>
         {
           if(currentdata.btiMessage.messageShort != 'USER_UPDATED_SUCCESS')
           {
             localStorage.setItem('currentUser', '');
            
             $('#forbiddenMsg').html(currentdata.btiMessage.message);
             $("#btnSessionOut").click();
            //  this.router.navigate(['login']);
           }
         })
        }
     }
     getConstants(currentLanguage)
     {
        
            if (currentLanguage == "1") {
                Constants.atATimeText = 'at a Time';
                Constants.selectedText='Selected'
                Constants.totalText='total'
                Constants.deleteConfirmationText='Are you sure to delete selected Record ?'
                Constants.serverErrorText='Server error. Please contact admin !';
                Constants.deleteCompanyAssociatedMessage='not deleted, Because There is some data associated with';
                Constants.deleteSuccessMessage='Deleted Successfully';
                Constants.butText='But';
                Constants.ConfirmPasswordText='Please enter same password';
                Constants.alertmessage='End Time should be greater then Start Time !';
                Constants.validationMessage='Please Fill Start and End Date !!';
                Constants.selectAll='Select all';
                Constants.unselectAll='Unselect all';
                Constants.selectUser='Please Select User';
                Constants.selectDays='Please Select Days';
                Constants.selectRoles='Please Select Roles';
                Constants.selectCompany='Please Select Company';
                Constants.selectUserGroup='Please Select User Group';
                Constants.select='--Select--';
                Constants.activeUsers='Active Users';
                Constants.inactiveUsers='Inactive Users';
                Constants.userGroups='User Groups';
                Constants.roleGroups='Role Groups';
                Constants.roles='Roles';
                Constants.passwordPolicy='Passwords must have at least 8 characters, one uppercase , one lowercase,one digit and one special character.';
                Constants.invalidPassword="Passwords must have at least 8 characters, one uppercase , one lowercase,one digit and one special character.";
                Constants.requiredValid='This field is required';
                Constants.tableViewtext='Table View';
                Constants.msgText='Please fill all fields !!';
                Constants.firstdateGreaterMsg="First Day is less than Last Day";
                Constants.lastdateGreaterMsg="Last Day is greater than First Day";
                Constants.quarter="Quarter";
                Constants.firstQuarter="st";
                Constants.secondQuarter="nd";
                Constants.thirdQuarter="rd";
                Constants.forthQuarter="th";
                Constants.selectAccount='Please Select Account Number';
                Constants.accountNumberMissing="Account Number Missing";
                Constants.confirmationModalTitle="Confirmation";
                Constants.confirmationModalBody="Are you sure want to update Status ?";
                Constants.deleteConfirmationModalBody="Are you sure to delete selected Record ?";
                Constants.OkText="Yes";
                Constants.CancelText="No";
                Constants.btnCancelText="Cancel";
                Constants.sameAccountNumberMsg="Same account number alreay exist..";
                Constants.accountNumberTitle="Create Account Number";
                Constants.createAccountNumber="Create";
                Constants.accountDescription="Account Description";
                Constants.close="Close";
                Constants.ValidationToFromValue="From Value Should be less than To value";
                Constants.fillAllFields="Please fill all fields !!";
                Constants.minimumRange="Please enter Minimum Range !";
                Constants.maximumRange="Please enter Max Range";
                Constants.fromPeriod="Please enter From Period";
                Constants.toPeriod="Please enter To Period";
                Constants.toValue="To Value Should be less then or equal to 999";
                Constants.toGreaterThan="To Value Should be greater then";
                Constants.toLessThan="To Value Should be less then";
                Constants.onePeriod="Minimum 1 Period record necessary";
                Constants.twoPeriod="Minimum 2 Period record necessary";
                Constants.lessThan="From Value Should be less then";
                Constants.greaterThan="From Value Should be greater then";
                Constants.maxrangeGreater="MaxRange Value Should be greater then";
                Constants.greaterorEqualValue="To value should not greater or equal to from value";
                Constants.minrange="Minimum Range Should be";
                Constants.agingPeriod="Not More aging period Allowed";
                Constants.sevenAgingPeriod="Only 7 aging period allowed !!";
                Constants.missingAccount="Account Number Missing !";
                Constants.yearAndSeries="Please select the Year and Series";
                Constants.oneRadio="Please select the one radio option";
                Constants.InvalidDate="Invalid date format";
                Constants.InvalidMonth="Invalid value for month";
                Constants.InvalidYear="Invalid value for year";
                Constants.InvalidDay="Invalid value for day";
                Constants.mustBe="must be ";
                Constants.footervalue="@ 2018 BTI. All rights reserved.";
                Constants.ForbiddenMsg="Forbidden: You don't have permission to access this system. Please Contact with admin for more details.";
                Constants.search="Search";
                Constants.currentPeriodLessthanPrv="Current Period Can't less  or equal than previous Period";
                Constants.currentPeriodgreaterthanPrv="Current Period Can't Greater or equal than Next Period";
                Constants.invalidPeriodDate="Please Enter Date in Valid Format (DD/MM/YY)";
                Constants.fillDate="Please Enter Date";
                Constants.AmountText="Amount";
                Constants.PercentageText="Percentage";
                Constants.YesText="Yes";
                Constants.NoText="No";
                Constants.logoutConfirmation="Are you sure to logout ?";
                Constants.clearText="Clear";
                Constants.updateField="Update";
                Constants.createButtonText="Create";
                
             }
             else if (currentLanguage == "2") {
                Constants.atATimeText =  'في الوقت';
                Constants.selectedText='المحدد';
                Constants.totalText='مجموع';
                Constants.deleteConfirmationText='هل تريد بالتأكيد حذف السجل المحدد؟';
                Constants.serverErrorText='خطأ في الخادم. يرجى الاتصال بالمسؤول!';
                Constants.deleteCompanyAssociatedMessage='لم يتم حذف، لأن هناك بعض البيانات المرتبطة';
                Constants.deleteSuccessMessage='حذف بنجاح';
                Constants.butText='لكن';
                Constants.ConfirmPasswordText='الرجاء إدخال كلمة المرور نفسها';
                Constants.alertmessage='يجب أن يكون وقت الانتهاء أكبر ثم وقت البدء !';
                Constants.validationMessage='يرجى ملء تاريخ البدء والانتهاء !!';
                Constants.selectAll='اختر الكل';
                Constants.unselectAll='إلغاء تحديد الكل';
                Constants.selectUser='الرجاء تحديد المستخدم';
                Constants.selectDays='الرجاء تحديد أيام';
                Constants.selectRoles='الرجاء تحديد الأدوار';
                Constants.selectCompany='الرجاء تحديد الشركة';
                Constants.selectUserGroup='الرجاء تحديد مجموعة المستخدمين';
                Constants.select='--تحديد--';
                Constants.activeUsers='المستخدمون النشطون';
                Constants.inactiveUsers='المستخدمون غير النشطين';
                Constants.userGroups='مجموعات المستخدمين';
                Constants.roleGroups='مجموعات الأدوار';
                Constants.roles='الأدوار';
                Constants.passwordPolicy='يجب أن تحتوي كلمات المرور على 8 أحرف على الأقل، وأحرف كبيرة، وأخرى صغيرة، ورقم واحد، وحرف خاص واحد.';
                Constants.invalidPassword="رمز مرور خاطئ";
                Constants.requiredValid='هذه الخانة مطلوبه';
                Constants.tableViewtext='عرض جدول';
                Constants.msgText='لو سمحت أملأ كل الحقول !!';
                Constants.firstdateGreaterMsg= 'اليوم الأول أقل من اليوم الأخير';
                Constants.lastdateGreaterMsg='اليوم الأخير أكبر من اليوم الأول';
                Constants.quarter="ربع";
                Constants.firstQuarter="شارع";
                Constants.secondQuarter="الثانية";
                Constants.thirdQuarter="الثالثة";
                Constants.forthQuarter="عشر";
                Constants.selectAccount='الرجاء تحديد رقم الحساب';
                Constants.accountNumberMissing='رقم الحساب مفقود';
                Constants.confirmationModalTitle="التأكيد";
                Constants.confirmationModalBody="هل تريد بالتأكيد تحديث الحالة؟";
                Constants.deleteConfirmationModalBody="هل تريد بالتأكيد حذف السجل المحدد؟";
                Constants.OkText="نعم فعلا";
                Constants.CancelText="لا";
                Constants.btnCancelText="إلغاء";
                Constants.sameAccountNumberMsg="نفس رقم الحساب موجود هنا.";
                Constants.accountNumberTitle="إنشاء رقم الحساب";
                Constants.createAccountNumber="خلق";
                Constants.accountDescription="وصف الحساب";
                Constants.close="قريب";
                Constants.ValidationToFromValue="من القيمة يجب أن تكون أقل من قيمة";
                Constants.fillAllFields="لو سمحت أملأ كل الحقول !!";
                Constants.minimumRange="يرجى إدخال الحد الأدنى المدى!";
                Constants.maximumRange="الرجاء إدخال النطاق الأقصى";
                Constants.fromPeriod="الرجاء إدخال من الفترة";
                Constants.toPeriod="الرجاء إدخال إلى الفترة";
                Constants.toValue="إلى القيمة يجب أن يكون أقل من ذلك أو يساوي 999";
                Constants.greaterThan="إلى القيمة يجب أن يكون أكبر ثم";
                Constants.lessThan="إلى القيمة يجب أن يكون أقل من ذلك";
                Constants.onePeriod="الحد الأدنى 1 سجل الفترة اللازمة";
                Constants.twoPeriod="الحد الأدنى 2 سجل الفترة اللازمة";
                Constants.lessThan="من القيمة يجب أن يكون أقل من ذلك";
                Constants.greaterThan="من القيمة يجب أن يكون أكبر ثم";
                Constants.maxrangeGreater="يجب أن تكون قيمة ماكسرانج أكبر ثم";
                Constants.greaterorEqualValue="يجب ألا تكون القيمة أكبر أو مساوية للقيمة";
                Constants.minrange="يجب أن يكون الحد الأدنى المدى";
                Constants.agingPeriod="لا مزيد من فترة الشيخوخة مسموح بها";
                Constants.sevenAgingPeriod="فقط 7 فترة الشيخوخة يسمح !!";
                Constants.missingAccount="رقم الحساب مفقود!";
                Constants.yearAndSeries="يرجى تحديد السنة والسنة";
                Constants.oneRadio="يرجى تحديد خيار الراديو الواحد";
                Constants.InvalidDate="تنسيق التاريخ غير صالح";
                Constants.InvalidMonth="قيمة غير صالحة للشهر";
                Constants.InvalidYear="قيمة غير صالحة للسنة";
                Constants.InvalidDay="قيمة غير صالحة لليوم";
                Constants.footervalue="@ 2018 بتي. كل الحقوق محفوظة.";
                Constants.ForbiddenMsg="ممنوع: ليس لديك إذن للوصول إلى هذا النظام. يرجى الاتصال مع المشرف لمزيد من التفاصيل.";
                Constants.search="بحث";
                Constants.currentPeriodLessthanPrv="الفترة الحالية لا يمكن أن تقل أو تساوي الفترة السابقة";
                Constants.currentPeriodgreaterthanPrv="الفترة الحالية لا يمكن أن تكون أكبر أو تساوي من الفترة التالية";
                Constants.invalidPeriodDate="يرجى إدخال التاريخ بصيغة صالحة (DD / MM / YY)";
                Constants.fillDate="يرجى إدخال تاريخ الفترة";
                Constants.AmountText="كمية";
                Constants.PercentageText="النسبة المئوية";
                Constants.YesText="نعم فعلا";
                Constants.NoText="لا";
                Constants.logoutConfirmation="هل أنت متأكد من تسجيل الخروج؟";
                Constants.clearText="Clear";
                Constants.updateField=" تحديث";
                Constants.createButtonText="إنشاء";
               
             }
             else {
                Constants.atATimeText = 'at a Time';
                Constants.selectedText='Selected';
                Constants.totalText='total';
                Constants.deleteConfirmationText='Are you sure to delete selected Record ?';
                Constants.serverErrorText='Server error. Please contact admin !';
                Constants.deleteCompanyAssociatedMessage='not deleted, Because There is some data associated with';
                Constants.deleteSuccessMessage='Deleted Successfully';
                Constants.butText='But';
                Constants.ConfirmPasswordText='Please enter same password';
                Constants.alertmessage='End Time should be greater then Start Time !';
                Constants.validationMessage='Please Fill Start and End Date !!';
                Constants.selectAll='Select all';
                Constants.unselectAll='Unselect all';
                Constants.selectUser='Please Select User';
                Constants.selectDays='Please Select Days';
                Constants.selectRoles='Please Select Roles';
                Constants.selectCompany='Please Select Company';
                Constants.selectUserGroup='Please Select User Group';
                Constants.select='--Select--';
                Constants.activeUsers='Active Users';
                Constants.inactiveUsers='Inactive Users';
                Constants.userGroups='User Groups';
                Constants.roleGroups='Role Groups';
                Constants.roles='Roles';
                Constants.passwordPolicy='Passwords must have at least 8 characters, one uppercase , one lowercase,one digit and one special character.';
                Constants.invalidPassword="Passwords must have at least 8 characters, one uppercase , one lowercase,one digit and one special character.";
                Constants.requiredValid='This field is required';
                Constants.tableViewtext='Table View';
                Constants.msgText='Please fill all fields !!'; 
                Constants.firstdateGreaterMsg="First Day is less than Last Day";
                Constants.lastdateGreaterMsg="Last Day is greater than First Day";
                Constants.quarter="Quarter";
                Constants.firstQuarter="st";
                Constants.secondQuarter="nd";
                Constants.thirdQuarter="rd";
                Constants.forthQuarter="th";
                Constants.selectAccount='Please Select Account Number';
                Constants.accountNumberMissing="Account Number Missing";
                Constants.confirmationModalTitle="Confirmation";
                Constants.confirmationModalBody="Are you sure want to update Status ?";
                Constants.deleteConfirmationModalBody="Are you sure to delete selected Record ?";
                Constants.OkText="Yes";
                Constants.CancelText="No";
                Constants.btnCancelText="Cancel";
                Constants.sameAccountNumberMsg="Same account number alreay exist..";
                Constants.accountNumberTitle="Please Fill Account Segments";
                Constants.createAccountNumber="Create";
                Constants.accountDescription="Account Description";
                Constants.close="Close";
                Constants.ValidationToFromValue="From Value Should be less than To value";
                Constants.fillAllFields="Please fill all fields !!";
                Constants.minimumRange="Please enter Minimum Range !";
                Constants.maximumRange="Please enter Max Range";
                Constants.fromPeriod="Please enter From Period";
                Constants.toPeriod="Please enter To Period";
                Constants.toValue="To Value Should be less then or equal to 999";
                Constants.toGreaterThan="To Value Should be greater then";
                Constants.toLessThan="To Value Should be less then";
                Constants.onePeriod="Minimum 1 Period record necessary";
                Constants.twoPeriod="Minimum 2 Period record necessary";
                Constants.lessThan="From Value Should be less then";
                Constants.greaterThan="From Value Should be greater then";
                Constants.maxrangeGreater="MaxRange Value Should be greater then";
                Constants.greaterorEqualValue="To value should not greater or equal to from value";
                Constants.minrange="Minimum Range Should be";
                Constants.agingPeriod="Not More aging period Allowed";
                Constants.sevenAgingPeriod="Only 7 aging period allowed !!";
                Constants.missingAccount="Account Number Missing !";
                Constants.yearAndSeries="Please select the Year and Series";
                Constants.oneRadio="Please select the one radio option";
                Constants.InvalidDate="Invalid date format";
                Constants.InvalidMonth="Invalid value for month";
                Constants.InvalidYear="Invalid value for year";
                Constants.InvalidDay="Invalid value for day";
                Constants.mustBe="must be ";
                Constants.footervalue="@ 2018 BTI. All rights reserved.";
                Constants.ForbiddenMsg="Forbidden: You don't have permission to access this system. Please Contact with admin for more details.";
                Constants.search="Search";
                Constants.currentPeriodLessthanPrv="Current Period Can't less  or equal than previous Period";
                Constants.currentPeriodgreaterthanPrv="Current Period Can't Greater or equal than Next Period";
                Constants.invalidPeriodDate="Please Enter Date in Valid Format (DD/MM/YY)";
                Constants.fillDate="Please Enter Date";
                Constants.AmountText="Amount";
                Constants.PercentageText="Percentage";
                Constants.YesText="Yes";
                Constants.NoText="No";
               
            }

        
     }

    getHeaderDetail() {
     
        var crlng = localStorage.getItem('currentLanguage');
       
        this.headers.delete("langid");
         this.headers.append("langid", crlng);
        return this.http
        .get(this.getHeaderDetailUrl, { headers: this.headers})
        .map(
           res => res.json()
        );
    }

    getLanguageListForDropDown() {
          return this.http
            .post(this.getLanguageListForDropDownURL, { }, { headers: this.headers })
            .map(data => data.json().result)
            .catch(this.handleError);
    }
    
    getLanguageById(languageId: string) {
        return this.http.post(this.getLanguageByIdUrl, { languageId: languageId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    getSideBarDetail(moduleId: string) {
        return this.http
        .post(this.getSideMenuUrl, JSON.stringify({ 'moduleId': moduleId}), { headers: this.headers })
        .map(
           res => res.json()
        );
    }

    getScreenDetail(moduleCode: string, screenCode: string) {
        return this.http
            .post(this.getScreenDetailUrl, JSON.stringify({ moduleCode: moduleCode, screenCode: screenCode }), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    screenGridDetail(moduleCode: string, screenCode: string) {
        return this.http
            .post(this.getscreenGridDetailUrl, JSON.stringify({ moduleCode: moduleCode, screenCode: screenCode, accessRoleId: this.accessRoleId }), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    getScreenDetailUser(moduleCode: string, screenCode: string) {
        return this.http
            .post(this.getScreenDetailUserUrl, JSON.stringify({ moduleCode: moduleCode, screenCode: screenCode }), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

            
    }
     getScreenDetailComman(moduleCode: string, screenCode: string) {
        
        return this.http
            .post(this.getScreenDetailUserCommanUrl, JSON.stringify({ moduleCode: moduleCode, screenCode: screenCode }), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    

    ScreenLockAndCheckEditAccess(screenCode: string)
    {
        return this.http
        .post(this.screenLockAndCheckEditAccessUrl, JSON.stringify({ screenCode: screenCode,userId: this.userData.userId}), { headers: this.headers })
        .toPromise()
        .then(res => res.json())
        .catch(this.handleError);
    }

        updateScreenCode(screenCode: string)
        {
            return this.http
            .post(this.screenLockAndCheckEditAccessUrl, JSON.stringify({ screenCode: screenCode,userId: this.userData.userId}), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }

    ValidateScreen(screenCode: string)
    {
            return this.http
            .post(this.screenLockAndCheckEditAccessUrl, JSON.stringify({ screenCode: screenCode,userId: this.userData.userId}), { headers: this.headers })
            .toPromise()
            .then(res => 
                {
                    var data = res.json()
            if(data.btiMessage.messageShort == 'SCREEN_ALREADY_LOCKED')
            {
                alert(data.btiMessage.message);
                        //this.router.navigate(["dashboard"]);
                        return true;
            }
                    else{
                        return false;
                    }
                })
                .catch(this.handleError);

    }

    setCurrentModule(moduleId:string) {
      debugger;
        this.moduleId = moduleId
        this.getSideBarDetail(moduleId).subscribe(data=>{
            this.sidebarValues = data.result[0];
            Constants.currentModule = moduleId;
            this.state$.next(this.sidebarValues);
        });
    }

    getCurrentModule() {
         return this.state$.asObservable();
    }

    validateUser(moduleCode,screenCode)
    {
         return this.http
        .get(this.getHeaderDetailUrl, { headers: this.headers})
        .map(
           res => res.json()
        );
    }

    getCommonConstantList() {
        return this.http
            .get(this.getCommonConstantListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
     //Get Active Session
    UpdateActiveSession()
    {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        
        if(userData != null){
             return this.http.post(this.updateActiveSessionUrl,JSON.stringify({'userId':userData.userId,'session': userData.session}), {headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
        }
        
    }

    getAllConstantList()
    {
      
         this.getCommonConstantList().then(data => {
          

            Constants.isDataFatch=true;
            this.constantsList = data.result;
            Constants.atATimeText = data.result.atATimeText;
            Constants.selectedText=data.result.selectedText;
            Constants.totalText=data.result.totalText;
            Constants.deleteConfirmationText=data.result.deleteConfirmationText;
            Constants.serverErrorText=data.result.serverErrorText;
            Constants.deleteCompanyAssociatedMessage=data.result.deleteCompanyAssociatedMessage;
            Constants.deleteSuccessMessage=data.result.deleteSuccessMessage;
            Constants.butText=data.result.butText;
            Constants.ConfirmPasswordText=data.result.ConfirmPasswordText;
            Constants.alertmessage=data.result.alertmessage;
            Constants.validationMessage=data.result.validationMessage;
            Constants.selectAll=data.result.selectAll;
            Constants.unselectAll=data.result.unselectAll;
            Constants.selectUser=data.result.selectUser;
            Constants.selectDays=data.result.selectDays;
            Constants.selectRoles=data.result.selectRoles;
            Constants.selectCompany=data.result.selectCompany;
            Constants.selectUserGroup=data.result.selectUserGroup;
            Constants.select=data.result.select;
            Constants.activeUsers=data.result.activeUsers;
            Constants.inactiveUsers=data.result.inactiveUsers;
            Constants.userGroups=data.result.userGroups;
            Constants.roleGroups=data.result.roleGroups;
            Constants.roles=data.result.roles;
            Constants.passwordPolicy=data.result.passwordPolicy;
            Constants.invalidPassword=data.result.invalidPassword;
            Constants.requiredValid=data.result.requiredValid;
            Constants.tableViewtext=data.result.tableViewtext;
            Constants.msgText=data.result.msgText;
            Constants.firstdateGreaterMsg=data.result.firstdateGreaterMsg;
            Constants.lastdateGreaterMsg=data.result.lastdateGreaterMsg;
            Constants.quarter=data.result.quarter;
            Constants.firstQuarter=data.result.firstQuarter;
            Constants.secondQuarter=data.result.secondQuarter;
            Constants.thirdQuarter=data.result.thirdQuarter;
            Constants.forthQuarter=data.result.forthQuarter;
            Constants.selectAccount=data.result.selectAccount;
            Constants.accountNumberMissing=data.result.accountNumberMissing;
            Constants.confirmationModalTitle=data.result.confirmationModalTitle;
            Constants.confirmationModalBody=data.result.confirmationModalBody;
            Constants.deleteConfirmationModalBody=data.result.deleteConfirmationModalBody;
            Constants.OkText=data.result.OkText;
            Constants.CancelText=data.result.CancelText;
            Constants.EmptyMessage=data.result.EmptyMessage;
            Constants.btnCancelText=data.result.btnCancelText;
            Constants.accountDescription=data.result.accountDescription;     
            Constants.ValidationToFromValue=data.result.ValidationToFromValue;   
            Constants.fillAllFields=data.result.fillAllFields;
            Constants.minimumRange=data.result.minimumRange;
            Constants.maximumRange=data.result.maximumRange;
            Constants.fromPeriod=data.result.fromPeriod;
            Constants.toPeriod=data.result.toPeriod;
            Constants.toValue=data.result.toValue;
            Constants.toGreaterThan=data.result.toGreaterThan;
            Constants.toLessThan=data.result.toLessThan;
            Constants.onePeriod=data.result.onePeriod;
            Constants.twoPeriod=data.result.fromPeriod;
            Constants.lessThan=data.result.lessThan;
            Constants.greaterThan=data.result.greaterThan;
            Constants.maxrangeGreater=data.result.maxrangeGreater;
            Constants.greaterorEqualValue=data.result.greaterorEqualValue;
            Constants.minrange=data.result.minrange;
            Constants.twoPeriod=data.result.twoPeriod;
            Constants.agingPeriod=data.result.agingPeriod;
            Constants.sevenAgingPeriod=data.result.sevenAgingPeriod;
            Constants.missingAccount=data.result.missingAccount;
            Constants.yearAndSeries=data.result.yearAndSeries;
            Constants.oneRadio=data.result.oneRadio;
            Constants.InvalidDate=data.result.InvalidDate;
            Constants.InvalidMonth=data.result.InvalidMonth;
            Constants.InvalidDay=data.result.InvalidDay;
            Constants.mustBe=data.result.mustBe;
            Constants.footervalue=data.result.FooterValue;
            Constants.ForbiddenMsg=data.result.ForbiddenMsg; 
            Constants.currentPeriodLessthanPrv=data.result.currentPeriodLessthanPrv;
            Constants.currentPeriodgreaterthanPrv=data.result.currentPeriodgreaterthanPrv;
            Constants.invalidPeriodDate=data.result.invalidPeriodDate; 
            Constants.fillDate=data.result.fillDate; 
            Constants.AmountText=data.result.AmountText; 
            Constants.PercentageText=data.result.PercentageText; 
            Constants.YesText=data.result.YesText;
            Constants.NoText=data.result.NoText;
            Constants.logoutConfirmation=data.result.logoutconfirmationText;
        });
    }

    onlyDecimalNumberKey(event) {
        let e = event;
        if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
          // let it happen, don't do anything
          return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

 
    getchangeColumnVisibleStatus(data) {        
        return this.http
            .put(this.getchangeColumnVisibleStatusUrl,{ 'columnList': data }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    gethideAllColumns(data) {        
        return this.http
            .put(this.gethideAllColumnsUrl,data, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    geShowAllColumns(data) {        
        return this.http
            .put(this.geShowAllColumnsUrl,data, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    getResetGrid(data) {        
        return this.http
            .put(this.geResetGridUrl,data, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    getCurrentMenus(moduleId:string) {
        debugger;
          this.moduleId = moduleId
       return   this.getSideBarDetail(moduleId).subscribe();
      }
}