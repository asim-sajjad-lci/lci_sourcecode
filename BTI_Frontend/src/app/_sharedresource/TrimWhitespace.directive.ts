import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[TrimWhitespace]'
})
export class TrimWhitespace {

  constructor(private el: ElementRef) { }

  @Input() TrimWhitespace: boolean;

  @HostListener('keydown', ['$event']) onKeyDown(event) {
    if(event.target.value == '' && event.keyCode == 32)
    {
      return false;
    }
  }
}


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/