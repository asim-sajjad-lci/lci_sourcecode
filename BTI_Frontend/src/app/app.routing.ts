import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { LoginComponent } from './authentications/_components/login/login.component';
import { VerifyOtpComponent } from './authentications/_components/verify-otp/verify-otp.component';
import { SelectCompanyComponent } from './authentications/_components/select-company/select-company.component';
import { ForgotPasswordComponent } from './authentications/_components/forgot-password/forgot-password.component';

export const AppRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
      path: '', children: [
          // { path: '', component: LoginHeaderComponent, outlet: 'header' },
          // { path: '', component: LoginSidebarComponent, outlet: 'sidebar' },
          // { path: 'login', loadChildren: './authentications/login/login.module#LoginModule' },
          { path: 'login', component: LoginComponent },
          { path: 'verifyotp/:userId/:otp/:isResetPassword', component: VerifyOtpComponent },
          // { path: 'verifyotp/:userId/:otp/:isResetPassword', component: VerifyOtpComponent },
          // { path: 'forgotpassword', component: ForgotPasswordComponent },
          // { path: 'resetpassword/:userId', component: ResetPasswordComponent },
           { path: 'selectCompany', component: SelectCompanyComponent  },
           { path: 'forgotPassword', component: ForgotPasswordComponent  },
          
      ]
  },

  
  {



  
  path: '',
  component: AdminLayoutComponent,
  children: [
    // {
    //   path: '',
    //   redirectTo: "dashboard",
    //   pathMatch: 'full'
    // },
    
    {
      path: 'dashboard',
      loadChildren: './dashboard/dashboard.module#DashboardModule'
    },{
      path: 'widget',
      loadChildren: './widget/widget.module#WidgetModule'
    },{
      path: 'basic',
      loadChildren: './components/basic/basic.module#BasicModule'
    },{
      path: 'advance',
      loadChildren: './components/advance/advance.module#AdvanceModule'
    },{
      path: 'animations',
      loadChildren: './animations/animations.module#AnimationsModule'
    },{
      path: 'forms',
      loadChildren: './components/forms/forms.module#FormsModule'
    },{
      path: 'bootstrap-table',
      loadChildren: './components/tables/bootstrap-table/bootstrap-table.module#BootstrapTableModule',
    },{
      path: 'data-table',
      loadChildren: './components/tables/data-table/data-table.module#DataTableModule',
    },{
      path: 'map',
      loadChildren: './map/map.module#MapModule',
    },{
      path: 'charts',
      loadChildren: './charts/charts.module#ChartsModule',
    },{
      path: 'maintenance/error',
      loadChildren: './maintenance/error/error.module#ErrorModule'
    },{
      path: 'maintenance/coming-soon',
      loadChildren: './maintenance/coming-soon/coming-soon.module#ComingSoonModule'
    },
    // {
    //   path: 'user',
    //   loadChildren: './user/user.module#UserModule'
    // },
    {
      path: 'crm-contact',
      loadChildren: './components/crm-contact/crm-contact.module#CrmContactModule'
    },{
      path: 'task',
      loadChildren: './components/task/task.module#TaskModule'
    },{
      path: 'editor',
      loadChildren: './components/editor/editor.module#EditorModule'
    },{
      path: 'invoice',
      loadChildren: './components/invoice/invoice.module#InvoiceModule'
    },{
      path: 'file-upload',
      loadChildren: './components/file-upload/file-upload.module#FileUploadModule'
    },{
      path: 'change-log',
      loadChildren: './change-log/change-log.module#ChangeLogModule'
    },{
      path: 'simple-page',
      loadChildren: './simple-page/simple-page.module#SimplePageModule'
    },{
      path: 'module_list',
      loadChildren: './all-modules/_components/all-modules.module#AllModulesModule'
     
    },{
      path: 'company',
      loadChildren: './userModule/_components/company-list/company-list.module#CompanyListModule'
      
      
    },{
      path: 'create-company/:companyId',
      loadChildren: './userModule/_components/create-company/create-company.module#CreateCompanyModule'
      
     
    },{
      path: 'create-company',
      loadChildren: './userModule/_components/create-company/create-company.module#CreateCompanyModule'
      
     
    },{
      path: 'role',
      loadChildren: './userModule/_components/role-list/role-list.module#RoleListModule'
    },{
      path: 'create-role',
      loadChildren: './userModule/_components/create-role/create-role.module#CreateRoleModule'
    },{
      path: 'create-role/:roleId',
      loadChildren: './userModule/_components/create-role/create-role.module#CreateRoleModule'
    },{
      path: 'rolegroup',
      loadChildren: './userModule/_components/role-group-list/role-group-list.module#RoleGroupListModule'
    },{
      path: 'create-rolegroup',
      loadChildren: './userModule/_components/create-role-group/create-role-group.module#CreateRoleGroupModule'
    },{
      path: 'create-rolegroup/:roleGroupId',
      loadChildren: './userModule/_components/create-role-group/create-role-group.module#CreateRoleGroupModule'
    },
    {
      path: 'usergroup',
      loadChildren: './userModule/_components/user-group/user-group.module#UserGroupModule'
    },{
      path: 'create-usergroup',
      loadChildren: './userModule/_components/create-user-group/create-user-group.module#CreateUserGroupModule'
    },{
      path: 'create-usergroup/:userGroupId',
      loadChildren: './userModule/_components/create-user-group/create-user-group.module#CreateUserGroupModule'
    },{
      path: 'user',
      loadChildren: './userModule/_components/user-list/user-list.module#UserListModule'
    },{
      path: 'create-user',
      loadChildren: './userModule/_components/create-user/create-user.module#CreateUserModule'
    },{
      path: 'create-user/:userId',
      loadChildren: './userModule/_components/create-user/create-user.module#CreateUserModule'
    }
    ,{
      path: 'user/:roleId',
      loadChildren: './userModule/_components/create-user/create-user.module#CreateUserModule'
    },
    {
      path: 'authSetting',
      loadChildren: './userModule/_components/auth-setting/auth-setting.module#AuthSettingModule'
    },{
      path: 'create-authSetting',
      loadChildren: './userModule/_components/create-auth-setting/create-auth-setting.module#CreateAuthSettingModule'
    },
   
    {
      path: 'create-authSetting/:authId',
      loadChildren: './userModule/_components/create-auth-setting/create-auth-setting.module#CreateAuthSettingModule'
    },{
      path: 'whiteListIP',
      loadChildren: './userModule/_components/whitelistip-list/whitelistip-list.module#WhitelistipListModule'
    },{
      path: 'create-whiteListIP',
      loadChildren: './userModule/_components/create-white-list-ip/create-white-list-ip.module#CreateWhiteListIPModule'
    },{
      path: 'create-whiteListIP/:WhiteListIP',
      loadChildren: './userModule/_components/create-white-list-ip/create-white-list-ip.module#CreateWhiteListIPModule'
    },{
      path: 'restrictIp',
      loadChildren: './userModule/_components/restrict-ip/restrict-ip.module#RestrictIPModule'
    }
    ,{
      path: 'hcm/division',
      loadChildren: './HcmModule/_components/division-setup-list/division-setup-list.module#DivisionSetupListModule'
    },{
      path: 'createdivsionsetup',
      loadChildren: './HcmModule/_components/create-division-setup/create-division-setup.module#CreateDivisionSetupModule'
    },{
      path: 'createdivsionsetup/:divisionId',
      loadChildren: './HcmModule/_components/create-division-setup/create-division-setup.module#CreateDivisionSetupModule'
    },{
      path: 'hcm/skillsSetup',
      loadChildren: './HcmModule/_components/skill-setup-list/skill-setup-list.module#SkillSetupListModule'
    },{
      path: 'createskillSetup',
      loadChildren: './HcmModule/_components/create-skills-setup/create-skill-setup.module#CreateSkillSetupModule'
    },{
      path: 'createskillSetup/:skillId',
      loadChildren: './HcmModule/_components/create-skills-setup/create-skill-setup.module#CreateSkillSetupModule'
    },{
      path: 'hcm/skillSetSetup',
      loadChildren: './HcmModule/_components/skill-set-setup/skill-set-setup.module#SkillSetSetupModule'
    },{
      path: 'Createskillsetsetup',
      loadChildren: './HcmModule/_components/create-skill-set-setup/create-skill-set-setup.module#CreateSkillSetSetupModule'
    },{
      path: 'Createskillsetsetup/:skillSetId',
      loadChildren: './HcmModule/_components/create-skill-set-setup/create-skill-set-setup.module#CreateSkillSetSetupModule'
    },{
      path: 'hcm/positionClass',
      loadChildren: './HcmModule/_components/position-class/position-class.module#PositionClassModule'
    },{
      path: 'createpositionclass',
      loadChildren: './HcmModule/_components/create-position-class/create-position-class.module#CreatePositionClassModule'
    }
    ,{
      path: 'createpositionclass/:positionClassId',
      loadChildren: './HcmModule/_components/create-position-class/create-position-class.module#CreatePositionClassModule'
    },{
      path: 'hcm/positionSetup',
      loadChildren: './HcmModule/_components/position-setup/position-setup.module#PositionSetupModule'
    },{
      path: 'createpositionSetup',
      loadChildren: './HcmModule/_components/create-position-setup/create-position-setup.module#CreatePositionSetupModule'
    },{
      path: 'createpositionSetup/:positionId',
      loadChildren: './HcmModule/_components/create-position-setup/create-position-setup.module#CreatePositionSetupModule'
    },{
      path: 'hcm/department',
      loadChildren: './HcmModule/_components/department-setup/department-setup.module#DepartmentSetupModule'
    },{
      path: 'createdepartmentsetup',
      loadChildren: './HcmModule/_components/create-department-setup/create-department-setup.module#CreateDepartmentSetupModule'
    },{
      path: 'createdepartmentsetup/:departmentId',
      loadChildren: './HcmModule/_components/create-department-setup/create-department-setup.module#CreateDepartmentSetupModule'
    },{
      path: 'hcm/location',
      loadChildren: './HcmModule/_components/location/location.module#LocationModule'
    },{
      path: 'createlocationsetup',
      loadChildren: './HcmModule/_components/create-location-setup/create-location.module#CreateLocationModule'
    }
    ,{
      path: 'createlocationsetup/:locationId',
      loadChildren: './HcmModule/_components/create-location-setup/create-location.module#CreateLocationModule'
    },{
      path: 'hcm/supervisor',
      loadChildren: './HcmModule/_components/supervisor-setup/supervisor-setup.module#SupervisorSetupModule'
    },{
      path: 'createSupervisorVisorSetup',
      loadChildren: './HcmModule/_components/create-supervisor-setup/create-supervisor-setup.module#CreateSupervisorSetupModule'
    },{
      path: 'createSupervisorVisorSetup/:supervisorId',
      loadChildren: './HcmModule/_components/create-supervisor-setup/create-supervisor-setup.module#CreateSupervisorSetupModule'
    },{
      path: 'hcm/payCodeSetup',
      loadChildren: './HcmModule/_components/paycode-setup/paycode-setup.module#PaycodeSetupModule'
    },{
      path: 'createpaycodesetup',
      loadChildren: './HcmModule/_components/create-paycode-setup/create-paycode-setup.module#CreatePaycodeSetupModule'
    },{
      path: 'createpaycodesetup/:payCodeId',
      loadChildren: './HcmModule/_components/create-paycode-setup/create-paycode-setup.module#CreatePaycodeSetupModule'
    } ,{
      path: 'createTicketRouteSetup/:id',
      loadChildren: './HcmModule/_components/create-ticket-route-setup/create-ticket-route-setup.module#CreateTicketRouteSetupModule'
    },{
      path: 'createTicketRouteSetup',
      loadChildren: './HcmModule/_components/create-ticket-route-setup/create-ticket-route-setup.module#CreateTicketRouteSetupModule'
    },{
      path: 'hcm/ticketroutesetup',
      loadChildren: './HcmModule/_components/ticket-route-setup-list/ticket-route-setup-list.module#TicketRouteSetupListModule'
    },{
      path: 'hcm/benefitCodeSetup',
      loadChildren: './HcmModule/_components/benefit-code-setup/benefit-code-setup.module#BenefitCodeSetupModule'
    },{
      path: 'createbenefitcodesetup/:benefitId',
      loadChildren: './HcmModule/_components/create-benefit-code-setup/create-benefit-code.module#CreateBenefitCodeModule'
    },{
      path: 'createbenefitcodesetup',
      loadChildren: './HcmModule/_components/create-benefit-code-setup/create-benefit-code.module#CreateBenefitCodeModule'
    },{
      path: 'hcm/deductionCodeSetup',
      loadChildren: './HcmModule/_components/deduction-code-setup/deduction-code-setup.module#DeductionCodeSetupModule'
    },{
      path: 'hcm/createdeductioncodesetup',
      loadChildren: './HcmModule/_components/create-deduction-code-setup/create-deduction-code-setup.module#CreateDeductionCodeSetupModule'
    },{
      path: 'createdeductioncodesetup/:diductionId',
      loadChildren: './HcmModule/_components/create-deduction-code-setup/create-deduction-code-setup.module#CreateDeductionCodeSetupModule'
    },{
      path: 'createTicketClassSetup/:id',
      loadChildren: './HcmModule/_components/create-ticket-class-setup/create-ticket-class-setup.module#CreateTicketClassSetupModule'
    },{
      path: 'createTicketClassSetup',
      loadChildren: './HcmModule/_components/create-ticket-class-setup/create-ticket-class-setup.module#CreateTicketClassSetupModule'
    },{
      path: 'hcm/ticketclasssetup',
      loadChildren: './HcmModule/_components/ticket-class-setup-list/ticket-class-setup-list.module#TicketClassSetupListModule'
    },{
      path: 'createTicketPriceSetup/:id',
      loadChildren: './HcmModule/_components/create-ticket-price-setup/create-ticket-price-setup.module#CreateTicketPriceSetupModule'
    },{
      path: 'createTicketPriceSetup',
      loadChildren: './HcmModule/_components/create-ticket-price-setup/create-ticket-price-setup.module#CreateTicketPriceSetupModule'
    },{
      path: 'hcm/ticketpricesetup',
      loadChildren: './HcmModule/_components/ticket-price-setup-list/ticket-price-setup-list.module#TicketPriceSetupListModule'
    },{
      path: 'hcm/employeeMaster',
      loadChildren: './HcmModule/_components/employee-master/employee-master.module#EmployeeMasterModule'
    },{
      path: 'createEmployeeMasterSetup',
      loadChildren: './HcmModule/_components/create-employee-master-setup/create-employee-master-setup.module#CreateEmployeeMasterSetupModule'
    },{
      path: 'createVacationTypeSetup/:id',
      loadChildren: './HcmModule/_components/create-vacation-type-setup/create-vacation-type-setup.module#CreateVacationTypeSetupModule'
    },{
      path: 'createVacationTypeSetup',
      loadChildren: './HcmModule/_components/create-vacation-type-setup/create-vacation-type-setup.module#CreateVacationTypeSetupModule'
    },{
      path: 'hcm/vacationtypesetup',
      loadChildren: './HcmModule/_components/vacation-type-setup-list/vacation-type-setup-list.module#VacationTypeSetupListModule'
    }                          
    
  ]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [
    //  {
    //   path: '',
    //   redirectTo: '/login',
    //   pathMatch: 'full'
    // },
    // {
    //   path: 'login',
    //   loadChildren: './authentications/login/login.module#LoginModule'
  
    // },
    {
      path: 'authentication',
      loadChildren: './authentication/authentication.module#AuthenticationModule'
    },
   
  {
    path: 'authentication',
    loadChildren: './authentication/authentication.module#AuthenticationModule'
  },{
    path: 'error',
    loadChildren: './error/error.module#ErrorModule'
  },{
    path: 'landing',
    loadChildren: './landing/landing.module#LandingModule'
  },{
    path: 'maintenance/offline-ui',
    loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
  },]
}, {
  path: '**',
  redirectTo: 'error/404'
}];