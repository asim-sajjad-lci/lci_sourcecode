import { TestBed, inject } from '@angular/core/testing';

import { RestrictIPService } from './restrict-ip.service';

describe('RestrictIPService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestrictIPService]
    });
  });

  it('should be created', inject([RestrictIPService], (service: RestrictIPService) => {
    expect(service).toBeTruthy();
  }));
});
