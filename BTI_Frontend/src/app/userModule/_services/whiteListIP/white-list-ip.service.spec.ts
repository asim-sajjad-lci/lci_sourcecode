import { TestBed, inject } from '@angular/core/testing';

import { WhiteListIPService } from './white-list-ip.service';

describe('WhiteListIPService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WhiteListIPService]
    });
  });

  it('should be created', inject([WhiteListIPService], (service: WhiteListIPService) => {
    expect(service).toBeTruthy();
  }));
});
