

/**
  * A service class for usergroup
  */
 import { Injectable } from '@angular/core'; 
 import { Headers, Http, RequestOptions } from '@angular/http'; 
 import { Observable } from "rxjs"; import 'rxjs/add/operator/toPromise'; import 'rxjs/Rx';
 import { PagedData } from '../../../_sharedresource/paged-data';
 import { Page } from '../../../_sharedresource/page'; 
 import { UserGroup } from "../../_models/usergroup/usergroup";

 import { IMultiSelectOption } from 'angular-4-dropdown-multiselect'; import { Constants } from '../../../_sharedresource/Constants';
   
 
 @Injectable()
 
 export class UserGroupService
 {
     private headers = new Headers({ 'content-type': 'application/json' });
     private roleGroupList = Constants.userModuleApiBaseUrl +'group/getRoleGroupList';
     private CompanyList = Constants.userModuleApiBaseUrl +'company/getAll';
     private getAllUserGroup =  Constants.userModuleApiBaseUrl + 'group/getAllUserGroup';
     private getUserGroup =  Constants.userModuleApiBaseUrl + 'group/getUserGroup';
     private createUserGroup = Constants.userModuleApiBaseUrl + 'group/saveUserGroup';
     private updateUserGroup = Constants.userModuleApiBaseUrl +'group/updateUserGroup';
     private deleteUserGroup = Constants.userModuleApiBaseUrl +'group/deleteUserGroup';
     private searchUserGroups = Constants.userModuleApiBaseUrl +'group/searchUserGroups';
 
     //initializing parameter for constructor
     constructor(private http: Http) {
       var userData = JSON.parse(localStorage.getItem('currentUser'));
       this.headers.append('session', userData.session);
       this.headers.append('userid', userData.userId);
       var currentLanguage = localStorage.getItem('currentLanguage') ?
             localStorage.getItem('currentLanguage') : "1";
         this.headers.append("langid", currentLanguage);
         this.headers.append("tenantid", localStorage.getItem('tenantid'));
     }
 
    //getting list of all rolegroup
     getRoleGroupList(){
 
          return this.http.put(this.roleGroupList,{"pageNumber":0,"pageSize":500},{headers:this.headers})
             .map(data => this.getRoleGroupData(data.json().result));
             
     }
     //getting lits of all company
     getCompanyList(pageNumber,pageSize){
              return this.http.put(this.CompanyList,{"pageNumber":pageNumber,"pageSize":pageSize},{headers:this.headers})
             .map(data => data.json().result);
     }
 
     //transforming data into IMultiSelectOption format 
     private getRoleGroupData(data: any): IMultiSelectOption[] {
         let roleDataObj: IMultiSelectOption;
         let roleData = new Array<IMultiSelectOption>();
         for (let i = 0; i < data.records.length; i++) {
             roleDataObj = { 'id': data.records[i].id, 'name': data.records[i].roleGroupName };
             roleData.push(roleDataObj);
         }
         return roleData;
     }
     
     //getting list of all usergroup
     getAllGroup(page:Page, searchKeyword):Observable<PagedData<UserGroup>>{
           return this.http.put(this.getAllUserGroup,{'searchKeyword': searchKeyword,'pageNumber': page.pageNumber,'pageSize':page.size }, {headers:this.headers})
              .map(data => this.getPagedData(page,data.json().result));
     }
 
     //get userGroup by ID for edit usergroup
     getGroup(id:string){
         return this.http.post(this.getUserGroup,{id:id},{headers:this.headers})
             .toPromise()
             .then(res => res.json())
             .catch(this.handleError);
     }
 
     //create userGroup
     createGroup(userGroup:UserGroup)
     {
       return this.http.put(this.createUserGroup,JSON.stringify(userGroup),{headers:this.headers})
         .toPromise().then(data => data.json())
         .catch(this.handleError)
     }
     
     //Update userGroup for edit
     updateGroup(userGroup:UserGroup){
         return this.http.post(this.updateUserGroup,JSON.stringify(userGroup),{headers : this.headers})
             .toPromise()
             .then(data => data.json())
             .catch(this.handleError);
     }
 
     //Delete userGroup 
     deleteGroup(ids:any){
         return this.http.post(this.deleteUserGroup,{'deleteIds':ids},{headers:this.headers})
             .toPromise()
             .then(data => data.json())
             .catch(this.handleError);
     } 
 
     //search usergroup
     searchGroup(page: Page,searchKeyword): Observable<PagedData<UserGroup>> {
         return this.http
             .post(this.searchUserGroups, { 'searchKeyword':searchKeyword,'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
             .map(data => this.getPagedData(page, data.json().result));
     }
 
     //Pagination of data 
     private getPagedData(page: Page, data: any): PagedData<UserGroup> {
         let pagedData = new PagedData<UserGroup>();
         if (data) {
             var gridRecords = data.records;
             page.totalElements = data.totalCount;
             page.totalPages = page.totalElements / page.size;
             let start = page.pageNumber * page.size;
             let end = Math.min((start + page.size), page.totalElements);
             for (let i = 0; i < gridRecords.length; i++) {
                 let jsonObj = gridRecords[i];
                 let group = new UserGroup(jsonObj.id,jsonObj.groupCode,jsonObj.roleGroupNames, jsonObj.groupName, jsonObj.groupDesc);
                 pagedData.data.push(group);
             }
             pagedData.page = page;
         }
         return pagedData;
     }
 
     //error handler
     private handleError(error: any): Promise<any> {
         console.error('An error occurred', error); // for demo purposes only
         return Promise.reject(error.message || error);
     }
 }