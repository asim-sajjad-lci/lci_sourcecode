import { TestBed, inject } from '@angular/core/testing';

import { AuthsettingService } from './authsetting.service';

describe('AuthsettingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthsettingService]
    });
  });

  it('should be created', inject([AuthsettingService], (service: AuthsettingService) => {
    expect(service).toBeTruthy();
  }));
});
