/**
 * A service class for authsetting
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';
import { AuthSetting } from "../../_models/authsetting/authsetting";
import { Constants } from '../../../_sharedresource/Constants'
import { IMultiSelectOption } from '../../../../../node_modules/angular-4-dropdown-multiselect';


@Injectable()

export class AuthSettingService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllUrl = Constants.userModuleApiBaseUrl + 'authSettings/getAllAuthSettings';
    private getAuthSettingURL = Constants.userModuleApiBaseUrl + 'authSettings/getAuthSettingById';
    private saveAuthSettingURL = Constants.userModuleApiBaseUrl + 'authSettings/saveAuthSetting';
    private updateAuthSettingURL = Constants.userModuleApiBaseUrl + 'authSettings/updateAuthSetting';
    private getCompanyListUrl = Constants.userModuleApiBaseUrl + 'company/getCompanyListForDropDown';
    private getUserGroupByCompanyUrl = Constants.userModuleApiBaseUrl + 'authSettings/getUserGroupListByCompanyId';
    private getUserListByUserGroupUrl = Constants.userModuleApiBaseUrl + 'authSettings/getUserListByCompanyAndUserGroup';
    private getDaysURL = Constants.userModuleApiBaseUrl + 'authSettings/getDays';
    private deleteAuthSettingURL = Constants.userModuleApiBaseUrl + 'authSettings/deleteAuthSetting';
   
    //initializing parameter for constructor  
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
            localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //save authSetting
    saveAuthSetting(role: AuthSetting) {
        
        return this.http.post(this.saveAuthSettingURL, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //save edited authsetting
    UpdateAuthSetting(role: AuthSetting) {
        return this.http.post(this.updateAuthSettingURL, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //getting details of authsetting
    getAuthSetting(id: string) {
        return this.http.post(this.getAuthSettingURL, { id: id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
    //getting list of company
    getCompanyList() {
        return this.http.put(this.getCompanyListUrl, { },{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    
   //getting list of days
    getDays(){
        return this.http.get(this.getDaysURL, { headers: this.headers })
        .map(data => this.BindDaysData(data.json()));
    }

   //transforming data into IMultiSelectOption format
    private BindDaysData(data: any): IMultiSelectOption[] {
        let roleDataObj: IMultiSelectOption;
        let roleData = new Array<IMultiSelectOption>();
        for (let i = 0; i < data.result.length; i++) {
            roleDataObj = { 'id': data.result[i].weekDayId, 'name': data.result[i].dayName };
            roleData.push(roleDataObj);
        }
        return roleData;
    }

    //getting usergroup list by company
    getUserGroupListByCompany(companyId : number) {
        return this.http.post(this.getUserGroupByCompanyUrl, {id: companyId  },{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

  getUserGroupListByAuth(authorizedUserGroupId : number) {
        return this.http.post(this.getUserGroupByCompanyUrl, {id: authorizedUserGroupId  },{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //getting user list by usergroup
    getUserListByUserGroup(authorizedUserGroupId : string, companyId : number) {
        return this.http.post(this.getUserListByUserGroupUrl, {id: authorizedUserGroupId,companyId:companyId},{ headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    private bindUsers(data: any): IMultiSelectOption[] {
       let roleDataObj: IMultiSelectOption;
        let roleData = new Array<IMultiSelectOption>();
        for (let i = 0; i < data.result.length; i++) {
            roleDataObj = { 'id': data.result[i].userId, 'name': data.result[i].firstName };
            roleData.push(roleDataObj);
        }
        return roleData;
    }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
       //search company 
    getlist(page: Page): Observable<PagedData<AuthSetting>> {
       debugger;
        return this.http
            .post(this.getAllUrl, {'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result))
            .catch(this.handleError);
    }

               //bind json data for view
    private getPagedData(page: Page, data: any): PagedData<AuthSetting> {
        let pagedData = new PagedData<AuthSetting>();
        if (data) {
           var gridRecords= data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let employee = new AuthSetting(jsonObj.id,jsonObj.companyId, jsonObj.authorizedUserGroupId, jsonObj.dayIds,
                    jsonObj.userIds, jsonObj.startDate, jsonObj.endDate, jsonObj.startTime, jsonObj.endTime,jsonObj.daysList,jsonObj.authSettingId,jsonObj.companyName, jsonObj.authorizedGroupName);
                pagedData.data.push(employee);
            }
                pagedData.page = page;
            }
        return pagedData;
            }
    //delete authsetting 
    deleteAuthSetting(ids: any) {
        return this.http.post(this.deleteAuthSettingURL, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    
}
