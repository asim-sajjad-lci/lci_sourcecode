/**
 * A service class for role
 */
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { PagedData } from '../../../_sharedresource/paged-data';
import { Page } from '../../../_sharedresource/page';

import { Constants } from '../../../_sharedresource/Constants'
import { Role } from '../../_models/roles/roles';
import { IMultiSelectOption } from 'angular-4-dropdown-multiselect';

@Injectable()
export class RoleService {
    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllRolesUrl = Constants.userModuleApiBaseUrl + 'group/searchAccessRoles';
    private getRoleDetailUrl = Constants.userModuleApiBaseUrl + 'group/getRoleDetails';
    private createRoleUrl = Constants.userModuleApiBaseUrl + 'group/saveRole';
    private updateRoleUrl = Constants.userModuleApiBaseUrl + 'group/updateRole';
    private deleteRoleUrl = Constants.userModuleApiBaseUrl + 'group/deleteRoles';
    private searchRoleUrl = Constants.userModuleApiBaseUrl + 'group/searchAccessRoles';
    private getListOfModules = Constants.userModuleApiBaseUrl + 'getListOfModules';
    private getlistOfScreenModulesUrl = Constants.userModuleApiBaseUrl + 'listOfScreenModules';
    private getlistOfTransactionModulesUrl = Constants.userModuleApiBaseUrl + 'listOfTransactionModules';
    private getlistOfReportModulesUrl = Constants.userModuleApiBaseUrl + 'listOfReportModules';
    private getlistOfScreenModulesByAccessRoleUrL = Constants.userModuleApiBaseUrl + 'listOfScreenModulesByAccessRole';
    private getlistOfTransactionModulesByAccessRoleUrl = Constants.userModuleApiBaseUrl + 'listOfTransactionModulesByAccessRole';
    private getlistOfReportModulesByAccessRoleUrl = Constants.userModuleApiBaseUrl + 'listOfReportModulesByAccessRole';
    private saveorUpdateAccessRoleScreenAccessUrl = Constants.userModuleApiBaseUrl + 'group/saveorUpdateAccessRoleScreenAccess';
    private saveorUpdateAccessRoleTransactionAccessUrl = Constants.userModuleApiBaseUrl + 'group/saveorUpdateAccessRoleTransactionAccess';
    private saveorUpdateAccessRoleReportAccessUrl = Constants.userModuleApiBaseUrl + 'group/saveorUpdateAccessRoleReportAccess';
    
    //initializing parameter for constructor 
    constructor(private http: Http) {
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }

    //add new role
    createRole(role: Role) {
         
        return this.http.put(this.createRoleUrl, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

     //save or Update AccessRole ScreenAccess
    saveorUpdateAccessRoleScreenAccess(role: Role) {
        ;
        var RequestData={
            accessRoleId :role.accessRoleId,
            modulesList : role.modulesList
        }
        return this.http.put(this.saveorUpdateAccessRoleScreenAccessUrl, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //save or Update AccessRole ScreenAccess
    saveorUpdateAccessRoleTransactionAccess(role: Role) {
        ;
        var RequestData={
            accessRoleId :role.accessRoleId,
            modulesList : role.modulesList
        }
        return this.http.put(this.saveorUpdateAccessRoleTransactionAccessUrl, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //save or Update AccessRole ScreenAccess
    saveorUpdateAccessRoleReportAccess(role: Role) {
        ;
        var RequestData={
            accessRoleId :role.accessRoleId,
            modulesList : role.modulesList
        }
        return this.http.put(this.saveorUpdateAccessRoleReportAccessUrl, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //update role for edit
    updateRole(role: Role) {
        return this.http.post(this.updateRoleUrl, JSON.stringify(role), { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }

    //delete role
    deleteRoles(ids: any) {
        return this.http.post(this.deleteRoleUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(data => data.json())
            .catch(this.handleError);
    }
    
    //get role details
    getRole(id: string) {
        return this.http.post(this.getRoleDetailUrl, { id: id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Module Screen detail By accessRoleId
    getlistOfScreenModulesByAccessRole(id: string) {
        return this.http.post(this.getlistOfScreenModulesByAccessRoleUrL, { accessRoleId: id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Transaction Module detail By accessRoleId
    getlistOfTransactionModulesByAccessRole(id: string) {
        return this.http.post(this.getlistOfTransactionModulesByAccessRoleUrl, { accessRoleId: id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //get Report Module detail By accessRoleId
    getlistOfReportModulesByAccessRole(id: string) {
        return this.http.post(this.getlistOfReportModulesByAccessRoleUrl, { accessRoleId: id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    // needed to bind roles grid
    getAllRoles(page: Page, searchKeyword): Observable<PagedData<Role>> {
        return this.http
            .post(this.getAllRolesUrl, { 'searchKeyword': searchKeyword, 'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result))
            .catch(this.handleError);
    }
    
    //get moduleList
    getModuleList(PageNumber,PageSize): Observable<PagedData<Role>> {
        return this.http
            .post(this.getListOfModules, { 'pageNumber': PageNumber, 'pageSize':PageSize }, { headers: this.headers })
            .map(data => data.json());
    }

    //get screen moduleList 
    getlistOfScreenModules(PageNumber,PageSize) {
        return this.http
            .post(this.getlistOfScreenModulesUrl, { 'pageNumber': PageNumber, 'pageSize':PageSize }, { headers: this.headers })
            .map(data => data.json());
    }

    //get report moduleList 
    getlistOfReportModules(PageNumber,PageSize) {
        return this.http
            .post(this.getlistOfReportModulesUrl, { 'pageNumber': PageNumber, 'pageSize':PageSize }, { headers: this.headers })
            .map(data => data.json());
    }

    //get transaction moduleList
    getlistOfTransactionModules(PageNumber,PageSize){
        return this.http
            .post(this.getlistOfTransactionModulesUrl, { 'pageNumber': PageNumber, 'pageSize':PageSize }, { headers: this.headers })
            .map(data => data.json());
    }

    //pagination of data
    private getPagedData(page: Page, data: any): PagedData<Role> {
      let pagedData = new PagedData<Role>();
        if (data) {
            var gridRecords = data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let role = new Role(jsonObj.id, jsonObj.roleId, jsonObj.roleName, jsonObj.roleDescription,jsonObj.accessRoleId,jsonObj.moduleList);
                pagedData.data.push(role);
            }
            pagedData.page = page;
        }
        return pagedData;
    }

    //needed to bind roles dropdown
    getRolesList() {
        return this.http.get(this.getAllRolesUrl, { headers: this.headers })
            .map(data => this.getRoleData(data.json().result));
    }

    //transforming data into IMultiSelectOption format
    private getRoleData(data: any): IMultiSelectOption[] {
        let roleDataObj: IMultiSelectOption;
        let roleData = new Array<IMultiSelectOption>();
        for (let i = 0; i < data.length; i++) {
            roleDataObj = { 'id': data[i].id, 'name': data[i].roleName };
            roleData.push(roleDataObj);
        }
        return roleData;
    }

    //returning observables
    searchRole(keyword: any) {
        return this.http.post(this.searchRoleUrl, { seachKeyword: keyword }, { headers: this.headers })
            .map(data => data.json())
            .catch(this.handleError);
    }

    //error handler
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}