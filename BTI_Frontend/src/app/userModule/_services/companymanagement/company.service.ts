import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable, BehaviorSubject } from "rxjs";
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import { Page } from '../../../_sharedresource/page';
import { PagedData } from '../../../_sharedresource/paged-data';

import { Constants } from '../../../_sharedresource/Constants';
import { Company } from 'app/userModule/_models/company/company';

@Injectable()

//service class for company
export class CompanyService {

    private headers = new Headers({ 'content-type': 'application/json' });
    private getAllUrl = Constants.userModuleApiBaseUrl + 'company/searchCompanies';
    private createCompanyUrl = Constants.userModuleApiBaseUrl + 'company/create';
    private getCompanyByIdUrl = Constants.userModuleApiBaseUrl + 'company/getCompanyById';
    private updateCompanyUrl = Constants.userModuleApiBaseUrl + 'company/update';
    private deleteCompanyUrl = Constants.userModuleApiBaseUrl + 'company/delete';
    private getCountryListUrl = Constants.userModuleApiBaseUrl + 'getCountryList';
    private getStateListUrl = Constants.userModuleApiBaseUrl + 'getStateListByCountryId';
    private getCityListUrl = Constants.userModuleApiBaseUrl + 'getCityListByStateId';
    private getCountryCodeUrl = Constants.userModuleApiBaseUrl + 'getCountryCodeByCountryId';
    private getCompanyListForDropDown = Constants.userModuleApiBaseUrl + 'company/getCompanyListForDropDown';
    private blockUnblockCompanyUrl = Constants.userModuleApiBaseUrl + 'company/blockUnblockCompany';
    private checkCompanyNameUrl = Constants.userModuleApiBaseUrl + 'company/checkCompanyName';
    private getCompanyByTenatedId = Constants.userModuleApiBaseUrl+'company/getCompanyByTenantId';
    

       //private CompanyName:string;
       private companyName = new BehaviorSubject<string>("");
       currentCompanyName = this.companyName.asObservable();
    
    //initializing parameter for constructor
    constructor (private http: Http) {
        console.log("new Company Service");
        if(localStorage.getItem('currentUser')){
            console.log("test");
        var userData = JSON.parse(localStorage.getItem('currentUser'));
        this.headers.append('session', userData.session);
        this.headers.append('userid', userData.userId);
        var currentLanguage = localStorage.getItem('currentLanguage') ?
        localStorage.getItem('currentLanguage') : "1";
        this.headers.append("langid", currentLanguage);
        this.headers.append("tenantid", localStorage.getItem('tenantid'));
    }
    }

    changeCompanyName(companyName:string){
        this.companyName.next(companyName);
    }

    getOneCompanyByTenatedId(tenatedId:string){
        return this.http.post(this.getCompanyByTenatedId,{"tenantId":tenatedId},{headers:this.headers})
        .map(data  => {
            console.log(data);
            this.changeCompanyName(data.json().result.name);
        })
        .catch(this.handleError);
    }
    getCurrentCompanyName(){
        return this.currentCompanyName;
    }

    //getting list of cities
    getCityList(stateId: any) {
        return this.http.post(this.getCityListUrl, { 'stateId': stateId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting list of states
    getStateList(countryId: any) {
        return this.http.post(this.getStateListUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting list of countrycode
    getCountryCode(countryId: any) {
        return this.http.post(this.getCountryCodeUrl, { 'countryId': countryId }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //getting list of countries
    getCountryList() {
        return this.http.get(this.getCountryListUrl, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //adding new company
    createCompany(company: Company) {
        return this.http.post(this.createCompanyUrl, JSON.stringify(company), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //save edited company 
    updateCompany(company: Company) {
        return this.http.post(this.updateCompanyUrl, JSON.stringify(company), { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
    //delete company 
    deleteCompany(ids: any) {
       
        return this.http.put(this.deleteCompanyUrl, { 'ids': ids }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }
     //blockUnblock company
    blockUnblockCompany(ids: any, IsActive) {
       return this.http.post(this.blockUnblockCompanyUrl, { 'id': ids,'isActive':!IsActive }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
 
    }

     //check Company Name
     checkCompanyName(name: string) {
        return this.http.post(this.checkCompanyNameUrl, { 'name': name}, { headers: this.headers })
             .toPromise()
             .then(res => res.json())
             .catch(this.handleError);
  
     }

    
    //getting company details
    getCompany(id: string) {
        return this.http.post(this.getCompanyByIdUrl, { id: id }, { headers: this.headers })
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }

    //search company 
    getlist(page: Page, searchKeyword): Observable<PagedData<Company>> {
       
        return this.http
            .post(this.getAllUrl, { 'searchKeyword': searchKeyword, 'pageNumber': page.pageNumber, 'pageSize': page.size }, { headers: this.headers })
            .map(data => this.getPagedData(page, data.json().result))
            .catch(this.handleError);
    }

    getlatlng(webAddress: string){		
       return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + webAddress)		
         .toPromise() 		
         .then(res => res.json()) 		
         .catch(this.handleError);		
    }

    //bind json data for view
    private getPagedData(page: Page, data: any): PagedData<Company> {
        let pagedData = new PagedData<Company>();
        if (data) {
           var gridRecords= data.records;
            page.totalElements = data.totalCount;
            page.totalPages = page.totalElements / page.size;
            let start = page.pageNumber * page.size;
            let end = Math.min((start + page.size), page.totalElements);
            for (let i = 0; i < gridRecords.length; i++) {
                let jsonObj = gridRecords[i];
                let employee = new Company(jsonObj.id, jsonObj.name, jsonObj.description, jsonObj.address,
                    jsonObj.isActive, jsonObj.state, jsonObj.stateId, jsonObj.city, jsonObj.cityId, jsonObj.country,
                    jsonObj.countryId, jsonObj.fax, jsonObj.email, jsonObj.phone, jsonObj.zipCode, jsonObj.companyCode,
                    jsonObj.countryCode, jsonObj.webAddress, jsonObj.lat, jsonObj.longitude);
                pagedData.data.push(employee);
            }
                pagedData.page = page;
            }
        return pagedData;
            }

    //error handler
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}