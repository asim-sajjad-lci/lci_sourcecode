// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-create-role',
//   templateUrl: './create-role.component.html',
//   styleUrls: ['./create-role.component.css']
// })
// export class CreateRoleComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import {Component,OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { NgForm } from '@angular/forms';

import {RoleService} from '../../_services/rolemanagement/role.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.css'],
  providers: [RoleService,GetScreenDetailService],
})
// export to make it available for other classes
export class CreateRoleComponent  implements OnInit{
    accessRoleId;
    moduleCode = Constants.userModuleCode;
    screenCode;
    isScreenLock;
    moduleName;
    screenName;
    actionType;
    IsChecked=Boolean;
    // defaultFormValues: [object];
    // availableFormValues: [object];

    defaultFormValues: any=[];
    availableFormValues:any= [];
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    roleId: string;
    moduleId:string;
    lblRole = "Create Role";
    PageNumber: 0;
    PageSize = 100;
    LstModule: any = [];
    LstScreenModule: any = [];
    LstReportModule: any = [];
    LstTransactionModule: any = [];
    ModuleArray: [object]
    showloader:boolean=false;
    showloader2:boolean=false;
    btndisabled:boolean=true;

    clearText = Constants.clearText;
    updateField=Constants.updateField;
    
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private roleService: RoleService,
        private getScreenDetailService: GetScreenDetailService,
        
    ) {
        //  var linkId =  <HTMLAnchorElement>document.getElementById("role");
        //linkId.className="active";
    }
    
    // Screen Initialization
    ngOnInit() {
        
        this.route.params.subscribe((params: Params) => {
            this.roleId = params['roleId'];
            if (this.roleId != '' && this.roleId != undefined) {
                this.screenCode = "S-1028";
                this.lblRole = "Edit Role";
                
                //defaultFormValues for edit Roles screen
                this.defaultFormValues = [
                    { 'fieldName': 'MANAGE_ROLE_ROLE_INFO', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_MODULE', 'fieldValue': '', 'helpMessage': ''  , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_TRANSACTIONS', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_REPORT', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_NAME', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_SAVE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_CANCEL', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_READ', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_WRITE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_DELETE', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_VIEW', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_POST', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_EXPORT', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_EMAIL', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                ];
                this.roleService.getRole(this.roleId).then(data => {
                    
                    this.model = data.result; // filling model
                    this.model.accessRoleId = this.roleId;
                });
                
                // Load Module Screen
                this.GetScreenModuleByAccessRoleId(this.roleId);
                
                // Load Transaction Screen
                this.GetTransactionModuleByAccessRoleId(this.roleId);                
                
                // Load Report Screen
                this.GetReportModuleByAccessRoleId(this.roleId);                
            }
            else {
        
                
                // this.roleService.getModuleList(this.PageNumber, this.PageSize).subscribe(data => {
                //     this.LstModule = data;
                // });
                
                this.GetScreenModule();
                this.GetTransactionModule();
                this.GetReportModule();
                
                this.screenCode = "S-1028";
                //defaultFormValues for add new Roles screen
                this.defaultFormValues = [
                    { 'fieldName': 'MANAGE_ROLE_ROLE_INFO', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_MODULE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_TRANSACTIONS', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_REPORT', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_NAME', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_SAVE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_CANCEL', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_READ', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_WRITE', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_DELETE', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_VIEW', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_POST', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
                    { 'fieldName': 'MANAGE_ROLE_ROLE_EXPORT', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                    { 'fieldName': 'MANAGE_ROLE_ROLE_EMAIL', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
                ];
            }
        });
        
            this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {

                this.moduleName = data.result.moduleName;
                this.screenName = data.result.dtoScreenDetail.screenName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            });
        }
        
        SelectModule(moduleId,actionType,event)
        {
            for(var i=0;i< this.LstScreenModule.length;i++)
            {
                if(actionType == "Read")
                {
                    if(event.target.checked){
                        if(moduleId == this.LstScreenModule[i].moduleId)
                        {
                            this.LstScreenModule[i].readAccess=true;
                            for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                            {
                                this.LstScreenModule[i].screensList[j].readAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].readAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if(moduleId == this.LstScreenModule[i].moduleId)
                        {
                            this.LstScreenModule[i].readAccess=false;
                            for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                            {
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].readAccess=false;
                                }
                            }
                        }
                        
                    }
                }
                if(actionType == "Write")
                {
                    if(event.target.checked){
                        if(moduleId == this.LstScreenModule[i].moduleId)
                        {
                            for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                            {
                                this.LstScreenModule[i].screensList[j].writeAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].writeAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if(moduleId == this.LstScreenModule[i].moduleId)
                        {
                            for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                            {
                                this.LstScreenModule[i].screensList[j].writeAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].writeAccess=false;
                                }
                            }
                        }
                        
                    }
                }
                if(actionType == "Delete")
                {
                    if(event.target.checked){
                        if(moduleId == this.LstScreenModule[i].moduleId)
                        {
                            for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                            {
                                this.LstScreenModule[i].screensList[j].deleteAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].deleteAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if(moduleId == this.LstScreenModule[i].moduleId)
                        {
                            for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                            {
                                this.LstScreenModule[i].screensList[j].deleteAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].deleteAccess=false;
                                }
                            }
                        }
                        
                    }
                }
            }
        }
        
        SelectScreen(screenId,actionType,event)
        {
            
            
            for(var i=0;i< this.LstScreenModule.length;i++)
            {
                if(actionType == "Read")
                {
                    if(event.target.checked){
                        
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                
                                this.LstScreenModule[i].screensList[j].readAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].readAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        let selected = this.LstScreenModule[i].screensList.filter((x) => x.readAccess)
                        
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].readAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].readAccess=false;
                                }
                            }
                        }
                    }
                }
                if(actionType == "Write")
                {
                    if(event.target.checked){
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].writeAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].writeAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].writeAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].writeAccess=false;
                                }
                            }
                        }
                    }
                }
                if(actionType == "Delete")
                {
                    if(event.target.checked){
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].deleteAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].deleteAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].deleteAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].deleteAccess=false;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        SelectField(screenId,actionType,event)
        {
            for(var i=0;i< this.LstScreenModule.length;i++)
            {
                if(actionType == "Read")
                {
                    if(event.target.checked){
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].readAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].readAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            this.LstScreenModule[i].readAccess=false;
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].readAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].readAccess=false;
                                }
                            }
                        }
                    }
                }
                if(actionType == "Write")
                {
                    if(event.target.checked){
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].writeAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].writeAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            this.LstScreenModule[i].writeAccess=false;
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].writeAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].writeAccess=false;
                                }
                            }
                        }
                    }
                }
                if(actionType == "Delete")
                {
                    if(event.target.checked){
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].deleteAccess=true;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].deleteAccess=true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for(var j=0;j<this.LstScreenModule[i].screensList.length;j++)
                        {
                            this.LstScreenModule[i].deleteAccess=false;
                            if(screenId == this.LstScreenModule[i].screensList[j].screenId)
                            {
                                this.LstScreenModule[i].screensList[j].deleteAccess=false;
                                for(var k=0;k<this.LstScreenModule[i].screensList[j].fieldList.length;k++)
                                {
                                    this.LstScreenModule[i].screensList[j].fieldList[k].deleteAccess=false;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        //getting data for role screen module
        GetScreenModule() {
            this.roleService.getlistOfScreenModules(this.PageNumber, this.PageSize).subscribe(data => {
                if (data.btiMessage.messageShort == 'SESSION_EXPIRED') {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.messageText = data.btiMessage.message;
                    window.setTimeout(() => {
                        this.router.navigate(['login']);
                    }, 100);
                }
                this.LstScreenModule = data.result;

                console.log("Modules",this.LstScreenModule)
                
            });
        }
        
        GetReportModule() {
            this.roleService.getlistOfReportModules(this.PageNumber, this.PageSize).subscribe(data => {
                this.LstReportModule = data.result;
            });
        }
        
        GetTransactionModule() {
            this.roleService.getlistOfTransactionModules(this.PageNumber, this.PageSize).subscribe(data => {
                this.LstTransactionModule = data.result;
            });
        }
        
        // Get Screen Module By AccessRoleId 
        GetScreenModuleByAccessRoleId(accessRoleId) {
            this.showloader2=true;
            this.roleService.getlistOfScreenModulesByAccessRole(this.roleId).then(data => {
                this.showloader2=false;
                this.LstScreenModule = data.result;

                console.log("Modules",this.LstScreenModule)
            });
        }
        
        // Get ReportModule By AccessRoleId
        GetReportModuleByAccessRoleId(accessRoleId) {
            this.roleService.getlistOfReportModulesByAccessRole(this.roleId).then(data => {
                this.LstReportModule = data.result;
            });
        }
        
        // Get Transaction Module By AccessRoleId
        GetTransactionModuleByAccessRoleId(accessRoleId) {
            this.roleService.getlistOfTransactionModulesByAccessRole(this.roleId).then(data => {
                this.LstTransactionModule = data.result;
            });
        }
        
        // function call for adding new role
        CreateRole(f: NgForm) {
            this.btndisabled=true;
            if (this.roleId != '' && this.roleId != undefined) {
                this.roleService.updateRole(this.model).then(data => {
                    var datacode = data.code;
                    this.btndisabled=false;
                    if (datacode == 200) {
                        this.model.accessRoleId = data.result.accessRoleId;
                        window.setTimeout(() => {
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            this.showMsg = true;
                            this.messageText = data.btiMessage.message;
                        }, 100);
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                            
                            
                        }, 4000);
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                });
            }
            else {
                this.roleService.createRole(this.model).then(data => {
                    
                    this.btndisabled=false;
                    var datacode = data.code;
                    if (datacode == 201) {
                        if(data.btiMessage.messageShort !='ACCESS_ROLE_NAME_EXIT')
                        {
                            this.model.accessRoleId = data.result.accessRoleId;
                            this.isSuccessMsg = true;
                            this.isfailureMsg = false;
                            f.resetForm();
                        }
                        else{
                            this.isSuccessMsg = false;
                            this.isfailureMsg = true;
                        }
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                        this.hasMsg = true;
                        window.setTimeout(() => {
                            this.showMsg = false;
                            this.hasMsg = false;
                          
                                this.router.navigate(['role']);   
                            
                        }, 4000);
                        
                    }
                }).catch(error => {
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                    }, 100)
                    
                });
            }
        }
        
        showHideGroup(divId:any,level:string,action:string)
        {
            debugger;
            if(action == 'Module')
            {
                if(level == 'Level1')
                {
                    var level1 =<HTMLUListElement>document.getElementById('ul_'+divId);
                    if(level1.className=="visible")
                    {
                        level1.className="hide";
                    }else
                    {
                        level1.className="visible";
                    }
                }
                else{
                    var level2 =<HTMLUListElement>document.getElementById('ulsubgroup_'+divId);
                    if(level2.className=="visible")
                    {
                        level2.className="hide";
                    }else
                    {
                        level2.className="visible";
                    }
                }
            }
            else if(action == 'Transaction')
            {
                if(level == 'Level1')
                {
                    var level1 =<HTMLUListElement>document.getElementById('TR_'+divId);
                    if(level1.className=="visible")
                    {
                        level1.className="hide";
                    }else
                    {
                        level1.className="visible";
                    }
                }
                else{
                    var level2 =<HTMLUListElement>document.getElementById('TRSubGroup_'+divId);
                    if(level2.className=="visible")
                    {
                        level2.className="hide";
                    }else
                    {
                        level2.className="visible";
                    }
                }
            }
            else if(action == 'Report')
            {
                if(level == 'Level1')
                {
                    var level1 =<HTMLUListElement>document.getElementById('Rpt_'+divId);
                    if(level1.className=="visible")
                    {
                        level1.className="hide";
                    }else
                    {
                        level1.className="visible";
                    }
                }
                else{
                    var level2 =<HTMLUListElement>document.getElementById('RptSubGroup_'+divId);
                    if(level2.className=="visible")
                    {
                        level2.className="hide";
                    }else
                    {
                        level2.className="visible";
                    }
                }
            }
            
            
            
            
            //level.style.display = "block";
            
        }
        SaveorUpdateAccessRoleScreenAccess()
        {
            this.showloader=true;
            this.btndisabled=true;
            this.model.modulesList=this.LstScreenModule;
            this.roleService.saveorUpdateAccessRoleScreenAccess(this.model).then(data => {
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 201) {
                    this.isSuccessMsg = true;
                    this.isfailureMsg = false;
                    this.showMsg = true;
                    this.showloader=false;
                    this.messageText = data.btiMessage.message;
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showloader=false;
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
                
            });
        }
        
        SaveorUpdateAccessTransactionScreenAccess()
        {
            this.showloader=true;
            this.btndisabled=true;
            this.model.modulesList=this.LstTransactionModule;
            this.roleService.saveorUpdateAccessRoleTransactionAccess(this.model).then(data => {
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 201) {
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showloader=false;
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
                
            });
        }
        
        SaveorUpdateAccessReportScreenAccess()
        {
            this.btndisabled=true;
            this.showloader=true;
            this.model.modulesList=this.LstReportModule;
            this.roleService.saveorUpdateAccessRoleReportAccess(this.model).then(data => {
                var datacode = data.code;
                this.btndisabled=false;
                if (datacode == 201) {
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.showloader=false;
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
                
            });
        }

        Clear(f: NgForm) {
            // f.resetForm({ country: 0, state: 0, city: 0 });
            // this.name=[];
            this.model=[];
            // this.selectedCountry=[];
            // this.selectedState=[];
            // this.selectedCity=[];
           } 
        
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.btndisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }
        
    }