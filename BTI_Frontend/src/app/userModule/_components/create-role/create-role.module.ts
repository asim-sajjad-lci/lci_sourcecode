
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { CreateRoleComponent } from './create-role.component';
import { CreateRoleRoutes } from './create-role.routing';





@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateRoleRoutes),
    SharedModule,
    NgxDatatableModule,
   
  ],

  
  declarations: [CreateRoleComponent]
})
export class CreateRoleModule { }




