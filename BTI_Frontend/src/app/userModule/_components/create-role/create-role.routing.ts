import { Routes } from '@angular/router';


import { CreateRoleComponent } from './create-role.component';

export const CreateRoleRoutes: Routes = [{
  path: '',
  component: CreateRoleComponent,
  data: {
    breadcrumb: ""
  }
}];