import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Constants } from 'app/_sharedresource/Constants';
import { DatatableComponent } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Router } from '../../../../../node_modules/@angular/router';
import { DOCUMENT } from '../../../../../node_modules/@angular/platform-browser';
import { Page } from 'app/_sharedresource/page';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { AuthSettingService } from 'app/userModule/_services/authsetting/authsetting.service';
import { AuthSetting } from 'app/userModule/_models/authsetting/authsetting';
import { HostListener } from '@angular/core';

@Component({
  selector: 'app-auth-setting',
  templateUrl: './auth-setting.component.html',
  styleUrls: ['./auth-setting.component.css'],
  providers: [AuthSettingService,GetScreenDetailService]
})
export class AuthSettingComponent  {
  page = new Page();
  rows = new Array<AuthSetting>();
  temp = new Array<AuthSetting>();
  selected = [];
  authId = {};
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1029";
  moduleName;
  isScreenLock;
  screenName;
  model: any = {};
  defaultFormValues: any = [];
  availableFormValues: any = [];
  searchKeyword = '';
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  ddPageSize=5;
  atATimeText=Constants.atATimeText;
  selectedText=Constants.selectedText;
  totalText=Constants.totalText;
  isConfirmationModalOpen:boolean=false;
  currentSelectedRow: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction : boolean = false;
  EmptyMessage = Constants.EmptyMessage;
  deleteConfirmationText = Constants.deleteConfirmationText;
  tblWidth=0;
  CreatebtnText=Constants.createButtonText;

  isLongString=false;
  moreDays=0;
  showMoreDays=[];
  showLessDays=[];

  
  @ViewChild(DatatableComponent) table: DatatableComponent;
  
  constructor(
      private router: Router,
      private authSettingService: AuthSettingService,
      private getScreenDetailService: GetScreenDetailService,
      @Inject(DOCUMENT) private document: any
  ) 
  {
      this.page.pageNumber = 0;
      this.page.size = 5;
      this.defaultFormValues = [
          { 'fieldName': 'MANAGE_AUTH_SETTING_COMPANY', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_AUTHORIZED_TO', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_USER', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_DAYS', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_FROM_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_TO_DATE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_FROM_TIME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_TO_TIME', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_EDIT', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_DELETE', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_ADD_NEW', 'fieldValue': '', 'helpMessage': '' ,'listDtoFieldValidationMessage': ''},
          { 'fieldName': 'MANAGE_AUTH_SETTING_SEARCH', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_ACTION', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
          { 'fieldName': 'MANAGE_AUTH_SETTING_AUTH_ID', 'fieldValue': '', 'helpMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
      ];
  }
  
  // Screen initialization 
  ngOnInit() {
      this.setPage({ offset: 0 });
      // getting screen for company
      this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
          {
              this.isScreenLock = res;
          });
          
          this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
              this.moduleName = data.result.moduleName;
              this.screenName=data.result.dtoScreenDetail.screenName;
              this.availableFormValues = data.result.dtoScreenDetail.fieldList;
              for (var j = 0; j < this.availableFormValues.length; j++) {
                  var fieldKey = this.availableFormValues[j]['fieldName'];
                  var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                  var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                  objDefault['fieldValue'] = objAvailable['fieldValue'];
                  objDefault['helpMessage'] = objAvailable['helpMessage'];
                  objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                  objDefault['readAccess'] = objAvailable['readAccess'];
                  objDefault['writeAccess'] = objAvailable['writeAccess'];
              }
          });


        //   window.setTimeout(() => {
        //     let myGrid = document.getElementById('MainGrid');
            
        //     myGrid.style.width = this.tblWidth + "px";
        //     }, 100);
          
      }
      
      // // setting pagination
      
      // setting pagination
      setPage(pageInfo) {
      
          this.selected = []; // remove any selected checkbox on paging
          this.page.pageNumber = pageInfo.offset;
          this.authSettingService.getlist(this.page).subscribe(pagedData => {
            debugger;
              this.page = pagedData.page;
              this.rows = pagedData.data;

              console.log("data",this.rows)
          });
      }


      updateFilter(event) {
        this.searchKeyword = event.target.value.toLowerCase();
        this.page.pageNumber = 0;
        this.setPage({ offset: 0, sortOn: this.page.sortOn, sortBy: this.page.sortBy });
        this.table.offset = 0;
        // this.benefitCodeSetupService.getlist(this.page, this.searchKeyword).subscribe(pagedData => {
        //     this.page = pagedData.page;
        //     this.rows = pagedData.data;
        //     this.table.offset = 0;
        // });
    }
      
      //default list on page
      onSelect({ selected }) {
          this.selected.splice(0, this.selected.length);
          this.selected.push(...selected);
      }
      
      // get company detail by row id
    //   edit(row: any) {
    //       this.authId = row.authSettingId;
    //       this.router.navigate(['authSetting/createauthSetting', this.authId]);  
    //   }

    // edit(row: any) {
    //     debugger;
    //     this.authId = row.authSettingId;
    //     var myurl = `${'create-authSetting'}/${this.authId}`;
    //     this.router.navigateByUrl(myurl);
    // }


    edit(event) {
        debugger;
        if(event.cellIndex!=0){
            this.authId = event.row.authSettingId;
            var myurl = `${'create-authSetting'}/${this.authId}`;
            this.router.navigateByUrl(myurl);
        }
       
    }
      varifyDelete()
      {
        this.isDeleteAction=true;
        this.confirmationModalBody = this.deleteConfirmationText;
        this.isConfirmationModalOpen = true;
      }
      // delete one or multiple companies
      delete() {
          var selectedRoles = [];
          for (var i = 0; i < this.selected.length; i++) {
              selectedRoles.push(this.selected[i].authSettingId);
          }
          this.authSettingService.deleteAuthSetting(selectedRoles).then(data => {
              window.scrollTo(0, 0);
              var datacode = data.code;
              if (datacode == 200) {
                  this.setPage({ offset: 0 });
                  this.closeModal();
                  window.setTimeout(() => {
                      this.isSuccessMsg = true;
                      this.isfailureMsg = false;
                      this.showMsg = true;
                      this.messageText = data.btiMessage.message;
                  }, 100);
                  this.hasMsg = true;
                  window.setTimeout(() => {
                      this.showMsg = false;
                      this.hasMsg = false;
                  }, 4000);
              }
          }).catch(error => {
              this.hasMsg = true;
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
              }, 100)
          });
      }
      
      changePageSize(event) {
          this.page.size = event.target.value;
          this.setPage({ offset: 0 });
      }
      
      closeModal()
      {
          this.isConfirmationModalOpen = false;
      }
      /** If Screen is Lock then prevent user to perform any action.
      *  This function also cover Role Management Write acceess functionality */
      LockScreen(writeAccess)
      {
          if(!writeAccess)
          {
              return true
          }
          else if(this.isScreenLock)
          {
              return true;
          }
          else{
              return false;
          }
      }

      goToCreate(){
        this.router.navigate(['create-authSetting']);   
      }

      checkClick(column){
          console.log(column)
      }


      EscapeModal(event){
        
        var key = event.key;
        if(key=="Escape"){
            this.closeModal();
        }
      }

      
      getRowValue(val){
      
       
        this.moreDays=0;
        if(val.length>2){
           
            this.showMoreDays=[];

           
            let com=[];
            this.isLongString=true;
            this.moreDays=0;
           
            for (let i = 0; i < val.length; i++) {
              
                if(i<2){
                    com.push(val[i]); 
                    this.moreDays=val.length-com.length;
                    this.showMoreDays=com;
                }
              
            }   
        }else{
            this.showLessDays=[];
            this.isLongString=false;
            this.showLessDays=val;
        }
    }

  }
  
