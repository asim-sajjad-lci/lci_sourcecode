import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhitelistipListComponent } from './whitelistip-list.component';

describe('WhitelistipListComponent', () => {
  let component: WhitelistipListComponent;
  let fixture: ComponentFixture<WhitelistipListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhitelistipListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhitelistipListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
