import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { RoleGroupListComponent } from './role-group-list.component';
import { RoleGroupListRoutes } from './role-group-list.routing';





@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RoleGroupListRoutes),
    SharedModule,
    NgxDatatableModule,
   
  ],

  
  declarations: [RoleGroupListComponent]
})
export class RoleGroupListModule { }
