import { Routes } from '@angular/router';
import { RoleGroupListComponent } from './role-group-list.component';




export const RoleGroupListRoutes: Routes = [{
  path: '',
  component: RoleGroupListComponent,
  data: {
    breadcrumb: ""
  }
}];