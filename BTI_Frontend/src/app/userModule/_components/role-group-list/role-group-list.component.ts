import { Component, OnInit,ViewChild } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Page } from '../../../_sharedresource/page';

import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';
import { RoleGroupService } from '../../_services/role-group/role-group.service';
import { RoleGroup } from '../../_models/rolegroup/rolegroup';

@Component({
  selector: 'app-role-group-list',
  templateUrl: './role-group-list.component.html',
  styleUrls: ['./role-group-list.component.css'],
  providers: [RoleGroupService,GetScreenDetailService]
})
export class RoleGroupListComponent implements OnInit {
  page = new Page();
  rows = new Array<RoleGroup>();
  temp = new Array<RoleGroup>();
  selected = [];
  roleGroupId = {};
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1011";
  defaultFormValues: any=[];
  availableFormValues: any=[];
  moduleName;
  screenName;
  isScreenLock;
  hasMessage;
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg = false;
  isfailureMsg = false;
  message = { 'type': '', 'text': '' };
  searchKeyword = '';
  ddPageSize = 5;
  atATimeText=Constants.atATimeText;
  selectedText=Constants.selectedText;
  totalText=Constants.totalText;
  isConfirmationModalOpen:boolean=false;
  currentSelectedRow: any;
  confirmationModalTitle = Constants.confirmationModalTitle;
  confirmationModalBody = Constants.deleteConfirmationText;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  isDeleteAction : boolean = false;
  EmptyMessage = Constants.EmptyMessage;
  deleteConfirmationText = Constants.deleteConfirmationText;
  CreatebtnText=Constants.createButtonText;
  moreRoles;
  showMoreRoles;
  isLongString;
  showLessRoles;
  
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
    private router: Router,
    private RoleGroupService: RoleGroupService,
    private getScreenDetailService: GetScreenDetailService) {
        this.page.pageNumber = 0;
        this.page.size = 5;
        //default formparameter for rolegroup management
        this.defaultFormValues = [
            { 'fieldName': 'ADD_NEW_GROUP', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'SEARCH_ROLE_GROUP', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_GROUP_ID', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_GROUP_NAME', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_GROUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_GROUP_ROLES', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'USER_ROLE_GROUP', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_GROUP_ACTION', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_GROUP_TABLE_VIEW', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_GROUP_ACTION_VIEW', 'fieldValue': '', 'helpMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ROLE_GROUP_ACTION_EDIT', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ROLE_GROUP_BUTTON_DELETE', 'fieldValue': '', 'helpMessage': '' , 'readAccess':'' , 'writeAccess':''},
        ];
    }
    // Screen initialization
    ngOnInit() {
        this.setPage({ offset: 0 });
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                this.moduleName = data.result.moduleName;
                this.screenName = data.result.dtoScreenDetail.screenName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                }
            });
        }
        
        // // setting pagination
        // setPage(pageInfo) {
        //     this.selected = []; // remove any selected checkbox on paging
        //     this.page.pageNumber = pageInfo.offset;
        //     this.RoleGroupService.getAllGroup(this.page).subscribe(pagedData => {
        //         this.page = pagedData.page;
        //         this.rows = pagedData.data;
        //     });
        // }
        //default list on page
        onSelect({ selected }) {
            this.selected.splice(0, this.selected.length);
            this.selected.push(...selected);
        }
        
       


         // get company detail by row id
//   edit(row: any) {
//     this.roleGroupId = row.id;
   

//     var myurl = `${'create-rolegroup'}/${this.roleGroupId}`;
//   this.router.navigateByUrl(myurl);
// }

edit(event) {

    if(event.cellIndex!=0){
        this.roleGroupId = event.row.id;
        var myurl = `${'create-rolegroup'}/${this.roleGroupId}`;
      this.router.navigateByUrl(myurl);
    }
    
    
}
        
        varifyDelete()
        {
            this.isDeleteAction=true;
        this.confirmationModalBody = this.deleteConfirmationText;
        this.isConfirmationModalOpen = true;
        }
        //delete one or multiple role groups
        delete() {
            var selectedRoles = [];
            for (var i = 0; i < this.selected.length; i++) {
                selectedRoles.push(this.selected[i].id);
            }
            this.RoleGroupService.deleteGroup(selectedRoles).then(data => {
                window.scrollTo(0, 0);
                var datacode = data.code;
                if (datacode == 200) {
                    this.setPage({ offset: 0 });
                    this.closeModal();
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        
        //setting pagination
        setPage(pageInfo) {
            this.selected = []; // remove any selected checkbox on paging
            this.page.pageNumber = pageInfo.offset;
            this.RoleGroupService.searchGroup(this.page, this.searchKeyword).subscribe(pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
            });
        }
        
        // search rolegroup details by group name 
        updateFilter(event) {
            this.searchKeyword = event.target.value.toLowerCase();
            this.page.pageNumber = 0;
            this.page.size = this.ddPageSize;
            this.RoleGroupService.searchGroup(this.page, this.searchKeyword).subscribe(pagedData => {
                
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.table.offset = 0;
            }, error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
        // Set default page size
        changePageSize(event) {
            this.page.size = event.target.value;
            this.setPage({ offset: 0 });
        }
        closeModal()
        {
            this.isConfirmationModalOpen = false;
        }

        goToCreate(){
          this.router.navigate(['create-rolegroup']);   
        }

     
        /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }


        EscapeModal(event){
        
            var key = event.key;
            if(key=="Escape"){
                this.closeModal();
            }
          }


          getRowValue(val){
      
       
            this.moreRoles=0;
            if(val.length>2){
               
                this.showMoreRoles=[];
    
               
                let com=[];
                this.isLongString=true;
                this.moreRoles=0;
               
                for (let i = 0; i < val.length; i++) {
                  
                    if(i<2){
                        com.push(val[i]); 
                        this.moreRoles=val.length-com.length;
                        this.showMoreRoles=com;
                    }
                  
                }   
            }else{
                this.showLessRoles=[];
                this.isLongString=false;
                this.showLessRoles=val;
            }
        }

}
