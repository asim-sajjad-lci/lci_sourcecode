import { Routes } from '@angular/router';
import { UserGroupComponent } from './user-group.component';




export const UserGroupRoutes: Routes = [{
  path: '',
  component: UserGroupComponent,
  data: {
    breadcrumb: ""
  }
}];