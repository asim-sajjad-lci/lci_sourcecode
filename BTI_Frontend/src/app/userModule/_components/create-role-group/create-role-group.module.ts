
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';


import { CreateRoleGroupRoutes } from './create-role-group.routing';
import { CreateRoleGroupComponent } from './create-role-group.component';
import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';






@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateRoleGroupRoutes),
    SharedModule,
    NgxDatatableModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule
  ],

  
  declarations: [CreateRoleGroupComponent]
})
export class CreateRoleGroupModule { }
