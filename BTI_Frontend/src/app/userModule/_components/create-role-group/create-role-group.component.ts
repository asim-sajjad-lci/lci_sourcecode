import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
 import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';
import {NgForm} from '@angular/forms';

import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';
import { RoleGroupService } from '../../_services/role-group/role-group.service';

@Component({
  selector: 'app-create-role-group',
  templateUrl: './create-role-group.component.html',
  styleUrls: ['./create-role-group.component.css'],
  providers: [RoleGroupService,GetScreenDetailService]
})
export class CreateRoleGroupComponent implements OnInit {
  roleGroupId: string;
    model: any = {};
    lblRole = "Create Group";
    // defaultFormValues: [object];
    // availableFormValues: [object];
    defaultFormValues :any [];
    availableFormValues:any [];
    messageText;
    moduleName;
    screenName;
    moduleCode = Constants.userModuleCode;
    screenCode;
    hasMsg = false;
    showMsg = false;
    showloader = true;
    isSuccessMsg;
    isfailureMsg;
    ddOptions: IMultiSelectOption[];
    LstCompany:IMultiSelectOption[];
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selectRoles = Constants.selectRoles;
    btndisabled:boolean=false;
    isScreenLock;
    roleLabel="lblRoles";

    clearText = Constants.clearText;
    updateField=Constants.updateField;

    ddl_Roles=[];
    selectedRoles=[];
    // Settings configuration
    ddSettings: IMultiSelectSettings = {
        buttonClasses: 'btn dropdown-toggle formm-dd',
        enableSearch: true,
        showCheckAll: true,
        showUncheckAll: true
    };
    // Text configuration
    ddTexts: IMultiSelectTexts = {
        defaultTitle: this.selectRoles,
        checkAll: this.selectAll,
        uncheckAll: this.unselectAll,
    };
    
    constructor(
      private router: Router,
      private route: ActivatedRoute,
      private roleGroupService:RoleGroupService,
      
      private getScreenDetailService: GetScreenDetailService){
      }
      // Screen initialization
      ngOnInit() {
          this.roleGroupService.getRoleList().subscribe(data => {
            //   this.ddOptions=data;
            for(var i=0;i<data.length;i++)
            {
                this.ddl_Roles.push({"id":data[i].id,"itemName":data[i].name });
               
            }

              console.log(this.ddOptions);
          });
          this.route.params.subscribe((params: Params) => {
              this.roleGroupId = params['roleGroupId'];
              if (this.roleGroupId != '' && this.roleGroupId != undefined) {
                  this.screenCode = "S-1013";
                  this.lblRole = "Edit Group";
                  //defaultFormValues for edit Role group screen
                  this.defaultFormValues = [
                      { 'fieldName': 'EDIT_NEW_ROLE_GROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                      { 'fieldName': 'EDIT_NEW_ROLE_ROLES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
                      { 'fieldName': 'EDIT_NEW_ROLE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
                      { 'fieldName': 'EDIT_NEW_ROLE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':'' },
                      { 'fieldName': 'EDIT_NEW_ROLE_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':'' },
                  ];
                  this.roleGroupService.getGroup(this.roleGroupId).then(data => {
                      this.model = data.result; // filling model
                      window.setTimeout(() => {
                         
                          this.model.roleIdList=this.model.roleIds;
                        //   this.selectedRoles=this.model.roleIdList;
                        var Roleslist=[];
                          for(var i=0;i<this.model.roleIdList.length;i++){
                            debugger;
                              var role=this.ddl_Roles.filter(p => p.id == this.model.roleIdList[i]);
                              for(var j=0;j<role.length;j++){
                                Roleslist.push(role[j]);
                              }
                           
                          }

                          this.selectedRoles=Roleslist;
                          
                      }, 100);
                  });
              }
              else {
                  this.model.roleGroupName = "";
                  this.model.roleGroupDescription = "";
                  this.model.id = "";
                  this.screenCode = "S-1012";
                  //defaultFormValues for Add new Role group screen
                  this.defaultFormValues = [
                      { 'fieldName': 'ADD_NEW_ROLE_GROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':'' },
                      { 'fieldName': 'ADD_NEW_ROLE_ROLES', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'readAccess':'' , 'writeAccess':''},
                      { 'fieldName': 'ADD_NEW_ROLE_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'readAccess':'' , 'writeAccess':''},
                      { 'fieldName': 'ADD_NEW_ROLE_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'readAccess':'' , 'writeAccess':''},
                      { 'fieldName': 'ADD_NEW_ROLE_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'readAccess':'' , 'writeAccess':''},
                  ];
              }
          });
          this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
          {
              this.isScreenLock = res;
          });
          this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
              this.moduleName=data.result.moduleName;
              this.screenName=data.result.dtoScreenDetail.screenName;
              this.availableFormValues = data.result.dtoScreenDetail.fieldList;
              for (var j = 0; j < this.availableFormValues.length; j++) {
                  var fieldKey = this.availableFormValues[j]['fieldName'];
                  var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                  var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                  objDefault['fieldValue'] = objAvailable['fieldValue'];
                  objDefault['helpMessage'] = objAvailable['helpMessage'];
                  objDefault['readAccess'] = objAvailable['readAccess'];
                  objDefault['writeAccess'] = objAvailable['writeAccess'];
                  objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
              }
          });


          if (this.roleGroupId != '' && this.roleGroupId != undefined) {
              this.onAutocompleteFocus(this.roleLabel);
          }
      }
      
      onChange() {
          
      }
      
      // function call for adding new role group
      CreateGroup(f:NgForm)
      {
          debugger;
          this.btndisabled=true;




          if(this.selectedRoles.length > 0)
          {
            var selectedRoleIds = [];
            if(this.selectedRoles.length!=0){

                for (var i = 0; i < this.selectedRoles.length; i++) {
                    var role = this.selectedRoles[i];
                    
                     selectedRoleIds.push(role.id);
                }

                this.model.roleIdList=selectedRoleIds;
                //this.model.dayIds=selectedDaysIds;
                //this.model.dayIds=this.selectDays;
            }

              this.showloader=true;
              if (this.roleGroupId != '' && this.roleGroupId != undefined) {
                  this.roleGroupService.updateGroup(this.model).then(data => {
                      this.showloader=false;
                      var datacode = data.code;
                      this.btndisabled=false;
                      if (datacode == 200) {
                          window.setTimeout(() => {
                              this.hasMsg = true;
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.messageText = data.btiMessage.message;;
                          }, 100);
                          window.setTimeout(() => {
                              this.router.navigate(['rolegroup']);
                          }, 2000);
                      }
                      else{
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = data.btiMessage.message;;
                          window.setTimeout(() => {
                              this.hasMsg = false;  
                          }, 4000);
                      }
                  });
              } 
              else{
                  this.roleGroupService.createGroup(this.model).then(data => {
                      window.scrollTo(0,0);
                      this.showloader=false;
                      this.btndisabled=false;
                      var datacode = data.code;
                      if (datacode == 201) {
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.messageText = data.btiMessage.message;;
                          }, 100);
                          window.setTimeout(() => {
                              this.router.navigate(['rolegroup']);
                          }, 2000);
                          this.hasMsg = true;
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                          }, 4000);
                          f.resetForm();
                      }
                      else{
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = data.btiMessage.message;
                          window.setTimeout(() => {
                              this.hasMsg = false;  
                          }, 4000);
                      }
                  });
              }
          }
          else{
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = "Please select Roles'!";
              }, 100);
          }
      }

      goBack(){
        this.router.navigate(['rolegroup']); 
      }


      onAutocompleteFocus(labelId){
        debugger;
         let elem: HTMLElement = document.getElementById(labelId);
         elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
       }
       
       onAutocompleteFocusOut(labelId){
         // let elem: HTMLElement = document.getElementById(labelId);
         // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
         
       }


       Clear(f: NgForm) {
    
        this.model=[];
        this.selectedRoles=[];
     
       }

        /** If Screen is Lock then prevent user to perform any action.
      *  This function also cover Role Management Write acceess functionality */
      LockScreen(writeAccess)
      {
          if(!writeAccess)
          {
              return true
          }
          else if(this.btndisabled)
          {
              return true
          }
          else if(this.isScreenLock)
          {
              return true;
          }
          else{
              return false;
          }
      }

}
