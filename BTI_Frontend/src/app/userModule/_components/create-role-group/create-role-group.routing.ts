import { Routes } from '@angular/router';



import { CreateRoleGroupComponent } from './create-role-group.component';

export const CreateRoleGroupRoutes: Routes = [{
  path: '',
  component: CreateRoleGroupComponent,
  data: {
    breadcrumb: ""
  }
}];