import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAuthSettingComponent } from './create-auth-setting.component';

describe('CreateAuthSettingComponent', () => {
  let component: CreateAuthSettingComponent;
  let fixture: ComponentFixture<CreateAuthSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAuthSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAuthSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
