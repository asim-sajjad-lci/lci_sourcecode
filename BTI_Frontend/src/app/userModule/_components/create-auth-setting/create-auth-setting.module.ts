import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { CreateAuthSettingRoutes } from './create-auth-setting.routing';
import { CreateAuthSettingComponent } from './create-auth-setting.component';
import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/dist/multiselect.component';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateAuthSettingRoutes),
    SharedModule,
    NgxDatatableModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule,
    NgxMyDatePickerModule.forRoot(),
    Ng2AutoCompleteModule,
    DateTimePickerModule
  ],

  
  declarations: [CreateAuthSettingComponent]
})

export class CreateAuthSettingModule { }
