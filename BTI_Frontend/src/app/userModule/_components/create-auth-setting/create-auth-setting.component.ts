import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';
import {NgForm} from '@angular/forms';
import { AuthSetting } from '../../_models/authsetting/authsetting';
import { AuthSettingService } from '../../_services/authsetting/authsetting.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';

import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import * as moment from 'moment';
import {Constants} from '../../../_sharedresource/Constants';
import { CommonService } from 'app/_sharedresource/_services/common-services.service';
import {  Input, ViewChild, ViewChildren, ElementRef, forwardRef, HostListener, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-create-auth-setting',
  templateUrl: './create-auth-setting.component.html',
  styleUrls: ['./create-auth-setting.component.css'],
  providers: [AuthSettingService,GetScreenDetailService,CommonService]
})
// export to make it available for other classes
export class CreateAuthSettingComponent {
    @ViewChild('filterInput') filterInput: ElementRef;
    @ViewChild('filterInput1') filterInput1: ElementRef;
    // @ViewChild('filterInput2') filterInput2: ElementRef;
    // @ViewChild('filterInput3') filterInput3: ElementRef;
    @ViewChildren('listItems') listItems: QueryList<ElementRef>;
   
    get value() {
        return this._value;
      }
    
      set value(val) {
        this._value = val;
      }
  
      _value: string;
      isListHide = true;
      isListHide1 = true;


  moduleName;
  screenName;
  moduleCode = Constants.userModuleCode;
  screenCode;
  defaultFormValues:any = [];
  availableFormValues:any = [];
  SelectedUser: [object];
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  model: any = {};
  roleGroupId: string;
  lblRole = "Create Group";
  companyId=0;
  AuthorizedUserGroupId=0;
  authId: string;
  LstCompany;
  LstUserGroup;
  LstGroup;
  LstUserList;
  ddOptions: IMultiSelectOption[];
  ddUsersOption: IMultiSelectOption[];
  LstDays:IMultiSelectOption[];
  LstUsers:IMultiSelectOption[];
  alertmessage = Constants.alertmessage;
  validationMessage = Constants.validationMessage;
  selectAll = Constants.selectAll; 
  unselectAll = Constants.unselectAll;
//   selectUser =Constants.selectUser;

clearText = Constants.clearText;
updateField=Constants.updateField;
isUnderupdate=false;

selectUser ="";
  selectDays= Constants.selectDays;
  selectedGroup;

  startDate;
  endDate;

  isCompanyOpen=false;
  isAuthtoOpen=false;




  currentUsers;
  
  btndisabled:boolean=false;
  isScreenLock;
  // selectCompany = Constants.selectCompany;
  select=Constants.select; 
  ddl_MultiSelectUser = [];
  ddl_Days=[];
  selectedDays=[];
  SelectedUsers = [];
  ddlCompanySetting = {};
  selectedCompany;
  currentCompany;
  currentUserGroup;
  // Settings configuration
  ddSettings: IMultiSelectSettings = {
      buttonClasses: 'btn dropdown-toggle formm-dd',
      enableSearch: true,
      showCheckAll: true,
      showUncheckAll: true
  };
  // Text configuration
  ddTexts: IMultiSelectTexts = {
      defaultTitle: this.selectUser,
      checkAll: this.selectAll,
      uncheckAll: this.unselectAll,
  };
  
  // DaysText configuration 
  ddDaysTexts: IMultiSelectTexts = {
      defaultTitle: this.selectDays,
      checkAll: this.selectAll,
      uncheckAll: this.unselectAll,
  };
  
  
  private myOptions: INgxMyDpOptions = {
      // other options...
      dateFormat: 'dd/mm/yyyy',
  };
  
  myOptions2: INgxMyDpOptions = {
      // other options...
      dateFormat: 'dd/mm/yyyy',
      disableUntil: {year:0, month: 0, day: 0 }
  };
  
  // Initialized to specific date (09.10.2018)
  private  Object = { date: { year: 2018, month: 10, day: 9 } };
  
  private momentVariable = null;
  
  // optional date changed callback
  onDateChanged(event: IMyDateModel): void {
      // date selected
      this.model.startTime='';
      this.model.endTime='';
      this.myOptions2.disableUntil = {year: event.date.year, month: event.date.month, day: event.date.day - 1 }
  }
  
  onDateChanged2(event: IMyDateModel): void {
      this.model.startTime='';
      this.model.endTime='';
      // date selected
  }
  clearStartDate()
  {
      this.model.startDate='';
      this.model.startTime='';
      this.model.endTime='';
  }
  clearEndDate()
  {
      this.model.endDate='';
      this.model.startTime='';
      this.model.endTime='';
  }
  
  // Returns copy of myOptions
  getCopyOfOptions(): INgxMyDpOptions {
      return JSON.parse(JSON.stringify(this.myOptions2));
  }
  
  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private authSettingService:AuthSettingService,
      private getScreenDetailService: GetScreenDetailService,
      private commonService:CommonService,
  ){
  }
  
  // Screen initialization 
  ngOnInit() {
      
      this.getAllCompany();
      this.getDays();
      
      this.SelectedUser = [{'userId':'0','firstName':''}];
      this.route.params.subscribe((params: Params) => {
          this.authId = params['authId'];
          
          if (this.authId != '' && this.authId != undefined) 
          {
              //defaul form parameter for edit screen
              
              this.defaultFormValues = [
                  
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_AUTHORIZEDTO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_USER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_DATES_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_DATES_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_TIME_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SELECT_TIME_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_EDIT_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
              ];
              
              this.screenCode = "S-1030";
              
            
              
              this.authSettingService.getAuthSetting(this.authId).then(data => {
                
                  this.model = data.result;
                  this.model.companyId = this.model.companyId;
                  this.isUnderupdate=true;

                  setTimeout(() => {
                    this.currentCompany=this.LstCompany.filter(p => p.id == this.model.companyId);
                    this.selectedCompany = this.currentCompany[0].name;
                  }, 1000);
               
                  this.model.authorizedUserGroupId=this.model.authorizedUserGroupId;
                 
                //   this.selectedCompany = this.model.companyId;
                
                  console.log("authdata",this.model)
                  
                 
                  if(data.result.startTime) {
                      this.model.startTime=data.result.startTime;
                  }
                  if(data.result.endTime) {
                      this.model.endTime =data.result.endTime;
                  }
                  var startdate = this.model.startDate;
                  var startdateData = startdate.split('/');
                  this.model.startDate = {"date":{"year":parseInt(startdateData[2]),"month":parseInt(startdateData[1]),"day":parseInt(startdateData[0])}};
                  
                  var enddate = this.model.endDate;
                  var enddateData = enddate.split('/');
                  this.model.endDate = {"date":{"year":parseInt(enddateData[2]),"month":parseInt(enddateData[1]),"day":parseInt(enddateData[0])}};

                  this.SelectedUsers=this.model.usersList;


                  this.changeCompanyEdit();
                
               
                 
                //   this.changeUserGroupEdit();
              });


              window.setTimeout(() => {
                  this.SelectedUsers=[];
                  this.selectedDays=[];
                var allUserLst=this.model.usersList;
                for(var i=0;i<allUserLst.length;i++)
                {
                    this.SelectedUsers.push({"id":allUserLst[i].userId,"itemName":allUserLst[i].firstName +' ' + allUserLst[i].lastName });
                }
    
    
                var daysid=this.model.dayIds;
                var daysName=this.model.daysList;
    
                for(var i=0;i<daysid.length;i++)
                {
                    this.selectedDays.push({"id":daysid[i],"itemName":daysName[i] });
                }

             

                this.onAutocompleteFocus('userLabel');
                this.onAutocompleteFocus('daysLabel');

            }, 1000);
             
          }
          else {
              this.screenCode = "S-1024";
              
              
              // default form parameter for adding company
              this.defaultFormValues = [
                  { 'fieldName': 'AUTHORIZED_SETTING_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_AUTHORIZEDTO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_USER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_SELECT_DAYS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_SELECT_DATES_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_SELECT_DATES_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_SELECT_TIME_FROM', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_SELECT_TIME_TO', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' },
                  { 'fieldName': 'AUTHORIZED_SETTING_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'','readAccess':'','writeAccess':'' }
              ];
          }
          
             this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
              {
                  this.isScreenLock = res;
              });
              //getting screen for adding new company
              this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                  this.screenName=data.result.dtoScreenDetail.screenName;
                  this.moduleName=data.result.moduleName;
                  this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                  for (var j = 0; j < this.availableFormValues.length; j++) {
                      var fieldKey = this.availableFormValues[j]['fieldName'];
                      var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                      var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                      objDefault['fieldValue'] = objAvailable['fieldValue'];
                      objDefault['helpMessage'] = objAvailable['helpMessage'];
                      objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                      objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                      objDefault['readAccess'] = objAvailable['readAccess'];
                      objDefault['writeAccess'] = objAvailable['writeAccess'];
                  }
              });
          });


         
          /*this.ddlCompanySetting = { 
              singleSelection: false, 
              text:this.selectUser,
              disabled : true,
              selectAllText: this.selectAll,
              unSelectAllText: this.unselectAll,
              enableSearchFilter: true,
              classes:"myclass custom-class"
          };  */
      }
      
      onItemSelect(item:any){
          //   this.commonService.closeMultiselect()
      }
      OnItemDeSelect(item:any){
          //  this.commonService.closeMultiselect()
      }
      onSelectAll(items: any){
          // this.commonService.closeMultiselect()
      }
      onDeSelectAll(items: any){
          //  this.commonService.closeMultiselect() 
      }
      toggle() {
       
        this.isListHide = !this.isListHide;
       
          this.filterInput.nativeElement.focus();
         
      }

      toggle1() {
       
        this.isListHide1 = !this.isListHide1;
       
          this.filterInput1.nativeElement.focus();
         
      }
      // get all company
      getAllCompany()
      {
          this.authSettingService.getCompanyList().then(data => {
              
              if(data.btiMessage.messageShort == 'SESSION_EXPIRED')
              {
                  this.isSuccessMsg = true;
                  this.isfailureMsg = false;
                  this.showMsg = true;
                  this.messageText = data.btiMessage.message;
                  window.setTimeout(() => {
                      this.router.navigate(['login']);
                  }, 100);
              }
              this.LstCompany = data.result.records;
              this.LstCompany.splice(0, 0, { "id": "", "name": this.select });
              
          });
          this.model.startTime = '';
          this.model.endTime = '';
          
      }
      
      // select checkbox for user
      SelectUser(option, event)
      {
          if(event.target.checked)
          {
              this.SelectedUser.push(option); 
              
          }
      }
      
      // Get all user group by company 
      changeCompany()
      {
          
          this.SelectedUsers=[];
          this.ddl_MultiSelectUser=[];
          
         this.model.companyId=this.selectedCompany.id;
         // this.selectedCompany=this.selectedCompany;

        // this.model.companyId=this.selectedCompany;
        // this.selectedCompany=this.selectedCompany;
          this.companyId=this.model.companyId;
          this.authSettingService.getUserGroupListByCompany(this.model.companyId).then(data => {
            
              this.LstGroup = data.result;
              
          });
      }

       // Get all user group by company 
       changeCompanyEdit()
       {
           
           this.SelectedUsers=[];
           this.ddl_MultiSelectUser=[];
           
          // this.model.companyId=this.selectedCompany.id;
         //   this.selectedCompany=this.selectedCompany.id;
 
         // this.model.companyId=this.selectedCompany;
         // this.selectedCompany=this.selectedCompany;
           this.companyId=this.model.companyId;
           this.authSettingService.getUserGroupListByCompany(this.model.companyId).then(data => {
             
               this.LstGroup = data.result;
               if(this.LstGroup!=undefined){
                this.currentUserGroup=this.LstGroup.filter(p => p.id == this.model.authorizedUserGroupId);
                this.selectedGroup = this.currentUserGroup[0];
                

              

                this.setAutocompleteLabel();
              
                this.changeUserGroup();




}
             
               
           });
       }
      
      changeAuthsetting()
      {
          this.AuthorizedUserGroupId=this.model.authorizedUserGroupId;
          this.authSettingService.getUserGroupListByAuth(this.model.authorizedUserGroupId).then(data => {
              this.LstGroup = data.result;
          });
      }
      // Get all users group by user group 
      changeUserGroup()
      {
          
         /// this.SelectedUsers=[];
          this.ddl_MultiSelectUser=[];

          this.model.authorizedUserGroupId=this.selectedGroup.id;
        //   this.selectedGroup=this.selectedGroup.id;

        
          this.authSettingService.getUserListByUserGroup(this.model.authorizedUserGroupId,this.companyId).then(data => {
              this.ddUsersOption=this.bindUsers(data.result);
              var allUserLst=data.result;
              for(var i=0;i<allUserLst.length;i++)
              {
                  this.ddl_MultiSelectUser.push({"id":allUserLst[i].userId,"itemName":allUserLst[i].firstName +' ' + allUserLst[i].lastName });
              }
          });


      }
      
      // Get all users group by user group 
      changeUserGroupEdit(usergroupId)
      {
         
    
            this.authSettingService.getUserListByUserGroup(usergroupId,this.companyId).then(data => {
                debugger;
                console.log("result",data)
                window.setTimeout(() => {
                if(data!=undefined){
                   // this.ddUsersOption=this.bindUsers(data.result);
                  var allUserLst=data.result;
                  for(var i=0;i<allUserLst.length;i++)
                  {
                      this.ddl_MultiSelectUser.push({"id":allUserLst[i].userId,"itemName":allUserLst[i].firstName +' ' + allUserLst[i].lastName });
                  } 
                }
                 

              
        
           // this.SelectedUsers=[];
            //this.ddl_MultiSelectUser=[];
  
            }, 4000);
               
              });
    
           
         // this.model.authorizedUserGroupId=this.selectedGroup.id;
        //   this.selectedGroup=this.selectedGroup.id;
       
        
      }
      // Bind user data
      private bindUsers(data: any): IMultiSelectOption[] {
          let roleDataObj: IMultiSelectOption;
          let roleData = new Array<IMultiSelectOption>();
          for (let i = 0; i < data.length; i++) {
              roleDataObj = { 'id': data[i].userId, 'name': data[i].firstName };
              roleData.push(roleDataObj);
          }
          return roleData;
      }
      
      // Get all days
      getDays()
      {
         
          this.authSettingService.getDays().subscribe(data => {
            
              this.ddOptions = data;
            //   this.ddl_Days=data;
              for(var i=0;i<this.ddOptions.length;i++)
              {
                  this.ddl_Days.push({"id":this.ddOptions[i].id,"itemName":this.ddOptions[i].name });
              }
          });
      }
      
      formatstartTime(selectedTime) { 
          
          this.model.startTime = moment(selectedTime).format('HH:mm');
          if(this.model.startDate == undefined || this.model.endDate == undefined || this.model.startDate == '' || this.model.endDate == '')
          {
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.validationMessage;
              this.model.startTime = '';
              window.scrollTo(0,0);
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
              return false;
          }
          this.route.params.subscribe((params: Params) => {
              this.authId = params['authId'];
              
              if (this.authId != '' && this.authId != undefined) {
                  if(this.model.startDate.formatted == undefined)
                  {
                      var startdate = this.model.startDate;
                      if(startdate.date != undefined)
                      {
                          this.model.startDate = startdate.date.day +'/'+ startdate.date.month +'/'+ startdate.date.year;
                      }
                  }
                  else
                  {
                      this.model.startDate = this.model.startDate.formatted;
                  }
                  
                  if(this.model.endDate.formatted == undefined)
                  {
                      var enddate = this.model.endDate;
                      if(enddate.date != undefined)
                      {
                          this.model.endDate = enddate.date.day +'/'+ enddate.date.month +'/'+ enddate.date.year;
                      }
                  }
                  else
                  {
                      this.model.endDate = this.model.endDate.formatted;
                  }
                  if(this.model.startDate == this.model.endDate)
                  {
                      
                      if(this.model.startTime.length > 0 && this.model.endTime.length > 0  && this.model.startTime >=  this.model.endTime )
                      {
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = this.alertmessage;
                          this.model.startTime = '';
                          window.scrollTo(0,0);
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                          }, 4000);
                      }
                      else
                      {
                          this.model.startTime = moment(selectedTime).format('HH:mm');
                      }
                  }
              }
              else
              {
                  if(this.model.startDate.formatted == this.model.endDate.formatted)
                  {
                      
                      
                      if(this.model.startTime.length > 0 && this.model.endTime.length > 0  && this.model.startTime >=  this.model.endTime )
                      {
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = this.alertmessage;
                          this.model.startTime = '';
                          window.scrollTo(0,0);
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                          }, 4000);
                      }
                      else
                      {
                          this.model.startTime = moment(selectedTime).format('HH:mm');
                      }
                  }
                  
              }
          });
          //{year: event.date.year, month: event.date.month, day: event.date.day - 1 }
          
          
      }
      
      formatendTime(selectedTime) { 
          
          this.model.endTime = moment(selectedTime).format('HH:mm');
          if(this.model.startDate == undefined || this.model.endDate == undefined || this.model.startDate == '' || this.model.endDate == '')
          {
              this.isfailureMsg = true;
              this.showMsg = true;
              this.hasMsg = true;
              this.messageText = this.validationMessage;
              this.model.endTime = '';
              window.scrollTo(0,0);
              window.setTimeout(() => {
                  this.showMsg = false;
                  this.hasMsg = false;
              }, 4000);
              return false;
          }
          this.route.params.subscribe((params: Params) => {
              this.authId = params['authId'];
              
              if (this.authId != '' && this.authId != undefined) {
                  if(this.model.startDate.formatted == undefined)
                  {
                      var startdate = this.model.startDate;
                      if(startdate.date != undefined)
                      {
                          this.model.startDate = startdate.date.day +'/'+ startdate.date.month +'/'+ startdate.date.year;
                      }
                  }
                  else
                  {
                      this.model.startDate = this.model.startDate.formatted;
                  }
                  
                  if(this.model.endDate.formatted == undefined)
                  {
                      var enddate = this.model.endDate;
                      if(enddate.date != undefined)
                      {
                          this.model.endDate = enddate.date.day +'/'+ enddate.date.month +'/'+ enddate.date.year;
                      }
                      
                  }
                  else
                  {
                      this.model.endDate = this.model.endDate.formatted;
                  }
                  if(this.model.startDate == this.model.endDate)
                  {
                      
                      if(this.model.startTime.length > 0 && this.model.endTime.length > 0  && this.model.startTime >=  this.model.endTime )
                      {
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = this.alertmessage;
                          this.model.endTime = '';
                          window.scrollTo(0,0);
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                          }, 4000);
                      }
                      else
                      {
                          this.model.endTime = moment(selectedTime).format('HH:mm');
                      }
                  }
              }
              else
              {
                  if(this.model.startDate.formatted == this.model.endDate.formatted)
                  {
                      if(this.model.startTime.length > 0 && this.model.startTime >=  this.model.endTime)
                      {
                          this.isfailureMsg = false;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = this.alertmessage;
                          this.model.endTime = '';
                          window.scrollTo(0,0);
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                          }, 4000);
                      }
                      else
                      {
                          this.model.endTime = moment(selectedTime).format('HH:mm');
                      }
                  }
              }});
          }
          // function call for adding new company
          CreateAuthSetting(f: NgForm) {
debugger;
            
              this.btndisabled=true;

              var selectedDaysIds = [];
            if(this.selectedDays.length!=0){

                for (var i = 0; i < this.selectedDays.length; i++) {
                    var day = this.selectedDays[i];
                    
                    selectedDaysIds.push(day.id);
                }

                this.model.dayIds=selectedDaysIds;
                //this.model.dayIds=this.selectDays;
            }

              if(this.SelectedUsers.length == 0)
              {
                  return false;
              }
              var selectedUserIds = [];
              for (var i = 0; i < this.SelectedUsers.length; i++) {
                  var MyCompany = this.SelectedUsers;
                  
                  selectedUserIds.push(MyCompany[i].id);
              }
              // var ArrSelectedUsers = SelectedUsersIds.join();
              this.model.userIds=selectedUserIds;
              if (this.authId != '' && this.authId != undefined) {
                  if(this.model.startDate.formatted == undefined)
                  {
                      var startdate = this.model.startDate;
                      if(startdate.date != undefined)
                      {
                          this.model.startDate = startdate.date.day +'/'+ startdate.date.month +'/'+ startdate.date.year;
                      }
                      
                  }
                  else
                  {
                      this.model.startDate = this.model.startDate.formatted;
                  }
                  
                  if(this.model.endDate.formatted == undefined)
                  {
                      var enddate = this.model.endDate;
                      if(enddate.date != undefined)
                      {
                          this.model.endDate = enddate.date.day +'/'+ enddate.date.month +'/'+ enddate.date.year;
                      }
                  }
                  else
                  {
                      this.model.endDate = this.model.endDate.formatted;
                  }
                  
                  
                  this.model.id = this.model.authSettingId;
                  this.authSettingService.UpdateAuthSetting(this.model).then(data => {
                      this.btndisabled=false;
                      var datacode = data.code;
                      
                      if (datacode == 200) {
                          
                          window.scrollTo(0,0);
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              // this.hasMsg = true;
                              this.messageText = data.btiMessage.message;
                          }, 100);
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                          }, 4000);
                          window.setTimeout(() => {
                              this.router.navigate(['authSetting']);
                          }, 2000);
                          this.hasMsg = true;
                      }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                      },100)
                      
                  });
              }
              else {
                  
                  this.model.startDate = this.model.startDate.formatted;
                  this.model.endDate = this.model.endDate.formatted;
                  this.authSettingService.saveAuthSetting(this.model).then(data => {
                      this.btndisabled=false;
                      window.scrollTo(0,0);
                      var datacode = data.code;
                      if (datacode == 200) {
                          
                          // window.setTimeout(() => {
                          this.isSuccessMsg = true;
                          this.isfailureMsg = false;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = data.btiMessage.message;
                          // }, 1000);
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                              this.router.navigate(['authSetting']);
                          }, 4000);
                          // f.resetForm();
                      }
                  }).catch(error => {
                      this.hasMsg = true;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                      }, 100)
                  });
              }
          }
          /** If Screen is Lock then prevent user to perform any action.
          *  This function also cover Role Management Write acceess functionality */
          LockScreen(writeAccess)
          {
              if(!writeAccess)
              {
                  return true
              }
              else if(this.btndisabled)
              {
                  return true
              }
              else if(this.isScreenLock)
              {
                  return true;
              }
              else{
                  return false;
              }
          }
          

          companyFocusIn(){
            this.isCompanyOpen=true;
          }
          companyFocusOut(userGroup){
            
            this.isCompanyOpen=false;
          
          }
             
          authFocusIn(){
              this.isAuthtoOpen=true;
          }
          authFocusOut(){
              this.isAuthtoOpen=false;
          }

           

          onAutocompleteFocus(labelId){
           
            let elem: HTMLElement = document.getElementById(labelId);
            elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
          }
          
          onAutocompleteFocusOut(labelId){
            // let elem: HTMLElement = document.getElementById(labelId);
            // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
            
          }

          setAutocompleteLabel(){
          
            if(this.model.companyId)
            {
                let elem: HTMLElement = document.getElementById("companyLabel");
               elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
               debugger;
           }
       
            if(this.model.authorizedUserGroupId)
            {
               let elem: HTMLElement = document.getElementById("UsergroupLabel");
               elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
       
              
            }
            if(this.model.cityId)
            {
               let elem2: HTMLElement = document.getElementById("cityLabel");
               elem2.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
            }
        }


        Clear(f: NgForm) {
            // f.resetForm({ country: 0, state: 0, city: 0 });
           this.selectedCompany=[];
           this.selectedGroup=[];
           this.SelectedUsers=[];
           this.selectedDays=[];
           this.startDate=[];
           this.endDate=[];
            this.model=[];
          }
          
      }
