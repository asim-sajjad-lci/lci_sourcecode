import { Routes } from '@angular/router';


import { CreateAuthSettingComponent } from './create-auth-setting.component';

export const CreateAuthSettingRoutes: Routes = [{
  path: '',
  component: CreateAuthSettingComponent,
  data: {
    breadcrumb: ""
  }
}];
