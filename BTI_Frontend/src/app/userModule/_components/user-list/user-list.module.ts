import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { RoleListRoutes } from 'app/userModule/_components/role-list/role-list.routing';
import { UserListComponent } from './user-list.component';
import { UserListRoutes } from './user-list.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserListRoutes),
    SharedModule,
    NgxDatatableModule,
  ],
  declarations: [UserListComponent]
})
export class UserListModule { }
