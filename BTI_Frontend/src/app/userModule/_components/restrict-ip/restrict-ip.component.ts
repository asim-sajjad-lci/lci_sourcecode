import { Component,OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
// import {DatePickerModule} from 'ng2-datepicker-bootstrap';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';
import {NgForm} from '@angular/forms';
import { AuthSetting } from '../../_models/authsetting/authsetting';

import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { CommonService } from '../../../_sharedresource/_services/common-services.service';
import { INgxMyDpOptions, IMyDateModel } from 'ngx-mydatepicker';
import * as moment from 'moment';
import {Constants} from '../../../_sharedresource/Constants';
import { RestrictIPService } from 'app/userModule/_services/restrictIP/restrict-ip.service';

@Component({
  selector: 'app-restrict-ip',
  templateUrl: './restrict-ip.component.html',
  styleUrls: ['./restrict-ip.component.css'],
  providers: [RestrictIPService,GetScreenDetailService,CommonService]
})
export class RestrictIPComponent implements OnInit {

  messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    roleGroupId: string;
    lblRole = "Create Group";
    ipChecked:boolean=false;
    alertmessage = Constants.alertmessage;
    validationMessage = Constants.validationMessage;
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    selectUser =Constants.selectUser;
    selectDays= Constants.selectDays;
    selectRoles = Constants.selectUser;
    // selectCompany = Constants.selectCompany;
    select=Constants.select; 
     ddl_MultiSelectUser = [];
     //ddl_MultiSelectUser:IMultiSelectOption[];
     userList=[];
    SelectedUsers = [];
    ddlUserSetting = {};
    isScreenLock;

    clearText = Constants.clearText;
    updateField=Constants.updateField;

    

    ddSettings: IMultiSelectSettings = {
        buttonClasses: 'btn dropdown-toggle formm-dd',
        enableSearch: true,
        showCheckAll: true,
        showUncheckAll: true
    };
    // Text configuration
    ddTexts: IMultiSelectTexts = {
        defaultTitle: this.selectRoles,
        checkAll: this.selectAll,
        uncheckAll: this.unselectAll,
    };

    btndisabled:boolean=false;
    // Initialized to specific date (09.10.2018)
    private  Object = { date: { year: 2018, month: 10, day: 9 } };

    private momentVariable = null;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private restrictIPService:RestrictIPService,
        private commonService:CommonService,
        private getScreenDetailService: GetScreenDetailService,
    ){
    }
    
    // Screen initialization 
    ngOnInit() {
         this.ddlUserSetting = { 
           singleSelection: false, 
           text:"",
           selectAllText: this.selectAll,
           unSelectAllText: this.unselectAll,
           enableSearchFilter: true,
           classes:"myclass custom-class"
         };  
           this. getAllUsers();
        this.getScreenDetailService.ValidateScreen("RestrictIP").then(res=>
            {
                this.isScreenLock = res;
            });
    }

    onItemSelect(item:any){
        //this.commonService.closeMultiselect()
    }
    OnItemDeSelect(item:any){
       
       // this.commonService.closeMultiselect()
    }
    onSelectAll(items: any){
        //this.commonService.closeMultiselect()
    }
    onDeSelectAll(items: any){
        
    }
 
    // get all company
    getAllUsers()
    {
      
        this.restrictIPService.getUserList().then(data => {
            var allUserLst=data.result.records;
            for(var i=0;i<allUserLst.length;i++)
            {
                this.ddl_MultiSelectUser.push({"id":allUserLst[i].id,"itemName":allUserLst[i].userName });
            }
        });
    }
    onCheckboxChange() {
        this.ipChecked = !this.ipChecked;
        this.model.ipChecked=this.ipChecked;
    }
   
           // function call for updating ip for user
    UpdateIpSetting(f: NgForm) {
        this.btndisabled=true;
  if(this.SelectedUsers.length == 0)
        {
            return false;
        }
        var selectedUserIds = [];
        for (var i = 0; i < this.SelectedUsers.length; i++) {
            var MyCompany = this.SelectedUsers;

            selectedUserIds.push(MyCompany[i].id);
        }
        
        this.model.ipChecked=!this.ipChecked;
        this.model.userIds=selectedUserIds;
    
        this.restrictIPService.updateIPSetting(this.model).then(data => {
                window.scrollTo(0,0);
                this.btndisabled=false;
                var datacode = data.code;
              if (datacode == 200) {
                window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message;
                    }, 100);
                 this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                    f.resetForm();
                     this.SelectedUsers=[];
                     this.ipChecked=false;
                  }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
             
        }


        Clear(f: NgForm) {
            // f.resetForm({ country: 0, state: 0, city: 0 });
      
           this.SelectedUsers=[];
           
            this.model=[];
          }

        
      onAutocompleteFocus(labelId){
        debugger;
         let elem: HTMLElement = document.getElementById(labelId);
         elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
       }
       
       onAutocompleteFocusOut(labelId){
         // let elem: HTMLElement = document.getElementById(labelId);
         // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
         
       }
}
