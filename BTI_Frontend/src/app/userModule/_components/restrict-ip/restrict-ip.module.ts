
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { RestrictIPRoutes } from './restrict-ip.routing';
import { RestrictIPComponent } from './restrict-ip.component';
import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/dist/multiselect.component';






@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RestrictIPRoutes),
    SharedModule,
    NgxDatatableModule,
    MultiselectDropdownModule,
    Ng2AutoCompleteModule,
    AngularMultiSelectModule,
   
  ],

  
  declarations: [RestrictIPComponent]
})
export class RestrictIPModule { }
