import { Routes } from '@angular/router';
import { RestrictIPComponent } from './restrict-ip.component';




export const RestrictIPRoutes: Routes = [{
  path: '',
  component: RestrictIPComponent,
  data: {
    breadcrumb: ""
  }
}];