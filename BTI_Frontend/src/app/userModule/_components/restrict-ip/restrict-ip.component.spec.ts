import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestrictIPComponent } from './restrict-ip.component';

describe('RestrictIPComponent', () => {
  let component: RestrictIPComponent;
  let fixture: ComponentFixture<RestrictIPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestrictIPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestrictIPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
