import { Routes } from '@angular/router';




import { CreateUserComponent } from './create-user.component';

export const CreateUserRoutes: Routes = [{
  path: '',
  component: CreateUserComponent,
  data: {
    breadcrumb: ""
  }
}];