import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/userModule/_services/usermanagement/user.service';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { Constants } from 'app/_sharedresource/Constants';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { NgForm } from '../../../../../node_modules/@angular/forms';
import {  Input, ViewChild, ViewChildren, ElementRef, forwardRef, HostListener, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';


const CSS_CLASS_NAMES = {
    highLight: 'dd-highlight-item',
  }
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
  providers: [UserService,GetScreenDetailService]
})
export class CreateUserComponent implements OnInit {
    @ViewChild('filterInput') filterInput: ElementRef;
    @ViewChild('filterInput1') filterInput1: ElementRef;
    @ViewChild('filterInput2') filterInput2: ElementRef;
    @ViewChild('filterInput3') filterInput3: ElementRef;
    @ViewChildren('listItems') listItems: QueryList<ElementRef>;
   




  moduleCode = Constants.userModuleCode;
    screenCode = "S-1019";
    moduleName;
    screenName;
    isScreenLock
    // defaultFormValues: [object];
    // availableFormValues: [object]; 
    get value() {
        return this._value;
      }
    
      set value(val) {
        this._value = val;
      }
  
      _value: string;

    isListHide = true;
    defaultFormValues:any [];
    availableFormValues:any []; 
    messageText;
    hasMsg = false;
    showMsg = false;
    isSuccessMsg;
    isfailureMsg;
    model: any = {};
    userId: string;
    lblRole = "Create User";
    countryOptions;
    stateOptions;
    cityOptions;
    userGroupOptions;
    ddOptions: IMultiSelectOption[];
    LstCompany:IMultiSelectOption[];
    selectAll = Constants.selectAll; 
    unselectAll = Constants.unselectAll;
    companyLabelId="companyLabel";
    // selectCompany = Constants.selectCompany;
    selectCompany = "";
    select=Constants.select;
     ddl_MultiSelectCompany = [];
    SelectedCompany = [];
    ddlCompanySetting = {};
    iSEmailReadonly:boolean=false;
    isDisabled:boolean= false;
    selecteddob;

    public selectedCountry;
    public selectedState;
    public selectedCity;
    public selectedUserGroup;

    currentCountry;
currentState;
currentCity;
currentUserGroup;
isUnderupdate=false;

isCountryOpen=false;
isStateOpen=false;
isCityOpen=false;
isUserGroupOpen=false;


clearText = Constants.clearText;
updateField=Constants.updateField;


//     private myOptions: INgxMyDpOptions = {
//     dateFormat: 'dd/mm/yyyy',
//     disableSince: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() +1 }
// };

 // Settings configuration
    ddSettings: IMultiSelectSettings = {
        buttonClasses: 'btn dropdown-toggle formm-dd',
        enableSearch: true,
        showCheckAll: true,
        showUncheckAll: true
    };
     // Text configuration
    ddTexts: IMultiSelectTexts = {
         defaultTitle: this.selectCompany,
        checkAll: this.selectAll,
        uncheckAll: this.unselectAll,
    };
    // Initialized to specific date (09.10.2018)
    private  Object = { date: { year: 2018, month: 10, day: 9 } };

    // optional date changed callback
    // onDateChanged(event: IMyDateModel): void {
       
    // }


    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
       
        private getScreenDetailService: GetScreenDetailService
    ) {
        //default form parameters for adding user
        this.defaultFormValues = [
            { 'fieldName': 'ADD_NEW_USER_LABEL_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':'' },
            { 'fieldName': 'ADD_NEW_USER_LABEL_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ARABIC_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ARABIC_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ARABIC_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_USER_GROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_LABEL_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_TEXT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_TEXT_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_POSTALCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},
            { 'fieldName': 'ADD_NEW_USER_DOB', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':''},

        ];
    }
    onChange() {
   
    }
  
   // Screen initialization
    ngOnInit() {
        this.ddlCompanySetting = { 
           singleSelection: false, 
           text:this.selectCompany,
           selectAllText: this.selectAll,
           unSelectAllText: this.unselectAll,
           enableSearchFilter: true,
           classes:"myclass custom-class"
         };            
    
    
        // Multiselect Dropdown End Here
    this.route.params.subscribe((params: Params) => {

            this.userId = params['userId'];
            if (this.userId != '' && this.userId != undefined) {
                this.iSEmailReadonly=true;
                this.lblRole = "Edit User";
                this.screenCode = "S-1018";
                this.onAutocompleteFocus(this.companyLabelId);
                // default form parameters for edit user detail
                this.defaultFormValues = [
                    { 'fieldName': 'EDIT_USER_LABEL_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ARABIC_FIRST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ARABIC_MIDDLE_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ARABIC_LAST_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_USER_GROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_COMPANY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_TEXT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_TEXT_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_POSTALCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_DOB', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_USER_ID', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_TEXT_RESET_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_OLD_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_NEW_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_LABEL_CONFIRM_PASSWORD', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_POSTALCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},
                    { 'fieldName': 'EDIT_USER_DOB', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'','readAccess':'','writeAccess':''},

                ];
        this.userService.getUser(this.userId).then(data => {

                    this.model = data.result; // filling model
                    var allCompanyLst=this.model.listOfCompanies;
                    this.isUnderupdate=true;
                    // if(event.userGroupId!=undefined){
                    setTimeout(() => {
                        this.currentCountry=this.countryOptions.filter(p => p.countryId == this.model.countryId);
                        this.selectedCountry = this.currentCountry[0].countryName;
        
                      }, 1000);
                    for(var i=0;i<allCompanyLst.length;i++)
                    {
                        this.SelectedCompany.push({"id":allCompanyLst[i].id,"itemName":allCompanyLst[i].name});
                    }
                    this.userService.getStateList(this.model.countryId).then(data => {
                        this.stateOptions = data.result;
                        this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
                        this.currentState=this.stateOptions.filter(p => p.stateId == this.model.stateId);
                        this.selectedState = this.currentState[0].stateName;
                    });
                    this.userService.getCityList(this.model.stateId).then(data => {
                     
                        this.cityOptions = data.result;
                        this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
                        this.currentCity=this.cityOptions.filter(p => p.cityId == this.model.cityId);
                        this.selectedCity = this.currentCity[0].cityName;
                    });

                   debugger;
                    this.model.companyIds = this.model.companyIds;
                    this.model.isActive = this.model.isActive;
                    this.model.userGroupId = this.model.listOfCompanies[0].userGroup.id;
                    this.model.zipCode=this.model.zipCode;
                  

                    setTimeout(() => {
                        
                        this.currentUserGroup=this.userGroupOptions.filter(p => p.id == this.model.userGroupId);
                        this.selectedUserGroup = this.currentUserGroup[0].groupName;
        
                      }, 1000);

                   
                    var today = this.model.dob;
                    var dateData = today.split('/');
                    this.model.dob = {"date":{"year":parseInt(dateData[2]),"month":parseInt(dateData[1]),"day":parseInt(dateData[0])}};

                    this.selecteddob= this.model.dob;

                    this.setAutocompleteLabel();
              });
              
            }
            
            else {
                this.iSEmailReadonly=false;
                this.model.countryId = "";
                this.model.stateId = "";
                this.model.cityId = "";
            }
        });

        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
        //getting screen for user details
        this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
            this.moduleName = data.result.moduleName
            this.screenName = data.result.dtoScreenDetail.screenName
            this.availableFormValues = data.result.dtoScreenDetail.fieldList;
            for (var j = 0; j < this.availableFormValues.length; j++) {
                var fieldKey = this.availableFormValues[j]['fieldName'];
                var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                objDefault['fieldValue'] = objAvailable['fieldValue'];
                objDefault['helpMessage'] = objAvailable['helpMessage'];
                objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                objDefault['readAccess'] = objAvailable['readAccess'];
                objDefault['writeAccess'] = objAvailable['writeAccess'];
                if (objAvailable['listDtoFieldValidationMessage']) {
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            }
        });

        this.model.isActive = true;

        // Country List Step 1
        this.userService.getCountryList().then(data => {
        this.countryOptions = data.result;
        this.countryOptions.splice(0, 0, { "countryId": "", "countryName": this.select });

        });

        this.userService.getCompanyList().then(data => {
            
            for(var i=0;i<data.result.records.length;i++)
            {
              
                this.ddl_MultiSelectCompany.push({ "id": data.result.records[i].id, "itemName": data.result.records[i].name })
            }
        });

        this.userService.getUserGroupList().then(data => {
            this.userGroupOptions = data.result.records;
            this.userGroupOptions.splice(0, 0, { "userGroupId": "", "groupName": this.select });
        });
    }
    onItemSelect(item:any){
    
    }
    OnItemDeSelect(item:any){
     
    }
    onSelectAll(items: any){
    }
    onDeSelectAll(items: any){
        
    }
    // event returning country data by country id
    toggle() {
        
        this.isListHide = !this.isListHide;
       
          this.filterInput.nativeElement.focus();
         
      }
      toggle1() {
        // this.isListHide = !this.isListHide;
       
        this.selectedState='';
        debugger
        // if (!this.isListHide) {
          setTimeout(() => this.filterInput1.nativeElement.focus(), 0);
          this.listItems.forEach((item) => {
            if (JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value) {
              this.addHightLightClass(item.nativeElement);
              this.scrollToView(item.nativeElement);
            } else {
              this.removeHightLightClass(item.nativeElement);
            }
          })
        // }
      }
      
      toggle2() {
        //  this.isListHide = !this.isListHide;
          this.selectedCity='';
      
      
        // if (!this.isListHide) {
          setTimeout(() => this.filterInput2.nativeElement.focus(), 0);
          this.listItems.forEach((item) => {
            if (JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value) {
              this.addHightLightClass(item.nativeElement);
              this.scrollToView(item.nativeElement);
            } else {
              this.removeHightLightClass(item.nativeElement);
            }
          })
        // }
      }


      
      toggle3() {
        //  this.isListHide = !this.isListHide;
          this.selectedUserGroup='';
      
        if (!this.isListHide) {
          setTimeout(() => this.filterInput3.nativeElement.focus(), 0);
          this.listItems.forEach((item) => {
            if (JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value) {
              this.addHightLightClass(item.nativeElement);
              this.scrollToView(item.nativeElement);
            } else {
              this.removeHightLightClass(item.nativeElement);
            }
          })
        }
      }

      addHightLightClass(elem: HTMLElement) {
        elem.classList.add(CSS_CLASS_NAMES.highLight)
      }
      scrollToView(elem?: HTMLElement) {
        if (elem) {
          setTimeout(() => elem.scrollIntoView(), 0)
        } else {
          const selectedItem = this.listItems.find((item) => JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value);
          if (selectedItem) {
            setTimeout(() => selectedItem.nativeElement.scrollIntoView(), 0);
          }
        }
      }
      removeHightLightClass(elem: HTMLElement) {
        elem.classList.remove(CSS_CLASS_NAMES.highLight);
      }
    getCountryData(event) {
        if(event.countryId!=undefined){
            this.model.stateId = "";
            this.model.cityId = "";
            this.selectedState="";
            this.selectedCity="";
           // var countryId = event.target.value ? event.target.value : -1;
           var countryId = event.countryId ? event.countryId : -1;
           this.model.countryId=countryId;
          
            this.userService.getCountryCode(countryId).then(data => {
                this.model.countryCode = data.result.countryCode;
            });
            // Country List Step 2
            this.userService.getStateList(countryId).then(data => {
                this.stateOptions = data.result;
                this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
            });
        }
        
    }

    // event fired for reset password by userId
    resetUserPassword(event){
        event.preventDefault();
        if (this.userId != '' && this.userId != undefined) {
            this.userService.resetUserPassword(this.model.userId).then(data => {
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        this.messageText = data.btiMessage.message+' !!';
                    }, 100);
            this.hasMsg = true;
                   window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
            }
            }).catch(error => {
            this.hasMsg = true;
                   window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)

            });
        }else{
            this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                         this.messageText = "Server error. Please contact admin !";
            }, 100)

        }

    }

    // //event returning state data by state id
    //  getStateData(event) {
    //     this.model.cityId = "";
    //     // City List Step 3
    //     var stateId = event.target.value ? event.target.value : -1;
    //     this.userService.getCityList(stateId).then(data => {
    //         this.cityOptions = data.result;
    //         this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
    //     });
    // }

      //event returning state data by state id
      userGroupFocusIn(){
        this.isUserGroupOpen=true;
      }
      userGroupFocusOut(userGroup){
        
        this.isUserGroupOpen=false;
      if(typeof userGroup.model === "string"){
          this.selectedUserGroup="";
          
      }
      }

      countryFocusout(selectedModel){
        
        this.isCountryOpen=false;
      if(typeof selectedModel.model === "string"){
          this.selectedCountry="";
          
      }
    }

    countryFocusIn(countryId){
        this.isCountryOpen=true;
    }

    stateFocusout(selectedModel){
      
      this.isStateOpen=false;
    if(typeof selectedModel.model === "string"){
        this.selectedState="";
        
    }
  }

  stateFocusIn(countryId){
      this.isStateOpen=true;
  }

  cityFocusout(selectedModel){
      
      this.isCityOpen=false;
    if(typeof selectedModel.model === "string"){
        this.selectedCity="";
        
    }
  }

  cityFocusIn(countryId){
      this.isCityOpen=true;
  }


      getStateData(event) {
        debugger
        // if(event.stateId!=undefined){
          this.model.cityId = "";
          this.selectedCity="";
         // var stateId = event.target.value ? event.target.value : -1;
         var stateId = event.stateId ? event.stateId : -1;
         this.model.stateId=stateId;
          this.userService.getCityList(stateId).then(data => {
              this.cityOptions = data.result;
              this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
          });
        // }
        
      }


      getCityData(event){
        debugger
        if(event.cityId!=undefined){
        // var stateId = event.target.value ? event.target.value : -1;
        var cityId = event.cityId ? event.cityId : -1;
        this.model.cityId=cityId;
        }
       }

    	      validateEmail(){		
           if (this.userId == '' || this.userId == undefined) {		
                this.userService.validateEmail(this.model.email).then(data => {		
                var datacode = data.code;		
                if (datacode == 302) {		
                    window.scrollTo(0, 0);		
                        window.setTimeout(() => {		
                            this.isSuccessMsg = false;		
                            this.isfailureMsg = true;		
                            this.showMsg = true;		
                            this.messageText = data.btiMessage.message;		
                        }, 100);		
                        this.hasMsg = true;		
                        // window.setTimeout(() => {		
                        //     this.showMsg = false;		
                        //     this.hasMsg = false;		
                        // }, 4000);		
                    }		
                else if(datacode == 200){		
                       window.setTimeout(() => {		
                            this.showMsg = false;		
                            this.hasMsg = false;		
                        }, 100);		
            }    		
         });		
        }		
       		
    }

    //function call for creating new user
    CreateUser(f: NgForm) {
        debugger;
        this.isDisabled=true;
        if(this.hasMsg || this.SelectedCompany.length == 0)
        {
            return false;
        }
        var selectedCompanyIds = [];
        for (var i = 0; i < this.SelectedCompany.length; i++) {
            var MyCompany = this.SelectedCompany;

            selectedCompanyIds.push(MyCompany[i].id);
        }
       // var ArrSelectedCompany = selectedCompanyIds.join();
        this.model.companyIds=selectedCompanyIds;
        if (this.userId != '' && this.userId != undefined) {
           // this.model.dob = this.model.dob.formatted;
            if(this.model.dob.formatted == undefined)
            {
               var dob = this.selecteddob;
               this.model.dob = dob.date.day +'/'+ dob.date.month +'/'+ dob.date.year;
            }
            else
            {
                this.model.dob = this.selecteddob.formatted;
            }
            
            this.userService.updateUser(this.model).then(data => {
                debugger;
                this.isDisabled=false;
                var datacode = data.code;
                if (datacode == 200) {
                window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                        this.messageText = data.btiMessage.message;;
                    }, 100);
                        window.setTimeout(() => {
                        this.router.navigate(['user']);
                        }, 2000);

                    this.hasMsg = true;
                    window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                }
                // else if (datacode == 302) {
                // window.scrollTo(0, 0);
                //     window.setTimeout(() => {
                //         this.isSuccessMsg = false;
                //         this.isfailureMsg = true;
                //         this.showMsg = true;
                //     window.setTimeout(() => {
                //         this.showMsg = false;
                //         this.hasMsg = false;
                //     }, 4000);
                //         this.messageText = data.btiMessage.message;
                //     }, 100);
                //         // window.setTimeout(() => {
                //         // this.router.navigate(['createuser/:userId']);
                //         // }, 2000);
                //     this.hasMsg = true;
                //     window.setTimeout(() => {
                //         this.showMsg = false;
                //         this.hasMsg = false;
                //     }, 4000);
                //     }
            }).catch(error => {
                this.isDisabled=false;
                this.hasMsg = true;
                    window.setTimeout(() => {
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                        this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
                });
        }
        else {
           console.log("usergroup",this.selectedUserGroup)
           this.model.userGroupId=this.selectedUserGroup.id;
             var dob = this.selecteddob;
            // this.model.dob = dob.formatted;
            // var dob = this.selecteddob;
            this.model.dob = dob.date.day +'/'+ dob.date.month +'/'+ dob.date.year;
           

           
           
            this.userService.createUser(this.model).then(data => {
                this.isDisabled=false;
                var datacode = data.code;
                if (datacode == 201) {
                    window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isSuccessMsg = true;
                        this.isfailureMsg = false;
                        this.showMsg = true;
                        window.setTimeout(() => {
                        this.showMsg = false;
                        this.hasMsg = false;
                    }, 4000);
                        this.messageText = data.btiMessage.message;;
                    }, 100);
                        window.setTimeout(() => {
                        this.router.navigate(['user']);
                        }, 2000);

                    this.hasMsg = true;

                    f.resetForm();

                }
                else if (datacode == 302) {
                window.scrollTo(0, 0);
                    window.setTimeout(() => {
                        this.isDisabled=false;
                        this.isSuccessMsg = false;
                        this.isfailureMsg = true;
                        this.showMsg = true;
                    // window.setTimeout(() => {
                    //     this.showMsg = false;
                    //     this.hasMsg = false;
                    // }, 4000);
                        this.messageText = data.btiMessage.message;
                    }, 100);
                        // window.setTimeout(() => {
                        // this.router.navigate(['createuser/:userId']);
                        // }, 2000);
                    this.hasMsg = true;
                    // window.setTimeout(() => {
                    //     this.showMsg = false;
                    //     this.hasMsg = false;
                    // }, 4000);
                    }
            }).catch(error => {
                this.hasMsg = true;
                window.setTimeout(() => {
                    this.isSuccessMsg = false;
                    this.isfailureMsg = true;
                    this.showMsg = true;
                    this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                }, 100)
            });
        }
    }
    onlyDecimalNumberKey(event) {
        return this.getScreenDetailService.onlyDecimalNumberKey(event);
    }


    onlyAlphabets(event)
    {
       
        var charCode = event.keyCode;
        if(charCode == 17 || charCode == 16)
        {
            return false;
        }
        if (charCode == 9 || charCode == 8 || charCode == 46 ||(charCode > 64 && charCode < 91) || (charCode > 111 && charCode < 123))
            return true;
        else
        {
            return false;
        }
    }
    onlyAlphabets2(event)
    {
        
        var charCode = event.keyCode;
        if(charCode == 8 || charCode == 46)
        {
            return true;
        }
        if (charCode > 64 && charCode < 91)
        {
            return true;
        }
        else if(charCode > 105 && charCode < 123)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

     /** If Screen is Lock then prevent user to perform any action.
        *  This function also cover Role Management Write acceess functionality */
        LockScreen(writeAccess)
        {
            if(!writeAccess)
            {
                return true
            }
            else if(this.isDisabled)
            {
                return true
            }
            else if(this.isScreenLock)
            {
                return true;
            }
            else{
                return false;
            }
        }

        goToCreate(){
            this.router.navigate(['create-user']);   
          }
          
          onAutocompleteFocus(labelId){
 
            let elem: HTMLElement = document.getElementById(labelId);
            elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
          }
          
          onAutocompleteFocusOut(labelId){
            // let elem: HTMLElement = document.getElementById(labelId);
            // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
            
          }



          Clear(f: NgForm) {
            // f.resetForm({ country: 0, state: 0, city: 0 });
            this.selectedCountry=[];
            this.selectedUserGroup=[];
            this.SelectedCompany=[];
            this.selectedState=[];
            this.selectedCity=[];
            this.selecteddob=[];
            this.model=[];
          }

          setAutocompleteLabel(){
    
            if(this.model.countryId)
            {
                let elem: HTMLElement = document.getElementById("countryLabel");
               elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
           }
       
            if(this.model.stateId)
            {
               let elem: HTMLElement = document.getElementById("stateLabel");
               elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
       
              
            }
            if(this.model.cityId)
            {
               let elem2: HTMLElement = document.getElementById("cityLabel");
               elem2.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
            }
            if(this.model.userGroupId)
            {
               let elem2: HTMLElement = document.getElementById("userGroupLabel");
               elem2.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
            }
        }

}
