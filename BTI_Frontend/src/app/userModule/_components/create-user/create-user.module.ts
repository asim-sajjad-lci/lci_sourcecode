

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';



import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { CreateUserRoutes } from './create-user.routing';
import { CreateUserComponent } from './create-user.component';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateUserRoutes),
    SharedModule,
    NgxDatatableModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule,
    NgxMyDatePickerModule.forRoot(),
    Ng2AutoCompleteModule,
  ],
  declarations: [CreateUserComponent]
})
export class CreateUserModule { }
