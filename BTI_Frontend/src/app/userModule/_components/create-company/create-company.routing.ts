import { Routes } from '@angular/router';


import { CreateCompanyComponent } from './create-company.component';

export const CreateCompanyRoutes: Routes = [{
  path: '',
  component: CreateCompanyComponent,
  data: {
    breadcrumb: ""
  }
}];