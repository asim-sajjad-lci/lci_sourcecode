import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {FileUploader} from "ng2-file-upload";
import { Constants } from 'app/_sharedresource/Constants';
import { Router, ActivatedRoute, Params } from '../../../../../node_modules/@angular/router';
import { CompanyService } from '../../_services/companymanagement/company.service';
import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import { NgForm } from '../../../../../node_modules/@angular/forms';
import { RequestOptions } from '../../../../../node_modules/@angular/http';
import { AgmMarker } from '../../../../../node_modules/@agm/core';
import {  Input, ViewChild, ViewChildren, ElementRef, forwardRef, HostListener, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import "rxjs/add/operator/filter"

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
const CSS_CLASS_NAMES = {
  highLight: 'dd-highlight-item',
}
@Component({
   selector: 'app-create-company',
   templateUrl: './create-company.component.html',
   styleUrls: ['./create-company.component.css'],
   providers: [CompanyService,GetScreenDetailService]
})
export class CreateCompanyComponent implements OnInit {

  @ViewChild('filterInput') filterInput: ElementRef;
  @ViewChild('filterInput1') filterInput1: ElementRef;
  @ViewChild('filterInput2') filterInput2: ElementRef;
  @ViewChildren('listItems') listItems: QueryList<ElementRef>;

  @HostListener('document:click', ['$event'])
  onClick(ev: MouseEvent) {
    const clickInside = this.elemRef.nativeElement.contains(ev.target);
    if (!clickInside) {
      this.isListHide = true;
    }
  }
  _items = [];

  _list = new BehaviorSubject<any[]>([]);
  @Input() placeholder = ' ';
  _value: string;
  _display: string = 'Select';

  isListHide = true;

  searchText = '';
 

  onChange: any = () => { };
  onTouched: any = () => { };

  keyDowns: Observable<KeyboardEvent> = Observable.fromEvent(this.elemRef.nativeElement, 'keydown');

  pressEnterKey: Observable<KeyboardEvent>;
  @Input()
  set list(list) {
    this._list.next(list);
  }

  set items(list) {
    this._items = list;
  }
  get items(): Array<{ id: number, display: string }> {
    return this._items;
  }
  get value() {
    return this._value;
  }

  set value(val) {
    this._value = val;
  }

  get display() {
    return this._display;
  }
  set display(value) {
    this._display = value;
  }


 
companyLogo="assets/images/company_logo_4.png";
imageSrc;
companyId: string;
moduleCode = Constants.userModuleCode;
screenCode;
moduleName;
screenName;
isScreenLock;
disabledButton;
defaultFormValues: any =[];
availableFormValues: any =[];
messageText;
hasMsg = false;
showloader = false;
showMsg = false;
isSuccessMsg;
isfailureMsg;
model: any = {};
countryOptions;
stateOptions;
cityOptions;
isActive= true;
select=Constants.select;
lat: any;
lng: any;
ShowInvalidWebAddress:boolean=false;
msgCompanyExist:string;
isModify:boolean=false;
uploader: FileUploader = new FileUploader({
  url: URL,
  isHTML5: true
});

isCountryOpen=false;
isStateOpen=false;
isCityOpen=false;

clearText = Constants.clearText;
updateField=Constants.updateField;
btnCancelText=Constants.btnCancelText;




logoFile:File;
hasBaseDropZoneOver = false;
hasAnotherDropZoneOver = false;
public selectedCountry;
public selectedState;
public selectedCity;
public isAddCountryPopUpOpen=false;


currentCountry;
currentState;
currentCity;
map;

options=[{
  "id": 1,
  "label": "City1",
  "checked":false
},
{
  "id": 2,
  "label": "City2",
  "checked":false
},
{
  "id": 3,
  "label": "City3",
  "checked":false
}]

public agmMarkers: marker[] = [
    {
      lat: this.lat,
      lng: this.lng,
      label: "",
	  draggable: true,
      
    }
   ];
selectedData=[];

@Output() change = new EventEmitter<any[]>();
data:any[]=[];

itemList = [];
  selectedItems = [];
  settings = {};
  count = 6;

name = 'Angular 5';
// lat:any;
// lng:any;
  
  constructor(
    private elemRef: ElementRef,
    private router: Router,
    private route: ActivatedRoute,
    private companyService: CompanyService,
    private getScreenDetailService: GetScreenDetailService){
     if (navigator)
    {
    navigator.geolocation.getCurrentPosition( pos => {
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;

        this.model.latitude=this.lat;
        this.model.longitude=this.lng;
    
      });

    }  
  }

  handleUpload(e):void{
    // alert(e.target.files)
    this.companyLogo = e.target.value;

 }
 readURL(e): void {
  if (e.target.files && e.target.files[0]) {
      const file = e.target.files[0];

      const reader = new FileReader();
      reader.onload = e => this.imageSrc = reader.result;

      reader.readAsDataURL(file);
  }
}
  ngOnInit() {
    console.log("akjfdklafj")

   
    this.model.lng=this.lng;
    this.model.lat=this.lat;
    this.disabledButton=false;
    this.model.isActive = "Y";
    this.companyService.getCountryList().then(data => {
        this.countryOptions = data.result;
        this.countryOptions.splice(0, 0, { "countryId": "", "countryName": this.select });
        
    });
    
    this.route.params.subscribe((params: Params) => {
        this.companyId = params['companyId'];

       
        
        if (this.companyId != '' && this.companyId != undefined) {
            //defaul form parameter for edit screen
            this.isModify = true;
           
            this.defaultFormValues = [
                
                { 'fieldName': 'EDIT_COMPANY_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_POSTAL_ZIPCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_BACK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_COMPANY_WEB_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_COMPANY_LATITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
                { 'fieldName': 'EDIT_COMPANY_LONGITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' },
            ];
            
            this.screenCode = "S-1020";
           
            this.companyService.getCompany(this.companyId).then(data => {
                debugger
                this.model = data.result;
                //this.countryName=this.model.countryName;
                setTimeout(() => {
                  this.currentCountry=this.countryOptions.filter(p => p.countryId == this.model.countryId);
                  this.selectedCountry = this.currentCountry[0].countryName;
  
                }, 1000);
             
                // return this.service.getAllcustomFields().subscribe(allcustFields => {
                //     this.custfieldData = allcustFields.result.records.filter(p => p.entityMasterId == 1);
              
                //   });

               
                this.lat=this.model.latitude;
                this.lng=this.model.longitude;
                // alert(this.model.countryId)
                this.companyService.getStateList(this.model.countryId).then(data => {
                    this.stateOptions = data.result;
                    this.currentState=this.stateOptions.filter(p => p.stateId == this.model.stateId);
                    this.selectedState= this.currentState[0].stateName;
                    this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });


                });
                this.companyService.getCityList(this.model.stateId).then(data => {
                    this.cityOptions = data.result;
                    this.currentCity=this.cityOptions.filter(p => p.cityId == this.model.cityId);
                    this.selectedCity=this.currentCity[0].cityName;
                    this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
                });

                this.setAutocompleteLabel();
            });
          
        }
        else {
            this.isModify = false;
            this.model.countryId = "";
            this.model.stateId = "";
            this.model.cityId = "";
            this.screenCode = "S-1006";
            
            // default form parameter for adding company
            this.defaultFormValues = [
                { 'fieldName': 'COMPANY_NAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'COUNTRY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'POSTAL_ZIPCODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'COUNTRY_CODE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'PHONE_NUMBER', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'FAX', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'EMAIL_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'STATE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'CITY', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
                { 'fieldName': 'BACK', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },
                { 'fieldName': 'ADD_NEW_COMPANY_WEB_ADDRESS', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
                { 'fieldName': 'ADD_NEW_COMPANY_LATITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' , 'readAccess':'' , 'writeAccess':'' },		
                { 'fieldName': 'ADD_NEW_COMPANY_LONGITUDE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '', 'readAccess':'' , 'writeAccess':''  },
            ];
        }
        //getting screen for adding new company
        this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
            {
                this.isScreenLock = res;
            });
            this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
                this.screenName=data.result.dtoScreenDetail.screenName;
                this.moduleName=data.result.moduleName;
                this.availableFormValues = data.result.dtoScreenDetail.fieldList;
                for (var j = 0; j < this.availableFormValues.length; j++) {
                    var fieldKey = this.availableFormValues[j]['fieldName'];
                    var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                    var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                    objDefault['fieldValue'] = objAvailable['fieldValue'];
                    objDefault['helpMessage'] = objAvailable['helpMessage'];
                    objDefault['readAccess'] = objAvailable['readAccess'];
                    objDefault['writeAccess'] = objAvailable['writeAccess'];
                    objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
                }
            });
        });

    this.itemList = [
      { "id": 0, "itemName": "addNew", "name": "IN" },
      { "id": 1, "itemName": "India", "name": "IN" },
      { "id": 2, "itemName": "Singapore", "name": "SN" },
      { "id": 3, "itemName": "Australia", "name": "AU" },
      { "id": 4, "itemName": "Canada", "name": "CA" },
      { "id": 5, "itemName": "South Korea", "name": "SK" },
      { "id": 6, "itemName": "Brazil", "name": "BR" }    
    ];

    this.selectedItems = [];
    this.settings = {
      singleSelection: false,
      text: "",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      addNewItemOnFilter: true
    };
  }
  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  
            // event returning country data by country id
            getCountryData(event) {
              // alert(event.countryId)
        
            //   this.model.stateId = "";
            //   this.model.cityId = "";
            //   this.selectedState="";
            //   this.selectedCity="";
             // var countryId = event.target.value ? event.target.value : -1;

             if(event.countryId!=undefined){
              var countryId = event.countryId ? event.countryId : -1;
             this.model.countryId=countryId;
 
               this.companyService.getCountryCode(countryId).then(data => {
                   this.model.countryCode = data.result.countryCode;
               });
               this.companyService.getStateList(countryId).then(data => {
                   this.stateOptions = data.result;
                  //  this.stateOptions.splice(0, 0, { "stateId": "", "stateName": this.select });
                  //  this.filterInput1.nativeElement.focus();
               });
             }        
          }
          
          countryFocusout(selectedModel){
              debugger;
              this.isCountryOpen=false;
            if(typeof selectedModel.model === "string"){
                this.selectedCountry="";
                
            }

          }

          countryFocusIn(countryId){
              this.isCountryOpen=true;
          }

          stateFocusout(selectedModel){
            debugger;
            this.isStateOpen=false;
          if(typeof selectedModel.model === "string"){
              this.selectedState="";
              
          }
        }

        stateFocusIn(countryId){
            this.isStateOpen=true;
        }

        cityFocusout(selectedModel){
            debugger;
            this.isCityOpen=false;
          if(typeof selectedModel.model === "string"){
              this.selectedCity="";
              
          }
        }

        cityFocusIn(countryId){
            this.isCityOpen=true;
        }
          
          //event returning state data by state id
          getStateData(event) {
            debugger
            //   this.model.cityId = "";
            //   this.selectedCity="";
             // var stateId = event.target.value ? event.target.value : -1;
             if(event.stateId!=undefined){
               
             var stateId = event.stateId ? event.stateId : -1;
             this.model.stateId=stateId;
              this.companyService.getCityList(stateId).then(data => {
                  this.cityOptions = data.result;
                  this.cityOptions.splice(0, 0, { "cityId": "", "cityName": this.select });
              });
             }
          }

          getCityData(event){
            
           // var stateId = event.target.value ? event.target.value : -1;
           var cityId = event.cityId ? event.cityId : -1;
           this.model.cityId=cityId;
           
          }
          
          getLatLong(){
              if(this.model.webAddress)
              {
                  if(this.isUrlValid(this.model.webAddress))
                  {
                      this.companyService.getlatlng(this.model.webAddress).then(data => {	
                          
                          this.lat=data.results[0].geometry.location.lat;
                          this.lng=data.results[0].geometry.location.lng;
                          this.model.latitude = data.results[0].geometry.location.lat;		
                          this.model.longitude = data.results[0].geometry.location.lng;	
                          // this.hideElement = false;	
                      });	      
                  }
                  else
                  {
                      this.model.latitude='';
                      this.model.longitude='';
                  }
              }	
              else
              {
                  this.model.latitude='';
                  this.model.longitude='';
              }
              
              
          }
          
          isUrlValid(userInput) {
              var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
              var arr = userInput.split(".");
              if(!userInput.match('www'))
              {
                  this.ShowInvalidWebAddress=true;
                  return false;
              }
              else if(arr.length < 3)
              {
                  this.ShowInvalidWebAddress=true;
                  return false;
              }
              
              if(res == null)
              {
                  this.ShowInvalidWebAddress=true;
                  return false;
              }        
              else
              {
                  this.ShowInvalidWebAddress=false;
                  return true;
              }
              
          }
          ValidURL(userInput) {
              
              var regexQuery = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";
              var url = new RegExp(regexQuery,"i");
              if (url.test(userInput)) {
                  // alert('valid url: ' + userInput);
                  this.ShowInvalidWebAddress=false;
                  return true;
              }
              
              //  alert('invalid url: ' + userInput);
              this.ShowInvalidWebAddress=true;
              return false;
          }
          
          ValidateCompany()
          {
              
              if(this.model.name && !this.isModify)
              {
                  this.companyService.checkCompanyName(this.model.name).then(data => {
                      
                      if(data.btiMessage.messageShort =='RECORD_ALREADY_EXIST')
                      {
                          this.model.name = '';
                          this.msgCompanyExist = data.btiMessage.message;
                      }
                      else{
                          this.msgCompanyExist='';
                      }
                  });
              }
          }
          // function call for adding new company
          CreateCompany(f: NgForm) {
              debugger
              window.scrollTo(0, 0);
              this.showloader=true;
              this.disabledButton=true;
              if(this.model.webAddress && this.ShowInvalidWebAddress ==  true)
              {
                  return false;
              }
              if (this.companyId != '' && this.companyId != undefined) {
                //   //    this.hideElement = false;
                //   console.log("model",this.model)
                //   this.model.countryId=this.model.countryId;
                //   this.model.cityId=this.model.cityId;
                //   this.model.stateId=this.model.stateId;
                //   console.log("model",this.model)
                  this.companyService.updateCompany(this.model).then(data => {
                    debugger;
                      var datacode = data.code;
                      this.showloader=false;
                      this.disabledButton=false;
                      if (datacode == 201) {
                          // this.hideElement = true;
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.messageText = data.btiMessage.message;
                          }, 100);
                          window.setTimeout(() => {
                              this.router.navigate(['company']);
                          }, 2000);
                          this.hasMsg = true;
                      }
                  }).catch(error => {
                    debugger;
                      this.hasMsg = true;
                      this.showloader=false;
                      this.disabledButton=false;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                      },100)
                      
                  });
              }
              else {
                  
                  this.model.isActive=true;
                  console.log("model",this.model)
                  this.model.city=this.model.cityId.cityName;
                  this.model.country= this.model.countryId.countryName;
                  this.model.state=this.model.stateId.stateName;
                  this.model.tenantId= "bti_crm_module_1";
                  this.model.companyCode= "C_1107";


                  this.model.countryId=this.model.countryId.countryId;
                  this.model.cityId=this.model.cityId.cityId;
                  this.model.stateId=this.model.stateId.stateId;
                 
                  console.log("model",this.model)
                  // this.hideElement = true;
                  this.companyService.createCompany(this.model).then(data => {
                      
                      this.model.isActive=true;
                      var datacode = data.code;
                      this.showloader=false;
                      this.disabledButton=false;
                      if (datacode == 201) {
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.messageText = data.btiMessage.message;;
                          }, 100);
                          window.setTimeout(() => {
                              this.router.navigate(['company']);
                          }, 2000);
                          this.hasMsg = true;
                      }
                  }).catch(error => {
                      this.hasMsg = true;
                      this.showloader=false;
                      this.disabledButton=false;
                      window.setTimeout(() => {
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.messageText = error._body.split(',')[4].split(':')[1].replace('"','').replace('"','');
                      }, 100)
                  });
              }
          }
          
          onlyDecimalNumberKey(event) {
              return this.getScreenDetailService.onlyDecimalNumberKey(event);
          }
          
          
          /** If Screen is Lock then prevent user to perform any action.
          *  This function also cover Role Management Write acceess functionality */
          LockScreen(writeAccess)
          {
              if(!writeAccess)
              {
                  return true
              }
              else if(this.disabledButton)
              {
                  return true
              }
              else if(this.isScreenLock)
              {
                  return true;
              }
              else{
                  return false;
              }
          }



openMyModal(event) {
  document.querySelector("#"+event).classList.add('md-show');
}

closeMyModal(event) {
  debugger;
  document.querySelector('#effect-15').classList.remove('md-show');
  
  //this.selectedCountry="";
}

goBack(){
    this.router.navigate(['company']); 
  }

onAddItem(data:string){
  this.count++;
  this.itemList.push({"id": this.count,"itemName":data,"name":data});
  this.selectedItems.push({"id": this.count,"itemName":data,"name":data});
}
onItemSelect(item: any) {
  console.log(item);
  console.log(this.selectedItems);
}
OnItemDeSelect(item: any) {
  console.log(item);
  console.log(this.selectedItems);
}
onSelectAll(items: any) {
  console.log(items);
}
onDeSelectAll(items: any) {
  console.log(items);
}

onCountrySelect(event){

//   if(event.value==-1){
//     this.selectedCountry="";
//     this.openMyModal('effect-15')
   
//   }
}


onAutocompleteFocus(labelId,ngmodel){

  let elem: HTMLElement = document.getElementById(labelId);
  elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
}

onAutocompleteFocusOut(labelId,ngmodel){
    // let elem: HTMLElement = document.getElementById(labelId);
    // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
    
}

onFileUploadFocus(btnId){
 debugger;
 
  let elem: HTMLElement = document.getElementById(btnId);
  
  elem.setAttribute("style", "color:darkgrey;");
}
// onFileUploadFocusOut(btnId){

//   // let elem: HTMLElement = document.getElementById(btnId);
  
//   // elem.setAttribute("style", "background-color:blue;");
// } 

sTbState: string = 'invisible';

toggleSt(e, el) {
  this.sTbState = (this.sTbState === 'invisible' ? 'visible' : 'invisible');
  if (this.sTbState === 'visible') {
    el.focus();
  }
}


toggle() {
  debugger;
  this.isListHide = !this.isListHide;
  // this.searchText = '';
  // this.selectedCountry='';
  // this.selectedState='';
  // if (!this.isListHide) {
    this.filterInput.nativeElement.focus();
   
}
toggle1() {
  // this.isListHide = !this.isListHide;
 
  this.selectedState='';
  debugger
  // if (!this.isListHide) {
    setTimeout(() => this.filterInput1.nativeElement.focus(), 0);
    this.listItems.forEach((item) => {
      if (JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value) {
        this.addHightLightClass(item.nativeElement);
        this.scrollToView(item.nativeElement);
      } else {
        this.removeHightLightClass(item.nativeElement);
      }
    })
  // }
}

toggle2() {
  //  this.isListHide = !this.isListHide;
    this.selectedCity='';


  // if (!this.isListHide) {
    setTimeout(() => this.filterInput2.nativeElement.focus(), 0);
    this.listItems.forEach((item) => {
      if (JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value) {
        this.addHightLightClass(item.nativeElement);
        this.scrollToView(item.nativeElement);
      } else {
        this.removeHightLightClass(item.nativeElement);
      }
    })
  // }
}

addHightLightClass(elem: HTMLElement) {
  elem.classList.add(CSS_CLASS_NAMES.highLight)
}
scrollToView(elem?: HTMLElement) {
  if (elem) {
    setTimeout(() => elem.scrollIntoView(), 0)
  } else {
    const selectedItem = this.listItems.find((item) => JSON.parse(item.nativeElement.getAttribute('data-dd-value'))['id'] === this.value);
    if (selectedItem) {
      setTimeout(() => selectedItem.nativeElement.scrollIntoView(), 0);
    }
  }
}
removeHightLightClass(elem: HTMLElement) {
  elem.classList.remove(CSS_CLASS_NAMES.highLight);
}

check(option)
{

  console.log(option);
  option.checked=!option.checked;
  
  this.data=this.options.filter((x:any)=>x.checked).map(x=>
    {
   //   this.selectedData.push(x.label);
      return {id:x.id,label:x.label}
     
    })
  this.change.emit(this.data);
}

remove(itmem){
 
 
  
  for(let i=0;i<this.data.length;i++)
  {
    if(this.data[i].id == itmem.id) 
    {
      itmem.checked=!itmem.checked;
      this.data.splice(i,1);

    }
  }

  for(let i=0;i<this.options.length;i++)
  {
    if(this.options[i].id == itmem.id) 
    {
     // itmem.checked=!itmem.checked;
      this.options[i].checked = false;

    }
  }
 // this.change.emit(this.data);
  console.log(itmem);
  
  
  this.change.emit(this.data);
 }

 setAutocompleteLabel(){
    
     if(this.model.countryId)
     {
         let elem: HTMLElement = document.getElementById("countryLabel");
        elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
    }

     if(this.model.stateId)
     {
        let elem: HTMLElement = document.getElementById("stateLabel");
        elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");

       
     }
     if(this.model.cityId)
     {
        let elem2: HTMLElement = document.getElementById("cityLabel");
        elem2.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
     }
 }


 Clear(f: NgForm) {
  // f.resetForm({ country: 0, state: 0, city: 0 });
  // this.name=[];
  this.model=[];
  this.selectedCountry=[];
  this.selectedState=[];
  this.selectedCity=[];
 }


 fileChange(event) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];

        this.logoFile=file;
      
    }
}



 mapReady(map) {
    this.map = map;

    var that = this;
    this.map.addListener("dragend", function () {
      //do what you want
    });
}



    // google maps zoom level
    zoom: number = 8;
    
    // initial center position for the map
    
    clickedMarker(label: string, index: number) {
      console.log(`clicked the marker: ${label || index}`)
    }
    
    mapClicked($event: any) {
        debugger;
    //   this.markers.push({
    //     lat: $event.coords.lat,
    //     lng: $event.coords.lng,
    //     draggable: true
    //   });

    this.lat= $event.coords.lat;
    this.lng=$event.coords.lng;
    this.model.latitude=this.lat;
    this.model.longitude=this.lng;

    }
    
    markerDragEnd(m: marker, $event: MouseEvent) {
      console.log('dragEnd', m, $event);
    }
    

    changeLatitude(event){
        debugger;
       this.lat=parseFloat(event.target.value);
        this.model.latitude=event.target.value;
    }


    changeLongitude(event){
        debugger;
        this.lng=parseFloat(event.target.value);
        this.model.longitude=event.target.value;
    }

    // markers: marker[] = [
    //     {
    //         lat: 51.673858,
    //         lng: 7.815982,
    //         label: 'A',
    //         draggable: true
    //     },
    //     {
    //         lat: 51.373858,
    //         lng: 7.215982,
    //         label: 'B',
    //         draggable: false
    //     },
    //     {
    //         lat: 51.723858,
    //         lng: 7.895982,
    //         label: 'C',
    //         draggable: true
    //     }
    // ]
  

}

interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}
//D:\mustahsin_project\BTI_ERP\Algora_ERP_Working\Algora_ERP\src\assets\images\blan_company_logo.png

//src\assets\images\Webp.net-resizeimage.png

//D:\mustahsin_project\BTI_ERP\Algora_ERP_Working\Algora_ERP\src\assets\images\blan_company_logo.png
//D:\mustahsin_project\BTI_ERP\Algora_ERP_Working\Algora_ERP\src\assets\images\company_logo_1.png
//D:\mustahsin_project\BTI_ERP\Algora_ERP_Working\Algora_ERP\src\assets\images\company_logo_3.png