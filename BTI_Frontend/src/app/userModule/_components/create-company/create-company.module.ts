import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { CreateCompanyComponent } from './create-company.component';
import { CreateCompanyRoutes } from './create-company.routing';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { AgmCoreModule } from '../../../../../node_modules/@agm/core/core.module';




@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateCompanyRoutes),
    SharedModule,
    NgxDatatableModule,
    Ng2AutoCompleteModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBFqCv1IAzVgnWA7D4uO6B_GDG-KedssKw'})
  ],

  
  declarations: [CreateCompanyComponent]
})
export class CreateCompanyModule { }
