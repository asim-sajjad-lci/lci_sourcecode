import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWhiteListIPComponent } from './create-white-list-ip.component';

describe('CreateWhiteListIPComponent', () => {
  let component: CreateWhiteListIPComponent;
  let fixture: ComponentFixture<CreateWhiteListIPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWhiteListIPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWhiteListIPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
