import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { CreateWhiteListIpRoutes } from './create-white-list-ip.routing';
import { CreateWhiteListIPComponent } from './create-white-list-ip.component';






@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateWhiteListIpRoutes),
    SharedModule,
    NgxDatatableModule,
    Ng2AutoCompleteModule,
  ],

  
  declarations: [CreateWhiteListIPComponent]
})
export class CreateWhiteListIPModule { }
