import { Component,OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-4-dropdown-multiselect';
import { NgForm } from '@angular/forms';

import { GetScreenDetailService } from '../../../_sharedresource/_services/get-screen-detail.service';
import {Constants} from '../../../_sharedresource/Constants';
import { UserGroupService } from '../../_services/usergroup/usergroup.service';

@Component({
  selector: 'app-create-user-group',
  templateUrl: './create-user-group.component.html',
  styleUrls: ['./create-user-group.component.css'],
  providers: [UserGroupService,GetScreenDetailService]
})
export class CreateUserGroupComponent implements OnInit {
  moduleName;
  screenName;
  moduleCode = Constants.userModuleCode;
  screenCode;
  messageText;
  hasMsg = false;
  showMsg = false;
  isSuccessMsg;
  isfailureMsg;
  // defaultFormValues: [object];
  // availableFormValues: [object];
  defaultFormValues:any [];
  availableFormValues:any [];
  model: any = {};
  userGroupId: string;
  lblRole = "Create Group";
  ddOptions: IMultiSelectOption[];
  LstCompany: IMultiSelectOption[];
  selectAll = Constants.selectAll; 
  unselectAll = Constants.unselectAll;
  selectUserGroup= Constants.selectUserGroup;
  btndisabled:boolean=false;
  isScreenLock;
  ddl_RoleGroup=[];
  selectedRoleGroup=[];
  roleLabelId="rolesLabel";

  clearText = Constants.clearText;
  updateField=Constants.updateField;
  
  // Settings configuration
  ddSettings: IMultiSelectSettings = {
      buttonClasses: 'btn dropdown-toggle formm-dd',
      enableSearch: true,
      showCheckAll: true,
      showUncheckAll: true
  };
  
  // Text configuration
  ddTexts: IMultiSelectTexts = {
      defaultTitle: this.selectUserGroup,
      checkAll: this.selectAll,
      uncheckAll: this.unselectAll,
  };
  
  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private userGroupService: UserGroupService,
      
      private getScreenDetailService: GetScreenDetailService,
  ) {
    
    
  }
  
  // Screen initialization
  ngOnInit() {
      this.userGroupService.getRoleGroupList().subscribe(data => {
          this.ddOptions = data;
          for(var i=0;i<data.length;i++)
          {
              this.ddl_RoleGroup.push({"id":data[i].id,"itemName":data[i].name });
            
          }

         

      });
      this.route.params.subscribe((params: Params) => {
          
          this.userGroupId = params['userGroupId'];
          if (this.userGroupId != '' && this.userGroupId != undefined) {

           
            
              this.screenCode = "S-1016";
            
              //default form values for editting user group
              this.defaultFormValues = [
                  { 'fieldName': 'EDIT_USER_GROUP_GROUPNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                  { 'fieldName': 'EDIT_USER_GROUP_ROLEGROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                  { 'fieldName': 'EDIT_USER_GROUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                  { 'fieldName': 'EDIT_USER_GROUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                  { 'fieldName': 'EDIT_USER_GROUP_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
              ];
            
              this.lblRole = "Edit Group";
              this.userGroupService.getGroup(this.userGroupId).then(data => {
                  this.model = data.result; 
                  window.setTimeout(() => {
                      this.model.roleGroupList = this.model.roleGroupIds;

                      //   this.selectedRoles=this.model.roleIdList;
                      var RolesGrouplist=[];
                        for(var i=0;i<this.model.roleGroupList.length;i++){
                          debugger;
                            var rolegroup=this.ddl_RoleGroup.filter(p => p.id == this.model.roleGroupList[i]);
                            for(var j=0;j<rolegroup.length;j++){
                                RolesGrouplist.push(rolegroup[j]);
                            }
                         
                        }

                        this.selectedRoleGroup=RolesGrouplist;
                       
                       
                  }, 100);
              });


             

              
          }
          else {
              this.model.groupName = "";
              this.model.groupDesc = "";
              this.model.id = "";
              this.screenCode = "S-1015";
              
              //default form values adding new user group
              this.defaultFormValues = [
                  { 'fieldName': 'ADD_NEW_USER_GROUP_GROUPNAME', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':''  },
                  { 'fieldName': 'ADD_NEW_USER_GROUP_ROLEGROUP', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                  { 'fieldName': 'ADD_NEW_USER_GROUP_DESCRIPTION', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                  { 'fieldName': 'ADD_NEW_USER_GROUP_SAVE', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '' ,'deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
                  { 'fieldName': 'ADD_NEW_USER_GROUP_CANCEL', 'fieldValue': '', 'helpMessage': '', 'listDtoFieldValidationMessage': '','deleteAccess':'', 'readAccess':'' , 'writeAccess':'' },
              ];
          }
      });
      
      //getting screen for user group
      this.getScreenDetailService.ValidateScreen(this.screenCode).then(res=>
          {
              this.isScreenLock = res;
          });
          this.getScreenDetailService.getScreenDetailUser(this.moduleCode, this.screenCode).then(data => {
              this.moduleName = data.result.moduleName;
              this.screenName = data.result.dtoScreenDetail.screenName;
              this.availableFormValues = data.result.dtoScreenDetail.fieldList;
              for (var j = 0; j < this.availableFormValues.length; j++) {
                  var fieldKey = this.availableFormValues[j]['fieldName'];
                  var objAvailable = this.availableFormValues.find(x => x['fieldName'] === fieldKey);
                  var objDefault = this.defaultFormValues.find(x => x['fieldName'] === fieldKey);
                  objDefault['fieldValue'] = objAvailable['fieldValue'];
                  objDefault['helpMessage'] = objAvailable['helpMessage'];
                  objDefault['deleteAccess'] = objAvailable['deleteAccess'];
                  objDefault['readAccess'] = objAvailable['readAccess'];
                  objDefault['writeAccess'] = objAvailable['writeAccess'];
                  objDefault['listDtoFieldValidationMessage'] = objAvailable['listDtoFieldValidationMessage'];
              }
          });

          debugger;
         
          if (this.userGroupId != '' && this.userGroupId != undefined) {
            this.onAutocompleteFocus('rolesLabel');
        }
      }
      
      // function call for creating new group
      CreateGroup(f: NgForm) {
          this.btndisabled=true;
debugger;

          if (this.selectedRoleGroup.length > 0) {


            var selectedRoleGroupIds = [];
            if(this.selectedRoleGroup.length!=0){

                for (var i = 0; i < this.selectedRoleGroup.length; i++) {
                    var role = this.selectedRoleGroup[i];
                    
                    selectedRoleGroupIds.push(role.id);
                }

                this.model.roleGroupList=selectedRoleGroupIds;
                //this.model.dayIds=selectedDaysIds;
                //this.model.dayIds=this.selectDays;
            }

              if (this.userGroupId != '' && this.userGroupId != undefined) {
                  this.userGroupService.updateGroup(this.model).then(data => {
                      var datacode = data.code;
                      this.btndisabled=false;
                      if (datacode == 200) {
                          window.setTimeout(() => {
                              this.hasMsg = true;
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.messageText = data.btiMessage.message;;
                          }, 100);
                          window.setTimeout(() => {
                              this.router.navigate(['usergroup']);
                          }, 2000);
                      }else{
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = data.btiMessage.message;
                          window.setTimeout(() => {
                              this.hasMsg = false;  
                          }, 4000);
                      }
                  });
              }
              else {
                  this.userGroupService.createGroup(this.model).then(data => {
                      window.scrollTo(0, 0);
                      var datacode = data.code;
                      this.btndisabled=false;
                      if (datacode == 201) {
                          window.setTimeout(() => {
                              this.isSuccessMsg = true;
                              this.isfailureMsg = false;
                              this.showMsg = true;
                              window.setTimeout(() => {
                                  this.showMsg = false;
                                  this.hasMsg = false;
                              }, 4000);
                              this.messageText = data.btiMessage.message;;
                          }, 100);
                          window.setTimeout(() => {
                              this.router.navigate(['usergroup']);
                          }, 2000);
                          this.hasMsg = true;
                          window.setTimeout(() => {
                              this.showMsg = false;
                              this.hasMsg = false;
                          }, 4000);
                          f.resetForm();
                      } else{
                          this.isSuccessMsg = false;
                          this.isfailureMsg = true;
                          this.showMsg = true;
                          this.hasMsg = true;
                          this.messageText = data.btiMessage.message;
                          window.setTimeout(() => {
                              this.hasMsg = false;  
                          }, 4000);
                          
                      }
                  });
              }
          }
          else {
              window.setTimeout(() => {
                  this.isSuccessMsg = false;
                  this.isfailureMsg = true;
                  this.showMsg = true;
                  this.messageText = "Please select User Group'!";
              }, 100);
          }
      }

      Clear(f: NgForm) {
        // f.resetForm({ country: 0, state: 0, city: 0 });
        this.selectedRoleGroup=[];
        this.model=[];
      }

      onAutocompleteFocus(labelId){
        debugger;
         let elem: HTMLElement = document.getElementById(labelId);
         if(elem!=null){
            elem.setAttribute("style", "top:-18px;font-size:14px;color:#5264AE;font-weight: bold;");
         }
        
       }
       
       onAutocompleteFocusOut(labelId){
         // let elem: HTMLElement = document.getElementById(labelId);
         // elem.setAttribute("style","color:#999;font-size:14px;font-weight:normal;position:absolute;pointer-events:none;left:20px;top:5px;transition:0.2s ease all;-moz-transition:0.2s ease all;-webkit-transition:0.2s ease all;");
         
       }
      
      /** If Screen is Lock then prevent user to perform any action.
      *  This function also cover Role Management Write acceess functionality */
      LockScreen(writeAccess)
      {
          if(!writeAccess)
          {
              return true
          }
          else if(this.btndisabled)
          {
              return true
          }
          else if(this.isScreenLock)
          {
              return true;
          }
          else{
              return false;
          }
      }

}
