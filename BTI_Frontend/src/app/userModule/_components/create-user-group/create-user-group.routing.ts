import { Routes } from '@angular/router';
import { CreateUserGroupComponent } from './create-user-group.component';




export const CreateUserGroupRoutes: Routes = [{
  path: '',
  component: CreateUserGroupComponent,
  data: {
    breadcrumb: ""
  }
}];