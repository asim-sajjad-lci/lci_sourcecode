
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';

import { MultiselectDropdownModule } from '../../../../../node_modules/angular-4-dropdown-multiselect';
import { CreateUserGroupRoutes } from './create-user-group.routing';
import { CreateUserGroupComponent } from './create-user-group.component';
import { AngularMultiSelectModule } from '../../../../../node_modules/angular4-multiselect-dropdown/angular4-multiselect-dropdown';






@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CreateUserGroupRoutes),
    SharedModule,
    NgxDatatableModule,
    MultiselectDropdownModule,
    AngularMultiSelectModule
   
  ],

  
  declarations: [CreateUserGroupComponent]
})
export class CreateUserGroupModule { }
