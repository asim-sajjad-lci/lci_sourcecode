import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyListRoutes } from './company-list.routing';

import { RouterModule } from '@angular/router';
import { CompanyListComponent } from './company-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';





@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CompanyListRoutes),
    SharedModule,
    NgxDatatableModule,
   
  ],

  
  declarations: [CompanyListComponent]
})
export class CompanyListModule { }




