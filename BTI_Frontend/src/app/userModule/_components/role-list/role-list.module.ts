import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { NgxDatatableModule } from '../../../../../node_modules/@swimlane/ngx-datatable';
import { Ng2AutoCompleteModule } from '../../../../../node_modules/ng2-auto-complete/dist/ng2-auto-complete.module';
import { RoleListComponent } from './role-list.component';
import { RoleListRoutes } from 'app/userModule/_components/role-list/role-list.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(RoleListRoutes),
    SharedModule,
    NgxDatatableModule,
  ],
  declarations: [RoleListComponent]
})
export class RoleListModule { }

