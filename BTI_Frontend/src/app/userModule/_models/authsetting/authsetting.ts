/**
 * A model for an authorization settings
 */
export class AuthSetting
{
     id: number;
    companyId:number;
    authorizedUserGroupId:string;
    dayIds : string[];
    userIds : string[];
    startDate:string;
    endDate:string;
    startTime:string;
    endTime:string;
    daysList:string[];
    authSettingId:number;
    companyName:string;
    authorizedGroupName:string;

    //initializing Authorization parameters
    constructor (id: number, companyId:number,authorizedUserGroupId:string,dayIds:string[],
        userIds:string[],startDate:string,endDate:string,startTime:string,endTime:string,daysList:string[], authSettingId:number,
    companyName:string,authorizedGroupName:string)
        {
            this.id=id;
            this.companyId=companyId;
            this.authorizedUserGroupId=authorizedUserGroupId;
            this.dayIds=dayIds;
            this.userIds=userIds;
            this.startDate=startDate;
            this.endDate=endDate;
            this.startTime=startTime;
            this.endTime=endTime;
            this.daysList=daysList;
            this.authSettingId= authSettingId;
            this.companyName= companyName;
            this.authorizedGroupName= authorizedGroupName;
        }
}