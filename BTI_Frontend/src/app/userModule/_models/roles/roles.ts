/**
 * A model for roles
 */
export class Role{
    id:number;
    roleId:string;
    roleName:string;
    roleDescription:string;
    accessRoleId:string;
    modulesList=[];
    
    //initializing role parameters
    constructor(id:number,roleId:string,roleName:string,roleDescription:string,accessRoleId:string,modulesList: any[])
      {
            this.id=id;
            this.roleId=roleId;
            this.roleName=roleName;
            this.roleDescription=roleDescription;
            this.accessRoleId=accessRoleId;
            this.modulesList=modulesList;
      }
}