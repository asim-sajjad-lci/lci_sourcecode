"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var dialog_config_1 = require("./dialog-config");
var VerticalCenterDirective = (function () {
    function VerticalCenterDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
        //Passed from parent view.
        this.dialogPaddingTop = 0;
    }
    VerticalCenterDirective.prototype.ngOnInit = function () {
        //Initial load need a little top offset.
        this.SetCenter(undefined, true);
    };
    VerticalCenterDirective.prototype.onResize = function (event) {
        this.SetCenter(event);
    };
    VerticalCenterDirective.prototype.SetCenter = function (event, isInit) {
        var eventTarget = event == undefined ? window : event.target;
        var wh = eventTarget.innerHeight;
        var sy = eventTarget.scrollY; //Top invisible height when scroll down.
        var ch = this.element.nativeElement.offsetHeight - this.dialogPaddingTop; //Dialog visible height
        //IE doesn't support scrollY but it automatically scrolls back to the top 0 position.
        //The scrollY needs to be added for Google Chrome, Firefox, and Microsoft Edge.
        var paddingTopValue = (wh - ch) / 2 + (sy == undefined ? 0 : sy) - dialog_config_1.DialogConfig.topOffset;
        if (paddingTopValue < 0) {
            paddingTopValue = 0;
        }
        //Cache dialogPaddingTop value for use in next resize.
        this.dialogPaddingTop = paddingTopValue;
        if (isInit) {
            paddingTopValue = paddingTopValue - dialog_config_1.DialogConfig.topOffset / 1.5;
        }
        this.renderer.setElementStyle(this.element.nativeElement, 'padding-top', paddingTopValue + 'px');
        this.renderer.setElementStyle(this.element.nativeElement, 'margin-right', 'auto');
        this.renderer.setElementStyle(this.element.nativeElement, 'margin-left', 'auto');
    };
    return VerticalCenterDirective;
}());
__decorate([
    core_1.HostListener('window:resize', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], VerticalCenterDirective.prototype, "onResize", null);
VerticalCenterDirective = __decorate([
    core_1.Directive({
        selector: '[vertical-center]'
    }),
    __metadata("design:paramtypes", [core_1.ElementRef, core_1.Renderer])
], VerticalCenterDirective);
exports.VerticalCenterDirective = VerticalCenterDirective;
//# sourceMappingURL=vertical-center.directive.js.map