//import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';

import { SettingsService } from './../../userModule/_services/settings/settings.service';
import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, AfterViewInit, Inject, HostListener } from '@angular/core';
import 'rxjs/add/operator/filter';
import { state, style, transition, animate, trigger, AUTO_STYLE } from '@angular/animations';

import { MenuItems } from '../../shared/menu-items/menu-items';
import { Router } from '@angular/router';

import { SelectCompanyService } from '../../authentications/_services/select-company.service';
import { GetScreenDetailService } from 'app/_sharedresource/_services/get-screen-detail.service';
import { DOCUMENT } from '../../../../node_modules/@angular/platform-browser';
import { Constants } from '../../_sharedresource/Constants';


export interface Options {
  heading?: string;
  removeFooter?: boolean;
  mapHeader?: boolean;
}

@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  providers: [SettingsService,GetScreenDetailService,SelectCompanyService],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('slideOnOff', [
      state('on', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('off', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('on => off', animate('400ms ease-in-out')),
      transition('off => on', animate('400ms ease-in-out'))
    ]),
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ])
  ]
})

export class AdminLayoutComponent implements OnInit {
  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  chatToggle = 'out';
  chatInnerToggle = 'off';
  innerHeight: string;
  isScrolled = false;
  isCollapsedMobile = 'no-block';
  toggleOn = true;
  windowWidth: number;
  currentmodule;
  ItemsMenu;
  sideMenus="";
  sideMenuItems;
  currentCompany;
  arrLanguage:[object];
  arrCompany:any=[];
  screenCategoryList: [object];
  resultList: [object];
  moduleId: string;
  assignBadge;
  currentLanguage:string;
  sidebarValues: Array<Object>;
  loggedInUserRole:string;
  currentLanguageName;
  headerValues = [];
  logoutConfirmation=Constants.logoutConfirmation;
  confirmationModalTitle=Constants.confirmationModalTitle;
  OkText = Constants.OkText;
  CancelText = Constants.CancelText;
  currentHeader;
  currentLng;


  isConfirmationModalOpen:boolean=false;
  @ViewChild('searchFriends') search_friends: ElementRef;
  @ViewChild('toggleButton') toggle_button: ElementRef;
  @ViewChild('sideMenu') side_menu: ElementRef;
  constructor(
    public menuItems: MenuItems, 
    private router: Router,
    private getScreenDetailService: GetScreenDetailService,
    private selectCompanyService: SelectCompanyService,
    private settingsService: SettingsService,@Inject(DOCUMENT) private document: any) 
    {
     
     
    this.currentmodule = localStorage.getItem('currentModule');
    this.sideMenuItems=localStorage.getItem("SidebarMenus");
    this.currentCompany= localStorage.getItem("currentCompany");


    this.setBadge();

    this.assignBadge = localStorage.getItem('assignBadge');
     
     var currentLanguage=localStorage.getItem('currentLanguage')?
                            localStorage.getItem('currentLanguage'):"1";

    this.currentLanguage=currentLanguage;

    this.currentLanguageName= localStorage.getItem('currentLanguageName');

    var userData = JSON.parse(localStorage.getItem('currentUser'));
    if(userData != null){
      this.loggedInUserRole=userData.role;
    }
   
    if(this.currentmodule!=2 && this.currentmodule!=21){
     
    this.getScreenDetailService.getSideBarDetail(this.currentmodule).subscribe(data => {
    
      console.log("sidebarValues....................");
      this.sideMenuItems=data.result[0];
      this.sideMenus=this.sideMenuItems.sideMenuList;
      console.log("side menus",this.sideMenus);
    });
  }
    // this.getScreenDetailService
         
    //       .getCurrentMenus(this.currentmodule)
    //       .(sidebarValues=>{ 

    //         console.log("sidebarValues....................");
    //         console.log(JSON.stringify(sidebarValues));
            

    //         if(typeof sidebarValues == 'string'){
    //           this.sidebarValues = [];
    //         } else {
    //           this.sidebarValues = sidebarValues;
    //         }
    //         // alert("this.sidebarValues: " + this.sidebarValues['moduleId']);
    //         if (this.sidebarValues['moduleId'] == '2') {
    //           // location.href = '/hcm/hcmMenu';
    //           // alert(location.href);
    //           // this.router.navigate(["/hcm/hcmMenu"]);
    //         }

    //        // For crm module
    //         if (this.sidebarValues['moduleId'] == 101 || 102) {
    //           // this.getCrmMenus();
    //         }

           
    // });
 
    // debugger;

    if(this.currentmodule==2 || this.currentmodule==21){
      if(this.sideMenuItems!="" && this.sideMenuItems!="undefined"){
        debugger;
        
        this.sideMenus = JSON.parse(localStorage.getItem("SidebarMenus"));
        console.log("side menus",this.sideMenus);
      }
    }
    
    




    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);


  }

  ngOnInit() {
    // this.setSideBar();

  

    if(this.currentLanguage=='2'){
      this.document.getElementById('pcoded').setAttribute('vertical-placement', 'right');
    }
    else{
      this.document.getElementById('pcoded').setAttribute('vertical-placement', 'left');
    }

    this.getScreenDetailService.getLanguageListForDropDown().subscribe(data =>
      {
       
               this.arrLanguage = data
      });

      this.selectCompanyService.getCompanyList().then(data => {
        if(data.result)
        {
          console.log("companies",data.result);
          this.arrCompany=data.result;
        }
     
 });




      







      
  }

  onClickedOutside(e: Event) {
    if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
      this.toggleOn = true;
      this.verticalNavType = 'offcanvas';
    }
  }


 

  gotoAllModules() {
   
    this.router.navigate(["module_list"]);
  }

  setSideBar(){
  
    if (this.currentmodule == "userModule") {
      this.ItemsMenu = this.menuItems.getAll();
    }
    else if (this.currentmodule == "crmModule") {
      this.ItemsMenu = this.menuItems.getAllCRM();
    }

    else {
      this.ItemsMenu = this.menuItems.getAll();
    }
  }


  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  searchFriendList(event) {
    const search = (this.search_friends.nativeElement.value).toLowerCase();
    let search_input: string;
    let search_parent: any;
    const friendList = document.querySelectorAll('.userlist-box .media-body .chat-header');
    Array.prototype.forEach.call(friendList, function (elements, index) {
      search_input = (elements.innerHTML).toLowerCase();
      search_parent = (elements.parentNode).parentNode;
      if (search_input.indexOf(search) !== -1) {
        search_parent.classList.add('show');
        search_parent.classList.remove('hide');
      } else {
        search_parent.classList.add('hide');
        search_parent.classList.remove('show');
      }
    });
  }


  

  toggleChat() {
    this.chatToggle = this.chatToggle === 'out' ? 'in' : 'out';
  }

  toggleChatInner() {
    this.chatInnerToggle = this.chatInnerToggle === 'off' ? 'on' : 'off';
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
      this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
      this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
    } else {
      this.verticalNavType = this.verticalNavType === 'expanded' ? 'collapsed' : 'expanded';
    }
  }
  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onScroll(event) {
    this.isScrolled = false;
  }

  ChangeLanguage(row) {
debugger;
    if(row.languageId==2 && this.currentmodule==2){
      this.currentmodule=21
    }
    else if(row.languageId==1 && this.currentmodule==21){
      this.currentmodule=2
    }
   
    this.currentLng=row.languageId;
    localStorage.setItem('currentLanguage',  row.languageId);
    localStorage.setItem('currentLanguageName',  row.languageName);

    this.getScreenDetailService.getLanguageById(row.languageId).then(data => {
          localStorage.setItem('languageOrientation',  data.result.languageOrientation);
                              
     });


     if(this.currentmodule!=1 && this.currentmodule!=13){
      this.getHeaderList();
    }    
    else{
      window.location.reload();
    }




 }


ConfirmLogout(){
  this.isConfirmationModalOpen =true;
}

closeModal(){
  this.isConfirmationModalOpen =false;
}

  Logout() {
    var userData = JSON.parse(localStorage.getItem('currentUser'));
      this.settingsService.logOut(userData.userId).then(data => {
        
        localStorage.setItem('currentUser', '');
        //to reset selected module
        localStorage.setItem('currentModule', '');
        localStorage.setItem('SidebarMenus', '');
        localStorage.setItem('tenantid','');
        this.router.navigate(['login']);

    });
   // this.companyService.changeCompanyName("");
  }

  changeCompany(company){

   this.currentCompany=company.name;

  }

  goToPage(path){
    debugger
    this.router.navigate([path]);
   }


   setBadge() {

    // Observable.interval(1000).takeWhile(() => true).subscribe(() =>
    //   this.assignBadge = localStorage.getItem('assignBadge')
    // );
  }


  setCurrentHeader(moduleId,header){

debugger;
    var SidebarMenus = [];
   // SidebarMenus[0] = prompt("sideMunus");
   this.getScreenDetailService.setCurrentModule(moduleId)
localStorage.setItem("SidebarMenus", JSON.stringify(header[0].screenCategoryList));

//...
//var storedNames = JSON.parse(localStorage.getItem("names"));
   
    localStorage.setItem('currentModule', moduleId);


    //this.router.navigate(["dashboard"]);
    debugger;

    window.location.reload();
    this.router.navigateByUrl('/AdminLayoutComponent', {skipLocationChange: true}).then(()=>
this.router.navigate(["dashboard"])); 
  }


  

  getHeaderList() {
    //this.updateSession();

    debugger;
      localStorage.setItem('currentLanguage',this.currentLng)
    this.getScreenDetailService.getHeaderDetail().subscribe(data =>
      {


        if(data.btiMessage.messageShort != 'FORBIDDEN')
        {
         
          this.headerValues = data.result;

          // for (let i = 0; i < this.headerValues.length; i++) {
          //   let mid = this.headerValues[i]['moduleId'];
           
          //       // this.screenCategoryList = data.result[i].screenCategoryList;
          //       // this.getScreenDetailService.setCurrentModule(this.moduleId);
                
          
          // }
        
        
          if(data.result!=[] && data.result!=undefined){
            this.currentHeader = data.result.filter(x => x.moduleId == this.currentmodule)
            console.log("current header..",this.currentHeader)
           
          }

          this.setCurrentHeader(this.currentmodule,this.currentHeader);

          console.log("header name");
          console.log(this.headerValues);

          //remove repeated CRM module
         // this.headerValues.splice(this.headerValues.length-1, 1);
        }
        else{
        
          localStorage.setItem('currentUser', '');
          this.router.navigate(['login']);
        }
      });
  }
  // EscapeModal(event){
      
  //   var key = event.key;
  //   if(key=="Escape"){
  //       this.closeModal();
  //   }
  // }

  @HostListener('document:keyup', ['$event']) handleKeyUp(event) {
    if (event.keyCode === 27) {
        this.closeModal();
    }
  }
}
