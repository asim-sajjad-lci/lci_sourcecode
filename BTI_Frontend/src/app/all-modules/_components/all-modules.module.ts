import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AllModuleRoutes } from './all-modules.routing';
import { SharedModule } from 'app/shared/shared.module';
import { AllModulesComponent } from './all-modules.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AllModuleRoutes),
    SharedModule
],
declarations: [AllModulesComponent]
})
export class AllModulesModule {
  
 }
