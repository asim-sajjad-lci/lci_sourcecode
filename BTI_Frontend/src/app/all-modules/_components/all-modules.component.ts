import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { GetScreenDetailService } from '../../_sharedresource/_services/get-screen-detail.service';
import { DOCUMENT } from '../../../../node_modules/@angular/platform-browser';
import { Constants } from '../../_sharedresource/Constants';

@Component({
  selector: 'app-all-modules',
  templateUrl: './all-modules.component.html',
  styleUrls: ['./all-modules.component.css'],
  providers: [GetScreenDetailService]
})
export class AllModulesComponent implements OnInit {

  isActive: number;
  moduleCode = Constants.userModuleCode;
  screenCode = "S-1003";
  screenName;
  defaultFormValues: [object];
  availableFormValues: [object];
  headerValues : [object];
  currentLanguage : string;
  loggedInUserRole:string;
  arrLanguage:[object];
  forbiddenMsg=Constants.ForbiddenMsg;
  Close=Constants.close;
  opportunityBadge;
  
  followMsgs;

  constructor( private router: Router,
    private getScreenDetailService:GetScreenDetailService,
    // private settingsService: SettingsService,
    // private languageSetupService:LanguageSetupService,
    // private companyService:CompanyService,
    // private commonService: CommonService,
    @Inject(DOCUMENT) private document: any) { 
    
  }

  ngOnInit() {
    debugger;
    this.getHeaderList();
  }

  getHeaderList() {
    //this.updateSession();

    
      
    this.getScreenDetailService.getHeaderDetail().subscribe(data =>
      {


        if(data.btiMessage.messageShort != 'FORBIDDEN')
        {
         
          this.headerValues = data.result;

        
          console.log("header name");
          console.log(this.headerValues);

          //remove repeated CRM module
         // this.headerValues.splice(this.headerValues.length-1, 1);
        }
        else{
        
          localStorage.setItem('currentUser', '');
          this.router.navigate(['login']);
        }
      });
  }

  setCurrentHeader(moduleId,header){


    var SidebarMenus = [];
   // SidebarMenus[0] = prompt("sideMunus");
   this.getScreenDetailService.setCurrentModule(moduleId)
localStorage.setItem("SidebarMenus", JSON.stringify(header.screenCategoryList));

//...
//var storedNames = JSON.parse(localStorage.getItem("names"));
   
    localStorage.setItem('currentModule', moduleId);
    //this.router.navigate(["dashboard"]);

    this.router.navigateByUrl('/AdminLayoutComponent', {skipLocationChange: true}).then(()=>
this.router.navigate(["dashboard"])); 
  }


  // setCurrentHeader(event,moduleId) {
   
  //   event.preventDefault();
  //   this.getScreenDetailService.setCurrentModule(moduleId)
  //   localStorage.setItem('currentModule', moduleId);
  //   if (moduleId == 2 || moduleId == 21) {
  //     this.router.navigate(["hcm"]);
  //   } else if (moduleId == 99 || moduleId == 100) {
  //     this.router.navigate(["ims"]);
  //   } else if (moduleId == 12 || moduleId == 14){
  //     this.router.navigate(["masterdata"]);
  //   } else if (moduleId == 101 || moduleId == 102){
  //     this.router.navigate(["crmdashboard"]);
    
  //   } else {
  //     this.router.navigate(["dashboard"]);
  //   }
  // }

}
