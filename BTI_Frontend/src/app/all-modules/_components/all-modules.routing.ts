import { Routes } from '@angular/router';


import { AllModulesComponent } from './all-modules.component';

export const AllModuleRoutes: Routes = [{
  path: '',
  component: AllModulesComponent,
  data: {
    breadcrumb: "Modules"
  }
}];