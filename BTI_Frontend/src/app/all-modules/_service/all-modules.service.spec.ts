import { TestBed, inject } from '@angular/core/testing';

import { AllModulesService } from './all-modules.service';

describe('AllModulesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllModulesService]
    });
  });

  it('should be created', inject([AllModulesService], (service: AllModulesService) => {
    expect(service).toBeTruthy();
  }));
});
