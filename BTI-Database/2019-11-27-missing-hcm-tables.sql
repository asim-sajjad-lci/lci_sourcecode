--
-- Table structure for table `hr10600`
--

DROP TABLE IF EXISTS `hr10600`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10600` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `refid` varchar(20) DEFAULT NULL,
  `wrkflwreqid` bigint(20) DEFAULT NULL,
  `loanamt` float DEFAULT NULL,
  `numofmths` smallint(6) DEFAULT NULL,
  `strtdt` date DEFAULT NULL,
  `enddt` date DEFAULT NULL,
  `remamt` float DEFAULT NULL,
  `iscmplt` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  `ded_amt` float DEFAULT NULL,
  `trxn_date` datetime DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `FK_hr10600_hr00101` (`empid`),
  CONSTRAINT `FK_hr10600_hr00101` FOREIGN KEY (`empid`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKe64athek0v5rohy2o9l1rarh` FOREIGN KEY (`empid`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `hr10601`
--

DROP TABLE IF EXISTS `hr10601`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10601` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `mnthyr` date DEFAULT NULL,
  `amt` float DEFAULT NULL,
  `pstpn` tinyint(4) DEFAULT NULL,
  `ispaid` tinyint(4) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT NULL,
  `reqidx` int(11) DEFAULT NULL,
  `hcmtrxinxd` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `FK_hr10601_hr10200` (`hcmtrxinxd`),
  KEY `FK_hr10601_hr10600` (`reqidx`),
  CONSTRAINT `FK8au3wl38jdtj5k4occ9v19wid` FOREIGN KEY (`reqidx`) REFERENCES `hr10600` (`idx`),
  CONSTRAINT `FK_hr10601_hr10200` FOREIGN KEY (`hcmtrxinxd`) REFERENCES `hr10200` (`hcmtrxinxd`),
  CONSTRAINT `FK_hr10601_hr10600` FOREIGN KEY (`reqidx`) REFERENCES `hr10600` (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hr00204`
--

DROP TABLE IF EXISTS `hr00204`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00204` (
  `empbindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `bencamt` decimal(10,3) DEFAULT NULL,
  `bencmethd` smallint(6) DEFAULT NULL,
  `dedcpert` decimal(10,5) DEFAULT NULL,
  `benceddt` datetime DEFAULT NULL,
  `benenddays` int(11) DEFAULT NULL,
  `bencfreqn` smallint(6) DEFAULT NULL,
  `bencinctv` bit(1) DEFAULT NULL,
  `bencmxlife` decimal(10,3) DEFAULT NULL,
  `bennoofdays` int(11) DEFAULT NULL,
  `bencmxperd` decimal(10,3) DEFAULT NULL,
  `bencmxyerl` decimal(10,3) DEFAULT NULL,
  `bencstdt` datetime DEFAULT NULL,
  `benctrxrq` bit(1) DEFAULT NULL,
  `bencindx` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `pycdfactr` decimal(15,6) DEFAULT NULL,
  `column_new` decimal(10,3) DEFAULT NULL,
  `bencamt_old` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`empbindx`),
  KEY `IDXloxptep54dond4v4805hnjxpk` (`empbindx`),
  KEY `FKbbmafe8ibrell2dk414rxiiaq` (`employindx`),
  KEY `FKtrufmi6xpf9o8sxcs8tn33gvj` (`bencindx`),
  CONSTRAINT `FKbbmafe8ibrell2dk414rxiiaq` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKtrufmi6xpf9o8sxcs8tn33gvj` FOREIGN KEY (`bencindx`) REFERENCES `hr40903` (`bencindx`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00204`
--

--
-- Table structure for table `hr00101`
--

DROP TABLE IF EXISTS `hr00101`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00101` (
  `employindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `empahirdt` date DEFAULT NULL,
  `empbor` datetime DEFAULT NULL,
  `empborh` varchar(255) DEFAULT NULL,
  `empcitzn` bit(1) DEFAULT NULL,
  `empfrtnm` char(31) DEFAULT NULL,
  `empfrtnma` char(31) DEFAULT NULL,
  `empgend` smallint(6) DEFAULT NULL,
  `emphirdt` datetime DEFAULT NULL,
  `employid` char(10) DEFAULT NULL,
  `empimg` longblob,
  `empimagr` bit(1) DEFAULT NULL,
  `empactiv` bit(1) DEFAULT NULL,
  `empdcdt` datetime DEFAULT NULL,
  `empresn` char(31) DEFAULT NULL,
  `emplstnm` char(31) DEFAULT NULL,
  `emplstnma` char(31) DEFAULT NULL,
  `emplstdt` datetime DEFAULT NULL,
  `empmast` smallint(6) DEFAULT NULL,
  `empmdlnm` char(31) DEFAULT NULL,
  `empmdlnma` char(31) DEFAULT NULL,
  `empsmok` bit(1) DEFAULT NULL,
  `emptitl` char(5) DEFAULT NULL,
  `emptitla` char(31) DEFAULT NULL,
  `empltyp` smallint(6) DEFAULT NULL,
  `empuserid` int(11) DEFAULT NULL,
  `empwrkhr` int(11) DEFAULT NULL,
  `empidexpr` datetime DEFAULT NULL,
  `empidexprh` varchar(255) DEFAULT NULL,
  `emppasexpr` datetime DEFAULT NULL,
  `emppasexprh` varchar(255) DEFAULT NULL,
  `empidnmbr` char(31) DEFAULT NULL,
  `emppasnmr` char(31) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `divindx` int(11) DEFAULT NULL,
  `empaddindx` int(11) DEFAULT NULL,
  `empnaindx` int(11) DEFAULT NULL,
  `locindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  `suprindx` int(11) DEFAULT NULL,
  `empnts` text,
  `proindex` int(11) DEFAULT NULL,
  PRIMARY KEY (`employindx`),
  KEY `IDX7mnv9md5c4t02d7g85ypvrr2r` (`employindx`),
  KEY `FK3jvxqupmrvps0xo9nmg5qg9fx` (`empnaindx`),
  KEY `FK618gtydif9mwlbckqxwqswock` (`potindx`),
  KEY `FK7gnv7bxynrdio8y63ruq8nh81` (`suprindx`),
  KEY `FKd6b6qq153u19xoeiji8gtbar0` (`locindx`),
  KEY `FKlr8b7sajbbq4hn1wev0g59wc3` (`divindx`),
  KEY `FKprctajrr7ltjko2wx81p1vdst` (`depindx`),
  KEY `FKs6s4vvlfreo8kxcwrmc17c109` (`empaddindx`),
  KEY `FKqwqapbjp5tqjhibflpi84g2ev` (`proindex`),
  CONSTRAINT `FK3jvxqupmrvps0xo9nmg5qg9fx` FOREIGN KEY (`empnaindx`) REFERENCES `hr00936` (`empnaindx`),
  CONSTRAINT `FK618gtydif9mwlbckqxwqswock` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FK7gnv7bxynrdio8y63ruq8nh81` FOREIGN KEY (`suprindx`) REFERENCES `hr40105` (`suprindx`),
  CONSTRAINT `FK9cuwmddi2qqiiij4fyig5fo10` FOREIGN KEY (`employindx`) REFERENCES `hr40110` (`miscindx`),
  CONSTRAINT `FKd6b6qq153u19xoeiji8gtbar0` FOREIGN KEY (`locindx`) REFERENCES `hr40104` (`locindx`),
  CONSTRAINT `FKlr8b7sajbbq4hn1wev0g59wc3` FOREIGN KEY (`divindx`) REFERENCES `hr40101` (`divindx`),
  CONSTRAINT `FKprctajrr7ltjko2wx81p1vdst` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKqwqapbjp5tqjhibflpi84g2ev` FOREIGN KEY (`proindex`) REFERENCES `hr40109` (`proindex`),
  CONSTRAINT `FKs6s4vvlfreo8kxcwrmc17c109` FOREIGN KEY (`empaddindx`) REFERENCES `hr00102` (`empaddindx`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00101`
--

--
-- Table structure for table `hr40960`
--

DROP TABLE IF EXISTS `hr40960`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40960` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(4) DEFAULT '0',
  `type_id` int(11) DEFAULT NULL,
  `type_indx_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `IDX4n7o5tyoqt3kbatoegc5afnfp` (`idx`),
  KEY `FKlhfc0k1oms3fplg11m0bwswre` (`type_indx_id`),
  CONSTRAINT `FKlhfc0k1oms3fplg11m0bwswre` FOREIGN KEY (`type_indx_id`) REFERENCES `hr40108` (`indxid`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40960`
--

--
-- Table structure for table `hr40108`
--

DROP TABLE IF EXISTS `hr40108`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40108` (
  `indxid` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL COMMENT 'previously known as simply ''id'' , now changed to it ''typeFieldCodeId''',
  `description` varchar(255) DEFAULT NULL,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  PRIMARY KEY (`indxid`),
  KEY `IDXqiwxk1e9udaqn2p00cc1p3mpm` (`indxid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

