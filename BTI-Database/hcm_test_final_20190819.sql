-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host:     Database: hcm_test_final
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hcm_test_final`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hcm_test_final` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hcm_test_final`;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `actlogid` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `creatddt` datetime DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `modulename` varchar(255) DEFAULT NULL,
  `screenname` varchar(255) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`actlogid`),
  KEY `IDXn0dl9xnmemqu83ab8chv942j2` (`actlogid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `file` longblob,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDXehba9gwo27313a6rnppv7q7kf` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachment`
--

LOCK TABLES `attachment` WRITE;
/*!40000 ALTER TABLE `attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `benefit_pay_code`
--

DROP TABLE IF EXISTS `benefit_pay_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benefit_pay_code` (
  `bencindx` int(11) NOT NULL,
  `pycdindx` int(11) NOT NULL,
  KEY `FKspe0imvwn4f0so2thyr0h6hu9` (`pycdindx`),
  KEY `FKt6hjau2dho9nbjtp12tph0no9` (`bencindx`),
  CONSTRAINT `FKspe0imvwn4f0so2thyr0h6hu9` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`),
  CONSTRAINT `FKt6hjau2dho9nbjtp12tph0no9` FOREIGN KEY (`bencindx`) REFERENCES `hr40903` (`bencindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `benefit_pay_code`
--

LOCK TABLES `benefit_pay_code` WRITE;
/*!40000 ALTER TABLE `benefit_pay_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `benefit_pay_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bti_message_hcm`
--

DROP TABLE IF EXISTS `bti_message_hcm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bti_message_hcm` (
  `bti_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `message_short` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`bti_message_id`),
  KEY `IDXi2daqd9pswwf9cif20my9uvfd` (`bti_message_id`),
  KEY `FKkoyew8jr79w4i79qbjpd111uh` (`lang_id`),
  CONSTRAINT `FKkoyew8jr79w4i79qbjpd111uh` FOREIGN KEY (`lang_id`) REFERENCES `language` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bti_message_hcm`
--

LOCK TABLES `bti_message_hcm` WRITE;
/*!40000 ALTER TABLE `bti_message_hcm` DISABLE KEYS */;
/*!40000 ALTER TABLE `bti_message_hcm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `build_department`
--

DROP TABLE IF EXISTS `build_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `build_department` (
  `build_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  KEY `FKll1x66qrt6daw38qwekbkx39s` (`department_id`),
  KEY `FK3qahyl8pfa1je01nixwdoogmy` (`build_id`),
  CONSTRAINT `FK3qahyl8pfa1je01nixwdoogmy` FOREIGN KEY (`build_id`) REFERENCES `hr10300` (`hcmbuldinx`),
  CONSTRAINT `FKll1x66qrt6daw38qwekbkx39s` FOREIGN KEY (`department_id`) REFERENCES `hr40102` (`depindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `build_department`
--

LOCK TABLES `build_department` WRITE;
/*!40000 ALTER TABLE `build_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `build_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `build_employee`
--

DROP TABLE IF EXISTS `build_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `build_employee` (
  `build_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  KEY `FKgd7l6kkgonfhxqkb1w0t1nclw` (`employee_id`),
  KEY `FKtk18ngm4msow0lrj2ri0uiw6o` (`build_id`),
  CONSTRAINT `FKgd7l6kkgonfhxqkb1w0t1nclw` FOREIGN KEY (`employee_id`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKtk18ngm4msow0lrj2ri0uiw6o` FOREIGN KEY (`build_id`) REFERENCES `hr10300` (`hcmbuldinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `build_employee`
--

LOCK TABLES `build_employee` WRITE;
/*!40000 ALTER TABLE `build_employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `build_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_master`
--

DROP TABLE IF EXISTS `city_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_master` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`city_id`),
  KEY `IDX5v1h5ej7vwrcvrujkh3r8xdw8` (`city_id`),
  KEY `FKeyydi3too6ktohdoiupipvbk3` (`lang_id`),
  KEY `FKfxtjuwt9iqx9n7xl6f8wl6uu4` (`state_id`),
  CONSTRAINT `FKeyydi3too6ktohdoiupipvbk3` FOREIGN KEY (`lang_id`) REFERENCES `language` (`lang_id`),
  CONSTRAINT `FKfxtjuwt9iqx9n7xl6f8wl6uu4` FOREIGN KEY (`state_id`) REFERENCES `state_master` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_master`
--

LOCK TABLES `city_master` WRITE;
/*!40000 ALTER TABLE `city_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`company_id`),
  KEY `IDXq79ooswnnmx4kafhpltwr6441` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country_master`
--

DROP TABLE IF EXISTS `country_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_master` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `default_country` int(11) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_id`),
  KEY `IDXplumugne3xa4n09g9l1f7nnu0` (`country_id`),
  KEY `FK6nagus764v1p8fubsfj435gvp` (`lang_id`),
  CONSTRAINT `FK6nagus764v1p8fubsfj435gvp` FOREIGN KEY (`lang_id`) REFERENCES `language` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country_master`
--

LOCK TABLES `country_master` WRITE;
/*!40000 ALTER TABLE `country_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `country_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deducation_pay_code`
--

DROP TABLE IF EXISTS `deducation_pay_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deducation_pay_code` (
  `dedcindx` int(11) NOT NULL,
  `pycdindx` int(11) NOT NULL,
  KEY `FK2jwajf23sbuujefskyg8f5492` (`pycdindx`),
  KEY `FK8wh0yt2fkh53rgc9ffpa76urf` (`dedcindx`),
  CONSTRAINT `FK2jwajf23sbuujefskyg8f5492` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`),
  CONSTRAINT `FK8wh0yt2fkh53rgc9ffpa76urf` FOREIGN KEY (`dedcindx`) REFERENCES `hr40902` (`dedcindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deducation_pay_code`
--

LOCK TABLES `deducation_pay_code` WRITE;
/*!40000 ALTER TABLE `deducation_pay_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `deducation_pay_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `employee_firstname` char(100) DEFAULT NULL,
  `employee_lastname` char(100) DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl00101_h`
--

DROP TABLE IF EXISTS `gl00101_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl00101_h` (
  `actindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accatnum` int(11) DEFAULT NULL,
  `accttype` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `actalias` char(21) DEFAULT NULL,
  `actaliasa` char(41) DEFAULT NULL,
  `acctentr` bit(1) DEFAULT NULL,
  `fxallocacc` bit(1) DEFAULT NULL,
  `actdescra` char(61) DEFAULT NULL,
  `actdescr` char(31) DEFAULT NULL,
  `actnumid` char(21) DEFAULT NULL,
  `tpclblnc` smallint(6) DEFAULT NULL,
  `unaccount` bit(1) DEFAULT NULL,
  `userdef1` char(21) DEFAULT NULL,
  `userdef2` char(21) DEFAULT NULL,
  `userdef3` char(21) DEFAULT NULL,
  `userdef4` char(21) DEFAULT NULL,
  `userdef5` char(21) DEFAULT NULL,
  PRIMARY KEY (`actindx`),
  KEY `IDXcphodcxkx08gbu06q7t87iurt` (`actindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl00101_h`
--

LOCK TABLES `gl00101_h` WRITE;
/*!40000 ALTER TABLE `gl00101_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl00101_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl00102_h`
--

DROP TABLE IF EXISTS `gl00102_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl00102_h` (
  `diminxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `dimnamea` char(61) DEFAULT NULL,
  `dimcolname` char(10) DEFAULT NULL,
  `dimname` char(31) DEFAULT NULL,
  `dimmask` char(10) DEFAULT NULL,
  PRIMARY KEY (`diminxd`),
  KEY `IDXaci5oj693t0y64lmemktdgvvk` (`diminxd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl00102_h`
--

LOCK TABLES `gl00102_h` WRITE;
/*!40000 ALTER TABLE `gl00102_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl00102_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl00103_h`
--

DROP TABLE IF EXISTS `gl00103_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl00103_h` (
  `diminxvalue` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `dimdescra` char(61) DEFAULT NULL,
  `dimdescr` char(31) DEFAULT NULL,
  `dimvalue` char(31) DEFAULT NULL,
  `diminxd` int(11) DEFAULT NULL,
  PRIMARY KEY (`diminxvalue`),
  KEY `IDXktdnac5ca6yyuryhkqia6lq7t` (`diminxvalue`),
  KEY `FK3jk11movg6d4eaf4qe1wx9cnx` (`diminxd`),
  CONSTRAINT `FK3jk11movg6d4eaf4qe1wx9cnx` FOREIGN KEY (`diminxd`) REFERENCES `gl00102_h` (`diminxd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl00103_h`
--

LOCK TABLES `gl00103_h` WRITE;
/*!40000 ALTER TABLE `gl00103_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl00103_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl00200_h`
--

DROP TABLE IF EXISTS `gl00200_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl00200_h` (
  `chekbokinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `actrowid` char(15) DEFAULT NULL,
  `bankid` char(15) DEFAULT NULL,
  `dscriptna` char(61) DEFAULT NULL,
  `dscriptn` char(31) DEFAULT NULL,
  `chekbokid` char(15) DEFAULT NULL,
  `inactive` bit(1) DEFAULT NULL,
  `nxtchknum` int(11) DEFAULT NULL,
  `nxtdepnum` int(11) DEFAULT NULL,
  PRIMARY KEY (`chekbokinx`),
  KEY `IDX9dlcivoopgcx65vobmrvwmu7x` (`chekbokinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl00200_h`
--

LOCK TABLES `gl00200_h` WRITE;
/*!40000 ALTER TABLE `gl00200_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl00200_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl40002_h`
--

DROP TABLE IF EXISTS `gl40002_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl40002_h` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `nxttrxnum` int(11) DEFAULT NULL,
  `seqnumbr` int(11) DEFAULT NULL,
  `series` int(11) DEFAULT NULL,
  `sourcdoc` varchar(255) DEFAULT NULL,
  `trxsrcpx` varchar(255) DEFAULT NULL,
  `trxsourc` varchar(255) DEFAULT NULL,
  `hcmdefinx` int(11) DEFAULT NULL,
  `hcmbuldinxd` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl42io4ttx9nrnlqc5ubah4prb` (`hcmdefinx`),
  KEY `FK189jdpfykrh81w7dqwjwetp9d` (`hcmbuldinxd`),
  CONSTRAINT `FK189jdpfykrh81w7dqwjwetp9d` FOREIGN KEY (`hcmbuldinxd`) REFERENCES `hr10350` (`hcmbuldinxd`),
  CONSTRAINT `FKl42io4ttx9nrnlqc5ubah4prb` FOREIGN KEY (`hcmdefinx`) REFERENCES `hr10301` (`hcmdefinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl40002_h`
--

LOCK TABLES `gl40002_h` WRITE;
/*!40000 ALTER TABLE `gl40002_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl40002_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gl49999_h`
--

DROP TABLE IF EXISTS `gl49999_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gl49999_h` (
  `actrowid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `sgmtnumb` int(11) DEFAULT NULL,
  `trxtblid` int(11) DEFAULT NULL,
  `actindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`actrowid`),
  KEY `IDX57ihyuikosl8jma5ol7n1r1as` (`actrowid`),
  KEY `FKsaiab36pkldhjcw06cjfw5nbx` (`actindx`),
  CONSTRAINT `FKsaiab36pkldhjcw06cjfw5nbx` FOREIGN KEY (`actindx`) REFERENCES `gl00101_h` (`actindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gl49999_h`
--

LOCK TABLES `gl49999_h` WRITE;
/*!40000 ALTER TABLE `gl49999_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `gl49999_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00101`
--

DROP TABLE IF EXISTS `hr00101`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00101` (
  `employindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `empahirdt` date DEFAULT NULL,
  `empbor` datetime DEFAULT NULL,
  `empborh` varchar(255) DEFAULT NULL,
  `empcitzn` bit(1) DEFAULT NULL,
  `empfrtnm` char(31) DEFAULT NULL,
  `empfrtnma` char(31) DEFAULT NULL,
  `empgend` smallint(6) DEFAULT NULL,
  `emphirdt` datetime DEFAULT NULL,
  `employid` char(10) DEFAULT NULL,
  `empimg` longblob,
  `empimagr` bit(1) DEFAULT NULL,
  `empactiv` bit(1) DEFAULT NULL,
  `empdcdt` datetime DEFAULT NULL,
  `empresn` char(31) DEFAULT NULL,
  `emplstnm` char(31) DEFAULT NULL,
  `emplstnma` char(31) DEFAULT NULL,
  `emplstdt` datetime DEFAULT NULL,
  `empmast` smallint(6) DEFAULT NULL,
  `empmdlnm` char(31) DEFAULT NULL,
  `empmdlnma` char(31) DEFAULT NULL,
  `empsmok` bit(1) DEFAULT NULL,
  `emptitl` char(5) DEFAULT NULL,
  `emptitla` char(31) DEFAULT NULL,
  `empltyp` smallint(6) DEFAULT NULL,
  `empuserid` int(11) DEFAULT NULL,
  `empwrkhr` int(11) DEFAULT NULL,
  `empidexpr` datetime DEFAULT NULL,
  `empidexprh` varchar(255) DEFAULT NULL,
  `emppasexpr` datetime DEFAULT NULL,
  `emppasexprh` varchar(255) DEFAULT NULL,
  `empidnmbr` char(31) DEFAULT NULL,
  `emppasnmr` char(31) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `divindx` int(11) DEFAULT NULL,
  `empaddindx` int(11) DEFAULT NULL,
  `empnaindx` int(11) DEFAULT NULL,
  `locindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  `suprindx` int(11) DEFAULT NULL,
  `bnkactnmbr` varchar(255) DEFAULT NULL,
  `empemail` varchar(255) DEFAULT NULL,
  `empnts` varchar(255) DEFAULT NULL,
  `bnkid` int(11) DEFAULT NULL,
  PRIMARY KEY (`employindx`),
  KEY `IDX7mnv9md5c4t02d7g85ypvrr2r` (`employindx`),
  KEY `FKprctajrr7ltjko2wx81p1vdst` (`depindx`),
  KEY `FKlr8b7sajbbq4hn1wev0g59wc3` (`divindx`),
  KEY `FKs6s4vvlfreo8kxcwrmc17c109` (`empaddindx`),
  KEY `FK3jvxqupmrvps0xo9nmg5qg9fx` (`empnaindx`),
  KEY `FKd6b6qq153u19xoeiji8gtbar0` (`locindx`),
  KEY `FK618gtydif9mwlbckqxwqswock` (`potindx`),
  KEY `FK7gnv7bxynrdio8y63ruq8nh81` (`suprindx`),
  KEY `FKhdk5acuabfhe44ub2etv2goy3` (`bnkid`),
  CONSTRAINT `FK3jvxqupmrvps0xo9nmg5qg9fx` FOREIGN KEY (`empnaindx`) REFERENCES `hr00936` (`empnaindx`),
  CONSTRAINT `FK618gtydif9mwlbckqxwqswock` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FK7gnv7bxynrdio8y63ruq8nh81` FOREIGN KEY (`suprindx`) REFERENCES `hr40105` (`suprindx`),
  CONSTRAINT `FKd6b6qq153u19xoeiji8gtbar0` FOREIGN KEY (`locindx`) REFERENCES `hr40104` (`locindx`),
  CONSTRAINT `FKhdk5acuabfhe44ub2etv2goy3` FOREIGN KEY (`bnkid`) REFERENCES `hr40107` (`id`),
  CONSTRAINT `FKlr8b7sajbbq4hn1wev0g59wc3` FOREIGN KEY (`divindx`) REFERENCES `hr40101` (`divindx`),
  CONSTRAINT `FKprctajrr7ltjko2wx81p1vdst` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKs6s4vvlfreo8kxcwrmc17c109` FOREIGN KEY (`empaddindx`) REFERENCES `hr00102` (`empaddindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00101`
--

LOCK TABLES `hr00101` WRITE;
/*!40000 ALTER TABLE `hr00101` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00101` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00102`
--

DROP TABLE IF EXISTS `hr00102`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00102` (
  `empaddindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `empaddr1` char(90) DEFAULT NULL,
  `empaddr2` char(90) DEFAULT NULL,
  `empaddra1` char(90) DEFAULT NULL,
  `empaddra2` char(90) DEFAULT NULL,
  `empaddid` char(10) DEFAULT NULL,
  `empbemai` char(255) DEFAULT NULL,
  `empbmobi` char(21) DEFAULT NULL,
  `empphext` char(10) DEFAULT NULL,
  `emploclnk` char(255) DEFAULT NULL,
  `empphone` char(21) DEFAULT NULL,
  `emppobox` char(31) DEFAULT NULL,
  `empemai` char(255) DEFAULT NULL,
  `empmobi` char(21) DEFAULT NULL,
  `empboxco` int(11) DEFAULT NULL,
  `loccity` int(11) DEFAULT NULL,
  `loccounty` int(11) DEFAULT NULL,
  `locstate` int(11) DEFAULT NULL,
  PRIMARY KEY (`empaddindx`),
  KEY `IDX3exilv0mu7rdnsk8mutt20syh` (`empaddindx`),
  KEY `FKbpt4luuia8tudf5k52r4v3o1y` (`loccity`),
  KEY `FK194gl75y4ysttnlxmj1k8lb49` (`loccounty`),
  KEY `FK12lec9pb17n0m05ye31w91nm3` (`locstate`),
  CONSTRAINT `FK12lec9pb17n0m05ye31w91nm3` FOREIGN KEY (`locstate`) REFERENCES `state_master` (`state_id`),
  CONSTRAINT `FK194gl75y4ysttnlxmj1k8lb49` FOREIGN KEY (`loccounty`) REFERENCES `country_master` (`country_id`),
  CONSTRAINT `FKbpt4luuia8tudf5k52r4v3o1y` FOREIGN KEY (`loccity`) REFERENCES `city_master` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00102`
--

LOCK TABLES `hr00102` WRITE;
/*!40000 ALTER TABLE `hr00102` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00102` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00103`
--

DROP TABLE IF EXISTS `hr00103`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00103` (
  `empcindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `empcaddr` char(150) DEFAULT NULL,
  `empchome` char(21) DEFAULT NULL,
  `empcmobil` char(21) DEFAULT NULL,
  `empcnma` char(150) DEFAULT NULL,
  `empcnmaa` char(150) DEFAULT NULL,
  `empcreshp` char(61) DEFAULT NULL,
  `empcseqc` int(11) DEFAULT NULL,
  `empcwrk` char(21) DEFAULT NULL,
  `empcext` char(10) DEFAULT NULL,
  `empdepseqn` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`empcindx`),
  KEY `IDXwy8p8us7680j9iwqcmtk16t9` (`empcindx`),
  KEY `FK839bb7bey3g04qa2guqlnqq9d` (`employindx`),
  CONSTRAINT `FK839bb7bey3g04qa2guqlnqq9d` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00103`
--

LOCK TABLES `hr00103` WRITE;
/*!40000 ALTER TABLE `hr00103` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00103` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00106`
--

DROP TABLE IF EXISTS `hr00106`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00106` (
  `emppsindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `employee_type` smallint(6) DEFAULT NULL,
  `position_effective_date` datetime DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `divindx` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `emppsindxd` int(11) DEFAULT NULL,
  `locindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  `suprindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`emppsindx`),
  KEY `IDXgqt2gs1x2ee02x5jra57lw3ur` (`emppsindx`),
  KEY `FKmxw39qchbomqaa09a5fs1xqxn` (`depindx`),
  KEY `FK9k81jnutt2p3iufc0oqthh1e5` (`divindx`),
  KEY `FK9yw5fono0g2t2bhngre6v585b` (`employindx`),
  KEY `FKjxfrb6jdlpaxgqqf0u4v4g2tl` (`emppsindxd`),
  KEY `FK14rqbg4rwtfakm3w4t8x8hdfs` (`locindx`),
  KEY `FK15k3jm6ry57u5e00uu9fdj5iy` (`potindx`),
  KEY `FKbxm0iec3t8ud1ropmot92fyot` (`suprindx`),
  CONSTRAINT `FK14rqbg4rwtfakm3w4t8x8hdfs` FOREIGN KEY (`locindx`) REFERENCES `hr40104` (`locindx`),
  CONSTRAINT `FK15k3jm6ry57u5e00uu9fdj5iy` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FK9k81jnutt2p3iufc0oqthh1e5` FOREIGN KEY (`divindx`) REFERENCES `hr40101` (`divindx`),
  CONSTRAINT `FK9yw5fono0g2t2bhngre6v585b` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKbxm0iec3t8ud1ropmot92fyot` FOREIGN KEY (`suprindx`) REFERENCES `hr40105` (`suprindx`),
  CONSTRAINT `FKjxfrb6jdlpaxgqqf0u4v4g2tl` FOREIGN KEY (`emppsindxd`) REFERENCES `hr00117` (`emppsindxd`),
  CONSTRAINT `FKmxw39qchbomqaa09a5fs1xqxn` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00106`
--

LOCK TABLES `hr00106` WRITE;
/*!40000 ALTER TABLE `hr00106` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00106` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00109`
--

DROP TABLE IF EXISTS `hr00109`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00109` (
  `empskindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `sklstindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`empskindx`),
  KEY `IDXuqic65wm56ehpi16t0hr9yqn` (`empskindx`),
  KEY `FKrjml7l9bbsbiaw2wpmm1q8qld` (`employindx`),
  KEY `FK2xksvq8ja7lrpgadwx6cj5vfi` (`sklstindx`),
  CONSTRAINT `FK2xksvq8ja7lrpgadwx6cj5vfi` FOREIGN KEY (`sklstindx`) REFERENCES `hr40601` (`sklstindx`),
  CONSTRAINT `FKrjml7l9bbsbiaw2wpmm1q8qld` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00109`
--

LOCK TABLES `hr00109` WRITE;
/*!40000 ALTER TABLE `hr00109` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00109` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00110`
--

DROP TABLE IF EXISTS `hr00110`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00110` (
  `empskindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `sklo` bit(1) DEFAULT NULL,
  `sklobtain` bit(1) DEFAULT NULL,
  `sklprof` int(11) DEFAULT NULL,
  `sklstreq` bit(1) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `sklexpdt` datetime DEFAULT NULL,
  `sklstseqn` int(11) DEFAULT NULL,
  `empskindx` int(11) DEFAULT NULL,
  `sklindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`empskindxd`),
  KEY `IDXi7k3i99nxrq7brc3fjdldvjpw` (`empskindxd`),
  KEY `FKs6xccfnfsh6ud5flnarmbxwam` (`empskindx`),
  KEY `FKl1d0xoknse88atj35phnu4ou1` (`sklindx`),
  CONSTRAINT `FKl1d0xoknse88atj35phnu4ou1` FOREIGN KEY (`sklindx`) REFERENCES `hr40600` (`sklindx`),
  CONSTRAINT `FKs6xccfnfsh6ud5flnarmbxwam` FOREIGN KEY (`empskindx`) REFERENCES `hr00109` (`empskindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00110`
--

LOCK TABLES `hr00110` WRITE;
/*!40000 ALTER TABLE `hr00110` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00110` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00117`
--

DROP TABLE IF EXISTS `hr00117`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00117` (
  `emppsindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `empreson` char(90) DEFAULT NULL,
  PRIMARY KEY (`emppsindxd`),
  KEY `IDX85rfao85mm4mq05l7slwxcu59` (`emppsindxd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00117`
--

LOCK TABLES `hr00117` WRITE;
/*!40000 ALTER TABLE `hr00117` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00117` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00201`
--

DROP TABLE IF EXISTS `hr00201`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00201` (
  `pstdtindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pycurrte` decimal(13,3) DEFAULT NULL,
  `pyefftdt` datetime DEFAULT NULL,
  `pynewrte` decimal(13,3) DEFAULT NULL,
  `pyrsnch` varchar(255) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `pycdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`pstdtindx`),
  KEY `IDXfh9x6lqp5ofkrbysenap9ie9h` (`pstdtindx`),
  KEY `FKhwmbjm5g7bu56ll2vpk5ewj2p` (`employindx`),
  KEY `FKjc0idkmdqyxla4jq3gqoxy6oa` (`pycdindx`),
  CONSTRAINT `FKhwmbjm5g7bu56ll2vpk5ewj2p` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKjc0idkmdqyxla4jq3gqoxy6oa` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00201`
--

LOCK TABLES `hr00201` WRITE;
/*!40000 ALTER TABLE `hr00201` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00201` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00202`
--

DROP TABLE IF EXISTS `hr00202`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00202` (
  `empdindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `dedcmethd` smallint(6) DEFAULT NULL,
  `dedcamt` decimal(10,3) DEFAULT NULL,
  `dedcpert` decimal(10,5) DEFAULT NULL,
  `dedceddt` datetime DEFAULT NULL,
  `dedcenddays` int(11) DEFAULT NULL,
  `dedcfreqn` smallint(6) DEFAULT NULL,
  `dedcinctv` bit(1) DEFAULT NULL,
  `dedcmxlife` decimal(10,3) DEFAULT NULL,
  `dedcnoofdays` int(11) DEFAULT NULL,
  `pycdfactr` decimal(15,6) DEFAULT NULL,
  `dedcmxperd` decimal(10,3) DEFAULT NULL,
  `dedcmxyerl` decimal(10,3) DEFAULT NULL,
  `dedcstdt` datetime DEFAULT NULL,
  `dedctrxrq` bit(1) DEFAULT NULL,
  `dedcindx` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`empdindx`),
  KEY `IDX5jakw0oj16lfuu85vedwcjfvy` (`empdindx`),
  KEY `FKqi2o1su583l48l0cdyp4anrhj` (`dedcindx`),
  KEY `FK9seobqw1yup039s8glm6rmwru` (`employindx`),
  CONSTRAINT `FK9seobqw1yup039s8glm6rmwru` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKqi2o1su583l48l0cdyp4anrhj` FOREIGN KEY (`dedcindx`) REFERENCES `hr40902` (`dedcindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00202`
--

LOCK TABLES `hr00202` WRITE;
/*!40000 ALTER TABLE `hr00202` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00202` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00203`
--

DROP TABLE IF EXISTS `hr00203`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00203` (
  `emppindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pycdbascdval` decimal(13,3) DEFAULT NULL,
  `pycdbascd` char(15) DEFAULT NULL,
  `baseonpaycodeid` int(11) DEFAULT NULL,
  `pycdinctv` bit(1) DEFAULT NULL,
  `pycdfactr` decimal(13,3) DEFAULT NULL,
  `pycdperd` smallint(6) DEFAULT NULL,
  `pycdrate` decimal(13,3) DEFAULT NULL,
  `pycduntpy` smallint(6) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `pycdindx` int(11) DEFAULT NULL,
  `pycdtypindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`emppindx`),
  KEY `IDXk92o958ou71qqqebxcunnacsv` (`emppindx`),
  KEY `FK5s751xnjutkrr8dxvl7p4i8q9` (`employindx`),
  KEY `FKsgr11k9cuce2dnaxjlbsatpgj` (`pycdindx`),
  KEY `FKob0dukfm1sn7y3u11drhk8kl9` (`pycdtypindx`),
  CONSTRAINT `FK5s751xnjutkrr8dxvl7p4i8q9` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKob0dukfm1sn7y3u11drhk8kl9` FOREIGN KEY (`pycdtypindx`) REFERENCES `hr40901` (`pycdtypindx`),
  CONSTRAINT `FKsgr11k9cuce2dnaxjlbsatpgj` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00203`
--

LOCK TABLES `hr00203` WRITE;
/*!40000 ALTER TABLE `hr00203` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00203` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00204`
--

DROP TABLE IF EXISTS `hr00204`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00204` (
  `empbindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `bencamt` decimal(10,3) DEFAULT NULL,
  `bencmethd` smallint(6) DEFAULT NULL,
  `dedcpert` decimal(10,5) DEFAULT NULL,
  `benceddt` datetime DEFAULT NULL,
  `benenddays` int(11) DEFAULT NULL,
  `bencfreqn` smallint(6) DEFAULT NULL,
  `bencinctv` bit(1) DEFAULT NULL,
  `bencmxlife` decimal(10,3) DEFAULT NULL,
  `bennoofdays` int(11) DEFAULT NULL,
  `pycdfactr` decimal(15,6) DEFAULT NULL,
  `bencmxperd` decimal(10,3) DEFAULT NULL,
  `bencmxyerl` decimal(10,3) DEFAULT NULL,
  `bencstdt` datetime DEFAULT NULL,
  `benctrxrq` bit(1) DEFAULT NULL,
  `bencindx` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`empbindx`),
  KEY `IDXloxptep54dond4v4805hnjxpk` (`empbindx`),
  KEY `FKtrufmi6xpf9o8sxcs8tn33gvj` (`bencindx`),
  KEY `FKbbmafe8ibrell2dk414rxiiaq` (`employindx`),
  CONSTRAINT `FKbbmafe8ibrell2dk414rxiiaq` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKtrufmi6xpf9o8sxcs8tn33gvj` FOREIGN KEY (`bencindx`) REFERENCES `hr40903` (`bencindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00204`
--

LOCK TABLES `hr00204` WRITE;
/*!40000 ALTER TABLE `hr00204` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00204` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00205`
--

DROP TABLE IF EXISTS `hr00205`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00205` (
  `empdpindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accnmbr` char(40) DEFAULT NULL,
  `accseqn` int(11) DEFAULT NULL,
  `accamnt` decimal(13,3) DEFAULT NULL,
  `bnknme` char(21) DEFAULT NULL,
  `accprnt` decimal(13,5) DEFAULT NULL,
  `accstats` smallint(6) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`empdpindx`),
  KEY `IDXpe5k6oyia3r1r8i27hn4dha3l` (`empdpindx`),
  KEY `FKt2bhcatrovrbwc5gsrhrp3dxd` (`employindx`),
  CONSTRAINT `FKt2bhcatrovrbwc5gsrhrp3dxd` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00205`
--

LOCK TABLES `hr00205` WRITE;
/*!40000 ALTER TABLE `hr00205` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00205` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00901`
--

DROP TABLE IF EXISTS `hr00901`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00901` (
  `applicindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `addrss` char(60) DEFAULT NULL,
  `age` smallint(6) DEFAULT NULL,
  `appldt` datetime DEFAULT NULL,
  `city` char(21) DEFAULT NULL,
  `colorcod` int(11) DEFAULT NULL,
  `country` char(31) DEFAULT NULL,
  `email` char(255) DEFAULT NULL,
  `frstnam` char(31) DEFAULT NULL,
  `gendr` smallint(6) DEFAULT NULL,
  `lstname` char(31) DEFAULT NULL,
  `mangrnam` char(150) DEFAULT NULL,
  `midlnam` char(31) DEFAULT NULL,
  `phone1` char(21) DEFAULT NULL,
  `phone2` char(21) DEFAULT NULL,
  `refdscrp` char(90) DEFAULT NULL,
  `refsourc` smallint(6) DEFAULT NULL,
  `rejcres` smallint(6) DEFAULT NULL,
  `rejcomns` char(90) DEFAULT NULL,
  `stats` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`applicindx`),
  KEY `IDXc23086so4q2t2v7hioemveo91` (`applicindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00901`
--

LOCK TABLES `hr00901` WRITE;
/*!40000 ALTER TABLE `hr00901` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00901` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00902`
--

DROP TABLE IF EXISTS `hr00902`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00902` (
  `aplcstindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `applicindx` tinyblob,
  `cstldgn` decimal(13,3) DEFAULT NULL,
  `cstmov` decimal(13,3) DEFAULT NULL,
  `cstothr` decimal(13,3) DEFAULT NULL,
  `reqseqcn` int(11) DEFAULT NULL,
  `reqtotal` decimal(13,3) DEFAULT NULL,
  `csttravl` decimal(13,3) DEFAULT NULL,
  `requiindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`aplcstindx`),
  KEY `IDXma2yeegxgra36buf4rkj2ew8l` (`aplcstindx`),
  KEY `FK6vks0kh7l9n7eo01xr323jr0x` (`requiindx`),
  CONSTRAINT `FK6vks0kh7l9n7eo01xr323jr0x` FOREIGN KEY (`requiindx`) REFERENCES `hr41101` (`requiindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00902`
--

LOCK TABLES `hr00902` WRITE;
/*!40000 ALTER TABLE `hr00902` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00902` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00904`
--

DROP TABLE IF EXISTS `hr00904`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00904` (
  `aplhirindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `emplmttyp` smallint(6) DEFAULT NULL,
  `hiredt` datetime DEFAULT NULL,
  `wrkhryealy` int(11) DEFAULT NULL,
  `applicindx` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`aplhirindx`),
  KEY `IDXql8hyf1rrkfwkch1qjkisqo92` (`aplhirindx`),
  KEY `FKi9dydi33ymssori39w7cuumyh` (`applicindx`),
  KEY `FK7ccmd6lrxmytwgkd2247iwpk0` (`employindx`),
  CONSTRAINT `FK7ccmd6lrxmytwgkd2247iwpk0` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKi9dydi33ymssori39w7cuumyh` FOREIGN KEY (`applicindx`) REFERENCES `hr00901` (`applicindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00904`
--

LOCK TABLES `hr00904` WRITE;
/*!40000 ALTER TABLE `hr00904` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00904` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00913`
--

DROP TABLE IF EXISTS `hr00913`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00913` (
  `miscbenindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `miscamntr` decimal(13,3) DEFAULT NULL,
  `miscprntr` decimal(15,5) DEFAULT NULL,
  `miscamnt` decimal(13,3) DEFAULT NULL,
  `miscprnt` decimal(13,5) DEFAULT NULL,
  `miscmaxamtrl` decimal(13,3) DEFAULT NULL,
  `miscmaxamtrp` decimal(13,3) DEFAULT NULL,
  `miscmaxamtrc` decimal(13,3) DEFAULT NULL,
  `miscemplor` bit(1) DEFAULT NULL,
  `miscmetodr` smallint(6) DEFAULT NULL,
  `miscenddt` datetime DEFAULT NULL,
  `miscfreq` int(11) DEFAULT NULL,
  `miscempl` bit(1) DEFAULT NULL,
  `miscmaxamtl` decimal(15,5) DEFAULT NULL,
  `miscmetod` smallint(6) DEFAULT NULL,
  `miscmaxamtp` decimal(13,3) DEFAULT NULL,
  `miscstrdt` datetime DEFAULT NULL,
  `accstats` smallint(6) DEFAULT NULL,
  `miscmaxamtc` decimal(15,5) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `miscindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`miscbenindx`),
  KEY `IDXjx4f3v2k6qe7a6bxq9fe9cu4f` (`miscbenindx`),
  KEY `FKit8epykjk3r3yidxk52ns4900` (`employindx`),
  KEY `FKae09qidhok9oeh6rxuye3eb2x` (`miscindx`),
  CONSTRAINT `FKae09qidhok9oeh6rxuye3eb2x` FOREIGN KEY (`miscindx`) REFERENCES `hr40300` (`miscindx`),
  CONSTRAINT `FKit8epykjk3r3yidxk52ns4900` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00913`
--

LOCK TABLES `hr00913` WRITE;
/*!40000 ALTER TABLE `hr00913` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00913` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00914`
--

DROP TABLE IF EXISTS `hr00914`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00914` (
  `hethinrindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hlinremexp` decimal(10,3) DEFAULT NULL,
  `hethenddt` datetime DEFAULT NULL,
  `hethstrdt` datetime DEFAULT NULL,
  `hlinremamt` decimal(10,3) DEFAULT NULL,
  `hlinrcoamt` decimal(10,3) DEFAULT NULL,
  `eligbdt` datetime DEFAULT NULL,
  `hlinrmaxbf` decimal(10,3) DEFAULT NULL,
  `overcst` bit(1) DEFAULT NULL,
  `hlpolyid` char(31) DEFAULT NULL,
  `accstats` smallint(6) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `hlinrindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hethinrindx`),
  KEY `IDXjnr1rnh4i60veqbgyqqcse4ou` (`hethinrindx`),
  KEY `FK1o37ulrdv393xxeb8dtjp0dsv` (`employindx`),
  KEY `FK2f01tdf9hafa5a2nbcu11qp3j` (`hlinrindx`),
  CONSTRAINT `FK1o37ulrdv393xxeb8dtjp0dsv` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FK2f01tdf9hafa5a2nbcu11qp3j` FOREIGN KEY (`hlinrindx`) REFERENCES `hr40301` (`hlinrindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00914`
--

LOCK TABLES `hr00914` WRITE;
/*!40000 ALTER TABLE `hr00914` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00914` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00930`
--

DROP TABLE IF EXISTS `hr00930`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00930` (
  `employindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `address` char(120) DEFAULT NULL,
  `aog` int(11) DEFAULT NULL,
  `commts` char(120) DEFAULT NULL,
  `dobdt` datetime DEFAULT NULL,
  `frstnme` char(31) DEFAULT NULL,
  `lstname` char(31) DEFAULT NULL,
  `middlnme` char(31) DEFAULT NULL,
  `gendr` smallint(6) DEFAULT NULL,
  `phone` char(21) DEFAULT NULL,
  `empdepseqn` int(11) DEFAULT NULL,
  `phonew` char(21) DEFAULT NULL,
  `loccity` int(11) DEFAULT NULL,
  `loccounty` int(11) DEFAULT NULL,
  `emprelshindx` int(11) DEFAULT NULL,
  `employindxm` int(11) DEFAULT NULL,
  `locstate` int(11) DEFAULT NULL,
  PRIMARY KEY (`employindx`),
  KEY `IDX9yywt0cjdv9v79ma6t6nkcrkc` (`employindx`),
  KEY `FK3abqyrifuf31ort7r6ary5uti` (`loccity`),
  KEY `FK1isqh9tgpkk6akaacjishrks6` (`loccounty`),
  KEY `FKao3qgec2r5857bgn4yafpaa49` (`emprelshindx`),
  KEY `FKtc55x0wqd00hgdpu27nw9ag90` (`employindxm`),
  KEY `FKcrlj1vk3y9ngth3ufjl6i6hxx` (`locstate`),
  CONSTRAINT `FK1isqh9tgpkk6akaacjishrks6` FOREIGN KEY (`loccounty`) REFERENCES `country_master` (`country_id`),
  CONSTRAINT `FK3abqyrifuf31ort7r6ary5uti` FOREIGN KEY (`loccity`) REFERENCES `city_master` (`city_id`),
  CONSTRAINT `FKao3qgec2r5857bgn4yafpaa49` FOREIGN KEY (`emprelshindx`) REFERENCES `hr00931` (`emprelshindx`),
  CONSTRAINT `FKcrlj1vk3y9ngth3ufjl6i6hxx` FOREIGN KEY (`locstate`) REFERENCES `state_master` (`state_id`),
  CONSTRAINT `FKtc55x0wqd00hgdpu27nw9ag90` FOREIGN KEY (`employindxm`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00930`
--

LOCK TABLES `hr00930` WRITE;
/*!40000 ALTER TABLE `hr00930` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00930` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00931`
--

DROP TABLE IF EXISTS `hr00931`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00931` (
  `emprelshindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `empreldscra` char(120) DEFAULT NULL,
  `empreldscr` char(61) DEFAULT NULL,
  `emprelshid` char(10) DEFAULT NULL,
  PRIMARY KEY (`emprelshindx`),
  KEY `IDX4t8coi5us7admvldgthje2ot5` (`emprelshindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00931`
--

LOCK TABLES `hr00931` WRITE;
/*!40000 ALTER TABLE `hr00931` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00931` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00935`
--

DROP TABLE IF EXISTS `hr00935`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00935` (
  `educindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `educscno` char(150) DEFAULT NULL,
  `educscdr` char(30) DEFAULT NULL,
  `educseqn` int(11) DEFAULT NULL,
  `educscyr` int(11) DEFAULT NULL,
  `educscgp` decimal(19,2) DEFAULT NULL,
  `educscmaj` char(90) DEFAULT NULL,
  `educschnm` char(90) DEFAULT NULL,
  `educscfyr` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`educindx`),
  KEY `IDX5b3wn8scagsg4cyklcilaej4w` (`educindx`),
  KEY `FKf7qfnpsci198r90syqlu6crv3` (`employindx`),
  CONSTRAINT `FKf7qfnpsci198r90syqlu6crv3` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00935`
--

LOCK TABLES `hr00935` WRITE;
/*!40000 ALTER TABLE `hr00935` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00935` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr00936`
--

DROP TABLE IF EXISTS `hr00936`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr00936` (
  `empnaindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `empnadscr` char(60) DEFAULT NULL,
  `empnadscra` char(90) DEFAULT NULL,
  `empnaid` char(5) DEFAULT NULL,
  PRIMARY KEY (`empnaindx`),
  KEY `IDXdneu4wsg1e4mbssbyjq6tvste` (`empnaindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr00936`
--

LOCK TABLES `hr00936` WRITE;
/*!40000 ALTER TABLE `hr00936` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr00936` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10100`
--

DROP TABLE IF EXISTS `hr10100`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10100` (
  `hcmtrxinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmtrxdscra` varchar(255) DEFAULT NULL,
  `hcmtrxdscr` varchar(255) DEFAULT NULL,
  `hcmtrxdt` datetime DEFAULT NULL,
  `hcmtrxid` int(11) DEFAULT NULL,
  `hcmtrxfrombuilds` bit(1) DEFAULT NULL,
  `hcmtrxfdt` datetime DEFAULT NULL,
  `hcmtrxtdt` datetime DEFAULT NULL,
  `hcmbatindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmtrxinx`),
  KEY `IDXececieakw7ackenjbgdb2vlgc` (`hcmtrxinx`),
  KEY `FKf0tnnbc0nf6tqnwqspbifsnnc` (`hcmbatindx`),
  CONSTRAINT `FKf0tnnbc0nf6tqnwqspbifsnnc` FOREIGN KEY (`hcmbatindx`) REFERENCES `hr80000` (`hcmbatindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10100`
--

LOCK TABLES `hr10100` WRITE;
/*!40000 ALTER TABLE `hr10100` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10100` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10200`
--

DROP TABLE IF EXISTS `hr10200`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10200` (
  `hcmtrxinxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmamnt` decimal(19,2) DEFAULT NULL,
  `hcmtrxdseq` int(11) DEFAULT NULL,
  `hcmtrxid` int(11) DEFAULT NULL,
  `hcmtrxfrombuild` bit(1) DEFAULT NULL,
  `hcmtrxfdt` datetime DEFAULT NULL,
  `hcmfromtrx` bit(1) DEFAULT NULL,
  `hcmparte` decimal(19,2) DEFAULT NULL,
  `hcmtrxtdt` datetime DEFAULT NULL,
  `hcmtrxtyp` smallint(6) DEFAULT NULL,
  `bencindx` int(11) DEFAULT NULL,
  `hcmbuldinx` int(11) DEFAULT NULL,
  `pycdindx` int(11) DEFAULT NULL,
  `dedcindx` int(11) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `diminxvalue` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `hcmtrxinx` int(11) DEFAULT NULL,
  `hcmbatindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmtrxinxd`),
  KEY `IDXt4g3wmml15sqqnpsswxnolng1` (`hcmtrxinxd`),
  KEY `FKtcbhe73f1f6m8rmidbumkr23n` (`bencindx`),
  KEY `FKl7xv7bwcm0khkurcifg0h44a8` (`hcmbuldinx`),
  KEY `FKdbfai3dcu8a2aw7p37e92fpt2` (`pycdindx`),
  KEY `FKk888a5wvwd6x3mgvb9tqrvkjs` (`dedcindx`),
  KEY `FK8adr728vew786ri10fqc2udvt` (`depindx`),
  KEY `FKhh43kdhxl6dnvki69ola94qfd` (`diminxvalue`),
  KEY `FKghoppdcrtk8ikiath0absqtbf` (`employindx`),
  KEY `FKqjiufl0l3nkou09079c9gw6b6` (`hcmtrxinx`),
  KEY `FKmsyqdymratn4qjhv4uej5fs95` (`hcmbatindx`),
  CONSTRAINT `FK8adr728vew786ri10fqc2udvt` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKdbfai3dcu8a2aw7p37e92fpt2` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`),
  CONSTRAINT `FKghoppdcrtk8ikiath0absqtbf` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKhh43kdhxl6dnvki69ola94qfd` FOREIGN KEY (`diminxvalue`) REFERENCES `gl00102_h` (`diminxd`),
  CONSTRAINT `FKk888a5wvwd6x3mgvb9tqrvkjs` FOREIGN KEY (`dedcindx`) REFERENCES `hr40902` (`dedcindx`),
  CONSTRAINT `FKl7xv7bwcm0khkurcifg0h44a8` FOREIGN KEY (`hcmbuldinx`) REFERENCES `hr10300` (`hcmbuldinx`),
  CONSTRAINT `FKmsyqdymratn4qjhv4uej5fs95` FOREIGN KEY (`hcmbatindx`) REFERENCES `hr80000` (`hcmbatindx`),
  CONSTRAINT `FKqjiufl0l3nkou09079c9gw6b6` FOREIGN KEY (`hcmtrxinx`) REFERENCES `hr10100` (`hcmtrxinx`),
  CONSTRAINT `FKtcbhe73f1f6m8rmidbumkr23n` FOREIGN KEY (`bencindx`) REFERENCES `hr40903` (`bencindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10200`
--

LOCK TABLES `hr10200` WRITE;
/*!40000 ALTER TABLE `hr10200` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10200` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10300`
--

DROP TABLE IF EXISTS `hr10300`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10300` (
  `hcmbuldinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmalldep` bit(1) DEFAULT NULL,
  `hcmallem` bit(1) DEFAULT NULL,
  `hcmpyr` bit(1) DEFAULT NULL,
  `hcmpbwek` bit(1) DEFAULT NULL,
  `hcmbulusr` char(15) DEFAULT NULL,
  `hcmbulddt` datetime DEFAULT NULL,
  `hcmpdms` bit(1) DEFAULT NULL,
  `hcmpyfdt` datetime DEFAULT NULL,
  `hcmpytdt` datetime DEFAULT NULL,
  `hcmdscr` char(140) DEFAULT NULL,
  `hcmfdepinx` int(11) DEFAULT NULL,
  `hcmfeminx` int(11) DEFAULT NULL,
  `hcmpmon` bit(1) DEFAULT NULL,
  `hcmpqur` bit(1) DEFAULT NULL,
  `hcmpsyr` bit(1) DEFAULT NULL,
  `hcmpsmon` bit(1) DEFAULT NULL,
  `hcmtdepinx` int(11) DEFAULT NULL,
  `hcmteminx` int(11) DEFAULT NULL,
  `hcmbultyp` smallint(6) DEFAULT NULL,
  `hcmpwek` bit(1) DEFAULT NULL,
  `hcmbatindx` int(11) DEFAULT NULL,
  `hcmdefinx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmbuldinx`),
  KEY `IDXsei56cpfh2xcoc2pv8wsospc7` (`hcmbuldinx`),
  KEY `FK89lpnhnoklkreklljv21u0knq` (`hcmbatindx`),
  KEY `FKop9uw7wi81le3mblp74p2nug2` (`hcmdefinx`),
  CONSTRAINT `FK89lpnhnoklkreklljv21u0knq` FOREIGN KEY (`hcmbatindx`) REFERENCES `hr80000` (`hcmbatindx`),
  CONSTRAINT `FKop9uw7wi81le3mblp74p2nug2` FOREIGN KEY (`hcmdefinx`) REFERENCES `hr10301` (`hcmdefinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10300`
--

LOCK TABLES `hr10300` WRITE;
/*!40000 ALTER TABLE `hr10300` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10300` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10301`
--

DROP TABLE IF EXISTS `hr10301`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10301` (
  `hcmdefinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmdefdsca` char(140) DEFAULT NULL,
  `hcmdefid` char(15) DEFAULT NULL,
  `hcmdefdsc` char(90) DEFAULT NULL,
  PRIMARY KEY (`hcmdefinx`),
  KEY `IDXg5mmc2qbpfwcstrb320qbej30` (`hcmdefinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10301`
--

LOCK TABLES `hr10301` WRITE;
/*!40000 ALTER TABLE `hr10301` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10301` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10310`
--

DROP TABLE IF EXISTS `hr10310`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10310` (
  `hcmdedinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmbuldinx` int(11) DEFAULT NULL,
  `dedcindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmdedinx`),
  KEY `IDXrblbkqcp6267dftfi05eq9gvv` (`hcmdedinx`),
  KEY `FK14kyr1qcnx2eofpc98f6ysmd1` (`hcmbuldinx`),
  KEY `FKr7xbwhcqs9jhlic4vuecm6hv2` (`dedcindx`),
  CONSTRAINT `FK14kyr1qcnx2eofpc98f6ysmd1` FOREIGN KEY (`hcmbuldinx`) REFERENCES `hr10300` (`hcmbuldinx`),
  CONSTRAINT `FKr7xbwhcqs9jhlic4vuecm6hv2` FOREIGN KEY (`dedcindx`) REFERENCES `hr40902` (`dedcindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10310`
--

LOCK TABLES `hr10310` WRITE;
/*!40000 ALTER TABLE `hr10310` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10310` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10320`
--

DROP TABLE IF EXISTS `hr10320`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10320` (
  `hcmdedinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `bencindx` int(11) DEFAULT NULL,
  `hcmbuldinx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmdedinx`),
  KEY `IDXknlkyput1ix3lh9d2vh9fovtq` (`hcmdedinx`),
  KEY `FK2gls1s3s22c9vv2whtis2qaqn` (`bencindx`),
  KEY `FKpta9sjqim3funa91po0dng307` (`hcmbuldinx`),
  CONSTRAINT `FK2gls1s3s22c9vv2whtis2qaqn` FOREIGN KEY (`bencindx`) REFERENCES `hr40903` (`bencindx`),
  CONSTRAINT `FKpta9sjqim3funa91po0dng307` FOREIGN KEY (`hcmbuldinx`) REFERENCES `hr10300` (`hcmbuldinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10320`
--

LOCK TABLES `hr10320` WRITE;
/*!40000 ALTER TABLE `hr10320` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10320` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10330`
--

DROP TABLE IF EXISTS `hr10330`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10330` (
  `hcmdedinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmbatindx` int(11) DEFAULT NULL,
  `hcmbuldinx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmdedinx`),
  KEY `IDX3uj3l6ycxsnwd4ucnb5og1i67` (`hcmdedinx`),
  KEY `FKkbamue6yakt2meooyrsagi6ju` (`hcmbatindx`),
  KEY `FKj8slltt2vrodjl0p0msnuw07l` (`hcmbuldinx`),
  CONSTRAINT `FKj8slltt2vrodjl0p0msnuw07l` FOREIGN KEY (`hcmbuldinx`) REFERENCES `hr10300` (`hcmbuldinx`),
  CONSTRAINT `FKkbamue6yakt2meooyrsagi6ju` FOREIGN KEY (`hcmbatindx`) REFERENCES `hr80000` (`hcmbatindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10330`
--

LOCK TABLES `hr10330` WRITE;
/*!40000 ALTER TABLE `hr10330` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10330` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10340`
--

DROP TABLE IF EXISTS `hr10340`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10340` (
  `hcmbuldinxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmtrxpyrl` varchar(255) DEFAULT NULL,
  `cheknmbr` varchar(255) DEFAULT NULL,
  `hcmcdbsd` int(11) DEFAULT NULL,
  `hcmcdindx` int(11) DEFAULT NULL,
  `hcmcdtdt` datetime DEFAULT NULL,
  `hcmcdtyp` smallint(6) DEFAULT NULL,
  `hcmcdfdt` datetime DEFAULT NULL,
  `hcmempseqn` int(11) DEFAULT NULL,
  `hcmbsdrte` decimal(13,10) DEFAULT NULL,
  `hcmrdsts` smallint(6) DEFAULT NULL,
  `hcmcdaamt` decimal(13,10) DEFAULT NULL,
  `hcmcdamt` decimal(13,10) DEFAULT NULL,
  `hcmcdhrs` int(11) DEFAULT NULL,
  `hcmcdprnt` decimal(13,10) DEFAULT NULL,
  `hcmbuldinx` int(11) DEFAULT NULL,
  `hcmdefinx` int(11) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `diminxvalue` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `locindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmbuldinxd`),
  KEY `IDXer2o1644vqdgdwk1xmmu4l961` (`hcmbuldinxd`),
  KEY `FKqh448w9qynt0vw1hsobhbeejd` (`hcmbuldinx`),
  KEY `FKcayxoon8odjbt3kpj5e8umgvg` (`hcmdefinx`),
  KEY `FK353e3jfqciu75vk1hy9affyt5` (`depindx`),
  KEY `FK94hgulv299dk4t5s2pjo713vy` (`diminxvalue`),
  KEY `FKdcjf1g4ry23li38rl0ujtogqg` (`employindx`),
  KEY `FKlnpp1pk6trtt8bf6rb3ppdalu` (`locindx`),
  KEY `FK67mmo1hsrf7wekpyambfscod3` (`potindx`),
  CONSTRAINT `FK353e3jfqciu75vk1hy9affyt5` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FK67mmo1hsrf7wekpyambfscod3` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FK94hgulv299dk4t5s2pjo713vy` FOREIGN KEY (`diminxvalue`) REFERENCES `gl00102_h` (`diminxd`),
  CONSTRAINT `FKcayxoon8odjbt3kpj5e8umgvg` FOREIGN KEY (`hcmdefinx`) REFERENCES `hr10301` (`hcmdefinx`),
  CONSTRAINT `FKdcjf1g4ry23li38rl0ujtogqg` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKlnpp1pk6trtt8bf6rb3ppdalu` FOREIGN KEY (`locindx`) REFERENCES `hr40104` (`locindx`),
  CONSTRAINT `FKqh448w9qynt0vw1hsobhbeejd` FOREIGN KEY (`hcmbuldinx`) REFERENCES `hr10300` (`hcmbuldinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10340`
--

LOCK TABLES `hr10340` WRITE;
/*!40000 ALTER TABLE `hr10340` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10340` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10350`
--

DROP TABLE IF EXISTS `hr10350`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10350` (
  `hcmbuldinxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `acctseqn` int(11) DEFAULT NULL,
  `hcmtrxpyrl` varchar(255) DEFAULT NULL,
  `cheknmbr` varchar(255) DEFAULT NULL,
  `hcmcdindx` int(11) DEFAULT NULL,
  `crdtamt` decimal(19,2) DEFAULT NULL,
  `debtamt` decimal(19,2) DEFAULT NULL,
  `ditpottyp` smallint(6) DEFAULT NULL,
  `actrowid` int(11) DEFAULT NULL,
  `hcmbuldinx` int(11) DEFAULT NULL,
  `hcmdefinx` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `hcmtrxinx` int(11) DEFAULT NULL,
  `hcmcalpostdt` datetime DEFAULT NULL,
  PRIMARY KEY (`hcmbuldinxd`),
  KEY `FKjcyransdp99r1j9w5bqwsq5m5` (`actrowid`),
  KEY `FKs6uh6il9pkhgmja0p5qjpcs6v` (`hcmbuldinx`),
  KEY `FKnyvt12vbq2yft0my4soslogrn` (`hcmdefinx`),
  KEY `FKsihy07sxfr0ryry1ptb8kq1pw` (`employindx`),
  KEY `FK9tgpsw2xamshxrt6ouwa0p8id` (`hcmtrxinx`),
  CONSTRAINT `FK9tgpsw2xamshxrt6ouwa0p8id` FOREIGN KEY (`hcmtrxinx`) REFERENCES `hr10100` (`hcmtrxinx`),
  CONSTRAINT `FKjcyransdp99r1j9w5bqwsq5m5` FOREIGN KEY (`actrowid`) REFERENCES `gl49999_h` (`actrowid`),
  CONSTRAINT `FKnyvt12vbq2yft0my4soslogrn` FOREIGN KEY (`hcmdefinx`) REFERENCES `hr10301` (`hcmdefinx`),
  CONSTRAINT `FKs6uh6il9pkhgmja0p5qjpcs6v` FOREIGN KEY (`hcmbuldinx`) REFERENCES `hr10300` (`hcmbuldinx`),
  CONSTRAINT `FKsihy07sxfr0ryry1ptb8kq1pw` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10350`
--

LOCK TABLES `hr10350` WRITE;
/*!40000 ALTER TABLE `hr10350` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10350` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10360`
--

DROP TABLE IF EXISTS `hr10360`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10360` (
  `hcmpyainx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmbuldinx` int(11) DEFAULT NULL,
  `pycdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpyainx`),
  KEY `IDXe78dfqwhv7u1wo5r9i2w4sm9b` (`hcmpyainx`),
  KEY `FKmitd4ee9no9muuwmydknmrssa` (`hcmbuldinx`),
  KEY `FK2u7mm676di1kauswnkmok0mso` (`pycdindx`),
  CONSTRAINT `FK2u7mm676di1kauswnkmok0mso` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`),
  CONSTRAINT `FKmitd4ee9no9muuwmydknmrssa` FOREIGN KEY (`hcmbuldinx`) REFERENCES `hr10300` (`hcmbuldinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10360`
--

LOCK TABLES `hr10360` WRITE;
/*!40000 ALTER TABLE `hr10360` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10360` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10400`
--

DROP TABLE IF EXISTS `hr10400`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10400` (
  `hcmpymtindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `chekdta` datetime DEFAULT NULL,
  `cheknmbr` int(11) DEFAULT NULL,
  `hcmmltyp` smallint(6) DEFAULT NULL,
  `hcmpydscr` char(60) DEFAULT NULL,
  `totlamt` decimal(10,3) DEFAULT NULL,
  `totlnetamt` decimal(10,3) DEFAULT NULL,
  `chekpstdt` datetime DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindx`),
  KEY `IDXpwi22lqm171pn7pnqtyg7ad7j` (`hcmpymtindx`),
  KEY `FKjj6nec2m6ap8qpd3d6rv9y4bk` (`employindx`),
  CONSTRAINT `FKjj6nec2m6ap8qpd3d6rv9y4bk` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10400`
--

LOCK TABLES `hr10400` WRITE;
/*!40000 ALTER TABLE `hr10400` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10400` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10401`
--

DROP TABLE IF EXISTS `hr10401`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10401` (
  `hcmpymtindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `hcmfdta` date DEFAULT NULL,
  `hcmtdta` date DEFAULT NULL,
  `hcmdys` int(11) DEFAULT NULL,
  `hcmhrs` int(11) DEFAULT NULL,
  `hcmamnt` decimal(19,2) DEFAULT NULL,
  `hcmamnp` decimal(19,2) DEFAULT NULL,
  `hcmpymtseq` int(11) DEFAULT NULL,
  `hcmpymtyp` smallint(6) DEFAULT NULL,
  `hcmwks` int(11) DEFAULT NULL,
  `diminxvalue` int(11) DEFAULT NULL,
  `hcmpymtindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindxd`),
  KEY `FKnwkcg1bggx267yw970vollqw` (`diminxvalue`),
  KEY `FKt7j8s7447yifnqgxehmurtdpd` (`hcmpymtindx`),
  CONSTRAINT `FKnwkcg1bggx267yw970vollqw` FOREIGN KEY (`diminxvalue`) REFERENCES `gl00103_h` (`diminxvalue`),
  CONSTRAINT `FKt7j8s7447yifnqgxehmurtdpd` FOREIGN KEY (`hcmpymtindx`) REFERENCES `hr10400` (`hcmpymtindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10401`
--

LOCK TABLES `hr10401` WRITE;
/*!40000 ALTER TABLE `hr10401` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10401` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10402`
--

DROP TABLE IF EXISTS `hr10402`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10402` (
  `hcmpymtindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `acctseqn` int(11) DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `crdtamt` tinyblob,
  `debtamt` tinyblob,
  `ditpottyp` smallint(6) DEFAULT NULL,
  `actrowid` int(11) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `hcmpymtindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindxd`),
  KEY `FKikp3b4axxhugisfxxs2lr4hip` (`actrowid`),
  KEY `FKjhcovixusjxcmvj733hkgc6np` (`depindx`),
  KEY `FKirr47s79piyo495kpvop6781g` (`hcmpymtindx`),
  KEY `FK8y837nr9ohk56tpv4rabpavxh` (`potindx`),
  CONSTRAINT `FK8y837nr9ohk56tpv4rabpavxh` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FKikp3b4axxhugisfxxs2lr4hip` FOREIGN KEY (`actrowid`) REFERENCES `gl49999_h` (`actrowid`),
  CONSTRAINT `FKirr47s79piyo495kpvop6781g` FOREIGN KEY (`hcmpymtindx`) REFERENCES `hr10400` (`hcmpymtindx`),
  CONSTRAINT `FKjhcovixusjxcmvj733hkgc6np` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10402`
--

LOCK TABLES `hr10402` WRITE;
/*!40000 ALTER TABLE `hr10402` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10402` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10500`
--

DROP TABLE IF EXISTS `hr10500`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10500` (
  `hcmactindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pycdnact` bit(1) DEFAULT NULL,
  `hcmactseqn` int(11) DEFAULT NULL,
  `pycdncur` decimal(10,3) DEFAULT NULL,
  `actefftdt` datetime DEFAULT NULL,
  `pycdnrat` decimal(10,3) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `pycdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmactindx`),
  KEY `IDX9utbdlm4kn793q3emln07opm7` (`hcmactindx`),
  KEY `FKhocomk039d4puqoi5nhf1mtpd` (`employindx`),
  KEY `FKk5o6rg8sc5vb7c9jt0jtkei1v` (`pycdindx`),
  CONSTRAINT `FKhocomk039d4puqoi5nhf1mtpd` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKk5o6rg8sc5vb7c9jt0jtkei1v` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10500`
--

LOCK TABLES `hr10500` WRITE;
/*!40000 ALTER TABLE `hr10500` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10500` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10600`
--

DROP TABLE IF EXISTS `hr10600`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10600` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `enddt` datetime DEFAULT NULL,
  `iscmplt` bit(1) DEFAULT NULL,
  `loanamt` double DEFAULT NULL,
  `numofmths` int(11) DEFAULT NULL,
  `remamt` double DEFAULT NULL,
  `strtdt` datetime DEFAULT NULL,
  `wrkflwreqid` int(11) DEFAULT NULL,
  `empid` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `FKe64athek0v5rohy2o9l1rarh` (`empid`),
  CONSTRAINT `FKe64athek0v5rohy2o9l1rarh` FOREIGN KEY (`empid`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10600`
--

LOCK TABLES `hr10600` WRITE;
/*!40000 ALTER TABLE `hr10600` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10600` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr10601`
--

DROP TABLE IF EXISTS `hr10601`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr10601` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `amt` double DEFAULT NULL,
  `ispaid` bit(1) DEFAULT NULL,
  `pstpn` bit(1) DEFAULT NULL,
  `mnthyr` datetime DEFAULT NULL,
  `reqidx` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `FK8au3wl38jdtj5k4occ9v19wid` (`reqidx`),
  CONSTRAINT `FK8au3wl38jdtj5k4occ9v19wid` FOREIGN KEY (`reqidx`) REFERENCES `hr10600` (`idx`),
  CONSTRAINT `FKl1se8ovbv64qilve6gdq37las` FOREIGN KEY (`idx`) REFERENCES `hr10600` (`idx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr10601`
--

LOCK TABLES `hr10601` WRITE;
/*!40000 ALTER TABLE `hr10601` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr10601` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40000`
--

DROP TABLE IF EXISTS `hr40000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40000` (
  `rwprindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `nxtprlnm` int(11) DEFAULT NULL,
  `incpyytd` bit(1) DEFAULT NULL,
  `dsppyratn` bit(1) DEFAULT NULL,
  `mksgjvprl` bit(1) DEFAULT NULL,
  `nxtmnlnm` int(11) DEFAULT NULL,
  `mkmulprl` bit(1) DEFAULT NULL,
  `nxtpymtnm` int(11) DEFAULT NULL,
  `diminxdd` int(11) DEFAULT NULL,
  `diminxde` int(11) DEFAULT NULL,
  `defchlmnl` int(11) DEFAULT NULL,
  `defchlprl` int(11) DEFAULT NULL,
  `diminxdp` int(11) DEFAULT NULL,
  `taxschdid` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprindx`),
  KEY `IDXa22swvpoyrokp0ig0yicmr7vv` (`rwprindx`),
  KEY `FKhv5jtwo4x0326lpy35j76ioer` (`diminxdd`),
  KEY `FKd9hlqosf2dgrl4lmxxbq4r8q4` (`diminxde`),
  KEY `FKmm838s3ljiqdpo4rs6nqvib1a` (`defchlmnl`),
  KEY `FKc7eaxwdpbvb6beeyssm5chg6n` (`defchlprl`),
  KEY `FKgkjejg1dheig3otuactv5prdi` (`diminxdp`),
  KEY `FKqs0wcuxmqfgxg0shu8avgcaw8` (`taxschdid`),
  CONSTRAINT `FKc7eaxwdpbvb6beeyssm5chg6n` FOREIGN KEY (`defchlprl`) REFERENCES `gl00200_h` (`chekbokinx`),
  CONSTRAINT `FKd9hlqosf2dgrl4lmxxbq4r8q4` FOREIGN KEY (`diminxde`) REFERENCES `gl00102_h` (`diminxd`),
  CONSTRAINT `FKgkjejg1dheig3otuactv5prdi` FOREIGN KEY (`diminxdp`) REFERENCES `gl00102_h` (`diminxd`),
  CONSTRAINT `FKhv5jtwo4x0326lpy35j76ioer` FOREIGN KEY (`diminxdd`) REFERENCES `gl00102_h` (`diminxd`),
  CONSTRAINT `FKmm838s3ljiqdpo4rs6nqvib1a` FOREIGN KEY (`defchlmnl`) REFERENCES `gl00200_h` (`chekbokinx`),
  CONSTRAINT `FKqs0wcuxmqfgxg0shu8avgcaw8` FOREIGN KEY (`taxschdid`) REFERENCES `sy03600_h` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40000`
--

LOCK TABLES `hr40000` WRITE;
/*!40000 ALTER TABLE `hr40000` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40000` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40101`
--

DROP TABLE IF EXISTS `hr40101`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40101` (
  `divindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `didscra` char(61) DEFAULT NULL,
  `didaddr` char(61) DEFAULT NULL,
  `didscr` char(31) DEFAULT NULL,
  `diviid` char(15) DEFAULT NULL,
  `didemal` char(150) DEFAULT NULL,
  `didfax` char(21) DEFAULT NULL,
  `didphone` char(21) DEFAULT NULL,
  `didcity` int(11) DEFAULT NULL,
  `didcountry` int(11) DEFAULT NULL,
  `didstate` int(11) DEFAULT NULL,
  PRIMARY KEY (`divindx`),
  KEY `IDX81lsw0sbdurlm0wk2d28gqeiu` (`divindx`),
  KEY `FKp8b6funj7h961u038twaxbuuo` (`didcity`),
  KEY `FKx3fmwrol28jf0fo7amp4kqm0` (`didcountry`),
  KEY `FKi7q963vghcihj93c871s6c04p` (`didstate`),
  CONSTRAINT `FKi7q963vghcihj93c871s6c04p` FOREIGN KEY (`didstate`) REFERENCES `state_master` (`state_id`),
  CONSTRAINT `FKp8b6funj7h961u038twaxbuuo` FOREIGN KEY (`didcity`) REFERENCES `city_master` (`city_id`),
  CONSTRAINT `FKx3fmwrol28jf0fo7amp4kqm0` FOREIGN KEY (`didcountry`) REFERENCES `country_master` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40101`
--

LOCK TABLES `hr40101` WRITE;
/*!40000 ALTER TABLE `hr40101` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40101` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40102`
--

DROP TABLE IF EXISTS `hr40102`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40102` (
  `depindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `depdscra` char(61) DEFAULT NULL,
  `depdscr` char(31) DEFAULT NULL,
  `depid` char(15) DEFAULT NULL,
  `gregoriandate` datetime DEFAULT NULL,
  `hijridate` char(61) DEFAULT NULL,
  PRIMARY KEY (`depindx`),
  KEY `IDX56nu37h4h2rfqida8uh24n817` (`depindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40102`
--

LOCK TABLES `hr40102` WRITE;
/*!40000 ALTER TABLE `hr40102` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40102` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40103`
--

DROP TABLE IF EXISTS `hr40103`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40103` (
  `potindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `potdscra` char(61) DEFAULT NULL,
  `potdscr` char(31) DEFAULT NULL,
  `potid` char(15) DEFAULT NULL,
  `potlngdscr` char(255) DEFAULT NULL,
  `rpttopot` char(15) DEFAULT NULL,
  `potclsindx` int(11) DEFAULT NULL,
  `sklstindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`potindx`),
  KEY `IDX25kfkn0ncvuhmi2iih7ke1cah` (`potindx`),
  KEY `FK1bbmd5s49jb5vgu84fs9vusqe` (`potclsindx`),
  KEY `FK2ext8agun21vu1ppuydtypnjf` (`sklstindx`),
  CONSTRAINT `FK1bbmd5s49jb5vgu84fs9vusqe` FOREIGN KEY (`potclsindx`) REFERENCES `hr40113` (`potclsindx`),
  CONSTRAINT `FK2ext8agun21vu1ppuydtypnjf` FOREIGN KEY (`sklstindx`) REFERENCES `hr40601` (`sklstindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40103`
--

LOCK TABLES `hr40103` WRITE;
/*!40000 ALTER TABLE `hr40103` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40103` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40104`
--

DROP TABLE IF EXISTS `hr40104`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40104` (
  `locindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `locdscra` char(61) DEFAULT NULL,
  `loccontc` char(80) DEFAULT NULL,
  `locdscr` char(31) DEFAULT NULL,
  `locfax` char(21) DEFAULT NULL,
  `locaddrs` char(61) DEFAULT NULL,
  `locid` char(15) DEFAULT NULL,
  `locphone` char(21) DEFAULT NULL,
  `loccity` int(11) DEFAULT NULL,
  `loccounty` int(11) DEFAULT NULL,
  `locstate` int(11) DEFAULT NULL,
  PRIMARY KEY (`locindx`),
  KEY `IDXm6xoi6h85w0v8bt1q2vbvs4xv` (`locindx`),
  KEY `FK7jr85antelxjaiejov328l38w` (`loccity`),
  KEY `FKfw46ufqylp1je62jmu9gsq6lc` (`loccounty`),
  KEY `FK3oljbyaf957hbeelqdfghhmt3` (`locstate`),
  CONSTRAINT `FK3oljbyaf957hbeelqdfghhmt3` FOREIGN KEY (`locstate`) REFERENCES `state_master` (`state_id`),
  CONSTRAINT `FK7jr85antelxjaiejov328l38w` FOREIGN KEY (`loccity`) REFERENCES `city_master` (`city_id`),
  CONSTRAINT `FKfw46ufqylp1je62jmu9gsq6lc` FOREIGN KEY (`loccounty`) REFERENCES `country_master` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40104`
--

LOCK TABLES `hr40104` WRITE;
/*!40000 ALTER TABLE `hr40104` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40104` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40105`
--

DROP TABLE IF EXISTS `hr40105`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40105` (
  `suprindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `suprdscra` char(61) DEFAULT NULL,
  `suprdscr` char(31) DEFAULT NULL,
  `emplynmn` char(150) DEFAULT NULL,
  `suprid` char(15) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`suprindx`),
  KEY `IDXkvp5auk8y9bq3n61b0pthrphd` (`suprindx`),
  KEY `FK3gegdhg43e2b11jnhynhg3e3y` (`employee_id`),
  CONSTRAINT `FK3gegdhg43e2b11jnhynhg3e3y` FOREIGN KEY (`employee_id`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40105`
--

LOCK TABLES `hr40105` WRITE;
/*!40000 ALTER TABLE `hr40105` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40105` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40107`
--

DROP TABLE IF EXISTS `hr40107`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40107` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `acct_no` varchar(50) DEFAULT NULL,
  `bnk_desc` varchar(100) DEFAULT NULL,
  `bnk_desc_ar` varchar(100) DEFAULT NULL,
  `bnk_name` varchar(100) DEFAULT NULL,
  `swft_code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40107`
--

LOCK TABLES `hr40107` WRITE;
/*!40000 ALTER TABLE `hr40107` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40107` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40108`
--

DROP TABLE IF EXISTS `hr40108`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40108` (
  `indxid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`indxid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40108`
--

LOCK TABLES `hr40108` WRITE;
/*!40000 ALTER TABLE `hr40108` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40108` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40109`
--

DROP TABLE IF EXISTS `hr40109`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40109` (
  `proindex` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `proardesc` char(120) DEFAULT NULL,
  `proarname` char(61) DEFAULT NULL,
  `prodesc` char(120) DEFAULT NULL,
  `proid` char(15) DEFAULT NULL,
  `proname` char(61) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  PRIMARY KEY (`proindex`),
  KEY `IDX74xit7m6p5gwa7jeluvbnova2` (`proindex`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40109`
--

LOCK TABLES `hr40109` WRITE;
/*!40000 ALTER TABLE `hr40109` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40109` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40113`
--

DROP TABLE IF EXISTS `hr40113`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40113` (
  `potclsindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `potclsdscra` char(31) DEFAULT NULL,
  `potclsdscr` char(15) DEFAULT NULL,
  `potclsid` char(15) DEFAULT NULL,
  PRIMARY KEY (`potclsindx`),
  KEY `IDXqkyygr2ueasbvrw0qawenr5ul` (`potclsindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40113`
--

LOCK TABLES `hr40113` WRITE;
/*!40000 ALTER TABLE `hr40113` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40113` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40123`
--

DROP TABLE IF EXISTS `hr40123`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40123` (
  `dcatchindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `dcatchfle` longblob,
  `dcatchdt` datetime DEFAULT NULL,
  `dcatchtm` datetime DEFAULT NULL,
  `dcatchtyp` smallint(6) DEFAULT NULL,
  `dcatchdscr` char(61) DEFAULT NULL,
  `dcatchrefr` char(61) DEFAULT NULL,
  `dcatchseqn` int(11) DEFAULT NULL,
  `dcatchdtype` char(61) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`dcatchindx`),
  KEY `IDXgp9k9xqo78dlws37bdji7lvwg` (`dcatchindx`),
  KEY `FK4o55agpjqg1pgfbqo3ry6ywwe` (`potindx`),
  CONSTRAINT `FK4o55agpjqg1pgfbqo3ry6ywwe` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40123`
--

LOCK TABLES `hr40123` WRITE;
/*!40000 ALTER TABLE `hr40123` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40123` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40133`
--

DROP TABLE IF EXISTS `hr40133`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40133` (
  `potplindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `potplinct` bit(1) DEFAULT NULL,
  `potpldscra` char(61) DEFAULT NULL,
  `potpldscr` char(31) DEFAULT NULL,
  `potpleddt` datetime DEFAULT NULL,
  `potplid` char(15) DEFAULT NULL,
  `potplstdt` datetime DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`potplindx`),
  KEY `IDX46yl7w8ltrwl7gt48orye9p7t` (`potplindx`),
  KEY `FKmwy390lmnl44mdax642qkusb1` (`potindx`),
  CONSTRAINT `FKmwy390lmnl44mdax642qkusb1` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40133`
--

LOCK TABLES `hr40133` WRITE;
/*!40000 ALTER TABLE `hr40133` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40133` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40143`
--

DROP TABLE IF EXISTS `hr40143`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40143` (
  `potplcindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `potplctyp` smallint(6) DEFAULT NULL,
  `potplcseqn` int(11) DEFAULT NULL,
  `potplcrte` double DEFAULT NULL,
  `bencindx` int(11) DEFAULT NULL,
  `dedcindx` int(11) DEFAULT NULL,
  `pycdindx` int(11) DEFAULT NULL,
  `potplindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`potplcindx`),
  KEY `IDX8x3593bv93xhg71hqte1wfkl6` (`potplcindx`),
  KEY `FKt0edrbcxnfw54o9ooevph030w` (`bencindx`),
  KEY `FKeq68u5r3drllq3nsutvabyj6s` (`dedcindx`),
  KEY `FKqq2fh17uk7p27yy7drie32hi6` (`pycdindx`),
  KEY `FK5ucjx9278kpcru7yvcnvdki9i` (`potplindx`),
  CONSTRAINT `FK5ucjx9278kpcru7yvcnvdki9i` FOREIGN KEY (`potplindx`) REFERENCES `hr40133` (`potplindx`),
  CONSTRAINT `FKeq68u5r3drllq3nsutvabyj6s` FOREIGN KEY (`dedcindx`) REFERENCES `hr40902` (`dedcindx`),
  CONSTRAINT `FKqq2fh17uk7p27yy7drie32hi6` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`),
  CONSTRAINT `FKt0edrbcxnfw54o9ooevph030w` FOREIGN KEY (`bencindx`) REFERENCES `hr40903` (`bencindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40143`
--

LOCK TABLES `hr40143` WRITE;
/*!40000 ALTER TABLE `hr40143` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40143` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40153`
--

DROP TABLE IF EXISTS `hr40153`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40153` (
  `potschindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `potschamt` decimal(13,3) DEFAULT NULL,
  `potschdscra` varchar(255) DEFAULT NULL,
  `potschdscr` varchar(255) DEFAULT NULL,
  `potschendt` date DEFAULT NULL,
  `potschseqn` int(11) DEFAULT NULL,
  `potschstdt` date DEFAULT NULL,
  `potplindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`potschindx`),
  KEY `IDXjotx3wneank3odxhfg3q2o97u` (`potschindx`),
  KEY `FKh9ocer1m3lro0hrpremcdk8f0` (`potplindx`),
  CONSTRAINT `FKh9ocer1m3lro0hrpremcdk8f0` FOREIGN KEY (`potplindx`) REFERENCES `hr40133` (`potplindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40153`
--

LOCK TABLES `hr40153` WRITE;
/*!40000 ALTER TABLE `hr40153` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40153` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40163`
--

DROP TABLE IF EXISTS `hr40163`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40163` (
  `potcrsindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `potcrsseqn` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  `trncclsindx` int(11) DEFAULT NULL,
  `trncindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`potcrsindx`),
  KEY `IDXfpp8jupb2tf7ujypv0jf404li` (`potcrsindx`),
  KEY `FKpmhh75gw6iyqd5s66t458i2o0` (`potindx`),
  KEY `FKtgbk8ayemh23wku9rwnc0973a` (`trncclsindx`),
  KEY `FKh9cbroa26gsc2c8amscrqkrp4` (`trncindx`),
  CONSTRAINT `FKh9cbroa26gsc2c8amscrqkrp4` FOREIGN KEY (`trncindx`) REFERENCES `hr40800` (`trncindx`),
  CONSTRAINT `FKpmhh75gw6iyqd5s66t458i2o0` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FKtgbk8ayemh23wku9rwnc0973a` FOREIGN KEY (`trncclsindx`) REFERENCES `hr40801` (`trncindxc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40163`
--

LOCK TABLES `hr40163` WRITE;
/*!40000 ALTER TABLE `hr40163` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40163` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40173`
--

DROP TABLE IF EXISTS `hr40173`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40173` (
  `potcrsdindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `potcrsdseqn` int(11) DEFAULT NULL,
  `potcrsindx` int(11) DEFAULT NULL,
  `trncclsindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`potcrsdindx`),
  KEY `IDXmqb45idpekfo813wy7ttdkeyn` (`potcrsdindx`),
  KEY `FKb6byqr9hxhfute2qomkimjpul` (`potcrsindx`),
  KEY `FKafb1a2linnhve7ga0ix5cvg1d` (`trncclsindx`),
  CONSTRAINT `FKafb1a2linnhve7ga0ix5cvg1d` FOREIGN KEY (`trncclsindx`) REFERENCES `hr40800` (`trncindx`),
  CONSTRAINT `FKb6byqr9hxhfute2qomkimjpul` FOREIGN KEY (`potcrsindx`) REFERENCES `hr40163` (`potcrsindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40173`
--

LOCK TABLES `hr40173` WRITE;
/*!40000 ALTER TABLE `hr40173` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40173` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40200`
--

DROP TABLE IF EXISTS `hr40200`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40200` (
  `atdrowid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `atacctype` smallint(6) DEFAULT NULL,
  `allwentattn` bit(1) DEFAULT NULL,
  `curryear` int(11) DEFAULT NULL,
  `lstaccrdy` datetime DEFAULT NULL,
  `nxttrxid` int(11) DEFAULT NULL,
  `nmofdywek` int(11) DEFAULT NULL,
  `nmofhrdy` int(11) DEFAULT NULL,
  PRIMARY KEY (`atdrowid`),
  KEY `IDX7s7sqo1tpbdkme221aqh4gj2k` (`atdrowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40200`
--

LOCK TABLES `hr40200` WRITE;
/*!40000 ALTER TABLE `hr40200` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40200` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40201`
--

DROP TABLE IF EXISTS `hr40201`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40201` (
  `idrow` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `atttydscr` varchar(255) DEFAULT NULL,
  `atttyindx` int(11) DEFAULT NULL,
  `atttysecqn` int(11) DEFAULT NULL,
  `resodscr` varchar(255) DEFAULT NULL,
  `resoindx` int(11) DEFAULT NULL,
  `resosecqn` int(11) DEFAULT NULL,
  PRIMARY KEY (`idrow`),
  KEY `IDXd68wvf1h8w1a255ohxh072ro3` (`idrow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40201`
--

LOCK TABLES `hr40201` WRITE;
/*!40000 ALTER TABLE `hr40201` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40201` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40202`
--

DROP TABLE IF EXISTS `hr40202`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40202` (
  `yrindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  `yract` int(11) DEFAULT NULL,
  PRIMARY KEY (`yrindx`),
  KEY `IDXjoix9f1rw6kw7723yp9v13net` (`yrindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40202`
--

LOCK TABLES `hr40202` WRITE;
/*!40000 ALTER TABLE `hr40202` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40202` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40203`
--

DROP TABLE IF EXISTS `hr40203`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40203` (
  `accrindxid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accrid` varchar(255) DEFAULT NULL,
  `accrdscra` varchar(255) DEFAULT NULL,
  `accrdscr` varchar(255) DEFAULT NULL,
  `mxacchrs` int(11) DEFAULT NULL,
  `mxhrsavl` int(11) DEFAULT NULL,
  `numofaccd` int(11) DEFAULT NULL,
  `numofaccr` int(11) DEFAULT NULL,
  `period` smallint(6) NOT NULL,
  `accrperd` smallint(6) DEFAULT NULL,
  `type` smallint(6) NOT NULL,
  `wrkhrsyr` int(11) DEFAULT NULL,
  `accbyindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`accrindxid`),
  KEY `IDX2k3pf51v6hiusdfmdn88vr42p` (`accrindxid`),
  KEY `FKk6t3kn9f4i336t27p9av14qx1` (`accbyindx`),
  CONSTRAINT `FKk6t3kn9f4i336t27p9av14qx1` FOREIGN KEY (`accbyindx`) REFERENCES `hr40213` (`accbyindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40203`
--

LOCK TABLES `hr40203` WRITE;
/*!40000 ALTER TABLE `hr40203` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40203` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40204`
--

DROP TABLE IF EXISTS `hr40204`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40204` (
  `tmecdindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accrperd` smallint(6) DEFAULT NULL,
  `tmecddscra` varchar(61) DEFAULT NULL,
  `tmecddscr` varchar(31) DEFAULT NULL,
  `tmecdinct` bit(1) DEFAULT NULL,
  `tmecdid` varchar(15) DEFAULT NULL,
  `tmecdtmty` smallint(6) DEFAULT NULL,
  `accschindx` int(11) DEFAULT NULL,
  `pycdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`tmecdindx`),
  KEY `IDX92hmbma263hpcs3c3kmce1ldy` (`tmecdindx`),
  KEY `FKnf4ux1c5rmkes4wyxdyf7nlp3` (`accschindx`),
  KEY `FKkv1i3v4lwa5ho23fkh369sb7h` (`pycdindx`),
  CONSTRAINT `FKkv1i3v4lwa5ho23fkh369sb7h` FOREIGN KEY (`pycdindx`) REFERENCES `hr40900` (`pycdindx`),
  CONSTRAINT `FKnf4ux1c5rmkes4wyxdyf7nlp3` FOREIGN KEY (`accschindx`) REFERENCES `hr40223` (`accschindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40204`
--

LOCK TABLES `hr40204` WRITE;
/*!40000 ALTER TABLE `hr40204` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40204` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40205`
--

DROP TABLE IF EXISTS `hr40205`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40205` (
  `shftindxid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `shftamnt` decimal(13,3) DEFAULT NULL,
  `shftdscra` varchar(255) DEFAULT NULL,
  `shftdscr` varchar(255) DEFAULT NULL,
  `shftinatv` bit(1) DEFAULT NULL,
  `shftperct` decimal(15,5) DEFAULT NULL,
  `shftid` varchar(255) DEFAULT NULL,
  `shfttype` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`shftindxid`),
  KEY `IDXc39hxerh3oesb3m0soo3xdxj1` (`shftindxid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40205`
--

LOCK TABLES `hr40205` WRITE;
/*!40000 ALTER TABLE `hr40205` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40205` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40211`
--

DROP TABLE IF EXISTS `hr40211`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40211` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `atttydscr` char(31) DEFAULT NULL,
  `atttyindx` int(11) DEFAULT NULL,
  `atttysecqn` int(11) DEFAULT NULL,
  `resodscr` char(31) DEFAULT NULL,
  `resoindx` int(11) DEFAULT NULL,
  `resosecqn` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX91y4iht59mw1dvm5yac35jd6r` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40211`
--

LOCK TABLES `hr40211` WRITE;
/*!40000 ALTER TABLE `hr40211` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40211` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40212`
--

DROP TABLE IF EXISTS `hr40212`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40212` (
  `accrrowid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accrud` bit(1) DEFAULT NULL,
  `enddt` datetime DEFAULT NULL,
  `perdtyp` smallint(6) DEFAULT NULL,
  `perdseqc` int(11) DEFAULT NULL,
  `strtdt` datetime DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  `yrindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`accrrowid`),
  KEY `IDXmnbgmkrx4dnj8cjmoroiau6g0` (`accrrowid`),
  KEY `FKdnw6e9tx4cbh38u4stclog4ej` (`yrindx`),
  CONSTRAINT `FKdnw6e9tx4cbh38u4stclog4ej` FOREIGN KEY (`yrindx`) REFERENCES `hr40202` (`yrindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40212`
--

LOCK TABLES `hr40212` WRITE;
/*!40000 ALTER TABLE `hr40212` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40212` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40213`
--

DROP TABLE IF EXISTS `hr40213`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40213` (
  `accbyindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accbydscr` varchar(31) DEFAULT NULL,
  `accbytyp` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`accbyindx`),
  KEY `IDXi4i7q50oa4lclt9nowrbqm0qv` (`accbyindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40213`
--

LOCK TABLES `hr40213` WRITE;
/*!40000 ALTER TABLE `hr40213` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40213` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40223`
--

DROP TABLE IF EXISTS `hr40223`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40223` (
  `accschindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accschdscra` char(61) DEFAULT NULL,
  `accschdscr` char(31) DEFAULT NULL,
  `enddt` date DEFAULT NULL,
  `accschid` char(15) DEFAULT NULL,
  `strtdt` date DEFAULT NULL,
  PRIMARY KEY (`accschindx`),
  KEY `IDX35f1ykygkudalppscq5th4jr9` (`accschindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40223`
--

LOCK TABLES `hr40223` WRITE;
/*!40000 ALTER TABLE `hr40223` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40223` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40233`
--

DROP TABLE IF EXISTS `hr40233`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40233` (
  `accschdinx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `accrdscr` char(31) DEFAULT NULL,
  `numofaccr` int(11) DEFAULT NULL,
  `accschsent` int(11) DEFAULT NULL,
  `accschseqn` int(11) DEFAULT NULL,
  `accrindxid` int(11) DEFAULT NULL,
  `accschindx` int(11) DEFAULT NULL,
  `accbyindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`accschdinx`),
  KEY `IDXbru5wl35v1i75y2q0fjiy3jp` (`accschdinx`),
  KEY `FKsvr2wuxs8nk3mqrd728m4x8b7` (`accrindxid`),
  KEY `FKelg52o66esa174vkd9vh5mwbf` (`accschindx`),
  KEY `FKqn0tipufu037l86b7ufv45f78` (`accbyindx`),
  CONSTRAINT `FKelg52o66esa174vkd9vh5mwbf` FOREIGN KEY (`accschindx`) REFERENCES `hr40223` (`accschindx`),
  CONSTRAINT `FKqn0tipufu037l86b7ufv45f78` FOREIGN KEY (`accbyindx`) REFERENCES `hr40213` (`accbyindx`),
  CONSTRAINT `FKsvr2wuxs8nk3mqrd728m4x8b7` FOREIGN KEY (`accrindxid`) REFERENCES `hr40203` (`accrindxid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40233`
--

LOCK TABLES `hr40233` WRITE;
/*!40000 ALTER TABLE `hr40233` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40233` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40300`
--

DROP TABLE IF EXISTS `hr40300`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40300` (
  `miscindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `miscid` char(15) DEFAULT NULL,
  `miscdscra` char(61) DEFAULT NULL,
  `miscamntr` decimal(13,3) DEFAULT NULL,
  `miscprntr` decimal(13,3) DEFAULT NULL,
  `miscdscr` char(31) DEFAULT NULL,
  `miscamnt` decimal(13,3) DEFAULT NULL,
  `miscprnt` decimal(13,3) DEFAULT NULL,
  `miscmaxamtrl` decimal(13,3) DEFAULT NULL,
  `miscmaxamtrp` decimal(13,3) DEFAULT NULL,
  `miscmaxamtrc` decimal(13,3) DEFAULT NULL,
  `miscemplor` bit(1) DEFAULT NULL,
  `miscmetodr` smallint(6) DEFAULT NULL,
  `miscenddt` date DEFAULT NULL,
  `miscfreq` smallint(6) DEFAULT NULL,
  `miscempl` bit(1) DEFAULT NULL,
  `miscmaxamtl` decimal(13,3) DEFAULT NULL,
  `miscmetod` smallint(6) DEFAULT NULL,
  `miscmaxamtp` decimal(13,3) DEFAULT NULL,
  `miscstrdt` date DEFAULT NULL,
  `miscmaxamtc` decimal(13,3) DEFAULT NULL,
  PRIMARY KEY (`miscindx`),
  KEY `IDXkel9nr9vr3fdoi9gdxvfpq7xc` (`miscindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40300`
--

LOCK TABLES `hr40300` WRITE;
/*!40000 ALTER TABLE `hr40300` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40300` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40301`
--

DROP TABLE IF EXISTS `hr40301`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40301` (
  `hlinrindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hlinrdscra` char(61) DEFAULT NULL,
  `hlinrdscr` char(31) DEFAULT NULL,
  `hlinremamt` decimal(13,3) DEFAULT NULL,
  `hlinrcoamt` decimal(13,3) DEFAULT NULL,
  `hlinremexp` decimal(13,3) DEFAULT NULL,
  `hlinrfreq` smallint(6) DEFAULT NULL,
  `hlinrgrno` char(31) DEFAULT NULL,
  `hlinrid` char(15) DEFAULT NULL,
  `hlinrmaxag` int(11) DEFAULT NULL,
  `hlinrmaxbf` decimal(13,3) DEFAULT NULL,
  `hlinrdepno` int(11) DEFAULT NULL,
  `inscomindx` int(11) DEFAULT NULL,
  `hlcovrindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hlinrindx`),
  KEY `IDXqjjnn9vyo6u9wart81f9ouxxi` (`hlinrindx`),
  KEY `FKbpbm18qx2s9m31uyrgwlut8rc` (`inscomindx`),
  KEY `FKous1iscp99yl58bsodm691bvc` (`hlcovrindx`),
  CONSTRAINT `FKbpbm18qx2s9m31uyrgwlut8rc` FOREIGN KEY (`inscomindx`) REFERENCES `hr40321` (`inscomindx`),
  CONSTRAINT `FKous1iscp99yl58bsodm691bvc` FOREIGN KEY (`hlcovrindx`) REFERENCES `hr40311` (`hlcovrindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40301`
--

LOCK TABLES `hr40301` WRITE;
/*!40000 ALTER TABLE `hr40301` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40301` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40311`
--

DROP TABLE IF EXISTS `hr40311`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40311` (
  `hlcovrindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hlcovrdscra` varchar(61) DEFAULT NULL,
  `hlcovrdscr` varchar(31) DEFAULT NULL,
  `hlcovrid` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`hlcovrindx`),
  KEY `IDXffywxemhiekqhy3ysql04xctl` (`hlcovrindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40311`
--

LOCK TABLES `hr40311` WRITE;
/*!40000 ALTER TABLE `hr40311` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40311` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40321`
--

DROP TABLE IF EXISTS `hr40321`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40321` (
  `inscomindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `inscomadrss` char(61) DEFAULT NULL,
  `inscomdscra` char(61) DEFAULT NULL,
  `inscomcont` char(91) DEFAULT NULL,
  `inscomdscr` char(31) DEFAULT NULL,
  `inscomemal` char(225) DEFAULT NULL,
  `inscomfax` char(21) DEFAULT NULL,
  `inscomid` char(15) DEFAULT NULL,
  `inscomphbu` char(21) DEFAULT NULL,
  `inscomcity` int(11) DEFAULT NULL,
  `inscomcountry` int(11) DEFAULT NULL,
  `inscomstate` int(11) DEFAULT NULL,
  PRIMARY KEY (`inscomindx`),
  KEY `IDX53xky6blmdsas6bq4ce102sw2` (`inscomindx`),
  KEY `FKioxlds6nmpjfhui9w9p6p2bbo` (`inscomcity`),
  KEY `FKhwgr59r1p5c47qi8wj59wpqyd` (`inscomcountry`),
  KEY `FKhykfjjr85ai5emsn6fh3b478t` (`inscomstate`),
  CONSTRAINT `FKhwgr59r1p5c47qi8wj59wpqyd` FOREIGN KEY (`inscomcountry`) REFERENCES `country_master` (`country_id`),
  CONSTRAINT `FKhykfjjr85ai5emsn6fh3b478t` FOREIGN KEY (`inscomstate`) REFERENCES `state_master` (`state_id`),
  CONSTRAINT `FKioxlds6nmpjfhui9w9p6p2bbo` FOREIGN KEY (`inscomcity`) REFERENCES `city_master` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40321`
--

LOCK TABLES `hr40321` WRITE;
/*!40000 ALTER TABLE `hr40321` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40321` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40331`
--

DROP TABLE IF EXISTS `hr40331`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40331` (
  `lfinrindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `lfinremamt` decimal(13,3) DEFAULT NULL,
  `lfinrchamt` decimal(13,3) DEFAULT NULL,
  `lfinrcovamt` decimal(13,3) DEFAULT NULL,
  `lfinreramt` decimal(13,3) DEFAULT NULL,
  `lfinrpledt` datetime DEFAULT NULL,
  `hlinrgrno` char(31) DEFAULT NULL,
  `lfinrtyp` smallint(6) DEFAULT NULL,
  `lfinrdscr` char(31) DEFAULT NULL,
  `lfinrdscra` char(61) DEFAULT NULL,
  `lfinrfreq` smallint(6) DEFAULT NULL,
  `lfinrid` char(15) DEFAULT NULL,
  `lfinrspamt` decimal(13,3) DEFAULT NULL,
  `lfinrplsdt` datetime DEFAULT NULL,
  `inscomindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`lfinrindx`),
  KEY `IDXea780lv6avl0wvcuu7eltsqu1` (`lfinrindx`),
  KEY `FKc6596tse2wen9flhg2bqe101` (`inscomindx`),
  CONSTRAINT `FKc6596tse2wen9flhg2bqe101` FOREIGN KEY (`inscomindx`) REFERENCES `hr40321` (`inscomindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40331`
--

LOCK TABLES `hr40331` WRITE;
/*!40000 ALTER TABLE `hr40331` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40331` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40341`
--

DROP TABLE IF EXISTS `hr40341`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40341` (
  `retrindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `retralwbon` bit(1) DEFAULT NULL,
  `retralwlod` bit(1) DEFAULT NULL,
  `retrminage` int(11) DEFAULT NULL,
  `retremlyramt` decimal(10,3) DEFAULT NULL,
  `retrfreq` smallint(6) DEFAULT NULL,
  `retracctn` char(31) DEFAULT NULL,
  `retramnt` decimal(10,3) DEFAULT NULL,
  `retrdscra` char(61) DEFAULT NULL,
  `retrdscr` char(31) DEFAULT NULL,
  `retrid` char(15) DEFAULT NULL,
  `retrmtperc` decimal(10,5) DEFAULT NULL,
  `retrempamty` decimal(10,3) DEFAULT NULL,
  `retrmtpercm` decimal(10,5) DEFAULT NULL,
  `retrpernt` decimal(10,5) DEFAULT NULL,
  `retrmedt` smallint(6) DEFAULT NULL,
  `retrwtdays` int(11) DEFAULT NULL,
  `inscomindx` int(11) DEFAULT NULL,
  `retrplnindx` int(11) DEFAULT NULL,
  `retrfndindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`retrindx`),
  KEY `IDX6ji48w3f780rlguoqltq4y1ds` (`retrindx`),
  KEY `FKobk4e1bg138ob88k9993a2sy2` (`inscomindx`),
  KEY `FK4et319isess3r5pvr1xikm544` (`retrplnindx`),
  KEY `FK32inpmj63oiwbxwl53h04xnrn` (`retrfndindx`),
  CONSTRAINT `FK32inpmj63oiwbxwl53h04xnrn` FOREIGN KEY (`retrfndindx`) REFERENCES `hr40361` (`retrfndindx`),
  CONSTRAINT `FK4et319isess3r5pvr1xikm544` FOREIGN KEY (`retrplnindx`) REFERENCES `hr40351` (`retrplnindx`),
  CONSTRAINT `FKobk4e1bg138ob88k9993a2sy2` FOREIGN KEY (`inscomindx`) REFERENCES `hr40321` (`inscomindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40341`
--

LOCK TABLES `hr40341` WRITE;
/*!40000 ALTER TABLE `hr40341` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40341` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40351`
--

DROP TABLE IF EXISTS `hr40351`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40351` (
  `retrplnindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `retrplnseq` int(11) DEFAULT NULL,
  `retrdaten` char(31) DEFAULT NULL,
  `retrindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`retrplnindx`),
  KEY `IDX2gtpf4gsepgybe7u3xbakh8j2` (`retrplnindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40351`
--

LOCK TABLES `hr40351` WRITE;
/*!40000 ALTER TABLE `hr40351` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40351` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40361`
--

DROP TABLE IF EXISTS `hr40361`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40361` (
  `retrfndindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `retrfndact` decimal(19,2) DEFAULT NULL,
  `retrfndid` char(15) DEFAULT NULL,
  `retrseqnc` int(11) DEFAULT NULL,
  `retrfnddscr` char(31) DEFAULT NULL,
  `retrindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`retrfndindx`),
  KEY `IDXqx32hroqbd6nir7uaa1fexat7` (`retrfndindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40361`
--

LOCK TABLES `hr40361` WRITE;
/*!40000 ALTER TABLE `hr40361` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40361` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40400`
--

DROP TABLE IF EXISTS `hr40400`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40400` (
  `intrvindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `intrvrng` int(11) DEFAULT NULL,
  `intrvdscr` char(31) DEFAULT NULL,
  `intrvdscra` char(61) DEFAULT NULL,
  `intrvid` char(15) DEFAULT NULL,
  PRIMARY KEY (`intrvindx`),
  KEY `IDX6me56wkb5o1rhi1ik7rx5jhoe` (`intrvindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40400`
--

LOCK TABLES `hr40400` WRITE;
/*!40000 ALTER TABLE `hr40400` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40400` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40401`
--

DROP TABLE IF EXISTS `hr40401`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40401` (
  `intrvindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `intrcatseq` int(11) DEFAULT NULL,
  `intrcactsq` int(11) DEFAULT NULL,
  `intrcactwegt` int(11) DEFAULT NULL,
  `intrcactdsc` char(150) DEFAULT NULL,
  `intrvindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`intrvindxd`),
  KEY `IDXljf8eqvqldvc0qw5ejb1ta42k` (`intrvindxd`),
  KEY `FKhax6mw4ewar5at1j80bo572bc` (`intrvindx`),
  CONSTRAINT `FKhax6mw4ewar5at1j80bo572bc` FOREIGN KEY (`intrvindx`) REFERENCES `hr40400` (`intrvindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40401`
--

LOCK TABLES `hr40401` WRITE;
/*!40000 ALTER TABLE `hr40401` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40401` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40500`
--

DROP TABLE IF EXISTS `hr40500`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40500` (
  `orntindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `orntdscra` char(61) DEFAULT NULL,
  `orntdscr` char(31) DEFAULT NULL,
  `orntid` char(15) DEFAULT NULL,
  `orntchkindx` int(11) DEFAULT NULL,
  `prchkindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`orntindx`),
  KEY `IDX7u0fida2lisnsl9ics40wt7v5` (`orntindx`),
  KEY `FKiwyqfr69af674n65gboboaeuo` (`orntchkindx`),
  KEY `FKd1jp0wh317xru7bybto0qgn0m` (`prchkindx`),
  CONSTRAINT `FKd1jp0wh317xru7bybto0qgn0m` FOREIGN KEY (`prchkindx`) REFERENCES `hr40521` (`prchkindx`),
  CONSTRAINT `FKiwyqfr69af674n65gboboaeuo` FOREIGN KEY (`orntchkindx`) REFERENCES `hr40501` (`orntchkindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40500`
--

LOCK TABLES `hr40500` WRITE;
/*!40000 ALTER TABLE `hr40500` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40500` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40501`
--

DROP TABLE IF EXISTS `hr40501`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40501` (
  `orntchkindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `orntchkdscr` char(150) DEFAULT NULL,
  `orntchkdscra` char(150) DEFAULT NULL,
  `prchkindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`orntchkindx`),
  KEY `IDXg8rkufr5f7iq1lewoul6tw13w` (`orntchkindx`),
  KEY `FKoag4al9fwv69vike9icbdcbha` (`prchkindx`),
  CONSTRAINT `FKoag4al9fwv69vike9icbdcbha` FOREIGN KEY (`prchkindx`) REFERENCES `hr40521` (`prchkindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40501`
--

LOCK TABLES `hr40501` WRITE;
/*!40000 ALTER TABLE `hr40501` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40501` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40521`
--

DROP TABLE IF EXISTS `hr40521`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40521` (
  `prchkindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `prchkdscra` char(141) DEFAULT NULL,
  `prchkdscr` char(81) DEFAULT NULL,
  `prchktyp` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`prchkindx`),
  KEY `IDXp02247wg3uwvv085ish0hhef7` (`prchkindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40521`
--

LOCK TABLES `hr40521` WRITE;
/*!40000 ALTER TABLE `hr40521` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40521` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40531`
--

DROP TABLE IF EXISTS `hr40531`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40531` (
  `orntindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `orntchkdscra` char(150) DEFAULT NULL,
  `orntchkdscr` char(150) DEFAULT NULL,
  `orntchkendt` date DEFAULT NULL,
  `orntchkenmt` date DEFAULT NULL,
  `orntchkresp` char(15) DEFAULT NULL,
  `orntchkseqn` int(11) DEFAULT NULL,
  `orntchkstdt` date DEFAULT NULL,
  `orntchkstmt` date DEFAULT NULL,
  `orntindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`orntindxd`),
  KEY `IDX2wpt7mapy8ok48ska239otga6` (`orntindxd`),
  KEY `FKhc3sqr7wxcc4wqm67uyq2k394` (`orntindx`),
  CONSTRAINT `FKhc3sqr7wxcc4wqm67uyq2k394` FOREIGN KEY (`orntindx`) REFERENCES `hr40500` (`orntindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40531`
--

LOCK TABLES `hr40531` WRITE;
/*!40000 ALTER TABLE `hr40531` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40531` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40600`
--

DROP TABLE IF EXISTS `hr40600`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40600` (
  `sklindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `skldscra` char(61) DEFAULT NULL,
  `sklcomamt` decimal(13,3) DEFAULT NULL,
  `sklcomper` smallint(6) DEFAULT NULL,
  `skldscr` char(31) DEFAULT NULL,
  `sklid` char(15) DEFAULT NULL,
  PRIMARY KEY (`sklindx`),
  KEY `IDXhs83jxnrl8ntmui7r2sx400rh` (`sklindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40600`
--

LOCK TABLES `hr40600` WRITE;
/*!40000 ALTER TABLE `hr40600` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40600` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40601`
--

DROP TABLE IF EXISTS `hr40601`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40601` (
  `sklstindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `sklstdscra` char(61) DEFAULT NULL,
  `sklstdscr` char(31) DEFAULT NULL,
  `sklstid` char(15) DEFAULT NULL,
  PRIMARY KEY (`sklstindx`),
  KEY `IDX3eqpn6uec45205hr4fscrrru2` (`sklstindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40601`
--

LOCK TABLES `hr40601` WRITE;
/*!40000 ALTER TABLE `hr40601` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40601` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40611`
--

DROP TABLE IF EXISTS `hr40611`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40611` (
  `sklstindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `sklstcomm` varchar(255) DEFAULT NULL,
  `sklstreq` bit(1) DEFAULT NULL,
  `sklstseqn` int(11) DEFAULT NULL,
  `sklstindx` int(11) DEFAULT NULL,
  `sklindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`sklstindxd`),
  KEY `IDXr7gpnmgjh9ojn16mxkjk7wrwf` (`sklstindxd`),
  KEY `FKqoj9y2ig8rjxuf9q9tqmao04v` (`sklstindx`),
  KEY `FK8dflv1l2rxxqh0qxdaeym5pou` (`sklindx`),
  CONSTRAINT `FK8dflv1l2rxxqh0qxdaeym5pou` FOREIGN KEY (`sklindx`) REFERENCES `hr40600` (`sklindx`),
  CONSTRAINT `FKqoj9y2ig8rjxuf9q9tqmao04v` FOREIGN KEY (`sklstindx`) REFERENCES `hr40601` (`sklstindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40611`
--

LOCK TABLES `hr40611` WRITE;
/*!40000 ALTER TABLE `hr40611` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40611` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40700`
--

DROP TABLE IF EXISTS `hr40700`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40700` (
  `termindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `predefinecheck_listtype_id` int(11) DEFAULT NULL,
  `termdscra` char(61) DEFAULT NULL,
  `termdscr` char(31) DEFAULT NULL,
  `termtid` char(15) DEFAULT NULL,
  `prchkindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`termindx`),
  KEY `IDX11vjrmevgacofpw2mpbaptuu4` (`termindx`),
  KEY `FK5yh4dp5t9rgjeu57paq0tfry3` (`prchkindx`),
  CONSTRAINT `FK5yh4dp5t9rgjeu57paq0tfry3` FOREIGN KEY (`prchkindx`) REFERENCES `hr40521` (`prchkindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40700`
--

LOCK TABLES `hr40700` WRITE;
/*!40000 ALTER TABLE `hr40700` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40700` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40721`
--

DROP TABLE IF EXISTS `hr40721`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40721` (
  `termtindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `termchkdscra` char(150) DEFAULT NULL,
  `termchkcomdt` date DEFAULT NULL,
  `termchkdscr` char(150) DEFAULT NULL,
  `termchkresp` char(150) DEFAULT NULL,
  `termchkseqn` int(11) DEFAULT NULL,
  `termindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`termtindxd`),
  KEY `IDX5k1vmsx3q9sl9abvut7rfxcct` (`termtindxd`),
  KEY `FKj7dfjpghsbo34qq6c2w8irevb` (`termindx`),
  CONSTRAINT `FKj7dfjpghsbo34qq6c2w8irevb` FOREIGN KEY (`termindx`) REFERENCES `hr40700` (`termindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40721`
--

LOCK TABLES `hr40721` WRITE;
/*!40000 ALTER TABLE `hr40721` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40721` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40800`
--

DROP TABLE IF EXISTS `hr40800`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40800` (
  `trncindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `trncnmea` varchar(255) DEFAULT NULL,
  `trncinco` bit(1) DEFAULT NULL,
  `trncnme` varchar(255) DEFAULT NULL,
  `trncempamt` decimal(10,3) DEFAULT NULL,
  `trncemamt` decimal(10,3) DEFAULT NULL,
  `trncinsramt` decimal(10,3) DEFAULT NULL,
  `trnclocn` varchar(255) DEFAULT NULL,
  `trncpreq` varchar(255) DEFAULT NULL,
  `trncsuplamt` decimal(10,3) DEFAULT NULL,
  `trncid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`trncindx`),
  KEY `IDXbv99y8d5ue315wg8fpbwgj9dv` (`trncindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40800`
--

LOCK TABLES `hr40800` WRITE;
/*!40000 ALTER TABLE `hr40800` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40800` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40801`
--

DROP TABLE IF EXISTS `hr40801`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40801` (
  `trncindxc` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `trncclsid` char(15) DEFAULT NULL,
  `trncclsloc` char(60) DEFAULT NULL,
  `trncclsnme` char(30) DEFAULT NULL,
  `trncclsedt` datetime DEFAULT NULL,
  `trncclsetm` char(30) DEFAULT NULL,
  `trncclsenrl` int(11) DEFAULT NULL,
  `trncclsintnme` char(90) DEFAULT NULL,
  `trncclsenmx` int(11) DEFAULT NULL,
  `trncclssdt` datetime DEFAULT NULL,
  `trncclsstm` char(30) DEFAULT NULL,
  `trncindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`trncindxc`),
  KEY `IDX7ogp1h480mpils82uiftyfmar` (`trncindxc`),
  KEY `FKs29f025mp6tc7570op6am3p57` (`trncindx`),
  CONSTRAINT `FKs29f025mp6tc7570op6am3p57` FOREIGN KEY (`trncindx`) REFERENCES `hr40800` (`trncindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40801`
--

LOCK TABLES `hr40801` WRITE;
/*!40000 ALTER TABLE `hr40801` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40801` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40811`
--

DROP TABLE IF EXISTS `hr40811`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40811` (
  `trncclsrwid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `trncclscmdt` datetime DEFAULT NULL,
  `trncclscomt` char(61) DEFAULT NULL,
  `emplfunme` char(150) DEFAULT NULL,
  `trncclsstst` smallint(6) DEFAULT NULL,
  `trncindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`trncclsrwid`),
  KEY `IDXscnsmdhlvf9dq9kgpr1922iyk` (`trncclsrwid`),
  KEY `FKg7svioru149y939llh0g6uotv` (`trncindx`),
  CONSTRAINT `FKg7svioru149y939llh0g6uotv` FOREIGN KEY (`trncindx`) REFERENCES `hr40800` (`trncindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40811`
--

LOCK TABLES `hr40811` WRITE;
/*!40000 ALTER TABLE `hr40811` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40811` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40811_employee_master`
--

DROP TABLE IF EXISTS `hr40811_employee_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40811_employee_master` (
  `training_batch_signup_trncclsrwid` int(11) NOT NULL,
  `employee_master_employindx` int(11) NOT NULL,
  KEY `FKdggebsy8cas71fvrqgsavgxob` (`employee_master_employindx`),
  KEY `FKj8ueuv3en8ejo8c624t6vn25g` (`training_batch_signup_trncclsrwid`),
  CONSTRAINT `FKdggebsy8cas71fvrqgsavgxob` FOREIGN KEY (`employee_master_employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FKj8ueuv3en8ejo8c624t6vn25g` FOREIGN KEY (`training_batch_signup_trncclsrwid`) REFERENCES `hr40811` (`trncclsrwid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40811_employee_master`
--

LOCK TABLES `hr40811_employee_master` WRITE;
/*!40000 ALTER TABLE `hr40811_employee_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40811_employee_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40811_hr40801`
--

DROP TABLE IF EXISTS `hr40811_hr40801`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40811_hr40801` (
  `training_batch_signup_id` int(11) NOT NULL,
  `training_course_detail_id` int(11) NOT NULL,
  KEY `FK5p2lt2gxc9n1rqfaulj7ou1o` (`training_course_detail_id`),
  KEY `FKpnb6wm5ve7memleskl5soad66` (`training_batch_signup_id`),
  CONSTRAINT `FK5p2lt2gxc9n1rqfaulj7ou1o` FOREIGN KEY (`training_course_detail_id`) REFERENCES `hr40801` (`trncindxc`),
  CONSTRAINT `FKpnb6wm5ve7memleskl5soad66` FOREIGN KEY (`training_batch_signup_id`) REFERENCES `hr40811` (`trncclsrwid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40811_hr40801`
--

LOCK TABLES `hr40811_hr40801` WRITE;
/*!40000 ALTER TABLE `hr40811_hr40801` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40811_hr40801` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40821`
--

DROP TABLE IF EXISTS `hr40821`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40821` (
  `trnskindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `skldscra` varchar(255) DEFAULT NULL,
  `skldscr` varchar(255) DEFAULT NULL,
  `sklseqnc` int(11) DEFAULT NULL,
  `sklstindx` int(11) DEFAULT NULL,
  `trncclsindx` int(11) DEFAULT NULL,
  `trncindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`trnskindx`),
  KEY `IDX82q9c8iylqo1t2smcx1ws55be` (`trnskindx`),
  KEY `FK9vh7onqb65vde0wsgi6s6q7as` (`sklstindx`),
  KEY `FKowrr96q70iwjna6f94jxnhdlr` (`trncclsindx`),
  KEY `FK4niu2g2h4qfy03cswb27baqdt` (`trncindx`),
  CONSTRAINT `FK4niu2g2h4qfy03cswb27baqdt` FOREIGN KEY (`trncindx`) REFERENCES `hr40800` (`trncindx`),
  CONSTRAINT `FK9vh7onqb65vde0wsgi6s6q7as` FOREIGN KEY (`sklstindx`) REFERENCES `hr40601` (`sklstindx`),
  CONSTRAINT `FKowrr96q70iwjna6f94jxnhdlr` FOREIGN KEY (`trncclsindx`) REFERENCES `hr40801` (`trncindxc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40821`
--

LOCK TABLES `hr40821` WRITE;
/*!40000 ALTER TABLE `hr40821` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40821` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40821_skills_setup`
--

DROP TABLE IF EXISTS `hr40821_skills_setup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40821_skills_setup` (
  `traning_course_class_skill_trnskindx` int(11) NOT NULL,
  `skills_setup_sklindx` int(11) NOT NULL,
  KEY `FKconrkijisocrvgyn1vebp7tdy` (`skills_setup_sklindx`),
  KEY `FKd0p5674g8o2ie5rhopj6b6542` (`traning_course_class_skill_trnskindx`),
  CONSTRAINT `FKconrkijisocrvgyn1vebp7tdy` FOREIGN KEY (`skills_setup_sklindx`) REFERENCES `hr40600` (`sklindx`),
  CONSTRAINT `FKd0p5674g8o2ie5rhopj6b6542` FOREIGN KEY (`traning_course_class_skill_trnskindx`) REFERENCES `hr40821` (`trnskindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40821_skills_setup`
--

LOCK TABLES `hr40821_skills_setup` WRITE;
/*!40000 ALTER TABLE `hr40821_skills_setup` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40821_skills_setup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40900`
--

DROP TABLE IF EXISTS `hr40900`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40900` (
  `pycdindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pycddscra` char(31) DEFAULT NULL,
  `pycdbascd` char(31) DEFAULT NULL,
  `pycdbascdval` decimal(10,3) DEFAULT NULL,
  `baseonpaycodeid` int(11) DEFAULT NULL,
  `pycddscr` char(31) DEFAULT NULL,
  `pycdinctv` bit(1) DEFAULT NULL,
  `pycdid` char(15) DEFAULT NULL,
  `pycdfactr` decimal(10,3) DEFAULT NULL,
  `pycdrate` decimal(10,3) DEFAULT NULL,
  `pycdperd` smallint(6) DEFAULT NULL,
  `pycduntpy` smallint(6) DEFAULT NULL,
  `pycdtypindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`pycdindx`),
  KEY `IDXmd2aerkluysf63pa1pw751jyh` (`pycdindx`),
  KEY `FKfyc04b3qkiysajxios03vs4pq` (`pycdtypindx`),
  CONSTRAINT `FKfyc04b3qkiysajxios03vs4pq` FOREIGN KEY (`pycdtypindx`) REFERENCES `hr40901` (`pycdtypindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40900`
--

LOCK TABLES `hr40900` WRITE;
/*!40000 ALTER TABLE `hr40900` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40900` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40901`
--

DROP TABLE IF EXISTS `hr40901`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40901` (
  `pycdtypindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pycdtypdscra` varchar(255) DEFAULT NULL,
  `pycdtypdscr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pycdtypindx`),
  KEY `IDXbwtbyxuyxgjwoh340pdabqh2j` (`pycdtypindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40901`
--

LOCK TABLES `hr40901` WRITE;
/*!40000 ALTER TABLE `hr40901` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40901` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40902`
--

DROP TABLE IF EXISTS `hr40902`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40902` (
  `dedcindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `allpaycode` bit(1) DEFAULT NULL,
  `dedcamt` decimal(13,3) DEFAULT NULL,
  `dedcdscra` varchar(255) DEFAULT NULL,
  `dedcid` varchar(255) DEFAULT NULL,
  `dedcdscr` varchar(255) DEFAULT NULL,
  `dedceddt` date DEFAULT NULL,
  `dedcenddays` int(11) DEFAULT NULL,
  `dedcfreqn` smallint(6) DEFAULT NULL,
  `dedcinctv` bit(1) DEFAULT NULL,
  `dedccustdate` bit(1) DEFAULT NULL,
  `dedcmxlife` decimal(13,3) DEFAULT NULL,
  `dedcmethd` smallint(6) DEFAULT NULL,
  `dedcnoofdays` int(11) DEFAULT NULL,
  `pycdfactr` decimal(15,6) DEFAULT NULL,
  `dedcmxperd` decimal(13,3) DEFAULT NULL,
  `dedcmxyerl` decimal(13,3) DEFAULT NULL,
  `dedcpert` decimal(13,3) DEFAULT NULL,
  `dedcstdt` date DEFAULT NULL,
  `dedctrxrq` bit(1) DEFAULT NULL,
  PRIMARY KEY (`dedcindx`),
  KEY `IDXhy962p0xslihuu833yywedpwo` (`dedcindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40902`
--

LOCK TABLES `hr40902` WRITE;
/*!40000 ALTER TABLE `hr40902` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40902` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40903`
--

DROP TABLE IF EXISTS `hr40903`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40903` (
  `bencindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `allpaycode` bit(1) DEFAULT NULL,
  `bencamt` decimal(13,3) DEFAULT NULL,
  `bencdscra` char(61) DEFAULT NULL,
  `bencid` char(15) DEFAULT NULL,
  `bencdscr` char(31) DEFAULT NULL,
  `benceddt` date DEFAULT NULL,
  `bencenddays` int(11) DEFAULT NULL,
  `bencfreqn` smallint(6) DEFAULT NULL,
  `bencinctv` bit(1) DEFAULT NULL,
  `dedccustdate` bit(1) DEFAULT NULL,
  `bencmxlife` decimal(13,3) DEFAULT NULL,
  `bencmethd` smallint(6) DEFAULT NULL,
  `bencnoofdays` int(11) DEFAULT NULL,
  `pycdfactr` decimal(15,6) DEFAULT NULL,
  `bencmxperd` decimal(13,3) DEFAULT NULL,
  `bencmxyerl` decimal(13,3) DEFAULT NULL,
  `bencpert` decimal(13,3) DEFAULT NULL,
  `bencstdt` date DEFAULT NULL,
  `benctrxrq` bit(1) DEFAULT NULL,
  PRIMARY KEY (`bencindx`),
  KEY `IDX7i1ns9yyrq0rd5fpfrl2nxttp` (`bencindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40903`
--

LOCK TABLES `hr40903` WRITE;
/*!40000 ALTER TABLE `hr40903` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40903` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40904`
--

DROP TABLE IF EXISTS `hr40904`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40904` (
  `smtxindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `smtxdscra` char(61) DEFAULT NULL,
  `smtxdscr` char(31) DEFAULT NULL,
  `smtxpyunt` smallint(6) DEFAULT NULL,
  `smtxid` char(15) DEFAULT NULL,
  `smtxcol` int(11) DEFAULT NULL,
  `smtxrow` int(11) DEFAULT NULL,
  `smtxtrow` int(11) DEFAULT NULL,
  PRIMARY KEY (`smtxindx`),
  KEY `IDXshp1iboob5b2jhlbty4kv23k9` (`smtxindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40904`
--

LOCK TABLES `hr40904` WRITE;
/*!40000 ALTER TABLE `hr40904` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40904` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40905`
--

DROP TABLE IF EXISTS `hr40905`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40905` (
  `pyschdindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pyschdbgdt` datetime DEFAULT NULL,
  `pyschddscr` char(31) DEFAULT NULL,
  `pyschddscra` char(61) DEFAULT NULL,
  `pyschdid` char(15) DEFAULT NULL,
  `pyschdpyper` smallint(6) DEFAULT NULL,
  `pyschdyr` int(11) DEFAULT NULL,
  PRIMARY KEY (`pyschdindx`),
  KEY `IDX3fba4k2gb65iggk3d29c5l0xm` (`pyschdindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40905`
--

LOCK TABLES `hr40905` WRITE;
/*!40000 ALTER TABLE `hr40905` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40905` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40914`
--

DROP TABLE IF EXISTS `hr40914`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40914` (
  `smtxrindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `smtxrdscra` varchar(255) DEFAULT NULL,
  `smtxrdscr` varchar(255) DEFAULT NULL,
  `smtxindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`smtxrindx`),
  KEY `IDXmo9wbj1gfp0lm30aba3m9aduk` (`smtxrindx`),
  KEY `FKr48otdc2qh8qx9hg01d00k0h8` (`smtxindx`),
  CONSTRAINT `FKr48otdc2qh8qx9hg01d00k0h8` FOREIGN KEY (`smtxindx`) REFERENCES `hr40904` (`smtxindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40914`
--

LOCK TABLES `hr40914` WRITE;
/*!40000 ALTER TABLE `hr40914` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40914` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40915`
--

DROP TABLE IF EXISTS `hr40915`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40915` (
  `pyschdindxl` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `locdnma` char(31) DEFAULT NULL,
  `pyschdact` bit(1) DEFAULT NULL,
  `locindx` int(11) DEFAULT NULL,
  `pyschdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`pyschdindxl`),
  KEY `IDXmxyxr9be0jrenpk3d2jjnvfwf` (`pyschdindxl`),
  KEY `FK5c7xrgru1l1iq2v262ruc7l32` (`locindx`),
  KEY `FK24mdjn6fpl6bjih2y68d9mwle` (`pyschdindx`),
  CONSTRAINT `FK24mdjn6fpl6bjih2y68d9mwle` FOREIGN KEY (`pyschdindx`) REFERENCES `hr40905` (`pyschdindx`),
  CONSTRAINT `FK5c7xrgru1l1iq2v262ruc7l32` FOREIGN KEY (`locindx`) REFERENCES `hr40104` (`locindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40915`
--

LOCK TABLES `hr40915` WRITE;
/*!40000 ALTER TABLE `hr40915` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40915` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40924`
--

DROP TABLE IF EXISTS `hr40924`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40924` (
  `smtxrindxr` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `smtxramt` decimal(19,2) DEFAULT NULL,
  `smtrrdscr` char(31) DEFAULT NULL,
  `smtxrseqnr` int(11) DEFAULT NULL,
  `smtxrindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`smtxrindxr`),
  KEY `IDXqsaot6lh0j1hxylgtnis173kq` (`smtxrindxr`),
  KEY `FK63m35g9a87ie29agw9e0vj8il` (`smtxrindx`),
  CONSTRAINT `FK63m35g9a87ie29agw9e0vj8il` FOREIGN KEY (`smtxrindx`) REFERENCES `hr40914` (`smtxrindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40924`
--

LOCK TABLES `hr40924` WRITE;
/*!40000 ALTER TABLE `hr40924` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40924` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40925`
--

DROP TABLE IF EXISTS `hr40925`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40925` (
  `pyschdindxp` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pyschdact` bit(1) DEFAULT NULL,
  `potdscr` char(31) DEFAULT NULL,
  `potdscra` char(61) DEFAULT NULL,
  `pyschdindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`pyschdindxp`),
  KEY `IDXtdyx94rr4txwdhs9vaotrq2vr` (`pyschdindxp`),
  KEY `FKccnjncv3kww82lh6ym6hnkqc3` (`pyschdindx`),
  KEY `FKamhq9wt2di9e9cp00tds182td` (`potindx`),
  CONSTRAINT `FKamhq9wt2di9e9cp00tds182td` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FKccnjncv3kww82lh6ym6hnkqc3` FOREIGN KEY (`pyschdindx`) REFERENCES `hr40905` (`pyschdindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40925`
--

LOCK TABLES `hr40925` WRITE;
/*!40000 ALTER TABLE `hr40925` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40925` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40934`
--

DROP TABLE IF EXISTS `hr40934`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40934` (
  `smtxcindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `smtxcdscra` char(61) DEFAULT NULL,
  `smtxcdscr` char(15) DEFAULT NULL,
  `smtxindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`smtxcindx`),
  KEY `IDXb8dccdhb97qgnmrnel7v5ff5r` (`smtxcindx`),
  KEY `FKl5keljn3b0ortkffxvxe955c0` (`smtxindx`),
  CONSTRAINT `FKl5keljn3b0ortkffxvxe955c0` FOREIGN KEY (`smtxindx`) REFERENCES `hr40904` (`smtxindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40934`
--

LOCK TABLES `hr40934` WRITE;
/*!40000 ALTER TABLE `hr40934` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40934` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40935`
--

DROP TABLE IF EXISTS `hr40935`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40935` (
  `pyschdindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `depdscra` char(61) DEFAULT NULL,
  `depdscr` char(31) DEFAULT NULL,
  `pyschdact` bit(1) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `pyschdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`pyschdindxd`),
  KEY `IDX64q09a2853v98wdrs11a16i8w` (`pyschdindxd`),
  KEY `FKd77jyosdmtcauunfhslxe62ag` (`depindx`),
  KEY `FK8xdre1vej6r2bqynn5s4roy3w` (`pyschdindx`),
  CONSTRAINT `FK8xdre1vej6r2bqynn5s4roy3w` FOREIGN KEY (`pyschdindx`) REFERENCES `hr40905` (`pyschdindx`),
  CONSTRAINT `FKd77jyosdmtcauunfhslxe62ag` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40935`
--

LOCK TABLES `hr40935` WRITE;
/*!40000 ALTER TABLE `hr40935` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40935` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40945`
--

DROP TABLE IF EXISTS `hr40945`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40945` (
  `pyschdindxe` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `emplynmn` char(150) DEFAULT NULL,
  `pyschdact` bit(1) DEFAULT NULL,
  `emplyindx` int(11) DEFAULT NULL,
  `pyschdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`pyschdindxe`),
  KEY `IDXmc8fm99sup2kg926mq2f7bgvt` (`pyschdindxe`),
  KEY `FK9rgq5djx8uj8tvulad3mcxgp7` (`emplyindx`),
  KEY `FK1gcyqpa60j8ivya8elhw0e78i` (`pyschdindx`),
  CONSTRAINT `FK1gcyqpa60j8ivya8elhw0e78i` FOREIGN KEY (`pyschdindx`) REFERENCES `hr40905` (`pyschdindx`),
  CONSTRAINT `FK9rgq5djx8uj8tvulad3mcxgp7` FOREIGN KEY (`emplyindx`) REFERENCES `hr00101` (`employindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40945`
--

LOCK TABLES `hr40945` WRITE;
/*!40000 ALTER TABLE `hr40945` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40945` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr40955`
--

DROP TABLE IF EXISTS `hr40955`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr40955` (
  `pyschdindxs` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `perdtdt` datetime DEFAULT NULL,
  `perdid` int(11) DEFAULT NULL,
  `perdnmn` char(21) DEFAULT NULL,
  `perdfdt` datetime DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  `pyschdindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`pyschdindxs`),
  KEY `IDXm3aymoo5doeut92dovoy3jr14` (`pyschdindxs`),
  KEY `FKd33n7yesaatjm0nvqsdbh3jeg` (`pyschdindx`),
  CONSTRAINT `FKd33n7yesaatjm0nvqsdbh3jeg` FOREIGN KEY (`pyschdindx`) REFERENCES `hr40905` (`pyschdindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr40955`
--

LOCK TABLES `hr40955` WRITE;
/*!40000 ALTER TABLE `hr40955` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr40955` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr41101`
--

DROP TABLE IF EXISTS `hr41101`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr41101` (
  `requiindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `adversf1` char(90) DEFAULT NULL,
  `adversf2` char(90) DEFAULT NULL,
  `adversf3` char(90) DEFAULT NULL,
  `adversf4` char(90) DEFAULT NULL,
  `applaply` int(11) DEFAULT NULL,
  `applinterv` int(11) DEFAULT NULL,
  `costadvirs` decimal(13,3) DEFAULT NULL,
  `costrecri` decimal(13,3) DEFAULT NULL,
  `intrclsdt` datetime DEFAULT NULL,
  `intrpostdt` datetime DEFAULT NULL,
  `jobpost` smallint(6) DEFAULT NULL,
  `mangrnam` char(150) DEFAULT NULL,
  `opndt` datetime DEFAULT NULL,
  `postavavle` int(11) DEFAULT NULL,
  `postfill` int(11) DEFAULT NULL,
  `recrnma` varchar(255) DEFAULT NULL,
  `stats` smallint(6) DEFAULT NULL,
  `comid` int(11) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `divindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  `suprindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`requiindx`),
  KEY `IDXqbdfbnud04p5c9gr6nt1hsldm` (`requiindx`),
  KEY `FKln7tsa1rekm82p10v6020ubi9` (`comid`),
  KEY `FKkdfjyfttmtgnx4q9dpkgg0o27` (`depindx`),
  KEY `FKvguf7eeqnse25w1lbas7movj` (`divindx`),
  KEY `FK15tpnaajnr9uteppu0xv3k474` (`potindx`),
  KEY `FKd2ycy3ldglrodnlkyxlxsugfn` (`suprindx`),
  CONSTRAINT `FK15tpnaajnr9uteppu0xv3k474` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FKd2ycy3ldglrodnlkyxlxsugfn` FOREIGN KEY (`suprindx`) REFERENCES `hr40105` (`suprindx`),
  CONSTRAINT `FKkdfjyfttmtgnx4q9dpkgg0o27` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKln7tsa1rekm82p10v6020ubi9` FOREIGN KEY (`comid`) REFERENCES `company` (`company_id`),
  CONSTRAINT `FKvguf7eeqnse25w1lbas7movj` FOREIGN KEY (`divindx`) REFERENCES `hr40101` (`divindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr41101`
--

LOCK TABLES `hr41101` WRITE;
/*!40000 ALTER TABLE `hr41101` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr41101` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr41102`
--

DROP TABLE IF EXISTS `hr41102`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr41102` (
  `extitindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `extitdscra` char(100) DEFAULT NULL,
  `extitdscr` char(100) DEFAULT NULL,
  `extitid` char(7) DEFAULT NULL,
  `extitinct` bit(1) DEFAULT NULL,
  `extitseqn` int(11) DEFAULT NULL,
  PRIMARY KEY (`extitindx`),
  KEY `IDXs150gy8v7ompe7wlxvyg26cx4` (`extitindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr41102`
--

LOCK TABLES `hr41102` WRITE;
/*!40000 ALTER TABLE `hr41102` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr41102` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr41200`
--

DROP TABLE IF EXISTS `hr41200`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr41200` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `pstelgd` int(11) DEFAULT NULL,
  `pstpag` int(11) DEFAULT NULL,
  `pstestm` tinyint(4) DEFAULT NULL,
  `pstpaytd` tinyint(4) DEFAULT NULL,
  `nmrhrdy` int(11) DEFAULT NULL,
  `nmrhrsdy` int(11) DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  KEY `IDXkq9cbwxwuttl175y29j7n42u` (`rowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr41200`
--

LOCK TABLES `hr41200` WRITE;
/*!40000 ALTER TABLE `hr41200` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr41200` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr41201`
--

DROP TABLE IF EXISTS `hr41201`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr41201` (
  `rowid` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `isperdi` smallint(6) DEFAULT NULL,
  `sectmedht` smallint(6) DEFAULT NULL,
  `wnepestr` datetime DEFAULT NULL,
  PRIMARY KEY (`rowid`),
  KEY `IDXesumb997wq4fx44680xi0sjx7` (`rowid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr41201`
--

LOCK TABLES `hr41201` WRITE;
/*!40000 ALTER TABLE `hr41201` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr41201` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr49999`
--

DROP TABLE IF EXISTS `hr49999`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr49999` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `prlacttpy` smallint(6) DEFAULT NULL,
  `pyrolindx` int(11) DEFAULT NULL,
  `actrowid` int(11) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `diminxd` int(11) DEFAULT NULL,
  `locindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  `financial_dimensions` tinyblob,
  PRIMARY KEY (`rwprlindx`),
  KEY `IDXsm2ed0bwfbo6upn31mt76m33j` (`rwprlindx`),
  KEY `FKc5xqlp5ml2fsleg3jugekulvl` (`actrowid`),
  KEY `FKbuwsuhlngf2pu2yqawktyi3il` (`depindx`),
  KEY `FKhodd7ohn15j4567bprmtgcx87` (`diminxd`),
  KEY `FK1cm399qwhnqbr5ctbef3tllny` (`locindx`),
  KEY `FKin8g4loyd81dg0t8vapdlo71k` (`potindx`),
  CONSTRAINT `FK1cm399qwhnqbr5ctbef3tllny` FOREIGN KEY (`locindx`) REFERENCES `hr40104` (`locindx`),
  CONSTRAINT `FKbuwsuhlngf2pu2yqawktyi3il` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKc5xqlp5ml2fsleg3jugekulvl` FOREIGN KEY (`actrowid`) REFERENCES `gl49999_h` (`actrowid`),
  CONSTRAINT `FKhodd7ohn15j4567bprmtgcx87` FOREIGN KEY (`diminxd`) REFERENCES `gl00102_h` (`diminxd`),
  CONSTRAINT `FKin8g4loyd81dg0t8vapdlo71k` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr49999`
--

LOCK TABLES `hr49999` WRITE;
/*!40000 ALTER TABLE `hr49999` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr49999` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr80000`
--

DROP TABLE IF EXISTS `hr80000`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr80000` (
  `hcmbatindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `apprval` bit(1) DEFAULT NULL,
  `apprddt` datetime DEFAULT NULL,
  `badscra` varchar(255) DEFAULT NULL,
  `batchid` varchar(255) DEFAULT NULL,
  `badscr` varchar(255) DEFAULT NULL,
  `batchfrombuild` bit(1) DEFAULT NULL,
  `batchfortranx` bit(1) DEFAULT NULL,
  `postingdate` datetime DEFAULT NULL,
  `tottrxno` int(11) DEFAULT NULL,
  `bachstas` smallint(6) DEFAULT NULL,
  `tottrxamt` double DEFAULT NULL,
  `bachtype` smallint(6) DEFAULT NULL,
  `apprusr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`hcmbatindx`),
  KEY `IDXhd19d6qukuitnqpln5pn91pv7` (`hcmbatindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr80000`
--

LOCK TABLES `hr80000` WRITE;
/*!40000 ALTER TABLE `hr80000` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr80000` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90100`
--

DROP TABLE IF EXISTS `hr90100`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90100` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmcdindx` int(11) DEFAULT NULL,
  `hcmcdtyp` smallint(6) DEFAULT NULL,
  `hcmprd1` decimal(10,3) DEFAULT NULL,
  `hcmprd10` decimal(10,3) DEFAULT NULL,
  `hcmprd11` decimal(10,3) DEFAULT NULL,
  `hcmprd12` decimal(10,3) DEFAULT NULL,
  `hcmprd2` decimal(10,3) DEFAULT NULL,
  `hcmprd3` decimal(10,3) DEFAULT NULL,
  `hcmprd4` decimal(10,3) DEFAULT NULL,
  `hcmprd5` decimal(10,3) DEFAULT NULL,
  `hcmprd6` decimal(10,3) DEFAULT NULL,
  `hcmprd7` decimal(10,3) DEFAULT NULL,
  `hcmprd8` decimal(10,3) DEFAULT NULL,
  `hcmprd9` decimal(10,3) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90100`
--

LOCK TABLES `hr90100` WRITE;
/*!40000 ALTER TABLE `hr90100` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90100` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90101`
--

DROP TABLE IF EXISTS `hr90101`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90101` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmcdindx` int(11) DEFAULT NULL,
  `hcmcdtyp` smallint(6) DEFAULT NULL,
  `hcmprd1` decimal(10,3) DEFAULT NULL,
  `hcmprd10` decimal(10,3) DEFAULT NULL,
  `hcmprd11` decimal(10,3) DEFAULT NULL,
  `hcmprd12` decimal(10,3) DEFAULT NULL,
  `hcmprd2` decimal(10,3) DEFAULT NULL,
  `hcmprd3` decimal(10,3) DEFAULT NULL,
  `hcmprd4` decimal(10,3) DEFAULT NULL,
  `hcmprd5` decimal(10,3) DEFAULT NULL,
  `hcmprd6` decimal(10,3) DEFAULT NULL,
  `hcmprd7` decimal(10,3) DEFAULT NULL,
  `hcmprd8` decimal(10,3) DEFAULT NULL,
  `hcmprd9` decimal(10,3) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90101`
--

LOCK TABLES `hr90101` WRITE;
/*!40000 ALTER TABLE `hr90101` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90101` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90102`
--

DROP TABLE IF EXISTS `hr90102`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90102` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmcdindx` int(11) DEFAULT NULL,
  `hcmcdtyp` smallint(6) DEFAULT NULL,
  `hcmprd1` decimal(10,3) DEFAULT NULL,
  `hcmprd10` decimal(10,3) DEFAULT NULL,
  `hcmprd11` decimal(10,3) DEFAULT NULL,
  `hcmprd12` decimal(10,3) DEFAULT NULL,
  `hcmprd2` decimal(10,3) DEFAULT NULL,
  `hcmprd3` decimal(10,3) DEFAULT NULL,
  `hcmprd4` decimal(10,3) DEFAULT NULL,
  `hcmprd5` decimal(10,3) DEFAULT NULL,
  `hcmprd6` decimal(10,3) DEFAULT NULL,
  `hcmprd7` decimal(10,3) DEFAULT NULL,
  `hcmprd8` decimal(10,3) DEFAULT NULL,
  `hcmprd9` decimal(10,3) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90102`
--

LOCK TABLES `hr90102` WRITE;
/*!40000 ALTER TABLE `hr90102` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90102` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90200`
--

DROP TABLE IF EXISTS `hr90200`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90200` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmtrxpyrl` varchar(255) DEFAULT NULL,
  `hcmcdbsd` int(11) DEFAULT NULL,
  `hcmcdfdt` date DEFAULT NULL,
  `hcmcdindx` int(11) DEFAULT NULL,
  `hcmempseqn` int(11) DEFAULT NULL,
  `hcmcdtdt` date DEFAULT NULL,
  `hcmcdtyp` smallint(6) DEFAULT NULL,
  `prlposturs` varchar(255) DEFAULT NULL,
  `prlpostdt` date DEFAULT NULL,
  `hcmbsdrte` decimal(10,3) DEFAULT NULL,
  `hcmcdaamt` decimal(10,3) DEFAULT NULL,
  `hcmcdamt` decimal(10,3) DEFAULT NULL,
  `hcmcdhrs` int(11) DEFAULT NULL,
  `hcmcdprnt` decimal(10,3) DEFAULT NULL,
  `hcmbatindx` int(11) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `diminxvalue` int(11) DEFAULT NULL,
  `locindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`),
  KEY `FKcejormkdfhsitxonvquyfdbrm` (`hcmbatindx`),
  KEY `FK81jh21dygfm6ytfxhmttw792a` (`depindx`),
  KEY `FK1tqmbktx43jthjhaqt5lam412` (`employindx`),
  KEY `FKimybj5w6uqv13ytrn6i9vc4lw` (`diminxvalue`),
  KEY `FKe9e9388cfv6epeaww1omes9as` (`locindx`),
  KEY `FK22099c0fn7hpnnh5ic3l21556` (`potindx`),
  CONSTRAINT `FK1tqmbktx43jthjhaqt5lam412` FOREIGN KEY (`employindx`) REFERENCES `hr00101` (`employindx`),
  CONSTRAINT `FK22099c0fn7hpnnh5ic3l21556` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FK81jh21dygfm6ytfxhmttw792a` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKcejormkdfhsitxonvquyfdbrm` FOREIGN KEY (`hcmbatindx`) REFERENCES `hr80000` (`hcmbatindx`),
  CONSTRAINT `FKe9e9388cfv6epeaww1omes9as` FOREIGN KEY (`locindx`) REFERENCES `hr40104` (`locindx`),
  CONSTRAINT `FKimybj5w6uqv13ytrn6i9vc4lw` FOREIGN KEY (`diminxvalue`) REFERENCES `gl00103_h` (`diminxvalue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90200`
--

LOCK TABLES `hr90200` WRITE;
/*!40000 ALTER TABLE `hr90200` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90200` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90201`
--

DROP TABLE IF EXISTS `hr90201`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90201` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `acctseqn` int(11) DEFAULT NULL,
  `actrowid` bigint(20) DEFAULT NULL,
  `hcmtrxpyrl` varchar(255) DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `crdtamt` decimal(10,3) DEFAULT NULL,
  `debtamt` decimal(10,3) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `ditpottyp` smallint(6) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `diminxvalue` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`),
  KEY `FKrims4xa9vl0qq9hmv4dbmo5dl` (`depindx`),
  KEY `FK49bug8e1fybw4dgg1wo1k7wd7` (`diminxvalue`),
  KEY `FK2pbp7kcnnobpnfgthi4wsbvyl` (`potindx`),
  CONSTRAINT `FK2pbp7kcnnobpnfgthi4wsbvyl` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FK49bug8e1fybw4dgg1wo1k7wd7` FOREIGN KEY (`diminxvalue`) REFERENCES `gl00103_h` (`diminxvalue`),
  CONSTRAINT `FKrims4xa9vl0qq9hmv4dbmo5dl` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90201`
--

LOCK TABLES `hr90201` WRITE;
/*!40000 ALTER TABLE `hr90201` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90201` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90210`
--

DROP TABLE IF EXISTS `hr90210`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90210` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmalldep` bit(1) DEFAULT NULL,
  `hcmallem` bit(1) DEFAULT NULL,
  `hcmbulddt` datetime DEFAULT NULL,
  `hcmbulusr` varchar(255) DEFAULT NULL,
  `cheknmbr` varchar(255) DEFAULT NULL,
  `hcmfdepinx` int(11) DEFAULT NULL,
  `hcmfeminx` int(11) DEFAULT NULL,
  `hcmpyr` bit(1) DEFAULT NULL,
  `hcmpbwek` bit(1) DEFAULT NULL,
  `hcmpdms` bit(1) DEFAULT NULL,
  `hcmpmon` bit(1) DEFAULT NULL,
  `hcmpqur` bit(1) DEFAULT NULL,
  `hcmpsyr` bit(1) DEFAULT NULL,
  `hcmpsmon` bit(1) DEFAULT NULL,
  `hcmpwek` bit(1) DEFAULT NULL,
  `hcmpytdt` datetime DEFAULT NULL,
  `hcmpyfdt` datetime DEFAULT NULL,
  `hcmpstdt` datetime DEFAULT NULL,
  `hcmtdepinx` int(11) DEFAULT NULL,
  `hcmteminx` int(11) DEFAULT NULL,
  `hcmdefinx` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`),
  KEY `FKtq3ig9th0gxidp9wfabdcq11p` (`hcmdefinx`),
  CONSTRAINT `FKtq3ig9th0gxidp9wfabdcq11p` FOREIGN KEY (`hcmdefinx`) REFERENCES `hr10301` (`hcmdefinx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90210`
--

LOCK TABLES `hr90210` WRITE;
/*!40000 ALTER TABLE `hr90210` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90210` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90300`
--

DROP TABLE IF EXISTS `hr90300`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90300` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmtrxpyrl` varchar(255) DEFAULT NULL,
  `hcmcdbsd` int(11) DEFAULT NULL,
  `hcmcdfdt` datetime DEFAULT NULL,
  `hcmcdindx` int(11) DEFAULT NULL,
  `hcmcdtdt` datetime DEFAULT NULL,
  `hcmcdtyp` smallint(6) DEFAULT NULL,
  `hcmempseqn` int(11) DEFAULT NULL,
  `prlposturs` varchar(255) DEFAULT NULL,
  `prlpostdt` datetime DEFAULT NULL,
  `hcmbsdrte` decimal(10,3) DEFAULT NULL,
  `hcmcdaamt` decimal(10,3) DEFAULT NULL,
  `hcmcdamt` decimal(10,3) DEFAULT NULL,
  `hcmcdhrs` int(11) DEFAULT NULL,
  `hcmcdprnt` decimal(10,3) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90300`
--

LOCK TABLES `hr90300` WRITE;
/*!40000 ALTER TABLE `hr90300` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90300` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90301`
--

DROP TABLE IF EXISTS `hr90301`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90301` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `acctseqn` int(11) DEFAULT NULL,
  `actrowid` bigint(20) DEFAULT NULL,
  `hcmtrxpyrl` varchar(255) DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `crdtamt` decimal(10,3) DEFAULT NULL,
  `debtamt` decimal(10,3) DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `ditpottyp` smallint(6) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90301`
--

LOCK TABLES `hr90301` WRITE;
/*!40000 ALTER TABLE `hr90301` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90301` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90310`
--

DROP TABLE IF EXISTS `hr90310`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90310` (
  `rwprlindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmalldep` bit(1) DEFAULT NULL,
  `hcmallem` bit(1) DEFAULT NULL,
  `hcmbulddt` datetime DEFAULT NULL,
  `hcmbulusr` varchar(255) DEFAULT NULL,
  `hcmfdepinx` int(11) DEFAULT NULL,
  `hcmfeminx` int(11) DEFAULT NULL,
  `hcmpyr` bit(1) DEFAULT NULL,
  `hcmpbwek` bit(1) DEFAULT NULL,
  `hcmpdms` bit(1) DEFAULT NULL,
  `hcmpmon` bit(1) DEFAULT NULL,
  `hcmpqur` bit(1) DEFAULT NULL,
  `hcmpsyr` bit(1) DEFAULT NULL,
  `hcmpsmon` bit(1) DEFAULT NULL,
  `hcmpwek` bit(1) DEFAULT NULL,
  `hcmpytdt` datetime DEFAULT NULL,
  `hcmpyfdt` datetime DEFAULT NULL,
  `hcmpstdt` datetime DEFAULT NULL,
  `hcmtdepinx` int(11) DEFAULT NULL,
  `hcmteminx` int(11) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  PRIMARY KEY (`rwprlindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90310`
--

LOCK TABLES `hr90310` WRITE;
/*!40000 ALTER TABLE `hr90310` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90310` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90400`
--

DROP TABLE IF EXISTS `hr90400`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90400` (
  `hcmpymtindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `chekdta` datetime DEFAULT NULL,
  `cheknmbr` int(11) DEFAULT NULL,
  `chekpstdt` datetime DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `hcmpydscr` varchar(255) DEFAULT NULL,
  `totlamt` decimal(10,3) DEFAULT NULL,
  `totlnetamt` decimal(10,3) DEFAULT NULL,
  `hcmbatindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindx`),
  KEY `FK2n6t1dc3dag5ov7s7lf90ag0c` (`hcmbatindx`),
  CONSTRAINT `FK2n6t1dc3dag5ov7s7lf90ag0c` FOREIGN KEY (`hcmbatindx`) REFERENCES `hr80000` (`hcmbatindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90400`
--

LOCK TABLES `hr90400` WRITE;
/*!40000 ALTER TABLE `hr90400` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90400` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90401`
--

DROP TABLE IF EXISTS `hr90401`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90401` (
  `hcmpymtindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `hcmfdta` datetime DEFAULT NULL,
  `hcmpymtyp` smallint(6) DEFAULT NULL,
  `hcmpymtseq` int(11) DEFAULT NULL,
  `hcmtdta` datetime DEFAULT NULL,
  `hcmamnt` decimal(10,3) DEFAULT NULL,
  `hcmhrs` int(11) DEFAULT NULL,
  `hcmdys` int(11) DEFAULT NULL,
  `hcmpymtindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindxd`),
  KEY `FKoqri2q4if8b0u6fdril0cb49t` (`hcmpymtindx`),
  CONSTRAINT `FKoqri2q4if8b0u6fdril0cb49t` FOREIGN KEY (`hcmpymtindx`) REFERENCES `hr90400` (`hcmpymtindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90401`
--

LOCK TABLES `hr90401` WRITE;
/*!40000 ALTER TABLE `hr90401` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90401` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90402`
--

DROP TABLE IF EXISTS `hr90402`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90402` (
  `hcmpymtindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `acctseqn` int(11) DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `crdtamt` decimal(10,3) DEFAULT NULL,
  `debtamt` decimal(10,3) DEFAULT NULL,
  `ditpottyp` smallint(6) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindxd`),
  KEY `FKjytg61l6cuttdqh8vxojmsg5l` (`depindx`),
  KEY `FKr5cd7hbg0l90ctelrbyf7wmx1` (`potindx`),
  CONSTRAINT `FKjytg61l6cuttdqh8vxojmsg5l` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKr5cd7hbg0l90ctelrbyf7wmx1` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90402`
--

LOCK TABLES `hr90402` WRITE;
/*!40000 ALTER TABLE `hr90402` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90402` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90600`
--

DROP TABLE IF EXISTS `hr90600`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90600` (
  `hcmpymtindx` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `chekdta` datetime DEFAULT NULL,
  `cheknmbr` int(11) DEFAULT NULL,
  `chekpstdt` datetime DEFAULT NULL,
  `employindx` int(11) DEFAULT NULL,
  `hcmpydscr` varchar(255) DEFAULT NULL,
  `totlamt` decimal(10,3) DEFAULT NULL,
  `totlnetamt` decimal(10,3) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  `hcmbatindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindx`),
  KEY `FK8ftqjaatvnrujobswpx0lqsdd` (`hcmbatindx`),
  CONSTRAINT `FK8ftqjaatvnrujobswpx0lqsdd` FOREIGN KEY (`hcmbatindx`) REFERENCES `hr80000` (`hcmbatindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90600`
--

LOCK TABLES `hr90600` WRITE;
/*!40000 ALTER TABLE `hr90600` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90600` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90601`
--

DROP TABLE IF EXISTS `hr90601`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90601` (
  `hcmpymtindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `hcmdys` int(11) DEFAULT NULL,
  `hcmfdta` datetime DEFAULT NULL,
  `hcmpymtyp` smallint(6) DEFAULT NULL,
  `hcmpymtseq` int(11) DEFAULT NULL,
  `hcmtdta` datetime DEFAULT NULL,
  `hcmamnt` decimal(10,3) DEFAULT NULL,
  `hcmhrs` int(11) DEFAULT NULL,
  `hcmwks` int(11) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  `hcmpymtindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindxd`),
  KEY `FKutio3qem02gd7l4cxwfa076` (`hcmpymtindx`),
  CONSTRAINT `FKutio3qem02gd7l4cxwfa076` FOREIGN KEY (`hcmpymtindx`) REFERENCES `hr10400` (`hcmpymtindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90601`
--

LOCK TABLES `hr90601` WRITE;
/*!40000 ALTER TABLE `hr90601` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90601` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr90602`
--

DROP TABLE IF EXISTS `hr90602`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr90602` (
  `hcmpymtindxd` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `acctseqn` int(11) DEFAULT NULL,
  `hcmcodinx` int(11) DEFAULT NULL,
  `crdtamt` decimal(10,3) DEFAULT NULL,
  `debtamt` decimal(10,3) DEFAULT NULL,
  `ditpottyp` smallint(6) DEFAULT NULL,
  `year1` int(11) DEFAULT NULL,
  `depindx` int(11) DEFAULT NULL,
  `hcmpymtindx` int(11) DEFAULT NULL,
  `potindx` int(11) DEFAULT NULL,
  PRIMARY KEY (`hcmpymtindxd`),
  KEY `FKjws6237lumggsgngpfdxu3n8v` (`depindx`),
  KEY `FKqrrpa3darhk3w03vtvt62vesi` (`hcmpymtindx`),
  KEY `FKa9fooscmclrblcaa5bt1rm1t` (`potindx`),
  CONSTRAINT `FKa9fooscmclrblcaa5bt1rm1t` FOREIGN KEY (`potindx`) REFERENCES `hr40103` (`potindx`),
  CONSTRAINT `FKjws6237lumggsgngpfdxu3n8v` FOREIGN KEY (`depindx`) REFERENCES `hr40102` (`depindx`),
  CONSTRAINT `FKqrrpa3darhk3w03vtvt62vesi` FOREIGN KEY (`hcmpymtindx`) REFERENCES `hr90600` (`hcmpymtindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr90602`
--

LOCK TABLES `hr90602` WRITE;
/*!40000 ALTER TABLE `hr90602` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr90602` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_hijri_day`
--

DROP TABLE IF EXISTS `hr_hijri_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_hijri_day` (
  `hijridayindx` int(11) NOT NULL AUTO_INCREMENT,
  `hijriday` int(2) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`hijridayindx`),
  KEY `IDXt1tshhe1wyajoknbw1x4uwa8k` (`hijridayindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_hijri_day`
--

LOCK TABLES `hr_hijri_day` WRITE;
/*!40000 ALTER TABLE `hr_hijri_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_hijri_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_hijri_month`
--

DROP TABLE IF EXISTS `hr_hijri_month`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_hijri_month` (
  `hijrimonthindx` int(11) NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(4) DEFAULT '0',
  `hijrimonth` char(60) DEFAULT NULL,
  PRIMARY KEY (`hijrimonthindx`),
  KEY `IDXg77jyxg0eok8bd5jt1bdwalxy` (`hijrimonthindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_hijri_month`
--

LOCK TABLES `hr_hijri_month` WRITE;
/*!40000 ALTER TABLE `hr_hijri_month` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_hijri_month` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_hijri_year`
--

DROP TABLE IF EXISTS `hr_hijri_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_hijri_year` (
  `hijriyearindx` int(11) NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(4) DEFAULT '0',
  `hijriyear` int(4) DEFAULT NULL,
  PRIMARY KEY (`hijriyearindx`),
  KEY `IDXhjlyvab48hq4y2j4ss5urcobg` (`hijriyearindx`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_hijri_year`
--

LOCK TABLES `hr_hijri_year` WRITE;
/*!40000 ALTER TABLE `hr_hijri_year` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_hijri_year` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `lang_name` varchar(255) DEFAULT NULL,
  `lang_orientation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`lang_id`),
  KEY `IDX5cp427d8g5c8nljg3c1v5u69x` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill_desc_set`
--

DROP TABLE IF EXISTS `skill_desc_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skill_desc_set` (
  `desc_id` int(11) NOT NULL,
  `setup_id` int(11) NOT NULL,
  KEY `FK550anw09bd6qhoyg3wja1cnfh` (`setup_id`),
  KEY `FKngoo6xpm1kqgjp82vsecf59nn` (`desc_id`),
  CONSTRAINT `FK550anw09bd6qhoyg3wja1cnfh` FOREIGN KEY (`setup_id`) REFERENCES `hr40600` (`sklindx`),
  CONSTRAINT `FKngoo6xpm1kqgjp82vsecf59nn` FOREIGN KEY (`desc_id`) REFERENCES `hr40611` (`sklstindxd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill_desc_set`
--

LOCK TABLES `skill_desc_set` WRITE;
/*!40000 ALTER TABLE `skill_desc_set` DISABLE KEYS */;
/*!40000 ALTER TABLE `skill_desc_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state_master`
--

DROP TABLE IF EXISTS `state_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state_master` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `state_code` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`state_id`),
  KEY `IDXc7b5p3q5ngt8ng95wyu6xw2cq` (`state_id`),
  KEY `FKbit3kv24ddslslqs9sy3evpjn` (`country_id`),
  KEY `FK4ola0a47jlvlvbp1l2al4p4mh` (`lang_id`),
  CONSTRAINT `FK4ola0a47jlvlvbp1l2al4p4mh` FOREIGN KEY (`lang_id`) REFERENCES `language` (`lang_id`),
  CONSTRAINT `FKbit3kv24ddslslqs9sy3evpjn` FOREIGN KEY (`country_id`) REFERENCES `country_master` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state_master`
--

LOCK TABLES `state_master` WRITE;
/*!40000 ALTER TABLE `state_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `state_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sy03600_h`
--

DROP TABLE IF EXISTS `sy03600_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sy03600_h` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `actrowid` bigint(20) DEFAULT NULL,
  `lsttotsal` decimal(10,2) DEFAULT NULL,
  `lsttottax` decimal(10,2) DEFAULT NULL,
  `lsttotvat` decimal(10,2) DEFAULT NULL,
  `maxvatamt` decimal(10,2) DEFAULT NULL,
  `minvatamt` decimal(10,2) DEFAULT NULL,
  `basperct` decimal(10,2) DEFAULT NULL,
  `basdid` int(11) DEFAULT NULL,
  `dscriptn` char(31) DEFAULT NULL,
  `dscriptna` char(61) DEFAULT NULL,
  `vatidnum` int(11) DEFAULT NULL,
  `taxschdid` char(15) DEFAULT NULL,
  `vatyp` int(11) DEFAULT NULL,
  `ytdtotsal` decimal(10,2) DEFAULT NULL,
  `ytdtottax` decimal(10,2) DEFAULT NULL,
  `ytdtotvat` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDXgr5u4ijqc6p9gfpv8y56ihldk` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sy03600_h`
--

LOCK TABLES `sy03600_h` WRITE;
/*!40000 ALTER TABLE `sy03600_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `sy03600_h` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_flow_assign`
--

DROP TABLE IF EXISTS `work_flow_assign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_flow_assign` (
  `work_flow_assign_id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `work_flow_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`work_flow_assign_id`),
  KEY `FK37cf5pxaiden8mwtscykquieu` (`company_id`),
  KEY `FKhct3on57cvmafqpvnstvnwa8e` (`work_flow_id`),
  CONSTRAINT `FK37cf5pxaiden8mwtscykquieu` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  CONSTRAINT `FKhct3on57cvmafqpvnstvnwa8e` FOREIGN KEY (`work_flow_id`) REFERENCES `work_flow_upload` (`work_flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_flow_assign`
--

LOCK TABLES `work_flow_assign` WRITE;
/*!40000 ALTER TABLE `work_flow_assign` DISABLE KEYS */;
/*!40000 ALTER TABLE `work_flow_assign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_flow_upload`
--

DROP TABLE IF EXISTS `work_flow_upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_flow_upload` (
  `work_flow_id` int(11) NOT NULL AUTO_INCREMENT,
  `creatddt` datetime DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `dex_row_id` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `modifdt` datetime DEFAULT NULL,
  `dex_row_ts` datetime DEFAULT NULL,
  `descfile` varchar(255) DEFAULT NULL,
  `work_flow_file` longblob,
  PRIMARY KEY (`work_flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_flow_upload`
--

LOCK TABLES `work_flow_upload` WRITE;
/*!40000 ALTER TABLE `work_flow_upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `work_flow_upload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'hcm_test_final'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-19 10:53:13
